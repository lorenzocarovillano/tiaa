/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:03:07 PM
**        * FROM NATURAL PROGRAM : Adsp550
************************************************************
**        * FILE NAME            : Adsp550.java
**        * CLASS NAME           : Adsp550
**        * INSTANCE NAME        : Adsp550
************************************************************
***********************************************************************
* PROGRAM  : ADSP550
* SYSTEM   : ADAS - ANNUITIAZTION SUNGUARD
* TITLE    : ACKNOWLEDGEMENT LETTERS
* WRITTEN  : JUNE, 2006
* AUTHOR   : COPIED FROM NAZN550 AND MODIFIED BY MARINA NACHBER
*
* THIS MODULE INTERFACES WITH POST SYSTEM TO PRODUCE ACKNOWLEDGEMENT
* LETTER.
* IT READS THE PARTICIPANT FILE BY LATEST ACTIVITY DATE = PREV BUS CURR
* DATE, CHECKS IF STATUS  M/T/L AND ACKNOWLEDGEMENT IND = SPACES,
* READS THE CONTRACT FILE AND GATHERS ALL INFO FOR POST TO PRINT THE
* LETTER. IF POST IS SUCCESSFUL, UPDATES ACKNOWLEDGEMENT IND WITH
* 'Y', IF NOT SUCCESSFUL - WRITES THE ERROR REPORT
*
* THIS PROGRAM CALLS FOLLOWING MODULES;
*
*  1. ADSN102  - PRODUCT INFORMATION
*  2. ADSN545  - TAX INFORMATION
*  3. ADSN555  - BENEFICIARY INFORMATION
*  4. ADSN553  - CONVERSION OF NAME TO LOWER CASE
*  5. PSTN9610 - POST OPEN
*  6. PSTN9502 - POST WRITE (GERRY KIRK)
*  7. PSTN9680 - POST CLOSE
*********************  MAINTENANCE LOG ******************************
* 09122006 - MARINA NACHBER - ADAS RESTRUCTURE
*          - IF RTB-IND IS 'Y', REJECT THIS TRANSACTION AND DISPLAY
*            AN ERROR ON THE REPORT
*
* 10182006 - MARINA NACHBER - ADAS RESTRUCTURE
*          - REPLICATE LOGIC FROM ADSP540: FUND TYPE AND FUND QUANTITY
*            SHOULD BE UNIQUE WITHIN ONE TICKER, OTHERWISE THE WHOLE
*            RECORD GOES TO THE ERROR REPORT
*
* 11/10/2006  MARINA NACHBER - CORRECTING PRODUCTION PROBLEM
*                 CHANGED TO AVOID THE ABEND  WHEN OMNI SENDS BAD DATA -
*                 DUPLICATE FUNDS
*
* 12/11/2006  MARINA NACHBER - PRODFIX
*                            - CHANGED TO DISPLAY ON ERROR REPORT AND
*                              NOT TO PRODUCE LETTERS FOR REQUESTS WITH
*                              INVALID OPTION 'JL"
*                            - ADDED DISPLAYS FOR BENE FIELDS
* 02/02/2007  O SOTTO - CHANGES TO TAX WITHHOLDING TEXT DISPLAYED
*                       ON THE LETTER (IM378343). SC 020207.
* 02/12/2007  MARINA NACHBER - RTB
*                              ADDED RTB INFO TO THE LETTER
* 03/07/2007  MARINA NACHBER - ATRA
* 04/24/2007  NICK CVETKOVIC - POPULATE RTB ALT-ADDRESS
* 06/05/2007  GUERREV        - POPULATED PSTL0175.ADP-ROLLVR-IND
*                              BASED ON VALUE OF
*                              ADS-PRTCPNT-VIEW.ADP-ALT-DEST-RLVR-IND
* 07/24/2007  E. MELNIK        NEW ANNUITY OPTION CHANGE MARKED BY
*                              EM 072407
* 03/17/2008  E. MELNIK        ROTH 403B/401K CHANGES.  EM 031708
* 01/14/2009  R. SACHARNY      CALL ADSN888 INSTEAD OF NECN4000(CFN) RS0
* 01/27/09  E. MELNIK TIAA ACCESS/STABLE RETURN ENHANCEMENTS. MARKED
*                         BY EM - 012709.
* 04/08/10  D.E.ANDER          ADABAS REPLATFORM PCPOP. MARKED DEA
* 06/02/10  D.E.ANDER          MOVED IF STATEMENT UP FOR TIAA07-10 DEA
* 06/28/10  D.E.ANDER     EXTRA CHANGES PER E. MELNIK MARKED DEA
* 11/28/11  O SOTTO       FIXED INDEX PROBLEM. SC 112811.
* 03/01/12 E. MELNIK   RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                      UPDATED LINKAGE AREAS.
* 03/05/12  O SOTTO       FIX FOR INC1648735 - TAX ELECTION FOR
*                         DEFERRED COMP PLANS.  SC 030512.
* 10/2013   O. SOTTO  CREF REA CHANGES MARKED WITH OS-102013.
* 08/2016   E. MELNIK PIN EXPANSION CHANGES.  MARKED BY EM - 083016.
* 02/22/2017 R.CARREON PIN EXPANSION 02222017
* 9/14/2015  E. MELNIK IA REPLATFORM CHANGES.  MARKED BY EM - 091417.
*********************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp550 extends BLNatBase
{
    // Data Areas
    private LdaPstl0175 ldaPstl0175;
    private PdaAdsa555 pdaAdsa555;
    private LdaAdsl540a ldaAdsl540a;
    private LdaAdsl401a ldaAdsl401a;
    private PdaPsta9500 pdaPsta9500;
    private PdaPsta9501 pdaPsta9501;
    private PdaPsta9610 pdaPsta9610;
    private PdaPsta9612 pdaPsta9612;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaCwfpdaun pdaCwfpdaun;
    private PdaAdsa888 pdaAdsa888;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Return_Code;

    private DataAccessProgramView vw_ads_Cntl_View;
    private DbsField ads_Cntl_View_Ads_Rcrd_Cde;

    private DbsGroup ads_Cntl_View_Ads_Cntl_Grp;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte;

    private DbsGroup ads_Cntl_View__R_Field_1;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte;

    private DbsGroup ads_Cntl_View__R_Field_2;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte_A;
    private DbsField ads_Cntl_View_Ads_Rpt_13;
    private DbsField pnd_Cntl_Bsnss_Prev_Dte;
    private DbsField pnd_Cntl_Bsnss_Dte;
    private DbsField pnd_Cntl_Bsnss_Tmrrw_Dte;
    private DbsField pnd_Ads_Cntl_Super_De_1;

    private DbsGroup pnd_Ads_Cntl_Super_De_1__R_Field_3;
    private DbsField pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Rcrd_Typ;
    private DbsField pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Bsnss_Dt;

    private DbsGroup pnd_Adsn998_Error_Handler;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Calling_Program;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Unique_Id;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Step_Name;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Appl_Id;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Error_Msg;

    private DbsGroup pnd_Tax_Errors_Info;
    private DbsField pnd_Tax_Errors_Info_Pnd_Tax_Error_Code;
    private DbsField pnd_Tax_Errors_Info_Pnd_Tax_Error_Desc;

    private DbsGroup pnd_Terminate;
    private DbsField pnd_Terminate_Pnd_Terminate_No;
    private DbsField pnd_Terminate_Pnd_Terminate_Msg;

    private DbsGroup pnd_Terminate__R_Field_4;
    private DbsField pnd_Terminate_Pnd_Terminate_Msg1;
    private DbsField pnd_Terminate_Pnd_Terminate_Msg2;
    private DbsField pnd_Terminate_Pnd_Terminate_Msg3;

    private DbsGroup sort_Key;
    private DbsField sort_Key_Sort_Key_Lgth;
    private DbsField sort_Key_Sort_Key_Array;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_5;
    private DbsField pnd_Date_A_Pnd_Date_Yyyymmdd;
    private DbsField pnd_A;
    private DbsField pnd_B;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_X;
    private DbsField pnd_In;
    private DbsField pnd_Io;
    private DbsField pnd_Real_Nbr;
    private DbsField pnd_Work_Tiaa_Acct_Cde;
    private DbsField pnd_Work_Cref_Acct_Cde;
    private DbsField pnd_Adp_Isn;
    private DbsField pnd_Adc_Isn;
    private DbsField pnd_Status;
    private DbsField pnd_Letter_Info_Found;
    private DbsField pnd_Jindex_Error;
    private DbsField pnd_Debug_On;
    private DbsField pnd_Letters_Written;
    private DbsField pnd_Error_Report_Entries;
    private DbsField pnd_Counter;
    private DbsField pnd_Appl_Key_Counter;
    private DbsField pnd_Parm_Racf_Id;
    private DbsField pnd_Prod_Code_N;
    private DbsField pnd_Name;
    private DbsField pnd_Parm_Rep_Name;
    private DbsField pnd_Num;
    private DbsField pnd_Post_Error;
    private DbsField pnd_Efm_Error;
    private DbsField pnd_Cur_Da_Contract_Count;

    private DbsGroup adsn555_Beneficiary;
    private DbsField adsn555_Beneficiary_Adsn555_Designation_Text;

    private DbsGroup adsn555_Beneficiary__R_Field_6;
    private DbsField adsn555_Beneficiary_Adsn555_Designation_Txt;
    private DbsField adsn555_Beneficiary_Adsn555_Designation_Txt_1;
    private DbsField adsn555_Beneficiary_Adsn555_Designation_Text_Cont;

    private DbsGroup adsn555_Primary;
    private DbsField adsn555_Primary_Adsn555_Bene_Name;
    private DbsField adsn555_Primary_Adsn555_Allocation_Pct;

    private DbsGroup adsn555_Contingent;
    private DbsField adsn555_Contingent_Adsn555_Cont_Name;
    private DbsField adsn555_Contingent_Adsn555_Cont_Alloc_Pct;
    private DbsField pnd_C;
    private DbsField pnd_D;
    private DbsField pnd_E;
    private DbsField pnd_Fund_Info_Ok;

    private DbsGroup pnd_Ackl_Req_Info;
    private DbsField pnd_Ackl_Req_Info_Pnd_Ackl_Lttr_Tkr_Symbl;
    private DbsField pnd_Ackl_Req_Info_Pnd_Ackl_Lttr_Acct_Typ;
    private DbsField pnd_Ackl_Req_Info_Pnd_Ackl_Lttr_Acct_Qty;
    private DbsField pnd_Rtb_Fund_Info_Ok;
    private DbsField pnd_Pindex_Error;
    private DbsField pnd_Rindex_Error;
    private DbsField pnd_Rtb_Tiaa;
    private DbsField pnd_Rtb_Cref;
    private DbsField pnd_M;
    private DbsField pnd_N;
    private DbsField pnd_P;
    private DbsField pnd_R;
    private DbsField pnd_Tiaa_Acct;
    private DbsField pnd_Tiaa_Type_Acct;

    private DbsGroup pnd_Rtb_Req_Info;
    private DbsField pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Tkr_Symbl;
    private DbsField pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Acct_Typ;
    private DbsField pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Acct_Qty;

    private DataAccessProgramView vw_adat;
    private DbsField adat_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField adat_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField adat_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField adat_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField adat_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField pnd_Naz_Table_Key;

    private DbsGroup pnd_Naz_Table_Key__R_Field_7;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id;
    private DbsField pnd_Tiaa;
    private DbsField pnd_Cref;
    private DbsField pnd_Dte;

    private DbsGroup pnd_Dte__R_Field_8;
    private DbsField pnd_Dte_Pnd_Dte_N;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaPstl0175 = new LdaPstl0175();
        registerRecord(ldaPstl0175);
        localVariables = new DbsRecord();
        pdaAdsa555 = new PdaAdsa555(localVariables);
        ldaAdsl540a = new LdaAdsl540a();
        registerRecord(ldaAdsl540a);
        registerRecord(ldaAdsl540a.getVw_ads_Prtcpnt_View());
        ldaAdsl401a = new LdaAdsl401a();
        registerRecord(ldaAdsl401a);
        registerRecord(ldaAdsl401a.getVw_ads_Cntrct_View());
        pdaPsta9500 = new PdaPsta9500(localVariables);
        pdaPsta9501 = new PdaPsta9501(localVariables);
        pdaPsta9610 = new PdaPsta9610(localVariables);
        pdaPsta9612 = new PdaPsta9612(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfpdaus = new PdaCwfpdaus(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);
        pdaAdsa888 = new PdaAdsa888(localVariables);

        // Local Variables
        pnd_Return_Code = localVariables.newFieldInRecord("pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 1);

        vw_ads_Cntl_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntl_View", "ADS-CNTL-VIEW"), "ADS_CNTL", "ADS_CNTL");
        ads_Cntl_View_Ads_Rcrd_Cde = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Rcrd_Cde", "ADS-RCRD-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ADS_RCRD_CDE");

        ads_Cntl_View_Ads_Cntl_Grp = vw_ads_Cntl_View.getRecord().newGroupInGroup("ADS_CNTL_VIEW_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_DTE");

        ads_Cntl_View__R_Field_1 = ads_Cntl_View_Ads_Cntl_Grp.newGroupInGroup("ads_Cntl_View__R_Field_1", "REDEFINE", ads_Cntl_View_Ads_Cntl_Bsnss_Dte);
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A = ads_Cntl_View__R_Field_1.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A", "ADS-CNTL-BSNSS-DTE-A", FieldType.STRING, 
            8);
        ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte", "ADS-CNTL-BSNSS-TMRRW-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_TMRRW_DTE");

        ads_Cntl_View__R_Field_2 = ads_Cntl_View_Ads_Cntl_Grp.newGroupInGroup("ads_Cntl_View__R_Field_2", "REDEFINE", ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte);
        ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte_A = ads_Cntl_View__R_Field_2.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte_A", "ADS-CNTL-BSNSS-TMRRW-DTE-A", 
            FieldType.STRING, 8);
        ads_Cntl_View_Ads_Rpt_13 = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Rpt_13", "ADS-RPT-13", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADS_RPT_13");
        registerRecord(vw_ads_Cntl_View);

        pnd_Cntl_Bsnss_Prev_Dte = localVariables.newFieldInRecord("pnd_Cntl_Bsnss_Prev_Dte", "#CNTL-BSNSS-PREV-DTE", FieldType.DATE);
        pnd_Cntl_Bsnss_Dte = localVariables.newFieldInRecord("pnd_Cntl_Bsnss_Dte", "#CNTL-BSNSS-DTE", FieldType.DATE);
        pnd_Cntl_Bsnss_Tmrrw_Dte = localVariables.newFieldInRecord("pnd_Cntl_Bsnss_Tmrrw_Dte", "#CNTL-BSNSS-TMRRW-DTE", FieldType.DATE);
        pnd_Ads_Cntl_Super_De_1 = localVariables.newFieldInRecord("pnd_Ads_Cntl_Super_De_1", "#ADS-CNTL-SUPER-DE-1", FieldType.STRING, 9);

        pnd_Ads_Cntl_Super_De_1__R_Field_3 = localVariables.newGroupInRecord("pnd_Ads_Cntl_Super_De_1__R_Field_3", "REDEFINE", pnd_Ads_Cntl_Super_De_1);
        pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Rcrd_Typ = pnd_Ads_Cntl_Super_De_1__R_Field_3.newFieldInGroup("pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Rcrd_Typ", 
            "#ADS-CNTL-RCRD-TYP", FieldType.STRING, 1);
        pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Bsnss_Dt = pnd_Ads_Cntl_Super_De_1__R_Field_3.newFieldInGroup("pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Bsnss_Dt", 
            "#ADS-CNTL-BSNSS-DT", FieldType.NUMERIC, 8);

        pnd_Adsn998_Error_Handler = localVariables.newGroupInRecord("pnd_Adsn998_Error_Handler", "#ADSN998-ERROR-HANDLER");
        pnd_Adsn998_Error_Handler_Pnd_Calling_Program = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Calling_Program", "#CALLING-PROGRAM", 
            FieldType.STRING, 8);
        pnd_Adsn998_Error_Handler_Pnd_Unique_Id = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Unique_Id", "#UNIQUE-ID", FieldType.NUMERIC, 
            12);
        pnd_Adsn998_Error_Handler_Pnd_Step_Name = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Step_Name", "#STEP-NAME", FieldType.STRING, 
            8);
        pnd_Adsn998_Error_Handler_Pnd_Appl_Id = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Appl_Id", "#APPL-ID", FieldType.STRING, 
            8);
        pnd_Adsn998_Error_Handler_Pnd_Error_Msg = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 
            79);

        pnd_Tax_Errors_Info = localVariables.newGroupInRecord("pnd_Tax_Errors_Info", "#TAX-ERRORS-INFO");
        pnd_Tax_Errors_Info_Pnd_Tax_Error_Code = pnd_Tax_Errors_Info.newFieldInGroup("pnd_Tax_Errors_Info_Pnd_Tax_Error_Code", "#TAX-ERROR-CODE", FieldType.STRING, 
            4);
        pnd_Tax_Errors_Info_Pnd_Tax_Error_Desc = pnd_Tax_Errors_Info.newFieldInGroup("pnd_Tax_Errors_Info_Pnd_Tax_Error_Desc", "#TAX-ERROR-DESC", FieldType.STRING, 
            44);

        pnd_Terminate = localVariables.newGroupInRecord("pnd_Terminate", "#TERMINATE");
        pnd_Terminate_Pnd_Terminate_No = pnd_Terminate.newFieldInGroup("pnd_Terminate_Pnd_Terminate_No", "#TERMINATE-NO", FieldType.PACKED_DECIMAL, 3);
        pnd_Terminate_Pnd_Terminate_Msg = pnd_Terminate.newFieldInGroup("pnd_Terminate_Pnd_Terminate_Msg", "#TERMINATE-MSG", FieldType.STRING, 177);

        pnd_Terminate__R_Field_4 = pnd_Terminate.newGroupInGroup("pnd_Terminate__R_Field_4", "REDEFINE", pnd_Terminate_Pnd_Terminate_Msg);
        pnd_Terminate_Pnd_Terminate_Msg1 = pnd_Terminate__R_Field_4.newFieldInGroup("pnd_Terminate_Pnd_Terminate_Msg1", "#TERMINATE-MSG1", FieldType.STRING, 
            59);
        pnd_Terminate_Pnd_Terminate_Msg2 = pnd_Terminate__R_Field_4.newFieldInGroup("pnd_Terminate_Pnd_Terminate_Msg2", "#TERMINATE-MSG2", FieldType.STRING, 
            59);
        pnd_Terminate_Pnd_Terminate_Msg3 = pnd_Terminate__R_Field_4.newFieldInGroup("pnd_Terminate_Pnd_Terminate_Msg3", "#TERMINATE-MSG3", FieldType.STRING, 
            59);

        sort_Key = localVariables.newGroupInRecord("sort_Key", "SORT-KEY");
        sort_Key_Sort_Key_Lgth = sort_Key.newFieldInGroup("sort_Key_Sort_Key_Lgth", "SORT-KEY-LGTH", FieldType.NUMERIC, 3);
        sort_Key_Sort_Key_Array = sort_Key.newFieldArrayInGroup("sort_Key_Sort_Key_Array", "SORT-KEY-ARRAY", FieldType.STRING, 1, new DbsArrayController(1, 
            3));
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_5 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_5", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_Yyyymmdd = pnd_Date_A__R_Field_5.newFieldInGroup("pnd_Date_A_Pnd_Date_Yyyymmdd", "#DATE-YYYYMMDD", FieldType.NUMERIC, 8);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);
        pnd_In = localVariables.newFieldInRecord("pnd_In", "#IN", FieldType.PACKED_DECIMAL, 5);
        pnd_Io = localVariables.newFieldInRecord("pnd_Io", "#IO", FieldType.PACKED_DECIMAL, 5);
        pnd_Real_Nbr = localVariables.newFieldInRecord("pnd_Real_Nbr", "#REAL-NBR", FieldType.STRING, 12);
        pnd_Work_Tiaa_Acct_Cde = localVariables.newFieldInRecord("pnd_Work_Tiaa_Acct_Cde", "#WORK-TIAA-ACCT-CDE", FieldType.STRING, 2);
        pnd_Work_Cref_Acct_Cde = localVariables.newFieldInRecord("pnd_Work_Cref_Acct_Cde", "#WORK-CREF-ACCT-CDE", FieldType.STRING, 2);
        pnd_Adp_Isn = localVariables.newFieldInRecord("pnd_Adp_Isn", "#ADP-ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Adc_Isn = localVariables.newFieldInRecord("pnd_Adc_Isn", "#ADC-ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Status = localVariables.newFieldInRecord("pnd_Status", "#STATUS", FieldType.STRING, 3);
        pnd_Letter_Info_Found = localVariables.newFieldInRecord("pnd_Letter_Info_Found", "#LETTER-INFO-FOUND", FieldType.BOOLEAN, 1);
        pnd_Jindex_Error = localVariables.newFieldInRecord("pnd_Jindex_Error", "#JINDEX-ERROR", FieldType.BOOLEAN, 1);
        pnd_Debug_On = localVariables.newFieldInRecord("pnd_Debug_On", "#DEBUG-ON", FieldType.BOOLEAN, 1);
        pnd_Letters_Written = localVariables.newFieldInRecord("pnd_Letters_Written", "#LETTERS-WRITTEN", FieldType.PACKED_DECIMAL, 4);
        pnd_Error_Report_Entries = localVariables.newFieldInRecord("pnd_Error_Report_Entries", "#ERROR-REPORT-ENTRIES", FieldType.PACKED_DECIMAL, 7);
        pnd_Counter = localVariables.newFieldInRecord("pnd_Counter", "#COUNTER", FieldType.PACKED_DECIMAL, 4);
        pnd_Appl_Key_Counter = localVariables.newFieldInRecord("pnd_Appl_Key_Counter", "#APPL-KEY-COUNTER", FieldType.NUMERIC, 5);
        pnd_Parm_Racf_Id = localVariables.newFieldInRecord("pnd_Parm_Racf_Id", "#PARM-RACF-ID", FieldType.STRING, 8);
        pnd_Prod_Code_N = localVariables.newFieldArrayInRecord("pnd_Prod_Code_N", "#PROD-CODE-N", FieldType.STRING, 2, new DbsArrayController(1, 20));
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 50);
        pnd_Parm_Rep_Name = localVariables.newFieldInRecord("pnd_Parm_Rep_Name", "#PARM-REP-NAME", FieldType.STRING, 35);
        pnd_Num = localVariables.newFieldInRecord("pnd_Num", "#NUM", FieldType.PACKED_DECIMAL, 7);
        pnd_Post_Error = localVariables.newFieldInRecord("pnd_Post_Error", "#POST-ERROR", FieldType.STRING, 1);
        pnd_Efm_Error = localVariables.newFieldInRecord("pnd_Efm_Error", "#EFM-ERROR", FieldType.STRING, 1);
        pnd_Cur_Da_Contract_Count = localVariables.newFieldInRecord("pnd_Cur_Da_Contract_Count", "#CUR-DA-CONTRACT-COUNT", FieldType.PACKED_DECIMAL, 3);

        adsn555_Beneficiary = localVariables.newGroupInRecord("adsn555_Beneficiary", "ADSN555-BENEFICIARY");
        adsn555_Beneficiary_Adsn555_Designation_Text = adsn555_Beneficiary.newFieldArrayInGroup("adsn555_Beneficiary_Adsn555_Designation_Text", "ADSN555-DESIGNATION-TEXT", 
            FieldType.STRING, 72, new DbsArrayController(1, 60));

        adsn555_Beneficiary__R_Field_6 = adsn555_Beneficiary.newGroupInGroup("adsn555_Beneficiary__R_Field_6", "REDEFINE", adsn555_Beneficiary_Adsn555_Designation_Text);
        adsn555_Beneficiary_Adsn555_Designation_Txt = adsn555_Beneficiary__R_Field_6.newFieldArrayInGroup("adsn555_Beneficiary_Adsn555_Designation_Txt", 
            "ADSN555-DESIGNATION-TXT", FieldType.STRING, 72, new DbsArrayController(1, 4, 1, 13));
        adsn555_Beneficiary_Adsn555_Designation_Txt_1 = adsn555_Beneficiary__R_Field_6.newFieldArrayInGroup("adsn555_Beneficiary_Adsn555_Designation_Txt_1", 
            "ADSN555-DESIGNATION-TXT-1", FieldType.STRING, 72, new DbsArrayController(1, 8));
        adsn555_Beneficiary_Adsn555_Designation_Text_Cont = adsn555_Beneficiary.newFieldArrayInGroup("adsn555_Beneficiary_Adsn555_Designation_Text_Cont", 
            "ADSN555-DESIGNATION-TEXT-CONT", FieldType.STRING, 72, new DbsArrayController(1, 60));

        adsn555_Primary = localVariables.newGroupInRecord("adsn555_Primary", "ADSN555-PRIMARY");
        adsn555_Primary_Adsn555_Bene_Name = adsn555_Primary.newFieldArrayInGroup("adsn555_Primary_Adsn555_Bene_Name", "ADSN555-BENE-NAME", FieldType.STRING, 
            35, new DbsArrayController(1, 20));
        adsn555_Primary_Adsn555_Allocation_Pct = adsn555_Primary.newFieldArrayInGroup("adsn555_Primary_Adsn555_Allocation_Pct", "ADSN555-ALLOCATION-PCT", 
            FieldType.STRING, 30, new DbsArrayController(1, 20));

        adsn555_Contingent = localVariables.newGroupInRecord("adsn555_Contingent", "ADSN555-CONTINGENT");
        adsn555_Contingent_Adsn555_Cont_Name = adsn555_Contingent.newFieldArrayInGroup("adsn555_Contingent_Adsn555_Cont_Name", "ADSN555-CONT-NAME", FieldType.STRING, 
            35, new DbsArrayController(1, 20));
        adsn555_Contingent_Adsn555_Cont_Alloc_Pct = adsn555_Contingent.newFieldArrayInGroup("adsn555_Contingent_Adsn555_Cont_Alloc_Pct", "ADSN555-CONT-ALLOC-PCT", 
            FieldType.STRING, 30, new DbsArrayController(1, 20));
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.PACKED_DECIMAL, 3);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.PACKED_DECIMAL, 3);
        pnd_E = localVariables.newFieldInRecord("pnd_E", "#E", FieldType.PACKED_DECIMAL, 3);
        pnd_Fund_Info_Ok = localVariables.newFieldInRecord("pnd_Fund_Info_Ok", "#FUND-INFO-OK", FieldType.BOOLEAN, 1);

        pnd_Ackl_Req_Info = localVariables.newGroupArrayInRecord("pnd_Ackl_Req_Info", "#ACKL-REQ-INFO", new DbsArrayController(1, 20));
        pnd_Ackl_Req_Info_Pnd_Ackl_Lttr_Tkr_Symbl = pnd_Ackl_Req_Info.newFieldInGroup("pnd_Ackl_Req_Info_Pnd_Ackl_Lttr_Tkr_Symbl", "#ACKL-LTTR-TKR-SYMBL", 
            FieldType.STRING, 10);
        pnd_Ackl_Req_Info_Pnd_Ackl_Lttr_Acct_Typ = pnd_Ackl_Req_Info.newFieldInGroup("pnd_Ackl_Req_Info_Pnd_Ackl_Lttr_Acct_Typ", "#ACKL-LTTR-ACCT-TYP", 
            FieldType.STRING, 1);
        pnd_Ackl_Req_Info_Pnd_Ackl_Lttr_Acct_Qty = pnd_Ackl_Req_Info.newFieldInGroup("pnd_Ackl_Req_Info_Pnd_Ackl_Lttr_Acct_Qty", "#ACKL-LTTR-ACCT-QTY", 
            FieldType.NUMERIC, 15, 6);
        pnd_Rtb_Fund_Info_Ok = localVariables.newFieldInRecord("pnd_Rtb_Fund_Info_Ok", "#RTB-FUND-INFO-OK", FieldType.BOOLEAN, 1);
        pnd_Pindex_Error = localVariables.newFieldInRecord("pnd_Pindex_Error", "#PINDEX-ERROR", FieldType.BOOLEAN, 1);
        pnd_Rindex_Error = localVariables.newFieldInRecord("pnd_Rindex_Error", "#RINDEX-ERROR", FieldType.BOOLEAN, 1);
        pnd_Rtb_Tiaa = localVariables.newFieldInRecord("pnd_Rtb_Tiaa", "#RTB-TIAA", FieldType.PACKED_DECIMAL, 3);
        pnd_Rtb_Cref = localVariables.newFieldInRecord("pnd_Rtb_Cref", "#RTB-CREF", FieldType.PACKED_DECIMAL, 3);
        pnd_M = localVariables.newFieldInRecord("pnd_M", "#M", FieldType.PACKED_DECIMAL, 3);
        pnd_N = localVariables.newFieldInRecord("pnd_N", "#N", FieldType.PACKED_DECIMAL, 3);
        pnd_P = localVariables.newFieldInRecord("pnd_P", "#P", FieldType.PACKED_DECIMAL, 3);
        pnd_R = localVariables.newFieldInRecord("pnd_R", "#R", FieldType.PACKED_DECIMAL, 3);
        pnd_Tiaa_Acct = localVariables.newFieldArrayInRecord("pnd_Tiaa_Acct", "#TIAA-ACCT", FieldType.STRING, 4, new DbsArrayController(1, 3));
        pnd_Tiaa_Type_Acct = localVariables.newFieldArrayInRecord("pnd_Tiaa_Type_Acct", "#TIAA-TYPE-ACCT", FieldType.STRING, 4, new DbsArrayController(1, 
            6));

        pnd_Rtb_Req_Info = localVariables.newGroupArrayInRecord("pnd_Rtb_Req_Info", "#RTB-REQ-INFO", new DbsArrayController(1, 14));
        pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Tkr_Symbl = pnd_Rtb_Req_Info.newFieldInGroup("pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Tkr_Symbl", "#RTB-LTTR-TKR-SYMBL", FieldType.STRING, 
            10);
        pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Acct_Typ = pnd_Rtb_Req_Info.newFieldInGroup("pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Acct_Typ", "#RTB-LTTR-ACCT-TYP", FieldType.STRING, 
            1);
        pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Acct_Qty = pnd_Rtb_Req_Info.newFieldInGroup("pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Acct_Qty", "#RTB-LTTR-ACCT-QTY", FieldType.NUMERIC, 
            15, 6);

        vw_adat = new DataAccessProgramView(new NameInfo("vw_adat", "ADAT"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        adat_Naz_Tbl_Rcrd_Actv_Ind = vw_adat.getRecord().newFieldInGroup("adat_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "NAZ_TBL_RCRD_ACTV_IND");
        adat_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        adat_Naz_Tbl_Rcrd_Typ_Ind = vw_adat.getRecord().newFieldInGroup("adat_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "NAZ_TBL_RCRD_TYP_IND");
        adat_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        adat_Naz_Tbl_Rcrd_Lvl1_Id = vw_adat.getRecord().newFieldInGroup("adat_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "NAZ_TBL_RCRD_LVL1_ID");
        adat_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        adat_Naz_Tbl_Rcrd_Lvl2_Id = vw_adat.getRecord().newFieldInGroup("adat_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "NAZ_TBL_RCRD_LVL2_ID");
        adat_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        adat_Naz_Tbl_Rcrd_Lvl3_Id = vw_adat.getRecord().newFieldInGroup("adat_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "NAZ_TBL_RCRD_LVL3_ID");
        adat_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        registerRecord(vw_adat);

        pnd_Naz_Table_Key = localVariables.newFieldInRecord("pnd_Naz_Table_Key", "#NAZ-TABLE-KEY", FieldType.STRING, 30);

        pnd_Naz_Table_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Naz_Table_Key__R_Field_7", "REDEFINE", pnd_Naz_Table_Key);
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind = pnd_Naz_Table_Key__R_Field_7.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind", "#NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id = pnd_Naz_Table_Key__R_Field_7.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id", "#NAZ-TABLE-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id = pnd_Naz_Table_Key__R_Field_7.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id", "#NAZ-TABLE-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id = pnd_Naz_Table_Key__R_Field_7.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id", "#NAZ-TABLE-LVL3-ID", 
            FieldType.STRING, 20);
        pnd_Tiaa = localVariables.newFieldInRecord("pnd_Tiaa", "#TIAA", FieldType.PACKED_DECIMAL, 3);
        pnd_Cref = localVariables.newFieldInRecord("pnd_Cref", "#CREF", FieldType.PACKED_DECIMAL, 3);
        pnd_Dte = localVariables.newFieldInRecord("pnd_Dte", "#DTE", FieldType.STRING, 8);

        pnd_Dte__R_Field_8 = localVariables.newGroupInRecord("pnd_Dte__R_Field_8", "REDEFINE", pnd_Dte);
        pnd_Dte_Pnd_Dte_N = pnd_Dte__R_Field_8.newFieldInGroup("pnd_Dte_Pnd_Dte_N", "#DTE-N", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Cntl_View.reset();
        vw_adat.reset();

        ldaPstl0175.initializeValues();
        ldaAdsl540a.initializeValues();
        ldaAdsl401a.initializeValues();

        localVariables.reset();
        sort_Key_Sort_Key_Lgth.setInitialValue(1);
        pnd_Tiaa_Acct.getValue(1).setInitialValue("TIA0");
        pnd_Tiaa_Acct.getValue(2).setInitialValue("TSV0");
        pnd_Tiaa_Acct.getValue(3).setInitialValue("TSA0");
        pnd_Tiaa_Type_Acct.getValue(1).setInitialValue("TIA0");
        pnd_Tiaa_Type_Acct.getValue(2).setInitialValue("TSV0");
        pnd_Tiaa_Type_Acct.getValue(3).setInitialValue("TSA0");
        pnd_Tiaa_Type_Acct.getValue(4).setInitialValue("REA1");
        pnd_Tiaa_Type_Acct.getValue(5).setInitialValue("REX1");
        pnd_Tiaa_Type_Acct.getValue(6).setInitialValue("WA50");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp550() throws Exception
    {
        super("Adsp550");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Adsp550|Main");
        OnErrorManager.pushEvent("ADSP550", onError);
        setupReports();
        while(true)
        {
            try
            {
                getReports().write(1, new TabSetting(1),Global.getPROGRAM(),new TabSetting(50),"ANNUITIZATION OF OMNI PLUS CONTRACTS",new TabSetting(110),"Run Date:",    //Natural: FORMAT ( 1 ) LS = 132 PS = 60;//Natural: WRITE ( 1 ) 001T *PROGRAM 050T 'ANNUITIZATION OF OMNI PLUS CONTRACTS' 110T 'Run Date:' ( BLI ) 120T *DATX ( AD = OD CD = NE ) / 050T 'ACKNOWLEDGEMENT LETTERS RUN REPORT' 110T 'Page No :' 120T *PAGE-NUMBER ( AD = OD CD = NE ZP = ON ) // 005T 'PIN' 17T 'EFFECTIVE' 053T 'ERROR MESSAGES' / 19T 'DATE' //
                    Color.blue, new FieldAttributes ("AD=I"),new TabSetting(120),Global.getDATX(), new FieldAttributes ("AD=OD"), Color.white,NEWLINE,new 
                    TabSetting(50),"ACKNOWLEDGEMENT LETTERS RUN REPORT",new TabSetting(110),"Page No :",new TabSetting(120),getReports().getPageNumberDbs(0), 
                    new FieldAttributes ("AD=OD"), Color.white, new ReportZeroPrint (true),NEWLINE,NEWLINE,new TabSetting(5),"PIN",new TabSetting(17),"EFFECTIVE",new 
                    TabSetting(53),"ERROR MESSAGES",NEWLINE,new TabSetting(19),"DATE",NEWLINE,NEWLINE);
                if (Global.isEscape()) return;
                //*  WRITE (1) TITLE LEFT JUSTIFIED
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Debug_On);                                                                                         //Natural: INPUT #DEBUG-ON
                //*  020207
                //*  020207
                //*  020207
                                                                                                                                                                          //Natural: PERFORM GET-FUND-TICKERS
                sub_Get_Fund_Tickers();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                 //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
                pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ067");                                                                                               //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ067'
                pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("ADS");                                                                                                  //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'ADS'
                pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Rcrd_Typ.setValue("F");                                                                                              //Natural: ASSIGN #ADS-CNTL-RCRD-TYP = 'F'
                pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Bsnss_Dt.reset();                                                                                                    //Natural: RESET #ADS-CNTL-BSNSS-DT
                vw_ads_Cntl_View.startDatabaseRead                                                                                                                        //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM #ADS-CNTL-SUPER-DE-1
                (
                "READ01",
                new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", pnd_Ads_Cntl_Super_De_1, WcType.BY) },
                new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
                2
                );
                READ01:
                while (condition(vw_ads_Cntl_View.readNextRow("READ01")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                //*  MOVE EDITED ADS-CNTL-BSNSS-DTE-A TO #CNTL-BSNSS-PREV-DTE
                //*    (EM=YYYYMMDD)
                //*  WRITE '=' ADS-CNTL-BSNSS-DTE
                if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                             //Natural: IF ADS-RPT-13 NE 0
                {
                    pnd_Cntl_Bsnss_Prev_Dte.setValue(ads_Cntl_View_Ads_Rpt_13);                                                                                           //Natural: MOVE ADS-RPT-13 TO #CNTL-BSNSS-PREV-DTE
                    getReports().write(0, "End-of-Month Business date used=",pnd_Cntl_Bsnss_Prev_Dte);                                                                    //Natural: WRITE 'End-of-Month Business date used=' #CNTL-BSNSS-PREV-DTE
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cntl_Bsnss_Prev_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A);                                            //Natural: MOVE EDITED ADS-CNTL-BSNSS-DTE-A TO #CNTL-BSNSS-PREV-DTE ( EM = YYYYMMDD )
                    getReports().write(0, "Regular Business date used=",pnd_Cntl_Bsnss_Prev_Dte);                                                                         //Natural: WRITE 'Regular Business date used=' #CNTL-BSNSS-PREV-DTE
                    if (Global.isEscape()) return;
                    //*  FOR POST
                }                                                                                                                                                         //Natural: END-IF
                pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                     //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
                ldaPstl0175.getPstl0175_Pstl0175_Rec_Id().setValue("AA");                                                                                                 //Natural: MOVE 'AA' TO PSTL0175-REC-ID
                //*  MOVE 23181    TO PSTL0175-DATA-LGTH   /* 02122007  MN
                //*  CALLNAT 'ADSN102' ADSN102-DATA-AREA
                ldaAdsl540a.getVw_ads_Prtcpnt_View().startDatabaseRead                                                                                                    //Natural: READ ADS-PRTCPNT-VIEW BY ADP-LST-ACTVTY-DTE = #CNTL-BSNSS-PREV-DTE
                (
                "READ02",
                new Wc[] { new Wc("ADP_LST_ACTVTY_DTE", ">=", pnd_Cntl_Bsnss_Prev_Dte, WcType.BY) },
                new Oc[] { new Oc("ADP_LST_ACTVTY_DTE", "ASC") }
                );
                READ02:
                while (condition(ldaAdsl540a.getVw_ads_Prtcpnt_View().readNextRow("READ02")))
                {
                    if (condition(pnd_Debug_On.getBoolean()))                                                                                                             //Natural: IF #DEBUG-ON
                    {
                        getReports().write(0, NEWLINE,"*");                                                                                                               //Natural: WRITE / '*'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "Read ADS-PRTCPNT loop ","=",ldaAdsl540a.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte());                                         //Natural: WRITE 'Read ADS-PRTCPNT loop ' '=' ADP-LST-ACTVTY-DTE
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "=",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                                                                             //Natural: WRITE '=' ADS-PRTCPNT-VIEW.RQST-ID
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "=",ldaAdsl540a.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte(), new ReportEditMask ("YYYYMMDD"));                                 //Natural: WRITE '=' ADP-LST-ACTVTY-DTE ( EM = YYYYMMDD )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "=",ldaAdsl540a.getAds_Prtcpnt_View_Adp_Effctv_Dte(), new ReportEditMask ("YYYYMMDD"));                                     //Natural: WRITE '=' ADP-EFFCTV-DTE ( EM = YYYYMMDD )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "=",ldaAdsl540a.getAds_Prtcpnt_View_Adp_Ackl_Lttr_Ind());                                                                   //Natural: WRITE '=' ADP-ACKL-LTTR-IND
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "=",ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde());                                                                    //Natural: WRITE '=' ADP-ANNT-TYP-CDE
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "=",ldaAdsl540a.getAds_Prtcpnt_View_Adp_Stts_Cde());                                                                        //Natural: WRITE '=' ADP-STTS-CDE
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "ISN: ",Global.getAstISN());                                                                                                //Natural: WRITE '=' *ISN
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte().greater(pnd_Cntl_Bsnss_Prev_Dte)))                                                 //Natural: IF ADP-LST-ACTVTY-DTE > #CNTL-BSNSS-PREV-DTE
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    //*  DEA
                    //*  DEA
                    if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Plan_Nbr().equals("TIAA07") || ldaAdsl540a.getAds_Prtcpnt_View_Adp_Plan_Nbr().equals("TIAA09")      //Natural: IF ADP-PLAN-NBR = 'TIAA07' OR = 'TIAA09' OR = 'TIAA10' OR = 'TIAA08'
                        || ldaAdsl540a.getAds_Prtcpnt_View_Adp_Plan_Nbr().equals("TIAA10") || ldaAdsl540a.getAds_Prtcpnt_View_Adp_Plan_Nbr().equals("TIAA08")))
                    {
                        //*    WRITE 'INTO THE IF STATEMENT' '=' ADP-PLAN-NBR
                        getReports().write(1, ldaAdsl540a.getAds_Prtcpnt_View_Adp_Unique_Id(),new ColumnSpacing(3),ldaAdsl540a.getAds_Prtcpnt_View_Adp_Effctv_Dte(),      //Natural: WRITE ( 1 ) ADP-UNIQUE-ID 3X ADP-EFFCTV-DTE ( EM = MM/DD/YY ) 3X ADP-STTS-CDE 36T 'NO LETTER PRODUCED, PLAN IS ' ADP-PLAN-NBR
                            new ReportEditMask ("MM/DD/YY"),new ColumnSpacing(3),ldaAdsl540a.getAds_Prtcpnt_View_Adp_Stts_Cde(),new TabSetting(36),"NO LETTER PRODUCED, PLAN IS ",
                            ldaAdsl540a.getAds_Prtcpnt_View_Adp_Plan_Nbr());
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Error_Report_Entries.nadd(1);                                                                                                                 //Natural: ADD 1 TO #ERROR-REPORT-ENTRIES
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  EM - 091417
                        if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Ackl_Lttr_Ind().equals(" ") && ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde().notEquals("R")  //Natural: IF ADP-ACKL-LTTR-IND = ' ' AND ADP-ANNT-TYP-CDE NE 'R' AND ( ADP-SBJCT-TO-WVR-IND NE 'A' ) AND ( SUBSTRING ( ADP-STTS-CDE,1,1 ) = 'M' OR SUBSTRING ( ADP-STTS-CDE,1,1 ) = 'T' OR SUBSTRING ( ADP-STTS-CDE,1,1 ) = 'L' )
                            && (ldaAdsl540a.getAds_Prtcpnt_View_Adp_Sbjct_To_Wvr_Ind().notEquals("A")) && (ldaAdsl540a.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,1).equals("M") 
                            || ldaAdsl540a.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,1).equals("T") || ldaAdsl540a.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,
                            1).equals("L"))))
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,1).equals("E") || ldaAdsl540a.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,1).equals("D")  //Natural: IF SUBSTRING ( ADP-STTS-CDE,1,1 ) EQ 'E' OR SUBSTRING ( ADP-STTS-CDE,1,1 ) EQ 'D' OR SUBSTRING ( ADP-STTS-CDE,1,1 ) EQ 'F' OR SUBSTRING ( ADP-STTS-CDE,1,1 ) EQ 'W'
                                || ldaAdsl540a.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,1).equals("F") || ldaAdsl540a.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,
                                1).equals("W")))
                            {
                                getReports().write(1, ldaAdsl540a.getAds_Prtcpnt_View_Adp_Unique_Id(),new ColumnSpacing(3),ldaAdsl540a.getAds_Prtcpnt_View_Adp_Effctv_Dte(),  //Natural: WRITE ( 1 ) ADP-UNIQUE-ID 3X ADP-EFFCTV-DTE ( EM = MM/DD/YYYY ) 36T 'NO LETTER PRODUCED, STATUS CODE IS ' ADP-STTS-CDE
                                    new ReportEditMask ("MM/DD/YYYY"),new TabSetting(36),"NO LETTER PRODUCED, STATUS CODE IS ",ldaAdsl540a.getAds_Prtcpnt_View_Adp_Stts_Cde());
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                //*          3X ADP-STTS-CDE
                                pnd_Error_Report_Entries.nadd(1);                                                                                                         //Natural: ADD 1 TO #ERROR-REPORT-ENTRIES
                                if (condition(true)) continue;                                                                                                            //Natural: ESCAPE TOP
                            }                                                                                                                                             //Natural: END-IF
                            getReports().write(0, "AFTER THE NEW INPUT AND ESCAPING TOP");                                                                                //Natural: WRITE 'AFTER THE NEW INPUT AND ESCAPING TOP'
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //* * 02122007  MN   START  -  TAKING OUT THIS RESTRICTION
                    //* * 09122006  MN
                    //* * IF ADP-RTB-IND = 'Y'
                    //* *    WRITE (1) ADP-UNIQUE-ID
                    //* *      3X ADP-EFFCTV-DTE(EM=MM/DD/YYYY)
                    //* *      36T   'No letter produced, this request has RTB information'
                    //* *    ADD 1 TO  #ERROR-REPORT-ENTRIES
                    //* *    ESCAPE TOP
                    //* *  END-IF
                    //* **** 09122006 END
                    //* * 02122007  MN   END
                    //* **** 12112006 START
                    if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("JL")))                                                                         //Natural: IF ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'JL'
                    {
                        getReports().write(1, ldaAdsl540a.getAds_Prtcpnt_View_Adp_Unique_Id(),new ColumnSpacing(3),ldaAdsl540a.getAds_Prtcpnt_View_Adp_Effctv_Dte(),      //Natural: WRITE ( 1 ) ADP-UNIQUE-ID 3X ADP-EFFCTV-DTE ( EM = MM/DD/YYYY ) 36T 'No letter produced, this request has invalid annuity opt (JL)'
                            new ReportEditMask ("MM/DD/YYYY"),new TabSetting(36),"No letter produced, this request has invalid annuity opt (JL)");
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Error_Report_Entries.nadd(1);                                                                                                                 //Natural: ADD 1 TO #ERROR-REPORT-ENTRIES
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //* **** 12112006 END
                    pnd_Adp_Isn.setValue(ldaAdsl540a.getVw_ads_Prtcpnt_View().getAstISN("Read02"));                                                                       //Natural: ASSIGN #ADP-ISN := *ISN
                    if (condition(pnd_Debug_On.getBoolean()))                                                                                                             //Natural: IF #DEBUG-ON
                    {
                        getReports().write(0, NEWLINE,"*");                                                                                                               //Natural: WRITE / '*'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "Participant record for REQ id: ",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                                               //Natural: WRITE 'Participant record for REQ id: ' ADS-PRTCPNT-VIEW.RQST-ID
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "Reading ADS-CNTRCT file ...");                                                                                             //Natural: WRITE 'Reading ADS-CNTRCT file ...'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "=",pnd_Adp_Isn);                                                                                                           //Natural: WRITE '=' #ADP-ISN
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  MN 11102006
                    pnd_Jindex_Error.reset();                                                                                                                             //Natural: RESET #JINDEX-ERROR
                    //*  MN 02122007
                    pnd_Pindex_Error.reset();                                                                                                                             //Natural: RESET #PINDEX-ERROR
                    //*  MN 02122007
                    pnd_Rindex_Error.reset();                                                                                                                             //Natural: RESET #RINDEX-ERROR
                    pnd_Letter_Info_Found.reset();                                                                                                                        //Natural: RESET #LETTER-INFO-FOUND
                    //*  020207
                    //*  021207  MN
                    pnd_Tiaa.reset();                                                                                                                                     //Natural: RESET #TIAA #CREF #RTB-TIAA #RTB-CREF
                    pnd_Cref.reset();
                    pnd_Rtb_Tiaa.reset();
                    pnd_Rtb_Cref.reset();
                    ldaAdsl401a.getVw_ads_Cntrct_View().startDatabaseFind                                                                                                 //Natural: FIND ADS-CNTRCT-VIEW WITH ADS-CNTRCT-VIEW.RQST-ID = ADS-PRTCPNT-VIEW.RQST-ID
                    (
                    "PND_PND_L0840",
                    new Wc[] { new Wc("RQST_ID", "=", ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id(), WcType.WITH) }
                    );
                    PND_PND_L0840:
                    while (condition(ldaAdsl401a.getVw_ads_Cntrct_View().readNextRow("PND_PND_L0840", true)))
                    {
                        ldaAdsl401a.getVw_ads_Cntrct_View().setIfNotFoundControlFlag(false);
                        if (condition(ldaAdsl401a.getVw_ads_Cntrct_View().getAstCOUNTER().equals(0)))                                                                     //Natural: IF NO RECORD FOUND
                        {
                            getReports().write(0, "No contracts for REQ# ",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                                                    //Natural: WRITE 'No contracts for REQ# ' ADS-PRTCPNT-VIEW.RQST-ID
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(1, ldaAdsl540a.getAds_Prtcpnt_View_Adp_Unique_Id(),new ColumnSpacing(3),ldaAdsl540a.getAds_Prtcpnt_View_Adp_Effctv_Dte(),  //Natural: WRITE ( 1 ) ADP-UNIQUE-ID 3X ADP-EFFCTV-DTE ( EM = MM/DD/YYYY ) 36T 'No contract record for RQST-ID ' ADS-PRTCPNT-VIEW.RQST-ID
                                new ReportEditMask ("MM/DD/YYYY"),new TabSetting(36),"No contract record for RQST-ID ",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Error_Report_Entries.nadd(1);                                                                                                             //Natural: ADD 1 TO #ERROR-REPORT-ENTRIES
                            if (true) break PND_PND_L0840;                                                                                                                //Natural: ESCAPE BOTTOM ( ##L0840. )
                        }                                                                                                                                                 //Natural: END-NOREC
                        //*  TEST
                        //*    WRITE '=' ADS-CNTRCT-VIEW.RQST-ID
                        //*    WRITE '=' ADC-SQNCE-NBR
                        if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Tkr_Symbl().getValue(1).equals(" ")))                                                  //Natural: IF ADC-ACKL-LTTR-TKR-SYMBL ( 1 ) = ' '
                        {
                            getReports().write(0, "No fund info for REQ# ",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                                                    //Natural: WRITE 'No fund info for REQ# ' ADS-PRTCPNT-VIEW.RQST-ID
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(1, ldaAdsl540a.getAds_Prtcpnt_View_Adp_Unique_Id(),new ColumnSpacing(3),ldaAdsl540a.getAds_Prtcpnt_View_Adp_Effctv_Dte(),  //Natural: WRITE ( 1 ) ADP-UNIQUE-ID 3X ADP-EFFCTV-DTE ( EM = MM/DD/YYYY ) 36T 'No fund info for  RQST-ID ' ADS-PRTCPNT-VIEW.RQST-ID
                                new ReportEditMask ("MM/DD/YYYY"),new TabSetting(36),"No fund info for  RQST-ID ",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Error_Report_Entries.nadd(1);                                                                                                             //Natural: ADD 1 TO #ERROR-REPORT-ENTRIES
                            if (true) break PND_PND_L0840;                                                                                                                //Natural: ESCAPE BOTTOM ( ##L0840. )
                        }                                                                                                                                                 //Natural: END-IF
                        //*  10182006 MN  START
                        pnd_Fund_Info_Ok.reset();                                                                                                                         //Natural: RESET #FUND-INFO-OK
                        pnd_Ackl_Req_Info.getValue("*").reset();                                                                                                          //Natural: RESET #ACKL-REQ-INFO ( * )
                        pnd_C.reset();                                                                                                                                    //Natural: RESET #C
                        pnd_D.reset();                                                                                                                                    //Natural: RESET #D
                        pnd_E.reset();                                                                                                                                    //Natural: RESET #E
                                                                                                                                                                          //Natural: PERFORM CHECK-FUND-INFO
                        sub_Check_Fund_Info();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("PND_PND_L0840"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0840"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(pnd_Fund_Info_Ok.getBoolean()))                                                                                                     //Natural: IF #FUND-INFO-OK
                        {
                                                                                                                                                                          //Natural: PERFORM ADJUST-ACKL-GROUP
                            sub_Adjust_Ackl_Group();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().write(0, "Fund info is not unique for Req #",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                                         //Natural: WRITE 'Fund info is not unique for Req #' ADS-PRTCPNT-VIEW.RQST-ID
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(1, ldaAdsl540a.getAds_Prtcpnt_View_Adp_Unique_Id(),new ColumnSpacing(3),ldaAdsl540a.getAds_Prtcpnt_View_Adp_Effctv_Dte(),  //Natural: WRITE ( 1 ) ADP-UNIQUE-ID 3X ADP-EFFCTV-DTE ( EM = MM/DD/YYYY ) 36T 'Fund info is not unique for RQST-ID ' ADS-PRTCPNT-VIEW.RQST-ID
                                new ReportEditMask ("MM/DD/YYYY"),new TabSetting(36),"Fund info is not unique for RQST-ID ",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Error_Report_Entries.nadd(1);                                                                                                             //Natural: ADD 1 TO #ERROR-REPORT-ENTRIES
                            if (true) break PND_PND_L0840;                                                                                                                //Natural: ESCAPE BOTTOM ( ##L0840. )
                        }                                                                                                                                                 //Natural: END-IF
                        //*  02122007  MN  START
                        if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rtb_Ind().equals("Y")))                                                                         //Natural: IF ADP-RTB-IND = 'Y'
                        {
                            pnd_Rtb_Fund_Info_Ok.reset();                                                                                                                 //Natural: RESET #RTB-FUND-INFO-OK
                            pnd_Rtb_Req_Info.getValue("*").reset();                                                                                                       //Natural: RESET #RTB-REQ-INFO ( * )
                            pnd_C.reset();                                                                                                                                //Natural: RESET #C
                            pnd_D.reset();                                                                                                                                //Natural: RESET #D
                            pnd_E.reset();                                                                                                                                //Natural: RESET #E
                                                                                                                                                                          //Natural: PERFORM CHECK-RTB-FUND-INFO
                            sub_Check_Rtb_Fund_Info();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            if (condition(pnd_Rtb_Fund_Info_Ok.getBoolean()))                                                                                             //Natural: IF #RTB-FUND-INFO-OK
                            {
                                                                                                                                                                          //Natural: PERFORM ADJUST-RTB-GROUP
                                sub_Adjust_Rtb_Group();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("PND_PND_L0840"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0840"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(Map.getDoInput())) {return;}
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                getReports().write(0, "RTB Fund info is not unique for Req #",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                                 //Natural: WRITE 'RTB Fund info is not unique for Req #' ADS-PRTCPNT-VIEW.RQST-ID
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("PND_PND_L0840"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0840"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                getReports().write(1, ldaAdsl540a.getAds_Prtcpnt_View_Adp_Unique_Id(),new ColumnSpacing(3),ldaAdsl540a.getAds_Prtcpnt_View_Adp_Effctv_Dte(),  //Natural: WRITE ( 1 ) ADP-UNIQUE-ID 3X ADP-EFFCTV-DTE ( EM = MM/DD/YYYY ) 36T 'RTB Fund info is not unique for RQST-ID ' ADS-PRTCPNT-VIEW.RQST-ID
                                    new ReportEditMask ("MM/DD/YYYY"),new TabSetting(36),"RTB Fund info is not unique for RQST-ID ",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("PND_PND_L0840"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0840"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                pnd_Error_Report_Entries.nadd(1);                                                                                                         //Natural: ADD 1 TO #ERROR-REPORT-ENTRIES
                                if (true) break PND_PND_L0840;                                                                                                            //Natural: ESCAPE BOTTOM ( ##L0840. )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        //*  02122007  MN  END
                        pnd_Adc_Isn.setValue(ldaAdsl401a.getVw_ads_Cntrct_View().getAstISN("PND_PND_L0840"));                                                             //Natural: ASSIGN #ADC-ISN := *ISN ( ##L0840. )
                        pnd_Letter_Info_Found.setValue(true);                                                                                                             //Natural: ASSIGN #LETTER-INFO-FOUND := TRUE
                        if (condition(pnd_Debug_On.getBoolean()))                                                                                                         //Natural: IF #DEBUG-ON
                        {
                            getReports().write(0, NEWLINE,"*");                                                                                                           //Natural: WRITE / '*'
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, "Before moving data to PDA");                                                                                           //Natural: WRITE 'Before moving data to PDA'
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, "=",pnd_Letter_Info_Found);                                                                                             //Natural: WRITE '=' #LETTER-INFO-FOUND
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0840"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        //*  10182006 MN  END
                        //*  FOR COMB DO PART MOVE IF SEQ NO = 1
                                                                                                                                                                          //Natural: PERFORM MOVE-PARTICIPANT-DATA-TO-PDA
                        sub_Move_Participant_Data_To_Pda();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("PND_PND_L0840"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0840"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM MOVE-DA-CONTRACT-DATA-TO-PDA
                        sub_Move_Da_Contract_Data_To_Pda();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("PND_PND_L0840"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0840"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //* * 11102006 MN   START
                        //*    IF #JINDEX-ERROR                      /* 02122006 MN
                        if (condition(pnd_Jindex_Error.getBoolean() || pnd_Pindex_Error.getBoolean() || pnd_Rindex_Error.getBoolean()))                                   //Natural: IF #JINDEX-ERROR OR #PINDEX-ERROR OR #RINDEX-ERROR
                        {
                            //*  FROM FIND CONTRACT
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        //* * 11102006 MN   END
                    }                                                                                                                                                     //Natural: END-FIND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(! (pnd_Letter_Info_Found.getBoolean())))                                                                                                //Natural: IF NOT #LETTER-INFO-FOUND
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //* * 11102006 MN   START
                    //*  IF #JINDEX-ERROR                      /* 02122006 MN
                    if (condition(pnd_Jindex_Error.getBoolean() || pnd_Pindex_Error.getBoolean() || pnd_Rindex_Error.getBoolean()))                                       //Natural: IF #JINDEX-ERROR OR #PINDEX-ERROR OR #RINDEX-ERROR
                    {
                        getReports().write(0, "Duplicate funds info for REQ# ",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                                                //Natural: WRITE 'Duplicate funds info for REQ# ' ADS-PRTCPNT-VIEW.RQST-ID
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(1, ldaAdsl540a.getAds_Prtcpnt_View_Adp_Unique_Id(),new ColumnSpacing(3),ldaAdsl540a.getAds_Prtcpnt_View_Adp_Effctv_Dte(),      //Natural: WRITE ( 1 ) ADP-UNIQUE-ID 3X ADP-EFFCTV-DTE ( EM = MM/DD/YYYY ) 36T 'Duplicate funds info for RQST-ID ' ADS-PRTCPNT-VIEW.RQST-ID
                            new ReportEditMask ("MM/DD/YYYY"),new TabSetting(36),"Duplicate funds info for RQST-ID ",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Error_Report_Entries.nadd(1);                                                                                                                 //Natural: ADD 1 TO #ERROR-REPORT-ENTRIES
                        //*  GO TO READ NEXT REQUEST
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //* * 11102006 MN   END
                                                                                                                                                                          //Natural: PERFORM CALL-BENEFICIARY-MODULE
                    sub_Call_Beneficiary_Module();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALL-TAX-ELECTION-MODULE
                    sub_Call_Tax_Election_Module();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALL-POST-FOR-OPEN
                    sub_Call_Post_For_Open();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                        //Natural: IF #RETURN-CODE NE ' '
                    {
                        getReports().write(0, "Bad return code for POST Open ",pnd_Return_Code,"for ",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                         //Natural: WRITE 'Bad return code for POST Open ' #RETURN-CODE 'for ' ADS-PRTCPNT-VIEW.RQST-ID
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM ERROR-ROUTINE-FOR-POST
                        sub_Error_Routine_For_Post();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALL-POST-FOR-CLOSE
                        sub_Call_Post_For_Close();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        getCurrentProcessState().getDbConv().dbRollback();                                                                                                //Natural: BACKOUT
                        getReports().write(1, ldaAdsl540a.getAds_Prtcpnt_View_Adp_Unique_Id(),new ColumnSpacing(3),ldaAdsl540a.getAds_Prtcpnt_View_Adp_Effctv_Dte(),      //Natural: WRITE ( 1 ) ADP-UNIQUE-ID 3X ADP-EFFCTV-DTE ( EM = MM/DD/YYYY ) 36T 'POST OPEN failed, return code  ' #RETURN-CODE
                            new ReportEditMask ("MM/DD/YYYY"),new TabSetting(36),"POST OPEN failed, return code  ",pnd_Return_Code);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Error_Report_Entries.nadd(1);                                                                                                                 //Natural: ADD 1 TO #ERROR-REPORT-ENTRIES
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                        //*    ESCAPE BOTTOM (0660) /*  WE WANT TO CONTINUE PROCESS
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALL-POST-FOR-WRITE
                    sub_Call_Post_For_Write();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                        //Natural: IF #RETURN-CODE NE ' '
                    {
                        getReports().write(0, "Bad return code for POST Write ",pnd_Return_Code,"for ",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                        //Natural: WRITE 'Bad return code for POST Write ' #RETURN-CODE 'for ' ADS-PRTCPNT-VIEW.RQST-ID
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM ERROR-ROUTINE-FOR-POST
                        sub_Error_Routine_For_Post();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALL-POST-FOR-CLOSE
                        sub_Call_Post_For_Close();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        getCurrentProcessState().getDbConv().dbRollback();                                                                                                //Natural: BACKOUT
                        //*    ESCAPE BOTTOM (0660)
                        getReports().write(1, ldaAdsl540a.getAds_Prtcpnt_View_Adp_Unique_Id(),new ColumnSpacing(3),ldaAdsl540a.getAds_Prtcpnt_View_Adp_Effctv_Dte(),      //Natural: WRITE ( 1 ) ADP-UNIQUE-ID 3X ADP-EFFCTV-DTE ( EM = MM/DD/YYYY ) 36T 'POST WRITE failed, return code ' #RETURN-CODE
                            new ReportEditMask ("MM/DD/YYYY"),new TabSetting(36),"POST WRITE failed, return code ",pnd_Return_Code);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Error_Report_Entries.nadd(1);                                                                                                                 //Natural: ADD 1 TO #ERROR-REPORT-ENTRIES
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-PI-DATA-FIELDS
                    sub_Initialize_Pi_Data_Fields();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM STORE-PI-RECORD
                    sub_Store_Pi_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                        //Natural: IF #RETURN-CODE NE ' '
                    {
                        getReports().write(0, "Bad return code for POST Store PI ",pnd_Return_Code,"for ",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                     //Natural: WRITE 'Bad return code for POST Store PI ' #RETURN-CODE 'for ' ADS-PRTCPNT-VIEW.RQST-ID
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM ERROR-ROUTINE-FOR-POST
                        sub_Error_Routine_For_Post();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALL-POST-FOR-CLOSE
                        sub_Call_Post_For_Close();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        getCurrentProcessState().getDbConv().dbRollback();                                                                                                //Natural: BACKOUT
                        //*    ESCAPE BOTTOM (0660)
                        getReports().write(1, ldaAdsl540a.getAds_Prtcpnt_View_Adp_Unique_Id(),new ColumnSpacing(3),ldaAdsl540a.getAds_Prtcpnt_View_Adp_Effctv_Dte(),      //Natural: WRITE ( 1 ) ADP-UNIQUE-ID 3X ADP-EFFCTV-DTE ( EM = MM/DD/YYYY ) 36T 'POST STORE PI failed, return code  ' #RETURN-CODE
                            new ReportEditMask ("MM/DD/YYYY"),new TabSetting(36),"POST STORE PI failed, return code  ",pnd_Return_Code);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Error_Report_Entries.nadd(1);                                                                                                                 //Natural: ADD 1 TO #ERROR-REPORT-ENTRIES
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALL-POST-FOR-CLOSE
                    sub_Call_Post_For_Close();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    if (condition(pnd_Return_Code.notEquals(" ")))                                                                                                        //Natural: IF #RETURN-CODE NE ' '
                    {
                        getReports().write(0, "Bad return code fromr POST Close ",pnd_Return_Code,"for ",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                      //Natural: WRITE 'Bad return code fromr POST Close ' #RETURN-CODE 'for ' ADS-PRTCPNT-VIEW.RQST-ID
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM ERROR-ROUTINE-FOR-POST
                        sub_Error_Routine_For_Post();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*    ESCAPE BOTTOM (0660)
                        getReports().write(1, ldaAdsl540a.getAds_Prtcpnt_View_Adp_Unique_Id(),new ColumnSpacing(3),ldaAdsl540a.getAds_Prtcpnt_View_Adp_Effctv_Dte(),      //Natural: WRITE ( 1 ) ADP-UNIQUE-ID 3X ADP-EFFCTV-DTE ( EM = MM/DD/YYYY ) 36T 'POST CLOSE failed, return code  ' #RETURN-CODE
                            new ReportEditMask ("MM/DD/YYYY"),new TabSetting(36),"POST CLOSE failed, return code  ",pnd_Return_Code);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Error_Report_Entries.nadd(1);                                                                                                                 //Natural: ADD 1 TO #ERROR-REPORT-ENTRIES
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Letters_Written.nadd(1);                                                                                                                      //Natural: ADD 1 TO #LETTERS-WRITTEN
                                                                                                                                                                          //Natural: PERFORM UPDATE-PART-RECORD
                        sub_Update_Part_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                        //*  MARINA 08/09/2006
                        ldaPstl0175.getPstl0175_Pstl0175_Data_Array().getValue("*").reset();                                                                              //Natural: RESET PSTL0175-DATA-ARRAY ( * )
                        //*  MARINA 08/09/2006
                        pdaAdsa555.getTax_Linkage().reset();                                                                                                              //Natural: RESET TAX-LINKAGE
                        getReports().write(0, "Writing a letter for ",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id(),"is successful");                                         //Natural: WRITE 'Writing a letter for ' ADS-PRTCPNT-VIEW.RQST-ID 'is successful'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB-MESSAGE
                sub_End_Of_Job_Message();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //* *********************************************************************
                //*                    O N   E R R O R   R O U T I N E
                //* *********************************************************************
                //*                                                                                                                                                       //Natural: ON ERROR
                //*  ==================== S U B R O U T I N E S =========================
                //*  10182006 MN  START
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FUND-INFO
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADJUST-ACKL-GROUP
                //*  10182006 MN  END
                //*  02122007 MN  START
                //* *************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-RTB-FUND-INFO
                //* * EM - 012709 START
                //* *IF ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-TKR-SYMBL (*) = 'WA51#'
                //* *  EXAMINE ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-TKR-SYMBL (*) FOR 'WA51#'
                //* *    GIVING INDEX #N
                //* *  IF #N > 0
                //* *    ADS-CNTRCT-VIEW.ADC-RTB-TICKER (#N) :=
                //* *      ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-TKR-SYMBL (#N)
                //* *  END-IF
                //* *END-IF
                //* * EM - 012709 END
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADJUST-RTB-GROUP
                //* * 02122007  MN END
                //* *********************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-PARTICIPANT-DATA-TO-PDA
                //*  NAP-SETTL-PCT-DOL-SWTCH
                //*  NAP-SETTL-ALL-TIAA-STD-PCT
                //*  NAP-SETTL-ALL-TIAA-GRD-AMT
                //*  NAP-SETTL-ALL-TIAA-GRD-REM
                //*  NAP-SETTL-ALL-TIAA-GRD-FLG
                //* * 02122007 MN START
                //*  COMMENTED OUT PER ED MELNIK
                //*  PSTL0175.ADP-RTB-ALT-DEST-RMNDR     (1)   :=
                //*                       ADS-PRTCPNT-VIEW.ADP-RTB-ALT-DEST-RMNDR
                //*  PSTL0175.ADP-RTB-ALT-DEST-CARR-NME    (1)   :=
                //*    ADS-PRTCPNT-VIEW.ADP-RTB-ALT-NAME
                //*  NBC 04/24/2007
                //*  IF ADP-RTB-ALT-ADDR-IND NE 'N'
                //*   PSTL0175.ADP-RTB-ALT-DEST-ADDR        (1,*) :=
                //*     ADS-PRTCPNT-VIEW.ADP-RTB-ALT-DEST-ADDR-TXT (*)
                //*   PSTL0175.ADP-RTB-ALT-DEST-ZIP         (1)   :=
                //*     ADS-PRTCPNT-VIEW.ADP-RTB-ALT-DEST-ADDR-ZIP
                //*  ELSE
                //*   PSTL0175.ADP-RTB-ALT-DEST-ADDR (1,*) :=
                //*     ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-TXT(*)
                //*   PSTL0175.ADP-RTB-ALT-DEST-ZIP         (1)   :=
                //*     ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-ZIP
                //*  END-IF
                //*  NBC 04/24/2007
                //*  PSTL0175.ADP-RTB-ALT-DEST-AMT       (*) :=
                //*                       ADS-PRTCPNT-VIEW.ADP-RTB-ALT-DEST-AMT       (*)
                //* *
                //*  PSTL0175.ADP-RTB-ALT-DEST-IRA-TYP(*) :=
                //*                       ADS-PRTCPNT-VIEW.ADP-RTB-ALT-DEST-IRA-TYP(*)
                //*  PSTL0175.ADP-RTB-ALT-DEST-PPCN      (*) := ??????????
                //*                       ADS-PRTCPNT-VIEW.ADP-RTB-ALT-DEST-PPCN      (*)
                //*  PSTL0175.ADP-RTB-ALT-DEST-PPCC      (*) := ???????
                //* *                    ADS-PRTCPNT-VIEW.ADP-RTB-ALT-DEST-PPCC      (*)
                //* * 02122007 MN END
                //* * MOVE NAP-SCND-ANNT-SSN            TO ANN-PART-SSN
                //* ******************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-ANNUITY-OPTION
                //* ********************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-CORRECT-ADDRESSES
                //* *************************************
                //* * DEFINE SUBROUTINE REMAINDER-PROCESS
                //* *************************************
                //* *
                //* * IF PSTL0175.NAP-RTB-ALT-DEST-RMNDR (1) = 'R'
                //* *  MOVE 1 TO #I
                //* *  MOVE 3 TO #J
                //* *  PERFORM MOVE-ADDRESSES
                //* *  MOVE 2 TO #I
                //* *  MOVE 1 TO #J
                //* *  PERFORM MOVE-ADDRESSES
                //* *  MOVE 3 TO #I
                //* *  MOVE 2 TO #J
                //* *  PERFORM MOVE-ADDRESSES
                //* * MOVE BY NAME #RTB-ALTERNATE-DEST TO RTB-ALTERNATE-DEST
                //* *  NAP-RTB-ALT-DEST-RLVR-IND  :=  #RTB-ALT-DEST-RLVR-IND
                //* *  NAP-RTB-ALT-DEST-RMNDR     :=  #RTB-ALT-DEST-RMNDR
                //* *  NAP-RTB-ALT-DEST-CARR-NME  :=  #RTB-ALT-DEST-CARR-NME
                //* *  NAP-RTB-ALT-DEST-ADDR      :=  #RTB-ALT-DEST-ADDR
                //* *  NAP-RTB-ALT-DEST-ZIP       :=  #RTB-ALT-DEST-ZIP
                //* *  NAP-RTB-ALT-DEST-ACCT-NBR  :=  #RTB-ALT-DEST-ACCT-NBR
                //* *  NAP-RTB-ALT-DEST-PCT       :=  #RTB-ALT-DEST-PCT
                //* *  NAP-RTB-ALT-DEST-AMT       :=  #RTB-ALT-DEST-AMT
                //* *  NAP-RTB-ALT-DEST-ACCT-TYP  :=  #RTB-ALT-DEST-ACCT-TYP
                //* *  NAP-RTB-ALT-DEST-TRNST-CDE :=  #RTB-ALT-DEST-TRNST-CDE
                //* * END-IF
                //* *
                //* * IF PSTL0175.NAP-RTB-ALT-DEST-RMNDR (2) = 'R'
                //* *  MOVE 1 TO #I
                //* *  MOVE 1 TO #J
                //* *  PERFORM MOVE-ADDRESSES
                //* *  MOVE 2 TO #I
                //* *  MOVE 3 TO #J
                //* *  PERFORM MOVE-ADDRESSES
                //* *  MOVE 3 TO #I
                //* *  MOVE 2 TO #J
                //* *  PERFORM MOVE-ADDRESSES
                //* *  MOVE BY NAME #RTB-ALTERNATE-DEST TO RTB-ALTERNATE-DEST
                //* * END-IF
                //* *
                //* * IF #RTB-ALTERNATE-DEST.NAP-RTB-ALT-DEST-RLVR-DEST-CDE(*) NE ' '
                //* *  IGNORE
                //* * ELSE
                //* *  MOVE ADP-RTB-ALT-DEST-RLVR-DEST-CDE(*)
                //* *    TO #RTB-ALTERNATE-DEST.NAP-RTB-ALT-DEST-RLVR-DEST-CDE(*)
                //* * END-IF
                //* *
                //* * END-SUBROUTINE          /*   REMAINDER-PROCESS
                //* *
                //* ***********************************
                //* * DEFINE SUBROUTINE MOVE-ADDRESSES
                //* ***********************************
                //* *
                //* * MOVE PSTL0175.NAP-RTB-ALT-DEST-RLVR-IND (#I)
                //* *  TO #RTB-ALT-DEST-RLVR-IND (#J)
                //* * MOVE PSTL0175.NAP-RTB-ALT-DEST-CARR-NME (#I)
                //* *  TO #RTB-ALT-DEST-CARR-NME (#J)
                //* * MOVE PSTL0175.NAP-RTB-ALT-DEST-ADDR     (#I,*)
                //* *  TO #RTB-ALT-DEST-ADDR     (#J,*)
                //* * MOVE PSTL0175.NAP-RTB-ALT-DEST-ZIP      (#I)
                //* *  TO #RTB-ALT-DEST-ZIP      (#J)
                //* * MOVE PSTL0175.NAP-RTB-ALT-DEST-ACCT-NBR (#I)
                //* *  TO #RTB-ALT-DEST-ACCT-NBR (#J)
                //* *
                //* * MOVE PSTL0175.NAP-RTB-ALT-DEST-PCT      (#I)
                //* *  TO #RTB-ALT-DEST-PCT      (#J)
                //* * MOVE PSTL0175.NAP-RTB-ALT-DEST-AMT      (#I)
                //* *   TO #RTB-ALT-DEST-AMT      (#J)
                //* * MOVE PSTL0175.NAP-RTB-ALT-DEST-ACCT-TYP (#I)
                //* *   TO #RTB-ALT-DEST-ACCT-TYP (#J)
                //* * MOVE PSTL0175.NAP-RTB-ALT-DEST-TRNST-CDE(#I)
                //* *  TO #RTB-ALT-DEST-TRNST-CDE(#J)
                //* * MOVE ADP-RTB-ALT-DEST-RLVR-DEST-CDE(#I)
                //* *  TO #RTB-ALT-DEST-RLVR-DEST-CDE(#J)
                //* *
                //* * END-SUBROUTINE         /*   MOVE-ADDRESSES
                //*  02122007 MN START
                //* **************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-ROLLOVER-DATA
                //* *
                //*  02122007 MN END
                //* *********************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-DA-CONTRACT-DATA-TO-PDA
                //*  IF ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-TKR-SYMBL (#I) =  'TSAF#'
                //*    IF ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-TKR-SYMBL (#I) =  'TSVX#'
                //*  THE FOLLOWING IS DONE WITH GET-FUND-TICKER-INFO
                //* ******************************************
                //*  DEFINE SUBROUTINE GET-NUMERIC-PRODUCT-CODES
                //* ******************************************
                //*  RESET #PROD-CODE-N (*)
                //*  FOR #I 1 20
                //*    IF ADS-CNTRCT-VIEW.ADC-ACCT-TYP (#I) = ' '
                //*      ESCAPE TOP
                //*    ELSE
                //*      FOR #J 1 #PROD-CT
                //*       IF ADS-CNTRCT-VIEW.ADC-ACCT-CDE (#I) NE #PROD-CD (#J)
                //*         ESCAPE TOP
                //*       ELSE
                //*         MOVE #PROD-CD-N (#J) TO #PROD-CODE-N (#I)
                //*         ESCAPE BOTTOM (2712)
                //*       END-IF
                //*     END-FOR
                //*   END-IF
                //*  END-FOR
                //*  END-SUBROUTINE    /*   GET-NUMERIC-PRODUCT-CODES
                //* ***********************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VARIABLE-INCOME-METHOD-PAYMENT
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TIAA-REA-PROCESS
                //*    (ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-TKR-SYMBL (#I) = 'TIAA#'
                //*    OR = 'TSAF#' OR = 'TSVX#')
                //* ******************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREF-PROCESS
                //* * 02122007 MN START
                //* ******************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RTB-PROCESS
                //* * 02122007 MN END
                //* ****************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-BENEFICIARY-MODULE
                //*  MOVE THIS INFO TO PSTL0175
                //*  BENEFICIARY INFO
                //*  PRIMARY INFO
                //*  CONTINGENT INFO
                //* *****************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-TAX-ELECTION-MODULE
                //* *  02122007  MN  START
                //* ******************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-ROLLOVER-INDICATOR
                //* * 02122007  MN  START
                //* ***********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST-FOR-OPEN
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST-FOR-WRITE
                //* ******************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-PI-DATA-FIELDS
                //* ********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-PI-RECORD
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST-FOR-CLOSE
                //* ***********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-PART-RECORD
                //* *********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FUND-TICKERS
                //*    ' NECN4000 RETURN CODE : ' NECA4000.RETURN-CDE
                //*    ' NECN4000 RETURN MSG : '  NECA4000.RETURN-MSG
                //* ***************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-ROUTINE-FOR-POST
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TERMINATE-ROUTINE
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-JOB-MESSAGE
                //* ****************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-PASSED-TAX-DATA
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Check_Fund_Info() throws Exception                                                                                                                   //Natural: CHECK-FUND-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Beginning of CHECK-FUND-INFO for RQST-ID :",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                                                //Natural: WRITE 'Beginning of CHECK-FUND-INFO for RQST-ID :' ADS-PRTCPNT-VIEW.RQST-ID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_D.setValue(1);                                                                                                                                                //Natural: ASSIGN #D := 1
        //*  ADD 1 TO #E
        F1:                                                                                                                                                               //Natural: FOR #C 1 20
        for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(20)); pnd_C.nadd(1))
        {
            pnd_D.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #D
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Tkr_Symbl().getValue(pnd_C).equals(" ")))                                                          //Natural: IF ADC-ACKL-LTTR-TKR-SYMBL ( #C ) = ' '
            {
                if (condition(pnd_C.notEquals(1)))                                                                                                                        //Natural: IF #C NE 1
                {
                    pnd_Fund_Info_Ok.setValue(true);                                                                                                                      //Natural: ASSIGN #FUND-INFO-OK := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (true) break F1;                                                                                                                                       //Natural: ESCAPE BOTTOM ( F1. )
            }                                                                                                                                                             //Natural: END-IF
            //*  OS-102013 START
            DbsUtil.examine(new ExamineSource(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Ticker().getValue("*")), new ExamineSearch(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Tkr_Symbl().getValue(pnd_C)),  //Natural: EXAMINE #PARM-AREA.#ACCT-TICKER ( * ) FOR ADC-ACKL-LTTR-TKR-SYMBL ( #C ) GIVING INDEX #N
                new ExamineGivingIndex(pnd_N));
            pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id.setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_N));                                   //Natural: ASSIGN #NAZ-TABLE-LVL3-ID := #ACCT-INVSTMNT-GRPNG-ID ( #N )
            //*  #NAZ-TABLE-LVL3-ID := ADC-ACKL-LTTR-TKR-SYMBL (#C) /* 020207 START
            //*  OS-102013 END
            vw_adat.startDatabaseRead                                                                                                                                     //Natural: READ ADAT BY NAZ-TBL-SUPER3 STARTING FROM #NAZ-TABLE-KEY
            (
            "READ03",
            new Wc[] { new Wc("NAZ_TBL_SUPER3", ">=", pnd_Naz_Table_Key, WcType.BY) },
            new Oc[] { new Oc("NAZ_TBL_SUPER3", "ASC") }
            );
            READ03:
            while (condition(vw_adat.readNextRow("READ03")))
            {
                if (condition(!(adat_Naz_Tbl_Rcrd_Actv_Ind.equals("Y") && adat_Naz_Tbl_Rcrd_Typ_Ind.equals("C"))))                                                        //Natural: ACCEPT IF NAZ-TBL-RCRD-ACTV-IND = 'Y' AND NAZ-TBL-RCRD-TYP-IND = 'C'
                {
                    continue;
                }
                //*    IF NAZ-TBL-RCRD-LVL3-ID NE ADC-ACKL-LTTR-TKR-SYMBL (#C) /* OS-102013
                //*  OS-102013
                if (condition(adat_Naz_Tbl_Rcrd_Lvl3_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id)))                                                              //Natural: IF NAZ-TBL-RCRD-LVL3-ID NE #NAZ-TABLE-LVL3-ID
                {
                    pnd_Cref.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #CREF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Tiaa.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #TIAA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  020207 END
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Tkr_Symbl().getValue(pnd_C).equals(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Tkr_Symbl().getValue(pnd_D)))) //Natural: IF ADC-ACKL-LTTR-TKR-SYMBL ( #C ) = ADC-ACKL-LTTR-TKR-SYMBL ( #D )
            {
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Acct_Typ().getValue(pnd_C).equals(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Acct_Typ().getValue(pnd_D))  //Natural: IF ADC-ACKL-LTTR-ACCT-TYP ( #C ) = ADC-ACKL-LTTR-ACCT-TYP ( #D ) AND ADC-ACKL-LTTR-ACCT-QTY ( #C ) = ADC-ACKL-LTTR-ACCT-QTY ( #D )
                    && ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Acct_Qty().getValue(pnd_C).equals(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Acct_Qty().getValue(pnd_D))))
                {
                    pnd_Fund_Info_Ok.setValue(true);                                                                                                                      //Natural: ASSIGN #FUND-INFO-OK := TRUE
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Fund_Info_Ok.reset();                                                                                                                             //Natural: RESET #FUND-INFO-OK
                    pnd_Ackl_Req_Info.getValue("*").reset();                                                                                                              //Natural: RESET #ACKL-REQ-INFO ( * )
                    if (true) break F1;                                                                                                                                   //Natural: ESCAPE BOTTOM ( F1. )
                }                                                                                                                                                         //Natural: END-IF
                //*  TICKER BREAK
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_E.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #E
                pnd_Ackl_Req_Info_Pnd_Ackl_Lttr_Tkr_Symbl.getValue(pnd_E).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Tkr_Symbl().getValue(pnd_C));             //Natural: MOVE ADC-ACKL-LTTR-TKR-SYMBL ( #C ) TO #ACKL-LTTR-TKR-SYMBL ( #E )
                pnd_Ackl_Req_Info_Pnd_Ackl_Lttr_Acct_Typ.getValue(pnd_E).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Acct_Typ().getValue(pnd_C));               //Natural: MOVE ADC-ACKL-LTTR-ACCT-TYP ( #C ) TO #ACKL-LTTR-ACCT-TYP ( #E )
                pnd_Ackl_Req_Info_Pnd_Ackl_Lttr_Acct_Qty.getValue(pnd_E).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Acct_Qty().getValue(pnd_C));               //Natural: MOVE ADC-ACKL-LTTR-ACCT-QTY ( #C ) TO #ACKL-LTTR-ACCT-QTY ( #E )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CHECK-FUND-INFO
    }
    private void sub_Adjust_Ackl_Group() throws Exception                                                                                                                 //Natural: ADJUST-ACKL-GROUP
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Beginning of ADJUST-ACKL-GROUP for RQST-ID :",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                                              //Natural: WRITE 'Beginning of ADJUST-ACKL-GROUP for RQST-ID :' ADS-PRTCPNT-VIEW.RQST-ID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Rqst_Info().getValue("*").reset();                                                                                   //Natural: RESET ADC-ACKL-LTTR-RQST-INFO ( * )
        ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Tkr_Symbl().getValue("*").setValue(pnd_Ackl_Req_Info_Pnd_Ackl_Lttr_Tkr_Symbl.getValue("*"));                         //Natural: MOVE #ACKL-LTTR-TKR-SYMBL ( * ) TO ADC-ACKL-LTTR-TKR-SYMBL ( * )
        ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Acct_Typ().getValue("*").setValue(pnd_Ackl_Req_Info_Pnd_Ackl_Lttr_Acct_Typ.getValue("*"));                           //Natural: MOVE #ACKL-LTTR-ACCT-TYP ( * ) TO ADC-ACKL-LTTR-ACCT-TYP ( * )
        ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Acct_Qty().getValue("*").setValue(pnd_Ackl_Req_Info_Pnd_Ackl_Lttr_Acct_Qty.getValue("*"));                           //Natural: MOVE #ACKL-LTTR-ACCT-QTY ( * ) TO ADC-ACKL-LTTR-ACCT-QTY ( * )
        //*  ADJUST-ACKL-GROUP
    }
    private void sub_Check_Rtb_Fund_Info() throws Exception                                                                                                               //Natural: CHECK-RTB-FUND-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pnd_N.reset();                                                                                                                                                    //Natural: RESET #N
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Beginning of CHECK-RTB-FUND-INFO  for RQST-ID :",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                                           //Natural: WRITE 'Beginning of CHECK-RTB-FUND-INFO  for RQST-ID :' ADS-PRTCPNT-VIEW.RQST-ID
            if (Global.isEscape()) return;
            getReports().write(0, "RTB TICKERS: ",ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Ticker().getValue("*"));                                                         //Natural: WRITE 'RTB TICKERS: ' ADC-RTB-TICKER ( * )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_D.setValue(1);                                                                                                                                                //Natural: ASSIGN #D := 1
        F2:                                                                                                                                                               //Natural: FOR #C 1 14
        for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(14)); pnd_C.nadd(1))
        {
            pnd_D.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #D
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Ticker().getValue(pnd_C).equals(" ")))                                                                   //Natural: IF ADC-RTB-TICKER ( #C ) = ' '
            {
                if (condition(pnd_C.notEquals(1)))                                                                                                                        //Natural: IF #C NE 1
                {
                    pnd_Rtb_Fund_Info_Ok.setValue(true);                                                                                                                  //Natural: ASSIGN #RTB-FUND-INFO-OK := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (true) break F2;                                                                                                                                       //Natural: ESCAPE BOTTOM ( F2. )
            }                                                                                                                                                             //Natural: END-IF
            //*  OS-102013 START
            DbsUtil.examine(new ExamineSource(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Ticker().getValue("*")), new ExamineSearch(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Ticker().getValue(pnd_C)),  //Natural: EXAMINE #ACCT-TICKER ( * ) FOR ADC-RTB-TICKER ( #C ) GIVING INDEX #N
                new ExamineGivingIndex(pnd_N));
            pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id.setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_N));                                   //Natural: ASSIGN #NAZ-TABLE-LVL3-ID := #ACCT-INVSTMNT-GRPNG-ID ( #N )
            //*  #NAZ-TABLE-LVL3-ID := ADC-RTB-TICKER (#C)
            //*  OS-102013 END
            vw_adat.startDatabaseRead                                                                                                                                     //Natural: READ ADAT BY NAZ-TBL-SUPER3 STARTING FROM #NAZ-TABLE-KEY
            (
            "READ04",
            new Wc[] { new Wc("NAZ_TBL_SUPER3", ">=", pnd_Naz_Table_Key, WcType.BY) },
            new Oc[] { new Oc("NAZ_TBL_SUPER3", "ASC") }
            );
            READ04:
            while (condition(vw_adat.readNextRow("READ04")))
            {
                if (condition(!(adat_Naz_Tbl_Rcrd_Actv_Ind.equals("Y") && adat_Naz_Tbl_Rcrd_Typ_Ind.equals("C"))))                                                        //Natural: ACCEPT IF NAZ-TBL-RCRD-ACTV-IND = 'Y' AND NAZ-TBL-RCRD-TYP-IND = 'C'
                {
                    continue;
                }
                //*  OS-102013
                if (condition(adat_Naz_Tbl_Rcrd_Lvl3_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id)))                                                              //Natural: IF NAZ-TBL-RCRD-LVL3-ID NE #NAZ-TABLE-LVL3-ID
                {
                    //*    IF NAZ-TBL-RCRD-LVL3-ID NE ADC-RTB-TICKER (#C)    /* OS-102013
                    pnd_Rtb_Cref.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #RTB-CREF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Rtb_Tiaa.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #RTB-TIAA
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  020207 END
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  112811 START
            if (condition(pnd_C.equals(14)))                                                                                                                              //Natural: IF #C EQ 14
            {
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Ticker().getValue(pnd_C).notEquals(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Ticker().getValue(pnd_C.getDec().subtract(1))))) //Natural: IF ADC-RTB-TICKER ( #C ) NE ADC-RTB-TICKER ( #C - 1 )
                {
                    pnd_E.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #E
                    pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Tkr_Symbl.getValue(pnd_E).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Ticker().getValue(pnd_C));                    //Natural: MOVE ADC-RTB-TICKER ( #C ) TO #RTB-LTTR-TKR-SYMBL ( #E )
                    pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Acct_Typ.getValue(pnd_E).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Acct_Type().getValue(pnd_C));                  //Natural: MOVE ADC-RTB-ACCT-TYPE ( #C ) TO #RTB-LTTR-ACCT-TYP ( #E )
                    pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Acct_Qty.getValue(pnd_E).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Acct_Qty().getValue(pnd_C));                   //Natural: MOVE ADC-RTB-ACCT-QTY ( #C ) TO #RTB-LTTR-ACCT-QTY ( #E )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Rtb_Fund_Info_Ok.setValue(true);                                                                                                                      //Natural: ASSIGN #RTB-FUND-INFO-OK := TRUE
                if (true) break F2;                                                                                                                                       //Natural: ESCAPE BOTTOM ( F2. )
                //*  112811 END
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Ticker().getValue(pnd_C).equals(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Ticker().getValue(pnd_D))))       //Natural: IF ADC-RTB-TICKER ( #C ) = ADC-RTB-TICKER ( #D )
            {
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Acct_Type().getValue(pnd_C).equals(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Acct_Type().getValue(pnd_D))  //Natural: IF ADC-RTB-ACCT-TYPE ( #C ) = ADC-RTB-ACCT-TYPE ( #D ) AND ADC-RTB-ACCT-QTY ( #C ) = ADC-RTB-ACCT-QTY ( #D )
                    && ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Acct_Qty().getValue(pnd_C).equals(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Acct_Qty().getValue(pnd_D))))
                {
                    pnd_Rtb_Fund_Info_Ok.setValue(true);                                                                                                                  //Natural: ASSIGN #RTB-FUND-INFO-OK := TRUE
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Rtb_Fund_Info_Ok.reset();                                                                                                                         //Natural: RESET #RTB-FUND-INFO-OK
                    pnd_Rtb_Req_Info.getValue("*").reset();                                                                                                               //Natural: RESET #RTB-REQ-INFO ( * )
                    if (true) break F2;                                                                                                                                   //Natural: ESCAPE BOTTOM ( F2. )
                }                                                                                                                                                         //Natural: END-IF
                //*  TICKER BREAK
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_E.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #E
                pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Tkr_Symbl.getValue(pnd_E).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Ticker().getValue(pnd_C));                        //Natural: MOVE ADC-RTB-TICKER ( #C ) TO #RTB-LTTR-TKR-SYMBL ( #E )
                pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Acct_Typ.getValue(pnd_E).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Acct_Type().getValue(pnd_C));                      //Natural: MOVE ADC-RTB-ACCT-TYPE ( #C ) TO #RTB-LTTR-ACCT-TYP ( #E )
                pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Acct_Qty.getValue(pnd_E).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Acct_Qty().getValue(pnd_C));                       //Natural: MOVE ADC-RTB-ACCT-QTY ( #C ) TO #RTB-LTTR-ACCT-QTY ( #E )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  FOR TEST ONLY
        getReports().write(0, "#RTB-LTTR-group info after move ");                                                                                                        //Natural: WRITE '#RTB-LTTR-group info after move '
        if (Global.isEscape()) return;
        //*  FOR TEST ONLY
        getReports().write(0, "#RTB-LTTR-TKR-SYMBL: ",pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Tkr_Symbl.getValue("*"));                                                             //Natural: WRITE '#RTB-LTTR-TKR-SYMBL: '#RTB-LTTR-TKR-SYMBL ( * )
        if (Global.isEscape()) return;
        //*  FOR TEST ONLY
        getReports().write(0, "#RTB-LTTR-ACCT-TYP : ",pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Acct_Typ.getValue("*"));                                                              //Natural: WRITE '#RTB-LTTR-ACCT-TYP : '#RTB-LTTR-ACCT-TYP ( * )
        if (Global.isEscape()) return;
        //*  FOR TEST ONLY
        getReports().write(0, "#RTB-LTTR-ACCT-QTY : ",pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Acct_Qty.getValue("*"));                                                              //Natural: WRITE '#RTB-LTTR-ACCT-QTY : '#RTB-LTTR-ACCT-QTY ( * )
        if (Global.isEscape()) return;
        //*  CHECK-RTB-FUND-INFO
    }
    private void sub_Adjust_Rtb_Group() throws Exception                                                                                                                  //Natural: ADJUST-RTB-GROUP
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Beginning of ADJUST-RTB-GROUP for RQST-ID :",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                                               //Natural: WRITE 'Beginning of ADJUST-RTB-GROUP for RQST-ID :' ADS-PRTCPNT-VIEW.RQST-ID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  USING CONTRCT RECORD FIELDS JUST TO HOUSE THIS INFO FOR THIS PROCESS
        //*  WITHOUT STORING IT ON THE RECORD. THE RECORD ITSELF HAS THE ORIGINAL
        //*  INFO. THIS IS THRUE FOR ACKL AND RTB GROUPS
        ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Fund_Info().getValue("*").reset();                                                                                         //Natural: RESET ADC-RTB-FUND-INFO ( * )
        ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Ticker().getValue("*").reset();                                                                                            //Natural: RESET ADC-RTB-TICKER ( * )
        ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Ticker().getValue("*").setValue(pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Tkr_Symbl.getValue("*"));                                    //Natural: MOVE #RTB-LTTR-TKR-SYMBL ( * ) TO ADC-RTB-TICKER ( * )
        ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Acct_Type().getValue("*").setValue(pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Acct_Typ.getValue("*"));                                  //Natural: MOVE #RTB-LTTR-ACCT-TYP ( * ) TO ADC-RTB-ACCT-TYPE ( * )
        ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Acct_Qty().getValue("*").setValue(pnd_Rtb_Req_Info_Pnd_Rtb_Lttr_Acct_Qty.getValue("*"));                                   //Natural: MOVE #RTB-LTTR-ACCT-QTY ( * ) TO ADC-RTB-ACCT-QTY ( * )
        //*  FOR TEST ONLY
        getReports().write(0, "ADC-RTB-group  after adjusting ");                                                                                                         //Natural: WRITE 'ADC-RTB-group  after adjusting '
        if (Global.isEscape()) return;
        //*  FOR TEST ONLY
        getReports().write(0, "ADC-RTB-TICKER :",ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Ticker().getValue("*"));                                                          //Natural: WRITE 'ADC-RTB-TICKER :' ADC-RTB-TICKER ( * )
        if (Global.isEscape()) return;
        //*  FOR TEST ONLY
        getReports().write(0, "ADC-RTB-ACCT-TYPE:",ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Acct_Type().getValue("*"));                                                     //Natural: WRITE 'ADC-RTB-ACCT-TYPE:' ADC-RTB-ACCT-TYPE ( * )
        if (Global.isEscape()) return;
        //*  FOR TEST ONLY
        getReports().write(0, "ADC-RTB-ACCT-QTY :",ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Acct_Qty().getValue("*"));                                                      //Natural: WRITE 'ADC-RTB-ACCT-QTY :' ADC-RTB-ACCT-QTY ( * )
        if (Global.isEscape()) return;
        //*  ADJUST-RTB-GROUP
    }
    private void sub_Move_Participant_Data_To_Pda() throws Exception                                                                                                      //Natural: MOVE-PARTICIPANT-DATA-TO-PDA
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Beginning of MOVE-PARTICIPANT-DATA-TO-PDA for RQST-ID :",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                                   //Natural: WRITE 'Beginning of MOVE-PARTICIPANT-DATA-TO-PDA for RQST-ID :' ADS-PRTCPNT-VIEW.RQST-ID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        ldaPstl0175.getPstl0175_Adp_Frst_Annt_Ssn().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Frst_Annt_Ssn());                                                        //Natural: ASSIGN PSTL0175.ADP-FRST-ANNT-SSN := ADS-PRTCPNT-VIEW.ADP-FRST-ANNT-SSN
        ldaPstl0175.getPstl0175_Adp_Frst_Annt_Dte_Of_Brth_N().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Frst_Annt_Dte_Of_Brth());                                      //Natural: ASSIGN PSTL0175.ADP-FRST-ANNT-DTE-OF-BRTH-N := ADS-PRTCPNT-VIEW.ADP-FRST-ANNT-DTE-OF-BRTH
        ldaPstl0175.getPstl0175_Adp_Crrspndnce_Perm_Addr_Txt().getValue("*").setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt().getValue("*"));      //Natural: ASSIGN PSTL0175.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( * ) := ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( * )
        ldaPstl0175.getPstl0175_Adp_Crrspndnce_Perm_Addr_Zip().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Zip());                                  //Natural: ASSIGN PSTL0175.ADP-CRRSPNDNCE-PERM-ADDR-ZIP := ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-ZIP
        ldaPstl0175.getPstl0175_Adp_Frst_Annt_Rsdnc_Cde().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Frst_Annt_Rsdnc_Cde());                                            //Natural: ASSIGN PSTL0175.ADP-FRST-ANNT-RSDNC-CDE := ADS-PRTCPNT-VIEW.ADP-FRST-ANNT-RSDNC-CDE
        ldaPstl0175.getPstl0175_Adp_Frst_Annt_Ctznshp().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Frst_Annt_Ctznshp());                                                //Natural: ASSIGN PSTL0175.ADP-FRST-ANNT-CTZNSHP := ADS-PRTCPNT-VIEW.ADP-FRST-ANNT-CTZNSHP
        ldaPstl0175.getPstl0175_Adp_Grntee_Period().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Grntee_Period());                                                        //Natural: ASSIGN PSTL0175.ADP-GRNTEE-PERIOD := ADS-PRTCPNT-VIEW.ADP-GRNTEE-PERIOD
        ldaPstl0175.getPstl0175_Adp_Annty_Strt_Dte_N().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annty_Strt_Dte());                                                    //Natural: ASSIGN PSTL0175.ADP-ANNTY-STRT-DTE-N := ADS-PRTCPNT-VIEW.ADP-ANNTY-STRT-DTE
        ldaPstl0175.getPstl0175_Adp_Pymnt_Mode().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Pymnt_Mode());                                                              //Natural: ASSIGN PSTL0175.ADP-PYMNT-MODE := ADS-PRTCPNT-VIEW.ADP-PYMNT-MODE
        ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Grd_Pct().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Settl_All_Tiaa_Grd_Pct());                                      //Natural: ASSIGN PSTL0175.ADP-SETTL-ALL-TIAA-GRD-PCT := ADS-PRTCPNT-VIEW.ADP-SETTL-ALL-TIAA-GRD-PCT
        ldaPstl0175.getPstl0175_Adp_Alt_Dest_Nme().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Alt_Dest_Nme());                                                          //Natural: ASSIGN PSTL0175.ADP-ALT-DEST-NME := ADS-PRTCPNT-VIEW.ADP-ALT-DEST-NME
        ldaPstl0175.getPstl0175_Adp_Alt_Dest_Addr_Txt().getValue("*").setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Alt_Dest_Addr_Txt().getValue("*"));                    //Natural: ASSIGN PSTL0175.ADP-ALT-DEST-ADDR-TXT ( * ) := ADS-PRTCPNT-VIEW.ADP-ALT-DEST-ADDR-TXT ( * )
        ldaPstl0175.getPstl0175_Adp_Alt_Dest_Addr_Zip().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Alt_Dest_Addr_Zip());                                                //Natural: ASSIGN PSTL0175.ADP-ALT-DEST-ADDR-ZIP := ADS-PRTCPNT-VIEW.ADP-ALT-DEST-ADDR-ZIP
        ldaPstl0175.getPstl0175_Adp_Alt_Dest_Acct_Nbr().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Alt_Dest_Acct_Nbr());                                                //Natural: ASSIGN PSTL0175.ADP-ALT-DEST-ACCT-NBR := ADS-PRTCPNT-VIEW.ADP-ALT-DEST-ACCT-NBR
        ldaPstl0175.getPstl0175_Adp_Alt_Dest_Trnst_Cde().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Alt_Dest_Trnst_Cde());                                              //Natural: ASSIGN PSTL0175.ADP-ALT-DEST-TRNST-CDE := ADS-PRTCPNT-VIEW.ADP-ALT-DEST-TRNST-CDE
        ldaPstl0175.getPstl0175_Adp_Alt_Dest_Acct_Typ().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Alt_Dest_Acct_Typ());                                                //Natural: ASSIGN PSTL0175.ADP-ALT-DEST-ACCT-TYP := ADS-PRTCPNT-VIEW.ADP-ALT-DEST-ACCT-TYP
        ldaPstl0175.getPstl0175_Plan_Name().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Plan_Nme());                                                                     //Natural: ASSIGN PSTL0175.PLAN-NAME := ADS-PRTCPNT-VIEW.ADP-PLAN-NME
        ldaPstl0175.getPstl0175_Adp_Rtb_Pymt_Ind().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rtb_Ind());                                                               //Natural: ASSIGN PSTL0175.ADP-RTB-PYMT-IND := ADS-PRTCPNT-VIEW.ADP-RTB-IND
        ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Rlvr_Ind().getValue(1).setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Rlvr_Ind());                            //Natural: ASSIGN PSTL0175.ADP-RTB-ALT-DEST-RLVR-IND ( 1 ) := ADS-PRTCPNT-VIEW.ADP-RTB-ALT-DEST-RLVR-IND
        ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Acct_Nbr().getValue(1).setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Acct_Nbr());                            //Natural: ASSIGN PSTL0175.ADP-RTB-ALT-DEST-ACCT-NBR ( 1 ) := ADS-PRTCPNT-VIEW.ADP-RTB-ALT-DEST-ACCT-NBR
        ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Pct().getValue(1).setValue(100);                                                                                         //Natural: ASSIGN PSTL0175.ADP-RTB-ALT-DEST-PCT ( 1 ) := 100
        ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Acct_Typ().getValue(1).setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Acct_Typ());                            //Natural: ASSIGN PSTL0175.ADP-RTB-ALT-DEST-ACCT-TYP ( 1 ) := ADS-PRTCPNT-VIEW.ADP-RTB-ALT-DEST-ACCT-TYP
        ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Trnst_Cde().getValue(1).setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Trnst_Cde());                          //Natural: ASSIGN PSTL0175.ADP-RTB-ALT-DEST-TRNST-CDE ( 1 ) := ADS-PRTCPNT-VIEW.ADP-RTB-ALT-DEST-TRNST-CDE
        ldaPstl0175.getPstl0175_Ann_Part_Ssn().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Scnd_Annt_Ssn());                                                             //Natural: ASSIGN PSTL0175.ANN-PART-SSN := ADS-PRTCPNT-VIEW.ADP-SCND-ANNT-SSN
        ldaPstl0175.getPstl0175_Participant_Name().setValue(DbsUtil.compress(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Frst_Annt_Title_Cde(), ldaAdsl540a.getAds_Prtcpnt_View_Adp_Frst_Annt_Frst_Nme(),  //Natural: COMPRESS ADP-FRST-ANNT-TITLE-CDE ADP-FRST-ANNT-FRST-NME ADP-FRST-ANNT-MID-NME ADP-FRST-ANNT-LST-NME ADP-FRST-ANNT-SFX INTO PARTICIPANT-NAME
            ldaAdsl540a.getAds_Prtcpnt_View_Adp_Frst_Annt_Mid_Nme(), ldaAdsl540a.getAds_Prtcpnt_View_Adp_Frst_Annt_Lst_Nme(), ldaAdsl540a.getAds_Prtcpnt_View_Adp_Frst_Annt_Sfx()));
        pnd_Name.setValue(DbsUtil.compress(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Scnd_Annt_Frst_Nme(), ldaAdsl540a.getAds_Prtcpnt_View_Adp_Scnd_Annt_Mid_Nme(),             //Natural: COMPRESS ADP-SCND-ANNT-FRST-NME ADP-SCND-ANNT-MID-NME ADP-SCND-ANNT-LST-NME INTO #NAME
            ldaAdsl540a.getAds_Prtcpnt_View_Adp_Scnd_Annt_Lst_Nme()));
        DbsUtil.callnat(Adsn553.class , getCurrentProcessState(), pnd_Name);                                                                                              //Natural: CALLNAT 'ADSN553' #NAME
        if (condition(Global.isEscape())) return;
        ldaPstl0175.getPstl0175_Ann_Part_Name().setValue(pnd_Name);                                                                                                       //Natural: MOVE #NAME TO PSTL0175.ANN-PART-NAME
                                                                                                                                                                          //Natural: PERFORM DETERMINE-ANNUITY-OPTION
        sub_Determine_Annuity_Option();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM DETERMINE-CORRECT-ADDRESSES
        sub_Determine_Correct_Addresses();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //* * PERFORM REMAINDER-PROCESS
        //*   02122007 MN
                                                                                                                                                                          //Natural: PERFORM CREATE-ROLLOVER-DATA
        sub_Create_Rollover_Data();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_Date_A.setValueEdited(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Frst_Annt_Dte_Of_Brth(),new ReportEditMask("YYYYMMDD"));                                            //Natural: MOVE EDITED ADS-PRTCPNT-VIEW.ADP-FRST-ANNT-DTE-OF-BRTH ( EM = YYYYMMDD ) TO #DATE-A
        ldaPstl0175.getPstl0175_Adp_Frst_Annt_Dte_Of_Brth_N().setValue(pnd_Date_A_Pnd_Date_Yyyymmdd);                                                                     //Natural: MOVE #DATE-YYYYMMDD TO PSTL0175.ADP-FRST-ANNT-DTE-OF-BRTH-N
        pnd_Date_A.setValueEdited(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annty_Strt_Dte(),new ReportEditMask("YYYYMMDD"));                                                   //Natural: MOVE EDITED ADS-PRTCPNT-VIEW.ADP-ANNTY-STRT-DTE ( EM = YYYYMMDD ) TO #DATE-A
        ldaPstl0175.getPstl0175_Adp_Annty_Strt_Dte_N().setValue(pnd_Date_A_Pnd_Date_Yyyymmdd);                                                                            //Natural: MOVE #DATE-YYYYMMDD TO PSTL0175.ADP-ANNTY-STRT-DTE-N
        pnd_Date_A.setValueEdited(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Scnd_Annt_Dte_Of_Brth(),new ReportEditMask("YYYYMMDD"));                                            //Natural: MOVE EDITED ADS-PRTCPNT-VIEW.ADP-SCND-ANNT-DTE-OF-BRTH ( EM = YYYYMMDD ) TO #DATE-A
        ldaPstl0175.getPstl0175_Ann_Part_Dob().setValue(pnd_Date_A_Pnd_Date_Yyyymmdd);                                                                                    //Natural: MOVE #DATE-YYYYMMDD TO PSTL0175.ANN-PART-DOB
        pnd_Cur_Da_Contract_Count.reset();                                                                                                                                //Natural: RESET #CUR-DA-CONTRACT-COUNT CONTRACT #POST-ERROR #EFM-ERROR
        ldaPstl0175.getPstl0175_Contract().reset();
        pnd_Post_Error.reset();
        pnd_Efm_Error.reset();
        //*      MOVE-PARTICIPANT-DATA-TO-PDA
    }
    private void sub_Determine_Annuity_Option() throws Exception                                                                                                          //Natural: DETERMINE-ANNUITY-OPTION
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Beginning of DETERMINE-ANNUITY-OPTION for RQST-ID :",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                                       //Natural: WRITE 'Beginning of DETERMINE-ANNUITY-OPTION for RQST-ID :' ADS-PRTCPNT-VIEW.RQST-ID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet2097 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'SL' AND PSTL0175.ADP-GRNTEE-PERIOD = 0
        if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("SL") && ldaPstl0175.getPstl0175_Adp_Grntee_Period().equals(getZero())))
        {
            decideConditionsMet2097++;
            ldaPstl0175.getPstl0175_Adp_Annty_Optn().setValue(1);                                                                                                         //Natural: MOVE 01 TO PSTL0175.ADP-ANNTY-OPTN
        }                                                                                                                                                                 //Natural: WHEN ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'SL' AND PSTL0175.ADP-GRNTEE-PERIOD > 0
        else if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("SL") && ldaPstl0175.getPstl0175_Adp_Grntee_Period().greater(getZero())))
        {
            decideConditionsMet2097++;
            ldaPstl0175.getPstl0175_Adp_Annty_Optn().setValue(2);                                                                                                         //Natural: MOVE 02 TO PSTL0175.ADP-ANNTY-OPTN
        }                                                                                                                                                                 //Natural: WHEN ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'LH' AND PSTL0175.ADP-GRNTEE-PERIOD = 0
        else if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("LH") && ldaPstl0175.getPstl0175_Adp_Grntee_Period().equals(getZero())))
        {
            decideConditionsMet2097++;
            ldaPstl0175.getPstl0175_Adp_Annty_Optn().setValue(3);                                                                                                         //Natural: MOVE 03 TO PSTL0175.ADP-ANNTY-OPTN
        }                                                                                                                                                                 //Natural: WHEN ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'LH' AND PSTL0175.ADP-GRNTEE-PERIOD > 0
        else if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("LH") && ldaPstl0175.getPstl0175_Adp_Grntee_Period().greater(getZero())))
        {
            decideConditionsMet2097++;
            ldaPstl0175.getPstl0175_Adp_Annty_Optn().setValue(4);                                                                                                         //Natural: MOVE 04 TO PSTL0175.ADP-ANNTY-OPTN
        }                                                                                                                                                                 //Natural: WHEN ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'LF' AND PSTL0175.ADP-GRNTEE-PERIOD = 0
        else if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("LF") && ldaPstl0175.getPstl0175_Adp_Grntee_Period().equals(getZero())))
        {
            decideConditionsMet2097++;
            ldaPstl0175.getPstl0175_Adp_Annty_Optn().setValue(5);                                                                                                         //Natural: MOVE 05 TO PSTL0175.ADP-ANNTY-OPTN
        }                                                                                                                                                                 //Natural: WHEN ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'LF' AND PSTL0175.ADP-GRNTEE-PERIOD > 0
        else if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("LF") && ldaPstl0175.getPstl0175_Adp_Grntee_Period().greater(getZero())))
        {
            decideConditionsMet2097++;
            ldaPstl0175.getPstl0175_Adp_Annty_Optn().setValue(6);                                                                                                         //Natural: MOVE 06 TO PSTL0175.ADP-ANNTY-OPTN
        }                                                                                                                                                                 //Natural: WHEN ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'J' AND PSTL0175.ADP-GRNTEE-PERIOD = 0
        else if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("J") && ldaPstl0175.getPstl0175_Adp_Grntee_Period().equals(getZero())))
        {
            decideConditionsMet2097++;
            ldaPstl0175.getPstl0175_Adp_Annty_Optn().setValue(7);                                                                                                         //Natural: MOVE 07 TO PSTL0175.ADP-ANNTY-OPTN
        }                                                                                                                                                                 //Natural: WHEN ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'J' AND PSTL0175.ADP-GRNTEE-PERIOD > 0
        else if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("J") && ldaPstl0175.getPstl0175_Adp_Grntee_Period().greater(getZero())))
        {
            decideConditionsMet2097++;
            ldaPstl0175.getPstl0175_Adp_Annty_Optn().setValue(8);                                                                                                         //Natural: MOVE 08 TO PSTL0175.ADP-ANNTY-OPTN
        }                                                                                                                                                                 //Natural: WHEN ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'AC'
        else if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("AC")))
        {
            decideConditionsMet2097++;
            ldaPstl0175.getPstl0175_Adp_Annty_Optn().setValue(9);                                                                                                         //Natural: MOVE 09 TO PSTL0175.ADP-ANNTY-OPTN
        }                                                                                                                                                                 //Natural: WHEN ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'IR'
        else if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("IR")))
        {
            decideConditionsMet2097++;
            ldaPstl0175.getPstl0175_Adp_Annty_Optn().setValue(10);                                                                                                        //Natural: MOVE 10 TO PSTL0175.ADP-ANNTY-OPTN
        }                                                                                                                                                                 //Natural: WHEN ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'LT' AND PSTL0175.ADP-GRNTEE-PERIOD = 0
        else if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("LT") && ldaPstl0175.getPstl0175_Adp_Grntee_Period().equals(getZero())))
        {
            decideConditionsMet2097++;
            ldaPstl0175.getPstl0175_Adp_Annty_Optn().setValue(11);                                                                                                        //Natural: MOVE 11 TO PSTL0175.ADP-ANNTY-OPTN
        }                                                                                                                                                                 //Natural: WHEN ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'LT' AND PSTL0175.ADP-GRNTEE-PERIOD > 0
        else if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("LT") && ldaPstl0175.getPstl0175_Adp_Grntee_Period().greater(getZero())))
        {
            decideConditionsMet2097++;
            ldaPstl0175.getPstl0175_Adp_Annty_Optn().setValue(12);                                                                                                        //Natural: MOVE 12 TO PSTL0175.ADP-ANNTY-OPTN
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ldaPstl0175.getPstl0175_Adp_Annty_Optn().setValue(99);                                                                                                        //Natural: MOVE 99 TO PSTL0175.ADP-ANNTY-OPTN
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*     DETERMINE-ANNUITY-OPTION
    }
    private void sub_Determine_Correct_Addresses() throws Exception                                                                                                       //Natural: DETERMINE-CORRECT-ADDRESSES
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Beginning of DETERMINE-CORRECT-ADDRESSES for RQST-ID :",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                                    //Natural: WRITE 'Beginning of DETERMINE-CORRECT-ADDRESSES for RQST-ID :' ADS-PRTCPNT-VIEW.RQST-ID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Alt_Dest_Addr_Ind().equals("N")))                                                                               //Natural: IF ADS-PRTCPNT-VIEW.ADP-ALT-DEST-ADDR-IND = 'N'
        {
            ldaPstl0175.getPstl0175_Adp_Alt_Dest_Nme().setValue(ldaPstl0175.getPstl0175_Participant_Name());                                                              //Natural: MOVE PSTL0175.PARTICIPANT-NAME TO PSTL0175.ADP-ALT-DEST-NME
            ldaPstl0175.getPstl0175_Adp_Alt_Dest_Addr_Txt().getValue("*").setValue(ldaPstl0175.getPstl0175_Adp_Crrspndnce_Perm_Addr_Txt().getValue("*"));                 //Natural: MOVE PSTL0175.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( * ) TO PSTL0175.ADP-ALT-DEST-ADDR-TXT ( * )
            ldaPstl0175.getPstl0175_Adp_Alt_Dest_Addr_Zip().setValue(ldaPstl0175.getPstl0175_Adp_Crrspndnce_Perm_Addr_Zip());                                             //Natural: MOVE PSTL0175.ADP-CRRSPNDNCE-PERM-ADDR-ZIP TO PSTL0175.ADP-ALT-DEST-ADDR-ZIP
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaPstl0175.getPstl0175_Adp_Alt_Dest_Trnst_Cde().equals("000000000") || ldaPstl0175.getPstl0175_Adp_Alt_Dest_Trnst_Cde().equals("0")))              //Natural: IF PSTL0175.ADP-ALT-DEST-TRNST-CDE = '000000000' OR = '0'
        {
            ldaPstl0175.getPstl0175_Adp_Alt_Dest_Trnst_Cde().reset();                                                                                                     //Natural: RESET PSTL0175.ADP-ALT-DEST-TRNST-CDE
        }                                                                                                                                                                 //Natural: END-IF
        //* * FOR #I 1 TO 3
        //* * 02122007  MN  START
        if (condition(ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Trnst_Cde().getValue(1).equals("000000000") || ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Trnst_Cde().getValue(1).equals("0"))) //Natural: IF PSTL0175.ADP-RTB-ALT-DEST-TRNST-CDE ( 1 ) = '000000000' OR = '0'
        {
            ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Trnst_Cde().getValue(1).reset();                                                                                     //Natural: RESET PSTL0175.ADP-RTB-ALT-DEST-TRNST-CDE ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF (PSTL0175.ADP-RTB-ALT-DEST-RLVR-IND (1) = 'N' AND
        //*   PSTL0175.ADP-RTB-ALT-DEST-ADDR (1,1) = ' ')
        //*   MOVE ' ' TO PSTL0175.ADP-RTB-ALT-DEST-RLVR-IND (1)
        //*  END-IF
        //* * END-FOR
        //*  REMOVED EXTRANEOUS CODE, ADDED ELSE TO POPULATE FROM CORR ADDR
        //*  IF NECESSARY
        if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rtb_Alt_Addr_Ind().equals("Y")))                                                                                //Natural: IF ADP-RTB-ALT-ADDR-IND = 'Y'
        {
            if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rtb_Alt_Name().equals(" ")))                                                                                //Natural: IF ADS-PRTCPNT-VIEW.ADP-RTB-ALT-NAME = ' '
            {
                ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Carr_Nme().getValue(1).setValue(ldaPstl0175.getPstl0175_Participant_Name());                                     //Natural: MOVE PSTL0175.PARTICIPANT-NAME TO PSTL0175.ADP-RTB-ALT-DEST-CARR-NME ( 1 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Carr_Nme().getValue(1).setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rtb_Alt_Name());                             //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-RTB-ALT-NAME TO PSTL0175.ADP-RTB-ALT-DEST-CARR-NME ( 1 )
            }                                                                                                                                                             //Natural: END-IF
            ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Addr().getValue(1,"*").setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_Txt().getValue("*"));          //Natural: MOVE ADP-RTB-ALT-DEST-ADDR-TXT ( * ) TO PSTL0175.ADP-RTB-ALT-DEST-ADDR ( 1,* )
            ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Zip().getValue(1).setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_Zip());                             //Natural: MOVE ADP-RTB-ALT-DEST-ADDR-ZIP TO PSTL0175.ADP-RTB-ALT-DEST-ZIP ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Carr_Nme().getValue(1).setValue(ldaPstl0175.getPstl0175_Participant_Name());                                         //Natural: MOVE PSTL0175.PARTICIPANT-NAME TO PSTL0175.ADP-RTB-ALT-DEST-CARR-NME ( 1 )
            ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Addr().getValue(1,"*").setValue(ldaPstl0175.getPstl0175_Adp_Crrspndnce_Perm_Addr_Txt().getValue("*"));               //Natural: MOVE PSTL0175.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( * ) TO PSTL0175.ADP-RTB-ALT-DEST-ADDR ( 1,* )
            ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Zip().getValue(1).setValue(ldaPstl0175.getPstl0175_Adp_Crrspndnce_Perm_Addr_Zip());                                  //Natural: MOVE PSTL0175.ADP-CRRSPNDNCE-PERM-ADDR-ZIP TO PSTL0175.ADP-RTB-ALT-DEST-ZIP ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        //*  END NBC 04/2007
        //* *
        //*    DETERMINE-CORRECT-ADDRESSES
    }
    private void sub_Create_Rollover_Data() throws Exception                                                                                                              //Natural: CREATE-ROLLOVER-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Beginning of CREATE-ROLLOVER-DATA for RQST-ID :",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                                           //Natural: WRITE 'Beginning of CREATE-ROLLOVER-DATA for RQST-ID :' ADS-PRTCPNT-VIEW.RQST-ID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  FOR #A 1 3
        //*  IF (#RTB-ALTERNATE-DEST.NAP-RTB-ALT-DEST-RLVR-DEST-CDE (#A) = 'RTHC'
        if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Rlvr_Dest().equals("RTHC") || ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Rlvr_Dest().equals("RTHT"))) //Natural: IF ( ADS-PRTCPNT-VIEW.ADP-RTB-ALT-DEST-RLVR-DEST = 'RTHC' OR = 'RTHT' )
        {
            ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Ira_Typ().getValue(1).setValue("R");                                                                                 //Natural: MOVE 'R' TO PSTL0175.ADP-RTB-ALT-DEST-IRA-TYP ( 1 )
            //* * MOVE ADP-RTB-ALT-DEST-PPCN (#A) ?????
            //* *   TO PSTL0175.NAP-RTB-ALT-DEST-PPCN (#A) ?????
            //* * ESCAPE TOP
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Rlvr_Dest().equals("IRAC") || ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Rlvr_Dest().equals("IRAT"))) //Natural: IF ( ADS-PRTCPNT-VIEW.ADP-RTB-ALT-DEST-RLVR-DEST = 'IRAC' OR = 'IRAT' )
        {
            ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Ira_Typ().getValue(1).setValue("C");                                                                                 //Natural: MOVE 'C' TO PSTL0175.ADP-RTB-ALT-DEST-IRA-TYP ( 1 )
            //* * MOVE ADP-RTB-ALT-DEST-PPCN (#A) ????????????
            //* *   TO PSTL0175.NAP-RTB-ALT-DEST-PPCN (#A) ?????????
            //* *  ESCAPE TOP
        }                                                                                                                                                                 //Natural: END-IF
        //* * END-FOR
        //*    CREATE-ROLLOVER-DATA
    }
    private void sub_Move_Da_Contract_Data_To_Pda() throws Exception                                                                                                      //Natural: MOVE-DA-CONTRACT-DATA-TO-PDA
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Start of MOVE-DA-CONTRACT-DATA-TO-PDA");                                                                                               //Natural: WRITE 'Start of MOVE-DA-CONTRACT-DATA-TO-PDA'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cur_Da_Contract_Count.nadd(1);                                                                                                                                //Natural: ADD 1 TO #CUR-DA-CONTRACT-COUNT
        //*  PERFORM GET-NUMERIC-PRODUCT-CODES
        pnd_A.reset();                                                                                                                                                    //Natural: RESET #A #J #K
        pnd_J.reset();
        pnd_K.reset();
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "For contract :",ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr());                                                                        //Natural: WRITE 'For contract :' ADS-CNTRCT-VIEW.ADC-TIAA-NBR
            if (Global.isEscape()) return;
            getReports().write(0, "=",pnd_Cur_Da_Contract_Count);                                                                                                         //Natural: WRITE '=' #CUR-DA-CONTRACT-COUNT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Payout().getValue(1,1).setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Ia_Tiaa_Nbr());                                         //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-IA-TIAA-NBR TO PSTL0175.ADC-TIAA-ACCT-PAYOUT ( 1,1 )
        ldaPstl0175.getPstl0175_Adc_Da_Tiaa_Nbr().getValue(pnd_Cur_Da_Contract_Count).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr());                            //Natural: MOVE ADS-CNTRCT-VIEW.ADC-TIAA-NBR TO PSTL0175.ADC-DA-TIAA-NBR ( #CUR-DA-CONTRACT-COUNT )
        ldaPstl0175.getPstl0175_Adc_Da_Cref_Nbr().getValue(pnd_Cur_Da_Contract_Count).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Cref_Nbr());                            //Natural: MOVE ADS-CNTRCT-VIEW.ADC-CREF-NBR TO PSTL0175.ADC-DA-CREF-NBR ( #CUR-DA-CONTRACT-COUNT )
        //*  02122007
        pdaAdsa555.getTax_Linkage_Ads_Rtb_T_Pymt_Ind().reset();                                                                                                           //Natural: RESET TAX-LINKAGE.ADS-RTB-T-PYMT-IND
        //*  02122007
        pdaAdsa555.getTax_Linkage_Ads_Rtb_C_Pymt_Ind().reset();                                                                                                           //Natural: RESET TAX-LINKAGE.ADS-RTB-C-PYMT-IND
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            //*  RESET #WORK-TIAA-ACCT-CDE
            //*        #WORK-CREF-ACCT-CDE
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Tkr_Symbl().getValue(pnd_I).equals(" ")))                                                          //Natural: IF ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-TKR-SYMBL ( #I ) = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    EXAMINE  NECA4000.CFN-TICKER-SYMBOL(*)    /* RS0
                //*  RS0
                DbsUtil.examine(new ExamineSource(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Ticker().getValue("*")), new ExamineSearch(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Tkr_Symbl().getValue(pnd_I)),  //Natural: EXAMINE #PARM-AREA.#ACCT-TICKER ( * ) FOR ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-TKR-SYMBL ( #I ) INDEX #B
                    new ExamineGivingIndex(pnd_B));
                if (condition(pnd_B.notEquals(getZero())))                                                                                                                //Natural: IF #B NE 0
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, "TICKER DOES NOT EXIST IN EXTERNAL :",ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Tkr_Symbl().getValue(pnd_I),                 //Natural: WRITE 'TICKER DOES NOT EXIST IN EXTERNAL :' ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-TKR-SYMBL ( #I ) 'FOR REQUEST ' ADS-CNTRCT-VIEW.RQST-ID ' CONTRACT :' ADS-CNTRCT-VIEW.ADC-TIAA-NBR
                        "FOR REQUEST ",ldaAdsl401a.getAds_Cntrct_View_Rqst_Id()," CONTRACT :",ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr());
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  OS-102013
                    DbsUtil.terminate(99);  if (true) return;                                                                                                             //Natural: TERMINATE 99
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* * EM - 012709 START
            //*  OS-102013 START
            if (condition(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_B).equals("TSA0")))                                                       //Natural: IF #ACCT-INVSTMNT-GRPNG-ID ( #B ) = 'TSA0'
            {
                ldaPstl0175.getPstl0175_Stable_Return_Ind().setValue("Y");                                                                                                //Natural: ASSIGN PSTL0175.STABLE-RETURN-IND := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_B).equals("TSV0")))                                                   //Natural: IF #ACCT-INVSTMNT-GRPNG-ID ( #B ) = 'TSV0'
                {
                    ldaPstl0175.getPstl0175_Stable_Return_Ind().setValue("V");                                                                                            //Natural: ASSIGN PSTL0175.STABLE-RETURN-IND := 'V'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* * EM - 012709 END
            //*  OS-102013 START
            if (condition(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_B).equals(pnd_Tiaa_Acct.getValue("*"))))                                  //Natural: IF #ACCT-INVSTMNT-GRPNG-ID ( #B ) = #TIAA-ACCT ( * )
            {
                //*  IF ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-TKR-SYMBL (#I) =  'TIAA#'
                //*      OR = 'TSAF#' OR = 'TSVX#'        /* DEA     /* EM - 012709
                //*  OS-102013 END
                                                                                                                                                                          //Natural: PERFORM TIAA-REA-PROCESS
                sub_Tiaa_Rea_Process();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                //* * 11102006 MN    START
                if (condition(pnd_Jindex_Error.getBoolean()))                                                                                                             //Natural: IF #JINDEX-ERROR
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                //* * 11102006 MN    END
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  OS-102013 START
                if (condition(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_B).equals("REA1") || pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_B).equals("REX1")  //Natural: IF #ACCT-INVSTMNT-GRPNG-ID ( #B ) = 'REA1' OR = 'REX1' OR = 'WA50'
                    || pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_B).equals("WA50")))
                {
                    //*    IF ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-TKR-SYMBL (#I) =  'TREA#'
                    //*        OR = 'WA51#'                             /* EM - 012709
                    //*  OS-102013 END
                                                                                                                                                                          //Natural: PERFORM TIAA-REA-PROCESS
                    sub_Tiaa_Rea_Process();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //* * 11102006 MN    START
                    if (condition(pnd_Jindex_Error.getBoolean()))                                                                                                         //Natural: IF #JINDEX-ERROR
                    {
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                    //* * 11102006 MN    END
                                                                                                                                                                          //Natural: PERFORM CREF-PROCESS
                    sub_Cref_Process();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  CREF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM CREF-PROCESS
                    sub_Cref_Process();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *  02122007  MN START
        if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rtb_Ind().equals("Y")))                                                                                         //Natural: IF ADS-PRTCPNT-VIEW.ADP-RTB-IND = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM RTB-PROCESS
            sub_Rtb_Process();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *  02122007  MN END
        if (condition(ldaPstl0175.getPstl0175_Adc_Cref_Acct_Payout().getValue("*","*").equals(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Ia_Cref_Nbr())))                        //Natural: IF PSTL0175.ADC-CREF-ACCT-PAYOUT ( *,* ) = ADS-PRTCPNT-VIEW.ADP-IA-CREF-NBR
        {
            if (condition(pnd_Debug_On.getBoolean()))                                                                                                                     //Natural: IF #DEBUG-ON
            {
                getReports().write(0, NEWLINE,"*");                                                                                                                       //Natural: WRITE / '*'
                if (Global.isEscape()) return;
                getReports().write(0, "ADP-IA-CREF-NBR = PSTL0175.ADC-CREF-ACCT-PAYOU");                                                                                  //Natural: WRITE 'ADP-IA-CREF-NBR = PSTL0175.ADC-CREF-ACCT-PAYOU'
                if (Global.isEscape()) return;
                getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Cref_Acct_Payout().getValue("*","*"));                                                              //Natural: WRITE '=' PSTL0175.ADC-CREF-ACCT-PAYOUT ( *,* )
                if (Global.isEscape()) return;
                getReports().write(0, "=",ldaAdsl540a.getAds_Prtcpnt_View_Adp_Ia_Cref_Nbr());                                                                             //Natural: WRITE '=' ADS-PRTCPNT-VIEW.ADP-IA-CREF-NBR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Debug_On.getBoolean()))                                                                                                                     //Natural: IF #DEBUG-ON
            {
                getReports().write(0, NEWLINE,"*");                                                                                                                       //Natural: WRITE / '*'
                if (Global.isEscape()) return;
                getReports().write(0, "ADP-IA-CREF-NBR not = PSTL0175.ADC-CREF-ACCT-PAYOU");                                                                              //Natural: WRITE 'ADP-IA-CREF-NBR not = PSTL0175.ADC-CREF-ACCT-PAYOU'
                if (Global.isEscape()) return;
                getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Cref_Acct_Payout().getValue("*","*"));                                                              //Natural: WRITE '=' PSTL0175.ADC-CREF-ACCT-PAYOUT ( *,* )
                if (Global.isEscape()) return;
                getReports().write(0, "=",ldaAdsl540a.getAds_Prtcpnt_View_Adp_Ia_Cref_Nbr());                                                                             //Natural: WRITE '=' ADS-PRTCPNT-VIEW.ADP-IA-CREF-NBR
                if (Global.isEscape()) return;
                //*    WRITE '=' PSTL0175.ADC-CREF-ACCT-CDE (*,*)
            }                                                                                                                                                             //Natural: END-IF
            //* * EM - 012709
            //*  OS-102013 START
            //*  IF (PSTL0175.ADC-CREF-ACCT-CDE (#CUR-DA-CONTRACT-COUNT,1) EQ '9' OR
            //*      = '1112')     AND
            //*      (PSTL0175.ADC-CREF-ACCT-CDE (#CUR-DA-CONTRACT-COUNT,2) NE '9' AND
            //*      PSTL0175.ADC-CREF-ACCT-CDE (#CUR-DA-CONTRACT-COUNT,2) NE '1112')
            //*    MOVE ADS-PRTCPNT-VIEW.ADP-IA-CREF-NBR TO
            //*      PSTL0175.ADC-CREF-ACCT-PAYOUT(#CUR-DA-CONTRACT-COUNT,2)
            //*  ELSE
            //*    EXAMINE FULL PSTL0175.ADC-CREF-ACCT-CDE (#CUR-DA-CONTRACT-COUNT,*)
            //*      FOR ' ' GIVING NUMBER IN #NUMBER
            //*    IF #NUMBER = 40
            //*      IGNORE
            //*    ELSE
            if (condition(DbsUtil.maskMatches(ldaPstl0175.getPstl0175_Adc_Cref_Acct_Nme().getValue(pnd_Cur_Da_Contract_Count,1),"'TIAA'") && ! (DbsUtil.maskMatches(ldaPstl0175.getPstl0175_Adc_Cref_Acct_Nme().getValue(pnd_Cur_Da_Contract_Count, //Natural: IF ( PSTL0175.ADC-CREF-ACCT-NME ( #CUR-DA-CONTRACT-COUNT,1 ) EQ MASK ( 'TIAA' ) ) AND ( PSTL0175.ADC-CREF-ACCT-NME ( #CUR-DA-CONTRACT-COUNT,2 ) NE MASK ( 'TIAA' ) )
                2),"'TIAA'"))))
            {
                ldaPstl0175.getPstl0175_Adc_Cref_Acct_Payout().getValue(pnd_Cur_Da_Contract_Count,2).setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Ia_Cref_Nbr());         //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-IA-CREF-NBR TO PSTL0175.ADC-CREF-ACCT-PAYOUT ( #CUR-DA-CONTRACT-COUNT,2 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaPstl0175.getPstl0175_Adc_Cref_Acct_Nme().getValue(pnd_Cur_Da_Contract_Count,1).equals(" ")))                                             //Natural: IF PSTL0175.ADC-CREF-ACCT-NME ( #CUR-DA-CONTRACT-COUNT,1 ) = ' '
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl0175.getPstl0175_Adc_Cref_Acct_Payout().getValue(pnd_Cur_Da_Contract_Count,1).setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Ia_Cref_Nbr());     //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-IA-CREF-NBR TO PSTL0175.ADC-CREF-ACCT-PAYOUT ( #CUR-DA-CONTRACT-COUNT,1 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  OS-102013 END
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM VARIABLE-INCOME-METHOD-PAYMENT
        sub_Variable_Income_Method_Payment();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (condition(pnd_J.equals(getZero())))                                                                                                                           //Natural: IF #J = 0
        {
            //*  02122007
            ldaPstl0175.getPstl0175_Adc_Da_Tiaa_Nbr().getValue(pnd_Cur_Da_Contract_Count).reset();                                                                        //Natural: RESET PSTL0175.ADC-DA-TIAA-NBR ( #CUR-DA-CONTRACT-COUNT ) PSTL0175.ADC-TIAA-RTB-NBR ( #CUR-DA-CONTRACT-COUNT )
            ldaPstl0175.getPstl0175_Adc_Tiaa_Rtb_Nbr().getValue(pnd_Cur_Da_Contract_Count).reset();
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_K.equals(getZero())))                                                                                                                           //Natural: IF #K = 0
        {
            //*  02122007
            ldaPstl0175.getPstl0175_Adc_Da_Cref_Nbr().getValue(pnd_Cur_Da_Contract_Count).reset();                                                                        //Natural: RESET PSTL0175.ADC-DA-CREF-NBR ( #CUR-DA-CONTRACT-COUNT ) PSTL0175.ADC-CREF-RTB-NBR ( #CUR-DA-CONTRACT-COUNT )
            ldaPstl0175.getPstl0175_Adc_Cref_Rtb_Nbr().getValue(pnd_Cur_Da_Contract_Count).reset();
        }                                                                                                                                                                 //Natural: END-IF
        //*    MOVE-DA-CONTRACT-DATA-TO-PDA
    }
    private void sub_Variable_Income_Method_Payment() throws Exception                                                                                                    //Natural: VARIABLE-INCOME-METHOD-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Start of VARIABLE-INCOME-METHOD-PAYMENT");                                                                                             //Natural: WRITE 'Start of VARIABLE-INCOME-METHOD-PAYMENT'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_X.setValue(1);                                                                                                                                                //Natural: MOVE 1 TO #X
        PND_PND_L2758:                                                                                                                                                    //Natural: FOR #I 1 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            //*  OS-102013
            //*  IF PSTL0175.ADC-CREF-ACCT-CDE (#CUR-DA-CONTRACT-COUNT,#X) EQ ' '
            if (condition(ldaPstl0175.getPstl0175_Adc_Cref_Acct_Nme().getValue(pnd_Cur_Da_Contract_Count,pnd_X).equals(" ")))                                             //Natural: IF PSTL0175.ADC-CREF-ACCT-NME ( #CUR-DA-CONTRACT-COUNT,#X ) EQ ' '
            {
                if (true) break PND_PND_L2758;                                                                                                                            //Natural: ESCAPE BOTTOM ( ##L2758. )
            }                                                                                                                                                             //Natural: END-IF
            //*  OS-102013 START
            DbsUtil.examine(new ExamineSource(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Ticker().getValue("*")), new ExamineSearch(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Tkr_Symbl().getValue(pnd_I)),  //Natural: EXAMINE #PARM-AREA.#ACCT-TICKER ( * ) FOR ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-TKR-SYMBL ( #I ) GIVING INDEX #IN
                new ExamineGivingIndex(pnd_In));
            if (condition(pnd_In.greater(getZero()) && pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_In).equals(pnd_Tiaa_Acct.getValue("*"))))    //Natural: IF #IN GT 0 AND #ACCT-INVSTMNT-GRPNG-ID ( #IN ) = #TIAA-ACCT ( * )
            {
                //*  IF ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-TKR-SYMBL (#I) = 'TIAA#'
                //*      OR = 'TSAF#' OR = 'TSVX#'                 /* EM - 012709 DEA
                //*  OS-102013 END
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Acct_Typ().getValue(pnd_I).equals(" ")))                                                           //Natural: IF ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-ACCT-TYP ( #I ) EQ ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  EXAMINE #PARM-AREA.#ACCT-NUM-FUND-CDE-A(*)
            //*  FOR PSTL0175.ADC-CREF-ACCT-CDE (#CUR-DA-CONTRACT-COUNT,#X)     GIVING
            //*  INDEX #IN                                                    /* DEA
            //*  IF #IN = 0                                                  /* DEA
            //*    ADD 1 TO #X
            //*    ESCAPE TOP /* (2758)
            //*  END-IF                                                      /* DEA
            //*  DEA
            DbsUtil.examine(new ExamineSource(ldaAdsl401a.getAds_Cntrct_View_Adc_Tkr_Symbl().getValue("*")), new ExamineSearch(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Tkr_Symbl().getValue(pnd_I)),  //Natural: EXAMINE ADS-CNTRCT-VIEW.ADC-TKR-SYMBL ( * ) FOR ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-TKR-SYMBL ( #I ) GIVING INDEX #IO
                new ExamineGivingIndex(pnd_Io));
            //*  DEA
            if (condition(pnd_Io.equals(getZero())))                                                                                                                      //Natural: IF #IO = 0
            {
                pnd_X.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #X
                //*  (2758)
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  DEA
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Acct_Typ().getValue(pnd_I).equals("P")))                                                           //Natural: IF ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-ACCT-TYP ( #I ) = 'P'
            {
                //*  DEA
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Typ().getValue(pnd_Io).equals("P") || ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Typ().getValue(pnd_Io).equals("%"))) //Natural: IF ADC-GRD-MNTHLY-TYP ( #IO ) EQ 'P' OR EQ '%'
                {
                    //*  DEA
                    ldaPstl0175.getPstl0175_Adc_Cref_Grd_Mnthly_Typ().getValue(pnd_Cur_Da_Contract_Count,pnd_X).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Typ().getValue(pnd_Io)); //Natural: MOVE ADC-GRD-MNTHLY-TYP ( #IO ) TO PSTL0175.ADC-CREF-GRD-MNTHLY-TYP ( #CUR-DA-CONTRACT-COUNT,#X )
                    //*  DEA
                    ldaPstl0175.getPstl0175_Adc_Cref_Grd_Mnthly_Qty().getValue(pnd_Cur_Da_Contract_Count,pnd_X).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Qty().getValue(pnd_Io)); //Natural: MOVE ADC-GRD-MNTHLY-QTY ( #IO ) TO PSTL0175.ADC-CREF-GRD-MNTHLY-QTY ( #CUR-DA-CONTRACT-COUNT,#X )
                    //*  DEA
                    ldaPstl0175.getPstl0175_Adc_Cref_Acct_Rem_Qty().getValue(pnd_Cur_Da_Contract_Count,pnd_X).compute(new ComputeParameters(false, ldaPstl0175.getPstl0175_Adc_Cref_Acct_Rem_Qty().getValue(pnd_Cur_Da_Contract_Count,pnd_X)),  //Natural: COMPUTE PSTL0175.ADC-CREF-ACCT-REM-QTY ( #CUR-DA-CONTRACT-COUNT,#X ) = 100 - ADC-GRD-MNTHLY-QTY ( #IO )
                        DbsField.subtract(100,ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Qty().getValue(pnd_Io)));
                    //*   ADC-GRD-MNTHLY-TYP (#IO) EQ 'D' OR ' ' /* DEA
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  DEA
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Typ().getValue(pnd_Io).equals(" ")))                                                      //Natural: IF ADC-GRD-MNTHLY-TYP ( #IO ) EQ ' '
                    {
                        ldaPstl0175.getPstl0175_Adc_Cref_Grd_Mnthly_Typ().getValue(pnd_Cur_Da_Contract_Count,pnd_X).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Acct_Typ().getValue(pnd_I)); //Natural: MOVE ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-ACCT-TYP ( #I ) TO PSTL0175.ADC-CREF-GRD-MNTHLY-TYP ( #CUR-DA-CONTRACT-COUNT,#X )
                        ldaPstl0175.getPstl0175_Adc_Cref_Acct_Rem_Qty().getValue(pnd_Cur_Da_Contract_Count,pnd_X).setValue(100);                                          //Natural: MOVE 100 TO PSTL0175.ADC-CREF-ACCT-REM-QTY ( #CUR-DA-CONTRACT-COUNT,#X )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  DEA
                        ldaPstl0175.getPstl0175_Adc_Cref_Grd_Mnthly_Typ().getValue(pnd_Cur_Da_Contract_Count,pnd_X).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Typ().getValue(pnd_Io)); //Natural: MOVE ADC-GRD-MNTHLY-TYP ( #IO ) TO PSTL0175.ADC-CREF-GRD-MNTHLY-TYP ( #CUR-DA-CONTRACT-COUNT,#X )
                        //*  DEA
                        ldaPstl0175.getPstl0175_Adc_Cref_Grd_Mnthly_Qty().getValue(pnd_Cur_Da_Contract_Count,pnd_X).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Qty().getValue(pnd_Io)); //Natural: MOVE ADC-GRD-MNTHLY-QTY ( #IO ) TO PSTL0175.ADC-CREF-GRD-MNTHLY-QTY ( #CUR-DA-CONTRACT-COUNT,#X )
                        ldaPstl0175.getPstl0175_Adc_Cref_Acct_Rem_Qty().getValue(pnd_Cur_Da_Contract_Count,pnd_X).reset();                                                //Natural: RESET PSTL0175.ADC-CREF-ACCT-REM-QTY ( #CUR-DA-CONTRACT-COUNT,#X )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  DEA
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Typ().getValue(pnd_Io).equals("P") || ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Typ().getValue(pnd_Io).equals("%"))) //Natural: IF ADC-GRD-MNTHLY-TYP ( #IO ) EQ 'P' OR EQ '%'
                {
                    //*  DEA
                    ldaPstl0175.getPstl0175_Adc_Cref_Grd_Mnthly_Typ().getValue(pnd_Cur_Da_Contract_Count,pnd_X).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Typ().getValue(pnd_Io)); //Natural: MOVE ADC-GRD-MNTHLY-TYP ( #IO ) TO PSTL0175.ADC-CREF-GRD-MNTHLY-TYP ( #CUR-DA-CONTRACT-COUNT,#X )
                    //*  DEA
                    ldaPstl0175.getPstl0175_Adc_Cref_Grd_Mnthly_Qty().getValue(pnd_Cur_Da_Contract_Count,pnd_X).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Qty().getValue(pnd_Io)); //Natural: MOVE ADC-GRD-MNTHLY-QTY ( #IO ) TO PSTL0175.ADC-CREF-GRD-MNTHLY-QTY ( #CUR-DA-CONTRACT-COUNT,#X )
                    //*  DEA
                    ldaPstl0175.getPstl0175_Adc_Cref_Acct_Rem_Qty().getValue(pnd_Cur_Da_Contract_Count,pnd_X).compute(new ComputeParameters(false, ldaPstl0175.getPstl0175_Adc_Cref_Acct_Rem_Qty().getValue(pnd_Cur_Da_Contract_Count,pnd_X)),  //Natural: COMPUTE PSTL0175.ADC-CREF-ACCT-REM-QTY ( #CUR-DA-CONTRACT-COUNT,#X ) = 100 - ADC-GRD-MNTHLY-QTY ( #IO )
                        DbsField.subtract(100,ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Qty().getValue(pnd_Io)));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  DEA
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Typ().getValue(pnd_Io).equals(" ")))                                                      //Natural: IF ADC-GRD-MNTHLY-TYP ( #IO ) EQ ' '
                    {
                        ldaPstl0175.getPstl0175_Adc_Cref_Grd_Mnthly_Typ().getValue(pnd_Cur_Da_Contract_Count,pnd_X).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Acct_Typ().getValue(pnd_I)); //Natural: MOVE ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-ACCT-TYP ( #I ) TO PSTL0175.ADC-CREF-GRD-MNTHLY-TYP ( #CUR-DA-CONTRACT-COUNT,#X )
                        ldaPstl0175.getPstl0175_Adc_Cref_Acct_Rem_Qty().getValue(pnd_Cur_Da_Contract_Count,pnd_X).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Acct_Qty().getValue(pnd_I)); //Natural: MOVE ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-ACCT-QTY ( #I ) TO PSTL0175.ADC-CREF-ACCT-REM-QTY ( #CUR-DA-CONTRACT-COUNT,#X )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  DEA
                        ldaPstl0175.getPstl0175_Adc_Cref_Grd_Mnthly_Typ().getValue(pnd_Cur_Da_Contract_Count,pnd_X).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Typ().getValue(pnd_Io)); //Natural: MOVE ADC-GRD-MNTHLY-TYP ( #IO ) TO PSTL0175.ADC-CREF-GRD-MNTHLY-TYP ( #CUR-DA-CONTRACT-COUNT,#X )
                        //*  DEA
                        ldaPstl0175.getPstl0175_Adc_Cref_Grd_Mnthly_Qty().getValue(pnd_Cur_Da_Contract_Count,pnd_X).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Qty().getValue(pnd_Io)); //Natural: MOVE ADC-GRD-MNTHLY-QTY ( #IO ) TO PSTL0175.ADC-CREF-GRD-MNTHLY-QTY ( #CUR-DA-CONTRACT-COUNT,#X )
                        ldaPstl0175.getPstl0175_Adc_Cref_Acct_Rem_Qty().getValue(pnd_Cur_Da_Contract_Count,pnd_X).compute(new ComputeParameters(false,                    //Natural: COMPUTE PSTL0175.ADC-CREF-ACCT-REM-QTY ( #CUR-DA-CONTRACT-COUNT,#X ) = ADS-CNTRCT-VIEW.ADC-ACCT-QTY ( #IO ) - ADC-GRD-MNTHLY-QTY ( #IO )
                            ldaPstl0175.getPstl0175_Adc_Cref_Acct_Rem_Qty().getValue(pnd_Cur_Da_Contract_Count,pnd_X)), ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Qty().getValue(pnd_Io).subtract(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Qty().getValue(pnd_Io)));
                        //*  DEA
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_X.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #X
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*     VARIABLE-INCOME-METHOD-PAYMENT
    }
    private void sub_Tiaa_Rea_Process() throws Exception                                                                                                                  //Natural: TIAA-REA-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Start of TIAA-REA-PROCESS ");                                                                                                          //Natural: WRITE 'Start of TIAA-REA-PROCESS '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* * 11102006 MN    START
        //*  EM - 012709
        if (condition(pnd_J.equals(3)))                                                                                                                                   //Natural: IF #J = 3
        {
            pnd_Jindex_Error.setValue(true);                                                                                                                              //Natural: ASSIGN #JINDEX-ERROR := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* * 11102006 MN    END
        pnd_J.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #J
        //*  MOVE NECA4000.CFN-FND-NUM-FUND-CDE (#B) TO       /* RS0
        //*  OS-102013 START
        //* *MOVE #PARM-AREA.#ACCT-NUM-FUND-CDE (#B) TO       /* RS0
        //* *PSTL0175.ADC-TIAA-ACCT-CDE(#CUR-DA-CONTRACT-COUNT,#J)
        ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Nme().getValue(pnd_Cur_Da_Contract_Count,pnd_J).setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Name_18().getValue(pnd_B));   //Natural: MOVE #PARM-AREA.#ACCT-NAME-18 ( #B ) TO PSTL0175.ADC-TIAA-ACCT-NME ( #CUR-DA-CONTRACT-COUNT,#J )
        //*  OS-102013
        ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Typ().getValue(pnd_Cur_Da_Contract_Count,pnd_J).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Acct_Typ().getValue(pnd_I)); //Natural: MOVE ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-ACCT-TYP ( #I ) TO PSTL0175.ADC-TIAA-ACCT-TYP ( #CUR-DA-CONTRACT-COUNT,#J )
        ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Qty().getValue(pnd_Cur_Da_Contract_Count,pnd_J).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Acct_Qty().getValue(pnd_I)); //Natural: MOVE ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-ACCT-QTY ( #I ) TO PSTL0175.ADC-TIAA-ACCT-QTY ( #CUR-DA-CONTRACT-COUNT,#J )
        //* **********************
        //* * EM - 012709 START
        //*  OS-102013 START
        //* *IF PSTL0175.ADC-TIAA-ACCT-CDE (#CUR-DA-CONTRACT-COUNT,#J) = '9'
        //* *    OR = '1112'                                 /* EM - 012709
        if (condition(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_B).equals("REA1") || pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_B).equals("REX1")  //Natural: IF #ACCT-INVSTMNT-GRPNG-ID ( #B ) = 'REA1' OR = 'REX1' OR = 'WA50'
            || pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_B).equals("WA50")))
        {
            //*  OS-102013 END
            ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Payout().getValue(pnd_Cur_Da_Contract_Count,pnd_J).setValueEdited(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Ia_Tiaa_Nbr(),new //Natural: MOVE EDITED ADS-PRTCPNT-VIEW.ADP-IA-TIAA-NBR ( EM = XXXXXXXX-REA ) TO PSTL0175.ADC-TIAA-ACCT-PAYOUT ( #CUR-DA-CONTRACT-COUNT,#J )
                ReportEditMask("XXXXXXXX-REA"));
            //* *  #REAL-NBR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Payout().getValue(pnd_Cur_Da_Contract_Count,pnd_J).equals(" ")))                                          //Natural: IF PSTL0175.ADC-TIAA-ACCT-PAYOUT ( #CUR-DA-CONTRACT-COUNT,#J ) = ' '
            {
                ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Payout().getValue(pnd_Cur_Da_Contract_Count,pnd_J).setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Ia_Tiaa_Nbr());     //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-IA-TIAA-NBR TO PSTL0175.ADC-TIAA-ACCT-PAYOUT ( #CUR-DA-CONTRACT-COUNT,#J )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* * EM - 012709 - END
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, "=",pnd_J);                                                                                                                             //Natural: WRITE '=' #J
            if (Global.isEscape()) return;
            getReports().write(0, "=",pnd_Cur_Da_Contract_Count);                                                                                                         //Natural: WRITE '=' #CUR-DA-CONTRACT-COUNT
            if (Global.isEscape()) return;
            //*  WRITE '=' PSTL0175.ADC-TIAA-ACCT-CDE(#CUR-DA-CONTRACT-COUNT,#J)
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Nme().getValue(pnd_Cur_Da_Contract_Count,pnd_J));                                             //Natural: WRITE '=' PSTL0175.ADC-TIAA-ACCT-NME ( #CUR-DA-CONTRACT-COUNT,#J )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Typ().getValue(pnd_Cur_Da_Contract_Count,pnd_J));                                             //Natural: WRITE '=' PSTL0175.ADC-TIAA-ACCT-TYP ( #CUR-DA-CONTRACT-COUNT,#J )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Qty().getValue(pnd_Cur_Da_Contract_Count,pnd_J));                                             //Natural: WRITE '=' PSTL0175.ADC-TIAA-ACCT-QTY ( #CUR-DA-CONTRACT-COUNT,#J )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Payout().getValue(pnd_Cur_Da_Contract_Count,pnd_J));                                          //Natural: WRITE '=' PSTL0175.ADC-TIAA-ACCT-PAYOUT ( #CUR-DA-CONTRACT-COUNT,#J )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  EM - 012709 START
        //* * IF PSTL0175.ADC-TIAA-ACCT-PAYOUT (*,*) = #REAL-NBR
        //* *  IGNORE
        //* * ELSE
        //* *  MOVE #REAL-NBR
        //* *    TO PSTL0175.ADC-TIAA-ACCT-PAYOUT (#CUR-DA-CONTRACT-COUNT,#J)
        //* *  RESET #REAL-NBR
        //* * END-IF
        //*  EM - 012709 - END
        if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Spcfc_Brkdwn_Ind().equals("Y") && (pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_B).equals(pnd_Tiaa_Acct.getValue("*"))))) //Natural: IF ADP-SPCFC-BRKDWN-IND = 'Y' AND ( #ACCT-INVSTMNT-GRPNG-ID ( #B ) = #TIAA-ACCT ( * ) )
        {
            //*  OS-102013 END
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Acct_Typ().getValue(pnd_I).equals("P")))                                                           //Natural: IF ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-ACCT-TYP ( #I ) = 'P'
            {
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Typ().getValue(pnd_I).equals("P") || ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Typ().getValue(pnd_I).equals("%"))) //Natural: IF ADS-CNTRCT-VIEW.ADC-GRD-MNTHLY-TYP ( #I ) = 'P' OR = '%'
                {
                    ldaPstl0175.getPstl0175_Adp_Settl_Pct_Dol_Swtch().setValue("P");                                                                                      //Natural: MOVE 'P' TO PSTL0175.ADP-SETTL-PCT-DOL-SWTCH
                    ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Grd_Pct().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Qty().getValue(pnd_I));                   //Natural: MOVE ADS-CNTRCT-VIEW.ADC-GRD-MNTHLY-QTY ( #I ) TO PSTL0175.ADP-SETTL-ALL-TIAA-GRD-PCT
                    ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Std_Pct().compute(new ComputeParameters(false, ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Std_Pct()),      //Natural: COMPUTE PSTL0175.ADP-SETTL-ALL-TIAA-STD-PCT = 100 - PSTL0175.ADP-SETTL-ALL-TIAA-GRD-PCT
                        DbsField.subtract(100,ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Grd_Pct()));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Typ().getValue(pnd_I).equals(" ")))                                                       //Natural: IF ADS-CNTRCT-VIEW.ADC-GRD-MNTHLY-TYP ( #I ) = ' '
                    {
                        ldaPstl0175.getPstl0175_Adp_Settl_Pct_Dol_Swtch().setValue("P");                                                                                  //Natural: MOVE 'P' TO PSTL0175.ADP-SETTL-PCT-DOL-SWTCH
                        ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Std_Pct().setValue(100);                                                                               //Natural: MOVE 100 TO PSTL0175.ADP-SETTL-ALL-TIAA-STD-PCT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaPstl0175.getPstl0175_Adp_Settl_Pct_Dol_Swtch().setValue("D");                                                                                  //Natural: MOVE 'D' TO PSTL0175.ADP-SETTL-PCT-DOL-SWTCH
                        ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Grd_Amt().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Qty().getValue(pnd_I));               //Natural: MOVE ADS-CNTRCT-VIEW.ADC-GRD-MNTHLY-QTY ( #I ) TO PSTL0175.ADP-SETTL-ALL-TIAA-GRD-AMT
                        ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Grd_Flg().setValue("Y");                                                                               //Natural: MOVE 'Y' TO PSTL0175.ADP-SETTL-ALL-TIAA-GRD-FLG
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Typ().getValue(pnd_I).equals("P") || ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Typ().getValue(pnd_I).equals("%"))) //Natural: IF ADS-CNTRCT-VIEW.ADC-GRD-MNTHLY-TYP ( #I ) = 'P' OR = '%'
                {
                    ldaPstl0175.getPstl0175_Adp_Settl_Pct_Dol_Swtch().setValue("P");                                                                                      //Natural: MOVE 'P' TO PSTL0175.ADP-SETTL-PCT-DOL-SWTCH
                    ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Grd_Pct().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Qty().getValue(pnd_I));                   //Natural: MOVE ADS-CNTRCT-VIEW.ADC-GRD-MNTHLY-QTY ( #I ) TO PSTL0175.ADP-SETTL-ALL-TIAA-GRD-PCT
                    ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Std_Pct().compute(new ComputeParameters(false, ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Std_Pct()),      //Natural: COMPUTE PSTL0175.ADP-SETTL-ALL-TIAA-STD-PCT = 100 - PSTL0175.ADP-SETTL-ALL-TIAA-GRD-PCT
                        DbsField.subtract(100,ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Grd_Pct()));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Typ().getValue(pnd_I).equals(" ")))                                                       //Natural: IF ADS-CNTRCT-VIEW.ADC-GRD-MNTHLY-TYP ( #I ) = ' '
                    {
                        ldaPstl0175.getPstl0175_Adp_Settl_Pct_Dol_Swtch().setValue("P");                                                                                  //Natural: MOVE 'P' TO PSTL0175.ADP-SETTL-PCT-DOL-SWTCH
                        ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Std_Pct().setValue(100);                                                                               //Natural: MOVE 100 TO PSTL0175.ADP-SETTL-ALL-TIAA-STD-PCT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaPstl0175.getPstl0175_Adp_Settl_Pct_Dol_Swtch().setValue("D");                                                                                  //Natural: MOVE 'D' TO PSTL0175.ADP-SETTL-PCT-DOL-SWTCH
                        ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Grd_Amt().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Qty().getValue(pnd_I));               //Natural: MOVE ADS-CNTRCT-VIEW.ADC-GRD-MNTHLY-QTY ( #I ) TO PSTL0175.ADP-SETTL-ALL-TIAA-GRD-AMT
                        ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Grd_Flg().setValue("Y");                                                                               //Natural: MOVE 'Y' TO PSTL0175.ADP-SETTL-ALL-TIAA-GRD-FLG
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  ADP-SPCFC-BRKDWN-IND = 'N' - ALWAYS IN ADAS-INTRA (VS ADAS)
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  OS-102013 START
            //*  IF ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-TKR-SYMBL (#I) = 'TIAA#'
            //*      OR = 'TSAF#' OR = 'TSVX#'                      /* EM - 012709  DEA
            if (condition(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_B).equals(pnd_Tiaa_Acct.getValue("*"))))                                  //Natural: IF #ACCT-INVSTMNT-GRPNG-ID ( #B ) = #TIAA-ACCT ( * )
            {
                //*  OS-102013 END
                ldaPstl0175.getPstl0175_Adp_Settl_Pct_Dol_Swtch().setValue("P");                                                                                          //Natural: MOVE 'P' TO PSTL0175.ADP-SETTL-PCT-DOL-SWTCH
                ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Std_Pct().compute(new ComputeParameters(false, ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Std_Pct()),          //Natural: COMPUTE PSTL0175.ADP-SETTL-ALL-TIAA-STD-PCT = 100 - PSTL0175.ADP-SETTL-ALL-TIAA-GRD-PCT
                    DbsField.subtract(100,ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Grd_Pct()));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*   TIAA-REA-PROCESS
    }
    private void sub_Cref_Process() throws Exception                                                                                                                      //Natural: CREF-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Start of CREF-PROCESS ");                                                                                                              //Natural: WRITE 'Start of CREF-PROCESS '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_K.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #K
        //*  MOVE  NECA4000.CFN-FND-NUM-FUND-CDE (#B) TO       /* RS0
        //*  OS-102013 START
        //* *MOVE  #PARM-AREA.#ACCT-NUM-FUND-CDE (#B) TO       /* RS0
        //* *PSTL0175.ADC-CREF-ACCT-CDE(#CUR-DA-CONTRACT-COUNT,#K)
        ldaPstl0175.getPstl0175_Adc_Cref_Acct_Nme().getValue(pnd_Cur_Da_Contract_Count,pnd_K).setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Name_18().getValue(pnd_B));   //Natural: MOVE #PARM-AREA.#ACCT-NAME-18 ( #B ) TO PSTL0175.ADC-CREF-ACCT-NME ( #CUR-DA-CONTRACT-COUNT,#K )
        //*  OS-102013 END
        ldaPstl0175.getPstl0175_Adc_Cref_Acct_Typ().getValue(pnd_Cur_Da_Contract_Count,pnd_K).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Acct_Typ().getValue(pnd_I)); //Natural: MOVE ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-ACCT-TYP ( #I ) TO PSTL0175.ADC-CREF-ACCT-TYP ( #CUR-DA-CONTRACT-COUNT,#K )
        ldaPstl0175.getPstl0175_Adc_Cref_Acct_Qty().getValue(pnd_Cur_Da_Contract_Count,pnd_K).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Acct_Qty().getValue(pnd_I)); //Natural: MOVE ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-ACCT-QTY ( #I ) TO PSTL0175.ADC-CREF-ACCT-QTY ( #CUR-DA-CONTRACT-COUNT,#K )
        //*     CREF-PROCESS
    }
    private void sub_Rtb_Process() throws Exception                                                                                                                       //Natural: RTB-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Start of RTB-PROCESS ");                                                                                                               //Natural: WRITE 'Start of RTB-PROCESS '
            if (Global.isEscape()) return;
            //*   WRITE 'FND-TICKER-SYMBOL    ' NECA4000.CFN-TICKER-SYMBOL    (1:11)
            //*  RS0
            getReports().write(0, "FND-TICKER-SYMBOL    ",pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Ticker().getValue(1,":",13));                                              //Natural: WRITE 'FND-TICKER-SYMBOL    ' #PARM-AREA.#ACCT-TICKER ( 1:13 )
            if (Global.isEscape()) return;
            //*  RS0
            getReports().write(0, "FND-NUM-FUND-CDE ",pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Num_Fund_Cde().getValue(1,":",13));                                            //Natural: WRITE 'FND-NUM-FUND-CDE ' #PARM-AREA.#ACCT-NUM-FUND-CDE ( 1:13 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_P.reset();                                                                                                                                                    //Natural: RESET #P #R #N
        pnd_R.reset();
        pnd_N.reset();
        FOR02:                                                                                                                                                            //Natural: FOR #M 1 14
        for (pnd_M.setValue(1); condition(pnd_M.lessOrEqual(14)); pnd_M.nadd(1))
        {
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Ticker().getValue(pnd_M).equals(" ")))                                                                   //Natural: IF ADS-CNTRCT-VIEW.ADC-RTB-TICKER ( #M ) = ' '
            {
                if (condition(pnd_M.equals(1)))                                                                                                                           //Natural: IF #M = 1
                {
                    getReports().write(0, "RTB fund info is missing although RTB ind is 'Y' ","For Request ",ldaAdsl401a.getAds_Cntrct_View_Rqst_Id(),                    //Natural: WRITE 'RTB fund info is missing although RTB ind is "Y" ' 'For Request ' ADS-CNTRCT-VIEW.RQST-ID ' Contract :' ADS-CNTRCT-VIEW.ADC-TIAA-NBR
                        " Contract :",ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr());
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    EXAMINE  NECA4000.CFN-TICKER-SYMBOL(*)      /* RS0
                //*  RS0
                DbsUtil.examine(new ExamineSource(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Ticker().getValue("*")), new ExamineSearch(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Ticker().getValue(pnd_M)),  //Natural: EXAMINE #PARM-AREA.#ACCT-TICKER ( * ) FOR ADS-CNTRCT-VIEW.ADC-RTB-TICKER ( #M ) INDEX #N
                    new ExamineGivingIndex(pnd_N));
                if (condition(pnd_N.notEquals(getZero())))                                                                                                                //Natural: IF #N NE 0
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, "RTB Ticker does not exist in the table :",ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Ticker().getValue(pnd_M),"For Request ",      //Natural: WRITE 'RTB Ticker does not exist in the table :' ADS-CNTRCT-VIEW.ADC-RTB-TICKER ( #M ) 'For Request ' ADS-CNTRCT-VIEW.RQST-ID ' Contract :' ADS-CNTRCT-VIEW.ADC-TIAA-NBR
                        ldaAdsl401a.getAds_Cntrct_View_Rqst_Id()," Contract :",ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr());
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  OS-102013 START
            //*  IF ADS-CNTRCT-VIEW.ADC-RTB-TICKER (#M) =  'TIAA#' OR = 'TREA#'
            //*      OR = 'TSAF#' OR = 'WA51#' OR = 'TSVX#'   /* EM - 012709     DEA
            if (condition(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_N).equals(pnd_Tiaa_Type_Acct.getValue("*"))))                             //Natural: IF #ACCT-INVSTMNT-GRPNG-ID ( #N ) = #TIAA-TYPE-ACCT ( * )
            {
                //*  OS-102013 END
                //*  EM - 012709
                if (condition(pnd_P.equals(3)))                                                                                                                           //Natural: IF #P = 3
                {
                    pnd_Pindex_Error.setValue(true);                                                                                                                      //Natural: ASSIGN #PINDEX-ERROR := TRUE
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                pdaAdsa555.getTax_Linkage_Ads_Rtb_T_Pymt_Ind().setValue("Y");                                                                                             //Natural: MOVE 'Y' TO TAX-LINKAGE.ADS-RTB-T-PYMT-IND
                if (condition(pnd_Debug_On.getBoolean()))                                                                                                                 //Natural: IF #DEBUG-ON
                {
                    getReports().write(0, "Inside of RTB-PROCESS ");                                                                                                      //Natural: WRITE 'Inside of RTB-PROCESS '
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "TAX-LINKAGE.ADS-RTB-T-PYMT-IND : ",pdaAdsa555.getTax_Linkage_Ads_Rtb_T_Pymt_Ind());                                            //Natural: WRITE 'TAX-LINKAGE.ADS-RTB-T-PYMT-IND : ' TAX-LINKAGE.ADS-RTB-T-PYMT-IND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_P.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #P
                ldaPstl0175.getPstl0175_Adc_Tiaa_Rtb_Nbr().getValue(pnd_Cur_Da_Contract_Count).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr());                   //Natural: MOVE ADS-CNTRCT-VIEW.ADC-TIAA-NBR TO PSTL0175.ADC-TIAA-RTB-NBR ( #CUR-DA-CONTRACT-COUNT )
                //*  OS-102013 START
                //*    MOVE #PARM-AREA.#ACCT-NUM-FUND-CDE (#N)     /* RS0
                //*      TO PSTL0175.ADC-TIAA-RTB-ACCT-CDE (#CUR-DA-CONTRACT-COUNT,#P)
                ldaPstl0175.getPstl0175_Adc_Tiaa_Rtb_Acct_Nme().getValue(pnd_Cur_Da_Contract_Count,pnd_P).setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Name_18().getValue(pnd_N)); //Natural: MOVE #PARM-AREA.#ACCT-NAME-18 ( #N ) TO PSTL0175.ADC-TIAA-RTB-ACCT-NME ( #CUR-DA-CONTRACT-COUNT,#P )
                //*  OS-102013 END
                ldaPstl0175.getPstl0175_Adc_Tiaa_Rtb_Acct_Qty().getValue(pnd_Cur_Da_Contract_Count,pnd_P).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Acct_Qty().getValue(pnd_M)); //Natural: MOVE ADS-CNTRCT-VIEW.ADC-RTB-ACCT-QTY ( #M ) TO PSTL0175.ADC-TIAA-RTB-ACCT-QTY ( #CUR-DA-CONTRACT-COUNT,#P )
                ldaPstl0175.getPstl0175_Adc_Tiaa_Rtb_Acct_Typ().getValue(pnd_Cur_Da_Contract_Count,pnd_P).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Acct_Type().getValue(pnd_M)); //Natural: MOVE ADS-CNTRCT-VIEW.ADC-RTB-ACCT-TYPE ( #M ) TO PSTL0175.ADC-TIAA-RTB-ACCT-TYP ( #CUR-DA-CONTRACT-COUNT,#P )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_R.equals(20)))                                                                                                                          //Natural: IF #R = 20
                {
                    pnd_Rindex_Error.setValue(true);                                                                                                                      //Natural: ASSIGN #RINDEX-ERROR := TRUE
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                pdaAdsa555.getTax_Linkage_Ads_Rtb_C_Pymt_Ind().setValue("Y");                                                                                             //Natural: MOVE 'Y' TO TAX-LINKAGE.ADS-RTB-C-PYMT-IND
                if (condition(pnd_Debug_On.getBoolean()))                                                                                                                 //Natural: IF #DEBUG-ON
                {
                    getReports().write(0, "Inside of RTB-PROCESS ");                                                                                                      //Natural: WRITE 'Inside of RTB-PROCESS '
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "TAX-LINKAGE.ADS-RTB-C-PYMT-IND : ",pdaAdsa555.getTax_Linkage_Ads_Rtb_C_Pymt_Ind());                                            //Natural: WRITE 'TAX-LINKAGE.ADS-RTB-C-PYMT-IND : ' TAX-LINKAGE.ADS-RTB-C-PYMT-IND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_R.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #R
                ldaPstl0175.getPstl0175_Adc_Cref_Rtb_Nbr().getValue(pnd_Cur_Da_Contract_Count).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Cref_Nbr());                   //Natural: MOVE ADS-CNTRCT-VIEW.ADC-CREF-NBR TO PSTL0175.ADC-CREF-RTB-NBR ( #CUR-DA-CONTRACT-COUNT )
                //*  OS-102013 START
                //*    MOVE #PARM-AREA.#ACCT-NUM-FUND-CDE (#N)   /* RS0
                //*      TO PSTL0175.ADC-CREF-RTB-ACCT-CDE (#CUR-DA-CONTRACT-COUNT,#R)
                ldaPstl0175.getPstl0175_Adc_Cref_Rtb_Acct_Nme().getValue(pnd_Cur_Da_Contract_Count,pnd_R).setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Name_18().getValue(pnd_N)); //Natural: MOVE #PARM-AREA.#ACCT-NAME-18 ( #N ) TO PSTL0175.ADC-CREF-RTB-ACCT-NME ( #CUR-DA-CONTRACT-COUNT,#R )
                //*  OS-102013 END
                ldaPstl0175.getPstl0175_Adc_Cref_Rtb_Acct_Qty().getValue(pnd_Cur_Da_Contract_Count,pnd_R).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Acct_Qty().getValue(pnd_M)); //Natural: MOVE ADS-CNTRCT-VIEW.ADC-RTB-ACCT-QTY ( #M ) TO PSTL0175.ADC-CREF-RTB-ACCT-QTY ( #CUR-DA-CONTRACT-COUNT,#R )
                ldaPstl0175.getPstl0175_Adc_Cref_Rtb_Acct_Typ().getValue(pnd_Cur_Da_Contract_Count,pnd_R).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Acct_Type().getValue(pnd_M)); //Natural: MOVE ADS-CNTRCT-VIEW.ADC-RTB-ACCT-TYPE ( #M ) TO PSTL0175.ADC-CREF-RTB-ACCT-TYP ( #CUR-DA-CONTRACT-COUNT,#R )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*     RTB-PROCESS
    }
    private void sub_Call_Beneficiary_Module() throws Exception                                                                                                           //Natural: CALL-BENEFICIARY-MODULE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Start of  CALL-BENEFICIARY-MODULE");                                                                                                   //Natural: WRITE 'Start of  CALL-BENEFICIARY-MODULE'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("IR") || ldaAdsl540a.getAds_Prtcpnt_View_Adp_Grntee_Period().greater(getZero()))))         //Natural: IF ( ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'IR' OR ADS-PRTCPNT-VIEW.ADP-GRNTEE-PERIOD GT 0 )
        {
            //*  12112006  MN
            if (condition(pnd_Debug_On.getBoolean()))                                                                                                                     //Natural: IF #DEBUG-ON
            {
                getReports().write(0, NEWLINE,"*");                                                                                                                       //Natural: WRITE / '*'
                if (Global.isEscape()) return;
                getReports().write(0, "REQ ID : ",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                                                                             //Natural: WRITE 'REQ ID : ' ADS-PRTCPNT-VIEW.RQST-ID
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            //*  CALLNAT 'ADSN555' PSTL0175 ADS-PRTCPNT-VIEW.RQST-ID
            DbsUtil.callnat(Adsn555.class , getCurrentProcessState(), adsn555_Beneficiary, adsn555_Primary, adsn555_Contingent, ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id()); //Natural: CALLNAT 'ADSN555' ADSN555-BENEFICIARY ADSN555-PRIMARY ADSN555-CONTINGENT ADS-PRTCPNT-VIEW.RQST-ID
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "After calling  CALL-BENEFICIARY-MODULE");                                                                                              //Natural: WRITE 'After calling  CALL-BENEFICIARY-MODULE'
            if (Global.isEscape()) return;
            //*  12112006 MN
            getReports().write(0, "Primary : ");                                                                                                                          //Natural: WRITE 'Primary : '
            if (Global.isEscape()) return;
            getReports().write(0, "=",adsn555_Primary_Adsn555_Bene_Name.getValue(1,":",5));                                                                               //Natural: WRITE '=' ADSN555-BENE-NAME ( 1:5 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",adsn555_Primary_Adsn555_Allocation_Pct.getValue(1,":",5));                                                                          //Natural: WRITE '=' ADSN555-ALLOCATION-PCT ( 1:5 )
            if (Global.isEscape()) return;
            getReports().write(0, "Contingent: ");                                                                                                                        //Natural: WRITE 'Contingent: '
            if (Global.isEscape()) return;
            getReports().write(0, "=",adsn555_Contingent_Adsn555_Cont_Name.getValue(1,":",5));                                                                            //Natural: WRITE '=' ADSN555-CONT-NAME ( 1:5 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",adsn555_Contingent_Adsn555_Cont_Alloc_Pct.getValue(1,":",5));                                                                       //Natural: WRITE '=' ADSN555-CONT-ALLOC-PCT ( 1:5 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        ldaPstl0175.getPstl0175_Designation_Text().getValue("*").setValue(adsn555_Beneficiary_Adsn555_Designation_Text.getValue("*"));                                    //Natural: ASSIGN DESIGNATION-TEXT ( * ) := ADSN555-DESIGNATION-TEXT ( * )
        ldaPstl0175.getPstl0175_Designation_Text_Cont().getValue("*").setValue(adsn555_Beneficiary_Adsn555_Designation_Text_Cont.getValue("*"));                          //Natural: ASSIGN DESIGNATION-TEXT-CONT ( * ) := ADSN555-DESIGNATION-TEXT-CONT ( * )
        ldaPstl0175.getPstl0175_Bene_Name().getValue("*").setValue(adsn555_Primary_Adsn555_Bene_Name.getValue("*"));                                                      //Natural: ASSIGN BENE-NAME ( * ) := ADSN555-BENE-NAME ( * )
        ldaPstl0175.getPstl0175_Allocation_Pct().getValue("*").setValue(adsn555_Primary_Adsn555_Allocation_Pct.getValue("*"));                                            //Natural: ASSIGN ALLOCATION-PCT ( * ) := ADSN555-ALLOCATION-PCT ( * )
        ldaPstl0175.getPstl0175_Cont_Name().getValue("*").setValue(adsn555_Contingent_Adsn555_Cont_Name.getValue("*"));                                                   //Natural: ASSIGN CONT-NAME ( * ) := ADSN555-CONT-NAME ( * )
        ldaPstl0175.getPstl0175_Cont_Alloc_Pct().getValue("*").setValue(adsn555_Contingent_Adsn555_Cont_Alloc_Pct.getValue("*"));                                         //Natural: ASSIGN CONT-ALLOC-PCT ( * ) := ADSN555-CONT-ALLOC-PCT ( * )
        //*   CALL-BENEFICIARY-MODULE
    }
    private void sub_Call_Tax_Election_Module() throws Exception                                                                                                          //Natural: CALL-TAX-ELECTION-MODULE
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Start of  CALL-TAX-ELECTION-MODULE");                                                                                                  //Natural: WRITE 'Start of  CALL-TAX-ELECTION-MODULE'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaAdsa555.getTax_Linkage_Ads_Ctzn_Cde().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Frst_Annt_Ctznshp());                                                       //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-FRST-ANNT-CTZNSHP TO ADS-CTZN-CDE
        pdaAdsa555.getTax_Linkage_Ads_Tx_Ss_Num().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Frst_Annt_Ssn());                                                          //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-FRST-ANNT-SSN TO ADS-TX-SS-NUM
        if (condition(pdaAdsa555.getTax_Linkage_Ads_Ctzn_Cde().equals("US") || pdaAdsa555.getTax_Linkage_Ads_Ctzn_Cde().equals("01")))                                    //Natural: IF ADS-CTZN-CDE = 'US' OR = '01'
        {
            pdaAdsa555.getTax_Linkage_Ads_Ctzn_Cde().setValue("01");                                                                                                      //Natural: ASSIGN ADS-CTZN-CDE := '01'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAdsa555.getTax_Linkage_Ads_Ctzn_Cde().setValue("03");                                                                                                      //Natural: ASSIGN ADS-CTZN-CDE := '03'
        }                                                                                                                                                                 //Natural: END-IF
        pdaAdsa555.getTax_Linkage_Ads_Term_Years().reset();                                                                                                               //Natural: RESET TAX-LINKAGE.ADS-TERM-YEARS TAX-LINKAGE.ADS-MONEY-SOURCE
        pdaAdsa555.getTax_Linkage_Ads_Money_Source().reset();
        //*  03072007  MN         START
        if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Sub_Plan_Nbr().getSubstring(1,3).equals("AA1") || ldaAdsl401a.getAds_Cntrct_View_Adc_Sub_Plan_Nbr().getSubstring(1,3).equals("TPA")  //Natural: IF SUBSTRING ( ADC-SUB-PLAN-NBR,1,3 ) = 'AA1' OR = 'TPA' OR = 'IPA' OR = 'AA2' OR = 'AA3' OR = 'TPB' OR = 'IPB'
            || ldaAdsl401a.getAds_Cntrct_View_Adc_Sub_Plan_Nbr().getSubstring(1,3).equals("IPA") || ldaAdsl401a.getAds_Cntrct_View_Adc_Sub_Plan_Nbr().getSubstring(1,3).equals("AA2") 
            || ldaAdsl401a.getAds_Cntrct_View_Adc_Sub_Plan_Nbr().getSubstring(1,3).equals("AA3") || ldaAdsl401a.getAds_Cntrct_View_Adc_Sub_Plan_Nbr().getSubstring(1,3).equals("TPB") 
            || ldaAdsl401a.getAds_Cntrct_View_Adc_Sub_Plan_Nbr().getSubstring(1,3).equals("IPB")))
        {
            pdaAdsa555.getTax_Linkage_Ads_Money_Source().setValue("SR1");                                                                                                 //Natural: ASSIGN TAX-LINKAGE.ADS-MONEY-SOURCE := 'SR1'
        }                                                                                                                                                                 //Natural: END-IF
        //*  03072007  MN         END
        if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("AC")))                                                                                     //Natural: IF ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'AC'
        {
            if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Grntee_Period().less(10)))                                                                                  //Natural: IF ADS-PRTCPNT-VIEW.ADP-GRNTEE-PERIOD < 10
            {
                pdaAdsa555.getTax_Linkage_Ads_Term_Years().setValue("Y");                                                                                                 //Natural: ASSIGN TAX-LINKAGE.ADS-TERM-YEARS := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAdsa555.getTax_Linkage_Ads_Term_Years().setValue("N");                                                                                                 //Natural: ASSIGN TAX-LINKAGE.ADS-TERM-YEARS := 'N'
            }                                                                                                                                                             //Natural: END-IF
            //*  102301 END
        }                                                                                                                                                                 //Natural: END-IF
        pdaAdsa555.getTax_Linkage_Ads_Pymnt_Typ_Ind().setValue("C");                                                                                                      //Natural: ASSIGN TAX-LINKAGE.ADS-PYMNT-TYP-IND := 'C'
        //*  030512 START
        if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Irc_Cde().equals("17") || ldaAdsl540a.getAds_Prtcpnt_View_Adp_Plan_Nbr().equals("TIAA05") ||                    //Natural: IF ADP-IRC-CDE = '17' OR ( ADP-PLAN-NBR = 'TIAA05' OR = 'TIAA06' )
            ldaAdsl540a.getAds_Prtcpnt_View_Adp_Plan_Nbr().equals("TIAA06")))
        {
            pdaAdsa555.getTax_Linkage_Ads_Pymnt_Typ_Ind().setValue("M");                                                                                                  //Natural: ASSIGN TAX-LINKAGE.ADS-PYMNT-TYP-IND := 'M'
            //*  030512 END
        }                                                                                                                                                                 //Natural: END-IF
        //*  020207
        if (condition(ldaPstl0175.getPstl0175_Adc_Da_Tiaa_Nbr().getValue("*").notEquals(" ") && pnd_Tiaa.greater(getZero())))                                             //Natural: IF PSTL0175.ADC-DA-TIAA-NBR ( * ) NE ' ' AND #TIAA GT 0
        {
            pdaAdsa555.getTax_Linkage_Ads_Tx_Tiaa_No().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Ia_Tiaa_Nbr());                                                       //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-IA-TIAA-NBR TO ADS-TX-TIAA-NO
            pdaAdsa555.getTax_Linkage_Ads_Tx_Tiaa_Payee_Cd().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Ia_Tiaa_Payee_Cd());                                            //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-IA-TIAA-PAYEE-CD TO ADS-TX-TIAA-PAYEE-CD
        }                                                                                                                                                                 //Natural: END-IF
        //*  020207
        if (condition(ldaPstl0175.getPstl0175_Adc_Da_Cref_Nbr().getValue("*").notEquals(" ") && pnd_Cref.greater(getZero())))                                             //Natural: IF PSTL0175.ADC-DA-CREF-NBR ( * ) NE ' ' AND #CREF GT 0
        {
            pdaAdsa555.getTax_Linkage_Ads_Tx_Cref_No().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Ia_Cref_Nbr());                                                       //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-IA-CREF-NBR TO ADS-TX-CREF-NO
            pdaAdsa555.getTax_Linkage_Ads_Tx_Cref_Payee_Cd().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Ia_Cref_Payee_Cd());                                            //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-IA-CREF-PAYEE-CD TO ADS-TX-CREF-PAYEE-CD
        }                                                                                                                                                                 //Natural: END-IF
        pdaAdsa555.getTax_Linkage_Ads_Settl_Option().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annty_Optn());                                                          //Natural: ASSIGN ADS-SETTL-OPTION := ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN
        if (condition((ldaAdsl540a.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("AC") && ldaAdsl540a.getAds_Prtcpnt_View_Adp_Grntee_Period().less(10))))                   //Natural: IF ( ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'AC' AND ADS-PRTCPNT-VIEW.ADP-GRNTEE-PERIOD < 10 )
        {
            if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Alt_Dest_Rlvr_Ind().equals("Y")))                                                                           //Natural: IF ADP-ALT-DEST-RLVR-IND = 'Y'
            {
                pdaAdsa555.getTax_Linkage_Ads_Pp_Rollover_Ind().setValue("Y");                                                                                            //Natural: MOVE 'Y' TO ADS-PP-ROLLOVER-IND
                //*  GGG 06/05/2007
                ldaPstl0175.getPstl0175_Adp_Rollvr_Ind().setValue("Y");                                                                                                   //Natural: MOVE 'Y' TO PSTL0175.ADP-ROLLVR-IND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAdsa555.getTax_Linkage_Ads_Pp_Rollover_Ind().setValue("N");                                                                                            //Natural: MOVE 'N' TO ADS-PP-ROLLOVER-IND
                //*  GGG 06/05/2007
                ldaPstl0175.getPstl0175_Adp_Rollvr_Ind().setValue("N");                                                                                                   //Natural: MOVE 'N' TO PSTL0175.ADP-ROLLVR-IND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Rollvr_Ind());                                                                                          //Natural: WRITE '=' PSTL0175.ADP-ROLLVR-IND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaAdsa555.getTax_Linkage_Ads_Mdo_Pymt_Ind().setValue("N");                                                                                                       //Natural: MOVE 'N' TO TAX-LINKAGE.ADS-MDO-PYMT-IND
        pdaAdsa555.getTax_Linkage_Ads_Pp_Pymt_Ind().setValue("Y");                                                                                                        //Natural: MOVE 'Y' TO TAX-LINKAGE.ADS-PP-PYMT-IND
        pdaAdsa555.getTax_Linkage_Ads_Rsdncy_Cde().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Frst_Annt_Rsdnc_Cde());                                                   //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-FRST-ANNT-RSDNC-CDE TO ADS-RSDNCY-CDE
        //* *  02122007 MN START
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Inside of  CALL-TAX-ELECTION-MODULE");                                                                                                 //Natural: WRITE 'Inside of  CALL-TAX-ELECTION-MODULE'
            if (Global.isEscape()) return;
            getReports().write(0, "TAX-LINKAGE.ADS-RTB-T-PYMT-IND: ",pdaAdsa555.getTax_Linkage_Ads_Rtb_T_Pymt_Ind());                                                     //Natural: WRITE 'TAX-LINKAGE.ADS-RTB-T-PYMT-IND: ' TAX-LINKAGE.ADS-RTB-T-PYMT-IND
            if (Global.isEscape()) return;
            getReports().write(0, "TAX-LINKAGE.ADS-RTB-C-PYMT-IND: ",pdaAdsa555.getTax_Linkage_Ads_Rtb_C_Pymt_Ind());                                                     //Natural: WRITE 'TAX-LINKAGE.ADS-RTB-C-PYMT-IND: ' TAX-LINKAGE.ADS-RTB-C-PYMT-IND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        if (condition(ldaPstl0175.getPstl0175_Adp_Rtb_Pymt_Ind().equals("Y")))                                                                                            //Natural: IF PSTL0175.ADP-RTB-PYMT-IND = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM SET-ROLLOVER-INDICATOR
            sub_Set_Rollover_Indicator();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *  02122007 MN END
        //*  EM 031708 START
        if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Roth_Rqst_Ind().equals("Y")))                                                                                   //Natural: IF ADS-PRTCPNT-VIEW.ADP-ROTH-RQST-IND = 'Y'
        {
            pdaAdsa555.getTax_Linkage_Ads_Money_Source().setValue("ROTHP");                                                                                               //Natural: MOVE 'ROTHP' TO TAX-LINKAGE.ADS-MONEY-SOURCE
            pdaAdsa555.getTax_Linkage_Ads_Lob().reset();                                                                                                                  //Natural: RESET TAX-LINKAGE.ADS-LOB
            if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Roth_Dsblty_Dte().notEquals(getZero())))                                                                    //Natural: IF ADS-PRTCPNT-VIEW.ADP-ROTH-DSBLTY-DTE NE 0
            {
                pdaAdsa555.getTax_Linkage_Ads_Hardship_Ind().setValue("D");                                                                                               //Natural: MOVE 'D' TO TAX-LINKAGE.ADS-HARDSHIP-IND
            }                                                                                                                                                             //Natural: END-IF
            pnd_Dte.setValueEdited(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Frst_Annt_Dte_Of_Brth(),new ReportEditMask("YYYYMMDD"));                                           //Natural: MOVE EDITED ADS-PRTCPNT-VIEW.ADP-FRST-ANNT-DTE-OF-BRTH ( EM = YYYYMMDD ) TO #DTE
            pdaAdsa555.getTax_Linkage_Ads_Dob().setValue(pnd_Dte_Pnd_Dte_N);                                                                                              //Natural: MOVE #DTE-N TO TAX-LINKAGE.ADS-DOB
            pnd_Dte.setValueEdited(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Roth_Frst_Cntrbtn_Dte(),new ReportEditMask("YYYYMMDD"));                                           //Natural: MOVE EDITED ADS-PRTCPNT-VIEW.ADP-ROTH-FRST-CNTRBTN-DTE ( EM = YYYYMMDD ) TO #DTE
            pdaAdsa555.getTax_Linkage_Ads_Roth_Frst_Cntrbtn_Dte().setValue(pnd_Dte_Pnd_Dte_N);                                                                            //Natural: MOVE #DTE-N TO TAX-LINKAGE.ADS-ROTH-FRST-CNTRBTN-DTE
            //*  EM 031708 END
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Tax_Errors_Info.reset();                                                                                                                                      //Natural: RESET #TAX-ERRORS-INFO
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(1, "before calling ADSN545",NEWLINE,"-",new RepeatItem(79));                                                                               //Natural: WRITE ( 1 ) 'before calling ADSN545'/'-' ( 79 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-PASSED-TAX-DATA
            sub_Display_Passed_Tax_Data();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Adsn545.class , getCurrentProcessState(), pdaAdsa555.getTax_Linkage(), ldaPstl0175.getPstl0175_Tax(), pnd_Tax_Errors_Info);                       //Natural: CALLNAT 'ADSN545' TAX-LINKAGE TAX #TAX-ERRORS-INFO
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(1, "-",new RepeatItem(79),NEWLINE,"after calling ADSN545",NEWLINE);                                                                        //Natural: WRITE ( 1 ) '-' ( 79 ) /'after calling ADSN545'/
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-PASSED-TAX-DATA
            sub_Display_Passed_Tax_Data();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Tax_Errors_Info_Pnd_Tax_Error_Code.equals(" ") || pnd_Tax_Errors_Info_Pnd_Tax_Error_Code.equals("0000")))                                       //Natural: IF #TAX-ERROR-CODE = ' ' OR = '0000'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "Bad return code from TAX module for REQ id ",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                                               //Natural: WRITE 'Bad return code from TAX module for REQ id ' ADS-PRTCPNT-VIEW.RQST-ID
            if (Global.isEscape()) return;
            getReports().write(0, "=",pnd_Tax_Errors_Info_Pnd_Tax_Error_Code);                                                                                            //Natural: WRITE '=' #TAX-ERROR-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "=",pnd_Tax_Errors_Info_Pnd_Tax_Error_Desc);                                                                                            //Natural: WRITE '=' #TAX-ERROR-DESC
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "After calling CALL-TAX-ELECTION-MODULE");                                                                                              //Natural: WRITE 'After calling CALL-TAX-ELECTION-MODULE'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*    CALL-TAX-ELECTION-MODULE
    }
    private void sub_Set_Rollover_Indicator() throws Exception                                                                                                            //Natural: SET-ROLLOVER-INDICATOR
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Beginning of SET-ROLLOVER-INDICATOR for RQST-ID :",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                                         //Natural: WRITE 'Beginning of SET-ROLLOVER-INDICATOR for RQST-ID :' ADS-PRTCPNT-VIEW.RQST-ID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* * EXAMINE FULL ADP-RTB-ALT-DEST-RLVR-IND (*) FOR FULL 'N'
        //* *  GIVING NUMBER IN #NUMBER
        //* *
        //* * IF #NUMBER = 3
        if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Rlvr_Ind().equals("N")))                                                                           //Natural: IF ADS-PRTCPNT-VIEW.ADP-RTB-ALT-DEST-RLVR-IND = 'N'
        {
            pdaAdsa555.getTax_Linkage_Ads_Rollover_Ind().setValue("N");                                                                                                   //Natural: MOVE 'N' TO TAX-LINKAGE.ADS-ROLLOVER-IND
            //*    PSTL0175.ADP-ROLLVR-IND                           /* GGG 06/05/2007
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        pdaAdsa555.getTax_Linkage_Ads_Rollover_Ind().setValue("Y");                                                                                                       //Natural: MOVE 'Y' TO TAX-LINKAGE.ADS-ROLLOVER-IND
        //*  PSTL0175.ADP-ROLLVR-IND                             /* GGG 06/05/2007
        //* * IF (ADP-RTB-ALT-DEST-PCT  > 0 OR  ?????????? - NOT ON OUR PART FILE
        //* *  ADP-RTB-ALT-DEST-AMT  > 0)
        //* *  IF ADP-RTB-ALT-DEST-RLVR-IND  NE 'Y'
        //* *     MOVE 'N' TO TAX-LINKAGE.ADS-ROLLOVER-IND
        //* *                  PSTL0175.ADS-ROLLVR-IND
        //* *     ESCAPE ROUTINE
        //* *   END-IF
        //* * END-IF
        //* * IF (ADP-RTB-ALT-DEST-PCT (2) > 0 OR
        //* *    ADP-RTB-ALT-DEST-AMT (2) > 0)
        //* *  IF ADP-RTB-ALT-DEST-RLVR-IND (2) NE 'Y'
        //* *    MOVE 'N' TO TAX-LINKAGE.ADS-ROLLOVER-IND
        //* *      NAZ-ROLLVR-IND
        //* *    ESCAPE ROUTINE
        //* *  END-IF
        //* * END-IF
        //* *
        //* * IF (ADP-RTB-ALT-DEST-PCT (3) > 0 OR
        //* *    ADP-RTB-ALT-DEST-AMT (3) > 0)
        //* *  MOVE 'N' TO TAX-LINKAGE.ADS-ROLLOVER-IND
        //* *    NAZ-ROLLVR-IND
        //* * END-IF
        //* *
        //*    SET-ROLLOVER-INDICATOR
    }
    private void sub_Call_Post_For_Open() throws Exception                                                                                                                //Natural: CALL-POST-FOR-OPEN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Start of  CALL-POST-FOR-OPEN for: ",ldaPstl0175.getPstl0175_Adp_Frst_Annt_Ssn());                                                      //Natural: WRITE 'Start of  CALL-POST-FOR-OPEN for: ' PSTL0175.ADP-FRST-ANNT-SSN
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  RESET PSTA9611                                  /* EM 083016 - START
        pdaPsta9612.getPsta9612().reset();                                                                                                                                //Natural: RESET PSTA9612
        pdaPsta9612.getPsta9612_Systm_Id_Cde().setValue("BATCHADS");                                                                                                      //Natural: MOVE 'BATCHADS' TO PSTA9612.SYSTM-ID-CDE
        pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PTRKZACK");                                                                                                         //Natural: MOVE 'PTRKZACK' TO PSTA9612.PCKGE-CDE
        pdaPsta9612.getPsta9612_Rqst_Origin_Unit_Cde().setValue(" ");                                                                                                     //Natural: MOVE ' ' TO PSTA9612.RQST-ORIGIN-UNIT-CDE
        pdaPsta9612.getPsta9612_Form_Lbrry_Id().setValue(" ");                                                                                                            //Natural: MOVE ' ' TO PSTA9612.FORM-LBRRY-ID
        pdaPsta9612.getPsta9612_Print_Fclty_Cde().setValue("F");                                                                                                          //Natural: MOVE 'F' TO PSTA9612.PRINT-FCLTY-CDE
        //*  MOVE #PRINTER-ID         TO PSTA9612.PRNTR-ID-CDE
        pdaPsta9612.getPsta9612_Wpid_Vldte_Ind().getValue(1).setValue("Y");                                                                                               //Natural: MOVE 'Y' TO PSTA9612.WPID-VLDTE-IND ( 1 )
        //*  MOVE ADP-UNIQUE-ID       TO PSTA9611.PIN-NBR
        pdaPsta9612.getPsta9612_Univ_Id().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Unique_Id());                                                                      //Natural: MOVE ADP-UNIQUE-ID TO PSTA9612.UNIV-ID
        pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue("P");                                                                                                              //Natural: MOVE 'P' TO PSTA9612.UNIV-ID-TYP
        pdaPsta9612.getPsta9612_Letter_Dte().setValue(Global.getDATX());                                                                                                  //Natural: MOVE *DATX TO PSTA9612.LETTER-DTE
        pdaPsta9612.getPsta9612_Addrss_Typ_Cde().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Typ_Cd());                                             //Natural: MOVE ADP-CRRSPNDNCE-PERM-ADDR-TYP-CD TO PSTA9612.ADDRSS-TYP-CDE
        pdaPsta9612.getPsta9612_Tiaa_Cntrct_Nbr().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr());                                                                //Natural: MOVE ADS-CNTRCT-VIEW.ADC-TIAA-NBR TO PSTA9612.TIAA-CNTRCT-NBR
        pdaPsta9612.getPsta9612_Full_Nme().setValue(ldaPstl0175.getPstl0175_Participant_Name());                                                                          //Natural: MOVE PARTICIPANT-NAME TO PSTA9612.FULL-NME
        pdaPsta9612.getPsta9612_Pckge_Dlvry_Typ().setValue("   ");                                                                                                        //Natural: MOVE '   ' TO PSTA9612.PCKGE-DLVRY-TYP
        pdaPsta9612.getPsta9612_Rqst_Log_Dte_Tme().getValue(1).setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Mit_Log_Dte_Tme());                                           //Natural: MOVE ADP-MIT-LOG-DTE-TME TO PSTA9612.RQST-LOG-DTE-TME ( 1 )
        pdaPsta9612.getPsta9612_Status_Cde().getValue(1).setValue("    ");                                                                                                //Natural: MOVE '    ' TO PSTA9612.STATUS-CDE ( 1 )
        pdaPsta9612.getPsta9612_Effctve_Dte().getValue(1).setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Effctv_Dte());                                                     //Natural: MOVE ADP-EFFCTV-DTE TO PSTA9612.EFFCTVE-DTE ( 1 )
        //*  CHG 9/97
        pdaPsta9612.getPsta9612_No_Updte_To_Mit_Ind().setValue(true);                                                                                                     //Natural: MOVE TRUE TO NO-UPDTE-TO-MIT-IND
        //*  IF ADP-ADDR-CHG-RPT-IND NOT = ' ' /* NOT ON ADS-PART REC
        pdaPsta9612.getPsta9612_Addrss_Line_1().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt().getValue(1));                                     //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( 1 ) TO PSTA9612.ADDRSS-LINE-1
        pdaPsta9612.getPsta9612_Addrss_Line_2().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt().getValue(2));                                     //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( 2 ) TO PSTA9612.ADDRSS-LINE-2
        pdaPsta9612.getPsta9612_Addrss_Line_3().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt().getValue(3));                                     //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( 3 ) TO PSTA9612.ADDRSS-LINE-3
        pdaPsta9612.getPsta9612_Addrss_Line_4().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt().getValue(4));                                     //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( 4 ) TO PSTA9612.ADDRSS-LINE-4
        pdaPsta9612.getPsta9612_Addrss_Line_5().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt().getValue(5));                                     //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( 5 ) TO PSTA9612.ADDRSS-LINE-5
        pdaPsta9612.getPsta9612_Addrss_Zp9_Nbr().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Zip());                                                //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-ZIP TO PSTA9612.ADDRSS-ZP9-NBR
        //*  END-IF
        pdaPsta9610.getPsta9610_Pst_Rqst_Id().reset();                                                                                                                    //Natural: RESET PSTA9610.PST-RQST-ID
        //*  CALLNAT 'PSTN9610'
        //*   PSTA9610
        //*   PSTA9611
        //*   MSG-INFO-SUB
        //*   PARM-EMPLOYEE-INFO-SUB
        //*   PARM-UNIT-INFO-SUB
        DbsUtil.callnat(Pstn9612.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9612' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        //*                                     /* EM - 083016 END
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
            pnd_Return_Code.setValue("E");                                                                                                                                //Natural: MOVE 'E' TO #RETURN-CODE
            //*  MOVE ##MSG TO #MSG
        }                                                                                                                                                                 //Natural: END-IF
        //*    CALL-POST-FOR-OPEN
    }
    private void sub_Call_Post_For_Write() throws Exception                                                                                                               //Natural: CALL-POST-FOR-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Start of  CALL-POST-FOR-WRITE for: ",ldaPstl0175.getPstl0175_Adp_Frst_Annt_Ssn());                                                     //Natural: WRITE 'Start of  CALL-POST-FOR-WRITE for: ' PSTL0175.ADP-FRST-ANNT-SSN
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaPsta9500.getPsta9500_Data_Typ().setValue("M");                                                                                                                 //Natural: MOVE 'M' TO DATA-TYP
        pnd_Appl_Key_Counter.setValue(10000);                                                                                                                             //Natural: MOVE 10000 TO #APPL-KEY-COUNTER
        pdaPsta9500.getPsta9500_Mail_Id().setValue(pdaPsta9610.getPsta9610_Pst_Rqst_Id());                                                                                //Natural: MOVE PSTA9610.PST-RQST-ID TO MAIL-ID
        ldaPstl0175.getPstl0175_Pstl0175_First_Record().setValue(true);                                                                                                   //Natural: MOVE TRUE TO PSTL0175-FIRST-RECORD
        pdaPsta9500.getPsta9500_Application_Key().reset();                                                                                                                //Natural: RESET PSTA9500.APPLICATION-KEY
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "###  PSTL0175 data before calling POST for write ####");                                                                               //Natural: WRITE '###  PSTL0175 data before calling POST for write ####'
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Plan_Name());                                                                                               //Natural: WRITE '=' PSTL0175.PLAN-NAME
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Participant_Name());                                                                                        //Natural: WRITE '=' PSTL0175.PARTICIPANT-NAME
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Frst_Annt_Ssn());                                                                                       //Natural: WRITE '=' PSTL0175.ADP-FRST-ANNT-SSN
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Frst_Annt_Dte_Of_Brth_N());                                                                             //Natural: WRITE '=' PSTL0175.ADP-FRST-ANNT-DTE-OF-BRTH-N
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Crrspndnce_Perm_Addr_Txt().getValue("*"));                                                              //Natural: WRITE '=' PSTL0175.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( * )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Crrspndnce_Perm_Addr_Zip());                                                                            //Natural: WRITE '=' PSTL0175.ADP-CRRSPNDNCE-PERM-ADDR-ZIP
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Frst_Annt_Rsdnc_Cde());                                                                                 //Natural: WRITE '=' PSTL0175.ADP-FRST-ANNT-RSDNC-CDE
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Frst_Annt_Ctznshp());                                                                                   //Natural: WRITE '=' PSTL0175.ADP-FRST-ANNT-CTZNSHP
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Annty_Optn());                                                                                          //Natural: WRITE '=' PSTL0175.ADP-ANNTY-OPTN
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Grntee_Period());                                                                                       //Natural: WRITE '=' PSTL0175.ADP-GRNTEE-PERIOD
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Annty_Strt_Dte_N());                                                                                    //Natural: WRITE '=' PSTL0175.ADP-ANNTY-STRT-DTE-N
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Pymnt_Mode());                                                                                          //Natural: WRITE '=' PSTL0175.ADP-PYMNT-MODE
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Settl_Pct_Dol_Swtch());                                                                                 //Natural: WRITE '=' PSTL0175.ADP-SETTL-PCT-DOL-SWTCH
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Grd_Pct());                                                                              //Natural: WRITE '=' PSTL0175.ADP-SETTL-ALL-TIAA-GRD-PCT
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Std_Pct());                                                                              //Natural: WRITE '=' PSTL0175.ADP-SETTL-ALL-TIAA-STD-PCT
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Grd_Amt());                                                                              //Natural: WRITE '=' PSTL0175.ADP-SETTL-ALL-TIAA-GRD-AMT
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Grd_Rem());                                                                              //Natural: WRITE '=' PSTL0175.ADP-SETTL-ALL-TIAA-GRD-REM
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Settl_All_Tiaa_Grd_Flg());                                                                              //Natural: WRITE '=' PSTL0175.ADP-SETTL-ALL-TIAA-GRD-FLG
            if (Global.isEscape()) return;
            getReports().write(0, "RTB alt destination info: ");                                                                                                          //Natural: WRITE 'RTB alt destination info: '
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Rlvr_Ind().getValue(1));                                                                   //Natural: WRITE '=' PSTL0175.ADP-RTB-ALT-DEST-RLVR-IND ( 1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Carr_Nme().getValue(1));                                                                   //Natural: WRITE '=' PSTL0175.ADP-RTB-ALT-DEST-CARR-NME ( 1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Addr().getValue(1,1));                                                                     //Natural: WRITE '=' PSTL0175.ADP-RTB-ALT-DEST-ADDR ( 1,1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Addr().getValue(1,2));                                                                     //Natural: WRITE '=' PSTL0175.ADP-RTB-ALT-DEST-ADDR ( 1,2 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Zip().getValue(1));                                                                        //Natural: WRITE '=' PSTL0175.ADP-RTB-ALT-DEST-ZIP ( 1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Acct_Nbr().getValue(1));                                                                   //Natural: WRITE '=' PSTL0175.ADP-RTB-ALT-DEST-ACCT-NBR ( 1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Pct().getValue(1));                                                                        //Natural: WRITE '=' PSTL0175.ADP-RTB-ALT-DEST-PCT ( 1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Amt().getValue(1));                                                                        //Natural: WRITE '=' PSTL0175.ADP-RTB-ALT-DEST-AMT ( 1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Acct_Typ().getValue(1));                                                                   //Natural: WRITE '=' PSTL0175.ADP-RTB-ALT-DEST-ACCT-TYP ( 1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Trnst_Cde().getValue(1));                                                                  //Natural: WRITE '=' PSTL0175.ADP-RTB-ALT-DEST-TRNST-CDE ( 1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Rtb_Alt_Dest_Ira_Typ().getValue(1));                                                                    //Natural: WRITE '=' PSTL0175.ADP-RTB-ALT-DEST-IRA-TYP ( 1 )
            if (Global.isEscape()) return;
            getReports().write(0, " contract info");                                                                                                                      //Natural: WRITE ' contract info'
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Da_Tiaa_Nbr().getValue("*"));                                                                           //Natural: WRITE '=' PSTL0175.ADC-DA-TIAA-NBR ( * )
            if (Global.isEscape()) return;
            //*  WRITE '='  PSTL0175.ADC-TIAA-ACCT-CDE (*,1)
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Nme().getValue("*",1));                                                                       //Natural: WRITE '=' PSTL0175.ADC-TIAA-ACCT-NME ( *,1 )
            if (Global.isEscape()) return;
            //*  WRITE '='  PSTL0175.ADC-TIAA-ACCT-CDE (*,2)
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Nme().getValue("*",2));                                                                       //Natural: WRITE '=' PSTL0175.ADC-TIAA-ACCT-NME ( *,2 )
            if (Global.isEscape()) return;
            //*  WRITE '='  PSTL0175.ADC-TIAA-ACCT-CDE (*,3)
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Nme().getValue("*",3));                                                                       //Natural: WRITE '=' PSTL0175.ADC-TIAA-ACCT-NME ( *,3 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Typ().getValue("*",1));                                                                       //Natural: WRITE '=' PSTL0175.ADC-TIAA-ACCT-TYP ( *,1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Typ().getValue("*",2));                                                                       //Natural: WRITE '=' PSTL0175.ADC-TIAA-ACCT-TYP ( *,2 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Typ().getValue("*",3));                                                                       //Natural: WRITE '=' PSTL0175.ADC-TIAA-ACCT-TYP ( *,3 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Qty().getValue("*",1));                                                                       //Natural: WRITE '=' PSTL0175.ADC-TIAA-ACCT-QTY ( *,1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Qty().getValue("*",2));                                                                       //Natural: WRITE '=' PSTL0175.ADC-TIAA-ACCT-QTY ( *,2 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Qty().getValue("*",3));                                                                       //Natural: WRITE '=' PSTL0175.ADC-TIAA-ACCT-QTY ( *,3 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Payout().getValue("*",1));                                                                    //Natural: WRITE '=' PSTL0175.ADC-TIAA-ACCT-PAYOUT ( *,1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Payout().getValue("*",2));                                                                    //Natural: WRITE '=' PSTL0175.ADC-TIAA-ACCT-PAYOUT ( *,2 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Tiaa_Acct_Payout().getValue("*",3));                                                                    //Natural: WRITE '=' PSTL0175.ADC-TIAA-ACCT-PAYOUT ( *,3 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Da_Cref_Nbr().getValue("*"));                                                                           //Natural: WRITE '=' PSTL0175.ADC-DA-CREF-NBR ( * )
            if (Global.isEscape()) return;
            //*  WRITE '='  PSTL0175.ADC-CREF-ACCT-CDE (*,1)
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Cref_Acct_Nme().getValue("*",1));                                                                       //Natural: WRITE '=' PSTL0175.ADC-CREF-ACCT-NME ( *,1 )
            if (Global.isEscape()) return;
            //*  WRITE '='  PSTL0175.ADC-CREF-ACCT-CDE (*,2)
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Cref_Acct_Nme().getValue("*",2));                                                                       //Natural: WRITE '=' PSTL0175.ADC-CREF-ACCT-NME ( *,2 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Cref_Acct_Typ().getValue("*",1));                                                                       //Natural: WRITE '=' PSTL0175.ADC-CREF-ACCT-TYP ( *,1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Cref_Acct_Typ().getValue("*",2));                                                                       //Natural: WRITE '=' PSTL0175.ADC-CREF-ACCT-TYP ( *,2 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Cref_Acct_Qty().getValue("*",1));                                                                       //Natural: WRITE '=' PSTL0175.ADC-CREF-ACCT-QTY ( *,1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Cref_Acct_Qty().getValue("*",2));                                                                       //Natural: WRITE '=' PSTL0175.ADC-CREF-ACCT-QTY ( *,2 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Cref_Grd_Mnthly_Typ().getValue("*",1));                                                                 //Natural: WRITE '=' PSTL0175.ADC-CREF-GRD-MNTHLY-TYP ( *,1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Cref_Grd_Mnthly_Typ().getValue("*",2));                                                                 //Natural: WRITE '=' PSTL0175.ADC-CREF-GRD-MNTHLY-TYP ( *,2 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Cref_Grd_Mnthly_Qty().getValue("*",1));                                                                 //Natural: WRITE '=' PSTL0175.ADC-CREF-GRD-MNTHLY-QTY ( *,1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Cref_Grd_Mnthly_Qty().getValue("*",2));                                                                 //Natural: WRITE '=' PSTL0175.ADC-CREF-GRD-MNTHLY-QTY ( *,2 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Cref_Acct_Rem_Qty().getValue("*",1));                                                                   //Natural: WRITE '=' PSTL0175.ADC-CREF-ACCT-REM-QTY ( *,1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Cref_Acct_Rem_Qty().getValue("*",2));                                                                   //Natural: WRITE '=' PSTL0175.ADC-CREF-ACCT-REM-QTY ( *,2 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Cref_Acct_Payout().getValue("*",1));                                                                    //Natural: WRITE '=' PSTL0175.ADC-CREF-ACCT-PAYOUT ( *,1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Cref_Acct_Payout().getValue("*",2));                                                                    //Natural: WRITE '=' PSTL0175.ADC-CREF-ACCT-PAYOUT ( *,2 )
            if (Global.isEscape()) return;
            getReports().write(0, "RTB financial info: ");                                                                                                                //Natural: WRITE 'RTB financial info: '
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Rtb_Pymt_Ind());                                                                                        //Natural: WRITE '=' PSTL0175.ADP-RTB-PYMT-IND
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Tiaa_Rtb_Nbr().getValue(1));                                                                            //Natural: WRITE '=' PSTL0175.ADC-TIAA-RTB-NBR ( 1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Tiaa_Rtb_Acct_Typ().getValue(1,"*"));                                                                   //Natural: WRITE '=' PSTL0175.ADC-TIAA-RTB-ACCT-TYP ( 1,* )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Tiaa_Rtb_Acct_Qty().getValue(1,"*"));                                                                   //Natural: WRITE '=' PSTL0175.ADC-TIAA-RTB-ACCT-QTY ( 1,* )
            if (Global.isEscape()) return;
            //*  WRITE '='  PSTL0175.ADC-TIAA-RTB-ACCT-CDE   (1,*)
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Tiaa_Rtb_Acct_Nme().getValue(1,"*"));                                                                   //Natural: WRITE '=' PSTL0175.ADC-TIAA-RTB-ACCT-NME ( 1,* )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Cref_Rtb_Nbr().getValue(1));                                                                            //Natural: WRITE '=' PSTL0175.ADC-CREF-RTB-NBR ( 1 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Cref_Rtb_Acct_Typ().getValue(1,"*"));                                                                   //Natural: WRITE '=' PSTL0175.ADC-CREF-RTB-ACCT-TYP ( 1,* )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Cref_Rtb_Acct_Qty().getValue(1,"*"));                                                                   //Natural: WRITE '=' PSTL0175.ADC-CREF-RTB-ACCT-QTY ( 1,* )
            if (Global.isEscape()) return;
            //*  WRITE '='  PSTL0175.ADC-CREF-RTB-ACCT-CDE   (1,*)
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adc_Cref_Rtb_Acct_Nme().getValue(1,"*"));                                                                   //Natural: WRITE '=' PSTL0175.ADC-CREF-RTB-ACCT-NME ( 1,* )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Adp_Rollvr_Ind());                                                                                          //Natural: WRITE '=' PSTL0175.ADP-ROLLVR-IND
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Stable_Return_Ind());                                                                                       //Natural: WRITE '=' PSTL0175.STABLE-RETURN-IND
            if (Global.isEscape()) return;
            //*  12112006 MN
            getReports().write(0, "Primary BENE info ");                                                                                                                  //Natural: WRITE 'Primary BENE info '
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Bene_Name().getValue(1,":",5));                                                                             //Natural: WRITE '=' PSTL0175.BENE-NAME ( 1:5 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Allocation_Pct().getValue(1,":",5));                                                                        //Natural: WRITE '=' PSTL0175.ALLOCATION-PCT ( 1:5 )
            if (Global.isEscape()) return;
            //*  12112006 MN
            getReports().write(0, "Contingent BENE info ");                                                                                                               //Natural: WRITE 'Contingent BENE info '
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Cont_Name().getValue(1,":",5));                                                                             //Natural: WRITE '=' PSTL0175.CONT-NAME ( 1:5 )
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Cont_Alloc_Pct().getValue(1,":",5));                                                                        //Natural: WRITE '=' PSTL0175.CONT-ALLOC-PCT ( 1:5 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Pstn9502.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9502' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY PSTL0175
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            sort_Key, ldaPstl0175.getPstl0175());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
            pnd_Return_Code.setValue("E");                                                                                                                                //Natural: MOVE 'E' TO #RETURN-CODE
            //*  MOVE ##MSG TO #MSG
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT
        }                                                                                                                                                                 //Natural: END-IF
        //*   CALL-POST-FOR-WRITE
    }
    private void sub_Initialize_Pi_Data_Fields() throws Exception                                                                                                         //Natural: INITIALIZE-PI-DATA-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Start of  INITIALIZE-PI-DATA-FIELDS for: ",ldaPstl0175.getPstl0175_Adp_Frst_Annt_Ssn());                                               //Natural: WRITE 'Start of  INITIALIZE-PI-DATA-FIELDS for: ' PSTL0175.ADP-FRST-ANNT-SSN
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        ldaPstl0175.getPstl0175_Pi_Data_Pstl0175_Pi_Data_Array().getValue("*").reset();                                                                                   //Natural: RESET PSTL0175-PI-DATA-ARRAY ( * )
        ldaPstl0175.getPstl0175_Pi_Data_Pstl0175_Pi_Rec_Id().setValue("PI");                                                                                              //Natural: ASSIGN PSTL0175-PI-REC-ID := 'PI'
        ldaPstl0175.getPstl0175_Pi_Data_Pi_Task_Type().setValue("SETPAY");                                                                                                //Natural: ASSIGN PI-TASK-TYPE := 'SETPAY'
        ldaPstl0175.getPstl0175_Pi_Data_Pi_Pin_Npin_Ppg().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Unique_Id());                                                      //Natural: ASSIGN PI-PIN-NPIN-PPG := ADP-UNIQUE-ID
        ldaPstl0175.getPstl0175_Pi_Data_Pi_Pin_Type().setValue("P");                                                                                                      //Natural: ASSIGN PI-PIN-TYPE := 'P'
        ldaPstl0175.getPstl0175_Pi_Data_Pi_Action_Step().setValue("None");                                                                                                //Natural: ASSIGN PI-ACTION-STEP := 'None'
        ldaPstl0175.getPstl0175_Pi_Data_Pi_Doc_Content().setValue("POST LETTER");                                                                                         //Natural: ASSIGN PI-DOC-CONTENT := 'POST LETTER'
        if (condition(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Pi_Task_Id().equals(" ")))                                                                                      //Natural: IF ADP-PI-TASK-ID = ' '
        {
            ldaPstl0175.getPstl0175_Pi_Data_Pi_Export_Ind().setValue("T");                                                                                                //Natural: ASSIGN PI-EXPORT-IND := 'T'
            ldaPstl0175.getPstl0175_Pi_Data_Pi_Task_Status().setValue("C");                                                                                               //Natural: ASSIGN PI-TASK-STATUS := 'C'
            //*  FOUND AN EXISTING TASK
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaPstl0175.getPstl0175_Pi_Data_Pi_Task_Id().setValue(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Pi_Task_Id());                                                      //Natural: MOVE ADP-PI-TASK-ID TO PI-TASK-ID
            ldaPstl0175.getPstl0175_Pi_Data_Pi_Export_Ind().setValue("I");                                                                                                //Natural: ASSIGN PI-EXPORT-IND := 'I'
        }                                                                                                                                                                 //Natural: END-IF
        //*  INITIALIZE-PI-DATA-FIELDS
    }
    private void sub_Store_Pi_Record() throws Exception                                                                                                                   //Natural: STORE-PI-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Start of  STORE-PI-RECORD for: ",ldaPstl0175.getPstl0175_Adp_Frst_Annt_Ssn());                                                         //Natural: WRITE 'Start of  STORE-PI-RECORD for: ' PSTL0175.ADP-FRST-ANNT-SSN
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Pi_Data_Pstl0175_Pi_Rec_Id());                                                                              //Natural: WRITE '=' PSTL0175-PI-REC-ID
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Pi_Data_Pi_Task_Type());                                                                                    //Natural: WRITE '=' PI-TASK-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Pi_Data_Pi_Pin_Npin_Ppg());                                                                                 //Natural: WRITE '=' PI-PIN-NPIN-PPG
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Pi_Data_Pi_Pin_Type());                                                                                     //Natural: WRITE '=' PI-PIN-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Pi_Data_Pi_Action_Step());                                                                                  //Natural: WRITE '=' PI-ACTION-STEP
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Pi_Data_Pi_Doc_Content());                                                                                  //Natural: WRITE '=' PI-DOC-CONTENT
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Pi_Data_Pi_Export_Ind());                                                                                   //Natural: WRITE '=' PI-EXPORT-IND
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Pi_Data_Pi_Task_Status());                                                                                  //Natural: WRITE '=' PI-TASK-STATUS
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Pi_Data_Pi_Task_Id());                                                                                      //Natural: WRITE '=' PI-TASK-ID
            if (Global.isEscape()) return;
            getReports().write(0, "=",ldaPstl0175.getPstl0175_Pi_Data_Pi_Export_Ind());                                                                                   //Natural: WRITE '=' PI-EXPORT-IND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY PSTL0175-PI-DATA
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            sort_Key, ldaPstl0175.getPstl0175_Pi_Data());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
            pnd_Return_Code.setValue("E");                                                                                                                                //Natural: MOVE 'E' TO #RETURN-CODE
            //*  MOVE ##MSG TO #MSG
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT
        }                                                                                                                                                                 //Natural: END-IF
        //*  STORE-PI-RECORD
    }
    private void sub_Call_Post_For_Close() throws Exception                                                                                                               //Natural: CALL-POST-FOR-CLOSE
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Start of  CALL-POST-FOR-CLOSE for: ",ldaPstl0175.getPstl0175_Adp_Frst_Annt_Ssn());                                                     //Natural: WRITE 'Start of  CALL-POST-FOR-CLOSE for: ' PSTL0175.ADP-FRST-ANNT-SSN
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALLNAT 'PSTN9680'                         /* EM - 083016 START
        //*   PSTA9610
        //*   PSTA9611
        //*   MSG-INFO-SUB
        //*   PARM-EMPLOYEE-INFO-SUB
        //*   PARM-UNIT-INFO-SUB
        //*  EM - 083016 END
        DbsUtil.callnat(Pstn9685.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9685' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
            pnd_Return_Code.setValue("E");                                                                                                                                //Natural: MOVE 'E' TO #RETURN-CODE
            //*  MOVE ##MSG TO #MSG
        }                                                                                                                                                                 //Natural: END-IF
        //*   CALL-POST-FOR-CLOSE
    }
    private void sub_Update_Part_Record() throws Exception                                                                                                                //Natural: UPDATE-PART-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, "Start of  UPDATE-PART-RECORD for ISN: ",pnd_Adp_Isn);                                                                                  //Natural: WRITE 'Start of  UPDATE-PART-RECORD for ISN: ' #ADP-ISN
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        GET01:                                                                                                                                                            //Natural: GET ADS-PRTCPNT-VIEW #ADP-ISN
        ldaAdsl540a.getVw_ads_Prtcpnt_View().readByID(pnd_Adp_Isn.getLong(), "GET01");
        ldaAdsl540a.getAds_Prtcpnt_View_Adp_Ackl_Lttr_Ind().setValue("Y");                                                                                                //Natural: ASSIGN ADP-ACKL-LTTR-IND := 'Y'
        ldaAdsl540a.getVw_ads_Prtcpnt_View().updateDBRow("GET01");                                                                                                        //Natural: UPDATE
        //*  UPDATE-PART-RECORD
    }
    private void sub_Get_Fund_Tickers() throws Exception                                                                                                                  //Natural: GET-FUND-TICKERS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, " GET-FUND-TICKERS Start: ");                                                                                                           //Natural: WRITE ' GET-FUND-TICKERS Start: '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  RESET NECA4000                   /* RS0
        //*  NECA4000.FUNCTION-CDE        := 'CFN'
        //*  NECA4000.INPT-KEY-OPTION-CDE := '01'
        //*  NECA4000.CFN-KEY-COMPANY-CDE := '001'
        //*  CALLNAT 'NECN4000' NECA4000
        //* * IF NECA4000.OUTPUT-CNT NE 0
        //* *  MOVE NECA4000.CFN-TICKER-SYMBOL(*)      TO #EXT-TICKER-SYMBOL(*)
        //* *  MOVE NECA4000.CFN-FND-NUM-FUND-CDE (#I) TO #EXT-ALPHA-FUND-CDE(*)
        //* *  ELSE
        //*  RS0
        DbsUtil.callnat(Adsn888.class , getCurrentProcessState(), pdaAdsa888.getPnd_Parm_Area(), pdaAdsa888.getPnd_Nbr_Acct());                                           //Natural: CALLNAT 'ADSN888' #PARM-AREA #NBR-ACCT
        if (condition(Global.isEscape())) return;
        //*  IF NECA4000.OUTPUT-CNT = 0       /* RS0
        //*  RS0
        if (condition(pdaAdsa888.getPnd_Nbr_Acct().equals(getZero())))                                                                                                    //Natural: IF #NBR-ACCT = 0
        {
            pnd_Terminate_Pnd_Terminate_No.setValue(90);                                                                                                                  //Natural: MOVE 90 TO #TERMINATE-NO
            pnd_Terminate_Pnd_Terminate_Msg.setValue(DbsUtil.compress("ERROR IN RETRIEVING FUND TICKER INFORMATION. ", "....VERIFY IF THE EXTERNALIZATION FILE 3/220 IS AVAILABLE")); //Natural: COMPRESS 'ERROR IN RETRIEVING FUND TICKER INFORMATION. ' '....VERIFY IF THE EXTERNALIZATION FILE 3/220 IS AVAILABLE' TO #TERMINATE-MSG
            pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("NEC-CALL");                                                                                                 //Natural: MOVE 'NEC-CALL' TO #STEP-NAME
            pnd_Adsn998_Error_Handler_Pnd_Error_Msg.setValue(pnd_Terminate_Pnd_Terminate_Msg);                                                                            //Natural: MOVE #TERMINATE-MSG TO #ERROR-MSG
                                                                                                                                                                          //Natural: PERFORM TERMINATE-ROUTINE
            sub_Terminate_Routine();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Debug_On.getBoolean()))                                                                                                                         //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*");                                                                                                                           //Natural: WRITE / '*'
            if (Global.isEscape()) return;
            getReports().write(0, " GET-FUND-TICKERS End: ");                                                                                                             //Natural: WRITE ' GET-FUND-TICKERS End: '
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaAdsa888.getPnd_Nbr_Acct());                                                                                                      //Natural: WRITE '=' #NBR-ACCT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-FUND-TICKERS
    }
    private void sub_Error_Routine_For_Post() throws Exception                                                                                                            //Natural: ERROR-ROUTINE-FOR-POST
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        getReports().write(0, "E R R O R :",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code());                                                                           //Natural: WRITE 'E R R O R :' MSG-INFO-SUB.##RETURN-CODE
        if (Global.isEscape()) return;
        getReports().write(0, "REQUEST-ID:",ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id());                                                                                   //Natural: WRITE 'REQUEST-ID:' ADS-PRTCPNT-VIEW.RQST-ID
        if (Global.isEscape()) return;
        DbsUtil.callnat(Pstn9980.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub());                                                                        //Natural: CALLNAT 'PSTN9980' MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        getReports().write(0, pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                                 //Natural: WRITE MSG-INFO-SUB.##MSG
        if (Global.isEscape()) return;
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().reset();                                                                                                             //Natural: RESET ##MSG-NR ##MSG ##RETURN-CODE
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().reset();
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().reset();
        //*    ERROR-ROUTINE-FOR-POST
    }
    private void sub_Terminate_Routine() throws Exception                                                                                                                 //Natural: TERMINATE-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        //*   PERFORM ERROR-LOG  /*  ASK ED IF WE NEED IT  !!!!!!!!!!!!!
        getReports().write(0, NEWLINE,NEWLINE,NEWLINE,NEWLINE," ================================================================= ",NEWLINE,"|*****************************************************************|", //Natural: WRITE /// /' ================================================================= ' /'|*****************************************************************|' /'|*****************************************************************|' /'|** TERMINATING ADAS BATCH PROCESS.....                         **|' /'|**                                                             **|' /'|**' #TERMINATE-MSG1 '**|' /'|**' #TERMINATE-MSG2 '**|' /'|**' #TERMINATE-MSG3 '**|' /'|**                                                             **|' /'|**  >>> IF NECESSARY, PLEASE CALL - ADAS SYSTEM SUPPORT <<<    **|' /'|*****************************************************************|' /'|*****************************************************************|' /' ================================================================= '
            NEWLINE,"|*****************************************************************|",NEWLINE,"|** TERMINATING ADAS BATCH PROCESS.....                         **|",
            NEWLINE,"|**                                                             **|",NEWLINE,"|**",pnd_Terminate_Pnd_Terminate_Msg1,"**|",NEWLINE,"|**",
            pnd_Terminate_Pnd_Terminate_Msg2,"**|",NEWLINE,"|**",pnd_Terminate_Pnd_Terminate_Msg3,"**|",NEWLINE,"|**                                                             **|",
            NEWLINE,"|**  >>> IF NECESSARY, PLEASE CALL - ADAS SYSTEM SUPPORT <<<    **|",NEWLINE,"|*****************************************************************|",
            NEWLINE,"|*****************************************************************|",NEWLINE," ================================================================= ");
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB-MESSAGE
        sub_End_Of_Job_Message();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        DbsUtil.terminate(pnd_Terminate_Pnd_Terminate_No);  if (true) return;                                                                                             //Natural: TERMINATE #TERMINATE-NO
        //*   TERMINATE-ROUTINE
    }
    private void sub_End_Of_Job_Message() throws Exception                                                                                                                //Natural: END-OF-JOB-MESSAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, NEWLINE,NEWLINE,NEWLINE,new TabSetting(35),"TOTAL ACKL Letters written=    : ",pnd_Letters_Written,NEWLINE,new TabSetting(35),              //Natural: WRITE ( 1 ) ///35T 'TOTAL ACKL Letters written=    : ' #LETTERS-WRITTEN / 35T 'TOTAL ERRORS : (No letter produced): ' #ERROR-REPORT-ENTRIES
            "TOTAL ERRORS : (No letter produced): ",pnd_Error_Report_Entries);
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,NEWLINE,NEWLINE,NEWLINE," ================================================================= ",NEWLINE,"|**                                                             **|", //Natural: WRITE /// /' ================================================================= ' /'|**                                                             **|' /'|**            ANNUITIZATION OF OMNI PLUS CONTRACTS             **|' /'|**                                                             **|' /'|**   ADAS - ACKNOWLEDGMENT LETTERS PROCESS ---- JOB RUN ENDED  **|' /'|**                                                             **|' /'|** TOTAL ACKL RECORDS WRITTEN         : ' #LETTERS-WRITTEN '               **|' /'|**                                                             **|' /' =================================================================|'
            NEWLINE,"|**            ANNUITIZATION OF OMNI PLUS CONTRACTS             **|",NEWLINE,"|**                                                             **|",
            NEWLINE,"|**   ADAS - ACKNOWLEDGMENT LETTERS PROCESS ---- JOB RUN ENDED  **|",NEWLINE,"|**                                                             **|",
            NEWLINE,"|** TOTAL ACKL RECORDS WRITTEN         : ",pnd_Letters_Written,"               **|",NEWLINE,"|**                                                             **|",
            NEWLINE," =================================================================|");
        if (Global.isEscape()) return;
        //*   END-OF-JOB-MESSAGE
    }
    private void sub_Display_Passed_Tax_Data() throws Exception                                                                                                           //Natural: DISPLAY-PASSED-TAX-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        //*  BEFORE/AFTER DISPLAY: TAX-LINKAGE, TAX, #TAX-ERRORS-INFO
        //*  EM 031708 START
        //*  EM 031708 END
        getReports().write(1, "TAX LINKAGE section",NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Tx_Tiaa_No(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Tx_Tiaa_Payee_No(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Tx_Cref_No(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Tx_Cref_Payee_No(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Rsdncy_Cde(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Ctzn_Cde(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Tx_Ss_Num(),NEWLINE,NEWLINE,"-",new  //Natural: WRITE ( 1 ) 'TAX LINKAGE section' /'=' ADS-TX-TIAA-NO /'=' ADS-TX-TIAA-PAYEE-NO /'=' ADS-TX-CREF-NO /'=' ADS-TX-CREF-PAYEE-NO /'=' ADS-RSDNCY-CDE /'=' ADS-CTZN-CDE /'=' ADS-TX-SS-NUM / /'-' ( 30 ) /'ADS Type-of-Payment Indicators'/'-' ( 30 ) /'=' ADS-PP-PYMT-IND /'=' ADS-MDO-PYMT-IND /'=' ADS-RTB-T-PYMT-IND /'=' ADS-RTB-C-PYMT-IND /'=' ADS-LS-PYMT-IND /'=' ADS-ROLLOVER-IND /'=' ADS-PP-ROLLOVER-IND /'=' ADS-FILLER /'=' ADS-SETTL-OPTION /'=' ADS-TERM-YEARS /'=' ADS-SPOUSE-IND /'=' ADS-MONEY-SOURCE /'=' ADS-LOB /'=' ADS-PYMNT-TYP-IND /'=' ADS-HARDSHIP-IND /'=' ADS-DOB /'=' ADS-DOD /'=' ADS-ROTH-FRST-CNTRBTN-DTE //'--------'/'TAX Info'/'--------' /'=' ADS-NRA-FLAG /'=' ADS-NRA-MSG-LNS1 /'=' ADS-T-F-LUMP-MSG-LNS ( AL = 60 ) /'=' ADS-T-S-LUMP-MSG-LNS ( AL = 60 ) /'=' ADS-T-L-LUMP-MSG-LNS ( AL = 60 ) /'=' ADS-T-F-PP-MSG-LNS ( AL = 60 ) /'=' ADS-T-S-PP-MSG-LNS ( AL = 60 ) /'=' ADS-T-L-PP-MSG-LNS ( AL = 60 ) /'=' ADS-C-F-LUMP-MSG-LNS ( AL = 60 ) /'=' ADS-C-S-LUMP-MSG-LNS ( AL = 60 ) /'=' ADS-C-L-LUMP-MSG-LNS ( AL = 60 ) /'=' ADS-C-F-PP-MSG-LNS ( AL = 60 ) /'=' ADS-C-S-PP-MSG-LNS ( AL = 60 ) /'=' ADS-C-L-PP-MSG-LNS ( AL = 60 ) //'---------'/'TAX Errors'/'---------'/ /'=' #TAX-ERROR-CODE /'=' #TAX-ERROR-DESC
            RepeatItem(30),NEWLINE,"ADS Type-of-Payment Indicators",NEWLINE,"-",new RepeatItem(30),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Pp_Pymt_Ind(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Mdo_Pymt_Ind(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Rtb_T_Pymt_Ind(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Rtb_C_Pymt_Ind(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Ls_Pymt_Ind(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Rollover_Ind(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Pp_Rollover_Ind(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Filler(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Settl_Option(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Term_Years(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Spouse_Ind(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Money_Source(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Lob(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Pymnt_Typ_Ind(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Hardship_Ind(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Dob(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Dod(),NEWLINE,"=",pdaAdsa555.getTax_Linkage_Ads_Roth_Frst_Cntrbtn_Dte(),NEWLINE,NEWLINE,"--------",NEWLINE,"TAX Info",NEWLINE,"--------",NEWLINE,"=",ldaPstl0175.getPstl0175_Ads_Nra_Flag(),NEWLINE,"=",ldaPstl0175.getPstl0175_Ads_Nra_Msg_Lns1(),NEWLINE,"=",ldaPstl0175.getPstl0175_Ads_T_F_Lump_Msg_Lns(), 
            new AlphanumericLength (60),NEWLINE,"=",ldaPstl0175.getPstl0175_Ads_T_S_Lump_Msg_Lns(), new AlphanumericLength (60),NEWLINE,"=",ldaPstl0175.getPstl0175_Ads_T_L_Lump_Msg_Lns(), 
            new AlphanumericLength (60),NEWLINE,"=",ldaPstl0175.getPstl0175_Ads_T_F_Pp_Msg_Lns(), new AlphanumericLength (60),NEWLINE,"=",ldaPstl0175.getPstl0175_Ads_T_S_Pp_Msg_Lns(), 
            new AlphanumericLength (60),NEWLINE,"=",ldaPstl0175.getPstl0175_Ads_T_L_Pp_Msg_Lns(), new AlphanumericLength (60),NEWLINE,"=",ldaPstl0175.getPstl0175_Ads_C_F_Lump_Msg_Lns(), 
            new AlphanumericLength (60),NEWLINE,"=",ldaPstl0175.getPstl0175_Ads_C_S_Lump_Msg_Lns(), new AlphanumericLength (60),NEWLINE,"=",ldaPstl0175.getPstl0175_Ads_C_L_Lump_Msg_Lns(), 
            new AlphanumericLength (60),NEWLINE,"=",ldaPstl0175.getPstl0175_Ads_C_F_Pp_Msg_Lns(), new AlphanumericLength (60),NEWLINE,"=",ldaPstl0175.getPstl0175_Ads_C_S_Pp_Msg_Lns(), 
            new AlphanumericLength (60),NEWLINE,"=",ldaPstl0175.getPstl0175_Ads_C_L_Pp_Msg_Lns(), new AlphanumericLength (60),NEWLINE,NEWLINE,"---------",
            NEWLINE,"TAX Errors",NEWLINE,"---------",NEWLINE,NEWLINE,"=",pnd_Tax_Errors_Info_Pnd_Tax_Error_Code,NEWLINE,"=",pnd_Tax_Errors_Info_Pnd_Tax_Error_Desc);
        if (Global.isEscape()) return;
        //*   DISPLAY-PASSED-TAX-DATA
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pnd_Adsn998_Error_Handler.reset();                                                                                                                                //Natural: RESET #ADSN998-ERROR-HANDLER
        pnd_Terminate_Pnd_Terminate_Msg.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "INTERNAL ERROR ENCOUNTERED IN PROGRAM :", Global.getINIT_PROGRAM(),     //Natural: COMPRESS 'INTERNAL ERROR ENCOUNTERED IN PROGRAM :' *INIT-PROGRAM ' ERR NO : ' *ERROR-NR ' ERR LINE :' *ERROR-LINE TO #TERMINATE-MSG LEAVING NO SPACE
            " ERR NO : ", Global.getERROR_NR(), " ERR LINE :", Global.getERROR_LINE()));
        pnd_Terminate_Pnd_Terminate_No.setValue(99);                                                                                                                      //Natural: MOVE 99 TO #TERMINATE-NO
        pnd_Adsn998_Error_Handler_Pnd_Error_Msg.setValue(pnd_Terminate_Pnd_Terminate_Msg);                                                                                //Natural: MOVE #TERMINATE-MSG TO #ERROR-MSG
                                                                                                                                                                          //Natural: PERFORM TERMINATE-ROUTINE
        sub_Terminate_Routine();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60");
    }
}
