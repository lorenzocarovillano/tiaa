/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:06:32 PM
**        * FROM NATURAL PROGRAM : Adspda01
************************************************************
**        * FILE NAME            : Adspda01.java
**        * CLASS NAME           : Adspda01
**        * INSTANCE NAME        : Adspda01
************************************************************
***********************************************************************
*  PROGRAM NAME: ADSPDA01
*  DESCRIPTION : THIS IS A ONE TIME PROGRAM TO UPDATE THE NST-TABLE-
*                ENTRY FILE FOR THE ADABAS REPLATFORM PROJECT.
*  DATE WRITTEN: 04/05/2010
***********************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adspda01 extends BLNatBase
{
    // Data Areas
    private LdaAdslda01 ldaAdslda01;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_X;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdslda01 = new LdaAdslda01();
        registerRecord(ldaAdslda01);
        registerRecord(ldaAdslda01.getVw_nst_Table_Entry_View());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAdslda01.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adspda01() throws Exception
    {
        super("Adspda01");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //*                               / USE ADSPDA02 TO VERIFY
        ldaAdslda01.getVw_nst_Table_Entry_View().reset();                                                                                                                 //Natural: RESET NST-TABLE-ENTRY-VIEW
        ldaAdslda01.getNst_Table_Entry_View_Entry_Actve_Ind().setValue("Y");                                                                                              //Natural: ASSIGN ENTRY-ACTVE-IND := 'Y'
        ldaAdslda01.getNst_Table_Entry_View_Entry_Table_Id_Nbr().setValue(19);                                                                                            //Natural: ASSIGN ENTRY-TABLE-ID-NBR := 19
        ldaAdslda01.getNst_Table_Entry_View_Entry_Table_Sub_Id().setValue("RC");                                                                                          //Natural: ASSIGN ENTRY-TABLE-SUB-ID := 'RC'
        ldaAdslda01.getNst_Table_Entry_View_Entry_Cde().setValue("A1");                                                                                                   //Natural: ASSIGN ENTRY-CDE := 'A1'
        ldaAdslda01.getNst_Table_Entry_View_Entry_Dscrptn_Txt().setValue("APO AA");                                                                                       //Natural: ASSIGN ENTRY-DSCRPTN-TXT := 'APO AA'
        ldaAdslda01.getNst_Table_Entry_View_Entry_User_Id().setValue(Global.getUSER());                                                                                   //Natural: ASSIGN ENTRY-USER-ID := *USER
        ldaAdslda01.getVw_nst_Table_Entry_View().insertDBRow();                                                                                                           //Natural: STORE NST-TABLE-ENTRY-VIEW
        ldaAdslda01.getVw_nst_Table_Entry_View().reset();                                                                                                                 //Natural: RESET NST-TABLE-ENTRY-VIEW
        ldaAdslda01.getNst_Table_Entry_View_Entry_Actve_Ind().setValue("Y");                                                                                              //Natural: ASSIGN ENTRY-ACTVE-IND := 'Y'
        ldaAdslda01.getNst_Table_Entry_View_Entry_Table_Id_Nbr().setValue(19);                                                                                            //Natural: ASSIGN ENTRY-TABLE-ID-NBR := 19
        ldaAdslda01.getNst_Table_Entry_View_Entry_Table_Sub_Id().setValue("RC");                                                                                          //Natural: ASSIGN ENTRY-TABLE-SUB-ID := 'RC'
        ldaAdslda01.getNst_Table_Entry_View_Entry_Cde().setValue("AP");                                                                                                   //Natural: ASSIGN ENTRY-CDE := 'AP'
        ldaAdslda01.getNst_Table_Entry_View_Entry_Dscrptn_Txt().setValue("APO AP");                                                                                       //Natural: ASSIGN ENTRY-DSCRPTN-TXT := 'APO AP'
        ldaAdslda01.getNst_Table_Entry_View_Entry_User_Id().setValue(Global.getUSER());                                                                                   //Natural: ASSIGN ENTRY-USER-ID := *USER
        ldaAdslda01.getVw_nst_Table_Entry_View().insertDBRow();                                                                                                           //Natural: STORE NST-TABLE-ENTRY-VIEW
        ldaAdslda01.getVw_nst_Table_Entry_View().reset();                                                                                                                 //Natural: RESET NST-TABLE-ENTRY-VIEW
        ldaAdslda01.getNst_Table_Entry_View_Entry_Actve_Ind().setValue("Y");                                                                                              //Natural: ASSIGN ENTRY-ACTVE-IND := 'Y'
        ldaAdslda01.getNst_Table_Entry_View_Entry_Table_Id_Nbr().setValue(19);                                                                                            //Natural: ASSIGN ENTRY-TABLE-ID-NBR := 19
        ldaAdslda01.getNst_Table_Entry_View_Entry_Table_Sub_Id().setValue("RC");                                                                                          //Natural: ASSIGN ENTRY-TABLE-SUB-ID := 'RC'
        ldaAdslda01.getNst_Table_Entry_View_Entry_Cde().setValue("AE");                                                                                                   //Natural: ASSIGN ENTRY-CDE := 'AE'
        ldaAdslda01.getNst_Table_Entry_View_Entry_Dscrptn_Txt().setValue("APO AE");                                                                                       //Natural: ASSIGN ENTRY-DSCRPTN-TXT := 'APO AE'
        ldaAdslda01.getNst_Table_Entry_View_Entry_User_Id().setValue(Global.getUSER());                                                                                   //Natural: ASSIGN ENTRY-USER-ID := *USER
        ldaAdslda01.getVw_nst_Table_Entry_View().insertDBRow();                                                                                                           //Natural: STORE NST-TABLE-ENTRY-VIEW
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(0, "3 RECORDS SUCCESSFULLY STORED ON NST-TABLE-ENTRY-VIEW");                                                                                   //Natural: WRITE '3 RECORDS SUCCESSFULLY STORED ON NST-TABLE-ENTRY-VIEW'
        if (Global.isEscape()) return;
    }

    //
}
