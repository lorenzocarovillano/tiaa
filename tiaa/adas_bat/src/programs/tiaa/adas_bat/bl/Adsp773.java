/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:05:16 PM
**        * FROM NATURAL PROGRAM : Adsp773
************************************************************
**        * FILE NAME            : Adsp773.java
**        * CLASS NAME           : Adsp773
**        * INSTANCE NAME        : Adsp773
************************************************************
************************************************************************
* PROGRAM  : ADSP773
* GENERATED: APRIL 12, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : UPDATES THE ADAS TRANSACTION HISTORY REPORT DATE
*
* REMARKS  : CLONED FROM NAZP773 (ADAM SYSTEM)
************************************************************************
* MOD DATE   MOD BY      DESCRIPTION OF CHANGES
* 04/12/04   E MELNIK    MODIFIED FOR ADAS (ANNUITIZATION SUNGUARD)
* 07/27/04   T. SHINE       IF MONTH-END PROCESS FALLS ON WEEKEND OR
*                           HOLIDAY, USE ADS-RPT-13 (FACTOR DATE) AS
*                           REPORT DATE.
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp773 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Save_Bizdate_Alpha;

    private DataAccessProgramView vw_adscntl;

    private DbsGroup adscntl_Ads_Cntl_Grp;
    private DbsField adscntl_Ads_Cntl_Rcrd_Typ_Cde;
    private DbsField adscntl_Ads_Cntl_Bsnss_Rcprcl_Dte;
    private DbsField adscntl_Ads_Cntl_Bsnss_Dte;

    private DbsGroup adscntl__R_Field_1;
    private DbsField adscntl_Pnd_Bizdate_Alpha;
    private DbsField adscntl_Ads_Cntl_Bsnss_Tmrrw_Dte;
    private DbsField adscntl_Ads_Trn_Hstry_Extrct_Rpt_Dte;
    private DbsField adscntl_Ads_Rpt_13;
    private DbsField pnd_Date_A;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Save_Bizdate_Alpha = localVariables.newFieldInRecord("pnd_Save_Bizdate_Alpha", "#SAVE-BIZDATE-ALPHA", FieldType.STRING, 8);

        vw_adscntl = new DataAccessProgramView(new NameInfo("vw_adscntl", "ADSCNTL"), "ADS_CNTL", "ADS_CNTL");

        adscntl_Ads_Cntl_Grp = vw_adscntl.getRecord().newGroupInGroup("ADSCNTL_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        adscntl_Ads_Cntl_Rcrd_Typ_Cde = adscntl_Ads_Cntl_Grp.newFieldInGroup("adscntl_Ads_Cntl_Rcrd_Typ_Cde", "ADS-CNTL-RCRD-TYP-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADS_CNTL_RCRD_TYP_CDE");
        adscntl_Ads_Cntl_Bsnss_Rcprcl_Dte = adscntl_Ads_Cntl_Grp.newFieldInGroup("adscntl_Ads_Cntl_Bsnss_Rcprcl_Dte", "ADS-CNTL-BSNSS-RCPRCL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_RCPRCL_DTE");
        adscntl_Ads_Cntl_Bsnss_Dte = adscntl_Ads_Cntl_Grp.newFieldInGroup("adscntl_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "ADS_CNTL_BSNSS_DTE");

        adscntl__R_Field_1 = adscntl_Ads_Cntl_Grp.newGroupInGroup("adscntl__R_Field_1", "REDEFINE", adscntl_Ads_Cntl_Bsnss_Dte);
        adscntl_Pnd_Bizdate_Alpha = adscntl__R_Field_1.newFieldInGroup("adscntl_Pnd_Bizdate_Alpha", "#BIZDATE-ALPHA", FieldType.STRING, 8);
        adscntl_Ads_Cntl_Bsnss_Tmrrw_Dte = adscntl_Ads_Cntl_Grp.newFieldInGroup("adscntl_Ads_Cntl_Bsnss_Tmrrw_Dte", "ADS-CNTL-BSNSS-TMRRW-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_TMRRW_DTE");
        adscntl_Ads_Trn_Hstry_Extrct_Rpt_Dte = vw_adscntl.getRecord().newFieldInGroup("adscntl_Ads_Trn_Hstry_Extrct_Rpt_Dte", "ADS-TRN-HSTRY-EXTRCT-RPT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADS_TRN_HSTRY_EXTRCT_RPT_DTE");
        adscntl_Ads_Rpt_13 = vw_adscntl.getRecord().newFieldInGroup("adscntl_Ads_Rpt_13", "ADS-RPT-13", FieldType.DATE, RepeatingFieldStrategy.None, "ADS_RPT_13");
        registerRecord(vw_adscntl);

        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_adscntl.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp773() throws Exception
    {
        super("Adsp773");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().write(0, "**** THIS IS THE NAT 314 VERSION OF NAZP773 ******");                                                                                      //Natural: WRITE '**** THIS IS THE NAT 314 VERSION OF NAZP773 ******'
        if (Global.isEscape()) return;
        vw_adscntl.startDatabaseRead                                                                                                                                      //Natural: READ ( 2 ) ADSCNTL BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "READ01",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        READ01:
        while (condition(vw_adscntl.readNextRow("READ01")))
        {
            if (condition(vw_adscntl.getAstCOUNTER().equals(2)))                                                                                                          //Natural: IF *COUNTER = 2
            {
                if (condition(adscntl_Ads_Rpt_13.notEquals(getZero())))                                                                                                   //Natural: IF ADS-RPT-13 NE 0
                {
                    adscntl_Ads_Trn_Hstry_Extrct_Rpt_Dte.setValue(adscntl_Ads_Rpt_13);                                                                                    //Natural: MOVE ADS-RPT-13 TO ADS-TRN-HSTRY-EXTRCT-RPT-DTE
                    pnd_Date_A.setValueEdited(adscntl_Ads_Rpt_13,new ReportEditMask("YYYYMMDD"));                                                                         //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE-A
                    pnd_Save_Bizdate_Alpha.setValue(pnd_Date_A);                                                                                                          //Natural: MOVE #DATE-A TO #SAVE-BIZDATE-ALPHA
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    adscntl_Ads_Trn_Hstry_Extrct_Rpt_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),adscntl_Pnd_Bizdate_Alpha);                                        //Natural: MOVE EDITED #BIZDATE-ALPHA TO ADS-TRN-HSTRY-EXTRCT-RPT-DTE ( EM = YYYYMMDD )
                    pnd_Save_Bizdate_Alpha.setValue(adscntl_Ads_Cntl_Bsnss_Dte);                                                                                          //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #SAVE-BIZDATE-ALPHA
                }                                                                                                                                                         //Natural: END-IF
                vw_adscntl.updateDBRow("READ01");                                                                                                                         //Natural: UPDATE
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_adscntl.startDatabaseRead                                                                                                                                      //Natural: READ ( 1 ) ADSCNTL BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "READ02",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        1
        );
        READ02:
        while (condition(vw_adscntl.readNextRow("READ02")))
        {
            if (condition(vw_adscntl.getAstCOUNTER().equals(1)))                                                                                                          //Natural: IF *COUNTER = 1
            {
                adscntl_Ads_Trn_Hstry_Extrct_Rpt_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Save_Bizdate_Alpha);                                               //Natural: MOVE EDITED #SAVE-BIZDATE-ALPHA TO ADS-TRN-HSTRY-EXTRCT-RPT-DTE ( EM = YYYYMMDD )
                vw_adscntl.updateDBRow("READ02");                                                                                                                         //Natural: UPDATE
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
