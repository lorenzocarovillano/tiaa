/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:06:16 PM
**        * FROM NATURAL PROGRAM : Adsp9994
************************************************************
**        * FILE NAME            : Adsp9994.java
**        * CLASS NAME           : Adsp9994
**        * INSTANCE NAME        : Adsp9994
************************************************************
************************************************************************
* PROGRAM...: ADSP9994
* READ ADAT FOR THE RQST ID THAT WILL BE UPDATED TO BE PICKED-UP
* BY ADSP6002 IN JOB PAN2300D.
* 03/01/2017  R.CARREON  RESTOWED FOR PIN EXPANSION
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp9994 extends BLNatBase
{
    // Data Areas
    private LdaAdsl401 ldaAdsl401;
    private LdaAdslcntl ldaAdslcntl;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_adat;
    private DbsField adat_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField adat_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField adat_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField adat_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField adat_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField adat_Naz_Tbl_Rcrd_Dscrptn_Txt;

    private DbsGroup adat__R_Field_1;
    private DbsField adat_Pnd_Cntrcts_Prcssd;
    private DbsGroup adat_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup;
    private DbsField adat_Naz_Tbl_Secndry_Dscrptn_Txt;

    private DbsGroup adat__R_Field_2;
    private DbsField adat_Pnd_Count;
    private DbsField adat_Naz_Tbl_Rcrd_Updt_Dte;
    private DbsField adat_Naz_Tbl_Updt_Racf_Id;
    private DbsField pnd_Naz_Table_Key;

    private DbsGroup pnd_Naz_Table_Key__R_Field_3;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id;
    private DbsField pnd_First_Run;
    private DbsField pnd_Rd_Cnt;
    private DbsField pnd_Counter;
    private DbsField pnd_Out;
    private DbsField pnd_Ads_Cntl_Bsnss_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl401 = new LdaAdsl401();
        registerRecord(ldaAdsl401);
        registerRecord(ldaAdsl401.getVw_ads_Prtcpnt_View());
        ldaAdslcntl = new LdaAdslcntl();
        registerRecord(ldaAdslcntl);
        registerRecord(ldaAdslcntl.getVw_ads_Cntl_View());

        // Local Variables
        localVariables = new DbsRecord();

        vw_adat = new DataAccessProgramView(new NameInfo("vw_adat", "ADAT"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        adat_Naz_Tbl_Rcrd_Actv_Ind = vw_adat.getRecord().newFieldInGroup("adat_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "NAZ_TBL_RCRD_ACTV_IND");
        adat_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        adat_Naz_Tbl_Rcrd_Typ_Ind = vw_adat.getRecord().newFieldInGroup("adat_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "NAZ_TBL_RCRD_TYP_IND");
        adat_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        adat_Naz_Tbl_Rcrd_Lvl1_Id = vw_adat.getRecord().newFieldInGroup("adat_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "NAZ_TBL_RCRD_LVL1_ID");
        adat_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        adat_Naz_Tbl_Rcrd_Lvl2_Id = vw_adat.getRecord().newFieldInGroup("adat_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "NAZ_TBL_RCRD_LVL2_ID");
        adat_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        adat_Naz_Tbl_Rcrd_Lvl3_Id = vw_adat.getRecord().newFieldInGroup("adat_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "NAZ_TBL_RCRD_LVL3_ID");
        adat_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        adat_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_adat.getRecord().newFieldInGroup("adat_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        adat_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");

        adat__R_Field_1 = vw_adat.getRecord().newGroupInGroup("adat__R_Field_1", "REDEFINE", adat_Naz_Tbl_Rcrd_Dscrptn_Txt);
        adat_Pnd_Cntrcts_Prcssd = adat__R_Field_1.newFieldInGroup("adat_Pnd_Cntrcts_Prcssd", "#CNTRCTS-PRCSSD", FieldType.NUMERIC, 6);
        adat_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup = vw_adat.getRecord().newGroupInGroup("ADAT_NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", "NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        adat_Naz_Tbl_Secndry_Dscrptn_Txt = adat_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup.newFieldArrayInGroup("adat_Naz_Tbl_Secndry_Dscrptn_Txt", "NAZ-TBL-SECNDRY-DSCRPTN-TXT", 
            FieldType.STRING, 80, new DbsArrayController(1, 1), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        adat_Naz_Tbl_Secndry_Dscrptn_Txt.setDdmHeader("CDE/2ND/DSC");

        adat__R_Field_2 = vw_adat.getRecord().newGroupInGroup("adat__R_Field_2", "REDEFINE", adat_Naz_Tbl_Secndry_Dscrptn_Txt);
        adat_Pnd_Count = adat__R_Field_2.newFieldInGroup("adat_Pnd_Count", "#COUNT", FieldType.NUMERIC, 6);
        adat_Naz_Tbl_Rcrd_Updt_Dte = vw_adat.getRecord().newFieldInGroup("adat_Naz_Tbl_Rcrd_Updt_Dte", "NAZ-TBL-RCRD-UPDT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "NAZ_TBL_RCRD_UPDT_DTE");
        adat_Naz_Tbl_Rcrd_Updt_Dte.setDdmHeader("LST UPDT");
        adat_Naz_Tbl_Updt_Racf_Id = vw_adat.getRecord().newFieldInGroup("adat_Naz_Tbl_Updt_Racf_Id", "NAZ-TBL-UPDT-RACF-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "NAZ_TBL_UPDT_RACF_ID");
        adat_Naz_Tbl_Updt_Racf_Id.setDdmHeader("UPDT BY");
        registerRecord(vw_adat);

        pnd_Naz_Table_Key = localVariables.newFieldInRecord("pnd_Naz_Table_Key", "#NAZ-TABLE-KEY", FieldType.STRING, 29);

        pnd_Naz_Table_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Naz_Table_Key__R_Field_3", "REDEFINE", pnd_Naz_Table_Key);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id = pnd_Naz_Table_Key__R_Field_3.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id", "#NAZ-TABLE-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id = pnd_Naz_Table_Key__R_Field_3.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id", "#NAZ-TABLE-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id = pnd_Naz_Table_Key__R_Field_3.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id", "#NAZ-TABLE-LVL3-ID", 
            FieldType.STRING, 20);
        pnd_First_Run = localVariables.newFieldInRecord("pnd_First_Run", "#FIRST-RUN", FieldType.BOOLEAN, 1);
        pnd_Rd_Cnt = localVariables.newFieldInRecord("pnd_Rd_Cnt", "#RD-CNT", FieldType.NUMERIC, 6);
        pnd_Counter = localVariables.newFieldInRecord("pnd_Counter", "#COUNTER", FieldType.NUMERIC, 6);
        pnd_Out = localVariables.newFieldInRecord("pnd_Out", "#OUT", FieldType.NUMERIC, 6);
        pnd_Ads_Cntl_Bsnss_Dte = localVariables.newFieldInRecord("pnd_Ads_Cntl_Bsnss_Dte", "#ADS-CNTL-BSNSS-DTE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_adat.reset();

        ldaAdsl401.initializeValues();
        ldaAdslcntl.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp9994() throws Exception
    {
        super("Adsp9994");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *
        getReports().write(0, "** EXECUTING ",Global.getPROGRAM());                                                                                                       //Natural: WRITE '** EXECUTING ' *PROGRAM
        if (Global.isEscape()) return;
        ldaAdslcntl.getVw_ads_Cntl_View().startDatabaseRead                                                                                                               //Natural: FORMAT ( 1 ) PS = 60 LS = 133;//Natural: READ ( 1 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "READ01",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        1
        );
        READ01:
        while (condition(ldaAdslcntl.getVw_ads_Cntl_View().readNextRow("READ01")))
        {
            pnd_Ads_Cntl_Bsnss_Dte.setValueEdited(ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Dte(),new ReportEditMask("99999999"));                                      //Natural: MOVE EDITED ADS-CNTL-BSNSS-DTE ( EM = 99999999 ) TO #ADS-CNTL-BSNSS-DTE
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(1, "=",pnd_Ads_Cntl_Bsnss_Dte);                                                                                                                //Natural: WRITE ( 1 ) '=' #ADS-CNTL-BSNSS-DTE
        if (Global.isEscape()) return;
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ099");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ099'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("UTL");                                                                                                          //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'UTL'
        //* *AZ-TABLE-LVL3-ID := 'RQST'
        vw_adat.startDatabaseRead                                                                                                                                         //Natural: READ ADAT BY ADAT.NAZ-TBL-SUPER1 STARTING FROM #NAZ-TABLE-KEY
        (
        "READ02",
        new Wc[] { new Wc("NAZ_TBL_SUPER1", ">=", pnd_Naz_Table_Key, WcType.BY) },
        new Oc[] { new Oc("NAZ_TBL_SUPER1", "ASC") }
        );
        READ02:
        while (condition(vw_adat.readNextRow("READ02")))
        {
            if (condition(adat_Naz_Tbl_Rcrd_Lvl1_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id) || adat_Naz_Tbl_Rcrd_Lvl2_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id))) //Natural: IF NAZ-TBL-RCRD-LVL1-ID NE #NAZ-TABLE-LVL1-ID OR NAZ-TBL-RCRD-LVL2-ID NE #NAZ-TABLE-LVL2-ID
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(adat_Naz_Tbl_Rcrd_Typ_Ind.equals("C") && adat_Naz_Tbl_Rcrd_Actv_Ind.equals("Y"))))                                                            //Natural: ACCEPT IF NAZ-TBL-RCRD-TYP-IND = 'C' AND NAZ-TBL-RCRD-ACTV-IND = 'Y'
            {
                continue;
            }
            getReports().write(1, "CONTRACT",adat_Naz_Tbl_Rcrd_Lvl3_Id,"RQST ID",adat_Naz_Tbl_Rcrd_Dscrptn_Txt);                                                          //Natural: WRITE ( 1 ) 'CONTRACT' NAZ-TBL-RCRD-LVL3-ID 'RQST ID' NAZ-TBL-RCRD-DSCRPTN-TXT
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaAdsl401.getVw_ads_Prtcpnt_View().startDatabaseFind                                                                                                         //Natural: FIND ADS-PRTCPNT-VIEW WITH ADS-PRTCPNT-VIEW.RQST-ID = NAZ-TBL-RCRD-DSCRPTN-TXT
            (
            "FIND01",
            new Wc[] { new Wc("RQST_ID", "=", adat_Naz_Tbl_Rcrd_Dscrptn_Txt, WcType.WITH) }
            );
            FIND01:
            while (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().readNextRow("FIND01", true)))
            {
                ldaAdsl401.getVw_ads_Prtcpnt_View().setIfNotFoundControlFlag(false);
                if (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().getAstCOUNTER().equals(0)))                                                                             //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(0, "NO RECORDS FOR",adat_Naz_Tbl_Rcrd_Dscrptn_Txt);                                                                                //Natural: WRITE 'NO RECORDS FOR' NAZ-TBL-RCRD-DSCRPTN-TXT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
                getReports().write(1, "STTS",ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde(),"OP",ldaAdsl401.getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind());                       //Natural: WRITE ( 1 ) 'STTS' ADS-PRTCPNT-VIEW.ADP-STTS-CDE 'OP' ADS-PRTCPNT-VIEW.ADP-OPN-CLSD-IND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(!(DbsUtil.maskMatches(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde(),"'T'") && ldaAdsl401.getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind().equals("O")))) //Natural: ACCEPT IF ADS-PRTCPNT-VIEW.ADP-STTS-CDE = MASK ( 'T' ) AND ADS-PRTCPNT-VIEW.ADP-OPN-CLSD-IND = 'O'
                {
                    continue;
                }
                getReports().write(1, "UPDATING",ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                                               //Natural: WRITE ( 1 ) 'UPDATING' ADS-PRTCPNT-VIEW.RQST-ID
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, "LAST ACTIVITY BEFORE",ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte());                                                        //Natural: WRITE ( 1 ) 'LAST ACTIVITY BEFORE' ADP-LST-ACTVTY-DTE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ads_Cntl_Bsnss_Dte);                                //Natural: MOVE EDITED #ADS-CNTL-BSNSS-DTE TO ADP-LST-ACTVTY-DTE ( EM = YYYYMMDD )
                ldaAdsl401.getVw_ads_Prtcpnt_View().updateDBRow("FIND01");                                                                                                //Natural: UPDATE
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                getReports().write(1, "LAST ACTIVITY AFTER",ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte());                                                         //Natural: WRITE ( 1 ) 'LAST ACTIVITY AFTER' ADP-LST-ACTVTY-DTE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133");
    }
}
