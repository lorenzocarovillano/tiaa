/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:03:33 PM
**        * FROM NATURAL PROGRAM : Adsp6001
************************************************************
**        * FILE NAME            : Adsp6001.java
**        * CLASS NAME           : Adsp6001
**        * INSTANCE NAME        : Adsp6001
************************************************************
************************************************************************
* HISTORY
* 04/19/04 E. MELNIK  MODIFIED FOR ANNUITIZATION SUNGUARD
* 11/18/04 T. SHINE   CHANGED CONTROL DATE FOR END OF MONTH
* 03/17/2008 E.MELNIK   ROTH 403B/401K CHANGES.  RECOMPILED FOR
*                       NEW ADSL401/ADSL450/ADSLCNTL.
* 12/01/2008 R.SACHARNY SUB TITLE (REA) CHANGED TO (REA/ACCESS) RS0
* 7/19/2010  C. MASON   RESTOW DUE TO LDA CHANGES
* 03/05/12   E. MELNIK RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                      UPDATED LINKAGE AREAS.
* 03/30/12   O. SOTTO  ADDITONAL RATE BASE EXP CHANGES. SC 033012.
* 01/18/13   E. MELNIK TNG PROJECT.  RESTOW TO INCLUDE UPDATED LINKAGE
*                      AREAS.
* 02/22/2017 R.CARREON PIN EXPANSION 02222017
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp6001 extends BLNatBase
{
    // Data Areas
    private LdaAdsl401 ldaAdsl401;
    private LdaAdsl450 ldaAdsl450;
    private LdaAdsl6005 ldaAdsl6005;
    private LdaAdslcntl ldaAdslcntl;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cis_Prtcpnt_File_View;
    private DbsField cis_Prtcpnt_File_View_Cis_Pin_Nbr;
    private DbsField pnd_Counter_Start;
    private DbsField pnd_Tiaa_Ind;
    private DbsField pnd_Total_Processed;
    private DbsField pnd_Error_Ind;
    private DbsField pnd_Cntrl_Total;
    private DbsField pnd_Cntrl_Ok;
    private DbsField pnd_Sub_Total_Grd;
    private DbsField pnd_Sub_Total_Std;
    private DbsField pnd_Cis_Super;
    private DbsField pnd_Date_Conv;

    private DbsGroup pnd_Date_Conv__R_Field_1;
    private DbsField pnd_Date_Conv_Pnd_Date_Conv_Ccyy;
    private DbsField pnd_Date_Conv_Pnd_Date_Conv_Mm;
    private DbsField pnd_Date_Conv_Pnd_Date_Conv_Dd;
    private DbsField pnd_Print_Date;

    private DbsGroup pnd_Print_Date__R_Field_2;
    private DbsField pnd_Print_Date_Pnd_Print_Date_Dd;
    private DbsField pnd_Print_Date_Pnd_Print_Date_Mm;
    private DbsField pnd_Print_Date_Pnd_Print_Date_Ccyy;
    private DbsField pnd_Part_Date;

    private DbsGroup pnd_Part_Date__R_Field_3;
    private DbsField pnd_Part_Date_Pnd_Part_Date_Numeric;
    private DbsField pnd_Nai_Super1;

    private DbsGroup pnd_Nai_Super1__R_Field_4;
    private DbsField pnd_Nai_Super1_Pnd_Rqst_Id_Ia;
    private DbsField pnd_Nai_Super1_Pnd_Nai_Record_Type;
    private DbsField pnd_Nai_Super1_Pnd_Nai_Sqnce_Nbr;
    private DbsField pnd_Temp_Date_A;

    private DbsGroup pnd_Temp_Date_A__R_Field_5;
    private DbsField pnd_Temp_Date_A_Pnd_Temp_Date_N;

    private DataAccessProgramView vw_ads_Ia_Rsl2;
    private DbsField ads_Ia_Rsl2_Count_Castadi_Dtl_Tiaa_Data;

    private DbsGroup ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Rate_Cd;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Acct_Cd;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Eff_Rte_Intrst;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt;

    private DbsGroup pnd_Ads_Ia_Rslt;
    private DbsField pnd_Ads_Ia_Rslt_Pnd_Adi_Tiaa_Rate_Cnt;
    private DbsField pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt;
    private DbsField pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt;
    private DbsField pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt;
    private DbsField pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt;
    private DbsField pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Acct_Cd;
    private DbsField pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd;
    private DbsField pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Eff_Rte_Intrst;
    private DbsField pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt;
    private DbsField pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt;
    private DbsField pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt;
    private DbsField pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt;
    private DbsField pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt;
    private DbsField pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt;
    private DbsField pnd_Max_Rate;
    private DbsField pnd_Max_Rslt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl401 = new LdaAdsl401();
        registerRecord(ldaAdsl401);
        registerRecord(ldaAdsl401.getVw_ads_Prtcpnt_View());
        ldaAdsl450 = new LdaAdsl450();
        registerRecord(ldaAdsl450);
        registerRecord(ldaAdsl450.getVw_ads_Ia_Rslt_View());
        ldaAdsl6005 = new LdaAdsl6005();
        registerRecord(ldaAdsl6005);
        ldaAdslcntl = new LdaAdslcntl();
        registerRecord(ldaAdslcntl);
        registerRecord(ldaAdslcntl.getVw_ads_Cntl_View());

        // Local Variables
        localVariables = new DbsRecord();

        vw_cis_Prtcpnt_File_View = new DataAccessProgramView(new NameInfo("vw_cis_Prtcpnt_File_View", "CIS-PRTCPNT-FILE-VIEW"), "CIS_PRTCPNT_FILE_12", 
            "CIS_PRTCPNT_FILE");
        cis_Prtcpnt_File_View_Cis_Pin_Nbr = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Pin_Nbr", "CIS-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "CIS_PIN_NBR");
        registerRecord(vw_cis_Prtcpnt_File_View);

        pnd_Counter_Start = localVariables.newFieldInRecord("pnd_Counter_Start", "#COUNTER-START", FieldType.NUMERIC, 3);
        pnd_Tiaa_Ind = localVariables.newFieldInRecord("pnd_Tiaa_Ind", "#TIAA-IND", FieldType.STRING, 1);
        pnd_Total_Processed = localVariables.newFieldInRecord("pnd_Total_Processed", "#TOTAL-PROCESSED", FieldType.NUMERIC, 7);
        pnd_Error_Ind = localVariables.newFieldInRecord("pnd_Error_Ind", "#ERROR-IND", FieldType.STRING, 1);
        pnd_Cntrl_Total = localVariables.newFieldInRecord("pnd_Cntrl_Total", "#CNTRL-TOTAL", FieldType.NUMERIC, 7);
        pnd_Cntrl_Ok = localVariables.newFieldInRecord("pnd_Cntrl_Ok", "#CNTRL-OK", FieldType.STRING, 1);
        pnd_Sub_Total_Grd = localVariables.newFieldInRecord("pnd_Sub_Total_Grd", "#SUB-TOTAL-GRD", FieldType.NUMERIC, 11, 2);
        pnd_Sub_Total_Std = localVariables.newFieldInRecord("pnd_Sub_Total_Std", "#SUB-TOTAL-STD", FieldType.NUMERIC, 11, 2);
        pnd_Cis_Super = localVariables.newFieldInRecord("pnd_Cis_Super", "#CIS-SUPER", FieldType.STRING, 35);
        pnd_Date_Conv = localVariables.newFieldInRecord("pnd_Date_Conv", "#DATE-CONV", FieldType.NUMERIC, 8);

        pnd_Date_Conv__R_Field_1 = localVariables.newGroupInRecord("pnd_Date_Conv__R_Field_1", "REDEFINE", pnd_Date_Conv);
        pnd_Date_Conv_Pnd_Date_Conv_Ccyy = pnd_Date_Conv__R_Field_1.newFieldInGroup("pnd_Date_Conv_Pnd_Date_Conv_Ccyy", "#DATE-CONV-CCYY", FieldType.NUMERIC, 
            4);
        pnd_Date_Conv_Pnd_Date_Conv_Mm = pnd_Date_Conv__R_Field_1.newFieldInGroup("pnd_Date_Conv_Pnd_Date_Conv_Mm", "#DATE-CONV-MM", FieldType.NUMERIC, 
            2);
        pnd_Date_Conv_Pnd_Date_Conv_Dd = pnd_Date_Conv__R_Field_1.newFieldInGroup("pnd_Date_Conv_Pnd_Date_Conv_Dd", "#DATE-CONV-DD", FieldType.NUMERIC, 
            2);
        pnd_Print_Date = localVariables.newFieldInRecord("pnd_Print_Date", "#PRINT-DATE", FieldType.NUMERIC, 8);

        pnd_Print_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Print_Date__R_Field_2", "REDEFINE", pnd_Print_Date);
        pnd_Print_Date_Pnd_Print_Date_Dd = pnd_Print_Date__R_Field_2.newFieldInGroup("pnd_Print_Date_Pnd_Print_Date_Dd", "#PRINT-DATE-DD", FieldType.NUMERIC, 
            2);
        pnd_Print_Date_Pnd_Print_Date_Mm = pnd_Print_Date__R_Field_2.newFieldInGroup("pnd_Print_Date_Pnd_Print_Date_Mm", "#PRINT-DATE-MM", FieldType.NUMERIC, 
            2);
        pnd_Print_Date_Pnd_Print_Date_Ccyy = pnd_Print_Date__R_Field_2.newFieldInGroup("pnd_Print_Date_Pnd_Print_Date_Ccyy", "#PRINT-DATE-CCYY", FieldType.NUMERIC, 
            4);
        pnd_Part_Date = localVariables.newFieldInRecord("pnd_Part_Date", "#PART-DATE", FieldType.STRING, 8);

        pnd_Part_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Part_Date__R_Field_3", "REDEFINE", pnd_Part_Date);
        pnd_Part_Date_Pnd_Part_Date_Numeric = pnd_Part_Date__R_Field_3.newFieldInGroup("pnd_Part_Date_Pnd_Part_Date_Numeric", "#PART-DATE-NUMERIC", FieldType.NUMERIC, 
            8);
        pnd_Nai_Super1 = localVariables.newFieldInRecord("pnd_Nai_Super1", "#NAI-SUPER1", FieldType.STRING, 41);

        pnd_Nai_Super1__R_Field_4 = localVariables.newGroupInRecord("pnd_Nai_Super1__R_Field_4", "REDEFINE", pnd_Nai_Super1);
        pnd_Nai_Super1_Pnd_Rqst_Id_Ia = pnd_Nai_Super1__R_Field_4.newFieldInGroup("pnd_Nai_Super1_Pnd_Rqst_Id_Ia", "#RQST-ID-IA", FieldType.STRING, 35);
        pnd_Nai_Super1_Pnd_Nai_Record_Type = pnd_Nai_Super1__R_Field_4.newFieldInGroup("pnd_Nai_Super1_Pnd_Nai_Record_Type", "#NAI-RECORD-TYPE", FieldType.STRING, 
            3);
        pnd_Nai_Super1_Pnd_Nai_Sqnce_Nbr = pnd_Nai_Super1__R_Field_4.newFieldInGroup("pnd_Nai_Super1_Pnd_Nai_Sqnce_Nbr", "#NAI-SQNCE-NBR", FieldType.NUMERIC, 
            3);
        pnd_Temp_Date_A = localVariables.newFieldInRecord("pnd_Temp_Date_A", "#TEMP-DATE-A", FieldType.STRING, 8);

        pnd_Temp_Date_A__R_Field_5 = localVariables.newGroupInRecord("pnd_Temp_Date_A__R_Field_5", "REDEFINE", pnd_Temp_Date_A);
        pnd_Temp_Date_A_Pnd_Temp_Date_N = pnd_Temp_Date_A__R_Field_5.newFieldInGroup("pnd_Temp_Date_A_Pnd_Temp_Date_N", "#TEMP-DATE-N", FieldType.NUMERIC, 
            8);

        vw_ads_Ia_Rsl2 = new DataAccessProgramView(new NameInfo("vw_ads_Ia_Rsl2", "ADS-IA-RSL2"), "ADS_IA_RSLT", "ADS_IA_RSLT", DdmPeriodicGroups.getInstance().getGroups("ADS_IA_RSLT"));
        ads_Ia_Rsl2_Count_Castadi_Dtl_Tiaa_Data = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Count_Castadi_Dtl_Tiaa_Data", "C*ADI-DTL-TIAA-DATA", 
            RepeatingFieldStrategy.CAsteriskVariable, "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");

        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data = vw_ads_Ia_Rsl2.getRecord().newGroupInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data", "ADI-DTL-TIAA-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Rate_Cd = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Rate_Cd", "ADI-DTL-TIAA-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt", "ADI-DTL-TIAA-DA-STD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt", "ADI-DTL-TIAA-DA-GRD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Acct_Cd = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Acct_Cd", "ADI-DTL-TIAA-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_ACCT_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd", "ADI-DTL-TIAA-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_IA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Eff_Rte_Intrst = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Eff_Rte_Intrst", "ADI-DTL-TIAA-EFF-RTE-INTRST", 
            FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_EFF_RTE_INTRST", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt", "ADI-DTL-TIAA-GRD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt", "ADI-DTL-TIAA-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt", "ADI-DTL-FNL-GRD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt", "ADI-DTL-FNL-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt", "ADI-DTL-TIAA-STD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt", "ADI-DTL-TIAA-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt", "ADI-DTL-FNL-STD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt", "ADI-DTL-FNL-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        registerRecord(vw_ads_Ia_Rsl2);

        pnd_Ads_Ia_Rslt = localVariables.newGroupInRecord("pnd_Ads_Ia_Rslt", "#ADS-IA-RSLT");
        pnd_Ads_Ia_Rslt_Pnd_Adi_Tiaa_Rate_Cnt = pnd_Ads_Ia_Rslt.newFieldInGroup("pnd_Ads_Ia_Rslt_Pnd_Adi_Tiaa_Rate_Cnt", "#ADI-TIAA-RATE-CNT", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt = pnd_Ads_Ia_Rslt.newFieldArrayInGroup("pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt", "#ADI-DTL-FNL-STD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 250));
        pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt = pnd_Ads_Ia_Rslt.newFieldArrayInGroup("pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt", "#ADI-DTL-FNL-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 250));
        pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt = pnd_Ads_Ia_Rslt.newFieldArrayInGroup("pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt", "#ADI-DTL-FNL-GRD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 250));
        pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt = pnd_Ads_Ia_Rslt.newFieldArrayInGroup("pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt", "#ADI-DTL-FNL-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 250));
        pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Acct_Cd = pnd_Ads_Ia_Rslt.newFieldArrayInGroup("pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Acct_Cd", "#ADI-DTL-TIAA-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 250));
        pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd = pnd_Ads_Ia_Rslt.newFieldArrayInGroup("pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd", "#ADI-DTL-TIAA-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 250));
        pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Eff_Rte_Intrst = pnd_Ads_Ia_Rslt.newFieldArrayInGroup("pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Eff_Rte_Intrst", "#ADI-DTL-TIAA-EFF-RTE-INTRST", 
            FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 250));
        pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt = pnd_Ads_Ia_Rslt.newFieldArrayInGroup("pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt", "#ADI-DTL-TIAA-DA-STD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 250));
        pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt = pnd_Ads_Ia_Rslt.newFieldArrayInGroup("pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt", "#ADI-DTL-TIAA-DA-GRD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 250));
        pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt = pnd_Ads_Ia_Rslt.newFieldArrayInGroup("pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt", "#ADI-DTL-TIAA-GRD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 250));
        pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt = pnd_Ads_Ia_Rslt.newFieldArrayInGroup("pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt", "#ADI-DTL-TIAA-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 250));
        pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt = pnd_Ads_Ia_Rslt.newFieldArrayInGroup("pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt", "#ADI-DTL-TIAA-STD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 250));
        pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt = pnd_Ads_Ia_Rslt.newFieldArrayInGroup("pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt", "#ADI-DTL-TIAA-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 250));
        pnd_Max_Rate = localVariables.newFieldInRecord("pnd_Max_Rate", "#MAX-RATE", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Rslt = localVariables.newFieldInRecord("pnd_Max_Rslt", "#MAX-RSLT", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cis_Prtcpnt_File_View.reset();
        vw_ads_Ia_Rsl2.reset();

        ldaAdsl401.initializeValues();
        ldaAdsl450.initializeValues();
        ldaAdsl6005.initializeValues();
        ldaAdslcntl.initializeValues();

        localVariables.reset();
        pnd_Max_Rate.setInitialValue(250);
        pnd_Max_Rslt.setInitialValue(125);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp6001() throws Exception
    {
        super("Adsp6001");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) LS = 132 PS = 55;//Natural: FORMAT ( 02 ) LS = 132 PS = 55;//Natural: FORMAT ( 03 ) LS = 132 PS = 55;//Natural: FORMAT ( 04 ) LS = 132 PS = 55
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 03 )
                                                                                                                                                                          //Natural: PERFORM NAZ-CONTROL
        sub_Naz_Control();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Cntrl_Ok.notEquals("Y")))                                                                                                                       //Natural: IF #CNTRL-OK NE 'Y'
        {
            getReports().write(0, "ERROR IN CONTROL RECORD SETUP ");                                                                                                      //Natural: WRITE 'ERROR IN CONTROL RECORD SETUP '
            if (Global.isEscape()) return;
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl401.getVw_ads_Prtcpnt_View().startDatabaseRead                                                                                                             //Natural: READ ADS-PRTCPNT-VIEW BY ADP-SUPER1 STARTING FROM 'O'
        (
        "PART",
        new Wc[] { new Wc("ADP_SUPER1", ">=", "O", WcType.BY) },
        new Oc[] { new Oc("ADP_SUPER1", "ASC") }
        );
        PART:
        while (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().readNextRow("PART")))
        {
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind().notEquals("O")))                                                                              //Natural: IF ADS-PRTCPNT-VIEW.ADP-OPN-CLSD-IND NE 'O'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Part_Date.setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                                             //Natural: MOVE EDITED ADS-PRTCPNT-VIEW.ADP-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #PART-DATE
            //*                                              /* TS 111804
            if (condition(ldaAdslcntl.getAds_Cntl_View_Ads_Rpt_13().notEquals(getZero())))                                                                                //Natural: IF ADS-RPT-13 NE 0
            {
                pnd_Temp_Date_A.setValueEdited(ldaAdslcntl.getAds_Cntl_View_Ads_Rpt_13(),new ReportEditMask("YYYYMMDD"));                                                 //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #TEMP-DATE-A
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Temp_Date_A_Pnd_Temp_Date_N.setValue(ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Dte());                                                              //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #TEMP-DATE-N
            }                                                                                                                                                             //Natural: END-IF
            //*                                              /* TS 111804
            //*  TS 111804
            if (condition(!((ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde_1().equals("T")) && (ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte().greater(ldaAdslcntl.getAds_Cntl_View_Ads_Cis_Intrfce_Dte()))  //Natural: ACCEPT IF ( ADS-PRTCPNT-VIEW.ADP-STTS-CDE-1 = 'T' ) AND ( ADS-PRTCPNT-VIEW.ADP-LST-ACTVTY-DTE > ADS-CNTL-VIEW.ADS-CIS-INTRFCE-DTE ) AND #PART-DATE-NUMERIC LE #TEMP-DATE-N AND ( ADS-PRTCPNT-VIEW.ADP-ANNT-TYP-CDE = 'M' )
                && pnd_Part_Date_Pnd_Part_Date_Numeric.lessOrEqual(pnd_Temp_Date_A_Pnd_Temp_Date_N) && (ldaAdsl401.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde().equals("M")))))
            {
                continue;
            }
            //* *    #PART-DATE-NUMERIC LE ADS-CNTL-VIEW.ADS-CNTL-BSNSS-DTE
            pnd_Error_Ind.reset();                                                                                                                                        //Natural: RESET #ERROR-IND
            pnd_Cis_Super.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                                                             //Natural: MOVE ADS-PRTCPNT-VIEW.RQST-ID TO #CIS-SUPER
            vw_cis_Prtcpnt_File_View.startDatabaseFind                                                                                                                    //Natural: FIND CIS-PRTCPNT-FILE-VIEW WITH CIS-RQST-ID-KEY = #CIS-SUPER
            (
            "CIS",
            new Wc[] { new Wc("CIS_RQST_ID_KEY", "=", pnd_Cis_Super, WcType.WITH) }
            );
            CIS:
            while (condition(vw_cis_Prtcpnt_File_View.readNextRow("CIS", true)))
            {
                vw_cis_Prtcpnt_File_View.setIfNotFoundControlFlag(false);
                if (condition(vw_cis_Prtcpnt_File_View.getAstCOUNTER().equals(0)))                                                                                        //Natural: IF NO RECORD FOUND
                {
                    getReports().write(0, "NO RECORDS FOUND FOR  ",pnd_Cis_Super);                                                                                        //Natural: WRITE 'NO RECORDS FOUND FOR  ' #CIS-SUPER
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("CIS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Error_Ind.setValue("Y");                                                                                                                          //Natural: MOVE 'Y' TO #ERROR-IND
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-NOREC
                pnd_Total_Processed.nadd(1);                                                                                                                              //Natural: ADD 1 TO #TOTAL-PROCESSED
                                                                                                                                                                          //Natural: PERFORM IA-RESULT-LOOKUP
                sub_Ia_Result_Lookup();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("CIS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Error_Ind.equals("Y")))                                                                                                                 //Natural: IF #ERROR-IND = 'Y'
                {
                    pnd_Total_Processed.nsubtract(1);                                                                                                                     //Natural: SUBTRACT 1 FROM #TOTAL-PROCESSED
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PART"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PART"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //* ***********************************************************************
        //* **     READ ITS CONTROL RECORD TO EXTRACT DATES                     ***
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NAZ-CONTROL
        //* ***********************************************************************
        //* ***********************************************************************
        //* **     FIND IA RESULT RECORDS FROM A PARTICIPANT RECORD             ***
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-RESULT-LOOKUP
        //*  033012 START
        //*      'TIAA/IND'          ADI-TIAA-STTLMNT
        //*        'REA/IND'           ADI-TIAA-RE-STTLMNT
        //*        'TIAA/GUAR/GRADED'  ADI-DTL-TIAA-GRD-GRNTD-AMT (#COUNTER-START)
        //*        ADI-DTL-TIAA-STD-GRNTD-AMT (#COUNTER-START)
        //*        'TIAA/GUAR/STAND'   ADI-DTL-TIAA-STD-GRNTD-AMT (#COUNTER-START)
        //*        'TIAA/GUAR/INTREST' ADI-DTL-TIAA-EFF-RTE-INTRST(#COUNTER-START)
    }
    private void sub_Naz_Control() throws Exception                                                                                                                       //Natural: NAZ-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        ldaAdslcntl.getVw_ads_Cntl_View().startDatabaseRead                                                                                                               //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "CNTRL",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        CNTRL:
        while (condition(ldaAdslcntl.getVw_ads_Cntl_View().readNextRow("CNTRL")))
        {
            if (condition(ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde().notEquals("F")))                                                                           //Natural: IF ADS-CNTL-RCRD-TYP-CDE NE 'F'
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cntrl_Total.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNTRL-TOTAL
            pnd_Cntrl_Ok.setValue("Y");                                                                                                                                   //Natural: MOVE 'Y' TO #CNTRL-OK
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Ia_Result_Lookup() throws Exception                                                                                                                  //Natural: IA-RESULT-LOOKUP
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Error_Ind.reset();                                                                                                                                            //Natural: RESET #ERROR-IND
        pnd_Nai_Super1_Pnd_Rqst_Id_Ia.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                                                 //Natural: MOVE ADS-PRTCPNT-VIEW.RQST-ID TO #RQST-ID-IA
        pnd_Nai_Super1_Pnd_Nai_Record_Type.setValue("ACT");                                                                                                               //Natural: MOVE 'ACT' TO #NAI-RECORD-TYPE
        pnd_Nai_Super1_Pnd_Nai_Sqnce_Nbr.setValue(1);                                                                                                                     //Natural: MOVE 1 TO #NAI-SQNCE-NBR
        ldaAdsl450.getVw_ads_Ia_Rslt_View().startDatabaseFind                                                                                                             //Natural: FIND ADS-IA-RSLT-VIEW WITH ADI-SUPER1 = #NAI-SUPER1
        (
        "FIND01",
        new Wc[] { new Wc("ADI_SUPER1", "=", pnd_Nai_Super1, WcType.WITH) }
        );
        FIND01:
        while (condition(ldaAdsl450.getVw_ads_Ia_Rslt_View().readNextRow("FIND01", true)))
        {
            ldaAdsl450.getVw_ads_Ia_Rslt_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAdsl450.getVw_ads_Ia_Rslt_View().getAstCOUNTER().equals(0)))                                                                                 //Natural: IF NO RECORD FOUND
            {
                pnd_Error_Ind.setValue("Y");                                                                                                                              //Natural: MOVE 'Y' TO #ERROR-IND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Ads_Ia_Rslt_Pnd_Adi_Tiaa_Rate_Cnt.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Count_Castadi_Dtl_Tiaa_Data());                                                 //Natural: ASSIGN #ADI-TIAA-RATE-CNT := ADS-IA-RSLT-VIEW.C*ADI-DTL-TIAA-DATA
            pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(1,        //Natural: ASSIGN #ADI-DTL-FNL-STD-PAY-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-FNL-STD-PAY-AMT ( 1:#MAX-RSLT )
                ":",pnd_Max_Rslt));
            pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(1,    //Natural: ASSIGN #ADI-DTL-FNL-STD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-FNL-STD-DVDND-AMT ( 1:#MAX-RSLT )
                ":",pnd_Max_Rslt));
            pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(1,        //Natural: ASSIGN #ADI-DTL-FNL-GRD-PAY-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-FNL-GRD-PAY-AMT ( 1:#MAX-RSLT )
                ":",pnd_Max_Rslt));
            pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(1,    //Natural: ASSIGN #ADI-DTL-FNL-GRD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-FNL-GRD-DVDND-AMT ( 1:#MAX-RSLT )
                ":",pnd_Max_Rslt));
            pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Acct_Cd.getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Acct_Cd().getValue(1,              //Natural: ASSIGN #ADI-DTL-TIAA-ACCT-CD ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-ACCT-CD ( 1:#MAX-RSLT )
                ":",pnd_Max_Rslt));
            pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(1,        //Natural: ASSIGN #ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-RSLT )
                ":",pnd_Max_Rslt));
            pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Eff_Rte_Intrst.getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Eff_Rte_Intrst().getValue(1, //Natural: ASSIGN #ADI-DTL-TIAA-EFF-RTE-INTRST ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-EFF-RTE-INTRST ( 1:#MAX-RSLT )
                ":",pnd_Max_Rslt));
            pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt.getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(1,        //Natural: ASSIGN #ADI-DTL-TIAA-DA-STD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-STD-AMT ( 1:#MAX-RSLT )
                ":",pnd_Max_Rslt));
            pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt.getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(1,        //Natural: ASSIGN #ADI-DTL-TIAA-DA-GRD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-GRD-AMT ( 1:#MAX-RSLT )
                ":",pnd_Max_Rslt));
            pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(1,  //Natural: ASSIGN #ADI-DTL-TIAA-GRD-GRNTD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( 1:#MAX-RSLT )
                ":",pnd_Max_Rslt));
            pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(1,  //Natural: ASSIGN #ADI-DTL-TIAA-GRD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( 1:#MAX-RSLT )
                ":",pnd_Max_Rslt));
            pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(1,  //Natural: ASSIGN #ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-RSLT )
                ":",pnd_Max_Rslt));
            pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(1,  //Natural: ASSIGN #ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-RSLT )
                ":",pnd_Max_Rslt));
            if (condition(pnd_Ads_Ia_Rslt_Pnd_Adi_Tiaa_Rate_Cnt.equals(pnd_Max_Rslt)))                                                                                    //Natural: IF #ADI-TIAA-RATE-CNT = #MAX-RSLT
            {
                pnd_Nai_Super1_Pnd_Nai_Sqnce_Nbr.setValue(2);                                                                                                             //Natural: MOVE 2 TO #NAI-SQNCE-NBR
                vw_ads_Ia_Rsl2.startDatabaseFind                                                                                                                          //Natural: FIND ADS-IA-RSL2 WITH ADI-SUPER1 = #NAI-SUPER1
                (
                "FIND02",
                new Wc[] { new Wc("ADI_SUPER1", "=", pnd_Nai_Super1, WcType.WITH) }
                );
                FIND02:
                while (condition(vw_ads_Ia_Rsl2.readNextRow("FIND02")))
                {
                    vw_ads_Ia_Rsl2.setIfNotFoundControlFlag(false);
                    pnd_Ads_Ia_Rslt_Pnd_Adi_Tiaa_Rate_Cnt.nadd(ads_Ia_Rsl2_Count_Castadi_Dtl_Tiaa_Data);                                                                  //Natural: ASSIGN #ADI-TIAA-RATE-CNT := #ADI-TIAA-RATE-CNT + ADS-IA-RSL2.C*ADI-DTL-TIAA-DATA
                    pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(1, //Natural: ASSIGN #ADI-DTL-FNL-STD-PAY-AMT ( #MAX-RSLT + 1:#MAX-RATE ) := ADS-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( 1:#MAX-RSLT )
                        ":",pnd_Max_Rslt));
                    pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(1, //Natural: ASSIGN #ADI-DTL-FNL-STD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATE ) := ADS-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( 1:#MAX-RSLT )
                        ":",pnd_Max_Rslt));
                    pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(1, //Natural: ASSIGN #ADI-DTL-FNL-GRD-PAY-AMT ( #MAX-RSLT + 1:#MAX-RATE ) := ADS-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( 1:#MAX-RSLT )
                        ":",pnd_Max_Rslt));
                    pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(1, //Natural: ASSIGN #ADI-DTL-FNL-GRD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATE ) := ADS-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( 1:#MAX-RSLT )
                        ":",pnd_Max_Rslt));
                    pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Acct_Cd.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Acct_Cd.getValue(1, //Natural: ASSIGN #ADI-DTL-TIAA-ACCT-CD ( #MAX-RSLT + 1:#MAX-RATE ) := ADS-IA-RSL2.ADI-DTL-TIAA-ACCT-CD ( 1:#MAX-RSLT )
                        ":",pnd_Max_Rslt));
                    pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(1, //Natural: ASSIGN #ADI-DTL-TIAA-IA-RATE-CD ( #MAX-RSLT + 1:#MAX-RATE ) := ADS-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-RSLT )
                        ":",pnd_Max_Rslt));
                    pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Eff_Rte_Intrst.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Eff_Rte_Intrst.getValue(1, //Natural: ASSIGN #ADI-DTL-TIAA-EFF-RTE-INTRST ( #MAX-RSLT + 1:#MAX-RATE ) := ADS-IA-RSL2.ADI-DTL-TIAA-EFF-RTE-INTRST ( 1:#MAX-RSLT )
                        ":",pnd_Max_Rslt));
                    pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt.getValue(1, //Natural: ASSIGN #ADI-DTL-TIAA-DA-STD-AMT ( #MAX-RSLT + 1:#MAX-RATE ) := ADS-IA-RSL2.ADI-DTL-TIAA-DA-STD-AMT ( 1:#MAX-RSLT )
                        ":",pnd_Max_Rslt));
                    pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt.getValue(1, //Natural: ASSIGN #ADI-DTL-TIAA-DA-GRD-AMT ( #MAX-RSLT + 1:#MAX-RATE ) := ADS-IA-RSL2.ADI-DTL-TIAA-DA-GRD-AMT ( 1:#MAX-RSLT )
                        ":",pnd_Max_Rslt));
                    pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(1, //Natural: ASSIGN #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #MAX-RSLT + 1:#MAX-RATE ) := ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( 1:#MAX-RSLT )
                        ":",pnd_Max_Rslt));
                    pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(1, //Natural: ASSIGN #ADI-DTL-TIAA-GRD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATE ) := ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( 1:#MAX-RSLT )
                        ":",pnd_Max_Rslt));
                    pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(1, //Natural: ASSIGN #ADI-DTL-TIAA-STD-GRNTD-AMT ( #MAX-RSLT + 1:#MAX-RATE ) := ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-RSLT )
                        ":",pnd_Max_Rslt));
                    pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(1, //Natural: ASSIGN #ADI-DTL-TIAA-STD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATE ) := ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-RSLT )
                        ":",pnd_Max_Rslt));
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  033012 END
            pnd_Counter_Start.reset();                                                                                                                                    //Natural: RESET #COUNTER-START
            //*  REPEAT UNTIL #COUNTER-START = C*ADI-DTL-TIAA-DATA   /* 033012
            //*  033012
            REPEAT01:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                if (condition(pnd_Counter_Start.equals(pnd_Ads_Ia_Rslt_Pnd_Adi_Tiaa_Rate_Cnt))) {break;}                                                                  //Natural: UNTIL #COUNTER-START = #ADI-TIAA-RATE-CNT
                pnd_Counter_Start.nadd(1);                                                                                                                                //Natural: ADD 1 TO #COUNTER-START
                if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Optn_Cde().equals(28) || ldaAdsl450.getAds_Ia_Rslt_View_Adi_Optn_Cde().equals(30)))                      //Natural: IF ADI-OPTN-CDE = 28 OR = 30
                {
                    pnd_Tiaa_Ind.setValue("T");                                                                                                                           //Natural: MOVE 'T' TO #TIAA-IND
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Optn_Cde().equals(25) || ldaAdsl450.getAds_Ia_Rslt_View_Adi_Optn_Cde().equals(27)))                  //Natural: IF ADI-OPTN-CDE = 25 OR = 27
                    {
                        pnd_Tiaa_Ind.setValue("I");                                                                                                                       //Natural: MOVE 'I' TO #TIAA-IND
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Tiaa_Ind.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Sttlmnt());                                                                         //Natural: MOVE ADI-TIAA-STTLMNT TO #TIAA-IND
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*    IF  ADI-DTL-TIAA-GRD-GRNTD-AMT (#COUNTER-START) > 0 OR   /* 033012
                //*        ADI-DTL-TIAA-STD-GRNTD-AMT (#COUNTER-START) > 0      /* 033012
                //*  033012
                //*  033012
                //*  RS0
                if (condition(pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Counter_Start).greater(getZero()) || pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Counter_Start).greater(getZero()))) //Natural: IF #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) > 0 OR #ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) > 0
                {
                    getReports().display(2, "UNIQUE/ID",                                                                                                                  //Natural: DISPLAY ( 02 ) 'UNIQUE/ID' ADS-PRTCPNT-VIEW.ADP-UNIQUE-ID 5X 'IA/TIAA/NUMBER' ADI-IA-TIAA-NBR 5X 'PAYMENT/MODE' ADP-PYMNT-MODE 5X 'TIAA/IND' #TIAA-IND 'REA/ACCESS/IND' ADI-TIAA-RE-STTLMNT 'CREF/IND' ADI-CREF-STTLMNT 'TIAA/GUAR/GRADED' #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) 'TIAA/GUAR/STAND/TPA-IPRO' #ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) 'TIAA/GUAR/INTREST' #ADI-DTL-TIAA-EFF-RTE-INTRST ( #COUNTER-START )
                    		ldaAdsl401.getAds_Prtcpnt_View_Adp_Unique_Id(),new ColumnSpacing(5),"IA/TIAA/NUMBER",
                    		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Tiaa_Nbr(),new ColumnSpacing(5),"PAYMENT/MODE",
                    		ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode(),new ColumnSpacing(5),"TIAA/IND",
                    		pnd_Tiaa_Ind,"REA/ACCESS/IND",
                    		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Re_Sttlmnt(),"CREF/IND",
                    		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Cref_Sttlmnt(),"TIAA/GUAR/GRADED",
                    		pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Counter_Start),"TIAA/GUAR/STAND/TPA-IPRO",
                    		pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Counter_Start),"TIAA/GUAR/INTREST",
                    		pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Eff_Rte_Intrst.getValue(pnd_Counter_Start));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*      ADD  ADI-DTL-TIAA-STD-GRNTD-AMT (#COUNTER-START) TO #SUB-TOTAL-STD
                    pnd_Sub_Total_Std.nadd(pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Counter_Start));                                                   //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) TO #SUB-TOTAL-STD
                    //*      ADD  ADI-DTL-TIAA-GRD-GRNTD-AMT (#COUNTER-START) TO #SUB-TOTAL-GRD
                    pnd_Sub_Total_Grd.nadd(pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Counter_Start));                                                   //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) TO #SUB-TOTAL-GRD
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-REPEAT
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(2, new ColumnSpacing(51),pnd_Sub_Total_Grd,pnd_Sub_Total_Std);                                                                             //Natural: WRITE ( 02 ) 51X #SUB-TOTAL-GRD #SUB-TOTAL-STD
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Sub_Total_Grd.reset();                                                                                                                                    //Natural: RESET #SUB-TOTAL-GRD #SUB-TOTAL-STD
            pnd_Sub_Total_Std.reset();
            getReports().skip(2, 1);                                                                                                                                      //Natural: SKIP ( 02 ) 1
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(64),"TIAA/CREF",NEWLINE,new ColumnSpacing(10),"RUN DATE  : ",Global.getDATU(),new  //Natural: WRITE ( 03 ) NOTITLE NOHDR 64X 'TIAA/CREF' / 10X 'RUN DATE  : ' *DATU 21X 'PENSIONS AND ANNUITIES PROJECT' 27X 'PROGRAM ID: ' *PROGRAM / 10X 'RUN TIME  : ' *TIMX 19X 'REPORT LISTING : TIAA  - INFORMATION ' 27X 'PAGE : ' *PAGE-NUMBER ( 03 ) ///
                        ColumnSpacing(21),"PENSIONS AND ANNUITIES PROJECT",new ColumnSpacing(27),"PROGRAM ID: ",Global.getPROGRAM(),NEWLINE,new ColumnSpacing(10),"RUN TIME  : ",Global.getTIMX(),new 
                        ColumnSpacing(19),"REPORT LISTING : TIAA  - INFORMATION ",new ColumnSpacing(27),"PAGE : ",getReports().getPageNumberDbs(3),NEWLINE,
                        NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=55");
        Global.format(2, "LS=132 PS=55");
        Global.format(3, "LS=132 PS=55");
        Global.format(4, "LS=132 PS=55");

        getReports().setDisplayColumns(2, "UNIQUE/ID",
        		ldaAdsl401.getAds_Prtcpnt_View_Adp_Unique_Id(),new ColumnSpacing(5),"IA/TIAA/NUMBER",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Tiaa_Nbr(),new ColumnSpacing(5),"PAYMENT/MODE",
        		ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode(),new ColumnSpacing(5),"TIAA/IND",
        		pnd_Tiaa_Ind,"REA/ACCESS/IND",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Re_Sttlmnt(),"CREF/IND",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Cref_Sttlmnt(),"TIAA/GUAR/GRADED",
        		pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt,"TIAA/GUAR/STAND/TPA-IPRO",
        		pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt,"TIAA/GUAR/INTREST",
        		pnd_Ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Eff_Rte_Intrst);
    }
}
