/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:06:14 PM
**        * FROM NATURAL PROGRAM : Adsp9992
************************************************************
**        * FILE NAME            : Adsp9992.java
**        * CLASS NAME           : Adsp9992
**        * INSTANCE NAME        : Adsp9992
************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp9992 extends BLNatBase
{
    // Data Areas
    private LdaAdsl540a ldaAdsl540a;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ads_Cntrct_View;
    private DbsField ads_Cntrct_View_Rqst_Id;
    private DbsField ads_Cntrct_View_Adc_Stts_Cde;
    private DbsField pnd_Naz_Super1;

    private DbsGroup pnd_Naz_Super1__R_Field_1;
    private DbsField pnd_Naz_Super1_Pnd_Naz_Rqst_Id;
    private DbsField pnd_Naz_Super1_Pnd_Naz_Tiaa_Nbr;
    private DbsField pnd_Rqst_Id;
    private DbsField pnd_Cnt_Count;
    private DbsField pnd_Count;
    private DbsField pnd_Et_Count;
    private DbsField pnd_Eff_Dte_N;

    private DbsGroup pnd_Eff_Dte_N__R_Field_2;
    private DbsField pnd_Eff_Dte_N_Pnd_Eff_Dte_A;
    private DbsField pnd_Lst_Upd_N;

    private DbsGroup pnd_Lst_Upd_N__R_Field_3;
    private DbsField pnd_Lst_Upd_N_Pnd_Lst_Upd_A;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl540a = new LdaAdsl540a();
        registerRecord(ldaAdsl540a);
        registerRecord(ldaAdsl540a.getVw_ads_Prtcpnt_View());

        // Local Variables
        localVariables = new DbsRecord();

        vw_ads_Cntrct_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntrct_View", "ADS-CNTRCT-VIEW"), "ADS_CNTRCT", "DS_CNTRCT");
        ads_Cntrct_View_Rqst_Id = vw_ads_Cntrct_View.getRecord().newFieldInGroup("ads_Cntrct_View_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "RQST_ID");
        ads_Cntrct_View_Adc_Stts_Cde = vw_ads_Cntrct_View.getRecord().newFieldInGroup("ads_Cntrct_View_Adc_Stts_Cde", "ADC-STTS-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "ADC_STTS_CDE");
        registerRecord(vw_ads_Cntrct_View);

        pnd_Naz_Super1 = localVariables.newFieldInRecord("pnd_Naz_Super1", "#NAZ-SUPER1", FieldType.STRING, 38);

        pnd_Naz_Super1__R_Field_1 = localVariables.newGroupInRecord("pnd_Naz_Super1__R_Field_1", "REDEFINE", pnd_Naz_Super1);
        pnd_Naz_Super1_Pnd_Naz_Rqst_Id = pnd_Naz_Super1__R_Field_1.newFieldInGroup("pnd_Naz_Super1_Pnd_Naz_Rqst_Id", "#NAZ-RQST-ID", FieldType.STRING, 
            30);
        pnd_Naz_Super1_Pnd_Naz_Tiaa_Nbr = pnd_Naz_Super1__R_Field_1.newFieldInGroup("pnd_Naz_Super1_Pnd_Naz_Tiaa_Nbr", "#NAZ-TIAA-NBR", FieldType.STRING, 
            8);
        pnd_Rqst_Id = localVariables.newFieldInRecord("pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 30);
        pnd_Cnt_Count = localVariables.newFieldInRecord("pnd_Cnt_Count", "#CNT-COUNT", FieldType.NUMERIC, 3);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.NUMERIC, 3);
        pnd_Et_Count = localVariables.newFieldInRecord("pnd_Et_Count", "#ET-COUNT", FieldType.NUMERIC, 3);
        pnd_Eff_Dte_N = localVariables.newFieldInRecord("pnd_Eff_Dte_N", "#EFF-DTE-N", FieldType.NUMERIC, 8);

        pnd_Eff_Dte_N__R_Field_2 = localVariables.newGroupInRecord("pnd_Eff_Dte_N__R_Field_2", "REDEFINE", pnd_Eff_Dte_N);
        pnd_Eff_Dte_N_Pnd_Eff_Dte_A = pnd_Eff_Dte_N__R_Field_2.newFieldInGroup("pnd_Eff_Dte_N_Pnd_Eff_Dte_A", "#EFF-DTE-A", FieldType.STRING, 8);
        pnd_Lst_Upd_N = localVariables.newFieldInRecord("pnd_Lst_Upd_N", "#LST-UPD-N", FieldType.NUMERIC, 8);

        pnd_Lst_Upd_N__R_Field_3 = localVariables.newGroupInRecord("pnd_Lst_Upd_N__R_Field_3", "REDEFINE", pnd_Lst_Upd_N);
        pnd_Lst_Upd_N_Pnd_Lst_Upd_A = pnd_Lst_Upd_N__R_Field_3.newFieldInGroup("pnd_Lst_Upd_N_Pnd_Lst_Upd_A", "#LST-UPD-A", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Cntrct_View.reset();

        ldaAdsl540a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp9992() throws Exception
    {
        super("Adsp9992");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 60 LS = 133;//Natural: AT TOP OF PAGE ( 1 )
        //*  READ ADS-PRTCPNT-VIEW BY ADP-SUPER2 STARTING FROM 'CL'
        ldaAdsl540a.getVw_ads_Prtcpnt_View().startDatabaseFind                                                                                                            //Natural: FIND ADS-PRTCPNT-VIEW WITH ADP-SUPER2 = 'CL46'
        (
        "PND_PND_L0360",
        new Wc[] { new Wc("ADP_SUPER2", "=", "CL46", WcType.WITH) }
        );
        PND_PND_L0360:
        while (condition(ldaAdsl540a.getVw_ads_Prtcpnt_View().readNextRow("PND_PND_L0360")))
        {
            ldaAdsl540a.getVw_ads_Prtcpnt_View().setIfNotFoundControlFlag(false);
            //*  IF (ADP-OPN-CLSD-IND NE 'C')
            //*      OR (SUBSTR(ADP-STTS-CDE,1,1) NE 'L')
            //*    ESCAPE BOTTOM
            //*  END-IF
            pnd_Eff_Dte_N_Pnd_Eff_Dte_A.setValueEdited(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                                  //Natural: MOVE EDITED ADP-EFFCTV-DTE ( EM = YYYYMMDD ) TO #EFF-DTE-A
            pnd_Lst_Upd_N_Pnd_Lst_Upd_A.setValueEdited(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rqst_Last_Updtd_Dte(),new ReportEditMask("YYYYMMDD"));                         //Natural: MOVE EDITED ADP-RQST-LAST-UPDTD-DTE ( EM = YYYYMMDD ) TO #LST-UPD-A
            //*  ACCEPT IF (#EFF-DTE-N = 20061231)
            //*    AND (ADP-STTS-CDE = 'L10' OR = 'L15')
            //*    AND (#LST-UPD-N = 20061231 OR = 20070101)
            //*  ADD 1 TO #COUNT
            //*  PIN EXPANSION
            //*  PIN EXPANSION
            getReports().display(1, "PRTC",                                                                                                                               //Natural: DISPLAY ( 1 ) 'PRTC' ADS-PRTCPNT-VIEW.RQST-ID ( AL = 12 ) 'CNT' ADS-CNTRCT-VIEW.RQST-ID ( AL = 12 ) 'PRTC OPN-IND' ADS-PRTCPNT-VIEW.ADP-OPN-CLSD-IND 'PRTC STTS' ADS-PRTCPNT-VIEW.ADP-STTS-CDE 'CNT STTS' ADS-CNTRCT-VIEW.ADC-STTS-CDE 'EFF-DTE' ADS-PRTCPNT-VIEW.ADP-EFFCTV-DTE ( EM = YYYYMMDD ) 'LST UPD' ADS-PRTCPNT-VIEW.ADP-RQST-LAST-UPDTD-DTE
            		ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id(), new AlphanumericLength (12),"CNT",
            		ads_Cntrct_View_Rqst_Id, new AlphanumericLength (12),"PRTC OPN-IND",
            		ldaAdsl540a.getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind(),"PRTC STTS",
            		ldaAdsl540a.getAds_Prtcpnt_View_Adp_Stts_Cde(),"CNT STTS",
            		ads_Cntrct_View_Adc_Stts_Cde,"EFF-DTE",
            		ldaAdsl540a.getAds_Prtcpnt_View_Adp_Effctv_Dte(), new ReportEditMask ("YYYYMMDD"),"LST UPD",
            		ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rqst_Last_Updtd_Dte());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L0360"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0360"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  END-READ
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  WRITE (1) / 'TOTAL NUMBER OF CASES = ' #COUNT
        getReports().write(1, NEWLINE,"TOTAL NUMBER OF CASES = ",ldaAdsl540a.getVw_ads_Prtcpnt_View().getAstCOUNTER());                                                   //Natural: WRITE ( 1 ) / 'TOTAL NUMBER OF CASES = ' *COUNTER ( ##L0360. )
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        pnd_Count.reset();                                                                                                                                                //Natural: RESET #COUNT
        //*  READ ADS-PRTCPNT-VIEW BY ADP-SUPER2 STARTING FROM 'CL'
        ldaAdsl540a.getVw_ads_Prtcpnt_View().startDatabaseFind                                                                                                            //Natural: FIND ADS-PRTCPNT-VIEW WITH ADP-SUPER2 = 'CL46'
        (
        "PND_PND_L0630",
        new Wc[] { new Wc("ADP_SUPER2", "=", "CL46", WcType.WITH) }
        );
        PND_PND_L0630:
        while (condition(ldaAdsl540a.getVw_ads_Prtcpnt_View().readNextRow("PND_PND_L0630")))
        {
            ldaAdsl540a.getVw_ads_Prtcpnt_View().setIfNotFoundControlFlag(false);
            //*  IF (ADP-OPN-CLSD-IND NE 'C')
            //*      OR (SUBSTR(ADP-STTS-CDE,1,1) NE 'L')
            //*    ESCAPE BOTTOM
            //*  END-IF
            pnd_Eff_Dte_N_Pnd_Eff_Dte_A.setValueEdited(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                                  //Natural: MOVE EDITED ADP-EFFCTV-DTE ( EM = YYYYMMDD ) TO #EFF-DTE-A
            pnd_Lst_Upd_N_Pnd_Lst_Upd_A.setValueEdited(ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rqst_Last_Updtd_Dte(),new ReportEditMask("YYYYMMDD"));                         //Natural: MOVE EDITED ADP-RQST-LAST-UPDTD-DTE ( EM = YYYYMMDD ) TO #LST-UPD-A
            //*  ACCEPT IF (#EFF-DTE-N = 20061231)
            //* *  AND (ADP-STTS-CDE = 'L10' OR = 'L15')
            //*    AND (#LST-UPD-N = 20061231 OR = 20070101)
            PND_PND_L0730:                                                                                                                                                //Natural: GET ADS-PRTCPNT-VIEW *ISN ( ##L0630. )
            ldaAdsl540a.getVw_ads_Prtcpnt_View().readByID(ldaAdsl540a.getVw_ads_Prtcpnt_View().getAstISN("PND_PND_L0630"), "PND_PND_L0730");
            ldaAdsl540a.getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind().setValue("O");                                                                                             //Natural: ASSIGN ADP-OPN-CLSD-IND := 'O'
            ldaAdsl540a.getAds_Prtcpnt_View_Adp_Stts_Cde().setValue("M10");                                                                                               //Natural: ASSIGN ADP-STTS-CDE := 'M10'
            ldaAdsl540a.getVw_ads_Prtcpnt_View().updateDBRow("PND_PND_L0730");                                                                                            //Natural: UPDATE ( ##L0730. )
            vw_ads_Cntrct_View.startDatabaseFind                                                                                                                          //Natural: FIND ADS-CNTRCT-VIEW WITH ADS-CNTRCT-VIEW.RQST-ID = ADS-PRTCPNT-VIEW.RQST-ID
            (
            "F1",
            new Wc[] { new Wc("RQST_ID", "=", ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id(), WcType.WITH) }
            );
            F1:
            while (condition(vw_ads_Cntrct_View.readNextRow("F1")))
            {
                vw_ads_Cntrct_View.setIfNotFoundControlFlag(false);
                pnd_Cnt_Count.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #CNT-COUNT
                ads_Cntrct_View_Adc_Stts_Cde.setValue("M10");                                                                                                             //Natural: ASSIGN ADC-STTS-CDE := 'M10'
                vw_ads_Cntrct_View.updateDBRow("F1");                                                                                                                     //Natural: UPDATE ( F1. )
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L0630"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0630"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Count.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #COUNT
            pnd_Et_Count.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #ET-COUNT
            if (condition(pnd_Et_Count.equals(100)))                                                                                                                      //Natural: IF #ET-COUNT = 100
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Et_Count.setValue(0);                                                                                                                                 //Natural: MOVE 0 TO #ET-COUNT
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(1, ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id(), new AlphanumericLength (12),ads_Cntrct_View_Rqst_Id, new AlphanumericLength                //Natural: DISPLAY ( 1 ) ADS-PRTCPNT-VIEW.RQST-ID ( AL = 12 ) ADS-CNTRCT-VIEW.RQST-ID ( AL = 12 ) ADS-PRTCPNT-VIEW.ADP-OPN-CLSD-IND ADS-PRTCPNT-VIEW.ADP-STTS-CDE ADS-CNTRCT-VIEW.ADC-STTS-CDE ADP-EFFCTV-DTE ( EM = YYYYMMDD )
                (12),ldaAdsl540a.getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind(),ldaAdsl540a.getAds_Prtcpnt_View_Adp_Stts_Cde(),ads_Cntrct_View_Adc_Stts_Cde,ldaAdsl540a.getAds_Prtcpnt_View_Adp_Effctv_Dte(), 
                new ReportEditMask ("YYYYMMDD"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L0630"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0630"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*     ADS-PRTCPNT-VIEW.ADP-RQST-LAST-UPDTD-DTE
            //*     ADP-ANNTY-STRT-DTE (EM=YYYYMMDD)
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  END-READ
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(1, NEWLINE,"TOTAL NUMBER OF PRTC RECORDS = ",pnd_Count);                                                                                       //Natural: WRITE ( 1 ) / 'TOTAL NUMBER OF PRTC RECORDS = ' #COUNT
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,"TOTAL NUMBER OF CNTRCT RECORDS = ",pnd_Cnt_Count);                                                                                 //Natural: WRITE ( 1 ) / 'TOTAL NUMBER OF CNTRCT RECORDS = ' #CNT-COUNT
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, Global.getPROGRAM(),"-","CHANGE STTS OF 02/29/08 REJECTED REQUESTS TO PENDING",NEWLINE,NEWLINE);                                //Natural: WRITE ( 1 ) *PROGRAM '-' 'CHANGE STTS OF 02/29/08 REJECTED REQUESTS TO PENDING' //
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133");

        getReports().setDisplayColumns(1, "PRTC",
        		ldaAdsl540a.getAds_Prtcpnt_View_Rqst_Id(), new AlphanumericLength (12),"CNT",
        		ads_Cntrct_View_Rqst_Id, new AlphanumericLength (12),"PRTC OPN-IND",
        		ldaAdsl540a.getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind(),"PRTC STTS",
        		ldaAdsl540a.getAds_Prtcpnt_View_Adp_Stts_Cde(),"CNT STTS",
        		ads_Cntrct_View_Adc_Stts_Cde,"EFF-DTE",
        		ldaAdsl540a.getAds_Prtcpnt_View_Adp_Effctv_Dte(), new ReportEditMask ("YYYYMMDD"),"LST UPD",
        		ldaAdsl540a.getAds_Prtcpnt_View_Adp_Rqst_Last_Updtd_Dte());
    }
}
