/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:02:12 PM
**        * FROM NATURAL PROGRAM : Nazp9000
************************************************************
**        * FILE NAME            : Nazp9000.java
**        * CLASS NAME           : Nazp9000
**        * INSTANCE NAME        : Nazp9000
************************************************************
************************************************************************
* PROGRAM  : NAZP9000
* FUNCTION : READ ADAM/ADAS SECURITY AND PRODUCE A FILE FOR SAS70
*            ENTITLEMENT REVIEW.
*
* HISTORY
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Nazp9000 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_adat;
    private DbsField adat_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField adat_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField adat_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField adat_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField adat_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField adat_Naz_Tbl_Rcrd_Dscrptn_Txt;

    private DbsGroup adat__R_Field_1;
    private DbsField adat_Pnd_Sys;
    private DbsField pnd_Naz_Table_Key;

    private DbsGroup pnd_Naz_Table_Key__R_Field_2;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id;

    private DataAccessProgramView vw_security;
    private DbsField security_Its_Scrty_Trn_Id;

    private DbsGroup security_Its_Scrty_Racf_Id_Redef;
    private DbsField security_Its_Scrty_Racf_Nme;
    private DbsField security_Its_Scrty_Racf_Nbr;
    private DbsField security_Its_Scrty_Usr_Unit;
    private DbsGroup security_Count_Castits_Scrty_Usr_OptnsMuGroup;
    private DbsField security_Count_Castits_Scrty_Usr_Optns;
    private DbsGroup security_Its_Scrty_Usr_OptnsMuGroup;
    private DbsField security_Its_Scrty_Usr_Optns;
    private DbsField security_Its_Cnstrct_Hold_Fld;
    private DbsField pnd_Output_File;
    private DbsField pnd_Out_Cnt;
    private DbsField pnd_Emp_Cnt;
    private DbsField pnd_A;
    private DbsField pnd_B;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_adat = new DataAccessProgramView(new NameInfo("vw_adat", "ADAT"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        adat_Naz_Tbl_Rcrd_Actv_Ind = vw_adat.getRecord().newFieldInGroup("adat_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "NAZ_TBL_RCRD_ACTV_IND");
        adat_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        adat_Naz_Tbl_Rcrd_Typ_Ind = vw_adat.getRecord().newFieldInGroup("adat_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "NAZ_TBL_RCRD_TYP_IND");
        adat_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        adat_Naz_Tbl_Rcrd_Lvl1_Id = vw_adat.getRecord().newFieldInGroup("adat_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "NAZ_TBL_RCRD_LVL1_ID");
        adat_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        adat_Naz_Tbl_Rcrd_Lvl2_Id = vw_adat.getRecord().newFieldInGroup("adat_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "NAZ_TBL_RCRD_LVL2_ID");
        adat_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        adat_Naz_Tbl_Rcrd_Lvl3_Id = vw_adat.getRecord().newFieldInGroup("adat_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "NAZ_TBL_RCRD_LVL3_ID");
        adat_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        adat_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_adat.getRecord().newFieldInGroup("adat_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        adat_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");

        adat__R_Field_1 = vw_adat.getRecord().newGroupInGroup("adat__R_Field_1", "REDEFINE", adat_Naz_Tbl_Rcrd_Dscrptn_Txt);
        adat_Pnd_Sys = adat__R_Field_1.newFieldArrayInGroup("adat_Pnd_Sys", "#SYS", FieldType.STRING, 4, new DbsArrayController(1, 10));
        registerRecord(vw_adat);

        pnd_Naz_Table_Key = localVariables.newFieldInRecord("pnd_Naz_Table_Key", "#NAZ-TABLE-KEY", FieldType.STRING, 29);

        pnd_Naz_Table_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Naz_Table_Key__R_Field_2", "REDEFINE", pnd_Naz_Table_Key);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id = pnd_Naz_Table_Key__R_Field_2.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id", "#NAZ-TABLE-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id = pnd_Naz_Table_Key__R_Field_2.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id", "#NAZ-TABLE-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id = pnd_Naz_Table_Key__R_Field_2.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id", "#NAZ-TABLE-LVL3-ID", 
            FieldType.STRING, 20);

        vw_security = new DataAccessProgramView(new NameInfo("vw_security", "SECURITY"), "ITS_USR_OPTN_SCRTY_RCRD", "ITS_USER_SECRTY");
        security_Its_Scrty_Trn_Id = vw_security.getRecord().newFieldInGroup("security_Its_Scrty_Trn_Id", "ITS-SCRTY-TRN-ID", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "ITS_SCRTY_TRN_ID");

        security_Its_Scrty_Racf_Id_Redef = vw_security.getRecord().newGroupInGroup("SECURITY_ITS_SCRTY_RACF_ID_REDEF", "ITS-SCRTY-RACF-ID-REDEF");
        security_Its_Scrty_Racf_Nme = security_Its_Scrty_Racf_Id_Redef.newFieldInGroup("security_Its_Scrty_Racf_Nme", "ITS-SCRTY-RACF-NME", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "ITS_SCRTY_RACF_NME");
        security_Its_Scrty_Racf_Nbr = security_Its_Scrty_Racf_Id_Redef.newFieldInGroup("security_Its_Scrty_Racf_Nbr", "ITS-SCRTY-RACF-NBR", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ITS_SCRTY_RACF_NBR");
        security_Its_Scrty_Usr_Unit = vw_security.getRecord().newFieldInGroup("security_Its_Scrty_Usr_Unit", "ITS-SCRTY-USR-UNIT", FieldType.STRING, 7, 
            RepeatingFieldStrategy.None, "ITS_SCRTY_USR_UNIT");
        security_Count_Castits_Scrty_Usr_Optns = vw_security.getRecord().newFieldInGroup("security_Count_Castits_Scrty_Usr_Optns", "C*ITS-SCRTY-USR-OPTNS", 
            RepeatingFieldStrategy.CAsteriskVariable, "ITS_USER_SECRTY_ITS_SCRTY_USR_OPTNS");
        security_Its_Scrty_Usr_OptnsMuGroup = vw_security.getRecord().newGroupInGroup("SECURITY_ITS_SCRTY_USR_OPTNSMuGroup", "ITS_SCRTY_USR_OPTNSMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ITS_USER_SECRTY_ITS_SCRTY_USR_OPTNS");
        security_Its_Scrty_Usr_Optns = security_Its_Scrty_Usr_OptnsMuGroup.newFieldArrayInGroup("security_Its_Scrty_Usr_Optns", "ITS-SCRTY-USR-OPTNS", 
            FieldType.STRING, 2, new DbsArrayController(1, 40), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ITS_SCRTY_USR_OPTNS");
        security_Its_Cnstrct_Hold_Fld = vw_security.getRecord().newFieldInGroup("security_Its_Cnstrct_Hold_Fld", "ITS-CNSTRCT-HOLD-FLD", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ITS_CNSTRCT_HOLD_FLD");
        registerRecord(vw_security);

        pnd_Output_File = localVariables.newFieldInRecord("pnd_Output_File", "#OUTPUT-FILE", FieldType.STRING, 40);
        pnd_Out_Cnt = localVariables.newFieldInRecord("pnd_Out_Cnt", "#OUT-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Emp_Cnt = localVariables.newFieldInRecord("pnd_Emp_Cnt", "#EMP-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_adat.reset();
        vw_security.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Nazp9000() throws Exception
    {
        super("Nazp9000");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ021");                                                                                                       //Natural: FORMAT ( 1 ) PS = 60 LS = 133;//Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ021'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("UTL");                                                                                                          //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'UTL'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id.setValue("UTL30");                                                                                                        //Natural: ASSIGN #NAZ-TABLE-LVL3-ID := 'UTL30'
        vw_adat.startDatabaseFind                                                                                                                                         //Natural: FIND ( 1 ) ADAT WITH ADAT.NAZ-TBL-SUPER1 = #NAZ-TABLE-KEY
        (
        "FIND01",
        new Wc[] { new Wc("NAZ_TBL_SUPER1", "=", pnd_Naz_Table_Key, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_adat.readNextRow("FIND01", true)))
        {
            vw_adat.setIfNotFoundControlFlag(false);
            if (condition(vw_adat.getAstCOUNTER().equals(0)))                                                                                                             //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "TABLE 30 NOT FOUND");                                                                                                              //Natural: WRITE 'TABLE 30 NOT FOUND'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(20);  if (true) return;                                                                                                                 //Natural: TERMINATE 20
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(adat_Pnd_Sys.getValue(1).equals(" ")))                                                                                                          //Natural: IF #SYS ( 1 ) = ' '
            {
                getReports().write(0, "INVALID TABLE 30 CONTENTS. PLEASE CHECK");                                                                                         //Natural: WRITE 'INVALID TABLE 30 CONTENTS. PLEASE CHECK'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(20);  if (true) return;                                                                                                                 //Natural: TERMINATE 20
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        FOR01:                                                                                                                                                            //Natural: FOR #B 1 10
        for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(10)); pnd_B.nadd(1))
        {
            if (condition(adat_Pnd_Sys.getValue(pnd_B).equals(" ")))                                                                                                      //Natural: IF #SYS ( #B ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            vw_security.startDatabaseRead                                                                                                                                 //Natural: READ SECURITY BY ITS-SCRTY-TRN-ID STARTING FROM #SYS ( #B )
            (
            "READ01",
            new Wc[] { new Wc("ITS_SCRTY_TRN_ID", ">=", adat_Pnd_Sys.getValue(pnd_B), WcType.BY) },
            new Oc[] { new Oc("ITS_SCRTY_TRN_ID", "ASC") }
            );
            READ01:
            while (condition(vw_security.readNextRow("READ01")))
            {
                if (condition(security_Its_Scrty_Trn_Id.notEquals(adat_Pnd_Sys.getValue(pnd_B))))                                                                         //Natural: IF ITS-SCRTY-TRN-ID NE #SYS ( #B )
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Emp_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #EMP-CNT
                FOR02:                                                                                                                                                    //Natural: FOR #A 1 C*ITS-SCRTY-USR-OPTNS
                for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(security_Count_Castits_Scrty_Usr_Optns)); pnd_A.nadd(1))
                {
                    if (condition(security_Its_Scrty_Usr_Optns.getValue(pnd_A).equals(" ")))                                                                              //Natural: IF ITS-SCRTY-USR-OPTNS ( #A ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Output_File.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, security_Its_Scrty_Racf_Nme, ",", security_Its_Scrty_Usr_Optns.getValue(pnd_A),  //Natural: COMPRESS ITS-SCRTY-RACF-NME ',' ITS-SCRTY-USR-OPTNS ( #A ) ',' ITS-SCRTY-TRN-ID INTO #OUTPUT-FILE LEAVING NO
                        ",", security_Its_Scrty_Trn_Id));
                    getReports().display(1, "FILE CONTENTS",                                                                                                              //Natural: DISPLAY ( 1 ) 'FILE CONTENTS' #OUTPUT-FILE
                    		pnd_Output_File);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getWorkFiles().write(1, false, pnd_Output_File);                                                                                                      //Natural: WRITE WORK FILE 1 #OUTPUT-FILE
                    pnd_Out_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #OUT-CNT
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, "TOTAL EXTRACT  :",pnd_Out_Cnt);                                                                                                            //Natural: WRITE ( 1 ) 'TOTAL EXTRACT  :' #OUT-CNT
        if (Global.isEscape()) return;
        getReports().write(1, "TOTAL EMPLOYEES:",pnd_Emp_Cnt);                                                                                                            //Natural: WRITE ( 1 ) 'TOTAL EMPLOYEES:' #EMP-CNT
        if (Global.isEscape()) return;
    }                                                                                                                                                                     //Natural: AT TOP OF PAGE ( 1 )

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, Global.getPROGRAM(),new TabSetting(20),"LIST OF ENTITLEMENTS");                                                                 //Natural: WRITE ( 1 ) *PROGRAM 20T 'LIST OF ENTITLEMENTS'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133");

        getReports().setDisplayColumns(1, "FILE CONTENTS",
        		pnd_Output_File);
    }
}
