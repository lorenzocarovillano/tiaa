/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:03:25 PM
**        * FROM NATURAL PROGRAM : Adsp595
************************************************************
**        * FILE NAME            : Adsp595.java
**        * CLASS NAME           : Adsp595
**        * INSTANCE NAME        : Adsp595
************************************************************
*******************************************************************
* THIS PROGRAM UPDATES THE DAILY CONTROL RECORD                   *
*   AFTER FINAL PREMIUM LETTER JOB RUNS SUCCESSFULLLY             *
* IT ALSO UPDATES THE PARTICIPANT RECORD TO MARK THE CASE CLOSED  *
*                                                                 *
* DATE     MODIFIED BY  DESC                                      *
* 04/13/04 H. KAKADIA   MODIFIED FOR ANNUITIZATION SUNGUARD       *
* 07/27/04   T. SHINE       IF MONTH-END PROCESS FALLS ON WEEKEND OR
*                           HOLIDAY, USE ADS-RPT-13 (FACTOR DATE) AS
*                           REPORT DATE.
* 03/17/2008 E.MELNIK   ROTH 403B/401K CHANGES.  RECOMPILED FOR
*                       NEW ADSL401.
* 7/19/2010 C. MASON    RESTOW DUE TO LDA CHANGES
* 01/18/13 E. MELNIK    TNG PROJECT.  RESTOW TO INCLUDE UPDATED LINKAGE
*                       AREAS.
* 03/23/17 E. MELNIK   RESTOWED FOR PIN EXPANSION.
*******************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp595 extends BLNatBase
{
    // Data Areas
    private LdaAdslcntl ldaAdslcntl;
    private LdaAdsl401 ldaAdsl401;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Prev_Business_Date;

    private DbsGroup pnd_Prev_Business_Date__R_Field_1;
    private DbsField pnd_Prev_Business_Date_Pnd_Prev_Business_Date_A;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_2;
    private DbsField pnd_Date_A_Pnd_Date_N;
    private DbsField pnd_Old_Fp_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdslcntl = new LdaAdslcntl();
        registerRecord(ldaAdslcntl);
        registerRecord(ldaAdslcntl.getVw_ads_Cntl_View());
        ldaAdsl401 = new LdaAdsl401();
        registerRecord(ldaAdsl401);
        registerRecord(ldaAdsl401.getVw_ads_Prtcpnt_View());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Prev_Business_Date = localVariables.newFieldInRecord("pnd_Prev_Business_Date", "#PREV-BUSINESS-DATE", FieldType.NUMERIC, 8);

        pnd_Prev_Business_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Prev_Business_Date__R_Field_1", "REDEFINE", pnd_Prev_Business_Date);
        pnd_Prev_Business_Date_Pnd_Prev_Business_Date_A = pnd_Prev_Business_Date__R_Field_1.newFieldInGroup("pnd_Prev_Business_Date_Pnd_Prev_Business_Date_A", 
            "#PREV-BUSINESS-DATE-A", FieldType.STRING, 8);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_2", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_2.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        pnd_Old_Fp_Dte = localVariables.newFieldInRecord("pnd_Old_Fp_Dte", "#OLD-FP-DTE", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAdslcntl.initializeValues();
        ldaAdsl401.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp595() throws Exception
    {
        super("Adsp595");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        ldaAdslcntl.getVw_ads_Cntl_View().startDatabaseRead                                                                                                               //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "READ01",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        READ01:
        while (condition(ldaAdslcntl.getVw_ads_Cntl_View().readNextRow("READ01")))
        {
            if (condition(ldaAdslcntl.getVw_ads_Cntl_View().getAstCOUNTER().equals(2)))                                                                                   //Natural: IF *COUNTER = 2
            {
                pnd_Old_Fp_Dte.setValue(ldaAdslcntl.getAds_Cntl_View_Ads_Fnl_Prm_Cntrl_Dte());                                                                            //Natural: ASSIGN #OLD-FP-DTE := ADS-FNL-PRM-CNTRL-DTE
                if (condition(ldaAdslcntl.getAds_Cntl_View_Ads_Rpt_13().notEquals(getZero())))                                                                            //Natural: IF ADS-RPT-13 NE 0
                {
                    pnd_Date_A.setValueEdited(ldaAdslcntl.getAds_Cntl_View_Ads_Rpt_13(),new ReportEditMask("YYYYMMDD"));                                                  //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE-A
                    pnd_Prev_Business_Date.setValue(pnd_Date_A_Pnd_Date_N);                                                                                               //Natural: MOVE #DATE-N TO #PREV-BUSINESS-DATE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Prev_Business_Date.setValue(ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Dte());                                                                   //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #PREV-BUSINESS-DATE
                }                                                                                                                                                         //Natural: END-IF
                ldaAdslcntl.getAds_Cntl_View_Ads_Fnl_Prm_Cntrl_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Prev_Business_Date_Pnd_Prev_Business_Date_A);      //Natural: MOVE EDITED #PREV-BUSINESS-DATE-A TO ADS-FNL-PRM-CNTRL-DTE ( EM = YYYYMMDD )
                ldaAdslcntl.getVw_ads_Cntl_View().updateDBRow("READ01");                                                                                                  //Natural: UPDATE
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaAdslcntl.getVw_ads_Cntl_View().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "READ02",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        1
        );
        READ02:
        while (condition(ldaAdslcntl.getVw_ads_Cntl_View().readNextRow("READ02")))
        {
            ldaAdslcntl.getAds_Cntl_View_Ads_Fnl_Prm_Cntrl_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Prev_Business_Date_Pnd_Prev_Business_Date_A);          //Natural: MOVE EDITED #PREV-BUSINESS-DATE-A TO ADS-FNL-PRM-CNTRL-DTE ( EM = YYYYMMDD )
            ldaAdslcntl.getVw_ads_Cntl_View().updateDBRow("READ02");                                                                                                      //Natural: UPDATE
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaAdsl401.getVw_ads_Prtcpnt_View().startDatabaseRead                                                                                                             //Natural: READ ADS-PRTCPNT-VIEW BY ADP-LST-ACTVTY-DTE STARTING FROM #OLD-FP-DTE
        (
        "READ03",
        new Wc[] { new Wc("ADP_LST_ACTVTY_DTE", ">=", pnd_Old_Fp_Dte, WcType.BY) },
        new Oc[] { new Oc("ADP_LST_ACTVTY_DTE", "ASC") }
        );
        READ03:
        while (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().readNextRow("READ03")))
        {
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte().greater(pnd_Prev_Business_Date)))                                                           //Natural: IF ADP-LST-ACTVTY-DTE GT #PREV-BUSINESS-DATE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde().equals("R") && ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde_1().equals("T"))))               //Natural: ACCEPT IF ADP-ANNT-TYP-CDE = 'R' AND ADP-STTS-CDE-1 = 'T'
            {
                continue;
            }
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind().setValue("C");                                                                                              //Natural: ASSIGN ADP-OPN-CLSD-IND := 'C'
            ldaAdsl401.getVw_ads_Prtcpnt_View().updateDBRow("READ03");                                                                                                    //Natural: UPDATE
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
