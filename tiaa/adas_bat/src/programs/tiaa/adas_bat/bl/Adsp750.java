/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:04:35 PM
**        * FROM NATURAL PROGRAM : Adsp750
************************************************************
**        * FILE NAME            : Adsp750.java
**        * CLASS NAME           : Adsp750
**        * INSTANCE NAME        : Adsp750
************************************************************
************************************************************************
* PROGRAM  : ADSP750
* GENERATED: APRIL 12, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : EXTRACT FOR TRANSACTION HISTORY REPORT
*
* THIS MODULE EXTRACTS ALL THE COMPLETED (CLOSED) ANNUITIZATION REQUESTS
* FROM PARTICIPANT FILE, UPTO THE CURRENT BUSINESS DATE.
*
* FOR EACH REQUEST, DATA IS ALSO EXTRACTED FROM DA-CONTRACT
* AND IA-RESULT FILES.
*
* EXTRACTED RECORDS ARE ASSIGNED A 3 DIGIT CODE FOR SORTING AND READING
* PURPOSE, AS UNDER:
*
*    100 - PARTICIPANT RECORD
*    20N - DUMMY RECORD, 3RD DIGIT INDICATES # OF DA-RESULT RECORDS
*    210 - DA-RESULT RECORDS WITH INCREMENT OF 10 UPTP 290
*    300 - IA-RESULT RECORD
*
* PARTICIPANT RECORD WITHOUT IA RSLT RECORDS WILL BE IGNORED
*
* REMARKS  : CLONED FROM NAZP750
************************  MAINTENANCE LOG ******************************
*  D A T E   PROGRAMMER     D E S C R I P T I O N
* 04/14/98   ZAFAR KHAN     CHECK ADDED TO IGNORE THE CONTRACT RECORDS
*                           WHICH ARE NOT COMPLETED. THE SUBROUTINE IS
*                           DACONTRACT-AND-DARESULT-PROCESS
* 05/22/98   ZAFAR KHAN     INTERNAL AND EXTERNAL ROLLOVER AMOUNT ADDED
*                           AFTER PARTICIPANT RECORD
* 01/27/99   ZAFAR KHAN     NAP-BPS-UNIT EXTENDED FROM 6 TO 8 BYTES
* 02/01/00   ZAFAR KHAN     CHECK ADDED TO LIMIT NUMBER OF PPG TO 15
* 01/09/01   C SCHNEIDER    WILL NOW BE WRITING TWO WORK FILES- ONE FOR
*                           TPA AND IO REQUESTS, AND THE OTHER FOR THE
*                           REMAINING SETTLEMENT OPTIONS.  BOTH WORK
*                           FILES NOW INCLUDE THE OPTION CODE, WHICH
*                           INCREASES THE WORK FILE BY 2 BYTES.  THIS
*                           OPTN-CDE WILL BE INCLUDED AS THE THIRD
*                           PARAMETER IN THE SORT IN P1125AND.
*
* 11/01      C SCHNEIDER  RESTOWED UNDER NATURAL 3.1.4
* 12/10      C SCHNEIDER  FOR IPROS,POPULATE FIRST PERIODIC PAYMENT DATE
*              WITH WHEN THE FIRST PAYMENT SHOULD HAVE PROCESSED
* 06/30      C SCHNEIDER  RE-STOWED NAZLDARS  ALLOW FOR 80 TIAA RATES
* 11/17      C SCHNEIDER  RE-STOWED NAZLIARS  ALLOW FOR 80 TIAA RATES
* 04/12/04   E MELNIK     MODIFIED FOR ADAS - ANNUITIZATION SUNGUARD
* 09/13/05   M NACHBER    REL 6.5 FOR TPA AND IPRO
*                         NO MODIFICATIONS, JUST RE-STOWED TO PICK UP
*                         NEW VERSIONS OF LDA's ADSL4*
* 03/17/2008 E.MELNIK   ROTH 403B/401K CHANGES.  RECOMPILED FOR
*                       NEW ADSL401/ADSL401A/ADSL450.
* 04/08/2009 R.SACHARNY ADD STABLE AND ACCESS FUNDS TO PROCESS(RS0)
* 7/19/2010  C. MASON   RESTOW DUE TO LDA CHANGES
* 9/25/2010  D.E.ANDER  ADDITION OF NEW DATASETS MARKED DEA
* 03/05/12   E. MELNIK RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                      UPDATED LINKAGE AREAS.
* 01/18/13   E. MELNIK TNG PROJECT.  RESTOW TO INCLUDE UPDATED LINKAGE
*                      AREAS.
* 10/15/13  E. MELNIK RECOMPILED FOR NEW ADSL401A.  CREF/REA REDESIGN
*                     CHANGES.
* 2/27/2017 R.CARREON PIN EXPANSION 02272017
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp750 extends BLNatBase
{
    // Data Areas
    private LdaAdsl401 ldaAdsl401;
    private LdaAdsl401a ldaAdsl401a;
    private LdaAdsl450 ldaAdsl450;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ads_Cntl_View;

    private DbsGroup ads_Cntl_View_Ads_Cntl_Grp;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte;
    private DbsField ads_Cntl_View_Ads_Trn_Hstry_Extrct_Rpt_Dte;
    private DbsField pnd_Cct_Cntl_Super_De_1;

    private DbsGroup pnd_Cct_Cntl_Super_De_1__R_Field_1;
    private DbsField pnd_Cct_Cntl_Super_De_1_Pnd_Cct_Cntl_Rcrd_Typ;
    private DbsField pnd_Cct_Cntl_Super_De_1_Pnd_Cct_Cntl_Bsnss_Dt;
    private DbsField pnd_Rec_1_Count;
    private DbsField pnd_Rec_2_Count;
    private DbsField pnd_Rec_3_Count;
    private DbsField pnd_Rec_4_Count;
    private DbsField pnd_Rec_Type;
    private DbsField pnd_Cnvrt_Annty_Optn;
    private DbsField pnd_Adc_Da_Gd_Cntrct_Ind;
    private DbsField pnd_Rec_Count;
    private DbsField pnd_Rec_Not_Found;
    private DbsField pnd_Adc_Super1;

    private DbsGroup pnd_Adc_Super1__R_Field_2;
    private DbsField pnd_Adc_Super1_Pnd_Adc_Rqst_Id;
    private DbsField pnd_Adc_Super1_Pnd_Adc_Sqnce_Nbr;
    private DbsField pnd_Adc_Super2_From;

    private DbsGroup pnd_Adc_Super2_From__R_Field_3;
    private DbsField pnd_Adc_Super2_From_Pnd_Adc_Rqst_Id_F;
    private DbsField pnd_Adc_Super2_From_Pnd_Adc_Sqnce_Nbr_F;
    private DbsField pnd_Adc_Super2_To;

    private DbsGroup pnd_Adc_Super2_To__R_Field_4;
    private DbsField pnd_Adc_Super2_To_Pnd_Adc_Rqst_Id_T;
    private DbsField pnd_Adc_Super2_To_Pnd_Adc_Sqnce_Nbr_T;
    private DbsField pnd_Adi_Super1_F;

    private DbsGroup pnd_Adi_Super1_F__R_Field_5;
    private DbsField pnd_Adi_Super1_F_Pnd_Adi_Rqst_Id_F;
    private DbsField pnd_Adi_Super1_F_Pnd_Adi_Rcrd_Cde_F;
    private DbsField pnd_Adi_Super1_F_Pnd_Adi_Sqnc_Nbr_F;
    private DbsField pnd_Adi_Super1_T;

    private DbsGroup pnd_Adi_Super1_T__R_Field_6;
    private DbsField pnd_Adi_Super1_T_Pnd_Adi_Rqst_Id_T;
    private DbsField pnd_Adi_Super1_T_Pnd_Adi_Rcrd_Cde_T;
    private DbsField pnd_Adi_Super1_T_Pnd_Adi_Sqnc_Nbr_T;
    private DbsField pnd_Adi_Super1_F2;

    private DbsGroup pnd_Adi_Super1_F2__R_Field_7;
    private DbsField pnd_Adi_Super1_F2_Pnd_Adi_Rqst_Id_F2;
    private DbsField pnd_Adi_Super1_F2_Pnd_Adi_Rcrd_Cde_F2;
    private DbsField pnd_Adi_Super1_F2_Pnd_Adi_Sqnc_Nbr_F2;
    private DbsField pnd_Max_Rslt;

    private DataAccessProgramView vw_ads_Ia_Rsl2;
    private DbsField pnd_I_A;
    private DbsField pnd_I_B;
    private DbsField pnd_I_C;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Value_Not_Moved;
    private DbsField pnd_Tiaa_Nbr;
    private DbsField pnd_Tiaa_Ind;
    private DbsField pnd_Cref_Nbr;
    private DbsField pnd_Cref_Ind;
    private DbsField pnd_Real_Nbr;
    private DbsField pnd_Real_Ind;
    private DbsField pnd_Adc_Ppg_Cde;
    private DbsField pnd_Adc_Ppg_Ind;
    private DbsField pnd_Tiaa_Stlmnt_Amt;
    private DbsField pnd_Cref_Stlmnt_Amt;
    private DbsField pnd_Real_Stlmnt_Amt;
    private DbsField pnd_Tiaa_Ivc_Amt;
    private DbsField pnd_Cref_Ivc_Amt;
    private DbsField pnd_Hold_Eff_Dt;
    private DbsField pnd_Work_Dt;

    private DbsGroup pnd_Work_Dt__R_Field_8;
    private DbsField pnd_Work_Dt_Pnd_Work_Dt_Cc;
    private DbsField pnd_Work_Dt_Pnd_Work_Dt_Yy;
    private DbsField pnd_Work_Dt_Pnd_Work_Dt_Mm;
    private DbsField pnd_Work_Dt_Pnd_Work_Dt_Dd;
    private DbsField pnd_Ipro_Frst_Pymnt_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl401 = new LdaAdsl401();
        registerRecord(ldaAdsl401);
        registerRecord(ldaAdsl401.getVw_ads_Prtcpnt_View());
        ldaAdsl401a = new LdaAdsl401a();
        registerRecord(ldaAdsl401a);
        registerRecord(ldaAdsl401a.getVw_ads_Cntrct_View());
        ldaAdsl450 = new LdaAdsl450();
        registerRecord(ldaAdsl450);
        registerRecord(ldaAdsl450.getVw_ads_Ia_Rslt_View());

        // Local Variables
        localVariables = new DbsRecord();

        vw_ads_Cntl_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntl_View", "ADS-CNTL-VIEW"), "ADS_CNTL", "ADS_CNTL");

        ads_Cntl_View_Ads_Cntl_Grp = vw_ads_Cntl_View.getRecord().newGroupInGroup("ADS_CNTL_VIEW_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_DTE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte", "ADS-CNTL-BSNSS-TMRRW-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_TMRRW_DTE");
        ads_Cntl_View_Ads_Trn_Hstry_Extrct_Rpt_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Trn_Hstry_Extrct_Rpt_Dte", "ADS-TRN-HSTRY-EXTRCT-RPT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADS_TRN_HSTRY_EXTRCT_RPT_DTE");
        registerRecord(vw_ads_Cntl_View);

        pnd_Cct_Cntl_Super_De_1 = localVariables.newFieldInRecord("pnd_Cct_Cntl_Super_De_1", "#CCT-CNTL-SUPER-DE-1", FieldType.STRING, 9);

        pnd_Cct_Cntl_Super_De_1__R_Field_1 = localVariables.newGroupInRecord("pnd_Cct_Cntl_Super_De_1__R_Field_1", "REDEFINE", pnd_Cct_Cntl_Super_De_1);
        pnd_Cct_Cntl_Super_De_1_Pnd_Cct_Cntl_Rcrd_Typ = pnd_Cct_Cntl_Super_De_1__R_Field_1.newFieldInGroup("pnd_Cct_Cntl_Super_De_1_Pnd_Cct_Cntl_Rcrd_Typ", 
            "#CCT-CNTL-RCRD-TYP", FieldType.STRING, 1);
        pnd_Cct_Cntl_Super_De_1_Pnd_Cct_Cntl_Bsnss_Dt = pnd_Cct_Cntl_Super_De_1__R_Field_1.newFieldInGroup("pnd_Cct_Cntl_Super_De_1_Pnd_Cct_Cntl_Bsnss_Dt", 
            "#CCT-CNTL-BSNSS-DT", FieldType.NUMERIC, 8);
        pnd_Rec_1_Count = localVariables.newFieldInRecord("pnd_Rec_1_Count", "#REC-1-COUNT", FieldType.PACKED_DECIMAL, 4);
        pnd_Rec_2_Count = localVariables.newFieldInRecord("pnd_Rec_2_Count", "#REC-2-COUNT", FieldType.PACKED_DECIMAL, 4);
        pnd_Rec_3_Count = localVariables.newFieldInRecord("pnd_Rec_3_Count", "#REC-3-COUNT", FieldType.PACKED_DECIMAL, 4);
        pnd_Rec_4_Count = localVariables.newFieldInRecord("pnd_Rec_4_Count", "#REC-4-COUNT", FieldType.PACKED_DECIMAL, 4);
        pnd_Rec_Type = localVariables.newFieldInRecord("pnd_Rec_Type", "#REC-TYPE", FieldType.NUMERIC, 3);
        pnd_Cnvrt_Annty_Optn = localVariables.newFieldInRecord("pnd_Cnvrt_Annty_Optn", "#CNVRT-ANNTY-OPTN", FieldType.STRING, 1);
        pnd_Adc_Da_Gd_Cntrct_Ind = localVariables.newFieldInRecord("pnd_Adc_Da_Gd_Cntrct_Ind", "#ADC-DA-GD-CNTRCT-IND", FieldType.BOOLEAN, 1);
        pnd_Rec_Count = localVariables.newFieldInRecord("pnd_Rec_Count", "#REC-COUNT", FieldType.NUMERIC, 1);
        pnd_Rec_Not_Found = localVariables.newFieldInRecord("pnd_Rec_Not_Found", "#REC-NOT-FOUND", FieldType.BOOLEAN, 1);
        pnd_Adc_Super1 = localVariables.newFieldInRecord("pnd_Adc_Super1", "#ADC-SUPER1", FieldType.STRING, 37);

        pnd_Adc_Super1__R_Field_2 = localVariables.newGroupInRecord("pnd_Adc_Super1__R_Field_2", "REDEFINE", pnd_Adc_Super1);
        pnd_Adc_Super1_Pnd_Adc_Rqst_Id = pnd_Adc_Super1__R_Field_2.newFieldInGroup("pnd_Adc_Super1_Pnd_Adc_Rqst_Id", "#ADC-RQST-ID", FieldType.STRING, 
            35);
        pnd_Adc_Super1_Pnd_Adc_Sqnce_Nbr = pnd_Adc_Super1__R_Field_2.newFieldInGroup("pnd_Adc_Super1_Pnd_Adc_Sqnce_Nbr", "#ADC-SQNCE-NBR", FieldType.NUMERIC, 
            2);
        pnd_Adc_Super2_From = localVariables.newFieldInRecord("pnd_Adc_Super2_From", "#ADC-SUPER2-FROM", FieldType.STRING, 37);

        pnd_Adc_Super2_From__R_Field_3 = localVariables.newGroupInRecord("pnd_Adc_Super2_From__R_Field_3", "REDEFINE", pnd_Adc_Super2_From);
        pnd_Adc_Super2_From_Pnd_Adc_Rqst_Id_F = pnd_Adc_Super2_From__R_Field_3.newFieldInGroup("pnd_Adc_Super2_From_Pnd_Adc_Rqst_Id_F", "#ADC-RQST-ID-F", 
            FieldType.STRING, 35);
        pnd_Adc_Super2_From_Pnd_Adc_Sqnce_Nbr_F = pnd_Adc_Super2_From__R_Field_3.newFieldInGroup("pnd_Adc_Super2_From_Pnd_Adc_Sqnce_Nbr_F", "#ADC-SQNCE-NBR-F", 
            FieldType.NUMERIC, 2);
        pnd_Adc_Super2_To = localVariables.newFieldInRecord("pnd_Adc_Super2_To", "#ADC-SUPER2-TO", FieldType.STRING, 37);

        pnd_Adc_Super2_To__R_Field_4 = localVariables.newGroupInRecord("pnd_Adc_Super2_To__R_Field_4", "REDEFINE", pnd_Adc_Super2_To);
        pnd_Adc_Super2_To_Pnd_Adc_Rqst_Id_T = pnd_Adc_Super2_To__R_Field_4.newFieldInGroup("pnd_Adc_Super2_To_Pnd_Adc_Rqst_Id_T", "#ADC-RQST-ID-T", FieldType.STRING, 
            35);
        pnd_Adc_Super2_To_Pnd_Adc_Sqnce_Nbr_T = pnd_Adc_Super2_To__R_Field_4.newFieldInGroup("pnd_Adc_Super2_To_Pnd_Adc_Sqnce_Nbr_T", "#ADC-SQNCE-NBR-T", 
            FieldType.NUMERIC, 2);
        pnd_Adi_Super1_F = localVariables.newFieldInRecord("pnd_Adi_Super1_F", "#ADI-SUPER1-F", FieldType.STRING, 41);

        pnd_Adi_Super1_F__R_Field_5 = localVariables.newGroupInRecord("pnd_Adi_Super1_F__R_Field_5", "REDEFINE", pnd_Adi_Super1_F);
        pnd_Adi_Super1_F_Pnd_Adi_Rqst_Id_F = pnd_Adi_Super1_F__R_Field_5.newFieldInGroup("pnd_Adi_Super1_F_Pnd_Adi_Rqst_Id_F", "#ADI-RQST-ID-F", FieldType.STRING, 
            35);
        pnd_Adi_Super1_F_Pnd_Adi_Rcrd_Cde_F = pnd_Adi_Super1_F__R_Field_5.newFieldInGroup("pnd_Adi_Super1_F_Pnd_Adi_Rcrd_Cde_F", "#ADI-RCRD-CDE-F", FieldType.STRING, 
            3);
        pnd_Adi_Super1_F_Pnd_Adi_Sqnc_Nbr_F = pnd_Adi_Super1_F__R_Field_5.newFieldInGroup("pnd_Adi_Super1_F_Pnd_Adi_Sqnc_Nbr_F", "#ADI-SQNC-NBR-F", FieldType.NUMERIC, 
            3);
        pnd_Adi_Super1_T = localVariables.newFieldInRecord("pnd_Adi_Super1_T", "#ADI-SUPER1-T", FieldType.STRING, 41);

        pnd_Adi_Super1_T__R_Field_6 = localVariables.newGroupInRecord("pnd_Adi_Super1_T__R_Field_6", "REDEFINE", pnd_Adi_Super1_T);
        pnd_Adi_Super1_T_Pnd_Adi_Rqst_Id_T = pnd_Adi_Super1_T__R_Field_6.newFieldInGroup("pnd_Adi_Super1_T_Pnd_Adi_Rqst_Id_T", "#ADI-RQST-ID-T", FieldType.STRING, 
            35);
        pnd_Adi_Super1_T_Pnd_Adi_Rcrd_Cde_T = pnd_Adi_Super1_T__R_Field_6.newFieldInGroup("pnd_Adi_Super1_T_Pnd_Adi_Rcrd_Cde_T", "#ADI-RCRD-CDE-T", FieldType.STRING, 
            3);
        pnd_Adi_Super1_T_Pnd_Adi_Sqnc_Nbr_T = pnd_Adi_Super1_T__R_Field_6.newFieldInGroup("pnd_Adi_Super1_T_Pnd_Adi_Sqnc_Nbr_T", "#ADI-SQNC-NBR-T", FieldType.NUMERIC, 
            3);
        pnd_Adi_Super1_F2 = localVariables.newFieldInRecord("pnd_Adi_Super1_F2", "#ADI-SUPER1-F2", FieldType.STRING, 41);

        pnd_Adi_Super1_F2__R_Field_7 = localVariables.newGroupInRecord("pnd_Adi_Super1_F2__R_Field_7", "REDEFINE", pnd_Adi_Super1_F2);
        pnd_Adi_Super1_F2_Pnd_Adi_Rqst_Id_F2 = pnd_Adi_Super1_F2__R_Field_7.newFieldInGroup("pnd_Adi_Super1_F2_Pnd_Adi_Rqst_Id_F2", "#ADI-RQST-ID-F2", 
            FieldType.STRING, 35);
        pnd_Adi_Super1_F2_Pnd_Adi_Rcrd_Cde_F2 = pnd_Adi_Super1_F2__R_Field_7.newFieldInGroup("pnd_Adi_Super1_F2_Pnd_Adi_Rcrd_Cde_F2", "#ADI-RCRD-CDE-F2", 
            FieldType.STRING, 3);
        pnd_Adi_Super1_F2_Pnd_Adi_Sqnc_Nbr_F2 = pnd_Adi_Super1_F2__R_Field_7.newFieldInGroup("pnd_Adi_Super1_F2_Pnd_Adi_Sqnc_Nbr_F2", "#ADI-SQNC-NBR-F2", 
            FieldType.NUMERIC, 3);
        pnd_Max_Rslt = localVariables.newFieldInRecord("pnd_Max_Rslt", "#MAX-RSLT", FieldType.PACKED_DECIMAL, 3);

        vw_ads_Ia_Rsl2 = new DataAccessProgramView(new NameInfo("vw_ads_Ia_Rsl2", "ADS-IA-RSL2"), "ADS_IA_RSLT", "ADS_IA_RSLT");
        registerRecord(vw_ads_Ia_Rsl2);

        pnd_I_A = localVariables.newFieldInRecord("pnd_I_A", "#I-A", FieldType.PACKED_DECIMAL, 2);
        pnd_I_B = localVariables.newFieldInRecord("pnd_I_B", "#I-B", FieldType.PACKED_DECIMAL, 2);
        pnd_I_C = localVariables.newFieldInRecord("pnd_I_C", "#I-C", FieldType.PACKED_DECIMAL, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 2);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 2);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.PACKED_DECIMAL, 2);
        pnd_Value_Not_Moved = localVariables.newFieldInRecord("pnd_Value_Not_Moved", "#VALUE-NOT-MOVED", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Nbr = localVariables.newFieldArrayInRecord("pnd_Tiaa_Nbr", "#TIAA-NBR", FieldType.STRING, 10, new DbsArrayController(1, 12));
        pnd_Tiaa_Ind = localVariables.newFieldArrayInRecord("pnd_Tiaa_Ind", "#TIAA-IND", FieldType.STRING, 6, new DbsArrayController(1, 12));
        pnd_Cref_Nbr = localVariables.newFieldArrayInRecord("pnd_Cref_Nbr", "#CREF-NBR", FieldType.STRING, 10, new DbsArrayController(1, 12));
        pnd_Cref_Ind = localVariables.newFieldArrayInRecord("pnd_Cref_Ind", "#CREF-IND", FieldType.STRING, 6, new DbsArrayController(1, 12));
        pnd_Real_Nbr = localVariables.newFieldArrayInRecord("pnd_Real_Nbr", "#REAL-NBR", FieldType.STRING, 10, new DbsArrayController(1, 12));
        pnd_Real_Ind = localVariables.newFieldArrayInRecord("pnd_Real_Ind", "#REAL-IND", FieldType.STRING, 6, new DbsArrayController(1, 12));
        pnd_Adc_Ppg_Cde = localVariables.newFieldArrayInRecord("pnd_Adc_Ppg_Cde", "#ADC-PPG-CDE", FieldType.STRING, 6, new DbsArrayController(1, 15));
        pnd_Adc_Ppg_Ind = localVariables.newFieldArrayInRecord("pnd_Adc_Ppg_Ind", "#ADC-PPG-IND", FieldType.STRING, 5, new DbsArrayController(1, 15));
        pnd_Tiaa_Stlmnt_Amt = localVariables.newFieldInRecord("pnd_Tiaa_Stlmnt_Amt", "#TIAA-STLMNT-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Cref_Stlmnt_Amt = localVariables.newFieldInRecord("pnd_Cref_Stlmnt_Amt", "#CREF-STLMNT-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Real_Stlmnt_Amt = localVariables.newFieldInRecord("pnd_Real_Stlmnt_Amt", "#REAL-STLMNT-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Tiaa_Ivc_Amt = localVariables.newFieldInRecord("pnd_Tiaa_Ivc_Amt", "#TIAA-IVC-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Cref_Ivc_Amt = localVariables.newFieldInRecord("pnd_Cref_Ivc_Amt", "#CREF-IVC-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Hold_Eff_Dt = localVariables.newFieldInRecord("pnd_Hold_Eff_Dt", "#HOLD-EFF-DT", FieldType.DATE);
        pnd_Work_Dt = localVariables.newFieldInRecord("pnd_Work_Dt", "#WORK-DT", FieldType.STRING, 8);

        pnd_Work_Dt__R_Field_8 = localVariables.newGroupInRecord("pnd_Work_Dt__R_Field_8", "REDEFINE", pnd_Work_Dt);
        pnd_Work_Dt_Pnd_Work_Dt_Cc = pnd_Work_Dt__R_Field_8.newFieldInGroup("pnd_Work_Dt_Pnd_Work_Dt_Cc", "#WORK-DT-CC", FieldType.NUMERIC, 2);
        pnd_Work_Dt_Pnd_Work_Dt_Yy = pnd_Work_Dt__R_Field_8.newFieldInGroup("pnd_Work_Dt_Pnd_Work_Dt_Yy", "#WORK-DT-YY", FieldType.NUMERIC, 2);
        pnd_Work_Dt_Pnd_Work_Dt_Mm = pnd_Work_Dt__R_Field_8.newFieldInGroup("pnd_Work_Dt_Pnd_Work_Dt_Mm", "#WORK-DT-MM", FieldType.NUMERIC, 2);
        pnd_Work_Dt_Pnd_Work_Dt_Dd = pnd_Work_Dt__R_Field_8.newFieldInGroup("pnd_Work_Dt_Pnd_Work_Dt_Dd", "#WORK-DT-DD", FieldType.NUMERIC, 2);
        pnd_Ipro_Frst_Pymnt_Dte = localVariables.newFieldInRecord("pnd_Ipro_Frst_Pymnt_Dte", "#IPRO-FRST-PYMNT-DTE", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Cntl_View.reset();
        vw_ads_Ia_Rsl2.reset();

        ldaAdsl401.initializeValues();
        ldaAdsl401a.initializeValues();
        ldaAdsl450.initializeValues();

        localVariables.reset();
        pnd_Adc_Da_Gd_Cntrct_Ind.setInitialValue(true);
        pnd_Rec_Not_Found.setInitialValue(true);
        pnd_Max_Rslt.setInitialValue(125);
        pnd_Value_Not_Moved.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp750() throws Exception
    {
        super("Adsp750");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        vw_ads_Cntl_View.startDatabaseRead                                                                                                                                //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "READ01",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        READ01:
        while (condition(vw_ads_Cntl_View.readNextRow("READ01")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "=",ads_Cntl_View_Ads_Trn_Hstry_Extrct_Rpt_Dte);                                                                                            //Natural: WRITE '=' ADS-TRN-HSTRY-EXTRCT-RPT-DTE
        if (Global.isEscape()) return;
        getReports().write(0, "=",ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte);                                                                                                //Natural: WRITE '=' ADS-CNTL-BSNSS-TMRRW-DTE
        if (Global.isEscape()) return;
        ldaAdsl401.getVw_ads_Prtcpnt_View().startDatabaseRead                                                                                                             //Natural: READ ADS-PRTCPNT-VIEW BY ADP-SUPER1 STARTING FROM 'O'
        (
        "READ02",
        new Wc[] { new Wc("ADP_SUPER1", ">=", "O", WcType.BY) },
        new Oc[] { new Oc("ADP_SUPER1", "ASC") }
        );
        READ02:
        while (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().readNextRow("READ02")))
        {
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind().notEquals("O")))                                                                              //Natural: IF ADP-OPN-CLSD-IND NE 'O'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,1).equals("T") && ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte().greater(ads_Cntl_View_Ads_Trn_Hstry_Extrct_Rpt_Dte)  //Natural: ACCEPT IF SUBSTR ( ADP-STTS-CDE,1,1 ) = 'T' AND ADP-LST-ACTVTY-DTE GT ADS-TRN-HSTRY-EXTRCT-RPT-DTE AND ADP-LST-ACTVTY-DTE LT ADS-CNTL-BSNSS-TMRRW-DTE
                && ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte().less(ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte))))
            {
                continue;
            }
                                                                                                                                                                          //Natural: PERFORM RESET-DATA-AREAS
            sub_Reset_Data_Areas();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM SET-UP-KEYS-FOR-ACCESS
            sub_Set_Up_Keys_For_Access();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CHECK-RQST-ON-DA-AND-IA-FILES
            sub_Check_Rqst_On_Da_And_Ia_Files();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Rec_Not_Found.getBoolean()))                                                                                                                //Natural: IF #REC-NOT-FOUND
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cnvrt_Annty_Optn.setValue("3");                                                                                                                           //Natural: MOVE '3' TO #CNVRT-ANNTY-OPTN
                                                                                                                                                                          //Natural: PERFORM DACONTRACT-AND-DARESULT-PROCESS
            sub_Dacontract_And_Daresult_Process();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM PROCESS-IA-RESULT-RECORD
            sub_Process_Ia_Result_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Rec_Type.setValue(100);                                                                                                                                   //Natural: MOVE 100 TO #REC-TYPE
            pnd_Rec_1_Count.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #REC-1-COUNT
            getWorkFiles().write(1, true, pnd_Rec_Type, ldaAdsl401.getVw_ads_Prtcpnt_View());                                                                             //Natural: WRITE WORK FILE 1 VARIABLE #REC-TYPE ADS-PRTCPNT-VIEW
            //*  DEA
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Srvvr_Ind().equals("Y")))                                                                                    //Natural: IF ADS-PRTCPNT-VIEW.ADP-SRVVR-IND = 'Y'
            {
                getWorkFiles().write(2, true, pnd_Rec_Type, ldaAdsl401.getVw_ads_Prtcpnt_View());                                                                         //Natural: WRITE WORK FILE 2 VARIABLE #REC-TYPE ADS-PRTCPNT-VIEW
                //*  DEA
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,NEWLINE,"TOTAL PARTICIPANT RECORDS WRITTEN TO OUTPUT -",pnd_Rec_1_Count);                                                           //Natural: WRITE // 'TOTAL PARTICIPANT RECORDS WRITTEN TO OUTPUT -' #REC-1-COUNT
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"TOTAL DA CONTRACT RECORDS PROCESSED         -",pnd_Rec_2_Count);                                                                   //Natural: WRITE / 'TOTAL DA CONTRACT RECORDS PROCESSED         -' #REC-2-COUNT
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"TOTAL DA RESULT   RECORDS WRITTEN TO OUTPUT -",pnd_Rec_3_Count);                                                                   //Natural: WRITE / 'TOTAL DA RESULT   RECORDS WRITTEN TO OUTPUT -' #REC-3-COUNT
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"TOTAL IA RESULT   RECORDS WRITTEN TO OUTPUT -",pnd_Rec_4_Count);                                                                   //Natural: WRITE / 'TOTAL IA RESULT   RECORDS WRITTEN TO OUTPUT -' #REC-4-COUNT
        if (Global.isEscape()) return;
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-DATA-AREAS
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-UP-KEYS-FOR-ACCESS
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-RQST-ON-DA-AND-IA-FILES
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DACONTRACT-AND-DARESULT-PROCESS
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TOTAL-SETTLEMENT-AMOUNT
        //* ******************************
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-AND-MOVE-GOOD-BAD-IND
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-AND-MOVE-TARGET-DATA
        //* ******************************
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-IA-RESULT-RECORD
        //*  040312 START
    }
    private void sub_Reset_Data_Areas() throws Exception                                                                                                                  //Natural: RESET-DATA-AREAS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        pnd_Rec_Count.reset();                                                                                                                                            //Natural: RESET #REC-COUNT #TIAA-NBR ( * ) #TIAA-IND ( * ) #CREF-NBR ( * ) #CREF-IND ( * ) #REAL-NBR ( * ) #REAL-IND ( * ) #TIAA-STLMNT-AMT #TIAA-IVC-AMT #CREF-STLMNT-AMT #CREF-IVC-AMT #REAL-STLMNT-AMT #ADC-PPG-CDE ( * ) #ADC-PPG-IND ( * ) #Y #I-A #I-B #I-C #IPRO-FRST-PYMNT-DTE
        pnd_Tiaa_Nbr.getValue("*").reset();
        pnd_Tiaa_Ind.getValue("*").reset();
        pnd_Cref_Nbr.getValue("*").reset();
        pnd_Cref_Ind.getValue("*").reset();
        pnd_Real_Nbr.getValue("*").reset();
        pnd_Real_Ind.getValue("*").reset();
        pnd_Tiaa_Stlmnt_Amt.reset();
        pnd_Tiaa_Ivc_Amt.reset();
        pnd_Cref_Stlmnt_Amt.reset();
        pnd_Cref_Ivc_Amt.reset();
        pnd_Real_Stlmnt_Amt.reset();
        pnd_Adc_Ppg_Cde.getValue("*").reset();
        pnd_Adc_Ppg_Ind.getValue("*").reset();
        pnd_Y.reset();
        pnd_I_A.reset();
        pnd_I_B.reset();
        pnd_I_C.reset();
        pnd_Ipro_Frst_Pymnt_Dte.reset();
    }
    private void sub_Set_Up_Keys_For_Access() throws Exception                                                                                                            //Natural: SET-UP-KEYS-FOR-ACCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        pnd_Adc_Super2_From_Pnd_Adc_Rqst_Id_F.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                                         //Natural: MOVE ADS-PRTCPNT-VIEW.RQST-ID TO #ADC-RQST-ID-F #ADC-RQST-ID-T #ADI-RQST-ID-F #ADI-RQST-ID-T
        pnd_Adc_Super2_To_Pnd_Adc_Rqst_Id_T.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());
        pnd_Adi_Super1_F_Pnd_Adi_Rqst_Id_F.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());
        pnd_Adi_Super1_T_Pnd_Adi_Rqst_Id_T.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());
        pnd_Rec_Type.setValue(200);                                                                                                                                       //Natural: MOVE 200 TO #REC-TYPE
        pnd_Adc_Super2_From_Pnd_Adc_Sqnce_Nbr_F.setValue(0);                                                                                                              //Natural: MOVE 0 TO #ADC-SQNCE-NBR-F
        pnd_Adc_Super2_To_Pnd_Adc_Sqnce_Nbr_T.setValue(99);                                                                                                               //Natural: MOVE 99 TO #ADC-SQNCE-NBR-T
        if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde().equals("R")))                                                                                     //Natural: IF ADP-ANNT-TYP-CDE = 'R'
        {
            pnd_Adi_Super1_F_Pnd_Adi_Rcrd_Cde_F.setValue("RS ");                                                                                                          //Natural: MOVE 'RS ' TO #ADI-RCRD-CDE-F #ADI-RCRD-CDE-T
            pnd_Adi_Super1_T_Pnd_Adi_Rcrd_Cde_T.setValue("RS ");
            pnd_Adi_Super1_F_Pnd_Adi_Sqnc_Nbr_F.setValue(12);                                                                                                             //Natural: MOVE 12 TO #ADI-SQNC-NBR-F #ADI-SQNC-NBR-T
            pnd_Adi_Super1_T_Pnd_Adi_Sqnc_Nbr_T.setValue(12);
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Adi_Super1_F_Pnd_Adi_Rcrd_Cde_F.setValue("ACT");                                                                                                          //Natural: MOVE 'ACT' TO #ADI-RCRD-CDE-F
            pnd_Adi_Super1_F_Pnd_Adi_Sqnc_Nbr_F.setValue(1);                                                                                                              //Natural: MOVE 1 TO #ADI-SQNC-NBR-F
            pnd_Adi_Super1_T_Pnd_Adi_Rcrd_Cde_T.setValue("ACT");                                                                                                          //Natural: MOVE 'ACT' TO #ADI-RCRD-CDE-T
            pnd_Adi_Super1_T_Pnd_Adi_Sqnc_Nbr_T.setValue(999);                                                                                                            //Natural: MOVE 999 TO #ADI-SQNC-NBR-T
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_Rqst_On_Da_And_Ia_Files() throws Exception                                                                                                     //Natural: CHECK-RQST-ON-DA-AND-IA-FILES
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        pnd_Rec_Not_Found.setValue(false);                                                                                                                                //Natural: MOVE FALSE TO #REC-NOT-FOUND
        ldaAdsl401a.getVw_ads_Cntrct_View().getTotalRowCount                                                                                                              //Natural: FIND NUMBER ADS-CNTRCT-VIEW WITH ADC-SUPER2 = #ADC-SUPER2-FROM THRU #ADC-SUPER2-TO
        (
        new Wc[] { new Wc("ADC_SUPER2", "<=", pnd_Adc_Super2_To, WcType.WITH) }
        );
        if (condition(ldaAdsl401a.getVw_ads_Cntrct_View().getAstNUMBER().equals(getZero())))                                                                              //Natural: IF *NUMBER ( ##L2660. ) = 0
        {
            getReports().write(0, "********* RQST-ID NOT FOUND ON CONTRACT FILE *********");                                                                              //Natural: WRITE '********* RQST-ID NOT FOUND ON CONTRACT FILE *********'
            if (Global.isEscape()) return;
            getReports().write(0, "=",pnd_Adc_Super2_From);                                                                                                               //Natural: WRITE '=' #ADC-SUPER2-FROM
            if (Global.isEscape()) return;
            getReports().write(0, "=",pnd_Adc_Super2_To);                                                                                                                 //Natural: WRITE '=' #ADC-SUPER2-TO
            if (Global.isEscape()) return;
            pnd_Rec_Not_Found.setValue(true);                                                                                                                             //Natural: MOVE TRUE TO #REC-NOT-FOUND
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl450.getVw_ads_Ia_Rslt_View().getTotalRowCount                                                                                                              //Natural: FIND NUMBER ADS-IA-RSLT-VIEW WITH ADI-SUPER1 = #ADI-SUPER1-F THRU #ADI-SUPER1-T
        (
        new Wc[] { new Wc("ADI_SUPER1", "<=", pnd_Adi_Super1_T, WcType.WITH) }
        );
        if (condition(ldaAdsl450.getVw_ads_Ia_Rslt_View().getAstNUMBER().equals(getZero())))                                                                              //Natural: IF *NUMBER ( ##L2770. ) = 0
        {
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde().equals("R")))                                                                                 //Natural: IF ADP-ANNT-TYP-CDE = 'R'
            {
                pnd_Adi_Super1_F_Pnd_Adi_Sqnc_Nbr_F.setValue(22);                                                                                                         //Natural: MOVE 22 TO #ADI-SQNC-NBR-F #ADI-SQNC-NBR-T
                pnd_Adi_Super1_T_Pnd_Adi_Sqnc_Nbr_T.setValue(22);
                ldaAdsl450.getVw_ads_Ia_Rslt_View().getTotalRowCount                                                                                                      //Natural: FIND NUMBER ADS-IA-RSLT-VIEW WITH ADI-SUPER1 = #ADI-SUPER1-F THRU #ADI-SUPER1-T
                (
                new Wc[] { new Wc("ADI_SUPER1", "<=", pnd_Adi_Super1_T, WcType.WITH) }
                );
                if (condition(ldaAdsl450.getVw_ads_Ia_Rslt_View().getAstNUMBER().equals(getZero())))                                                                      //Natural: IF *NUMBER ( ##L2840. ) = 0
                {
                    getReports().write(0, "********* RQST-ID NOT FOUND ON IA RESULT FILE **********");                                                                    //Natural: WRITE '********* RQST-ID NOT FOUND ON IA RESULT FILE **********'
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",pnd_Adi_Super1_F);                                                                                                          //Natural: WRITE '=' #ADI-SUPER1-F
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",pnd_Adi_Super1_T);                                                                                                          //Natural: WRITE '=' #ADI-SUPER1-T
                    if (Global.isEscape()) return;
                    pnd_Rec_Not_Found.setValue(true);                                                                                                                     //Natural: MOVE TRUE TO #REC-NOT-FOUND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "********* RQST-ID NOT FOUND ON IA RESULT FILE **********");                                                                        //Natural: WRITE '********* RQST-ID NOT FOUND ON IA RESULT FILE **********'
                if (Global.isEscape()) return;
                getReports().write(0, "=",pnd_Adi_Super1_F);                                                                                                              //Natural: WRITE '=' #ADI-SUPER1-F
                if (Global.isEscape()) return;
                getReports().write(0, "=",pnd_Adi_Super1_T);                                                                                                              //Natural: WRITE '=' #ADI-SUPER1-T
                if (Global.isEscape()) return;
                pnd_Rec_Not_Found.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #REC-NOT-FOUND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Dacontract_And_Daresult_Process() throws Exception                                                                                                   //Natural: DACONTRACT-AND-DARESULT-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        ldaAdsl401a.getVw_ads_Cntrct_View().startDatabaseFind                                                                                                             //Natural: FIND ADS-CNTRCT-VIEW WITH ADC-SUPER2 = #ADC-SUPER2-FROM THRU #ADC-SUPER2-TO
        (
        "FIND01",
        new Wc[] { new Wc("ADC_SUPER2", "<=", pnd_Adc_Super2_To, WcType.WITH) }
        );
        FIND01:
        while (condition(ldaAdsl401a.getVw_ads_Cntrct_View().readNextRow("FIND01")))
        {
            ldaAdsl401a.getVw_ads_Cntrct_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Stts_Cde().getSubstring(1,1).equals("T")))                                                                   //Natural: IF SUBSTRING ( ADC-STTS-CDE,1,1 ) = 'T'
            {
                pnd_Rec_2_Count.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REC-2-COUNT
                pnd_Adc_Super1_Pnd_Adc_Rqst_Id.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                                        //Natural: MOVE ADS-PRTCPNT-VIEW.RQST-ID TO #ADC-RQST-ID
                pnd_Adc_Super1_Pnd_Adc_Sqnce_Nbr.setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Sqnce_Nbr());                                                                //Natural: MOVE ADC-SQNCE-NBR TO #ADC-SQNCE-NBR
                pnd_Rec_Count.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #REC-COUNT
                pnd_Rec_3_Count.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REC-3-COUNT
                pnd_Rec_Type.nadd(10);                                                                                                                                    //Natural: ADD 10 TO #REC-TYPE
                getWorkFiles().write(1, true, pnd_Rec_Type, ldaAdsl401a.getVw_ads_Cntrct_View());                                                                         //Natural: WRITE WORK FILE 1 VARIABLE #REC-TYPE ADS-CNTRCT-VIEW
                //*  DEA
                if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Srvvr_Ind().equals("Y")))                                                                                //Natural: IF ADS-PRTCPNT-VIEW.ADP-SRVVR-IND = 'Y'
                {
                    getWorkFiles().write(2, true, pnd_Rec_Type, ldaAdsl401a.getVw_ads_Cntrct_View());                                                                     //Natural: WRITE WORK FILE 2 VARIABLE #REC-TYPE ADS-CNTRCT-VIEW
                    //*  DEA
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM TOTAL-SETTLEMENT-AMOUNT
                sub_Total_Settlement_Amount();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CHECK-AND-MOVE-GOOD-BAD-IND
                sub_Check_And_Move_Good_Bad_Ind();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CHECK-AND-MOVE-TARGET-DATA
                sub_Check_And_Move_Target_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_Rec_Type.setValue(200);                                                                                                                                       //Natural: MOVE 200 TO #REC-TYPE
        pnd_Rec_Type.nadd(pnd_Rec_Count);                                                                                                                                 //Natural: ADD #REC-COUNT TO #REC-TYPE
        getWorkFiles().write(1, true, pnd_Rec_Type, ldaAdsl401a.getAds_Cntrct_View_Rqst_Id(), pnd_Adc_Ppg_Cde.getValue("*"), pnd_Adc_Ppg_Ind.getValue("*"),               //Natural: WRITE WORK FILE 1 VARIABLE #REC-TYPE ADS-CNTRCT-VIEW.RQST-ID #ADC-PPG-CDE ( * ) #ADC-PPG-IND ( * ) #TIAA-NBR ( * ) #TIAA-IND ( * ) #TIAA-STLMNT-AMT #CREF-NBR ( * ) #CREF-IND ( * ) #CREF-STLMNT-AMT #REAL-NBR ( * ) #REAL-IND ( * ) #REAL-STLMNT-AMT
            pnd_Tiaa_Nbr.getValue("*"), pnd_Tiaa_Ind.getValue("*"), pnd_Tiaa_Stlmnt_Amt, pnd_Cref_Nbr.getValue("*"), pnd_Cref_Ind.getValue("*"), pnd_Cref_Stlmnt_Amt, 
            pnd_Real_Nbr.getValue("*"), pnd_Real_Ind.getValue("*"), pnd_Real_Stlmnt_Amt);
        //*  DEA
        if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Srvvr_Ind().equals("Y")))                                                                                        //Natural: IF ADS-PRTCPNT-VIEW.ADP-SRVVR-IND = 'Y'
        {
            getWorkFiles().write(2, true, pnd_Rec_Type, ldaAdsl401a.getAds_Cntrct_View_Rqst_Id(), pnd_Adc_Ppg_Cde.getValue("*"), pnd_Adc_Ppg_Ind.getValue("*"),           //Natural: WRITE WORK FILE 2 VARIABLE #REC-TYPE ADS-CNTRCT-VIEW.RQST-ID #ADC-PPG-CDE ( * ) #ADC-PPG-IND ( * ) #TIAA-NBR ( * ) #TIAA-IND ( * ) #TIAA-STLMNT-AMT #CREF-NBR ( * ) #CREF-IND ( * ) #CREF-STLMNT-AMT #REAL-NBR ( * ) #REAL-IND ( * ) #REAL-STLMNT-AMT
                pnd_Tiaa_Nbr.getValue("*"), pnd_Tiaa_Ind.getValue("*"), pnd_Tiaa_Stlmnt_Amt, pnd_Cref_Nbr.getValue("*"), pnd_Cref_Ind.getValue("*"), pnd_Cref_Stlmnt_Amt, 
                pnd_Real_Nbr.getValue("*"), pnd_Real_Ind.getValue("*"), pnd_Real_Stlmnt_Amt);
            //*  DEA
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Total_Settlement_Amount() throws Exception                                                                                                           //Natural: TOTAL-SETTLEMENT-AMOUNT
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #J 1 TO 20
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(20)); pnd_J.nadd(1))
        {
            //*  RS0
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_J).equals("T") || ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_J).equals("Y"))) //Natural: IF ADC-ACCT-CDE ( #J ) = 'T' OR ADC-ACCT-CDE ( #J ) = 'Y'
            {
                pnd_Tiaa_Stlmnt_Amt.nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_J));                                                             //Natural: ADD ADC-ACCT-ACTL-AMT ( #J ) TO #TIAA-STLMNT-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* RS0
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_J).equals("R") || ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_J).equals("D"))) //Natural: IF ADC-ACCT-CDE ( #J ) = 'R' OR ADC-ACCT-CDE ( #J ) = 'D'
                {
                    pnd_Real_Stlmnt_Amt.nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_J));                                                         //Natural: ADD ADC-ACCT-ACTL-AMT ( #J ) TO #REAL-STLMNT-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cref_Stlmnt_Amt.nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_J));                                                         //Natural: ADD ADC-ACCT-ACTL-AMT ( #J ) TO #CREF-STLMNT-AMT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Check_And_Move_Good_Bad_Ind() throws Exception                                                                                                       //Natural: CHECK-AND-MOVE-GOOD-BAD-IND
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        pnd_Value_Not_Moved.setValue(true);                                                                                                                               //Natural: MOVE TRUE TO #VALUE-NOT-MOVED
        FOR02:                                                                                                                                                            //Natural: FOR #K 1 TO 20
        for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(20)); pnd_K.nadd(1))
        {
            //*  RS0
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_K).equals("T") || ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_K).equals("Y"))) //Natural: IF ADC-ACCT-CDE ( #K ) = 'T' OR ADC-ACCT-CDE ( #K ) = 'Y'
            {
                pnd_I_A.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #I-A
                pnd_Tiaa_Nbr.getValue(pnd_I_A).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr());                                                                   //Natural: MOVE ADC-TIAA-NBR TO #TIAA-NBR ( #I-A )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* RS0
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_K).equals("R") || ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_K).equals("D"))) //Natural: IF ADC-ACCT-CDE ( #K ) = 'R' OR ADC-ACCT-CDE ( #K ) = 'D'
                {
                    pnd_I_B.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #I-B
                    pnd_Real_Nbr.getValue(pnd_I_B).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr());                                                               //Natural: MOVE ADC-TIAA-NBR TO #REAL-NBR ( #I-B )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_K).notEquals(" ")))                                                          //Natural: IF ADC-ACCT-CDE ( #K ) NE ' '
                    {
                        if (condition(pnd_Value_Not_Moved.getBoolean()))                                                                                                  //Natural: IF #VALUE-NOT-MOVED
                        {
                            pnd_Value_Not_Moved.setValue(false);                                                                                                          //Natural: MOVE FALSE TO #VALUE-NOT-MOVED
                            pnd_I_C.nadd(1);                                                                                                                              //Natural: ADD 1 TO #I-C
                            pnd_Cref_Nbr.getValue(pnd_I_C).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Cref_Nbr());                                                       //Natural: MOVE ADC-CREF-NBR TO #CREF-NBR ( #I-C )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Check_And_Move_Target_Data() throws Exception                                                                                                        //Natural: CHECK-AND-MOVE-TARGET-DATA
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #X 1 TO 15
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(15)); pnd_X.nadd(1))
        {
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Ppg_Cde().getValue(pnd_X).notEquals(" ")))                                                                   //Natural: IF ADC-PPG-CDE ( #X ) NE ' '
            {
                pnd_Y.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #Y
                //*  02/01/00
                if (condition(pnd_Y.less(16)))                                                                                                                            //Natural: IF #Y < 16
                {
                    pnd_Adc_Ppg_Cde.getValue(pnd_Y).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Ppg_Cde().getValue(pnd_X));                                               //Natural: MOVE ADC-PPG-CDE ( #X ) TO #ADC-PPG-CDE ( #Y )
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Ppg_Trgtd().getValue(pnd_X).equals("S")))                                                            //Natural: IF ADC-PPG-TRGTD ( #X ) = 'S'
                    {
                        pnd_Adc_Ppg_Ind.getValue(pnd_Y).setValue("(YES)");                                                                                                //Natural: MOVE '(YES)' TO #ADC-PPG-IND ( #Y )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Adc_Ppg_Ind.getValue(pnd_Y).setValue("(NO) ");                                                                                                //Natural: MOVE '(NO) ' TO #ADC-PPG-IND ( #Y )
                    }                                                                                                                                                     //Natural: END-IF
                    //*  02/01/00
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, "************* FOLLOWING PPG WERE IGNORED (TOTAL>15)","*************");                                                         //Natural: WRITE '************* FOLLOWING PPG WERE IGNORED (TOTAL>15)' '*************'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",pnd_Rec_Type);                                                                                                              //Natural: WRITE '=' #REC-TYPE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl401a.getAds_Cntrct_View_Rqst_Id());                                                                                  //Natural: WRITE '=' ADS-CNTRCT-VIEW.RQST-ID
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",pnd_Tiaa_Nbr.getValue("*"));                                                                                                //Natural: WRITE '=' #TIAA-NBR ( * )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl401a.getAds_Cntrct_View_Adc_Ppg_Cde().getValue(pnd_X));                                                              //Natural: WRITE '=' ADC-PPG-CDE ( #X )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Process_Ia_Result_Record() throws Exception                                                                                                          //Natural: PROCESS-IA-RESULT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        ldaAdsl450.getVw_ads_Ia_Rslt_View().startDatabaseFind                                                                                                             //Natural: FIND ( 1 ) ADS-IA-RSLT-VIEW WITH ADI-SUPER1 = #ADI-SUPER1-F THRU #ADI-SUPER1-T
        (
        "FIND02",
        new Wc[] { new Wc("ADI_SUPER1", "<=", pnd_Adi_Super1_T, WcType.WITH) },
        1
        );
        FIND02:
        while (condition(ldaAdsl450.getVw_ads_Ia_Rslt_View().readNextRow("FIND02")))
        {
            ldaAdsl450.getVw_ads_Ia_Rslt_View().setIfNotFoundControlFlag(false);
            ldaAdsl450.getPnd_More().setValue("N");                                                                                                                       //Natural: ASSIGN #MORE := 'N'
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Count_Castadi_Dtl_Tiaa_Data().equals(pnd_Max_Rslt)))                                                             //Natural: IF ADS-IA-RSLT-VIEW.C*ADI-DTL-TIAA-DATA = #MAX-RSLT
            {
                pnd_Adi_Super1_F2.setValue(pnd_Adi_Super1_F);                                                                                                             //Natural: ASSIGN #ADI-SUPER1-F2 := #ADI-SUPER1-F
                if (condition(pnd_Adi_Super1_F_Pnd_Adi_Rcrd_Cde_F.equals("ACT")))                                                                                         //Natural: IF #ADI-RCRD-CDE-F = 'ACT'
                {
                    pnd_Adi_Super1_F2_Pnd_Adi_Sqnc_Nbr_F2.setValue(2);                                                                                                    //Natural: ASSIGN #ADI-SQNC-NBR-F2 := 2
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Adi_Super1_F2_Pnd_Adi_Rcrd_Cde_F2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Adi_Super1_F_Pnd_Adi_Rcrd_Cde_F,                   //Natural: COMPRESS #ADI-RCRD-CDE-F '1' INTO #ADI-RCRD-CDE-F2 LEAVING NO
                        "1"));
                }                                                                                                                                                         //Natural: END-IF
                vw_ads_Ia_Rsl2.startDatabaseFind                                                                                                                          //Natural: FIND ADS-IA-RSL2 WITH ADI-SUPER1 = #ADI-SUPER1-F2
                (
                "FIND03",
                new Wc[] { new Wc("ADI_SUPER1", "=", pnd_Adi_Super1_F2, WcType.WITH) }
                );
                FIND03:
                while (condition(vw_ads_Ia_Rsl2.readNextRow("FIND03")))
                {
                    vw_ads_Ia_Rsl2.setIfNotFoundControlFlag(false);
                    ldaAdsl450.getPnd_More().setValue("Y");                                                                                                               //Natural: ASSIGN #MORE := 'Y'
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  040312 END
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Crrspndnce_Addr_Lst_Chg_Dte().setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Finl_Periodic_Py_Dte());                         //Natural: MOVE ADI-FINL-PERIODIC-PY-DTE TO ADP-CRRSPNDNCE-ADDR-LST-CHG-DTE
            pnd_Rec_4_Count.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #REC-4-COUNT
            pnd_Rec_Type.setValue(300);                                                                                                                                   //Natural: MOVE 300 TO #REC-TYPE
            //*  040312
            getWorkFiles().write(1, true, pnd_Rec_Type, ldaAdsl450.getVw_ads_Ia_Rslt_View(), ldaAdsl450.getPnd_More());                                                   //Natural: WRITE WORK FILE 1 VARIABLE #REC-TYPE ADS-IA-RSLT-VIEW #MORE
            //*  DEA
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Srvvr_Ind().equals("Y")))                                                                                    //Natural: IF ADS-PRTCPNT-VIEW.ADP-SRVVR-IND = 'Y'
            {
                //*  040312
                getWorkFiles().write(2, true, pnd_Rec_Type, ldaAdsl450.getVw_ads_Ia_Rslt_View(), ldaAdsl450.getPnd_More());                                               //Natural: WRITE WORK FILE 2 VARIABLE #REC-TYPE ADS-IA-RSLT-VIEW #MORE
                //*  DEA
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //
}
