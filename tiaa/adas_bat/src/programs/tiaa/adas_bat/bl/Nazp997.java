/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:02:19 PM
**        * FROM NATURAL PROGRAM : Nazp997
************************************************************
**        * FILE NAME            : Nazp997.java
**        * CLASS NAME           : Nazp997
**        * INSTANCE NAME        : Nazp997
************************************************************
************************************************************************
* PROGRAM: NAZP997
* DESCRIPTION: BRIDGE PROGRAM EXECUTED IN P9997ANR.
* HOW TO USE: CREATE OWN MODULE, PUT THE NAME IN THE #CALLED-PGM, SUBMIT
*             P9997ANR. THIS WAY, YOU CAN PRESERVE THE UTILITY MODULE
*             YOU CREATED THAT MAY BE USEFUL IN THE FUTURE. WHEN
*             REPLACING THE #CALLED-PGM, DON'T JUST OVERLAY THE VALUE,
*             DUP IT, COMMENT-OUT THE EXISTING ONE THEN CHANGE THE
*             VALUE FOR THE DUPED. THIS WAY, AN AUDIT TRAIL OF ALL THE
*             UTILITY MODULES ARE KEPT. THANK YOU FOR YOUR COOPERATION.
* 08/23/2011  O SOTTO  USE ADAT TABLE NAZ021UTLUTL19 TO GET THE
*                      MODULE TO BE EXECUTED.
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Nazp997 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;

    private DbsGroup naz_Table_Ddm__R_Field_1;
    private DbsField naz_Table_Ddm_Pnd_Called_Pgm;
    private DbsField pnd_Naz_Table_Key;

    private DbsGroup pnd_Naz_Table_Key__R_Field_2;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");

        naz_Table_Ddm__R_Field_1 = vw_naz_Table_Ddm.getRecord().newGroupInGroup("naz_Table_Ddm__R_Field_1", "REDEFINE", naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt);
        naz_Table_Ddm_Pnd_Called_Pgm = naz_Table_Ddm__R_Field_1.newFieldInGroup("naz_Table_Ddm_Pnd_Called_Pgm", "#CALLED-PGM", FieldType.STRING, 8);
        registerRecord(vw_naz_Table_Ddm);

        pnd_Naz_Table_Key = localVariables.newFieldInRecord("pnd_Naz_Table_Key", "#NAZ-TABLE-KEY", FieldType.STRING, 30);

        pnd_Naz_Table_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Naz_Table_Key__R_Field_2", "REDEFINE", pnd_Naz_Table_Key);
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind = pnd_Naz_Table_Key__R_Field_2.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind", "#NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id = pnd_Naz_Table_Key__R_Field_2.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id", "#NAZ-TABLE-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id = pnd_Naz_Table_Key__R_Field_2.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id", "#NAZ-TABLE-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id = pnd_Naz_Table_Key__R_Field_2.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id", "#NAZ-TABLE-LVL3-ID", 
            FieldType.STRING, 20);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_naz_Table_Ddm.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Nazp997() throws Exception
    {
        super("Nazp997");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                         //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ021");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ021'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("UTL");                                                                                                          //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'UTL'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id.setValue("UTL19");                                                                                                        //Natural: ASSIGN #NAZ-TABLE-LVL3-ID := 'UTL19'
        vw_naz_Table_Ddm.startDatabaseFind                                                                                                                                //Natural: FIND NAZ-TABLE-DDM WITH NAZ-TBL-SUPER3 = #NAZ-TABLE-KEY
        (
        "FIND01",
        new Wc[] { new Wc("NAZ_TBL_SUPER3", "=", pnd_Naz_Table_Key, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_naz_Table_Ddm.readNextRow("FIND01", true)))
        {
            vw_naz_Table_Ddm.setIfNotFoundControlFlag(false);
            if (condition(vw_naz_Table_Ddm.getAstCOUNTER().equals(0)))                                                                                                    //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "NO RECORDS FOUND FOR",pnd_Naz_Table_Key);                                                                                          //Natural: WRITE 'NO RECORDS FOUND FOR' #NAZ-TABLE-KEY
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(99);  if (true) return;                                                                                                                 //Natural: TERMINATE 99
            }                                                                                                                                                             //Natural: END-NOREC
            DbsUtil.invokeMain(DbsUtil.getBlType(naz_Table_Ddm_Pnd_Called_Pgm), getCurrentProcessState());                                                                //Natural: FETCH RETURN #CALLED-PGM
            if (condition(Global.isEscape())) return;
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //
}
