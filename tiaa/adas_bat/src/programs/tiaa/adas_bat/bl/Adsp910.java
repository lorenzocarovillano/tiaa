/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:05:33 PM
**        * FROM NATURAL PROGRAM : Adsp910
************************************************************
**        * FILE NAME            : Adsp910.java
**        * CLASS NAME           : Adsp910
**        * INSTANCE NAME        : Adsp910
************************************************************
************************************************************************
* PROGRAM  : ADSP910
* GENERATED: APRIL 02, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : THIS MODULE CREATES THE ADS CONTROL FILE RECORDS
*
* REMARKS  : CLONED FROM NAZP910
************************  MAINTENANCE LOG ******************************
*  D A T E   PROGRAMMER     D E S C R I P T I O N
* 08/02/2000 O. SOTTO    COMMENTED-OUT ESCAPE BOTTOM IN ON ERROR ROUTINE
*                        TO COMPLY WITH NATURAL 3.
* 12/04/2002 J. VLISMAS  CHANGED CALL FROM FCPN110 TO CPWN110 TO
*                        ACCOMMODATE CALL TO NEW EXTERNILIZATIN MODULE
*                        NECN4000.
* 04/02/2004 E. MELNIK   MODIFIED FOR ADAS (ANNUITIZATION SUNGUARD)
* 04/16/2004 C. AVE      CHANGED HEADER
* 07/21/2004 VILLANUEVA  ADD LOGIC TO STORE MONTH-END DATE FROM FACTOR
*                        FILE IN ADAS CONTROL FILE ONLY IF MONTH-END
*                        DATE FALLS ON WEEKEND OR HOLIDAY.
* 05/19/2011 E. MELNIK   MEOW CHANGES.  MARKED BY EM - 051911.
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp910 extends BLNatBase
{
    // Data Areas
    private PdaCpwa115 pdaCpwa115;
    private LdaAdslcntl ldaAdslcntl;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsGroup naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup;
    private DbsField naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt;
    private DbsField pnd_Nmbr_Dly_Recs_In;
    private DbsField pnd_Nmbr_Dly_Recs_Ot;
    private DbsField pnd_Convert_Date;

    private DbsGroup pnd_Convert_Date__R_Field_1;
    private DbsField pnd_Convert_Date_Pnd_Convert_Date_Cy;
    private DbsField pnd_Convert_Date_Pnd_Convert_Date_Mm;
    private DbsField pnd_Convert_Date_Pnd_Convert_Date_Dd;
    private DbsField pnd_Factor_Date;

    private DbsGroup pnd_Factor_Date__R_Field_2;
    private DbsField pnd_Factor_Date_Pnd_Factor_Date_Cy;
    private DbsField pnd_Factor_Date_Pnd_Factor_Date_Mm;
    private DbsField pnd_Factor_Date_Pnd_Factor_Date_Dd;

    private DbsGroup pnd_Factor_Date__R_Field_3;
    private DbsField pnd_Factor_Date_Pnd_Factor_Date_A;
    private DbsField pnd_Factor_Date_D;
    private DbsField pnd_Compute_Date_D;
    private DbsField pnd_Compute_Date_A;

    private DbsGroup pnd_Compute_Date_A__R_Field_4;
    private DbsField pnd_Compute_Date_A_Pnd_Compute_Date_Cy;
    private DbsField pnd_Compute_Date_A_Pnd_Compute_Date_Mm;
    private DbsField pnd_Compute_Date_A_Pnd_Compute_Date_Dd;
    private DbsField pnd_Day_Of_Week;
    private DbsField pnd_Naz_Table_Super3;

    private DbsGroup pnd_Naz_Table_Super3__R_Field_5;
    private DbsField pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField pnd_Dly_Mth_Cmpre_Dte;

    private DbsGroup pnd_Dly_Mth_Cmpre_Dte__R_Field_6;
    private DbsField pnd_Dly_Mth_Cmpre_Dte_Pnd_Dly_Mth_Cmpre_Dte_Cy;
    private DbsField pnd_Dly_Mth_Cmpre_Dte_Pnd_Dly_Mth_Cmpre_Dte_Mm;
    private DbsField pnd_Dly_Mth_Cmpre_Dte_Pnd_Dly_Mth_Cmpre_Dte_Dd;
    private DbsField pnd_Prntline_Dmy_Ltrl;

    private DbsGroup pnd_Prntline_Prior_Bsnss_Dte;
    private DbsField pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Prior_Bsnss_Dte_M;
    private DbsField pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Slash_1;
    private DbsField pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Prior_Bsnss_Dte_D;
    private DbsField pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Slash_2;
    private DbsField pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Prior_Bsnss_Dte_Cy;

    private DbsGroup pnd_Prntline_New_Bsnss_Dte;
    private DbsField pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_M;
    private DbsField pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_Slash_3;
    private DbsField pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_D;
    private DbsField pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_Slash_4;
    private DbsField pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_Cy;

    private DbsGroup pnd_Prntline_Prior_Nxt_Bsnss_Dte;
    private DbsField pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Prior_Nxt_Bsnss_Dte_M;
    private DbsField pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Slash_5;
    private DbsField pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Prior_Nxt_Bsnss_Dte_D;
    private DbsField pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Slash_6;
    private DbsField pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Prior_Nxt_Bsnss_Dte_Cy;

    private DbsGroup pnd_Prntline_New_Nxt_Bsnss_Dte;
    private DbsField pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_New_Nxt_Bsnss_Dte_M;
    private DbsField pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_Slash_7;
    private DbsField pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_New_Nxt_Bsnss_Dte_D;
    private DbsField pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_Slash_8;
    private DbsField pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_New_Nxt_Bsnss_Dte_Cy;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCpwa115 = new PdaCpwa115(localVariables);
        ldaAdslcntl = new LdaAdslcntl();
        registerRecord(ldaAdslcntl);
        registerRecord(ldaAdslcntl.getVw_ads_Cntl_View());

        // Local Variables

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup = vw_naz_Table_Ddm.getRecord().newGroupInGroup("NAZ_TABLE_DDM_NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", 
            "NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt = naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup.newFieldArrayInGroup("naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt", 
            "NAZ-TBL-SECNDRY-DSCRPTN-TXT", FieldType.STRING, 80, new DbsArrayController(1, 100), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt.setDdmHeader("CDE/2ND/DSC");
        registerRecord(vw_naz_Table_Ddm);

        pnd_Nmbr_Dly_Recs_In = localVariables.newFieldInRecord("pnd_Nmbr_Dly_Recs_In", "#NMBR-DLY-RECS-IN", FieldType.PACKED_DECIMAL, 3);
        pnd_Nmbr_Dly_Recs_Ot = localVariables.newFieldInRecord("pnd_Nmbr_Dly_Recs_Ot", "#NMBR-DLY-RECS-OT", FieldType.PACKED_DECIMAL, 3);
        pnd_Convert_Date = localVariables.newFieldInRecord("pnd_Convert_Date", "#CONVERT-DATE", FieldType.NUMERIC, 8);

        pnd_Convert_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Convert_Date__R_Field_1", "REDEFINE", pnd_Convert_Date);
        pnd_Convert_Date_Pnd_Convert_Date_Cy = pnd_Convert_Date__R_Field_1.newFieldInGroup("pnd_Convert_Date_Pnd_Convert_Date_Cy", "#CONVERT-DATE-CY", 
            FieldType.NUMERIC, 4);
        pnd_Convert_Date_Pnd_Convert_Date_Mm = pnd_Convert_Date__R_Field_1.newFieldInGroup("pnd_Convert_Date_Pnd_Convert_Date_Mm", "#CONVERT-DATE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Convert_Date_Pnd_Convert_Date_Dd = pnd_Convert_Date__R_Field_1.newFieldInGroup("pnd_Convert_Date_Pnd_Convert_Date_Dd", "#CONVERT-DATE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Factor_Date = localVariables.newFieldInRecord("pnd_Factor_Date", "#FACTOR-DATE", FieldType.NUMERIC, 8);

        pnd_Factor_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Factor_Date__R_Field_2", "REDEFINE", pnd_Factor_Date);
        pnd_Factor_Date_Pnd_Factor_Date_Cy = pnd_Factor_Date__R_Field_2.newFieldInGroup("pnd_Factor_Date_Pnd_Factor_Date_Cy", "#FACTOR-DATE-CY", FieldType.NUMERIC, 
            4);
        pnd_Factor_Date_Pnd_Factor_Date_Mm = pnd_Factor_Date__R_Field_2.newFieldInGroup("pnd_Factor_Date_Pnd_Factor_Date_Mm", "#FACTOR-DATE-MM", FieldType.NUMERIC, 
            2);
        pnd_Factor_Date_Pnd_Factor_Date_Dd = pnd_Factor_Date__R_Field_2.newFieldInGroup("pnd_Factor_Date_Pnd_Factor_Date_Dd", "#FACTOR-DATE-DD", FieldType.NUMERIC, 
            2);

        pnd_Factor_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Factor_Date__R_Field_3", "REDEFINE", pnd_Factor_Date);
        pnd_Factor_Date_Pnd_Factor_Date_A = pnd_Factor_Date__R_Field_3.newFieldInGroup("pnd_Factor_Date_Pnd_Factor_Date_A", "#FACTOR-DATE-A", FieldType.STRING, 
            8);
        pnd_Factor_Date_D = localVariables.newFieldInRecord("pnd_Factor_Date_D", "#FACTOR-DATE-D", FieldType.DATE);
        pnd_Compute_Date_D = localVariables.newFieldInRecord("pnd_Compute_Date_D", "#COMPUTE-DATE-D", FieldType.DATE);
        pnd_Compute_Date_A = localVariables.newFieldInRecord("pnd_Compute_Date_A", "#COMPUTE-DATE-A", FieldType.STRING, 8);

        pnd_Compute_Date_A__R_Field_4 = localVariables.newGroupInRecord("pnd_Compute_Date_A__R_Field_4", "REDEFINE", pnd_Compute_Date_A);
        pnd_Compute_Date_A_Pnd_Compute_Date_Cy = pnd_Compute_Date_A__R_Field_4.newFieldInGroup("pnd_Compute_Date_A_Pnd_Compute_Date_Cy", "#COMPUTE-DATE-CY", 
            FieldType.NUMERIC, 4);
        pnd_Compute_Date_A_Pnd_Compute_Date_Mm = pnd_Compute_Date_A__R_Field_4.newFieldInGroup("pnd_Compute_Date_A_Pnd_Compute_Date_Mm", "#COMPUTE-DATE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Compute_Date_A_Pnd_Compute_Date_Dd = pnd_Compute_Date_A__R_Field_4.newFieldInGroup("pnd_Compute_Date_A_Pnd_Compute_Date_Dd", "#COMPUTE-DATE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Day_Of_Week = localVariables.newFieldInRecord("pnd_Day_Of_Week", "#DAY-OF-WEEK", FieldType.STRING, 9);
        pnd_Naz_Table_Super3 = localVariables.newFieldInRecord("pnd_Naz_Table_Super3", "#NAZ-TABLE-SUPER3", FieldType.STRING, 30);

        pnd_Naz_Table_Super3__R_Field_5 = localVariables.newGroupInRecord("pnd_Naz_Table_Super3__R_Field_5", "REDEFINE", pnd_Naz_Table_Super3);
        pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Typ_Ind = pnd_Naz_Table_Super3__R_Field_5.newFieldInGroup("pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Typ_Ind", 
            "#NAZ-TBL-RCRD-TYP-IND", FieldType.STRING, 1);
        pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl1_Id = pnd_Naz_Table_Super3__R_Field_5.newFieldInGroup("pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl1_Id", 
            "#NAZ-TBL-RCRD-LVL1-ID", FieldType.STRING, 6);
        pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl2_Id = pnd_Naz_Table_Super3__R_Field_5.newFieldInGroup("pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl2_Id", 
            "#NAZ-TBL-RCRD-LVL2-ID", FieldType.STRING, 3);
        pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl3_Id = pnd_Naz_Table_Super3__R_Field_5.newFieldInGroup("pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl3_Id", 
            "#NAZ-TBL-RCRD-LVL3-ID", FieldType.STRING, 20);
        pnd_Dly_Mth_Cmpre_Dte = localVariables.newFieldInRecord("pnd_Dly_Mth_Cmpre_Dte", "#DLY-MTH-CMPRE-DTE", FieldType.NUMERIC, 8);

        pnd_Dly_Mth_Cmpre_Dte__R_Field_6 = localVariables.newGroupInRecord("pnd_Dly_Mth_Cmpre_Dte__R_Field_6", "REDEFINE", pnd_Dly_Mth_Cmpre_Dte);
        pnd_Dly_Mth_Cmpre_Dte_Pnd_Dly_Mth_Cmpre_Dte_Cy = pnd_Dly_Mth_Cmpre_Dte__R_Field_6.newFieldInGroup("pnd_Dly_Mth_Cmpre_Dte_Pnd_Dly_Mth_Cmpre_Dte_Cy", 
            "#DLY-MTH-CMPRE-DTE-CY", FieldType.NUMERIC, 4);
        pnd_Dly_Mth_Cmpre_Dte_Pnd_Dly_Mth_Cmpre_Dte_Mm = pnd_Dly_Mth_Cmpre_Dte__R_Field_6.newFieldInGroup("pnd_Dly_Mth_Cmpre_Dte_Pnd_Dly_Mth_Cmpre_Dte_Mm", 
            "#DLY-MTH-CMPRE-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Dly_Mth_Cmpre_Dte_Pnd_Dly_Mth_Cmpre_Dte_Dd = pnd_Dly_Mth_Cmpre_Dte__R_Field_6.newFieldInGroup("pnd_Dly_Mth_Cmpre_Dte_Pnd_Dly_Mth_Cmpre_Dte_Dd", 
            "#DLY-MTH-CMPRE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Prntline_Dmy_Ltrl = localVariables.newFieldInRecord("pnd_Prntline_Dmy_Ltrl", "#PRNTLINE-DMY-LTRL", FieldType.STRING, 7);

        pnd_Prntline_Prior_Bsnss_Dte = localVariables.newGroupInRecord("pnd_Prntline_Prior_Bsnss_Dte", "#PRNTLINE-PRIOR-BSNSS-DTE");
        pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Prior_Bsnss_Dte_M = pnd_Prntline_Prior_Bsnss_Dte.newFieldInGroup("pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Prior_Bsnss_Dte_M", 
            "#PRNTLINE-PRIOR-BSNSS-DTE-M", FieldType.NUMERIC, 2);
        pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Slash_1 = pnd_Prntline_Prior_Bsnss_Dte.newFieldInGroup("pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Slash_1", 
            "#PRNTLINE-SLASH-1", FieldType.STRING, 1);
        pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Prior_Bsnss_Dte_D = pnd_Prntline_Prior_Bsnss_Dte.newFieldInGroup("pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Prior_Bsnss_Dte_D", 
            "#PRNTLINE-PRIOR-BSNSS-DTE-D", FieldType.NUMERIC, 2);
        pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Slash_2 = pnd_Prntline_Prior_Bsnss_Dte.newFieldInGroup("pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Slash_2", 
            "#PRNTLINE-SLASH-2", FieldType.STRING, 1);
        pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Prior_Bsnss_Dte_Cy = pnd_Prntline_Prior_Bsnss_Dte.newFieldInGroup("pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Prior_Bsnss_Dte_Cy", 
            "#PRNTLINE-PRIOR-BSNSS-DTE-CY", FieldType.NUMERIC, 4);

        pnd_Prntline_New_Bsnss_Dte = localVariables.newGroupInRecord("pnd_Prntline_New_Bsnss_Dte", "#PRNTLINE-NEW-BSNSS-DTE");
        pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_M = pnd_Prntline_New_Bsnss_Dte.newFieldInGroup("pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_M", 
            "#PRNTLINE-NEW-BSNSS-DTE-M", FieldType.NUMERIC, 2);
        pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_Slash_3 = pnd_Prntline_New_Bsnss_Dte.newFieldInGroup("pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_Slash_3", 
            "#PRNTLINE-SLASH-3", FieldType.STRING, 1);
        pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_D = pnd_Prntline_New_Bsnss_Dte.newFieldInGroup("pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_D", 
            "#PRNTLINE-NEW-BSNSS-DTE-D", FieldType.NUMERIC, 2);
        pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_Slash_4 = pnd_Prntline_New_Bsnss_Dte.newFieldInGroup("pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_Slash_4", 
            "#PRNTLINE-SLASH-4", FieldType.STRING, 1);
        pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_Cy = pnd_Prntline_New_Bsnss_Dte.newFieldInGroup("pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_Cy", 
            "#PRNTLINE-NEW-BSNSS-DTE-CY", FieldType.NUMERIC, 4);

        pnd_Prntline_Prior_Nxt_Bsnss_Dte = localVariables.newGroupInRecord("pnd_Prntline_Prior_Nxt_Bsnss_Dte", "#PRNTLINE-PRIOR-NXT-BSNSS-DTE");
        pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Prior_Nxt_Bsnss_Dte_M = pnd_Prntline_Prior_Nxt_Bsnss_Dte.newFieldInGroup("pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Prior_Nxt_Bsnss_Dte_M", 
            "#PRNTLINE-PRIOR-NXT-BSNSS-DTE-M", FieldType.NUMERIC, 2);
        pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Slash_5 = pnd_Prntline_Prior_Nxt_Bsnss_Dte.newFieldInGroup("pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Slash_5", 
            "#PRNTLINE-SLASH-5", FieldType.STRING, 1);
        pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Prior_Nxt_Bsnss_Dte_D = pnd_Prntline_Prior_Nxt_Bsnss_Dte.newFieldInGroup("pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Prior_Nxt_Bsnss_Dte_D", 
            "#PRNTLINE-PRIOR-NXT-BSNSS-DTE-D", FieldType.NUMERIC, 2);
        pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Slash_6 = pnd_Prntline_Prior_Nxt_Bsnss_Dte.newFieldInGroup("pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Slash_6", 
            "#PRNTLINE-SLASH-6", FieldType.STRING, 1);
        pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Prior_Nxt_Bsnss_Dte_Cy = pnd_Prntline_Prior_Nxt_Bsnss_Dte.newFieldInGroup("pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Prior_Nxt_Bsnss_Dte_Cy", 
            "#PRNTLINE-PRIOR-NXT-BSNSS-DTE-CY", FieldType.NUMERIC, 4);

        pnd_Prntline_New_Nxt_Bsnss_Dte = localVariables.newGroupInRecord("pnd_Prntline_New_Nxt_Bsnss_Dte", "#PRNTLINE-NEW-NXT-BSNSS-DTE");
        pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_New_Nxt_Bsnss_Dte_M = pnd_Prntline_New_Nxt_Bsnss_Dte.newFieldInGroup("pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_New_Nxt_Bsnss_Dte_M", 
            "#PRNTLINE-NEW-NXT-BSNSS-DTE-M", FieldType.NUMERIC, 2);
        pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_Slash_7 = pnd_Prntline_New_Nxt_Bsnss_Dte.newFieldInGroup("pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_Slash_7", 
            "#PRNTLINE-SLASH-7", FieldType.STRING, 1);
        pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_New_Nxt_Bsnss_Dte_D = pnd_Prntline_New_Nxt_Bsnss_Dte.newFieldInGroup("pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_New_Nxt_Bsnss_Dte_D", 
            "#PRNTLINE-NEW-NXT-BSNSS-DTE-D", FieldType.NUMERIC, 2);
        pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_Slash_8 = pnd_Prntline_New_Nxt_Bsnss_Dte.newFieldInGroup("pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_Slash_8", 
            "#PRNTLINE-SLASH-8", FieldType.STRING, 1);
        pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_New_Nxt_Bsnss_Dte_Cy = pnd_Prntline_New_Nxt_Bsnss_Dte.newFieldInGroup("pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_New_Nxt_Bsnss_Dte_Cy", 
            "#PRNTLINE-NEW-NXT-BSNSS-DTE-CY", FieldType.NUMERIC, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_naz_Table_Ddm.reset();

        ldaAdslcntl.initializeValues();

        localVariables.reset();
        pnd_Nmbr_Dly_Recs_In.setInitialValue(0);
        pnd_Nmbr_Dly_Recs_Ot.setInitialValue(0);
        pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Slash_1.setInitialValue("/");
        pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Slash_2.setInitialValue("/");
        pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_M.setInitialValue(0);
        pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_Slash_3.setInitialValue("/");
        pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_D.setInitialValue(0);
        pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_Slash_4.setInitialValue("/");
        pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_Cy.setInitialValue(0);
        pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Prior_Nxt_Bsnss_Dte_M.setInitialValue(0);
        pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Slash_5.setInitialValue("/");
        pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Prior_Nxt_Bsnss_Dte_D.setInitialValue(0);
        pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Slash_6.setInitialValue("/");
        pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Prior_Nxt_Bsnss_Dte_Cy.setInitialValue(0);
        pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_Slash_7.setInitialValue("/");
        pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_Slash_8.setInitialValue("/");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp910() throws Exception
    {
        super("Adsp910");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("ADSP910", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 132 PS = 60 SG = OFF
        //*  IF EOM IS A WEEKEND OR HOLIDAY, UPDATE 2ND ADAS CTRL REC (ESV 7.71.04)
        //*  =====================================================================
        //*  PERFORM CHECK-IF-MONTH-END /* EM - 051911 START
        //*  IF #EOM-PROCESS
        //*     READ-ADS.
        //*     READ (2) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        //*     END-READ
        //*     IF *COUNTER(READ-ADS.) EQ 2
        //*        GET ADS-CNTL-VIEW *ISN(READ-ADS.)
        //*        MOVE #FACTOR-DATE-D TO ADS-CNTL-VIEW.ADS-RPT-13
        //*        UPDATE
        //*        END TRANSACTION
        //*     END-IF
        //*    PERFORM WRITE-EOM-REPORT
        //*  ELSE                       /* EM - 051911 END
                                                                                                                                                                          //Natural: PERFORM WRITE-TITLES
        sub_Write_Titles();
        if (condition(Global.isEscape())) {return;}
        //*   UPDATE DAILY CONTROL RECORD PROCESSING
        pnd_Prntline_Dmy_Ltrl.setValue("DAILY");                                                                                                                          //Natural: MOVE 'DAILY' TO #PRNTLINE-DMY-LTRL
        //*   FIND THE LATEST DAILY RECORD ON FILE
        ldaAdslcntl.getVw_ads_Cntl_View().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "READ01",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        1
        );
        READ01:
        while (condition(ldaAdslcntl.getVw_ads_Cntl_View().readNextRow("READ01")))
        {
            pnd_Nmbr_Dly_Recs_In.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #NMBR-DLY-RECS-IN
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Nmbr_Dly_Recs_In.equals(getZero())))                                                                                                            //Natural: IF #NMBR-DLY-RECS-IN EQ 0
        {
                                                                                                                                                                          //Natural: PERFORM NO-RECORD-FOUND-PROCESS
            sub_No_Record_Found_Process();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM RECORD-FOUND-PROCESS
            sub_Record_Found_Process();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  END-IF                     /* EM - 051911
        //*  /* EM - 051911 START
        //* ***********************************
        //*  DEFINE SUBROUTINE CHECK-IF-MONTH-END                   /* ESV 07-21-04
        //* ************************************
        //*  MOVE 8 TO #ACT-KEY-TYPE
        //*  CALLNAT 'DVAN150R' ACT-VSAM-RECORD DASA099
        //*  IF #ACT-RETURN-INDICATOR NE 0
        //*    WRITE '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! '
        //*    WRITE 'ERROR IN CALL TO DVAN150R TO DETERMINE MONTH-END'
        //*    WRITE 'DATE IN FACTOR FILE.'
        //*    WRITE '#ACT-RETURN-INDICATOR: ' #ACT-RETURN-INDICATOR
        //*    WRITE '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! '
        //*    TERMINATE
        //*  END-IF
        //*  MOVE #ACT-LATEST-DATE(1)   TO #FACTOR-DATE
        //*  MOVE EDITED #FACTOR-DATE-A TO #FACTOR-DATE-D (EM=YYYYMMDD)
        //*  COMPUTE #COMPUTE-DATE-D = #FACTOR-DATE-D + 1
        //*  MOVE EDITED #COMPUTE-DATE-D (EM=YYYYMMDD) TO #COMPUTE-DATE-A
        //*                                             /* CHECK IF EOM ESV 7.21.04
        //*  IF #FACTOR-DATE-MM NE #COMPUTE-DATE-MM
        //*     MOVE EDITED #FACTOR-DATE-D  (EM=N(9)) TO #DAY-OF-WEEK
        //*     IF #DAY-OF-WEEK = 'SATURDAY' OR = 'SUNDAY'
        //*        MOVE TRUE  TO #EOM-PROCESS
        //*     ELSE
        //*        #NAZ-TBL-RCRD-TYP-IND := 'C'
        //*        #NAZ-TBL-RCRD-LVL1-ID := 'NAZ006'
        //*        #NAZ-TBL-RCRD-LVL2-ID := 'DT'
        //*        #NAZ-TBL-RCRD-LVL3-ID := #FACTOR-DATE
        //*        READ NAZ-TABLE-DDM BY NAZ-TBL-SUPER3
        //*                                    STARTING FROM #NAZ-TABLE-SUPER3
        //*          IF NAZ-TABLE-DDM.NAZ-TBL-RCRD-LVL1-ID NE #NAZ-TBL-RCRD-LVL1-ID
        //*          OR NAZ-TABLE-DDM.NAZ-TBL-RCRD-LVL2-ID NE #NAZ-TBL-RCRD-LVL2-ID
        //*             ESCAPE BOTTOM
        //*          END-IF
        //*          ACCEPT IF NAZ-TABLE-DDM.NAZ-TBL-RCRD-ACTV-IND EQ 'Y'
        //*          IF #FACTOR-DATE-A EQ SUBSTR (NAZ-TBL-RCRD-LVL3-ID,1,8)
        //*             IF SUBSTR (NAZ-TBL-SECNDRY-DSCRPTN-TXT(1),1,2) EQ ' '
        //*                MOVE TRUE  TO #EOM-PROCESS
        //*             END-IF
        //*             ESCAPE BOTTOM
        //*          END-IF
        //*      END-READ
        //*   END-IF
        //*  END-IF
        //*  END-SUBROUTINE
        //* ********************************
        //*  DEFINE SUBROUTINE WRITE-EOM-REPORT     /* ESV 07-21-04
        //* ********************************
        //*  WRITE NOTITLE NOHDR
        //*    'PROGRAM   : ' *PROGRAM
        //*    55T 'ANNUITIZATION OF OMNIPLUS CONTRACTS'
        //*   113T 'RUN DATE: ' *DATX( EM=MM/DD/YYYY ) /
        //*    61T 'CREATE CONTROL RECORDS'
        //*   113T 'RUN TIME: ' *TIMX /
        //*   113T '    PAGE: ' *PAGE-NUMBER ////
        //*   WRITE ///
        //*  /' ================================================================= '
        //*  /'|*****************************************************************|'
        //*  /'|*****************************************************************|'
        //*  /'|**  >>>> MONTH END PROCESSING FALLS ON A WEEKEND/HOLIDAY <<<<  **|'
        //*  /'|**                                                             **|'
        //*  /'|**  ADS-RPT-13 IN ADS-CNTL FILE (2ND RECORD) HAS BEEN UPDATED  **|'
        //*  /'|**     WITH MONTH-END DATE : ' #FACTOR-DATE-D (EM=YYYY/MM/DD) '**|'
        //*  /'|**                                                             **|'
        //*  /'|*****************************************************************|'
        //*  /'|*****************************************************************|'
        //*  /' ================================================================= '
        //*  END-SUBROUTINE
        //*  /* EM 051911 END
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NO-RECORD-FOUND-PROCESS
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RECORD-FOUND-PROCESS
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-REPORT-DATES
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-NEW-DAILY-REC
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TITLES
        //*  LINE 5, 6, 7
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DETAIL-LINE
        //* ********************************
        //*       ERROR PROCESSING
        //* ********************************
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_No_Record_Found_Process() throws Exception                                                                                                           //Natural: NO-RECORD-FOUND-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"NO DAILY CONTROL RECORD FOUND  ",NEWLINE);                                                            //Natural: WRITE // 'NO DAILY CONTROL RECORD FOUND  ' /
        if (Global.isEscape()) return;
        pdaCpwa115.getCpwa115_For_Date().setValue(Global.getDATN());                                                                                                      //Natural: MOVE *DATN TO FOR-DATE
        pdaCpwa115.getCpwa115_For_Time().setValue(Global.getTIMN());                                                                                                      //Natural: MOVE *TIMN TO FOR-TIME
        pnd_Convert_Date.reset();                                                                                                                                         //Natural: RESET #CONVERT-DATE #PRNTLINE-PRIOR-BSNSS-DTE-CY #PRNTLINE-PRIOR-BSNSS-DTE-M #PRNTLINE-PRIOR-BSNSS-DTE-D
        pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Prior_Bsnss_Dte_Cy.reset();
        pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Prior_Bsnss_Dte_M.reset();
        pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Prior_Bsnss_Dte_D.reset();
        //*   OBTAIN  DATE INFORMATION FROM
        //*   THE EXTERNALIZATION FILE
        DbsUtil.callnat(Cpwn119.class , getCurrentProcessState(), pdaCpwa115.getCpwa115());                                                                               //Natural: CALLNAT 'CPWN119' USING CPWA115
        if (condition(Global.isEscape())) return;
        if (condition(pdaCpwa115.getCpwa115_Error_Return_Code().notEquals(getZero())))                                                                                    //Natural: IF ERROR-RETURN-CODE NE 0
        {
            getReports().write(0, ReportOption.NOTITLE,"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ");                                                            //Natural: WRITE '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! '
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"ERROR IN CALL TO CPWN115 TO DETERMINE BUSINESS DATES ");                                                          //Natural: WRITE 'ERROR IN CALL TO CPWN115 TO DETERMINE BUSINESS DATES '
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"DATE PASSED TO PROGRAM CPWN115: ",Global.getDATN());                                                              //Natural: WRITE 'DATE PASSED TO PROGRAM CPWN115: ' *DATN
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"CPWA115.RETURN-CODE: ",pdaCpwa115.getCpwa115_Error_Return_Code());                                                //Natural: WRITE 'CPWA115.RETURN-CODE: ' CPWA115.ERROR-RETURN-CODE
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ");                                                            //Natural: WRITE '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! '
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"!   1 =  FOR-DATE  ENTERED IS INVALID            ! ");                                                            //Natural: WRITE '!   1 =  FOR-DATE  ENTERED IS INVALID            ! '
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"!   2 =  FOR-TIME  ENTERED IS INVALID            ! ");                                                            //Natural: WRITE '!   2 =  FOR-TIME  ENTERED IS INVALID            ! '
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"!   6 =  FOR-DATE  ENTERED IS NOT ON EXT TABLE.  ! ");                                                            //Natural: WRITE '!   6 =  FOR-DATE  ENTERED IS NOT ON EXT TABLE.  ! '
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"!!  DO NOT PROCEED WITH SETTLEMENTS PROCESSING  !! ");                                                            //Natural: WRITE '!!  DO NOT PROCEED WITH SETTLEMENTS PROCESSING  !! '
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ");                                                            //Natural: WRITE '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! '
            if (Global.isEscape()) return;
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Dte().setValue(pdaCpwa115.getCpwa115_Current_Business_Date());                                                        //Natural: MOVE CURRENT-BUSINESS-DATE TO ADS-CNTL-BSNSS-DTE
        ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte().setValue(pdaCpwa115.getCpwa115_Next_Business_Date());                                                     //Natural: MOVE NEXT-BUSINESS-DATE TO ADS-CNTL-BSNSS-TMRRW-DTE
        ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte().compute(new ComputeParameters(false, ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte()),          //Natural: COMPUTE ADS-CNTL-BSNSS-RCPRCL-DTE = 100000000 - ADS-CNTL-BSNSS-DTE
            DbsField.subtract(100000000,ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Dte()));
        pnd_Convert_Date.setValue(ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Dte());                                                                                     //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #CONVERT-DATE
        pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_Cy.setValue(pnd_Convert_Date_Pnd_Convert_Date_Cy);                                                          //Natural: MOVE #CONVERT-DATE-CY TO #PRNTLINE-NEW-BSNSS-DTE-CY
        pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_M.setValue(pnd_Convert_Date_Pnd_Convert_Date_Mm);                                                           //Natural: MOVE #CONVERT-DATE-MM TO #PRNTLINE-NEW-BSNSS-DTE-M
        pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_D.setValue(pnd_Convert_Date_Pnd_Convert_Date_Dd);                                                           //Natural: MOVE #CONVERT-DATE-DD TO #PRNTLINE-NEW-BSNSS-DTE-D
        pnd_Convert_Date.setValue(ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte());                                                                               //Natural: MOVE ADS-CNTL-BSNSS-TMRRW-DTE TO #CONVERT-DATE
        pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_New_Nxt_Bsnss_Dte_Cy.setValue(pnd_Convert_Date_Pnd_Convert_Date_Cy);                                                  //Natural: MOVE #CONVERT-DATE-CY TO #PRNTLINE-NEW-NXT-BSNSS-DTE-CY
        pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_New_Nxt_Bsnss_Dte_M.setValue(pnd_Convert_Date_Pnd_Convert_Date_Mm);                                                   //Natural: MOVE #CONVERT-DATE-MM TO #PRNTLINE-NEW-NXT-BSNSS-DTE-M
        pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_New_Nxt_Bsnss_Dte_D.setValue(pnd_Convert_Date_Pnd_Convert_Date_Dd);                                                   //Natural: MOVE #CONVERT-DATE-DD TO #PRNTLINE-NEW-NXT-BSNSS-DTE-D
        ldaAdslcntl.getAds_Cntl_View_Ads_Rcrd_Cde().setValue("41");                                                                                                       //Natural: ASSIGN ADS-RCRD-CDE = '41'
        ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde().setValue("F");                                                                                               //Natural: ASSIGN ADS-CNTL-RCRD-TYP-CDE = 'F'
                                                                                                                                                                          //Natural: PERFORM INIT-REPORT-DATES
        sub_Init_Report_Dates();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM STORE-NEW-DAILY-REC
        sub_Store_New_Daily_Rec();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL-LINE
        sub_Write_Detail_Line();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Record_Found_Process() throws Exception                                                                                                              //Natural: RECORD-FOUND-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        //*  THIS LOGIC IS PROCESSED WHEN A RECORD IS FOUND ON THE FILE.
        //*   MOVE DATES FROM RECORD TO PRINT LINE PRIOR DATES
        pnd_Convert_Date.setValue(ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Dte());                                                                                     //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #CONVERT-DATE
        pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Prior_Bsnss_Dte_Cy.setValue(pnd_Convert_Date_Pnd_Convert_Date_Cy);                                                      //Natural: MOVE #CONVERT-DATE-CY TO #PRNTLINE-PRIOR-BSNSS-DTE-CY
        pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Prior_Bsnss_Dte_M.setValue(pnd_Convert_Date_Pnd_Convert_Date_Mm);                                                       //Natural: MOVE #CONVERT-DATE-MM TO #PRNTLINE-PRIOR-BSNSS-DTE-M
        pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Prior_Bsnss_Dte_D.setValue(pnd_Convert_Date_Pnd_Convert_Date_Dd);                                                       //Natural: MOVE #CONVERT-DATE-DD TO #PRNTLINE-PRIOR-BSNSS-DTE-D
        pnd_Convert_Date.setValue(ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte());                                                                               //Natural: MOVE ADS-CNTL-BSNSS-TMRRW-DTE TO #CONVERT-DATE
        pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Prior_Nxt_Bsnss_Dte_Cy.setValue(pnd_Convert_Date_Pnd_Convert_Date_Cy);                                              //Natural: MOVE #CONVERT-DATE-CY TO #PRNTLINE-PRIOR-NXT-BSNSS-DTE-CY
        pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Prior_Nxt_Bsnss_Dte_M.setValue(pnd_Convert_Date_Pnd_Convert_Date_Mm);                                               //Natural: MOVE #CONVERT-DATE-MM TO #PRNTLINE-PRIOR-NXT-BSNSS-DTE-M
        pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Prior_Nxt_Bsnss_Dte_D.setValue(pnd_Convert_Date_Pnd_Convert_Date_Dd);                                               //Natural: MOVE #CONVERT-DATE-DD TO #PRNTLINE-PRIOR-NXT-BSNSS-DTE-D
        //*   OBTAIN NEW DATES FROM THE EXTERNALIZATION FILE
        //*   MOVE THE NEXT BUSINESS DATE TO THE CURRENT BUSINESS DATE
        //*   AND PARAMETER DATE.
        ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Dte().setValue(ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte());                                              //Natural: MOVE ADS-CNTL-BSNSS-TMRRW-DTE TO ADS-CNTL-BSNSS-DTE
        pdaCpwa115.getCpwa115_For_Date().setValue(ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte());                                                               //Natural: MOVE ADS-CNTL-BSNSS-TMRRW-DTE TO FOR-DATE
        pdaCpwa115.getCpwa115_For_Time().setValue(Global.getTIMN());                                                                                                      //Natural: MOVE *TIMN TO FOR-TIME
        DbsUtil.callnat(Cpwn119.class , getCurrentProcessState(), pdaCpwa115.getCpwa115());                                                                               //Natural: CALLNAT 'CPWN119' USING CPWA115
        if (condition(Global.isEscape())) return;
        if (condition(pdaCpwa115.getCpwa115_Error_Return_Code().notEquals(getZero())))                                                                                    //Natural: IF ERROR-RETURN-CODE NE 0
        {
            getReports().write(0, ReportOption.NOTITLE,"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ");                                                            //Natural: WRITE '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! '
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"ERROR IN CALL TO CPWN115 TO DETERMINE BUSINESS DATES ");                                                          //Natural: WRITE 'ERROR IN CALL TO CPWN115 TO DETERMINE BUSINESS DATES '
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"DATE PASSED TO PROGRAM CPWN115: ",ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte());                       //Natural: WRITE 'DATE PASSED TO PROGRAM CPWN115: ' ADS-CNTL-BSNSS-TMRRW-DTE
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"CPWA115.RETURN-CODE: ",pdaCpwa115.getCpwa115_Error_Return_Code());                                                //Natural: WRITE 'CPWA115.RETURN-CODE: ' CPWA115.ERROR-RETURN-CODE
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ");                                                            //Natural: WRITE '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! '
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"!   1 =  FOR-DATE  ENTERED IS INVALID            ! ");                                                            //Natural: WRITE '!   1 =  FOR-DATE  ENTERED IS INVALID            ! '
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"!   2 =  FOR-TIME  ENTERED IS INVALID            ! ");                                                            //Natural: WRITE '!   2 =  FOR-TIME  ENTERED IS INVALID            ! '
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"!   6 =  FOR-DATE  ENTERED IS NOT ON EXT TABLE.  ! ");                                                            //Natural: WRITE '!   6 =  FOR-DATE  ENTERED IS NOT ON EXT TABLE.  ! '
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"!!  DO NOT PROCEED WITH SETTLEMENTS PROCESSING  !! ");                                                            //Natural: WRITE '!!  DO NOT PROCEED WITH SETTLEMENTS PROCESSING  !! '
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ");                                                            //Natural: WRITE '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! '
            if (Global.isEscape()) return;
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte().setValue(pdaCpwa115.getCpwa115_Next_Business_Date());                                                     //Natural: MOVE NEXT-BUSINESS-DATE TO ADS-CNTL-BSNSS-TMRRW-DTE
        ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte().compute(new ComputeParameters(false, ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte()),          //Natural: COMPUTE ADS-CNTL-BSNSS-RCPRCL-DTE = 100000000 - ADS-CNTL-BSNSS-DTE
            DbsField.subtract(100000000,ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Dte()));
        pnd_Convert_Date.setValue(ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Dte());                                                                                     //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #CONVERT-DATE
        pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_Cy.setValue(pnd_Convert_Date_Pnd_Convert_Date_Cy);                                                          //Natural: MOVE #CONVERT-DATE-CY TO #PRNTLINE-NEW-BSNSS-DTE-CY
        pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_M.setValue(pnd_Convert_Date_Pnd_Convert_Date_Mm);                                                           //Natural: MOVE #CONVERT-DATE-MM TO #PRNTLINE-NEW-BSNSS-DTE-M
        pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_D.setValue(pnd_Convert_Date_Pnd_Convert_Date_Dd);                                                           //Natural: MOVE #CONVERT-DATE-DD TO #PRNTLINE-NEW-BSNSS-DTE-D
        pnd_Convert_Date.setValue(ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte());                                                                               //Natural: MOVE ADS-CNTL-BSNSS-TMRRW-DTE TO #CONVERT-DATE
        pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_New_Nxt_Bsnss_Dte_Cy.setValue(pnd_Convert_Date_Pnd_Convert_Date_Cy);                                                  //Natural: MOVE #CONVERT-DATE-CY TO #PRNTLINE-NEW-NXT-BSNSS-DTE-CY
        pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_New_Nxt_Bsnss_Dte_M.setValue(pnd_Convert_Date_Pnd_Convert_Date_Mm);                                                   //Natural: MOVE #CONVERT-DATE-MM TO #PRNTLINE-NEW-NXT-BSNSS-DTE-M
        pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_New_Nxt_Bsnss_Dte_D.setValue(pnd_Convert_Date_Pnd_Convert_Date_Dd);                                                   //Natural: MOVE #CONVERT-DATE-DD TO #PRNTLINE-NEW-NXT-BSNSS-DTE-D
        ldaAdslcntl.getAds_Cntl_View_Ads_Rcrd_Cde().setValue("41");                                                                                                       //Natural: ASSIGN ADS-RCRD-CDE = '41'
        ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde().setValue("F");                                                                                               //Natural: ASSIGN ADS-CNTL-RCRD-TYP-CDE = 'F'
                                                                                                                                                                          //Natural: PERFORM STORE-NEW-DAILY-REC
        sub_Store_New_Daily_Rec();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL-LINE
        sub_Write_Detail_Line();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Init_Report_Dates() throws Exception                                                                                                                 //Natural: INIT-REPORT-DATES
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        ldaAdslcntl.getAds_Cntl_View_Ads_Inc_Dlay_Trn_Rpt_Dte().setValue(Global.getDATX());                                                                               //Natural: ASSIGN ADS-INC-DLAY-TRN-RPT-DTE = *DATX
        ldaAdslcntl.getAds_Cntl_View_Ads_Rej_Trn_Rpt_Dte().setValue(Global.getDATX());                                                                                    //Natural: ASSIGN ADS-REJ-TRN-RPT-DTE = *DATX
        ldaAdslcntl.getAds_Cntl_View_Ads_Trn_Hstry_Extrct_Rpt_Dte().setValue(Global.getDATX());                                                                           //Natural: ASSIGN ADS-TRN-HSTRY-EXTRCT-RPT-DTE = *DATX
        ldaAdslcntl.getAds_Cntl_View_Ads_Pndg_Trn_Rpt_Dte().setValue(Global.getDATX());                                                                                   //Natural: ASSIGN ADS-PNDG-TRN-RPT-DTE = *DATX
        ldaAdslcntl.getAds_Cntl_View_Ads_Inst_Lttr_Dte().setValue(Global.getDATX());                                                                                      //Natural: ASSIGN ADS-INST-LTTR-DTE = *DATX
        ldaAdslcntl.getAds_Cntl_View_Ads_Cis_Intrfce_Dte().setValue(Global.getDATX());                                                                                    //Natural: ASSIGN ADS-CIS-INTRFCE-DTE = *DATX
        ldaAdslcntl.getAds_Cntl_View_Ads_Fnl_Prm_Cntrl_Dte().setValue(Global.getDATX());                                                                                  //Natural: ASSIGN ADS-FNL-PRM-CNTRL-DTE = *DATX
        ldaAdslcntl.getAds_Cntl_View_Ads_Daily_Actvty_Dte().setValue(Global.getDATX());                                                                                   //Natural: ASSIGN ADS-DAILY-ACTVTY-DTE = *DATX
        ldaAdslcntl.getAds_Cntl_View_Ads_Fnl_Prm_Accum_Dte().setValue(Global.getDATX());                                                                                  //Natural: ASSIGN ADS-FNL-PRM-ACCUM-DTE = *DATX
    }
    private void sub_Store_New_Daily_Rec() throws Exception                                                                                                               //Natural: STORE-NEW-DAILY-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        ldaAdslcntl.getVw_ads_Cntl_View().insertDBRow();                                                                                                                  //Natural: STORE ADS-CNTL-VIEW
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        pnd_Nmbr_Dly_Recs_Ot.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #NMBR-DLY-RECS-OT
    }
    private void sub_Write_Titles() throws Exception                                                                                                                      //Natural: WRITE-TITLES
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,"PROGRAM   : ",Global.getPROGRAM(),new TabSetting(55),"ANNUITIZATION OF OMNIPLUS CONTRACTS",new     //Natural: WRITE NOTITLE NOHDR 'PROGRAM   : ' *PROGRAM 55T 'ANNUITIZATION OF OMNIPLUS CONTRACTS' 113T 'RUN DATE: ' *DATX ( EM = MM/DD/YYYY ) / 61T 'CREATE CONTROL RECORDS' 113T 'RUN TIME: ' *TIMX / 113T '    PAGE: ' *PAGE-NUMBER // 30X ' PREVIOUS        PREVIOUS        CURRENT          CURRENT     ' / 30X ' BUSINESS        NXT BUSINESS    BUSINESS         NXT BUSINESS' / 30X ' DATE            DATE            DATE             DATE        ' /
            TabSetting(113),"RUN DATE: ",Global.getDATX(), new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(61),"CREATE CONTROL RECORDS",new TabSetting(113),"RUN TIME: ",Global.getTIMX(),NEWLINE,new 
            TabSetting(113),"    PAGE: ",getReports().getPageNumberDbs(0),NEWLINE,NEWLINE,new ColumnSpacing(30)," PREVIOUS        PREVIOUS        CURRENT          CURRENT     ",NEWLINE,new 
            ColumnSpacing(30)," BUSINESS        NXT BUSINESS    BUSINESS         NXT BUSINESS",NEWLINE,new ColumnSpacing(30)," DATE            DATE            DATE             DATE        ",
            NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Write_Detail_Line() throws Exception                                                                                                                 //Natural: WRITE-DETAIL-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        if (condition(pnd_Prntline_Dmy_Ltrl.equals("DAILY  ")))                                                                                                           //Natural: IF #PRNTLINE-DMY-LTRL = 'DAILY  '
        {
            getReports().write(0, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(2),pnd_Prntline_Dmy_Ltrl,new ColumnSpacing(18),pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Prior_Bsnss_Dte_M,pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Slash_1,pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Prior_Bsnss_Dte_D,pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Slash_2,pnd_Prntline_Prior_Bsnss_Dte_Pnd_Prntline_Prior_Bsnss_Dte_Cy,new  //Natural: WRITE / 2X #PRNTLINE-DMY-LTRL 18X #PRNTLINE-PRIOR-BSNSS-DTE 3X #PRNTLINE-PRIOR-NXT-BSNSS-DTE 3X #PRNTLINE-NEW-BSNSS-DTE 3X #PRNTLINE-NEW-NXT-BSNSS-DTE / / 2X 'NUMBER DAILY   RECRDS FOUND: ' #NMBR-DLY-RECS-IN 5X 'NUMBER NEW DAILY   RECRDS: ' #NMBR-DLY-RECS-OT
                ColumnSpacing(3),pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Prior_Nxt_Bsnss_Dte_M,pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Slash_5,pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Prior_Nxt_Bsnss_Dte_D,pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Slash_6,pnd_Prntline_Prior_Nxt_Bsnss_Dte_Pnd_Prntline_Prior_Nxt_Bsnss_Dte_Cy,new 
                ColumnSpacing(3),pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_M,pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_Slash_3,pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_D,pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_Slash_4,pnd_Prntline_New_Bsnss_Dte_Pnd_Prntline_New_Bsnss_Dte_Cy,new 
                ColumnSpacing(3),pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_New_Nxt_Bsnss_Dte_M,pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_Slash_7,pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_New_Nxt_Bsnss_Dte_D,pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_Slash_8,pnd_Prntline_New_Nxt_Bsnss_Dte_Pnd_Prntline_New_Nxt_Bsnss_Dte_Cy,NEWLINE,NEWLINE,new 
                ColumnSpacing(2),"NUMBER DAILY   RECRDS FOUND: ",pnd_Nmbr_Dly_Recs_In,new ColumnSpacing(5),"NUMBER NEW DAILY   RECRDS: ",pnd_Nmbr_Dly_Recs_Ot);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        if (condition(Global.getERROR_NR().equals(3003)))                                                                                                                 //Natural: IF *ERROR-NR = 3003
        {
            getReports().write(0, ReportOption.NOTITLE,"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");                                                                       //Natural: WRITE '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
            getReports().write(0, ReportOption.NOTITLE,"!!ERROR!!ERROR!!ERROR!!ERROR!!ERROR!!!!!");                                                                       //Natural: WRITE '!!ERROR!!ERROR!!ERROR!!ERROR!!ERROR!!!!!'
            getReports().write(0, ReportOption.NOTITLE,"PROGRAM ADSP910                         ");                                                                       //Natural: WRITE 'PROGRAM ADSP910                         '
            getReports().write(0, ReportOption.NOTITLE,"IN ON ERROR PROCESSING ERROR CODE = 3003");                                                                       //Natural: WRITE 'IN ON ERROR PROCESSING ERROR CODE = 3003'
            getReports().write(0, ReportOption.NOTITLE,"IN ON ERROR PROCESSING ERROR CODE = 3003");                                                                       //Natural: WRITE 'IN ON ERROR PROCESSING ERROR CODE = 3003'
            getReports().write(0, ReportOption.NOTITLE,"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");                                                                       //Natural: WRITE '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
            //* *  ESCAPE BOTTOM /* OS - 08022000
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE
            if (condition(Global.isEscape())){return;}
            getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,"ERROR  ERROR  ERROR  ERROR   ",NEWLINE);                                                       //Natural: WRITE NOHDR 'ERROR  ERROR  ERROR  ERROR   ' /
            getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,"ERROR  ERROR  ERROR  ERROR   ",NEWLINE);                                                       //Natural: WRITE NOHDR 'ERROR  ERROR  ERROR  ERROR   ' /
            getReports().write(0, ReportOption.NOTITLE,ReportOption.NOHDR,"ERROR  ERROR  ERROR  ERROR   ",NEWLINE,Global.getERROR_NR(),NEWLINE,Global.getERROR_LINE());   //Natural: WRITE NOHDR 'ERROR  ERROR  ERROR  ERROR   ' / *ERROR-NR / *ERROR-LINE
        }                                                                                                                                                                 //Natural: END-IF
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60 SG=OFF");
    }
}
