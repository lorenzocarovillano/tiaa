/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:05:51 PM
**        * FROM NATURAL PROGRAM : Adsp950
************************************************************
**        * FILE NAME            : Adsp950.java
**        * CLASS NAME           : Adsp950
**        * INSTANCE NAME        : Adsp950
************************************************************
************************************************************************
* PROGRAM  : ADSP950
* SYSTEM   : ANNUITIZATION SUNGARD
* TITLE    : CREATE FLAT FILES FOR THE RECONCILIATION
* WRITTEN  : JAN, 2006
* FUNCTION : THIS PROGRAM READS ADS-CNTRCT, ADS-IA-RSLT AND ADS-PRTCPNT
*            AND CREATES 3 FLAT FILES FOR EACH REQUEST FOR THE PREVIUOS
*            BUSINESS DATE FROM ADS-CNTL
*            IT IS EXECUTED IN JOB PAN2400D
*
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* --------   -------   ---------------------------------------------
* 03/17/2008 E.MELNIK   ROTH 403B/401K CHANGES.  RECOMPILED FOR
*                       NEW ADSL401/ADSL401A/ADSL450.
* 7/19/2010  C. MASON   RESTOW DUE TO LDA CHANGES
* 03/07/12   E. MELNIK RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                      UPDATED LINKAGE AREAS.
* 04/03/12   O. SOTTO  ADDITIONAL RATE BASE CHANGES.  SC 040312.
* 01/18/13   E. MELNIK TNG PROJECT.  RESTOW TO INCLUDE UPDATED LINKAGE
*                      AREAS.
* 10/07/13   E. MELNIK RECOMPILED FOR NEW ADSL401A.  CREF/REA
*                      REDESIGN CHANGES.
* 02/23/2017 R. CARREON PIN EXPANSION 02232017
*********************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp950 extends BLNatBase
{
    // Data Areas
    private LdaAdsl401a ldaAdsl401a;
    private LdaAdsl401 ldaAdsl401;
    private LdaAdsl450 ldaAdsl450;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Contr_Rec_Read;
    private DbsField pnd_Counters_Pnd_Part_Rec_Read;
    private DbsField pnd_Counters_Pnd_Ia_Result_Rec_Read;
    private DbsField pnd_Counters_Pnd_Contr_Rec_Written;
    private DbsField pnd_Counters_Pnd_Part_Rec_Written;
    private DbsField pnd_Counters_Pnd_Ia_Result_Rec_Written;

    private DataAccessProgramView vw_ads_Cntl_View;

    private DbsGroup ads_Cntl_View_Ads_Cntl_Grp;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte;

    private DbsGroup ads_Cntl_View__R_Field_1;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A;
    private DbsField ads_Cntl_View_Ads_Rpt_13;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_2;
    private DbsField pnd_Date_Pnd_Date_N;
    private DbsField pnd_Last_Activity_Date;

    private DbsGroup pnd_Last_Activity_Date__R_Field_3;
    private DbsField pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N;
    private DbsField pnd_Key_Cntl_Bsnss_Dte;
    private DbsField pnd_Adi_Super1;

    private DbsGroup pnd_Adi_Super1__R_Field_4;
    private DbsField pnd_Adi_Super1_Pnd_Rqst_Id;
    private DbsField pnd_Adi_Super1_Pnd_Adi_Ia_Rcrd_Cde;
    private DbsField pnd_Adi_Super1_Pnd_Adi_Sqnce_Nbr;
    private DbsField pls_Debug_On;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl401a = new LdaAdsl401a();
        registerRecord(ldaAdsl401a);
        registerRecord(ldaAdsl401a.getVw_ads_Cntrct_View());
        ldaAdsl401 = new LdaAdsl401();
        registerRecord(ldaAdsl401);
        registerRecord(ldaAdsl401.getVw_ads_Prtcpnt_View());
        ldaAdsl450 = new LdaAdsl450();
        registerRecord(ldaAdsl450);
        registerRecord(ldaAdsl450.getVw_ads_Ia_Rslt_View());

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Contr_Rec_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Contr_Rec_Read", "#CONTR-REC-READ", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Counters_Pnd_Part_Rec_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Part_Rec_Read", "#PART-REC-READ", FieldType.PACKED_DECIMAL, 5);
        pnd_Counters_Pnd_Ia_Result_Rec_Read = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ia_Result_Rec_Read", "#IA-RESULT-REC-READ", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Counters_Pnd_Contr_Rec_Written = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Contr_Rec_Written", "#CONTR-REC-WRITTEN", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Counters_Pnd_Part_Rec_Written = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Part_Rec_Written", "#PART-REC-WRITTEN", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Counters_Pnd_Ia_Result_Rec_Written = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Ia_Result_Rec_Written", "#IA-RESULT-REC-WRITTEN", FieldType.PACKED_DECIMAL, 
            5);

        vw_ads_Cntl_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntl_View", "ADS-CNTL-VIEW"), "ADS_CNTL", "ADS_CNTL");

        ads_Cntl_View_Ads_Cntl_Grp = vw_ads_Cntl_View.getRecord().newGroupInGroup("ADS_CNTL_VIEW_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_DTE");

        ads_Cntl_View__R_Field_1 = ads_Cntl_View_Ads_Cntl_Grp.newGroupInGroup("ads_Cntl_View__R_Field_1", "REDEFINE", ads_Cntl_View_Ads_Cntl_Bsnss_Dte);
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A = ads_Cntl_View__R_Field_1.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A", "ADS-CNTL-BSNSS-DTE-A", FieldType.STRING, 
            8);
        ads_Cntl_View_Ads_Rpt_13 = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Rpt_13", "ADS-RPT-13", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADS_RPT_13");
        registerRecord(vw_ads_Cntl_View);

        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 8);

        pnd_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Date__R_Field_2", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_N = pnd_Date__R_Field_2.newFieldInGroup("pnd_Date_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        pnd_Last_Activity_Date = localVariables.newFieldInRecord("pnd_Last_Activity_Date", "#LAST-ACTIVITY-DATE", FieldType.STRING, 8);

        pnd_Last_Activity_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Last_Activity_Date__R_Field_3", "REDEFINE", pnd_Last_Activity_Date);
        pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N = pnd_Last_Activity_Date__R_Field_3.newFieldInGroup("pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N", 
            "#LAST-ACTIVITY-DATE-N", FieldType.NUMERIC, 8);
        pnd_Key_Cntl_Bsnss_Dte = localVariables.newFieldInRecord("pnd_Key_Cntl_Bsnss_Dte", "#KEY-CNTL-BSNSS-DTE", FieldType.DATE);
        pnd_Adi_Super1 = localVariables.newFieldInRecord("pnd_Adi_Super1", "#ADI-SUPER1", FieldType.STRING, 41);

        pnd_Adi_Super1__R_Field_4 = localVariables.newGroupInRecord("pnd_Adi_Super1__R_Field_4", "REDEFINE", pnd_Adi_Super1);
        pnd_Adi_Super1_Pnd_Rqst_Id = pnd_Adi_Super1__R_Field_4.newFieldInGroup("pnd_Adi_Super1_Pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 35);
        pnd_Adi_Super1_Pnd_Adi_Ia_Rcrd_Cde = pnd_Adi_Super1__R_Field_4.newFieldInGroup("pnd_Adi_Super1_Pnd_Adi_Ia_Rcrd_Cde", "#ADI-IA-RCRD-CDE", FieldType.STRING, 
            3);
        pnd_Adi_Super1_Pnd_Adi_Sqnce_Nbr = pnd_Adi_Super1__R_Field_4.newFieldInGroup("pnd_Adi_Super1_Pnd_Adi_Sqnce_Nbr", "#ADI-SQNCE-NBR", FieldType.STRING, 
            3);
        pls_Debug_On = WsIndependent.getInstance().newFieldInRecord("pls_Debug_On", "+DEBUG-ON", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Cntl_View.reset();

        ldaAdsl401a.initializeValues();
        ldaAdsl401.initializeValues();
        ldaAdsl450.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp950() throws Exception
    {
        super("Adsp950");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Adsp950|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT ( 01 ) LS = 132 PS = 55;//Natural: FORMAT ( 02 ) LS = 132 PS = 55;//Natural: FORMAT ( 03 ) LS = 132 PS = 55
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 01 )
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 02 )
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 03 )
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pls_Debug_On);                                                                                         //Natural: INPUT +DEBUG-ON
                if (condition(pls_Debug_On.getBoolean()))                                                                                                                 //Natural: IF +DEBUG-ON
                {
                    getReports().write(0, "Reading  ADS-CNTL ....");                                                                                                      //Natural: WRITE 'Reading  ADS-CNTL ....'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                vw_ads_Cntl_View.startDatabaseRead                                                                                                                        //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
                (
                "READ01",
                new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
                new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
                2
                );
                READ01:
                while (condition(vw_ads_Cntl_View.readNextRow("READ01")))
                {
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                             //Natural: IF ADS-RPT-13 NE 0
                {
                    //*   BOTH ARE D FORMAT
                    pnd_Key_Cntl_Bsnss_Dte.setValue(ads_Cntl_View_Ads_Rpt_13);                                                                                            //Natural: MOVE ADS-RPT-13 TO #KEY-CNTL-BSNSS-DTE
                    pnd_Date.setValueEdited(ads_Cntl_View_Ads_Rpt_13,new ReportEditMask("YYYYMMDD"));                                                                     //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE
                    getReports().write(0, "=",ads_Cntl_View_Ads_Rpt_13, new ReportEditMask ("YYYYMMDD"));                                                                 //Natural: WRITE '=' ADS-RPT-13 ( EM = YYYYMMDD )
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Key_Cntl_Bsnss_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A);                                             //Natural: MOVE EDITED ADS-CNTL-BSNSS-DTE-A TO #KEY-CNTL-BSNSS-DTE ( EM = YYYYMMDD )
                    getReports().write(0, "=",ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A);                                                                                        //Natural: WRITE '=' ADS-CNTL-BSNSS-DTE-A
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(0, "ADS-CNTL File data: ");                                                                                                            //Natural: WRITE 'ADS-CNTL File data: '
                if (Global.isEscape()) return;
                getReports().write(0, "=",ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A);                                                                                            //Natural: WRITE '=' ADS-CNTL-BSNSS-DTE-A
                if (Global.isEscape()) return;
                getReports().write(0, "=",ads_Cntl_View_Ads_Rpt_13, new ReportEditMask ("YYYYMMDD"));                                                                     //Natural: WRITE '=' ADS-RPT-13 ( EM = YYYYMMDD )
                if (Global.isEscape()) return;
                getReports().write(0, "Business Date used is ",pnd_Key_Cntl_Bsnss_Dte, new ReportEditMask ("YYYYMMDD"));                                                  //Natural: WRITE 'Business Date used is ' #KEY-CNTL-BSNSS-DTE ( EM = YYYYMMDD )
                if (Global.isEscape()) return;
                if (condition(pls_Debug_On.getBoolean()))                                                                                                                 //Natural: IF +DEBUG-ON
                {
                    getReports().write(0, "Reading ADS-PRTCPNT....by key ",pnd_Key_Cntl_Bsnss_Dte);                                                                       //Natural: WRITE 'Reading ADS-PRTCPNT....by key ' #KEY-CNTL-BSNSS-DTE
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl401.getVw_ads_Prtcpnt_View().startDatabaseRead                                                                                                     //Natural: READ ADS-PRTCPNT-VIEW BY ADP-LST-ACTVTY-DTE STARTING FROM #KEY-CNTL-BSNSS-DTE
                (
                "READ02",
                new Wc[] { new Wc("ADP_LST_ACTVTY_DTE", ">=", pnd_Key_Cntl_Bsnss_Dte, WcType.BY) },
                new Oc[] { new Oc("ADP_LST_ACTVTY_DTE", "ASC") }
                );
                READ02:
                while (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().readNextRow("READ02")))
                {
                    pnd_Counters_Pnd_Part_Rec_Read.nadd(1);                                                                                                               //Natural: ADD 1 TO #PART-REC-READ
                    pnd_Last_Activity_Date.setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                            //Natural: MOVE EDITED ADP-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #LAST-ACTIVITY-DATE
                    if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                         //Natural: IF ADS-RPT-13 NE 0
                    {
                        if (condition(pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N.greater(pnd_Date_Pnd_Date_N)))                                                      //Natural: IF #LAST-ACTIVITY-DATE-N > #DATE-N
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N.greater(ads_Cntl_View_Ads_Cntl_Bsnss_Dte)))                                         //Natural: IF #LAST-ACTIVITY-DATE-N > ADS-CNTL-BSNSS-DTE
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(!(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,1).equals("T"))))                                                        //Natural: ACCEPT IF SUBSTR ( ADP-STTS-CDE,1,1 ) = 'T'
                    {
                        continue;
                    }
                    getWorkFiles().write(1, false, ldaAdsl401.getVw_ads_Prtcpnt_View());                                                                                  //Natural: WRITE WORK FILE 1 ADS-PRTCPNT-VIEW
                    pnd_Counters_Pnd_Part_Rec_Written.nadd(1);                                                                                                            //Natural: ADD 1 TO #PART-REC-WRITTEN
                    getReports().display(1, "Request/   Id ",                                                                                                             //Natural: DISPLAY ( 01 ) 'Request/   Id ' ADS-PRTCPNT-VIEW.RQST-ID 'Last Activity/    Date' ADP-LST-ACTVTY-DTE ( EM = YYYYMMDD ) 'Status/ Code' ADS-PRTCPNT-VIEW.ADP-STTS-CDE
                    		ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id(),"Last Activity/    Date",
                    		ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte(), new ReportEditMask ("YYYYMMDD"),"Status/ Code",
                    		ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde());
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM READ-CONTRACT-FILE
                    sub_Read_Contract_File();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM READ-IA-RESULT-FILE
                    sub_Read_Ia_Result_File();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,"  ");                                                                                                         //Natural: WRITE ( 01 ) '  '
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,"ADS-PRTCPNT records read     ",pnd_Counters_Pnd_Part_Rec_Read);                                               //Natural: WRITE ( 01 ) 'ADS-PRTCPNT records read     ' #PART-REC-READ
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,"ADS-PRTCPNT records written  ",pnd_Counters_Pnd_Part_Rec_Written);                                            //Natural: WRITE ( 01 ) 'ADS-PRTCPNT records written  ' #PART-REC-WRITTEN
                if (Global.isEscape()) return;
                getReports().write(2, ReportOption.NOTITLE,"  ");                                                                                                         //Natural: WRITE ( 02 ) '  '
                if (Global.isEscape()) return;
                getReports().write(2, ReportOption.NOTITLE,"ADS-CNTRCT  records read     ",pnd_Counters_Pnd_Contr_Rec_Read);                                              //Natural: WRITE ( 02 ) 'ADS-CNTRCT  records read     ' #CONTR-REC-READ
                if (Global.isEscape()) return;
                getReports().write(2, ReportOption.NOTITLE,"ADS-CNTRCT  records written  ",pnd_Counters_Pnd_Contr_Rec_Written);                                           //Natural: WRITE ( 02 ) 'ADS-CNTRCT  records written  ' #CONTR-REC-WRITTEN
                if (Global.isEscape()) return;
                getReports().write(3, ReportOption.NOTITLE,"  ");                                                                                                         //Natural: WRITE ( 03 ) '  '
                if (Global.isEscape()) return;
                getReports().write(3, ReportOption.NOTITLE,"ADS-IA-RSLT records read     ",pnd_Counters_Pnd_Ia_Result_Rec_Read);                                          //Natural: WRITE ( 03 ) 'ADS-IA-RSLT records read     ' #IA-RESULT-REC-READ
                if (Global.isEscape()) return;
                getReports().write(3, ReportOption.NOTITLE,"ADS-IA-RSLT records written  ",pnd_Counters_Pnd_Ia_Result_Rec_Written);                                       //Natural: WRITE ( 03 ) 'ADS-IA-RSLT records written  ' #IA-RESULT-REC-WRITTEN
                if (Global.isEscape()) return;
                //* *****************************************************************
                //*      S U B R O U T I N E S
                //* *****************************************************************
                //* ***********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CONTRACT-FILE
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IA-RESULT-FILE
                //*    (ADI-IA-RCRD-CDE = 'RS' AND ADI-SQNCE-NBR = 12  OR = 22)
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Read_Contract_File() throws Exception                                                                                                                //Natural: READ-CONTRACT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
            getReports().write(0, "Reading ADS-CNTRCT  ....by ",ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                                //Natural: WRITE 'Reading ADS-CNTRCT  ....by 'ADS-PRTCPNT-VIEW.RQST-ID
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl401a.getVw_ads_Cntrct_View().startDatabaseRead                                                                                                             //Natural: READ ADS-CNTRCT-VIEW BY ADS-CNTRCT-VIEW.RQST-ID = ADS-PRTCPNT-VIEW.RQST-ID
        (
        "READ03",
        new Wc[] { new Wc("RQST_ID", ">=", ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id(), WcType.BY) },
        new Oc[] { new Oc("RQST_ID", "ASC") }
        );
        READ03:
        while (condition(ldaAdsl401a.getVw_ads_Cntrct_View().readNextRow("READ03")))
        {
            pnd_Counters_Pnd_Contr_Rec_Read.nadd(1);                                                                                                                      //Natural: ADD 1 TO #CONTR-REC-READ
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Rqst_Id().notEquals(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id())))                                                  //Natural: IF ADS-CNTRCT-VIEW.RQST-ID NE ADS-PRTCPNT-VIEW.RQST-ID
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(2, false, ldaAdsl401a.getVw_ads_Cntrct_View());                                                                                          //Natural: WRITE WORK FILE 2 ADS-CNTRCT-VIEW
            pnd_Counters_Pnd_Contr_Rec_Written.nadd(1);                                                                                                                   //Natural: ADD 1 TO #CONTR-REC-WRITTEN
            getReports().display(2, "Request/   Id ",                                                                                                                     //Natural: DISPLAY ( 02 ) 'Request/   Id ' ADS-CNTRCT-VIEW.RQST-ID 'Status/ Code' ADS-CNTRCT-VIEW.ADC-STTS-CDE
            		ldaAdsl401a.getAds_Cntrct_View_Rqst_Id(),"Status/ Code",
            		ldaAdsl401a.getAds_Cntrct_View_Adc_Stts_Cde());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  READ-CONTRACT-FILE
    }
    private void sub_Read_Ia_Result_File() throws Exception                                                                                                               //Natural: READ-IA-RESULT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        pnd_Adi_Super1.reset();                                                                                                                                           //Natural: RESET #ADI-SUPER1
        pnd_Adi_Super1_Pnd_Rqst_Id.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                                                    //Natural: ASSIGN #RQST-ID := ADS-PRTCPNT-VIEW.RQST-ID
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
            getReports().write(0, "Reading ADS-IA-RSLT ... by #ADI-SUPER1 ",pnd_Adi_Super1);                                                                              //Natural: WRITE 'Reading ADS-IA-RSLT ... by #ADI-SUPER1 ' #ADI-SUPER1
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl450.getVw_ads_Ia_Rslt_View().startDatabaseRead                                                                                                             //Natural: READ ADS-IA-RSLT-VIEW BY ADI-SUPER1 = #ADI-SUPER1
        (
        "READ04",
        new Wc[] { new Wc("ADI_SUPER1", ">=", pnd_Adi_Super1, WcType.BY) },
        new Oc[] { new Oc("ADI_SUPER1", "ASC") }
        );
        READ04:
        while (condition(ldaAdsl450.getVw_ads_Ia_Rslt_View().readNextRow("READ04")))
        {
            pnd_Counters_Pnd_Ia_Result_Rec_Read.nadd(1);                                                                                                                  //Natural: ADD 1 TO #IA-RESULT-REC-READ
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Rqst_Id().notEquals(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id())))                                                  //Natural: IF ADS-IA-RSLT-VIEW.RQST-ID NE ADS-PRTCPNT-VIEW.RQST-ID
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!((ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde().equals("ACT") || (DbsUtil.maskMatches(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde(),"'RS'")  //Natural: ACCEPT IF ADI-IA-RCRD-CDE = 'ACT' OR ( ADI-IA-RCRD-CDE = MASK ( 'RS' ) AND ADI-SQNCE-NBR = 12 OR = 22 )
                && (ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr().equals(12) || ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr().equals(22)))))))
            {
                continue;
            }
            getWorkFiles().write(3, false, ldaAdsl450.getVw_ads_Ia_Rslt_View());                                                                                          //Natural: WRITE WORK FILE 3 ADS-IA-RSLT-VIEW
            pnd_Counters_Pnd_Ia_Result_Rec_Written.nadd(1);                                                                                                               //Natural: ADD 1 TO #IA-RESULT-REC-WRITTEN
            getReports().display(3, "Request/   Id ",                                                                                                                     //Natural: DISPLAY ( 03 ) 'Request/   Id ' ADS-IA-RSLT-VIEW.RQST-ID 'Record/ Code' ADS-IA-RSLT-VIEW.ADI-IA-RCRD-CDE 'Sequence/Number' ADS-IA-RSLT-VIEW.ADI-SQNCE-NBR
            		ldaAdsl450.getAds_Ia_Rslt_View_Rqst_Id(),"Record/ Code",
            		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde(),"Sequence/Number",
            		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*   READ-IA-RESULT-FILE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(52),"TIAA/CREF",NEWLINE,new ColumnSpacing(1),"RUN DATE  : ",Global.getDATU(),new  //Natural: WRITE ( 01 ) NOTITLE NOHDR 52X 'TIAA/CREF' / 1X 'RUN DATE  : ' *DATU 21X 'ADAS RECONCILIATION PROCESS' 15X 'PROGRAM ID: ' *PROGRAM / 1X 'RUN TIME  : ' *TIMX 26X 'Participant File' 21X 'PAGE : ' *PAGE-NUMBER ( 01 ) ///
                        ColumnSpacing(21),"ADAS RECONCILIATION PROCESS",new ColumnSpacing(15),"PROGRAM ID: ",Global.getPROGRAM(),NEWLINE,new ColumnSpacing(1),"RUN TIME  : ",Global.getTIMX(),new 
                        ColumnSpacing(26),"Participant File",new ColumnSpacing(21),"PAGE : ",getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(52),"TIAA/CREF",NEWLINE,new ColumnSpacing(1),"RUN DATE  : ",Global.getDATU(),new  //Natural: WRITE ( 02 ) NOTITLE NOHDR 52X 'TIAA/CREF' / 1X 'RUN DATE  : ' *DATU 21X 'ADAS RECONCILIATION PROCESS' 15X 'PROGRAM ID: ' *PROGRAM / 1X 'RUN TIME  : ' *TIMX 28X 'Contract File ' 21X 'PAGE : ' *PAGE-NUMBER ( 02 ) ///
                        ColumnSpacing(21),"ADAS RECONCILIATION PROCESS",new ColumnSpacing(15),"PROGRAM ID: ",Global.getPROGRAM(),NEWLINE,new ColumnSpacing(1),"RUN TIME  : ",Global.getTIMX(),new 
                        ColumnSpacing(28),"Contract File ",new ColumnSpacing(21),"PAGE : ",getReports().getPageNumberDbs(2),NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(52),"TIAA/CREF",NEWLINE,new ColumnSpacing(1),"RUN DATE  : ",Global.getDATU(),new  //Natural: WRITE ( 03 ) NOTITLE NOHDR 52X 'TIAA/CREF' / 1X 'RUN DATE  : ' *DATU 21X 'ADAS RECONCILIATION PROCESS' 15X 'PROGRAM ID: ' *PROGRAM / 1X 'RUN TIME  : ' *TIMX 25X 'IA Result File ' 23X 'PAGE : ' *PAGE-NUMBER ( 03 ) ///
                        ColumnSpacing(21),"ADAS RECONCILIATION PROCESS",new ColumnSpacing(15),"PROGRAM ID: ",Global.getPROGRAM(),NEWLINE,new ColumnSpacing(1),"RUN TIME  : ",Global.getTIMX(),new 
                        ColumnSpacing(25),"IA Result File ",new ColumnSpacing(23),"PAGE : ",getReports().getPageNumberDbs(3),NEWLINE,NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=55");
        Global.format(2, "LS=132 PS=55");
        Global.format(3, "LS=132 PS=55");

        getReports().setDisplayColumns(1, "Request/   Id ",
        		ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id(),"Last Activity/    Date",
        		ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte(), new ReportEditMask ("YYYYMMDD"),"Status/ Code",
        		ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde());
        getReports().setDisplayColumns(2, "Request/   Id ",
        		ldaAdsl401a.getAds_Cntrct_View_Rqst_Id(),"Status/ Code",
        		ldaAdsl401a.getAds_Cntrct_View_Adc_Stts_Cde());
        getReports().setDisplayColumns(3, "Request/   Id ",
        		ldaAdsl450.getAds_Ia_Rslt_View_Rqst_Id(),"Record/ Code",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde(),"Sequence/Number",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr());
    }
}
