/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:02:56 PM
**        * FROM NATURAL PROGRAM : Adsp401
************************************************************
**        * FILE NAME            : Adsp401.java
**        * CLASS NAME           : Adsp401
**        * INSTANCE NAME        : Adsp401
************************************************************
***********************************************************************
* PROGRAM  : ADSP401
* SYSTEM   : ADAS - ANNUITIZATION SUNGUARD
* TITLE    : ADAS BATCH (STEP 2) - DRIVER MODULE FOR PROCESSING ADAS
*          :                       REQUESTS
* GENERATED: MARCH 9, 2004
* AUTHOR   : EDWINA VILLANUEVA
* FUNCTION : THIS PROGRAM IS CLONED FROM THE ORIGINAL ADAM BATCH PROGRAM
*          : NAZN401. THE ADAS VERSION IS CODED TO CONFORM WITH THE NEW
*          : ANNUITIZATION - BRAND X, WHICH PROCESS ANNUITY REQUESTS
*          : BASED ON SETTLEMENT DATA RECEIVED FROM SUNGARD OMNI PLUS
*          : AND ADDITIONAL ANNUITY INFO. ENTERED IN ADAS ONLINE SYSTEM.
*          : IT PERFORMS THE FOLLOWING STEPS:
*          :
*          : 1.  READ ADAS RECORDS W/ 'PENDING READY TO PROCESS' STATUS
*          : 2.  PERFORM ADAS EDITS (ADSN410)
*          : 3.  CALCULATE SETTLEMENT AMOUNTS (ADSN420)
*          : 4.  CALL CALM INTERFACE TO GENERATE LEDGERS IN PEOPLESOFT
*          :     (ADSN430)
*          : 5.  SUMMARIZE SETTLEMENT AMOUNTS (ADSN440)
*          : 6.  PERFORM ACTUARIAL INTERFACE TO CALCULATE PAYOUT AMOUNTS
*          :     (ADSN450)
*          : 7.  PERFORM IA INTERFACE TO SETUP ANNUITY PAYMENTS
*          :     (ADSN600)
*          : 8.  UPDATE ADAS KDO RECORDS WITH BATCH RESULT INFORMATION
*          :     (ADSN470)
*          : 9.  CLOSE MIT REQUESTS (CREATED BY ADAS ONLINE) FOR EACH
*          :     ANNUITY REQUEST PROCESSED (ADSN480)
*
*********************  MAINTENANCE LOG ******************************
*
*  D A T E   PROGRAMMER     D E S C R I P T I O N
*
* 06-15-04   E.VILLANUEVA   REMOVE LOGIC TO COMPARE TOTAL ADC-DTL-TIAA-
*                           OPN-ACCUM-AMT WITH ADC-ACCT-OPN-ACCUM-AMT
*                           PER ED MELNIK's instructions.
* 06-16-04   E.VILLANUEVA   REMOVE UPDATE OF CONTRACT RECORDS AFTER
*                           CALCULATION OF SETTLEMENTS. THIS WILL AVOID
*                           COMMITTING CALM UPDATES IF THERE's an
*                           ERROR IN PROCESSING.
* 07-13-04   E.VILLANUEVA   ADD INDEPENDENT VARIABLES : EOM-INDR AND
*                           EOM-FACTOR-DTE TO BE USED FOR CALM INTERFACE
*                           TURN ON EOM-INDR ONLY IF FACTOR LATEST DATE
*                           IS A MONTH-END AND FALLS ON A SATURDAY,
*                           SUNDAY OR HOLIDAY.
* 07-14-04   E.VILLANUEVA   INSERT LOGIC TO STORE ADC-ERR-RPT-DATA(*) IN
*                           ADS-CNTRCT FOR ERRORS ENCOUNTERED AFTER THE
*                           CONTRACT PROCESSING LOOP.
* 07-16-04   E.VILLANUEVA   INCLUDE PLAN-NO WHEN CALLING MIT
* 07-21-04   E.VILLANUEVA   UPDATE ADP-LST-ACTVTY-DTE WITH FACTOR FILE
*                           DATE DURING MONTH-END PROCESS
* 08-23-04   E.VILLANUEVA   DISPLAY DISCREPANCY IN FUND RATE TOTAL AMT
* 08312005   M.NACHBER      REL 6.5: CHANGES FOR IPRO's and TPA'S
* 12052006   M.NACHBER      MIMINUM DISTRIBUTION COMPLIANCE TEST (MDIB)
* 12-05-07   D.GEARY        ADD TESTS FOR ERROR STATUS L66+L67 (MDIB)
* 02-22-08   O SOTTO TIAA RATE EXPANSION TO 99 OCCURS. SC 022208.
* 03-05-08   O SOTTO CHECK *ERROR-NO AFTER CALL TO ADSN600. IF THERE
*                    IS AN ADABAS PROBLEM, TERMINATE PROCESSING.
*                    SC 030508.
* 04-22-08   R.SACHARNY     PROCESS ROTH REQUESTS (CHANGES MARKED RS0)
* 12-10-08   E.MELNIK    TIAA ACCES/STABLE RETURN ENHANCEMENTS.
*                        MARKED BY EM - 121008.
* 01-13-09 R SACHARNY  CALL ADSN888 FOR EXTERNALIZATION (RS1)
* 07-08-09 C MASON     CHANGES PER MDM CONVERSION
* 03-03-10 C MASON     OPEN & CLOSE MQ; UPDATE CALL MDMN210 TO MDMN210A
* 03-19-10 C MASON     PCPOP CONVERSION - RESTOW FOR CHANGES TO ADSA401
* 12-10-08   E.MELNIK  SVSAA ENHANCEMENTS.
*                      MARKED BY EM - 111510.
* 05-19-11   E.MELNIK  MEOW CHANGES.  MARKED BY EM - 051911.
* 03/01/12 E. MELNIK   RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                      UPDATED LINKAGE AREAS.  CHANGES MARKED BY
*                      EM - 030112.
* 10/11/12 O. SOTTO    DON't call MDM for resettlements. SC 101112.
* 01/17/13 O. SOTTO    CHECK COR WHENEVER MDM RETURN CODE IS NOT ZERO.
*                      SC 011713.
* 01/18/13 E. MELNIK   TNG PROJECT.  RESTOW TO INCLUDE UPDATED LINKAGE
*                      AREAS.
* 10/17/13 O. SOTTO    CREF REA CHANGES MARKED WITH OS-102013.
*                      COMMENTED-OUT CALL TO MDM AND COR AMONG THE
*                      CHANGES.
* 03/25/15 E. MELNIK   CALM SUNSET PROJECT.  MARKED BY EM - CALM15.
* 02/22/2017 R.CARREON PIN EXPANSION 02222017
* 03/14/17 E. MELNIK   IA REPLATFORM PROJECT. MARKED BY EM - IA17.
* 11/13/17 E. MELNIK   IA REPLATFORM PROJECT. CIS CONTRACT APPROVAL
*                      VALIDATIONS WILL NOT BE MADE FOR APC REQUESTS.
*                      MARKED BY EM - IA1117.
* 11/28/17 E. MELNIK   IA REPLATFORM PROJECT. CIS CONTRACT APPROVAL
*                      VALIDATIONS REINSTATED FOR APC REQUESTS.
*                      MARKED BY EM - IA1117.
* 04/12/19 E. MELNIK   RESTOWED FOR UPDATED ADSA401 WITH IRC CDE.
*********************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp401 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private LdaAdsl401 ldaAdsl401;
    private LdaAdsl401a ldaAdsl401a;
    private PdaAdsa401 pdaAdsa401;
    private PdaAdsa470 pdaAdsa470;
    private PdaAdsa480 pdaAdsa480;
    private PdaAdsa450 pdaAdsa450;
    private PdaNeca4000 pdaNeca4000;
    private PdaAdspda_M pdaAdspda_M;
    private PdaAdsa888 pdaAdsa888;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ads_Ia_Rslt_View;
    private DbsField ads_Ia_Rslt_View_Rqst_Id;
    private DbsField ads_Ia_Rslt_View_Adi_Ia_Rcrd_Cde;
    private DbsField ads_Ia_Rslt_View_Adi_Sqnce_Nbr;
    private DbsField ads_Ia_Rslt_View_Adi_Stts_Cde;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;

    private DbsGroup pnd_Constant_Variables;
    private DbsField pnd_Constant_Variables_Pnd_Open_Ind;
    private DbsField pnd_Constant_Variables_Pnd_Ready_To_Process;
    private DbsField pnd_Constant_Variables_Pnd_System_Error_Found;
    private DbsField pnd_Constant_Variables_Pnd_Successful;
    private DbsField pnd_Constant_Variables_Pnd_Resettlement;
    private DbsField pnd_Constant_Variables_Pnd_Tiaa;
    private DbsField pnd_Constant_Variables_Pnd_Rea;
    private DbsField pnd_Constant_Variables_Pnd_Access;
    private DbsField pnd_Constant_Variables_Pnd_Batchads;
    private DbsField pnd_Constant_Variables_Pnd_Update_Action;
    private DbsField pnd_Constant_Variables_Pnd_Max_Cntrct;
    private DbsField pnd_Constant_Variables_Pnd_Inactive_Ia;
    private DbsField pnd_Constant_Variables_Pnd_Missing_Ia;
    private DbsField pnd_Constant_Variables_Pnd_Ac_Annty;
    private DbsField pnd_Constant_Variables_Pnd_Database_Error;
    private DbsField pnd_Constant_Variables_Pnd_Tiaa_Tckr;
    private DbsField pnd_Constant_Variables_Pnd_Stbl_Tckr;
    private DbsField pnd_Constant_Variables_Pnd_Stbl_Value_Tckr;
    private DbsField pnd_Constant_Variables_Pnd_Max_Rates;

    private DbsGroup pnd_Keys;
    private DbsField pnd_Keys_Pnd_Adp_Super2;

    private DbsGroup pnd_Keys__R_Field_1;
    private DbsField pnd_Keys_Pnd_Super2_Opn_Clsd_Ind;
    private DbsField pnd_Keys_Pnd_Super2_Stts_Cde;
    private DbsField pnd_Keys_Pnd_Adi_Super1;

    private DbsGroup pnd_Keys__R_Field_2;
    private DbsField pnd_Keys_Pnd_Adi_Rqst_Id;
    private DbsField pnd_Keys_Pnd_Adi_Ia_Rcrd_Cde;
    private DbsField pnd_Keys_Pnd_Adi_Sqnce_Nbr;
    private DbsField pnd_Keys_Pnd_Cor_Cntrct_Key4;

    private DbsGroup pnd_Keys__R_Field_3;
    private DbsField pnd_Keys_Pnd_Cor_Key4_Cntrct;
    private DbsField pnd_Keys_Pnd_Cor_Key4_Pin;
    private DbsField pnd_Keys_Pnd_Cor_Key4_Rcd;
    private DbsField pnd_Keys_Pnd_Cor_Key4_Payee;
    private DbsField pnd_Keys_Pnd_Naz_Table_Super3;

    private DbsGroup pnd_Keys__R_Field_4;
    private DbsField pnd_Keys_Pnd_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField pnd_Keys_Pnd_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField pnd_Keys_Pnd_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField pnd_Keys_Pnd_Naz_Tbl_Rcrd_Lvl3_Id;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Cntrct_Cnt;
    private DbsField pnd_Counters_Pnd_Number_Of_Cref_Funds;
    private DbsField pnd_Counters_Pnd_I;
    private DbsField pnd_Counters_Pnd_P;

    private DbsGroup pnd_Logicals;
    private DbsField pnd_Logicals_Pnd_Cntrct_In_Cor;
    private DbsField pnd_Logicals_Pnd_Cntrct_Not_Issued;
    private DbsField pnd_Logicals_Pnd_Tiaa_Rqstd;
    private DbsField pnd_Logicals_Pnd_Tiaa_Multi_Funded_Rqst;
    private DbsField pnd_Logicals_Pnd_Cref_Multi_Funded_Rqst;
    private DbsField pnd_Logicals_Pnd_Rea_Rqstd;
    private DbsField pnd_Logicals_Pnd_Access_Rqstd;
    private DbsField pnd_Logicals_Pnd_Rea_Mnthly_Unts;
    private DbsField pnd_Logicals_Pnd_Cref_Mnthly_Unts;
    private DbsField pnd_Logicals_Pnd_Ia_Process_Completed;

    private DbsGroup pnd_Adsa043_Call_Cis;
    private DbsField pnd_Adsa043_Call_Cis_Pnd_Rsdnce_Typ;
    private DbsField pnd_Adsa043_Call_Cis_Pnd_Rqst_Typ;
    private DbsField pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Rsdnce;
    private DbsField pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Ctznshp;
    private DbsField pnd_Adsa043_Call_Cis_Pnd_Annty_Strt_Dte;
    private DbsField pnd_Adsa043_Call_Cis_Pnd_Da_Tiaa_Nbr;
    private DbsField pnd_Adsa043_Call_Cis_Pnd_Cor_Pin_Nbr;
    private DbsField pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Ssn;
    private DbsField pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Dte_Of_Brth;
    private DbsField pnd_Adsa043_Call_Cis_Pnd_Org_Issue_St;
    private DbsField pnd_Adsa043_Call_Cis_Pnd_Rsdnce_Issue_St;
    private DbsField pnd_Adsa043_Call_Cis_Pnd_Apc_Rqst_Ind;
    private DbsField pnd_Adsa043_Call_Cis_Pnd_Ia_Cntrct_Apprvl_Ind;

    private DbsGroup pnd_Adsa043_Call_Cis_Pnd_Cis_Cntrct_Tbl;
    private DbsField pnd_Adsa043_Call_Cis_Pnd_Issue_State_Code;
    private DbsField pnd_Adsa043_Call_Cis_Pnd_Issue_Date;

    private DbsGroup pnd_Adsa600_Addl_Data;
    private DbsField pnd_Adsa600_Addl_Data_Pnd_Prdc_Ivc;
    private DbsField pnd_Adsa600_Addl_Data_Pnd_Err_Cde_X;
    private DbsField pnd_Adsa600_Addl_Data_Pnd_Program_Nme;
    private DbsField pnd_Adsa600_Addl_Data_Pnd_Err_Nr;
    private DbsField pnd_Adsa600_Addl_Data_Pnd_Err_Line_X;

    private DbsGroup pnd_Adsn998_Error_Handler;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Calling_Program;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Unique_Id;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Step_Name;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Appl_Id;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Error_Msg;

    private DbsGroup pnd_Get_Lob;
    private DbsField pnd_Get_Lob_Pnd_Lob_Cntrct;
    private DbsField pnd_Get_Lob_Pnd_Lob;
    private DbsField pnd_Get_Lob_Pnd_Sub_Lob;

    private DbsGroup pnd_Misc_Variables;
    private DbsField pnd_Misc_Variables_Pnd_Cct_Rcrd_Cde;
    private DbsField pnd_Misc_Variables_Pnd_Date_A;

    private DbsGroup pnd_Misc_Variables__R_Field_5;
    private DbsField pnd_Misc_Variables_Pnd_Date_N;
    private DbsField pnd_Misc_Variables_Pnd_Terminate_No;
    private DbsField pnd_Misc_Variables_Pnd_Terminate_Msg;

    private DbsGroup pnd_Misc_Variables__R_Field_6;
    private DbsField pnd_Misc_Variables_Pnd_Terminate_Msg1;
    private DbsField pnd_Misc_Variables_Pnd_Terminate_Msg2;
    private DbsField pnd_Misc_Variables_Pnd_Total_Actl_Amt;
    private DbsField pnd_Misc_Variables_Pnd_Total_Opn_Accum;
    private DbsField pnd_Misc_Variables_Pnd_Process_Step;
    private DbsField pnd_Misc_Variables_Pnd_Display_Adsa401;
    private DbsField pnd_Misc_Variables_Pnd_Display_Adsa450;
    private DbsField pnd_Misc_Variables_Pnd_Comp_Dte;
    private DbsField pnd_Misc_Variables_Pnd_Factor_Dte;

    private DbsGroup pnd_Misc_Variables__R_Field_7;
    private DbsField pnd_Misc_Variables_Pnd_Factor_Dte_Yyyy;
    private DbsField pnd_Misc_Variables_Pnd_Factor_Dte_Mm;
    private DbsField pnd_Misc_Variables_Pnd_Factor_Dte_Dd;
    private DbsField pnd_Misc_Variables_Pnd_Compute_Dte;

    private DbsGroup pnd_Misc_Variables__R_Field_8;
    private DbsField pnd_Misc_Variables_Pnd_Compute_Dte_Yyyy;
    private DbsField pnd_Misc_Variables_Pnd_Compute_Dte_Mm;
    private DbsField pnd_Misc_Variables_Pnd_Compute_Dte_Dd;
    private DbsField pnd_Misc_Variables_Pnd_Day_Of_Week;
    private DbsField pnd_Misc_Variables_Pnd_Ipro_Tpa_Ind;
    private DbsField pnd_Misc_Variables_Pnd_L;
    private DbsField pnd_Misc_Variables_Pnd_Wk_Cntrct_Lob;
    private DbsField pnd_Misc_Variables_Pnd_Origin_Lob_Ind;

    private DbsGroup pnd_Addit_450_Parameters;
    private DbsField pnd_Addit_450_Parameters_Pnd_Adp_Scnd_Annt_Rltnshp;
    private DbsField pnd_Addit_450_Parameters_Pnd_Return_Message;
    private DbsField pnd_Addit_450_Parameters_Pnd_Return_Code;
    private DbsField pnd_Rc;
    private DbsField pnd_Military_Res_Codes;
    private DbsField pnd_Request_Rejected;
    private DbsField pnd_Request_Processed;
    private DbsField pnd_Adas_Status_File;

    private DbsGroup pnd_Adas_Status_File__R_Field_9;
    private DbsField pnd_Adas_Status_File_Pnd_Rqst_Id_35;
    private DbsField pnd_Adas_Status_File_Pnd_Ia_Tiaa_Cntrct_Nbr;
    private DbsField pnd_Adas_Status_File_Pnd_Ia_Cref_Cntrct_Nbr;
    private DbsField pnd_Adas_Status_File_Pnd_Status;
    private DbsField pnd_Adas_Status_File_Pnd_Sub_Status;
    private DbsField pls_No_Of_Restart;
    private DbsField pls_Cics_System_Name;
    private DbsField pls_Tiaa_Rate_Cnt;
    private DbsField pls_Cntl_Bsnss_Prev_Dte;
    private DbsField pls_Cntl_Bsnss_Dte;
    private DbsField pls_Cntl_Bsnss_Tmrrw_Dte;
    private DbsField pls_Ia_Next_Cycle_Chk_Dte;
    private DbsField pls_First_Err_Rqstid;
    private DbsField pls_Debug_On;
    private DbsField pls_Tot_System_Errors;
    private DbsField pls_Tot_Prtcpnt_Rqst;
    private DbsField pls_Tot_Cntrct_Recs;
    private DbsField pls_Tot_Rqst_Errors;
    private DbsField pls_Tot_Rqst_Successful;
    private DbsField pls_Tot_In_Progress;
    private DbsField pls_Ticker;
    private DbsField pls_Invstmnt_Grpng_Id;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        ldaAdsl401 = new LdaAdsl401();
        registerRecord(ldaAdsl401);
        registerRecord(ldaAdsl401.getVw_ads_Prtcpnt_View());
        ldaAdsl401a = new LdaAdsl401a();
        registerRecord(ldaAdsl401a);
        registerRecord(ldaAdsl401a.getVw_ads_Cntrct_View());
        localVariables = new DbsRecord();
        pdaAdsa401 = new PdaAdsa401(localVariables);
        pdaAdsa470 = new PdaAdsa470(localVariables);
        pdaAdsa480 = new PdaAdsa480(localVariables);
        pdaAdsa450 = new PdaAdsa450(localVariables);
        pdaNeca4000 = new PdaNeca4000(localVariables);
        pdaAdspda_M = new PdaAdspda_M(localVariables);
        pdaAdsa888 = new PdaAdsa888(localVariables);

        // Local Variables

        vw_ads_Ia_Rslt_View = new DataAccessProgramView(new NameInfo("vw_ads_Ia_Rslt_View", "ADS-IA-RSLT-VIEW"), "ADS_IA_RSLT", "ADS_IA_RSLT");
        ads_Ia_Rslt_View_Rqst_Id = vw_ads_Ia_Rslt_View.getRecord().newFieldInGroup("ads_Ia_Rslt_View_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "RQST_ID");
        ads_Ia_Rslt_View_Adi_Ia_Rcrd_Cde = vw_ads_Ia_Rslt_View.getRecord().newFieldInGroup("ads_Ia_Rslt_View_Adi_Ia_Rcrd_Cde", "ADI-IA-RCRD-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "ADI_IA_RCRD_CDE");
        ads_Ia_Rslt_View_Adi_Sqnce_Nbr = vw_ads_Ia_Rslt_View.getRecord().newFieldInGroup("ads_Ia_Rslt_View_Adi_Sqnce_Nbr", "ADI-SQNCE-NBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "ADI_SQNCE_NBR");
        ads_Ia_Rslt_View_Adi_Stts_Cde = vw_ads_Ia_Rslt_View.getRecord().newFieldInGroup("ads_Ia_Rslt_View_Adi_Stts_Cde", "ADI-STTS-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "ADI_STTS_CDE");
        registerRecord(vw_ads_Ia_Rslt_View);

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        registerRecord(vw_naz_Table_Ddm);

        pnd_Constant_Variables = localVariables.newGroupInRecord("pnd_Constant_Variables", "#CONSTANT-VARIABLES");
        pnd_Constant_Variables_Pnd_Open_Ind = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Open_Ind", "#OPEN-IND", FieldType.STRING, 
            1);
        pnd_Constant_Variables_Pnd_Ready_To_Process = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Ready_To_Process", "#READY-TO-PROCESS", 
            FieldType.STRING, 3);
        pnd_Constant_Variables_Pnd_System_Error_Found = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_System_Error_Found", "#SYSTEM-ERROR-FOUND", 
            FieldType.STRING, 3);
        pnd_Constant_Variables_Pnd_Successful = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Successful", "#SUCCESSFUL", FieldType.STRING, 
            1);
        pnd_Constant_Variables_Pnd_Resettlement = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Resettlement", "#RESETTLEMENT", FieldType.STRING, 
            1);
        pnd_Constant_Variables_Pnd_Tiaa = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Tiaa", "#TIAA", FieldType.STRING, 1);
        pnd_Constant_Variables_Pnd_Rea = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Rea", "#REA", FieldType.STRING, 1);
        pnd_Constant_Variables_Pnd_Access = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Access", "#ACCESS", FieldType.STRING, 1);
        pnd_Constant_Variables_Pnd_Batchads = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Batchads", "#BATCHADS", FieldType.STRING, 
            8);
        pnd_Constant_Variables_Pnd_Update_Action = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Update_Action", "#UPDATE-ACTION", 
            FieldType.STRING, 2);
        pnd_Constant_Variables_Pnd_Max_Cntrct = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Max_Cntrct", "#MAX-CNTRCT", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Constant_Variables_Pnd_Inactive_Ia = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Inactive_Ia", "#INACTIVE-IA", FieldType.STRING, 
            3);
        pnd_Constant_Variables_Pnd_Missing_Ia = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Missing_Ia", "#MISSING-IA", FieldType.STRING, 
            3);
        pnd_Constant_Variables_Pnd_Ac_Annty = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Ac_Annty", "#AC-ANNTY", FieldType.STRING, 
            2);
        pnd_Constant_Variables_Pnd_Database_Error = pnd_Constant_Variables.newFieldArrayInGroup("pnd_Constant_Variables_Pnd_Database_Error", "#DATABASE-ERROR", 
            FieldType.NUMERIC, 7, new DbsArrayController(1, 2));
        pnd_Constant_Variables_Pnd_Tiaa_Tckr = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Tiaa_Tckr", "#TIAA-TCKR", FieldType.STRING, 
            10);
        pnd_Constant_Variables_Pnd_Stbl_Tckr = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Stbl_Tckr", "#STBL-TCKR", FieldType.STRING, 
            10);
        pnd_Constant_Variables_Pnd_Stbl_Value_Tckr = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Stbl_Value_Tckr", "#STBL-VALUE-TCKR", 
            FieldType.STRING, 10);
        pnd_Constant_Variables_Pnd_Max_Rates = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Max_Rates", "#MAX-RATES", FieldType.NUMERIC, 
            3);

        pnd_Keys = localVariables.newGroupInRecord("pnd_Keys", "#KEYS");
        pnd_Keys_Pnd_Adp_Super2 = pnd_Keys.newFieldInGroup("pnd_Keys_Pnd_Adp_Super2", "#ADP-SUPER2", FieldType.STRING, 4);

        pnd_Keys__R_Field_1 = pnd_Keys.newGroupInGroup("pnd_Keys__R_Field_1", "REDEFINE", pnd_Keys_Pnd_Adp_Super2);
        pnd_Keys_Pnd_Super2_Opn_Clsd_Ind = pnd_Keys__R_Field_1.newFieldInGroup("pnd_Keys_Pnd_Super2_Opn_Clsd_Ind", "#SUPER2-OPN-CLSD-IND", FieldType.STRING, 
            1);
        pnd_Keys_Pnd_Super2_Stts_Cde = pnd_Keys__R_Field_1.newFieldInGroup("pnd_Keys_Pnd_Super2_Stts_Cde", "#SUPER2-STTS-CDE", FieldType.STRING, 3);
        pnd_Keys_Pnd_Adi_Super1 = pnd_Keys.newFieldInGroup("pnd_Keys_Pnd_Adi_Super1", "#ADI-SUPER1", FieldType.STRING, 41);

        pnd_Keys__R_Field_2 = pnd_Keys.newGroupInGroup("pnd_Keys__R_Field_2", "REDEFINE", pnd_Keys_Pnd_Adi_Super1);
        pnd_Keys_Pnd_Adi_Rqst_Id = pnd_Keys__R_Field_2.newFieldInGroup("pnd_Keys_Pnd_Adi_Rqst_Id", "#ADI-RQST-ID", FieldType.STRING, 35);
        pnd_Keys_Pnd_Adi_Ia_Rcrd_Cde = pnd_Keys__R_Field_2.newFieldInGroup("pnd_Keys_Pnd_Adi_Ia_Rcrd_Cde", "#ADI-IA-RCRD-CDE", FieldType.STRING, 3);
        pnd_Keys_Pnd_Adi_Sqnce_Nbr = pnd_Keys__R_Field_2.newFieldInGroup("pnd_Keys_Pnd_Adi_Sqnce_Nbr", "#ADI-SQNCE-NBR", FieldType.NUMERIC, 3);
        pnd_Keys_Pnd_Cor_Cntrct_Key4 = pnd_Keys.newFieldInGroup("pnd_Keys_Pnd_Cor_Cntrct_Key4", "#COR-CNTRCT-KEY4", FieldType.STRING, 26);

        pnd_Keys__R_Field_3 = pnd_Keys.newGroupInGroup("pnd_Keys__R_Field_3", "REDEFINE", pnd_Keys_Pnd_Cor_Cntrct_Key4);
        pnd_Keys_Pnd_Cor_Key4_Cntrct = pnd_Keys__R_Field_3.newFieldInGroup("pnd_Keys_Pnd_Cor_Key4_Cntrct", "#COR-KEY4-CNTRCT", FieldType.STRING, 10);
        pnd_Keys_Pnd_Cor_Key4_Pin = pnd_Keys__R_Field_3.newFieldInGroup("pnd_Keys_Pnd_Cor_Key4_Pin", "#COR-KEY4-PIN", FieldType.NUMERIC, 12);
        pnd_Keys_Pnd_Cor_Key4_Rcd = pnd_Keys__R_Field_3.newFieldInGroup("pnd_Keys_Pnd_Cor_Key4_Rcd", "#COR-KEY4-RCD", FieldType.NUMERIC, 2);
        pnd_Keys_Pnd_Cor_Key4_Payee = pnd_Keys__R_Field_3.newFieldInGroup("pnd_Keys_Pnd_Cor_Key4_Payee", "#COR-KEY4-PAYEE", FieldType.NUMERIC, 2);
        pnd_Keys_Pnd_Naz_Table_Super3 = pnd_Keys.newFieldInGroup("pnd_Keys_Pnd_Naz_Table_Super3", "#NAZ-TABLE-SUPER3", FieldType.STRING, 30);

        pnd_Keys__R_Field_4 = pnd_Keys.newGroupInGroup("pnd_Keys__R_Field_4", "REDEFINE", pnd_Keys_Pnd_Naz_Table_Super3);
        pnd_Keys_Pnd_Naz_Tbl_Rcrd_Typ_Ind = pnd_Keys__R_Field_4.newFieldInGroup("pnd_Keys_Pnd_Naz_Tbl_Rcrd_Typ_Ind", "#NAZ-TBL-RCRD-TYP-IND", FieldType.STRING, 
            1);
        pnd_Keys_Pnd_Naz_Tbl_Rcrd_Lvl1_Id = pnd_Keys__R_Field_4.newFieldInGroup("pnd_Keys_Pnd_Naz_Tbl_Rcrd_Lvl1_Id", "#NAZ-TBL-RCRD-LVL1-ID", FieldType.STRING, 
            6);
        pnd_Keys_Pnd_Naz_Tbl_Rcrd_Lvl2_Id = pnd_Keys__R_Field_4.newFieldInGroup("pnd_Keys_Pnd_Naz_Tbl_Rcrd_Lvl2_Id", "#NAZ-TBL-RCRD-LVL2-ID", FieldType.STRING, 
            3);
        pnd_Keys_Pnd_Naz_Tbl_Rcrd_Lvl3_Id = pnd_Keys__R_Field_4.newFieldInGroup("pnd_Keys_Pnd_Naz_Tbl_Rcrd_Lvl3_Id", "#NAZ-TBL-RCRD-LVL3-ID", FieldType.STRING, 
            20);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Cntrct_Cnt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Cntrct_Cnt", "#CNTRCT-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Counters_Pnd_Number_Of_Cref_Funds = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Number_Of_Cref_Funds", "#NUMBER-OF-CREF-FUNDS", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Counters_Pnd_I = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Counters_Pnd_P = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_P", "#P", FieldType.PACKED_DECIMAL, 3);

        pnd_Logicals = localVariables.newGroupInRecord("pnd_Logicals", "#LOGICALS");
        pnd_Logicals_Pnd_Cntrct_In_Cor = pnd_Logicals.newFieldInGroup("pnd_Logicals_Pnd_Cntrct_In_Cor", "#CNTRCT-IN-COR", FieldType.BOOLEAN, 1);
        pnd_Logicals_Pnd_Cntrct_Not_Issued = pnd_Logicals.newFieldInGroup("pnd_Logicals_Pnd_Cntrct_Not_Issued", "#CNTRCT-NOT-ISSUED", FieldType.BOOLEAN, 
            1);
        pnd_Logicals_Pnd_Tiaa_Rqstd = pnd_Logicals.newFieldInGroup("pnd_Logicals_Pnd_Tiaa_Rqstd", "#TIAA-RQSTD", FieldType.BOOLEAN, 1);
        pnd_Logicals_Pnd_Tiaa_Multi_Funded_Rqst = pnd_Logicals.newFieldInGroup("pnd_Logicals_Pnd_Tiaa_Multi_Funded_Rqst", "#TIAA-MULTI-FUNDED-RQST", FieldType.BOOLEAN, 
            1);
        pnd_Logicals_Pnd_Cref_Multi_Funded_Rqst = pnd_Logicals.newFieldInGroup("pnd_Logicals_Pnd_Cref_Multi_Funded_Rqst", "#CREF-MULTI-FUNDED-RQST", FieldType.BOOLEAN, 
            1);
        pnd_Logicals_Pnd_Rea_Rqstd = pnd_Logicals.newFieldInGroup("pnd_Logicals_Pnd_Rea_Rqstd", "#REA-RQSTD", FieldType.BOOLEAN, 1);
        pnd_Logicals_Pnd_Access_Rqstd = pnd_Logicals.newFieldInGroup("pnd_Logicals_Pnd_Access_Rqstd", "#ACCESS-RQSTD", FieldType.BOOLEAN, 1);
        pnd_Logicals_Pnd_Rea_Mnthly_Unts = pnd_Logicals.newFieldInGroup("pnd_Logicals_Pnd_Rea_Mnthly_Unts", "#REA-MNTHLY-UNTS", FieldType.BOOLEAN, 1);
        pnd_Logicals_Pnd_Cref_Mnthly_Unts = pnd_Logicals.newFieldInGroup("pnd_Logicals_Pnd_Cref_Mnthly_Unts", "#CREF-MNTHLY-UNTS", FieldType.BOOLEAN, 
            1);
        pnd_Logicals_Pnd_Ia_Process_Completed = pnd_Logicals.newFieldInGroup("pnd_Logicals_Pnd_Ia_Process_Completed", "#IA-PROCESS-COMPLETED", FieldType.BOOLEAN, 
            1);

        pnd_Adsa043_Call_Cis = localVariables.newGroupInRecord("pnd_Adsa043_Call_Cis", "#ADSA043-CALL-CIS");
        pnd_Adsa043_Call_Cis_Pnd_Rsdnce_Typ = pnd_Adsa043_Call_Cis.newFieldInGroup("pnd_Adsa043_Call_Cis_Pnd_Rsdnce_Typ", "#RSDNCE-TYP", FieldType.STRING, 
            1);
        pnd_Adsa043_Call_Cis_Pnd_Rqst_Typ = pnd_Adsa043_Call_Cis.newFieldInGroup("pnd_Adsa043_Call_Cis_Pnd_Rqst_Typ", "#RQST-TYP", FieldType.STRING, 1);
        pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Rsdnce = pnd_Adsa043_Call_Cis.newFieldInGroup("pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Rsdnce", "#FRST-ANNT-RSDNCE", 
            FieldType.STRING, 3);
        pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Ctznshp = pnd_Adsa043_Call_Cis.newFieldInGroup("pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Ctznshp", "#FRST-ANNT-CTZNSHP", 
            FieldType.STRING, 2);
        pnd_Adsa043_Call_Cis_Pnd_Annty_Strt_Dte = pnd_Adsa043_Call_Cis.newFieldInGroup("pnd_Adsa043_Call_Cis_Pnd_Annty_Strt_Dte", "#ANNTY-STRT-DTE", FieldType.NUMERIC, 
            8);
        pnd_Adsa043_Call_Cis_Pnd_Da_Tiaa_Nbr = pnd_Adsa043_Call_Cis.newFieldArrayInGroup("pnd_Adsa043_Call_Cis_Pnd_Da_Tiaa_Nbr", "#DA-TIAA-NBR", FieldType.STRING, 
            10, new DbsArrayController(1, 6));
        pnd_Adsa043_Call_Cis_Pnd_Cor_Pin_Nbr = pnd_Adsa043_Call_Cis.newFieldInGroup("pnd_Adsa043_Call_Cis_Pnd_Cor_Pin_Nbr", "#COR-PIN-NBR", FieldType.NUMERIC, 
            12);
        pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Ssn = pnd_Adsa043_Call_Cis.newFieldInGroup("pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Ssn", "#FRST-ANNT-SSN", FieldType.NUMERIC, 
            9);
        pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Dte_Of_Brth = pnd_Adsa043_Call_Cis.newFieldInGroup("pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Dte_Of_Brth", "#FRST-ANNT-DTE-OF-BRTH", 
            FieldType.NUMERIC, 8);
        pnd_Adsa043_Call_Cis_Pnd_Org_Issue_St = pnd_Adsa043_Call_Cis.newFieldInGroup("pnd_Adsa043_Call_Cis_Pnd_Org_Issue_St", "#ORG-ISSUE-ST", FieldType.STRING, 
            2);
        pnd_Adsa043_Call_Cis_Pnd_Rsdnce_Issue_St = pnd_Adsa043_Call_Cis.newFieldInGroup("pnd_Adsa043_Call_Cis_Pnd_Rsdnce_Issue_St", "#RSDNCE-ISSUE-ST", 
            FieldType.STRING, 2);
        pnd_Adsa043_Call_Cis_Pnd_Apc_Rqst_Ind = pnd_Adsa043_Call_Cis.newFieldInGroup("pnd_Adsa043_Call_Cis_Pnd_Apc_Rqst_Ind", "#APC-RQST-IND", FieldType.STRING, 
            1);
        pnd_Adsa043_Call_Cis_Pnd_Ia_Cntrct_Apprvl_Ind = pnd_Adsa043_Call_Cis.newFieldArrayInGroup("pnd_Adsa043_Call_Cis_Pnd_Ia_Cntrct_Apprvl_Ind", "#IA-CNTRCT-APPRVL-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 4));

        pnd_Adsa043_Call_Cis_Pnd_Cis_Cntrct_Tbl = pnd_Adsa043_Call_Cis.newGroupArrayInGroup("pnd_Adsa043_Call_Cis_Pnd_Cis_Cntrct_Tbl", "#CIS-CNTRCT-TBL", 
            new DbsArrayController(1, 20));
        pnd_Adsa043_Call_Cis_Pnd_Issue_State_Code = pnd_Adsa043_Call_Cis_Pnd_Cis_Cntrct_Tbl.newFieldInGroup("pnd_Adsa043_Call_Cis_Pnd_Issue_State_Code", 
            "#ISSUE-STATE-CODE", FieldType.STRING, 2);
        pnd_Adsa043_Call_Cis_Pnd_Issue_Date = pnd_Adsa043_Call_Cis_Pnd_Cis_Cntrct_Tbl.newFieldInGroup("pnd_Adsa043_Call_Cis_Pnd_Issue_Date", "#ISSUE-DATE", 
            FieldType.NUMERIC, 8);

        pnd_Adsa600_Addl_Data = localVariables.newGroupInRecord("pnd_Adsa600_Addl_Data", "#ADSA600-ADDL-DATA");
        pnd_Adsa600_Addl_Data_Pnd_Prdc_Ivc = pnd_Adsa600_Addl_Data.newFieldInGroup("pnd_Adsa600_Addl_Data_Pnd_Prdc_Ivc", "#PRDC-IVC", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Adsa600_Addl_Data_Pnd_Err_Cde_X = pnd_Adsa600_Addl_Data.newFieldInGroup("pnd_Adsa600_Addl_Data_Pnd_Err_Cde_X", "#ERR-CDE-X", FieldType.STRING, 
            3);
        pnd_Adsa600_Addl_Data_Pnd_Program_Nme = pnd_Adsa600_Addl_Data.newFieldInGroup("pnd_Adsa600_Addl_Data_Pnd_Program_Nme", "#PROGRAM-NME", FieldType.STRING, 
            8);
        pnd_Adsa600_Addl_Data_Pnd_Err_Nr = pnd_Adsa600_Addl_Data.newFieldInGroup("pnd_Adsa600_Addl_Data_Pnd_Err_Nr", "#ERR-NR", FieldType.STRING, 4);
        pnd_Adsa600_Addl_Data_Pnd_Err_Line_X = pnd_Adsa600_Addl_Data.newFieldInGroup("pnd_Adsa600_Addl_Data_Pnd_Err_Line_X", "#ERR-LINE-X", FieldType.STRING, 
            4);

        pnd_Adsn998_Error_Handler = localVariables.newGroupInRecord("pnd_Adsn998_Error_Handler", "#ADSN998-ERROR-HANDLER");
        pnd_Adsn998_Error_Handler_Pnd_Calling_Program = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Calling_Program", "#CALLING-PROGRAM", 
            FieldType.STRING, 8);
        pnd_Adsn998_Error_Handler_Pnd_Unique_Id = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Unique_Id", "#UNIQUE-ID", FieldType.NUMERIC, 
            12);
        pnd_Adsn998_Error_Handler_Pnd_Step_Name = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Step_Name", "#STEP-NAME", FieldType.STRING, 
            8);
        pnd_Adsn998_Error_Handler_Pnd_Appl_Id = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Appl_Id", "#APPL-ID", FieldType.STRING, 
            8);
        pnd_Adsn998_Error_Handler_Pnd_Error_Msg = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 
            79);

        pnd_Get_Lob = localVariables.newGroupInRecord("pnd_Get_Lob", "#GET-LOB");
        pnd_Get_Lob_Pnd_Lob_Cntrct = pnd_Get_Lob.newFieldInGroup("pnd_Get_Lob_Pnd_Lob_Cntrct", "#LOB-CNTRCT", FieldType.STRING, 10);
        pnd_Get_Lob_Pnd_Lob = pnd_Get_Lob.newFieldInGroup("pnd_Get_Lob_Pnd_Lob", "#LOB", FieldType.STRING, 10);
        pnd_Get_Lob_Pnd_Sub_Lob = pnd_Get_Lob.newFieldInGroup("pnd_Get_Lob_Pnd_Sub_Lob", "#SUB-LOB", FieldType.STRING, 12);

        pnd_Misc_Variables = localVariables.newGroupInRecord("pnd_Misc_Variables", "#MISC-VARIABLES");
        pnd_Misc_Variables_Pnd_Cct_Rcrd_Cde = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Cct_Rcrd_Cde", "#CCT-RCRD-CDE", FieldType.STRING, 
            2);
        pnd_Misc_Variables_Pnd_Date_A = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Misc_Variables__R_Field_5 = pnd_Misc_Variables.newGroupInGroup("pnd_Misc_Variables__R_Field_5", "REDEFINE", pnd_Misc_Variables_Pnd_Date_A);
        pnd_Misc_Variables_Pnd_Date_N = pnd_Misc_Variables__R_Field_5.newFieldInGroup("pnd_Misc_Variables_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        pnd_Misc_Variables_Pnd_Terminate_No = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Terminate_No", "#TERMINATE-NO", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Misc_Variables_Pnd_Terminate_Msg = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Terminate_Msg", "#TERMINATE-MSG", FieldType.STRING, 
            118);

        pnd_Misc_Variables__R_Field_6 = pnd_Misc_Variables.newGroupInGroup("pnd_Misc_Variables__R_Field_6", "REDEFINE", pnd_Misc_Variables_Pnd_Terminate_Msg);
        pnd_Misc_Variables_Pnd_Terminate_Msg1 = pnd_Misc_Variables__R_Field_6.newFieldInGroup("pnd_Misc_Variables_Pnd_Terminate_Msg1", "#TERMINATE-MSG1", 
            FieldType.STRING, 59);
        pnd_Misc_Variables_Pnd_Terminate_Msg2 = pnd_Misc_Variables__R_Field_6.newFieldInGroup("pnd_Misc_Variables_Pnd_Terminate_Msg2", "#TERMINATE-MSG2", 
            FieldType.STRING, 59);
        pnd_Misc_Variables_Pnd_Total_Actl_Amt = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Total_Actl_Amt", "#TOTAL-ACTL-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Misc_Variables_Pnd_Total_Opn_Accum = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Total_Opn_Accum", "#TOTAL-OPN-ACCUM", FieldType.NUMERIC, 
            11, 2);
        pnd_Misc_Variables_Pnd_Process_Step = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Process_Step", "#PROCESS-STEP", FieldType.STRING, 
            35);
        pnd_Misc_Variables_Pnd_Display_Adsa401 = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Display_Adsa401", "#DISPLAY-ADSA401", FieldType.BOOLEAN, 
            1);
        pnd_Misc_Variables_Pnd_Display_Adsa450 = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Display_Adsa450", "#DISPLAY-ADSA450", FieldType.BOOLEAN, 
            1);
        pnd_Misc_Variables_Pnd_Comp_Dte = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Comp_Dte", "#COMP-DTE", FieldType.DATE);
        pnd_Misc_Variables_Pnd_Factor_Dte = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Factor_Dte", "#FACTOR-DTE", FieldType.STRING, 8);

        pnd_Misc_Variables__R_Field_7 = pnd_Misc_Variables.newGroupInGroup("pnd_Misc_Variables__R_Field_7", "REDEFINE", pnd_Misc_Variables_Pnd_Factor_Dte);
        pnd_Misc_Variables_Pnd_Factor_Dte_Yyyy = pnd_Misc_Variables__R_Field_7.newFieldInGroup("pnd_Misc_Variables_Pnd_Factor_Dte_Yyyy", "#FACTOR-DTE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Misc_Variables_Pnd_Factor_Dte_Mm = pnd_Misc_Variables__R_Field_7.newFieldInGroup("pnd_Misc_Variables_Pnd_Factor_Dte_Mm", "#FACTOR-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Misc_Variables_Pnd_Factor_Dte_Dd = pnd_Misc_Variables__R_Field_7.newFieldInGroup("pnd_Misc_Variables_Pnd_Factor_Dte_Dd", "#FACTOR-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Misc_Variables_Pnd_Compute_Dte = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Compute_Dte", "#COMPUTE-DTE", FieldType.STRING, 
            8);

        pnd_Misc_Variables__R_Field_8 = pnd_Misc_Variables.newGroupInGroup("pnd_Misc_Variables__R_Field_8", "REDEFINE", pnd_Misc_Variables_Pnd_Compute_Dte);
        pnd_Misc_Variables_Pnd_Compute_Dte_Yyyy = pnd_Misc_Variables__R_Field_8.newFieldInGroup("pnd_Misc_Variables_Pnd_Compute_Dte_Yyyy", "#COMPUTE-DTE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Misc_Variables_Pnd_Compute_Dte_Mm = pnd_Misc_Variables__R_Field_8.newFieldInGroup("pnd_Misc_Variables_Pnd_Compute_Dte_Mm", "#COMPUTE-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Misc_Variables_Pnd_Compute_Dte_Dd = pnd_Misc_Variables__R_Field_8.newFieldInGroup("pnd_Misc_Variables_Pnd_Compute_Dte_Dd", "#COMPUTE-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Misc_Variables_Pnd_Day_Of_Week = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Day_Of_Week", "#DAY-OF-WEEK", FieldType.STRING, 
            9);
        pnd_Misc_Variables_Pnd_Ipro_Tpa_Ind = pnd_Misc_Variables.newFieldArrayInGroup("pnd_Misc_Variables_Pnd_Ipro_Tpa_Ind", "#IPRO-TPA-IND", FieldType.STRING, 
            1, new DbsArrayController(1, 6));
        pnd_Misc_Variables_Pnd_L = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_L", "#L", FieldType.PACKED_DECIMAL, 3);
        pnd_Misc_Variables_Pnd_Wk_Cntrct_Lob = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Wk_Cntrct_Lob", "#WK-CNTRCT-LOB", FieldType.STRING, 
            10);
        pnd_Misc_Variables_Pnd_Origin_Lob_Ind = pnd_Misc_Variables.newFieldArrayInGroup("pnd_Misc_Variables_Pnd_Origin_Lob_Ind", "#ORIGIN-LOB-IND", FieldType.STRING, 
            1, new DbsArrayController(1, 12));

        pnd_Addit_450_Parameters = localVariables.newGroupInRecord("pnd_Addit_450_Parameters", "#ADDIT-450-PARAMETERS");
        pnd_Addit_450_Parameters_Pnd_Adp_Scnd_Annt_Rltnshp = pnd_Addit_450_Parameters.newFieldInGroup("pnd_Addit_450_Parameters_Pnd_Adp_Scnd_Annt_Rltnshp", 
            "#ADP-SCND-ANNT-RLTNSHP", FieldType.STRING, 1);
        pnd_Addit_450_Parameters_Pnd_Return_Message = pnd_Addit_450_Parameters.newFieldInGroup("pnd_Addit_450_Parameters_Pnd_Return_Message", "#RETURN-MESSAGE", 
            FieldType.STRING, 30);
        pnd_Addit_450_Parameters_Pnd_Return_Code = pnd_Addit_450_Parameters.newFieldInGroup("pnd_Addit_450_Parameters_Pnd_Return_Code", "#RETURN-CODE", 
            FieldType.STRING, 2);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_Military_Res_Codes = localVariables.newFieldArrayInRecord("pnd_Military_Res_Codes", "#MILITARY-RES-CODES", FieldType.STRING, 2, new DbsArrayController(1, 
            3));
        pnd_Request_Rejected = localVariables.newFieldInRecord("pnd_Request_Rejected", "#REQUEST-REJECTED", FieldType.STRING, 20);
        pnd_Request_Processed = localVariables.newFieldInRecord("pnd_Request_Processed", "#REQUEST-PROCESSED", FieldType.STRING, 20);
        pnd_Adas_Status_File = localVariables.newFieldInRecord("pnd_Adas_Status_File", "#ADAS-STATUS-FILE", FieldType.STRING, 361);

        pnd_Adas_Status_File__R_Field_9 = localVariables.newGroupInRecord("pnd_Adas_Status_File__R_Field_9", "REDEFINE", pnd_Adas_Status_File);
        pnd_Adas_Status_File_Pnd_Rqst_Id_35 = pnd_Adas_Status_File__R_Field_9.newFieldInGroup("pnd_Adas_Status_File_Pnd_Rqst_Id_35", "#RQST-ID-35", FieldType.STRING, 
            35);
        pnd_Adas_Status_File_Pnd_Ia_Tiaa_Cntrct_Nbr = pnd_Adas_Status_File__R_Field_9.newFieldInGroup("pnd_Adas_Status_File_Pnd_Ia_Tiaa_Cntrct_Nbr", "#IA-TIAA-CNTRCT-NBR", 
            FieldType.STRING, 10);
        pnd_Adas_Status_File_Pnd_Ia_Cref_Cntrct_Nbr = pnd_Adas_Status_File__R_Field_9.newFieldInGroup("pnd_Adas_Status_File_Pnd_Ia_Cref_Cntrct_Nbr", "#IA-CREF-CNTRCT-NBR", 
            FieldType.STRING, 10);
        pnd_Adas_Status_File_Pnd_Status = pnd_Adas_Status_File__R_Field_9.newFieldInGroup("pnd_Adas_Status_File_Pnd_Status", "#STATUS", FieldType.STRING, 
            50);
        pnd_Adas_Status_File_Pnd_Sub_Status = pnd_Adas_Status_File__R_Field_9.newFieldInGroup("pnd_Adas_Status_File_Pnd_Sub_Status", "#SUB-STATUS", FieldType.STRING, 
            256);
        pls_No_Of_Restart = WsIndependent.getInstance().newFieldInRecord("pls_No_Of_Restart", "+NO-OF-RESTART", FieldType.PACKED_DECIMAL, 3);
        pls_Cics_System_Name = WsIndependent.getInstance().newFieldInRecord("pls_Cics_System_Name", "+CICS-SYSTEM-NAME", FieldType.STRING, 4);
        pls_Tiaa_Rate_Cnt = WsIndependent.getInstance().newFieldInRecord("pls_Tiaa_Rate_Cnt", "+TIAA-RATE-CNT", FieldType.PACKED_DECIMAL, 3);
        pls_Cntl_Bsnss_Prev_Dte = WsIndependent.getInstance().newFieldInRecord("pls_Cntl_Bsnss_Prev_Dte", "+CNTL-BSNSS-PREV-DTE", FieldType.DATE);
        pls_Cntl_Bsnss_Dte = WsIndependent.getInstance().newFieldInRecord("pls_Cntl_Bsnss_Dte", "+CNTL-BSNSS-DTE", FieldType.DATE);
        pls_Cntl_Bsnss_Tmrrw_Dte = WsIndependent.getInstance().newFieldInRecord("pls_Cntl_Bsnss_Tmrrw_Dte", "+CNTL-BSNSS-TMRRW-DTE", FieldType.DATE);
        pls_Ia_Next_Cycle_Chk_Dte = WsIndependent.getInstance().newFieldInRecord("pls_Ia_Next_Cycle_Chk_Dte", "+IA-NEXT-CYCLE-CHK-DTE", FieldType.DATE);
        pls_First_Err_Rqstid = WsIndependent.getInstance().newFieldInRecord("pls_First_Err_Rqstid", "+FIRST-ERR-RQSTID", FieldType.STRING, 35);
        pls_Debug_On = WsIndependent.getInstance().newFieldInRecord("pls_Debug_On", "+DEBUG-ON", FieldType.BOOLEAN, 1);
        pls_Tot_System_Errors = WsIndependent.getInstance().newFieldInRecord("pls_Tot_System_Errors", "+TOT-SYSTEM-ERRORS", FieldType.PACKED_DECIMAL, 8);
        pls_Tot_Prtcpnt_Rqst = WsIndependent.getInstance().newFieldInRecord("pls_Tot_Prtcpnt_Rqst", "+TOT-PRTCPNT-RQST", FieldType.PACKED_DECIMAL, 8);
        pls_Tot_Cntrct_Recs = WsIndependent.getInstance().newFieldInRecord("pls_Tot_Cntrct_Recs", "+TOT-CNTRCT-RECS", FieldType.PACKED_DECIMAL, 8);
        pls_Tot_Rqst_Errors = WsIndependent.getInstance().newFieldInRecord("pls_Tot_Rqst_Errors", "+TOT-RQST-ERRORS", FieldType.PACKED_DECIMAL, 8);
        pls_Tot_Rqst_Successful = WsIndependent.getInstance().newFieldInRecord("pls_Tot_Rqst_Successful", "+TOT-RQST-SUCCESSFUL", FieldType.PACKED_DECIMAL, 
            8);
        pls_Tot_In_Progress = WsIndependent.getInstance().newFieldInRecord("pls_Tot_In_Progress", "+TOT-IN-PROGRESS", FieldType.PACKED_DECIMAL, 8);
        pls_Ticker = WsIndependent.getInstance().newFieldArrayInRecord("pls_Ticker", "+TICKER", FieldType.STRING, 10, new DbsArrayController(1, 100));
        pls_Invstmnt_Grpng_Id = WsIndependent.getInstance().newFieldArrayInRecord("pls_Invstmnt_Grpng_Id", "+INVSTMNT-GRPNG-ID", FieldType.STRING, 4, 
            new DbsArrayController(1, 100));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Ia_Rslt_View.reset();
        vw_naz_Table_Ddm.reset();

        ldaAdsl401.initializeValues();
        ldaAdsl401a.initializeValues();

        localVariables.reset();
        pnd_Constant_Variables_Pnd_Open_Ind.setInitialValue("O");
        pnd_Constant_Variables_Pnd_Ready_To_Process.setInitialValue("M10");
        pnd_Constant_Variables_Pnd_System_Error_Found.setInitialValue("M99");
        pnd_Constant_Variables_Pnd_Successful.setInitialValue("T");
        pnd_Constant_Variables_Pnd_Resettlement.setInitialValue("R");
        pnd_Constant_Variables_Pnd_Tiaa.setInitialValue("T");
        pnd_Constant_Variables_Pnd_Rea.setInitialValue("R");
        pnd_Constant_Variables_Pnd_Access.setInitialValue("D");
        pnd_Constant_Variables_Pnd_Batchads.setInitialValue("BATCHADS");
        pnd_Constant_Variables_Pnd_Update_Action.setInitialValue("MO");
        pnd_Constant_Variables_Pnd_Max_Cntrct.setInitialValue(12);
        pnd_Constant_Variables_Pnd_Inactive_Ia.setInitialValue("L22");
        pnd_Constant_Variables_Pnd_Missing_Ia.setInitialValue("L23");
        pnd_Constant_Variables_Pnd_Ac_Annty.setInitialValue("AC");
        pnd_Constant_Variables_Pnd_Database_Error.getValue(1).setInitialValue(3017);
        pnd_Constant_Variables_Pnd_Database_Error.getValue(2).setInitialValue(3018);
        pnd_Constant_Variables_Pnd_Tiaa_Tckr.setInitialValue("TIA0");
        pnd_Constant_Variables_Pnd_Stbl_Tckr.setInitialValue("TSA0");
        pnd_Constant_Variables_Pnd_Stbl_Value_Tckr.setInitialValue("TSV0");
        pnd_Constant_Variables_Pnd_Max_Rates.setInitialValue(250);
        pnd_Military_Res_Codes.getValue(1).setInitialValue("AP");
        pnd_Military_Res_Codes.getValue(2).setInitialValue("AE");
        pnd_Military_Res_Codes.getValue(3).setInitialValue("A1");
        pnd_Request_Rejected.setInitialValue("Platform Rejected");
        pnd_Request_Processed.setInitialValue("Platform Complete");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp401() throws Exception
    {
        super("Adsp401");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Adsp401|Main");
        OnErrorManager.pushEvent("ADSP401", onError);
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT LS = 120 PS = 100
                //*  PERFORM OPEN-MQ                            /*CM - FOR CALLS TO MDM
                //*  >> GET STATIC VARIABLE INFO TO BE USED LATER IN THE PROCESS <<
                //*  --------------------------------------------------------------
                if (condition(pls_No_Of_Restart.equals(getZero()), INPUT_1))                                                                                              //Natural: IF +NO-OF-RESTART EQ 0
                {
                    //*  OS-102013                /*CM - FOR CALLS TO MDM
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
                    sub_Open_Mq();
                    if (condition(Global.isEscape())) {return;}
                    //*  >>> TO RUN JOBS WITH DEBUG DISPLAYS, SET DEBUG-ON PARM IN JCL  <<<
                    //*  ==================================================================
                    DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pls_Debug_On);                                                                                     //Natural: INPUT +DEBUG-ON
                    //*  ==================================================================
                    //*  USED IN ERRL
                    pls_Cics_System_Name.setValue("BATCH");                                                                                                               //Natural: MOVE 'BATCH' TO +CICS-SYSTEM-NAME
                                                                                                                                                                          //Natural: PERFORM ADSN401-READ-IA-CONTROL
                    sub_Adsn401_Read_Ia_Control();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADSN402-READ-CONTROL-REC
                    sub_Adsn402_Read_Control_Rec();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    //*  OS-102013
                                                                                                                                                                          //Natural: PERFORM GET-TOTAL-FUND-COUNT
                    sub_Get_Total_Fund_Count();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    //*  PERFORM CHECK-IF-END-OF-MONTH /* ESV 07-13-04    /* EM - 051911
                }                                                                                                                                                         //Natural: END-IF
                pdaAdsa401.getPnd_Adsa401_Pnd_Cics_System_Name().setValue(pls_Cics_System_Name);                                                                          //Natural: MOVE +CICS-SYSTEM-NAME TO #ADSA401.#CICS-SYSTEM-NAME
                //* *MOVE +FUND-CNT              TO #ADSA401.#FUND-CNT   /* OS-102013
                //*  OS-102013
                pdaAdsa401.getPnd_Adsa401_Pnd_Fund_Cnt().setValue(20);                                                                                                    //Natural: MOVE 20 TO #ADSA401.#FUND-CNT
                //*  MOVE +TIAA-RATE-CNT         TO #ADSA401.#TIAA-RATE-CNT /* OS-102013
                //*  OS-102013
                pdaAdsa401.getPnd_Adsa401_Pnd_Tiaa_Rate_Cnt().setValue(pnd_Constant_Variables_Pnd_Max_Rates);                                                             //Natural: MOVE #MAX-RATES TO #ADSA401.#TIAA-RATE-CNT
                pdaAdsa401.getPnd_Adsa401_Pnd_Cntl_Bsnss_Dte().setValue(pls_Cntl_Bsnss_Dte);                                                                              //Natural: MOVE +CNTL-BSNSS-DTE TO #ADSA401.#CNTL-BSNSS-DTE
                pdaAdsa401.getPnd_Adsa401_Pnd_Cntl_Bsnss_Tmrrw_Dte().setValue(pls_Cntl_Bsnss_Tmrrw_Dte);                                                                  //Natural: MOVE +CNTL-BSNSS-TMRRW-DTE TO #ADSA401.#CNTL-BSNSS-TMRRW-DTE
                if (condition(pls_Debug_On.getBoolean()))                                                                                                                 //Natural: IF +DEBUG-ON
                {
                    getReports().write(0, "*************** STARTING PROCESS ****************");                                                                           //Natural: WRITE '*************** STARTING PROCESS ****************'
                    if (Global.isEscape()) return;
                    //*  OS-102013
                    //*  OS-102013
                    getReports().write(0, "=",pls_Cics_System_Name,"=",pdaAdsa401.getPnd_Adsa401_Pnd_Fund_Cnt(),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Tiaa_Rate_Cnt(),        //Natural: WRITE '=' +CICS-SYSTEM-NAME '=' #FUND-CNT '=' #TIAA-RATE-CNT '=' +CNTL-BSNSS-DTE '=' +CNTL-BSNSS-TMRRW-DTE '=' +IA-NEXT-CYCLE-CHK-DTE
                        "=",pls_Cntl_Bsnss_Dte,"=",pls_Cntl_Bsnss_Tmrrw_Dte,"=",pls_Ia_Next_Cycle_Chk_Dte);
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //* *********************************************************************
                //*          >> MAIN READ LOOP (READ ADS PARTICIPANT FILE) <<
                //* *********************************************************************
                pnd_Keys_Pnd_Super2_Opn_Clsd_Ind.setValue(pnd_Constant_Variables_Pnd_Open_Ind);                                                                           //Natural: MOVE #OPEN-IND TO #SUPER2-OPN-CLSD-IND
                pnd_Keys_Pnd_Super2_Stts_Cde.setValue(pnd_Constant_Variables_Pnd_Ready_To_Process);                                                                       //Natural: MOVE #READY-TO-PROCESS TO #SUPER2-STTS-CDE
                if (condition(pls_Debug_On.getBoolean()))                                                                                                                 //Natural: IF +DEBUG-ON
                {
                    getReports().write(0, ">>> BEFORE MAIN READ LOOP","=",pls_No_Of_Restart,NEWLINE,"*",new RepeatItem(100));                                             //Natural: WRITE '>>> BEFORE MAIN READ LOOP' '=' +NO-OF-RESTART / '*' ( 100 )
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("MAINREAD");                                                                                             //Natural: ASSIGN #STEP-NAME := 'MAINREAD'
                ldaAdsl401.getVw_ads_Prtcpnt_View().startDatabaseRead                                                                                                     //Natural: READ ADS-PRTCPNT-VIEW BY ADP-SUPER2 = #ADP-SUPER2
                (
                "READ_PRTCPNT",
                new Wc[] { new Wc("ADP_SUPER2", ">=", pnd_Keys_Pnd_Adp_Super2, WcType.BY) },
                new Oc[] { new Oc("ADP_SUPER2", "ASC") }
                );
                READ_PRTCPNT:
                while (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().readNextRow("READ_PRTCPNT")))
                {
                    if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind().notEquals(pnd_Constant_Variables_Pnd_Open_Ind) || ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().greater(pnd_Constant_Variables_Pnd_System_Error_Found))) //Natural: IF ADS-PRTCPNT-VIEW.ADP-OPN-CLSD-IND NE #OPEN-IND OR ADS-PRTCPNT-VIEW.ADP-STTS-CDE GT #SYSTEM-ERROR-FOUND
                    {
                        if (true) break READ_PRTCPNT;                                                                                                                     //Natural: ESCAPE BOTTOM ( READ-PRTCPNT. )
                    }                                                                                                                                                     //Natural: END-IF
                    //*  IF +DEBUG-ON
                    //*  RS0
                    getReports().write(0, "*",new RepeatItem(100),NEWLINE);                                                                                               //Natural: WRITE '*' ( 100 ) /
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ">>>> NEW REQUEST <<<<",NEWLINE,"STATUS : ",ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde(),"RQST-ID :",ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id(),"PIN : ",ldaAdsl401.getAds_Prtcpnt_View_Adp_Unique_Id(),"EFF-DATE : ",ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte(),  //Natural: WRITE '>>>> NEW REQUEST <<<<' / 'STATUS : ' ADS-PRTCPNT-VIEW.ADP-STTS-CDE 'RQST-ID :' ADS-PRTCPNT-VIEW.RQST-ID 'PIN : ' ADS-PRTCPNT-VIEW.ADP-UNIQUE-ID 'EFF-DATE : ' ADS-PRTCPNT-VIEW.ADP-EFFCTV-DTE ( EM = YYYYMMDD ) 'IN PROGRESS : ' ADS-PRTCPNT-VIEW.ADP-IN-PRGRSS-IND 'ANNT-TYPE: ' ADS-PRTCPNT-VIEW.ADP-ANNT-TYP-CDE 'ANNTY-OPTN : ' ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN 'LAST-UPDTD : ' ADS-PRTCPNT-VIEW.ADP-RQST-LAST-UPDTD-ID 'WPID : ' ADS-PRTCPNT-VIEW.ADP-WPID 'MIT-LOG-DTE: ' ADS-PRTCPNT-VIEW.ADP-MIT-LOG-DTE-TME 'USER :' ADS-PRTCPNT-VIEW.ADP-ENTRY-USER-ID 'ROTH-RQST  : ' ADS-PRTCPNT-VIEW.ADP-ROTH-RQST-IND 'ROTH-PLAN  : ' ADS-PRTCPNT-VIEW.ADP-ROTH-PLAN-TYPE 'ROTH-CNRB-D: ' ADS-PRTCPNT-VIEW.ADP-ROTH-FRST-CNTRBTN-DTE 'ROTH-DSBL-D: ' ADS-PRTCPNT-VIEW.ADP-ROTH-DSBLTY-DTE
                        new ReportEditMask ("YYYYMMDD"),"IN PROGRESS : ",ldaAdsl401.getAds_Prtcpnt_View_Adp_In_Prgrss_Ind(),"ANNT-TYPE: ",ldaAdsl401.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde(),
                        "ANNTY-OPTN : ",ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Optn(),"LAST-UPDTD : ",ldaAdsl401.getAds_Prtcpnt_View_Adp_Rqst_Last_Updtd_Id(),
                        "WPID : ",ldaAdsl401.getAds_Prtcpnt_View_Adp_Wpid(),"MIT-LOG-DTE: ",ldaAdsl401.getAds_Prtcpnt_View_Adp_Mit_Log_Dte_Tme(),"USER :",
                        ldaAdsl401.getAds_Prtcpnt_View_Adp_Entry_User_Id(),"ROTH-RQST  : ",ldaAdsl401.getAds_Prtcpnt_View_Adp_Roth_Rqst_Ind(),"ROTH-PLAN  : ",
                        ldaAdsl401.getAds_Prtcpnt_View_Adp_Roth_Plan_Type(),"ROTH-CNRB-D: ",ldaAdsl401.getAds_Prtcpnt_View_Adp_Roth_Frst_Cntrbtn_Dte(),"ROTH-DSBL-D: ",
                        ldaAdsl401.getAds_Prtcpnt_View_Adp_Roth_Dsblty_Dte());
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "*",new RepeatItem(100));                                                                                                       //Natural: WRITE '*' ( 100 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*    RESET #DISPLAY-ADSA401
                    //*  END-IF
                    //*     >> RESET VARIABLES FOR EVERY NEW PARTICIPANT <<
                    //*     -----------------------------------------------
                    pdaAdsa401.getPnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd().reset();                                                                                         //Natural: RESET #ADSA401-PRTCPNT-RCRD #ADSA401-CNTRCT-RCRD #ADSA401-VARIABLES #ADSA470-PRTCPNT-RCRD #ADSA470-CNTRCT-RCRD ( * ) #ADSA470-OTHER-VARIABLES #COUNTERS #LOGICALS #ADSA043-CALL-CIS #ADSA450-ACTUARIAL-DATA
                    pdaAdsa401.getPnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd().reset();
                    pdaAdsa401.getPnd_Adsa401_Pnd_Adsa401_Variables().reset();
                    pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Prtcpnt_Rcrd().reset();
                    pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Cntrct_Rcrd().getValue("*").reset();
                    pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Other_Variables().reset();
                    pnd_Counters.reset();
                    pnd_Logicals.reset();
                    pnd_Adsa043_Call_Cis.reset();
                    pdaAdsa450.getPnd_Adsa450_Actuarial_Data().reset();
                    pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Ia_Next_Cycle_Check_Dte().setValue(pls_Ia_Next_Cycle_Chk_Dte);                                           //Natural: MOVE +IA-NEXT-CYCLE-CHK-DTE TO #IA-NEXT-CYCLE-CHECK-DTE
                    pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Prtcpnt_Isn().setValue(ldaAdsl401.getVw_ads_Prtcpnt_View().getAstISN("READ_PRTCPNT"));                              //Natural: MOVE *ISN ( READ-PRTCPNT. ) TO #ADSA401.#CUR-PRTCPNT-ISN
                    pls_Tot_Prtcpnt_Rqst.nadd(1);                                                                                                                         //Natural: ADD 1 TO +TOT-PRTCPNT-RQST
                    //*     >> CHECK IF IA PROCESS COMPLETED BUT FAILED TO UPDATE KDO <<
                    //*     ------------------------------------------------------------
                    if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_In_Prgrss_Ind().getBoolean()))                                                                       //Natural: IF ADS-PRTCPNT-VIEW.ADP-IN-PRGRSS-IND
                    {
                        pls_Tot_In_Progress.nadd(1);                                                                                                                      //Natural: ADD 1 TO +TOT-IN-PROGRESS
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-IA-PROCESS-COMPLETED
                        sub_Check_If_Ia_Process_Completed();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(pnd_Logicals_Pnd_Ia_Process_Completed.getBoolean()))                                                                                //Natural: IF #IA-PROCESS-COMPLETED
                        {
                                                                                                                                                                          //Natural: PERFORM COMPLETE-KDO-MIT-UPDATE
                            sub_Complete_Kdo_Mit_Update();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            //*  EM - IA17
                                                                                                                                                                          //Natural: PERFORM WRITE-STATUS-RECORD
                            sub_Write_Status_Record();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    PND_PND_L2085:                                                                                                                                        //Natural: GET ADS-PRTCPNT-VIEW #ADSA401.#CUR-PRTCPNT-ISN
                    ldaAdsl401.getVw_ads_Prtcpnt_View().readByID(pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Prtcpnt_Isn().getLong(), "PND_PND_L2085");
                    //*  IF +EOM-INDR    /* ESV 07-21-04         /* EM - 051911 START
                    //*  MOVE +EOM-FACTOR-DTE TO ADS-PRTCPNT-VIEW.ADP-LST-ACTVTY-DTE
                    //*  ELSE                                    /* EM - 051911 END
                    ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte().setValue(pdaAdsa401.getPnd_Adsa401_Pnd_Cntl_Bsnss_Dte());                                         //Natural: MOVE #ADSA401.#CNTL-BSNSS-DTE TO ADS-PRTCPNT-VIEW.ADP-LST-ACTVTY-DTE
                    //*  END-IF                                  /* EM - 051911
                    ldaAdsl401.getAds_Prtcpnt_View_Adp_In_Prgrss_Ind().setValue(true);                                                                                    //Natural: MOVE TRUE TO ADS-PRTCPNT-VIEW.ADP-IN-PRGRSS-IND
                    ldaAdsl401.getVw_ads_Prtcpnt_View().updateDBRow("PND_PND_L2085");                                                                                     //Natural: UPDATE ( ##L2085. )
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                    pdaAdsa401.getPnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd().setValuesByName(ldaAdsl401.getVw_ads_Prtcpnt_View());                                            //Natural: MOVE BY NAME ADS-PRTCPNT-VIEW TO #ADSA401-PRTCPNT-RCRD
                    //*     >> RETRIEVE IA CONTRACT LOB/SUB-LOB  <<
                    //*     ---------------------------------------
                    pnd_Get_Lob.reset();                                                                                                                                  //Natural: RESET #GET-LOB
                    pnd_Get_Lob_Pnd_Lob_Cntrct.setValue(pdaAdsa401.getPnd_Adsa401_Adp_Ia_Tiaa_Nbr());                                                                     //Natural: MOVE #ADSA401.ADP-IA-TIAA-NBR TO #LOB-CNTRCT
                                                                                                                                                                          //Natural: PERFORM GET-CNTRCT-LOB
                    sub_Get_Cntrct_Lob();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    pdaAdsa401.getPnd_Adsa401_Pnd_Ia_Cntrct_Lob().setValue(pnd_Get_Lob_Pnd_Lob);                                                                          //Natural: MOVE #LOB TO #ADSA401.#IA-CNTRCT-LOB
                    pdaAdsa401.getPnd_Adsa401_Pnd_Ia_Cntrct_Sub_Lob().setValue(pnd_Get_Lob_Pnd_Sub_Lob);                                                                  //Natural: MOVE #SUB-LOB TO #ADSA401.#IA-CNTRCT-SUB-LOB
                    //*     >> FIND MATCHING CONTRACT RECORD IN ADS-CNTRCT FILE <<
                    //*     --------------------------------------------------------
                    //*  08312005 MN
                    pnd_Misc_Variables_Pnd_L.reset();                                                                                                                     //Natural: RESET #L
                    pnd_Misc_Variables_Pnd_Process_Step.setValue("FIND ADS CNTRCT");                                                                                      //Natural: MOVE 'FIND ADS CNTRCT' TO #PROCESS-STEP
                    pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("FINDCNTR");                                                                                         //Natural: ASSIGN #STEP-NAME := 'FINDCNTR'
                    ldaAdsl401a.getVw_ads_Cntrct_View().startDatabaseFind                                                                                                 //Natural: FIND ADS-CNTRCT-VIEW WITH ADS-CNTRCT-VIEW.RQST-ID = #ADSA401.RQST-ID
                    (
                    "FIND_CNTRCT",
                    new Wc[] { new Wc("RQST_ID", "=", pdaAdsa401.getPnd_Adsa401_Rqst_Id(), WcType.WITH) }
                    );
                    FIND_CNTRCT:
                    while (condition(ldaAdsl401a.getVw_ads_Cntrct_View().readNextRow("FIND_CNTRCT", true)))
                    {
                        ldaAdsl401a.getVw_ads_Cntrct_View().setIfNotFoundControlFlag(false);
                        if (condition(ldaAdsl401a.getVw_ads_Cntrct_View().getAstCOUNTER().equals(0)))                                                                     //Natural: IF NO RECORDS FOUND
                        {
                            pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L03");                                                                                 //Natural: MOVE 'L03' TO #ERROR-STATUS
                            if (true) break FIND_CNTRCT;                                                                                                                  //Natural: ESCAPE BOTTOM ( FIND-CNTRCT. )
                        }                                                                                                                                                 //Natural: END-NOREC
                        if (condition(pnd_Counters_Pnd_Cntrct_Cnt.equals(pnd_Constant_Variables_Pnd_Max_Cntrct)))                                                         //Natural: IF #CNTRCT-CNT EQ #MAX-CNTRCT
                        {
                            pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L07");                                                                                 //Natural: MOVE 'L07' TO #ERROR-STATUS
                            if (true) break FIND_CNTRCT;                                                                                                                  //Natural: ESCAPE BOTTOM ( FIND-CNTRCT. )
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Counters_Pnd_Cntrct_Cnt.nadd(1);                                                                                                              //Natural: ADD 1 TO #CNTRCT-CNT
                        pls_Tot_Cntrct_Recs.nadd(1);                                                                                                                      //Natural: ADD 1 TO +TOT-CNTRCT-RECS
                        pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Cntrct_Isn().setValue(ldaAdsl401a.getVw_ads_Cntrct_View().getAstISN("FIND_CNTRCT"));                            //Natural: MOVE *ISN ( FIND-CNTRCT. ) TO #ADSA401.#CUR-CNTRCT-ISN
                        //* * MN NEW TPA FIELDS IN #ADSA401-CNTRCT-RCRD
                        pdaAdsa401.getPnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd().setValuesByName(ldaAdsl401a.getVw_ads_Cntrct_View());                                         //Natural: MOVE BY NAME ADS-CNTRCT-VIEW TO #ADSA401-CNTRCT-RCRD
                        pdaAdsa401.getPnd_Adsa401_Adc_Rqst_Id().setValue(ldaAdsl401a.getAds_Cntrct_View_Rqst_Id());                                                       //Natural: MOVE ADS-CNTRCT-VIEW.RQST-ID TO #ADSA401.ADC-RQST-ID
                        //*          >>> CHECK IF VALID CONTRACT - EXISTS IN COR <<<
                        //*          -----------------------------------------------
                        //*  OS-102013 START
                        //*    MOVE 'VALIDATE COR CONTRACT' TO #PROCESS-STEP
                        //*    WRITE '=' #ADSA401.ADP-ANNT-TYP-CDE  /* TEMPOS
                        //*    IF #ADSA401.ADP-ANNT-TYP-CDE NE #RESETTLEMENT  /* 101112
                        //*      PERFORM VALIDATE-CONTRACT-IN-COR
                        //*      IF NOT #CNTRCT-IN-COR
                        //*        MOVE 'L17' TO #ERROR-STATUS
                        //*        ESCAPE BOTTOM (2205)
                        //*      END-IF
                        //*    END-IF                                         /* 101112
                        //*  OS-102013 END
                        //*          >>> CHECK IF TOTAL TIAA RATE AMOUNTS EQUALS FUND AMOUNT <<<
                        //*          -----------------------------------------------------------
                        pnd_Misc_Variables_Pnd_Process_Step.setValue("CHECK TOTAL TIAA/STBL RATE AMTS");                                                                  //Natural: MOVE 'CHECK TOTAL TIAA/STBL RATE AMTS' TO #PROCESS-STEP
                        pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("CHK-RATE");                                                                                     //Natural: ASSIGN #STEP-NAME := 'CHK-RATE'
                        //*    EXAMINE ADS-CNTRCT-VIEW.ADC-TKR-SYMBL(*) FOR #TIAA-TCKR
                        //*      GIVING INDEX #I
                        //*    IF #I = 0
                        //*      EXAMINE ADS-CNTRCT-VIEW.ADC-TKR-SYMBL(*) FOR #STBL-TCKR
                        //*        GIVING INDEX #I
                        //*    END-IF
                        //*    IF #I = 0
                        //*      EXAMINE ADS-CNTRCT-VIEW.ADC-TKR-SYMBL(*) FOR #STBL-VALUE-TCKR
                        //*        GIVING INDEX #I
                        //*    END-IF
                        pnd_Counters_Pnd_I.setValue(0);                                                                                                                   //Natural: ASSIGN #I := 0
                        if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Invstmnt_Grpng_Id().getValue("*").equals(pnd_Constant_Variables_Pnd_Tiaa_Tckr)                   //Natural: IF ADS-CNTRCT-VIEW.ADC-INVSTMNT-GRPNG-ID ( * ) = #TIAA-TCKR OR = #STBL-TCKR OR = #STBL-VALUE-TCKR
                            || ldaAdsl401a.getAds_Cntrct_View_Adc_Invstmnt_Grpng_Id().getValue("*").equals(pnd_Constant_Variables_Pnd_Stbl_Tckr) || ldaAdsl401a.getAds_Cntrct_View_Adc_Invstmnt_Grpng_Id().getValue("*").equals(pnd_Constant_Variables_Pnd_Stbl_Value_Tckr)))
                        {
                            DbsUtil.examine(new ExamineSource(ldaAdsl401a.getAds_Cntrct_View_Adc_Invstmnt_Grpng_Id().getValue("*")), new ExamineSearch(pnd_Constant_Variables_Pnd_Tiaa_Tckr),  //Natural: EXAMINE ADS-CNTRCT-VIEW.ADC-INVSTMNT-GRPNG-ID ( * ) FOR #TIAA-TCKR GIVING INDEX #P
                                new ExamineGivingIndex(pnd_Counters_Pnd_P));
                            if (condition(pnd_Counters_Pnd_P.equals(getZero())))                                                                                          //Natural: IF #P = 0
                            {
                                DbsUtil.examine(new ExamineSource(ldaAdsl401a.getAds_Cntrct_View_Adc_Invstmnt_Grpng_Id().getValue("*")), new ExamineSearch(pnd_Constant_Variables_Pnd_Stbl_Tckr),  //Natural: EXAMINE ADS-CNTRCT-VIEW.ADC-INVSTMNT-GRPNG-ID ( * ) FOR #STBL-TCKR GIVING INDEX #P
                                    new ExamineGivingIndex(pnd_Counters_Pnd_P));
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pnd_Counters_Pnd_P.equals(getZero())))                                                                                          //Natural: IF #P = 0
                            {
                                DbsUtil.examine(new ExamineSource(ldaAdsl401a.getAds_Cntrct_View_Adc_Invstmnt_Grpng_Id().getValue("*")), new ExamineSearch(pnd_Constant_Variables_Pnd_Stbl_Value_Tckr),  //Natural: EXAMINE ADS-CNTRCT-VIEW.ADC-INVSTMNT-GRPNG-ID ( * ) FOR #STBL-VALUE-TCKR GIVING INDEX #P
                                    new ExamineGivingIndex(pnd_Counters_Pnd_P));
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pnd_Counters_Pnd_P.greater(getZero())))                                                                                         //Natural: IF #P GT 0
                            {
                                DbsUtil.examine(new ExamineSource(ldaAdsl401a.getAds_Cntrct_View_Adc_Tkr_Symbl().getValue("*")), new ExamineSearch(ldaAdsl401a.getAds_Cntrct_View_Adc_T395_Ticker().getValue(pnd_Counters_Pnd_P)),  //Natural: EXAMINE ADS-CNTRCT-VIEW.ADC-TKR-SYMBL ( * ) FOR ADS-CNTRCT-VIEW.ADC-T395-TICKER ( #P ) GIVING INDEX #I
                                    new ExamineGivingIndex(pnd_Counters_Pnd_I));
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        //*  OS-102013 END
                        if (condition(pnd_Counters_Pnd_I.notEquals(getZero())))                                                                                           //Natural: IF #I NE 0
                        {
                            pnd_Misc_Variables_Pnd_Total_Actl_Amt.reset();                                                                                                //Natural: RESET #TOTAL-ACTL-AMT #TOTAL-OPN-ACCUM
                            pnd_Misc_Variables_Pnd_Total_Opn_Accum.reset();
                            pnd_Misc_Variables_Pnd_Total_Actl_Amt.nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Actl_Amt().getValue("*"));                             //Natural: ADD ADS-CNTRCT-VIEW.ADC-DTL-TIAA-ACTL-AMT ( * ) TO #TOTAL-ACTL-AMT
                            //*                                      /* COMMENT OUT PER E. MELNIK 6-15
                            //*             ADD ADS-CNTRCT-VIEW.ADC-DTL-TIAA-OPN-ACCUM-AMT(*) TO
                            //*                 #TOTAL-OPN-ACCUM
                            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_Counters_Pnd_I).notEquals(pnd_Misc_Variables_Pnd_Total_Actl_Amt))) //Natural: IF ADS-CNTRCT-VIEW.ADC-ACCT-ACTL-AMT ( #I ) NE #TOTAL-ACTL-AMT
                            {
                                //*                OR ADS-CNTRCT-VIEW.ADC-ACCT-OPN-ACCUM-AMT(#I) NE
                                //*                #TOTAL-OPN-ACCUM
                                //*  ESV 08-23-04
                                if (condition(pls_Debug_On.getBoolean()))                                                                                                 //Natural: IF +DEBUG-ON
                                {
                                    getReports().write(0, ">>>> CHECK TOTAL TIAA/STBL RATE AMOUNTS <<<<<",NEWLINE,"=",ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_Counters_Pnd_I), //Natural: WRITE '>>>> CHECK TOTAL TIAA/STBL RATE AMOUNTS <<<<<' / '=' ADS-CNTRCT-VIEW.ADC-DTL-TIAA-RATE-CDE ( #I ) / '=' ADS-CNTRCT-VIEW.ADC-ACCT-ACTL-AMT ( #I ) / '=' #TOTAL-ACTL-AMT
                                        NEWLINE,"=",ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_Counters_Pnd_I),NEWLINE,"=",pnd_Misc_Variables_Pnd_Total_Actl_Amt);
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom("FIND_CNTRCT"))) break;
                                        else if (condition(Global.isEscapeBottomImmediate("FIND_CNTRCT"))) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: END-IF
                                pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L16");                                                                             //Natural: MOVE 'L16' TO #ERROR-STATUS
                                if (true) break FIND_CNTRCT;                                                                                                              //Natural: ESCAPE BOTTOM ( FIND-CNTRCT. )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        //*  CM
                        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Frst_Annt_Rsdnc_Cde().equals(pnd_Military_Res_Codes.getValue("*"))))                                  //Natural: IF #ADSA401.ADP-FRST-ANNT-RSDNC-CDE = #MILITARY-RES-CODES ( * )
                        {
                            pnd_Adsa043_Call_Cis_Pnd_Issue_State_Code.getValue(pnd_Counters_Pnd_Cntrct_Cnt).setValue("NY");                                               //Natural: MOVE 'NY' TO #ADSA043-CALL-CIS.#ISSUE-STATE-CODE ( #CNTRCT-CNT )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Adsa043_Call_Cis_Pnd_Issue_State_Code.getValue(pnd_Counters_Pnd_Cntrct_Cnt).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Cntrct_Issue_State()); //Natural: MOVE #ADSA401.ADC-CNTRCT-ISSUE-STATE TO #ADSA043-CALL-CIS.#ISSUE-STATE-CODE ( #CNTRCT-CNT )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Cntrct_Issue_Dte().notEquals(getZero())))                                                             //Natural: IF #ADSA401.ADC-CNTRCT-ISSUE-DTE NE 0
                        {
                            pnd_Misc_Variables_Pnd_Date_A.setValueEdited(pdaAdsa401.getPnd_Adsa401_Adc_Cntrct_Issue_Dte(),new ReportEditMask("YYYYMMDD"));                //Natural: MOVE EDITED #ADSA401.ADC-CNTRCT-ISSUE-DTE ( EM = YYYYMMDD ) TO #DATE-A
                            pnd_Adsa043_Call_Cis_Pnd_Issue_Date.getValue(pnd_Counters_Pnd_Cntrct_Cnt).setValue(pnd_Misc_Variables_Pnd_Date_N);                            //Natural: MOVE #DATE-N TO #ADSA043-CALL-CIS.#ISSUE-DATE ( #CNTRCT-CNT )
                        }                                                                                                                                                 //Natural: END-IF
                        //*          >>> RETRIEVE DA CONTRACT LOB/SUB-LOB  <<<
                        //*          -----------------------------------------
                        pnd_Get_Lob_Pnd_Lob_Cntrct.setValue(pdaAdsa401.getPnd_Adsa401_Adc_Tiaa_Nbr());                                                                    //Natural: MOVE #ADSA401.ADC-TIAA-NBR TO #LOB-CNTRCT
                                                                                                                                                                          //Natural: PERFORM GET-CNTRCT-LOB
                        sub_Get_Cntrct_Lob();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FIND_CNTRCT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FIND_CNTRCT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Lob().setValue(pnd_Get_Lob_Pnd_Lob);                                                                         //Natural: MOVE #LOB TO #ADSA401.#CNTRCT-LOB
                        pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Sub_Lob().setValue(pnd_Get_Lob_Pnd_Sub_Lob);                                                                 //Natural: MOVE #SUB-LOB TO #ADSA401.#CNTRCT-SUB-LOB
                        //*          >>> ADAS CONTRACT EDITS <<<
                        //*          ---------------------------
                        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Annt_Typ_Cde().notEquals(pnd_Constant_Variables_Pnd_Resettlement)))                                   //Natural: IF #ADSA401.ADP-ANNT-TYP-CDE NE #RESETTLEMENT
                        {
                            pnd_Misc_Variables_Pnd_Process_Step.setValue("CONTRACT EDITS-ADSN410");                                                                       //Natural: MOVE 'CONTRACT EDITS-ADSN410' TO #PROCESS-STEP
                            pnd_Adsn998_Error_Handler_Pnd_Calling_Program.setValue("ADSN410");                                                                            //Natural: ASSIGN #CALLING-PROGRAM := 'ADSN410'
                            pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("CONTEDIT");                                                                                 //Natural: ASSIGN #STEP-NAME := 'CONTEDIT'
                            DbsUtil.callnat(Adsn410.class , getCurrentProcessState(), pdaAdsa401.getPnd_Adsa401());                                                       //Natural: CALLNAT 'ADSN410' #ADSA401
                            if (condition(Global.isEscape())) return;
                            if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().notEquals(" ")))                                                                   //Natural: IF #ERROR-STATUS NE ' '
                            {
                                if (true) break FIND_CNTRCT;                                                                                                              //Natural: ESCAPE BOTTOM ( FIND-CNTRCT. )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        //*          >>> CALCULATE SETTLEMENTS <<<
                        //*          -----------------------------
                        pnd_Misc_Variables_Pnd_Process_Step.setValue("CALCULATE SETTLEMENT-ADSN420");                                                                     //Natural: MOVE 'CALCULATE SETTLEMENT-ADSN420' TO #PROCESS-STEP
                        pnd_Adsn998_Error_Handler_Pnd_Calling_Program.setValue("ADSN420");                                                                                //Natural: ASSIGN #CALLING-PROGRAM := 'ADSN420'
                        pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("CALCSTTL");                                                                                     //Natural: ASSIGN #STEP-NAME := 'CALCSTTL'
                        if (condition(pls_Debug_On.getBoolean()))                                                                                                         //Natural: IF +DEBUG-ON
                        {
                            pnd_Misc_Variables_Pnd_Display_Adsa401.setValue(true);                                                                                        //Natural: MOVE TRUE TO #DISPLAY-ADSA401
                        }                                                                                                                                                 //Natural: END-IF
                        DbsUtil.callnat(Adsn420.class , getCurrentProcessState(), pdaAdsa401.getPnd_Adsa401());                                                           //Natural: CALLNAT 'ADSN420' #ADSA401
                        if (condition(Global.isEscape())) return;
                        if (condition(pls_Debug_On.getBoolean()))                                                                                                         //Natural: IF +DEBUG-ON
                        {
                            pnd_Misc_Variables_Pnd_Display_Adsa401.reset();                                                                                               //Natural: RESET #DISPLAY-ADSA401
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().notEquals(" ")))                                                                       //Natural: IF #ERROR-STATUS NE ' '
                        {
                            if (condition(pls_Debug_On.getBoolean()))                                                                                                     //Natural: IF +DEBUG-ON
                            {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-#ADSA401
                                sub_Display_Pnd_Adsa401();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("FIND_CNTRCT"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("FIND_CNTRCT"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(Map.getDoInput())) {return;}
                            }                                                                                                                                             //Natural: END-IF
                            if (true) break FIND_CNTRCT;                                                                                                                  //Natural: ESCAPE BOTTOM ( FIND-CNTRCT. )
                            //*                           /* REMOVE UPDATE OF CONTRACT REC. ESV 6-16-04
                            //*         ELSE
                            //*             >>> UPDATE ADS-CNTRCT RECORD WITH RESULTS FROM ADSN420 <<<
                            //*             -----------------------------------------------------------
                            //*             GET ADS-CNTRCT-VIEW #ADSA401.#CUR-CNTRCT-ISN
                            //*             MOVE BY NAME #ADSA401-CNTRCT-RCRD TO ADS-CNTRCT-VIEW
                            //*             UPDATE (2940)
                            //*             END TRANSACTION
                        }                                                                                                                                                 //Natural: END-IF
                        //*          >>> CALM INTERFACE <<<              /* EM - CALM15 START
                        //*          ----------------------
                        //*    MOVE 'CALM INTERFACE-ADSN430' TO #PROCESS-STEP
                        //*    #CALLING-PROGRAM := 'ADSN430'
                        //*    #STEP-NAME       := 'CALLCALM'
                        //*    IF +DEBUG-ON
                        //*      MOVE TRUE TO #DISPLAY-ADSA401
                        //*    END-IF
                        //*    WRITE 'BEFORE CALL TO ADSN430'                      /*DEA
                        //*    WRITE '=' ADS-PRTCPNT-VIEW.ADP-SRVVR-IND                /*DEA
                        //*    CALLNAT 'ADSN430' #ADSA401
                        //*    IF +DEBUG-ON
                        //*      RESET #DISPLAY-ADSA401
                        //*    END-IF
                        //*    IF #ERROR-STATUS NE ' '
                        //*      PERFORM DISPLAY-#ADSA401
                        //*      ESCAPE BOTTOM (2205)
                        //*    END-IF
                        //*                                              /* EM - CALM15 END
                        //*          >>> SUMMARIZE SETTLEMENTS <<<
                        //*          -----------------------------
                        pnd_Misc_Variables_Pnd_Process_Step.setValue("SUMMARIZE SETTLEMENT-ADSN440");                                                                     //Natural: MOVE 'SUMMARIZE SETTLEMENT-ADSN440' TO #PROCESS-STEP
                        pnd_Adsn998_Error_Handler_Pnd_Calling_Program.setValue("ADSN440");                                                                                //Natural: ASSIGN #CALLING-PROGRAM := 'ADSN440'
                        pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("SUMSETTL");                                                                                     //Natural: ASSIGN #STEP-NAME := 'SUMSETTL'
                        if (condition(pls_Debug_On.getBoolean()))                                                                                                         //Natural: IF +DEBUG-ON
                        {
                            pnd_Misc_Variables_Pnd_Display_Adsa401.setValue(true);                                                                                        //Natural: MOVE TRUE TO #DISPLAY-ADSA401 #DISPLAY-ADSA450
                            pnd_Misc_Variables_Pnd_Display_Adsa450.setValue(true);
                        }                                                                                                                                                 //Natural: END-IF
                        DbsUtil.callnat(Adsn440.class , getCurrentProcessState(), pdaAdsa401.getPnd_Adsa401(), pdaAdsa450.getPnd_Adsa450_Actuarial_Data());               //Natural: CALLNAT 'ADSN440' #ADSA401 #ADSA450-ACTUARIAL-DATA
                        if (condition(Global.isEscape())) return;
                        if (condition(pls_Debug_On.getBoolean()))                                                                                                         //Natural: IF +DEBUG-ON
                        {
                            pnd_Misc_Variables_Pnd_Display_Adsa401.reset();                                                                                               //Natural: RESET #DISPLAY-ADSA401 #DISPLAY-ADSA450
                            pnd_Misc_Variables_Pnd_Display_Adsa450.reset();
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().notEquals(" ")))                                                                       //Natural: IF #ERROR-STATUS NE ' '
                        {
                            if (condition(pls_Debug_On.getBoolean()))                                                                                                     //Natural: IF +DEBUG-ON
                            {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-#ADSA401
                                sub_Display_Pnd_Adsa401();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("FIND_CNTRCT"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("FIND_CNTRCT"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM DISPLAY-#ADSA450
                                sub_Display_Pnd_Adsa450();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("FIND_CNTRCT"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("FIND_CNTRCT"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(Map.getDoInput())) {return;}
                            }                                                                                                                                             //Natural: END-IF
                            if (true) break FIND_CNTRCT;                                                                                                                  //Natural: ESCAPE BOTTOM ( FIND-CNTRCT. )
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM MOVE-CNTRCT-DATA-FOR-UPDT
                        sub_Move_Cntrct_Data_For_Updt();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FIND_CNTRCT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FIND_CNTRCT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //* * 08312005 MN START
                        pnd_Misc_Variables_Pnd_L.nadd(1);                                                                                                                 //Natural: ADD 1 TO #L
                        pnd_Misc_Variables_Pnd_Origin_Lob_Ind.getValue(pnd_Misc_Variables_Pnd_L).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Orig_Lob_Ind());                  //Natural: ASSIGN #ORIGIN-LOB-IND ( #L ) := #ADSA401.ADC-ORIG-LOB-IND
                        //* * 08312005 MN END
                        //*  END OF CONTRACT LOOP
                    }                                                                                                                                                     //Natural: END-FIND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*    >>> CHECK IF THERE's reject in contract <<<
                    //*    -------------------------------------------
                    if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().notEquals(" ")))                                                                           //Natural: IF #ERROR-STATUS NE ' '
                    {
                        //*  EM - IA17
                                                                                                                                                                          //Natural: PERFORM WRITE-STATUS-RECORD
                        sub_Write_Status_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM MOVE-CNTRCT-DATA-FOR-UPDT
                        sub_Move_Cntrct_Data_For_Updt();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADSN470-UPDATE-PRCTPNT-CNTRCT
                        sub_Adsn470_Update_Prctpnt_Cntrct();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADSN480-UPDATE-MIT-RECORD
                        sub_Adsn480_Update_Mit_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                        //* *  08312005 MN  START
                        //*  IPROS ORGINATING FROM TPAS CANNOT BE COMBINED WITH ANY OTHER
                        //*  CONTRACTS. 1 - 'IPRO from TPA'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Misc_Variables_Pnd_Origin_Lob_Ind.getValue(1,":",pnd_Counters_Pnd_Cntrct_Cnt).equals("1") && pnd_Misc_Variables_Pnd_Origin_Lob_Ind.getValue(1, //Natural: IF #ORIGIN-LOB-IND ( 1:#CNTRCT-CNT ) = '1' AND #ORIGIN-LOB-IND ( 1:#CNTRCT-CNT ) NE '1'
                            ":",pnd_Counters_Pnd_Cntrct_Cnt).notEquals("1")))
                        {
                            pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L18");                                                                                 //Natural: ASSIGN #ERROR-STATUS := 'L18'
                            //*  EM - IA17
                                                                                                                                                                          //Natural: PERFORM WRITE-STATUS-RECORD
                            sub_Write_Status_Record();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM MOVE-CNTRCT-DATA-FOR-UPDT
                            sub_Move_Cntrct_Data_For_Updt();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADSN470-UPDATE-PRCTPNT-CNTRCT
                            sub_Adsn470_Update_Prctpnt_Cntrct();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADSN480-UPDATE-MIT-RECORD
                            sub_Adsn480_Update_Mit_Record();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                        //* *  08312005 MN  END
                    }                                                                                                                                                     //Natural: END-IF
                    //*     >>> CHECK IVC ROLLOVER ELIGIBILITY RULES <<<
                    //*     --------------------------------------------
                    if (condition(pls_Debug_On.getBoolean()))                                                                                                             //Natural: IF +DEBUG-ON
                    {
                        getReports().write(0, ">>> CHK IVC ROLLOVER-ADSN404","=",pdaAdsa401.getPnd_Adsa401_Pnd_Total_Tiaa_Ivc_Amount(),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Total_Cref_Ivc_Amount(),"=",pdaAdsa401.getPnd_Adsa401_Adp_Annty_Optn(),"=",pdaAdsa401.getPnd_Adsa401_Adp_Alt_Dest_Rlvr_Ind(),NEWLINE,"*",new  //Natural: WRITE '>>> CHK IVC ROLLOVER-ADSN404' '=' #TOTAL-TIAA-IVC-AMOUNT '=' #TOTAL-CREF-IVC-AMOUNT '=' #ADSA401.ADP-ANNTY-OPTN '=' #ADSA401.ADP-ALT-DEST-RLVR-IND /'*' ( 100 )
                            RepeatItem(100));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Total_Tiaa_Ivc_Amount().greater(getZero()) || pdaAdsa401.getPnd_Adsa401_Pnd_Total_Cref_Ivc_Amount().greater(getZero()))) //Natural: IF #TOTAL-TIAA-IVC-AMOUNT > 0 OR #TOTAL-CREF-IVC-AMOUNT > 0
                    {
                        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Annty_Optn().equals(pnd_Constant_Variables_Pnd_Ac_Annty) && pdaAdsa401.getPnd_Adsa401_Adp_Alt_Dest_Rlvr_Ind().equals("Y"))) //Natural: IF #ADSA401.ADP-ANNTY-OPTN = #AC-ANNTY AND #ADSA401.ADP-ALT-DEST-RLVR-IND = 'Y'
                        {
                            pnd_Misc_Variables_Pnd_Process_Step.setValue("CHK IVC ROLLOVER-ADSN404");                                                                     //Natural: MOVE 'CHK IVC ROLLOVER-ADSN404' TO #PROCESS-STEP
                            pnd_Adsn998_Error_Handler_Pnd_Calling_Program.setValue("ADSN404");                                                                            //Natural: ASSIGN #CALLING-PROGRAM := 'ADSN404'
                            pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("IVC-ROLL");                                                                                 //Natural: ASSIGN #STEP-NAME := 'IVC-ROLL'
                            if (condition(pls_Debug_On.getBoolean()))                                                                                                     //Natural: IF +DEBUG-ON
                            {
                                pnd_Misc_Variables_Pnd_Display_Adsa401.setValue(true);                                                                                    //Natural: MOVE TRUE TO #DISPLAY-ADSA401
                            }                                                                                                                                             //Natural: END-IF
                            DbsUtil.callnat(Adsn404.class , getCurrentProcessState(), pdaAdsa401.getPnd_Adsa401());                                                       //Natural: CALLNAT 'ADSN404' #ADSA401
                            if (condition(Global.isEscape())) return;
                            if (condition(pls_Debug_On.getBoolean()))                                                                                                     //Natural: IF +DEBUG-ON
                            {
                                pnd_Misc_Variables_Pnd_Display_Adsa401.reset();                                                                                           //Natural: RESET #DISPLAY-ADSA401
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().notEquals(" ")))                                                                   //Natural: IF #ERROR-STATUS NE ' '
                            {
                                if (condition(pls_Debug_On.getBoolean()))                                                                                                 //Natural: IF +DEBUG-ON
                                {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-#ADSA401
                                    sub_Display_Pnd_Adsa401();
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                                        else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                    if (condition(Map.getDoInput())) {return;}
                                }                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ADSN470-UPDATE-PRCTPNT-CNTRCT
                                sub_Adsn470_Update_Prctpnt_Cntrct();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADSN480-UPDATE-MIT-RECORD
                                sub_Adsn480_Update_Mit_Record();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(Map.getDoInput())) {return;}
                                if (condition(true)) continue;                                                                                                            //Natural: ESCAPE TOP
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*     >>> CHECK IF IA CONTRACT IS ISSUED  <<<
                    //*     ---------------------------------------
                    if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Annt_Typ_Cde().equals(pnd_Constant_Variables_Pnd_Resettlement)))                                          //Natural: IF #ADSA401.ADP-ANNT-TYP-CDE = #RESETTLEMENT
                    {
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-IA-CNTRCT-ISSUED
                        sub_Check_If_Ia_Cntrct_Issued();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().notEquals(" ")))                                                                       //Natural: IF #ERROR-STATUS NE ' '
                        {
                                                                                                                                                                          //Natural: PERFORM ADSN470-UPDATE-PRCTPNT-CNTRCT
                            sub_Adsn470_Update_Prctpnt_Cntrct();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADSN480-UPDATE-MIT-RECORD
                            sub_Adsn480_Update_Mit_Record();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*     >>> CALL CIS <<<
                    //*    -----------------
                    if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Annt_Typ_Cde().notEquals(pnd_Constant_Variables_Pnd_Resettlement)))                                       //Natural: IF #ADSA401.ADP-ANNT-TYP-CDE NE #RESETTLEMENT
                    {
                                                                                                                                                                          //Natural: PERFORM ADSN043-CALL-CIS-OR-CNTRCT-APPRVL
                        sub_Adsn043_Call_Cis_Or_Cntrct_Apprvl();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().notEquals(" ")))                                                                       //Natural: IF #ERROR-STATUS NE ' '
                        {
                            //*                                  /* ALREADY PERFORMED IN ADSN470 -ESV
                            //*            MOVE #ADSA401.ADC-ERR-MSG(1) TO
                            //*                                     #ADSA470.ADC-ERR-MSG(#CNTRCT-CNT,1)
                                                                                                                                                                          //Natural: PERFORM ADSN470-UPDATE-PRCTPNT-CNTRCT
                            sub_Adsn470_Update_Prctpnt_Cntrct();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADSN480-UPDATE-MIT-RECORD
                            sub_Adsn480_Update_Mit_Record();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*     >>> PERFORM CONTRACT APPROVAL <<<
                    //*     ---------------------------------
                    //*  EM - IA1117 START
                    if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Annt_Typ_Cde().notEquals(pnd_Constant_Variables_Pnd_Resettlement)))                                       //Natural: IF #ADSA401.ADP-ANNT-TYP-CDE NE #RESETTLEMENT
                    {
                        //*  IF (#ADSA401.ADP-ANNT-TYP-CDE NE #RESETTLEMENT) AND
                        //*      (#ADSA401.ADP-SBJCT-TO-WVR-IND NE 'A')    /* EM - IA1117 END
                        pnd_Misc_Variables_Pnd_Process_Step.setValue("STTS-CHECK, CNTRCT-APPRVL");                                                                        //Natural: MOVE 'STTS-CHECK, CNTRCT-APPRVL' TO #PROCESS-STEP
                                                                                                                                                                          //Natural: PERFORM CHANGE-STTS-CHECK-MULT-FUND
                        sub_Change_Stts_Check_Mult_Fund();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CHECK-CNTRCT-APPROVAL
                        sub_Check_Cntrct_Approval();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().notEquals(" ")))                                                                       //Natural: IF #ERROR-STATUS NE ' '
                        {
                                                                                                                                                                          //Natural: PERFORM ADSN470-UPDATE-PRCTPNT-CNTRCT
                            sub_Adsn470_Update_Prctpnt_Cntrct();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADSN480-UPDATE-MIT-RECORD
                            sub_Adsn480_Update_Mit_Record();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*     >>> PROCESS HOLD CODE <<<
                    //*     -------------------------
                    pnd_Misc_Variables_Pnd_Process_Step.setValue("PROCESS HOLD CODE");                                                                                    //Natural: MOVE 'PROCESS HOLD CODE' TO #PROCESS-STEP
                    pnd_Adsn998_Error_Handler_Pnd_Calling_Program.setValue("ADSN403");                                                                                    //Natural: ASSIGN #CALLING-PROGRAM := 'ADSN403'
                    pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("HOLD-CDE");                                                                                         //Natural: ASSIGN #STEP-NAME := 'HOLD-CDE'
                    if (condition(pls_Debug_On.getBoolean()))                                                                                                             //Natural: IF +DEBUG-ON
                    {
                        pnd_Misc_Variables_Pnd_Display_Adsa401.setValue(true);                                                                                            //Natural: MOVE TRUE TO #DISPLAY-ADSA401
                        pnd_Misc_Variables_Pnd_Display_Adsa450.setValue(true);                                                                                            //Natural: MOVE TRUE TO #DISPLAY-ADSA450
                    }                                                                                                                                                     //Natural: END-IF
                    DbsUtil.callnat(Adsn403.class , getCurrentProcessState(), pdaAdsa401.getPnd_Adsa401(), pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_All_Tiaa_Contracts().getValue("*"),  //Natural: CALLNAT 'ADSN403' #ADSA401 #ADSA450-ACTUARIAL-DATA.#ALL-TIAA-CONTRACTS ( * ) #ADSA450-ACTUARIAL-DATA.#ALL-CREF-CONTRACTS ( * )
                        pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_All_Cref_Contracts().getValue("*"));
                    if (condition(Global.isEscape())) return;
                    if (condition(pls_Debug_On.getBoolean()))                                                                                                             //Natural: IF +DEBUG-ON
                    {
                        pnd_Misc_Variables_Pnd_Display_Adsa401.reset();                                                                                                   //Natural: RESET #DISPLAY-ADSA401
                        pnd_Misc_Variables_Pnd_Display_Adsa450.reset();                                                                                                   //Natural: RESET #DISPLAY-ADSA450
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().notEquals(" ")))                                                                           //Natural: IF #ERROR-STATUS NE ' '
                    {
                        if (condition(pls_Debug_On.getBoolean()))                                                                                                         //Natural: IF +DEBUG-ON
                        {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-#ADSA401
                            sub_Display_Pnd_Adsa401();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            getReports().write(0, "=",pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_All_Tiaa_Contracts().getValue("*"),"=",pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_All_Cref_Contracts().getValue("*")); //Natural: WRITE '=' #ADSA450-ACTUARIAL-DATA.#ALL-TIAA-CONTRACTS ( * ) '=' #ADSA450-ACTUARIAL-DATA.#ALL-CREF-CONTRACTS ( * )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ADSN470-UPDATE-PRCTPNT-CNTRCT
                        sub_Adsn470_Update_Prctpnt_Cntrct();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADSN480-UPDATE-MIT-RECORD
                        sub_Adsn480_Update_Mit_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*     >>> ACTUARIAL INTERFACE <<<
                    //*     ---------------------------
                    pnd_Misc_Variables_Pnd_Process_Step.setValue("ACTUARIAL INTERFACE");                                                                                  //Natural: MOVE 'ACTUARIAL INTERFACE' TO #PROCESS-STEP
                    pnd_Adsn998_Error_Handler_Pnd_Calling_Program.setValue("ADSN450");                                                                                    //Natural: ASSIGN #CALLING-PROGRAM := 'ADSN450'
                    pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("ACTUARAL");                                                                                         //Natural: ASSIGN #STEP-NAME := 'ACTUARAL'
                    if (condition(pls_Debug_On.getBoolean()))                                                                                                             //Natural: IF +DEBUG-ON
                    {
                        pnd_Misc_Variables_Pnd_Display_Adsa401.setValue(true);                                                                                            //Natural: MOVE TRUE TO #DISPLAY-ADSA401 #DISPLAY-ADSA450
                        pnd_Misc_Variables_Pnd_Display_Adsa450.setValue(true);
                        //*  12052006 MN
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Addit_450_Parameters_Pnd_Adp_Scnd_Annt_Rltnshp.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Rltnshp());                                  //Natural: ASSIGN #ADP-SCND-ANNT-RLTNSHP := ADP-SCND-ANNT-RLTNSHP
                    //*  12052006 MN
                    if (condition(pls_Debug_On.getBoolean()))                                                                                                             //Natural: IF +DEBUG-ON
                    {
                        getReports().write(0, "********** Before call to ADSN450 ************","=",pnd_Addit_450_Parameters_Pnd_Adp_Scnd_Annt_Rltnshp);                   //Natural: WRITE '********** Before call to ADSN450 ************' '=' #ADP-SCND-ANNT-RLTNSHP
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  12052006 MN
                    //*  12052006 MN
                    DbsUtil.callnat(Adsn450.class , getCurrentProcessState(), pdaAdsa401.getPnd_Adsa401(), pdaAdsa450.getPnd_Adsa450_Actuarial_Data(),                    //Natural: CALLNAT 'ADSN450' #ADSA401 #ADSA450-ACTUARIAL-DATA #ADP-SCND-ANNT-RLTNSHP #RETURN-MESSAGE
                        pnd_Addit_450_Parameters_Pnd_Adp_Scnd_Annt_Rltnshp, pnd_Addit_450_Parameters_Pnd_Return_Message);
                    if (condition(Global.isEscape())) return;
                    //*                    #RETURN-CODE                     /* 12052006 MN
                    //*  12052006 MN
                    if (condition(pls_Debug_On.getBoolean()))                                                                                                             //Natural: IF +DEBUG-ON
                    {
                        getReports().write(0, "*********** After call to ADSN450 **************** ","=",pnd_Addit_450_Parameters_Pnd_Adp_Scnd_Annt_Rltnshp,               //Natural: WRITE '*********** After call to ADSN450 **************** ' '=' #ADP-SCND-ANNT-RLTNSHP '=' #RETURN-MESSAGE
                            "=",pnd_Addit_450_Parameters_Pnd_Return_Message);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pls_Debug_On.getBoolean()))                                                                                                             //Natural: IF +DEBUG-ON
                    {
                        pnd_Misc_Variables_Pnd_Display_Adsa401.reset();                                                                                                   //Natural: RESET #DISPLAY-ADSA401
                        pnd_Misc_Variables_Pnd_Display_Adsa450.reset();                                                                                                   //Natural: RESET #DISPLAY-ADSA450
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().notEquals(" ")))                                                                           //Natural: IF #ERROR-STATUS NE ' '
                    {
                        if (condition(pls_Debug_On.getBoolean()))                                                                                                         //Natural: IF +DEBUG-ON
                        {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-#ADSA401
                            sub_Display_Pnd_Adsa401();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM DISPLAY-#ADSA450
                            sub_Display_Pnd_Adsa450();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ADSN470-UPDATE-PRCTPNT-CNTRCT
                        sub_Adsn470_Update_Prctpnt_Cntrct();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-STATUS-RECORD
                        sub_Write_Status_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADSN480-UPDATE-MIT-RECORD
                        sub_Adsn480_Update_Mit_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*     >>> IA INTERFACE <<<
                    //*     --------------------
                    pnd_Misc_Variables_Pnd_Process_Step.setValue("IA INTERFACE");                                                                                         //Natural: MOVE 'IA INTERFACE' TO #PROCESS-STEP
                                                                                                                                                                          //Natural: PERFORM ADSN600-IA-INTERFACE
                    sub_Adsn600_Ia_Interface();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().notEquals(" ")))                                                                           //Natural: IF #ERROR-STATUS NE ' '
                    {
                        if (condition(pls_Debug_On.getBoolean()))                                                                                                         //Natural: IF +DEBUG-ON
                        {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-ADSN600-PARMS
                            sub_Display_Adsn600_Parms();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  COMMIT DB03 UPDATE
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                    }                                                                                                                                                     //Natural: END-IF
                    //*     >>> UPDATE PARTICIPANT & CONTRACT RECORD / PERFORM MIT UPDATE <<<
                    //*     -----------------------------------------------------------------
                                                                                                                                                                          //Natural: PERFORM ADSN470-UPDATE-PRCTPNT-CNTRCT
                    sub_Adsn470_Update_Prctpnt_Cntrct();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  EM - IA17
                                                                                                                                                                          //Natural: PERFORM WRITE-STATUS-RECORD
                    sub_Write_Status_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADSN480-UPDATE-MIT-RECORD
                    sub_Adsn480_Update_Mit_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  END PARTICIPANT READ
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB-MESSAGE
                sub_End_Of_Job_Message();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //* ***************** O N   E R R O R   R O U T I N E *********************
                //*                                                                                                                                                       //Natural: ON ERROR
                //* *********************** S U B R O U T I N E S ************************
                //*  --------------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADSN401-READ-IA-CONTROL
                //*  --------------------------------------- *
                //*  ---------------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADSN402-READ-CONTROL-REC
                //*  ---------------------------------------- *
                //*  OS-102013 START
                //*  ------------------------------------ *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-TOTAL-FUND-COUNT
                //*  OS-102013 END
                //*  /* EM - 051911 START
                //*  ------------------------------------- *
                //*  DEFINE SUBROUTINE CHECK-IF-END-OF-MONTH /* ESV 07-13-04
                //*  ------------------------------------- *
                //*  MOVE 8 TO #ACT-KEY-TYPE
                //*  CALLNAT 'DVAN150R' ACT-VSAM-RECORD DASA099
                //*  IF #ACT-RETURN-INDICATOR NE 0
                //*   MOVE 'DVAN150R' TO #CALLING-PROGRAM
                //*   MOVE 'FACTOR' TO #STEP-NAME
                //*   COMPRESS
                //*     'ERROR IN ACCESSING FACTOR FILE DATE (DVAN150R) '
                //*     'ERROR CODE :' #ACT-RETURN-INDICATOR TO #ERROR-MSG LEAVING NO SPACE
                //*   PERFORM ERROR-LOG
                //*   MOVE 92 TO #TERMINATE-NO
                //*   MOVE #ERROR-MSG TO #TERMINATE-MSG
                //*   PERFORM TERMINATE-PROCESS
                //*  END-IF
                //*  MOVE #ACT-LATEST-DATE(1) TO #FACTOR-DTE
                //*  MOVE EDITED #FACTOR-DTE TO +EOM-FACTOR-DTE (EM=YYYYMMDD)
                //*  COMPUTE #COMP-DTE = +EOM-FACTOR-DTE + 1
                //*  MOVE EDITED #COMP-DTE (EM=YYYYMMDD) TO #COMPUTE-DTE
                //*  IF #FACTOR-DTE-MM NE #COMPUTE-DTE-MM
                //*   MOVE EDITED +EOM-FACTOR-DTE (EM=N(9)) TO #DAY-OF-WEEK
                //*   IF #DAY-OF-WEEK = 'Saturday' OR = 'Sunday'
                //*     MOVE TRUE  TO +EOM-INDR
                //*   ELSE
                //*     #NAZ-TBL-RCRD-TYP-IND := 'C'
                //*     #NAZ-TBL-RCRD-LVL1-ID := 'NAZ006'
                //*     #NAZ-TBL-RCRD-LVL2-ID := 'DT'
                //*     #NAZ-TBL-RCRD-LVL3-ID := #FACTOR-DTE
                //*     READ NAZ-TABLE-DDM BY NAZ-TBL-SUPER3
                //*         STARTING FROM #NAZ-TABLE-SUPER3
                //*       IF NAZ-TABLE-DDM.NAZ-TBL-RCRD-LVL1-ID NE #NAZ-TBL-RCRD-LVL1-ID
                //*          OR NAZ-TABLE-DDM.NAZ-TBL-RCRD-LVL2-ID NE #NAZ-TBL-RCRD-LVL2-ID
                //*         ESCAPE BOTTOM
                //*       END-IF
                //*       ACCEPT IF NAZ-TABLE-DDM.NAZ-TBL-RCRD-ACTV-IND EQ 'Y'
                //*       IF #FACTOR-DTE EQ SUBSTR (NAZ-TBL-RCRD-LVL3-ID,1,8)
                //*         IF SUBSTR (NAZ-TBL-SECNDRY-DSCRPTN-TXT(1),1,2) EQ ' '
                //*           MOVE TRUE  TO +EOM-INDR
                //*         END-IF
                //*         ESCAPE BOTTOM
                //*       END-IF
                //*     END-READ
                //*   END-IF
                //*  END-IF
                //*  END-SUBROUTINE
                //*  /* EM - 051911 END
                //*  OS-102013 START - COMMENTED-OUT THE FOLLOWING ROUTINE
                //*  ------------------------------------- *            /* EM - IA17 START
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-STATUS-RECORD
                //*  ------------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STATUS-DESCRIPTION
                //*  ------------------------------------- *
                //*  ---------------------------------------- *
                //* *DEFINE SUBROUTINE VALIDATE-CONTRACT-IN-COR
                //*  ---------------------------------------- *
                //* *#STEP-NAME       := 'VALIDCOR'
                //* *RESET #CNTRCT-IN-COR  #COR-CNTRCT-KEY4
                //* *#COR-KEY4-CNTRCT := ADS-CNTRCT-VIEW.ADC-TIAA-NBR
                //* *#COR-KEY4-RCD    := 2
                //* *WRITE 'VALIDATING CONTRACT'
                //* *  ADS-CNTRCT-VIEW.ADC-TIAA-NBR
                //* * CM -REPLACE READ OR COR FILE PER MDM CONVERSION
                //* *#I-CONTRACT-NUMBER := ADS-CNTRCT-VIEW.ADC-TIAA-NBR
                //* *#I-TIAA-CREF-IND   := 'T'
                //* *#I-PAYEE-CODE := 01
                //* *CALLNAT 'MDMN210A' #MDMA210                          /* CM 3/4/10
                //* *WRITE 'AFTER MDMN210A RC =' #O-RETURN-CODE
                //* *IF #O-RETURN-CODE EQ '0000'
                //*   IF (#O-CONTRACT-STATUS-CODE EQ 'H')  OR
                //*       #O-PLATFORM-IND EQ 'B' AND
                //*       #O-CONTRACT-STATUS-CODE EQ ' '
                //*     #CNTRCT-IN-COR := TRUE
                //* *  IF (#O-PLATFORM-IND EQ 'S' OR #O-PLATFORM-IND EQ 'B') AND
                //* *    (#O-CONTRACT-STATUS-CODE EQ 'H' OR #O-CONTRACT-STATUS-CODE EQ ' ')
                //* *    #CNTRCT-IN-COR := TRUE
                //* *  END-IF
                //* *END-IF
                //*  011713 START
                //* *IF NOT #CNTRCT-IN-COR
                //* *  READ COR-XREF-CNTRCT-VIEW BY COR-SUPER-CNTRCT-PIN-PAYEE
                //* *      STARTING FROM #COR-CNTRCT-KEY4
                //* *    IF COR-XREF-CNTRCT-VIEW.CNTRCT-NBR         NE #COR-KEY4-CNTRCT
                //* *        OR COR-XREF-CNTRCT-VIEW.PH-RCD-TYPE-CDE NE #COR-KEY4-RCD
                //* *      ESCAPE BOTTOM
                //* *    END-IF
                //*                            /* IDENTIFY IF CONTRACT IS SUNGUARD ACTIVE
                //* *    WRITE '=' #SUNGARD-IND '=' COR-XREF-CNTRCT-VIEW.CNTRCT-STATUS-CDE
                //* *    IF (COR-XREF-CNTRCT-VIEW.CNTRCT-STATUS-CDE EQ 'H')  OR
                //* *#SUNGARD-IND EQ 'B' AND COR-XREF-CNTRCT-VIEW.CNTRCT-STATUS-CDE EQ ' ')
                //* *      #CNTRCT-IN-COR := TRUE
                //* *    END-IF
                //* *    ESCAPE BOTTOM
                //* *  END-READ
                //* *END-IF
                //*  011713 END
                //* *END-SUBROUTINE
                //*  OS-102013 END
                //*  -------------------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-IA-PROCESS-COMPLETED
                //*  -------------------------------------------- *
                //*  --------------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPLETE-KDO-MIT-UPDATE
                //*  --------------------------------------- *
                //*  ----------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CNTRCT-LOB
                //*  ----------------------------- *
                //* * 08312005 MN START
                //*  --------------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-IA-CNTRCT-ISSUED
                //*  ----------------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-CNTRCT-DATA-FOR-UPDT
                //*  --------------------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADSN470-UPDATE-PRCTPNT-CNTRCT
                //*  ----------------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADSN480-UPDATE-MIT-RECORD
                //*  ------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-LOG
                //*  ------------------------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADSN043-CALL-CIS-OR-CNTRCT-APPRVL
                //*  ----------------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHANGE-STTS-CHECK-MULT-FUND
                //*  ----------------------------------------- *
                //*  ------------------------------------ *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-CNTRCT-APPROVAL
                //*  ------------------------------------ *
                //*  ----------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADSN600-IA-INTERFACE
                //*  ----------------------------------- *
                //*  ----------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: HANDLE-SYSTEM-ERROR
                //*    WHEN +NO-OF-RESTART EQ 5
                //*  -------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-#ADSA401
                //* * 08312005 MN START
                //* * 08312005 MN END
                //* * 08312005 MN START
                //*  --------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-#ADSA450
                //* *R #P 1 TO 80
                //*  ------------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-ADSN600-PARMS
                //*  ------------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-ADSA043-PARMS
                //*  OS-102013 START - COMMENTED-OUT THE FOLLOWING ROUTINE
                //* **************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
                //*  OS-102013 END
                //* *************************************
                //*  ---------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-JOB-MESSAGE
                //*  --------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TERMINATE-PROCESS
                //*  CM- CLOSE MQ  OS-102013
                DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                              //Natural: FETCH RETURN 'MDMP0012'
                if (condition(Global.isEscape())) return;
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Adsn401_Read_Ia_Control() throws Exception                                                                                                           //Natural: ADSN401-READ-IA-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Adsn998_Error_Handler_Pnd_Calling_Program.setValue("ADSN401");                                                                                                //Natural: ASSIGN #CALLING-PROGRAM := 'ADSN401'
        pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("IA-CHKDT");                                                                                                     //Natural: ASSIGN #STEP-NAME := 'IA-CHKDT'
        DbsUtil.callnat(Adsn401.class , getCurrentProcessState(), pls_Ia_Next_Cycle_Chk_Dte);                                                                             //Natural: CALLNAT 'ADSN401' +IA-NEXT-CYCLE-CHK-DTE
        if (condition(Global.isEscape())) return;
    }
    private void sub_Adsn402_Read_Control_Rec() throws Exception                                                                                                          //Natural: ADSN402-READ-CONTROL-REC
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Adsn998_Error_Handler_Pnd_Calling_Program.setValue("ADSN402");                                                                                                //Natural: ASSIGN #CALLING-PROGRAM := 'ADSN402'
        pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("CTRL-DTE");                                                                                                     //Natural: ASSIGN #STEP-NAME := 'CTRL-DTE'
        DbsUtil.callnat(Adsn402.class , getCurrentProcessState(), pls_Cntl_Bsnss_Prev_Dte, pls_Cntl_Bsnss_Dte, pls_Cntl_Bsnss_Tmrrw_Dte, pnd_Misc_Variables_Pnd_Cct_Rcrd_Cde); //Natural: CALLNAT 'ADSN402' +CNTL-BSNSS-PREV-DTE +CNTL-BSNSS-DTE +CNTL-BSNSS-TMRRW-DTE #CCT-RCRD-CDE
        if (condition(Global.isEscape())) return;
    }
    private void sub_Get_Total_Fund_Count() throws Exception                                                                                                              //Natural: GET-TOTAL-FUND-COUNT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------ *
        //*  RS1
        DbsUtil.callnat(Adsn888.class , getCurrentProcessState(), pdaAdsa888.getPnd_Parm_Area(), pdaAdsa888.getPnd_Nbr_Acct());                                           //Natural: CALLNAT 'ADSN888' #PARM-AREA #NBR-ACCT
        if (condition(Global.isEscape())) return;
        pls_Ticker.getValue("*").setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Ticker().getValue("*"));                                                                   //Natural: MOVE #ACCT-TICKER ( * ) TO +TICKER ( * )
        pls_Invstmnt_Grpng_Id.getValue("*").setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue("*"));                                             //Natural: MOVE #ACCT-INVSTMNT-GRPNG-ID ( * ) TO +INVSTMNT-GRPNG-ID ( * )
        //* *WRITE 'ADSP401, RECS COUNT FROM ADSN888 --> ' #NBR-ACCT    /* RS1
        //* *MOVE #NBR-ACCT TO +FUND-CNT                 /* RS1
        //* *MOVE #MAX-RATES TO +TIAA-RATE-CNT           /* 022208
        //*  #CALLING-PROGRAM := 'NECN4000'
        //*  #STEP-NAME       := 'FND-LKUP'
        //*  RESET NECA4000
        //*  NECA4000.FUNCTION-CDE        := 'CFN'
        //*  NECA4000.INPT-KEY-OPTION-CDE := '01'   /* BY COMPANY FUND
        //*  NECA4000.CFN-KEY-COMPANY-CDE := '001'  /* BY COMPANY FUND
        //*  CALLNAT 'NECN4000' NECA4000
        //*  MOVE NECA4000.OUTPUT-CNT TO +FUND-CNT
    }
    private void sub_Write_Status_Record() throws Exception                                                                                                               //Natural: WRITE-STATUS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------- *
        if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Sbjct_To_Wvr_Ind().equals("A")))                                                                                 //Natural: IF ADS-PRTCPNT-VIEW.ADP-SBJCT-TO-WVR-IND = 'A'
        {
            pnd_Adas_Status_File.reset();                                                                                                                                 //Natural: RESET #ADAS-STATUS-FILE
            pnd_Adas_Status_File_Pnd_Rqst_Id_35.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                                       //Natural: ASSIGN #RQST-ID-35 := ADS-PRTCPNT-VIEW.RQST-ID
            pnd_Adas_Status_File_Pnd_Ia_Tiaa_Cntrct_Nbr.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Ia_Tiaa_Nbr());                                                       //Natural: ASSIGN #IA-TIAA-CNTRCT-NBR := ADS-PRTCPNT-VIEW.ADP-IA-TIAA-NBR
            pnd_Adas_Status_File_Pnd_Ia_Cref_Cntrct_Nbr.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Ia_Cref_Nbr());                                                       //Natural: ASSIGN #IA-CREF-CNTRCT-NBR := ADS-PRTCPNT-VIEW.ADP-IA-CREF-NBR
            if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().notEquals(" ")))                                                                                   //Natural: IF #ERROR-STATUS NE ' '
            {
                pnd_Adas_Status_File_Pnd_Status.setValue(pnd_Request_Rejected);                                                                                           //Natural: ASSIGN #STATUS := #REQUEST-REJECTED
                                                                                                                                                                          //Natural: PERFORM STATUS-DESCRIPTION
                sub_Status_Description();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Adas_Status_File_Pnd_Status.setValue(pnd_Request_Processed);                                                                                          //Natural: ASSIGN #STATUS := #REQUEST-PROCESSED
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(1, false, pnd_Adas_Status_File);                                                                                                         //Natural: WRITE WORK FILE 1 #ADAS-STATUS-FILE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Status_Description() throws Exception                                                                                                                //Natural: STATUS-DESCRIPTION
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Keys_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                                  //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
        pnd_Keys_Pnd_Naz_Tbl_Rcrd_Lvl1_Id.setValue("NAZ062");                                                                                                             //Natural: ASSIGN #NAZ-TBL-RCRD-LVL1-ID := 'NAZ062'
        pnd_Keys_Pnd_Naz_Tbl_Rcrd_Lvl2_Id.setValue("PSC");                                                                                                                //Natural: ASSIGN #NAZ-TBL-RCRD-LVL2-ID := 'PSC'
        pnd_Keys_Pnd_Naz_Tbl_Rcrd_Lvl3_Id.setValue(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status());                                                                         //Natural: ASSIGN #NAZ-TBL-RCRD-LVL3-ID := #ERROR-STATUS
        vw_naz_Table_Ddm.startDatabaseFind                                                                                                                                //Natural: FIND NAZ-TABLE-DDM WITH NAZ-TBL-SUPER3 = #NAZ-TABLE-SUPER3
        (
        "FIND01",
        new Wc[] { new Wc("NAZ_TBL_SUPER3", "=", pnd_Keys_Pnd_Naz_Table_Super3, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_naz_Table_Ddm.readNextRow("FIND01", true)))
        {
            vw_naz_Table_Ddm.setIfNotFoundControlFlag(false);
            if (condition(vw_naz_Table_Ddm.getAstCOUNTER().equals(0)))                                                                                                    //Natural: IF NO RECORDS FOUND
            {
                getReports().write(0, "DESCRIPTION NOT FOUND FOR STATUS ",pnd_Adas_Status_File_Pnd_Status);                                                               //Natural: WRITE 'DESCRIPTION NOT FOUND FOR STATUS ' #STATUS
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Adas_Status_File_Pnd_Sub_Status.setValue(naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt);                                                                         //Natural: ASSIGN #SUB-STATUS := NAZ-TBL-RCRD-DSCRPTN-TXT
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  EM - IA17 END
    }
    private void sub_Check_If_Ia_Process_Completed() throws Exception                                                                                                     //Natural: CHECK-IF-IA-PROCESS-COMPLETED
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("CHKIACMP");                                                                                                     //Natural: ASSIGN #STEP-NAME := 'CHKIACMP'
        pnd_Keys_Pnd_Adi_Rqst_Id.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                                                      //Natural: MOVE ADS-PRTCPNT-VIEW.RQST-ID TO #ADI-RQST-ID
        if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde().equals(pnd_Constant_Variables_Pnd_Resettlement)))                                                 //Natural: IF ADS-PRTCPNT-VIEW.ADP-ANNT-TYP-CDE EQ #RESETTLEMENT
        {
            pnd_Keys_Pnd_Adi_Ia_Rcrd_Cde.setValue("RS");                                                                                                                  //Natural: MOVE 'RS' TO #ADI-IA-RCRD-CDE
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Rsttlmnt_Cnt().equals("1")))                                                                                 //Natural: IF ADS-PRTCPNT-VIEW.ADP-RSTTLMNT-CNT EQ '1'
            {
                pnd_Keys_Pnd_Adi_Sqnce_Nbr.setValue(12);                                                                                                                  //Natural: MOVE 12 TO #ADI-SQNCE-NBR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Keys_Pnd_Adi_Sqnce_Nbr.setValue(22);                                                                                                                  //Natural: MOVE 22 TO #ADI-SQNCE-NBR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Keys_Pnd_Adi_Ia_Rcrd_Cde.setValue("ACT");                                                                                                                 //Natural: MOVE 'ACT' TO #ADI-IA-RCRD-CDE
            pnd_Keys_Pnd_Adi_Sqnce_Nbr.setValue(1);                                                                                                                       //Natural: MOVE 1 TO #ADI-SQNCE-NBR
        }                                                                                                                                                                 //Natural: END-IF
        vw_ads_Ia_Rslt_View.startDatabaseFind                                                                                                                             //Natural: FIND ADS-IA-RSLT-VIEW WITH ADI-SUPER1 EQ #ADI-SUPER1
        (
        "FIND02",
        new Wc[] { new Wc("ADI_SUPER1", "=", pnd_Keys_Pnd_Adi_Super1, WcType.WITH) }
        );
        FIND02:
        while (condition(vw_ads_Ia_Rslt_View.readNextRow("FIND02", true)))
        {
            vw_ads_Ia_Rslt_View.setIfNotFoundControlFlag(false);
            if (condition(vw_ads_Ia_Rslt_View.getAstCOUNTER().equals(0)))                                                                                                 //Natural: IF NO RECORDS FOUND
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Logicals_Pnd_Ia_Process_Completed.setValue(true);                                                                                                         //Natural: MOVE TRUE TO #IA-PROCESS-COMPLETED
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
            getReports().write(0, ">>>>CHECK-IF-IA-PROCESS COMPLETE","STAT : ",ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde(),"PIN : ",ldaAdsl401.getAds_Prtcpnt_View_Adp_Unique_Id(),"CONTRACTS-IN-RQST : ",ldaAdsl401.getAds_Prtcpnt_View_Adp_Cntrcts_In_Rqst().getValue("*"),"IA-PROC-COMPLETE  : ",pnd_Logicals_Pnd_Ia_Process_Completed,NEWLINE,"*",new  //Natural: WRITE '>>>>CHECK-IF-IA-PROCESS COMPLETE' 'STAT : ' ADS-PRTCPNT-VIEW.ADP-STTS-CDE 'PIN : ' ADS-PRTCPNT-VIEW.ADP-UNIQUE-ID 'CONTRACTS-IN-RQST : ' ADS-PRTCPNT-VIEW.ADP-CNTRCTS-IN-RQST ( * ) 'IA-PROC-COMPLETE  : ' #IA-PROCESS-COMPLETED / '*' ( 100 )
                RepeatItem(100));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Complete_Kdo_Mit_Update() throws Exception                                                                                                           //Natural: COMPLETE-KDO-MIT-UPDATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("CMPLTKDO");                                                                                                     //Natural: ASSIGN #STEP-NAME := 'CMPLTKDO'
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
            getReports().write(0, ">>>COMPLETE-MIT-KDO-UPDATE");                                                                                                          //Natural: WRITE '>>>COMPLETE-MIT-KDO-UPDATE'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Misc_Variables_Pnd_Process_Step.setValue("COMPLETE-KDO-MIT-UPDATE");                                                                                          //Natural: MOVE 'COMPLETE-KDO-MIT-UPDATE' TO #PROCESS-STEP
        GET01:                                                                                                                                                            //Natural: GET ADS-PRTCPNT-VIEW #ADSA401.#CUR-PRTCPNT-ISN
        ldaAdsl401.getVw_ads_Prtcpnt_View().readByID(pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Prtcpnt_Isn().getLong(), "GET01");
        //*  IF +EOM-INDR     /* ESV 07-21-04               /* EM - 051911 START
        //*   MOVE +EOM-FACTOR-DTE          TO ADS-PRTCPNT-VIEW.ADP-LST-ACTVTY-DTE
        //*  ELSE                                           /* EM - 051911 END
        ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte().setValue(pdaAdsa401.getPnd_Adsa401_Pnd_Cntl_Bsnss_Dte());                                                     //Natural: MOVE #ADSA401.#CNTL-BSNSS-DTE TO ADS-PRTCPNT-VIEW.ADP-LST-ACTVTY-DTE
        //*  END-IF                                         /* EM - 051911
        pdaAdsa401.getPnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd().setValuesByName(ldaAdsl401.getVw_ads_Prtcpnt_View());                                                        //Natural: MOVE BY NAME ADS-PRTCPNT-VIEW TO #ADSA401-PRTCPNT-RCRD
        ldaAdsl401a.getVw_ads_Cntrct_View().startDatabaseFind                                                                                                             //Natural: FIND ADS-CNTRCT-VIEW WITH ADS-CNTRCT-VIEW.RQST-ID = #ADSA401.RQST-ID
        (
        "FIND_CNTRCT2",
        new Wc[] { new Wc("RQST_ID", "=", pdaAdsa401.getPnd_Adsa401_Rqst_Id(), WcType.WITH) }
        );
        FIND_CNTRCT2:
        while (condition(ldaAdsl401a.getVw_ads_Cntrct_View().readNextRow("FIND_CNTRCT2", true)))
        {
            ldaAdsl401a.getVw_ads_Cntrct_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAdsl401a.getVw_ads_Cntrct_View().getAstCOUNTER().equals(0)))                                                                                 //Natural: IF NO RECORDS FOUND
            {
                pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L03");                                                                                             //Natural: MOVE 'L03' TO #ERROR-STATUS
                if (true) break FIND_CNTRCT2;                                                                                                                             //Natural: ESCAPE BOTTOM ( FIND-CNTRCT2. )
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(pnd_Counters_Pnd_Cntrct_Cnt.equals(pnd_Constant_Variables_Pnd_Max_Cntrct)))                                                                     //Natural: IF #CNTRCT-CNT EQ #MAX-CNTRCT
            {
                pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L07");                                                                                             //Natural: MOVE 'L07' TO #ERROR-STATUS
                if (true) break FIND_CNTRCT2;                                                                                                                             //Natural: ESCAPE BOTTOM ( FIND-CNTRCT2. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Counters_Pnd_Cntrct_Cnt.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CNTRCT-CNT
            pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Cntrct_Isn().getValue(pnd_Counters_Pnd_Cntrct_Cnt).setValue(ldaAdsl401a.getVw_ads_Cntrct_View().getAstISN("FIND_CNTRCT2")); //Natural: MOVE *ISN TO #ADSA470-CNTRCT-ISN ( #CNTRCT-CNT )
            pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Cntrct_Rcrd().getValue(pnd_Counters_Pnd_Cntrct_Cnt).setValuesByName(ldaAdsl401a.getVw_ads_Cntrct_View());               //Natural: MOVE BY NAME ADS-CNTRCT-VIEW TO #ADSA470-CNTRCT-RCRD ( #CNTRCT-CNT )
            //* * 08312005 MN START
            pdaAdsa470.getPnd_Adsa470_Adc_Tpa_Guarant_Commut_Grd_Amt().getValue(pnd_Counters_Pnd_Cntrct_Cnt,"*").setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tpa_Guarant_Commut_Grd_Amt().getValue("*")); //Natural: MOVE ADS-CNTRCT-VIEW.ADC-TPA-GUARANT-COMMUT-GRD-AMT ( * ) TO #ADSA470.ADC-TPA-GUARANT-COMMUT-GRD-AMT ( #CNTRCT-CNT,* )
            pdaAdsa470.getPnd_Adsa470_Adc_Tpa_Guarant_Commut_Std_Amt().getValue(pnd_Counters_Pnd_Cntrct_Cnt,"*").setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tpa_Guarant_Commut_Std_Amt().getValue("*")); //Natural: MOVE ADS-CNTRCT-VIEW.ADC-TPA-GUARANT-COMMUT-STD-AMT ( * ) TO #ADSA470.ADC-TPA-GUARANT-COMMUT-STD-AMT ( #CNTRCT-CNT,* )
            pdaAdsa470.getPnd_Adsa470_Adc_Orig_Lob_Ind().getValue(pnd_Counters_Pnd_Cntrct_Cnt).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Orig_Lob_Ind());               //Natural: MOVE ADS-CNTRCT-VIEW.ADC-ORIG-LOB-IND TO #ADSA470.ADC-ORIG-LOB-IND ( #CNTRCT-CNT )
            pdaAdsa470.getPnd_Adsa470_Adc_Tpa_Paymnt_Cnt().getValue(pnd_Counters_Pnd_Cntrct_Cnt).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tpa_Paymnt_Cnt());           //Natural: MOVE ADS-CNTRCT-VIEW.ADC-TPA-PAYMNT-CNT TO #ADSA470.ADC-TPA-PAYMNT-CNT ( #CNTRCT-CNT )
            //* * 08312005 MN END
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
            getReports().write(0, "KEY RQSTID : ",pdaAdsa401.getPnd_Adsa401_Rqst_Id(),"ERROR-STAT : ",pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status(),NEWLINE,"*",new        //Natural: WRITE 'KEY RQSTID : ' #ADSA401.RQST-ID 'ERROR-STAT : ' #ERROR-STATUS / '*' ( 100 )
                RepeatItem(100));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ADSN470-UPDATE-PRCTPNT-CNTRCT
        sub_Adsn470_Update_Prctpnt_Cntrct();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADSN480-UPDATE-MIT-RECORD
        sub_Adsn480_Update_Mit_Record();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    }
    private void sub_Get_Cntrct_Lob() throws Exception                                                                                                                    //Natural: GET-CNTRCT-LOB
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Adsn998_Error_Handler_Pnd_Calling_Program.setValue("NECN4000");                                                                                               //Natural: ASSIGN #CALLING-PROGRAM := 'NECN4000'
        pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("GET-LOB");                                                                                                      //Natural: ASSIGN #STEP-NAME := 'GET-LOB'
        pdaNeca4000.getNeca4000().reset();                                                                                                                                //Natural: RESET NECA4000
        pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().setValue(pnd_Get_Lob_Pnd_Lob_Cntrct);                                                                                 //Natural: ASSIGN NECA4000.PRD-KEY4-ACCT-NBR := #LOB-CNTRCT
        pdaNeca4000.getNeca4000_Function_Cde().setValue("PRD");                                                                                                           //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'PRD'
        pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("04");                                                                                                     //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '04'
        DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                            //Natural: CALLNAT 'NECN4000' NECA4000
        if (condition(Global.isEscape())) return;
        if (condition(pdaNeca4000.getNeca4000_Return_Cde().equals("00") || pdaNeca4000.getNeca4000_Return_Cde().equals(" ")))                                             //Natural: IF NECA4000.RETURN-CDE = '00' OR = ' '
        {
            pnd_Get_Lob_Pnd_Sub_Lob.setValue(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1));                                                                  //Natural: ASSIGN #SUB-LOB := PRD-SUB-PRODUCT-CDE ( 1 )
            pnd_Get_Lob_Pnd_Lob.setValue(pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1));                                                                          //Natural: ASSIGN #LOB := PRD-PRODUCT-CDE ( 1 )
            pnd_Misc_Variables_Pnd_Wk_Cntrct_Lob.setValue(pnd_Get_Lob_Pnd_Lob);                                                                                           //Natural: ASSIGN #WK-CNTRCT-LOB := #LOB
            if (condition(pnd_Misc_Variables_Pnd_Wk_Cntrct_Lob.getSubstring(1,3).equals("TPA")))                                                                          //Natural: IF SUBSTR ( #WK-CNTRCT-LOB,1,3 ) EQ 'TPA'
            {
                pnd_Get_Lob_Pnd_Lob.setValue(pnd_Misc_Variables_Pnd_Wk_Cntrct_Lob.getSubstring(1,3));                                                                     //Natural: ASSIGN #LOB := SUBSTR ( #WK-CNTRCT-LOB,1,3 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Get_Lob_Pnd_Lob.equals("IA")))                                                                                                              //Natural: IF #LOB EQ 'IA'
            {
                if (condition(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1).equals("IPRO")))                                                                  //Natural: IF PRD-SUB-PRODUCT-CDE ( 1 ) EQ 'IPRO'
                {
                    pnd_Get_Lob_Pnd_Lob.setValue(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1));                                                              //Natural: ASSIGN #LOB := PRD-SUB-PRODUCT-CDE ( 1 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1).equals("IA-RET")))                                                            //Natural: IF PRD-SUB-PRODUCT-CDE ( 1 ) EQ 'IA-RET'
                    {
                        pnd_Get_Lob_Pnd_Lob.setValue("TPA");                                                                                                              //Natural: ASSIGN #LOB := 'TPA'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* * 08312005 MN END
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "PRD RECORD NOT FOUND IN EXTERNAL CALL NECN4000 FOR CONTRACT :",pnd_Get_Lob_Pnd_Lob_Cntrct);                                            //Natural: WRITE 'PRD RECORD NOT FOUND IN EXTERNAL CALL NECN4000 FOR CONTRACT :' #LOB-CNTRCT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_If_Ia_Cntrct_Issued() throws Exception                                                                                                         //Natural: CHECK-IF-IA-CNTRCT-ISSUED
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------- *
        pnd_Misc_Variables_Pnd_Process_Step.setValue("CHECK-IF-IA-CNTRCT-ISSUED");                                                                                        //Natural: MOVE 'CHECK-IF-IA-CNTRCT-ISSUED' TO #PROCESS-STEP
        pnd_Logicals_Pnd_Cntrct_Not_Issued.reset();                                                                                                                       //Natural: RESET #CNTRCT-NOT-ISSUED #I
        pnd_Counters_Pnd_I.reset();
        pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("IA-ISSUE");                                                                                                     //Natural: ASSIGN #STEP-NAME := 'IA-ISSUE'
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO #FUND-CNT
        for (pnd_Counters_Pnd_I.setValue(1); condition(pnd_Counters_Pnd_I.lessOrEqual(pdaAdsa401.getPnd_Adsa401_Pnd_Fund_Cnt())); pnd_Counters_Pnd_I.nadd(1))
        {
            if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Acct_Cde().getValue(pnd_Counters_Pnd_I).equals(" ")))                                         //Natural: IF #ADSA450-ACTUARIAL-DATA.#CREF-ACCT-CDE ( #I ) EQ ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  EM - 121008
            if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Acct_Cde().getValue(pnd_Counters_Pnd_I).equals(pnd_Constant_Variables_Pnd_Rea)                //Natural: IF #ADSA450-ACTUARIAL-DATA.#CREF-ACCT-CDE ( #I ) = #REA OR = #ACCESS
                || pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Acct_Cde().getValue(pnd_Counters_Pnd_I).equals(pnd_Constant_Variables_Pnd_Access)))
            {
                if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Ia_Tiaa_Rea_Nbr().equals(" ")))                                                                               //Natural: IF #ADSA401.ADP-IA-TIAA-REA-NBR = ' '
                {
                    pnd_Logicals_Pnd_Cntrct_Not_Issued.setValue(true);                                                                                                    //Natural: MOVE TRUE TO #CNTRCT-NOT-ISSUED
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Ia_Cref_Nbr().equals(" ")))                                                                                   //Natural: IF #ADSA401.ADP-IA-CREF-NBR = ' '
                {
                    pnd_Logicals_Pnd_Cntrct_Not_Issued.setValue(true);                                                                                                    //Natural: MOVE TRUE TO #CNTRCT-NOT-ISSUED
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Logicals_Pnd_Cntrct_Not_Issued.getBoolean()))                                                                                                   //Natural: IF #CNTRCT-NOT-ISSUED
        {
            pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L01");                                                                                                 //Natural: MOVE 'L01' TO #ERROR-STATUS
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Move_Cntrct_Data_For_Updt() throws Exception                                                                                                         //Natural: MOVE-CNTRCT-DATA-FOR-UPDT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------- *
        if (condition(pnd_Counters_Pnd_Cntrct_Cnt.notEquals(getZero())))                                                                                                  //Natural: IF #CNTRCT-CNT NE 0
        {
            if (condition(pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Cntrct_Isn().getValue(pnd_Counters_Pnd_Cntrct_Cnt).equals(getZero())))                                    //Natural: IF #ADSA470-CNTRCT-ISN ( #CNTRCT-CNT ) EQ 0
            {
                pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Cntrct_Isn().getValue(pnd_Counters_Pnd_Cntrct_Cnt).setValue(pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Cntrct_Isn());        //Natural: MOVE #ADSA401.#CUR-CNTRCT-ISN TO #ADSA470-CNTRCT-ISN ( #CNTRCT-CNT )
                pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Cntrct_Rcrd().getValue(pnd_Counters_Pnd_Cntrct_Cnt).setValuesByName(pdaAdsa401.getPnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd()); //Natural: MOVE BY NAME #ADSA401-CNTRCT-RCRD TO #ADSA470-CNTRCT-RCRD ( #CNTRCT-CNT )
                //* * 08312005 MN START
                pdaAdsa470.getPnd_Adsa470_Adc_Tpa_Guarant_Commut_Grd_Amt().getValue(pnd_Counters_Pnd_Cntrct_Cnt,"*").setValue(pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Grd_Amt().getValue("*")); //Natural: MOVE #ADSA401.ADC-TPA-GUARANT-COMMUT-GRD-AMT ( * ) TO #ADSA470.ADC-TPA-GUARANT-COMMUT-GRD-AMT ( #CNTRCT-CNT,* )
                pdaAdsa470.getPnd_Adsa470_Adc_Tpa_Guarant_Commut_Std_Amt().getValue(pnd_Counters_Pnd_Cntrct_Cnt,"*").setValue(pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Std_Amt().getValue("*")); //Natural: MOVE #ADSA401.ADC-TPA-GUARANT-COMMUT-STD-AMT ( * ) TO #ADSA470.ADC-TPA-GUARANT-COMMUT-STD-AMT ( #CNTRCT-CNT,* )
                pdaAdsa470.getPnd_Adsa470_Adc_Orig_Lob_Ind().getValue(pnd_Counters_Pnd_Cntrct_Cnt).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Orig_Lob_Ind());                //Natural: MOVE #ADSA401.ADC-ORIG-LOB-IND TO #ADSA470.ADC-ORIG-LOB-IND ( #CNTRCT-CNT )
                pdaAdsa470.getPnd_Adsa470_Adc_Tpa_Paymnt_Cnt().getValue(pnd_Counters_Pnd_Cntrct_Cnt).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Paymnt_Cnt());            //Natural: MOVE #ADSA401.ADC-TPA-PAYMNT-CNT TO #ADSA470.ADC-TPA-PAYMNT-CNT ( #CNTRCT-CNT )
                //* * 08312005 MN END
                pdaAdsa470.getPnd_Adsa470_Adc_Stts_Cde().getValue(pnd_Counters_Pnd_Cntrct_Cnt).setValue(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status());                    //Natural: MOVE #ERROR-STATUS TO #ADSA470.ADC-STTS-CDE ( #CNTRCT-CNT )
                pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().reset();                                                                                                     //Natural: RESET #ERROR-STATUS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Adsn470_Update_Prctpnt_Cntrct() throws Exception                                                                                                     //Natural: ADSN470-UPDATE-PRCTPNT-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------- *
        if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().notEquals(" ")))                                                                                       //Natural: IF #ERROR-STATUS NE ' '
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Adsn998_Error_Handler_Pnd_Calling_Program.setValue("ADSN470");                                                                                                //Natural: ASSIGN #CALLING-PROGRAM := 'ADSN470'
        pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("UPDT-ADS");                                                                                                     //Natural: ASSIGN #STEP-NAME := 'UPDT-ADS'
        pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Prtcpnt_Isn().setValue(pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Prtcpnt_Isn());                                                    //Natural: MOVE #ADSA401.#CUR-PRTCPNT-ISN TO #ADSA470-PRTCPNT-ISN
        pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Error_Status().setValue(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status());                                                      //Natural: MOVE #ERROR-STATUS TO #ADSA470-ERROR-STATUS
        pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Cntl_Bsnss_Dte().setValue(pdaAdsa401.getPnd_Adsa401_Pnd_Cntl_Bsnss_Dte());                                                  //Natural: MOVE #ADSA401.#CNTL-BSNSS-DTE TO #ADSA470-CNTL-BSNSS-DTE
        pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Fund_Cnt().setValue(pdaAdsa401.getPnd_Adsa401_Pnd_Fund_Cnt());                                                              //Natural: MOVE #ADSA401.#FUND-CNT TO #ADSA470-FUND-CNT
        //*  ESV 07-14-04
        pdaAdsa470.getPnd_Adsa470_Adc_Err_Cde().getValue(1,":",pnd_Counters_Pnd_Cntrct_Cnt,"*").setValue(pdaAdsa401.getPnd_Adsa401_Adc_Err_Cde().getValue("*"));          //Natural: MOVE #ADSA401.ADC-ERR-CDE ( * ) TO #ADSA470.ADC-ERR-CDE ( 1:#CNTRCT-CNT,* )
        pdaAdsa470.getPnd_Adsa470_Adc_Err_Msg().getValue(1,":",pnd_Counters_Pnd_Cntrct_Cnt,"*").setValue(pdaAdsa401.getPnd_Adsa401_Adc_Err_Msg().getValue("*"));          //Natural: MOVE #ADSA401.ADC-ERR-MSG ( * ) TO #ADSA470.ADC-ERR-MSG ( 1:#CNTRCT-CNT,* )
        pdaAdsa470.getPnd_Adsa470_Adc_Err_Acct_Cde().getValue(1,":",pnd_Counters_Pnd_Cntrct_Cnt,"*").setValue(pdaAdsa401.getPnd_Adsa401_Adc_Err_Acct_Cde().getValue("*")); //Natural: MOVE #ADSA401.ADC-ERR-ACCT-CDE ( * ) TO #ADSA470.ADC-ERR-ACCT-CDE ( 1:#CNTRCT-CNT,* )
        pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Prtcpnt_Rcrd().setValuesByName(pdaAdsa401.getPnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd());                                       //Natural: MOVE BY NAME #ADSA401-PRTCPNT-RCRD TO #ADSA470-PRTCPNT-RCRD
        pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Debug_On().setValue(pls_Debug_On.getBoolean());                                                                             //Natural: MOVE +DEBUG-ON TO #ADSA470-DEBUG-ON
        DbsUtil.callnat(Adsn470.class , getCurrentProcessState(), pdaAdsa470.getPnd_Adsa470());                                                                           //Natural: CALLNAT 'ADSN470' #ADSA470
        if (condition(Global.isEscape())) return;
        if (condition(pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Error_Status().notEquals(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status())))                                      //Natural: IF #ADSA470-ERROR-STATUS NE #ERROR-STATUS
        {
            pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue(pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Error_Status());                                                  //Natural: MOVE #ADSA470-ERROR-STATUS TO #ERROR-STATUS
        }                                                                                                                                                                 //Natural: END-IF
        pdaAdsa401.getPnd_Adsa401_Adp_Stts_Cde().setValue(pdaAdsa470.getPnd_Adsa470_Adp_Stts_Cde());                                                                      //Natural: MOVE #ADSA470.ADP-STTS-CDE TO #ADSA401.ADP-STTS-CDE
        if (condition(!pdaAdsa470.getPnd_Adsa470_Adp_Stts_Cde().getSubstring(1,1).equals(pnd_Constant_Variables_Pnd_Successful)))                                         //Natural: IF SUBSTRING ( #ADSA470.ADP-STTS-CDE,1,1 ) NE #SUCCESSFUL
        {
            pls_Tot_Rqst_Errors.nadd(1);                                                                                                                                  //Natural: ADD 1 TO +TOT-RQST-ERRORS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pls_Tot_Rqst_Successful.nadd(1);                                                                                                                              //Natural: ADD 1 TO +TOT-RQST-SUCCESSFUL
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
            getReports().write(0, ">>>>ADSN470-UPDATE-PRTCPNT-CNTRCT","KEY RQSTID : ",pdaAdsa401.getPnd_Adsa401_Rqst_Id(),"ERROR-STAT : ",pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Error_Status(),"CNTRCT-ISN : ",pdaAdsa401.getPnd_Adsa401_Adp_Cntrcts_In_Rqst().getValue("*"),">>>>PROCESS-STEP : ",pnd_Misc_Variables_Pnd_Process_Step,NEWLINE,"*",new  //Natural: WRITE '>>>>ADSN470-UPDATE-PRTCPNT-CNTRCT' 'KEY RQSTID : ' #ADSA401.RQST-ID 'ERROR-STAT : ' #ADSA470-ERROR-STATUS 'CNTRCT-ISN : ' #ADSA401.ADP-CNTRCTS-IN-RQST ( * ) '>>>>PROCESS-STEP : ' #PROCESS-STEP / '*' ( 100 )
                RepeatItem(100));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Adsn480_Update_Mit_Record() throws Exception                                                                                                         //Natural: ADSN480-UPDATE-MIT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------- *
        pdaAdsa480.getPnd_Adsa480_Mit_Update().reset();                                                                                                                   //Natural: RESET #ADSA480-MIT-UPDATE
        pnd_Adsn998_Error_Handler_Pnd_Calling_Program.setValue("ADSN480");                                                                                                //Natural: ASSIGN #CALLING-PROGRAM := 'ADSN480'
        pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("CALL-MIT");                                                                                                     //Natural: ASSIGN #STEP-NAME := 'CALL-MIT'
        pdaAdsa480.getPnd_Adsa480_Mit_Update().setValuesByName(pdaAdsa401.getPnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd());                                                     //Natural: MOVE BY NAME #ADSA401-PRTCPNT-RCRD TO #ADSA480-MIT-UPDATE
        pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Entry_User_Id().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Rqst_Last_Updtd_Id());                                            //Natural: MOVE #ADSA401.ADP-RQST-LAST-UPDTD-ID TO #ADSA480-MIT-UPDATE.ADP-ENTRY-USER-ID #USER-ID
        pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_User_Id().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Rqst_Last_Updtd_Id());
        pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Type_Of_Process().setValue(pnd_Constant_Variables_Pnd_Batchads);                                                         //Natural: MOVE #BATCHADS TO #TYPE-OF-PROCESS
        pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Action_Cde().setValue(pnd_Constant_Variables_Pnd_Update_Action);                                                         //Natural: MOVE #UPDATE-ACTION TO #ACTION-CDE
        pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Debug_On().setValue(pls_Debug_On.getBoolean());                                                                      //Natural: MOVE +DEBUG-ON TO #MIT-DEBUG-ON
        //*                                                       /* ESV 07-16-04
        pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Plan_Nbr().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Plan_Nbr());                                                      //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-PLAN-NBR TO #ADSA480-MIT-UPDATE.ADP-PLAN-NBR
        DbsUtil.callnat(Adsn480.class , getCurrentProcessState(), pdaAdsa480.getPnd_Adsa480_Mit_Update());                                                                //Natural: CALLNAT 'ADSN480' #ADSA480-MIT-UPDATE
        if (condition(Global.isEscape())) return;
        //*  COMMIT MIT UPDATE
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        if (condition(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Msg().notEquals(" ")))                                                                                 //Natural: IF #MIT-MSG NE ' '
        {
            pnd_Adsn998_Error_Handler_Pnd_Error_Msg.setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Msg());                                                         //Natural: ASSIGN #ERROR-MSG := #MIT-MSG
                                                                                                                                                                          //Natural: PERFORM ERROR-LOG
            sub_Error_Log();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Log() throws Exception                                                                                                                         //Natural: ERROR-LOG
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------- *
        pnd_Adsn998_Error_Handler_Pnd_Appl_Id.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaAdsa401.getPnd_Adsa401_Pnd_Cics_System_Name(),                  //Natural: COMPRESS #ADSA401.#CICS-SYSTEM-NAME *INIT-ID INTO #APPL-ID LEAVING NO SPACE
            Global.getINIT_ID()));
        if (condition(pnd_Adsn998_Error_Handler_Pnd_Calling_Program.equals(" ")))                                                                                         //Natural: IF #CALLING-PROGRAM EQ ' '
        {
            pnd_Adsn998_Error_Handler_Pnd_Calling_Program.setValue(Global.getPROGRAM());                                                                                  //Natural: ASSIGN #CALLING-PROGRAM := *PROGRAM
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Adsn998_Error_Handler_Pnd_Unique_Id.setValue(pdaAdsa401.getPnd_Adsa401_Adp_Unique_Id());                                                                      //Natural: ASSIGN #UNIQUE-ID := #ADSA401.ADP-UNIQUE-ID
        DbsUtil.callnat(Adsn998.class , getCurrentProcessState(), pdaAdspda_M.getMsg_Info_Sub(), pnd_Adsn998_Error_Handler);                                              //Natural: CALLNAT 'ADSN998' MSG-INFO-SUB #ADSN998-ERROR-HANDLER
        if (condition(Global.isEscape())) return;
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
            getReports().write(0, ">>>>INSIDE ERROR-LOG ");                                                                                                               //Natural: WRITE '>>>>INSIDE ERROR-LOG '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Adsn043_Call_Cis_Or_Cntrct_Apprvl() throws Exception                                                                                                 //Natural: ADSN043-CALL-CIS-OR-CNTRCT-APPRVL
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------------------- *
        pnd_Misc_Variables_Pnd_Process_Step.setValue("CALL-CIS-APPRVL ADSN043");                                                                                          //Natural: MOVE 'CALL-CIS-APPRVL ADSN043' TO #PROCESS-STEP
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
            getReports().write(0, ">>>>ADSN043-CALL-CIS-OR-CNTRCT-APPRVL","=",pdaAdsa401.getPnd_Adsa401_Adp_Annt_Typ_Cde(),NEWLINE,"*",new RepeatItem(100));              //Natural: WRITE '>>>>ADSN043-CALL-CIS-OR-CNTRCT-APPRVL' '=' #ADSA401.ADP-ANNT-TYP-CDE / '*' ( 100 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Adsn998_Error_Handler_Pnd_Calling_Program.setValue("ADSN043");                                                                                                //Natural: ASSIGN #CALLING-PROGRAM := 'ADSN043'
        pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("CALL-CIS");                                                                                                     //Natural: ASSIGN #STEP-NAME := 'CALL-CIS'
        pdaAdspda_M.getMsg_Info_Sub().reset();                                                                                                                            //Natural: RESET MSG-INFO-SUB
        pnd_Adsa043_Call_Cis_Pnd_Rsdnce_Typ.setValue(pdaAdsa401.getPnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Typ_Cd());                                                        //Natural: MOVE #ADSA401.ADP-CRRSPNDNCE-PERM-ADDR-TYP-CD TO #RSDNCE-TYP
        //*  CM
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Frst_Annt_Rsdnc_Cde().equals("AA") || pdaAdsa401.getPnd_Adsa401_Adp_Frst_Annt_Rsdnc_Cde().equals("AP")))              //Natural: IF #ADSA401.ADP-FRST-ANNT-RSDNC-CDE = 'AA' OR = 'AP'
        {
            pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Rsdnce.setValue("NY");                                                                                                     //Natural: ASSIGN #FRST-ANNT-RSDNCE := 'NY'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Rsdnce.setValue(pdaAdsa401.getPnd_Adsa401_Adp_Frst_Annt_Rsdnc_Cde());                                                      //Natural: MOVE #ADSA401.ADP-FRST-ANNT-RSDNC-CDE TO #FRST-ANNT-RSDNCE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Ctznshp.setValue(pdaAdsa401.getPnd_Adsa401_Adp_Frst_Annt_Ctznshp());                                                           //Natural: MOVE #ADSA401.ADP-FRST-ANNT-CTZNSHP TO #FRST-ANNT-CTZNSHP
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Annty_Strt_Dte().notEquals(getZero())))                                                                               //Natural: IF #ADSA401.ADP-ANNTY-STRT-DTE NE 0
        {
            pnd_Misc_Variables_Pnd_Date_A.setValueEdited(pdaAdsa401.getPnd_Adsa401_Adp_Annty_Strt_Dte(),new ReportEditMask("YYYYMMDD"));                                  //Natural: MOVE EDITED #ADSA401.ADP-ANNTY-STRT-DTE ( EM = YYYYMMDD ) TO #DATE-A
            pnd_Adsa043_Call_Cis_Pnd_Annty_Strt_Dte.setValue(pnd_Misc_Variables_Pnd_Date_N);                                                                              //Natural: MOVE #DATE-N TO #ANNTY-STRT-DTE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Adsa043_Call_Cis_Pnd_Da_Tiaa_Nbr.getValue("*").setValue(pdaAdsa401.getPnd_Adsa401_Adp_Cntrcts_In_Rqst().getValue(1,":",6));                                   //Natural: MOVE #ADSA401.ADP-CNTRCTS-IN-RQST ( 1:6 ) TO #DA-TIAA-NBR ( * )
        pnd_Adsa043_Call_Cis_Pnd_Cor_Pin_Nbr.setValue(pdaAdsa401.getPnd_Adsa401_Adp_Unique_Id());                                                                         //Natural: MOVE #ADSA401.ADP-UNIQUE-ID TO #COR-PIN-NBR
        pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Ssn.setValue(pdaAdsa401.getPnd_Adsa401_Adp_Frst_Annt_Ssn());                                                                   //Natural: MOVE #ADSA401.ADP-FRST-ANNT-SSN TO #FRST-ANNT-SSN
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Frst_Annt_Dte_Of_Brth().notEquals(getZero())))                                                                        //Natural: IF #ADSA401.ADP-FRST-ANNT-DTE-OF-BRTH NE 0
        {
            pnd_Misc_Variables_Pnd_Date_A.setValueEdited(pdaAdsa401.getPnd_Adsa401_Adp_Frst_Annt_Dte_Of_Brth(),new ReportEditMask("YYYYMMDD"));                           //Natural: MOVE EDITED #ADSA401.ADP-FRST-ANNT-DTE-OF-BRTH ( EM = YYYYMMDD ) TO #DATE-A
            pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Dte_Of_Brth.setValue(pnd_Misc_Variables_Pnd_Date_N);                                                                       //Natural: MOVE #DATE-N TO #FRST-ANNT-DTE-OF-BRTH
        }                                                                                                                                                                 //Natural: END-IF
        //*  #APC-RQST-IND := #ADSA401.ADP-SBJCT-TO-WVR-IND /* EM - IA1117
        DbsUtil.callnat(Adsn043.class , getCurrentProcessState(), pdaAdspda_M.getMsg_Info_Sub(), pnd_Adsa043_Call_Cis);                                                   //Natural: CALLNAT 'ADSN043' MSG-INFO-SUB #ADSA043-CALL-CIS
        if (condition(Global.isEscape())) return;
        if (condition(pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E")))                                                                                     //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E'
        {
            pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L08");                                                                                                 //Natural: MOVE 'L08' TO #ERROR-STATUS
            pdaAdsa401.getPnd_Adsa401_Adc_Err_Msg().getValue(1).setValue(pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                      //Natural: MOVE MSG-INFO-SUB.##MSG TO #ADSA401.ADC-ERR-MSG ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAdsa401.getPnd_Adsa401_Adp_Orgnl_Issue_State().setValue(pnd_Adsa043_Call_Cis_Pnd_Org_Issue_St);                                                            //Natural: MOVE #ORG-ISSUE-ST TO #ADSA401.ADP-ORGNL-ISSUE-STATE
            pdaAdsa401.getPnd_Adsa401_Adp_State_Of_Issue().setValue(pnd_Adsa043_Call_Cis_Pnd_Rsdnce_Issue_St);                                                            //Natural: MOVE #RSDNCE-ISSUE-ST TO #ADSA401.ADP-STATE-OF-ISSUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
            getReports().write(0, ">>> AFTER ADSN043-CALL-CIS-OR-CNTRCT-APPRVL","=",pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(),"=",pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code(), //Natural: WRITE '>>> AFTER ADSN043-CALL-CIS-OR-CNTRCT-APPRVL' '=' ##MSG '=' ##RETURN-CODE '=' ##ERROR-FIELD
                "=",pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Error_Field());
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM DISPLAY-ADSA043-PARMS
            sub_Display_Adsa043_Parms();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Change_Stts_Check_Mult_Fund() throws Exception                                                                                                       //Natural: CHANGE-STTS-CHECK-MULT-FUND
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("MULTIFND");                                                                                                     //Natural: ASSIGN #STEP-NAME := 'MULTIFND'
        if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde().getValue("*").notEquals("  ")))                                                        //Natural: IF #ADSA450-ACTUARIAL-DATA.#TIAA-RATE-CDE ( * ) NE '  '
        {
            pnd_Logicals_Pnd_Tiaa_Rqstd.setValue(true);                                                                                                                   //Natural: ASSIGN #TIAA-RQSTD = TRUE
            if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Actl_Amt().getValue("*").notEquals(getZero()) && pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Actl_Amt().getValue("*").notEquals(getZero()))) //Natural: IF #ADSA450-ACTUARIAL-DATA.#TIAA-STNDRD-ACTL-AMT ( * ) NE 0 AND #ADSA450-ACTUARIAL-DATA.#TIAA-GRADED-ACTL-AMT ( * ) NE 0
            {
                pnd_Logicals_Pnd_Tiaa_Multi_Funded_Rqst.setValue(true);                                                                                                   //Natural: ASSIGN #TIAA-MULTI-FUNDED-RQST = TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Counters_Pnd_Number_Of_Cref_Funds.reset();                                                                                                                    //Natural: RESET #NUMBER-OF-CREF-FUNDS
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO #FUND-CNT
        for (pnd_Counters_Pnd_I.setValue(1); condition(pnd_Counters_Pnd_I.lessOrEqual(pdaAdsa401.getPnd_Adsa401_Pnd_Fund_Cnt())); pnd_Counters_Pnd_I.nadd(1))
        {
            if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Acct_Cde().getValue(pnd_Counters_Pnd_I).equals(" ")))                                         //Natural: IF #ADSA450-ACTUARIAL-DATA.#CREF-ACCT-CDE ( #I ) EQ ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Acct_Cde().getValue(pnd_Counters_Pnd_I).equals(pnd_Constant_Variables_Pnd_Rea)))              //Natural: IF #ADSA450-ACTUARIAL-DATA.#CREF-ACCT-CDE ( #I ) = #REA
            {
                pnd_Logicals_Pnd_Rea_Rqstd.setValue(true);                                                                                                                //Natural: ASSIGN #REA-RQSTD = TRUE
                if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Graded_Mnthly_Actl_Amt().getValue(pnd_Counters_Pnd_I).notEquals(getZero())))              //Natural: IF #ADSA450-ACTUARIAL-DATA.#CREF-GRADED-MNTHLY-ACTL-AMT ( #I ) NE 0
                {
                    pnd_Logicals_Pnd_Rea_Mnthly_Unts.setValue(true);                                                                                                      //Natural: ASSIGN #REA-MNTHLY-UNTS = TRUE
                }                                                                                                                                                         //Natural: END-IF
                //*  START EM - 121008
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Acct_Cde().getValue(pnd_Counters_Pnd_I).equals(pnd_Constant_Variables_Pnd_Access)))       //Natural: IF #ADSA450-ACTUARIAL-DATA.#CREF-ACCT-CDE ( #I ) = #ACCESS
                {
                    pnd_Logicals_Pnd_Access_Rqstd.setValue(true);                                                                                                         //Natural: ASSIGN #ACCESS-RQSTD = TRUE
                    //*  END EM - 121008
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Graded_Mnthly_Actl_Amt().getValue(pnd_Counters_Pnd_I).notEquals(getZero())))          //Natural: IF #ADSA450-ACTUARIAL-DATA.#CREF-GRADED-MNTHLY-ACTL-AMT ( #I ) NE 0
                    {
                        pnd_Logicals_Pnd_Cref_Mnthly_Unts.setValue(true);                                                                                                 //Natural: ASSIGN #CREF-MNTHLY-UNTS = TRUE
                        pnd_Counters_Pnd_Number_Of_Cref_Funds.nadd(1);                                                                                                    //Natural: ADD 1 TO #NUMBER-OF-CREF-FUNDS
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(! (pnd_Logicals_Pnd_Tiaa_Multi_Funded_Rqst.getBoolean()) && pnd_Counters_Pnd_Number_Of_Cref_Funds.greater(1)))                                      //Natural: IF NOT #TIAA-MULTI-FUNDED-RQST AND #NUMBER-OF-CREF-FUNDS > 1
        {
            pnd_Logicals_Pnd_Cref_Multi_Funded_Rqst.setValue(true);                                                                                                       //Natural: ASSIGN #CREF-MULTI-FUNDED-RQST = TRUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_Cntrct_Approval() throws Exception                                                                                                             //Natural: CHECK-CNTRCT-APPROVAL
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("CNTRAPRV");                                                                                                     //Natural: ASSIGN #STEP-NAME := 'CNTRAPRV'
        short decideConditionsMet3179 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #TIAA-RQSTD
        if (condition(pnd_Logicals_Pnd_Tiaa_Rqstd.getBoolean()))
        {
            decideConditionsMet3179++;
            if (condition(pnd_Adsa043_Call_Cis_Pnd_Ia_Cntrct_Apprvl_Ind.getValue(1).equals("S") && pnd_Logicals_Pnd_Tiaa_Multi_Funded_Rqst.getBoolean()))                 //Natural: IF #IA-CNTRCT-APPRVL-IND ( 1 ) = 'S' AND #TIAA-MULTI-FUNDED-RQST
            {
                pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L09");                                                                                             //Natural: MOVE 'L09' TO #ERROR-STATUS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #NUMBER-OF-CREF-FUNDS > 0
        else if (condition(pnd_Counters_Pnd_Number_Of_Cref_Funds.greater(getZero())))
        {
            decideConditionsMet3179++;
            short decideConditionsMet3185 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #IA-CNTRCT-APPRVL-IND ( 2 );//Natural: VALUE 'S'
            if (condition((pnd_Adsa043_Call_Cis_Pnd_Ia_Cntrct_Apprvl_Ind.getValue(2).equals("S"))))
            {
                decideConditionsMet3185++;
                if (condition(pnd_Logicals_Pnd_Cref_Multi_Funded_Rqst.getBoolean() || pnd_Logicals_Pnd_Cref_Mnthly_Unts.getBoolean()))                                    //Natural: IF #CREF-MULTI-FUNDED-RQST OR #CREF-MNTHLY-UNTS
                {
                    pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L09");                                                                                         //Natural: MOVE 'L09' TO #ERROR-STATUS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'A'
            else if (condition((pnd_Adsa043_Call_Cis_Pnd_Ia_Cntrct_Apprvl_Ind.getValue(2).equals("A"))))
            {
                decideConditionsMet3185++;
                if (condition(pnd_Logicals_Pnd_Cref_Mnthly_Unts.getBoolean()))                                                                                            //Natural: IF #CREF-MNTHLY-UNTS
                {
                    pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L09");                                                                                         //Natural: MOVE 'L09' TO #ERROR-STATUS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN #REA-RQSTD
        else if (condition(pnd_Logicals_Pnd_Rea_Rqstd.getBoolean()))
        {
            decideConditionsMet3179++;
            if (condition(((pnd_Adsa043_Call_Cis_Pnd_Ia_Cntrct_Apprvl_Ind.getValue(3).equals("S") || pnd_Adsa043_Call_Cis_Pnd_Ia_Cntrct_Apprvl_Ind.getValue(3).equals("A"))  //Natural: IF #IA-CNTRCT-APPRVL-IND ( 3 ) = 'S' OR = 'A' AND #REA-MNTHLY-UNTS
                && pnd_Logicals_Pnd_Rea_Mnthly_Unts.getBoolean())))
            {
                pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L09");                                                                                             //Natural: MOVE 'L09' TO #ERROR-STATUS
                //*  EM - 121008 START
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #ACCESS-RQSTD
        else if (condition(pnd_Logicals_Pnd_Access_Rqstd.getBoolean()))
        {
            decideConditionsMet3179++;
            if (condition(pnd_Adsa043_Call_Cis_Pnd_Ia_Cntrct_Apprvl_Ind.getValue(4).notEquals("T")))                                                                      //Natural: IF #IA-CNTRCT-APPRVL-IND ( 4 ) NE 'T'
            {
                pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L09");                                                                                             //Natural: MOVE 'L09' TO #ERROR-STATUS
                //*  EM - 121008 END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Adsn600_Ia_Interface() throws Exception                                                                                                              //Natural: ADSN600-IA-INTERFACE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Adsn998_Error_Handler_Pnd_Calling_Program.setValue("ADSN600");                                                                                                //Natural: ASSIGN #CALLING-PROGRAM := 'ADSN600'
        pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("CALL-IA");                                                                                                      //Natural: ASSIGN #STEP-NAME := 'CALL-IA'
        pnd_Adsa600_Addl_Data.reset();                                                                                                                                    //Natural: RESET #ADSA600-ADDL-DATA
        DbsUtil.callnat(Adsn600.class , getCurrentProcessState(), pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Iarslt_Isn().getValue("*"), pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Prtcpnt_Isn(),  //Natural: CALLNAT 'ADSN600' #ADSA401.#CUR-IARSLT-ISN ( * ) #ADSA401.#CUR-PRTCPNT-ISN #ADSA401.#CUR-CNTRCT-ISN #ADSA401.#CNTL-BSNSS-DTE #ADSA401.#CNTL-BSNSS-TMRRW-DTE #ADSA401.ADP-ORGNL-ISSUE-STATE #ADSA401.ADP-ALT-DEST-HOLD-CDE #ADSA401.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( * ) #ADSA401.ADP-CRRSPNDNCE-PERM-ADDR-ZIP #ADSA401.ADP-CRRSPNDNCE-PERM-ADDR-TYP-CD #PRDC-IVC #ADSA401.ADP-IVC-PYMNT-RULE #ERR-CDE-X #PROGRAM-NME #ERR-NR #ERR-LINE-X #ADSA401.#IA-ORIGIN-CODE
            pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Cntrct_Isn(), pdaAdsa401.getPnd_Adsa401_Pnd_Cntl_Bsnss_Dte(), pdaAdsa401.getPnd_Adsa401_Pnd_Cntl_Bsnss_Tmrrw_Dte(), 
            pdaAdsa401.getPnd_Adsa401_Adp_Orgnl_Issue_State(), pdaAdsa401.getPnd_Adsa401_Adp_Alt_Dest_Hold_Cde(), pdaAdsa401.getPnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Txt().getValue("*"), 
            pdaAdsa401.getPnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Zip(), pdaAdsa401.getPnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Typ_Cd(), pnd_Adsa600_Addl_Data_Pnd_Prdc_Ivc, 
            pdaAdsa401.getPnd_Adsa401_Adp_Ivc_Pymnt_Rule(), pnd_Adsa600_Addl_Data_Pnd_Err_Cde_X, pnd_Adsa600_Addl_Data_Pnd_Program_Nme, pnd_Adsa600_Addl_Data_Pnd_Err_Nr, 
            pnd_Adsa600_Addl_Data_Pnd_Err_Line_X, pdaAdsa401.getPnd_Adsa401_Pnd_Ia_Origin_Code());
        if (condition(Global.isEscape())) return;
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
            getReports().write(0, "...AFTER ADSN600 CALL"," SYSERR NO : ",Global.getERROR_NR()," SYSERR LINE :",Global.getERROR_LINE(),"=",pnd_Adsa600_Addl_Data_Pnd_Err_Cde_X, //Natural: WRITE '...AFTER ADSN600 CALL' ' SYSERR NO : ' *ERROR-NR ' SYSERR LINE :' *ERROR-LINE '=' #ERR-CDE-X '=' #PROGRAM-NME '=' #ERR-NR '=' #ERR-LINE-X
                "=",pnd_Adsa600_Addl_Data_Pnd_Program_Nme,"=",pnd_Adsa600_Addl_Data_Pnd_Err_Nr,"=",pnd_Adsa600_Addl_Data_Pnd_Err_Line_X);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  030508 START
        if (condition(pnd_Adsa600_Addl_Data_Pnd_Err_Cde_X.notEquals(" ")))                                                                                                //Natural: IF #ERR-CDE-X NE ' '
        {
            if (condition(Global.getERROR_NR().greaterOrEqual(3001) && Global.getERROR_NR().lessOrEqual(3999)))                                                           //Natural: IF *ERROR-NR = 3001 THRU 3999
            {
                getCurrentProcessState().getDbConv().dbRollback();                                                                                                        //Natural: BACKOUT TRANSACTION
                pnd_Adsn998_Error_Handler_Pnd_Error_Msg.setValue("Encountered ADABAS Problem");                                                                           //Natural: ASSIGN #ERROR-MSG := 'Encountered ADABAS Problem'
                pnd_Misc_Variables_Pnd_Terminate_No.setValue(99);                                                                                                         //Natural: MOVE 99 TO #TERMINATE-NO
                pnd_Misc_Variables_Pnd_Terminate_Msg.setValue(pnd_Adsn998_Error_Handler_Pnd_Error_Msg);                                                                   //Natural: MOVE #ERROR-MSG TO #TERMINATE-MSG
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESS
                sub_Terminate_Process();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  030508 END
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((pnd_Adsa600_Addl_Data_Pnd_Err_Nr.notEquals(" ")) && (pnd_Adsa600_Addl_Data_Pnd_Err_Nr.notEquals("0000"))))                                         //Natural: IF ( #ERR-NR NE ' ' ) AND ( #ERR-NR NE '0000' )
        {
                                                                                                                                                                          //Natural: PERFORM HANDLE-SYSTEM-ERROR
            sub_Handle_System_Error();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Adsa600_Addl_Data_Pnd_Err_Cde_X.notEquals(" ")))                                                                                                //Natural: IF #ERR-CDE-X NE ' '
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            short decideConditionsMet3239 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #ERR-CDE-X;//Natural: VALUE #INACTIVE-IA
            if (condition((pnd_Adsa600_Addl_Data_Pnd_Err_Cde_X.equals(pnd_Constant_Variables_Pnd_Inactive_Ia))))
            {
                decideConditionsMet3239++;
                pnd_Adsn998_Error_Handler_Pnd_Error_Msg.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "ERROR RECEIVED FROM IA PROCESS - IA CONTROL RECORD NOT ACTIVE,",  //Natural: COMPRESS 'ERROR RECEIVED FROM IA PROCESS - IA CONTROL RECORD NOT ACTIVE,' 'ERROR CODE: ' #ERR-CDE-X ',RQST-ID: ' #ADSA401.RQST-ID TO #ERROR-MSG LEAVING NO SPACE
                    "ERROR CODE: ", pnd_Adsa600_Addl_Data_Pnd_Err_Cde_X, ",RQST-ID: ", pdaAdsa401.getPnd_Adsa401_Rqst_Id()));
                                                                                                                                                                          //Natural: PERFORM ERROR-LOG
                sub_Error_Log();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Misc_Variables_Pnd_Terminate_No.setValue(97);                                                                                                         //Natural: MOVE 97 TO #TERMINATE-NO
                pnd_Misc_Variables_Pnd_Terminate_Msg.setValue(pnd_Adsn998_Error_Handler_Pnd_Error_Msg);                                                                   //Natural: MOVE #ERROR-MSG TO #TERMINATE-MSG
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESS
                sub_Terminate_Process();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: VALUE #MISSING-IA
            else if (condition((pnd_Adsa600_Addl_Data_Pnd_Err_Cde_X.equals(pnd_Constant_Variables_Pnd_Missing_Ia))))
            {
                decideConditionsMet3239++;
                pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("IA-CALL");                                                                                              //Natural: MOVE 'IA-CALL' TO #STEP-NAME
                pnd_Adsn998_Error_Handler_Pnd_Error_Msg.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "ERROR RECEIVED FROM IA PROCESS - IA RECORD MISSING,",   //Natural: COMPRESS 'ERROR RECEIVED FROM IA PROCESS - IA RECORD MISSING,' 'ERROR CODE :' #ERR-CDE-X ',RQST-ID :' #ADSA401.RQST-ID TO #ERROR-MSG LEAVING NO SPACE
                    "ERROR CODE :", pnd_Adsa600_Addl_Data_Pnd_Err_Cde_X, ",RQST-ID :", pdaAdsa401.getPnd_Adsa401_Rqst_Id()));
                                                                                                                                                                          //Natural: PERFORM ERROR-LOG
                sub_Error_Log();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Misc_Variables_Pnd_Terminate_No.setValue(96);                                                                                                         //Natural: MOVE 96 TO #TERMINATE-NO
                pnd_Misc_Variables_Pnd_Terminate_Msg.setValue(pnd_Adsn998_Error_Handler_Pnd_Error_Msg);                                                                   //Natural: MOVE #ERROR-MSG TO #TERMINATE-MSG
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESS
                sub_Terminate_Process();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue(pnd_Adsa600_Addl_Data_Pnd_Err_Cde_X);                                                               //Natural: MOVE #ERR-CDE-X TO #ERROR-STATUS
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Handle_System_Error() throws Exception                                                                                                               //Natural: HANDLE-SYSTEM-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------- *
        pls_Tot_System_Errors.nadd(1);                                                                                                                                    //Natural: ADD 1 TO +TOT-SYSTEM-ERRORS
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        if (condition(pls_First_Err_Rqstid.equals(" ")))                                                                                                                  //Natural: IF +FIRST-ERR-RQSTID EQ ' '
        {
            pls_First_Err_Rqstid.setValue(pdaAdsa401.getPnd_Adsa401_Rqst_Id());                                                                                           //Natural: MOVE #ADSA401.RQST-ID TO +FIRST-ERR-RQSTID
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, ">>> ON ERROR FOR RQST-ID",ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id(),"TPGM: ",Global.getINIT_PROGRAM(),"PROG: ",Global.getPROGRAM(),"ERR: ",Global.getERROR_NR(),"ELIN: ",Global.getERROR_LINE(),"=",pnd_Misc_Variables_Pnd_Process_Step,"=",pnd_Adsa600_Addl_Data_Pnd_Err_Nr,"=",pnd_Adsn998_Error_Handler_Pnd_Calling_Program,NEWLINE,"*",new  //Natural: WRITE '>>> ON ERROR FOR RQST-ID' ADS-PRTCPNT-VIEW.RQST-ID '=' *INIT-PROGRAM '=' *PROGRAM '=' *ERROR-NR '=' *ERROR-LINE '=' #PROCESS-STEP '=' #ERR-NR '=' #CALLING-PROGRAM / '*' ( 100 )
            RepeatItem(100));
        if (Global.isEscape()) return;
        //*   >>> CHECK IF THE NATURAL ERROR OCCURED IN IA INTERFACE ADSN600 <<<<
        //*  -------------------------------------------------------------------
        if (condition(pnd_Adsn998_Error_Handler_Pnd_Calling_Program.equals("ADSN600")))                                                                                   //Natural: IF #CALLING-PROGRAM EQ 'ADSN600'
        {
            pnd_Adsn998_Error_Handler_Pnd_Error_Msg.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "INTERNAL SYSTEM ERROR RECEIVED FROM IA INTERFACE",          //Natural: COMPRESS 'INTERNAL SYSTEM ERROR RECEIVED FROM IA INTERFACE' ',PROGRAM: ' #PROGRAM-NME ',ERROR NO: ' #ERR-NR ',ERROR LINE: ' #ERR-LINE-X TO #ERROR-MSG LEAVING NO SPACE
                ",PROGRAM: ", pnd_Adsa600_Addl_Data_Pnd_Program_Nme, ",ERROR NO: ", pnd_Adsa600_Addl_Data_Pnd_Err_Nr, ",ERROR LINE: ", pnd_Adsa600_Addl_Data_Pnd_Err_Line_X));
            if (condition(pls_Debug_On.getBoolean()))                                                                                                                     //Natural: IF +DEBUG-ON
            {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-ADSN600-PARMS
                sub_Display_Adsn600_Parms();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getReports().write(0, "=",pnd_Adsn998_Error_Handler_Pnd_Error_Msg);                                                                                       //Natural: WRITE '=' #ERROR-MSG
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Adsn998_Error_Handler_Pnd_Error_Msg.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "INTERNAL ERROR ENCOUNTERED IN PROGRAM :",                   //Natural: COMPRESS 'INTERNAL ERROR ENCOUNTERED IN PROGRAM :' *INIT-PROGRAM ' ERR NO : ' *ERROR-NR ' ERR LINE :' *ERROR-LINE TO #ERROR-MSG LEAVING NO SPACE
                Global.getINIT_PROGRAM(), " ERR NO : ", Global.getERROR_NR(), " ERR LINE :", Global.getERROR_LINE()));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
            short decideConditionsMet3282 = 0;                                                                                                                            //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #DISPLAY-ADSA401
            if (condition(pnd_Misc_Variables_Pnd_Display_Adsa401.getBoolean()))
            {
                decideConditionsMet3282++;
                                                                                                                                                                          //Natural: PERFORM DISPLAY-#ADSA401
                sub_Display_Pnd_Adsa401();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: WHEN #DISPLAY-ADSA450
            if (condition(pnd_Misc_Variables_Pnd_Display_Adsa450.getBoolean()))
            {
                decideConditionsMet3282++;
                                                                                                                                                                          //Natural: PERFORM DISPLAY-#ADSA401
                sub_Display_Pnd_Adsa401();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet3282 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ERROR-LOG
        sub_Error_Log();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (condition(Global.getERROR_NR().equals(pnd_Constant_Variables_Pnd_Database_Error.getValue("*"))))                                                              //Natural: IF *ERROR-NR EQ #DATABASE-ERROR ( * )
        {
            pnd_Misc_Variables_Pnd_Terminate_No.setValue(99);                                                                                                             //Natural: MOVE 99 TO #TERMINATE-NO
            pnd_Misc_Variables_Pnd_Terminate_Msg.setValue(DbsUtil.compress(pnd_Adsn998_Error_Handler_Pnd_Error_Msg, " PRTCPNT RQST-ID : ", ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id())); //Natural: COMPRESS #ERROR-MSG ' PRTCPNT RQST-ID : ' ADS-PRTCPNT-VIEW.RQST-ID TO #TERMINATE-MSG
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESS
            sub_Terminate_Process();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet3300 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN +FIRST-ERR-RQSTID = #ADSA401.RQST-ID AND +NO-OF-RESTART > 0
            if (condition(pls_First_Err_Rqstid.equals(pdaAdsa401.getPnd_Adsa401_Rqst_Id()) && pls_No_Of_Restart.greater(getZero())))
            {
                decideConditionsMet3300++;
                pnd_Misc_Variables_Pnd_Terminate_Msg.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "INTERNAL ERROR STILL FOUND ON SECOND RE-START",            //Natural: COMPRESS 'INTERNAL ERROR STILL FOUND ON SECOND RE-START' ' CYCLE FOR PRTCPNT RQST-ID :' #ADSA401.RQST-ID TO #TERMINATE-MSG LEAVING NO SPACE
                    " CYCLE FOR PRTCPNT RQST-ID :", pdaAdsa401.getPnd_Adsa401_Rqst_Id()));
                pnd_Misc_Variables_Pnd_Terminate_No.setValue(93);                                                                                                         //Natural: MOVE 93 TO #TERMINATE-NO
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESS
                sub_Terminate_Process();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: WHEN +NO-OF-RESTART EQ 4
            else if (condition(pls_No_Of_Restart.equals(4)))
            {
                decideConditionsMet3300++;
                pnd_Misc_Variables_Pnd_Terminate_Msg.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "MAXIMUM RESTART TRY HAVE BEEN EXCEEDED, LATEST",           //Natural: COMPRESS 'MAXIMUM RESTART TRY HAVE BEEN EXCEEDED, LATEST' 'ERROR ENCOUNTERED IN PROGRAM : ' *INIT-PROGRAM ' ERROR NO: ' *ERROR-NR ' ERROR LINE: ' *ERROR-LINE ' PRTCPNT RQST-ID :' #ADSA401.RQST-ID TO #TERMINATE-MSG LEAVING NO SPACE
                    "ERROR ENCOUNTERED IN PROGRAM : ", Global.getINIT_PROGRAM(), " ERROR NO: ", Global.getERROR_NR(), " ERROR LINE: ", Global.getERROR_LINE(), 
                    " PRTCPNT RQST-ID :", pdaAdsa401.getPnd_Adsa401_Rqst_Id()));
                pnd_Misc_Variables_Pnd_Terminate_No.setValue(95);                                                                                                         //Natural: MOVE 95 TO #TERMINATE-NO
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESS
                sub_Terminate_Process();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: WHEN #ADSA401.#CUR-PRTCPNT-ISN NE 0
            else if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Prtcpnt_Isn().notEquals(getZero())))
            {
                decideConditionsMet3300++;
                PND_PND_L7735:                                                                                                                                            //Natural: GET ADS-PRTCPNT-VIEW #ADSA401.#CUR-PRTCPNT-ISN
                ldaAdsl401.getVw_ads_Prtcpnt_View().readByID(pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Prtcpnt_Isn().getLong(), "PND_PND_L7735");
                ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().setValue(pnd_Constant_Variables_Pnd_System_Error_Found);                                                    //Natural: MOVE #SYSTEM-ERROR-FOUND TO ADS-PRTCPNT-VIEW.ADP-STTS-CDE
                ldaAdsl401.getVw_ads_Prtcpnt_View().updateDBRow("PND_PND_L7735");                                                                                         //Natural: UPDATE ( ##L7735. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                Global.getERROR_NR().reset();                                                                                                                             //Natural: RESET *ERROR-NR
                pls_No_Of_Restart.nadd(1);                                                                                                                                //Natural: ADD 1 TO +NO-OF-RESTART
                //*      FETCH RETURN 'MDMP0012'                  /* CM- CLOSE MQ
                DbsUtil.invokeMain(DbsUtil.getBlType("ADSP401"), getCurrentProcessState());                                                                               //Natural: FETCH RETURN 'ADSP401'
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Misc_Variables_Pnd_Terminate_Msg.setValue(DbsUtil.compress(pnd_Adsn998_Error_Handler_Pnd_Error_Msg, " PRTCPNT RQST-ID : "));                          //Natural: COMPRESS #ERROR-MSG ' PRTCPNT RQST-ID : ' TO #TERMINATE-MSG
                pnd_Misc_Variables_Pnd_Terminate_No.setValue(94);                                                                                                         //Natural: MOVE 94 TO #TERMINATE-NO
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESS
                sub_Terminate_Process();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Display_Pnd_Adsa401() throws Exception                                                                                                               //Natural: DISPLAY-#ADSA401
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------- *
        getReports().write(0, NEWLINE,"*",new RepeatItem(100));                                                                                                           //Natural: WRITE / '*' ( 100 )
        if (Global.isEscape()) return;
        getReports().write(0, ">>> PROCESS STEP : ",pnd_Misc_Variables_Pnd_Process_Step);                                                                                 //Natural: WRITE '>>> PROCESS STEP : ' #PROCESS-STEP
        if (Global.isEscape()) return;
        //*  RS0
        //*  RS0
        //*  RS0
        //*  RS0
        getReports().write(0, " =====> VALUES IN #ADSA401 PARM < =====",NEWLINE,"** #ADSA401-PRTCPNT-RCRD **","=",pdaAdsa401.getPnd_Adsa401_Rqst_Id(),NEWLINE,new         //Natural: WRITE ' =====> VALUES IN #ADSA401 PARM < =====' / '** #ADSA401-PRTCPNT-RCRD **' '=' #ADSA401.RQST-ID /5T '=' #ADSA401.ADP-IA-TIAA-NBR 40T '=' #ADSA401.ADP-IA-TIAA-REA-NBR /5T '=' #ADSA401.ADP-IA-TIAA-PAYEE-CD 40T '=' #ADSA401.ADP-IA-CREF-NBR /5T '=' #ADSA401.ADP-IA-CREF-PAYEE-CD 40T '=' #ADSA401.ADP-BPS-UNIT /5T '=' #ADSA401.ADP-ANNT-TYP-CDE 40T '=' #ADSA401.ADP-EMPLYMNT-TERM-DTE /5T '=' #ADSA401.ADP-CNTRCTS-IN-RQST ( * ) /5T '=' #ADSA401.ADP-UNIQUE-ID /5T '=' #ADSA401.ADP-EFFCTV-DTE 40T '=' #ADSA401.ADP-CNTR-PRT-ISSUE-DTE /5T '=' #ADSA401.ADP-ENTRY-DTE 40T '=' #ADSA401.ADP-ENTRY-TME /5T '=' #ADSA401.ADP-ENTRY-USER-ID 40T '=' #ADSA401.ADP-REP-NME /5T '=' #ADSA401.ADP-LST-ACTVTY-DTE 40T '=' #ADSA401.ADP-WPID /3T '=' #ADSA401.ADP-MIT-LOG-DTE-TME 40T '=' #ADSA401.ADP-OPN-CLSD-IND /5T '=' #ADSA401.ADP-IN-PRGRSS-IND 40T '=' #ADSA401.ADP-RCPRCL-DTE /5T '=' #ADSA401.ADP-STTS-CDE 40T '=' #ADSA401.ADP-FRST-ANNT-SSN /5T '=' #ADSA401.ADP-SCND-ANNT-SSN 40T '=' #ADSA401.ADP-ANNTY-OPTN /5T '=' #ADSA401.ADP-GRNTEE-PERIOD 40T '=' #ADSA401.ADP-GRNTEE-PERIOD-MNTHS /5T '=' #ADSA401.ADP-ANNTY-STRT-DTE 40T '=' #ADSA401.ADP-PYMNT-MODE /5T '=' #ADSA401.ADP-SETTL-ALL-TIAA-GRD-PCT 40T '=' #ADSA401.ADP-SETTL-ALL-CREF-MON-PCT /5T '=' #ADSA401.ADP-RQST-LAST-UPDTD-ID 40T '=' #ADSA401.ADP-RQST-LAST-UPDTD-DTE /5T '=' #ADSA401.ADP-RSTTLMNT-CNT 40T '=' #ADSA401.ADP-STATE-OF-ISSUE /5T '=' #ADSA401.ADP-ORGNL-ISSUE-STATE 40T '=' #ADSA401.ADP-IVC-RLVR-CDE /5T '=' #ADSA401.ADP-IVC-PYMNT-RULE 40T '=' #ADSA401.ADP-FRM-IRC-CDE ( * ) /5T '=' #ADSA401.ADP-TO-IRC-CDE 40T '=' #ADSA401.ADP-ROTH-PLAN-TYPE /5T '=' #ADSA401.ADP-ROTH-FRST-CNTRBTN-DTE 2X '=' #ADSA401.ADP-ROTH-DSBLTY-DTE /5T '=' #ADSA401.ADP-ROTH-RQST-IND / '** #ADSA401-CNTRCT-RCRD **' /5T '=' #ADSA401.ADC-RQST-ID /5T '=' #ADSA401.ADC-SQNCE-NBR /5T '=' #ADSA401.ADC-STTS-CDE 40T '=' #ADSA401.ADC-LST-ACTVTY-DTE / '=' #ADSA401.ADC-TIAA-NBR 40T '=' #ADSA401.ADC-CREF-NBR /5T '=' #ADSA401.ADC-GD-CNTRCT-IND 40T '=' #ADSA401.ADC-TIAA-IVC-GD-IND /5T '=' #ADSA401.ADC-CREF-IVC-GD-IND 40T '=' #ADSA401.ADC-CNTRCT-ISSUE-STATE /5T '=' #ADSA401.ADC-CNTRCT-ISSUE-DTE 40T '=' #ADSA401.ADC-HOLD-CDE /5T '=' #ADSA401.ADC-ORIG-LOB-IND / '**** ADC-RQST-INFO ****'
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Ia_Tiaa_Nbr(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Ia_Tiaa_Rea_Nbr(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Ia_Tiaa_Payee_Cd(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Ia_Cref_Nbr(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Ia_Cref_Payee_Cd(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Bps_Unit(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Annt_Typ_Cde(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Emplymnt_Term_Dte(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Cntrcts_In_Rqst().getValue("*"),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Unique_Id(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Effctv_Dte(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Cntr_Prt_Issue_Dte(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Entry_Dte(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Entry_Tme(),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Entry_User_Id(),new 
            TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Rep_Nme(),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Lst_Actvty_Dte(),new 
            TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Wpid(),NEWLINE,new TabSetting(3),"=",pdaAdsa401.getPnd_Adsa401_Adp_Mit_Log_Dte_Tme(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Opn_Clsd_Ind(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_In_Prgrss_Ind(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Rcprcl_Dte(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Stts_Cde(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Frst_Annt_Ssn(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Scnd_Annt_Ssn(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Annty_Optn(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Grntee_Period(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Grntee_Period_Mnths(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Annty_Strt_Dte(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Pymnt_Mode(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Settl_All_Tiaa_Grd_Pct(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Settl_All_Cref_Mon_Pct(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Rqst_Last_Updtd_Id(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Rqst_Last_Updtd_Dte(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Rsttlmnt_Cnt(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_State_Of_Issue(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Orgnl_Issue_State(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Ivc_Rlvr_Cde(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Ivc_Pymnt_Rule(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Frm_Irc_Cde().getValue("*"),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_To_Irc_Cde(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Roth_Plan_Type(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Roth_Frst_Cntrbtn_Dte(),new ColumnSpacing(2),"=",pdaAdsa401.getPnd_Adsa401_Adp_Roth_Dsblty_Dte(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Roth_Rqst_Ind(),NEWLINE,"** #ADSA401-CNTRCT-RCRD **",NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Rqst_Id(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Sqnce_Nbr(),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Stts_Cde(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Lst_Actvty_Dte(),NEWLINE,"=",pdaAdsa401.getPnd_Adsa401_Adc_Tiaa_Nbr(),new 
            TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Cref_Nbr(),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Gd_Cntrct_Ind(),new 
            TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Tiaa_Ivc_Gd_Ind(),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Cref_Ivc_Gd_Ind(),new 
            TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Cntrct_Issue_State(),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Cntrct_Issue_Dte(),new 
            TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Hold_Cde(),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Orig_Lob_Ind(),NEWLINE,
            "**** ADC-RQST-INFO ****");
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #P 1 TO 20
        for (pnd_Counters_Pnd_P.setValue(1); condition(pnd_Counters_Pnd_P.lessOrEqual(20)); pnd_Counters_Pnd_P.nadd(1))
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_P).equals(" ") && pdaAdsa401.getPnd_Adsa401_Adc_Tkr_Symbl().getValue(pnd_Counters_Pnd_P).equals(" "))) //Natural: IF #ADSA401.ADC-ACCT-CDE ( #P ) EQ ' ' AND #ADSA401.ADC-TKR-SYMBL ( #P ) EQ ' '
            {
                getReports().write(0, ".....END OF ADC-RQST-INFO");                                                                                                       //Natural: WRITE '.....END OF ADC-RQST-INFO'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "=",pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_P),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Tkr_Symbl().getValue(pnd_Counters_Pnd_P),NEWLINE,"=",pdaAdsa401.getPnd_Adsa401_Adc_Acct_Stts_Cde().getValue(pnd_Counters_Pnd_P),new  //Natural: WRITE '=' #ADSA401.ADC-ACCT-CDE ( #P ) 40T '=' #ADSA401.ADC-TKR-SYMBL ( #P ) / '=' #ADSA401.ADC-ACCT-STTS-CDE ( #P ) 40T '=' #ADSA401.ADC-ACCT-TYP ( #P ) / '=' #ADSA401.ADC-ACCT-QTY ( #P ) 40T '=' #ADSA401.ADC-ACCT-ACTL-AMT ( #P ) / '=' #ADSA401.ADC-ACCT-ACTL-NBR-UNITS ( #P ) 40T '=' #ADSA401.ADC-GRD-MNTHLY-TYP ( #P ) / '=' #ADSA401.ADC-GRD-MNTHLY-QTY ( #P ) 40T '=' #ADSA401.ADC-GRD-MNTHLY-ACTL-AMT ( #P ) / '=' #ADSA401.ADC-STNDRD-ANNL-ACTL-AMT ( #P ) 50T '=' #ADSA401.ADC-ACCT-CREF-RATE-CDE ( #P ) / '=' #ADSA401.ADC-IVC-AMT ( #P ) 40T '=' #ADSA401.ADC-ACCT-OPN-ACCUM-AMT ( #P ) / '=' #ADSA401.ADC-ACCT-OPN-NBR-UNITS ( #P )
                TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Acct_Typ().getValue(pnd_Counters_Pnd_P),NEWLINE,"=",pdaAdsa401.getPnd_Adsa401_Adc_Acct_Qty().getValue(pnd_Counters_Pnd_P),new 
                TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Acct_Actl_Amt().getValue(pnd_Counters_Pnd_P),NEWLINE,"=",pdaAdsa401.getPnd_Adsa401_Adc_Acct_Actl_Nbr_Units().getValue(pnd_Counters_Pnd_P),new 
                TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Typ().getValue(pnd_Counters_Pnd_P),NEWLINE,"=",pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Qty().getValue(pnd_Counters_Pnd_P),new 
                TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_Counters_Pnd_P),NEWLINE,"=",pdaAdsa401.getPnd_Adsa401_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_Counters_Pnd_P),new 
                TabSetting(50),"=",pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cref_Rate_Cde().getValue(pnd_Counters_Pnd_P),NEWLINE,"=",pdaAdsa401.getPnd_Adsa401_Adc_Ivc_Amt().getValue(pnd_Counters_Pnd_P),new 
                TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Acct_Opn_Accum_Amt().getValue(pnd_Counters_Pnd_P),NEWLINE,"=",pdaAdsa401.getPnd_Adsa401_Adc_Acct_Opn_Nbr_Units().getValue(pnd_Counters_Pnd_P));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  EM - 030112
        getReports().write(0, "=",pdaAdsa401.getPnd_Adsa401_Adc_Err_Cde().getValue(1),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Err_Acct_Cde().getValue(1),    //Natural: WRITE '=' #ADSA401.ADC-ERR-CDE ( 1 ) 40T '=' #ADSA401.ADC-ERR-ACCT-CDE ( 1 ) / '=' #ADSA401.ADC-ERR-MSG ( 1 ) / '=' #ADSA401.ADC-TRK-RO-IVC / '**** ADC-TIAA-DATA ****'
            NEWLINE,"=",pdaAdsa401.getPnd_Adsa401_Adc_Err_Msg().getValue(1),NEWLINE,"=",pdaAdsa401.getPnd_Adsa401_Adc_Trk_Ro_Ivc(),NEWLINE,"**** ADC-TIAA-DATA ****");
        if (Global.isEscape()) return;
        FOR04:                                                                                                                                                            //Natural: FOR #P 1 TO #MAX-RATES
        for (pnd_Counters_Pnd_P.setValue(1); condition(pnd_Counters_Pnd_P.lessOrEqual(pnd_Constant_Variables_Pnd_Max_Rates)); pnd_Counters_Pnd_P.nadd(1))
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_Counters_Pnd_P).equals(" ")))                                                    //Natural: IF #ADSA401.ADC-DTL-TIAA-RATE-CDE ( #P ) EQ ' '
            {
                getReports().write(0, new TabSetting(5),".... END OF ADC-TIAA-DATA ");                                                                                    //Natural: WRITE 5T '.... END OF ADC-TIAA-DATA '
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_Counters_Pnd_P),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_Counters_Pnd_P),NEWLINE,new  //Natural: WRITE 5T '=' #ADSA401.ADC-DTL-TIAA-RATE-CDE ( #P ) 40T '=' #ADSA401.ADC-DTL-TIAA-ACTL-AMT ( #P ) /5T '=' #ADSA401.ADC-DTL-TIAA-GRD-ACTL-AMT ( #P ) 50T '=' #ADSA401.ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #P ) /5T '=' #ADSA401.ADC-DTL-TIAA-IA-RATE-CDE ( #P ) 40T '=' #ADSA401.ADC-DTL-TIAA-OPN-ACCUM-AMT ( #P ) /5T '=' #ADSA401.ADC-TPA-GUARANT-COMMUT-AMT ( #P ) 50T '=' #ADSA401.ADC-TPA-GUARANT-COMMUT-GRD-AMT ( #P ) /5T '=' #ADSA401.ADC-TPA-GUARANT-COMMUT-STD-AMT ( #P )
                TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_Counters_Pnd_P),new TabSetting(50),"=",pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_Counters_Pnd_P),NEWLINE,new 
                TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Ia_Rate_Cde().getValue(pnd_Counters_Pnd_P),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Opn_Accum_Amt().getValue(pnd_Counters_Pnd_P),NEWLINE,new 
                TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Amt().getValue(pnd_Counters_Pnd_P),new TabSetting(50),"=",pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Grd_Amt().getValue(pnd_Counters_Pnd_P),NEWLINE,new 
                TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Std_Amt().getValue(pnd_Counters_Pnd_P));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* * 08312005 MN END
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "*** #CURRENT-CNTRCT-PRTCPNT-INFO ***",NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Cntrct_Isn(),new                     //Natural: WRITE '*** #CURRENT-CNTRCT-PRTCPNT-INFO ***' /5T '=' #ADSA401.#CUR-CNTRCT-ISN 40T '=' #ADSA401.#CUR-PRTCPNT-ISN /5T '=' #ADSA401.#CUR-IARSLT-ISN ( 1 ) 40T '=' #ADSA401.#CUR-IARSLT-ISN ( 2 ) /5T '=' #ADSA401.#IA-CNTRCT-LOB /5T '=' #ADSA401.#IA-CNTRCT-SUB-LOB 40T '=' #ADSA401.#CNTRCT-LOB /5T '=' #ADSA401.#CNTRCT-SUB-LOB 40T '=' #ADSA401.#ERROR-STATUS /5T '=' #ADSA401.#CHECK-RULE-ELGBLTY 40T '=' #ADSA401.#TOTAL-TIAA-IVC-AMOUNT /5T '=' #ADSA401.#TOTAL-CREF-IVC-AMOUNT /5T '=' #ADSA401.#IA-ORIGIN-CODE
            TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Prtcpnt_Isn(),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Iarslt_Isn().getValue(1),new 
            TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Iarslt_Isn().getValue(2),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Ia_Cntrct_Lob(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Ia_Cntrct_Sub_Lob(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Lob(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Sub_Lob(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Check_Rule_Elgblty(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Total_Tiaa_Ivc_Amount(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Total_Cref_Ivc_Amount(),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Ia_Origin_Code());
        if (Global.isEscape()) return;
    }
    private void sub_Display_Pnd_Adsa450() throws Exception                                                                                                               //Natural: DISPLAY-#ADSA450
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------- *
        getReports().write(0, NEWLINE,"*",new RepeatItem(100));                                                                                                           //Natural: WRITE / '*' ( 100 )
        if (Global.isEscape()) return;
        getReports().write(0, ">>> PROCESS STEP : ",pnd_Misc_Variables_Pnd_Process_Step);                                                                                 //Natural: WRITE '>>> PROCESS STEP : ' #PROCESS-STEP
        if (Global.isEscape()) return;
        //*  022208
        getReports().write(0, "====> VALUES IN ADSA450-ACTUARIAL-DATA PARM <====",NEWLINE,new TabSetting(5),"=",pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Ia_Next_Cycle_Check_Dte(),new  //Natural: WRITE '====> VALUES IN ADSA450-ACTUARIAL-DATA PARM <====' /5T '=' #IA-NEXT-CYCLE-CHECK-DTE 40T '=' #ACCOUNT-C /5T '=' #ACCOUNT-R 40T '=' #ACCOUNT-T /5T '=' #ALL-TIAA-CONTRACTS ( * ) /5T '=' #ALL-CREF-CONTRACTS ( * ) / '*** #DTL-TIAA-DATA ***'
            TabSetting(40),"=",pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Account_C(),NEWLINE,new TabSetting(5),"=",pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Account_R(),new 
            TabSetting(40),"=",pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Account_T(),NEWLINE,new TabSetting(5),"=",pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_All_Tiaa_Contracts().getValue("*"),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_All_Cref_Contracts().getValue("*"),NEWLINE,"*** #DTL-TIAA-DATA ***");
        if (Global.isEscape()) return;
        FOR05:                                                                                                                                                            //Natural: FOR #P 1 TO #MAX-RATES
        for (pnd_Counters_Pnd_P.setValue(1); condition(pnd_Counters_Pnd_P.lessOrEqual(pnd_Constant_Variables_Pnd_Max_Rates)); pnd_Counters_Pnd_P.nadd(1))
        {
            if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde().getValue(pnd_Counters_Pnd_P).equals(" ")))                                         //Natural: IF #TIAA-RATE-CDE ( #P ) EQ ' '
            {
                getReports().write(0, new TabSetting(5),"....END OF #DTL-TIAA-DATA ");                                                                                    //Natural: WRITE 5T'....END OF #DTL-TIAA-DATA '
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, new TabSetting(5),"=",pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde().getValue(pnd_Counters_Pnd_P),new                     //Natural: WRITE 5T '=' #TIAA-RATE-CDE ( #P ) 40T '=' #TIAA-STNDRD-ACTL-AMT ( #P ) 80T '=' #TIAA-GRADED-ACTL-AMT ( #P ) /5T '=' #TIAA-STNDRD-GUAR-COMM-AMT ( #P ) 50T '=' #TIAA-GRADED-GUAR-COMM-AMT ( #P )
                TabSetting(40),"=",pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Actl_Amt().getValue(pnd_Counters_Pnd_P),new TabSetting(80),"=",pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Actl_Amt().getValue(pnd_Counters_Pnd_P),NEWLINE,new 
                TabSetting(5),"=",pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Guar_Comm_Amt().getValue(pnd_Counters_Pnd_P),new TabSetting(50),
                "=",pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Guar_Comm_Amt().getValue(pnd_Counters_Pnd_P));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "*** #DTL-CREF-DATA ***");                                                                                                                  //Natural: WRITE '*** #DTL-CREF-DATA ***'
        if (Global.isEscape()) return;
        FOR06:                                                                                                                                                            //Natural: FOR #P 1 TO 12
        for (pnd_Counters_Pnd_P.setValue(1); condition(pnd_Counters_Pnd_P.lessOrEqual(12)); pnd_Counters_Pnd_P.nadd(1))
        {
            if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Acct_Cde().getValue(pnd_Counters_Pnd_P).equals(" ")))                                         //Natural: IF #CREF-ACCT-CDE ( #P ) EQ ' '
            {
                getReports().write(0, new TabSetting(5),"....END OF #DTL-CREF-DATA");                                                                                     //Natural: WRITE 5T '....END OF #DTL-CREF-DATA'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, new TabSetting(5),"=",pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Acct_Cde().getValue(pnd_Counters_Pnd_P),new                     //Natural: WRITE 5T '=' #CREF-ACCT-CDE ( #P ) 40T '=' #CREF-RATE-CDE ( #P ) /5T '=' #CREF-GRADED-MNTHLY-ACTL-AMT ( #P ) 50T '=' #CREF-STNDRD-ANNUAL-ACTL-AMT ( #P )
                TabSetting(40),"=",pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Rate_Cde().getValue(pnd_Counters_Pnd_P),NEWLINE,new TabSetting(5),"=",pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Graded_Mnthly_Actl_Amt().getValue(pnd_Counters_Pnd_P),new 
                TabSetting(50),"=",pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Stndrd_Annual_Actl_Amt().getValue(pnd_Counters_Pnd_P));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* * 12052006 MN
        //*  DG 12/07
        if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().equals("L63") || pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().equals("L66") || pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().equals("L67"))) //Natural: IF #ERROR-STATUS = 'L63' OR = 'L66' OR = 'L67'
        {
            getReports().write(0, "***********   ERROR DURING COMLIANCE TEST  ************* ");                                                                           //Natural: WRITE '***********   ERROR DURING COMLIANCE TEST  ************* '
            if (Global.isEscape()) return;
            getReports().write(0, "=",pnd_Addit_450_Parameters_Pnd_Return_Message);                                                                                       //Natural: WRITE '=' #RETURN-MESSAGE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* * 12052006 MN
    }
    private void sub_Display_Adsn600_Parms() throws Exception                                                                                                             //Natural: DISPLAY-ADSN600-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------- *
        getReports().write(0, NEWLINE,"*",new RepeatItem(100));                                                                                                           //Natural: WRITE / '*' ( 100 )
        if (Global.isEscape()) return;
        getReports().write(0, ">>> PROCESS STEP : ",pnd_Misc_Variables_Pnd_Process_Step);                                                                                 //Natural: WRITE '>>> PROCESS STEP : ' #PROCESS-STEP
        if (Global.isEscape()) return;
        getReports().write(0, " >>>>> ADSN600-PARMS <<<<<",NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Iarslt_Isn().getValue(1),new                   //Natural: WRITE ' >>>>> ADSN600-PARMS <<<<<' /5T '=' #ADSA401.#CUR-IARSLT-ISN ( 1 ) 40T '=' #ADSA401.#CUR-IARSLT-ISN ( 2 ) /5T '=' #ADSA401.#CUR-PRTCPNT-ISN /5T '=' #ADSA401.#CUR-CNTRCT-ISN 40T '=' #ADSA401.#CNTL-BSNSS-DTE /5T '=' #ADSA401.#CNTL-BSNSS-TMRRW-DTE 40T '=' #ADSA401.ADP-ORGNL-ISSUE-STATE /5T '=' #ADSA401.ADP-ALT-DEST-HOLD-CDE /5T '=' #ADSA401.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( * ) /5T '=' #ADSA401.ADP-CRRSPNDNCE-PERM-ADDR-ZIP 50T '=' #ADSA401.ADP-CRRSPNDNCE-PERM-ADDR-TYP-CD /5T '=' #ADSA401.#IA-ORIGIN-CODE /5T '=' #PRDC-IVC 40T '=' #ADSA401.ADP-IVC-PYMNT-RULE /5T '=' #ERR-CDE-X 40T '=' #PROGRAM-NME /5T '=' #ERR-NR 40T '=' #ERR-LINE-X
            TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Iarslt_Isn().getValue(2),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Prtcpnt_Isn(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Cntrct_Isn(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Cntl_Bsnss_Dte(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Cntl_Bsnss_Tmrrw_Dte(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Orgnl_Issue_State(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Alt_Dest_Hold_Cde(),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Txt().getValue("*"),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Zip(),new TabSetting(50),"=",pdaAdsa401.getPnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Typ_Cd(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Ia_Origin_Code(),NEWLINE,new TabSetting(5),"=",pnd_Adsa600_Addl_Data_Pnd_Prdc_Ivc,new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Ivc_Pymnt_Rule(),NEWLINE,new 
            TabSetting(5),"=",pnd_Adsa600_Addl_Data_Pnd_Err_Cde_X,new TabSetting(40),"=",pnd_Adsa600_Addl_Data_Pnd_Program_Nme,NEWLINE,new TabSetting(5),"=",pnd_Adsa600_Addl_Data_Pnd_Err_Nr,new 
            TabSetting(40),"=",pnd_Adsa600_Addl_Data_Pnd_Err_Line_X);
        if (Global.isEscape()) return;
    }
    private void sub_Display_Adsa043_Parms() throws Exception                                                                                                             //Natural: DISPLAY-ADSA043-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------- *
        getReports().write(0, NEWLINE,"*",new RepeatItem(100));                                                                                                           //Natural: WRITE / '*' ( 100 )
        if (Global.isEscape()) return;
        getReports().write(0, ">>> PROCESS STEP : ",pnd_Misc_Variables_Pnd_Process_Step);                                                                                 //Natural: WRITE '>>> PROCESS STEP : ' #PROCESS-STEP
        if (Global.isEscape()) return;
        getReports().write(0, " >>>>> ADSA043-PARMS <<<<<",NEWLINE,new TabSetting(5),"=",pnd_Adsa043_Call_Cis_Pnd_Rsdnce_Typ,new TabSetting(40),"=",pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Rsdnce,NEWLINE,new  //Natural: WRITE ' >>>>> ADSA043-PARMS <<<<<' /5T '=' #ADSA043-CALL-CIS.#RSDNCE-TYP 40T '=' #ADSA043-CALL-CIS.#FRST-ANNT-RSDNCE /5T '=' #ADSA043-CALL-CIS.#FRST-ANNT-CTZNSHP 40T '=' #ADSA043-CALL-CIS.#ANNTY-STRT-DTE /5T '=' #ADSA043-CALL-CIS.#DA-TIAA-NBR ( * ) /5T '=' #ADSA043-CALL-CIS.#COR-PIN-NBR 40T '=' #ADSA043-CALL-CIS.#FRST-ANNT-SSN /5T '=' #ADSA043-CALL-CIS.#FRST-ANNT-DTE-OF-BRTH 40T '=' #ADSA043-CALL-CIS.#ORG-ISSUE-ST /5T '=' #ADSA043-CALL-CIS.#RSDNCE-ISSUE-ST 40T '=' #ADSA043-CALL-CIS.#IA-CNTRCT-APPRVL-IND ( * )
            TabSetting(5),"=",pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Ctznshp,new TabSetting(40),"=",pnd_Adsa043_Call_Cis_Pnd_Annty_Strt_Dte,NEWLINE,new TabSetting(5),"=",pnd_Adsa043_Call_Cis_Pnd_Da_Tiaa_Nbr.getValue("*"),NEWLINE,new 
            TabSetting(5),"=",pnd_Adsa043_Call_Cis_Pnd_Cor_Pin_Nbr,new TabSetting(40),"=",pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Ssn,NEWLINE,new TabSetting(5),"=",pnd_Adsa043_Call_Cis_Pnd_Frst_Annt_Dte_Of_Brth,new 
            TabSetting(40),"=",pnd_Adsa043_Call_Cis_Pnd_Org_Issue_St,NEWLINE,new TabSetting(5),"=",pnd_Adsa043_Call_Cis_Pnd_Rsdnce_Issue_St,new TabSetting(40),
            "=",pnd_Adsa043_Call_Cis_Pnd_Ia_Cntrct_Apprvl_Ind.getValue("*"));
        if (Global.isEscape()) return;
        FOR07:                                                                                                                                                            //Natural: FOR #P 1 TO 20
        for (pnd_Counters_Pnd_P.setValue(1); condition(pnd_Counters_Pnd_P.lessOrEqual(20)); pnd_Counters_Pnd_P.nadd(1))
        {
            if (condition(pnd_Adsa043_Call_Cis_Pnd_Issue_State_Code.getValue(pnd_Counters_Pnd_P).equals(" ")))                                                            //Natural: IF #ISSUE-STATE-CODE ( #P ) EQ ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, new TabSetting(5),"=",pnd_Adsa043_Call_Cis_Pnd_Issue_State_Code.getValue(pnd_Counters_Pnd_P),new TabSetting(40),"=",                    //Natural: WRITE 5T '=' #ADSA043-CALL-CIS.#ISSUE-STATE-CODE ( #P ) 40T '=' #ADSA043-CALL-CIS.#ISSUE-DATE ( #P )
                pnd_Adsa043_Call_Cis_Pnd_Issue_Date.getValue(pnd_Counters_Pnd_P));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        //*  CM - OPEN MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0")))                                                                   //Natural: IF ##DATA-RESPONSE ( 1 ) = '0'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR08:                                                                                                                                                            //Natural: FOR #I = 1 TO 60
        for (pnd_Counters_Pnd_I.setValue(1); condition(pnd_Counters_Pnd_I.lessOrEqual(60)); pnd_Counters_Pnd_I.nadd(1))
        {
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_Counters_Pnd_I))); //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Rc);                                                                                                                                //Natural: WRITE '=' #RC
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    }
    private void sub_End_Of_Job_Message() throws Exception                                                                                                                //Natural: END-OF-JOB-MESSAGE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------- *
        getReports().write(0, NEWLINE,NEWLINE,NEWLINE,NEWLINE," ================================================================= ",NEWLINE,"|**                                                             **|", //Natural: WRITE /// /' ================================================================= ' /'|**                                                             **|' /'|**            ANNUITIZATION OF OMNI PLUS CONTRACTS             **|' /'|**                                                             **|' /'|**   ADAS ANNUITIZATION STEP 2 PROCESS ---- JOB RUN ENDED      **|' /'|**                                                             **|' /'|** TOTAL PRTCPNT REQUESTS READ     : ' +TOT-PRTCPNT-RQST '               **|' /'|** TOTAL CONTRACTS PROCESSED       : ' +TOT-CNTRCT-RECS '               **|' /'|** TOTAL IN-PROGRESS RQST COMPLETED: ' +TOT-IN-PROGRESS '               **|' /'|** TOTAL RQS SUCCESSFULLY PROCESSED: ' +TOT-RQST-SUCCESSFUL '               **|' /'|** TOTAL RQST WITH VALIDATION ERROR: ' +TOT-RQST-ERRORS '               **|' /'|** TOTAL REQUEST WITH SYSTEM ERROR : ' +TOT-SYSTEM-ERRORS '               **|' /'|**                                                             **|' /'|**                                                             **|' /' ================================================================= '
            NEWLINE,"|**            ANNUITIZATION OF OMNI PLUS CONTRACTS             **|",NEWLINE,"|**                                                             **|",
            NEWLINE,"|**   ADAS ANNUITIZATION STEP 2 PROCESS ---- JOB RUN ENDED      **|",NEWLINE,"|**                                                             **|",
            NEWLINE,"|** TOTAL PRTCPNT REQUESTS READ     : ",pls_Tot_Prtcpnt_Rqst,"               **|",NEWLINE,"|** TOTAL CONTRACTS PROCESSED       : ",
            pls_Tot_Cntrct_Recs,"               **|",NEWLINE,"|** TOTAL IN-PROGRESS RQST COMPLETED: ",pls_Tot_In_Progress,"               **|",NEWLINE,"|** TOTAL RQS SUCCESSFULLY PROCESSED: ",
            pls_Tot_Rqst_Successful,"               **|",NEWLINE,"|** TOTAL RQST WITH VALIDATION ERROR: ",pls_Tot_Rqst_Errors,"               **|",NEWLINE,
            "|** TOTAL REQUEST WITH SYSTEM ERROR : ",pls_Tot_System_Errors,"               **|",NEWLINE,"|**                                                             **|",
            NEWLINE,"|**                                                             **|",NEWLINE," ================================================================= ");
        if (Global.isEscape()) return;
    }
    private void sub_Terminate_Process() throws Exception                                                                                                                 //Natural: TERMINATE-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------- *
        getReports().write(0, NEWLINE,NEWLINE,NEWLINE,NEWLINE," ================================================================= ",NEWLINE,"|*****************************************************************|", //Natural: WRITE /// /' ================================================================= ' /'|*****************************************************************|' /'|*****************************************************************|' /'|** TERMINATING ADAS BATCH PROCESS.....                         **|' /'|**                                                             **|' /'|**' #TERMINATE-MSG1 '**|' /'|**' #TERMINATE-MSG2 '**|' /'|**                                                             **|' /'|** >>> PLEASE CONTACT ANNUITIZATION - ADAS SYSTEM SUPPORT <<<  **|' /'|*****************************************************************|' /'|*****************************************************************|' /' ================================================================= '
            NEWLINE,"|*****************************************************************|",NEWLINE,"|** TERMINATING ADAS BATCH PROCESS.....                         **|",
            NEWLINE,"|**                                                             **|",NEWLINE,"|**",pnd_Misc_Variables_Pnd_Terminate_Msg1,"**|",NEWLINE,
            "|**",pnd_Misc_Variables_Pnd_Terminate_Msg2,"**|",NEWLINE,"|**                                                             **|",NEWLINE,"|** >>> PLEASE CONTACT ANNUITIZATION - ADAS SYSTEM SUPPORT <<<  **|",
            NEWLINE,"|*****************************************************************|",NEWLINE,"|*****************************************************************|",
            NEWLINE," ================================================================= ");
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB-MESSAGE
        sub_End_Of_Job_Message();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        DbsUtil.terminate(pnd_Misc_Variables_Pnd_Terminate_No);  if (true) return;                                                                                        //Natural: TERMINATE #TERMINATE-NO
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
                                                                                                                                                                          //Natural: PERFORM HANDLE-SYSTEM-ERROR
        sub_Handle_System_Error();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=120 PS=100");
    }
}
