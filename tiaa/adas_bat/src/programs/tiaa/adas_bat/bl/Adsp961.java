/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:05:52 PM
**        * FROM NATURAL PROGRAM : Adsp961
************************************************************
**        * FILE NAME            : Adsp961.java
**        * CLASS NAME           : Adsp961
**        * INSTANCE NAME        : Adsp961
************************************************************
************************************************************************
* PROGRAM  : ADSP961
* GENERATED: APRIL 13, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : READ SORTED WORK FILE FOR ALL UNITS.
*            GENERATES REJECTION/CANCELLED REPORT, BREAKING BY UNIT
*
* REMARKS  : CLONED FROM NAZP961 (ADAM SYSTEM)
************************************************************************
* MOD DATE   MOD BY      DESCRIPTION OF CHANGES
* 02-23-99   DON MEADE   WRITE REPORT TO CMPRT01
* 04/13/04   C AVE       MODIFIED FOR ADAS (ANNUITIZATION SUNGUARD)
* 07/27/04   T. SHINE       IF MONTH-END PROCESS FALLS ON WEEKEND OR
*                           HOLIDAY, USE ADS-RPT-13 (FACTOR DATE) AS
*                           REPORT DATE.
* 02/23/2017 R.CARREON   PIN EXPANSION 02232017
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp961 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cntlrcd;

    private DbsGroup cntlrcd_Ads_Cntl_Grp;
    private DbsField cntlrcd_Ads_Cntl_Rcrd_Typ_Cde;
    private DbsField cntlrcd_Ads_Cntl_Bsnss_Rcprcl_Dte;
    private DbsField cntlrcd_Ads_Cntl_Bsnss_Dte;
    private DbsField cntlrcd_Ads_Cntl_Bsnss_Tmrrw_Dte;
    private DbsField cntlrcd_Ads_Rpt_13;

    private DataAccessProgramView vw_tblrec;
    private DbsField tblrec_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField tblrec_Naz_Tbl_Rcrd_Dscrptn_Txt;

    private DbsGroup tblrec__R_Field_1;
    private DbsField tblrec_Pnd_Tbl_Rcrd_Wpid;
    private DbsField tblrec_Pnd_Dscrptn_Txt_Filler_1;
    private DbsField tblrec_Pnd_Tbl_Rcrd_Mit_Stts;
    private DbsField tblrec_Pnd_Dscrptn_Txt_Filler_2;
    private DbsField tblrec_Pnd_Tbl_Rcrd_Unit;
    private DbsField tblrec_Pnd_Dscrptn_Txt_Filler_3;
    private DbsField tblrec_Pnd_Tbl_Rcrd_Racf;
    private DbsField tblrec_Pnd_Dscrptn_Txt_Filler_4;
    private DbsGroup tblrec_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup;
    private DbsField tblrec_Naz_Tbl_Secndry_Dscrptn_Txt;
    private DbsField pnd_Sd1;

    private DbsGroup pnd_Sd1__R_Field_2;
    private DbsField pnd_Sd1_Pnd_Sd1_Lvl1;
    private DbsField pnd_Sd1_Pnd_Sd1_Lvl2;
    private DbsField pnd_Sd1_Pnd_Sd1_Lvl3;

    private DbsGroup pnd_Sd1__R_Field_3;
    private DbsField pnd_Sd1_Pnd_Lvl_3_Stts;
    private DbsField pnd_Sd1_Pnd_Filler;
    private DbsField pnd_Wkf;

    private DbsGroup pnd_Wkf__R_Field_4;
    private DbsField pnd_Wkf_Pnd_Wkf_Bps_Unit;
    private DbsField pnd_Wkf_Pnd_Wkf_Cntrct;
    private DbsField pnd_Wkf_Pnd_Wkf_Unique_Id;
    private DbsField pnd_Wkf_Pnd_Wkf_Eff_Dte;

    private DbsGroup pnd_Wkf__R_Field_5;
    private DbsField pnd_Wkf_Pnd_Wkf_Eff_Dte_Cc;
    private DbsField pnd_Wkf_Pnd_Wkf_Eff_Dte_Yy;
    private DbsField pnd_Wkf_Pnd_Wkf_Eff_Dte_Mm;
    private DbsField pnd_Wkf_Pnd_Wkf_Eff_Dte_Dd;
    private DbsField pnd_Wkf_Pnd_Wkf_Nap_Stts_Cde;

    private DbsGroup pnd_Wkf__R_Field_6;
    private DbsField pnd_Wkf_Pnd_Wkf_Nap_Stts_Cde_Byte_1;
    private DbsField pnd_Wkf_Pnd_Filler;
    private DbsField pnd_Wkf_Pnd_Wkf_Nac_Stts_Cde;
    private DbsField pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr;
    private DbsField pnd_Wkf_Pnd_Wkf_Lst_Updte;

    private DbsGroup pnd_Wkf__R_Field_7;
    private DbsField pnd_Wkf_Pnd_Wkf_Lst_Updte_Cc;
    private DbsField pnd_Wkf_Pnd_Wkf_Lst_Updte_Yy;
    private DbsField pnd_Wkf_Pnd_Wkf_Lst_Updte_Mm;
    private DbsField pnd_Wkf_Pnd_Wkf_Lst_Updte_Dd;
    private DbsField pnd_Wkf_Pnd_Wkf_Annty_Strt_Dte;
    private DbsField pnd_Wkf_Pnd_Wkf_Repl_Ind;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Count;
    private DbsField pnd_Zero;
    private DbsField pnd_Work_Cnt;
    private DbsField pnd_Write_Cnt;
    private DbsField pnd_Write_Literal;
    private DbsField pnd_Prev_Unit;
    private DbsField pnd_Prev_Repl_Ind;
    private DbsField pnd_Summary_Literal;
    private DbsField pnd_Business_Dte;

    private DbsGroup pnd_Business_Dte__R_Field_8;
    private DbsField pnd_Business_Dte_Pnd_Business_Cc;
    private DbsField pnd_Business_Dte_Pnd_Business_Yy;
    private DbsField pnd_Business_Dte_Pnd_Business_Mm;
    private DbsField pnd_Business_Dte_Pnd_Business_Dd;
    private DbsField pnd_Formatted_Eff_Dte;

    private DbsGroup pnd_Formatted_Eff_Dte__R_Field_9;
    private DbsField pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Mm;
    private DbsField pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Dd;
    private DbsField pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Yy;
    private DbsField pnd_Formatted_Bsns_Dte;

    private DbsGroup pnd_Formatted_Bsns_Dte__R_Field_10;
    private DbsField pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Mm;
    private DbsField pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Dd;
    private DbsField pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Yy;
    private DbsField pnd_Formatted_Lst_Updte;

    private DbsGroup pnd_Formatted_Lst_Updte__R_Field_11;
    private DbsField pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Mm;
    private DbsField pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Dd;
    private DbsField pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Yy;
    private DbsField pnd_Wkf_Repl_Cnt;
    private DbsField pnd_Wkf_Mat_Cnt;
    private DbsField pnd_Repl_Break_Cnt;
    private DbsField pnd_Tbl_Desc;
    private DbsField pnd_Tbl_Error_Msg;

    private DbsGroup pnd_Tbl_Error_Msg__R_Field_12;
    private DbsField pnd_Tbl_Error_Msg_Pnd_Tbl_Lit;
    private DbsField pnd_Tbl_Error_Msg_Pnd_Tbl_Err_Cd;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_13;
    private DbsField pnd_Date_A_Pnd_Date_N;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cntlrcd = new DataAccessProgramView(new NameInfo("vw_cntlrcd", "CNTLRCD"), "ADS_CNTL", "ADS_CNTL");

        cntlrcd_Ads_Cntl_Grp = vw_cntlrcd.getRecord().newGroupInGroup("CNTLRCD_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        cntlrcd_Ads_Cntl_Rcrd_Typ_Cde = cntlrcd_Ads_Cntl_Grp.newFieldInGroup("cntlrcd_Ads_Cntl_Rcrd_Typ_Cde", "ADS-CNTL-RCRD-TYP-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADS_CNTL_RCRD_TYP_CDE");
        cntlrcd_Ads_Cntl_Bsnss_Rcprcl_Dte = cntlrcd_Ads_Cntl_Grp.newFieldInGroup("cntlrcd_Ads_Cntl_Bsnss_Rcprcl_Dte", "ADS-CNTL-BSNSS-RCPRCL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_RCPRCL_DTE");
        cntlrcd_Ads_Cntl_Bsnss_Dte = cntlrcd_Ads_Cntl_Grp.newFieldInGroup("cntlrcd_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "ADS_CNTL_BSNSS_DTE");
        cntlrcd_Ads_Cntl_Bsnss_Tmrrw_Dte = cntlrcd_Ads_Cntl_Grp.newFieldInGroup("cntlrcd_Ads_Cntl_Bsnss_Tmrrw_Dte", "ADS-CNTL-BSNSS-TMRRW-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_TMRRW_DTE");
        cntlrcd_Ads_Rpt_13 = vw_cntlrcd.getRecord().newFieldInGroup("cntlrcd_Ads_Rpt_13", "ADS-RPT-13", FieldType.DATE, RepeatingFieldStrategy.None, "ADS_RPT_13");
        registerRecord(vw_cntlrcd);

        vw_tblrec = new DataAccessProgramView(new NameInfo("vw_tblrec", "TBLREC"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        tblrec_Naz_Tbl_Rcrd_Lvl3_Id = vw_tblrec.getRecord().newFieldInGroup("tblrec_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", FieldType.STRING, 20, 
            RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        tblrec_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        tblrec_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_tblrec.getRecord().newFieldInGroup("tblrec_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        tblrec_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");

        tblrec__R_Field_1 = vw_tblrec.getRecord().newGroupInGroup("tblrec__R_Field_1", "REDEFINE", tblrec_Naz_Tbl_Rcrd_Dscrptn_Txt);
        tblrec_Pnd_Tbl_Rcrd_Wpid = tblrec__R_Field_1.newFieldInGroup("tblrec_Pnd_Tbl_Rcrd_Wpid", "#TBL-RCRD-WPID", FieldType.STRING, 6);
        tblrec_Pnd_Dscrptn_Txt_Filler_1 = tblrec__R_Field_1.newFieldInGroup("tblrec_Pnd_Dscrptn_Txt_Filler_1", "#DSCRPTN-TXT-FILLER-1", FieldType.STRING, 
            1);
        tblrec_Pnd_Tbl_Rcrd_Mit_Stts = tblrec__R_Field_1.newFieldInGroup("tblrec_Pnd_Tbl_Rcrd_Mit_Stts", "#TBL-RCRD-MIT-STTS", FieldType.NUMERIC, 4);
        tblrec_Pnd_Dscrptn_Txt_Filler_2 = tblrec__R_Field_1.newFieldInGroup("tblrec_Pnd_Dscrptn_Txt_Filler_2", "#DSCRPTN-TXT-FILLER-2", FieldType.STRING, 
            1);
        tblrec_Pnd_Tbl_Rcrd_Unit = tblrec__R_Field_1.newFieldInGroup("tblrec_Pnd_Tbl_Rcrd_Unit", "#TBL-RCRD-UNIT", FieldType.STRING, 6);
        tblrec_Pnd_Dscrptn_Txt_Filler_3 = tblrec__R_Field_1.newFieldInGroup("tblrec_Pnd_Dscrptn_Txt_Filler_3", "#DSCRPTN-TXT-FILLER-3", FieldType.STRING, 
            1);
        tblrec_Pnd_Tbl_Rcrd_Racf = tblrec__R_Field_1.newFieldInGroup("tblrec_Pnd_Tbl_Rcrd_Racf", "#TBL-RCRD-RACF", FieldType.STRING, 8);
        tblrec_Pnd_Dscrptn_Txt_Filler_4 = tblrec__R_Field_1.newFieldInGroup("tblrec_Pnd_Dscrptn_Txt_Filler_4", "#DSCRPTN-TXT-FILLER-4", FieldType.STRING, 
            33);
        tblrec_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup = vw_tblrec.getRecord().newGroupInGroup("TBLREC_NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", "NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        tblrec_Naz_Tbl_Secndry_Dscrptn_Txt = tblrec_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup.newFieldArrayInGroup("tblrec_Naz_Tbl_Secndry_Dscrptn_Txt", "NAZ-TBL-SECNDRY-DSCRPTN-TXT", 
            FieldType.STRING, 80, new DbsArrayController(1, 1), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        tblrec_Naz_Tbl_Secndry_Dscrptn_Txt.setDdmHeader("CDE/2ND/DSC");
        registerRecord(vw_tblrec);

        pnd_Sd1 = localVariables.newFieldInRecord("pnd_Sd1", "#SD1", FieldType.STRING, 29);

        pnd_Sd1__R_Field_2 = localVariables.newGroupInRecord("pnd_Sd1__R_Field_2", "REDEFINE", pnd_Sd1);
        pnd_Sd1_Pnd_Sd1_Lvl1 = pnd_Sd1__R_Field_2.newFieldInGroup("pnd_Sd1_Pnd_Sd1_Lvl1", "#SD1-LVL1", FieldType.STRING, 6);
        pnd_Sd1_Pnd_Sd1_Lvl2 = pnd_Sd1__R_Field_2.newFieldInGroup("pnd_Sd1_Pnd_Sd1_Lvl2", "#SD1-LVL2", FieldType.STRING, 3);
        pnd_Sd1_Pnd_Sd1_Lvl3 = pnd_Sd1__R_Field_2.newFieldInGroup("pnd_Sd1_Pnd_Sd1_Lvl3", "#SD1-LVL3", FieldType.STRING, 20);

        pnd_Sd1__R_Field_3 = pnd_Sd1__R_Field_2.newGroupInGroup("pnd_Sd1__R_Field_3", "REDEFINE", pnd_Sd1_Pnd_Sd1_Lvl3);
        pnd_Sd1_Pnd_Lvl_3_Stts = pnd_Sd1__R_Field_3.newFieldInGroup("pnd_Sd1_Pnd_Lvl_3_Stts", "#LVL-3-STTS", FieldType.STRING, 3);
        pnd_Sd1_Pnd_Filler = pnd_Sd1__R_Field_3.newFieldInGroup("pnd_Sd1_Pnd_Filler", "#FILLER", FieldType.STRING, 17);
        pnd_Wkf = localVariables.newFieldInRecord("pnd_Wkf", "#WKF", FieldType.STRING, 71);

        pnd_Wkf__R_Field_4 = localVariables.newGroupInRecord("pnd_Wkf__R_Field_4", "REDEFINE", pnd_Wkf);
        pnd_Wkf_Pnd_Wkf_Bps_Unit = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Bps_Unit", "#WKF-BPS-UNIT", FieldType.STRING, 8);
        pnd_Wkf_Pnd_Wkf_Cntrct = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Cntrct", "#WKF-CNTRCT", FieldType.STRING, 10);
        pnd_Wkf_Pnd_Wkf_Unique_Id = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Unique_Id", "#WKF-UNIQUE-ID", FieldType.NUMERIC, 12);
        pnd_Wkf_Pnd_Wkf_Eff_Dte = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Eff_Dte", "#WKF-EFF-DTE", FieldType.NUMERIC, 8);

        pnd_Wkf__R_Field_5 = pnd_Wkf__R_Field_4.newGroupInGroup("pnd_Wkf__R_Field_5", "REDEFINE", pnd_Wkf_Pnd_Wkf_Eff_Dte);
        pnd_Wkf_Pnd_Wkf_Eff_Dte_Cc = pnd_Wkf__R_Field_5.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Eff_Dte_Cc", "#WKF-EFF-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Wkf_Pnd_Wkf_Eff_Dte_Yy = pnd_Wkf__R_Field_5.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Eff_Dte_Yy", "#WKF-EFF-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Wkf_Pnd_Wkf_Eff_Dte_Mm = pnd_Wkf__R_Field_5.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Eff_Dte_Mm", "#WKF-EFF-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Wkf_Pnd_Wkf_Eff_Dte_Dd = pnd_Wkf__R_Field_5.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Eff_Dte_Dd", "#WKF-EFF-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Wkf_Pnd_Wkf_Nap_Stts_Cde = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Nap_Stts_Cde", "#WKF-NAP-STTS-CDE", FieldType.STRING, 3);

        pnd_Wkf__R_Field_6 = pnd_Wkf__R_Field_4.newGroupInGroup("pnd_Wkf__R_Field_6", "REDEFINE", pnd_Wkf_Pnd_Wkf_Nap_Stts_Cde);
        pnd_Wkf_Pnd_Wkf_Nap_Stts_Cde_Byte_1 = pnd_Wkf__R_Field_6.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Nap_Stts_Cde_Byte_1", "#WKF-NAP-STTS-CDE-BYTE-1", FieldType.STRING, 
            1);
        pnd_Wkf_Pnd_Filler = pnd_Wkf__R_Field_6.newFieldInGroup("pnd_Wkf_Pnd_Filler", "#FILLER", FieldType.STRING, 2);
        pnd_Wkf_Pnd_Wkf_Nac_Stts_Cde = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Nac_Stts_Cde", "#WKF-NAC-STTS-CDE", FieldType.STRING, 3);
        pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr", "#WKF-IA-TIAA-NBR", FieldType.STRING, 10);
        pnd_Wkf_Pnd_Wkf_Lst_Updte = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Lst_Updte", "#WKF-LST-UPDTE", FieldType.NUMERIC, 8);

        pnd_Wkf__R_Field_7 = pnd_Wkf__R_Field_4.newGroupInGroup("pnd_Wkf__R_Field_7", "REDEFINE", pnd_Wkf_Pnd_Wkf_Lst_Updte);
        pnd_Wkf_Pnd_Wkf_Lst_Updte_Cc = pnd_Wkf__R_Field_7.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Lst_Updte_Cc", "#WKF-LST-UPDTE-CC", FieldType.NUMERIC, 2);
        pnd_Wkf_Pnd_Wkf_Lst_Updte_Yy = pnd_Wkf__R_Field_7.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Lst_Updte_Yy", "#WKF-LST-UPDTE-YY", FieldType.NUMERIC, 2);
        pnd_Wkf_Pnd_Wkf_Lst_Updte_Mm = pnd_Wkf__R_Field_7.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Lst_Updte_Mm", "#WKF-LST-UPDTE-MM", FieldType.NUMERIC, 2);
        pnd_Wkf_Pnd_Wkf_Lst_Updte_Dd = pnd_Wkf__R_Field_7.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Lst_Updte_Dd", "#WKF-LST-UPDTE-DD", FieldType.NUMERIC, 2);
        pnd_Wkf_Pnd_Wkf_Annty_Strt_Dte = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Annty_Strt_Dte", "#WKF-ANNTY-STRT-DTE", FieldType.NUMERIC, 
            8);
        pnd_Wkf_Pnd_Wkf_Repl_Ind = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Repl_Ind", "#WKF-REPL-IND", FieldType.STRING, 1);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.INTEGER, 4);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.INTEGER, 4);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Zero = localVariables.newFieldInRecord("pnd_Zero", "#ZERO", FieldType.PACKED_DECIMAL, 5);
        pnd_Work_Cnt = localVariables.newFieldInRecord("pnd_Work_Cnt", "#WORK-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Write_Cnt = localVariables.newFieldInRecord("pnd_Write_Cnt", "#WRITE-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Write_Literal = localVariables.newFieldInRecord("pnd_Write_Literal", "#WRITE-LITERAL", FieldType.STRING, 13);
        pnd_Prev_Unit = localVariables.newFieldInRecord("pnd_Prev_Unit", "#PREV-UNIT", FieldType.STRING, 8);
        pnd_Prev_Repl_Ind = localVariables.newFieldInRecord("pnd_Prev_Repl_Ind", "#PREV-REPL-IND", FieldType.STRING, 1);
        pnd_Summary_Literal = localVariables.newFieldInRecord("pnd_Summary_Literal", "#SUMMARY-LITERAL", FieldType.STRING, 13);
        pnd_Business_Dte = localVariables.newFieldInRecord("pnd_Business_Dte", "#BUSINESS-DTE", FieldType.NUMERIC, 8);

        pnd_Business_Dte__R_Field_8 = localVariables.newGroupInRecord("pnd_Business_Dte__R_Field_8", "REDEFINE", pnd_Business_Dte);
        pnd_Business_Dte_Pnd_Business_Cc = pnd_Business_Dte__R_Field_8.newFieldInGroup("pnd_Business_Dte_Pnd_Business_Cc", "#BUSINESS-CC", FieldType.NUMERIC, 
            2);
        pnd_Business_Dte_Pnd_Business_Yy = pnd_Business_Dte__R_Field_8.newFieldInGroup("pnd_Business_Dte_Pnd_Business_Yy", "#BUSINESS-YY", FieldType.NUMERIC, 
            2);
        pnd_Business_Dte_Pnd_Business_Mm = pnd_Business_Dte__R_Field_8.newFieldInGroup("pnd_Business_Dte_Pnd_Business_Mm", "#BUSINESS-MM", FieldType.NUMERIC, 
            2);
        pnd_Business_Dte_Pnd_Business_Dd = pnd_Business_Dte__R_Field_8.newFieldInGroup("pnd_Business_Dte_Pnd_Business_Dd", "#BUSINESS-DD", FieldType.NUMERIC, 
            2);
        pnd_Formatted_Eff_Dte = localVariables.newFieldInRecord("pnd_Formatted_Eff_Dte", "#FORMATTED-EFF-DTE", FieldType.NUMERIC, 6);

        pnd_Formatted_Eff_Dte__R_Field_9 = localVariables.newGroupInRecord("pnd_Formatted_Eff_Dte__R_Field_9", "REDEFINE", pnd_Formatted_Eff_Dte);
        pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Mm = pnd_Formatted_Eff_Dte__R_Field_9.newFieldInGroup("pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Mm", 
            "#FORMATTED-EFF-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Dd = pnd_Formatted_Eff_Dte__R_Field_9.newFieldInGroup("pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Dd", 
            "#FORMATTED-EFF-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Yy = pnd_Formatted_Eff_Dte__R_Field_9.newFieldInGroup("pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Yy", 
            "#FORMATTED-EFF-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Formatted_Bsns_Dte = localVariables.newFieldInRecord("pnd_Formatted_Bsns_Dte", "#FORMATTED-BSNS-DTE", FieldType.NUMERIC, 6);

        pnd_Formatted_Bsns_Dte__R_Field_10 = localVariables.newGroupInRecord("pnd_Formatted_Bsns_Dte__R_Field_10", "REDEFINE", pnd_Formatted_Bsns_Dte);
        pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Mm = pnd_Formatted_Bsns_Dte__R_Field_10.newFieldInGroup("pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Mm", 
            "#FORMATTED-BSNS-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Dd = pnd_Formatted_Bsns_Dte__R_Field_10.newFieldInGroup("pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Dd", 
            "#FORMATTED-BSNS-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Yy = pnd_Formatted_Bsns_Dte__R_Field_10.newFieldInGroup("pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Yy", 
            "#FORMATTED-BSNS-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Formatted_Lst_Updte = localVariables.newFieldInRecord("pnd_Formatted_Lst_Updte", "#FORMATTED-LST-UPDTE", FieldType.NUMERIC, 6);

        pnd_Formatted_Lst_Updte__R_Field_11 = localVariables.newGroupInRecord("pnd_Formatted_Lst_Updte__R_Field_11", "REDEFINE", pnd_Formatted_Lst_Updte);
        pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Mm = pnd_Formatted_Lst_Updte__R_Field_11.newFieldInGroup("pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Mm", 
            "#FORMATTED-LST-UPDTE-MM", FieldType.NUMERIC, 2);
        pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Dd = pnd_Formatted_Lst_Updte__R_Field_11.newFieldInGroup("pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Dd", 
            "#FORMATTED-LST-UPDTE-DD", FieldType.NUMERIC, 2);
        pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Yy = pnd_Formatted_Lst_Updte__R_Field_11.newFieldInGroup("pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Yy", 
            "#FORMATTED-LST-UPDTE-YY", FieldType.NUMERIC, 2);
        pnd_Wkf_Repl_Cnt = localVariables.newFieldInRecord("pnd_Wkf_Repl_Cnt", "#WKF-REPL-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Wkf_Mat_Cnt = localVariables.newFieldInRecord("pnd_Wkf_Mat_Cnt", "#WKF-MAT-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Repl_Break_Cnt = localVariables.newFieldInRecord("pnd_Repl_Break_Cnt", "#REPL-BREAK-CNT", FieldType.NUMERIC, 1);
        pnd_Tbl_Desc = localVariables.newFieldInRecord("pnd_Tbl_Desc", "#TBL-DESC", FieldType.STRING, 62);
        pnd_Tbl_Error_Msg = localVariables.newFieldInRecord("pnd_Tbl_Error_Msg", "#TBL-ERROR-MSG", FieldType.STRING, 62);

        pnd_Tbl_Error_Msg__R_Field_12 = localVariables.newGroupInRecord("pnd_Tbl_Error_Msg__R_Field_12", "REDEFINE", pnd_Tbl_Error_Msg);
        pnd_Tbl_Error_Msg_Pnd_Tbl_Lit = pnd_Tbl_Error_Msg__R_Field_12.newFieldInGroup("pnd_Tbl_Error_Msg_Pnd_Tbl_Lit", "#TBL-LIT", FieldType.STRING, 20);
        pnd_Tbl_Error_Msg_Pnd_Tbl_Err_Cd = pnd_Tbl_Error_Msg__R_Field_12.newFieldInGroup("pnd_Tbl_Error_Msg_Pnd_Tbl_Err_Cd", "#TBL-ERR-CD", FieldType.STRING, 
            42);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_13 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_13", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_13.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cntlrcd.reset();
        vw_tblrec.reset();

        localVariables.reset();
        pnd_Count.setInitialValue(0);
        pnd_Zero.setInitialValue(0);
        pnd_Work_Cnt.setInitialValue(0);
        pnd_Write_Cnt.setInitialValue(0);
        pnd_Write_Literal.setInitialValue(" ");
        pnd_Prev_Unit.setInitialValue(" ");
        pnd_Prev_Repl_Ind.setInitialValue(" ");
        pnd_Summary_Literal.setInitialValue(" ");
        pnd_Wkf_Repl_Cnt.setInitialValue(0);
        pnd_Wkf_Mat_Cnt.setInitialValue(0);
        pnd_Repl_Break_Cnt.setInitialValue(1);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp961() throws Exception
    {
        super("Adsp961");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
                                                                                                                                                                          //Natural: FORMAT LS = 133 PS = 55;//Natural: FORMAT ( 1 ) LS = 133 PS = 55;//Natural: PERFORM GET-BSNS-DTE
        sub_Get_Bsns_Dte();
        if (condition(Global.isEscape())) {return;}
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 #WKF
        while (condition(getWorkFiles().read(1, pnd_Wkf)))
        {
            //* ************************************************************
            //*                                                                                                                                                           //Natural: AT END OF DATA
            pnd_Work_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #WORK-CNT
            if (condition(pnd_Wkf_Pnd_Wkf_Repl_Ind.equals("Y")))                                                                                                          //Natural: IF #WKF-REPL-IND = 'Y'
            {
                pnd_Wkf_Repl_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #WKF-REPL-CNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Wkf_Mat_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #WKF-MAT-CNT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Cnt.equals(1)))                                                                                                                        //Natural: IF #WORK-CNT = 1
            {
                pnd_Prev_Unit.setValue(pnd_Wkf_Pnd_Wkf_Bps_Unit);                                                                                                         //Natural: MOVE #WKF-BPS-UNIT TO #PREV-UNIT
                pnd_Prev_Repl_Ind.setValue(pnd_Wkf_Pnd_Wkf_Repl_Ind);                                                                                                     //Natural: MOVE #WKF-REPL-IND TO #PREV-REPL-IND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Wkf_Pnd_Wkf_Bps_Unit.equals(pnd_Prev_Unit)))                                                                                                //Natural: IF #WKF-BPS-UNIT = #PREV-UNIT
            {
                if (condition(pnd_Wkf_Pnd_Wkf_Repl_Ind.equals(pnd_Prev_Repl_Ind)))                                                                                        //Natural: IF #WKF-REPL-IND = #PREV-REPL-IND
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  TYPE BREAK
                                                                                                                                                                          //Natural: PERFORM TYPE-BREAK
                    sub_Type_Break();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Wkf_Pnd_Wkf_Repl_Ind.equals(pnd_Prev_Repl_Ind)))                                                                                        //Natural: IF #WKF-REPL-IND = #PREV-REPL-IND
                {
                                                                                                                                                                          //Natural: PERFORM UNIT-BREAK
                    sub_Unit_Break();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  UNIT AND TYPE BREAK
                                                                                                                                                                          //Natural: PERFORM UNIT-TYPE-BREAK
                    sub_Unit_Type_Break();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM TBL-ACCESS
            sub_Tbl_Access();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL-LINE
            sub_Write_Detail_Line();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            //* ************************************************************
            if (condition(pnd_Prev_Repl_Ind.equals("Y")))                                                                                                                 //Natural: IF #PREV-REPL-IND = 'Y'
            {
                pnd_Summary_Literal.setValue("RESETTLEMENTS");                                                                                                            //Natural: MOVE 'RESETTLEMENTS' TO #SUMMARY-LITERAL
                pnd_Count.setValue(pnd_Wkf_Repl_Cnt);                                                                                                                     //Natural: MOVE #WKF-REPL-CNT TO #COUNT
                pnd_Wkf_Repl_Cnt.setValue(1);                                                                                                                             //Natural: MOVE 1 TO #WKF-REPL-CNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Summary_Literal.setValue("MATURITIES   ");                                                                                                            //Natural: MOVE 'MATURITIES   ' TO #SUMMARY-LITERAL
                pnd_Count.setValue(pnd_Wkf_Mat_Cnt);                                                                                                                      //Natural: MOVE #WKF-MAT-CNT TO #COUNT
                pnd_Wkf_Mat_Cnt.setValue(1);                                                                                                                              //Natural: MOVE 1 TO #WKF-MAT-CNT
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"TOTAL REJECTED ",pnd_Summary_Literal," - ",pnd_Prev_Unit,": ",                    //Natural: WRITE ( 1 ) //// 'TOTAL REJECTED ' #SUMMARY-LITERAL ' - ' #PREV-UNIT ': ' #COUNT
                pnd_Count);
            if (condition(Global.isEscape())) return;
            pnd_Prev_Repl_Ind.setValue(pnd_Wkf_Pnd_Wkf_Repl_Ind);                                                                                                         //Natural: MOVE #WKF-REPL-IND TO #PREV-REPL-IND
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(37),"<<< End Of Report >>>");                                                   //Natural: WRITE ( 1 ) /// 37T '<<< End Of Report >>>'
        if (Global.isEscape()) return;
        //* ****************************************************************
        //* ***********************************************************
        //* *3X 'LAST' 7X 'UNIQUE' 7X 'IA' 10X 'DA' 7X 'EFFECTIVE' 31X 'STATUS' /
        //* *'UPDATE DTE' 6X 'ID' 6X 'CONTRACT' 4X 'CONTRACT' 6X 'DATE' 31X
        //* *'----------' 3X '--------' 3X '---------' 3X '---------' 3X
        //* *'---------'
        //* *3X '--------------------------------------------------------------'
        //* ****************************************************************
        //* ****************************************************************
        //* ****************************************************************
        //* ****************************************************************
        //* ****************************************************************
        //* ****************************************************************
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //* *********************************************************
    }
    private void sub_Get_Bsns_Dte() throws Exception                                                                                                                      //Natural: GET-BSNS-DTE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************************************
        vw_cntlrcd.startDatabaseRead                                                                                                                                      //Natural: READ ( 2 ) CNTLRCD BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "READ02",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        READ02:
        while (condition(vw_cntlrcd.readNextRow("READ02")))
        {
            if (condition(vw_cntlrcd.getAstCOUNTER().equals(2)))                                                                                                          //Natural: IF *COUNTER = 2
            {
                if (condition(cntlrcd_Ads_Rpt_13.notEquals(getZero())))                                                                                                   //Natural: IF ADS-RPT-13 NE 0
                {
                    pnd_Date_A.setValueEdited(cntlrcd_Ads_Rpt_13,new ReportEditMask("YYYYMMDD"));                                                                         //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE-A
                    pnd_Business_Dte.setValue(pnd_Date_A_Pnd_Date_N);                                                                                                     //Natural: MOVE #DATE-N TO #BUSINESS-DTE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  TEST PURPOSES
                    pnd_Business_Dte.setValue(cntlrcd_Ads_Cntl_Bsnss_Dte);                                                                                                //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #BUSINESS-DTE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Yy.setValue(pnd_Business_Dte_Pnd_Business_Yy);                                                              //Natural: MOVE #BUSINESS-YY TO #FORMATTED-BSNS-DTE-YY
                pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Mm.setValue(pnd_Business_Dte_Pnd_Business_Mm);                                                              //Natural: MOVE #BUSINESS-MM TO #FORMATTED-BSNS-DTE-MM
                pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Dd.setValue(pnd_Business_Dte_Pnd_Business_Dd);                                                              //Natural: MOVE #BUSINESS-DD TO #FORMATTED-BSNS-DTE-DD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Heading_Rtn() throws Exception                                                                                                                       //Natural: HEADING-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************************************
        if (condition(pnd_Prev_Repl_Ind.equals("Y")))                                                                                                                     //Natural: IF #PREV-REPL-IND = 'Y'
        {
            pnd_Summary_Literal.setValue("RESETTLEMENTS");                                                                                                                //Natural: MOVE 'RESETTLEMENTS' TO #SUMMARY-LITERAL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Summary_Literal.setValue("MATURITIES   ");                                                                                                                //Natural: MOVE 'MATURITIES   ' TO #SUMMARY-LITERAL
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"PROGRAM: ",Global.getPROGRAM(),new TabSetting(43),"ANNUITIZATION OF OMNIPLUS CONTRACTS",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR / 'PROGRAM: ' *PROGRAM 43T 'ANNUITIZATION OF OMNIPLUS CONTRACTS' 101T 'RUN DATE:' *DATU / 41T 'REJECTION / CANCELLED REPORT - ' #PREV-UNIT 101T 'RUN TIME:' *TIMX / 47T 'AS OF ACCOUNTING DATE' #FORMATTED-BSNS-DTE ( EM = 99/99/99 ) 101T 'PAGE:' *PAGE-NUMBER ( 1 ) // #SUMMARY-LITERAL ' -' #PREV-UNIT / '----------------------' / 3X 'LAST' 17T 'UNIQUE' 34T 'IA' 46T 'DA' 55T 'EFFECTIVE' 75T 'STATUS' / 'UPDATE DTE' 19T 'ID' 31T 'CONTRACT' 43T 'CONTRACT' 57T 'DATE' 72T 'DESCRIPTION' / '----------' 15T '-------------' 31T '---------' 43T '---------' 55T '---------' 67T '--------------------------------------------------------------'
            TabSetting(101),"RUN DATE:",Global.getDATU(),NEWLINE,new TabSetting(41),"REJECTION / CANCELLED REPORT - ",pnd_Prev_Unit,new TabSetting(101),"RUN TIME:",Global.getTIMX(),NEWLINE,new 
            TabSetting(47),"AS OF ACCOUNTING DATE",pnd_Formatted_Bsns_Dte, new ReportEditMask ("99/99/99"),new TabSetting(101),"PAGE:",getReports().getPageNumberDbs(1),NEWLINE,NEWLINE,pnd_Summary_Literal," -",pnd_Prev_Unit,NEWLINE,"----------------------",NEWLINE,new 
            ColumnSpacing(3),"LAST",new TabSetting(17),"UNIQUE",new TabSetting(34),"IA",new TabSetting(46),"DA",new TabSetting(55),"EFFECTIVE",new TabSetting(75),"STATUS",NEWLINE,"UPDATE DTE",new 
            TabSetting(19),"ID",new TabSetting(31),"CONTRACT",new TabSetting(43),"CONTRACT",new TabSetting(57),"DATE",new TabSetting(72),"DESCRIPTION",NEWLINE,"----------",new 
            TabSetting(15),"-------------",new TabSetting(31),"---------",new TabSetting(43),"---------",new TabSetting(55),"---------",new TabSetting(67),
            "--------------------------------------------------------------");
        if (Global.isEscape()) return;
    }
    private void sub_Tbl_Access() throws Exception                                                                                                                        //Natural: TBL-ACCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************************************
        pnd_Sd1.reset();                                                                                                                                                  //Natural: RESET #SD1
        pnd_Sd1_Pnd_Sd1_Lvl1.setValue("NAZ063");                                                                                                                          //Natural: ASSIGN #SD1-LVL1 := 'NAZ063'
        pnd_Sd1_Pnd_Sd1_Lvl2.setValue("MIT");                                                                                                                             //Natural: ASSIGN #SD1-LVL2 := 'MIT'
        pnd_Sd1_Pnd_Lvl_3_Stts.setValue(pnd_Wkf_Pnd_Wkf_Nac_Stts_Cde);                                                                                                    //Natural: ASSIGN #LVL-3-STTS := #WKF-NAC-STTS-CDE
        vw_tblrec.startDatabaseFind                                                                                                                                       //Natural: FIND TBLREC WITH NAZ-TBL-SUPER1 = #SD1
        (
        "FIND01",
        new Wc[] { new Wc("NAZ_TBL_SUPER1", "=", pnd_Sd1, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_tblrec.readNextRow("FIND01", true)))
        {
            vw_tblrec.setIfNotFoundControlFlag(false);
            if (condition(vw_tblrec.getAstCOUNTER().equals(0)))                                                                                                           //Natural: IF NO RECORD FOUND
            {
                pnd_Tbl_Error_Msg_Pnd_Tbl_Err_Cd.setValue(pnd_Wkf_Pnd_Wkf_Nac_Stts_Cde);                                                                                  //Natural: ASSIGN #TBL-ERR-CD = #WKF-NAC-STTS-CDE
                pnd_Tbl_Error_Msg_Pnd_Tbl_Lit.setValue("NO MESSAGE FOR CODE ");                                                                                           //Natural: ASSIGN #TBL-LIT = 'NO MESSAGE FOR CODE '
                pnd_Tbl_Desc.setValue(pnd_Tbl_Error_Msg);                                                                                                                 //Natural: ASSIGN #TBL-DESC = #TBL-ERROR-MSG
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Tbl_Desc.setValue(tblrec_Naz_Tbl_Secndry_Dscrptn_Txt.getValue(1));                                                                                        //Natural: MOVE NAZ-TBL-SECNDRY-DSCRPTN-TXT ( 1 ) TO #TBL-DESC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Unit_Type_Break() throws Exception                                                                                                                   //Natural: UNIT-TYPE-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************************************
        if (condition(pnd_Prev_Repl_Ind.equals("Y")))                                                                                                                     //Natural: IF #PREV-REPL-IND = 'Y'
        {
            pnd_Summary_Literal.setValue("RESETTLEMENTS");                                                                                                                //Natural: MOVE 'RESETTLEMENTS' TO #SUMMARY-LITERAL
            pnd_Count.setValue(pnd_Wkf_Repl_Cnt);                                                                                                                         //Natural: MOVE #WKF-REPL-CNT TO #COUNT
            pnd_Wkf_Repl_Cnt.setValue(0);                                                                                                                                 //Natural: MOVE 0 TO #WKF-REPL-CNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Summary_Literal.setValue("MATURITIES   ");                                                                                                                //Natural: MOVE 'MATURITIES   ' TO #SUMMARY-LITERAL
            pnd_Count.setValue(pnd_Wkf_Mat_Cnt);                                                                                                                          //Natural: MOVE #WKF-MAT-CNT TO #COUNT
            pnd_Wkf_Mat_Cnt.setValue(0);                                                                                                                                  //Natural: MOVE 0 TO #WKF-MAT-CNT
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"TOTAL REJECTED ",pnd_Summary_Literal," - ",pnd_Prev_Unit,": ",pnd_Count);             //Natural: WRITE ( 1 ) //// 'TOTAL REJECTED ' #SUMMARY-LITERAL ' - ' #PREV-UNIT ': ' #COUNT
        if (Global.isEscape()) return;
        pnd_Prev_Unit.setValue(pnd_Wkf_Pnd_Wkf_Bps_Unit);                                                                                                                 //Natural: MOVE #WKF-BPS-UNIT TO #PREV-UNIT
        pnd_Prev_Repl_Ind.setValue(pnd_Wkf_Pnd_Wkf_Repl_Ind);                                                                                                             //Natural: MOVE #WKF-REPL-IND TO #PREV-REPL-IND
        getReports().eject(0, true);                                                                                                                                      //Natural: EJECT
                                                                                                                                                                          //Natural: PERFORM HEADING-RTN
        sub_Heading_Rtn();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Unit_Break() throws Exception                                                                                                                        //Natural: UNIT-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************************************
        if (condition(pnd_Prev_Repl_Ind.equals("Y")))                                                                                                                     //Natural: IF #PREV-REPL-IND = 'Y'
        {
            pnd_Summary_Literal.setValue("RESETTLEMENTS");                                                                                                                //Natural: MOVE 'RESETTLEMENTS' TO #SUMMARY-LITERAL
            pnd_Count.setValue(pnd_Wkf_Repl_Cnt);                                                                                                                         //Natural: MOVE #WKF-REPL-CNT TO #COUNT
            pnd_Wkf_Repl_Cnt.setValue(0);                                                                                                                                 //Natural: MOVE 0 TO #WKF-REPL-CNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Summary_Literal.setValue("MATURITIES   ");                                                                                                                //Natural: MOVE 'MATURITIES   ' TO #SUMMARY-LITERAL
            pnd_Count.setValue(pnd_Wkf_Mat_Cnt);                                                                                                                          //Natural: MOVE #WKF-MAT-CNT TO #COUNT
            pnd_Wkf_Mat_Cnt.setValue(0);                                                                                                                                  //Natural: MOVE 0 TO #WKF-MAT-CNT
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"TOTAL REJECTED ",pnd_Summary_Literal," - ",pnd_Prev_Unit,": ",pnd_Count);             //Natural: WRITE ( 1 ) //// 'TOTAL REJECTED ' #SUMMARY-LITERAL ' - ' #PREV-UNIT ': ' #COUNT
        if (Global.isEscape()) return;
        pnd_Prev_Unit.setValue(pnd_Wkf_Pnd_Wkf_Bps_Unit);                                                                                                                 //Natural: MOVE #WKF-BPS-UNIT TO #PREV-UNIT
        getReports().eject(0, true);                                                                                                                                      //Natural: EJECT
                                                                                                                                                                          //Natural: PERFORM HEADING-RTN
        sub_Heading_Rtn();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Type_Break() throws Exception                                                                                                                        //Natural: TYPE-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************************************
        if (condition(pnd_Prev_Repl_Ind.equals("Y")))                                                                                                                     //Natural: IF #PREV-REPL-IND = 'Y'
        {
            pnd_Summary_Literal.setValue("RESETTLEMENTS");                                                                                                                //Natural: MOVE 'RESETTLEMENTS' TO #SUMMARY-LITERAL
            pnd_Count.setValue(pnd_Wkf_Repl_Cnt);                                                                                                                         //Natural: MOVE #WKF-REPL-CNT TO #COUNT
            pnd_Wkf_Repl_Cnt.setValue(0);                                                                                                                                 //Natural: MOVE 0 TO #WKF-REPL-CNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Summary_Literal.setValue("MATURITIES   ");                                                                                                                //Natural: MOVE 'MATURITIES   ' TO #SUMMARY-LITERAL
            pnd_Count.setValue(pnd_Wkf_Mat_Cnt);                                                                                                                          //Natural: MOVE #WKF-MAT-CNT TO #COUNT
            pnd_Wkf_Mat_Cnt.setValue(0);                                                                                                                                  //Natural: MOVE 0 TO #WKF-MAT-CNT
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"TOTAL REJECTED ",pnd_Summary_Literal," - ",pnd_Prev_Unit,": ",pnd_Count);             //Natural: WRITE ( 1 ) //// 'TOTAL REJECTED ' #SUMMARY-LITERAL ' - ' #PREV-UNIT ': ' #COUNT
        if (Global.isEscape()) return;
        pnd_Prev_Repl_Ind.setValue(pnd_Wkf_Pnd_Wkf_Repl_Ind);                                                                                                             //Natural: MOVE #WKF-REPL-IND TO #PREV-REPL-IND
        getReports().eject(0, true);                                                                                                                                      //Natural: EJECT
                                                                                                                                                                          //Natural: PERFORM HEADING-RTN
        sub_Heading_Rtn();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Write_Detail_Line() throws Exception                                                                                                                 //Natural: WRITE-DETAIL-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************************************
        pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Yy.setValue(pnd_Wkf_Pnd_Wkf_Eff_Dte_Yy);                                                                              //Natural: MOVE #WKF-EFF-DTE-YY TO #FORMATTED-EFF-DTE-YY
        pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Mm.setValue(pnd_Wkf_Pnd_Wkf_Eff_Dte_Mm);                                                                              //Natural: MOVE #WKF-EFF-DTE-MM TO #FORMATTED-EFF-DTE-MM
        pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Dd.setValue(pnd_Wkf_Pnd_Wkf_Eff_Dte_Dd);                                                                              //Natural: MOVE #WKF-EFF-DTE-DD TO #FORMATTED-EFF-DTE-DD
        pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Yy.setValue(pnd_Wkf_Pnd_Wkf_Lst_Updte_Yy);                                                                        //Natural: MOVE #WKF-LST-UPDTE-YY TO #FORMATTED-LST-UPDTE-YY
        pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Mm.setValue(pnd_Wkf_Pnd_Wkf_Lst_Updte_Mm);                                                                        //Natural: MOVE #WKF-LST-UPDTE-MM TO #FORMATTED-LST-UPDTE-MM
        pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Dd.setValue(pnd_Wkf_Pnd_Wkf_Lst_Updte_Dd);                                                                        //Natural: MOVE #WKF-LST-UPDTE-DD TO #FORMATTED-LST-UPDTE-DD
        //* * WRITE (1) / #FORMATTED-LST-UPDTE (EM=99/99/99) 6X
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Formatted_Lst_Updte, new ReportEditMask ("99/99/99"),new TabSetting(15),pnd_Wkf_Pnd_Wkf_Unique_Id,new      //Natural: WRITE ( 1 ) / #FORMATTED-LST-UPDTE ( EM = 99/99/99 ) 15T #WKF-UNIQUE-ID 31T #WKF-IA-TIAA-NBR ( EM = XXXXXXX-X ) 43T #WKF-CNTRCT ( EM = XXXXXXX-X ) 55T #FORMATTED-EFF-DTE ( EM = 99/99/99 ) 67T #TBL-DESC
            TabSetting(31),pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr, new ReportEditMask ("XXXXXXX-X"),new TabSetting(43),pnd_Wkf_Pnd_Wkf_Cntrct, new ReportEditMask ("XXXXXXX-X"),new 
            TabSetting(55),pnd_Formatted_Eff_Dte, new ReportEditMask ("99/99/99"),new TabSetting(67),pnd_Tbl_Desc);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *********************************************************************
                                                                                                                                                                          //Natural: PERFORM HEADING-RTN
                    sub_Heading_Rtn();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=55");
        Global.format(1, "LS=133 PS=55");
    }
}
