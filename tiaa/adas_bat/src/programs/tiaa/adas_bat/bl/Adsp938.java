/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:05:48 PM
**        * FROM NATURAL PROGRAM : Adsp938
************************************************************
**        * FILE NAME            : Adsp938.java
**        * CLASS NAME           : Adsp938
**        * INSTANCE NAME        : Adsp938
************************************************************
************************************************************************
* PROGRAM  : ADSP938
* GENERATED: MARCH 08, 2010
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : READS WORK FILE FROM ADSP933, WRITES CASES PENDING
*            SETTLEMENT
*
* REMARKS  : CLONED FROM ADSP934
************************************************************************
* MOD DATE   MOD BY      DESCRIPTION OF CHANGES
* 04/16/04   C. AVE      CLONED FROM ADSP932
* 07/27/04   T. SHINE       IF MONTH-END PROCESS FALLS ON WEEKEND OR
*                           HOLIDAY, USE ADS-RPT-13 (FACTOR DATE) AS
*                           REPORT DATE.
* 03/08/10   D.E.ANDER   CLONED FROM ADSP934
*                        REPLATFORM ADABAS - PCPOP
*                        ADD SURVIVOR-IND ON REPORT    MARKED DEA
* 02/23/2017 R.CARREON   PIN EXPANSION 02232017
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp938 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cntlrcd;

    private DbsGroup cntlrcd_Ads_Cntl_Grp;
    private DbsField cntlrcd_Ads_Cntl_Rcrd_Typ_Cde;
    private DbsField cntlrcd_Ads_Cntl_Bsnss_Rcprcl_Dte;
    private DbsField cntlrcd_Ads_Cntl_Bsnss_Dte;
    private DbsField cntlrcd_Ads_Cntl_Bsnss_Tmrrw_Dte;
    private DbsField cntlrcd_Ads_Rpt_13;

    private DataAccessProgramView vw_tblrec;
    private DbsField tblrec_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField tblrec_Naz_Tbl_Rcrd_Dscrptn_Txt;

    private DbsGroup tblrec__R_Field_1;
    private DbsField tblrec_Pnd_Tbl_Rcrd_Wpid;
    private DbsField tblrec_Pnd_Dscrptn_Txt_Filler_1;
    private DbsField tblrec_Pnd_Tbl_Rcrd_Mit_Stts;
    private DbsField tblrec_Pnd_Dscrptn_Txt_Filler_2;
    private DbsField tblrec_Pnd_Tbl_Rcrd_Unit;
    private DbsField tblrec_Pnd_Dscrptn_Txt_Filler_3;
    private DbsField tblrec_Pnd_Tbl_Rcrd_Racf;
    private DbsField tblrec_Pnd_Dscrptn_Txt_Filler_4;
    private DbsGroup tblrec_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup;
    private DbsField tblrec_Naz_Tbl_Secndry_Dscrptn_Txt;
    private DbsField pnd_Sd1;

    private DbsGroup pnd_Sd1__R_Field_2;
    private DbsField pnd_Sd1_Pnd_Sd1_Lvl1;
    private DbsField pnd_Sd1_Pnd_Sd1_Lvl2;
    private DbsField pnd_Sd1_Pnd_Sd1_Lvl3;

    private DbsGroup pnd_Sd1__R_Field_3;
    private DbsField pnd_Sd1_Pnd_Lvl_3_Stts;
    private DbsField pnd_Sd1_Pnd_Filler;
    private DbsField pnd_Wkf;

    private DbsGroup pnd_Wkf__R_Field_4;
    private DbsField pnd_Wkf_Pnd_Wkf_Bps_Unit;
    private DbsField pnd_Wkf_Pnd_Wkf_Cntrct;
    private DbsField pnd_Wkf_Pnd_Wkf_Unique_Id;
    private DbsField pnd_Wkf_Pnd_Wkf_Eff_Dte;

    private DbsGroup pnd_Wkf__R_Field_5;
    private DbsField pnd_Wkf_Pnd_Wkf_Eff_Dte_Cc;
    private DbsField pnd_Wkf_Pnd_Wkf_Eff_Dte_Yy;
    private DbsField pnd_Wkf_Pnd_Wkf_Eff_Dte_Mm;
    private DbsField pnd_Wkf_Pnd_Wkf_Eff_Dte_Dd;
    private DbsField pnd_Wkf_Pnd_Wkf_Nap_Stts_Cde;

    private DbsGroup pnd_Wkf__R_Field_6;
    private DbsField pnd_Wkf_Pnd_Wkf_Nap_Stts_Cde_Byte_1;
    private DbsField pnd_Wkf_Pnd_Filler;
    private DbsField pnd_Wkf_Pnd_Wkf_Nac_Stts_Cde;
    private DbsField pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr;
    private DbsField pnd_Wkf_Pnd_Wkf_Lst_Updte;

    private DbsGroup pnd_Wkf__R_Field_7;
    private DbsField pnd_Wkf_Pnd_Wkf_Lst_Updte_Cc;
    private DbsField pnd_Wkf_Pnd_Wkf_Lst_Updte_Yy;
    private DbsField pnd_Wkf_Pnd_Wkf_Lst_Updte_Mm;
    private DbsField pnd_Wkf_Pnd_Wkf_Lst_Updte_Dd;
    private DbsField pnd_Wkf_Pnd_Wkf_Annty_Strt_Dte;
    private DbsField pnd_Wkf_Pnd_Wkf_Repl_Ind;
    private DbsField pnd_Wkf_Pnd_Wkf_Survivor_Ind;
    private DbsField pnd_X;
    private DbsField pnd_Business_Dte;

    private DbsGroup pnd_Business_Dte__R_Field_8;
    private DbsField pnd_Business_Dte_Pnd_Business_Cc;
    private DbsField pnd_Business_Dte_Pnd_Business_Yy;
    private DbsField pnd_Business_Dte_Pnd_Business_Mm;
    private DbsField pnd_Business_Dte_Pnd_Business_Dd;
    private DbsField pnd_Formatted_Eff_Dte;

    private DbsGroup pnd_Formatted_Eff_Dte__R_Field_9;
    private DbsField pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Mm;
    private DbsField pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Dd;
    private DbsField pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Yy;
    private DbsField pnd_Formatted_Bsns_Dte;

    private DbsGroup pnd_Formatted_Bsns_Dte__R_Field_10;
    private DbsField pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Mm;
    private DbsField pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Dd;
    private DbsField pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Yy;
    private DbsField pnd_Formatted_Lst_Updte;

    private DbsGroup pnd_Formatted_Lst_Updte__R_Field_11;
    private DbsField pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Mm;
    private DbsField pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Dd;
    private DbsField pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Yy;
    private DbsField pnd_Wkf_Repl_Cnt;
    private DbsField pnd_Wkf_Mat_Cnt;
    private DbsField pnd_Tbl_Desc;
    private DbsField pnd_Tbl_Error_Msg;

    private DbsGroup pnd_Tbl_Error_Msg__R_Field_12;
    private DbsField pnd_Tbl_Error_Msg_Pnd_Tbl_Lit;
    private DbsField pnd_Tbl_Error_Msg_Pnd_Tbl_Err_Cd;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_13;
    private DbsField pnd_Date_A_Pnd_Date_N;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Pnd_Wkf_Repl_IndOld;
    private DbsField sort01Pnd_Wkf_Bps_UnitOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_cntlrcd = new DataAccessProgramView(new NameInfo("vw_cntlrcd", "CNTLRCD"), "ADS_CNTL", "ADS_CNTL");

        cntlrcd_Ads_Cntl_Grp = vw_cntlrcd.getRecord().newGroupInGroup("CNTLRCD_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        cntlrcd_Ads_Cntl_Rcrd_Typ_Cde = cntlrcd_Ads_Cntl_Grp.newFieldInGroup("cntlrcd_Ads_Cntl_Rcrd_Typ_Cde", "ADS-CNTL-RCRD-TYP-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADS_CNTL_RCRD_TYP_CDE");
        cntlrcd_Ads_Cntl_Bsnss_Rcprcl_Dte = cntlrcd_Ads_Cntl_Grp.newFieldInGroup("cntlrcd_Ads_Cntl_Bsnss_Rcprcl_Dte", "ADS-CNTL-BSNSS-RCPRCL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_RCPRCL_DTE");
        cntlrcd_Ads_Cntl_Bsnss_Dte = cntlrcd_Ads_Cntl_Grp.newFieldInGroup("cntlrcd_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "ADS_CNTL_BSNSS_DTE");
        cntlrcd_Ads_Cntl_Bsnss_Tmrrw_Dte = cntlrcd_Ads_Cntl_Grp.newFieldInGroup("cntlrcd_Ads_Cntl_Bsnss_Tmrrw_Dte", "ADS-CNTL-BSNSS-TMRRW-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_TMRRW_DTE");
        cntlrcd_Ads_Rpt_13 = vw_cntlrcd.getRecord().newFieldInGroup("cntlrcd_Ads_Rpt_13", "ADS-RPT-13", FieldType.DATE, RepeatingFieldStrategy.None, "ADS_RPT_13");
        registerRecord(vw_cntlrcd);

        vw_tblrec = new DataAccessProgramView(new NameInfo("vw_tblrec", "TBLREC"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        tblrec_Naz_Tbl_Rcrd_Lvl3_Id = vw_tblrec.getRecord().newFieldInGroup("tblrec_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", FieldType.STRING, 20, 
            RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        tblrec_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        tblrec_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_tblrec.getRecord().newFieldInGroup("tblrec_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        tblrec_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");

        tblrec__R_Field_1 = vw_tblrec.getRecord().newGroupInGroup("tblrec__R_Field_1", "REDEFINE", tblrec_Naz_Tbl_Rcrd_Dscrptn_Txt);
        tblrec_Pnd_Tbl_Rcrd_Wpid = tblrec__R_Field_1.newFieldInGroup("tblrec_Pnd_Tbl_Rcrd_Wpid", "#TBL-RCRD-WPID", FieldType.STRING, 6);
        tblrec_Pnd_Dscrptn_Txt_Filler_1 = tblrec__R_Field_1.newFieldInGroup("tblrec_Pnd_Dscrptn_Txt_Filler_1", "#DSCRPTN-TXT-FILLER-1", FieldType.STRING, 
            1);
        tblrec_Pnd_Tbl_Rcrd_Mit_Stts = tblrec__R_Field_1.newFieldInGroup("tblrec_Pnd_Tbl_Rcrd_Mit_Stts", "#TBL-RCRD-MIT-STTS", FieldType.NUMERIC, 4);
        tblrec_Pnd_Dscrptn_Txt_Filler_2 = tblrec__R_Field_1.newFieldInGroup("tblrec_Pnd_Dscrptn_Txt_Filler_2", "#DSCRPTN-TXT-FILLER-2", FieldType.STRING, 
            1);
        tblrec_Pnd_Tbl_Rcrd_Unit = tblrec__R_Field_1.newFieldInGroup("tblrec_Pnd_Tbl_Rcrd_Unit", "#TBL-RCRD-UNIT", FieldType.STRING, 6);
        tblrec_Pnd_Dscrptn_Txt_Filler_3 = tblrec__R_Field_1.newFieldInGroup("tblrec_Pnd_Dscrptn_Txt_Filler_3", "#DSCRPTN-TXT-FILLER-3", FieldType.STRING, 
            1);
        tblrec_Pnd_Tbl_Rcrd_Racf = tblrec__R_Field_1.newFieldInGroup("tblrec_Pnd_Tbl_Rcrd_Racf", "#TBL-RCRD-RACF", FieldType.STRING, 8);
        tblrec_Pnd_Dscrptn_Txt_Filler_4 = tblrec__R_Field_1.newFieldInGroup("tblrec_Pnd_Dscrptn_Txt_Filler_4", "#DSCRPTN-TXT-FILLER-4", FieldType.STRING, 
            33);
        tblrec_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup = vw_tblrec.getRecord().newGroupInGroup("TBLREC_NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", "NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        tblrec_Naz_Tbl_Secndry_Dscrptn_Txt = tblrec_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup.newFieldArrayInGroup("tblrec_Naz_Tbl_Secndry_Dscrptn_Txt", "NAZ-TBL-SECNDRY-DSCRPTN-TXT", 
            FieldType.STRING, 80, new DbsArrayController(1, 1), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        tblrec_Naz_Tbl_Secndry_Dscrptn_Txt.setDdmHeader("CDE/2ND/DSC");
        registerRecord(vw_tblrec);

        pnd_Sd1 = localVariables.newFieldInRecord("pnd_Sd1", "#SD1", FieldType.STRING, 29);

        pnd_Sd1__R_Field_2 = localVariables.newGroupInRecord("pnd_Sd1__R_Field_2", "REDEFINE", pnd_Sd1);
        pnd_Sd1_Pnd_Sd1_Lvl1 = pnd_Sd1__R_Field_2.newFieldInGroup("pnd_Sd1_Pnd_Sd1_Lvl1", "#SD1-LVL1", FieldType.STRING, 6);
        pnd_Sd1_Pnd_Sd1_Lvl2 = pnd_Sd1__R_Field_2.newFieldInGroup("pnd_Sd1_Pnd_Sd1_Lvl2", "#SD1-LVL2", FieldType.STRING, 3);
        pnd_Sd1_Pnd_Sd1_Lvl3 = pnd_Sd1__R_Field_2.newFieldInGroup("pnd_Sd1_Pnd_Sd1_Lvl3", "#SD1-LVL3", FieldType.STRING, 20);

        pnd_Sd1__R_Field_3 = pnd_Sd1__R_Field_2.newGroupInGroup("pnd_Sd1__R_Field_3", "REDEFINE", pnd_Sd1_Pnd_Sd1_Lvl3);
        pnd_Sd1_Pnd_Lvl_3_Stts = pnd_Sd1__R_Field_3.newFieldInGroup("pnd_Sd1_Pnd_Lvl_3_Stts", "#LVL-3-STTS", FieldType.STRING, 3);
        pnd_Sd1_Pnd_Filler = pnd_Sd1__R_Field_3.newFieldInGroup("pnd_Sd1_Pnd_Filler", "#FILLER", FieldType.STRING, 17);
        pnd_Wkf = localVariables.newFieldInRecord("pnd_Wkf", "#WKF", FieldType.STRING, 83);

        pnd_Wkf__R_Field_4 = localVariables.newGroupInRecord("pnd_Wkf__R_Field_4", "REDEFINE", pnd_Wkf);
        pnd_Wkf_Pnd_Wkf_Bps_Unit = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Bps_Unit", "#WKF-BPS-UNIT", FieldType.STRING, 8);
        pnd_Wkf_Pnd_Wkf_Cntrct = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Cntrct", "#WKF-CNTRCT", FieldType.STRING, 10);
        pnd_Wkf_Pnd_Wkf_Unique_Id = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Unique_Id", "#WKF-UNIQUE-ID", FieldType.NUMERIC, 12);
        pnd_Wkf_Pnd_Wkf_Eff_Dte = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Eff_Dte", "#WKF-EFF-DTE", FieldType.NUMERIC, 8);

        pnd_Wkf__R_Field_5 = pnd_Wkf__R_Field_4.newGroupInGroup("pnd_Wkf__R_Field_5", "REDEFINE", pnd_Wkf_Pnd_Wkf_Eff_Dte);
        pnd_Wkf_Pnd_Wkf_Eff_Dte_Cc = pnd_Wkf__R_Field_5.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Eff_Dte_Cc", "#WKF-EFF-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Wkf_Pnd_Wkf_Eff_Dte_Yy = pnd_Wkf__R_Field_5.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Eff_Dte_Yy", "#WKF-EFF-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Wkf_Pnd_Wkf_Eff_Dte_Mm = pnd_Wkf__R_Field_5.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Eff_Dte_Mm", "#WKF-EFF-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Wkf_Pnd_Wkf_Eff_Dte_Dd = pnd_Wkf__R_Field_5.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Eff_Dte_Dd", "#WKF-EFF-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Wkf_Pnd_Wkf_Nap_Stts_Cde = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Nap_Stts_Cde", "#WKF-NAP-STTS-CDE", FieldType.STRING, 3);

        pnd_Wkf__R_Field_6 = pnd_Wkf__R_Field_4.newGroupInGroup("pnd_Wkf__R_Field_6", "REDEFINE", pnd_Wkf_Pnd_Wkf_Nap_Stts_Cde);
        pnd_Wkf_Pnd_Wkf_Nap_Stts_Cde_Byte_1 = pnd_Wkf__R_Field_6.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Nap_Stts_Cde_Byte_1", "#WKF-NAP-STTS-CDE-BYTE-1", FieldType.STRING, 
            1);
        pnd_Wkf_Pnd_Filler = pnd_Wkf__R_Field_6.newFieldInGroup("pnd_Wkf_Pnd_Filler", "#FILLER", FieldType.STRING, 2);
        pnd_Wkf_Pnd_Wkf_Nac_Stts_Cde = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Nac_Stts_Cde", "#WKF-NAC-STTS-CDE", FieldType.STRING, 3);
        pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr", "#WKF-IA-TIAA-NBR", FieldType.STRING, 10);
        pnd_Wkf_Pnd_Wkf_Lst_Updte = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Lst_Updte", "#WKF-LST-UPDTE", FieldType.NUMERIC, 8);

        pnd_Wkf__R_Field_7 = pnd_Wkf__R_Field_4.newGroupInGroup("pnd_Wkf__R_Field_7", "REDEFINE", pnd_Wkf_Pnd_Wkf_Lst_Updte);
        pnd_Wkf_Pnd_Wkf_Lst_Updte_Cc = pnd_Wkf__R_Field_7.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Lst_Updte_Cc", "#WKF-LST-UPDTE-CC", FieldType.NUMERIC, 2);
        pnd_Wkf_Pnd_Wkf_Lst_Updte_Yy = pnd_Wkf__R_Field_7.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Lst_Updte_Yy", "#WKF-LST-UPDTE-YY", FieldType.NUMERIC, 2);
        pnd_Wkf_Pnd_Wkf_Lst_Updte_Mm = pnd_Wkf__R_Field_7.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Lst_Updte_Mm", "#WKF-LST-UPDTE-MM", FieldType.NUMERIC, 2);
        pnd_Wkf_Pnd_Wkf_Lst_Updte_Dd = pnd_Wkf__R_Field_7.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Lst_Updte_Dd", "#WKF-LST-UPDTE-DD", FieldType.NUMERIC, 2);
        pnd_Wkf_Pnd_Wkf_Annty_Strt_Dte = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Annty_Strt_Dte", "#WKF-ANNTY-STRT-DTE", FieldType.NUMERIC, 
            8);
        pnd_Wkf_Pnd_Wkf_Repl_Ind = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Repl_Ind", "#WKF-REPL-IND", FieldType.STRING, 1);
        pnd_Wkf_Pnd_Wkf_Survivor_Ind = pnd_Wkf__R_Field_4.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Survivor_Ind", "#WKF-SURVIVOR-IND", FieldType.NUMERIC, 12);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.INTEGER, 4);
        pnd_Business_Dte = localVariables.newFieldInRecord("pnd_Business_Dte", "#BUSINESS-DTE", FieldType.NUMERIC, 8);

        pnd_Business_Dte__R_Field_8 = localVariables.newGroupInRecord("pnd_Business_Dte__R_Field_8", "REDEFINE", pnd_Business_Dte);
        pnd_Business_Dte_Pnd_Business_Cc = pnd_Business_Dte__R_Field_8.newFieldInGroup("pnd_Business_Dte_Pnd_Business_Cc", "#BUSINESS-CC", FieldType.NUMERIC, 
            2);
        pnd_Business_Dte_Pnd_Business_Yy = pnd_Business_Dte__R_Field_8.newFieldInGroup("pnd_Business_Dte_Pnd_Business_Yy", "#BUSINESS-YY", FieldType.NUMERIC, 
            2);
        pnd_Business_Dte_Pnd_Business_Mm = pnd_Business_Dte__R_Field_8.newFieldInGroup("pnd_Business_Dte_Pnd_Business_Mm", "#BUSINESS-MM", FieldType.NUMERIC, 
            2);
        pnd_Business_Dte_Pnd_Business_Dd = pnd_Business_Dte__R_Field_8.newFieldInGroup("pnd_Business_Dte_Pnd_Business_Dd", "#BUSINESS-DD", FieldType.NUMERIC, 
            2);
        pnd_Formatted_Eff_Dte = localVariables.newFieldInRecord("pnd_Formatted_Eff_Dte", "#FORMATTED-EFF-DTE", FieldType.NUMERIC, 6);

        pnd_Formatted_Eff_Dte__R_Field_9 = localVariables.newGroupInRecord("pnd_Formatted_Eff_Dte__R_Field_9", "REDEFINE", pnd_Formatted_Eff_Dte);
        pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Mm = pnd_Formatted_Eff_Dte__R_Field_9.newFieldInGroup("pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Mm", 
            "#FORMATTED-EFF-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Dd = pnd_Formatted_Eff_Dte__R_Field_9.newFieldInGroup("pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Dd", 
            "#FORMATTED-EFF-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Yy = pnd_Formatted_Eff_Dte__R_Field_9.newFieldInGroup("pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Yy", 
            "#FORMATTED-EFF-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Formatted_Bsns_Dte = localVariables.newFieldInRecord("pnd_Formatted_Bsns_Dte", "#FORMATTED-BSNS-DTE", FieldType.NUMERIC, 6);

        pnd_Formatted_Bsns_Dte__R_Field_10 = localVariables.newGroupInRecord("pnd_Formatted_Bsns_Dte__R_Field_10", "REDEFINE", pnd_Formatted_Bsns_Dte);
        pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Mm = pnd_Formatted_Bsns_Dte__R_Field_10.newFieldInGroup("pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Mm", 
            "#FORMATTED-BSNS-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Dd = pnd_Formatted_Bsns_Dte__R_Field_10.newFieldInGroup("pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Dd", 
            "#FORMATTED-BSNS-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Yy = pnd_Formatted_Bsns_Dte__R_Field_10.newFieldInGroup("pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Yy", 
            "#FORMATTED-BSNS-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Formatted_Lst_Updte = localVariables.newFieldInRecord("pnd_Formatted_Lst_Updte", "#FORMATTED-LST-UPDTE", FieldType.NUMERIC, 6);

        pnd_Formatted_Lst_Updte__R_Field_11 = localVariables.newGroupInRecord("pnd_Formatted_Lst_Updte__R_Field_11", "REDEFINE", pnd_Formatted_Lst_Updte);
        pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Mm = pnd_Formatted_Lst_Updte__R_Field_11.newFieldInGroup("pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Mm", 
            "#FORMATTED-LST-UPDTE-MM", FieldType.NUMERIC, 2);
        pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Dd = pnd_Formatted_Lst_Updte__R_Field_11.newFieldInGroup("pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Dd", 
            "#FORMATTED-LST-UPDTE-DD", FieldType.NUMERIC, 2);
        pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Yy = pnd_Formatted_Lst_Updte__R_Field_11.newFieldInGroup("pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Yy", 
            "#FORMATTED-LST-UPDTE-YY", FieldType.NUMERIC, 2);
        pnd_Wkf_Repl_Cnt = localVariables.newFieldInRecord("pnd_Wkf_Repl_Cnt", "#WKF-REPL-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Wkf_Mat_Cnt = localVariables.newFieldInRecord("pnd_Wkf_Mat_Cnt", "#WKF-MAT-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Tbl_Desc = localVariables.newFieldInRecord("pnd_Tbl_Desc", "#TBL-DESC", FieldType.STRING, 62);
        pnd_Tbl_Error_Msg = localVariables.newFieldInRecord("pnd_Tbl_Error_Msg", "#TBL-ERROR-MSG", FieldType.STRING, 62);

        pnd_Tbl_Error_Msg__R_Field_12 = localVariables.newGroupInRecord("pnd_Tbl_Error_Msg__R_Field_12", "REDEFINE", pnd_Tbl_Error_Msg);
        pnd_Tbl_Error_Msg_Pnd_Tbl_Lit = pnd_Tbl_Error_Msg__R_Field_12.newFieldInGroup("pnd_Tbl_Error_Msg_Pnd_Tbl_Lit", "#TBL-LIT", FieldType.STRING, 20);
        pnd_Tbl_Error_Msg_Pnd_Tbl_Err_Cd = pnd_Tbl_Error_Msg__R_Field_12.newFieldInGroup("pnd_Tbl_Error_Msg_Pnd_Tbl_Err_Cd", "#TBL-ERR-CD", FieldType.STRING, 
            42);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_13 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_13", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_13.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Pnd_Wkf_Repl_IndOld = internalLoopRecord.newFieldInRecord("Sort01_Pnd_Wkf_Repl_Ind_OLD", "Pnd_Wkf_Repl_Ind_OLD", FieldType.STRING, 1);
        sort01Pnd_Wkf_Bps_UnitOld = internalLoopRecord.newFieldInRecord("Sort01_Pnd_Wkf_Bps_Unit_OLD", "Pnd_Wkf_Bps_Unit_OLD", FieldType.STRING, 8);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cntlrcd.reset();
        vw_tblrec.reset();
        internalLoopRecord.reset();

        localVariables.reset();
        pnd_Wkf_Repl_Cnt.setInitialValue(0);
        pnd_Wkf_Mat_Cnt.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Adsp938() throws Exception
    {
        super("Adsp938");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
                                                                                                                                                                          //Natural: FORMAT LS = 133 PS = 55;//Natural: FORMAT ( 2 ) LS = 133 PS = 55;//Natural: PERFORM GET-BSNS-DTE
        sub_Get_Bsns_Dte();
        if (condition(Global.isEscape())) {return;}
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 #WKF
        while (condition(getWorkFiles().read(1, pnd_Wkf)))
        {
            if (condition(!(pnd_Wkf_Pnd_Wkf_Nap_Stts_Cde_Byte_1.equals("F") || pnd_Wkf_Pnd_Wkf_Nap_Stts_Cde_Byte_1.equals("M"))))                                         //Natural: ACCEPT IF #WKF-NAP-STTS-CDE-BYTE-1 EQ 'F' OR = 'M'
            {
                continue;
            }
            getSort().writeSortInData(pnd_Wkf_Pnd_Wkf_Bps_Unit, pnd_Wkf_Pnd_Wkf_Repl_Ind, pnd_Wkf_Pnd_Wkf_Eff_Dte, pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr, pnd_Wkf_Pnd_Wkf_Nap_Stts_Cde,  //Natural: END-ALL
                pnd_Wkf_Pnd_Wkf_Nac_Stts_Cde, pnd_Wkf_Pnd_Wkf_Cntrct, pnd_Wkf_Pnd_Wkf_Unique_Id, pnd_Wkf_Pnd_Wkf_Lst_Updte, pnd_Wkf_Pnd_Wkf_Survivor_Ind);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  DEA
        getSort().sortData(pnd_Wkf_Pnd_Wkf_Bps_Unit, pnd_Wkf_Pnd_Wkf_Repl_Ind, pnd_Wkf_Pnd_Wkf_Eff_Dte, pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr, pnd_Wkf_Pnd_Wkf_Nap_Stts_Cde,        //Natural: SORT BY #WKF-BPS-UNIT #WKF-REPL-IND #WKF-EFF-DTE #WKF-IA-TIAA-NBR #WKF-NAP-STTS-CDE #WKF-NAC-STTS-CDE USING #WKF-CNTRCT #WKF-UNIQUE-ID #WKF-LST-UPDTE #WKF-SURVIVOR-IND
            pnd_Wkf_Pnd_Wkf_Nac_Stts_Cde);
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Wkf_Pnd_Wkf_Bps_Unit, pnd_Wkf_Pnd_Wkf_Repl_Ind, pnd_Wkf_Pnd_Wkf_Eff_Dte, pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr, 
            pnd_Wkf_Pnd_Wkf_Nap_Stts_Cde, pnd_Wkf_Pnd_Wkf_Nac_Stts_Cde, pnd_Wkf_Pnd_Wkf_Cntrct, pnd_Wkf_Pnd_Wkf_Unique_Id, pnd_Wkf_Pnd_Wkf_Lst_Updte, pnd_Wkf_Pnd_Wkf_Survivor_Ind)))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Yy.setValue(pnd_Wkf_Pnd_Wkf_Eff_Dte_Yy);                                                                          //Natural: MOVE #WKF-EFF-DTE-YY TO #FORMATTED-EFF-DTE-YY
            pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Mm.setValue(pnd_Wkf_Pnd_Wkf_Eff_Dte_Mm);                                                                          //Natural: MOVE #WKF-EFF-DTE-MM TO #FORMATTED-EFF-DTE-MM
            pnd_Formatted_Eff_Dte_Pnd_Formatted_Eff_Dte_Dd.setValue(pnd_Wkf_Pnd_Wkf_Eff_Dte_Dd);                                                                          //Natural: MOVE #WKF-EFF-DTE-DD TO #FORMATTED-EFF-DTE-DD
            pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Yy.setValue(pnd_Wkf_Pnd_Wkf_Lst_Updte_Yy);                                                                    //Natural: MOVE #WKF-LST-UPDTE-YY TO #FORMATTED-LST-UPDTE-YY
            pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Mm.setValue(pnd_Wkf_Pnd_Wkf_Lst_Updte_Mm);                                                                    //Natural: MOVE #WKF-LST-UPDTE-MM TO #FORMATTED-LST-UPDTE-MM
            pnd_Formatted_Lst_Updte_Pnd_Formatted_Lst_Updte_Dd.setValue(pnd_Wkf_Pnd_Wkf_Lst_Updte_Dd);                                                                    //Natural: MOVE #WKF-LST-UPDTE-DD TO #FORMATTED-LST-UPDTE-DD
            if (condition(pnd_Wkf_Pnd_Wkf_Repl_Ind.equals("Y")))                                                                                                          //Natural: IF #WKF-REPL-IND = 'Y'
            {
                pnd_Wkf_Repl_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #WKF-REPL-CNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Wkf_Mat_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #WKF-MAT-CNT
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM READ-DSCRPTN-TBL
            sub_Read_Dscrptn_Tbl();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  3X /* 02232017
            //*  3X /* REMOVE
            //*  3X /* SPACES
            //*  2X /*
            //*  3X /*
            getReports().display(2, "LAST/UPDATE DTE",                                                                                                                    //Natural: DISPLAY ( 2 ) 'LAST/UPDATE DTE' #FORMATTED-LST-UPDTE ( EM = 99/99/99 ) 3X 'DA/ID' #WKF-UNIQUE-ID 'IA/CONTRACT' #WKF-IA-TIAA-NBR ( EM = XXXXXXX-X ) 'DA/CONTRACT' #WKF-CNTRCT ( EM = XXXXXXX-X ) 'EFFECT/DATE' #FORMATTED-EFF-DTE ( EM = 99/99/99 ) 'IA/ID' #WKF-SURVIVOR-IND 'STATUS/DESCRIPTION' #TBL-DESC
            		pnd_Formatted_Lst_Updte, new ReportEditMask ("99/99/99"),new ColumnSpacing(3),"DA/ID",
            		pnd_Wkf_Pnd_Wkf_Unique_Id,"IA/CONTRACT",
            		pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr, new ReportEditMask ("XXXXXXX-X"),"DA/CONTRACT",
            		pnd_Wkf_Pnd_Wkf_Cntrct, new ReportEditMask ("XXXXXXX-X"),"EFFECT/DATE",
            		pnd_Formatted_Eff_Dte, new ReportEditMask ("99/99/99"),"IA/ID",
            		pnd_Wkf_Pnd_Wkf_Survivor_Ind,"STATUS/DESCRIPTION",
            		pnd_Tbl_Desc);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF #WKF-IA-TIAA-NBR
            //*                                                                                                                                                           //Natural: AT BREAK OF #WKF-REPL-IND
            //*                                                                                                                                                           //Natural: AT BREAK OF #WKF-BPS-UNIT
            sort01Pnd_Wkf_Repl_IndOld.setValue(pnd_Wkf_Pnd_Wkf_Repl_Ind);                                                                                                 //Natural: END-SORT
            sort01Pnd_Wkf_Bps_UnitOld.setValue(pnd_Wkf_Pnd_Wkf_Bps_Unit);
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //* ****************************************************************
        //* ****************************************************************
        //* ****************************************************************
    }                                                                                                                                                                     //Natural: AT TOP OF PAGE ( 2 )
    private void sub_Get_Bsns_Dte() throws Exception                                                                                                                      //Natural: GET-BSNS-DTE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************************************
        vw_cntlrcd.startDatabaseRead                                                                                                                                      //Natural: READ ( 2 ) CNTLRCD BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "READ02",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        READ02:
        while (condition(vw_cntlrcd.readNextRow("READ02")))
        {
            if (condition(vw_cntlrcd.getAstCOUNTER().equals(2)))                                                                                                          //Natural: IF *COUNTER = 2
            {
                if (condition(cntlrcd_Ads_Rpt_13.notEquals(getZero())))                                                                                                   //Natural: IF ADS-RPT-13 NE 0
                {
                    pnd_Date_A.setValueEdited(cntlrcd_Ads_Rpt_13,new ReportEditMask("YYYYMMDD"));                                                                         //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE-A
                    pnd_Business_Dte.setValue(pnd_Date_A_Pnd_Date_N);                                                                                                     //Natural: MOVE #DATE-N TO #BUSINESS-DTE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Business_Dte.setValue(cntlrcd_Ads_Cntl_Bsnss_Dte);                                                                                                //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #BUSINESS-DTE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Yy.setValue(pnd_Business_Dte_Pnd_Business_Yy);                                                              //Natural: MOVE #BUSINESS-YY TO #FORMATTED-BSNS-DTE-YY
                pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Mm.setValue(pnd_Business_Dte_Pnd_Business_Mm);                                                              //Natural: MOVE #BUSINESS-MM TO #FORMATTED-BSNS-DTE-MM
                pnd_Formatted_Bsns_Dte_Pnd_Formatted_Bsns_Dte_Dd.setValue(pnd_Business_Dte_Pnd_Business_Dd);                                                              //Natural: MOVE #BUSINESS-DD TO #FORMATTED-BSNS-DTE-DD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_Dscrptn_Tbl() throws Exception                                                                                                                  //Natural: READ-DSCRPTN-TBL
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************************************
        pnd_Sd1.reset();                                                                                                                                                  //Natural: RESET #SD1
        pnd_Sd1_Pnd_Sd1_Lvl1.setValue("NAZ063");                                                                                                                          //Natural: ASSIGN #SD1-LVL1 := 'NAZ063'
        pnd_Sd1_Pnd_Sd1_Lvl2.setValue("MIT");                                                                                                                             //Natural: ASSIGN #SD1-LVL2 := 'MIT'
        pnd_Sd1_Pnd_Lvl_3_Stts.setValue(pnd_Wkf_Pnd_Wkf_Nac_Stts_Cde);                                                                                                    //Natural: ASSIGN #LVL-3-STTS := #WKF-NAC-STTS-CDE
        getReports().write(0, "WORK STATUS:",pnd_Wkf_Pnd_Wkf_Nac_Stts_Cde);                                                                                               //Natural: WRITE 'WORK STATUS:' #WKF-NAC-STTS-CDE
        if (Global.isEscape()) return;
        vw_tblrec.startDatabaseFind                                                                                                                                       //Natural: FIND TBLREC WITH NAZ-TBL-SUPER1 = #SD1
        (
        "FIND01",
        new Wc[] { new Wc("NAZ_TBL_SUPER1", "=", pnd_Sd1, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_tblrec.readNextRow("FIND01", true)))
        {
            vw_tblrec.setIfNotFoundControlFlag(false);
            if (condition(vw_tblrec.getAstCOUNTER().equals(0)))                                                                                                           //Natural: IF NO RECORD FOUND
            {
                pnd_Tbl_Error_Msg_Pnd_Tbl_Err_Cd.setValue(pnd_Wkf_Pnd_Wkf_Nac_Stts_Cde);                                                                                  //Natural: ASSIGN #TBL-ERR-CD = #WKF-NAC-STTS-CDE
                pnd_Tbl_Error_Msg_Pnd_Tbl_Lit.setValue("NO MESSAGE FOR CODE ");                                                                                           //Natural: ASSIGN #TBL-LIT = 'NO MESSAGE FOR CODE '
                pnd_Tbl_Desc.setValue(pnd_Tbl_Error_Msg);                                                                                                                 //Natural: ASSIGN #TBL-DESC = #TBL-ERROR-MSG
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Tbl_Desc.setValue(tblrec_Naz_Tbl_Secndry_Dscrptn_Txt.getValue(1));                                                                                        //Natural: MOVE NAZ-TBL-SECNDRY-DSCRPTN-TXT ( 1 ) TO #TBL-DESC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //* *********************************************************************
                    //*  DEA
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"PROGRAM: ",Global.getPROGRAM(),new TabSetting(35),"ANNUITIZATION OF SURVIVOR OMNIPLUS CONTRACTS",new  //Natural: WRITE ( 2 ) NOTITLE NOHDR / 'PROGRAM: ' *PROGRAM 35T 'ANNUITIZATION OF SURVIVOR OMNIPLUS CONTRACTS' 101T 'RUN DATE:' *DATU / 45T 'CASES PENDING SETTLEMENT REPORT' 101T 'RUN TIME:' *TIMX / 47T 'AS OF ACCOUNTING DATE' #FORMATTED-BSNS-DTE ( EM = 99/99/99 ) 101T 'PAGE:' *PAGE-NUMBER ( 2 ) //
                        TabSetting(101),"RUN DATE:",Global.getDATU(),NEWLINE,new TabSetting(45),"CASES PENDING SETTLEMENT REPORT",new TabSetting(101),"RUN TIME:",Global.getTIMX(),NEWLINE,new 
                        TabSetting(47),"AS OF ACCOUNTING DATE",pnd_Formatted_Bsns_Dte, new ReportEditMask ("99/99/99"),new TabSetting(101),"PAGE:",getReports().getPageNumberDbs(2),
                        NEWLINE,NEWLINE);
                    if (condition(pnd_Wkf_Pnd_Wkf_Repl_Ind.equals(" ")))                                                                                                  //Natural: IF #WKF-REPL-IND = ' '
                    {
                        getReports().write(2, ReportOption.NOTITLE,"MATURITIES  ",pnd_Wkf_Pnd_Wkf_Bps_Unit);                                                              //Natural: WRITE ( 2 ) 'MATURITIES  ' #WKF-BPS-UNIT
                        getReports().write(2, ReportOption.NOTITLE,"-------------------",NEWLINE,NEWLINE);                                                                //Natural: WRITE ( 2 ) '-------------------'//
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(2, ReportOption.NOTITLE,"RESETTLEMENTS ",pnd_Wkf_Pnd_Wkf_Bps_Unit);                                                            //Natural: WRITE ( 2 ) 'RESETTLEMENTS ' #WKF-BPS-UNIT
                        getReports().write(2, ReportOption.NOTITLE,"----------------------",NEWLINE,NEWLINE);                                                             //Natural: WRITE ( 2 ) '----------------------'//
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Wkf_Pnd_Wkf_Ia_Tiaa_NbrIsBreak = pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr.isBreak(endOfData);
        boolean pnd_Wkf_Pnd_Wkf_Repl_IndIsBreak = pnd_Wkf_Pnd_Wkf_Repl_Ind.isBreak(endOfData);
        boolean pnd_Wkf_Pnd_Wkf_Bps_UnitIsBreak = pnd_Wkf_Pnd_Wkf_Bps_Unit.isBreak(endOfData);
        if (condition(pnd_Wkf_Pnd_Wkf_Ia_Tiaa_NbrIsBreak || pnd_Wkf_Pnd_Wkf_Repl_IndIsBreak || pnd_Wkf_Pnd_Wkf_Bps_UnitIsBreak))
        {
            getReports().write(2, ReportOption.NOTITLE," ");                                                                                                              //Natural: WRITE ( 2 ) ' '
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Wkf_Pnd_Wkf_Repl_IndIsBreak || pnd_Wkf_Pnd_Wkf_Bps_UnitIsBreak))
        {
            if (condition(sort01Pnd_Wkf_Repl_IndOld.equals(" ")))                                                                                                         //Natural: IF OLD ( #WKF-REPL-IND ) = ' '
            {
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"TOTAL PENDING MATURITIES FOR UNIT",sort01Pnd_Wkf_Bps_UnitOld,pnd_Wkf_Mat_Cnt, //Natural: WRITE ( 2 ) //// 'TOTAL PENDING MATURITIES FOR UNIT' OLD ( #WKF-BPS-UNIT ) #WKF-MAT-CNT ( EM = ZZ,ZZZ )
                    new ReportEditMask ("ZZ,ZZZ"));
                if (condition(Global.isEscape())) return;
                pnd_Wkf_Mat_Cnt.reset();                                                                                                                                  //Natural: RESET #WKF-MAT-CNT
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 2 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"TOTAL PENDING RESETTLEMENTS FOR UNIT",sort01Pnd_Wkf_Bps_UnitOld,pnd_Wkf_Repl_Cnt,  //Natural: WRITE ( 2 ) //// 'TOTAL PENDING RESETTLEMENTS FOR UNIT' OLD ( #WKF-BPS-UNIT ) #WKF-REPL-CNT ( EM = ZZ,ZZZ )
                    new ReportEditMask ("ZZ,ZZZ"));
                if (condition(Global.isEscape())) return;
                pnd_Wkf_Repl_Cnt.reset();                                                                                                                                 //Natural: RESET #WKF-REPL-CNT
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 2 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Wkf_Pnd_Wkf_Bps_UnitIsBreak))
        {
            if (condition(sort01Pnd_Wkf_Repl_IndOld.equals(pnd_Wkf_Pnd_Wkf_Repl_Ind)))                                                                                    //Natural: IF OLD ( #WKF-REPL-IND ) = #WKF-REPL-IND
            {
                if (condition(pnd_Wkf_Pnd_Wkf_Repl_Ind.equals(" ")))                                                                                                      //Natural: IF #WKF-REPL-IND = ' '
                {
                    if (condition(pnd_Wkf_Mat_Cnt.greater(getZero())))                                                                                                    //Natural: IF #WKF-MAT-CNT GT 0
                    {
                        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"TOTAL PENDING MATURITIES FOR UNIT",sort01Pnd_Wkf_Bps_UnitOld,pnd_Wkf_Mat_Cnt,  //Natural: WRITE ( 2 ) //// 'TOTAL PENDING MATURITIES FOR UNIT' OLD ( #WKF-BPS-UNIT ) #WKF-MAT-CNT ( EM = ZZ,ZZZ )
                            new ReportEditMask ("ZZ,ZZZ"));
                        if (condition(Global.isEscape())) return;
                        pnd_Wkf_Mat_Cnt.reset();                                                                                                                          //Natural: RESET #WKF-MAT-CNT
                        getReports().newPage(new ReportSpecification(2));                                                                                                 //Natural: NEWPAGE ( 2 )
                        if (condition(Global.isEscape())){return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Wkf_Repl_Cnt.greater(getZero())))                                                                                                   //Natural: IF #WKF-REPL-CNT GT 0
                    {
                        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"TOTAL PENDING RESETTLEMENTS FOR UNIT",sort01Pnd_Wkf_Bps_UnitOld,pnd_Wkf_Repl_Cnt,  //Natural: WRITE ( 2 ) //// 'TOTAL PENDING RESETTLEMENTS FOR UNIT' OLD ( #WKF-BPS-UNIT ) #WKF-REPL-CNT ( EM = ZZ,ZZZ )
                            new ReportEditMask ("ZZ,ZZZ"));
                        if (condition(Global.isEscape())) return;
                        pnd_Wkf_Repl_Cnt.reset();                                                                                                                         //Natural: RESET #WKF-REPL-CNT
                        getReports().newPage(new ReportSpecification(2));                                                                                                 //Natural: NEWPAGE ( 2 )
                        if (condition(Global.isEscape())){return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=55");
        Global.format(2, "LS=133 PS=55");

        getReports().setDisplayColumns(2, "LAST/UPDATE DTE",
        		pnd_Formatted_Lst_Updte, new ReportEditMask ("99/99/99"),new ColumnSpacing(3),"DA/ID",
        		pnd_Wkf_Pnd_Wkf_Unique_Id,"IA/CONTRACT",
        		pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr, new ReportEditMask ("XXXXXXX-X"),"DA/CONTRACT",
        		pnd_Wkf_Pnd_Wkf_Cntrct, new ReportEditMask ("XXXXXXX-X"),"EFFECT/DATE",
        		pnd_Formatted_Eff_Dte, new ReportEditMask ("99/99/99"),"IA/ID",
        		pnd_Wkf_Pnd_Wkf_Survivor_Ind,"STATUS/DESCRIPTION",
        		pnd_Tbl_Desc);
    }
}
