/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:04:45 PM
**        * FROM NATURAL PROGRAM : Adsp761
************************************************************
**        * FILE NAME            : Adsp761.java
**        * CLASS NAME           : Adsp761
**        * INSTANCE NAME        : Adsp761
************************************************************
************************************************************************
* PROGRAM  : ADSP761
* GENERATED: APRIL 7, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : THIS MODULE CALCULATES TIAA IA RENEWAL PAYMENTS,
*            PROCESSED BY ADAS
*
* REMARKS  : CLONED FROM NAZP761 (ADAM SYSTEM)
************************************************************************
* MOD DATE   MOD BY      DESCRIPTION OF CHANGES
* 12/19/97   ZAFAR KHAN  READ COMMAND CHANGED TO FIND FOR IA RESULT
*                        RECORD
* 11/17/03   C SCHNEIDER RE-STOWED NAZLIARC TO ALLOW FOR 80 TIAA RATES
* 04/07/04   E MELNIK    MODIFIED FOR ADAS (ANNUITIZATION SUNGUARD)
* 04/07/08   O SOTTO     TIAA RATE EXPANSION TO 99 OCCURS. SC 040708.
* 03/17/2008 E.MELNIK   ROTH 403B/401K CHANGES.  RECOMPILED FOR
*                       NEW ADSL450.
* 03/05/12   E. MELNIK RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                      UPDATED LINKAGE AREAS.  CHANGES MARKED BY
*                      EM - 030512.
* 02/27/2017 R.CARREON PIN EXPANSION 02272017
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp761 extends BLNatBase
{
    // Data Areas
    private GdaAdsg870 gdaAdsg870;
    private LdaAdsl762 ldaAdsl762;
    private LdaAdsl450 ldaAdsl450;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ads_Ia_Rslt2;
    private DbsField ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data;

    private DbsGroup ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt;
    private DbsField pnd_Ads_Ia_Super1;

    private DbsGroup pnd_Ads_Ia_Super1__R_Field_1;
    private DbsField pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rqst_Id;
    private DbsField pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde;
    private DbsField pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr;
    private DbsField pnd_Ads_Ia_Super1_2;

    private DbsGroup pnd_Ads_Ia_Super1_2__R_Field_2;
    private DbsField pnd_Ads_Ia_Super1_2_Pnd_Ads_Ia_Rqst_Id_2;
    private DbsField pnd_Ads_Ia_Super1_2_Pnd_Ads_Ia_Rcrd_Cde_2;
    private DbsField pnd_Ads_Ia_Super1_2_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr_2;

    private DbsGroup pnd_All_Indx;
    private DbsField pnd_All_Indx_Pnd_A_Indx;
    private DbsField pnd_All_Indx_Pnd_B_Indx;
    private DbsField pnd_All_Indx_Pnd_C_Indx;
    private DbsField pnd_All_Indx_Pnd_Next_Indx;
    private DbsField pnd_All_Indx_Pnd_Tiaa_Indx_4;
    private DbsField pnd_All_Indx_Pnd_Tiaa_Indx_5;
    private DbsField pnd_All_Indx_Pnd_Tiaa_Indx_6;
    private DbsField pnd_All_Indx_Pnd_Prod_Indx;
    private DbsField counter;
    private DbsField pnd_Rec_Found;
    private DbsField pnd_Max_Rates;
    private DbsField pnd_Max_Rslt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaAdsg870 = GdaAdsg870.getInstance(getCallnatLevel());
        registerRecord(gdaAdsg870);
        if (gdaOnly) return;

        ldaAdsl762 = new LdaAdsl762();
        registerRecord(ldaAdsl762);
        ldaAdsl450 = new LdaAdsl450();
        registerRecord(ldaAdsl450);
        registerRecord(ldaAdsl450.getVw_ads_Ia_Rslt_View());

        // Local Variables
        localVariables = new DbsRecord();

        vw_ads_Ia_Rslt2 = new DataAccessProgramView(new NameInfo("vw_ads_Ia_Rslt2", "ADS-IA-RSLT2"), "ADS_IA_RSLT", "ADS_IA_RSLT", DdmPeriodicGroups.getInstance().getGroups("ADS_IA_RSLT"));
        ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data = vw_ads_Ia_Rslt2.getRecord().newFieldInGroup("ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data", "C*ADI-DTL-TIAA-DATA", 
            RepeatingFieldStrategy.CAsteriskVariable, "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");

        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data = vw_ads_Ia_Rslt2.getRecord().newGroupInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data", "ADI-DTL-TIAA-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd", "ADI-DTL-TIAA-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt", "ADI-DTL-TIAA-DA-STD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt", "ADI-DTL-TIAA-DA-GRD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd", "ADI-DTL-TIAA-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_ACCT_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd", "ADI-DTL-TIAA-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_IA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst", "ADI-DTL-TIAA-EFF-RTE-INTRST", 
            FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_EFF_RTE_INTRST", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt", "ADI-DTL-TIAA-GRD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt", "ADI-DTL-TIAA-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt", "ADI-DTL-FNL-GRD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt", "ADI-DTL-FNL-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt", "ADI-DTL-TIAA-STD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt", "ADI-DTL-TIAA-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt", "ADI-DTL-FNL-STD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt", "ADI-DTL-FNL-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        registerRecord(vw_ads_Ia_Rslt2);

        pnd_Ads_Ia_Super1 = localVariables.newFieldInRecord("pnd_Ads_Ia_Super1", "#ADS-IA-SUPER1", FieldType.STRING, 41);

        pnd_Ads_Ia_Super1__R_Field_1 = localVariables.newGroupInRecord("pnd_Ads_Ia_Super1__R_Field_1", "REDEFINE", pnd_Ads_Ia_Super1);
        pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rqst_Id = pnd_Ads_Ia_Super1__R_Field_1.newFieldInGroup("pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rqst_Id", "#ADS-IA-RQST-ID", 
            FieldType.STRING, 35);
        pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde = pnd_Ads_Ia_Super1__R_Field_1.newFieldInGroup("pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde", "#ADS-IA-RCRD-CDE", 
            FieldType.STRING, 3);
        pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr = pnd_Ads_Ia_Super1__R_Field_1.newFieldInGroup("pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr", "#ADS-IA-RCRD-SQNCE-NBR", 
            FieldType.NUMERIC, 3);
        pnd_Ads_Ia_Super1_2 = localVariables.newFieldInRecord("pnd_Ads_Ia_Super1_2", "#ADS-IA-SUPER1-2", FieldType.STRING, 41);

        pnd_Ads_Ia_Super1_2__R_Field_2 = localVariables.newGroupInRecord("pnd_Ads_Ia_Super1_2__R_Field_2", "REDEFINE", pnd_Ads_Ia_Super1_2);
        pnd_Ads_Ia_Super1_2_Pnd_Ads_Ia_Rqst_Id_2 = pnd_Ads_Ia_Super1_2__R_Field_2.newFieldInGroup("pnd_Ads_Ia_Super1_2_Pnd_Ads_Ia_Rqst_Id_2", "#ADS-IA-RQST-ID-2", 
            FieldType.STRING, 35);
        pnd_Ads_Ia_Super1_2_Pnd_Ads_Ia_Rcrd_Cde_2 = pnd_Ads_Ia_Super1_2__R_Field_2.newFieldInGroup("pnd_Ads_Ia_Super1_2_Pnd_Ads_Ia_Rcrd_Cde_2", "#ADS-IA-RCRD-CDE-2", 
            FieldType.STRING, 3);
        pnd_Ads_Ia_Super1_2_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr_2 = pnd_Ads_Ia_Super1_2__R_Field_2.newFieldInGroup("pnd_Ads_Ia_Super1_2_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr_2", 
            "#ADS-IA-RCRD-SQNCE-NBR-2", FieldType.NUMERIC, 3);

        pnd_All_Indx = localVariables.newGroupInRecord("pnd_All_Indx", "#ALL-INDX");
        pnd_All_Indx_Pnd_A_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_A_Indx", "#A-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_B_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_B_Indx", "#B-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_C_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_C_Indx", "#C-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Next_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Next_Indx", "#NEXT-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Tiaa_Indx_4 = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Tiaa_Indx_4", "#TIAA-INDX-4", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Tiaa_Indx_5 = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Tiaa_Indx_5", "#TIAA-INDX-5", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Tiaa_Indx_6 = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Tiaa_Indx_6", "#TIAA-INDX-6", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Prod_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Prod_Indx", "#PROD-INDX", FieldType.PACKED_DECIMAL, 3);
        counter = localVariables.newFieldInRecord("counter", "COUNTER", FieldType.PACKED_DECIMAL, 3);
        pnd_Rec_Found = localVariables.newFieldInRecord("pnd_Rec_Found", "#REC-FOUND", FieldType.NUMERIC, 1);
        pnd_Max_Rates = localVariables.newFieldInRecord("pnd_Max_Rates", "#MAX-RATES", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Rslt = localVariables.newFieldInRecord("pnd_Max_Rslt", "#MAX-RSLT", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Ia_Rslt2.reset();

        ldaAdsl762.initializeValues();
        ldaAdsl450.initializeValues();

        localVariables.reset();
        pnd_All_Indx_Pnd_A_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_B_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_C_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Next_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Tiaa_Indx_4.setInitialValue(0);
        pnd_All_Indx_Pnd_Tiaa_Indx_5.setInitialValue(0);
        pnd_All_Indx_Pnd_Tiaa_Indx_6.setInitialValue(0);
        pnd_All_Indx_Pnd_Prod_Indx.setInitialValue(0);
        pnd_Max_Rates.setInitialValue(250);
        pnd_Max_Rslt.setInitialValue(125);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp761() throws Exception
    {
        super("Adsp761");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 79 PS = 60;//Natural: FORMAT ( 1 ) LS = 80 PS = 60;//Natural: FORMAT ( 2 ) LS = 80 PS = 60
                                                                                                                                                                          //Natural: PERFORM READ-ADI-RSLT-RENEWAL-PAYMENT-DATA
        sub_Read_Adi_Rslt_Renewal_Payment_Data();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Rec_Found.equals(1)))                                                                                                                           //Natural: IF #REC-FOUND = 1
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-STANDARD-RENEWAL-PAYMENTS-DATA
            sub_Process_Standard_Renewal_Payments_Data();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM MOVE-STANDARD-RENEWAL-PAYMENT-INTO-GLOBAL
            sub_Move_Standard_Renewal_Payment_Into_Global();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-GRADED-RENEWAL-PAYMENTS-DATA
            sub_Process_Graded_Renewal_Payments_Data();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM MOVE-GRADED-RENEWAL-PAYMENT-INTO-GLOBAL
            sub_Move_Graded_Renewal_Payment_Into_Global();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-ADI-RSLT-RENEWAL-PAYMENT-DATA
        //* **********************************************************************
        //*  THIS PARA WILL SEQUENCE IA PAYMENTS/RENEWAL INFOR FOR STANDARD METHOD
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-STANDARD-RENEWAL-PAYMENTS-DATA
        //*  CURTIS RATE CHANGE 6/03
        //*  R #B-INDX 1  TO 60
        //* *FOR #B-INDX 1  TO 80
        //*  ADDED ADS-IA-RSLT-VIEW FOR UNIQUENESS -
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-STANDARD-RENEWAL-PAYMENT-INTO-GLOBAL
        //*  CURTIS RATE CHANGE 6/03
        //*  R #B-INDX 1 TO 60
        //* *FOR #B-INDX 1 TO 80
        //* *********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-GRADED-RENEWAL-PAYMENTS-DATA
        //*  CURTIS RATE CHANGE 6/03
        //*  R #B-INDX 1   TO 60
        //* *FOR #B-INDX 1   TO 80
        //*  ADDED ADS-IA-RSLT-VIEW FOR UNIQUENESS -
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-GRADED-RENEWAL-PAYMENT-INTO-GLOBAL
        //* *FOR #B-INDX 1 TO 80
    }
    private void sub_Read_Adi_Rslt_Renewal_Payment_Data() throws Exception                                                                                                //Natural: READ-ADI-RSLT-RENEWAL-PAYMENT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rqst_Id.setValue(gdaAdsg870.getAdsg870_Pnd_Nai_Rslt_Rqst_Id());                                                                      //Natural: MOVE #NAI-RSLT-RQST-ID TO #ADS-IA-RQST-ID
        pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde.setValue("PP ");                                                                                                            //Natural: MOVE 'PP ' TO #ADS-IA-RCRD-CDE
        pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr.setValue(999);                                                                                                        //Natural: MOVE 999 TO #ADS-IA-RCRD-SQNCE-NBR
        pnd_Rec_Found.setValue(0);                                                                                                                                        //Natural: MOVE 0 TO #REC-FOUND
        ldaAdsl450.getVw_ads_Ia_Rslt_View().startDatabaseFind                                                                                                             //Natural: FIND ADS-IA-RSLT-VIEW WITH ADI-SUPER1 = #ADS-IA-SUPER1
        (
        "FIND01",
        new Wc[] { new Wc("ADI_SUPER1", "=", pnd_Ads_Ia_Super1, WcType.WITH) }
        );
        FIND01:
        while (condition(ldaAdsl450.getVw_ads_Ia_Rslt_View().readNextRow("FIND01", true)))
        {
            ldaAdsl450.getVw_ads_Ia_Rslt_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAdsl450.getVw_ads_Ia_Rslt_View().getAstCOUNTER().equals(0)))                                                                                 //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, NEWLINE,"RECORD NOT FOUND FOR -",pnd_Ads_Ia_Super1);                                                                                //Natural: WRITE / 'RECORD NOT FOUND FOR -' #ADS-IA-SUPER1
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Rec_Found.setValue(1);                                                                                                                                    //Natural: MOVE 1 TO #REC-FOUND
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  READ THE SECOND IA RSLT RECORD, IF EXISTS /* EM - 030512 START
        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Count_Castadi_Dtl_Tiaa_Data().equals(pnd_Max_Rslt)))                                                                 //Natural: IF ADS-IA-RSLT-VIEW.C*ADI-DTL-TIAA-DATA = #MAX-RSLT
        {
            pnd_Ads_Ia_Super1_2_Pnd_Ads_Ia_Rqst_Id_2.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Rqst_Id());                                                                  //Natural: ASSIGN #ADS-IA-RQST-ID-2 := ADS-IA-RSLT-VIEW.RQST-ID
            pnd_Ads_Ia_Super1_2_Pnd_Ads_Ia_Rcrd_Cde_2.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde());                                                         //Natural: ASSIGN #ADS-IA-RCRD-CDE-2 := ADS-IA-RSLT-VIEW.ADI-IA-RCRD-CDE
            pnd_Ads_Ia_Super1_2_Pnd_Ads_Ia_Rcrd_Cde_2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde(),          //Natural: COMPRESS ADS-IA-RSLT-VIEW.ADI-IA-RCRD-CDE '1' INTO #ADS-IA-RCRD-CDE-2 LEAVING NO SPACE
                "1"));
            pnd_Ads_Ia_Super1_2_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr_2.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr());                                                     //Natural: ASSIGN #ADS-IA-RCRD-SQNCE-NBR-2 := ADS-IA-RSLT-VIEW.ADI-SQNCE-NBR
            vw_ads_Ia_Rslt2.startDatabaseFind                                                                                                                             //Natural: FIND ADS-IA-RSLT2 WITH ADI-SUPER1 = #ADS-IA-SUPER1-2
            (
            "FIND02",
            new Wc[] { new Wc("ADI_SUPER1", "=", pnd_Ads_Ia_Super1_2, WcType.WITH) }
            );
            FIND02:
            while (condition(vw_ads_Ia_Rslt2.readNextRow("FIND02")))
            {
                vw_ads_Ia_Rslt2.setIfNotFoundControlFlag(false);
                ldaAdsl450.getPnd_More().setValue("Y");                                                                                                                   //Natural: ASSIGN #MORE := 'Y'
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
            //*  EM - 030512 END
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Standard_Renewal_Payments_Data() throws Exception                                                                                            //Natural: PROCESS-STANDARD-RENEWAL-PAYMENTS-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        pnd_All_Indx_Pnd_Tiaa_Indx_4.reset();                                                                                                                             //Natural: RESET #TIAA-INDX-4
        FOR01:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #MAX-RSLT
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rslt)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                        //Natural: IF ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) = ' '
            {
                //*    ESCAPE BOTTOM (1250)                       /* 040708
                //*  040708
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_All_Indx_Pnd_Prod_Indx.reset();                                                                                                                           //Natural: RESET #PROD-INDX
            DbsUtil.examine(new ExamineSource(ldaAdsl762.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Std_Payment_Save().getValue("*")), new                     //Natural: EXAMINE #TIAA-IA-RATE-STD-PAYMENT-SAVE ( * ) FOR ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) GIVING INDEX #PROD-INDX
                ExamineSearch(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx)), new ExamineGivingIndex(pnd_All_Indx_Pnd_Prod_Indx));
            if (condition(pnd_All_Indx_Pnd_Prod_Indx.equals(getZero())))                                                                                                  //Natural: IF #PROD-INDX = 0
            {
                pnd_All_Indx_Pnd_Tiaa_Indx_4.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TIAA-INDX-4
                ldaAdsl762.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Std_Payment_Save().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_4).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) TO #TIAA-IA-RATE-STD-PAYMENT-SAVE ( #TIAA-INDX-4 )
                ldaAdsl762.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Gur_Payment_Save().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_4).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GUR-PAYMENT-SAVE ( #TIAA-INDX-4 )
                ldaAdsl762.getPnd_Tiaa_Ia_Std_Guranteed_Final().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx));      //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GURANTEED-FINAL
                gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Gur_Fin_Renew().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GUR-FIN-RENEW
                ldaAdsl762.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Div_Payment_Save().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_4).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIV-PAYMENT-SAVE ( #TIAA-INDX-4 )
                ldaAdsl762.getPnd_Tiaa_Ia_Std_Dividend_Final().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx));       //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIVIDEND-FINAL
                gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Div_Fin_Renew().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIV-FIN-RENEW
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    MOVE ADI-DTL-TIAA-IA-RATE-CD        (#B-INDX)
                //*      TO #TIAA-IA-RATE-STD-PAYMENT-SAVE (#PROD-INDX)
                ldaAdsl762.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Gur_Payment_Save().getValue(pnd_All_Indx_Pnd_Prod_Indx).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GUR-PAYMENT-SAVE ( #PROD-INDX )
                ldaAdsl762.getPnd_Tiaa_Ia_Std_Guranteed_Final().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx));      //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GURANTEED-FINAL
                gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Gur_Fin_Renew().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GUR-FIN-RENEW
                ldaAdsl762.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Div_Payment_Save().getValue(pnd_All_Indx_Pnd_Prod_Indx).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIV-PAYMENT-SAVE ( #PROD-INDX )
                ldaAdsl762.getPnd_Tiaa_Ia_Std_Dividend_Final().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx));       //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIVIDEND-FINAL
                gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Div_Fin_Renew().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIV-FIN-RENEW
            }                                                                                                                                                             //Natural: END-IF
            //*  EM - 030512 END
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  POPULATE DATA FROM THE SECOND IA RSLT RECORD, IF EXISTS
        //*  030512 START
        if (condition(ldaAdsl450.getPnd_More().equals("Y")))                                                                                                              //Natural: IF #MORE = 'Y'
        {
            FOR02:                                                                                                                                                        //Natural: FOR #B-INDX 1 TO ADS-IA-RSLT2.C*ADI-DTL-TIAA-DATA
            for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
            {
                pnd_All_Indx_Pnd_Prod_Indx.reset();                                                                                                                       //Natural: RESET #PROD-INDX
                DbsUtil.examine(new ExamineSource(ldaAdsl762.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Std_Payment_Save().getValue("*")),                     //Natural: EXAMINE #TIAA-IA-RATE-STD-PAYMENT-SAVE ( * ) FOR ADS-IA-RSLT2.ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) GIVING INDEX #PROD-INDX
                    new ExamineSearch(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_All_Indx_Pnd_B_Indx)), new ExamineGivingIndex(pnd_All_Indx_Pnd_Prod_Indx));
                if (condition(pnd_All_Indx_Pnd_Prod_Indx.equals(getZero())))                                                                                              //Natural: IF #PROD-INDX = 0
                {
                    pnd_All_Indx_Pnd_Tiaa_Indx_4.nadd(1);                                                                                                                 //Natural: ADD 1 TO #TIAA-INDX-4
                    ldaAdsl762.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Std_Payment_Save().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_4).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE ADS-IA-RSLT2.ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) TO #TIAA-IA-RATE-STD-PAYMENT-SAVE ( #TIAA-INDX-4 )
                    ldaAdsl762.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Gur_Payment_Save().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_4).nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GUR-PAYMENT-SAVE ( #TIAA-INDX-4 )
                    ldaAdsl762.getPnd_Tiaa_Ia_Std_Guranteed_Final().nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx));                      //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GURANTEED-FINAL
                    gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Gur_Fin_Renew().nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx));                //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GUR-FIN-RENEW
                    ldaAdsl762.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Div_Payment_Save().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_4).nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIV-PAYMENT-SAVE ( #TIAA-INDX-4 )
                    ldaAdsl762.getPnd_Tiaa_Ia_Std_Dividend_Final().nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx));                       //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIVIDEND-FINAL
                    gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Div_Fin_Renew().nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx));                //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIV-FIN-RENEW
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*    MOVE ADI-DTL-TIAA-IA-RATE-CD        (#B-INDX)
                    //*      TO #TIAA-IA-RATE-STD-PAYMENT-SAVE (#PROD-INDX)
                    ldaAdsl762.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Gur_Payment_Save().getValue(pnd_All_Indx_Pnd_Prod_Indx).nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GUR-PAYMENT-SAVE ( #PROD-INDX )
                    ldaAdsl762.getPnd_Tiaa_Ia_Std_Guranteed_Final().nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx));                      //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GURANTEED-FINAL
                    gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Gur_Fin_Renew().nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx));                //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GUR-FIN-RENEW
                    ldaAdsl762.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Div_Payment_Save().getValue(pnd_All_Indx_Pnd_Prod_Indx).nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIV-PAYMENT-SAVE ( #PROD-INDX )
                    ldaAdsl762.getPnd_Tiaa_Ia_Std_Dividend_Final().nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx));                       //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIVIDEND-FINAL
                    gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Div_Fin_Renew().nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx));                //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIV-FIN-RENEW
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //*  EM - 030512 END
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Move_Standard_Renewal_Payment_Into_Global() throws Exception                                                                                         //Natural: MOVE-STANDARD-RENEWAL-PAYMENT-INTO-GLOBAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        //*  040708
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR03:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #MAX-RATES
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rates)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl762.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Std_Payment_Save().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))             //Natural: IF #TIAA-IA-RATE-STD-PAYMENT-SAVE ( #B-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl762.getPnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Std_Payment().setValue(ldaAdsl762.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Std_Payment_Save().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-RATE-STD-PAYMENT-SAVE ( #B-INDX ) TO #TIAA-IA-RATE-STD-PAYMENT
            gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Gur_Pay_Renew().getValue(pnd_All_Indx_Pnd_B_Indx).setValue(ldaAdsl762.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Gur_Payment_Save().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-STD-GUR-PAYMENT-SAVE ( #B-INDX ) TO #TIAA-IA-STD-GUR-PAY-RENEW ( #B-INDX )
            gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Div_Pay_Renew().getValue(pnd_All_Indx_Pnd_B_Indx).setValue(ldaAdsl762.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Div_Payment_Save().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-STD-DIV-PAYMENT-SAVE ( #B-INDX ) TO #TIAA-IA-STD-DIV-PAY-RENEW ( #B-INDX )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Process_Graded_Renewal_Payments_Data() throws Exception                                                                                              //Natural: PROCESS-GRADED-RENEWAL-PAYMENTS-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        //*  040708
        pnd_All_Indx_Pnd_Tiaa_Indx_6.reset();                                                                                                                             //Natural: RESET #TIAA-INDX-6
        FOR04:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #MAX-RSLT
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rslt)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                        //Natural: IF ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_All_Indx_Pnd_Prod_Indx.reset();                                                                                                                           //Natural: RESET #PROD-INDX
            DbsUtil.examine(new ExamineSource(ldaAdsl762.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Grd_Payment_Save().getValue("*")), new                     //Natural: EXAMINE #TIAA-IA-RATE-GRD-PAYMENT-SAVE ( * ) FOR ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) GIVING INDEX #PROD-INDX
                ExamineSearch(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx)), new ExamineGivingIndex(pnd_All_Indx_Pnd_Prod_Indx));
            if (condition(pnd_All_Indx_Pnd_Prod_Indx.equals(getZero())))                                                                                                  //Natural: IF #PROD-INDX = 0
            {
                pnd_All_Indx_Pnd_Tiaa_Indx_6.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TIAA-INDX-6
                ldaAdsl762.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Grd_Payment_Save().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_6).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) TO #TIAA-IA-RATE-GRD-PAYMENT-SAVE ( #TIAA-INDX-6 )
                ldaAdsl762.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Gur_Payment_Save().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_6).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GUR-PAYMENT-SAVE ( #TIAA-INDX-6 )
                ldaAdsl762.getPnd_Tiaa_Ia_Grd_Guranteed_Final().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx));      //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GURANTEED-FINAL
                gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Gur_Fin_Renew().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GUR-FIN-RENEW
                ldaAdsl762.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Div_Payment_Save().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_6).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIV-PAYMENT-SAVE ( #TIAA-INDX-6 )
                ldaAdsl762.getPnd_Tiaa_Ia_Grd_Dividend_Final().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx));       //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIVIDEND-FINAL
                gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Div_Fin_Renew().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIV-FIN-RENEW
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl762.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Grd_Payment_Save().getValue(pnd_All_Indx_Pnd_Prod_Indx).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) TO #TIAA-IA-RATE-GRD-PAYMENT-SAVE ( #PROD-INDX )
                ldaAdsl762.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Gur_Payment_Save().getValue(pnd_All_Indx_Pnd_Prod_Indx).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GUR-PAYMENT-SAVE ( #PROD-INDX )
                ldaAdsl762.getPnd_Tiaa_Ia_Grd_Guranteed_Final().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx));      //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GURANTEED-FINAL
                gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Gur_Fin_Renew().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GUR-FIN-RENEW
                ldaAdsl762.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Div_Payment_Save().getValue(pnd_All_Indx_Pnd_Prod_Indx).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIV-PAYMENT-SAVE ( #PROD-INDX )
                ldaAdsl762.getPnd_Tiaa_Ia_Grd_Dividend_Final().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx));       //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIVIDEND-FINAL
                gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Div_Fin_Renew().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIV-FIN-RENEW
            }                                                                                                                                                             //Natural: END-IF
            //*  EM - 030512 END
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  POPULATE DATA FROM THE SECOND IA RSLT RECORD, IF EXISTS
        //*  EM - 030512 START
        if (condition(ldaAdsl450.getPnd_More().equals("Y")))                                                                                                              //Natural: IF #MORE = 'Y'
        {
            FOR05:                                                                                                                                                        //Natural: FOR #B-INDX 1 TO ADS-IA-RSLT2.C*ADI-DTL-TIAA-DATA
            for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
            {
                pnd_All_Indx_Pnd_Prod_Indx.reset();                                                                                                                       //Natural: RESET #PROD-INDX
                DbsUtil.examine(new ExamineSource(ldaAdsl762.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Grd_Payment_Save().getValue("*")),                     //Natural: EXAMINE #TIAA-IA-RATE-GRD-PAYMENT-SAVE ( * ) FOR ADS-IA-RSLT2.ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) GIVING INDEX #PROD-INDX
                    new ExamineSearch(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_All_Indx_Pnd_B_Indx)), new ExamineGivingIndex(pnd_All_Indx_Pnd_Prod_Indx));
                if (condition(pnd_All_Indx_Pnd_Prod_Indx.equals(getZero())))                                                                                              //Natural: IF #PROD-INDX = 0
                {
                    pnd_All_Indx_Pnd_Tiaa_Indx_6.nadd(1);                                                                                                                 //Natural: ADD 1 TO #TIAA-INDX-6
                    ldaAdsl762.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Grd_Payment_Save().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_6).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE ADS-IA-RSLT2.ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) TO #TIAA-IA-RATE-GRD-PAYMENT-SAVE ( #TIAA-INDX-6 )
                    ldaAdsl762.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Gur_Payment_Save().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_6).nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GUR-PAYMENT-SAVE ( #TIAA-INDX-6 )
                    ldaAdsl762.getPnd_Tiaa_Ia_Grd_Guranteed_Final().nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx));                      //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GURANTEED-FINAL
                    gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Gur_Fin_Renew().nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx));                //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GUR-FIN-RENEW
                    ldaAdsl762.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Div_Payment_Save().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_6).nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIV-PAYMENT-SAVE ( #TIAA-INDX-6 )
                    ldaAdsl762.getPnd_Tiaa_Ia_Grd_Dividend_Final().nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx));                       //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIVIDEND-FINAL
                    gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Div_Fin_Renew().nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx));                //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIV-FIN-RENEW
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaAdsl762.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Grd_Payment_Save().getValue(pnd_All_Indx_Pnd_Prod_Indx).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE ADS-IA-RSLT2.ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) TO #TIAA-IA-RATE-GRD-PAYMENT-SAVE ( #PROD-INDX )
                    ldaAdsl762.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Gur_Payment_Save().getValue(pnd_All_Indx_Pnd_Prod_Indx).nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GUR-PAYMENT-SAVE ( #PROD-INDX )
                    ldaAdsl762.getPnd_Tiaa_Ia_Grd_Guranteed_Final().nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx));                      //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GURANTEED-FINAL
                    gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Gur_Fin_Renew().nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx));                //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GUR-FIN-RENEW
                    ldaAdsl762.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Div_Payment_Save().getValue(pnd_All_Indx_Pnd_Prod_Indx).nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIV-PAYMENT-SAVE ( #PROD-INDX )
                    ldaAdsl762.getPnd_Tiaa_Ia_Grd_Dividend_Final().nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx));                       //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIVIDEND-FINAL
                    gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Div_Fin_Renew().nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_All_Indx_Pnd_B_Indx));                //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIV-FIN-RENEW
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //*  EM - 030512 END
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Move_Graded_Renewal_Payment_Into_Global() throws Exception                                                                                           //Natural: MOVE-GRADED-RENEWAL-PAYMENT-INTO-GLOBAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  040708
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR06:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #MAX-RATES
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rates)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl762.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Grd_Payment_Save().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))             //Natural: IF #TIAA-IA-RATE-GRD-PAYMENT-SAVE ( #B-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl762.getPnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Grd_Payment().setValue(ldaAdsl762.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Grd_Payment_Save().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-RATE-GRD-PAYMENT-SAVE ( #B-INDX ) TO #TIAA-IA-RATE-GRD-PAYMENT
            gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Gur_Pay_Renew().getValue(pnd_All_Indx_Pnd_B_Indx).setValue(ldaAdsl762.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Gur_Payment_Save().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-GRD-GUR-PAYMENT-SAVE ( #B-INDX ) TO #TIAA-IA-GRD-GUR-PAY-RENEW ( #B-INDX )
            gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Div_Pay_Renew().getValue(pnd_All_Indx_Pnd_B_Indx).setValue(ldaAdsl762.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Div_Payment_Save().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-GRD-DIV-PAYMENT-SAVE ( #B-INDX ) TO #TIAA-IA-GRD-DIV-PAY-RENEW ( #B-INDX )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=79 PS=60");
        Global.format(1, "LS=80 PS=60");
        Global.format(2, "LS=80 PS=60");
    }
}
