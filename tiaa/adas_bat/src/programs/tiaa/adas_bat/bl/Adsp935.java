/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:05:45 PM
**        * FROM NATURAL PROGRAM : Adsp935
************************************************************
**        * FILE NAME            : Adsp935.java
**        * CLASS NAME           : Adsp935
**        * INSTANCE NAME        : Adsp935
************************************************************
************************************************************************
* PROGRAM  : ADSP935
* GENERATED: APRIL 16, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : THIS PROGRAM UPDATES THE DAILY CONTROL RECORD
*            (ADS-PNDG-TRN-RPT-DTE)
*
* REMARKS  : CLONED FROM ADSP936
* 07/27/04   T. SHINE       IF MONTH-END PROCESS FALLS ON WEEKEND OR
*                           HOLIDAY, USE ADS-RPT-13 (FACTOR DATE) AS
*                           REPORT DATE.
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp935 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Current_Date;
    private DbsField pnd_Save_Cntl_Business_Date;

    private DbsGroup pnd_Save_Cntl_Business_Date__R_Field_1;
    private DbsField pnd_Save_Cntl_Business_Date_Pnd_Save_Cntl_Business_Date_A;

    private DataAccessProgramView vw_ads_Cntl;

    private DbsGroup ads_Cntl_Ads_Cntl_Grp;
    private DbsField ads_Cntl_Ads_Cntl_Rcrd_Typ_Cde;
    private DbsField ads_Cntl_Ads_Cntl_Bsnss_Rcprcl_Dte;
    private DbsField ads_Cntl_Ads_Cntl_Bsnss_Dte;
    private DbsField ads_Cntl_Ads_Pndg_Trn_Rpt_Dte;
    private DbsField ads_Cntl_Ads_Rpt_13;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_2;
    private DbsField pnd_Date_A_Pnd_Date_N;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Current_Date = localVariables.newFieldInRecord("pnd_Current_Date", "#CURRENT-DATE", FieldType.NUMERIC, 8);
        pnd_Save_Cntl_Business_Date = localVariables.newFieldInRecord("pnd_Save_Cntl_Business_Date", "#SAVE-CNTL-BUSINESS-DATE", FieldType.NUMERIC, 8);

        pnd_Save_Cntl_Business_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Save_Cntl_Business_Date__R_Field_1", "REDEFINE", pnd_Save_Cntl_Business_Date);
        pnd_Save_Cntl_Business_Date_Pnd_Save_Cntl_Business_Date_A = pnd_Save_Cntl_Business_Date__R_Field_1.newFieldInGroup("pnd_Save_Cntl_Business_Date_Pnd_Save_Cntl_Business_Date_A", 
            "#SAVE-CNTL-BUSINESS-DATE-A", FieldType.STRING, 8);

        vw_ads_Cntl = new DataAccessProgramView(new NameInfo("vw_ads_Cntl", "ADS-CNTL"), "ADS_CNTL", "ADS_CNTL");

        ads_Cntl_Ads_Cntl_Grp = vw_ads_Cntl.getRecord().newGroupInGroup("ADS_CNTL_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        ads_Cntl_Ads_Cntl_Rcrd_Typ_Cde = ads_Cntl_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_Ads_Cntl_Rcrd_Typ_Cde", "ADS-CNTL-RCRD-TYP-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADS_CNTL_RCRD_TYP_CDE");
        ads_Cntl_Ads_Cntl_Bsnss_Rcprcl_Dte = ads_Cntl_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_Ads_Cntl_Bsnss_Rcprcl_Dte", "ADS-CNTL-BSNSS-RCPRCL-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_RCPRCL_DTE");
        ads_Cntl_Ads_Cntl_Bsnss_Dte = ads_Cntl_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 8, 
            RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_DTE");
        ads_Cntl_Ads_Pndg_Trn_Rpt_Dte = vw_ads_Cntl.getRecord().newFieldInGroup("ads_Cntl_Ads_Pndg_Trn_Rpt_Dte", "ADS-PNDG-TRN-RPT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADS_PNDG_TRN_RPT_DTE");
        ads_Cntl_Ads_Rpt_13 = vw_ads_Cntl.getRecord().newFieldInGroup("ads_Cntl_Ads_Rpt_13", "ADS-RPT-13", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADS_RPT_13");
        registerRecord(vw_ads_Cntl);

        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_2", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_2.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Cntl.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp935() throws Exception
    {
        super("Adsp935");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //*  MOVE *DATN TO #CURRENT-DATE
        vw_ads_Cntl.startDatabaseRead                                                                                                                                     //Natural: READ ( 2 ) ADS-CNTL BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F00000000'
        (
        "READ01",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F00000000", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        READ01:
        while (condition(vw_ads_Cntl.readNextRow("READ01")))
        {
            if (condition(vw_ads_Cntl.getAstCOUNTER().equals(2)))                                                                                                         //Natural: IF *COUNTER = 2
            {
                if (condition(ads_Cntl_Ads_Rpt_13.notEquals(getZero())))                                                                                                  //Natural: IF ADS-RPT-13 NE 0
                {
                    pnd_Date_A.setValueEdited(ads_Cntl_Ads_Rpt_13,new ReportEditMask("YYYYMMDD"));                                                                        //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE-A
                    pnd_Save_Cntl_Business_Date.setValue(pnd_Date_A_Pnd_Date_N);                                                                                          //Natural: MOVE #DATE-N TO #SAVE-CNTL-BUSINESS-DATE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Save_Cntl_Business_Date.setValue(ads_Cntl_Ads_Cntl_Bsnss_Dte);                                                                                    //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #SAVE-CNTL-BUSINESS-DATE
                }                                                                                                                                                         //Natural: END-IF
                ads_Cntl_Ads_Pndg_Trn_Rpt_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Save_Cntl_Business_Date_Pnd_Save_Cntl_Business_Date_A);                   //Natural: MOVE EDITED #SAVE-CNTL-BUSINESS-DATE-A TO ADS-PNDG-TRN-RPT-DTE ( EM = YYYYMMDD )
                vw_ads_Cntl.updateDBRow("READ01");                                                                                                                        //Natural: UPDATE
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_ads_Cntl.startDatabaseRead                                                                                                                                     //Natural: READ ( 1 ) ADS-CNTL BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F00000000'
        (
        "READ02",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F00000000", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        1
        );
        READ02:
        while (condition(vw_ads_Cntl.readNextRow("READ02")))
        {
            if (condition(vw_ads_Cntl.getAstCOUNTER().equals(1)))                                                                                                         //Natural: IF *COUNTER = 1
            {
                ads_Cntl_Ads_Pndg_Trn_Rpt_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Save_Cntl_Business_Date_Pnd_Save_Cntl_Business_Date_A);                   //Natural: MOVE EDITED #SAVE-CNTL-BUSINESS-DATE-A TO ADS-PNDG-TRN-RPT-DTE ( EM = YYYYMMDD )
                vw_ads_Cntl.updateDBRow("READ02");                                                                                                                        //Natural: UPDATE
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
