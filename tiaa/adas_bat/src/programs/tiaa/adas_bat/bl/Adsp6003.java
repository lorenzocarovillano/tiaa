/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:03:47 PM
**        * FROM NATURAL PROGRAM : Adsp6003
************************************************************
**        * FILE NAME            : Adsp6003.java
**        * CLASS NAME           : Adsp6003
**        * INSTANCE NAME        : Adsp6003
************************************************************
*******************************************************************
* THIS PROGRAM UPDATES THE DAILY CONTROL RECORD ADDR CHG IND      *
*   ONLY AFTER THE PREVIOUS JOB HAS RAN SUCCESSFULLLY             *
*
* CHANGES :
* 04/07/04 HK CHANGE THE PROGRAM TO USE THE NEW ADS VIEW FOR OMNI
*             INTERFACE CHANGES.
*
* 07/27/04   T. SHINE       IF MONTH-END PROCESS FALLS ON WEEKEND OR
*                           HOLIDAY, USE ADS-RPT-13 (FACTOR DATE) AS
*                           REPORT DATE.
* 03/17/2008 E.MELNIK   ROTH 403B/401K CHANGES.  RECOMPILED FOR
*                       NEW ADSLCNTL.
*******************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp6003 extends BLNatBase
{
    // Data Areas
    private LdaAdslcntl ldaAdslcntl;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Current_Date;
    private DbsField pnd_Save_Cntl_Business_Date;

    private DbsGroup pnd_Save_Cntl_Business_Date__R_Field_1;
    private DbsField pnd_Save_Cntl_Business_Date_Pnd_Save_Cntl_Business_Date_A;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_2;
    private DbsField pnd_Date_A_Pnd_Date_N;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdslcntl = new LdaAdslcntl();
        registerRecord(ldaAdslcntl);
        registerRecord(ldaAdslcntl.getVw_ads_Cntl_View());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Current_Date = localVariables.newFieldInRecord("pnd_Current_Date", "#CURRENT-DATE", FieldType.NUMERIC, 8);
        pnd_Save_Cntl_Business_Date = localVariables.newFieldInRecord("pnd_Save_Cntl_Business_Date", "#SAVE-CNTL-BUSINESS-DATE", FieldType.NUMERIC, 8);

        pnd_Save_Cntl_Business_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Save_Cntl_Business_Date__R_Field_1", "REDEFINE", pnd_Save_Cntl_Business_Date);
        pnd_Save_Cntl_Business_Date_Pnd_Save_Cntl_Business_Date_A = pnd_Save_Cntl_Business_Date__R_Field_1.newFieldInGroup("pnd_Save_Cntl_Business_Date_Pnd_Save_Cntl_Business_Date_A", 
            "#SAVE-CNTL-BUSINESS-DATE-A", FieldType.STRING, 8);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_2", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_2.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAdslcntl.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp6003() throws Exception
    {
        super("Adsp6003");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //*  MOVE *DATN TO #CURRENT-DATE
        ldaAdslcntl.getVw_ads_Cntl_View().startDatabaseRead                                                                                                               //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F00000000'
        (
        "PND_PND_L0340",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F00000000", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        PND_PND_L0340:
        while (condition(ldaAdslcntl.getVw_ads_Cntl_View().readNextRow("PND_PND_L0340")))
        {
            if (condition(ldaAdslcntl.getVw_ads_Cntl_View().getAstCOUNTER().equals(2)))                                                                                   //Natural: IF *COUNTER ( ##L0340. ) = 2
            {
                if (condition(ldaAdslcntl.getAds_Cntl_View_Ads_Rpt_13().notEquals(getZero())))                                                                            //Natural: IF ADS-RPT-13 NE 0
                {
                    pnd_Date_A.setValueEdited(ldaAdslcntl.getAds_Cntl_View_Ads_Rpt_13(),new ReportEditMask("YYYYMMDD"));                                                  //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE-A
                    pnd_Save_Cntl_Business_Date.setValue(pnd_Date_A_Pnd_Date_N);                                                                                          //Natural: MOVE #DATE-N TO #SAVE-CNTL-BUSINESS-DATE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Save_Cntl_Business_Date.setValue(ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Dte());                                                              //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #SAVE-CNTL-BUSINESS-DATE
                }                                                                                                                                                         //Natural: END-IF
                ldaAdslcntl.getAds_Cntl_View_Ads_Cis_Intrfce_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Save_Cntl_Business_Date_Pnd_Save_Cntl_Business_Date_A); //Natural: MOVE EDITED #SAVE-CNTL-BUSINESS-DATE-A TO ADS-CIS-INTRFCE-DTE ( EM = YYYYMMDD )
                ldaAdslcntl.getVw_ads_Cntl_View().updateDBRow("PND_PND_L0340");                                                                                           //Natural: UPDATE ( ##L0340. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaAdslcntl.getVw_ads_Cntl_View().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F00000000'
        (
        "PND_PND_L0510",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F00000000", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        1
        );
        PND_PND_L0510:
        while (condition(ldaAdslcntl.getVw_ads_Cntl_View().readNextRow("PND_PND_L0510")))
        {
            if (condition(ldaAdslcntl.getVw_ads_Cntl_View().getAstCOUNTER().equals(1)))                                                                                   //Natural: IF *COUNTER ( ##L0510. ) = 1
            {
                ldaAdslcntl.getAds_Cntl_View_Ads_Cis_Intrfce_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Save_Cntl_Business_Date_Pnd_Save_Cntl_Business_Date_A); //Natural: MOVE EDITED #SAVE-CNTL-BUSINESS-DATE-A TO ADS-CIS-INTRFCE-DTE ( EM = YYYYMMDD )
                ldaAdslcntl.getVw_ads_Cntl_View().updateDBRow("PND_PND_L0510");                                                                                           //Natural: UPDATE ( ##L0510. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
