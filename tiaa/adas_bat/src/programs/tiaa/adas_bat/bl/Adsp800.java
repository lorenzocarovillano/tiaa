/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:05:31 PM
**        * FROM NATURAL PROGRAM : Adsp800
************************************************************
**        * FILE NAME            : Adsp800.java
**        * CLASS NAME           : Adsp800
**        * INSTANCE NAME        : Adsp800
************************************************************
************************************************************************
* PROGRAM  : ADSP800
* GENERATED: APRIL 17, 2015
* SYSTEM   : ADAS BATCH (EXECUTED IN PAN2150D)
* PURPOSE  : THIS MODULE EXPANDS THE ADAS FILE FOR RECONNET
*
* REMARKS  : CLONED FROM FAFP4500
**********************  MAINTENANCE LOG ********************************
*  D A T E     PROGRAMMER     D E S C R I P T I O N
*  4/7/2017    E. MELNIK      PIN EXPANSION
*********************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp800 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField in;

    private DbsGroup in__R_Field_1;
    private DbsField in_Part1;
    private DbsField in_Adi_Lst_Actvty_Dte;
    private DbsField in_Adi_Annty_Strt_Dte;
    private DbsField in_Adi_Instllmnt_Dte;
    private DbsField in_Adi_Frst_Pymnt_Due_Dte;
    private DbsField in_Adi_Frst_Ck_Pd_Dte;
    private DbsField in_Adi_Finl_Periodic_Py_Dte;
    private DbsField in_Adi_Finl_Prtl_Py_Dte;
    private DbsField in_Part2;
    private DbsField out;

    private DbsGroup out__R_Field_2;
    private DbsField out_Part1;
    private DbsField out_Adi_Lst_Actvty_Dte;
    private DbsField out_Adi_Annty_Strt_Dte;
    private DbsField out_Adi_Instllmnt_Dte;
    private DbsField out_Adi_Frst_Pymnt_Due_Dte;
    private DbsField out_Adi_Frst_Ck_Pd_Dte;
    private DbsField out_Adi_Finl_Periodic_Py_Dte;
    private DbsField out_Adi_Finl_Prtl_Py_Dte;
    private DbsField out_Part2;
    private DbsField wk_Check_Date;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        in = localVariables.newFieldArrayInRecord("in", "IN", FieldType.STRING, 1, new DbsArrayController(1, 18114));

        in__R_Field_1 = localVariables.newGroupInRecord("in__R_Field_1", "REDEFINE", in);
        in_Part1 = in__R_Field_1.newFieldInGroup("in_Part1", "PART1", FieldType.STRING, 313);
        in_Adi_Lst_Actvty_Dte = in__R_Field_1.newFieldInGroup("in_Adi_Lst_Actvty_Dte", "ADI-LST-ACTVTY-DTE", FieldType.DATE);
        in_Adi_Annty_Strt_Dte = in__R_Field_1.newFieldInGroup("in_Adi_Annty_Strt_Dte", "ADI-ANNTY-STRT-DTE", FieldType.DATE);
        in_Adi_Instllmnt_Dte = in__R_Field_1.newFieldInGroup("in_Adi_Instllmnt_Dte", "ADI-INSTLLMNT-DTE", FieldType.DATE);
        in_Adi_Frst_Pymnt_Due_Dte = in__R_Field_1.newFieldInGroup("in_Adi_Frst_Pymnt_Due_Dte", "ADI-FRST-PYMNT-DUE-DTE", FieldType.DATE);
        in_Adi_Frst_Ck_Pd_Dte = in__R_Field_1.newFieldInGroup("in_Adi_Frst_Ck_Pd_Dte", "ADI-FRST-CK-PD-DTE", FieldType.DATE);
        in_Adi_Finl_Periodic_Py_Dte = in__R_Field_1.newFieldInGroup("in_Adi_Finl_Periodic_Py_Dte", "ADI-FINL-PERIODIC-PY-DTE", FieldType.DATE);
        in_Adi_Finl_Prtl_Py_Dte = in__R_Field_1.newFieldInGroup("in_Adi_Finl_Prtl_Py_Dte", "ADI-FINL-PRTL-PY-DTE", FieldType.DATE);
        in_Part2 = in__R_Field_1.newFieldInGroup("in_Part2", "PART2", FieldType.STRING, 17773);
        out = localVariables.newFieldArrayInRecord("out", "OUT", FieldType.STRING, 1, new DbsArrayController(1, 18142));

        out__R_Field_2 = localVariables.newGroupInRecord("out__R_Field_2", "REDEFINE", out);
        out_Part1 = out__R_Field_2.newFieldInGroup("out_Part1", "PART1", FieldType.STRING, 313);
        out_Adi_Lst_Actvty_Dte = out__R_Field_2.newFieldInGroup("out_Adi_Lst_Actvty_Dte", "ADI-LST-ACTVTY-DTE", FieldType.STRING, 8);
        out_Adi_Annty_Strt_Dte = out__R_Field_2.newFieldInGroup("out_Adi_Annty_Strt_Dte", "ADI-ANNTY-STRT-DTE", FieldType.STRING, 8);
        out_Adi_Instllmnt_Dte = out__R_Field_2.newFieldInGroup("out_Adi_Instllmnt_Dte", "ADI-INSTLLMNT-DTE", FieldType.STRING, 8);
        out_Adi_Frst_Pymnt_Due_Dte = out__R_Field_2.newFieldInGroup("out_Adi_Frst_Pymnt_Due_Dte", "ADI-FRST-PYMNT-DUE-DTE", FieldType.STRING, 8);
        out_Adi_Frst_Ck_Pd_Dte = out__R_Field_2.newFieldInGroup("out_Adi_Frst_Ck_Pd_Dte", "ADI-FRST-CK-PD-DTE", FieldType.STRING, 8);
        out_Adi_Finl_Periodic_Py_Dte = out__R_Field_2.newFieldInGroup("out_Adi_Finl_Periodic_Py_Dte", "ADI-FINL-PERIODIC-PY-DTE", FieldType.STRING, 8);
        out_Adi_Finl_Prtl_Py_Dte = out__R_Field_2.newFieldInGroup("out_Adi_Finl_Prtl_Py_Dte", "ADI-FINL-PRTL-PY-DTE", FieldType.STRING, 8);
        out_Part2 = out__R_Field_2.newFieldInGroup("out_Part2", "PART2", FieldType.STRING, 17773);
        wk_Check_Date = localVariables.newFieldInRecord("wk_Check_Date", "WK-CHECK-DATE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp800() throws Exception
    {
        super("Adsp800");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //*  ----------------------------------------------------------------------
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 IN ( * )
        while (condition(getWorkFiles().read(1, in.getValue("*"))))
        {
            out.getValue("*").reset();                                                                                                                                    //Natural: RESET OUT ( * )
                                                                                                                                                                          //Natural: PERFORM MOVE-FIELDS
            sub_Move_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getWorkFiles().write(2, false, out.getValue("*"));                                                                                                            //Natural: WRITE WORK FILE 2 OUT ( * )
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-FIELDS
    }
    private void sub_Move_Fields() throws Exception                                                                                                                       //Natural: MOVE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        out_Part1.setValue(in_Part1);                                                                                                                                     //Natural: MOVE IN.PART1 TO OUT.PART1
        getReports().write(0, in_Adi_Lst_Actvty_Dte);                                                                                                                     //Natural: WRITE IN.ADI-LST-ACTVTY-DTE
        if (Global.isEscape()) return;
        wk_Check_Date.setValueEdited(in_Adi_Lst_Actvty_Dte,new ReportEditMask("YYYYMMDD"));                                                                               //Natural: MOVE EDITED IN.ADI-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO WK-CHECK-DATE
        out_Adi_Lst_Actvty_Dte.setValue(wk_Check_Date);                                                                                                                   //Natural: MOVE WK-CHECK-DATE TO OUT.ADI-LST-ACTVTY-DTE
        getReports().write(0, in_Adi_Lst_Actvty_Dte);                                                                                                                     //Natural: WRITE IN.ADI-LST-ACTVTY-DTE
        if (Global.isEscape()) return;
        getReports().write(0, out_Adi_Lst_Actvty_Dte);                                                                                                                    //Natural: WRITE OUT.ADI-LST-ACTVTY-DTE
        if (Global.isEscape()) return;
        wk_Check_Date.setValueEdited(in_Adi_Lst_Actvty_Dte,new ReportEditMask("YYYYMMDD"));                                                                               //Natural: MOVE EDITED IN.ADI-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO WK-CHECK-DATE
        out_Adi_Lst_Actvty_Dte.setValue(wk_Check_Date);                                                                                                                   //Natural: MOVE WK-CHECK-DATE TO OUT.ADI-LST-ACTVTY-DTE
        wk_Check_Date.setValueEdited(in_Adi_Instllmnt_Dte,new ReportEditMask("YYYYMMDD"));                                                                                //Natural: MOVE EDITED IN.ADI-INSTLLMNT-DTE ( EM = YYYYMMDD ) TO WK-CHECK-DATE
        out_Adi_Instllmnt_Dte.setValue(wk_Check_Date);                                                                                                                    //Natural: MOVE WK-CHECK-DATE TO OUT.ADI-INSTLLMNT-DTE
        wk_Check_Date.setValueEdited(in_Adi_Frst_Pymnt_Due_Dte,new ReportEditMask("YYYYMMDD"));                                                                           //Natural: MOVE EDITED IN.ADI-FRST-PYMNT-DUE-DTE ( EM = YYYYMMDD ) TO WK-CHECK-DATE
        out_Adi_Frst_Pymnt_Due_Dte.setValue(wk_Check_Date);                                                                                                               //Natural: MOVE WK-CHECK-DATE TO OUT.ADI-FRST-PYMNT-DUE-DTE
        wk_Check_Date.setValueEdited(in_Adi_Frst_Ck_Pd_Dte,new ReportEditMask("YYYYMMDD"));                                                                               //Natural: MOVE EDITED IN.ADI-FRST-CK-PD-DTE ( EM = YYYYMMDD ) TO WK-CHECK-DATE
        out_Adi_Frst_Ck_Pd_Dte.setValue(wk_Check_Date);                                                                                                                   //Natural: MOVE WK-CHECK-DATE TO OUT.ADI-FRST-CK-PD-DTE
        wk_Check_Date.setValueEdited(in_Adi_Finl_Periodic_Py_Dte,new ReportEditMask("YYYYMMDD"));                                                                         //Natural: MOVE EDITED IN.ADI-FINL-PERIODIC-PY-DTE ( EM = YYYYMMDD ) TO WK-CHECK-DATE
        out_Adi_Finl_Periodic_Py_Dte.setValue(wk_Check_Date);                                                                                                             //Natural: MOVE WK-CHECK-DATE TO OUT.ADI-FINL-PERIODIC-PY-DTE
        if (condition(in_Adi_Finl_Prtl_Py_Dte.equals(getZero())))                                                                                                         //Natural: IF IN.ADI-FINL-PRTL-PY-DTE = 0
        {
            out_Adi_Finl_Prtl_Py_Dte.setValue("19800101");                                                                                                                //Natural: MOVE '19800101' TO OUT.ADI-FINL-PRTL-PY-DTE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            wk_Check_Date.setValueEdited(in_Adi_Finl_Prtl_Py_Dte,new ReportEditMask("YYYYMMDD"));                                                                         //Natural: MOVE EDITED IN.ADI-FINL-PRTL-PY-DTE ( EM = YYYYMMDD ) TO WK-CHECK-DATE
            out_Adi_Finl_Prtl_Py_Dte.setValue(wk_Check_Date);                                                                                                             //Natural: MOVE WK-CHECK-DATE TO OUT.ADI-FINL-PRTL-PY-DTE
        }                                                                                                                                                                 //Natural: END-IF
        out_Part2.setValue(in_Part2);                                                                                                                                     //Natural: MOVE IN.PART2 TO OUT.PART2
    }

    //
}
