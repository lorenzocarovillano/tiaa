/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:05:20 PM
**        * FROM NATURAL PROGRAM : Adsp780
************************************************************
**        * FILE NAME            : Adsp780.java
**        * CLASS NAME           : Adsp780
**        * INSTANCE NAME        : Adsp780
************************************************************
************************************************************************
* PROGRAM  : ADSP780
* GENERATED: APRIL 15, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : DAILY ACTIVITY REPORT - MATURITIES AND RTB
*            PROGRAM WILL READ WORKFILE TAD.P05.PVT.ADSP710 AND REPORTS
*            THE DA TO IA MATURITIES AND RTB IN EFFECTIVE DATE SEQUENCE
*
* REMARKS  : CLONED FROM NAZP780 (ADAM SYSTEM)
************************************************************************
* MOD DATE   MOD BY     DESCRIPTION OF CHANGES
* 02/20/98  ZAFAR KHAN  RTB ACCUMULATIONS WILL BE ADDED IN ANNUAL COUNTS
* 05/28/98  ZAFAR KHAN   ROTH/CLASSIC ROLLOVER PROCESS ADDED
* 11/13/00  FAZAL TWAHIR TPA / IO CHANGES MARKED WITH M001
* 10/12/01  FAZAL TWAHIR BUCKET IO/ TPA ACCUMS ONLY (NO INT-ROLLOVER)
* 01/26/04  OSCAR SOTTO  99 RATES CHANGES. SCAN FOR 012604.
* 04/15/04  C. AVE       MODIFIED ADAS (ANNUITIZATION SUNGUARD)
* 07/27/04  T. SHINE       IF MONTH-END PROCESS FALLS ON WEEKEND OR
*                          HOLIDAY, USE ADS-RPT-13 (FACTOR DATE) AS
*                          REPORT DATE.
* 05/06/08  GUERRERO     CHECKED ROTH IND ON WORK FILE TO PRINT ROTH RPT
* 12/16/08  SACHARNY     ADD 'Stable Return' & 'Access' FUNDS RS0
* 04/12/10  C.MASON      RESTOW DUE TO LDA CAHNGE
* 05/20/10  D.E.ANDER    ADD GRADED AND STANDARD INFO MARKED DEA
* 01/04/11  O. SOTTO     ASSIGN A NEW FIELD TO REFORMAT EFFECTIVE
*                        DATE IN ORDER NOT TO OVERLAY #DATE-A WHICH
*                        CONTAINS ADS-RPT-13 VALUE. SC 010411.
* 03/06/12   E. MELNIK RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                      UPDATED LINKAGE AREAS.  CHANGES MARKED BY
*                      EM - 030612.
* 01/18/13   E. MELNIK TNG PROJECT.  RESTOW TO INCLUDE UPDATED LINKAGE
*                      AREAS.
* 10/23/13  E. MELNIK CREF/REA REDESIGN CHANGES.
*                     MARKED BY EM - 102313.
* 02/28/2017 R.CARREON RESTOWED FOR PIN EXPANSION 02282017
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp780 extends BLNatBase
{
    // Data Areas
    private LdaAdsl780 ldaAdsl780;
    private LdaAdsl401a ldaAdsl401a;
    private PdaNeca4000 pdaNeca4000;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Rec_Type;

    private DbsGroup pnd_Rec_Type__R_Field_1;
    private DbsField pnd_Rec_Type_Pnd_Filler;
    private DbsField pnd_Rec_Type_Pnd_Z_Indx;
    private DbsField pnd_Alpha_Fund_Cde;

    private DataAccessProgramView vw_ads_Cntl_View;

    private DbsGroup ads_Cntl_View_Ads_Cntl_Grp;
    private DbsField ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte;
    private DbsField ads_Cntl_View_Ads_Daily_Actvty_Dte;
    private DbsField ads_Cntl_View_Ads_Rpt_13;

    private DbsGroup pnd_All_Indx;
    private DbsField pnd_All_Indx_Pnd_A_Indx;
    private DbsField pnd_All_Indx_Pnd_B_Indx;
    private DbsField pnd_All_Indx_Pnd_C_Indx;
    private DbsField pnd_All_Indx_Pnd_Next_Indx;
    private DbsField pnd_All_Indx_Pnd_Prod_Indx;
    private DbsField pnd_All_Indx_Pnd_Loop;
    private DbsField pnd_All_Indx_Pnd_Pos;
    private DbsField pnd_All_Indx_Pnd_Z;
    private DbsField pnd_Dest;
    private DbsField pnd_Roth_Ind;
    private DbsField pnd_Prod_Cde;
    private DbsField pnd_Da_Count;
    private DbsField pnd_Rlvr_Amt;
    private DbsField pnd_Cntl_Isn;
    private DbsField pnd_Total_Number_Rec;
    private DbsField pnd_Total_Number_Rec_Roth;
    private DbsField pnd_First_Time;
    private DbsField pnd_Tiaa_Prod_Code;
    private DbsField pnd_Cref_Prod_Found;
    private DbsField pnd_Nap_Annty_Optn;
    private DbsField pnd_Roth_Only_Run;
    private DbsField pnd_Stbl_Fund;
    private DbsField pnd_Svsaa_Fund;
    private DbsField pnd_Max_Rate;
    private DbsField pnd_Prev_Bsnss_Dte_N;

    private DbsGroup pnd_Prev_Bsnss_Dte_N__R_Field_2;
    private DbsField pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A;
    private DbsField pnd_Prev_Bsnss_Dte_D;
    private DbsField pnd_Date_Test;
    private DbsField pnd_Work_Business_Date;

    private DbsGroup pnd_Work_Business_Date__R_Field_3;
    private DbsField pnd_Work_Business_Date_Pnd_Work_Business_Date_Cc;
    private DbsField pnd_Work_Business_Date_Pnd_Work_Business_Date_Yy;
    private DbsField pnd_Work_Business_Date_Pnd_Work_Business_Date_Mm;
    private DbsField pnd_Work_Business_Date_Pnd_Work_Business_Date_Dd;
    private DbsField pnd_Today_Business_Date;

    private DbsGroup pnd_Today_Business_Date__R_Field_4;
    private DbsField pnd_Today_Business_Date_Pnd_Today_Business_Date_Mm;
    private DbsField pnd_Today_Business_Date_Pnd_Today_Business_Date_Dd;
    private DbsField pnd_Today_Business_Date_Pnd_Today_Business_Date_Yy;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd;

    private DbsGroup pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_5;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd;
    private DbsField pnd_Work_Accounting_Dte_Yyyymmdd;

    private DbsGroup pnd_Work_Accounting_Dte_Yyyymmdd__R_Field_6;
    private DbsField pnd_Work_Accounting_Dte_Yyyymmdd_Pnd_Work_Accounting_Dte;
    private DbsField pnd_Today_Accounting_Dte_Mmddccyy;

    private DbsGroup pnd_Today_Accounting_Dte_Mmddccyy__R_Field_7;
    private DbsField pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Mm;
    private DbsField pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Dd;
    private DbsField pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Cc;
    private DbsField pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Yy;
    private DbsField pnd_Rqst_Id;

    private DbsGroup pnd_Rqst_Id__R_Field_8;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Unique_Id;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte;

    private DbsGroup pnd_Rqst_Id__R_Field_9;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Cc;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Yy;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Mm;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Dd;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Entry_Dte;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Entry_Tme;
    private DbsField pnd_Prev_Effctv_Dte;
    private DbsField pnd_Prev_Effctv_Dte_A;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_10;
    private DbsField pnd_Date_A_Pnd_Date_N;
    private DbsField pnd_W_Date_A;

    private DbsGroup pnd_W_Date_A__R_Field_11;
    private DbsField pnd_W_Date_A_Pnd_Date_Cc;
    private DbsField pnd_W_Date_A_Pnd_Date_Yy;
    private DbsField pnd_W_Date_A_Pnd_Date_Mm;
    private DbsField pnd_W_Date_A_Pnd_Date_Dd;
    private DbsField pnd_Pec_Nbr_Active_Acct;
    private DbsField pnd_Heading;
    private DbsField pnd_Adp_Effctv_Date;
    private DbsField pnd_Adp_Effctv_Date_A;
    private DbsField pnd_J;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl780 = new LdaAdsl780();
        registerRecord(ldaAdsl780);
        ldaAdsl401a = new LdaAdsl401a();
        registerRecord(ldaAdsl401a);
        registerRecord(ldaAdsl401a.getVw_ads_Cntrct_View());
        localVariables = new DbsRecord();
        pdaNeca4000 = new PdaNeca4000(localVariables);

        // Local Variables
        pnd_Rec_Type = localVariables.newFieldInRecord("pnd_Rec_Type", "#REC-TYPE", FieldType.NUMERIC, 3);

        pnd_Rec_Type__R_Field_1 = localVariables.newGroupInRecord("pnd_Rec_Type__R_Field_1", "REDEFINE", pnd_Rec_Type);
        pnd_Rec_Type_Pnd_Filler = pnd_Rec_Type__R_Field_1.newFieldInGroup("pnd_Rec_Type_Pnd_Filler", "#FILLER", FieldType.NUMERIC, 2);
        pnd_Rec_Type_Pnd_Z_Indx = pnd_Rec_Type__R_Field_1.newFieldInGroup("pnd_Rec_Type_Pnd_Z_Indx", "#Z-INDX", FieldType.NUMERIC, 1);
        pnd_Alpha_Fund_Cde = localVariables.newFieldArrayInRecord("pnd_Alpha_Fund_Cde", "#ALPHA-FUND-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));

        vw_ads_Cntl_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntl_View", "ADS-CNTL-VIEW"), "ADS_CNTL", "ADS_CNTL");

        ads_Cntl_View_Ads_Cntl_Grp = vw_ads_Cntl_View.getRecord().newGroupInGroup("ADS_CNTL_VIEW_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde", "ADS-CNTL-RCRD-TYP-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADS_CNTL_RCRD_TYP_CDE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte", "ADS-CNTL-BSNSS-RCPRCL-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_RCPRCL_DTE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_DTE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte", "ADS-CNTL-BSNSS-TMRRW-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_TMRRW_DTE");
        ads_Cntl_View_Ads_Daily_Actvty_Dte = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Daily_Actvty_Dte", "ADS-DAILY-ACTVTY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADS_DAILY_ACTVTY_DTE");
        ads_Cntl_View_Ads_Rpt_13 = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Rpt_13", "ADS-RPT-13", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADS_RPT_13");
        registerRecord(vw_ads_Cntl_View);

        pnd_All_Indx = localVariables.newGroupInRecord("pnd_All_Indx", "#ALL-INDX");
        pnd_All_Indx_Pnd_A_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_A_Indx", "#A-INDX", FieldType.PACKED_DECIMAL, 4);
        pnd_All_Indx_Pnd_B_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_B_Indx", "#B-INDX", FieldType.PACKED_DECIMAL, 4);
        pnd_All_Indx_Pnd_C_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_C_Indx", "#C-INDX", FieldType.PACKED_DECIMAL, 4);
        pnd_All_Indx_Pnd_Next_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Next_Indx", "#NEXT-INDX", FieldType.PACKED_DECIMAL, 4);
        pnd_All_Indx_Pnd_Prod_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Prod_Indx", "#PROD-INDX", FieldType.PACKED_DECIMAL, 4);
        pnd_All_Indx_Pnd_Loop = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Loop", "#LOOP", FieldType.PACKED_DECIMAL, 4);
        pnd_All_Indx_Pnd_Pos = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Pos", "#POS", FieldType.PACKED_DECIMAL, 4);
        pnd_All_Indx_Pnd_Z = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Z", "#Z", FieldType.NUMERIC, 2);
        pnd_Dest = localVariables.newFieldArrayInRecord("pnd_Dest", "#DEST", FieldType.STRING, 4, new DbsArrayController(1, 3));
        pnd_Roth_Ind = localVariables.newFieldInRecord("pnd_Roth_Ind", "#ROTH-IND", FieldType.STRING, 1);
        pnd_Prod_Cde = localVariables.newFieldArrayInRecord("pnd_Prod_Cde", "#PROD-CDE", FieldType.STRING, 1, new DbsArrayController(1, 20));
        pnd_Da_Count = localVariables.newFieldArrayInRecord("pnd_Da_Count", "#DA-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Rlvr_Amt = localVariables.newFieldArrayInRecord("pnd_Rlvr_Amt", "#RLVR-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20));
        pnd_Cntl_Isn = localVariables.newFieldInRecord("pnd_Cntl_Isn", "#CNTL-ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Total_Number_Rec = localVariables.newFieldInRecord("pnd_Total_Number_Rec", "#TOTAL-NUMBER-REC", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Number_Rec_Roth = localVariables.newFieldInRecord("pnd_Total_Number_Rec_Roth", "#TOTAL-NUMBER-REC-ROTH", FieldType.PACKED_DECIMAL, 7);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Prod_Code = localVariables.newFieldInRecord("pnd_Tiaa_Prod_Code", "#TIAA-PROD-CODE", FieldType.BOOLEAN, 1);
        pnd_Cref_Prod_Found = localVariables.newFieldInRecord("pnd_Cref_Prod_Found", "#CREF-PROD-FOUND", FieldType.BOOLEAN, 1);
        pnd_Nap_Annty_Optn = localVariables.newFieldInRecord("pnd_Nap_Annty_Optn", "#NAP-ANNTY-OPTN", FieldType.STRING, 2);
        pnd_Roth_Only_Run = localVariables.newFieldInRecord("pnd_Roth_Only_Run", "#ROTH-ONLY-RUN", FieldType.BOOLEAN, 1);
        pnd_Stbl_Fund = localVariables.newFieldInRecord("pnd_Stbl_Fund", "#STBL-FUND", FieldType.BOOLEAN, 1);
        pnd_Svsaa_Fund = localVariables.newFieldInRecord("pnd_Svsaa_Fund", "#SVSAA-FUND", FieldType.BOOLEAN, 1);
        pnd_Max_Rate = localVariables.newFieldInRecord("pnd_Max_Rate", "#MAX-RATE", FieldType.PACKED_DECIMAL, 3);
        pnd_Prev_Bsnss_Dte_N = localVariables.newFieldInRecord("pnd_Prev_Bsnss_Dte_N", "#PREV-BSNSS-DTE-N", FieldType.NUMERIC, 8);

        pnd_Prev_Bsnss_Dte_N__R_Field_2 = localVariables.newGroupInRecord("pnd_Prev_Bsnss_Dte_N__R_Field_2", "REDEFINE", pnd_Prev_Bsnss_Dte_N);
        pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A = pnd_Prev_Bsnss_Dte_N__R_Field_2.newFieldInGroup("pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A", "#PREV-BSNSS-DTE-A", 
            FieldType.STRING, 8);
        pnd_Prev_Bsnss_Dte_D = localVariables.newFieldInRecord("pnd_Prev_Bsnss_Dte_D", "#PREV-BSNSS-DTE-D", FieldType.DATE);
        pnd_Date_Test = localVariables.newFieldInRecord("pnd_Date_Test", "#DATE-TEST", FieldType.NUMERIC, 8);
        pnd_Work_Business_Date = localVariables.newFieldInRecord("pnd_Work_Business_Date", "#WORK-BUSINESS-DATE", FieldType.STRING, 8);

        pnd_Work_Business_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Work_Business_Date__R_Field_3", "REDEFINE", pnd_Work_Business_Date);
        pnd_Work_Business_Date_Pnd_Work_Business_Date_Cc = pnd_Work_Business_Date__R_Field_3.newFieldInGroup("pnd_Work_Business_Date_Pnd_Work_Business_Date_Cc", 
            "#WORK-BUSINESS-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Work_Business_Date_Pnd_Work_Business_Date_Yy = pnd_Work_Business_Date__R_Field_3.newFieldInGroup("pnd_Work_Business_Date_Pnd_Work_Business_Date_Yy", 
            "#WORK-BUSINESS-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Work_Business_Date_Pnd_Work_Business_Date_Mm = pnd_Work_Business_Date__R_Field_3.newFieldInGroup("pnd_Work_Business_Date_Pnd_Work_Business_Date_Mm", 
            "#WORK-BUSINESS-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Work_Business_Date_Pnd_Work_Business_Date_Dd = pnd_Work_Business_Date__R_Field_3.newFieldInGroup("pnd_Work_Business_Date_Pnd_Work_Business_Date_Dd", 
            "#WORK-BUSINESS-DATE-DD", FieldType.NUMERIC, 2);
        pnd_Today_Business_Date = localVariables.newFieldInRecord("pnd_Today_Business_Date", "#TODAY-BUSINESS-DATE", FieldType.STRING, 8);

        pnd_Today_Business_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Today_Business_Date__R_Field_4", "REDEFINE", pnd_Today_Business_Date);
        pnd_Today_Business_Date_Pnd_Today_Business_Date_Mm = pnd_Today_Business_Date__R_Field_4.newFieldInGroup("pnd_Today_Business_Date_Pnd_Today_Business_Date_Mm", 
            "#TODAY-BUSINESS-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Today_Business_Date_Pnd_Today_Business_Date_Dd = pnd_Today_Business_Date__R_Field_4.newFieldInGroup("pnd_Today_Business_Date_Pnd_Today_Business_Date_Dd", 
            "#TODAY-BUSINESS-DATE-DD", FieldType.NUMERIC, 2);
        pnd_Today_Business_Date_Pnd_Today_Business_Date_Yy = pnd_Today_Business_Date__R_Field_4.newFieldInGroup("pnd_Today_Business_Date_Pnd_Today_Business_Date_Yy", 
            "#TODAY-BUSINESS-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd = localVariables.newFieldInRecord("pnd_Prev_Accounting_Dte_Yyyymmdd", "#PREV-ACCOUNTING-DTE-YYYYMMDD", FieldType.STRING, 
            8);

        pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_5 = localVariables.newGroupInRecord("pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_5", "REDEFINE", pnd_Prev_Accounting_Dte_Yyyymmdd);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_5.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc", 
            "#PREV-ACCOUNT-DTE-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_5.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy", 
            "#PREV-ACCOUNT-DTE-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_5.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm", 
            "#PREV-ACCOUNT-DTE-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_5.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd", 
            "#PREV-ACCOUNT-DTE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Work_Accounting_Dte_Yyyymmdd = localVariables.newFieldInRecord("pnd_Work_Accounting_Dte_Yyyymmdd", "#WORK-ACCOUNTING-DTE-YYYYMMDD", FieldType.STRING, 
            8);

        pnd_Work_Accounting_Dte_Yyyymmdd__R_Field_6 = localVariables.newGroupInRecord("pnd_Work_Accounting_Dte_Yyyymmdd__R_Field_6", "REDEFINE", pnd_Work_Accounting_Dte_Yyyymmdd);
        pnd_Work_Accounting_Dte_Yyyymmdd_Pnd_Work_Accounting_Dte = pnd_Work_Accounting_Dte_Yyyymmdd__R_Field_6.newFieldInGroup("pnd_Work_Accounting_Dte_Yyyymmdd_Pnd_Work_Accounting_Dte", 
            "#WORK-ACCOUNTING-DTE", FieldType.NUMERIC, 8);
        pnd_Today_Accounting_Dte_Mmddccyy = localVariables.newFieldInRecord("pnd_Today_Accounting_Dte_Mmddccyy", "#TODAY-ACCOUNTING-DTE-MMDDCCYY", FieldType.STRING, 
            10);

        pnd_Today_Accounting_Dte_Mmddccyy__R_Field_7 = localVariables.newGroupInRecord("pnd_Today_Accounting_Dte_Mmddccyy__R_Field_7", "REDEFINE", pnd_Today_Accounting_Dte_Mmddccyy);
        pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Mm = pnd_Today_Accounting_Dte_Mmddccyy__R_Field_7.newFieldInGroup("pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Mm", 
            "#TODAY-ACCOUNTING-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Dd = pnd_Today_Accounting_Dte_Mmddccyy__R_Field_7.newFieldInGroup("pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Dd", 
            "#TODAY-ACCOUNTING-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Cc = pnd_Today_Accounting_Dte_Mmddccyy__R_Field_7.newFieldInGroup("pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Cc", 
            "#TODAY-ACCOUNTING-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Yy = pnd_Today_Accounting_Dte_Mmddccyy__R_Field_7.newFieldInGroup("pnd_Today_Accounting_Dte_Mmddccyy_Pnd_Today_Accounting_Dte_Yy", 
            "#TODAY-ACCOUNTING-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Rqst_Id = localVariables.newFieldInRecord("pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 35);

        pnd_Rqst_Id__R_Field_8 = localVariables.newGroupInRecord("pnd_Rqst_Id__R_Field_8", "REDEFINE", pnd_Rqst_Id);
        pnd_Rqst_Id_Pnd_Nap_Unique_Id = pnd_Rqst_Id__R_Field_8.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Unique_Id", "#NAP-UNIQUE-ID", FieldType.NUMERIC, 12);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte = pnd_Rqst_Id__R_Field_8.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte", "#NAP-EFFCTV-DTE", FieldType.NUMERIC, 
            8);

        pnd_Rqst_Id__R_Field_9 = pnd_Rqst_Id__R_Field_8.newGroupInGroup("pnd_Rqst_Id__R_Field_9", "REDEFINE", pnd_Rqst_Id_Pnd_Nap_Effctv_Dte);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Cc = pnd_Rqst_Id__R_Field_9.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Cc", "#NAP-EFFCTV-DTE-CC", FieldType.NUMERIC, 
            2);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Yy = pnd_Rqst_Id__R_Field_9.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Yy", "#NAP-EFFCTV-DTE-YY", FieldType.NUMERIC, 
            2);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Mm = pnd_Rqst_Id__R_Field_9.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Mm", "#NAP-EFFCTV-DTE-MM", FieldType.NUMERIC, 
            2);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Dd = pnd_Rqst_Id__R_Field_9.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Dd", "#NAP-EFFCTV-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_Rqst_Id_Pnd_Nap_Entry_Dte = pnd_Rqst_Id__R_Field_8.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Entry_Dte", "#NAP-ENTRY-DTE", FieldType.NUMERIC, 8);
        pnd_Rqst_Id_Pnd_Nap_Entry_Tme = pnd_Rqst_Id__R_Field_8.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Entry_Tme", "#NAP-ENTRY-TME", FieldType.NUMERIC, 7);
        pnd_Prev_Effctv_Dte = localVariables.newFieldInRecord("pnd_Prev_Effctv_Dte", "#PREV-EFFCTV-DTE", FieldType.DATE);
        pnd_Prev_Effctv_Dte_A = localVariables.newFieldInRecord("pnd_Prev_Effctv_Dte_A", "#PREV-EFFCTV-DTE-A", FieldType.STRING, 8);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_10 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_10", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_10.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        pnd_W_Date_A = localVariables.newFieldInRecord("pnd_W_Date_A", "#W-DATE-A", FieldType.STRING, 8);

        pnd_W_Date_A__R_Field_11 = localVariables.newGroupInRecord("pnd_W_Date_A__R_Field_11", "REDEFINE", pnd_W_Date_A);
        pnd_W_Date_A_Pnd_Date_Cc = pnd_W_Date_A__R_Field_11.newFieldInGroup("pnd_W_Date_A_Pnd_Date_Cc", "#DATE-CC", FieldType.NUMERIC, 2);
        pnd_W_Date_A_Pnd_Date_Yy = pnd_W_Date_A__R_Field_11.newFieldInGroup("pnd_W_Date_A_Pnd_Date_Yy", "#DATE-YY", FieldType.NUMERIC, 2);
        pnd_W_Date_A_Pnd_Date_Mm = pnd_W_Date_A__R_Field_11.newFieldInGroup("pnd_W_Date_A_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);
        pnd_W_Date_A_Pnd_Date_Dd = pnd_W_Date_A__R_Field_11.newFieldInGroup("pnd_W_Date_A_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);
        pnd_Pec_Nbr_Active_Acct = localVariables.newFieldInRecord("pnd_Pec_Nbr_Active_Acct", "#PEC-NBR-ACTIVE-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Heading = localVariables.newFieldInRecord("pnd_Heading", "#HEADING", FieldType.STRING, 50);
        pnd_Adp_Effctv_Date = localVariables.newFieldInRecord("pnd_Adp_Effctv_Date", "#ADP-EFFCTV-DATE", FieldType.DATE);
        pnd_Adp_Effctv_Date_A = localVariables.newFieldInRecord("pnd_Adp_Effctv_Date_A", "#ADP-EFFCTV-DATE-A", FieldType.STRING, 8);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Cntl_View.reset();

        ldaAdsl780.initializeValues();
        ldaAdsl401a.initializeValues();

        localVariables.reset();
        pnd_All_Indx_Pnd_A_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_B_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_C_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Next_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Prod_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Loop.setInitialValue(0);
        pnd_All_Indx_Pnd_Pos.setInitialValue(0);
        pnd_All_Indx_Pnd_Z.setInitialValue(0);
        pnd_Cntl_Isn.setInitialValue(0);
        pnd_Total_Number_Rec.setInitialValue(0);
        pnd_Total_Number_Rec_Roth.setInitialValue(0);
        pnd_First_Time.setInitialValue(true);
        pnd_Tiaa_Prod_Code.setInitialValue(true);
        pnd_Cref_Prod_Found.setInitialValue(true);
        pnd_Roth_Only_Run.setInitialValue(false);
        pnd_Max_Rate.setInitialValue(250);
        pnd_Pec_Nbr_Active_Acct.setInitialValue(20);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp780() throws Exception
    {
        super("Adsp780");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 79 PS = 60;//Natural: FORMAT ( 1 ) LS = 79 PS = 60;//Natural: FORMAT ( 2 ) LS = 79 PS = 60
        //*  CALLNAT 'ADSN888'   /* NEW EXTERNALIZATION + SEQUENCING OF PROD CDE
        //*   #PAS-EXT-CNTRL-02-WORK-AREA
        //*   #PEC-NBR-ACTIVE-ACCT
        //* * EM - 102313 START - NEW EXTERNALIZATION + SEQUENCING OF PROD CDE
        DbsUtil.callnat(Adsn889.class , getCurrentProcessState(), pnd_Alpha_Fund_Cde.getValue(1,":",20));                                                                 //Natural: CALLNAT 'ADSN889' #ALPHA-FUND-CDE ( 1:20 )
        if (condition(Global.isEscape())) return;
        //* * EM - 102313 END
                                                                                                                                                                          //Natural: PERFORM CONTROL-RECORD-RTN
        sub_Control_Record_Rtn();
        if (condition(Global.isEscape())) {return;}
        //*  M001
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #NAP-ANNTY-OPTN #ADP-EFFCTV-DATE ADS-CNTRCT-VIEW #DA-COUNT ( * ) #RLVR-AMT ( * ) #PROD-CDE ( * ) #DEST ( * ) #ROTH-IND
        while (condition(getWorkFiles().read(1, pnd_Nap_Annty_Optn, pnd_Adp_Effctv_Date, ldaAdsl401a.getVw_ads_Cntrct_View(), pnd_Da_Count.getValue("*"), 
            pnd_Rlvr_Amt.getValue("*"), pnd_Prod_Cde.getValue("*"), pnd_Dest.getValue("*"), pnd_Roth_Ind)))
        {
            pnd_Work_Accounting_Dte_Yyyymmdd.setValueEdited(ldaAdsl401a.getAds_Cntrct_View_Adc_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                          //Natural: MOVE EDITED ADC-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #WORK-ACCOUNTING-DTE-YYYYMMDD
            //*  GG050608
            if (condition(pnd_Roth_Ind.equals("Y")))                                                                                                                      //Natural: IF #ROTH-IND = 'Y'
            {
                pnd_Roth_Only_Run.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #ROTH-ONLY-RUN
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                                 //Natural: IF ADS-RPT-13 NE 0
            {
                if (condition(!(pnd_Work_Accounting_Dte_Yyyymmdd_Pnd_Work_Accounting_Dte.equals(pnd_Date_A_Pnd_Date_N))))                                                 //Natural: ACCEPT IF #WORK-ACCOUNTING-DTE = #DATE-N
                {
                    continue;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(!(pnd_Work_Accounting_Dte_Yyyymmdd_Pnd_Work_Accounting_Dte.equals(ads_Cntl_View_Ads_Cntl_Bsnss_Dte))))                                      //Natural: ACCEPT IF #WORK-ACCOUNTING-DTE = ADS-CNTL-BSNSS-DTE
                {
                    continue;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Adp_Effctv_Date_A.setValueEdited(pnd_Adp_Effctv_Date,new ReportEditMask("YYYYMMDD"));                                                                     //Natural: MOVE EDITED #ADP-EFFCTV-DATE ( EM = YYYYMMDD ) TO #ADP-EFFCTV-DATE-A
            if (condition(pnd_First_Time.getBoolean()))                                                                                                                   //Natural: IF #FIRST-TIME
            {
                pnd_Prev_Effctv_Dte.setValue(pnd_Adp_Effctv_Date);                                                                                                        //Natural: MOVE #ADP-EFFCTV-DATE TO #PREV-EFFCTV-DTE
                pnd_Prev_Effctv_Dte_A.setValue(pnd_Adp_Effctv_Date_A);                                                                                                    //Natural: MOVE #ADP-EFFCTV-DATE-A TO #PREV-EFFCTV-DTE-A
                pnd_First_Time.setValue(false);                                                                                                                           //Natural: MOVE FALSE TO #FIRST-TIME
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Adp_Effctv_Date_A.notEquals(pnd_Prev_Effctv_Dte_A)))                                                                                        //Natural: IF #ADP-EFFCTV-DATE-A NE #PREV-EFFCTV-DTE-A
            {
                                                                                                                                                                          //Natural: PERFORM BREAK-OF-EFFECTIVE-DATE-RTN
                sub_Break_Of_Effective_Date_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Prev_Effctv_Dte.setValue(pnd_Adp_Effctv_Date);                                                                                                        //Natural: MOVE #ADP-EFFCTV-DATE TO #PREV-EFFCTV-DTE
                pnd_Prev_Effctv_Dte_A.setValue(pnd_Adp_Effctv_Date_A);                                                                                                    //Natural: MOVE #ADP-EFFCTV-DATE-A TO #PREV-EFFCTV-DTE-A
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, ldaAdsl401a.getAds_Cntrct_View_Rqst_Id(),pnd_Nap_Annty_Optn,pnd_Dest.getValue(1),ldaAdsl401a.getAds_Cntrct_View_Adc_Lst_Actvty_Dte(),   //Natural: WRITE ADS-CNTRCT-VIEW.RQST-ID #NAP-ANNTY-OPTN #DEST ( 1 ) ADS-CNTRCT-VIEW.ADC-LST-ACTVTY-DTE #ADP-EFFCTV-DATE #ROTH-IND
                pnd_Adp_Effctv_Date,pnd_Roth_Ind);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR01:                                                                                                                                                        //Natural: FOR #A-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
            for (pnd_All_Indx_Pnd_A_Indx.setValue(1); condition(pnd_All_Indx_Pnd_A_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_A_Indx.nadd(1))
            {
                //*  RS0
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).equals("T") || ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).equals("Y"))) //Natural: IF ADC-ACCT-CDE ( #A-INDX ) = 'T' OR ADC-ACCT-CDE ( #A-INDX ) = 'Y'
                {
                    pnd_Total_Number_Rec.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-NUMBER-REC
                    //*  DEA
                    pnd_Svsaa_Fund.setValue(false);                                                                                                                       //Natural: MOVE FALSE TO #SVSAA-FUND #STBL-FUND
                    pnd_Stbl_Fund.setValue(false);
                    //*  RS0
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).equals("Y")))                                           //Natural: IF ADC-ACCT-CDE ( #A-INDX ) = 'Y'
                    {
                        pnd_Stbl_Fund.setValue(true);                                                                                                                     //Natural: MOVE TRUE TO #STBL-FUND
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //* * EM - 102313 START
                        //*        EXAMINE ADC-T395-FUND-ID(*) FOR 'SA' GIVING #J
                        DbsUtil.examine(new ExamineSource(ldaAdsl401a.getAds_Cntrct_View_Adc_Invstmnt_Grpng_Id().getValue("*")), new ExamineSearch("TSV0"),               //Natural: EXAMINE ADC-INVSTMNT-GRPNG-ID ( * ) FOR 'TSV0' GIVING #J
                            new ExamineGivingNumber(pnd_J));
                        //* * EM - 102313 END
                        //*  DEA
                        if (condition(pnd_J.notEquals(getZero())))                                                                                                        //Natural: IF #J NE 0
                        {
                            //*  DEA
                            pnd_Svsaa_Fund.setValue(true);                                                                                                                //Natural: MOVE TRUE TO #SVSAA-FUND
                            //*  DEA
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-ACCUMULATIONS
                    sub_Calculate_Tiaa_Accumulations();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Total_Number_Rec.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-NUMBER-REC
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-PRODUCT-ACCUMULATION
                    sub_Calculate_Cref_Product_Accumulation();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //* * EM - 102313 START
                    //*      EXAMINE #PEC-ACCT-NAME-1 (*) FOR #PROD-CDE (#A-INDX)
                    DbsUtil.examine(new ExamineSource(pnd_Alpha_Fund_Cde.getValue("*")), new ExamineSearch(pnd_Prod_Cde.getValue(pnd_All_Indx_Pnd_A_Indx)),               //Natural: EXAMINE #ALPHA-FUND-CDE ( * ) FOR #PROD-CDE ( #A-INDX ) GIVING INDEX #POS
                        new ExamineGivingIndex(pnd_All_Indx_Pnd_Pos));
                    //* * EM - 102313 END
                    if (condition(pnd_All_Indx_Pnd_Pos.greater(getZero())))                                                                                               //Natural: IF #POS > 0
                    {
                                                                                                                                                                          //Natural: PERFORM BUILT-ROLLOVER-TOTALS
                        sub_Built_Rollover_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  GG050608
                if (condition(pnd_Roth_Ind.equals("Y")))                                                                                                                  //Natural: IF #ROTH-IND = 'Y'
                {
                    pnd_Total_Number_Rec_Roth.nadd(1);                                                                                                                    //Natural: ADD 1 TO #TOTAL-NUMBER-REC-ROTH
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  GG050608
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 );//Natural: AT TOP OF PAGE ( 2 )
                                                                                                                                                                          //Natural: PERFORM BREAK-OF-EFFECTIVE-DATE-RTN
        sub_Break_Of_Effective_Date_Rtn();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BREAK-OF-ACCOUNTING-DATE-RTN
        sub_Break_Of_Accounting_Date_Rtn();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB-RTN
        sub_End_Of_Job_Rtn();
        if (condition(Global.isEscape())) {return;}
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONTROL-RECORD-RTN
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TIAA-ACCUMULATIONS
        //* *********************************
        //* *R #B-INDX 1   TO 60
        //*  CURTIS NEW CODES EGGTRA
        //*  M001
        //* *****************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUCKET-TIAA-STANDARD-ACCUMULATION
        //*  M001
        //* *****************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUCKET-IPRO-STANDARD-ACCUMULATION
        //*  M001
        //* *****************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUCKET-IPRO-ROLLOVER-ACCUMULATION
        //*  M001
        //* *****************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUCKET-TPA-STANDARD-ACCUMULATION
        //* *****************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUCKET-TIAA-GRADED-ACCUMULATION
        //*  M001
        //* *****************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUCKET-TPA-ROLLOVER-ACCUMULATION
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-CREF-PRODUCT-ACCUMULATION
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILT-ROLLOVER-TOTALS
        //*  CURTIS NEW CODES EGGTRA
        //*            OR = '57BT' OR = '57BC'
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BREAK-OF-ACCOUNTING-DATE-RTN
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BREAK-OF-EFFECTIVE-DATE-RTN
        //*  M001
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BREAK-OF-SETTLEMENT-TYPE
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-JOB-RTN
    }
    private void sub_Control_Record_Rtn() throws Exception                                                                                                                //Natural: CONTROL-RECORD-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        vw_ads_Cntl_View.startDatabaseRead                                                                                                                                //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "READ02",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        READ02:
        while (condition(vw_ads_Cntl_View.readNextRow("READ02")))
        {
            //*  MOVE 20011001 TO ADS-CNTL-BSNSS-DTE
            //*  MOVE 20011002 TO ADS-CNTL-BSNSS-TMRRW-DTE
            if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                                 //Natural: IF ADS-RPT-13 NE 0
            {
                pnd_Date_A.setValueEdited(ads_Cntl_View_Ads_Rpt_13,new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE-A
                pnd_Prev_Bsnss_Dte_N.setValue(pnd_Date_A_Pnd_Date_N);                                                                                                     //Natural: MOVE #DATE-N TO #PREV-BSNSS-DTE-N
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Prev_Bsnss_Dte_N.setValue(ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                                          //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #PREV-BSNSS-DTE-N
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                                 //Natural: IF ADS-RPT-13 NE 0
            {
                pnd_Prev_Accounting_Dte_Yyyymmdd.setValue(pnd_Date_A);                                                                                                    //Natural: MOVE #DATE-A TO #PREV-ACCOUNTING-DTE-YYYYMMDD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Prev_Accounting_Dte_Yyyymmdd.setValue(ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                              //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #PREV-ACCOUNTING-DTE-YYYYMMDD
            }                                                                                                                                                             //Natural: END-IF
            //* *  MOVE ADS-CNTL-BSNSS-TMRRW-DTE TO #PREV-ACCOUNTING-DTE-YYYYMMDD
            ldaAdsl780.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm);                       //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-MM TO #ACCOUNTING-DATE-MCDY-MM
            ldaAdsl780.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd);                       //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-DD TO #ACCOUNTING-DATE-MCDY-DD
            ldaAdsl780.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc);                       //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-CC TO #ACCOUNTING-DATE-MCDY-CC
            ldaAdsl780.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy);                       //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-YY TO #ACCOUNTING-DATE-MCDY-YY
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    //*  012604
    private void sub_Calculate_Tiaa_Accumulations() throws Exception                                                                                                      //Natural: CALCULATE-TIAA-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #MAX-RATE
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rate)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            //*  ROLLOVERS
            //*  BYPASS TPA AND IO
            if (condition(pnd_Nap_Annty_Optn.equals("TC") || pnd_Nap_Annty_Optn.equals("TR") || pnd_Nap_Annty_Optn.equals("TE") || pnd_Nap_Annty_Optn.equals("IO")))      //Natural: IF #NAP-ANNTY-OPTN = 'TC' OR = 'TR' OR = 'TE' OR = 'IO'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Dest.getValue("*").equals("RTHC") || pnd_Dest.getValue("*").equals("RTHT") || pnd_Dest.getValue("*").equals("IRAC")                     //Natural: IF #DEST ( * ) = 'RTHC' OR = 'RTHT' OR = 'IRAC' OR = 'IRAT' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC'
                    || pnd_Dest.getValue("*").equals("IRAT") || pnd_Dest.getValue("*").equals("03BT") || pnd_Dest.getValue("*").equals("03BC") || pnd_Dest.getValue("*").equals("QPLT") 
                    || pnd_Dest.getValue("*").equals("QPLC")))
                {
                    //*            OR = '57BT' OR = '57BC'
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                  //Natural: IF ADC-DTL-TIAA-RATE-CDE ( #B-INDX ) = ' '
                    {
                        ldaAdsl780.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Rtb_Rlvr_Cnt().nadd(pnd_Da_Count.getValue(1));                                               //Natural: ADD #DA-COUNT ( 1 ) TO #TIAA-RATE-STD-RTB-RLVR-CNT
                        ldaAdsl780.getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Rtb_Rlvr_Cnt().nadd(pnd_Da_Count.getValue(1));                                                 //Natural: ADD #DA-COUNT ( 1 ) TO #TIAA-SUB-RATE-RTB-RLVR-CNT
                        ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Rtb_Rlvr_Cnt().nadd(pnd_Da_Count.getValue(1));                                         //Natural: ADD #DA-COUNT ( 1 ) TO #TOTAL-RATE-RTB-RLVR-CNT
                        ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Rlvr_Cnt().nadd(pnd_Da_Count.getValue(1));                                                //Natural: ADD #DA-COUNT ( 1 ) TO #TIAA-GRN-STD-RTB-RLVR-CNT
                        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Rlvr_Cnt().nadd(pnd_Da_Count.getValue(1));                                              //Natural: ADD #DA-COUNT ( 1 ) TO #TIAA-SUB-GRN-RTB-RLVR-CNT
                        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Rtb_Rlvr_Cnt().nadd(pnd_Da_Count.getValue(1));                                         //Natural: ADD #DA-COUNT ( 1 ) TO #GRAND-RATE-RTB-RLVR-CNT
                        ldaAdsl780.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Rtb_Rlvr_Amt().nadd(pnd_Rlvr_Amt.getValue(1));                                                    //Natural: ADD #RLVR-AMT ( 1 ) TO #TIAA-STD-RTB-RLVR-AMT
                        ldaAdsl780.getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rtb_Rlvr_Amt().nadd(pnd_Rlvr_Amt.getValue(1));                                                      //Natural: ADD #RLVR-AMT ( 1 ) TO #TIAA-SUB-RTB-RLVR-AMT
                        ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rtb_Rlvr_Amt().nadd(pnd_Rlvr_Amt.getValue(1));                                              //Natural: ADD #RLVR-AMT ( 1 ) TO #TOTAL-RTB-RLVR-AMT
                        ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Rlvr_Amt().nadd(pnd_Rlvr_Amt.getValue(1));                                                //Natural: ADD #RLVR-AMT ( 1 ) TO #TIAA-GRN-STD-RTB-RLVR-AMT
                        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Rlvr_Amt().nadd(pnd_Rlvr_Amt.getValue(1));                                              //Natural: ADD #RLVR-AMT ( 1 ) TO #TIAA-SUB-GRN-RTB-RLVR-AMT
                        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rtb_Rlvr_Amt().nadd(pnd_Rlvr_Amt.getValue(1));                                              //Natural: ADD #RLVR-AMT ( 1 ) TO #GRAND-RTB-RLVR-AMT
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  RTB ACCUMULATIONS
            //* *  IF ADC-DTL-TIAA-RTB-ACTL-AMT (#B-INDX)  NE 0
            //* *     ADD 1 TO #TIAA-RATE-STD-RTB-CNT
            //* *     ADD 1 TO #TIAA-SUB-RATE-RTB-CNT
            //* *     ADD 1 TO #TOTAL-RATE-RTB-CNT
            //* *     ADD 1 TO #TIAA-GRN-STD-RTB-CNT
            //* *     ADD 1 TO #TIAA-SUB-GRN-RTB-CNT
            //* *     ADD 1 TO #GRAND-RATE-RTB-CNT
            //* *   ADD NAD-DTL-TIAA-RTB-ACTL-AMT (#B-INDX) TO #TIAA-STD-RTB-AMT
            //* *   ADD NAD-DTL-TIAA-RTB-ACTL-AMT (#B-INDX) TO #TIAA-SUB-RTB-AMT
            //* *   ADD NAD-DTL-TIAA-RTB-ACTL-AMT (#B-INDX) TO #TOTAL-RTB-AMT
            //* *   ADD NAD-DTL-TIAA-RTB-ACTL-AMT (#B-INDX) TO #TIAA-GRN-STD-RTB-AMT
            //* *   ADD NAD-DTL-TIAA-RTB-ACTL-AMT (#B-INDX) TO #TIAA-SUB-GRN-RTB-AMT
            //* *   ADD NAD-DTL-TIAA-RTB-ACTL-AMT (#B-INDX) TO #GRAND-RTB-AMT
            //* *  END-IF
            //*  TIAA STANDARD
            //*  M001
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).notEquals(getZero())))                          //Natural: IF ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) NE 0
            {
                if (condition(pnd_Nap_Annty_Optn.equals("TC") || pnd_Nap_Annty_Optn.equals("TR") || pnd_Nap_Annty_Optn.equals("TE")))                                     //Natural: IF #NAP-ANNTY-OPTN = 'TC' OR = 'TR' OR = 'TE'
                {
                    //* **     IF #DEST (1) = 'IRAC' OR = 'IRAT'
                    //* **        PERFORM BUCKET-TPA-ROLLOVER-ACCUMULATION
                    //* **     ELSE
                                                                                                                                                                          //Natural: PERFORM BUCKET-TPA-STANDARD-ACCUMULATION
                    sub_Bucket_Tpa_Standard_Accumulation();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //* **     END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Nap_Annty_Optn.equals("IO")))                                                                                                       //Natural: IF #NAP-ANNTY-OPTN = 'IO'
                    {
                        //* **        IF #DEST (1) = 'IRAC' OR = 'IRAT'
                        //* **           PERFORM BUCKET-IPRO-ROLLOVER-ACCUMULATION
                        //* **        ELSE
                                                                                                                                                                          //Natural: PERFORM BUCKET-IPRO-STANDARD-ACCUMULATION
                        sub_Bucket_Ipro_Standard_Accumulation();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //* **        END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM BUCKET-TIAA-STANDARD-ACCUMULATION
                        sub_Bucket_Tiaa_Standard_Accumulation();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  TIAA GRADED
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).notEquals(getZero())))                             //Natural: IF ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) NE 0
            {
                                                                                                                                                                          //Natural: PERFORM BUCKET-TIAA-GRADED-ACCUMULATION
                sub_Bucket_Tiaa_Graded_Accumulation();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Bucket_Tiaa_Standard_Accumulation() throws Exception                                                                                                 //Natural: BUCKET-TIAA-STANDARD-ACCUMULATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************
        //*  RS0
        if (condition(pnd_Stbl_Fund.getBoolean()))                                                                                                                        //Natural: IF #STBL-FUND
        {
            ldaAdsl780.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Cnt().nadd(1);                                                                                  //Natural: ADD 1 TO #TIAA-STBL-STD-MATURITY-CNT
            ldaAdsl780.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-STBL-STD-MATURITY-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAdsl780.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Maturity_Cnt().nadd(1);                                                                                  //Natural: ADD 1 TO #TIAA-RATE-STD-MATURITY-CNT
            ldaAdsl780.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-STD-MATURITY-AMT
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl780.getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt().nadd(1);                                                                                        //Natural: ADD 1 TO #TIAA-SUB-RATE-MATURITY-CNT
        ldaAdsl780.getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-MATURITY-AMT
        ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #TOTAL-RATE-MATURITY-CNT
        ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TOTAL-MATURITY-AMT
        //*  RS0
        if (condition(pnd_Stbl_Fund.getBoolean()))                                                                                                                        //Natural: IF #STBL-FUND
        {
            ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Cnt().nadd(1);                                                                              //Natural: ADD 1 TO #TIAA-GRN-STBL-STD-MATURITY-CNT
            ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-STBL-STD-MATURITY-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  DEA START
            if (condition(pnd_Svsaa_Fund.getBoolean()))                                                                                                                   //Natural: IF #SVSAA-FUND
            {
                ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Csv().nadd(1);                                                                          //Natural: ADD 1 TO #TIAA-GRN-STBL-STD-MATURITY-CSV
                ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Asv().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-STBL-STD-MATURITY-ASV
                //*  DEA END
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Cnt().nadd(1);                                                                               //Natural: ADD 1 TO #TIAA-GRN-STD-MATURITY-CNT
                ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-STD-MATURITY-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #TIAA-SUB-GRN-MATURITY-CNT
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-GRN-MATURITY-AMT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #GRAND-MATURITY-AMT
    }
    private void sub_Bucket_Ipro_Standard_Accumulation() throws Exception                                                                                                 //Natural: BUCKET-IPRO-STANDARD-ACCUMULATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************
        ldaAdsl780.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Io_Accm_Cnt().nadd(1);                                                                                                //Natural: ADD 1 TO #TIAA-IO-ACCM-CNT
        ldaAdsl780.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Io_Accm_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx));  //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-IO-ACCM-AMT
        ldaAdsl780.getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt().nadd(1);                                                                                        //Natural: ADD 1 TO #TIAA-SUB-RATE-MATURITY-CNT
        ldaAdsl780.getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-MATURITY-AMT
        ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #TOTAL-RATE-MATURITY-CNT
        ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TOTAL-MATURITY-AMT
        ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Io_Accm_Cnt().nadd(1);                                                                                            //Natural: ADD 1 TO #TIAA-GRN-IO-ACCM-CNT
        ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Io_Accm_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-IO-ACCM-AMT
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #TIAA-SUB-GRN-MATURITY-CNT
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-GRN-MATURITY-AMT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #GRAND-MATURITY-AMT
    }
    private void sub_Bucket_Ipro_Rollover_Accumulation() throws Exception                                                                                                 //Natural: BUCKET-IPRO-ROLLOVER-ACCUMULATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************
        ldaAdsl780.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Io_Rlvr_Cnt().nadd(1);                                                                                                //Natural: ADD 1 TO #TIAA-IO-RLVR-CNT
        ldaAdsl780.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Io_Rlvr_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx));  //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-IO-RLVR-AMT
        ldaAdsl780.getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Rtb_Rlvr_Cnt().nadd(1);                                                                                        //Natural: ADD 1 TO #TIAA-SUB-RATE-RTB-RLVR-CNT
        ldaAdsl780.getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rtb_Rlvr_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-RTB-RLVR-AMT
        ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Rtb_Rlvr_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #TOTAL-RATE-RTB-RLVR-CNT
        ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rtb_Rlvr_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TOTAL-RTB-RLVR-AMT
        ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Io_Rlvr_Cnt().nadd(1);                                                                                            //Natural: ADD 1 TO #TIAA-GRN-IO-RLVR-CNT
        ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Io_Rlvr_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-IO-RLVR-AMT
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Rlvr_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #TIAA-SUB-GRN-RTB-RLVR-CNT
        ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Rlvr_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-STD-RTB-RLVR-AMT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Rtb_Rlvr_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #GRAND-RATE-RTB-RLVR-CNT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rtb_Rlvr_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #GRAND-RTB-RLVR-AMT
    }
    private void sub_Bucket_Tpa_Standard_Accumulation() throws Exception                                                                                                  //Natural: BUCKET-TPA-STANDARD-ACCUMULATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************
        ldaAdsl780.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Accm_Cnt().nadd(1);                                                                                               //Natural: ADD 1 TO #TIAA-TPA-ACCM-CNT
        ldaAdsl780.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Accm_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-TPA-ACCM-AMT
        ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #TOTAL-RATE-MATURITY-CNT
        ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TOTAL-MATURITY-AMT
        ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Tpa_Accm_Cnt().nadd(1);                                                                                           //Natural: ADD 1 TO #TIAA-GRN-TPA-ACCM-CNT
        ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Tpa_Accm_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-TPA-ACCM-AMT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #GRAND-MATURITY-AMT
    }
    private void sub_Bucket_Tiaa_Graded_Accumulation() throws Exception                                                                                                   //Natural: BUCKET-TIAA-GRADED-ACCUMULATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************
        //*  RS0
        if (condition(pnd_Stbl_Fund.getBoolean()))                                                                                                                        //Natural: IF #STBL-FUND
        {
            ldaAdsl780.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Cnt().nadd(1);                                                                                  //Natural: ADD 1 TO #TIAA-STBL-GRD-MATURITY-CNT
            ldaAdsl780.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-STBL-GRD-MATURITY-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAdsl780.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Grd_Maturity_Cnt().nadd(1);                                                                                  //Natural: ADD 1 TO #TIAA-RATE-GRD-MATURITY-CNT
            ldaAdsl780.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Grd_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRD-MATURITY-AMT
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl780.getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt().nadd(1);                                                                                        //Natural: ADD 1 TO #TIAA-SUB-RATE-MATURITY-CNT
        ldaAdsl780.getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx));  //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-MATURITY-AMT
        ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #TOTAL-RATE-MATURITY-CNT
        ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TOTAL-MATURITY-AMT
        //*  RS0
        if (condition(pnd_Stbl_Fund.getBoolean()))                                                                                                                        //Natural: IF #STBL-FUND
        {
            ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Cnt().nadd(1);                                                                              //Natural: ADD 1 TO #TIAA-GRN-STBL-GRD-MATURITY-CNT
            ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-STBL-GRD-MATURITY-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  DEA START
            if (condition(pnd_Svsaa_Fund.getBoolean()))                                                                                                                   //Natural: IF #SVSAA-FUND
            {
                ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Csv().nadd(1);                                                                          //Natural: ADD 1 TO #TIAA-GRN-STBL-GRD-MATURITY-CSV
                ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Asv().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-STBL-GRD-MATURITY-ASV
                //*  DEA END
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Cnt().nadd(1);                                                                               //Natural: ADD 1 TO #TIAA-GRN-GRD-MATURITY-CNT
                ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-GRD-MATURITY-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #TIAA-SUB-GRN-MATURITY-CNT
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-GRN-MATURITY-AMT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #GRAND-MATURITY-AMT
    }
    private void sub_Bucket_Tpa_Rollover_Accumulation() throws Exception                                                                                                  //Natural: BUCKET-TPA-ROLLOVER-ACCUMULATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************
        ldaAdsl780.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Rlvr_Cnt().nadd(1);                                                                                               //Natural: ADD 1 TO #TIAA-TPA-RLVR-CNT
        ldaAdsl780.getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Rlvr_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-TPA-RLVR-AMT
        ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Rtb_Rlvr_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #TOTAL-RATE-RTB-RLVR-CNT
        ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rtb_Rlvr_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TOTAL-RTB-RLVR-AMT
        ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Tpa_Rlvr_Cnt().nadd(1);                                                                                           //Natural: ADD 1 TO #TIAA-GRN-TPA-RLVR-CNT
        ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Tpa_Rlvr_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-TPA-RLVR-AMT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Rtb_Rlvr_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #GRAND-RATE-RTB-RLVR-CNT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rtb_Rlvr_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #GRAND-RTB-RLVR-AMT
    }
    private void sub_Calculate_Cref_Product_Accumulation() throws Exception                                                                                               //Natural: CALCULATE-CREF-PRODUCT-ACCUMULATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pnd_Cref_Prod_Found.setValue(false);                                                                                                                              //Natural: MOVE FALSE TO #CREF-PROD-FOUND
        FOR03:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            //*   IF #PEC-ACCT-NAME-1 (#B-INDX) = ' '     /* EM - 102313 START
            //*     ESCAPE ROUTINE
            //*   END-IF
            if (condition(pnd_Alpha_Fund_Cde.getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                                                              //Natural: IF #ALPHA-FUND-CDE ( #B-INDX ) = ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
                //*  EM - 102313 END
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  IF #PEC-ACCT-NAME-1 (#B-INDX) = ADC-ACCT-CDE (#A-INDX) /* EM - 102313
            //*  EM - 102313
            if (condition(pnd_Alpha_Fund_Cde.getValue(pnd_All_Indx_Pnd_B_Indx).equals(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx))))  //Natural: IF #ALPHA-FUND-CDE ( #B-INDX ) = ADC-ACCT-CDE ( #A-INDX )
            {
                pnd_Cref_Prod_Found.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO #CREF-PROD-FOUND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cref_Prod_Found.setValue(false);                                                                                                                      //Natural: MOVE FALSE TO #CREF-PROD-FOUND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero())))                           //Natural: IF ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) NE 0
                {
                    ldaAdsl780.getPnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                          //Natural: ADD 1 TO #CREF-MTH-MAT-CNT ( #B-INDX )
                    //*  RS0
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADC-ACCT-CDE ( #A-INDX ) NE 'R' AND ADC-ACCT-CDE ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl780.getPnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Maturity_Cnt().nadd(1);                                                      //Natural: ADD 1 TO #CREF-SUB-RATE-MTH-MATURITY-CNT
                        ldaAdsl780.getPnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Cnt().nadd(1);                                                       //Natural: ADD 1 TO #CREF-SUB-GRN-MTH-MATURITY-CNT
                        ldaAdsl780.getPnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) TO #CREF-SUB-MTH-MATURITY-AMT
                        ldaAdsl780.getPnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) TO #CREF-SUB-GRN-MTH-MATURITY-AMT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt().nadd(1);                                                                    //Natural: ADD 1 TO #TOTAL-RATE-MATURITY-CNT
                    ldaAdsl780.getPnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                      //Natural: ADD 1 TO #CREF-GRN-MTH-MAT-CNT ( #B-INDX )
                    ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                    //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
                    ldaAdsl780.getPnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) TO #CREF-MTH-MAT-AMT ( #B-INDX )
                    ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) TO #TOTAL-MATURITY-AMT
                    ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) TO #GRAND-MATURITY-AMT
                    ldaAdsl780.getPnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) TO #CREF-GRN-MTH-MAT-AMT ( #B-INDX )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero())))                          //Natural: IF ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) NE 0
                {
                    ldaAdsl780.getPnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                         //Natural: ADD 1 TO #CREF-ANN-MAT-CNT ( #B-INDX )
                    //*  RS0
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADC-ACCT-CDE ( #A-INDX ) NE 'R' AND ADC-ACCT-CDE ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl780.getPnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Maturity_Cnt().nadd(1);                                                     //Natural: ADD 1 TO #CREF-SUB-RATE-ANN-MATURITY-CNT
                        ldaAdsl780.getPnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Cnt().nadd(1);                                                      //Natural: ADD 1 TO #CREF-SUB-GRN-ANN-MATURITY-CNT
                        ldaAdsl780.getPnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) TO #CREF-SUB-ANN-MATURITY-AMT
                        ldaAdsl780.getPnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) TO #CREF-SUB-GRN-ANN-MATURITY-AMT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt().nadd(1);                                                                    //Natural: ADD 1 TO #TOTAL-RATE-MATURITY-CNT
                    ldaAdsl780.getPnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                     //Natural: ADD 1 TO #CREF-GRN-ANN-MAT-CNT ( #B-INDX )
                    ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                    //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
                    ldaAdsl780.getPnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) TO #CREF-ANN-MAT-AMT ( #B-INDX )
                    ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) TO #TOTAL-MATURITY-AMT
                    ldaAdsl780.getPnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) TO #CREF-GRN-ANN-MAT-AMT ( #B-INDX )
                    ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) TO #GRAND-MATURITY-AMT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* *  IF #CREF-PROD-FOUND
            //* *    IF ADC-ACCT-RTB-ACTL-AMT (#A-INDX) NE 0
            //* *      ADD 1 TO #CREF-ANN-RTB-CNT (#B-INDX)
            //* *      IF ADC-ACCT-CDE(#A-INDX) NE 'R'
            //* *        ADD 1 TO #CREF-SUB-RATE-ANN-RTB-CNT
            //* *        ADD 1 TO #CREF-SUB-GRN-ANN-RTB-CNT
            //* *        ADD ADC-ACCT-RTB-ACTL-AMT (#A-INDX) TO
            //* *          #CREF-SUB-ANN-RTB-AMT
            //* *        ADD ADC-ACCT-RTB-ACTL-AMT (#A-INDX) TO
            //* *          #CREF-SUB-GRN-ANN-RTB-AMT
            //* *      END-IF
            //* *      ADD 1 TO #TOTAL-RATE-RTB-CNT
            //* *      ADD 1 TO #CREF-GRN-ANN-RTB-CNT (#B-INDX)
            //* *      ADD 1 TO #GRAND-RATE-RTB-CNT
            //* *      ADD ADC-ACCT-RTB-ACTL-AMT (#A-INDX) TO
            //* *        #CREF-ANN-RTB-AMT (#B-INDX)
            //* *      ADD ADC-ACCT-RTB-ACTL-AMT (#A-INDX) TO #TOTAL-RTB-AMT
            //* *      ADD ADC-ACCT-RTB-ACTL-AMT (#A-INDX) TO #GRAND-RTB-AMT
            //* *      ADD ADC-ACCT-RTB-ACTL-AMT (#A-INDX) TO
            //* *        #CREF-GRN-ANN-RTB-AMT (#B-INDX)
            //* *    END-IF
            //* *  END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Built_Rollover_Totals() throws Exception                                                                                                             //Natural: BUILT-ROLLOVER-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        if (condition(pnd_Dest.getValue("*").equals("RTHC") || pnd_Dest.getValue("*").equals("RTHT") || pnd_Dest.getValue("*").equals("IRAC") || pnd_Dest.getValue("*").equals("IRAT")  //Natural: IF #DEST ( * ) = 'RTHC' OR = 'RTHT' OR = 'IRAC' OR = 'IRAT' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC'
            || pnd_Dest.getValue("*").equals("03BT") || pnd_Dest.getValue("*").equals("03BC") || pnd_Dest.getValue("*").equals("QPLT") || pnd_Dest.getValue("*").equals("QPLC")))
        {
            FOR04:                                                                                                                                                        //Natural: FOR #B-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
            for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
            {
                //*    IF #PEC-ACCT-NAME-1 (#B-INDX) = #PROD-CDE (#POS) /* EM - 102313
                //*  EM - 102313
                if (condition(pnd_Alpha_Fund_Cde.getValue(pnd_All_Indx_Pnd_B_Indx).equals(pnd_Prod_Cde.getValue(pnd_All_Indx_Pnd_Pos))))                                  //Natural: IF #ALPHA-FUND-CDE ( #B-INDX ) = #PROD-CDE ( #POS )
                {
                    if (condition(pnd_Rlvr_Amt.getValue(pnd_All_Indx_Pnd_Pos).greater(getZero())))                                                                        //Natural: IF #RLVR-AMT ( #POS ) > 0
                    {
                        ldaAdsl780.getPnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                //Natural: ADD 1 TO #CREF-ANN-RTB-RLVR-CNT ( #B-INDX )
                        //*  RS0
                        if (condition(pnd_Prod_Cde.getValue(pnd_All_Indx_Pnd_Pos).notEquals("R") && pnd_Prod_Cde.getValue(pnd_All_Indx_Pnd_Pos).notEquals("D")))          //Natural: IF #PROD-CDE ( #POS ) NE 'R' AND #PROD-CDE ( #POS ) NE 'D'
                        {
                            ldaAdsl780.getPnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Rtb_Rlvr_Cnt().nadd(1);                                                 //Natural: ADD 1 TO #CREF-SUB-RATE-ANN-RTB-RLVR-CNT
                            ldaAdsl780.getPnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Rlvr_Cnt().nadd(1);                                                  //Natural: ADD 1 TO #CREF-SUB-GRN-ANN-RTB-RLVR-CNT
                            ldaAdsl780.getPnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Rtb_Rlvr_Amt().nadd(pnd_Rlvr_Amt.getValue(pnd_All_Indx_Pnd_Pos));            //Natural: ADD #RLVR-AMT ( #POS ) TO #CREF-SUB-ANN-RTB-RLVR-AMT
                            ldaAdsl780.getPnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Rlvr_Amt().nadd(pnd_Rlvr_Amt.getValue(pnd_All_Indx_Pnd_Pos));        //Natural: ADD #RLVR-AMT ( #POS ) TO #CREF-SUB-GRN-ANN-RTB-RLVR-AMT
                        }                                                                                                                                                 //Natural: END-IF
                        ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Rtb_Rlvr_Cnt().nadd(1);                                                                //Natural: ADD 1 TO #TOTAL-RATE-RTB-RLVR-CNT
                        ldaAdsl780.getPnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                 //Natural: ADD 1 TO #CREF-GRN-ANN-RTB-RLC ( #B-INDX )
                        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Rtb_Rlvr_Cnt().nadd(1);                                                                //Natural: ADD 1 TO #GRAND-RATE-RTB-RLVR-CNT
                        ldaAdsl780.getPnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(pnd_Rlvr_Amt.getValue(pnd_All_Indx_Pnd_Pos)); //Natural: ADD #RLVR-AMT ( #POS ) TO #CREF-ANN-RTB-RLVR-AMT ( #B-INDX )
                        ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rtb_Rlvr_Amt().nadd(pnd_Rlvr_Amt.getValue(pnd_All_Indx_Pnd_Pos));                           //Natural: ADD #RLVR-AMT ( #POS ) TO #TOTAL-RTB-RLVR-AMT
                        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rtb_Rlvr_Amt().nadd(pnd_Rlvr_Amt.getValue(pnd_All_Indx_Pnd_Pos));                           //Natural: ADD #RLVR-AMT ( #POS ) TO #GRAND-RTB-RLVR-AMT
                        ldaAdsl780.getPnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(pnd_Rlvr_Amt.getValue(pnd_All_Indx_Pnd_Pos)); //Natural: ADD #RLVR-AMT ( #POS ) TO #CREF-GRN-ANN-RTB-RLA ( #B-INDX )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Break_Of_Accounting_Date_Rtn() throws Exception                                                                                                      //Natural: BREAK-OF-ACCOUNTING-DATE-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  GG050608
        if (condition(! (pnd_Roth_Only_Run.getBoolean())))                                                                                                                //Natural: IF NOT #ROTH-ONLY-RUN
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm464.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM464'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm464.class));                                                                           //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM464'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Prev_Accounting_Dte_Yyyymmdd.setValueEdited(ldaAdsl401a.getAds_Cntrct_View_Adc_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                              //Natural: MOVE EDITED ADC-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #PREV-ACCOUNTING-DTE-YYYYMMDD
        ldaAdsl780.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm);                           //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-MM TO #ACCOUNTING-DATE-MCDY-MM
        ldaAdsl780.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd);                           //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-DD TO #ACCOUNTING-DATE-MCDY-DD
        ldaAdsl780.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc);                           //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-CC TO #ACCOUNTING-DATE-MCDY-CC
        ldaAdsl780.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy);                           //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-YY TO #ACCOUNTING-DATE-MCDY-YY
        ldaAdsl780.getPnd_Tiaa_Grand_Accum().reset();                                                                                                                     //Natural: RESET #TIAA-GRAND-ACCUM #TIAA-SUB-GRN-ACCUM #CREF-MONTHLY-GRAND-ACCUM ( * ) #CREF-ANNUALLY-GRAND-ACCUM ( * ) #CREF-MONTHLY-SUB-GRAND-ACCUM #CREF-ANNUALLY-SUB-GRAND-ACCUM #CREF-AND-TIAA-GRAND-ACCUM
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum().reset();
        ldaAdsl780.getPnd_Cref_Monthly_Grand_Accum().getValue("*").reset();
        ldaAdsl780.getPnd_Cref_Annually_Grand_Accum().getValue("*").reset();
        ldaAdsl780.getPnd_Cref_Monthly_Sub_Grand_Accum().reset();
        ldaAdsl780.getPnd_Cref_Annually_Sub_Grand_Accum().reset();
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum().reset();
    }
    private void sub_Break_Of_Effective_Date_Rtn() throws Exception                                                                                                       //Natural: BREAK-OF-EFFECTIVE-DATE-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //* *MOVE EDITED #PREV-EFFCTV-DTE (EM=YYYYMMDD) TO #DATE-A  /* 010411
        //*  010411
        pnd_W_Date_A.setValueEdited(pnd_Prev_Effctv_Dte,new ReportEditMask("YYYYMMDD"));                                                                                  //Natural: MOVE EDITED #PREV-EFFCTV-DTE ( EM = YYYYMMDD ) TO #W-DATE-A
        ldaAdsl780.getPnd_Effective_Date_Pnd_Effective_Date_Mcdy_Mm().setValue(pnd_W_Date_A_Pnd_Date_Mm);                                                                 //Natural: MOVE #DATE-MM TO #EFFECTIVE-DATE-MCDY-MM
        ldaAdsl780.getPnd_Effective_Date_Pnd_Effective_Date_Mcdy_Dd().setValue(pnd_W_Date_A_Pnd_Date_Dd);                                                                 //Natural: MOVE #DATE-DD TO #EFFECTIVE-DATE-MCDY-DD
        ldaAdsl780.getPnd_Effective_Date_Pnd_Effective_Date_Mcdy_Cc().setValue(pnd_W_Date_A_Pnd_Date_Cc);                                                                 //Natural: MOVE #DATE-CC TO #EFFECTIVE-DATE-MCDY-CC
        ldaAdsl780.getPnd_Effective_Date_Pnd_Effective_Date_Mcdy_Yy().setValue(pnd_W_Date_A_Pnd_Date_Yy);                                                                 //Natural: MOVE #DATE-YY TO #EFFECTIVE-DATE-MCDY-YY
        //*  GG050608
        if (condition(! (pnd_Roth_Only_Run.getBoolean())))                                                                                                                //Natural: IF NOT #ROTH-ONLY-RUN
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm463.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM463'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm463.class));                                                                           //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM463'
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl780.getPnd_Tiaa_Minor_Accum().reset();                                                                                                                     //Natural: RESET #TIAA-MINOR-ACCUM #TIAA-SUB-ACCUM #CREF-MONTHLY-MINOR-ACCUM ( * ) #CREF-ANNUALLY-MINOR-ACCUM ( * ) #CREF-MONTHLY-SUB-TOTAL-ACCUM #CREF-ANNUALLY-SUB-TOTAL-ACCUM #TOTAL-CREF-AND-TIAA-ACCUM
        ldaAdsl780.getPnd_Tiaa_Sub_Accum().reset();
        ldaAdsl780.getPnd_Cref_Monthly_Minor_Accum().getValue("*").reset();
        ldaAdsl780.getPnd_Cref_Annually_Minor_Accum().getValue("*").reset();
        ldaAdsl780.getPnd_Cref_Monthly_Sub_Total_Accum().reset();
        ldaAdsl780.getPnd_Cref_Annually_Sub_Total_Accum().reset();
        ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum().reset();
    }
    private void sub_Break_Of_Settlement_Type() throws Exception                                                                                                          //Natural: BREAK-OF-SETTLEMENT-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  GG050608
        if (condition(! (pnd_Roth_Only_Run.getBoolean())))                                                                                                                //Natural: IF NOT #ROTH-ONLY-RUN
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm462.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM462'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm462.class));                                                                           //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM462'
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_End_Of_Job_Rtn() throws Exception                                                                                                                    //Natural: END-OF-JOB-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  GG050608
        if (condition(! (pnd_Roth_Only_Run.getBoolean())))                                                                                                                //Natural: IF NOT #ROTH-ONLY-RUN
        {
            getReports().write(1, ReportOption.NOTITLE,"**********************************************************",NEWLINE,"*  ADSP780 (DAILY ACTIVITY MATURITIES) - JOB RUN ENDED   *"); //Natural: WRITE ( 1 ) '**********************************************************' / '*  ADSP780 (DAILY ACTIVITY MATURITIES) - JOB RUN ENDED   *'
            if (Global.isEscape()) return;
            if (condition(pnd_Total_Number_Rec.equals(getZero())))                                                                                                        //Natural: IF #TOTAL-NUMBER-REC = 0
            {
                getReports().write(1, ReportOption.NOTITLE,"*            NO TRANSACTIONS PROCESSED TODAY             *");                                                 //Natural: WRITE ( 1 ) '*            NO TRANSACTIONS PROCESSED TODAY             *'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(1, ReportOption.NOTITLE,"*   NUMBER OF TRANSACTIONS PROCESSED TODAY :",pnd_Total_Number_Rec, new ReportEditMask ("ZZ,ZZZ,ZZ9"),        //Natural: WRITE ( 1 ) '*   NUMBER OF TRANSACTIONS PROCESSED TODAY :' #TOTAL-NUMBER-REC ( EM = ZZ,ZZZ,ZZ9 ) '  *'
                    "  *");
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,"**********************************************************");                                                     //Natural: WRITE ( 1 ) '**********************************************************'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  GG050608
            getReports().write(2, ReportOption.NOTITLE,"**********************************************************");                                                     //Natural: WRITE ( 2 ) '**********************************************************'
            if (Global.isEscape()) return;
            if (condition(pnd_Total_Number_Rec_Roth.equals(getZero())))                                                                                                   //Natural: IF #TOTAL-NUMBER-REC-ROTH = 0
            {
                getReports().write(2, ReportOption.NOTITLE,"*       NO ROTH TRANSACTIONS PROCESSED TODAY             *");                                                 //Natural: WRITE ( 2 ) '*       NO ROTH TRANSACTIONS PROCESSED TODAY             *'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(2, ReportOption.NOTITLE,"* NUMBER OF ROTH TRANS PROCESSED TODAY:       ",pnd_Total_Number_Rec_Roth, new ReportEditMask                 //Natural: WRITE ( 2 ) '* NUMBER OF ROTH TRANS PROCESSED TODAY:       ' #TOTAL-NUMBER-REC-ROTH ( EM = ZZ,ZZZ,ZZ9 ) '*'
                    ("ZZ,ZZZ,ZZ9"),"*");
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(2, ReportOption.NOTITLE,"**********************************************************");                                                     //Natural: WRITE ( 2 ) '**********************************************************'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Heading.setValue("       Annuitization of OmniPlus Contracts        ");                                                                           //Natural: ASSIGN #HEADING := '       Annuitization of OmniPlus Contracts        '
                    if (condition(! (pnd_Roth_Only_Run.getBoolean())))                                                                                                    //Natural: IF NOT #ROTH-ONLY-RUN
                    {
                        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm462.class));                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM462'
                    }                                                                                                                                                     //Natural: END-IF
                    //*  GG050608
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Heading.setValue("Annuitization of OmniPlus Roth 403b/401k Contracts");                                                                           //Natural: ASSIGN #HEADING := 'Annuitization of OmniPlus Roth 403b/401k Contracts'
                    if (condition(pnd_Roth_Only_Run.getBoolean()))                                                                                                        //Natural: IF #ROTH-ONLY-RUN
                    {
                        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm462.class));                                                               //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM462'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=79 PS=60");
        Global.format(1, "LS=79 PS=60");
        Global.format(2, "LS=79 PS=60");
    }
}
