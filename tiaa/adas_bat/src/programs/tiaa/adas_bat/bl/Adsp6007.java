/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:03:58 PM
**        * FROM NATURAL PROGRAM : Adsp6007
************************************************************
**        * FILE NAME            : Adsp6007.java
**        * CLASS NAME           : Adsp6007
**        * INSTANCE NAME        : Adsp6007
************************************************************
***********************************************************
* ADSP6007
*
* UPDATE FOR CIS ADD
* CLONE OF ADSP6002
*
* REVISED 11/20/98 BY DON MEADE TO ADD LAST GUARANTEED PAYMENT AMOUNT,
*   STORED ON CIS FILE IN "MDO TRADITIONAL AMOUNT" FIELD
*
* REVISED 12/9/98 BY DON MEADE TO INSERT DIAGNOSTIC DISPLAYS
*
* 01/27/99   ZAFAR KHAN  NAP-BPS-UNIT EXTENDED FROM 6 TO 8 BYTES
*
* 06/99      E. MELNIK   ADDITIONAL CODE TO CALL NEW ACTUARIAL MODULE
*                        TO GET NEW MORTALITY BASIS INFORMATION AND
*                        PUT IT ON THE CIS FILE.  CIS WILL PRINT IT
*                        ON THE CONTRACT ISSUE PACKAGE.
*
* 01/01      J. VLISMAS  ADD NEW OPTION-CODE VALUES FOR TPA AND IPRO
*
* 10/23/2003 OS RECOMPILED TO GET UPDATED NAZL6003 (80 TIAA RATES).
* 04/19/2004 E. MELNIK MODIFIED FOR ANNUITIZATION SUNGUARD
* 11/18/2004 T. SHINE CHANGED TO CHECK FOR END OF MONTH PROCESSING
* 05/19/2005 M. NACHBER CHANGED TO MOVE SPACES TO CIS-CNTRCT-APPRVL-IND
*                       INSTEAD OF 'Y' FOR  R5
* 07/23/2007 E. MELNIK  NEW 75% ANNUITY OPTION CHANGE. MARKED BY
*                       EM 072307
* 03/17/2008 E.MELNIK   ROTH 403B/401K CHANGES.  RECOMPILED FOR
*                       NEW ADSL401/ADSL450/ADSLCNTL.
* 08/29/2008 E.MELNIK   ATRA ENDORSEMENT CHANGES.  MARKED BY
*                       082908.
* 01/23/09  E. MELNIK TIAA ACCESS/STABLE RETURN ENHANCEMENTS. MARKED
*                         BY EM - 012309.
*
* 3/16/2010 C. MASON  PCPOP CONVERSION - ADD CODE TO HANDLE 457B PRIVATE
* 7/07/2010 C. MASON  MODIFIED CLONE OF ADSP6002 TO PROCESS CIS
*                     ADD/MAIN REQUESTS
* 03/05/12  E. MELNIK RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                     UPDATED LINKAGE AREAS.
* 05/31/12  E. MELNIK ADDITIONAL CHANGE.  MARKED BY EM 053112.
* 01/18/13  E. MELNIK TNG PROJECT.  RESTOW TO INCLUDE UPDATED LINKAGE
*                      AREAS.
* 03/11/14   E. MELNIK CREF/REA REDESIGN CHANGES.
*                      MARKED BY EM - 031114.
* 03/18/14   O. SOTTO  EXTRACT SUFFIX FROM 2ND ANNT LAST NAME
*                      (IF ENTERED) AND PASS TO CIS.  THIS IS TO
*                      RESOLVE MDM ISSUE WHEN SUFFIX IS PASSED AS
*                      PART OF LAST NAME.  MARKED OS - 031814.
* 02/24/2017 R. CARREON PIN EXPANSION 02242017
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp6007 extends BLNatBase
{
    // Data Areas
    private LdaAdsl401 ldaAdsl401;
    private LdaAdslcntl ldaAdslcntl;
    private LdaAdsl401a ldaAdsl401a;
    private PdaCisa1000 pdaCisa1000;
    private PdaNeca4000 pdaNeca4000;
    private PdaAdsa888 pdaAdsa888;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cis_Prtcpnt_File_View;
    private DbsField cis_Prtcpnt_File_View_Cis_Pin_Nbr;
    private DbsField cis_Prtcpnt_File_View_Cis_Tiaa_Doi;
    private DbsField cis_Prtcpnt_File_View_Cis_Rqst_Id_Key;
    private DbsField cis_Prtcpnt_File_View_Cis_Status_Cd;
    private DbsField cis_Prtcpnt_File_View_Cis_Cref_Doi;
    private DbsField cis_Prtcpnt_File_View_Cis_Rqst_Id;
    private DbsField cis_Prtcpnt_File_View_Cis_Opn_Clsd_Ind;
    private DbsField cis_Prtcpnt_File_View_Cis_Cntrct_Type;
    private DbsField cis_Prtcpnt_File_View_Cis_Cntrct_Print_Dte;
    private DbsField cis_Prtcpnt_File_View_Cis_Appl_Rcvd_Dte;
    private DbsField cis_Prtcpnt_File_View_Cis_Appl_Rcvd_User_Id;
    private DbsField cis_Prtcpnt_File_View_Cis_Appl_Entry_User_Id;
    private DbsField cis_Prtcpnt_File_View_Cis_Annty_Option;
    private DbsField cis_Prtcpnt_File_View_Cis_Pymnt_Mode;
    private DbsField cis_Prtcpnt_File_View_Cis_Annty_Start_Dte;
    private DbsField cis_Prtcpnt_File_View_Cis_Annty_End_Dte;
    private DbsField cis_Prtcpnt_File_View_Cis_Grnted_Period_Yrs;
    private DbsField cis_Prtcpnt_File_View_Cis_Grnted_Period_Dys;
    private DbsField cis_Prtcpnt_File_View_Cis_Scnd_Annt_Prfx;
    private DbsField cis_Prtcpnt_File_View_Cis_Scnd_Annt_Frst_Nme;
    private DbsField cis_Prtcpnt_File_View_Cis_Scnd_Annt_Mid_Nme;
    private DbsField cis_Prtcpnt_File_View_Cis_Scnd_Annt_Lst_Nme;
    private DbsField cis_Prtcpnt_File_View_Cis_Scnd_Annt_Sffx;
    private DbsField cis_Prtcpnt_File_View_Cis_Scnd_Annt_Ssn;
    private DbsField cis_Prtcpnt_File_View_Cis_Scnd_Annt_Dob;
    private DbsField cis_Prtcpnt_File_View_Cis_Scnd_Annt_Sex_Cde;
    private DbsField cis_Prtcpnt_File_View_Cis_Grnted_Grd_Amt;
    private DbsField cis_Prtcpnt_File_View_Cis_Grnted_Std_Amt;
    private DbsField cis_Prtcpnt_File_View_Count_Castcis_Tiaa_Commuted_Info;

    private DbsGroup cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info;
    private DbsField cis_Prtcpnt_File_View_Cis_Comut_Grnted_Amt;
    private DbsField cis_Prtcpnt_File_View_Cis_Comut_Int_Rate;
    private DbsField cis_Prtcpnt_File_View_Cis_Comut_Pymt_Method;
    private DbsField cis_Prtcpnt_File_View_Cis_Grnted_Int_Rate;
    private DbsField cis_Prtcpnt_File_View_Cis_Surv_Redct_Amt;
    private DbsField cis_Prtcpnt_File_View_Count_Castcis_Da_Tiaa_Cntrcts_Rqst;

    private DbsGroup cis_Prtcpnt_File_View_Cis_Da_Tiaa_Cntrcts_Rqst;
    private DbsField cis_Prtcpnt_File_View_Cis_Da_Tiaa_Nbr;
    private DbsField cis_Prtcpnt_File_View_Cis_Da_Tiaa_Proceeds_Amt;
    private DbsField cis_Prtcpnt_File_View_Cis_Da_Rea_Proceeds_Amt;
    private DbsField cis_Prtcpnt_File_View_Count_Castcis_Da_Cref_Cntrcts_Rqst;

    private DbsGroup cis_Prtcpnt_File_View_Cis_Da_Cref_Cntrcts_Rqst;
    private DbsField cis_Prtcpnt_File_View_Cis_Da_Cert_Nbr;
    private DbsField cis_Prtcpnt_File_View_Cis_Da_Cref_Proceeds_Amt;
    private DbsField cis_Prtcpnt_File_View_Count_Castcis_Cref_Annty_Pymnt;

    private DbsGroup cis_Prtcpnt_File_View_Cis_Cref_Annty_Pymnt;
    private DbsField cis_Prtcpnt_File_View_Cis_Cref_Acct_Cde;
    private DbsField cis_Prtcpnt_File_View_Cis_Cref_Mnthly_Nbr_Units;
    private DbsField cis_Prtcpnt_File_View_Cis_Cref_Annual_Nbr_Units;
    private DbsField cis_Prtcpnt_File_View_Cis_Cref_Annty_Amt;
    private DbsField cis_Prtcpnt_File_View_Cis_Rea_Annty_Amt;
    private DbsField cis_Prtcpnt_File_View_Cis_Rea_Surv_Nbr_Units;
    private DbsField cis_Prtcpnt_File_View_Cis_Rea_Mnthly_Nbr_Units;
    private DbsField cis_Prtcpnt_File_View_Cis_Rea_Annual_Nbr_Units;
    private DbsField cis_Prtcpnt_File_View_Cis_Cntrct_Apprvl_Ind;
    private DbsField cis_Prtcpnt_File_View_Cis_Orig_Issue_State;
    private DbsField cis_Prtcpnt_File_View_Cis_Issue_State_Cd;
    private DbsField cis_Prtcpnt_File_View_Cis_Lob;
    private DbsField cis_Prtcpnt_File_View_Cis_Lob_Type;
    private DbsField cis_Prtcpnt_File_View_Cis_Mail_Instructions;
    private DbsField cis_Prtcpnt_File_View_Cis_Pull_Code;

    private DbsGroup cis_Prtcpnt_File_View_Cis_Address_Info;
    private DbsField cis_Prtcpnt_File_View_Cis_Address_Chg_Ind;
    private DbsField cis_Prtcpnt_File_View_Cis_Address_Dest_Name;
    private DbsGroup cis_Prtcpnt_File_View_Cis_Address_TxtMuGroup;
    private DbsField cis_Prtcpnt_File_View_Cis_Address_Txt;
    private DbsField cis_Prtcpnt_File_View_Cis_Zip_Code;
    private DbsField cis_Prtcpnt_File_View_Cis_Bank_Pymnt_Acct_Nmbr;
    private DbsField cis_Prtcpnt_File_View_Cis_Bank_Aba_Acct_Nmbr;
    private DbsField cis_Prtcpnt_File_View_Cis_Stndrd_Trn_Cd;
    private DbsField cis_Prtcpnt_File_View_Cis_Finalist_Reason_Cd;
    private DbsField cis_Prtcpnt_File_View_Cis_Addr_Usage_Code;
    private DbsField cis_Prtcpnt_File_View_Cis_Checking_Saving_Cd;
    private DbsField cis_Prtcpnt_File_View_Cis_Addr_Stndrd_Code;
    private DbsField cis_Prtcpnt_File_View_Cis_Stndrd_Overide;
    private DbsField cis_Prtcpnt_File_View_Cis_Postal_Data_Fields;
    private DbsField cis_Prtcpnt_File_View_Cis_Geographic_Cd;
    private DbsField cis_Prtcpnt_File_View_Cis_Rtb_Pct;
    private DbsField cis_Prtcpnt_File_View_Cis_Rtb_Amt;

    private DbsGroup cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data;
    private DbsField cis_Prtcpnt_File_View_Cis_Rtb_Dest_Pct;
    private DbsField cis_Prtcpnt_File_View_Cis_Rtb_Dest_Amt;
    private DbsField cis_Prtcpnt_File_View_Cis_Rtb_Dest_Nme;
    private DbsGroup cis_Prtcpnt_File_View_Cis_Rtb_Dest_AddrMuGroup;
    private DbsField cis_Prtcpnt_File_View_Cis_Rtb_Dest_Addr;
    private DbsField cis_Prtcpnt_File_View_Cis_Rtb_Bank_Accnt_Nbr;
    private DbsField cis_Prtcpnt_File_View_Cis_Rtb_Trnst_Nbr;
    private DbsField cis_Prtcpnt_File_View_Cis_Rtb_Dest_Zip;
    private DbsField cis_Prtcpnt_File_View_Cis_Rtb_Dest_Type_Cd;
    private DbsField cis_Prtcpnt_File_View_Cis_Trnsf_Flag;
    private DbsField cis_Prtcpnt_File_View_Cis_Trnsf_Cert_Nbr;
    private DbsField cis_Prtcpnt_File_View_Cis_Trnsf_Tiaa_Rea_Nbr;
    private DbsField cis_Prtcpnt_File_View_Cis_Mdo_Traditional_Amt;

    private DbsGroup cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info_2;
    private DbsField cis_Prtcpnt_File_View_Cis_Comut_Grnted_Amt_2;
    private DbsField cis_Prtcpnt_File_View_Cis_Comut_Int_Rate_2;
    private DbsField cis_Prtcpnt_File_View_Cis_Comut_Pymt_Method_2;
    private DbsField cis_Prtcpnt_File_View_Cis_Comut_Mortality_Basis;
    private DbsField cis_Prtcpnt_File_View_Cis_Sg_Text_Udf_1;
    private DbsField cis_Prtcpnt_File_View_Cis_Tacc_Annty_Amt;

    private DbsGroup cis_Prtcpnt_File_View_Cis_Tacc_Fund_Info;
    private DbsField cis_Prtcpnt_File_View_Cis_Tacc_Ind;
    private DbsField cis_Prtcpnt_File_View_Cis_Tacc_Account;
    private DbsField cis_Prtcpnt_File_View_Cis_Tacc_Mnthly_Nbr_Units;
    private DbsField cis_Prtcpnt_File_View_Cis_Tacc_Annual_Nbr_Units;
    private DbsField cis_Prtcpnt_File_View_Cis_Institution_Name;
    private DbsField cis_Prtcpnt_File_View_Cis_Four_Fifty_Seven_Ind;

    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;
    private DbsField pnd_Naz_Table_Key;

    private DbsGroup pnd_Naz_Table_Key__R_Field_1;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id;
    private DbsField pnd_End_Date;
    private DbsField pnd_Annty_Strt_Dte;

    private DbsGroup pnd_Annty_Strt_Dte__R_Field_2;
    private DbsField pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Cc;
    private DbsField pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Yy;
    private DbsField pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Mm;
    private DbsField pnd_Annty_Strt_Dte_Fillerx;
    private DbsField pnd_Ok_Sw;
    private DbsField pnd_Counter;
    private DbsField pnd_Counter_Read;
    private DbsField pnd_Counter_Compress;
    private DbsField pnd_Counter_Start;
    private DbsField pnd_Counter_End;
    private DbsField pnd_Counter_Cis_T;
    private DbsField pnd_Counter_Cis_C;
    private DbsField pnd_Total_Processed;
    private DbsField pnd_Error_Ind;
    private DbsField pnd_Cntrl_Total;
    private DbsField pnd_Cntrl_Ok;
    private DbsField pnd_Rtb_Count;
    private DbsField pnd_Tiaa_Ia_No;
    private DbsField pnd_Cref_Ia_No;
    private DbsField pnd_Cis_Cref_Annty_Amt;
    private DbsField pnd_Cis_Rea_Annty_Amt;
    private DbsField pnd_Cis_Acc_Annty_Amt;
    private DbsField pnd_Cis_Cref_Number;
    private DbsField pnd_Cis_Rea_Number;
    private DbsField pnd_Cis_Tiaa_Number;
    private DbsField pnd_Cis_Total_Da_Cref;
    private DbsField pnd_Cis_Total_Da_Rea;
    private DbsField pnd_Cis_Total_Da_Access;
    private DbsField pnd_Cis_Total_Da_Tiaa;
    private DbsField pnd_Cis_Total_Da_Amt;
    private DbsField pnd_Cis_Tiaa_Grd_Amt;
    private DbsField pnd_Cis_Tiaa_Std_Amt;
    private DbsField pnd_Cis_Fnl_Std_Pay_Amt;
    private DbsField pnd_Cis_Comut_Gruar_Amt_G;
    private DbsField pnd_Cis_Comut_Int_Rate_G;
    private DbsField pnd_Cis_Comut_Payment_G;
    private DbsField pnd_Cis_Comut_Gruar_Amt_S;
    private DbsField pnd_Cis_Comut_Int_Rate_S;
    private DbsField pnd_Cis_Comut_Payment_S;
    private DbsField pnd_Control_Date_Numeric;

    private DbsGroup pnd_Control_Date_Numeric__R_Field_3;
    private DbsField pnd_Control_Date_Numeric_Pnd_Control_Date_Cc;
    private DbsField pnd_Control_Date_Numeric_Pnd_Control_Date_Yymmdd;
    private DbsField pnd_Part_Date;

    private DbsGroup pnd_Part_Date__R_Field_4;
    private DbsField pnd_Part_Date_Pnd_Part_Date_Numeric;
    private DbsField pnd_Hold_Last_Name;
    private DbsField pnd_Adat_Sffx;
    private DbsField pnd_A;
    private DbsField pnd_C;
    private DbsField pnd_D;
    private DbsField pnd_Work_Method_2;

    private DbsGroup pnd_Work_Fields;
    private DbsField pnd_Work_Fields_Pnd_Work_1_1;
    private DbsField pnd_Work_Fields_Pnd_Work_Dot;
    private DbsField pnd_Work_Fields_Pnd_Work_2_4;
    private DbsField pnd_Eff_Date;

    private DbsGroup pnd_Eff_Date__R_Field_5;
    private DbsField pnd_Eff_Date_Pnd_Eff_Date_Numeric;
    private DbsField pnd_Cis_Super;
    private DbsField pnd_Adc_Super2_From;

    private DbsGroup pnd_Adc_Super2_From__R_Field_6;
    private DbsField pnd_Adc_Super2_From_Pnd_Rqst_Id_F;
    private DbsField pnd_Adc_Super2_From_Pnd_Adc_Sqnce_Nbr_F;
    private DbsField pnd_Adc_Super2_To;

    private DbsGroup pnd_Adc_Super2_To__R_Field_7;
    private DbsField pnd_Adc_Super2_To_Pnd_Rqst_Id_T;
    private DbsField pnd_Adc_Super2_To_Pnd_Adc_Sqnce_Nbr_T;
    private DbsField pnd_Adi_Super1;

    private DbsGroup pnd_Adi_Super1__R_Field_8;
    private DbsField pnd_Adi_Super1_Pnd_Rqst_Id_Ia;
    private DbsField pnd_Adi_Super1_Pnd_Adi_Record_Type;
    private DbsField pnd_Adi_Super1_Pnd_Adi_Sqnce_Nbr;
    private DbsField testing_Mode;
    private DbsField pnd_B;
    private DbsField pnd_Ws_Lob_Nmbr;
    private DbsField pnd_Work_Ipro_Summary_Guar_Amount;
    private DbsField pnd_Work_Tpa_Summary_Guar_Amount;
    private DbsField pnd_Temp_Date_A;

    private DbsGroup pnd_Temp_Date_A__R_Field_9;
    private DbsField pnd_Temp_Date_A_Pnd_Temp_Date_N;
    private DbsField pnd_Cis_Sg_Text_Udf_1;

    private DbsGroup pnd_Cis_Sg_Text_Udf_1__R_Field_10;
    private DbsField pnd_Cis_Sg_Text_Udf_1_Pnd_Atra_Ind;
    private DbsField pnd_Cis_Sg_Text_Udf_1_Pnd_Atra_Issue_Dte;
    private DbsField pnd_Idx;
    private DbsField pnd_Max_Cnt;
    private DbsField pnd_Total_Cis_Add_Requests;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl401 = new LdaAdsl401();
        registerRecord(ldaAdsl401);
        registerRecord(ldaAdsl401.getVw_ads_Prtcpnt_View());
        ldaAdslcntl = new LdaAdslcntl();
        registerRecord(ldaAdslcntl);
        registerRecord(ldaAdslcntl.getVw_ads_Cntl_View());
        ldaAdsl401a = new LdaAdsl401a();
        registerRecord(ldaAdsl401a);
        registerRecord(ldaAdsl401a.getVw_ads_Cntrct_View());
        localVariables = new DbsRecord();
        pdaCisa1000 = new PdaCisa1000(localVariables);
        pdaNeca4000 = new PdaNeca4000(localVariables);
        pdaAdsa888 = new PdaAdsa888(localVariables);

        // Local Variables

        vw_cis_Prtcpnt_File_View = new DataAccessProgramView(new NameInfo("vw_cis_Prtcpnt_File_View", "CIS-PRTCPNT-FILE-VIEW"), "CIS_PRTCPNT_FILE_12", 
            "CIS_PRTCPNT_FILE", DdmPeriodicGroups.getInstance().getGroups("CIS_PRTCPNT_FILE_12"));
        cis_Prtcpnt_File_View_Cis_Pin_Nbr = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Pin_Nbr", "CIS-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "CIS_PIN_NBR");
        cis_Prtcpnt_File_View_Cis_Tiaa_Doi = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Tiaa_Doi", "CIS-TIAA-DOI", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_TIAA_DOI");
        cis_Prtcpnt_File_View_Cis_Rqst_Id_Key = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Rqst_Id_Key", "CIS-RQST-ID-KEY", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "CIS_RQST_ID_KEY");
        cis_Prtcpnt_File_View_Cis_Status_Cd = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Status_Cd", "CIS-STATUS-CD", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_STATUS_CD");
        cis_Prtcpnt_File_View_Cis_Cref_Doi = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Cref_Doi", "CIS-CREF-DOI", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_CREF_DOI");
        cis_Prtcpnt_File_View_Cis_Rqst_Id = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Rqst_Id", "CIS-RQST-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CIS_RQST_ID");
        cis_Prtcpnt_File_View_Cis_Opn_Clsd_Ind = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Opn_Clsd_Ind", "CIS-OPN-CLSD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_OPN_CLSD_IND");
        cis_Prtcpnt_File_View_Cis_Cntrct_Type = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Cntrct_Type", "CIS-CNTRCT-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_CNTRCT_TYPE");
        cis_Prtcpnt_File_View_Cis_Cntrct_Print_Dte = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Cntrct_Print_Dte", 
            "CIS-CNTRCT-PRINT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CIS_CNTRCT_PRINT_DTE");
        cis_Prtcpnt_File_View_Cis_Appl_Rcvd_Dte = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Appl_Rcvd_Dte", "CIS-APPL-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_APPL_RCVD_DTE");
        cis_Prtcpnt_File_View_Cis_Appl_Rcvd_User_Id = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Appl_Rcvd_User_Id", 
            "CIS-APPL-RCVD-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_APPL_RCVD_USER_ID");
        cis_Prtcpnt_File_View_Cis_Appl_Entry_User_Id = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Appl_Entry_User_Id", 
            "CIS-APPL-ENTRY-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_APPL_ENTRY_USER_ID");
        cis_Prtcpnt_File_View_Cis_Annty_Option = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Annty_Option", "CIS-ANNTY-OPTION", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_ANNTY_OPTION");
        cis_Prtcpnt_File_View_Cis_Pymnt_Mode = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Pymnt_Mode", "CIS-PYMNT-MODE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_PYMNT_MODE");
        cis_Prtcpnt_File_View_Cis_Annty_Start_Dte = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Annty_Start_Dte", 
            "CIS-ANNTY-START-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CIS_ANNTY_START_DTE");
        cis_Prtcpnt_File_View_Cis_Annty_End_Dte = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Annty_End_Dte", "CIS-ANNTY-END-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_ANNTY_END_DTE");
        cis_Prtcpnt_File_View_Cis_Grnted_Period_Yrs = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Grnted_Period_Yrs", 
            "CIS-GRNTED-PERIOD-YRS", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_GRNTED_PERIOD_YRS");
        cis_Prtcpnt_File_View_Cis_Grnted_Period_Dys = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Grnted_Period_Dys", 
            "CIS-GRNTED-PERIOD-DYS", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CIS_GRNTED_PERIOD_DYS");
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Prfx = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Scnd_Annt_Prfx", "CIS-SCND-ANNT-PRFX", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_PRFX");
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Frst_Nme = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Scnd_Annt_Frst_Nme", 
            "CIS-SCND-ANNT-FRST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_FRST_NME");
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Mid_Nme = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Scnd_Annt_Mid_Nme", 
            "CIS-SCND-ANNT-MID-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_MID_NME");
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Lst_Nme = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Scnd_Annt_Lst_Nme", 
            "CIS-SCND-ANNT-LST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_LST_NME");
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Sffx = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Scnd_Annt_Sffx", "CIS-SCND-ANNT-SFFX", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_SFFX");
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Ssn = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Scnd_Annt_Ssn", "CIS-SCND-ANNT-SSN", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_SSN");
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Dob = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Scnd_Annt_Dob", "CIS-SCND-ANNT-DOB", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_DOB");
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Sex_Cde = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Scnd_Annt_Sex_Cde", 
            "CIS-SCND-ANNT-SEX-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_SEX_CDE");
        cis_Prtcpnt_File_View_Cis_Grnted_Grd_Amt = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Grnted_Grd_Amt", "CIS-GRNTED-GRD-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_GRNTED_GRD_AMT");
        cis_Prtcpnt_File_View_Cis_Grnted_Std_Amt = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Grnted_Std_Amt", "CIS-GRNTED-STD-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_GRNTED_STD_AMT");
        cis_Prtcpnt_File_View_Count_Castcis_Tiaa_Commuted_Info = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Count_Castcis_Tiaa_Commuted_Info", 
            "C*CIS-TIAA-COMMUTED-INFO", RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO");

        cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info = vw_cis_Prtcpnt_File_View.getRecord().newGroupInGroup("cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info", 
            "CIS-TIAA-COMMUTED-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO");
        cis_Prtcpnt_File_View_Cis_Comut_Grnted_Amt = cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Comut_Grnted_Amt", 
            "CIS-COMUT-GRNTED-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_GRNTED_AMT", 
            "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO");
        cis_Prtcpnt_File_View_Cis_Comut_Int_Rate = cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Comut_Int_Rate", 
            "CIS-COMUT-INT-RATE", FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_INT_RATE", 
            "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO");
        cis_Prtcpnt_File_View_Cis_Comut_Pymt_Method = cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Comut_Pymt_Method", 
            "CIS-COMUT-PYMT-METHOD", FieldType.STRING, 8, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_PYMT_METHOD", 
            "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO");
        cis_Prtcpnt_File_View_Cis_Grnted_Int_Rate = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Grnted_Int_Rate", 
            "CIS-GRNTED-INT-RATE", FieldType.NUMERIC, 5, 3, RepeatingFieldStrategy.None, "CIS_GRNTED_INT_RATE");
        cis_Prtcpnt_File_View_Cis_Surv_Redct_Amt = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Surv_Redct_Amt", "CIS-SURV-REDCT-AMT", 
            FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, "CIS_SURV_REDCT_AMT");
        cis_Prtcpnt_File_View_Count_Castcis_Da_Tiaa_Cntrcts_Rqst = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Count_Castcis_Da_Tiaa_Cntrcts_Rqst", 
            "C*CIS-DA-TIAA-CNTRCTS-RQST", RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");

        cis_Prtcpnt_File_View_Cis_Da_Tiaa_Cntrcts_Rqst = vw_cis_Prtcpnt_File_View.getRecord().newGroupInGroup("cis_Prtcpnt_File_View_Cis_Da_Tiaa_Cntrcts_Rqst", 
            "CIS-DA-TIAA-CNTRCTS-RQST", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Prtcpnt_File_View_Cis_Da_Tiaa_Nbr = cis_Prtcpnt_File_View_Cis_Da_Tiaa_Cntrcts_Rqst.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Da_Tiaa_Nbr", 
            "CIS-DA-TIAA-NBR", FieldType.STRING, 10, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_TIAA_NBR", 
            "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Prtcpnt_File_View_Cis_Da_Tiaa_Proceeds_Amt = cis_Prtcpnt_File_View_Cis_Da_Tiaa_Cntrcts_Rqst.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Da_Tiaa_Proceeds_Amt", 
            "CIS-DA-TIAA-PROCEEDS-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_TIAA_PROCEEDS_AMT", 
            "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Prtcpnt_File_View_Cis_Da_Rea_Proceeds_Amt = cis_Prtcpnt_File_View_Cis_Da_Tiaa_Cntrcts_Rqst.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Da_Rea_Proceeds_Amt", 
            "CIS-DA-REA-PROCEEDS-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_REA_PROCEEDS_AMT", 
            "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Prtcpnt_File_View_Count_Castcis_Da_Cref_Cntrcts_Rqst = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Count_Castcis_Da_Cref_Cntrcts_Rqst", 
            "C*CIS-DA-CREF-CNTRCTS-RQST", RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_DA_CREF_CNTRCTS_RQST");

        cis_Prtcpnt_File_View_Cis_Da_Cref_Cntrcts_Rqst = vw_cis_Prtcpnt_File_View.getRecord().newGroupInGroup("cis_Prtcpnt_File_View_Cis_Da_Cref_Cntrcts_Rqst", 
            "CIS-DA-CREF-CNTRCTS-RQST", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_DA_CREF_CNTRCTS_RQST");
        cis_Prtcpnt_File_View_Cis_Da_Cert_Nbr = cis_Prtcpnt_File_View_Cis_Da_Cref_Cntrcts_Rqst.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Da_Cert_Nbr", 
            "CIS-DA-CERT-NBR", FieldType.STRING, 10, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_CERT_NBR", 
            "CIS_PRTCPNT_FILE_CIS_DA_CREF_CNTRCTS_RQST");
        cis_Prtcpnt_File_View_Cis_Da_Cref_Proceeds_Amt = cis_Prtcpnt_File_View_Cis_Da_Cref_Cntrcts_Rqst.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Da_Cref_Proceeds_Amt", 
            "CIS-DA-CREF-PROCEEDS-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_CREF_PROCEEDS_AMT", 
            "CIS_PRTCPNT_FILE_CIS_DA_CREF_CNTRCTS_RQST");
        cis_Prtcpnt_File_View_Count_Castcis_Cref_Annty_Pymnt = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Count_Castcis_Cref_Annty_Pymnt", 
            "C*CIS-CREF-ANNTY-PYMNT", RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");

        cis_Prtcpnt_File_View_Cis_Cref_Annty_Pymnt = vw_cis_Prtcpnt_File_View.getRecord().newGroupInGroup("cis_Prtcpnt_File_View_Cis_Cref_Annty_Pymnt", 
            "CIS-CREF-ANNTY-PYMNT", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Prtcpnt_File_View_Cis_Cref_Acct_Cde = cis_Prtcpnt_File_View_Cis_Cref_Annty_Pymnt.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Cref_Acct_Cde", 
            "CIS-CREF-ACCT-CDE", FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CREF_ACCT_CDE", 
            "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Prtcpnt_File_View_Cis_Cref_Mnthly_Nbr_Units = cis_Prtcpnt_File_View_Cis_Cref_Annty_Pymnt.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Cref_Mnthly_Nbr_Units", 
            "CIS-CREF-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CREF_MNTHLY_NBR_UNITS", 
            "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Prtcpnt_File_View_Cis_Cref_Annual_Nbr_Units = cis_Prtcpnt_File_View_Cis_Cref_Annty_Pymnt.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Cref_Annual_Nbr_Units", 
            "CIS-CREF-ANNUAL-NBR-UNITS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CREF_ANNUAL_NBR_UNITS", 
            "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Prtcpnt_File_View_Cis_Cref_Annty_Amt = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Cref_Annty_Amt", "CIS-CREF-ANNTY-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_CREF_ANNTY_AMT");
        cis_Prtcpnt_File_View_Cis_Rea_Annty_Amt = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Rea_Annty_Amt", "CIS-REA-ANNTY-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_REA_ANNTY_AMT");
        cis_Prtcpnt_File_View_Cis_Rea_Surv_Nbr_Units = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Rea_Surv_Nbr_Units", 
            "CIS-REA-SURV-NBR-UNITS", FieldType.NUMERIC, 11, 3, RepeatingFieldStrategy.None, "CIS_REA_SURV_NBR_UNITS");
        cis_Prtcpnt_File_View_Cis_Rea_Mnthly_Nbr_Units = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Rea_Mnthly_Nbr_Units", 
            "CIS-REA-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 11, 3, RepeatingFieldStrategy.None, "CIS_REA_MNTHLY_NBR_UNITS");
        cis_Prtcpnt_File_View_Cis_Rea_Annual_Nbr_Units = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Rea_Annual_Nbr_Units", 
            "CIS-REA-ANNUAL-NBR-UNITS", FieldType.NUMERIC, 11, 3, RepeatingFieldStrategy.None, "CIS_REA_ANNUAL_NBR_UNITS");
        cis_Prtcpnt_File_View_Cis_Cntrct_Apprvl_Ind = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Cntrct_Apprvl_Ind", 
            "CIS-CNTRCT-APPRVL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_CNTRCT_APPRVL_IND");
        cis_Prtcpnt_File_View_Cis_Orig_Issue_State = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Orig_Issue_State", 
            "CIS-ORIG-ISSUE-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_ORIG_ISSUE_STATE");
        cis_Prtcpnt_File_View_Cis_Issue_State_Cd = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Issue_State_Cd", "CIS-ISSUE-STATE-CD", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_ISSUE_STATE_CD");
        cis_Prtcpnt_File_View_Cis_Lob = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Lob", "CIS-LOB", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_LOB");
        cis_Prtcpnt_File_View_Cis_Lob_Type = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Lob_Type", "CIS-LOB-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_LOB_TYPE");
        cis_Prtcpnt_File_View_Cis_Mail_Instructions = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Mail_Instructions", 
            "CIS-MAIL-INSTRUCTIONS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_MAIL_INSTRUCTIONS");
        cis_Prtcpnt_File_View_Cis_Pull_Code = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Pull_Code", "CIS-PULL-CODE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CIS_PULL_CODE");

        cis_Prtcpnt_File_View_Cis_Address_Info = vw_cis_Prtcpnt_File_View.getRecord().newGroupInGroup("cis_Prtcpnt_File_View_Cis_Address_Info", "CIS-ADDRESS-INFO", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Address_Chg_Ind = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Address_Chg_Ind", 
            "CIS-ADDRESS-CHG-IND", FieldType.STRING, 1, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_ADDRESS_CHG_IND", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Address_Dest_Name = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Address_Dest_Name", 
            "CIS-ADDRESS-DEST-NAME", FieldType.STRING, 35, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_ADDRESS_DEST_NAME", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Address_TxtMuGroup = cis_Prtcpnt_File_View_Cis_Address_Info.newGroupInGroup("CIS_PRTCPNT_FILE_VIEW_CIS_ADDRESS_TXTMuGroup", 
            "CIS_ADDRESS_TXTMuGroup", RepeatingFieldStrategy.PeriodicGroupSubTableFieldArray, "CIS_PRTCPNT_FILE_CIS_ADDRESS_TXT", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Address_Txt = cis_Prtcpnt_File_View_Cis_Address_TxtMuGroup.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Address_Txt", 
            "CIS-ADDRESS-TXT", FieldType.STRING, 35, new DbsArrayController(1, 3, 1, 5) , RepeatingFieldStrategy.SubTableFieldArray, "CIS_ADDRESS_TXT", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Zip_Code = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Zip_Code", "CIS-ZIP-CODE", 
            FieldType.STRING, 9, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_ZIP_CODE", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Bank_Pymnt_Acct_Nmbr = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Bank_Pymnt_Acct_Nmbr", 
            "CIS-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 21, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_BANK_PYMNT_ACCT_NMBR", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Bank_Aba_Acct_Nmbr = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Bank_Aba_Acct_Nmbr", 
            "CIS-BANK-ABA-ACCT-NMBR", FieldType.STRING, 9, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_BANK_ABA_ACCT_NMBR", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Stndrd_Trn_Cd = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Stndrd_Trn_Cd", 
            "CIS-STNDRD-TRN-CD", FieldType.STRING, 2, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_STNDRD_TRN_CD", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Finalist_Reason_Cd = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Finalist_Reason_Cd", 
            "CIS-FINALIST-REASON-CD", FieldType.STRING, 10, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_FINALIST_REASON_CD", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Addr_Usage_Code = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Addr_Usage_Code", 
            "CIS-ADDR-USAGE-CODE", FieldType.STRING, 1, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_ADDR_USAGE_CODE", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Checking_Saving_Cd = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Checking_Saving_Cd", 
            "CIS-CHECKING-SAVING-CD", FieldType.STRING, 1, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CHECKING_SAVING_CD", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Addr_Stndrd_Code = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Addr_Stndrd_Code", 
            "CIS-ADDR-STNDRD-CODE", FieldType.STRING, 1, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_ADDR_STNDRD_CODE", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Stndrd_Overide = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Stndrd_Overide", 
            "CIS-STNDRD-OVERIDE", FieldType.STRING, 1, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_STNDRD_OVERIDE", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Postal_Data_Fields = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Postal_Data_Fields", 
            "CIS-POSTAL-DATA-FIELDS", FieldType.STRING, 44, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_POSTAL_DATA_FIELDS", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Geographic_Cd = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Geographic_Cd", 
            "CIS-GEOGRAPHIC-CD", FieldType.STRING, 2, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_GEOGRAPHIC_CD", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Rtb_Pct = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Pct", "CIS-RTB-PCT", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CIS_RTB_PCT");
        cis_Prtcpnt_File_View_Cis_Rtb_Amt = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Amt", "CIS-RTB-AMT", FieldType.NUMERIC, 
            11, 2, RepeatingFieldStrategy.None, "CIS_RTB_AMT");

        cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data = vw_cis_Prtcpnt_File_View.getRecord().newGroupInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data", "CIS-RTB-DEST-DATA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_View_Cis_Rtb_Dest_Pct = cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Dest_Pct", 
            "CIS-RTB-DEST-PCT", FieldType.NUMERIC, 3, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_DEST_PCT", 
            "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_View_Cis_Rtb_Dest_Amt = cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Dest_Amt", 
            "CIS-RTB-DEST-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_DEST_AMT", 
            "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_View_Cis_Rtb_Dest_Nme = cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Dest_Nme", 
            "CIS-RTB-DEST-NME", FieldType.STRING, 35, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_DEST_NME", 
            "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_View_Cis_Rtb_Dest_AddrMuGroup = cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data.newGroupInGroup("CIS_PRTCPNT_FILE_VIEW_CIS_RTB_DEST_ADDRMuGroup", 
            "CIS_RTB_DEST_ADDRMuGroup", RepeatingFieldStrategy.PeriodicGroupSubTableFieldArray, "CIS_PRTCPNT_FILE_CIS_RTB_DEST_ADDR", "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_View_Cis_Rtb_Dest_Addr = cis_Prtcpnt_File_View_Cis_Rtb_Dest_AddrMuGroup.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Dest_Addr", 
            "CIS-RTB-DEST-ADDR", FieldType.STRING, 35, new DbsArrayController(1, 3, 1, 5) , RepeatingFieldStrategy.SubTableFieldArray, "CIS_RTB_DEST_ADDR", 
            "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_View_Cis_Rtb_Bank_Accnt_Nbr = cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Bank_Accnt_Nbr", 
            "CIS-RTB-BANK-ACCNT-NBR", FieldType.STRING, 21, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_BANK_ACCNT_NBR", 
            "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_View_Cis_Rtb_Trnst_Nbr = cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Trnst_Nbr", 
            "CIS-RTB-TRNST-NBR", FieldType.NUMERIC, 9, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_TRNST_NBR", 
            "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_View_Cis_Rtb_Dest_Zip = cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Dest_Zip", 
            "CIS-RTB-DEST-ZIP", FieldType.STRING, 9, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_DEST_ZIP", 
            "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_View_Cis_Rtb_Dest_Type_Cd = cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Dest_Type_Cd", 
            "CIS-RTB-DEST-TYPE-CD", FieldType.STRING, 1, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_DEST_TYPE_CD", 
            "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_View_Cis_Trnsf_Flag = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Trnsf_Flag", "CIS-TRNSF-FLAG", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_TRNSF_FLAG");
        cis_Prtcpnt_File_View_Cis_Trnsf_Cert_Nbr = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Trnsf_Cert_Nbr", "CIS-TRNSF-CERT-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_TRNSF_CERT_NBR");
        cis_Prtcpnt_File_View_Cis_Trnsf_Tiaa_Rea_Nbr = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Trnsf_Tiaa_Rea_Nbr", 
            "CIS-TRNSF-TIAA-REA-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_TRNSF_TIAA_REA_NBR");
        cis_Prtcpnt_File_View_Cis_Mdo_Traditional_Amt = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Mdo_Traditional_Amt", 
            "CIS-MDO-TRADITIONAL-AMT", FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_MDO_TRADITIONAL_AMT");

        cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info_2 = vw_cis_Prtcpnt_File_View.getRecord().newGroupInGroup("cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info_2", 
            "CIS-TIAA-COMMUTED-INFO-2", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_File_View_Cis_Comut_Grnted_Amt_2 = cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info_2.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Comut_Grnted_Amt_2", 
            "CIS-COMUT-GRNTED-AMT-2", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_GRNTED_AMT_2", 
            "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_File_View_Cis_Comut_Int_Rate_2 = cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info_2.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Comut_Int_Rate_2", 
            "CIS-COMUT-INT-RATE-2", FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_INT_RATE_2", 
            "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_File_View_Cis_Comut_Pymt_Method_2 = cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info_2.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Comut_Pymt_Method_2", 
            "CIS-COMUT-PYMT-METHOD-2", FieldType.STRING, 8, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_PYMT_METHOD_2", 
            "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_File_View_Cis_Comut_Mortality_Basis = cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info_2.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Comut_Mortality_Basis", 
            "CIS-COMUT-MORTALITY-BASIS", FieldType.STRING, 30, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_MORTALITY_BASIS", 
            "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_File_View_Cis_Sg_Text_Udf_1 = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Sg_Text_Udf_1", "CIS-SG-TEXT-UDF-1", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_SG_TEXT_UDF_1");
        cis_Prtcpnt_File_View_Cis_Tacc_Annty_Amt = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Tacc_Annty_Amt", "CIS-TACC-ANNTY-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_TACC_ANNTY_AMT");

        cis_Prtcpnt_File_View_Cis_Tacc_Fund_Info = vw_cis_Prtcpnt_File_View.getRecord().newGroupInGroup("cis_Prtcpnt_File_View_Cis_Tacc_Fund_Info", "CIS-TACC-FUND-INFO", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Prtcpnt_File_View_Cis_Tacc_Ind = cis_Prtcpnt_File_View_Cis_Tacc_Fund_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Tacc_Ind", "CIS-TACC-IND", 
            FieldType.STRING, 4, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TACC_IND", "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Prtcpnt_File_View_Cis_Tacc_Account = cis_Prtcpnt_File_View_Cis_Tacc_Fund_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Tacc_Account", 
            "CIS-TACC-ACCOUNT", FieldType.STRING, 10, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TACC_ACCOUNT", 
            "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Prtcpnt_File_View_Cis_Tacc_Mnthly_Nbr_Units = cis_Prtcpnt_File_View_Cis_Tacc_Fund_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Tacc_Mnthly_Nbr_Units", 
            "CIS-TACC-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TACC_MNTHLY_NBR_UNITS", 
            "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Prtcpnt_File_View_Cis_Tacc_Annual_Nbr_Units = cis_Prtcpnt_File_View_Cis_Tacc_Fund_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Tacc_Annual_Nbr_Units", 
            "CIS-TACC-ANNUAL-NBR-UNITS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TACC_ANNUAL_NBR_UNITS", 
            "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Prtcpnt_File_View_Cis_Institution_Name = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Institution_Name", 
            "CIS-INSTITUTION-NAME", FieldType.STRING, 72, RepeatingFieldStrategy.None, "CIS_INSTITUTION_NAME");
        cis_Prtcpnt_File_View_Cis_Four_Fifty_Seven_Ind = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Four_Fifty_Seven_Ind", 
            "CIS-FOUR-FIFTY-SEVEN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_FOUR_FIFTY_SEVEN_IND");
        registerRecord(vw_cis_Prtcpnt_File_View);

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        registerRecord(vw_iaa_Cntrct);

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        registerRecord(vw_naz_Table_Ddm);

        pnd_Naz_Table_Key = localVariables.newFieldInRecord("pnd_Naz_Table_Key", "#NAZ-TABLE-KEY", FieldType.STRING, 30);

        pnd_Naz_Table_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Naz_Table_Key__R_Field_1", "REDEFINE", pnd_Naz_Table_Key);
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind = pnd_Naz_Table_Key__R_Field_1.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind", "#NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id = pnd_Naz_Table_Key__R_Field_1.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id", "#NAZ-TABLE-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id = pnd_Naz_Table_Key__R_Field_1.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id", "#NAZ-TABLE-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id = pnd_Naz_Table_Key__R_Field_1.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id", "#NAZ-TABLE-LVL3-ID", 
            FieldType.STRING, 20);
        pnd_End_Date = localVariables.newFieldInRecord("pnd_End_Date", "#END-DATE", FieldType.DATE);
        pnd_Annty_Strt_Dte = localVariables.newFieldInRecord("pnd_Annty_Strt_Dte", "#ANNTY-STRT-DTE", FieldType.STRING, 8);

        pnd_Annty_Strt_Dte__R_Field_2 = localVariables.newGroupInRecord("pnd_Annty_Strt_Dte__R_Field_2", "REDEFINE", pnd_Annty_Strt_Dte);
        pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Cc = pnd_Annty_Strt_Dte__R_Field_2.newFieldInGroup("pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Cc", "#ANNTY-STRT-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Yy = pnd_Annty_Strt_Dte__R_Field_2.newFieldInGroup("pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Yy", "#ANNTY-STRT-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Mm = pnd_Annty_Strt_Dte__R_Field_2.newFieldInGroup("pnd_Annty_Strt_Dte_Pnd_Annty_Strt_Dte_Mm", "#ANNTY-STRT-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Annty_Strt_Dte_Fillerx = pnd_Annty_Strt_Dte__R_Field_2.newFieldInGroup("pnd_Annty_Strt_Dte_Fillerx", "FILLERX", FieldType.NUMERIC, 2);
        pnd_Ok_Sw = localVariables.newFieldInRecord("pnd_Ok_Sw", "#OK-SW", FieldType.STRING, 1);
        pnd_Counter = localVariables.newFieldInRecord("pnd_Counter", "#COUNTER", FieldType.NUMERIC, 3);
        pnd_Counter_Read = localVariables.newFieldInRecord("pnd_Counter_Read", "#COUNTER-READ", FieldType.NUMERIC, 3);
        pnd_Counter_Compress = localVariables.newFieldInRecord("pnd_Counter_Compress", "#COUNTER-COMPRESS", FieldType.NUMERIC, 3);
        pnd_Counter_Start = localVariables.newFieldInRecord("pnd_Counter_Start", "#COUNTER-START", FieldType.NUMERIC, 3);
        pnd_Counter_End = localVariables.newFieldInRecord("pnd_Counter_End", "#COUNTER-END", FieldType.NUMERIC, 3);
        pnd_Counter_Cis_T = localVariables.newFieldInRecord("pnd_Counter_Cis_T", "#COUNTER-CIS-T", FieldType.NUMERIC, 3);
        pnd_Counter_Cis_C = localVariables.newFieldInRecord("pnd_Counter_Cis_C", "#COUNTER-CIS-C", FieldType.NUMERIC, 3);
        pnd_Total_Processed = localVariables.newFieldInRecord("pnd_Total_Processed", "#TOTAL-PROCESSED", FieldType.NUMERIC, 7);
        pnd_Error_Ind = localVariables.newFieldInRecord("pnd_Error_Ind", "#ERROR-IND", FieldType.STRING, 1);
        pnd_Cntrl_Total = localVariables.newFieldInRecord("pnd_Cntrl_Total", "#CNTRL-TOTAL", FieldType.NUMERIC, 7);
        pnd_Cntrl_Ok = localVariables.newFieldInRecord("pnd_Cntrl_Ok", "#CNTRL-OK", FieldType.STRING, 1);
        pnd_Rtb_Count = localVariables.newFieldInRecord("pnd_Rtb_Count", "#RTB-COUNT", FieldType.NUMERIC, 3);
        pnd_Tiaa_Ia_No = localVariables.newFieldInRecord("pnd_Tiaa_Ia_No", "#TIAA-IA-NO", FieldType.STRING, 10);
        pnd_Cref_Ia_No = localVariables.newFieldInRecord("pnd_Cref_Ia_No", "#CREF-IA-NO", FieldType.STRING, 10);
        pnd_Cis_Cref_Annty_Amt = localVariables.newFieldInRecord("pnd_Cis_Cref_Annty_Amt", "#CIS-CREF-ANNTY-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Cis_Rea_Annty_Amt = localVariables.newFieldInRecord("pnd_Cis_Rea_Annty_Amt", "#CIS-REA-ANNTY-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Cis_Acc_Annty_Amt = localVariables.newFieldInRecord("pnd_Cis_Acc_Annty_Amt", "#CIS-ACC-ANNTY-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Cis_Cref_Number = localVariables.newFieldInRecord("pnd_Cis_Cref_Number", "#CIS-CREF-NUMBER", FieldType.STRING, 10);
        pnd_Cis_Rea_Number = localVariables.newFieldInRecord("pnd_Cis_Rea_Number", "#CIS-REA-NUMBER", FieldType.STRING, 10);
        pnd_Cis_Tiaa_Number = localVariables.newFieldInRecord("pnd_Cis_Tiaa_Number", "#CIS-TIAA-NUMBER", FieldType.STRING, 10);
        pnd_Cis_Total_Da_Cref = localVariables.newFieldInRecord("pnd_Cis_Total_Da_Cref", "#CIS-TOTAL-DA-CREF", FieldType.NUMERIC, 11, 2);
        pnd_Cis_Total_Da_Rea = localVariables.newFieldInRecord("pnd_Cis_Total_Da_Rea", "#CIS-TOTAL-DA-REA", FieldType.NUMERIC, 11, 2);
        pnd_Cis_Total_Da_Access = localVariables.newFieldInRecord("pnd_Cis_Total_Da_Access", "#CIS-TOTAL-DA-ACCESS", FieldType.NUMERIC, 11, 2);
        pnd_Cis_Total_Da_Tiaa = localVariables.newFieldInRecord("pnd_Cis_Total_Da_Tiaa", "#CIS-TOTAL-DA-TIAA", FieldType.NUMERIC, 11, 2);
        pnd_Cis_Total_Da_Amt = localVariables.newFieldInRecord("pnd_Cis_Total_Da_Amt", "#CIS-TOTAL-DA-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Cis_Tiaa_Grd_Amt = localVariables.newFieldInRecord("pnd_Cis_Tiaa_Grd_Amt", "#CIS-TIAA-GRD-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Cis_Tiaa_Std_Amt = localVariables.newFieldInRecord("pnd_Cis_Tiaa_Std_Amt", "#CIS-TIAA-STD-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Cis_Fnl_Std_Pay_Amt = localVariables.newFieldInRecord("pnd_Cis_Fnl_Std_Pay_Amt", "#CIS-FNL-STD-PAY-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Cis_Comut_Gruar_Amt_G = localVariables.newFieldArrayInRecord("pnd_Cis_Comut_Gruar_Amt_G", "#CIS-COMUT-GRUAR-AMT-G", FieldType.NUMERIC, 11, 
            2, new DbsArrayController(1, 1));
        pnd_Cis_Comut_Int_Rate_G = localVariables.newFieldArrayInRecord("pnd_Cis_Comut_Int_Rate_G", "#CIS-COMUT-INT-RATE-G", FieldType.NUMERIC, 5, 3, 
            new DbsArrayController(1, 1));
        pnd_Cis_Comut_Payment_G = localVariables.newFieldArrayInRecord("pnd_Cis_Comut_Payment_G", "#CIS-COMUT-PAYMENT-G", FieldType.STRING, 8, new DbsArrayController(1, 
            1));
        pnd_Cis_Comut_Gruar_Amt_S = localVariables.newFieldArrayInRecord("pnd_Cis_Comut_Gruar_Amt_S", "#CIS-COMUT-GRUAR-AMT-S", FieldType.NUMERIC, 11, 
            2, new DbsArrayController(1, 4));
        pnd_Cis_Comut_Int_Rate_S = localVariables.newFieldArrayInRecord("pnd_Cis_Comut_Int_Rate_S", "#CIS-COMUT-INT-RATE-S", FieldType.NUMERIC, 5, 3, 
            new DbsArrayController(1, 4));
        pnd_Cis_Comut_Payment_S = localVariables.newFieldArrayInRecord("pnd_Cis_Comut_Payment_S", "#CIS-COMUT-PAYMENT-S", FieldType.STRING, 8, new DbsArrayController(1, 
            4));
        pnd_Control_Date_Numeric = localVariables.newFieldInRecord("pnd_Control_Date_Numeric", "#CONTROL-DATE-NUMERIC", FieldType.NUMERIC, 8);

        pnd_Control_Date_Numeric__R_Field_3 = localVariables.newGroupInRecord("pnd_Control_Date_Numeric__R_Field_3", "REDEFINE", pnd_Control_Date_Numeric);
        pnd_Control_Date_Numeric_Pnd_Control_Date_Cc = pnd_Control_Date_Numeric__R_Field_3.newFieldInGroup("pnd_Control_Date_Numeric_Pnd_Control_Date_Cc", 
            "#CONTROL-DATE-CC", FieldType.STRING, 2);
        pnd_Control_Date_Numeric_Pnd_Control_Date_Yymmdd = pnd_Control_Date_Numeric__R_Field_3.newFieldInGroup("pnd_Control_Date_Numeric_Pnd_Control_Date_Yymmdd", 
            "#CONTROL-DATE-YYMMDD", FieldType.STRING, 6);
        pnd_Part_Date = localVariables.newFieldInRecord("pnd_Part_Date", "#PART-DATE", FieldType.STRING, 8);

        pnd_Part_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Part_Date__R_Field_4", "REDEFINE", pnd_Part_Date);
        pnd_Part_Date_Pnd_Part_Date_Numeric = pnd_Part_Date__R_Field_4.newFieldInGroup("pnd_Part_Date_Pnd_Part_Date_Numeric", "#PART-DATE-NUMERIC", FieldType.NUMERIC, 
            8);
        pnd_Hold_Last_Name = localVariables.newFieldInRecord("pnd_Hold_Last_Name", "#HOLD-LAST-NAME", FieldType.STRING, 30);
        pnd_Adat_Sffx = localVariables.newFieldArrayInRecord("pnd_Adat_Sffx", "#ADAT-SFFX", FieldType.STRING, 8, new DbsArrayController(1, 6));
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.PACKED_DECIMAL, 3);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.PACKED_DECIMAL, 3);
        pnd_Work_Method_2 = localVariables.newFieldInRecord("pnd_Work_Method_2", "#WORK-METHOD-2", FieldType.STRING, 4);

        pnd_Work_Fields = localVariables.newGroupInRecord("pnd_Work_Fields", "#WORK-FIELDS");
        pnd_Work_Fields_Pnd_Work_1_1 = pnd_Work_Fields.newFieldInGroup("pnd_Work_Fields_Pnd_Work_1_1", "#WORK-1-1", FieldType.STRING, 1);
        pnd_Work_Fields_Pnd_Work_Dot = pnd_Work_Fields.newFieldInGroup("pnd_Work_Fields_Pnd_Work_Dot", "#WORK-DOT", FieldType.STRING, 1);
        pnd_Work_Fields_Pnd_Work_2_4 = pnd_Work_Fields.newFieldInGroup("pnd_Work_Fields_Pnd_Work_2_4", "#WORK-2-4", FieldType.STRING, 3);
        pnd_Eff_Date = localVariables.newFieldInRecord("pnd_Eff_Date", "#EFF-DATE", FieldType.STRING, 8);

        pnd_Eff_Date__R_Field_5 = localVariables.newGroupInRecord("pnd_Eff_Date__R_Field_5", "REDEFINE", pnd_Eff_Date);
        pnd_Eff_Date_Pnd_Eff_Date_Numeric = pnd_Eff_Date__R_Field_5.newFieldInGroup("pnd_Eff_Date_Pnd_Eff_Date_Numeric", "#EFF-DATE-NUMERIC", FieldType.NUMERIC, 
            8);
        pnd_Cis_Super = localVariables.newFieldInRecord("pnd_Cis_Super", "#CIS-SUPER", FieldType.STRING, 35);
        pnd_Adc_Super2_From = localVariables.newFieldInRecord("pnd_Adc_Super2_From", "#ADC-SUPER2-FROM", FieldType.STRING, 37);

        pnd_Adc_Super2_From__R_Field_6 = localVariables.newGroupInRecord("pnd_Adc_Super2_From__R_Field_6", "REDEFINE", pnd_Adc_Super2_From);
        pnd_Adc_Super2_From_Pnd_Rqst_Id_F = pnd_Adc_Super2_From__R_Field_6.newFieldInGroup("pnd_Adc_Super2_From_Pnd_Rqst_Id_F", "#RQST-ID-F", FieldType.STRING, 
            35);
        pnd_Adc_Super2_From_Pnd_Adc_Sqnce_Nbr_F = pnd_Adc_Super2_From__R_Field_6.newFieldInGroup("pnd_Adc_Super2_From_Pnd_Adc_Sqnce_Nbr_F", "#ADC-SQNCE-NBR-F", 
            FieldType.NUMERIC, 2);
        pnd_Adc_Super2_To = localVariables.newFieldInRecord("pnd_Adc_Super2_To", "#ADC-SUPER2-TO", FieldType.STRING, 37);

        pnd_Adc_Super2_To__R_Field_7 = localVariables.newGroupInRecord("pnd_Adc_Super2_To__R_Field_7", "REDEFINE", pnd_Adc_Super2_To);
        pnd_Adc_Super2_To_Pnd_Rqst_Id_T = pnd_Adc_Super2_To__R_Field_7.newFieldInGroup("pnd_Adc_Super2_To_Pnd_Rqst_Id_T", "#RQST-ID-T", FieldType.STRING, 
            35);
        pnd_Adc_Super2_To_Pnd_Adc_Sqnce_Nbr_T = pnd_Adc_Super2_To__R_Field_7.newFieldInGroup("pnd_Adc_Super2_To_Pnd_Adc_Sqnce_Nbr_T", "#ADC-SQNCE-NBR-T", 
            FieldType.NUMERIC, 2);
        pnd_Adi_Super1 = localVariables.newFieldInRecord("pnd_Adi_Super1", "#ADI-SUPER1", FieldType.STRING, 41);

        pnd_Adi_Super1__R_Field_8 = localVariables.newGroupInRecord("pnd_Adi_Super1__R_Field_8", "REDEFINE", pnd_Adi_Super1);
        pnd_Adi_Super1_Pnd_Rqst_Id_Ia = pnd_Adi_Super1__R_Field_8.newFieldInGroup("pnd_Adi_Super1_Pnd_Rqst_Id_Ia", "#RQST-ID-IA", FieldType.STRING, 35);
        pnd_Adi_Super1_Pnd_Adi_Record_Type = pnd_Adi_Super1__R_Field_8.newFieldInGroup("pnd_Adi_Super1_Pnd_Adi_Record_Type", "#ADI-RECORD-TYPE", FieldType.STRING, 
            3);
        pnd_Adi_Super1_Pnd_Adi_Sqnce_Nbr = pnd_Adi_Super1__R_Field_8.newFieldInGroup("pnd_Adi_Super1_Pnd_Adi_Sqnce_Nbr", "#ADI-SQNCE-NBR", FieldType.NUMERIC, 
            3);
        testing_Mode = localVariables.newFieldInRecord("testing_Mode", "TESTING-MODE", FieldType.BOOLEAN, 1);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Lob_Nmbr = localVariables.newFieldInRecord("pnd_Ws_Lob_Nmbr", "#WS-LOB-NMBR", FieldType.NUMERIC, 2);
        pnd_Work_Ipro_Summary_Guar_Amount = localVariables.newFieldInRecord("pnd_Work_Ipro_Summary_Guar_Amount", "#WORK-IPRO-SUMMARY-GUAR-AMOUNT", FieldType.NUMERIC, 
            11, 2);
        pnd_Work_Tpa_Summary_Guar_Amount = localVariables.newFieldInRecord("pnd_Work_Tpa_Summary_Guar_Amount", "#WORK-TPA-SUMMARY-GUAR-AMOUNT", FieldType.NUMERIC, 
            11, 2);
        pnd_Temp_Date_A = localVariables.newFieldInRecord("pnd_Temp_Date_A", "#TEMP-DATE-A", FieldType.STRING, 8);

        pnd_Temp_Date_A__R_Field_9 = localVariables.newGroupInRecord("pnd_Temp_Date_A__R_Field_9", "REDEFINE", pnd_Temp_Date_A);
        pnd_Temp_Date_A_Pnd_Temp_Date_N = pnd_Temp_Date_A__R_Field_9.newFieldInGroup("pnd_Temp_Date_A_Pnd_Temp_Date_N", "#TEMP-DATE-N", FieldType.NUMERIC, 
            8);
        pnd_Cis_Sg_Text_Udf_1 = localVariables.newFieldInRecord("pnd_Cis_Sg_Text_Udf_1", "#CIS-SG-TEXT-UDF-1", FieldType.STRING, 10);

        pnd_Cis_Sg_Text_Udf_1__R_Field_10 = localVariables.newGroupInRecord("pnd_Cis_Sg_Text_Udf_1__R_Field_10", "REDEFINE", pnd_Cis_Sg_Text_Udf_1);
        pnd_Cis_Sg_Text_Udf_1_Pnd_Atra_Ind = pnd_Cis_Sg_Text_Udf_1__R_Field_10.newFieldInGroup("pnd_Cis_Sg_Text_Udf_1_Pnd_Atra_Ind", "#ATRA-IND", FieldType.STRING, 
            1);
        pnd_Cis_Sg_Text_Udf_1_Pnd_Atra_Issue_Dte = pnd_Cis_Sg_Text_Udf_1__R_Field_10.newFieldInGroup("pnd_Cis_Sg_Text_Udf_1_Pnd_Atra_Issue_Dte", "#ATRA-ISSUE-DTE", 
            FieldType.STRING, 8);
        pnd_Idx = localVariables.newFieldInRecord("pnd_Idx", "#IDX", FieldType.NUMERIC, 2);
        pnd_Max_Cnt = localVariables.newFieldInRecord("pnd_Max_Cnt", "#MAX-CNT", FieldType.NUMERIC, 2);
        pnd_Total_Cis_Add_Requests = localVariables.newFieldInRecord("pnd_Total_Cis_Add_Requests", "#TOTAL-CIS-ADD-REQUESTS", FieldType.NUMERIC, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cis_Prtcpnt_File_View.reset();
        vw_iaa_Cntrct.reset();
        vw_naz_Table_Ddm.reset();

        ldaAdsl401.initializeValues();
        ldaAdslcntl.initializeValues();
        ldaAdsl401a.initializeValues();

        localVariables.reset();
        pnd_Max_Cnt.setInitialValue(20);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp6007() throws Exception
    {
        super("Adsp6007");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  RS1                                                                                                                                                          //Natural: FORMAT ( 1 ) LS = 132 PS = 60
        DbsUtil.callnat(Adsn888.class , getCurrentProcessState(), pdaAdsa888.getPnd_Parm_Area(), pdaAdsa888.getPnd_Nbr_Acct());                                           //Natural: CALLNAT 'ADSN888' #PARM-AREA #NBR-ACCT
        if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM NAZ-CONTROL
        sub_Naz_Control();
        if (condition(Global.isEscape())) {return;}
        testing_Mode.setValue(false);                                                                                                                                     //Natural: ASSIGN TESTING-MODE := FALSE
        if (condition(testing_Mode.getBoolean()))                                                                                                                         //Natural: IF TESTING-MODE
        {
            getReports().write(0, "PROG: ",Global.getPROGRAM());                                                                                                          //Natural: WRITE '=' *PROGRAM
            if (Global.isEscape()) return;
            getReports().write(0, "LIB: ",Global.getLIBRARY_ID());                                                                                                        //Natural: WRITE '=' *LIBRARY-ID
            if (Global.isEscape()) return;
            getReports().write(0, "DATU: ",Global.getDATU());                                                                                                             //Natural: WRITE '=' *DATU
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                         //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ010");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ010'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("SFX");                                                                                                          //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'SFX'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id.setValue("S001");                                                                                                         //Natural: ASSIGN #NAZ-TABLE-LVL3-ID := 'S001'
        pnd_D.reset();                                                                                                                                                    //Natural: RESET #D
        vw_naz_Table_Ddm.startDatabaseRead                                                                                                                                //Natural: READ NAZ-TABLE-DDM BY NAZ-TBL-SUPER3 STARTING FROM #NAZ-TABLE-KEY
        (
        "READ01",
        new Wc[] { new Wc("NAZ_TBL_SUPER3", ">=", pnd_Naz_Table_Key, WcType.BY) },
        new Oc[] { new Oc("NAZ_TBL_SUPER3", "ASC") }
        );
        READ01:
        while (condition(vw_naz_Table_Ddm.readNextRow("READ01")))
        {
            if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id) || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id))) //Natural: IF NAZ-TBL-RCRD-LVL1-ID NE #NAZ-TABLE-LVL1-ID OR NAZ-TBL-RCRD-LVL2-ID NE #NAZ-TABLE-LVL2-ID
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_D.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #D
            pnd_Adat_Sffx.getValue(pnd_D).setValue(naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt);                                                                               //Natural: ASSIGN #ADAT-SFFX ( #D ) := NAZ-TBL-RCRD-DSCRPTN-TXT
            DbsUtil.examine(new ExamineSource(pnd_Adat_Sffx.getValue(pnd_D)), new ExamineTranslate(TranslateOption.Upper));                                               //Natural: EXAMINE #ADAT-SFFX ( #D ) AND TRANSLATE INTO UPPER CASE
            getReports().write(0, "ADAT Suffix: ",pnd_Adat_Sffx.getValue(pnd_D));                                                                                         //Natural: WRITE 'ADAT Suffix: ' #ADAT-SFFX ( #D )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*                                                 /* OS - 031814 END
        if (condition(pnd_Cntrl_Ok.notEquals("Y")))                                                                                                                       //Natural: IF #CNTRL-OK NE 'Y'
        {
            getReports().write(0, "ERROR IN CONTROL RECORD SETUP ");                                                                                                      //Natural: WRITE 'ERROR IN CONTROL RECORD SETUP '
            if (Global.isEscape()) return;
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "FIELDS FROM NAZ CONTROL RECORD");                                                                                                          //Natural: WRITE 'FIELDS FROM NAZ CONTROL RECORD'
        if (Global.isEscape()) return;
        getReports().write(0, "=",ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Dte());                                                                                     //Natural: WRITE '=' ADS-CNTL-BSNSS-DTE
        if (Global.isEscape()) return;
        getReports().write(0, "=",ldaAdslcntl.getAds_Cntl_View_Ads_Cis_Intrfce_Dte());                                                                                    //Natural: WRITE '=' ADS-CIS-INTRFCE-DTE
        if (Global.isEscape()) return;
        getReports().write(0, "*");                                                                                                                                       //Natural: WRITE '*'
        if (Global.isEscape()) return;
        ldaAdsl401.getVw_ads_Prtcpnt_View().startDatabaseRead                                                                                                             //Natural: READ ADS-PRTCPNT-VIEW BY ADP-SUPER1 STARTING FROM 'O'
        (
        "PART",
        new Wc[] { new Wc("ADP_SUPER1", ">=", "O", WcType.BY) },
        new Oc[] { new Oc("ADP_SUPER1", "ASC") }
        );
        PART:
        while (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().readNextRow("PART")))
        {
            getReports().write(0, "============================================");                                                                                        //Natural: WRITE '============================================'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PART"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PART"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",ldaAdsl401.getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind());                                                                                 //Natural: WRITE '=' ADS-PRTCPNT-VIEW.ADP-OPN-CLSD-IND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PART"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PART"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                                                          //Natural: WRITE '=' ADS-PRTCPNT-VIEW.RQST-ID
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PART"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PART"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "============================================");                                                                                        //Natural: WRITE '============================================'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PART"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PART"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind().notEquals("O")))                                                                              //Natural: IF ADS-PRTCPNT-VIEW.ADP-OPN-CLSD-IND NE 'O'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Part_Date.setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                                             //Natural: MOVE EDITED ADS-PRTCPNT-VIEW.ADP-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #PART-DATE
            getReports().write(0, "FIELDS FROM NAZ PARTICIPANT RECORD");                                                                                                  //Natural: WRITE 'FIELDS FROM NAZ PARTICIPANT RECORD'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PART"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PART"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde());                                                                                     //Natural: WRITE '=' ADP-STTS-CDE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PART"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PART"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte());                                                                               //Natural: WRITE '=' ADP-LST-ACTVTY-DTE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PART"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PART"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",pnd_Part_Date_Pnd_Part_Date_Numeric);                                                                                               //Natural: WRITE '=' #PART-DATE-NUMERIC
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PART"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PART"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",ldaAdsl401.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde());                                                                                 //Natural: WRITE '=' ADP-ANNT-TYP-CDE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PART"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PART"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "*");                                                                                                                                   //Natural: WRITE '*'
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PART"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PART"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                              /* TS 111804
            if (condition(ldaAdslcntl.getAds_Cntl_View_Ads_Rpt_13().notEquals(getZero())))                                                                                //Natural: IF ADS-RPT-13 NE 0
            {
                pnd_Temp_Date_A.setValueEdited(ldaAdslcntl.getAds_Cntl_View_Ads_Rpt_13(),new ReportEditMask("YYYYMMDD"));                                                 //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #TEMP-DATE-A
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Temp_Date_A_Pnd_Temp_Date_N.setValue(ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Dte());                                                              //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #TEMP-DATE-N
            }                                                                                                                                                             //Natural: END-IF
            //*                                              /* TS 111804
            //*  TS 111804
            if (condition(!((ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().equals("C10")) && (ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte().greater(ldaAdslcntl.getAds_Cntl_View_Ads_Cis_Intrfce_Dte()))  //Natural: ACCEPT IF ( ADS-PRTCPNT-VIEW.ADP-STTS-CDE = 'C10' ) AND ( ADS-PRTCPNT-VIEW.ADP-LST-ACTVTY-DTE > ADS-CNTL-VIEW.ADS-CIS-INTRFCE-DTE ) AND #PART-DATE-NUMERIC LE #TEMP-DATE-N AND ( ADS-PRTCPNT-VIEW.ADP-ANNT-TYP-CDE = 'M' )
                && pnd_Part_Date_Pnd_Part_Date_Numeric.lessOrEqual(pnd_Temp_Date_A_Pnd_Temp_Date_N) && (ldaAdsl401.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde().equals("M")))))
            {
                continue;
            }
            //* *    #PART-DATE-NUMERIC LE ADS-CNTL-VIEW.ADS-CNTL-BSNSS-DTE
            pnd_Error_Ind.reset();                                                                                                                                        //Natural: RESET #ERROR-IND
            getReports().write(0, "ACCEPTED RQST ID ",ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                                          //Natural: WRITE 'ACCEPTED RQST ID ' ADS-PRTCPNT-VIEW.RQST-ID
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PART"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PART"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Cis_Super.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                                                             //Natural: MOVE ADS-PRTCPNT-VIEW.RQST-ID TO #CIS-SUPER
            vw_cis_Prtcpnt_File_View.startDatabaseFind                                                                                                                    //Natural: FIND CIS-PRTCPNT-FILE-VIEW WITH CIS-RQST-ID-KEY = #CIS-SUPER
            (
            "CIS",
            new Wc[] { new Wc("CIS_RQST_ID_KEY", "=", pnd_Cis_Super, WcType.WITH) }
            );
            CIS:
            while (condition(vw_cis_Prtcpnt_File_View.readNextRow("CIS", true)))
            {
                vw_cis_Prtcpnt_File_View.setIfNotFoundControlFlag(false);
                if (condition(vw_cis_Prtcpnt_File_View.getAstCOUNTER().equals(0)))                                                                                        //Natural: IF NO RECORD FOUND
                {
                    getReports().write(0, "DID NOT FIND CIS PRTCPNT RECORD FOR RQST");                                                                                    //Natural: WRITE 'DID NOT FIND CIS PRTCPNT RECORD FOR RQST'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("CIS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Error_Ind.setValue("Y");                                                                                                                          //Natural: MOVE 'Y' TO #ERROR-IND
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-NOREC
                if (condition(testing_Mode.getBoolean()))                                                                                                                 //Natural: IF TESTING-MODE
                {
                    getReports().write(0, " CIS-GRNTED-GRD-AMT AFTER  READ ",cis_Prtcpnt_File_View_Cis_Grnted_Grd_Amt);                                                   //Natural: WRITE ' CIS-GRNTED-GRD-AMT AFTER  READ ' CIS-GRNTED-GRD-AMT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("CIS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, " CIS-GRNTED-STD-AMT AFTER  READ ",cis_Prtcpnt_File_View_Cis_Grnted_Std_Amt);                                                   //Natural: WRITE ' CIS-GRNTED-STD-AMT AFTER  READ ' CIS-GRNTED-STD-AMT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("CIS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                cis_Prtcpnt_File_View_Cis_Trnsf_Flag.setValue("N");                                                                                                       //Natural: MOVE 'N' TO CIS-TRNSF-FLAG
                                                                                                                                                                          //Natural: PERFORM MOVE-PARTICIPANT-FIELDS
                sub_Move_Participant_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("CIS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Total_Processed.nadd(1);                                                                                                                              //Natural: ADD 1 TO #TOTAL-PROCESSED
                                                                                                                                                                          //Natural: PERFORM CNTRCT-RECORD-LOOKUP
                sub_Cntrct_Record_Lookup();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("CIS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Error_Ind.equals("Y")))                                                                                                                 //Natural: IF #ERROR-IND = 'Y'
                {
                    pnd_Total_Processed.nsubtract(1);                                                                                                                     //Natural: SUBTRACT 1 FROM #TOTAL-PROCESSED
                    getReports().write(0, "ERROR IN CNTRCT-RECORD-LOOKUP");                                                                                               //Natural: WRITE 'ERROR IN CNTRCT-RECORD-LOOKUP'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("CIS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM IA-RESULT-LOOKUP
                sub_Ia_Result_Lookup();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("CIS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Error_Ind.equals("Y")))                                                                                                                 //Natural: IF #ERROR-IND = 'Y'
                {
                    getReports().write(0, "ERROR IN IA-RESULT-LOOKUP");                                                                                                   //Natural: WRITE 'ERROR IN IA-RESULT-LOOKUP'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("CIS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Total_Processed.nsubtract(1);                                                                                                                     //Natural: SUBTRACT 1 FROM #TOTAL-PROCESSED
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl401.getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind().setValue("C");                                                                                          //Natural: MOVE 'C' TO ADS-PRTCPNT-VIEW.ADP-OPN-CLSD-IND
                ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().setValue("C15");                                                                                            //Natural: MOVE 'C15' TO ADS-PRTCPNT-VIEW.ADP-STTS-CDE
                //*  /* 082908 START
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Sub_Plan_Nbr().getSubstring(1,3).equals("AA1") || ldaAdsl401a.getAds_Cntrct_View_Adc_Sub_Plan_Nbr().getSubstring(1,3).equals("AA2")  //Natural: IF SUBSTRING ( ADS-CNTRCT-VIEW.ADC-SUB-PLAN-NBR,1,3 ) = 'AA1' OR = 'AA2' OR = 'AA3'
                    || ldaAdsl401a.getAds_Cntrct_View_Adc_Sub_Plan_Nbr().getSubstring(1,3).equals("AA3")))
                {
                    pnd_Cis_Sg_Text_Udf_1_Pnd_Atra_Ind.setValue("A");                                                                                                     //Natural: MOVE 'A' TO #ATRA-IND
                    pnd_Cis_Sg_Text_Udf_1_Pnd_Atra_Issue_Dte.setValueEdited(ldaAdsl401a.getAds_Cntrct_View_Adc_Cntrct_Issue_Dte(),new ReportEditMask("YYYYMMDD"));        //Natural: MOVE EDITED ADS-CNTRCT-VIEW.ADC-CNTRCT-ISSUE-DTE ( EM = YYYYMMDD ) TO #ATRA-ISSUE-DTE
                    cis_Prtcpnt_File_View_Cis_Sg_Text_Udf_1.setValue(pnd_Cis_Sg_Text_Udf_1);                                                                              //Natural: MOVE #CIS-SG-TEXT-UDF-1 TO CIS-SG-TEXT-UDF-1
                }                                                                                                                                                         //Natural: END-IF
                //*  /* 082908 END
                //*    TEMP ONLY TESTING
                //*    MOVE 'I'  TO  CIS-STATUS-CD
                //*    MOVE 'IA' TO  CIS-RQST-ID
                //*    MOVE 'O'  TO  CIS-OPN-CLSD-IND
                if (condition(testing_Mode.getBoolean()))                                                                                                                 //Natural: IF TESTING-MODE
                {
                    //*      WRITE ' NAI-OPTN-CDE       BEFORE UPDT   ' ADI-OPTN-CDE
                    getReports().write(0, " CIS-GRNTED-GRD-AMT BEFORE UPDT   ",cis_Prtcpnt_File_View_Cis_Grnted_Grd_Amt);                                                 //Natural: WRITE ' CIS-GRNTED-GRD-AMT BEFORE UPDT   ' CIS-GRNTED-GRD-AMT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("CIS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, " CIS-GRNTED-STD-AMT BEFORE UPDT   ",cis_Prtcpnt_File_View_Cis_Grnted_Std_Amt);                                                 //Natural: WRITE ' CIS-GRNTED-STD-AMT BEFORE UPDT   ' CIS-GRNTED-STD-AMT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("CIS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, " CIS-REA-ANNTY-AMT  BEFORE UPDT   ",cis_Prtcpnt_File_View_Cis_Rea_Annty_Amt);                                                  //Natural: WRITE ' CIS-REA-ANNTY-AMT  BEFORE UPDT   ' CIS-REA-ANNTY-AMT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("CIS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, " CIS-CREF-ANNTY-AMT BEFORE UPDT   ",cis_Prtcpnt_File_View_Cis_Cref_Annty_Amt);                                                 //Natural: WRITE ' CIS-CREF-ANNTY-AMT BEFORE UPDT   ' CIS-CREF-ANNTY-AMT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("CIS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*   WRITE REPORT HERE
                getReports().display(1, "DA/PIN",                                                                                                                         //Natural: DISPLAY ( 1 ) 'DA/PIN' ADP-UNIQUE-ID 3X 'IA/CONTRACT' ADP-IA-TIAA-NBR ( EM = XXXXXXX-X ) 3X 'DA/CONTRACT' ADC-TIAA-NBR ( EM = XXXXXXX-X ) 3X
                		ldaAdsl401.getAds_Prtcpnt_View_Adp_Unique_Id(),new ColumnSpacing(3),"IA/CONTRACT",
                		ldaAdsl401.getAds_Prtcpnt_View_Adp_Ia_Tiaa_Nbr(), new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(3),"DA/CONTRACT",
                		ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr(), new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(3));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("CIS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Total_Cis_Add_Requests.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TOTAL-CIS-ADD-REQUESTS
                vw_cis_Prtcpnt_File_View.updateDBRow("CIS");                                                                                                              //Natural: UPDATE ( CIS. )
                ldaAdsl401.getVw_ads_Prtcpnt_View().updateDBRow("PART");                                                                                                  //Natural: UPDATE ( PART. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pdaCisa1000.getCisa1000_Pnd_Cis_Entry_Date_Time_Key().setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                 //Natural: MOVE ADS-PRTCPNT-VIEW.RQST-ID TO #CIS-ENTRY-DATE-TIME-KEY
                pdaCisa1000.getCisa1000_Pnd_Cis_Requestor().setValue("IA");                                                                                               //Natural: MOVE 'IA' TO #CIS-REQUESTOR
                pdaCisa1000.getCisa1000_Pnd_Cis_Pin_Number().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Unique_Id());                                                    //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-UNIQUE-ID TO #CIS-PIN-NUMBER
                pdaCisa1000.getCisa1000_Pnd_Cis_Soc_Sec_Number().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Frst_Annt_Ssn());                                            //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-FRST-ANNT-SSN TO #CIS-SOC-SEC-NUMBER
                pdaCisa1000.getCisa1000_Pnd_Cis_Dob().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Frst_Annt_Dte_Of_Brth());                                               //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-FRST-ANNT-DTE-OF-BRTH TO #CIS-DOB
                //*    MOVE ADI-IA-TIAA-NBR              TO  #CIS-TIAA-CNTRCT-NBR(1)
                pdaCisa1000.getCisa1000_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue(1).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Ia_Tiaa_Nbr());                                 //Natural: MOVE ADP-IA-TIAA-NBR TO #CIS-TIAA-CNTRCT-NBR ( 1 )
                //*    MOVE ADI-IA-CREF-NBR              TO  #CIS-CREF-CNTRCT-NBR
                pdaCisa1000.getCisa1000_Pnd_Cis_Cref_Cntrct_Nbr().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Ia_Cref_Nbr());                                             //Natural: MOVE ADP-IA-CREF-NBR TO #CIS-CREF-CNTRCT-NBR
                pdaCisa1000.getCisa1000_Pnd_Cis_Rqst_Log_Dte_Tme().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Mit_Log_Dte_Tme());                                        //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-MIT-LOG-DTE-TME TO #CIS-RQST-LOG-DTE-TME
                pdaCisa1000.getCisa1000_Pnd_Cis_Assign_Issue_Cntrct_Ind().setValue("I");                                                                                  //Natural: MOVE 'I' TO #CIS-ASSIGN-ISSUE-CNTRCT-IND
                pdaCisa1000.getCisa1000_Pnd_Cis_Product_Cde().setValue(" ");                                                                                              //Natural: MOVE ' ' TO #CIS-PRODUCT-CDE
                pdaCisa1000.getCisa1000_Pnd_Cis_Cntrct_Type().setValue(" ");                                                                                              //Natural: MOVE ' ' TO #CIS-CNTRCT-TYPE
                pdaCisa1000.getCisa1000_Pnd_Cis_Msg_Text().setValue(" ");                                                                                                 //Natural: MOVE ' ' TO #CIS-MSG-TEXT
                pdaCisa1000.getCisa1000_Pnd_Cis_Msg_Number().getValue("*").setValue(" ");                                                                                 //Natural: MOVE ' ' TO #CIS-MSG-NUMBER ( * )
                pdaCisa1000.getCisa1000_Pnd_Cis_Functions().getValue(6).setValue("YB");                                                                                   //Natural: MOVE 'YB' TO #CIS-FUNCTIONS ( 6 )
                pdaCisa1000.getCisa1000_Pnd_Cis_Functions().getValue(4).setValue("YB");                                                                                   //Natural: MOVE 'YB' TO #CIS-FUNCTIONS ( 4 )
                pdaCisa1000.getCisa1000_Pnd_Cis_Functions().getValue(9).setValue("YB");                                                                                   //Natural: MOVE 'YB' TO #CIS-FUNCTIONS ( 9 )
                pdaCisa1000.getCisa1000_Pnd_Cis_Mit_Function_Code().setValue("MO");                                                                                       //Natural: MOVE 'MO' TO #CIS-MIT-FUNCTION-CODE
                //*    WRITE 'BEFORE FUNCTION '   #CIS-MSG-NUMBER(*)
                DbsUtil.callnat(Cisn1000.class , getCurrentProcessState(), pdaCisa1000.getCisa1000(), pdaCisa1000.getPnd_Cis_Misc());                                     //Natural: CALLNAT 'CISN1000' USING CISA1000 #CIS-MISC
                if (condition(Global.isEscape())) return;
                getReports().write(0, "MESSAGE FROM CISN1000 ",pdaCisa1000.getCisa1000_Pnd_Cis_Msg_Text());                                                               //Natural: WRITE 'MESSAGE FROM CISN1000 ' #CIS-MSG-TEXT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("CIS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PART"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PART"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,"TOTAL CIS ADD REQUESTS:=",pnd_Total_Cis_Add_Requests);                                        //Natural: WRITE ( 1 ) /// 'TOTAL CIS ADD REQUESTS:=' #TOTAL-CIS-ADD-REQUESTS
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //* ***********************************************************************
        //* **     READ ITS CONTROL RECORD TO EXTRACT DATES                     ***
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NAZ-CONTROL
        //* ***********************************************************************
        //* ***********************************************************************
        //* **     MOVE PARTICIPANT FIELDS TO CIS KDO FILE                      ***
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-PARTICIPANT-FIELDS
        //* ***********************************************************************
        //* ***********************************************************************
        //* **     FIND DA RESULT RECORDS FROM A PARTICIPANT RECORD             ***
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CNTRCT-RECORD-LOOKUP
        //* ***********************************************************************
        //* ***********************************************************************
        //* **     MOVE TIAA DA RESULTS FIELDS TO CIS KDO FILE                  ***
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-DA-TIAA-RESULTS-FIELDS
        //* ***********************************************************************
        //* ***********************************************************************
        //* **     MOVE CREF DA RESULTS FIELDS TO CIS KDO FILE                  ***
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-DA-CREF-RESULTS-FIELDS
        //* ***********************************************************************
        //* ***********************************************************************
        //* **     FIND IA RESULT RECORDS FROM A PARTICIPANT RECORD             ***
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-RESULT-LOOKUP
        //* ***********************************************************************
        //* ***********************************************************************
        //* **     MOVE IA RESULTS FIELDS TO CIS KDO FILE                       ***
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-IA-TIAA-RESULTS-FIELDS
        //* ***********************************************************************
        //* ***********************************************************************
        //* **     MOVE IA INFO PASSED FROM ACTUARIAL MODULE TO CIS KDO FILE    ***
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-IA-TIAA-ACTUARIAL-INFO
        //* ***********************************************************************
        //* ***********************************************************************
        //* **     MOVE IA CREF RESULT FIELDS TO CIS KDO FILE                   ***
        //* ***********************************************************************
        //* ***********************************************************************
        getReports().write(0, " ****** AAA ******** ");                                                                                                                   //Natural: WRITE ' ****** AAA ******** '
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
    }
    private void sub_Naz_Control() throws Exception                                                                                                                       //Natural: NAZ-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        ldaAdslcntl.getVw_ads_Cntl_View().startDatabaseRead                                                                                                               //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "CNTRL",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        CNTRL:
        while (condition(ldaAdslcntl.getVw_ads_Cntl_View().readNextRow("CNTRL")))
        {
            if (condition(ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde().notEquals("F")))                                                                           //Natural: IF ADS-CNTL-RCRD-TYP-CDE NE 'F'
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cntrl_Total.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNTRL-TOTAL
            pnd_Cntrl_Ok.setValue("Y");                                                                                                                                   //Natural: MOVE 'Y' TO #CNTRL-OK
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Move_Participant_Fields() throws Exception                                                                                                           //Natural: MOVE-PARTICIPANT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //*  CHANGED BY P.J.C. ON 8/11/97
        pnd_Control_Date_Numeric.setValue(ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Dte());                                                                             //Natural: MOVE ADS-CNTL-VIEW.ADS-CNTL-BSNSS-DTE TO #CONTROL-DATE-NUMERIC
        cis_Prtcpnt_File_View_Cis_Cntrct_Print_Dte.setValueEdited(new ReportEditMask("YYMMDD"),pnd_Control_Date_Numeric_Pnd_Control_Date_Yymmdd);                         //Natural: MOVE EDITED #CONTROL-DATE-YYMMDD TO CIS-CNTRCT-PRINT-DTE ( EM = YYMMDD )
        if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("SL")))                                                                                      //Natural: IF ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'SL'
        {
            cis_Prtcpnt_File_View_Cis_Annty_Option.setValue("OL");                                                                                                        //Natural: MOVE 'OL' TO CIS-ANNTY-OPTION
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("LF")))                                                                                  //Natural: IF ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'LF'
            {
                cis_Prtcpnt_File_View_Cis_Annty_Option.setValue("LSF");                                                                                                   //Natural: MOVE 'LSF' TO CIS-ANNTY-OPTION
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("J")))                                                                               //Natural: IF ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'J'
                {
                    cis_Prtcpnt_File_View_Cis_Annty_Option.setValue("JS");                                                                                                //Natural: MOVE 'JS' TO CIS-ANNTY-OPTION
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("LH")))                                                                          //Natural: IF ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'LH'
                    {
                        cis_Prtcpnt_File_View_Cis_Annty_Option.setValue("LS");                                                                                            //Natural: MOVE 'LS' TO CIS-ANNTY-OPTION
                        //*  EM 072307 - START
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Optn().equals("LT")))                                                                      //Natural: IF ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN = 'LT'
                        {
                            //*  EM 072307 - END
                            cis_Prtcpnt_File_View_Cis_Annty_Option.setValue("LST");                                                                                       //Natural: MOVE 'LST' TO CIS-ANNTY-OPTION
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            cis_Prtcpnt_File_View_Cis_Annty_Option.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Optn());                                             //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN TO CIS-ANNTY-OPTION
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        cis_Prtcpnt_File_View_Cis_Appl_Rcvd_Dte.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Forms_Rcvd_Dte());                                                            //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-FORMS-RCVD-DTE TO CIS-APPL-RCVD-DTE
        cis_Prtcpnt_File_View_Cis_Appl_Rcvd_User_Id.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Entry_User_Id());                                                         //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-ENTRY-USER-ID TO CIS-APPL-RCVD-USER-ID
        if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_1().equals(1)))                                                                                       //Natural: IF ADS-PRTCPNT-VIEW.ADP-PYMNT-MODE-1 = 1
        {
            cis_Prtcpnt_File_View_Cis_Pymnt_Mode.setValue("M");                                                                                                           //Natural: MOVE 'M' TO CIS-PYMNT-MODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_1().equals(6)))                                                                                   //Natural: IF ADS-PRTCPNT-VIEW.ADP-PYMNT-MODE-1 = 6
            {
                cis_Prtcpnt_File_View_Cis_Pymnt_Mode.setValue("Q");                                                                                                       //Natural: MOVE 'Q' TO CIS-PYMNT-MODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_1().equals(7)))                                                                               //Natural: IF ADS-PRTCPNT-VIEW.ADP-PYMNT-MODE-1 = 7
                {
                    cis_Prtcpnt_File_View_Cis_Pymnt_Mode.setValue("S");                                                                                                   //Natural: MOVE 'S' TO CIS-PYMNT-MODE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_1().equals(8)))                                                                           //Natural: IF ADS-PRTCPNT-VIEW.ADP-PYMNT-MODE-1 = 8
                    {
                        cis_Prtcpnt_File_View_Cis_Pymnt_Mode.setValue("A");                                                                                               //Natural: MOVE 'A' TO CIS-PYMNT-MODE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        cis_Prtcpnt_File_View_Cis_Annty_Start_Dte.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Strt_Dte());                                                          //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-ANNTY-STRT-DTE TO CIS-ANNTY-START-DTE
        cis_Prtcpnt_File_View_Cis_Grnted_Period_Yrs.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Grntee_Period());                                                         //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-GRNTEE-PERIOD TO CIS-GRNTED-PERIOD-YRS
        cis_Prtcpnt_File_View_Cis_Grnted_Period_Dys.setValue(0);                                                                                                          //Natural: MOVE 00 TO CIS-GRNTED-PERIOD-DYS
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Frst_Nme.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Frst_Nme());                                                   //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-SCND-ANNT-FRST-NME TO CIS-SCND-ANNT-FRST-NME
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Mid_Nme.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Mid_Nme());                                                     //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-SCND-ANNT-MID-NME TO CIS-SCND-ANNT-MID-NME
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Lst_Nme.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Lst_Nme());                                                     //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-SCND-ANNT-LST-NME TO CIS-SCND-ANNT-LST-NME
        //*  OS - 031814 START
        DbsUtil.examine(new ExamineSource(cis_Prtcpnt_File_View_Cis_Scnd_Annt_Lst_Nme), new ExamineSearch(" "), new ExamineGivingPosition(pnd_A));                        //Natural: EXAMINE CIS-SCND-ANNT-LST-NME FOR ' ' GIVING POSITION #A
        if (condition(pnd_A.greater(getZero()) && pnd_A.less(30)))                                                                                                        //Natural: IF #A GT 0 AND #A LT 30
        {
            pnd_C.compute(new ComputeParameters(false, pnd_C), DbsField.add(1,(DbsField.subtract(30,pnd_A))));                                                            //Natural: ASSIGN #C := 1 + ( 30 - #A )
            if (condition(!cis_Prtcpnt_File_View_Cis_Scnd_Annt_Lst_Nme.getSubstring(pnd_A.getInt(),pnd_C.getInt()).equals(" ")))                                          //Natural: IF SUBSTR ( CIS-SCND-ANNT-LST-NME,#A,#C ) NE ' '
            {
                pnd_Hold_Last_Name.setValue(cis_Prtcpnt_File_View_Cis_Scnd_Annt_Lst_Nme.getSubstring(pnd_A.getInt(),pnd_C.getInt()));                                     //Natural: ASSIGN #HOLD-LAST-NAME := SUBSTR ( CIS-SCND-ANNT-LST-NME,#A,#C )
                DbsUtil.examine(new ExamineSource(pnd_Hold_Last_Name), new ExamineTranslate(TranslateOption.Upper));                                                      //Natural: EXAMINE #HOLD-LAST-NAME AND TRANSLATE INTO UPPER CASE
                DbsUtil.examine(new ExamineSource(pnd_Hold_Last_Name), new ExamineSearch("."), new ExamineReplace(" "));                                                  //Natural: EXAMINE #HOLD-LAST-NAME FOR '.' REPLACE WITH ' '
                pnd_Hold_Last_Name.setValue(pnd_Hold_Last_Name, MoveOption.LeftJustified);                                                                                //Natural: MOVE LEFT #HOLD-LAST-NAME TO #HOLD-LAST-NAME
                if (condition(pnd_Hold_Last_Name.equals(pnd_Adat_Sffx.getValue("*"))))                                                                                    //Natural: IF #HOLD-LAST-NAME = #ADAT-SFFX ( * )
                {
                    cis_Prtcpnt_File_View_Cis_Scnd_Annt_Sffx.setValue(cis_Prtcpnt_File_View_Cis_Scnd_Annt_Lst_Nme.getSubstring(pnd_A.getInt(),pnd_C.getInt()));           //Natural: ASSIGN CIS-SCND-ANNT-SFFX := SUBSTR ( CIS-SCND-ANNT-LST-NME,#A,#C )
                    cis_Prtcpnt_File_View_Cis_Scnd_Annt_Sffx.setValue(cis_Prtcpnt_File_View_Cis_Scnd_Annt_Sffx, MoveOption.LeftJustified);                                //Natural: MOVE LEFT CIS-SCND-ANNT-SFFX TO CIS-SCND-ANNT-SFFX
                    setValueToSubstring(" ",cis_Prtcpnt_File_View_Cis_Scnd_Annt_Lst_Nme,pnd_A.getInt(),pnd_C.getInt());                                                   //Natural: MOVE ' ' TO SUBSTR ( CIS-SCND-ANNT-LST-NME,#A,#C )
                    getReports().write(0, "=",cis_Prtcpnt_File_View_Cis_Scnd_Annt_Frst_Nme,NEWLINE,"=",cis_Prtcpnt_File_View_Cis_Scnd_Annt_Lst_Nme,NEWLINE,               //Natural: WRITE '=' CIS-SCND-ANNT-FRST-NME / '=' CIS-SCND-ANNT-LST-NME / '=' CIS-SCND-ANNT-SFFX
                        "=",cis_Prtcpnt_File_View_Cis_Scnd_Annt_Sffx);
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  OS - 031814 END
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Ssn.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Ssn());                                                             //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-SCND-ANNT-SSN TO CIS-SCND-ANNT-SSN
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Dob.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Dte_Of_Brth());                                                     //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-SCND-ANNT-DTE-OF-BRTH TO CIS-SCND-ANNT-DOB
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Sex_Cde.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Sex_Cde());                                                     //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-SCND-ANNT-SEX-CDE TO CIS-SCND-ANNT-SEX-CDE
        //*  VE ADS-PRTCPNT-VIEW.ADP-FRST-ANNT-RSDNC-CDE
        //*  ISSUE STATE CODE
        cis_Prtcpnt_File_View_Cis_Orig_Issue_State.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Orgnl_Issue_State());                                                      //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-ORGNL-ISSUE-STATE TO CIS-ORIG-ISSUE-STATE
        //*  CURRENT STATE WHERE THEY LIVE
        cis_Prtcpnt_File_View_Cis_Issue_State_Cd.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_State_Of_Issue());                                                           //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-STATE-OF-ISSUE TO CIS-ISSUE-STATE-CD
        //* *MOVE ADS-PRTCPNT-VIEW.ADP-NEW-OLD-CNTRCT-IND
        //*  MOVE 'Y'                          /* 05192005 MN
        //*  05192005 MN
        cis_Prtcpnt_File_View_Cis_Cntrct_Apprvl_Ind.setValue(" ");                                                                                                        //Natural: MOVE ' ' TO CIS-CNTRCT-APPRVL-IND
        //*  VE ADS-PRTCPNT-VIEW.ADP-PULL-IA-CNTRCT-PCKG-IND
        //*  TO CIS-PULL-CODE
        //* *
        //* *IF   ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-CHG-IND > ' '
        //* *  MOVE 'Y' TO CIS-ADDRESS-CHG-IND(1)
        //* *END-IF
        cis_Prtcpnt_File_View_Cis_Addr_Usage_Code.getValue(1).setValue("1");                                                                                              //Natural: MOVE '1' TO CIS-ADDR-USAGE-CODE ( 1 )
        cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(1,1).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt().getValue(1));                          //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( 1 ) TO CIS-ADDRESS-TXT ( 1,1 )
        cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(1,2).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt().getValue(2));                          //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( 2 ) TO CIS-ADDRESS-TXT ( 1,2 )
        cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(1,3).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt().getValue(3));                          //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( 3 ) TO CIS-ADDRESS-TXT ( 1,3 )
        cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(1,4).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt().getValue(4));                          //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( 4 ) TO CIS-ADDRESS-TXT ( 1,4 )
        cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(1,5).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt().getValue(5));                          //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( 5 ) TO CIS-ADDRESS-TXT ( 1,5 )
        cis_Prtcpnt_File_View_Cis_Zip_Code.getValue(1).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Zip());                                           //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-ZIP TO CIS-ZIP-CODE ( 1 )
        //* *
        if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Alt_Dest_Addr_Ind().equals("Y")))                                                                                //Natural: IF ADS-PRTCPNT-VIEW.ADP-ALT-DEST-ADDR-IND = 'Y'
        {
            cis_Prtcpnt_File_View_Cis_Addr_Usage_Code.getValue(2).setValue("2");                                                                                          //Natural: MOVE '2' TO CIS-ADDR-USAGE-CODE ( 2 )
            cis_Prtcpnt_File_View_Cis_Address_Chg_Ind.getValue(2).setValue("Y");                                                                                          //Natural: MOVE 'Y' TO CIS-ADDRESS-CHG-IND ( 2 )
            cis_Prtcpnt_File_View_Cis_Address_Dest_Name.getValue(2).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Alt_Dest_Nme());                                          //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-ALT-DEST-NME TO CIS-ADDRESS-DEST-NAME ( 2 )
            cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(2,1).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Alt_Dest_Addr_Txt().getValue(1));                             //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-ALT-DEST-ADDR-TXT ( 1 ) TO CIS-ADDRESS-TXT ( 2,1 )
            cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(2,2).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Alt_Dest_Addr_Txt().getValue(2));                             //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-ALT-DEST-ADDR-TXT ( 2 ) TO CIS-ADDRESS-TXT ( 2,2 )
            cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(2,3).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Alt_Dest_Addr_Txt().getValue(3));                             //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-ALT-DEST-ADDR-TXT ( 3 ) TO CIS-ADDRESS-TXT ( 2,3 )
            cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(2,4).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Alt_Dest_Addr_Txt().getValue(4));                             //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-ALT-DEST-ADDR-TXT ( 4 ) TO CIS-ADDRESS-TXT ( 2,4 )
            cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(2,5).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Alt_Dest_Addr_Txt().getValue(5));                             //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-ALT-DEST-ADDR-TXT ( 5 ) TO CIS-ADDRESS-TXT ( 2,5 )
            cis_Prtcpnt_File_View_Cis_Zip_Code.getValue(2).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Alt_Dest_Addr_Zip());                                              //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-ALT-DEST-ADDR-ZIP TO CIS-ZIP-CODE ( 2 )
            cis_Prtcpnt_File_View_Cis_Bank_Pymnt_Acct_Nmbr.getValue(2).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Alt_Dest_Acct_Nbr());                                  //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-ALT-DEST-ACCT-NBR TO CIS-BANK-PYMNT-ACCT-NMBR ( 2 )
            cis_Prtcpnt_File_View_Cis_Bank_Aba_Acct_Nmbr.getValue(2).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Alt_Dest_Trnst_Cde());                                   //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-ALT-DEST-TRNST-CDE TO CIS-BANK-ABA-ACCT-NMBR ( 2 )
            cis_Prtcpnt_File_View_Cis_Checking_Saving_Cd.getValue(2).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Alt_Dest_Acct_Typ());                                    //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-ALT-DEST-ACCT-TYP TO CIS-CHECKING-SAVING-CD ( 2 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            cis_Prtcpnt_File_View_Cis_Addr_Usage_Code.getValue(1).setValue("3");                                                                                          //Natural: MOVE '3' TO CIS-ADDR-USAGE-CODE ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Pull_Ia_Cntrct_Pckg_Rsn().equals("SPEC")))                                                                       //Natural: IF ADP-PULL-IA-CNTRCT-PCKG-RSN = 'SPEC'
        {
            cis_Prtcpnt_File_View_Cis_Mail_Instructions.setValue("S");                                                                                                    //Natural: MOVE 'S' TO CIS-MAIL-INSTRUCTIONS
            cis_Prtcpnt_File_View_Cis_Pull_Code.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Pull_Ia_Cntrct_Pckg_Rsn());                                                   //Natural: MOVE ADP-PULL-IA-CNTRCT-PCKG-RSN TO CIS-PULL-CODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Pull_Ia_Cntrct_Pckg_Rsn().equals("OVER")))                                                                   //Natural: IF ADP-PULL-IA-CNTRCT-PCKG-RSN = 'OVER'
            {
                cis_Prtcpnt_File_View_Cis_Mail_Instructions.setValue("O");                                                                                                //Natural: MOVE 'O' TO CIS-MAIL-INSTRUCTIONS
                cis_Prtcpnt_File_View_Cis_Pull_Code.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Pull_Ia_Cntrct_Pckg_Rsn());                                               //Natural: MOVE ADP-PULL-IA-CNTRCT-PCKG-RSN TO CIS-PULL-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                cis_Prtcpnt_File_View_Cis_Mail_Instructions.setValue("A");                                                                                                //Natural: MOVE 'A' TO CIS-MAIL-INSTRUCTIONS
                cis_Prtcpnt_File_View_Cis_Pull_Code.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Pull_Ia_Cntrct_Pckg_Rsn());                                               //Natural: MOVE ADP-PULL-IA-CNTRCT-PCKG-RSN TO CIS-PULL-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* * CM - 3/16/10
        //*  IF ADS-PRTCPNT-VIEW.ADP-IRC-CDE = '16'                  /* DEA
        //*  DEA
        if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Irc_Cde().equals("17")))                                                                                         //Natural: IF ADS-PRTCPNT-VIEW.ADP-IRC-CDE = '17'
        {
            //*  MOVE 'P' TO #457B-PRIVATE-IND
            cis_Prtcpnt_File_View_Cis_Four_Fifty_Seven_Ind.setValue("Y");                                                                                                 //Natural: MOVE 'Y' TO CIS-FOUR-FIFTY-SEVEN-IND
            cis_Prtcpnt_File_View_Cis_Institution_Name.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Inst_Nme());                                                           //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-INST-NME TO CIS-INSTITUTION-NAME
        }                                                                                                                                                                 //Natural: END-IF
        //* * CM END
    }
    private void sub_Cntrct_Record_Lookup() throws Exception                                                                                                              //Natural: CNTRCT-RECORD-LOOKUP
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Error_Ind.reset();                                                                                                                                            //Natural: RESET #ERROR-IND
        pnd_Adc_Super2_From_Pnd_Rqst_Id_F.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                                             //Natural: MOVE ADS-PRTCPNT-VIEW.RQST-ID TO #RQST-ID-F
        pnd_Adc_Super2_From_Pnd_Adc_Sqnce_Nbr_F.setValue(1);                                                                                                              //Natural: MOVE 1 TO #ADC-SQNCE-NBR-F
        pnd_Adc_Super2_To_Pnd_Rqst_Id_T.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                                               //Natural: MOVE ADS-PRTCPNT-VIEW.RQST-ID TO #RQST-ID-T
        pnd_Adc_Super2_To_Pnd_Adc_Sqnce_Nbr_T.setValue(99);                                                                                                               //Natural: MOVE 99 TO #ADC-SQNCE-NBR-T
        pnd_Counter_Cis_T.setValue(0);                                                                                                                                    //Natural: MOVE 0 TO #COUNTER-CIS-T
        pnd_Counter_Cis_C.setValue(0);                                                                                                                                    //Natural: MOVE 0 TO #COUNTER-CIS-C
        ldaAdsl401a.getVw_ads_Cntrct_View().startDatabaseFind                                                                                                             //Natural: FIND ADS-CNTRCT-VIEW WITH ADC-SUPER2 = #ADC-SUPER2-FROM THRU #ADC-SUPER2-TO
        (
        "FIND01",
        new Wc[] { new Wc("ADC_SUPER2", "<=", pnd_Adc_Super2_To, WcType.WITH) }
        );
        FIND01:
        while (condition(ldaAdsl401a.getVw_ads_Cntrct_View().readNextRow("FIND01", true)))
        {
            ldaAdsl401a.getVw_ads_Cntrct_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAdsl401a.getVw_ads_Cntrct_View().getAstCOUNTER().equals(0)))                                                                                 //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "NO CNTRCT RECORD FOUND FOR RQST",ldaAdsl401a.getAds_Cntrct_View_Rqst_Id());                                                        //Natural: WRITE 'NO CNTRCT RECORD FOUND FOR RQST' RQST-ID
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Error_Ind.setValue("Y");                                                                                                                              //Natural: MOVE 'Y' TO #ERROR-IND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            //* *ACCEPT IF ADS-IA-RSLT-VIEW.ADI-STTS-CDE-1 = 'T'
                                                                                                                                                                          //Natural: PERFORM MOVE-DA-TIAA-RESULTS-FIELDS
            sub_Move_Da_Tiaa_Results_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM MOVE-DA-CREF-RESULTS-FIELDS
            sub_Move_Da_Cref_Results_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Move_Da_Tiaa_Results_Fields() throws Exception                                                                                                       //Natural: MOVE-DA-TIAA-RESULTS-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Counter_Start.reset();                                                                                                                                        //Natural: RESET #COUNTER-START #COUNTER-END #OK-SW #CIS-REA-NUMBER #CIS-TIAA-NUMBER #CIS-TOTAL-DA-REA #CIS-TOTAL-DA-TIAA
        pnd_Counter_End.reset();
        pnd_Ok_Sw.reset();
        pnd_Cis_Rea_Number.reset();
        pnd_Cis_Tiaa_Number.reset();
        pnd_Cis_Total_Da_Rea.reset();
        pnd_Cis_Total_Da_Tiaa.reset();
        //*  MOVE C*ADC-RQST-INFO TO #COUNTER-END
        //*  REPEAT UNTIL #COUNTER-START = #COUNTER-END
        //*   RESET #CIS-TOTAL-DA-AMT
        //* *
        //*   ADD 1 TO #COUNTER-START
        //*   IF ADC-ACCT-CDE(#COUNTER-START) = 'R'
        //*     MOVE 'Y' TO #OK-SW
        //*     MOVE ADS-CNTRCT-VIEW.ADC-TIAA-NBR TO
        //*       #CIS-REA-NUMBER
        //*       #CIS-TIAA-NUMBER
        //*     COMPUTE #CIS-TOTAL-DA-AMT =
        //*       ADS-CNTRCT-VIEW.ADC-ACCT-ACTL-AMT(#COUNTER-START)
        //*     ADD #CIS-TOTAL-DA-AMT TO #CIS-TOTAL-DA-REA
        //*   ELSE
        //*   IF ADC-ACCT-CDE(#COUNTER-START) = 'D'           /* EM - 012309 START
        //*       MOVE 'Y' TO #OK-SW
        //*       MOVE ADS-CNTRCT-VIEW.ADC-TIAA-NBR TO
        //*         #CIS-REA-NUMBER
        //*         #CIS-TIAA-NUMBER
        //*       COMPUTE #CIS-TOTAL-DA-AMT =
        //*         ADS-CNTRCT-VIEW.ADC-ACCT-ACTL-AMT(#COUNTER-START)
        //*       ADD #CIS-TOTAL-DA-AMT TO #CIS-TOTAL-DA-REA    /* EM - 012309 END
        //*     ELSE
        //*       IF ADC-ACCT-CDE(#COUNTER-START) = 'T' OR = 'Y' /* EM - 012309
        //*         MOVE 'Y' TO #OK-SW
        //*         MOVE ADS-CNTRCT-VIEW.ADC-TIAA-NBR TO
        //*           #CIS-TIAA-NUMBER
        //*         COMPUTE #CIS-TOTAL-DA-AMT =
        //*           ADS-CNTRCT-VIEW.ADC-ACCT-ACTL-AMT(#COUNTER-START)
        //*         ADD #CIS-TOTAL-DA-AMT TO #CIS-TOTAL-DA-TIAA
        //*       END-IF
        //*     END-IF
        //*   END-IF
        //*  END-REPEAT
        if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Proceeds_Amt().greater(getZero())))                                                                         //Natural: IF ADC-TIAA-PROCEEDS-AMT > 0
        {
            pnd_Ok_Sw.setValue("Y");                                                                                                                                      //Natural: MOVE 'Y' TO #OK-SW
            pnd_Cis_Tiaa_Number.setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr());                                                                                  //Natural: MOVE ADS-CNTRCT-VIEW.ADC-TIAA-NBR TO #CIS-TIAA-NUMBER
            pnd_Cis_Total_Da_Tiaa.setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Proceeds_Amt());                                                                       //Natural: ASSIGN #CIS-TOTAL-DA-TIAA := ADC-TIAA-PROCEEDS-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Rea_Proceeds_Amt().greater(getZero())))                                                                          //Natural: IF ADC-REA-PROCEEDS-AMT > 0
        {
            pnd_Ok_Sw.setValue("Y");                                                                                                                                      //Natural: MOVE 'Y' TO #OK-SW
            pnd_Cis_Tiaa_Number.setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr());                                                                                  //Natural: MOVE ADS-CNTRCT-VIEW.ADC-TIAA-NBR TO #CIS-TIAA-NUMBER
            pnd_Cis_Total_Da_Rea.setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Rea_Proceeds_Amt());                                                                         //Natural: ASSIGN #CIS-TOTAL-DA-REA := ADC-REA-PROCEEDS-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ok_Sw.equals("Y")))                                                                                                                             //Natural: IF #OK-SW = 'Y'
        {
            pnd_Counter_Cis_T.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #COUNTER-CIS-T
            cis_Prtcpnt_File_View_Cis_Da_Tiaa_Nbr.getValue(pnd_Counter_Cis_T).setValue(pnd_Cis_Tiaa_Number);                                                              //Natural: MOVE #CIS-TIAA-NUMBER TO CIS-DA-TIAA-NBR ( #COUNTER-CIS-T )
            cis_Prtcpnt_File_View_Cis_Da_Rea_Proceeds_Amt.getValue(pnd_Counter_Cis_T).setValue(pnd_Cis_Total_Da_Rea);                                                     //Natural: MOVE #CIS-TOTAL-DA-REA TO CIS-DA-REA-PROCEEDS-AMT ( #COUNTER-CIS-T )
            cis_Prtcpnt_File_View_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_Counter_Cis_T).setValue(pnd_Cis_Total_Da_Tiaa);                                                   //Natural: MOVE #CIS-TOTAL-DA-TIAA TO CIS-DA-TIAA-PROCEEDS-AMT ( #COUNTER-CIS-T )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Move_Da_Cref_Results_Fields() throws Exception                                                                                                       //Natural: MOVE-DA-CREF-RESULTS-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Counter_Start.reset();                                                                                                                                        //Natural: RESET #COUNTER-START #COUNTER-END #OK-SW #CIS-CREF-NUMBER #CIS-TOTAL-DA-CREF
        pnd_Counter_End.reset();
        pnd_Ok_Sw.reset();
        pnd_Cis_Cref_Number.reset();
        pnd_Cis_Total_Da_Cref.reset();
        //*  MOVE C*ADC-RQST-INFO TO #COUNTER-END
        //*  REPEAT UNTIL #COUNTER-START = #COUNTER-END
        //*   RESET #CIS-TOTAL-DA-AMT
        //*   ADD 1 TO #COUNTER-START
        //*   IF ADC-ACCT-CDE(#COUNTER-START) = 'T' OR = 'R'
        //*       OR = 'Y' OR = 'D'                          /* EM - 012309
        //*     IGNORE
        //*   ELSE
        //*     MOVE 'Y' TO #OK-SW
        //*     MOVE ADS-CNTRCT-VIEW.ADC-CREF-NBR TO
        //*       #CIS-CREF-NUMBER
        //*     COMPUTE #CIS-TOTAL-DA-AMT =
        //*       ADS-CNTRCT-VIEW.ADC-ACCT-ACTL-AMT(#COUNTER-START)
        //* *     NAZ-DA-RSLT-DDM-VIEW.NAD-ACCT-RTB-ACTL-AMT(#COUNTER-START))
        //*     ADD #CIS-TOTAL-DA-AMT TO #CIS-TOTAL-DA-CREF
        //*   END-IF
        //*  END-REPEAT
        if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Cref_Proceeds_Amt().greater(getZero())))                                                                         //Natural: IF ADC-CREF-PROCEEDS-AMT > 0
        {
            pnd_Ok_Sw.setValue("Y");                                                                                                                                      //Natural: MOVE 'Y' TO #OK-SW
            pnd_Cis_Cref_Number.setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Cref_Nbr());                                                                                  //Natural: MOVE ADS-CNTRCT-VIEW.ADC-CREF-NBR TO #CIS-CREF-NUMBER
            pnd_Cis_Total_Da_Cref.setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Cref_Proceeds_Amt());                                                                       //Natural: ASSIGN #CIS-TOTAL-DA-CREF := ADC-CREF-PROCEEDS-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ok_Sw.equals("Y")))                                                                                                                             //Natural: IF #OK-SW = 'Y'
        {
            pnd_Counter_Cis_C.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #COUNTER-CIS-C
            cis_Prtcpnt_File_View_Cis_Da_Cert_Nbr.getValue(pnd_Counter_Cis_C).setValue(pnd_Cis_Cref_Number);                                                              //Natural: MOVE #CIS-CREF-NUMBER TO CIS-DA-CERT-NBR ( #COUNTER-CIS-C )
            cis_Prtcpnt_File_View_Cis_Da_Cref_Proceeds_Amt.getValue(pnd_Counter_Cis_C).setValue(pnd_Cis_Total_Da_Cref);                                                   //Natural: MOVE #CIS-TOTAL-DA-CREF TO CIS-DA-CREF-PROCEEDS-AMT ( #COUNTER-CIS-C )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Ia_Result_Lookup() throws Exception                                                                                                                  //Natural: IA-RESULT-LOOKUP
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Error_Ind.reset();                                                                                                                                            //Natural: RESET #ERROR-IND
        if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Rea_Proceeds_Amt().equals(getZero()) && ldaAdsl401a.getAds_Cntrct_View_Adc_Cref_Proceeds_Amt().equals(getZero()))) //Natural: IF ADC-REA-PROCEEDS-AMT = 0 AND ADC-CREF-PROCEEDS-AMT = 0
        {
            cis_Prtcpnt_File_View_Cis_Tiaa_Doi.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Cntr_Prt_Issue_Dte());                                                         //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CNTR-PRT-ISSUE-DTE TO CIS-TIAA-DOI
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            cis_Prtcpnt_File_View_Cis_Tiaa_Doi.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Cntr_Prt_Issue_Dte());                                                         //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CNTR-PRT-ISSUE-DTE TO CIS-TIAA-DOI
            cis_Prtcpnt_File_View_Cis_Cref_Doi.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Cntr_Prt_Issue_Dte());                                                         //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CNTR-PRT-ISSUE-DTE TO CIS-CREF-DOI
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((ldaAdsl401a.getAds_Cntrct_View_Adc_Rea_Proceeds_Amt().greater(getZero()) || ldaAdsl401a.getAds_Cntrct_View_Adc_Cref_Proceeds_Amt().greater(getZero())))) //Natural: IF ( ADC-REA-PROCEEDS-AMT > 0 OR ADC-CREF-PROCEEDS-AMT > 0 )
        {
            cis_Prtcpnt_File_View_Cis_Cntrct_Type.setValue("B");                                                                                                          //Natural: MOVE 'B' TO CIS-CNTRCT-TYPE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((ldaAdsl401a.getAds_Cntrct_View_Adc_Cref_Proceeds_Amt().equals(getZero()) && ldaAdsl401a.getAds_Cntrct_View_Adc_Rea_Proceeds_Amt().equals(getZero()))  //Natural: IF ( ADC-CREF-PROCEEDS-AMT = 0 AND ADC-REA-PROCEEDS-AMT = 0 ) AND ADC-TIAA-PROCEEDS-AMT > 0
            && ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Proceeds_Amt().greater(getZero())))
        {
            cis_Prtcpnt_File_View_Cis_Cntrct_Type.setValue("T");                                                                                                          //Natural: MOVE 'T' TO CIS-CNTRCT-TYPE
        }                                                                                                                                                                 //Natural: END-IF
        cis_Prtcpnt_File_View_Cis_Annty_Start_Dte.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Strt_Dte());                                                          //Natural: MOVE ADP-ANNTY-STRT-DTE TO CIS-ANNTY-START-DTE
        //*   IF ADI-FINL-PERIODIC-PY-DTE > 0
        //*     MOVE ADI-FINL-PERIODIC-PY-DTE  TO CIS-ANNTY-END-DTE
        //*     MOVE ADI-FINL-PERIODIC-PY-DTE  TO #END-DATE
        //*   ELSE
        //*     MOVE ADI-FINL-PRTL-PY-DTE      TO CIS-ANNTY-END-DTE
        //*     MOVE ADI-FINL-PRTL-PY-DTE      TO #END-DATE
        //*   END-IF
        cis_Prtcpnt_File_View_Cis_Annty_End_Dte.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Fnl_Prdc_Pay_Dte());                                                          //Natural: MOVE ADP-FNL-PRDC-PAY-DTE TO CIS-ANNTY-END-DTE
                                                                                                                                                                          //Natural: PERFORM MOVE-IA-TIAA-RESULTS-FIELDS
        sub_Move_Ia_Tiaa_Results_Fields();
        if (condition(Global.isEscape())) {return;}
        //*  MOVE   #CIS-TIAA-GRD-AMT              TO CIS-GRNTED-GRD-AMT
        //*  MOVE   #CIS-TIAA-STD-AMT              TO CIS-GRNTED-STD-AMT
        //*  MOVE   #CIS-FNL-STD-PAY-AMT           TO CIS-MDO-TRADITIONAL-AMT
        cis_Prtcpnt_File_View_Cis_Grnted_Grd_Amt.setValue(pnd_Cis_Tiaa_Grd_Amt);                                                                                          //Natural: MOVE #CIS-TIAA-GRD-AMT TO CIS-GRNTED-GRD-AMT
        cis_Prtcpnt_File_View_Cis_Grnted_Std_Amt.setValue(pnd_Cis_Tiaa_Std_Amt);                                                                                          //Natural: MOVE #CIS-TIAA-STD-AMT TO CIS-GRNTED-STD-AMT
        cis_Prtcpnt_File_View_Cis_Mdo_Traditional_Amt.setValue(pnd_Cis_Fnl_Std_Pay_Amt);                                                                                  //Natural: MOVE #CIS-FNL-STD-PAY-AMT TO CIS-MDO-TRADITIONAL-AMT
        if (condition(testing_Mode.getBoolean()))                                                                                                                         //Natural: IF TESTING-MODE
        {
            getReports().write(0, " CIS-GRNTED-GRD-AMT   C ",cis_Prtcpnt_File_View_Cis_Grnted_Grd_Amt);                                                                   //Natural: WRITE ' CIS-GRNTED-GRD-AMT   C ' CIS-GRNTED-GRD-AMT
            if (Global.isEscape()) return;
            getReports().write(0, " CIS-GRNTED-STD-AMT   C ",cis_Prtcpnt_File_View_Cis_Grnted_Std_Amt);                                                                   //Natural: WRITE ' CIS-GRNTED-STD-AMT   C ' CIS-GRNTED-STD-AMT
            if (Global.isEscape()) return;
            //*  WRITE ' ADI-OPTN-CDE         C ' ADI-OPTN-CDE
            //*  WRITE ' ADS-IA-RSLT-VIEW     C ' ADS-IA-RSLT-VIEW.RQST-ID
            getReports().write(0, " CIS-GRNTED-GRD-AMT   C ",cis_Prtcpnt_File_View_Cis_Grnted_Grd_Amt);                                                                   //Natural: WRITE ' CIS-GRNTED-GRD-AMT   C ' CIS-GRNTED-GRD-AMT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  MOVE ADP-TIAA-COMMUTED-INFO-2 TO CIS-TIAA-COMMUTED-INFO-2
        //*  MORTALITY BASIS CHANGE
                                                                                                                                                                          //Natural: PERFORM MOVE-IA-TIAA-ACTUARIAL-INFO
        sub_Move_Ia_Tiaa_Actuarial_Info();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Error_Ind.equals("Y")))                                                                                                                         //Natural: IF #ERROR-IND = 'Y'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM MOVE-IA-CREF-RESULTS-FIELDS
        sub_Move_Ia_Cref_Results_Fields();
        if (condition(Global.isEscape())) {return;}
        //*  END-FIND
    }
    private void sub_Move_Ia_Tiaa_Results_Fields() throws Exception                                                                                                       //Natural: MOVE-IA-TIAA-RESULTS-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Counter_Start.reset();                                                                                                                                        //Natural: RESET #COUNTER-START #COUNTER-COMPRESS #CIS-TIAA-GRD-AMT #CIS-TIAA-STD-AMT #CIS-COMUT-GRUAR-AMT-G ( 1 ) #CIS-COMUT-INT-RATE-G ( 1 ) #CIS-COMUT-PAYMENT-G ( 1 ) #CIS-COMUT-GRUAR-AMT-S ( * ) #CIS-COMUT-INT-RATE-S ( * ) #CIS-COMUT-PAYMENT-S ( * )
        pnd_Counter_Compress.reset();
        pnd_Cis_Tiaa_Grd_Amt.reset();
        pnd_Cis_Tiaa_Std_Amt.reset();
        pnd_Cis_Comut_Gruar_Amt_G.getValue(1).reset();
        pnd_Cis_Comut_Int_Rate_G.getValue(1).reset();
        pnd_Cis_Comut_Payment_G.getValue(1).reset();
        pnd_Cis_Comut_Gruar_Amt_S.getValue("*").reset();
        pnd_Cis_Comut_Int_Rate_S.getValue("*").reset();
        pnd_Cis_Comut_Payment_S.getValue("*").reset();
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Counter_Start.equals(pnd_Max_Cnt))) {break;}                                                                                                //Natural: UNTIL #COUNTER-START = #MAX-CNT
            pnd_Counter_Start.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #COUNTER-START
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Pymt_Method_2().getValue(pnd_Counter_Start).equals("GRADED")))                                         //Natural: IF ADP-COMUT-PYMT-METHOD-2 ( #COUNTER-START ) = 'GRADED'
            {
                pnd_Cis_Tiaa_Grd_Amt.nadd(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Grnted_Amt_2().getValue(pnd_Counter_Start));                                           //Natural: ADD ADP-COMUT-GRNTED-AMT-2 ( #COUNTER-START ) TO #CIS-TIAA-GRD-AMT
                pnd_Cis_Comut_Gruar_Amt_G.getValue(1).nadd(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Grnted_Amt_2().getValue(pnd_Counter_Start));                          //Natural: ADD ADP-COMUT-GRNTED-AMT-2 ( #COUNTER-START ) TO #CIS-COMUT-GRUAR-AMT-G ( 1 )
                pnd_Cis_Comut_Int_Rate_G.getValue(1).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Int_Rate_2().getValue(pnd_Counter_Start));                         //Natural: MOVE ADP-COMUT-INT-RATE-2 ( #COUNTER-START ) TO #CIS-COMUT-INT-RATE-G ( 1 )
                pnd_Cis_Comut_Payment_G.getValue(1).setValue("GRADED");                                                                                                   //Natural: MOVE 'GRADED' TO #CIS-COMUT-PAYMENT-G ( 1 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Pymt_Method_2().getValue(pnd_Counter_Start).equals("STANDARD")))                                       //Natural: IF ADP-COMUT-PYMT-METHOD-2 ( #COUNTER-START ) = 'STANDARD'
            {
                pnd_Cis_Tiaa_Std_Amt.nadd(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Grnted_Amt_2().getValue(pnd_Counter_Start));                                           //Natural: ADD ADP-COMUT-GRNTED-AMT-2 ( #COUNTER-START ) TO #CIS-TIAA-STD-AMT
                if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Int_Rate_2().getValue(pnd_Counter_Start).equals(pnd_Cis_Comut_Int_Rate_S.getValue("*"))))          //Natural: IF ADP-COMUT-INT-RATE-2 ( #COUNTER-START ) = #CIS-COMUT-INT-RATE-S ( * )
                {
                    pnd_Counter_Read.reset();                                                                                                                             //Natural: RESET #COUNTER-READ
                    REPEAT02:                                                                                                                                             //Natural: REPEAT
                    while (condition(whileTrue))
                    {
                        if (condition(pnd_Counter_Read.equals(4))) {break;}                                                                                               //Natural: UNTIL #COUNTER-READ = 4
                        pnd_Counter_Read.nadd(1);                                                                                                                         //Natural: ADD 1 TO #COUNTER-READ
                        if (condition(pnd_Cis_Comut_Int_Rate_S.getValue(pnd_Counter_Read).equals(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Int_Rate_2().getValue(pnd_Counter_Start)))) //Natural: IF #CIS-COMUT-INT-RATE-S ( #COUNTER-READ ) = ADP-COMUT-INT-RATE-2 ( #COUNTER-START )
                        {
                            pnd_Cis_Comut_Gruar_Amt_S.getValue(pnd_Counter_Read).nadd(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Grnted_Amt_2().getValue(pnd_Counter_Start)); //Natural: ADD ADP-COMUT-GRNTED-AMT-2 ( #COUNTER-START ) TO #CIS-COMUT-GRUAR-AMT-S ( #COUNTER-READ )
                            pnd_Cis_Comut_Int_Rate_S.getValue(pnd_Counter_Read).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Int_Rate_2().getValue(pnd_Counter_Start)); //Natural: MOVE ADP-COMUT-INT-RATE-2 ( #COUNTER-START ) TO #CIS-COMUT-INT-RATE-S ( #COUNTER-READ )
                            pnd_Cis_Comut_Payment_S.getValue(pnd_Counter_Read).setValue("STANDARD");                                                                      //Natural: MOVE 'STANDARD' TO #CIS-COMUT-PAYMENT-S ( #COUNTER-READ )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-REPEAT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Counter_Compress.nadd(1);                                                                                                                         //Natural: ADD 1 TO #COUNTER-COMPRESS
                    pnd_Cis_Comut_Gruar_Amt_S.getValue(pnd_Counter_Compress).nadd(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Grnted_Amt_2().getValue(pnd_Counter_Start));   //Natural: ADD ADP-COMUT-GRNTED-AMT-2 ( #COUNTER-START ) TO #CIS-COMUT-GRUAR-AMT-S ( #COUNTER-COMPRESS )
                    pnd_Cis_Comut_Int_Rate_S.getValue(pnd_Counter_Compress).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Int_Rate_2().getValue(pnd_Counter_Start));  //Natural: MOVE ADP-COMUT-INT-RATE-2 ( #COUNTER-START ) TO #CIS-COMUT-INT-RATE-S ( #COUNTER-COMPRESS )
                    pnd_Cis_Comut_Payment_S.getValue(pnd_Counter_Compress).setValue("STANDARD");                                                                          //Natural: MOVE 'STANDARD' TO #CIS-COMUT-PAYMENT-S ( #COUNTER-COMPRESS )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cis_Fnl_Std_Pay_Amt.nadd(ldaAdsl401.getAds_Prtcpnt_View_Adp_Dtl_Fnl_Std_Pay_Amt());                                                                       //Natural: ADD ADP-DTL-FNL-STD-PAY-AMT TO #CIS-FNL-STD-PAY-AMT
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //* ***********************************************************************
        //* **           MOVE TABLE TO CIS DATABASE                             ***
        //* ***********************************************************************
        //* ***********************************************************************
        cis_Prtcpnt_File_View_Cis_Comut_Grnted_Amt.getValue(1).setValue(pnd_Cis_Comut_Gruar_Amt_G.getValue(1));                                                           //Natural: MOVE #CIS-COMUT-GRUAR-AMT-G ( 1 ) TO CIS-COMUT-GRNTED-AMT ( 1 )
        cis_Prtcpnt_File_View_Cis_Comut_Int_Rate.getValue(1).setValue(pnd_Cis_Comut_Int_Rate_G.getValue(1));                                                              //Natural: MOVE #CIS-COMUT-INT-RATE-G ( 1 ) TO CIS-COMUT-INT-RATE ( 1 )
        cis_Prtcpnt_File_View_Cis_Comut_Pymt_Method.getValue(1).setValue(pnd_Cis_Comut_Payment_G.getValue(1));                                                            //Natural: MOVE #CIS-COMUT-PAYMENT-G ( 1 ) TO CIS-COMUT-PYMT-METHOD ( 1 )
        pnd_Counter_Start.reset();                                                                                                                                        //Natural: RESET #COUNTER-START #COUNTER-COMPRESS
        pnd_Counter_Compress.reset();
        //*  GRADED IS IN FIRST OCCUR
        pnd_Counter_Compress.setValue(1);                                                                                                                                 //Natural: MOVE 1 TO #COUNTER-COMPRESS
        if (condition(pnd_Cis_Comut_Gruar_Amt_G.getValue(1).equals(getZero())))                                                                                           //Natural: IF #CIS-COMUT-GRUAR-AMT-G ( 1 ) = 0
        {
            //*  NO GRADED
            pnd_Counter_Compress.setValue(0);                                                                                                                             //Natural: MOVE 0 TO #COUNTER-COMPRESS
        }                                                                                                                                                                 //Natural: END-IF
        REPEAT03:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Counter_Start.equals(4))) {break;}                                                                                                          //Natural: UNTIL #COUNTER-START = 4
            pnd_Counter_Start.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #COUNTER-START
            pnd_Counter_Compress.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #COUNTER-COMPRESS
            cis_Prtcpnt_File_View_Cis_Comut_Grnted_Amt.getValue(pnd_Counter_Compress).setValue(pnd_Cis_Comut_Gruar_Amt_S.getValue(pnd_Counter_Start));                    //Natural: MOVE #CIS-COMUT-GRUAR-AMT-S ( #COUNTER-START ) TO CIS-COMUT-GRNTED-AMT ( #COUNTER-COMPRESS )
            cis_Prtcpnt_File_View_Cis_Comut_Int_Rate.getValue(pnd_Counter_Compress).setValue(pnd_Cis_Comut_Int_Rate_S.getValue(pnd_Counter_Start));                       //Natural: MOVE #CIS-COMUT-INT-RATE-S ( #COUNTER-START ) TO CIS-COMUT-INT-RATE ( #COUNTER-COMPRESS )
            cis_Prtcpnt_File_View_Cis_Comut_Pymt_Method.getValue(pnd_Counter_Compress).setValue(pnd_Cis_Comut_Payment_S.getValue(pnd_Counter_Start));                     //Natural: MOVE #CIS-COMUT-PAYMENT-S ( #COUNTER-START ) TO CIS-COMUT-PYMT-METHOD ( #COUNTER-COMPRESS )
            if (condition(testing_Mode.getBoolean()))                                                                                                                     //Natural: IF TESTING-MODE
            {
                //*    WRITE ' ADI-OPTN-CDE  F        ' ADI-OPTN-CDE
                //*    WRITE ' NAZ-IA-RSLT-DDM-VIEW F ' ADS-IA-RSLT-VIEW.RQST-ID
                getReports().write(0, " CIS-COMUT-GRNTED-AMT F ",cis_Prtcpnt_File_View_Cis_Comut_Grnted_Amt.getValue(pnd_Counter_Compress));                              //Natural: WRITE ' CIS-COMUT-GRNTED-AMT F ' CIS-COMUT-GRNTED-AMT ( #COUNTER-COMPRESS )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Move_Ia_Tiaa_Actuarial_Info() throws Exception                                                                                                       //Natural: MOVE-IA-TIAA-ACTUARIAL-INFO
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Counter_Start.reset();                                                                                                                                        //Natural: RESET #COUNTER-START
        //*  PRM-NAZA6007
                                                                                                                                                                          //Natural: PERFORM READ-CONTROL-FILE
        sub_Read_Control_File();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Ws_Lob_Nmbr.equals(1)))                                                                                                                         //Natural: IF #WS-LOB-NMBR = 01
        {
            cis_Prtcpnt_File_View_Cis_Lob_Type.setValue(2);                                                                                                               //Natural: MOVE 2 TO CIS-LOB-TYPE
            cis_Prtcpnt_File_View_Cis_Lob.setValue("D");                                                                                                                  //Natural: MOVE 'D' TO CIS-LOB
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Ws_Lob_Nmbr.equals(2)))                                                                                                                     //Natural: IF #WS-LOB-NMBR = 02
            {
                cis_Prtcpnt_File_View_Cis_Lob_Type.setValue(7);                                                                                                           //Natural: MOVE 7 TO CIS-LOB-TYPE
                cis_Prtcpnt_File_View_Cis_Lob.setValue("D");                                                                                                              //Natural: MOVE 'D' TO CIS-LOB
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *******************************************************************
        pnd_Counter_Start.reset();                                                                                                                                        //Natural: RESET #COUNTER-START
        //*  PRM-STD-GRD-IND                        /* AC AFTER EM 7/03
        if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Pymt_Method_2().getValue("*").equals("GRADED")))                                                           //Natural: IF ADP-COMUT-PYMT-METHOD-2 ( * ) = 'GRADED'
        {
            FOR01:                                                                                                                                                        //Natural: FOR #COUNTER-START = 1 TO #MAX-CNT
            for (pnd_Counter_Start.setValue(1); condition(pnd_Counter_Start.lessOrEqual(pnd_Max_Cnt)); pnd_Counter_Start.nadd(1))
            {
                if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Pymt_Method_2().getValue(pnd_Counter_Start).equals(" ")))                                          //Natural: IF ADP-COMUT-PYMT-METHOD-2 ( #COUNTER-START ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Counter.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #COUNTER
                cis_Prtcpnt_File_View_Cis_Comut_Grnted_Amt_2.getValue(pnd_Counter_Start).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Grnted_Amt_2().getValue(pnd_Counter_Start)); //Natural: MOVE ADP-COMUT-GRNTED-AMT-2 ( #COUNTER-START ) TO CIS-COMUT-GRNTED-AMT-2 ( #COUNTER-START )
                cis_Prtcpnt_File_View_Cis_Comut_Pymt_Method_2.getValue(pnd_Counter_Start).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Pymt_Method_2().getValue(pnd_Counter_Start)); //Natural: MOVE ADP-COMUT-PYMT-METHOD-2 ( #COUNTER-START ) TO CIS-COMUT-PYMT-METHOD-2 ( #COUNTER-START )
                cis_Prtcpnt_File_View_Cis_Comut_Int_Rate_2.getValue(pnd_Counter_Start).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Int_Rate_2().getValue(pnd_Counter_Start)); //Natural: MOVE ADP-COMUT-INT-RATE-2 ( #COUNTER-START ) TO CIS-COMUT-INT-RATE-2 ( #COUNTER-START )
                cis_Prtcpnt_File_View_Cis_Comut_Mortality_Basis.getValue(pnd_Counter_Start).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Mortality_Basis().getValue(pnd_Counter_Start)); //Natural: MOVE ADP-COMUT-MORTALITY-BASIS ( #COUNTER-START ) TO CIS-COMUT-MORTALITY-BASIS ( #COUNTER-START )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Counter_Start.reset();                                                                                                                                        //Natural: RESET #COUNTER-START
        //*   PRM-STD-GRD-IND PRM-RCGP-RATE-CODE(*)
        //*  PRM-RCGP-RATE-CODE(*)    /* AC AFTER EM 7/03
        //*  PRM-RCGP-GUAR-PAYMT (*) PRM-RETURNED-FROM-AIAN063
        if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Pymt_Method_2().getValue("*").equals("STANDARD")))                                                         //Natural: IF ADP-COMUT-PYMT-METHOD-2 ( * ) = 'STANDARD'
        {
            FOR02:                                                                                                                                                        //Natural: FOR #COUNTER-START = 1 TO #MAX-CNT
            for (pnd_Counter_Start.setValue(1); condition(pnd_Counter_Start.lessOrEqual(pnd_Max_Cnt)); pnd_Counter_Start.nadd(1))
            {
                if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Pymt_Method_2().getValue(pnd_Counter_Start).equals(" ")))                                          //Natural: IF ADP-COMUT-PYMT-METHOD-2 ( #COUNTER-START ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Counter.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #COUNTER
                cis_Prtcpnt_File_View_Cis_Comut_Grnted_Amt_2.getValue(pnd_Counter).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Grnted_Amt_2().getValue(pnd_Counter_Start)); //Natural: MOVE ADP-COMUT-GRNTED-AMT-2 ( #COUNTER-START ) TO CIS-COMUT-GRNTED-AMT-2 ( #COUNTER )
                cis_Prtcpnt_File_View_Cis_Comut_Pymt_Method_2.getValue(pnd_Counter).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Pymt_Method_2().getValue(pnd_Counter_Start)); //Natural: MOVE ADP-COMUT-PYMT-METHOD-2 ( #COUNTER-START ) TO CIS-COMUT-PYMT-METHOD-2 ( #COUNTER )
                cis_Prtcpnt_File_View_Cis_Comut_Int_Rate_2.getValue(pnd_Counter).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Int_Rate_2().getValue(pnd_Counter_Start)); //Natural: MOVE ADP-COMUT-INT-RATE-2 ( #COUNTER-START ) TO CIS-COMUT-INT-RATE-2 ( #COUNTER )
                cis_Prtcpnt_File_View_Cis_Comut_Mortality_Basis.getValue(pnd_Counter).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Comut_Mortality_Basis().getValue(pnd_Counter_Start)); //Natural: MOVE ADP-COMUT-MORTALITY-BASIS ( #COUNTER-START ) TO CIS-COMUT-MORTALITY-BASIS ( #COUNTER )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Move_Ia_Cref_Results_Fields() throws Exception                                                                                                       //Natural: MOVE-IA-CREF-RESULTS-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Counter_Start.reset();                                                                                                                                        //Natural: RESET #COUNTER-START #COUNTER-COMPRESS #CIS-REA-ANNTY-AMT #CIS-CREF-ANNTY-AMT #CIS-ACC-ANNTY-AMT
        pnd_Counter_Compress.reset();
        pnd_Cis_Rea_Annty_Amt.reset();
        pnd_Cis_Cref_Annty_Amt.reset();
        pnd_Cis_Acc_Annty_Amt.reset();
        //*  C*ADP-ANNTY-UNITS-RQST-INFO
        REPEAT04:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Counter_Start.equals(20))) {break;}                                                                                                         //Natural: UNTIL #COUNTER-START = 20
            pnd_Counter_Start.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #COUNTER-START
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Tkr_Symbl().getValue(pnd_Counter_Start).equals("TREA#")))                                                    //Natural: IF ADP-TKR-SYMBL ( #COUNTER-START ) = 'TREA#'
            {
                if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Settl_All_Cref_Mon_Pct().equals(getZero())))                                                             //Natural: IF ADP-SETTL-ALL-CREF-MON-PCT = 0
                {
                    cis_Prtcpnt_File_View_Cis_Rea_Annual_Nbr_Units.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Units().getValue(pnd_Counter_Start));                //Natural: MOVE ADP-ANNTY-UNITS ( #COUNTER-START ) TO CIS-REA-ANNUAL-NBR-UNITS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    cis_Prtcpnt_File_View_Cis_Rea_Mnthly_Nbr_Units.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Units().getValue(pnd_Counter_Start));                //Natural: MOVE ADP-ANNTY-UNITS ( #COUNTER-START ) TO CIS-REA-MNTHLY-NBR-UNITS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Tkr_Symbl().getValue(pnd_Counter_Start).equals("WA51#")))                                                //Natural: IF ADP-TKR-SYMBL ( #COUNTER-START ) = 'WA51#'
                {
                    if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Settl_All_Cref_Mon_Pct().equals(getZero())))                                                         //Natural: IF ADP-SETTL-ALL-CREF-MON-PCT = 0
                    {
                        cis_Prtcpnt_File_View_Cis_Tacc_Annual_Nbr_Units.getValue(1).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Units().getValue(pnd_Counter_Start)); //Natural: MOVE ADP-ANNTY-UNITS ( #COUNTER-START ) TO CIS-TACC-ANNUAL-NBR-UNITS ( 1 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        cis_Prtcpnt_File_View_Cis_Tacc_Mnthly_Nbr_Units.getValue(1).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Units().getValue(pnd_Counter_Start)); //Natural: MOVE ADP-ANNTY-UNITS ( #COUNTER-START ) TO CIS-TACC-MNTHLY-NBR-UNITS ( 1 )
                    }                                                                                                                                                     //Natural: END-IF
                    cis_Prtcpnt_File_View_Cis_Tacc_Ind.getValue(1).setValue("A");                                                                                         //Natural: MOVE 'A' TO CIS-TACC-IND ( 1 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Counter_Compress.nadd(1);                                                                                                                         //Natural: ADD 1 TO #COUNTER-COMPRESS
                    if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Settl_All_Cref_Mon_Pct().equals(100)))                                                               //Natural: IF ADP-SETTL-ALL-CREF-MON-PCT = 100
                    {
                        cis_Prtcpnt_File_View_Cis_Cref_Mnthly_Nbr_Units.getValue(pnd_Counter_Compress).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Units().getValue(pnd_Counter_Start)); //Natural: MOVE ADP-ANNTY-UNITS ( #COUNTER-START ) TO CIS-CREF-MNTHLY-NBR-UNITS ( #COUNTER-COMPRESS )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        cis_Prtcpnt_File_View_Cis_Cref_Annual_Nbr_Units.getValue(pnd_Counter_Compress).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Units().getValue(pnd_Counter_Start)); //Natural: MOVE ADP-ANNTY-UNITS ( #COUNTER-START ) TO CIS-CREF-ANNUAL-NBR-UNITS ( #COUNTER-COMPRESS )
                        DbsUtil.examine(new ExamineSource(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Ticker().getValue("*")), new ExamineSearch(ldaAdsl401.getAds_Prtcpnt_View_Adp_Tkr_Symbl().getValue(pnd_Counter_Start)),  //Natural: EXAMINE #ACCT-TICKER ( * ) FOR ADP-TKR-SYMBL ( #COUNTER-START ) GIVING INDEX #IDX
                            new ExamineGivingIndex(pnd_Idx));
                        if (condition(pnd_Idx.greater(getZero())))                                                                                                        //Natural: IF #IDX > 0
                        {
                            cis_Prtcpnt_File_View_Cis_Cref_Acct_Cde.getValue(pnd_Counter_Compress).setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Name_1().getValue(pnd_Idx)); //Natural: MOVE #ACCT-NAME-1 ( #IDX ) TO CIS-CREF-ACCT-CDE ( #COUNTER-COMPRESS )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        cis_Prtcpnt_File_View_Cis_Rea_Annty_Amt.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Rea_Annty_Amt());                                                             //Natural: MOVE ADP-REA-ANNTY-AMT TO CIS-REA-ANNTY-AMT
        cis_Prtcpnt_File_View_Cis_Cref_Annty_Amt.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Cref_Annty_Amt());                                                           //Natural: MOVE ADP-CREF-ANNTY-AMT TO CIS-CREF-ANNTY-AMT
        cis_Prtcpnt_File_View_Cis_Tacc_Annty_Amt.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Tacc_Annty_Amt());                                                           //Natural: MOVE ADP-TACC-ANNTY-AMT TO CIS-TACC-ANNTY-AMT
    }
    private void sub_Read_Control_File() throws Exception                                                                                                                 //Natural: READ-CONTROL-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //*  GET PRODUCT/CONTRACT TYPE
        //*  BY ACCT
        pdaNeca4000.getNeca4000().reset();                                                                                                                                //Natural: RESET NECA4000
        pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr());                                                              //Natural: ASSIGN NECA4000.PRD-KEY4-ACCT-NBR := ADC-TIAA-NBR
        pdaNeca4000.getNeca4000_Function_Cde().setValue("PRD");                                                                                                           //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'PRD'
        pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("04");                                                                                                     //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '04'
        pdaNeca4000.getNeca4000_Request_Ind().setValue(" ");                                                                                                              //Natural: ASSIGN NECA4000.REQUEST-IND := ' '
        DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                            //Natural: CALLNAT 'NECN4000' NECA4000
        if (condition(Global.isEscape())) return;
        if (condition(pdaNeca4000.getNeca4000_Return_Cde().equals(" ") || pdaNeca4000.getNeca4000_Return_Cde().equals("00")))                                             //Natural: IF NECA4000.RETURN-CDE EQ ' ' OR EQ '00'
        {
            pnd_Ws_Lob_Nmbr.setValue(pdaNeca4000.getNeca4000_Prd_Product_Nbr().getValue(1));                                                                              //Natural: ASSIGN #WS-LOB-NMBR := PRD-PRODUCT-NBR ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,"PROGRAM: ",Global.getPROGRAM(),new TabSetting(48),"ANNUITIZATION CIS ADD REQUESTS",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR / 'PROGRAM: ' *PROGRAM 48T 'ANNUITIZATION CIS ADD REQUESTS' 101T 'RUN DATE:' *DATU / 101T 'RUN TIME:' *TIMX / 101T 'PAGE:' *PAGE-NUMBER ( 1 ) //
                        TabSetting(101),"RUN DATE:",Global.getDATU(),NEWLINE,new TabSetting(101),"RUN TIME:",Global.getTIMX(),NEWLINE,new TabSetting(101),
                        "PAGE:",getReports().getPageNumberDbs(1),NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60");

        getReports().setDisplayColumns(1, "DA/PIN",
        		ldaAdsl401.getAds_Prtcpnt_View_Adp_Unique_Id(),new ColumnSpacing(3),"IA/CONTRACT",
        		ldaAdsl401.getAds_Prtcpnt_View_Adp_Ia_Tiaa_Nbr(), new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(3),"DA/CONTRACT",
        		ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr(), new ReportEditMask ("XXXXXXX-X"),new ColumnSpacing(3));
    }
}
