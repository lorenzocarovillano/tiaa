/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:03:15 PM
**        * FROM NATURAL PROGRAM : Adsp560
************************************************************
**        * FILE NAME            : Adsp560.java
**        * CLASS NAME           : Adsp560
**        * INSTANCE NAME        : Adsp560
************************************************************
***************************************************************
* ADSP560  NOTIFICATION LETTER
*********************************************************************
*
* NEW ANNUITIZATION SYSTEM
*
* THIS MODULE INTERFACES WITH POST SYSTEM TO PRODUCE NOTIFICATION
* LETTER.
*
* PROGRAM HAS TWO TEST MODE SWITCHES. IF #UPDATE-ON, CREATE THE POST
* LETTER.  IF NOT #LETTER-ONLY UPDATE THE FILE ALSO
* THIS ALLOWS TESTING LETTERS WITHOUT HAVING TO CONTINUALLY RECREATE
* INPUT FILE
*
*
*********************  MAINTENANCE LOG ******************************
*
*  D A T E   PROGRAMMER     D E S C R I P T I O N
*
* 05/09/06   N CVETKOVIC    CLONED FROM ADSN560
*                         CHANGED TO PROGRAM, REMOVED DEAD CODE ETC
* 08/28/06   N CVETKOVIC    CORRECTED BLANK LINE IN ADDRESS PROBLEM
* 11/11/09   O SOTTO      PRODUCTION FIX - INCLUDE ADMINISTRATOR NAME
*                         IN SELECT PLANS. THESE PLANS ARE STORED
*                         IN ADAT TABLE NAZ071. SC 111109.
* 01/20/10   O SOTTO      PRODUCTION FIX - DON't produce the letter
*                         FOR SELECTED PLANS STORED IN ADAT TABLE
*                         NAZ059. SC 012010.
* 03/19/10   O SOTTO      PRODUCTION FIX - INSITUTION ADDRESSES ARE
*                         NOT PROPERLY LOADED. SC 031910.
* 06/07/10   O SOTTO      PRODUCTION FIX - IF COMMA FROM CONTACT NAME
*                         EXCEEDS 1, MOVE 'NONE' TO #WK-COMPRESSED-NME
*                         TO PREVENT ERROR. SC 060710.
* 06/04/12   O SOTTO      PRODUCTION FIX - SELECT PLANS MUST ALWAYS
*                         RECEIVE THE LETTER REGARDLESS OF IRC
*                         CODE.  ADAT TABLE NAZ077 CONTAINS THESE.
*                         SC 060412.
* 04/18/2014 O SOTTO      PRODUCTION FIX. SOME IRC CODE IS '2 ' INSTEAD
*                         OF '02'.  FIXED TO RECOGNIZE 02 OR 2.
*                         CHANGES MARKED 041814.
* 5/07/2015  E. MELNIK BENE IN THE PLAN CHANGES.  MARKED BY EM - 050715.
* 8/31/2016  E. MELNIK PIN EXPANSION CHANGES.  MARKED BY EM - 083016.
* 2/22/2017  R. CARREON PIN EXPANSION 02222017
***************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp560 extends BLNatBase
{
    // Data Areas
    private PdaScia8200 pdaScia8200;
    private LdaPstl0185 ldaPstl0185;
    private PdaPsta9500 pdaPsta9500;
    private PdaPsta9501 pdaPsta9501;
    private PdaPsta9610 pdaPsta9610;
    private PdaPsta9612 pdaPsta9612;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaCwfpdaun pdaCwfpdaun;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Return_Cde;
    private DbsField pnd_Msg;

    private DbsGroup pnd_Msg__R_Field_1;
    private DbsField pnd_Msg_Pnd_Err_Ppg;
    private DbsField pnd_Msg_Pnd_Err_Ltr;
    private DbsField pnd_Msg_Pnd_Err_Txt;

    private DataAccessProgramView vw_ads_Prtcpnt_View;
    private DbsField ads_Prtcpnt_View_Rqst_Id;
    private DbsField ads_Prtcpnt_View_Adp_Annt_Typ_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Ia_Tiaa_Nbr;
    private DbsField ads_Prtcpnt_View_Adp_Ia_Tiaa_Payee_Cd;
    private DbsField ads_Prtcpnt_View_Adp_Ia_Cref_Nbr;
    private DbsField ads_Prtcpnt_View_Adp_Ia_Cref_Payee_Cd;
    private DbsGroup ads_Prtcpnt_View_Adp_Cntrcts_In_RqstMuGroup;
    private DbsField ads_Prtcpnt_View_Adp_Cntrcts_In_Rqst;
    private DbsField ads_Prtcpnt_View_Adp_Unique_Id;
    private DbsField ads_Prtcpnt_View_Adp_Bps_Unit;
    private DbsField ads_Prtcpnt_View_Adp_Wpid;
    private DbsField ads_Prtcpnt_View_Adp_Effctv_Dte;
    private DbsField ads_Prtcpnt_View_Adp_Entry_User_Id;
    private DbsField ads_Prtcpnt_View_Adp_Mit_Log_Dte_Tme;
    private DbsField ads_Prtcpnt_View_Adp_Opn_Clsd_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Stts_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Frst_Annt_Title_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Frst_Annt_Frst_Nme;
    private DbsField ads_Prtcpnt_View_Adp_Frst_Annt_Mid_Nme;
    private DbsField ads_Prtcpnt_View_Adp_Frst_Annt_Lst_Nme;
    private DbsField ads_Prtcpnt_View_Adp_Frst_Annt_Ssn;
    private DbsField ads_Prtcpnt_View_Adp_Annty_Strt_Dte;
    private DbsGroup ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_TxtMuGroup;
    private DbsField ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt;
    private DbsField ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Zip;
    private DbsField ads_Prtcpnt_View_Adp_Rep_Nme;
    private DbsField ads_Prtcpnt_View_Adp_Rqst_Last_Updtd_Id;
    private DbsField ads_Prtcpnt_View_Adp_Pi_Task_Id;
    private DbsField ads_Prtcpnt_View_Adp_Lst_Actvty_Dte;
    private DbsField ads_Prtcpnt_View_Adp_Irc_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Plan_Nbr;
    private DbsField ads_Prtcpnt_View_Adp_Ntfctn_Lttr_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Bene_Client_Ind;

    private DataAccessProgramView vw_cwf_Org_Unit_Tbl_View;
    private DbsField cwf_Org_Unit_Tbl_View_Unit_Cde;
    private DbsField cwf_Org_Unit_Tbl_View_Unit_Sprvsr_Racf_Id;
    private DbsField cwf_Org_Unit_Tbl_View_Updte_Oprtr_Cde;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;
    private DbsField pnd_Naz_Table_Key;

    private DbsGroup pnd_Naz_Table_Key__R_Field_2;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id;
    private DbsField pnd_None;

    private DbsGroup sort_Key2;
    private DbsField sort_Key2_Sort_Key_Lgth;
    private DbsField sort_Key2_Sort_Key_Array;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_3;
    private DbsField pnd_Date_A_Pnd_Date_Yyyymmdd;
    private DbsField pnd_Counter;
    private DbsField pnd_Appl_Key_Counter;
    private DbsField pnd_Parm_Racf_Id;
    private DbsField pnd_Dummy_Plan;
    private DbsField pnd_Annt_Name;
    private DbsField pnd_Name;
    private DbsField pnd_Name_Saved;
    private DbsField pnd_Psg_Error;
    private DbsField pnd_Post_Error;
    private DbsField pnd_Efm_Error;
    private DbsField pnd_Error_Text;

    private DbsGroup pnd_Error_Text__R_Field_4;
    private DbsField pnd_Error_Text_Pnd_Star_1;
    private DbsField pnd_Error_Text_Pnd_Text;
    private DbsField pnd_Error_Text_Pnd_Star_2;
    private DbsField pnd_Error_Code;

    private DbsGroup pnd_Error_Code__R_Field_5;
    private DbsField pnd_Error_Code_Pnd_Err_Code;
    private DbsField pnd_A;
    private DbsField pnd_I;
    private DbsField pnd_X;
    private DbsField pnd_B;
    private DbsField pnd_Do_Not_Send_Letter;

    private DbsGroup pnd_Work;
    private DbsField pnd_Work_Pnd_Wk_Compressed_Nme;
    private DbsField pnd_Work_Pnd_Wk_F_Name;
    private DbsField pnd_Work_Pnd_Wk_L_Name;
    private DbsField pnd_Work_Pnd_Wk_M_Name;
    private DbsField pnd_Work_Pnd_Wk_Zip;

    private DbsGroup pnd_Work__R_Field_6;
    private DbsField pnd_Work_Pnd_Wk_Zip5;
    private DbsField pnd_Work_Pnd_Wk_Zip4;
    private DbsField pnd_Work_Pnd_Wk_Zip9;
    private DbsField pnd_Work_Pnd_Wk_Cntr;
    private DbsField pnd_Work_Pnd_Wk_City;
    private DbsField pnd_Work_Pnd_Wk_City_State_Zip9;
    private DbsField pnd_Work_Pnd_Addr2_Fnd;
    private DbsField pnd_Work_Pnd_Addr3_Fnd;
    private DbsField pnd_Work_Pnd_Dont_Print;

    private DataAccessProgramView vw_ads_Cntl_View;
    private DbsField ads_Cntl_View_Ads_Rcrd_Cde;

    private DbsGroup ads_Cntl_View_Ads_Cntl_Grp;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte;

    private DbsGroup ads_Cntl_View__R_Field_7;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte;

    private DbsGroup ads_Cntl_View__R_Field_8;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte_A;
    private DbsField ads_Cntl_View_Ads_Rpt_13;
    private DbsField pnd_Cntl_Bsnss_Prev_Dte;
    private DbsField pnd_Cntl_Bsnss_Dte;
    private DbsField pnd_Cntl_Bsnss_Tmrrw_Dte;
    private DbsField pnd_Status;
    private DbsField pnd_Ads_Cntl_Super_De_1;

    private DbsGroup pnd_Ads_Cntl_Super_De_1__R_Field_9;
    private DbsField pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Rcrd_Typ;
    private DbsField pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Bsnss_Dt;
    private DbsField pnd_First_Plan_Call;
    private DbsField pnd_Print;
    private DbsField pnd_Update_On;
    private DbsField pnd_Letter_Only;
    private DbsField pnd_Exception;
    private DbsField pnd_Letters_Written;
    private DbsField pnd_Error_Report_Entries;
    private DbsField pnd_Psg_Errors;

    private DbsGroup pnd_Adsn998_Error_Handler;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Calling_Program;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Unique_Id;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Step_Name;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Appl_Id;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Error_Msg;

    private DbsGroup pnd_Terminate;
    private DbsField pnd_Terminate_Pnd_Terminate_No;
    private DbsField pnd_Terminate_Pnd_Terminate_Msg;

    private DbsGroup pnd_Terminate__R_Field_10;
    private DbsField pnd_Terminate_Pnd_Terminate_Msg1;
    private DbsField pnd_Terminate_Pnd_Terminate_Msg2;
    private DbsField pnd_Terminate_Pnd_Terminate_Msg3;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaScia8200 = new PdaScia8200(localVariables);
        ldaPstl0185 = new LdaPstl0185();
        registerRecord(ldaPstl0185);
        pdaPsta9500 = new PdaPsta9500(localVariables);
        pdaPsta9501 = new PdaPsta9501(localVariables);
        pdaPsta9610 = new PdaPsta9610(localVariables);
        pdaPsta9612 = new PdaPsta9612(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfpdaus = new PdaCwfpdaus(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);

        // Local Variables
        pnd_Return_Cde = localVariables.newFieldInRecord("pnd_Return_Cde", "#RETURN-CDE", FieldType.STRING, 1);
        pnd_Msg = localVariables.newFieldInRecord("pnd_Msg", "#MSG", FieldType.STRING, 79);

        pnd_Msg__R_Field_1 = localVariables.newGroupInRecord("pnd_Msg__R_Field_1", "REDEFINE", pnd_Msg);
        pnd_Msg_Pnd_Err_Ppg = pnd_Msg__R_Field_1.newFieldInGroup("pnd_Msg_Pnd_Err_Ppg", "#ERR-PPG", FieldType.STRING, 6);
        pnd_Msg_Pnd_Err_Ltr = pnd_Msg__R_Field_1.newFieldInGroup("pnd_Msg_Pnd_Err_Ltr", "#ERR-LTR", FieldType.STRING, 11);
        pnd_Msg_Pnd_Err_Txt = pnd_Msg__R_Field_1.newFieldInGroup("pnd_Msg_Pnd_Err_Txt", "#ERR-TXT", FieldType.STRING, 62);

        vw_ads_Prtcpnt_View = new DataAccessProgramView(new NameInfo("vw_ads_Prtcpnt_View", "ADS-PRTCPNT-VIEW"), "ADS_PRTCPNT", "ADS_PRTCPNT");
        ads_Prtcpnt_View_Rqst_Id = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "RQST_ID");
        ads_Prtcpnt_View_Adp_Annt_Typ_Cde = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Annt_Typ_Cde", "ADP-ANNT-TYP-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_ANNT_TYP_CDE");
        ads_Prtcpnt_View_Adp_Ia_Tiaa_Nbr = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Ia_Tiaa_Nbr", "ADP-IA-TIAA-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "ADP_IA_TIAA_NBR");
        ads_Prtcpnt_View_Adp_Ia_Tiaa_Payee_Cd = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Ia_Tiaa_Payee_Cd", "ADP-IA-TIAA-PAYEE-CD", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ADP_IA_TIAA_PAYEE_CD");
        ads_Prtcpnt_View_Adp_Ia_Cref_Nbr = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Ia_Cref_Nbr", "ADP-IA-CREF-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "ADP_IA_CREF_NBR");
        ads_Prtcpnt_View_Adp_Ia_Cref_Payee_Cd = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Ia_Cref_Payee_Cd", "ADP-IA-CREF-PAYEE-CD", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ADP_IA_CREF_PAYEE_CD");
        ads_Prtcpnt_View_Adp_Cntrcts_In_RqstMuGroup = vw_ads_Prtcpnt_View.getRecord().newGroupInGroup("ADS_PRTCPNT_VIEW_ADP_CNTRCTS_IN_RQSTMuGroup", "ADP_CNTRCTS_IN_RQSTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_PRTCPNT_ADP_CNTRCTS_IN_RQST");
        ads_Prtcpnt_View_Adp_Cntrcts_In_Rqst = ads_Prtcpnt_View_Adp_Cntrcts_In_RqstMuGroup.newFieldArrayInGroup("ads_Prtcpnt_View_Adp_Cntrcts_In_Rqst", 
            "ADP-CNTRCTS-IN-RQST", FieldType.STRING, 10, new DbsArrayController(1, 6), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADP_CNTRCTS_IN_RQST");
        ads_Prtcpnt_View_Adp_Unique_Id = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Unique_Id", "ADP-UNIQUE-ID", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "ADP_UNIQUE_ID");
        ads_Prtcpnt_View_Adp_Bps_Unit = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Bps_Unit", "ADP-BPS-UNIT", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADP_BPS_UNIT");
        ads_Prtcpnt_View_Adp_Wpid = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Wpid", "ADP-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "ADP_WPID");
        ads_Prtcpnt_View_Adp_Effctv_Dte = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Effctv_Dte", "ADP-EFFCTV-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADP_EFFCTV_DTE");
        ads_Prtcpnt_View_Adp_Entry_User_Id = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Entry_User_Id", "ADP-ENTRY-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADP_ENTRY_USER_ID");
        ads_Prtcpnt_View_Adp_Mit_Log_Dte_Tme = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Mit_Log_Dte_Tme", "ADP-MIT-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "ADP_MIT_LOG_DTE_TME");
        ads_Prtcpnt_View_Adp_Opn_Clsd_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Opn_Clsd_Ind", "ADP-OPN-CLSD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_OPN_CLSD_IND");
        ads_Prtcpnt_View_Adp_Stts_Cde = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Stts_Cde", "ADP-STTS-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "ADP_STTS_CDE");
        ads_Prtcpnt_View_Adp_Frst_Annt_Title_Cde = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Frst_Annt_Title_Cde", "ADP-FRST-ANNT-TITLE-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_TITLE_CDE");
        ads_Prtcpnt_View_Adp_Frst_Annt_Frst_Nme = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Frst_Annt_Frst_Nme", "ADP-FRST-ANNT-FRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_FRST_NME");
        ads_Prtcpnt_View_Adp_Frst_Annt_Mid_Nme = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Frst_Annt_Mid_Nme", "ADP-FRST-ANNT-MID-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_MID_NME");
        ads_Prtcpnt_View_Adp_Frst_Annt_Lst_Nme = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Frst_Annt_Lst_Nme", "ADP-FRST-ANNT-LST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_LST_NME");
        ads_Prtcpnt_View_Adp_Frst_Annt_Ssn = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Frst_Annt_Ssn", "ADP-FRST-ANNT-SSN", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_SSN");
        ads_Prtcpnt_View_Adp_Annty_Strt_Dte = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Annty_Strt_Dte", "ADP-ANNTY-STRT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_ANNTY_STRT_DTE");
        ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_TxtMuGroup = vw_ads_Prtcpnt_View.getRecord().newGroupInGroup("ADS_PRTCPNT_VIEW_ADP_CRRSPNDNCE_PERM_ADDR_TXTMuGroup", 
            "ADP_CRRSPNDNCE_PERM_ADDR_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "ADS_PRTCPNT_ADP_CRRSPNDNCE_PERM_ADDR_TXT");
        ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt = ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_TxtMuGroup.newFieldArrayInGroup("ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt", 
            "ADP-CRRSPNDNCE-PERM-ADDR-TXT", FieldType.STRING, 35, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArray, "ADP_CRRSPNDNCE_PERM_ADDR_TXT");
        ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Zip = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Zip", 
            "ADP-CRRSPNDNCE-PERM-ADDR-ZIP", FieldType.STRING, 9, RepeatingFieldStrategy.None, "ADP_CRRSPNDNCE_PERM_ADDR_ZIP");
        ads_Prtcpnt_View_Adp_Rep_Nme = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Rep_Nme", "ADP-REP-NME", FieldType.STRING, 
            40, RepeatingFieldStrategy.None, "ADP_REP_NME");
        ads_Prtcpnt_View_Adp_Rqst_Last_Updtd_Id = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Rqst_Last_Updtd_Id", "ADP-RQST-LAST-UPDTD-ID", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "ADP_RQST_LAST_UPDTD_ID");
        ads_Prtcpnt_View_Adp_Pi_Task_Id = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Pi_Task_Id", "ADP-PI-TASK-ID", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "ADP_PI_TASK_ID");
        ads_Prtcpnt_View_Adp_Lst_Actvty_Dte = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Lst_Actvty_Dte", "ADP-LST-ACTVTY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_LST_ACTVTY_DTE");
        ads_Prtcpnt_View_Adp_Irc_Cde = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Irc_Cde", "ADP-IRC-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADP_IRC_CDE");
        ads_Prtcpnt_View_Adp_Plan_Nbr = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Plan_Nbr", "ADP-PLAN-NBR", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "ADP_PLAN_NBR");
        ads_Prtcpnt_View_Adp_Ntfctn_Lttr_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Ntfctn_Lttr_Ind", "ADP-NTFCTN-LTTR-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_NTFCTN_LTTR_IND");
        ads_Prtcpnt_View_Adp_Bene_Client_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Bene_Client_Ind", "ADP-BENE-CLIENT-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_BENE_CLIENT_IND");
        registerRecord(vw_ads_Prtcpnt_View);

        vw_cwf_Org_Unit_Tbl_View = new DataAccessProgramView(new NameInfo("vw_cwf_Org_Unit_Tbl_View", "CWF-ORG-UNIT-TBL-VIEW"), "CWF_ORG_UNIT_TBL", "CWF_ASSIGN_RULE");
        cwf_Org_Unit_Tbl_View_Unit_Cde = vw_cwf_Org_Unit_Tbl_View.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_View_Unit_Cde", "UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "UNIT_CDE");
        cwf_Org_Unit_Tbl_View_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Org_Unit_Tbl_View_Unit_Sprvsr_Racf_Id = vw_cwf_Org_Unit_Tbl_View.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_View_Unit_Sprvsr_Racf_Id", 
            "UNIT-SPRVSR-RACF-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, "UNIT_SPRVSR_RACF_ID");
        cwf_Org_Unit_Tbl_View_Unit_Sprvsr_Racf_Id.setDdmHeader("SUPERVISOR/RACF/ID");
        cwf_Org_Unit_Tbl_View_Updte_Oprtr_Cde = vw_cwf_Org_Unit_Tbl_View.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_View_Updte_Oprtr_Cde", "UPDTE-OPRTR-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "UPDTE_OPRTR_CDE");
        cwf_Org_Unit_Tbl_View_Updte_Oprtr_Cde.setDdmHeader("UPDATE/OPERATOR");
        registerRecord(vw_cwf_Org_Unit_Tbl_View);

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        registerRecord(vw_naz_Table_Ddm);

        pnd_Naz_Table_Key = localVariables.newFieldInRecord("pnd_Naz_Table_Key", "#NAZ-TABLE-KEY", FieldType.STRING, 30);

        pnd_Naz_Table_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Naz_Table_Key__R_Field_2", "REDEFINE", pnd_Naz_Table_Key);
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind = pnd_Naz_Table_Key__R_Field_2.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind", "#NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id = pnd_Naz_Table_Key__R_Field_2.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id", "#NAZ-TABLE-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id = pnd_Naz_Table_Key__R_Field_2.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id", "#NAZ-TABLE-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id = pnd_Naz_Table_Key__R_Field_2.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id", "#NAZ-TABLE-LVL3-ID", 
            FieldType.STRING, 20);
        pnd_None = localVariables.newFieldInRecord("pnd_None", "#NONE", FieldType.BOOLEAN, 1);

        sort_Key2 = localVariables.newGroupInRecord("sort_Key2", "SORT-KEY2");
        sort_Key2_Sort_Key_Lgth = sort_Key2.newFieldInGroup("sort_Key2_Sort_Key_Lgth", "SORT-KEY-LGTH", FieldType.NUMERIC, 3);
        sort_Key2_Sort_Key_Array = sort_Key2.newFieldArrayInGroup("sort_Key2_Sort_Key_Array", "SORT-KEY-ARRAY", FieldType.STRING, 1, new DbsArrayController(1, 
            3));
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_3 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_3", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_Yyyymmdd = pnd_Date_A__R_Field_3.newFieldInGroup("pnd_Date_A_Pnd_Date_Yyyymmdd", "#DATE-YYYYMMDD", FieldType.NUMERIC, 8);
        pnd_Counter = localVariables.newFieldInRecord("pnd_Counter", "#COUNTER", FieldType.PACKED_DECIMAL, 4);
        pnd_Appl_Key_Counter = localVariables.newFieldInRecord("pnd_Appl_Key_Counter", "#APPL-KEY-COUNTER", FieldType.NUMERIC, 5);
        pnd_Parm_Racf_Id = localVariables.newFieldInRecord("pnd_Parm_Racf_Id", "#PARM-RACF-ID", FieldType.STRING, 8);
        pnd_Dummy_Plan = localVariables.newFieldInRecord("pnd_Dummy_Plan", "#DUMMY-PLAN", FieldType.STRING, 6);
        pnd_Annt_Name = localVariables.newFieldInRecord("pnd_Annt_Name", "#ANNT-NAME", FieldType.STRING, 50);
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 50);
        pnd_Name_Saved = localVariables.newFieldArrayInRecord("pnd_Name_Saved", "#NAME-SAVED", FieldType.STRING, 35, new DbsArrayController(1, 20));
        pnd_Psg_Error = localVariables.newFieldInRecord("pnd_Psg_Error", "#PSG-ERROR", FieldType.BOOLEAN, 1);
        pnd_Post_Error = localVariables.newFieldInRecord("pnd_Post_Error", "#POST-ERROR", FieldType.STRING, 1);
        pnd_Efm_Error = localVariables.newFieldInRecord("pnd_Efm_Error", "#EFM-ERROR", FieldType.STRING, 1);
        pnd_Error_Text = localVariables.newFieldInRecord("pnd_Error_Text", "#ERROR-TEXT", FieldType.STRING, 35);

        pnd_Error_Text__R_Field_4 = localVariables.newGroupInRecord("pnd_Error_Text__R_Field_4", "REDEFINE", pnd_Error_Text);
        pnd_Error_Text_Pnd_Star_1 = pnd_Error_Text__R_Field_4.newFieldInGroup("pnd_Error_Text_Pnd_Star_1", "#STAR-1", FieldType.STRING, 2);
        pnd_Error_Text_Pnd_Text = pnd_Error_Text__R_Field_4.newFieldInGroup("pnd_Error_Text_Pnd_Text", "#TEXT", FieldType.STRING, 31);
        pnd_Error_Text_Pnd_Star_2 = pnd_Error_Text__R_Field_4.newFieldInGroup("pnd_Error_Text_Pnd_Star_2", "#STAR-2", FieldType.STRING, 2);
        pnd_Error_Code = localVariables.newFieldInRecord("pnd_Error_Code", "#ERROR-CODE", FieldType.NUMERIC, 4);

        pnd_Error_Code__R_Field_5 = localVariables.newGroupInRecord("pnd_Error_Code__R_Field_5", "REDEFINE", pnd_Error_Code);
        pnd_Error_Code_Pnd_Err_Code = pnd_Error_Code__R_Field_5.newFieldInGroup("pnd_Error_Code_Pnd_Err_Code", "#ERR-CODE", FieldType.STRING, 4);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 2);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 2);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.NUMERIC, 2);
        pnd_Do_Not_Send_Letter = localVariables.newFieldInRecord("pnd_Do_Not_Send_Letter", "#DO-NOT-SEND-LETTER", FieldType.BOOLEAN, 1);

        pnd_Work = localVariables.newGroupInRecord("pnd_Work", "#WORK");
        pnd_Work_Pnd_Wk_Compressed_Nme = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_Compressed_Nme", "#WK-COMPRESSED-NME", FieldType.STRING, 40);
        pnd_Work_Pnd_Wk_F_Name = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_F_Name", "#WK-F-NAME", FieldType.STRING, 35);
        pnd_Work_Pnd_Wk_L_Name = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_L_Name", "#WK-L-NAME", FieldType.STRING, 35);
        pnd_Work_Pnd_Wk_M_Name = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_M_Name", "#WK-M-NAME", FieldType.STRING, 35);
        pnd_Work_Pnd_Wk_Zip = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_Zip", "#WK-ZIP", FieldType.STRING, 9);

        pnd_Work__R_Field_6 = pnd_Work.newGroupInGroup("pnd_Work__R_Field_6", "REDEFINE", pnd_Work_Pnd_Wk_Zip);
        pnd_Work_Pnd_Wk_Zip5 = pnd_Work__R_Field_6.newFieldInGroup("pnd_Work_Pnd_Wk_Zip5", "#WK-ZIP5", FieldType.STRING, 5);
        pnd_Work_Pnd_Wk_Zip4 = pnd_Work__R_Field_6.newFieldInGroup("pnd_Work_Pnd_Wk_Zip4", "#WK-ZIP4", FieldType.STRING, 4);
        pnd_Work_Pnd_Wk_Zip9 = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_Zip9", "#WK-ZIP9", FieldType.STRING, 10);
        pnd_Work_Pnd_Wk_Cntr = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_Cntr", "#WK-CNTR", FieldType.INTEGER, 2);
        pnd_Work_Pnd_Wk_City = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_City", "#WK-CITY", FieldType.STRING, 29);
        pnd_Work_Pnd_Wk_City_State_Zip9 = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Wk_City_State_Zip9", "#WK-CITY-STATE-ZIP9", FieldType.STRING, 35);
        pnd_Work_Pnd_Addr2_Fnd = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Addr2_Fnd", "#ADDR2-FND", FieldType.BOOLEAN, 1);
        pnd_Work_Pnd_Addr3_Fnd = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Addr3_Fnd", "#ADDR3-FND", FieldType.BOOLEAN, 1);
        pnd_Work_Pnd_Dont_Print = pnd_Work.newFieldInGroup("pnd_Work_Pnd_Dont_Print", "#DONT-PRINT", FieldType.BOOLEAN, 1);

        vw_ads_Cntl_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntl_View", "ADS-CNTL-VIEW"), "ADS_CNTL", "ADS_CNTL");
        ads_Cntl_View_Ads_Rcrd_Cde = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Rcrd_Cde", "ADS-RCRD-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ADS_RCRD_CDE");

        ads_Cntl_View_Ads_Cntl_Grp = vw_ads_Cntl_View.getRecord().newGroupInGroup("ADS_CNTL_VIEW_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_DTE");

        ads_Cntl_View__R_Field_7 = ads_Cntl_View_Ads_Cntl_Grp.newGroupInGroup("ads_Cntl_View__R_Field_7", "REDEFINE", ads_Cntl_View_Ads_Cntl_Bsnss_Dte);
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A = ads_Cntl_View__R_Field_7.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A", "ADS-CNTL-BSNSS-DTE-A", FieldType.STRING, 
            8);
        ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte", "ADS-CNTL-BSNSS-TMRRW-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_TMRRW_DTE");

        ads_Cntl_View__R_Field_8 = ads_Cntl_View_Ads_Cntl_Grp.newGroupInGroup("ads_Cntl_View__R_Field_8", "REDEFINE", ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte);
        ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte_A = ads_Cntl_View__R_Field_8.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte_A", "ADS-CNTL-BSNSS-TMRRW-DTE-A", 
            FieldType.STRING, 8);
        ads_Cntl_View_Ads_Rpt_13 = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Rpt_13", "ADS-RPT-13", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADS_RPT_13");
        registerRecord(vw_ads_Cntl_View);

        pnd_Cntl_Bsnss_Prev_Dte = localVariables.newFieldInRecord("pnd_Cntl_Bsnss_Prev_Dte", "#CNTL-BSNSS-PREV-DTE", FieldType.DATE);
        pnd_Cntl_Bsnss_Dte = localVariables.newFieldInRecord("pnd_Cntl_Bsnss_Dte", "#CNTL-BSNSS-DTE", FieldType.DATE);
        pnd_Cntl_Bsnss_Tmrrw_Dte = localVariables.newFieldInRecord("pnd_Cntl_Bsnss_Tmrrw_Dte", "#CNTL-BSNSS-TMRRW-DTE", FieldType.DATE);
        pnd_Status = localVariables.newFieldInRecord("pnd_Status", "#STATUS", FieldType.STRING, 3);
        pnd_Ads_Cntl_Super_De_1 = localVariables.newFieldInRecord("pnd_Ads_Cntl_Super_De_1", "#ADS-CNTL-SUPER-DE-1", FieldType.STRING, 9);

        pnd_Ads_Cntl_Super_De_1__R_Field_9 = localVariables.newGroupInRecord("pnd_Ads_Cntl_Super_De_1__R_Field_9", "REDEFINE", pnd_Ads_Cntl_Super_De_1);
        pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Rcrd_Typ = pnd_Ads_Cntl_Super_De_1__R_Field_9.newFieldInGroup("pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Rcrd_Typ", 
            "#ADS-CNTL-RCRD-TYP", FieldType.STRING, 1);
        pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Bsnss_Dt = pnd_Ads_Cntl_Super_De_1__R_Field_9.newFieldInGroup("pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Bsnss_Dt", 
            "#ADS-CNTL-BSNSS-DT", FieldType.NUMERIC, 8);
        pnd_First_Plan_Call = localVariables.newFieldInRecord("pnd_First_Plan_Call", "#FIRST-PLAN-CALL", FieldType.BOOLEAN, 1);
        pnd_Print = localVariables.newFieldInRecord("pnd_Print", "#PRINT", FieldType.BOOLEAN, 1);
        pnd_Update_On = localVariables.newFieldInRecord("pnd_Update_On", "#UPDATE-ON", FieldType.BOOLEAN, 1);
        pnd_Letter_Only = localVariables.newFieldInRecord("pnd_Letter_Only", "#LETTER-ONLY", FieldType.BOOLEAN, 1);
        pnd_Exception = localVariables.newFieldInRecord("pnd_Exception", "#EXCEPTION", FieldType.BOOLEAN, 1);
        pnd_Letters_Written = localVariables.newFieldInRecord("pnd_Letters_Written", "#LETTERS-WRITTEN", FieldType.PACKED_DECIMAL, 7);
        pnd_Error_Report_Entries = localVariables.newFieldInRecord("pnd_Error_Report_Entries", "#ERROR-REPORT-ENTRIES", FieldType.PACKED_DECIMAL, 7);
        pnd_Psg_Errors = localVariables.newFieldInRecord("pnd_Psg_Errors", "#PSG-ERRORS", FieldType.PACKED_DECIMAL, 7);

        pnd_Adsn998_Error_Handler = localVariables.newGroupInRecord("pnd_Adsn998_Error_Handler", "#ADSN998-ERROR-HANDLER");
        pnd_Adsn998_Error_Handler_Pnd_Calling_Program = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Calling_Program", "#CALLING-PROGRAM", 
            FieldType.STRING, 8);
        pnd_Adsn998_Error_Handler_Pnd_Unique_Id = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Unique_Id", "#UNIQUE-ID", FieldType.NUMERIC, 
            12);
        pnd_Adsn998_Error_Handler_Pnd_Step_Name = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Step_Name", "#STEP-NAME", FieldType.STRING, 
            8);
        pnd_Adsn998_Error_Handler_Pnd_Appl_Id = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Appl_Id", "#APPL-ID", FieldType.STRING, 
            8);
        pnd_Adsn998_Error_Handler_Pnd_Error_Msg = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 
            79);

        pnd_Terminate = localVariables.newGroupInRecord("pnd_Terminate", "#TERMINATE");
        pnd_Terminate_Pnd_Terminate_No = pnd_Terminate.newFieldInGroup("pnd_Terminate_Pnd_Terminate_No", "#TERMINATE-NO", FieldType.PACKED_DECIMAL, 3);
        pnd_Terminate_Pnd_Terminate_Msg = pnd_Terminate.newFieldInGroup("pnd_Terminate_Pnd_Terminate_Msg", "#TERMINATE-MSG", FieldType.STRING, 177);

        pnd_Terminate__R_Field_10 = pnd_Terminate.newGroupInGroup("pnd_Terminate__R_Field_10", "REDEFINE", pnd_Terminate_Pnd_Terminate_Msg);
        pnd_Terminate_Pnd_Terminate_Msg1 = pnd_Terminate__R_Field_10.newFieldInGroup("pnd_Terminate_Pnd_Terminate_Msg1", "#TERMINATE-MSG1", FieldType.STRING, 
            59);
        pnd_Terminate_Pnd_Terminate_Msg2 = pnd_Terminate__R_Field_10.newFieldInGroup("pnd_Terminate_Pnd_Terminate_Msg2", "#TERMINATE-MSG2", FieldType.STRING, 
            59);
        pnd_Terminate_Pnd_Terminate_Msg3 = pnd_Terminate__R_Field_10.newFieldInGroup("pnd_Terminate_Pnd_Terminate_Msg3", "#TERMINATE-MSG3", FieldType.STRING, 
            59);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Prtcpnt_View.reset();
        vw_cwf_Org_Unit_Tbl_View.reset();
        vw_naz_Table_Ddm.reset();
        vw_ads_Cntl_View.reset();

        ldaPstl0185.initializeValues();

        localVariables.reset();
        sort_Key2_Sort_Key_Lgth.setInitialValue(1);
        pnd_First_Plan_Call.setInitialValue(true);
        pnd_Print.setInitialValue(false);
        pnd_Update_On.setInitialValue(true);
        pnd_Letter_Only.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp560() throws Exception
    {
        super("Adsp560");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("ADSP560", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                    O N   E R R O R   R O U T I N E
        //*                                                                                                                                                               //Natural: ON ERROR;//Natural: FORMAT ( 1 ) LS = 132 PS = 60;//Natural: AT TOP OF PAGE ( 1 )
                                                                                                                                                                          //Natural: PERFORM ERROR-LOG
        sub_Error_Log();
        if (condition(Global.isEscape())) {return;}
        pnd_Error_Text_Pnd_Star_1.setValue("* ");                                                                                                                         //Natural: MOVE '* ' TO #STAR-1
        //*  FOR POST & EFM CONSTRUCT
        pnd_Error_Text_Pnd_Star_2.setValue(" *");                                                                                                                         //Natural: MOVE ' *' TO #STAR-2
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        pnd_Name_Saved.getValue("*").reset();                                                                                                                             //Natural: RESET #NAME-SAVED ( * )
                                                                                                                                                                          //Natural: PERFORM GET-DATE
        sub_Get_Date();
        if (condition(Global.isEscape())) {return;}
        vw_ads_Prtcpnt_View.startDatabaseRead                                                                                                                             //Natural: READ ADS-PRTCPNT-VIEW BY ADP-LST-ACTVTY-DTE STARTING FROM #CNTL-BSNSS-PREV-DTE
        (
        "READ1",
        new Wc[] { new Wc("ADP_LST_ACTVTY_DTE", ">=", pnd_Cntl_Bsnss_Prev_Dte, WcType.BY) },
        new Oc[] { new Oc("ADP_LST_ACTVTY_DTE", "ASC") }
        );
        READ1:
        while (condition(vw_ads_Prtcpnt_View.readNextRow("READ1")))
        {
            //*    THRU #CNTL-BSNSS-PREV-DTE
            //*  060412 START
            pnd_Exception.reset();                                                                                                                                        //Natural: RESET #EXCEPTION
            if (condition(ads_Prtcpnt_View_Adp_Ntfctn_Lttr_Ind.equals(" ")))                                                                                              //Natural: IF ADS-PRTCPNT-VIEW.ADP-NTFCTN-LTTR-IND = ' '
            {
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-EXCEPTION
                sub_Check_For_Exception();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  ACCEPT IF ADS-PRTCPNT-VIEW.ADP-IRC-CDE= '01' OR = '02'
            //*  0411814 START
            if (condition(DbsUtil.is(ads_Prtcpnt_View_Adp_Irc_Cde.getText(),"N2")))                                                                                       //Natural: IF ADS-PRTCPNT-VIEW.ADP-IRC-CDE IS ( N2 )
            {
                pnd_B.compute(new ComputeParameters(false, pnd_B), ads_Prtcpnt_View_Adp_Irc_Cde.val());                                                                   //Natural: ASSIGN #B := VAL ( ADS-PRTCPNT-VIEW.ADP-IRC-CDE )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_B.reset();                                                                                                                                            //Natural: RESET #B
            }                                                                                                                                                             //Natural: END-IF
            //*  ACCEPT IF ((ADS-PRTCPNT-VIEW.ADP-IRC-CDE= '01' OR = '02')
            //*  041814 END
            //*  060412  END
            //*  EM - 050715
            //*  EM - 050715
            if (condition(!(((((pnd_B.equals(1) || pnd_B.equals(2)) || pnd_Exception.getBoolean()) && ads_Prtcpnt_View_Adp_Ntfctn_Lttr_Ind.equals(" "))                   //Natural: ACCEPT IF ( ( #B = 1 OR = 2 ) OR #EXCEPTION ) AND ADS-PRTCPNT-VIEW.ADP-NTFCTN-LTTR-IND = ' ' AND ( ADS-PRTCPNT-VIEW.ADP-BENE-CLIENT-IND = '0' OR ADS-PRTCPNT-VIEW.ADP-BENE-CLIENT-IND GT '3' )
                && (ads_Prtcpnt_View_Adp_Bene_Client_Ind.equals("0") || ads_Prtcpnt_View_Adp_Bene_Client_Ind.greater("3"))))))
            {
                continue;
            }
            if (condition(ads_Prtcpnt_View_Adp_Lst_Actvty_Dte.greater(pnd_Cntl_Bsnss_Prev_Dte)))                                                                          //Natural: IF ADP-LST-ACTVTY-DTE > #CNTL-BSNSS-PREV-DTE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Print.getBoolean()))                                                                                                                        //Natural: IF #PRINT
            {
                getReports().write(0, "Found a record with","=",ads_Prtcpnt_View_Adp_Stts_Cde,"and","=",ads_Prtcpnt_View_Adp_Annt_Typ_Cde);                               //Natural: WRITE 'Found a record with' '=' ADP-STTS-CDE 'and' '=' ADP-ANNT-TYP-CDE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  LETTERS ARE CREATED FOR ALL STATUS CODES EXCEPT THOSE THAT
            //*  START WITH 'E', 'D', 'F' OR 'W'.
            //*  STATUSES STARTING WITH 'M', 'T' OR 'L' SHOULD GENERATE
            //*  LETTERS.
            if (condition(ads_Prtcpnt_View_Adp_Stts_Cde.getSubstring(1,1).equals("E") || ads_Prtcpnt_View_Adp_Stts_Cde.getSubstring(1,1).equals("F") ||                   //Natural: IF SUBSTRING ( ADP-STTS-CDE,1,1 ) EQ 'E' OR SUBSTRING ( ADP-STTS-CDE,1,1 ) EQ 'F' OR SUBSTRING ( ADP-STTS-CDE,1,1 ) EQ 'D'
                ads_Prtcpnt_View_Adp_Stts_Cde.getSubstring(1,1).equals("D")))
            {
                getReports().write(1, ads_Prtcpnt_View_Adp_Unique_Id,new ColumnSpacing(3),ads_Prtcpnt_View_Adp_Effctv_Dte, new ReportEditMask ("MM/DD/YYYY"),new          //Natural: WRITE ( 1 ) ADP-UNIQUE-ID 3X ADP-EFFCTV-DTE ( EM = MM/DD/YYYY ) 3X ADP-STTS-CDE 36T 'No letter produced, see status code'
                    ColumnSpacing(3),ads_Prtcpnt_View_Adp_Stts_Cde,new TabSetting(36),"No letter produced, see status code");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Error_Report_Entries.nadd(1);                                                                                                                         //Natural: ADD 1 TO #ERROR-REPORT-ENTRIES
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!ads_Prtcpnt_View_Adp_Stts_Cde.getSubstring(1,1).equals("M") && !ads_Prtcpnt_View_Adp_Stts_Cde.getSubstring(1,1).equals("T")                    //Natural: IF SUBSTRING ( ADP-STTS-CDE,1,1 ) NE 'M' AND SUBSTRING ( ADP-STTS-CDE,1,1 ) NE 'T' AND SUBSTRING ( ADP-STTS-CDE,1,1 ) NE 'L'
                && !ads_Prtcpnt_View_Adp_Stts_Cde.getSubstring(1,1).equals("L")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ads_Prtcpnt_View_Adp_Annt_Typ_Cde.equals("R")))                                                                                                 //Natural: IF ADP-ANNT-TYP-CDE = 'R'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  012010 START
                                                                                                                                                                          //Natural: PERFORM CHECK-PLAN
            sub_Check_Plan();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Work_Pnd_Dont_Print.getBoolean()))                                                                                                          //Natural: IF #DONT-PRINT
            {
                getReports().write(1, ads_Prtcpnt_View_Adp_Unique_Id,new ColumnSpacing(3),ads_Prtcpnt_View_Adp_Effctv_Dte, new ReportEditMask ("MM/DD/YYYY"),new          //Natural: WRITE ( 1 ) ADP-UNIQUE-ID 3X ADP-EFFCTV-DTE ( EM = MM/DD/YYYY ) 3X ADP-STTS-CDE 36T 'No letter produced because of plan' ADP-PLAN-NBR
                    ColumnSpacing(3),ads_Prtcpnt_View_Adp_Stts_Cde,new TabSetting(36),"No letter produced because of plan",ads_Prtcpnt_View_Adp_Plan_Nbr);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Error_Report_Entries.nadd(1);                                                                                                                         //Natural: ADD 1 TO #ERROR-REPORT-ENTRIES
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  012010 END
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM MOVE-PARTICIPANT-DATA
            sub_Move_Participant_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Print.getBoolean()))                                                                                                                        //Natural: IF #PRINT
            {
                getReports().write(0, "Processing a letter for this date","=",ads_Prtcpnt_View_Adp_Lst_Actvty_Dte);                                                       //Natural: WRITE 'Processing a letter for this date' '=' ADP-LST-ACTVTY-DTE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  02/02/99  3 LINES
            if (condition(pnd_Do_Not_Send_Letter.getBoolean()))                                                                                                           //Natural: IF #DO-NOT-SEND-LETTER
            {
                if (condition(pnd_Print.getBoolean()))                                                                                                                    //Natural: IF #PRINT
                {
                    getReports().write(0, "Do not Send Letter condition, no letter produced");                                                                            //Natural: WRITE 'Do not Send Letter condition, no letter produced'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-SUPERVISOR-RACF-ID
            sub_Get_Supervisor_Racf_Id();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CALL-POST-FOR-OPEN
            sub_Call_Post_For_Open();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                              //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
            {
                                                                                                                                                                          //Natural: PERFORM POST-ERROR
                sub_Post_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALL-POST-FOR-WRITE
            sub_Call_Post_For_Write();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                              //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
            {
                                                                                                                                                                          //Natural: PERFORM POST-ERROR
                sub_Post_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Psg_Error.getBoolean()))                                                                                                                    //Natural: IF #PSG-ERROR
            {
                                                                                                                                                                          //Natural: PERFORM PSG-ERROR
                sub_Psg_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALL-POST-FOR-POWER-IMAGE
            sub_Call_Post_For_Power_Image();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                              //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
            {
                                                                                                                                                                          //Natural: PERFORM POST-ERROR
                sub_Post_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALL-POST-FOR-CLOSE
            sub_Call_Post_For_Close();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                              //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
            {
                                                                                                                                                                          //Natural: PERFORM POST-ERROR
                sub_Post_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "Successfully stored a letter on POST for"," PIN=",ads_Prtcpnt_View_Adp_Unique_Id,"RQST-ID=",ads_Prtcpnt_View_Rqst_Id);             //Natural: WRITE 'Successfully stored a letter on POST for' ' PIN=' ADP-UNIQUE-ID 'RQST-ID=' ADS-PRTCPNT-VIEW.RQST-ID
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Update_On.getBoolean()))                                                                                                                    //Natural: IF #UPDATE-ON
            {
                pnd_Letters_Written.nadd(1);                                                                                                                              //Natural: ADD 1 TO #LETTERS-WRITTEN
                if (condition(! (pnd_Letter_Only.getBoolean())))                                                                                                          //Natural: IF NOT #LETTER-ONLY
                {
                    PND_PND_L3960:                                                                                                                                        //Natural: GET ADS-PRTCPNT-VIEW *ISN
                    vw_ads_Prtcpnt_View.readByID(vw_ads_Prtcpnt_View.getAstISN("READ1"), "PND_PND_L3960");
                    ads_Prtcpnt_View_Adp_Ntfctn_Lttr_Ind.setValue("Y");                                                                                                   //Natural: ASSIGN ADP-NTFCTN-LTTR-IND := 'Y'
                    vw_ads_Prtcpnt_View.updateDBRow("PND_PND_L3960");                                                                                                     //Natural: UPDATE ( ##L3960. )
                }                                                                                                                                                         //Natural: END-IF
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB-MESSAGE
        sub_End_Of_Job_Message();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Letters_Written.notEquals(getZero())))                                                                                                          //Natural: IF #LETTERS-WRITTEN NE 0
        {
            pdaScia8200.getCon_Part_Rec_Con_Eof_Sw().setValue("Y");                                                                                                       //Natural: ASSIGN CON-EOF-SW := 'Y'
            //*  SGRD5 KG
            DbsUtil.callnat(Scin8200.class , getCurrentProcessState(), pdaScia8200.getCon_File_Open_Sw(), pdaScia8200.getCon_Part_Rec());                                 //Natural: CALLNAT 'SCIN8200' CON-FILE-OPEN-SW CON-PART-REC
            if (condition(Global.isEscape())) return;
            if (condition(pdaScia8200.getCon_Part_Rec_Con_Return_Code().notEquals(getZero()) || pdaScia8200.getCon_Part_Rec_Con_Contact_Err_Msg().notEquals(" ")))        //Natural: IF CON-RETURN-CODE NE 0 OR CON-CONTACT-ERR-MSG NE ' '
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-LOG
                sub_Error_Log();
                if (condition(Global.isEscape())) {return;}
                getReports().write(0, "*CALL ERROR (PSG9061): on final CLOSE call");                                                                                      //Natural: WRITE '*CALL ERROR (PSG9061): on final CLOSE call'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "No letters written, Oracle not called on EOJ");                                                                                        //Natural: WRITE 'No letters written, Oracle not called on EOJ'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-PARTICIPANT-DATA
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-INSTITUTION-ADDRESS
        //*    OR #LETTERS-WRITTEN = 1 AND #PSG-ERRORS = 0 DEBUG FORCE ERROR
        //*  WHICH ADDRESS LINES THAT WERE RETURNED ARE POPULATED?
        //*  -----------------------------------------------------
        //*    BOTH ADDRESS LINES BLANK
        //*    ------------------------
        //*    LINE 2 HAS DATA, LINE 3 DOES NOT
        //*    --------------------------------
        //*    LINE 2 & 3 ARE BOTH BLANK
        //*    -------------------------
        //*      MOVE CON-ADDRESS-LINE2 TO INSTITUTION-ADDRESS(2)
        //*      MOVE CON-ADDRESS-LINE3 TO INSTITUTION-ADDRESS(3)
        //*    LINE 2 BLANK BUT LINE 3 HAS DATA
        //*    --------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POST-ERROR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PSG-ERROR
        //*  DEFINE SUBROUTINE MOVE-CONTACT-NAME-ADDRESS
        //*  RESET INSTITUTION-ADDRESS (*)
        //*  MOVE #COMPRESSED-NME (1) TO PLAN-ADMINISTRATOR
        //*   INSTITUTION-NAME
        //*  IF #TITLE-NME (1) = ' '
        //*   IF #ORG-NME (1) = ' '
        //*     MOVE INSTITUTION-ADDRESS1 (1) TO INSTITUTION-ADDRESS (1)
        //*     MOVE INSTITUTION-ADDRESS2 (1) TO INSTITUTION-ADDRESS (2)
        //*     MOVE INSTITUTION-ADDRESS3 (1) TO INSTITUTION-ADDRESS (3)
        //*     MOVE INSTITUTION-ADDRESS4 (1) TO INSTITUTION-ADDRESS (4)
        //*     MOVE INSTITUTION-ADDRESS5 (1) TO INSTITUTION-ADDRESS (5)
        //*   ELSE
        //*     MOVE #ORG-NME        (1) TO INSTITUTION-ADDRESS (1)
        //*     MOVE INSTITUTION-ADDRESS1 (1) TO INSTITUTION-ADDRESS (2)
        //*     MOVE INSTITUTION-ADDRESS2 (1) TO INSTITUTION-ADDRESS (3)
        //*     MOVE INSTITUTION-ADDRESS3 (1) TO INSTITUTION-ADDRESS (4)
        //*     MOVE INSTITUTION-ADDRESS4 (1) TO INSTITUTION-ADDRESS (5)
        //*   END-IF
        //*  ELSE
        //*   IF #ORG-NME (1) = ' '
        //*     MOVE #TITLE-NME      (1) TO INSTITUTION-ADDRESS (1)
        //*     MOVE INSTITUTION-ADDRESS1 (1) TO INSTITUTION-ADDRESS (2)
        //*     MOVE INSTITUTION-ADDRESS2 (1) TO INSTITUTION-ADDRESS (3)
        //*     MOVE INSTITUTION-ADDRESS3 (1) TO INSTITUTION-ADDRESS (4)
        //*     MOVE INSTITUTION-ADDRESS4 (1) TO INSTITUTION-ADDRESS (5)
        //*   ELSE
        //*     IF INSTITUTION-ADDRESS4 (1) = ' '
        //*       MOVE #TITLE-NME      (1) TO INSTITUTION-ADDRESS (1)
        //*       MOVE #ORG-NME        (1) TO INSTITUTION-ADDRESS (2)
        //*       MOVE INSTITUTION-ADDRESS1 (1) TO INSTITUTION-ADDRESS (3)
        //*       MOVE INSTITUTION-ADDRESS2 (1) TO INSTITUTION-ADDRESS (4)
        //*       MOVE INSTITUTION-ADDRESS3 (1) TO INSTITUTION-ADDRESS (5)
        //*     ELSE
        //*       IF INSTITUTION-ADDRESS5 (1) = ' '
        //*         MOVE #TITLE-NME      (1) TO INSTITUTION-ADDRESS (1)
        //*         MOVE INSTITUTION-ADDRESS1 (1) TO INSTITUTION-ADDRESS (2)
        //*         MOVE INSTITUTION-ADDRESS2 (1) TO INSTITUTION-ADDRESS (3)
        //*         MOVE INSTITUTION-ADDRESS3 (1) TO INSTITUTION-ADDRESS (4)
        //*         MOVE INSTITUTION-ADDRESS4 (1) TO INSTITUTION-ADDRESS (5)
        //*       ELSE
        //*         MOVE INSTITUTION-ADDRESS1 (1) TO INSTITUTION-ADDRESS (1)
        //*         MOVE INSTITUTION-ADDRESS2 (1) TO INSTITUTION-ADDRESS (2)
        //*         MOVE INSTITUTION-ADDRESS3 (1) TO INSTITUTION-ADDRESS (3)
        //*         MOVE INSTITUTION-ADDRESS4 (1) TO INSTITUTION-ADDRESS (4)
        //*         MOVE INSTITUTION-ADDRESS5 (1) TO INSTITUTION-ADDRESS (5)
        //*       END-IF
        //*     END-IF
        //*   END-IF
        //*  END-IF
        //*  END-SUBROUTINE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-SUPERVISOR-RACF-ID
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DATE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST-FOR-OPEN
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST-FOR-WRITE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST-FOR-CLOSE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST-FOR-POWER-IMAGE
        //* **************************************************060412***************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-EXCEPTION
        //* ***********************************************************************
        //* **************************************************012010***************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PLAN
        //* **************************************************111109***************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-PLAN-ADMIN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-ROUTINE-FOR-POST
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TERMINATE-ROUTINE
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-JOB-MESSAGE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-LOG
    }
    private void sub_Move_Participant_Data() throws Exception                                                                                                             //Natural: MOVE-PARTICIPANT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Annt_Name.setValue(DbsUtil.compress(ads_Prtcpnt_View_Adp_Frst_Annt_Title_Cde, ads_Prtcpnt_View_Adp_Frst_Annt_Frst_Nme, ads_Prtcpnt_View_Adp_Frst_Annt_Mid_Nme,  //Natural: COMPRESS ADP-FRST-ANNT-TITLE-CDE ADP-FRST-ANNT-FRST-NME ADP-FRST-ANNT-MID-NME ADP-FRST-ANNT-LST-NME INTO #ANNT-NAME
            ads_Prtcpnt_View_Adp_Frst_Annt_Lst_Nme));
        DbsUtil.callnat(Adsn553.class , getCurrentProcessState(), pnd_Annt_Name);                                                                                         //Natural: CALLNAT 'ADSN553' #ANNT-NAME
        if (condition(Global.isEscape())) return;
        pnd_Post_Error.reset();                                                                                                                                           //Natural: RESET #POST-ERROR #EFM-ERROR
        pnd_Efm_Error.reset();
    }
    private void sub_Get_Institution_Address() throws Exception                                                                                                           //Natural: GET-INSTITUTION-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_First_Plan_Call.getBoolean()))                                                                                                                  //Natural: IF #FIRST-PLAN-CALL
        {
            pnd_First_Plan_Call.setValue(false);                                                                                                                          //Natural: ASSIGN #FIRST-PLAN-CALL := FALSE
            pdaScia8200.getCon_File_Open_Sw().setValue("N");                                                                                                              //Natural: ASSIGN CON-FILE-OPEN-SW := 'N'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaScia8200.getCon_File_Open_Sw().setValue(" ");                                                                                                              //Natural: ASSIGN CON-FILE-OPEN-SW := ' '
            //*  SGRD5 KG
        }                                                                                                                                                                 //Natural: END-IF
        pdaScia8200.getCon_Part_Rec_Con_Eof_Sw().setValue("N");                                                                                                           //Natural: ASSIGN CON-EOF-SW := 'N'
        pdaScia8200.getCon_Part_Rec_Con_Function_Code().getValue(1).setValue("08");                                                                                       //Natural: ASSIGN CON-FUNCTION-CODE ( 1 ) := '08'
        pdaScia8200.getCon_Part_Rec_Con_Access_Level().setValue(" ");                                                                                                     //Natural: ASSIGN CON-ACCESS-LEVEL := ' '
        pdaScia8200.getCon_Part_Rec_Con_Contact_Type().setValue("E");                                                                                                     //Natural: ASSIGN CON-CONTACT-TYPE := 'E'
        pdaScia8200.getCon_Part_Rec_Con_Plan_Num().setValue(ads_Prtcpnt_View_Adp_Plan_Nbr);                                                                               //Natural: ASSIGN CON-PLAN-NUM := ADP-PLAN-NBR
        if (condition(pnd_Print.getBoolean()))                                                                                                                            //Natural: IF #PRINT
        {
            getReports().write(0, "Calling Oracle with","=",ads_Prtcpnt_View_Adp_Plan_Nbr);                                                                               //Natural: WRITE 'Calling Oracle with' '=' ADP-PLAN-NBR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  SGRD5 KG
        DbsUtil.callnat(Scin8200.class , getCurrentProcessState(), pdaScia8200.getCon_File_Open_Sw(), pdaScia8200.getCon_Part_Rec());                                     //Natural: CALLNAT 'SCIN8200' CON-FILE-OPEN-SW CON-PART-REC
        if (condition(Global.isEscape())) return;
        //*   #PRINT
        getReports().write(0, "=",pdaScia8200.getCon_Part_Rec_Con_Contact_Name(),NEWLINE,"=",pdaScia8200.getCon_Part_Rec_Con_Plan_Id(),NEWLINE,"=",pdaScia8200.getCon_Part_Rec_Con_Emplyer_Id(), //Natural: WRITE '=' CON-CONTACT-NAME /'=' CON-PLAN-ID /'=' CON-EMPLYER-ID / '=' CON-COMPANY-NAME / '=' CON-E-MAIL-ADDRESS / '=' CON-PLAN-NAME / '=' CON-ZIP / '=' CON-CITY / '=' CON-ADDRESS-LINE1 / '=' CON-ADDRESS-LINE2 / '=' CON-ADDRESS-LINE3 / '=' CON-ADDRESS-LINE4
            NEWLINE,"=",pdaScia8200.getCon_Part_Rec_Con_Company_Name(),NEWLINE,"=",pdaScia8200.getCon_Part_Rec_Con_E_Mail_Address(),NEWLINE,"=",pdaScia8200.getCon_Part_Rec_Con_Plan_Name(),
            NEWLINE,"=",pdaScia8200.getCon_Part_Rec_Con_Zip(),NEWLINE,"=",pdaScia8200.getCon_Part_Rec_Con_City(),NEWLINE,"=",pdaScia8200.getCon_Part_Rec_Con_Address_Line1(),
            NEWLINE,"=",pdaScia8200.getCon_Part_Rec_Con_Address_Line2(),NEWLINE,"=",pdaScia8200.getCon_Part_Rec_Con_Address_Line3(),NEWLINE,"=",pdaScia8200.getCon_Part_Rec_Con_Address_Line4());
        if (Global.isEscape()) return;
        //*  END-IF
        if (condition(pdaScia8200.getCon_Part_Rec_Con_Return_Code().notEquals(getZero()) || pdaScia8200.getCon_Part_Rec_Con_Contact_Err_Msg().notEquals(" ")))            //Natural: IF CON-RETURN-CODE NE 0 OR CON-CONTACT-ERR-MSG NE ' '
        {
            pnd_Psg_Errors.nadd(1);                                                                                                                                       //Natural: ASSIGN #PSG-ERRORS := #PSG-ERRORS + 1
            pnd_Psg_Error.setValue(true);                                                                                                                                 //Natural: ASSIGN #PSG-ERROR := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Psg_Error.setValue(false);                                                                                                                                //Natural: ASSIGN #PSG-ERROR := FALSE
            //*  CREATE NAME VALUES
            //*  ------------------
            //*  060710 START
            DbsUtil.examine(new ExamineSource(pdaScia8200.getCon_Part_Rec_Con_Contact_Name()), new ExamineSearch(","), new ExamineGivingNumber(pnd_A));                   //Natural: EXAMINE CON-CONTACT-NAME FOR ',' GIVING NUMBER IN #A
            if (condition(pnd_A.greater(1)))                                                                                                                              //Natural: IF #A GT 1
            {
                getReports().write(0, "=",pdaScia8200.getCon_Part_Rec_Con_Contact_Name()," has more than 1 comma");                                                       //Natural: WRITE '=' CON-CONTACT-NAME ' has more than 1 comma'
                if (Global.isEscape()) return;
                pnd_Work_Pnd_Wk_Compressed_Nme.setValue("NONE");                                                                                                          //Natural: ASSIGN #WK-COMPRESSED-NME := 'NONE'
                //*  060710 END
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaScia8200.getCon_Part_Rec_Con_Contact_Name().separate(SeparateOption.WithAnyDelimiters, ",", pnd_Work_Pnd_Wk_L_Name, pnd_Work_Pnd_Wk_F_Name);           //Natural: SEPARATE CON-CONTACT-NAME INTO #WK-L-NAME #WK-F-NAME WITH DELIMITERS ','
                pnd_Work_Pnd_Wk_L_Name.setValue(pnd_Work_Pnd_Wk_L_Name, MoveOption.LeftJustified);                                                                        //Natural: MOVE LEFT JUSTIFIED #WK-L-NAME TO #WK-L-NAME
                pnd_Work_Pnd_Wk_F_Name.setValue(pnd_Work_Pnd_Wk_F_Name, MoveOption.LeftJustified);                                                                        //Natural: MOVE LEFT JUSTIFIED #WK-F-NAME TO #WK-F-NAME
                pnd_Work_Pnd_Wk_Compressed_Nme.setValue(DbsUtil.compress(pnd_Work_Pnd_Wk_F_Name, pnd_Work_Pnd_Wk_L_Name));                                                //Natural: COMPRESS #WK-F-NAME #WK-L-NAME INTO #WK-COMPRESSED-NME
                //*  060710
            }                                                                                                                                                             //Natural: END-IF
            //*  CREATE ADDRESS VALUES
            //*  ---------------------
            pnd_Work_Pnd_Wk_Zip.setValue(pdaScia8200.getCon_Part_Rec_Con_Zip());                                                                                          //Natural: MOVE CON-ZIP TO #WK-ZIP
            if (condition(pnd_Work_Pnd_Wk_Zip4.equals(" ")))                                                                                                              //Natural: IF #WK-ZIP4 = ' '
            {
                pnd_Work_Pnd_Wk_Zip4.setValue("0000");                                                                                                                    //Natural: MOVE '0000' TO #WK-ZIP4
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Pnd_Wk_Zip9.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Work_Pnd_Wk_Zip5, "-", pnd_Work_Pnd_Wk_Zip4));                              //Natural: COMPRESS #WK-ZIP5 '-' #WK-ZIP4 INTO #WK-ZIP9 LEAVING NO
            //*  PLACE COMMA AFTER CITY
            //*  ----------------------
            pnd_Work_Pnd_Wk_City.setValue(pdaScia8200.getCon_Part_Rec_Con_City());                                                                                        //Natural: MOVE CON-CITY TO #WK-CITY
            FOR01:                                                                                                                                                        //Natural: FOR #WK-CNTR 28 TO 1 STEP -1
            for (pnd_Work_Pnd_Wk_Cntr.setValue(28); condition(pnd_Work_Pnd_Wk_Cntr.greaterOrEqual(1)); pnd_Work_Pnd_Wk_Cntr.nsubtract(1))
            {
                if (condition(!pnd_Work_Pnd_Wk_City.getSubstring(pnd_Work_Pnd_Wk_Cntr.getInt()).equals(" ")))                                                             //Natural: IF SUBSTRING ( #WK-CITY,#WK-CNTR ) NE ' '
                {
                    pnd_Work_Pnd_Wk_Cntr.nadd(1);                                                                                                                         //Natural: ADD 1 TO #WK-CNTR
                    setValueToSubstring(",",pnd_Work_Pnd_Wk_City,pnd_Work_Pnd_Wk_Cntr.getInt());                                                                          //Natural: MOVE ',' TO SUBSTRING ( #WK-CITY,#WK-CNTR )
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //*  BUILD THE CITY, STATE ZIP9
            //*  --------------------------
            //*  031910
            //*  031910
            pnd_Work_Pnd_Wk_City_State_Zip9.setValue(DbsUtil.compress(pnd_Work_Pnd_Wk_City, pdaScia8200.getCon_Part_Rec_Con_State(), pnd_Work_Pnd_Wk_Zip9));              //Natural: COMPRESS #WK-CITY CON-STATE #WK-ZIP9 INTO #WK-CITY-STATE-ZIP9
            pnd_Work_Pnd_Addr2_Fnd.setValue(false);                                                                                                                       //Natural: ASSIGN #ADDR2-FND := FALSE
            pnd_Work_Pnd_Addr3_Fnd.setValue(false);                                                                                                                       //Natural: ASSIGN #ADDR3-FND := FALSE
            short decideConditionsMet1180 = 0;                                                                                                                            //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN CON-ADDRESS-LINE2 > ' '
            if (condition(pdaScia8200.getCon_Part_Rec_Con_Address_Line2().greater(" ")))
            {
                decideConditionsMet1180++;
                ldaPstl0185.getPstl0185_Data_Institution_Address().getValue(2).setValue(pdaScia8200.getCon_Part_Rec_Con_Address_Line2());                                 //Natural: MOVE CON-ADDRESS-LINE2 TO INSTITUTION-ADDRESS ( 2 )
                pnd_Work_Pnd_Addr2_Fnd.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #ADDR2-FND
            }                                                                                                                                                             //Natural: WHEN CON-ADDRESS-LINE3 > ' '
            if (condition(pdaScia8200.getCon_Part_Rec_Con_Address_Line3().greater(" ")))
            {
                decideConditionsMet1180++;
                ldaPstl0185.getPstl0185_Data_Institution_Address().getValue(3).setValue(pdaScia8200.getCon_Part_Rec_Con_Address_Line3());                                 //Natural: MOVE CON-ADDRESS-LINE3 TO INSTITUTION-ADDRESS ( 3 )
                pnd_Work_Pnd_Addr3_Fnd.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #ADDR3-FND
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet1180 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  DETERMINE WHERE TO PLACE ADDRESS LINES
            //*  --------------------------------------
            //*  --------------------------------------
            //*   NBC 8/28/2006
            //*  #ADDR2-FND := FALSE                            /* 031910
            //*  #ADDR3-FND := FALSE                            /* 031910
            short decideConditionsMet1197 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN NOT #ADDR2-FND AND NOT #ADDR3-FND
            if (condition(! ((pnd_Work_Pnd_Addr2_Fnd.getBoolean()) && ! (pnd_Work_Pnd_Addr3_Fnd.getBoolean()))))
            {
                decideConditionsMet1197++;
                ldaPstl0185.getPstl0185_Data_Institution_Address().getValue(2).setValue(pnd_Work_Pnd_Wk_City_State_Zip9);                                                 //Natural: MOVE #WK-CITY-STATE-ZIP9 TO INSTITUTION-ADDRESS ( 2 )
                ldaPstl0185.getPstl0185_Data_Institution_Address().getValue(3).setValue(" ");                                                                             //Natural: MOVE ' ' TO INSTITUTION-ADDRESS ( 3 )
                ldaPstl0185.getPstl0185_Data_Institution_Address().getValue(4).setValue(" ");                                                                             //Natural: MOVE ' ' TO INSTITUTION-ADDRESS ( 4 )
            }                                                                                                                                                             //Natural: WHEN #ADDR2-FND AND NOT #ADDR3-FND
            else if (condition(pnd_Work_Pnd_Addr2_Fnd.getBoolean() && ! (pnd_Work_Pnd_Addr3_Fnd.getBoolean())))
            {
                decideConditionsMet1197++;
                ldaPstl0185.getPstl0185_Data_Institution_Address().getValue(3).setValue(pnd_Work_Pnd_Wk_City_State_Zip9);                                                 //Natural: MOVE #WK-CITY-STATE-ZIP9 TO INSTITUTION-ADDRESS ( 3 )
                //*      MOVE CON-ADDRESS-LINE2 TO INSTITUTION-ADDRESS(2) /* 031910
                ldaPstl0185.getPstl0185_Data_Institution_Address().getValue(4).setValue(" ");                                                                             //Natural: MOVE ' ' TO INSTITUTION-ADDRESS ( 4 )
            }                                                                                                                                                             //Natural: WHEN #ADDR2-FND AND #ADDR3-FND
            else if (condition(pnd_Work_Pnd_Addr2_Fnd.getBoolean() && pnd_Work_Pnd_Addr3_Fnd.getBoolean()))
            {
                decideConditionsMet1197++;
                ldaPstl0185.getPstl0185_Data_Institution_Address().getValue(4).setValue(pnd_Work_Pnd_Wk_City_State_Zip9);                                                 //Natural: MOVE #WK-CITY-STATE-ZIP9 TO INSTITUTION-ADDRESS ( 4 )
            }                                                                                                                                                             //Natural: WHEN NOT #ADDR2-FND AND #ADDR3-FND
            else if (condition(! ((pnd_Work_Pnd_Addr2_Fnd.getBoolean()) && pnd_Work_Pnd_Addr3_Fnd.getBoolean())))
            {
                decideConditionsMet1197++;
                ldaPstl0185.getPstl0185_Data_Institution_Address().getValue(2).setValue(pdaScia8200.getCon_Part_Rec_Con_Address_Line3());                                 //Natural: MOVE CON-ADDRESS-LINE3 TO INSTITUTION-ADDRESS ( 2 )
                ldaPstl0185.getPstl0185_Data_Institution_Address().getValue(3).setValue(pnd_Work_Pnd_Wk_City_State_Zip9);                                                 //Natural: MOVE #WK-CITY-STATE-ZIP9 TO INSTITUTION-ADDRESS ( 3 )
                ldaPstl0185.getPstl0185_Data_Institution_Address().getValue(4).setValue(" ");                                                                             //Natural: MOVE ' ' TO INSTITUTION-ADDRESS ( 4 )
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                getReports().write(0, "COULD NOT DETERMINE WHERE TO PUT ADDRESS LINE");                                                                                   //Natural: WRITE 'COULD NOT DETERMINE WHERE TO PUT ADDRESS LINE'
                if (Global.isEscape()) return;
                //*         #AB-PLAN
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  MOVE CON-ADDRESS-LINE1 TO INSTITUTION-ADDRESS(1)     /* 031910
            ldaPstl0185.getPstl0185_Data_Institution_Address().getValue(5).setValue(" ");                                                                                 //Natural: MOVE ' ' TO INSTITUTION-ADDRESS ( 5 )
            ldaPstl0185.getPstl0185_Data_Institution_Address().getValue(6).setValue(" ");                                                                                 //Natural: MOVE ' ' TO INSTITUTION-ADDRESS ( 6 )
            ldaPstl0185.getPstl0185_Data_Institution_Address().getValue(1).setValue(pdaScia8200.getCon_Part_Rec_Con_Address_Line1());                                     //Natural: ASSIGN INSTITUTION-ADDRESS ( 1 ) := CON-ADDRESS-LINE1
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Do_Not_Send_Letter.setValue(false);                                                                                                                           //Natural: MOVE FALSE TO #DO-NOT-SEND-LETTER
    }
    private void sub_Post_Error() throws Exception                                                                                                                        //Natural: POST-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(0, "Error calling POST:");                                                                                                                     //Natural: WRITE 'Error calling POST:'
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"=",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code(),NEWLINE,"=",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                       //Natural: WRITE / '=' MSG-INFO-SUB.##RETURN-CODE / '=' ##MSG
        if (Global.isEscape()) return;
        getReports().write(0, "Backout executed, record not updated");                                                                                                    //Natural: WRITE 'Backout executed, record not updated'
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
    }
    private void sub_Psg_Error() throws Exception                                                                                                                         //Natural: PSG-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(0, "Error calling PSG9061");                                                                                                                   //Natural: WRITE 'Error calling PSG9061'
        if (Global.isEscape()) return;
        getReports().write(0, "*CALL ERROR (PSG9061):",pdaScia8200.getCon_Part_Rec_Con_Return_Code(),pdaScia8200.getCon_Part_Rec_Con_Contact_Err_Msg());                  //Natural: WRITE '*CALL ERROR (PSG9061):' CON-RETURN-CODE CON-CONTACT-ERR-MSG
        if (Global.isEscape()) return;
        getReports().write(0, "PLAN:",pdaScia8200.getCon_Part_Rec_Con_Plan_Num());                                                                                        //Natural: WRITE 'PLAN:' CON-PLAN-NUM
        if (Global.isEscape()) return;
        getReports().write(0, "PIN",ads_Prtcpnt_View_Adp_Unique_Id);                                                                                                      //Natural: WRITE 'PIN' ADP-UNIQUE-ID
        if (Global.isEscape()) return;
        getReports().write(0, "Backout executed, record not updated");                                                                                                    //Natural: WRITE 'Backout executed, record not updated'
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM CALL-POST-FOR-CLOSE
        sub_Call_Post_For_Close();
        if (condition(Global.isEscape())) {return;}
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
    }
    private void sub_Get_Supervisor_Racf_Id() throws Exception                                                                                                            //Natural: GET-SUPERVISOR-RACF-ID
    {
        if (BLNatReinput.isReinput()) return;

        vw_cwf_Org_Unit_Tbl_View.startDatabaseFind                                                                                                                        //Natural: FIND CWF-ORG-UNIT-TBL-VIEW WITH UNIQ-UNIT-CDE = ADP-BPS-UNIT
        (
        "FIND01",
        new Wc[] { new Wc("UNIQ_UNIT_CDE", "=", ads_Prtcpnt_View_Adp_Bps_Unit, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_cwf_Org_Unit_Tbl_View.readNextRow("FIND01", true)))
        {
            vw_cwf_Org_Unit_Tbl_View.setIfNotFoundControlFlag(false);
            if (condition(vw_cwf_Org_Unit_Tbl_View.getAstCOUNTER().equals(0)))                                                                                            //Natural: IF NO RECORD FOUND
            {
                cwf_Org_Unit_Tbl_View_Unit_Sprvsr_Racf_Id.setValue(Global.getINIT_USER());                                                                                //Natural: MOVE *INIT-USER TO UNIT-SPRVSR-RACF-ID
                getReports().write(0, "No Unit found for ",ads_Prtcpnt_View_Adp_Bps_Unit);                                                                                //Natural: WRITE 'No Unit found for ' ADP-BPS-UNIT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(cwf_Org_Unit_Tbl_View_Unit_Sprvsr_Racf_Id.equals(" ")))                                                                                         //Natural: IF UNIT-SPRVSR-RACF-ID = ' '
            {
                getReports().write(0, "Blank supervisor for",ads_Prtcpnt_View_Adp_Bps_Unit);                                                                              //Natural: WRITE 'Blank supervisor for' ADP-BPS-UNIT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Date() throws Exception                                                                                                                          //Natural: GET-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //*  MODIFY AS PER ED MELNIK EMAIL
        vw_ads_Cntl_View.startDatabaseRead                                                                                                                                //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM #ADS-CNTL-SUPER-DE-1
        (
        "READ01",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", pnd_Ads_Cntl_Super_De_1, WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        READ01:
        while (condition(vw_ads_Cntl_View.readNextRow("READ01")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                                     //Natural: IF ADS-RPT-13 NE 0
        {
            pnd_Cntl_Bsnss_Prev_Dte.setValue(ads_Cntl_View_Ads_Rpt_13);                                                                                                   //Natural: MOVE ADS-RPT-13 TO #CNTL-BSNSS-PREV-DTE
            getReports().write(0, "End-of-Month Business date used=",pnd_Cntl_Bsnss_Prev_Dte);                                                                            //Natural: WRITE 'End-of-Month Business date used=' #CNTL-BSNSS-PREV-DTE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Cntl_Bsnss_Prev_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A);                                                    //Natural: MOVE EDITED ADS-CNTL-BSNSS-DTE-A TO #CNTL-BSNSS-PREV-DTE ( EM = YYYYMMDD )
            getReports().write(0, "Regular Business date used=",pnd_Cntl_Bsnss_Prev_Dte);                                                                                 //Natural: WRITE 'Regular Business date used=' #CNTL-BSNSS-PREV-DTE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Call_Post_For_Open() throws Exception                                                                                                                //Natural: CALL-POST-FOR-OPEN
    {
        if (BLNatReinput.isReinput()) return;

        //*  RESET PSTA9611                                  /* EM - 083016 START
        pdaPsta9612.getPsta9612().reset();                                                                                                                                //Natural: RESET PSTA9612
        pdaPsta9612.getPsta9612_Systm_Id_Cde().setValue("ADS ");                                                                                                          //Natural: MOVE 'ADS ' TO PSTA9612.SYSTM-ID-CDE
        pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PTRKZNOT");                                                                                                         //Natural: MOVE 'PTRKZNOT' TO PSTA9612.PCKGE-CDE
        pdaPsta9612.getPsta9612_Rqst_Origin_Unit_Cde().setValue(" ");                                                                                                     //Natural: MOVE ' ' TO PSTA9612.RQST-ORIGIN-UNIT-CDE
        pdaPsta9612.getPsta9612_Form_Lbrry_Id().setValue(" ");                                                                                                            //Natural: MOVE ' ' TO PSTA9612.FORM-LBRRY-ID
        //* *VE 'F'                     TO PSTA9612.PRINT-FCLTY-CDE /* 120501
        //*  MOVE #PRINTER-ID             TO PSTA9611.PRNTR-ID-CDE
        pdaPsta9612.getPsta9612_Prntr_Id_Cde().setValue(" ");                                                                                                             //Natural: MOVE ' ' TO PSTA9612.PRNTR-ID-CDE
        pdaPsta9612.getPsta9612_Wpid_Vldte_Ind().getValue(1).setValue("Y");                                                                                               //Natural: MOVE 'Y' TO PSTA9612.WPID-VLDTE-IND ( 1 )
        //*  MOVE ADP-UNIQUE-ID           TO PSTA9611.PIN-NBR
        pdaPsta9612.getPsta9612_Univ_Id().setValue(ads_Prtcpnt_View_Adp_Unique_Id);                                                                                       //Natural: MOVE ADP-UNIQUE-ID TO PSTA9612.UNIV-ID
        pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue("P");                                                                                                              //Natural: MOVE 'P' TO PSTA9612.UNIV-ID-TYP
        pdaPsta9612.getPsta9612_Letter_Dte().setValue(Global.getDATX());                                                                                                  //Natural: MOVE *DATX TO PSTA9612.LETTER-DTE
        pdaPsta9612.getPsta9612_Tiaa_Cntrct_Nbr().setValue(ads_Prtcpnt_View_Adp_Cntrcts_In_Rqst.getValue(1));                                                             //Natural: MOVE ADP-CNTRCTS-IN-RQST ( 1 ) TO PSTA9612.TIAA-CNTRCT-NBR
        pdaPsta9612.getPsta9612_Pckge_Dlvry_Typ().setValue("   ");                                                                                                        //Natural: MOVE '   ' TO PSTA9612.PCKGE-DLVRY-TYP
        pdaPsta9612.getPsta9612_Rqst_Log_Dte_Tme().getValue(1).setValue(ads_Prtcpnt_View_Adp_Mit_Log_Dte_Tme);                                                            //Natural: MOVE ADP-MIT-LOG-DTE-TME TO PSTA9612.RQST-LOG-DTE-TME ( 1 )
        pdaPsta9612.getPsta9612_Status_Cde().getValue(1).setValue("    ");                                                                                                //Natural: MOVE '    ' TO PSTA9612.STATUS-CDE ( 1 )
        pdaPsta9612.getPsta9612_Effctve_Dte().getValue(1).setValue(ads_Prtcpnt_View_Adp_Effctv_Dte);                                                                      //Natural: MOVE ADP-EFFCTV-DTE TO PSTA9612.EFFCTVE-DTE ( 1 )
        pdaPsta9612.getPsta9612_No_Updte_To_Mit_Ind().setValue(true);                                                                                                     //Natural: MOVE TRUE TO NO-UPDTE-TO-MIT-IND
        pdaPsta9612.getPsta9612_No_Updte_To_Efm_Ind().setValue(true);                                                                                                     //Natural: MOVE TRUE TO NO-UPDTE-TO-EFM-IND
        pdaPsta9612.getPsta9612_Empl_Sgntry_Unit_Cde().setValue(ads_Prtcpnt_View_Adp_Bps_Unit);                                                                           //Natural: MOVE ADP-BPS-UNIT TO PSTA9612.EMPL-SGNTRY-UNIT-CDE
        pdaPsta9612.getPsta9612_Empl_Sgntry_Oprtr_Cde().setValue(cwf_Org_Unit_Tbl_View_Unit_Sprvsr_Racf_Id);                                                              //Natural: MOVE UNIT-SPRVSR-RACF-ID TO PSTA9612.EMPL-SGNTRY-OPRTR-CDE
        //*  WRITE '=' PSTA9611.EMPL-SGNTRY-UNIT-CDE
        //*  WRITE '=' PSTA9611.EMPL-SGNTRY-OPRTR-CDE
        pdaPsta9612.getPsta9612_Full_Nme().setValue(pnd_Annt_Name);                                                                                                       //Natural: MOVE #ANNT-NAME TO FULL-NME
        pdaPsta9610.getPsta9610_Pst_Rqst_Id().reset();                                                                                                                    //Natural: RESET PSTA9610.PST-RQST-ID
        //* *IF ADP-ADDR-CHG-RPT-IND NOT = ' '
        pdaPsta9612.getPsta9612_Addrss_Line_1().setValue(ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt.getValue(1));                                                      //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( 1 ) TO PSTA9612.ADDRSS-LINE-1
        pdaPsta9612.getPsta9612_Addrss_Line_2().setValue(ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt.getValue(2));                                                      //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( 2 ) TO PSTA9612.ADDRSS-LINE-2
        pdaPsta9612.getPsta9612_Addrss_Line_3().setValue(ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt.getValue(3));                                                      //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( 3 ) TO PSTA9612.ADDRSS-LINE-3
        pdaPsta9612.getPsta9612_Addrss_Line_4().setValue(ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt.getValue(4));                                                      //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( 4 ) TO PSTA9612.ADDRSS-LINE-4
        pdaPsta9612.getPsta9612_Addrss_Line_5().setValue(ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt.getValue(5));                                                      //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( 5 ) TO PSTA9612.ADDRSS-LINE-5
        pdaPsta9612.getPsta9612_Addrss_Zp9_Nbr().setValue(ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Zip);                                                                 //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-CRRSPNDNCE-PERM-ADDR-ZIP TO PSTA9612.ADDRSS-ZP9-NBR
        //* *END-IF
        //*  CALLNAT 'PSTN9610'
        //*   PSTA9610
        //*   PSTA9611
        //*   MSG-INFO-SUB
        //*   PARM-EMPLOYEE-INFO-SUB
        //*   PARM-UNIT-INFO-SUB
        //*  EM - 083016 END
        DbsUtil.callnat(Pstn9612.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9612' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
    }
    private void sub_Call_Post_For_Write() throws Exception                                                                                                               //Natural: CALL-POST-FOR-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        ldaPstl0185.getPstl0185_Data_Pstl0185_Data_Array().getValue("*").reset();                                                                                         //Natural: RESET PSTL0185-DATA-ARRAY ( * )
                                                                                                                                                                          //Natural: PERFORM GET-INSTITUTION-ADDRESS
        sub_Get_Institution_Address();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Psg_Error.getBoolean()))                                                                                                                        //Natural: IF #PSG-ERROR
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Date_A.setValueEdited(ads_Prtcpnt_View_Adp_Annty_Strt_Dte,new ReportEditMask("YYYYMMDD"));                                                                    //Natural: MOVE EDITED ADP-ANNTY-STRT-DTE ( EM = YYYYMMDD ) TO #DATE-A
        ldaPstl0185.getPstl0185_Data_Annuity_Start_Date().setValue(pnd_Date_A_Pnd_Date_Yyyymmdd);                                                                         //Natural: MOVE #DATE-YYYYMMDD TO ANNUITY-START-DATE
        ldaPstl0185.getPstl0185_Data_Pstl0185_Rec_Id().setValue("a");                                                                                                     //Natural: MOVE 'a' TO PSTL0185-REC-ID
        pdaPsta9500.getPsta9500_Data_Typ().setValue("M");                                                                                                                 //Natural: MOVE 'M' TO DATA-TYP
        pnd_Appl_Key_Counter.setValue(10000);                                                                                                                             //Natural: MOVE 10000 TO #APPL-KEY-COUNTER
        pdaPsta9500.getPsta9500_Mail_Id().setValue(pdaPsta9610.getPsta9610_Pst_Rqst_Id());                                                                                //Natural: MOVE PSTA9610.PST-RQST-ID TO MAIL-ID
        //*  111109
                                                                                                                                                                          //Natural: PERFORM CHECK-PLAN-ADMIN
        sub_Check_Plan_Admin();
        if (condition(Global.isEscape())) {return;}
        //*  DON't put admin name                 /* 111109
        //*  111109
        if (condition(pnd_None.getBoolean()))                                                                                                                             //Natural: IF #NONE
        {
            ldaPstl0185.getPstl0185_Data_Plan_Administrator().setValue("NONE");                                                                                           //Natural: ASSIGN PLAN-ADMINISTRATOR := 'NONE'
            //*  111109
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaPstl0185.getPstl0185_Data_Plan_Administrator().setValue(pnd_Work_Pnd_Wk_Compressed_Nme);                                                                   //Natural: MOVE #WK-COMPRESSED-NME TO PLAN-ADMINISTRATOR
            //*  111109
        }                                                                                                                                                                 //Natural: END-IF
        ldaPstl0185.getPstl0185_Data_Institution_Name().setValue(pdaScia8200.getCon_Part_Rec_Con_Company_Name());                                                         //Natural: MOVE CON-COMPANY-NAME TO INSTITUTION-NAME
        pdaPsta9500.getPsta9500_Application_Key().reset();                                                                                                                //Natural: RESET PSTA9500.APPLICATION-KEY
        //*  CHANGE BACK TO SORT-KEY AFTER TEST
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY2 PSTL0185-DATA
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            sort_Key2, ldaPstl0185.getPstl0185_Data());
        if (condition(Global.isEscape())) return;
    }
    private void sub_Call_Post_For_Close() throws Exception                                                                                                               //Natural: CALL-POST-FOR-CLOSE
    {
        if (BLNatReinput.isReinput()) return;

        //*  CALLNAT 'PSTN9680'                              /* EM - 083016 START
        //*    PSTA9610
        //*   PSTA9611
        //*   MSG-INFO-SUB
        //*   PARM-EMPLOYEE-INFO-SUB
        //*   PARM-UNIT-INFO-SUB
        //*  EM - 083016 - END
        DbsUtil.callnat(Pstn9685.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9685' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
    }
    private void sub_Call_Post_For_Power_Image() throws Exception                                                                                                         //Natural: CALL-POST-FOR-POWER-IMAGE
    {
        if (BLNatReinput.isReinput()) return;

        //*  PIN EXPANSION 02222017
        ldaPstl0185.getPstl0185_Data_Pstl0185_Data_Array().getValue("*").reset();                                                                                         //Natural: RESET PSTL0185-DATA-ARRAY ( * )
        ldaPstl0185.getPstl0185_Data_Pstl0185_Rec_Id().setValue("PI");                                                                                                    //Natural: ASSIGN PSTL0185-REC-ID := 'PI'
        ldaPstl0185.getPstl0185_Data_Pi_Task_Type().setValue("SETPAY");                                                                                                   //Natural: ASSIGN PI-TASK-TYPE := 'SETPAY'
        ldaPstl0185.getPstl0185_Data_Pi_Pin_Npin_Ppg().setValue(ads_Prtcpnt_View_Rqst_Id.getSubstring(1,12));                                                             //Natural: ASSIGN PI-PIN-NPIN-PPG := SUBSTR ( RQST-ID,1,12 )
        ldaPstl0185.getPstl0185_Data_Pi_Pin_Type().setValue("P");                                                                                                         //Natural: ASSIGN PI-PIN-TYPE := 'P'
        ldaPstl0185.getPstl0185_Data_Pi_Action_Step().setValue("None");                                                                                                   //Natural: ASSIGN PI-ACTION-STEP := 'None'
        ldaPstl0185.getPstl0185_Data_Pi_Doc_Content().setValue("POST LETTER");                                                                                            //Natural: ASSIGN PI-DOC-CONTENT := 'POST LETTER'
        if (condition(ads_Prtcpnt_View_Adp_Pi_Task_Id.equals(" ")))                                                                                                       //Natural: IF ADP-PI-TASK-ID = ' '
        {
            ldaPstl0185.getPstl0185_Data_Pi_Export_Ind().setValue("T");                                                                                                   //Natural: ASSIGN PI-EXPORT-IND := 'T'
            ldaPstl0185.getPstl0185_Data_Pi_Task_Status().setValue("C");                                                                                                  //Natural: ASSIGN PI-TASK-STATUS := 'C'
            //*  FOUND AN EXISTING TASK
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaPstl0185.getPstl0185_Data_Pi_Task_Id().setValue(ads_Prtcpnt_View_Adp_Pi_Task_Id);                                                                          //Natural: MOVE ADP-PI-TASK-ID TO PI-TASK-ID
            ldaPstl0185.getPstl0185_Data_Pi_Export_Ind().setValue("I");                                                                                                   //Natural: ASSIGN PI-EXPORT-IND := 'I'
        }                                                                                                                                                                 //Natural: END-IF
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        //*  SORT-KEY-STRUCT
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY PSTL0185-DATA
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl0185.getSort_Key(), ldaPstl0185.getPstl0185_Data());
        if (condition(Global.isEscape())) return;
    }
    private void sub_Check_For_Exception() throws Exception                                                                                                               //Natural: CHECK-FOR-EXCEPTION
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                         //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ077");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ077'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("ADS");                                                                                                          //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'ADS'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id.setValue(ads_Prtcpnt_View_Adp_Plan_Nbr);                                                                                  //Natural: ASSIGN #NAZ-TABLE-LVL3-ID := ADP-PLAN-NBR
        vw_naz_Table_Ddm.startDatabaseFind                                                                                                                                //Natural: FIND NAZ-TABLE-DDM WITH NAZ-TBL-SUPER3 = #NAZ-TABLE-KEY
        (
        "FIND02",
        new Wc[] { new Wc("NAZ_TBL_SUPER3", "=", pnd_Naz_Table_Key, WcType.WITH) }
        );
        FIND02:
        while (condition(vw_naz_Table_Ddm.readNextRow("FIND02")))
        {
            vw_naz_Table_Ddm.setIfNotFoundControlFlag(false);
            if (condition(!(naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.equals("Y"))))                                                                                            //Natural: ACCEPT IF NAZ-TBL-RCRD-ACTV-IND = 'Y'
            {
                continue;
            }
            pnd_Exception.setValue(true);                                                                                                                                 //Natural: ASSIGN #EXCEPTION := TRUE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  060412 END
    }
    private void sub_Check_Plan() throws Exception                                                                                                                        //Natural: CHECK-PLAN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Work_Pnd_Dont_Print.reset();                                                                                                                                  //Natural: RESET #DONT-PRINT
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                         //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ059");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ059'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("ADS");                                                                                                          //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'ADS'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id.setValue(ads_Prtcpnt_View_Adp_Plan_Nbr);                                                                                  //Natural: ASSIGN #NAZ-TABLE-LVL3-ID := ADP-PLAN-NBR
        vw_naz_Table_Ddm.startDatabaseFind                                                                                                                                //Natural: FIND NAZ-TABLE-DDM WITH NAZ-TBL-SUPER3 = #NAZ-TABLE-KEY
        (
        "FIND03",
        new Wc[] { new Wc("NAZ_TBL_SUPER3", "=", pnd_Naz_Table_Key, WcType.WITH) }
        );
        FIND03:
        while (condition(vw_naz_Table_Ddm.readNextRow("FIND03")))
        {
            vw_naz_Table_Ddm.setIfNotFoundControlFlag(false);
            if (condition(!(naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.equals("Y"))))                                                                                            //Natural: ACCEPT IF NAZ-TBL-RCRD-ACTV-IND = 'Y'
            {
                continue;
            }
            pnd_Work_Pnd_Dont_Print.setValue(true);                                                                                                                       //Natural: ASSIGN #DONT-PRINT := TRUE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  012010 END
    }
    private void sub_Check_Plan_Admin() throws Exception                                                                                                                  //Natural: CHECK-PLAN-ADMIN
    {
        if (BLNatReinput.isReinput()) return;

        pnd_None.setValue(true);                                                                                                                                          //Natural: ASSIGN #NONE := TRUE
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                         //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ071");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ071'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("ADS");                                                                                                          //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'ADS'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id.setValue(ads_Prtcpnt_View_Adp_Plan_Nbr);                                                                                  //Natural: ASSIGN #NAZ-TABLE-LVL3-ID := ADP-PLAN-NBR
        vw_naz_Table_Ddm.startDatabaseFind                                                                                                                                //Natural: FIND NAZ-TABLE-DDM WITH NAZ-TBL-SUPER3 = #NAZ-TABLE-KEY
        (
        "FIND04",
        new Wc[] { new Wc("NAZ_TBL_SUPER3", "=", pnd_Naz_Table_Key, WcType.WITH) }
        );
        FIND04:
        while (condition(vw_naz_Table_Ddm.readNextRow("FIND04")))
        {
            vw_naz_Table_Ddm.setIfNotFoundControlFlag(false);
            pnd_None.setValue(false);                                                                                                                                     //Natural: ASSIGN #NONE := FALSE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  111109
    }
    private void sub_Error_Routine_For_Post() throws Exception                                                                                                            //Natural: ERROR-ROUTINE-FOR-POST
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        getReports().write(0, "E R R O R :",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code());                                                                           //Natural: WRITE 'E R R O R :' MSG-INFO-SUB.##RETURN-CODE
        if (Global.isEscape()) return;
        getReports().write(0, "REQUEST-ID:",ads_Prtcpnt_View_Rqst_Id);                                                                                                    //Natural: WRITE 'REQUEST-ID:' ADS-PRTCPNT-VIEW.RQST-ID
        if (Global.isEscape()) return;
        DbsUtil.callnat(Pstn9980.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub());                                                                        //Natural: CALLNAT 'PSTN9980' MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        getReports().write(0, pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                                 //Natural: WRITE MSG-INFO-SUB.##MSG
        if (Global.isEscape()) return;
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().reset();                                                                                                             //Natural: RESET ##MSG-NR ##MSG ##RETURN-CODE
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().reset();
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().reset();
    }
    private void sub_Terminate_Routine() throws Exception                                                                                                                 //Natural: TERMINATE-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
                                                                                                                                                                          //Natural: PERFORM ERROR-LOG
        sub_Error_Log();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, NEWLINE,NEWLINE,NEWLINE,NEWLINE," ================================================================= ",NEWLINE,"|*****************************************************************|", //Natural: WRITE /// /' ================================================================= ' /'|*****************************************************************|' /'|*****************************************************************|' /'|** TERMINATING ADAS BATCH PROCESS.....                         **|' /'|**                                                             **|' /'|**' #TERMINATE-MSG1 '**|' /'|**' #TERMINATE-MSG2 '**|' /'|**' #TERMINATE-MSG3 '**|' /'|**                                                             **|' /'|**  >>> IF NECESSARY, PLEASE CALL - ADAS SYSTEM SUPPORT <<<    **|' /'|*****************************************************************|' /'|*****************************************************************|' /' ================================================================= '
            NEWLINE,"|*****************************************************************|",NEWLINE,"|** TERMINATING ADAS BATCH PROCESS.....                         **|",
            NEWLINE,"|**                                                             **|",NEWLINE,"|**",pnd_Terminate_Pnd_Terminate_Msg1,"**|",NEWLINE,"|**",
            pnd_Terminate_Pnd_Terminate_Msg2,"**|",NEWLINE,"|**",pnd_Terminate_Pnd_Terminate_Msg3,"**|",NEWLINE,"|**                                                             **|",
            NEWLINE,"|**  >>> IF NECESSARY, PLEASE CALL - ADAS SYSTEM SUPPORT <<<    **|",NEWLINE,"|*****************************************************************|",
            NEWLINE,"|*****************************************************************|",NEWLINE," ================================================================= ");
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB-MESSAGE
        sub_End_Of_Job_Message();
        if (condition(Global.isEscape())) {return;}
        DbsUtil.terminate(pnd_Terminate_Pnd_Terminate_No);  if (true) return;                                                                                             //Natural: TERMINATE #TERMINATE-NO
    }
    private void sub_End_Of_Job_Message() throws Exception                                                                                                                //Natural: END-OF-JOB-MESSAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, NEWLINE,NEWLINE,NEWLINE,new TabSetting(35),"TOTAL PRTKZNOT Letters written=    : ",pnd_Letters_Written,NEWLINE,new TabSetting(35),          //Natural: WRITE ( 1 ) ///35T 'TOTAL PRTKZNOT Letters written=    : ' #LETTERS-WRITTEN / 35T 'TOTAL ERRORS : (No letter produced): ' #ERROR-REPORT-ENTRIES
            "TOTAL ERRORS : (No letter produced): ",pnd_Error_Report_Entries);
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,NEWLINE,NEWLINE,NEWLINE," ================================================================= ",NEWLINE,"|**                                                             **|", //Natural: WRITE /// /' ================================================================= ' /'|**                                                             **|' /'|**            ANNUITIZATION OF OMNI PLUS CONTRACTS             **|' /'|**                                                             **|' /'|**   ADAS - Notification LETTERS PROCESS ---- JOB RUN ENDED    **|' /'|**                                                             **|' /'|** TOTAL PRTKZNOT Letters written=    : ' #LETTERS-WRITTEN '             **|' /'|**                                      RQST                   **|' /'|** NUMBER OF ERRORS on PSG call       : ' #PSG-ERRORS /' =================================================================|'
            NEWLINE,"|**            ANNUITIZATION OF OMNI PLUS CONTRACTS             **|",NEWLINE,"|**                                                             **|",
            NEWLINE,"|**   ADAS - Notification LETTERS PROCESS ---- JOB RUN ENDED    **|",NEWLINE,"|**                                                             **|",
            NEWLINE,"|** TOTAL PRTKZNOT Letters written=    : ",pnd_Letters_Written,"             **|",NEWLINE,"|**                                      RQST                   **|",
            NEWLINE,"|** NUMBER OF ERRORS on PSG call       : ",pnd_Psg_Errors,NEWLINE," =================================================================|");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Log() throws Exception                                                                                                                         //Natural: ERROR-LOG
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------- *
        pnd_Adsn998_Error_Handler_Pnd_Appl_Id.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "BATCH", Global.getINIT_ID()));                                    //Natural: COMPRESS 'BATCH' *INIT-ID INTO #APPL-ID LEAVING NO SPACE
        pnd_Adsn998_Error_Handler_Pnd_Calling_Program.setValue(Global.getPROGRAM());                                                                                      //Natural: ASSIGN #CALLING-PROGRAM := *PROGRAM
        pnd_Adsn998_Error_Handler_Pnd_Unique_Id.setValue(ads_Prtcpnt_View_Adp_Unique_Id);                                                                                 //Natural: ASSIGN #UNIQUE-ID := ADP-UNIQUE-ID
        DbsUtil.callnat(Adsn998.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub(), pnd_Adsn998_Error_Handler);                                              //Natural: CALLNAT 'ADSN998' MSG-INFO-SUB #ADSN998-ERROR-HANDLER
        if (condition(Global.isEscape())) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, new TabSetting(1),Global.getPROGRAM(),new TabSetting(50),"ANNUITIZATION OF OMNI PLUS CONTRACTS",new TabSetting(110),"Run Date:",  //Natural: WRITE ( 1 ) 001T *PROGRAM 050T 'ANNUITIZATION OF OMNI PLUS CONTRACTS' 110T 'Run Date:' ( BLI ) 120T *DATX ( AD = OD CD = NE ) / 050T 'NOTIFICATION LETTERS RUN REPORT' 110T 'Page No :' 120T *PAGE-NUMBER ( AD = OD CD = NE ZP = ON ) // 005T 'PIN' 17T 'EFFECTIVE' 29T 'STATUS' 048T 'ERROR MESSAGES' / 19T 'DATE' 30T 'CODE' //
                        Color.blue, new FieldAttributes ("AD=I"),new TabSetting(120),Global.getDATX(), new FieldAttributes ("AD=OD"), Color.white,NEWLINE,new 
                        TabSetting(50),"NOTIFICATION LETTERS RUN REPORT",new TabSetting(110),"Page No :",new TabSetting(120),getReports().getPageNumberDbs(0), 
                        new FieldAttributes ("AD=OD"), Color.white, new ReportZeroPrint (true),NEWLINE,NEWLINE,new TabSetting(5),"PIN",new TabSetting(17),"EFFECTIVE",new 
                        TabSetting(29),"STATUS",new TabSetting(48),"ERROR MESSAGES",NEWLINE,new TabSetting(19),"DATE",new TabSetting(30),"CODE",NEWLINE,
                        NEWLINE);
                    //*  WRITE (1) TITLE LEFT JUSTIFIED
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pnd_Adsn998_Error_Handler.reset();                                                                                                                                //Natural: RESET #ADSN998-ERROR-HANDLER
        pnd_Terminate_Pnd_Terminate_Msg.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "INTERNAL ERROR IN PROGRAM :", Global.getPROGRAM(), " ERR NO : ",        //Natural: COMPRESS 'INTERNAL ERROR IN PROGRAM :' *PROGRAM ' ERR NO : ' *ERROR-NR ' ERR LINE :' *ERROR-LINE TO #TERMINATE-MSG LEAVING NO SPACE
            Global.getERROR_NR(), " ERR LINE :", Global.getERROR_LINE()));
        pnd_Terminate_Pnd_Terminate_No.setValue(99);                                                                                                                      //Natural: MOVE 99 TO #TERMINATE-NO
        pnd_Adsn998_Error_Handler_Pnd_Error_Msg.setValue(pnd_Terminate_Pnd_Terminate_Msg);                                                                                //Natural: MOVE #TERMINATE-MSG TO #ERROR-MSG
                                                                                                                                                                          //Natural: PERFORM TERMINATE-ROUTINE
        sub_Terminate_Routine();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60");
    }
}
