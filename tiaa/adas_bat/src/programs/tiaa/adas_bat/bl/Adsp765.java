/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:04:56 PM
**        * FROM NATURAL PROGRAM : Adsp765
************************************************************
**        * FILE NAME            : Adsp765.java
**        * CLASS NAME           : Adsp765
**        * INSTANCE NAME        : Adsp765
************************************************************
************************************************************************
* PROGRAM  : ADSP765
* GENERATED: APRIL 13, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : THIS MODULE REPORTS  REAL  ESTATE REQUESTS WHICH WERE
*            PROCESSED BY ADAS
*
* REMARKS  : CLONED FROM NAZP765 (ADAM SYSTEM)
************************************************************************
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
*  1/29      CSHNEIDER   RESTOWED PGM
* 02/05/98   ZAFAR KHAN  REVALUATION METHOD HEADING FIXED
* 05/22/98   ZAFAR KHAN  ROLLOVER CERTIFICATE NUMBER AND TOTAL ROLLOVER
*                        AMOUNT ADDED IN PROGRAM AS WELL AS MAP NAZM761
* 01/27/99   ZAFAR KHAN  NAP-BPS-UNIT EXTENDED FROM 6 TO 8 BYTES
* 04/01      C SCHNEIDER RE-STOWED BECAUSE PRAP-KEY ADDED TO NAZLPRTC
* 11/01      C SCHNEIDER RESTOWED UNDER NATURAL 3.1.4
* ----------CURTIS 99 RATES 6/03
* 11/17/03   C SCHNEIDER RE-STOWED NAZLIARS  ALLOW FOR 80 TIAA RATES
* 04/12/04   C AVE       MODIFIED FOR ADAS (ANNUITIZATION SUNGUARD)
* 02/22/08   O SOTTO RESTOWED FOR TIAA RATE EXPANSION.
* 03/17/2008 E.MELNIK   ROTH 403B/401K CHANGES.  RECOMPILED FOR
*                       NEW ADSL401/ADSL401A/ADSL450.
* 05/06/2008 G.GUERRERO ADDED CODE TO CREATE ROTH ONLY REPORT.
* 12/01/2008 R.SACHARNY CHANGED REPORT HEADING TO INCLUDE "Access" RS0
* 7/19/2010  C. MASON   RESTOW DUE TO LDA CHANGES
* 03/06/12   E. MELNIK  RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                       UPDATED LINKAGE AREAS.
* 08/20/12   O. SOTTO   FIX TO NOT PRINT THE IVC LINE IF TIAA WAS
*                       SETTLED(INC1810146).  THIS WILL BE IMPLEMENTED
*                       AFTER RBE.  NEW MAPS WERE CREATED TO DISPLAY
*                       REA/ACCESS HEADINGS INSTEAD OF CREF.  THE FF
*                       MAPS WILL BE CHANGED:
*                       ADSM861 TO ADSM861A, ADSM871 TO ADSM871A,
*                       ADSM874 (COMMENTED-OUT), ADSM882 TO ADSM882A.
*                       SC 082012 TO SEE THE CHANGES.
* 01/18/13   E. MELNIK  TNG PROJECT.  RESTOW TO INCLUDE UPDATED LINKAGE
*                       AREAS.
* 10/22/13  E. MELNIK CREF/REA REDESIGN CHANGES.
*                     MARKED BY EM - 102213.
* 02/27/2017 R.CARREON RESTOWED FOR PIN EXPANSION
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp765 extends BLNatBase
{
    // Data Areas
    private GdaAdsg870 gdaAdsg870;
    private LdaAdsl770 ldaAdsl770;
    private LdaAdsl761 ldaAdsl761;
    private LdaAdsl401 ldaAdsl401;
    private LdaAdsl401a ldaAdsl401a;
    private LdaAdsl450 ldaAdsl450;
    private PdaAdspda_M pdaAdspda_M;
    private PdaNeca4000 pdaNeca4000;
    private PdaAdsa888 pdaAdsa888;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Type_Of_Call;
    private DbsField pnd_Code;
    private DbsField pnd_Desc;
    private DbsField pnd_Payment_Mode;

    private DbsGroup pnd_Payment_Mode__R_Field_1;
    private DbsField pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1;
    private DbsField pnd_Payment_Mode_Pnd_Payment_Mode_Byte_2;
    private DbsField pnd_Payment_Mode_All;

    private DbsGroup pnd_All_Indx;
    private DbsField pnd_All_Indx_Pnd_Z;
    private DbsField pnd_All_Indx_Pnd_A;
    private DbsField pnd_All_Indx_Pnd_Xx;
    private DbsField pnd_All_Indx_Pnd_A_Indx;
    private DbsField pnd_All_Indx_Pnd_B_Indx;
    private DbsField pnd_All_Indx_Pnd_C_Indx;
    private DbsField pnd_All_Indx_Pnd_X_Indx;
    private DbsField pnd_All_Indx_Pnd_Y_Indx;
    private DbsField pnd_All_Indx_Pnd_Next_Indx;
    private DbsField pnd_All_Indx_Pnd_Cref_Indx_1;
    private DbsField pnd_All_Indx_Pnd_Cref_Indx_2;
    private DbsField pnd_All_Indx_Pnd_Cref_Indx_3;
    private DbsField pnd_All_Indx_Pnd_Cref_Indx_4;
    private DbsField pnd_All_Indx_Pnd_Cref_Indx_5;
    private DbsField pnd_All_Indx_Pnd_Prod_Indx;
    private DbsField pnd_Total_Cref_Nbr;
    private DbsField pnd_New_Header_Page_3;
    private DbsField pnd_Cref_Record;
    private DbsField pnd_Real_Estate_Record;
    private DbsField pnd_Tiaa_Trans;
    private DbsField pnd_Cref_Trans;
    private DbsField pnd_Real_Trans;
    private DbsField pnd_First_Time;
    private DbsField pnd_New_Record;
    private DbsField pnd_Cref_Rec_Found;
    private DbsField pnd_Record_Written;
    private DbsField pnd_End_Of_Ia_File;
    private DbsField pnd_Tiaa_Settled;
    private DbsField pnd_Roth_Indicator_On;
    private DbsField pnd_Prev_Tiaa_Nbr;
    private DbsField pnd_Nac_Ppg_Code;
    private DbsField pnd_Nac_Ppg_Ind;
    private DbsField pnd_Rec_Type;

    private DbsGroup pnd_Rec_Type__R_Field_2;
    private DbsField pnd_Rec_Type_Pnd_Filler;
    private DbsField pnd_Rec_Type_Pnd_Z_Indx;
    private DbsField pnd_Tiaa_Nbr;
    private DbsField pnd_Tiaa_Ind;
    private DbsField pnd_Tiaa_Stlmnt_Amt;
    private DbsField pnd_Tiaa_Rtb_Stlmnt_Amt;
    private DbsField pnd_Cref_Nbr;
    private DbsField pnd_Cref_Ind;
    private DbsField pnd_Cref_Stlmnt_Amt;
    private DbsField pnd_Cref_Rtb_Stlmnt_Amt;
    private DbsField pnd_Real_Nbr;
    private DbsField pnd_Real_Ind;
    private DbsField pnd_Real_Stlmnt_Amt;
    private DbsField pnd_Real_Rtb_Stlmnt_Amt;
    private DbsField pnd_Tiaa_Rlvr_A;

    private DbsGroup pnd_Tiaa_Rlvr_A__R_Field_3;
    private DbsField pnd_Tiaa_Rlvr_A_Pnd_Tiaa_Rlvr;
    private DbsField pnd_Cref_Rlvr_A;

    private DbsGroup pnd_Cref_Rlvr_A__R_Field_4;
    private DbsField pnd_Cref_Rlvr_A_Pnd_Cref_Rlvr;
    private DbsField pnd_Real_Rlvr_A;

    private DbsGroup pnd_Real_Rlvr_A__R_Field_5;
    private DbsField pnd_Real_Rlvr_A_Pnd_Real_Rlvr;
    private DbsField pnd_Time;

    private DbsGroup pnd_Time__R_Field_6;
    private DbsField pnd_Time_Pnd_Time_Hh;
    private DbsField pnd_Time_Pnd_Time_Filler1;
    private DbsField pnd_Time_Pnd_Time_Mm;
    private DbsField pnd_Time_Pnd_Time_Filler2;
    private DbsField pnd_Time_Pnd_Time_Ss;
    private DbsField pnd_Heading;
    private DbsField pnd_Page_Number_Map;
    private DbsField pnd_Pec_Nbr_Active_Acct;
    private DbsField pnd_Asterisk;
    private DbsField pnd_Comment1;
    private DbsField pnd_Comment2;
    private DbsField pnd_Amount;
    private DbsField pnd_Work_Rqst;

    private DbsGroup pnd_Work_Rqst__R_Field_7;
    private DbsField pnd_Work_Rqst_Pnd_Work_Rqst_30;
    private DbsField pnd_Work_Rqst_Pnd_Work_Rqst_5;

    private DbsGroup pnd_Work_Rqst__R_Field_8;
    private DbsField pnd_Work_Rqst_Pnd_Work_Rqst_Pin7;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaAdsg870 = GdaAdsg870.getInstance(getCallnatLevel());
        registerRecord(gdaAdsg870);
        if (gdaOnly) return;

        ldaAdsl770 = new LdaAdsl770();
        registerRecord(ldaAdsl770);
        ldaAdsl761 = new LdaAdsl761();
        registerRecord(ldaAdsl761);
        ldaAdsl401 = new LdaAdsl401();
        registerRecord(ldaAdsl401);
        registerRecord(ldaAdsl401.getVw_ads_Prtcpnt_View());
        ldaAdsl401a = new LdaAdsl401a();
        registerRecord(ldaAdsl401a);
        registerRecord(ldaAdsl401a.getVw_ads_Cntrct_View());
        ldaAdsl450 = new LdaAdsl450();
        registerRecord(ldaAdsl450);
        registerRecord(ldaAdsl450.getVw_ads_Ia_Rslt_View());
        localVariables = new DbsRecord();
        pdaAdspda_M = new PdaAdspda_M(localVariables);
        pdaNeca4000 = new PdaNeca4000(localVariables);
        pdaAdsa888 = new PdaAdsa888(localVariables);

        // Local Variables
        pnd_Type_Of_Call = localVariables.newFieldInRecord("pnd_Type_Of_Call", "#TYPE-OF-CALL", FieldType.STRING, 1);
        pnd_Code = localVariables.newFieldInRecord("pnd_Code", "#CODE", FieldType.STRING, 2);
        pnd_Desc = localVariables.newFieldInRecord("pnd_Desc", "#DESC", FieldType.STRING, 20);
        pnd_Payment_Mode = localVariables.newFieldInRecord("pnd_Payment_Mode", "#PAYMENT-MODE", FieldType.NUMERIC, 3);

        pnd_Payment_Mode__R_Field_1 = localVariables.newGroupInRecord("pnd_Payment_Mode__R_Field_1", "REDEFINE", pnd_Payment_Mode);
        pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1 = pnd_Payment_Mode__R_Field_1.newFieldInGroup("pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1", "#PAYMENT-MODE-BYTE-1", 
            FieldType.NUMERIC, 1);
        pnd_Payment_Mode_Pnd_Payment_Mode_Byte_2 = pnd_Payment_Mode__R_Field_1.newFieldInGroup("pnd_Payment_Mode_Pnd_Payment_Mode_Byte_2", "#PAYMENT-MODE-BYTE-2", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Mode_All = localVariables.newFieldInRecord("pnd_Payment_Mode_All", "#PAYMENT-MODE-ALL", FieldType.STRING, 20);

        pnd_All_Indx = localVariables.newGroupInRecord("pnd_All_Indx", "#ALL-INDX");
        pnd_All_Indx_Pnd_Z = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Z", "#Z", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_A = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Xx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Xx", "#XX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_A_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_A_Indx", "#A-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_B_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_B_Indx", "#B-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_C_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_C_Indx", "#C-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_X_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_X_Indx", "#X-INDX", FieldType.PACKED_DECIMAL, 2);
        pnd_All_Indx_Pnd_Y_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Y_Indx", "#Y-INDX", FieldType.PACKED_DECIMAL, 2);
        pnd_All_Indx_Pnd_Next_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Next_Indx", "#NEXT-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Cref_Indx_1 = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Cref_Indx_1", "#CREF-INDX-1", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Cref_Indx_2 = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Cref_Indx_2", "#CREF-INDX-2", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Cref_Indx_3 = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Cref_Indx_3", "#CREF-INDX-3", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Cref_Indx_4 = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Cref_Indx_4", "#CREF-INDX-4", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Cref_Indx_5 = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Cref_Indx_5", "#CREF-INDX-5", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Prod_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Prod_Indx", "#PROD-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Total_Cref_Nbr = localVariables.newFieldInRecord("pnd_Total_Cref_Nbr", "#TOTAL-CREF-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_New_Header_Page_3 = localVariables.newFieldInRecord("pnd_New_Header_Page_3", "#NEW-HEADER-PAGE-3", FieldType.BOOLEAN, 1);
        pnd_Cref_Record = localVariables.newFieldInRecord("pnd_Cref_Record", "#CREF-RECORD", FieldType.BOOLEAN, 1);
        pnd_Real_Estate_Record = localVariables.newFieldInRecord("pnd_Real_Estate_Record", "#REAL-ESTATE-RECORD", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Trans = localVariables.newFieldInRecord("pnd_Tiaa_Trans", "#TIAA-TRANS", FieldType.BOOLEAN, 1);
        pnd_Cref_Trans = localVariables.newFieldInRecord("pnd_Cref_Trans", "#CREF-TRANS", FieldType.BOOLEAN, 1);
        pnd_Real_Trans = localVariables.newFieldInRecord("pnd_Real_Trans", "#REAL-TRANS", FieldType.BOOLEAN, 1);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_New_Record = localVariables.newFieldInRecord("pnd_New_Record", "#NEW-RECORD", FieldType.BOOLEAN, 1);
        pnd_Cref_Rec_Found = localVariables.newFieldInRecord("pnd_Cref_Rec_Found", "#CREF-REC-FOUND", FieldType.BOOLEAN, 1);
        pnd_Record_Written = localVariables.newFieldInRecord("pnd_Record_Written", "#RECORD-WRITTEN", FieldType.BOOLEAN, 1);
        pnd_End_Of_Ia_File = localVariables.newFieldInRecord("pnd_End_Of_Ia_File", "#END-OF-IA-FILE", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Settled = localVariables.newFieldInRecord("pnd_Tiaa_Settled", "#TIAA-SETTLED", FieldType.BOOLEAN, 1);
        pnd_Roth_Indicator_On = localVariables.newFieldInRecord("pnd_Roth_Indicator_On", "#ROTH-INDICATOR-ON", FieldType.BOOLEAN, 1);
        pnd_Prev_Tiaa_Nbr = localVariables.newFieldInRecord("pnd_Prev_Tiaa_Nbr", "#PREV-TIAA-NBR", FieldType.STRING, 10);
        pnd_Nac_Ppg_Code = localVariables.newFieldArrayInRecord("pnd_Nac_Ppg_Code", "#NAC-PPG-CODE", FieldType.STRING, 6, new DbsArrayController(1, 15));
        pnd_Nac_Ppg_Ind = localVariables.newFieldArrayInRecord("pnd_Nac_Ppg_Ind", "#NAC-PPG-IND", FieldType.STRING, 5, new DbsArrayController(1, 15));
        pnd_Rec_Type = localVariables.newFieldInRecord("pnd_Rec_Type", "#REC-TYPE", FieldType.NUMERIC, 3);

        pnd_Rec_Type__R_Field_2 = localVariables.newGroupInRecord("pnd_Rec_Type__R_Field_2", "REDEFINE", pnd_Rec_Type);
        pnd_Rec_Type_Pnd_Filler = pnd_Rec_Type__R_Field_2.newFieldInGroup("pnd_Rec_Type_Pnd_Filler", "#FILLER", FieldType.NUMERIC, 2);
        pnd_Rec_Type_Pnd_Z_Indx = pnd_Rec_Type__R_Field_2.newFieldInGroup("pnd_Rec_Type_Pnd_Z_Indx", "#Z-INDX", FieldType.NUMERIC, 1);
        pnd_Tiaa_Nbr = localVariables.newFieldArrayInRecord("pnd_Tiaa_Nbr", "#TIAA-NBR", FieldType.STRING, 10, new DbsArrayController(1, 12));
        pnd_Tiaa_Ind = localVariables.newFieldArrayInRecord("pnd_Tiaa_Ind", "#TIAA-IND", FieldType.STRING, 6, new DbsArrayController(1, 12));
        pnd_Tiaa_Stlmnt_Amt = localVariables.newFieldInRecord("pnd_Tiaa_Stlmnt_Amt", "#TIAA-STLMNT-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Tiaa_Rtb_Stlmnt_Amt = localVariables.newFieldInRecord("pnd_Tiaa_Rtb_Stlmnt_Amt", "#TIAA-RTB-STLMNT-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Cref_Nbr = localVariables.newFieldArrayInRecord("pnd_Cref_Nbr", "#CREF-NBR", FieldType.STRING, 10, new DbsArrayController(1, 12));
        pnd_Cref_Ind = localVariables.newFieldArrayInRecord("pnd_Cref_Ind", "#CREF-IND", FieldType.STRING, 6, new DbsArrayController(1, 12));
        pnd_Cref_Stlmnt_Amt = localVariables.newFieldInRecord("pnd_Cref_Stlmnt_Amt", "#CREF-STLMNT-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Cref_Rtb_Stlmnt_Amt = localVariables.newFieldInRecord("pnd_Cref_Rtb_Stlmnt_Amt", "#CREF-RTB-STLMNT-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Real_Nbr = localVariables.newFieldArrayInRecord("pnd_Real_Nbr", "#REAL-NBR", FieldType.STRING, 10, new DbsArrayController(1, 12));
        pnd_Real_Ind = localVariables.newFieldArrayInRecord("pnd_Real_Ind", "#REAL-IND", FieldType.STRING, 6, new DbsArrayController(1, 12));
        pnd_Real_Stlmnt_Amt = localVariables.newFieldInRecord("pnd_Real_Stlmnt_Amt", "#REAL-STLMNT-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Real_Rtb_Stlmnt_Amt = localVariables.newFieldInRecord("pnd_Real_Rtb_Stlmnt_Amt", "#REAL-RTB-STLMNT-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Tiaa_Rlvr_A = localVariables.newFieldInRecord("pnd_Tiaa_Rlvr_A", "#TIAA-RLVR-A", FieldType.STRING, 11);

        pnd_Tiaa_Rlvr_A__R_Field_3 = localVariables.newGroupInRecord("pnd_Tiaa_Rlvr_A__R_Field_3", "REDEFINE", pnd_Tiaa_Rlvr_A);
        pnd_Tiaa_Rlvr_A_Pnd_Tiaa_Rlvr = pnd_Tiaa_Rlvr_A__R_Field_3.newFieldInGroup("pnd_Tiaa_Rlvr_A_Pnd_Tiaa_Rlvr", "#TIAA-RLVR", FieldType.NUMERIC, 11, 
            2);
        pnd_Cref_Rlvr_A = localVariables.newFieldInRecord("pnd_Cref_Rlvr_A", "#CREF-RLVR-A", FieldType.STRING, 11);

        pnd_Cref_Rlvr_A__R_Field_4 = localVariables.newGroupInRecord("pnd_Cref_Rlvr_A__R_Field_4", "REDEFINE", pnd_Cref_Rlvr_A);
        pnd_Cref_Rlvr_A_Pnd_Cref_Rlvr = pnd_Cref_Rlvr_A__R_Field_4.newFieldInGroup("pnd_Cref_Rlvr_A_Pnd_Cref_Rlvr", "#CREF-RLVR", FieldType.NUMERIC, 11, 
            2);
        pnd_Real_Rlvr_A = localVariables.newFieldInRecord("pnd_Real_Rlvr_A", "#REAL-RLVR-A", FieldType.STRING, 11);

        pnd_Real_Rlvr_A__R_Field_5 = localVariables.newGroupInRecord("pnd_Real_Rlvr_A__R_Field_5", "REDEFINE", pnd_Real_Rlvr_A);
        pnd_Real_Rlvr_A_Pnd_Real_Rlvr = pnd_Real_Rlvr_A__R_Field_5.newFieldInGroup("pnd_Real_Rlvr_A_Pnd_Real_Rlvr", "#REAL-RLVR", FieldType.NUMERIC, 11, 
            2);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.NUMERIC, 8);

        pnd_Time__R_Field_6 = localVariables.newGroupInRecord("pnd_Time__R_Field_6", "REDEFINE", pnd_Time);
        pnd_Time_Pnd_Time_Hh = pnd_Time__R_Field_6.newFieldInGroup("pnd_Time_Pnd_Time_Hh", "#TIME-HH", FieldType.NUMERIC, 2);
        pnd_Time_Pnd_Time_Filler1 = pnd_Time__R_Field_6.newFieldInGroup("pnd_Time_Pnd_Time_Filler1", "#TIME-FILLER1", FieldType.STRING, 1);
        pnd_Time_Pnd_Time_Mm = pnd_Time__R_Field_6.newFieldInGroup("pnd_Time_Pnd_Time_Mm", "#TIME-MM", FieldType.NUMERIC, 2);
        pnd_Time_Pnd_Time_Filler2 = pnd_Time__R_Field_6.newFieldInGroup("pnd_Time_Pnd_Time_Filler2", "#TIME-FILLER2", FieldType.STRING, 1);
        pnd_Time_Pnd_Time_Ss = pnd_Time__R_Field_6.newFieldInGroup("pnd_Time_Pnd_Time_Ss", "#TIME-SS", FieldType.NUMERIC, 2);
        pnd_Heading = localVariables.newFieldInRecord("pnd_Heading", "#HEADING", FieldType.STRING, 44);
        pnd_Page_Number_Map = localVariables.newFieldInRecord("pnd_Page_Number_Map", "#PAGE-NUMBER-MAP", FieldType.NUMERIC, 8);
        pnd_Pec_Nbr_Active_Acct = localVariables.newFieldInRecord("pnd_Pec_Nbr_Active_Acct", "#PEC-NBR-ACTIVE-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Asterisk = localVariables.newFieldInRecord("pnd_Asterisk", "#ASTERISK", FieldType.STRING, 1);
        pnd_Comment1 = localVariables.newFieldInRecord("pnd_Comment1", "#COMMENT1", FieldType.STRING, 79);
        pnd_Comment2 = localVariables.newFieldInRecord("pnd_Comment2", "#COMMENT2", FieldType.STRING, 79);
        pnd_Amount = localVariables.newFieldInRecord("pnd_Amount", "#AMOUNT", FieldType.NUMERIC, 14, 2);
        pnd_Work_Rqst = localVariables.newFieldInRecord("pnd_Work_Rqst", "#WORK-RQST", FieldType.STRING, 35);

        pnd_Work_Rqst__R_Field_7 = localVariables.newGroupInRecord("pnd_Work_Rqst__R_Field_7", "REDEFINE", pnd_Work_Rqst);
        pnd_Work_Rqst_Pnd_Work_Rqst_30 = pnd_Work_Rqst__R_Field_7.newFieldInGroup("pnd_Work_Rqst_Pnd_Work_Rqst_30", "#WORK-RQST-30", FieldType.STRING, 
            30);
        pnd_Work_Rqst_Pnd_Work_Rqst_5 = pnd_Work_Rqst__R_Field_7.newFieldInGroup("pnd_Work_Rqst_Pnd_Work_Rqst_5", "#WORK-RQST-5", FieldType.STRING, 5);

        pnd_Work_Rqst__R_Field_8 = localVariables.newGroupInRecord("pnd_Work_Rqst__R_Field_8", "REDEFINE", pnd_Work_Rqst);
        pnd_Work_Rqst_Pnd_Work_Rqst_Pin7 = pnd_Work_Rqst__R_Field_8.newFieldInGroup("pnd_Work_Rqst_Pnd_Work_Rqst_Pin7", "#WORK-RQST-PIN7", FieldType.NUMERIC, 
            7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAdsl770.initializeValues();
        ldaAdsl761.initializeValues();
        ldaAdsl401.initializeValues();
        ldaAdsl401a.initializeValues();
        ldaAdsl450.initializeValues();

        localVariables.reset();
        pnd_Type_Of_Call.setInitialValue("R");
        pnd_All_Indx_Pnd_Z.setInitialValue(0);
        pnd_All_Indx_Pnd_A.setInitialValue(0);
        pnd_All_Indx_Pnd_Xx.setInitialValue(0);
        pnd_All_Indx_Pnd_A_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_B_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_C_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_X_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Y_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Next_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Cref_Indx_1.setInitialValue(0);
        pnd_All_Indx_Pnd_Cref_Indx_2.setInitialValue(0);
        pnd_All_Indx_Pnd_Cref_Indx_3.setInitialValue(0);
        pnd_All_Indx_Pnd_Cref_Indx_4.setInitialValue(0);
        pnd_All_Indx_Pnd_Cref_Indx_5.setInitialValue(0);
        pnd_All_Indx_Pnd_Prod_Indx.setInitialValue(0);
        pnd_Total_Cref_Nbr.setInitialValue(0);
        pnd_New_Header_Page_3.setInitialValue(true);
        pnd_Cref_Record.setInitialValue(true);
        pnd_Real_Estate_Record.setInitialValue(true);
        pnd_Tiaa_Trans.setInitialValue(true);
        pnd_Cref_Trans.setInitialValue(true);
        pnd_Real_Trans.setInitialValue(true);
        pnd_First_Time.setInitialValue(true);
        pnd_New_Record.setInitialValue(true);
        pnd_Cref_Rec_Found.setInitialValue(true);
        pnd_Record_Written.setInitialValue(true);
        pnd_End_Of_Ia_File.setInitialValue(true);
        pnd_Roth_Indicator_On.setInitialValue(false);
        pnd_Pec_Nbr_Active_Acct.setInitialValue(20);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp765() throws Exception
    {
        super("Adsp765");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 90 PS = 60;//Natural: FORMAT ( 1 ) LS = 90 PS = 60;//Natural: FORMAT ( 2 ) LS = 90 PS = 60;//Natural: FORMAT ( 3 ) LS = 90 PS = 60
        //*  NEW EXTERNALIZATION + SEQUENCING OF PROD CDE
        DbsUtil.callnat(Adsn888.class , getCurrentProcessState(), pdaAdsa888.getPnd_Parm_Area(), pdaAdsa888.getPnd_Nbr_Acct());                                           //Natural: CALLNAT 'ADSN888' #PARM-AREA #NBR-ACCT
        if (condition(Global.isEscape())) return;
        PND_PND_L0850:                                                                                                                                                    //Natural: REPEAT
        while (condition(whileTrue))
        {
            getWorkFiles().read(5, pnd_Rec_Type, ldaAdsl401.getVw_ads_Prtcpnt_View(), pnd_Tiaa_Rlvr_A, pnd_Cref_Rlvr_A, pnd_Real_Rlvr_A);                                 //Natural: READ WORK FILE 5 ONCE #REC-TYPE ADS-PRTCPNT-VIEW #TIAA-RLVR-A #CREF-RLVR-A #REAL-RLVR-A
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                if (true) break PND_PND_L0850;                                                                                                                            //Natural: ESCAPE BOTTOM ( ##L0850. )
            }                                                                                                                                                             //Natural: END-ENDFILE
            //*  GG050608
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Roth_Rqst_Ind().equals("Y")))                                                                                //Natural: IF ADP-ROTH-RQST-IND = 'Y'
            {
                pnd_Roth_Indicator_On.setValue(true);                                                                                                                     //Natural: MOVE TRUE TO #ROTH-INDICATOR-ON
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde().equals("R")))                                                                                 //Natural: IF ADP-ANNT-TYP-CDE = 'R'
            {
                ldaAdsl761.getPnd_Final_Prem_Ind().setValue("FINAL-PREMIUM");                                                                                             //Natural: MOVE 'FINAL-PREMIUM' TO #FINAL-PREM-IND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl761.getPnd_Final_Prem_Ind().setValue(" ");                                                                                                         //Natural: MOVE ' ' TO #FINAL-PREM-IND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_First_Time.getBoolean()))                                                                                                                   //Natural: IF #FIRST-TIME
            {
                pnd_First_Time.setValue(false);                                                                                                                           //Natural: MOVE FALSE TO #FIRST-TIME
                pnd_New_Header_Page_3.setValue(false);                                                                                                                    //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-3
            }                                                                                                                                                             //Natural: END-IF
            //*  PIN EXPANSION
            ldaAdsl761.getPnd_Rqst_Id().setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                                               //Natural: MOVE ADS-PRTCPNT-VIEW.RQST-ID TO #RQST-ID #WORK-RQST
            pnd_Work_Rqst.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());
            if (condition(pnd_Work_Rqst_Pnd_Work_Rqst_5.equals(" ")))                                                                                                     //Natural: IF #WORK-RQST-5 = ' '
            {
                ldaAdsl761.getPnd_Rqst_Id_Pnd_Unique_Id().setValue(pnd_Work_Rqst_Pnd_Work_Rqst_Pin7);                                                                     //Natural: ASSIGN #RQST-ID.#UNIQUE-ID := #WORK-RQST-PIN7
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl761.getPnd_Nap_Mit_Log_Dte_Tme().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Mit_Log_Dte_Tme());                                                       //Natural: MOVE ADP-MIT-LOG-DTE-TME TO #NAP-MIT-LOG-DTE-TME
            ldaAdsl761.getPnd_Nap_Bps_Unit().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Bps_Unit());                                                                     //Natural: MOVE ADP-BPS-UNIT TO #NAP-BPS-UNIT
                                                                                                                                                                          //Natural: PERFORM BUILD-CREF-PARTCIPANT-HEADER-DATA
            sub_Build_Cref_Partcipant_Header_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L0850"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0850"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM BUILD-CREF-DATA-ENTRY-INFORMATION
            sub_Build_Cref_Data_Entry_Information();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L0850"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0850"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_New_Record.setValue(true);                                                                                                                                //Natural: MOVE TRUE TO #NEW-RECORD
            pnd_Cref_Record.setValue(false);                                                                                                                              //Natural: MOVE FALSE TO #CREF-RECORD
            pnd_Real_Estate_Record.setValue(false);                                                                                                                       //Natural: MOVE FALSE TO #REAL-ESTATE-RECORD
            pnd_Cref_Trans.setValue(false);                                                                                                                               //Natural: MOVE FALSE TO #CREF-TRANS
            pnd_Real_Trans.setValue(false);                                                                                                                               //Natural: MOVE FALSE TO #REAL-TRANS
            pnd_Total_Cref_Nbr.setValue(0);                                                                                                                               //Natural: MOVE 0 TO #TOTAL-CREF-NBR
            ldaAdsl761.getPnd_Target_All_Pnd_Target_Disc().getValue("*").reset();                                                                                         //Natural: RESET #TARGET-DISC ( * )
            ldaAdsl761.getPnd_Cref_Cert_Ind().getValue("*").reset();                                                                                                      //Natural: RESET #CREF-CERT-IND ( * )
            getWorkFiles().read(5, pnd_Rec_Type, ldaAdsl401a.getAds_Cntrct_View_Rqst_Id(), pnd_Nac_Ppg_Code.getValue("*"), pnd_Nac_Ppg_Ind.getValue("*"),                 //Natural: READ WORK FILE 5 ONCE #REC-TYPE ADS-CNTRCT-VIEW.RQST-ID #NAC-PPG-CODE ( * ) #NAC-PPG-IND ( * ) #TIAA-NBR ( * ) #TIAA-IND ( * ) #TIAA-STLMNT-AMT #CREF-NBR ( * ) #CREF-IND ( * ) #CREF-STLMNT-AMT #REAL-NBR ( * ) #REAL-IND ( * ) #REAL-STLMNT-AMT
                pnd_Tiaa_Nbr.getValue("*"), pnd_Tiaa_Ind.getValue("*"), pnd_Tiaa_Stlmnt_Amt, pnd_Cref_Nbr.getValue("*"), pnd_Cref_Ind.getValue("*"), pnd_Cref_Stlmnt_Amt, 
                pnd_Real_Nbr.getValue("*"), pnd_Real_Ind.getValue("*"), pnd_Real_Stlmnt_Amt);
            pnd_All_Indx_Pnd_X_Indx.setValue(pnd_Rec_Type_Pnd_Z_Indx);                                                                                                    //Natural: MOVE #Z-INDX TO #X-INDX
            PND_PND_L1145:                                                                                                                                                //Natural: FOR #Y-INDX 1 TO #X-INDX
            for (pnd_All_Indx_Pnd_Y_Indx.setValue(1); condition(pnd_All_Indx_Pnd_Y_Indx.lessOrEqual(pnd_All_Indx_Pnd_X_Indx)); pnd_All_Indx_Pnd_Y_Indx.nadd(1))
            {
                getWorkFiles().read(5, pnd_Rec_Type, ldaAdsl401a.getVw_ads_Cntrct_View());                                                                                //Natural: READ WORK FILE 5 ONCE #REC-TYPE ADS-CNTRCT-VIEW
                if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                  //Natural: AT END OF FILE
                {
                    if (true) break PND_PND_L1145;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L1145. )
                }                                                                                                                                                         //Natural: END-ENDFILE
                //*  082012 START
                pnd_Cref_Record.setValue(false);                                                                                                                          //Natural: MOVE FALSE TO #CREF-RECORD
                pnd_Tiaa_Settled.setValue(false);                                                                                                                         //Natural: ASSIGN #TIAA-SETTLED := FALSE
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue("*").equals("T")))                                                                   //Natural: IF ADC-ACCT-CDE ( * ) = 'T'
                {
                    pnd_Tiaa_Settled.setValue(true);                                                                                                                      //Natural: ASSIGN #TIAA-SETTLED := TRUE
                    //*  082012 END
                }                                                                                                                                                         //Natural: END-IF
                PND_PND_L1210:                                                                                                                                            //Natural: FOR #A-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
                for (pnd_All_Indx_Pnd_A_Indx.setValue(1); condition(pnd_All_Indx_Pnd_A_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_A_Indx.nadd(1))
                {
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).equals(" ")))                                           //Natural: IF ADC-ACCT-CDE ( #A-INDX ) = ' '
                    {
                        if (true) break PND_PND_L1210;                                                                                                                    //Natural: ESCAPE BOTTOM ( ##L1210. )
                    }                                                                                                                                                     //Natural: END-IF
                    //*  RS0
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).equals("R") || ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).equals("D"))) //Natural: IF ADC-ACCT-CDE ( #A-INDX ) = 'R' OR ADC-ACCT-CDE ( #A-INDX ) = 'D'
                    {
                        pnd_Total_Cref_Nbr.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TOTAL-CREF-NBR
                        pnd_Cref_Record.setValue(true);                                                                                                                   //Natural: MOVE TRUE TO #CREF-RECORD
                        pnd_Real_Trans.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #REAL-TRANS
                        pnd_Real_Estate_Record.setValue(true);                                                                                                            //Natural: MOVE TRUE TO #REAL-ESTATE-RECORD
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L1145"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1145"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_New_Record.getBoolean() && pnd_Real_Trans.getBoolean()))                                                                                //Natural: IF #NEW-RECORD AND #REAL-TRANS
                {
                                                                                                                                                                          //Natural: PERFORM BUILD-CREF-DA-RSLT-HEADER-DATA
                    sub_Build_Cref_Da_Rslt_Header_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L1145"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1145"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-CREF-PARTICIPANT-HEADER-DATA
                    sub_Write_Cref_Participant_Header_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L1145"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1145"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM RESET-HEADER-TOTALS
                    sub_Reset_Header_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L1145"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1145"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_New_Record.setValue(false);                                                                                                                       //Natural: MOVE FALSE TO #NEW-RECORD
                    ldaAdsl761.getPnd_Final_Prem_Ind().reset();                                                                                                           //Natural: RESET #FINAL-PREM-IND
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Real_Trans.getBoolean()))                                                                                                               //Natural: IF #REAL-TRANS
                {
                    ldaAdsl761.getPnd_Tiaa_Contract_Nbr().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr());                                                        //Natural: MOVE ADC-TIAA-NBR TO #TIAA-CONTRACT-NBR
                    ldaAdsl761.getPnd_Cref_Certificate_Nbr().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Cref_Nbr());                                                     //Natural: MOVE ADC-CREF-NBR TO #CREF-CERTIFICATE-NBR
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Real_Trans.getBoolean()))                                                                                                               //Natural: IF #REAL-TRANS
                {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-DA-RSLT-ACCUMULATIONS
                    sub_Calculate_Cref_Da_Rslt_Accumulations();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L1145"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1145"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-TOTAL-SETTLEMENT
                    sub_Calculate_Cref_Total_Settlement();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L1145"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1145"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-CREF-DETAIL-LINE
                    sub_Write_Cref_Detail_Line();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L1145"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1145"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Cref_Record.setValue(false);                                                                                                                      //Natural: MOVE FALSE TO #CREF-RECORD
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM RESET-CREF-DETAIL-TOTAL-SAVE-AREA
                sub_Reset_Cref_Detail_Total_Save_Area();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L1145"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1145"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Cref_Trans.reset();                                                                                                                                   //Natural: RESET #CREF-TRANS
                pnd_Tiaa_Trans.reset();                                                                                                                                   //Natural: RESET #TIAA-TRANS
                pnd_Real_Trans.reset();                                                                                                                                   //Natural: RESET #REAL-TRANS
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L0850"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0850"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM READ-IA-RESULT-FILE
            sub_Read_Ia_Result_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L0850"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0850"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* ******************************************************************
            if (condition(! (pnd_End_Of_Ia_File.getBoolean())))                                                                                                           //Natural: IF NOT #END-OF-IA-FILE
            {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-IA-RSLT-ACCUMULATIONS
                sub_Calculate_Cref_Ia_Rslt_Accumulations();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L0850"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0850"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  082012
                if (condition(pnd_Real_Estate_Record.getBoolean() && ! (pnd_Tiaa_Settled.getBoolean())))                                                                  //Natural: IF #REAL-ESTATE-RECORD AND NOT #TIAA-SETTLED
                {
                    ldaAdsl770.getPnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Total().setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Ivc_Amt());                                  //Natural: MOVE ADI-TIAA-IVC-AMT TO #CREF-IVC-TOTAL
                    ldaAdsl770.getPnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Rtb().setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Prdc_Ivc_Amt());                               //Natural: MOVE ADI-TIAA-PRDC-IVC-AMT TO #CREF-IVC-RTB
                    ldaAdsl770.getPnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Annuity().compute(new ComputeParameters(true, ldaAdsl770.getPnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Annuity()),  //Natural: COMPUTE ROUNDED #CREF-IVC-ANNUITY = ADI-TIAA-IVC-AMT - ADI-TIAA-PRDC-IVC-AMT
                        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Ivc_Amt().subtract(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Prdc_Ivc_Amt()));
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-PREVIOUS-ANNUITANT-TOTAL-RTN
                sub_Process_Previous_Annuitant_Total_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L0850"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0850"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  GG050608
            pnd_Roth_Indicator_On.resetInitial();                                                                                                                         //Natural: RESET INITIAL #ROTH-INDICATOR-ON
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB-RTN
        sub_End_Of_Job_Rtn();
        if (condition(Global.isEscape())) {return;}
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IA-RESULT-FILE
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-CREF-PARTCIPANT-HEADER-DATA
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-CREF-DATA-ENTRY-INFORMATION
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-CREF-DA-RSLT-HEADER-DATA
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CREF-PARTICIPANT-HEADER-DATA
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-CREF-DA-RSLT-ACCUMULATIONS
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CREF-DA-ACCUMULATIONS
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-CREF-TOTAL-SETTLEMENT
        //*  ----------CURTIS 99 RATES 6/03
        //*  ----------CURTIS 99 RATES 6/03
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-CREF-IA-RSLT-ACCUMULATIONS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-CREF-MONTHLY-METHOD
        //*  ----------CURTIS 99 RATES 6/03
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-CREF-ANNUALLY-METHOD
        //*  ----------CURTIS 99 RATES 6/03
        //*           ** SET UP PRINT FIELDS **
        //* *             *******************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CREF-DETAIL-LINE
        //*  ----------CURTIS 99 RATES 6/03
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CREF-TOTAL-SETTLEMENT-LINE
        //*  ----------CURTIS 99 RATES 6/03
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CREF-MONTHLY-METHOD
        //*  ----------CURTIS 99 RATES 6/03
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CREF-ANNUALLY-METHOD
        //*  ----------CURTIS 99 RATES 6/03
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-PREVIOUS-ANNUITANT-TOTAL-RTN
        //*       WRITE   (1) NOTITLE USING FORM 'ADSM762'
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-CREF-DETAIL-TOTAL-SAVE-AREA
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-HEADER-TOTALS
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-CREF-FINAL-TOTAL-SAVE-AREA
        //* ********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PUT-ASTERISK-BESIDE-TOTAL
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-JOB-RTN
    }
    private void sub_Read_Ia_Result_File() throws Exception                                                                                                               //Natural: READ-IA-RESULT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        pnd_End_Of_Ia_File.setValue(false);                                                                                                                               //Natural: MOVE FALSE TO #END-OF-IA-FILE
        getWorkFiles().read(5, pnd_Rec_Type, ldaAdsl450.getVw_ads_Ia_Rslt_View(), ldaAdsl450.getPnd_More());                                                              //Natural: READ WORK FILE 5 ONCE #REC-TYPE ADS-IA-RSLT-VIEW #MORE
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            pnd_End_Of_Ia_File.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #END-OF-IA-FILE
        }                                                                                                                                                                 //Natural: END-ENDFILE
    }
    private void sub_Build_Cref_Partcipant_Header_Data() throws Exception                                                                                                 //Natural: BUILD-CREF-PARTCIPANT-HEADER-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        ldaAdsl761.getPnd_Partcipant_Name_1().setValue(DbsUtil.compress(ldaAdsl401.getAds_Prtcpnt_View_Adp_Frst_Annt_Frst_Nme(), ldaAdsl401.getAds_Prtcpnt_View_Adp_Frst_Annt_Mid_Nme(),  //Natural: COMPRESS ADP-FRST-ANNT-FRST-NME ADP-FRST-ANNT-MID-NME ADP-FRST-ANNT-LST-NME INTO #PARTCIPANT-NAME-1
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Frst_Annt_Lst_Nme()));
        ldaAdsl761.getPnd_Partcipant_Name_2().setValue(DbsUtil.compress(ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Frst_Nme(), ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Mid_Nme(),  //Natural: COMPRESS ADP-SCND-ANNT-FRST-NME ADP-SCND-ANNT-MID-NME ADP-SCND-ANNT-LST-NME INTO #PARTCIPANT-NAME-2
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Lst_Nme()));
        pnd_Code.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Frst_Annt_Rsdnc_Cde());                                                                                      //Natural: MOVE ADP-FRST-ANNT-RSDNC-CDE TO #CODE
        DbsUtil.callnat(Adsn035.class , getCurrentProcessState(), pdaAdspda_M.getMsg_Info_Sub(), pnd_Type_Of_Call, pnd_Code, pnd_Desc);                                   //Natural: CALLNAT 'ADSN035' MSG-INFO-SUB #TYPE-OF-CALL #CODE #DESC
        if (condition(Global.isEscape())) return;
        ldaAdsl761.getPnd_Residence().setValue(pnd_Desc);                                                                                                                 //Natural: MOVE #DESC TO #RESIDENCE
        ldaAdsl761.getPnd_Form_Comp_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Forms_Rcvd_Dte(),new ReportEditMask("YYYYMMDD"));                    //Natural: MOVE EDITED ADP-FORMS-RCVD-DTE ( EM = YYYYMMDD ) TO #FORM-COMP-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Mm());                       //Natural: MOVE #FORM-COMP-DTE-MM TO #FORM-COMP-DTE-MDCY-MM
        ldaAdsl761.getPnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Dd());                       //Natural: MOVE #FORM-COMP-DTE-DD TO #FORM-COMP-DTE-MDCY-DD
        ldaAdsl761.getPnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Yyyy());                   //Natural: MOVE #FORM-COMP-DTE-YYYY TO #FORM-COMP-DTE-MDCY-YYYY
        //*  /*
        ldaAdsl761.getPnd_Frst_Perod_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Strt_Dte(),new ReportEditMask("YYYYMMDD"));                   //Natural: MOVE EDITED ADP-ANNTY-STRT-DTE ( EM = YYYYMMDD ) TO #FRST-PEROD-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Mm());                   //Natural: MOVE #FRST-PEROD-DTE-MM TO #FRST-PEROD-DTE-MDCY-MM
        ldaAdsl761.getPnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Dd());                   //Natural: MOVE #FRST-PEROD-DTE-DD TO #FRST-PEROD-DTE-MDCY-DD
        ldaAdsl761.getPnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Yyyy());               //Natural: MOVE #FRST-PEROD-DTE-YYYY TO #FRST-PEROD-DTE-MDCY-YYYY
        ldaAdsl761.getPnd_Non_Prem_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                         //Natural: MOVE EDITED ADP-EFFCTV-DTE ( EM = YYYYMMDD ) TO #NON-PREM-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Mm());                           //Natural: MOVE #NON-PREM-DTE-MM TO #NON-PREM-DTE-MDCY-MM
        ldaAdsl761.getPnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Dd());                           //Natural: MOVE #NON-PREM-DTE-DD TO #NON-PREM-DTE-MDCY-DD
        ldaAdsl761.getPnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Yyyy());                       //Natural: MOVE #NON-PREM-DTE-YYYY TO #NON-PREM-DTE-MDCY-YYYY
        ldaAdsl761.getPnd_Cont_Issue_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Cntr_Prt_Issue_Dte(),new ReportEditMask("YYYYMMDD"));               //Natural: MOVE EDITED ADP-CNTR-PRT-ISSUE-DTE ( EM = YYYYMMDD ) TO #CONT-ISSUE-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Mm());                   //Natural: MOVE #CONT-ISSUE-DTE-MM TO #CONT-ISSUE-DTE-MDCY-MM
        ldaAdsl761.getPnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Dd());                   //Natural: MOVE #CONT-ISSUE-DTE-DD TO #CONT-ISSUE-DTE-MDCY-DD
        ldaAdsl761.getPnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Yyyy());               //Natural: MOVE #CONT-ISSUE-DTE-YYYY TO #CONT-ISSUE-DTE-MDCY-YYYY
        ldaAdsl761.getPnd_Effective_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                        //Natural: MOVE EDITED ADP-EFFCTV-DTE ( EM = YYYYMMDD ) TO #EFFECTIVE-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Dd());                       //Natural: MOVE #EFFECTIVE-DTE-DD TO #EFFECTIVE-DTE-MDCY-DD
        ldaAdsl761.getPnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Mm());                       //Natural: MOVE #EFFECTIVE-DTE-MM TO #EFFECTIVE-DTE-MDCY-MM
        ldaAdsl761.getPnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Yyyy());                   //Natural: MOVE #EFFECTIVE-DTE-YYYY TO #EFFECTIVE-DTE-MDCY-YYYY
        gdaAdsg870.getAdsg870_Pnd_Naza870_Hold_Settl_Dte().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte(),new ReportEditMask("CCYYMMDD"));                //Natural: MOVE EDITED ADP-EFFCTV-DTE ( EM = CCYYMMDD ) TO #NAZA870-HOLD-SETTL-DTE
        ldaAdsl761.getPnd_Frst_Birth_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Frst_Annt_Dte_Of_Brth(),new ReportEditMask("YYYYMMDD"));            //Natural: MOVE EDITED ADP-FRST-ANNT-DTE-OF-BRTH ( EM = YYYYMMDD ) TO #FRST-BIRTH-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Mm());                   //Natural: MOVE #FRST-BIRTH-DTE-MM TO #FRST-BIRTH-DTE-MDCY-MM
        ldaAdsl761.getPnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Dd());                   //Natural: MOVE #FRST-BIRTH-DTE-DD TO #FRST-BIRTH-DTE-MDCY-DD
        ldaAdsl761.getPnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Yyyy());               //Natural: MOVE #FRST-BIRTH-DTE-YYYY TO #FRST-BIRTH-DTE-MDCY-YYYY
        ldaAdsl761.getPnd_Scnd_Birth_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Dte_Of_Brth(),new ReportEditMask("YYYYMMDD"));            //Natural: MOVE EDITED ADP-SCND-ANNT-DTE-OF-BRTH ( EM = YYYYMMDD ) TO #SCND-BIRTH-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Mm());                   //Natural: MOVE #SCND-BIRTH-DTE-MM TO #SCND-BIRTH-DTE-MDCY-MM
        ldaAdsl761.getPnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Dd());                   //Natural: MOVE #SCND-BIRTH-DTE-DD TO #SCND-BIRTH-DTE-MDCY-DD
        ldaAdsl761.getPnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Yyyy());               //Natural: MOVE #SCND-BIRTH-DTE-YYYY TO #SCND-BIRTH-DTE-MDCY-YYYY
        ldaAdsl761.getPnd_Annt_Start_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Strt_Dte(),new ReportEditMask("YYYYMMDD"));                   //Natural: MOVE EDITED ADP-ANNTY-STRT-DTE ( EM = YYYYMMDD ) TO #ANNT-START-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Mm());                   //Natural: MOVE #ANNT-START-DTE-MM TO #ANNT-START-DTE-MDCY-MM
        ldaAdsl761.getPnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Dd());                   //Natural: MOVE #ANNT-START-DTE-DD TO #ANNT-START-DTE-MDCY-DD
        ldaAdsl761.getPnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Yyyy());               //Natural: MOVE #ANNT-START-DTE-YYYY TO #ANNT-START-DTE-MDCY-YYYY
        ldaAdsl761.getPnd_Empl_Term_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Emplymnt_Term_Dte(),new ReportEditMask("YYYYMMDD"));                 //Natural: MOVE EDITED ADP-EMPLYMNT-TERM-DTE ( EM = YYYYMMDD ) TO #EMPL-TERM-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Mm());                       //Natural: MOVE #EMPL-TERM-DTE-MM TO #EMPL-TERM-DTE-MDCY-MM
        ldaAdsl761.getPnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Dd());                       //Natural: MOVE #EMPL-TERM-DTE-DD TO #EMPL-TERM-DTE-MDCY-DD
        ldaAdsl761.getPnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Yyyy());                   //Natural: MOVE #EMPL-TERM-DTE-YYYY TO #EMPL-TERM-DTE-MDCY-YYYY
        ldaAdsl761.getPnd_Last_Guar_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Crrspndnce_Addr_Lst_Chg_Dte(),new ReportEditMask("YYYYMMDD"));       //Natural: MOVE EDITED ADP-CRRSPNDNCE-ADDR-LST-CHG-DTE ( EM = YYYYMMDD ) TO #LAST-GUAR-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Mm());                       //Natural: MOVE #LAST-GUAR-DTE-MM TO #LAST-GUAR-DTE-MDCY-MM
        ldaAdsl761.getPnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Dd());                       //Natural: MOVE #LAST-GUAR-DTE-DD TO #LAST-GUAR-DTE-MDCY-DD
        ldaAdsl761.getPnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Yyyy());                   //Natural: MOVE #LAST-GUAR-DTE-YYYY TO #LAST-GUAR-DTE-MDCY-YYYY
        ldaAdsl761.getPnd_Da_Accum_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                         //Natural: MOVE EDITED ADP-EFFCTV-DTE ( EM = YYYYMMDD ) TO #DA-ACCUM-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Mm());                               //Natural: MOVE #DA-ACCUM-DTE-MM TO #DA-ACCUM-DTE-MDCY-MM
        ldaAdsl761.getPnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Dd());                               //Natural: MOVE #DA-ACCUM-DTE-DD TO #DA-ACCUM-DTE-MDCY-DD
        ldaAdsl761.getPnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Yy().setValue(ldaAdsl761.getPnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Yy());                               //Natural: MOVE #DA-ACCUM-DTE-YY TO #DA-ACCUM-DTE-MDCY-YY
        //* *
        ldaAdsl761.getPnd_Ia_Number().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Ia_Tiaa_Nbr());                                                                         //Natural: MOVE ADP-IA-TIAA-NBR TO #IA-NUMBER
        ldaAdsl761.getPnd_Social_Sec_Nbr_1().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Frst_Annt_Ssn());                                                                //Natural: MOVE ADP-FRST-ANNT-SSN TO #SOCIAL-SEC-NBR-1
        ldaAdsl761.getPnd_Social_Sec_Nbr_2().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Ssn());                                                                //Natural: MOVE ADP-SCND-ANNT-SSN TO #SOCIAL-SEC-NBR-2
        ldaAdsl761.getPnd_Work_Rqst_Id().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Wpid());                                                                             //Natural: MOVE ADP-WPID TO #WORK-RQST-ID
        ldaAdsl761.getPnd_Sex_1().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Frst_Annt_Sex_Cde());                                                                       //Natural: MOVE ADP-FRST-ANNT-SEX-CDE TO #SEX-1
        ldaAdsl761.getPnd_Sex_2().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Sex_Cde());                                                                       //Natural: MOVE ADP-SCND-ANNT-SEX-CDE TO #SEX-2
        ldaAdsl761.getPnd_Frst_Citizenship().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Frst_Annt_Ctznshp());                                                            //Natural: MOVE ADP-FRST-ANNT-CTZNSHP TO #FRST-CITIZENSHIP
        ldaAdsl761.getPnd_Scnd_Spouse().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Rltnshp());                                                                 //Natural: MOVE ADP-SCND-ANNT-RLTNSHP TO #SCND-SPOUSE
        ldaAdsl761.getPnd_Nap_Annty_Optn().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Optn());                                                                     //Natural: MOVE ADP-ANNTY-OPTN TO #NAP-ANNTY-OPTN
        pnd_Payment_Mode.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode());                                                                                       //Natural: MOVE ADP-PYMNT-MODE TO #PAYMENT-MODE
        ldaAdsl761.getPnd_Nap_Grntee_Period().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Grntee_Period());                                                               //Natural: MOVE ADP-GRNTEE-PERIOD TO #NAP-GRNTEE-PERIOD
        ldaAdsl761.getPnd_Rollover_Amount().setValue(pnd_Real_Rlvr_A_Pnd_Real_Rlvr);                                                                                      //Natural: MOVE #REAL-RLVR TO #ROLLOVER-AMOUNT
        pnd_All_Indx_Pnd_A.reset();                                                                                                                                       //Natural: RESET #A #ROLLOVER-CONTRACT-NBR ( * )
        ldaAdsl761.getPnd_Rollover_Contract_Nbr().getValue("*").reset();
        if (condition(pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(1)))                                                                                                //Natural: IF #PAYMENT-MODE-BYTE-1 = 1
        {
            pnd_Payment_Mode_All.setValue("MONTHLY");                                                                                                                     //Natural: MOVE 'MONTHLY' TO #PAYMENT-MODE-ALL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(6)))                                                                                            //Natural: IF #PAYMENT-MODE-BYTE-1 = 6
            {
                pnd_Payment_Mode_All.setValue("QUARTLY");                                                                                                                 //Natural: MOVE 'QUARTLY' TO #PAYMENT-MODE-ALL
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(7)))                                                                                        //Natural: IF #PAYMENT-MODE-BYTE-1 = 7
                {
                    pnd_Payment_Mode_All.setValue("SEMI ANNUAL");                                                                                                         //Natural: MOVE 'SEMI ANNUAL' TO #PAYMENT-MODE-ALL
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(8)))                                                                                    //Natural: IF #PAYMENT-MODE-BYTE-1 = 8
                    {
                        pnd_Payment_Mode_All.setValue("ANNUAL");                                                                                                          //Natural: MOVE 'ANNUAL' TO #PAYMENT-MODE-ALL
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl761.getPnd_Nap_Pymnt_Mode_All().setValue(DbsUtil.compress(CompressOption.WithDelimiters, '/', pnd_Payment_Mode, pnd_Payment_Mode_All));                    //Natural: COMPRESS #PAYMENT-MODE #PAYMENT-MODE-ALL INTO #NAP-PYMNT-MODE-ALL WITH DELIMITER '/'
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX #DESTINATION-CDE ( * ) #RTB-ROLLOVER-CDE ( * )
        ldaAdsl761.getPnd_Destination_Code_Pnd_Destination_Cde().getValue("*").reset();
        ldaAdsl761.getPnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde().getValue("*").reset();
    }
    private void sub_Build_Cref_Data_Entry_Information() throws Exception                                                                                                 //Natural: BUILD-CREF-DATA-ENTRY-INFORMATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaAdsl761.getPnd_First_Entered_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Entry_Dte(),new ReportEditMask("YYYYMMDD"));                         //Natural: MOVE EDITED ADP-ENTRY-DTE ( EM = YYYYMMDD ) TO #FIRST-ENTERED-YYYYMMDD
        ldaAdsl761.getPnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Cc().setValue(ldaAdsl761.getPnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Cc());                   //Natural: MOVE #FIRST-ENTERED-CC TO #FIRST-ENTERED-DTE-MDCY-CC
        ldaAdsl761.getPnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Yy().setValue(ldaAdsl761.getPnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Yy());                   //Natural: MOVE #FIRST-ENTERED-YY TO #FIRST-ENTERED-DTE-MDCY-YY
        ldaAdsl761.getPnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Mm());                   //Natural: MOVE #FIRST-ENTERED-MM TO #FIRST-ENTERED-DTE-MDCY-MM
        ldaAdsl761.getPnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Dd());                   //Natural: MOVE #FIRST-ENTERED-DD TO #FIRST-ENTERED-DTE-MDCY-DD
        ldaAdsl761.getPnd_First_Verified_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Rqst_1st_Vrfd_Dte(),new ReportEditMask("YYYYMMDD"));                //Natural: MOVE EDITED ADP-RQST-1ST-VRFD-DTE ( EM = YYYYMMDD ) TO #FIRST-VERIFIED-YYYYMMDD
        ldaAdsl761.getPnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Cc().setValue(ldaAdsl761.getPnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Cc());               //Natural: MOVE #FIRST-VERIFIED-CC TO #FIRST-VERIFIED-DTE-MDCY-CC
        ldaAdsl761.getPnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Yy().setValue(ldaAdsl761.getPnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Yy());               //Natural: MOVE #FIRST-VERIFIED-YY TO #FIRST-VERIFIED-DTE-MDCY-YY
        ldaAdsl761.getPnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Mm());               //Natural: MOVE #FIRST-VERIFIED-MM TO #FIRST-VERIFIED-DTE-MDCY-MM
        ldaAdsl761.getPnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Dd());               //Natural: MOVE #FIRST-VERIFIED-DD TO #FIRST-VERIFIED-DTE-MDCY-DD
        ldaAdsl761.getPnd_Last_Updated_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Rqst_Last_Updtd_Dte(),new ReportEditMask("YYYYMMDD"));                //Natural: MOVE EDITED ADP-RQST-LAST-UPDTD-DTE ( EM = YYYYMMDD ) TO #LAST-UPDATED-YYYYMMDD
        ldaAdsl761.getPnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Cc().setValue(ldaAdsl761.getPnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Cc());                       //Natural: MOVE #LAST-UPDATED-CC TO #LAST-UPDATED-DTE-MDCY-CC
        ldaAdsl761.getPnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Yy().setValue(ldaAdsl761.getPnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Yy());                       //Natural: MOVE #LAST-UPDATED-YY TO #LAST-UPDATED-DTE-MDCY-YY
        ldaAdsl761.getPnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Mm());                       //Natural: MOVE #LAST-UPDATED-MM TO #LAST-UPDATED-DTE-MDCY-MM
        ldaAdsl761.getPnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Dd());                       //Natural: MOVE #LAST-UPDATED-DD TO #LAST-UPDATED-DTE-MDCY-DD
        ldaAdsl761.getPnd_Last_Verified_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Rqst_Last_Vrfd_Dte(),new ReportEditMask("YYYYMMDD"));                //Natural: MOVE EDITED ADP-RQST-LAST-VRFD-DTE ( EM = YYYYMMDD ) TO #LAST-VERIFIED-YYYYMMDD
        ldaAdsl761.getPnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Cc().setValue(ldaAdsl761.getPnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Cc());                   //Natural: MOVE #LAST-VERIFIED-CC TO #LAST-VERIFIED-DTE-MDCY-CC
        ldaAdsl761.getPnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Yy().setValue(ldaAdsl761.getPnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Yy());                   //Natural: MOVE #LAST-VERIFIED-YY TO #LAST-VERIFIED-DTE-MDCY-YY
        ldaAdsl761.getPnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Mm());                   //Natural: MOVE #LAST-VERIFIED-MM TO #LAST-VERIFIED-DTE-MDCY-MM
        ldaAdsl761.getPnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Dd());                   //Natural: MOVE #LAST-VERIFIED-DD TO #LAST-VERIFIED-DTE-MDCY-DD
        ldaAdsl770.getPnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_Last_Verified_By().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Rqst_Last_Vrfd_Id());                         //Natural: MOVE ADP-RQST-LAST-VRFD-ID TO #CREF-RQST-LAST-VERIFIED-BY
        ldaAdsl770.getPnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_Last_Updated_By().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Rqst_Last_Updtd_Id());                         //Natural: MOVE ADP-RQST-LAST-UPDTD-ID TO #CREF-RQST-LAST-UPDATED-BY
        ldaAdsl770.getPnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_First_Verified_By().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Rqst_1st_Vrfd_Id());                         //Natural: MOVE ADP-RQST-1ST-VRFD-ID TO #CREF-RQST-FIRST-VERIFIED-BY
        ldaAdsl770.getPnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_First_Entered_By().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Entry_User_Id());                             //Natural: MOVE ADP-ENTRY-USER-ID TO #CREF-RQST-FIRST-ENTERED-BY
    }
    private void sub_Build_Cref_Da_Rslt_Header_Data() throws Exception                                                                                                    //Natural: BUILD-CREF-DA-RSLT-HEADER-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        ldaAdsl761.getPnd_Tiaa_Contract_Nbr().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr());                                                                    //Natural: MOVE ADC-TIAA-NBR TO #TIAA-CONTRACT-NBR
        ldaAdsl761.getPnd_Cref_Certificate_Nbr().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Cref_Nbr());                                                                 //Natural: MOVE ADC-CREF-NBR TO #CREF-CERTIFICATE-NBR
        ldaAdsl761.getPnd_Account_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                      //Natural: MOVE EDITED ADP-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #ACCOUNT-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Mm());                               //Natural: MOVE #ACCOUNT-DTE-MM TO #ACCOUNT-DTE-MDCY-MM
        ldaAdsl761.getPnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Dd());                               //Natural: MOVE #ACCOUNT-DTE-DD TO #ACCOUNT-DTE-MDCY-DD
        ldaAdsl761.getPnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Yyyy());                           //Natural: MOVE #ACCOUNT-DTE-YYYY TO #ACCOUNT-DTE-MDCY-YYYY
        ldaAdsl761.getPnd_Target_All_Pnd_Target_Number().getValue("*").setValue(pnd_Nac_Ppg_Code.getValue("*"));                                                          //Natural: MOVE #NAC-PPG-CODE ( * ) TO #TARGET-NUMBER ( * )
        ldaAdsl761.getPnd_Target_All_Pnd_Target_Disc().getValue("*").setValue(pnd_Nac_Ppg_Ind.getValue("*"));                                                             //Natural: MOVE #NAC-PPG-IND ( * ) TO #TARGET-DISC ( * )
        FOR01:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            //*  RS0
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_B_Indx).equals("R") || ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_B_Indx).equals("D"))) //Natural: IF ADC-ACCT-CDE ( #B-INDX ) = 'R' OR ADC-ACCT-CDE ( #B-INDX ) = 'D'
            {
                ldaAdsl770.getPnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Settle_Amount().setValue(pnd_Real_Stlmnt_Amt);                                               //Natural: MOVE #REAL-STLMNT-AMT TO #CREF-HEADER-SETTLE-AMOUNT
                ldaAdsl770.getPnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Rtb_Settle_Amount().setValue(pnd_Real_Rtb_Stlmnt_Amt);                                       //Natural: MOVE #REAL-RTB-STLMNT-AMT TO #CREF-HEADER-RTB-SETTLE-AMOUNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO 10
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(10)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(pnd_Real_Nbr.getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                                                                    //Natural: IF #REAL-NBR ( #B-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl761.getPnd_Cref_Cert_Nbr().getValue(pnd_All_Indx_Pnd_B_Indx).setValueEdited(pnd_Real_Nbr.getValue(pnd_All_Indx_Pnd_B_Indx),new ReportEditMask("XXXXXXX-XXX")); //Natural: MOVE EDITED #REAL-NBR ( #B-INDX ) ( EM = XXXXXXX-XXX ) TO #CREF-CERT-NBR ( #B-INDX )
            ldaAdsl761.getPnd_Cref_Cert_Ind().getValue(pnd_All_Indx_Pnd_B_Indx).setValue(pnd_Real_Ind.getValue(pnd_All_Indx_Pnd_B_Indx));                                 //Natural: MOVE #REAL-IND ( #B-INDX ) TO #CREF-CERT-IND ( #B-INDX )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Cref_Participant_Header_Data() throws Exception                                                                                                //Natural: WRITE-CREF-PARTICIPANT-HEADER-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //*  082012
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm861a.class));                                                                              //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM861A'
        //*  GG050608
        if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                                //Natural: IF #ROTH-INDICATOR-ON
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            //*  082012
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm861a.class));                                                                          //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM861A'
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl761.getPnd_Cref_Cert_Nbr().getValue("*").reset();                                                                                                          //Natural: RESET #CREF-CERT-NBR ( * )
        ldaAdsl761.getPnd_Tiaa_Cert_Nbr().getValue("*").reset();                                                                                                          //Natural: RESET #TIAA-CERT-NBR ( * )
    }
    private void sub_Calculate_Cref_Da_Rslt_Accumulations() throws Exception                                                                                              //Natural: CALCULATE-CREF-DA-RSLT-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //*  *********************************************************************
        pnd_All_Indx_Pnd_A_Indx.reset();                                                                                                                                  //Natural: RESET #A-INDX
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        pnd_All_Indx_Pnd_Cref_Indx_1.reset();                                                                                                                             //Natural: RESET #CREF-INDX-1
        FOR03:                                                                                                                                                            //Natural: FOR #A-INDX 1 TO 20
        for (pnd_All_Indx_Pnd_A_Indx.setValue(1); condition(pnd_All_Indx_Pnd_A_Indx.lessOrEqual(20)); pnd_All_Indx_Pnd_A_Indx.nadd(1))
        {
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).equals(" ")))                                                   //Natural: IF ADC-ACCT-CDE ( #A-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  RS0
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).equals("R") || ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).equals("D"))) //Natural: IF ADC-ACCT-CDE ( #A-INDX ) = 'R' OR ADC-ACCT-CDE ( #A-INDX ) = 'D'
            {
                ldaAdsl770.getPnd_Cref_Rec_Cnt().nadd(1);                                                                                                                 //Natural: ADD 1 TO #CREF-REC-CNT
                //*  GG050608
                if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                        //Natural: IF #ROTH-INDICATOR-ON
                {
                    ldaAdsl770.getPnd_Cref_Rec_Cnt_Roth().nadd(1);                                                                                                        //Natural: ADD 1 TO #CREF-REC-CNT-ROTH
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-CREF-DA-ACCUMULATIONS
                sub_Get_Cref_Da_Accumulations();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Get_Cref_Da_Accumulations() throws Exception                                                                                                         //Natural: GET-CREF-DA-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero())))                                   //Natural: IF ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) NE 0
        {
            pnd_All_Indx_Pnd_Cref_Indx_1.nadd(1);                                                                                                                         //Natural: ADD 1 TO #CREF-INDX-1
            ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Method().getValue(pnd_All_Indx_Pnd_Cref_Indx_1).setValue("MONTHLY");                                  //Natural: MOVE 'MONTHLY' TO #C-SAVE-DA-METHOD ( #CREF-INDX-1 )
            ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Prod().getValue(pnd_All_Indx_Pnd_Cref_Indx_1).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: MOVE ADC-ACCT-CDE ( #A-INDX ) TO #C-SAVE-CREF-PROD ( #CREF-INDX-1 )
            ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Rate().getValue(pnd_All_Indx_Pnd_Cref_Indx_1).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cref_Rate_Cde().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: MOVE ADC-ACCT-CREF-RATE-CDE ( #A-INDX ) TO #C-SAVE-DA-RATE ( #CREF-INDX-1 )
            ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Accum_Amt().getValue(pnd_All_Indx_Pnd_Cref_Indx_1).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Opn_Accum_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-ACCT-OPN-ACCUM-AMT ( #A-INDX ) TO #C-SAVE-DA-ACCUM-AMT ( #CREF-INDX-1 )
            ldaAdsl770.getPnd_Cref_Sub_Total_Pnd_Cref_Sub_Da_Accum_Total().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Opn_Accum_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-ACCT-OPN-ACCUM-AMT ( #A-INDX ) TO #CREF-SUB-DA-ACCUM-TOTAL
            ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Amt().getValue(pnd_All_Indx_Pnd_Cref_Indx_1).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-ACCT-ACTL-AMT ( #A-INDX ) TO #C-SAVE-SETTLED-AMT ( #CREF-INDX-1 )
            ldaAdsl770.getPnd_Cref_Sub_Total_Pnd_Cref_Sub_Settled_Total().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx));     //Natural: ADD ADC-ACCT-ACTL-AMT ( #A-INDX ) TO #CREF-SUB-SETTLED-TOTAL
            ldaAdsl770.getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Da_Accum_Units().getValue(pnd_All_Indx_Pnd_Cref_Indx_1).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Opn_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-ACCT-OPN-NBR-UNITS ( #A-INDX ) TO #C-SAVE-DA-ACCUM-UNITS ( #CREF-INDX-1 )
            ldaAdsl770.getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Settled_Units().getValue(pnd_All_Indx_Pnd_Cref_Indx_1).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-ACCT-ACTL-NBR-UNITS ( #A-INDX ) TO #C-SAVE-SETTLED-UNITS ( #CREF-INDX-1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero())))                              //Natural: IF ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) NE 0
            {
                pnd_All_Indx_Pnd_Cref_Indx_1.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CREF-INDX-1
                ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Prod().getValue(pnd_All_Indx_Pnd_Cref_Indx_1).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: MOVE ADC-ACCT-CDE ( #A-INDX ) TO #C-SAVE-CREF-PROD ( #CREF-INDX-1 )
                ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Rate().getValue(pnd_All_Indx_Pnd_Cref_Indx_1).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cref_Rate_Cde().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: MOVE ADC-ACCT-CREF-RATE-CDE ( #A-INDX ) TO #C-SAVE-DA-RATE ( #CREF-INDX-1 )
                ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Method().getValue(pnd_All_Indx_Pnd_Cref_Indx_1).setValue("ANNUALLY");                             //Natural: MOVE 'ANNUALLY' TO #C-SAVE-DA-METHOD ( #CREF-INDX-1 )
                ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Accum_Amt().getValue(pnd_All_Indx_Pnd_Cref_Indx_1).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Opn_Accum_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-ACCT-OPN-ACCUM-AMT ( #A-INDX ) TO #C-SAVE-DA-ACCUM-AMT ( #CREF-INDX-1 )
                ldaAdsl770.getPnd_Cref_Sub_Total_Pnd_Cref_Sub_Da_Accum_Total().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Opn_Accum_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-ACCT-OPN-ACCUM-AMT ( #A-INDX ) TO #CREF-SUB-DA-ACCUM-TOTAL
                ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Amt().getValue(pnd_All_Indx_Pnd_Cref_Indx_1).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-ACCT-ACTL-AMT ( #A-INDX ) TO #C-SAVE-SETTLED-AMT ( #CREF-INDX-1 )
                ldaAdsl770.getPnd_Cref_Sub_Total_Pnd_Cref_Sub_Settled_Total().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-ACCT-ACTL-AMT ( #A-INDX ) TO #CREF-SUB-SETTLED-TOTAL
                ldaAdsl770.getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Da_Accum_Units().getValue(pnd_All_Indx_Pnd_Cref_Indx_1).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Opn_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-ACCT-OPN-NBR-UNITS ( #A-INDX ) TO #C-SAVE-DA-ACCUM-UNITS ( #CREF-INDX-1 )
                ldaAdsl770.getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Settled_Units().getValue(pnd_All_Indx_Pnd_Cref_Indx_1).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-ACCT-ACTL-NBR-UNITS ( #A-INDX ) TO #C-SAVE-SETTLED-UNITS ( #CREF-INDX-1 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Calculate_Cref_Total_Settlement() throws Exception                                                                                                   //Natural: CALCULATE-CREF-TOTAL-SETTLEMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        pnd_Cref_Rec_Found.setValue(false);                                                                                                                               //Natural: MOVE FALSE TO #CREF-REC-FOUND
        pnd_Record_Written.setValue(false);                                                                                                                               //Natural: MOVE FALSE TO #RECORD-WRITTEN
        PND_PND_L3330:                                                                                                                                                    //Natural: FOR #B-INDX 1 TO 80
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(80)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Prod().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                //Natural: IF #C-SAVE-CREF-PROD ( #B-INDX ) = ' '
            {
                if (true) break PND_PND_L3330;                                                                                                                            //Natural: ESCAPE BOTTOM ( ##L3330. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_All_Indx_Pnd_C_Indx.reset();                                                                                                                              //Natural: RESET #C-INDX
            pnd_Cref_Rec_Found.setValue(false);                                                                                                                           //Natural: MOVE FALSE TO #CREF-REC-FOUND
            pnd_Record_Written.setValue(false);                                                                                                                           //Natural: MOVE FALSE TO #RECORD-WRITTEN
            PND_PND_L3375:                                                                                                                                                //Natural: FOR #C-INDX 1 TO 80
            for (pnd_All_Indx_Pnd_C_Indx.setValue(1); condition(pnd_All_Indx_Pnd_C_Indx.lessOrEqual(80)); pnd_All_Indx_Pnd_C_Indx.nadd(1))
            {
                if (condition(ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Cref_Final_Prod().getValue(pnd_All_Indx_Pnd_C_Indx).equals(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Prod().getValue(pnd_All_Indx_Pnd_B_Indx))  //Natural: IF #C-SAVE-CREF-FINAL-PROD ( #C-INDX ) = #C-SAVE-CREF-PROD ( #B-INDX ) AND #C-SAVE-DA-FINAL-RATE ( #C-INDX ) = #C-SAVE-DA-RATE ( #B-INDX ) AND #C-SAVE-DA-FINAL-METHOD ( #C-INDX ) = #C-SAVE-DA-METHOD ( #B-INDX )
                    && ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Rate().getValue(pnd_All_Indx_Pnd_C_Indx).equals(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Rate().getValue(pnd_All_Indx_Pnd_B_Indx)) 
                    && ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Method().getValue(pnd_All_Indx_Pnd_C_Indx).equals(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Method().getValue(pnd_All_Indx_Pnd_B_Indx))))
                {
                    pnd_Cref_Rec_Found.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #CREF-REC-FOUND
                    if (true) break PND_PND_L3375;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L3375. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L3330"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L3330"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Cref_Rec_Found.getBoolean()))                                                                                                               //Natural: IF #CREF-REC-FOUND
            {
                pnd_Cref_Rec_Found.setValue(false);                                                                                                                       //Natural: MOVE FALSE TO #CREF-REC-FOUND
                ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Rate().getValue(pnd_All_Indx_Pnd_C_Indx).setValue(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Rate().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-DA-RATE ( #B-INDX ) TO #C-SAVE-DA-FINAL-RATE ( #C-INDX )
                ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Cref_Final_Prod().getValue(pnd_All_Indx_Pnd_C_Indx).setValue(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Prod().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-CREF-PROD ( #B-INDX ) TO #C-SAVE-CREF-FINAL-PROD ( #C-INDX )
                ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Method().getValue(pnd_All_Indx_Pnd_C_Indx).setValue(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Method().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-DA-METHOD ( #B-INDX ) TO #C-SAVE-DA-FINAL-METHOD ( #C-INDX )
                ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Accum_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).nadd(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Accum_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-DA-ACCUM-AMT ( #B-INDX ) TO #C-SAVE-DA-FINAL-ACCUM-AMT ( #C-INDX )
                ldaAdsl770.getPnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Da_Accum_Total().nadd(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Accum_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-DA-ACCUM-AMT ( #B-INDX ) TO #CREF-TOT-DA-ACCUM-TOTAL
                ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Settled_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).nadd(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-SETTLED-AMT ( #B-INDX ) TO #C-SAVE-FINAL-SETTLED-AMT ( #C-INDX )
                ldaAdsl770.getPnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Total().nadd(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-SETTLED-AMT ( #B-INDX ) TO #CREF-TOT-SETTLED-TOTAL
                ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Rtb_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).nadd(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Rtb_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-RTB-AMT ( #B-INDX ) TO #C-SAVE-FINAL-RTB-AMT ( #C-INDX )
                ldaAdsl770.getPnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Rtb_Total().nadd(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Rtb_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-RTB-AMT ( #B-INDX ) TO #CREF-TOT-RTB-TOTAL
                ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Settled_Rtb_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).nadd(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Rtb_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-SETTLED-RTB-AMT ( #B-INDX ) TO #C-SAVE-FINAL-SETTLED-RTB-AMT ( #C-INDX )
                ldaAdsl770.getPnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Rtb_Total().nadd(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Rtb_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-SETTLED-RTB-AMT ( #B-INDX ) TO #CREF-TOT-SETTLED-RTB-TOTAL
                //*  06-22-04 C. AVE
                ldaAdsl770.getPnd_Cref_Sub_Total_Pnd_Cref_Sub_Settled_Rtb_Total().nadd(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Rtb_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-SETTLED-RTB-AMT ( #B-INDX ) TO #CREF-SUB-SETTLED-RTB-TOTAL
                ldaAdsl770.getPnd_C_Save_Final_Total_Units_Pnd_C_Save_Da_Final_Accum_Units().getValue(pnd_All_Indx_Pnd_C_Indx).nadd(ldaAdsl770.getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Da_Accum_Units().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-DA-ACCUM-UNITS ( #B-INDX ) TO #C-SAVE-DA-FINAL-ACCUM-UNITS ( #C-INDX )
                ldaAdsl770.getPnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Settled_Units().getValue(pnd_All_Indx_Pnd_C_Indx).nadd(ldaAdsl770.getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Settled_Units().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-SETTLED-UNITS ( #B-INDX ) TO #C-SAVE-FINAL-SETTLED-UNITS ( #C-INDX )
                ldaAdsl770.getPnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Rtb_Units().getValue(pnd_All_Indx_Pnd_C_Indx).nadd(ldaAdsl770.getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Rtb_Units().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-RTB-UNITS ( #B-INDX ) TO #C-SAVE-FINAL-RTB-UNITS ( #C-INDX )
                ldaAdsl770.getPnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Settled_Rtb_Units().getValue(pnd_All_Indx_Pnd_C_Indx).nadd(ldaAdsl770.getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Settled_Rtb_Units().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-SETTLED-RTB-UNITS ( #B-INDX ) TO #C-SAVE-FINAL-SETTLED-RTB-UNITS ( #C-INDX )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_All_Indx_Pnd_Cref_Indx_2.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CREF-INDX-2
                ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Rate().getValue(pnd_All_Indx_Pnd_Cref_Indx_2).setValue(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Rate().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-DA-RATE ( #B-INDX ) TO #C-SAVE-DA-FINAL-RATE ( #CREF-INDX-2 )
                ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Cref_Final_Prod().getValue(pnd_All_Indx_Pnd_Cref_Indx_2).setValue(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Prod().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-CREF-PROD ( #B-INDX ) TO #C-SAVE-CREF-FINAL-PROD ( #CREF-INDX-2 )
                ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Method().getValue(pnd_All_Indx_Pnd_Cref_Indx_2).setValue(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Method().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-DA-METHOD ( #B-INDX ) TO #C-SAVE-DA-FINAL-METHOD ( #CREF-INDX-2 )
                ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Accum_Amt().getValue(pnd_All_Indx_Pnd_Cref_Indx_2).nadd(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Accum_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-DA-ACCUM-AMT ( #B-INDX ) TO #C-SAVE-DA-FINAL-ACCUM-AMT ( #CREF-INDX-2 )
                ldaAdsl770.getPnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Da_Accum_Total().nadd(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Accum_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-DA-ACCUM-AMT ( #B-INDX ) TO #CREF-TOT-DA-ACCUM-TOTAL
                ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Settled_Amt().getValue(pnd_All_Indx_Pnd_Cref_Indx_2).nadd(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-SETTLED-AMT ( #B-INDX ) TO #C-SAVE-FINAL-SETTLED-AMT ( #CREF-INDX-2 )
                ldaAdsl770.getPnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Total().nadd(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-SETTLED-AMT ( #B-INDX ) TO #CREF-TOT-SETTLED-TOTAL
                ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Rtb_Amt().getValue(pnd_All_Indx_Pnd_Cref_Indx_2).nadd(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Rtb_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-RTB-AMT ( #B-INDX ) TO #C-SAVE-FINAL-RTB-AMT ( #CREF-INDX-2 )
                ldaAdsl770.getPnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Rtb_Total().nadd(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Rtb_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-RTB-AMT ( #B-INDX ) TO #CREF-TOT-RTB-TOTAL
                ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Settled_Rtb_Amt().getValue(pnd_All_Indx_Pnd_Cref_Indx_2).nadd(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Rtb_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-SETTLED-RTB-AMT ( #B-INDX ) TO #C-SAVE-FINAL-SETTLED-RTB-AMT ( #CREF-INDX-2 )
                ldaAdsl770.getPnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Rtb_Total().nadd(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Rtb_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-SETTLED-RTB-AMT ( #B-INDX ) TO #CREF-TOT-SETTLED-RTB-TOTAL
                //*  06-22-04 C. AVE
                ldaAdsl770.getPnd_Cref_Sub_Total_Pnd_Cref_Sub_Settled_Rtb_Total().nadd(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Rtb_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-SETTLED-RTB-AMT ( #B-INDX ) TO #CREF-SUB-SETTLED-RTB-TOTAL
                ldaAdsl770.getPnd_C_Save_Final_Total_Units_Pnd_C_Save_Da_Final_Accum_Units().getValue(pnd_All_Indx_Pnd_Cref_Indx_2).nadd(ldaAdsl770.getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Da_Accum_Units().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-DA-ACCUM-UNITS ( #B-INDX ) TO #C-SAVE-DA-FINAL-ACCUM-UNITS ( #CREF-INDX-2 )
                ldaAdsl770.getPnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Settled_Units().getValue(pnd_All_Indx_Pnd_Cref_Indx_2).nadd(ldaAdsl770.getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Settled_Units().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-SETTLED-UNITS ( #B-INDX ) TO #C-SAVE-FINAL-SETTLED-UNITS ( #CREF-INDX-2 )
                ldaAdsl770.getPnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Rtb_Units().getValue(pnd_All_Indx_Pnd_Cref_Indx_2).nadd(ldaAdsl770.getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Rtb_Units().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-RTB-UNITS ( #B-INDX ) TO #C-SAVE-FINAL-RTB-UNITS ( #CREF-INDX-2 )
                ldaAdsl770.getPnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Settled_Rtb_Units().getValue(pnd_All_Indx_Pnd_Cref_Indx_2).nadd(ldaAdsl770.getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Settled_Rtb_Units().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #C-SAVE-SETTLED-RTB-UNITS ( #B-INDX ) TO #C-SAVE-FINAL-SETTLED-RTB-UNITS ( #CREF-INDX-2 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Calculate_Cref_Ia_Rslt_Accumulations() throws Exception                                                                                              //Natural: CALCULATE-CREF-IA-RSLT-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        pnd_All_Indx_Pnd_Prod_Indx.reset();                                                                                                                               //Natural: RESET #PROD-INDX
        FOR04:                                                                                                                                                            //Natural: FOR #A-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
        for (pnd_All_Indx_Pnd_A_Indx.setValue(1); condition(pnd_All_Indx_Pnd_A_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_A_Indx.nadd(1))
        {
            //*  RS1
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).equals("R") || ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).equals("D"))) //Natural: IF ADI-DTL-CREF-ACCT-CD ( #A-INDX ) = 'R' OR ADI-DTL-CREF-ACCT-CD ( #A-INDX ) = 'D'
            {
                pnd_Cref_Record.setValue(true);                                                                                                                           //Natural: MOVE TRUE TO #CREF-RECORD
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-MONTHLY-METHOD
                sub_Calculate_Cref_Monthly_Method();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-ANNUALLY-METHOD
                sub_Calculate_Cref_Annually_Method();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Calculate_Cref_Monthly_Method() throws Exception                                                                                                     //Natural: CALCULATE-CREF-MONTHLY-METHOD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx).equals(getZero())))                                      //Natural: IF ADI-DTL-CREF-MNTHLY-AMT ( #A-INDX ) = 0
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cref_Rec_Found.setValue(false);                                                                                                                               //Natural: MOVE FALSE TO #CREF-REC-FOUND
        pnd_Record_Written.setValue(false);                                                                                                                               //Natural: MOVE FALSE TO #RECORD-WRITTEN
        FOR05:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO 80
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(80)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(pnd_Record_Written.getBoolean()))                                                                                                               //Natural: IF #RECORD-WRITTEN
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Prod_Monthly().getValue(pnd_All_Indx_Pnd_B_Indx).equals(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx))  //Natural: IF #C-SAVE-PROD-MONTHLY ( #B-INDX ) = ADI-DTL-CREF-ACCT-CD ( #A-INDX ) AND #C-SAVE-IA-RATE-MONTHLY ( #B-INDX ) = ADI-DTL-CREF-IA-RATE-CD ( #A-INDX ) AND #C-SAVE-DA-RATE-MONTHLY ( #B-INDX ) = ADI-DTL-CREF-DA-RATE-CD ( #A-INDX )
                && ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Rate_Monthly().getValue(pnd_All_Indx_Pnd_B_Indx).equals(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_A_Indx)) 
                && ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Da_Rate_Monthly().getValue(pnd_All_Indx_Pnd_B_Indx).equals(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Rate_Cd().getValue(pnd_All_Indx_Pnd_A_Indx))))
            {
                pnd_Cref_Rec_Found.setValue(true);                                                                                                                        //Natural: MOVE TRUE TO #CREF-REC-FOUND
            }                                                                                                                                                             //Natural: END-IF
            pnd_Record_Written.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #RECORD-WRITTEN
            if (condition(pnd_Cref_Rec_Found.getBoolean()))                                                                                                               //Natural: IF #CREF-REC-FOUND
            {
                pnd_Cref_Rec_Found.setValue(false);                                                                                                                       //Natural: MOVE FALSE TO #CREF-REC-FOUND
                ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Prod_Monthly().getValue(pnd_All_Indx_Pnd_B_Indx).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: MOVE ADI-DTL-CREF-ACCT-CD ( #A-INDX ) TO #C-SAVE-PROD-MONTHLY ( #B-INDX )
                ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Rate_Monthly().getValue(pnd_All_Indx_Pnd_B_Indx).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: MOVE ADI-DTL-CREF-IA-RATE-CD ( #A-INDX ) TO #C-SAVE-IA-RATE-MONTHLY ( #B-INDX )
                ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Da_Rate_Monthly().getValue(pnd_All_Indx_Pnd_B_Indx).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Rate_Cd().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: MOVE ADI-DTL-CREF-DA-RATE-CD ( #A-INDX ) TO #C-SAVE-DA-RATE-MONTHLY ( #B-INDX )
                ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Settled_Monthly_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-MNTHLY-AMT ( #A-INDX ) TO #C-SAVE-SETTLED-MONTHLY-AMT ( #B-INDX )
                ldaAdsl770.getPnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Settled_Monthly_Final_Amt().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-MNTHLY-AMT ( #A-INDX ) TO #CREF-SETTLED-MONTHLY-FINAL-AMT
                ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Units().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #C-SAVE-IA-MONTHLY-UNITS ( #B-INDX )
                ldaAdsl770.getPnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Units().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #CREF-IA-MONTHLY-FINAL-UNITS
                ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Payments().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #C-SAVE-IA-MONTHLY-PAYMENTS ( #B-INDX )
                ldaAdsl770.getPnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Payments().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #CREF-IA-MONTHLY-FINAL-PAYMENTS
                ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Unit_Val().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL ( #A-INDX ) TO #C-SAVE-IA-MONTHLY-UNIT-VAL ( #B-INDX )
                ldaAdsl770.getPnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Unit_Val().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL ( #A-INDX ) TO #CREF-IA-MONTHLY-FINAL-UNIT-VAL
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_All_Indx_Pnd_Cref_Indx_3.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CREF-INDX-3
                ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Prod_Monthly().getValue(pnd_All_Indx_Pnd_Cref_Indx_3).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: MOVE ADI-DTL-CREF-ACCT-CD ( #A-INDX ) TO #C-SAVE-PROD-MONTHLY ( #CREF-INDX-3 )
                ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Rate_Monthly().getValue(pnd_All_Indx_Pnd_Cref_Indx_3).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: MOVE ADI-DTL-CREF-IA-RATE-CD ( #A-INDX ) TO #C-SAVE-IA-RATE-MONTHLY ( #CREF-INDX-3 )
                ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Da_Rate_Monthly().getValue(pnd_All_Indx_Pnd_Cref_Indx_3).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Rate_Cd().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: MOVE ADI-DTL-CREF-DA-RATE-CD ( #A-INDX ) TO #C-SAVE-DA-RATE-MONTHLY ( #CREF-INDX-3 )
                ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Settled_Monthly_Amt().getValue(pnd_All_Indx_Pnd_Cref_Indx_3).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-MNTHLY-AMT ( #A-INDX ) TO #C-SAVE-SETTLED-MONTHLY-AMT ( #CREF-INDX-3 )
                ldaAdsl770.getPnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Settled_Monthly_Final_Amt().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-MNTHLY-AMT ( #A-INDX ) TO #CREF-SETTLED-MONTHLY-FINAL-AMT
                ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Units().getValue(pnd_All_Indx_Pnd_Cref_Indx_3).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #C-SAVE-IA-MONTHLY-UNITS ( #CREF-INDX-3 )
                ldaAdsl770.getPnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Units().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #CREF-IA-MONTHLY-FINAL-UNITS
                ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Payments().getValue(pnd_All_Indx_Pnd_Cref_Indx_3).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #C-SAVE-IA-MONTHLY-PAYMENTS ( #CREF-INDX-3 )
                ldaAdsl770.getPnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Payments().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #CREF-IA-MONTHLY-FINAL-PAYMENTS
                ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Unit_Val().getValue(pnd_All_Indx_Pnd_Cref_Indx_3).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL ( #A-INDX ) TO #C-SAVE-IA-MONTHLY-UNIT-VAL ( #CREF-INDX-3 )
                ldaAdsl770.getPnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Unit_Val().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL ( #A-INDX ) TO #CREF-IA-MONTHLY-FINAL-UNIT-VAL
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Calculate_Cref_Annually_Method() throws Exception                                                                                                    //Natural: CALCULATE-CREF-ANNUALLY-METHOD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx).equals(getZero())))                                        //Natural: IF ADI-DTL-CREF-ANNL-AMT ( #A-INDX ) = 0
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cref_Rec_Found.setValue(false);                                                                                                                               //Natural: MOVE FALSE TO #CREF-REC-FOUND
        pnd_Record_Written.setValue(false);                                                                                                                               //Natural: MOVE FALSE TO #RECORD-WRITTEN
        FOR06:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO 80
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(80)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(pnd_Record_Written.getBoolean()))                                                                                                               //Natural: IF #RECORD-WRITTEN
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Prod_Annually().getValue(pnd_All_Indx_Pnd_B_Indx).equals(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx))  //Natural: IF #C-SAVE-PROD-ANNUALLY ( #B-INDX ) = ADI-DTL-CREF-ACCT-CD ( #A-INDX ) AND #C-SAVE-IA-RATE-ANNUALLY ( #B-INDX ) = ADI-DTL-CREF-IA-RATE-CD ( #A-INDX ) AND #C-SAVE-DA-RATE-ANNUALLY ( #B-INDX ) = ADI-DTL-CREF-DA-RATE-CD ( #A-INDX )
                && ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Rate_Annually().getValue(pnd_All_Indx_Pnd_B_Indx).equals(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_A_Indx)) 
                && ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Da_Rate_Annually().getValue(pnd_All_Indx_Pnd_B_Indx).equals(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Rate_Cd().getValue(pnd_All_Indx_Pnd_A_Indx))))
            {
                pnd_Cref_Rec_Found.setValue(true);                                                                                                                        //Natural: MOVE TRUE TO #CREF-REC-FOUND
            }                                                                                                                                                             //Natural: END-IF
            pnd_Record_Written.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #RECORD-WRITTEN
            if (condition(pnd_Cref_Rec_Found.getBoolean()))                                                                                                               //Natural: IF #CREF-REC-FOUND
            {
                pnd_Cref_Rec_Found.setValue(false);                                                                                                                       //Natural: MOVE FALSE TO #CREF-REC-FOUND
                ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Prod_Annually().getValue(pnd_All_Indx_Pnd_B_Indx).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: MOVE ADI-DTL-CREF-ACCT-CD ( #A-INDX ) TO #C-SAVE-PROD-ANNUALLY ( #B-INDX )
                ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Rate_Annually().getValue(pnd_All_Indx_Pnd_B_Indx).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: MOVE ADI-DTL-CREF-IA-RATE-CD ( #A-INDX ) TO #C-SAVE-IA-RATE-ANNUALLY ( #B-INDX )
                ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Da_Rate_Annually().getValue(pnd_All_Indx_Pnd_B_Indx).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Rate_Cd().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: MOVE ADI-DTL-CREF-DA-RATE-CD ( #A-INDX ) TO #C-SAVE-DA-RATE-ANNUALLY ( #B-INDX )
                ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Settled_Annually_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-ANNL-AMT ( #A-INDX ) TO #C-SAVE-SETTLED-ANNUALLY-AMT ( #B-INDX )
                ldaAdsl770.getPnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Settled_Annually_Final_Amt().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-ANNL-AMT ( #A-INDX ) TO #CREF-SETTLED-ANNUALLY-FINAL-AMT
                ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Units().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #C-SAVE-IA-ANNUALLY-UNITS ( #B-INDX )
                ldaAdsl770.getPnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Units().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #CREF-IA-ANNUALLY-FINAL-UNITS
                ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Payments().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #C-SAVE-IA-ANNUALLY-PAYMENTS ( #B-INDX )
                ldaAdsl770.getPnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Payments().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #CREF-IA-ANNUALLY-FINAL-PAYMENTS
                ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Unit_Val().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Annl_Unit_Val().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #A-INDX ) TO #C-SAVE-IA-ANNUALLY-UNIT-VAL ( #B-INDX )
                ldaAdsl770.getPnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Unit_Val().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Annl_Unit_Val().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #A-INDX ) TO #CREF-IA-ANNUALLY-FINAL-UNIT-VAL
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_All_Indx_Pnd_Cref_Indx_4.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CREF-INDX-4
                ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Prod_Annually().getValue(pnd_All_Indx_Pnd_Cref_Indx_4).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: MOVE ADI-DTL-CREF-ACCT-CD ( #A-INDX ) TO #C-SAVE-PROD-ANNUALLY ( #CREF-INDX-4 )
                ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Rate_Annually().getValue(pnd_All_Indx_Pnd_Cref_Indx_4).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: MOVE ADI-DTL-CREF-IA-RATE-CD ( #A-INDX ) TO #C-SAVE-IA-RATE-ANNUALLY ( #CREF-INDX-4 )
                ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Da_Rate_Annually().getValue(pnd_All_Indx_Pnd_Cref_Indx_4).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Rate_Cd().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: MOVE ADI-DTL-CREF-DA-RATE-CD ( #A-INDX ) TO #C-SAVE-DA-RATE-ANNUALLY ( #CREF-INDX-4 )
                ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Settled_Annually_Amt().getValue(pnd_All_Indx_Pnd_Cref_Indx_4).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-ANNL-AMT ( #A-INDX ) TO #C-SAVE-SETTLED-ANNUALLY-AMT ( #CREF-INDX-4 )
                ldaAdsl770.getPnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Settled_Annually_Final_Amt().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-ANNL-AMT ( #A-INDX ) TO #CREF-SETTLED-ANNUALLY-FINAL-AMT
                ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Units().getValue(pnd_All_Indx_Pnd_Cref_Indx_4).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #C-SAVE-IA-ANNUALLY-UNITS ( #CREF-INDX-4 )
                ldaAdsl770.getPnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Units().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #CREF-IA-ANNUALLY-FINAL-UNITS
                ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Payments().getValue(pnd_All_Indx_Pnd_Cref_Indx_4).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #C-SAVE-IA-ANNUALLY-PAYMENTS ( #CREF-INDX-4 )
                ldaAdsl770.getPnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Payments().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #CREF-IA-ANNUALLY-FINAL-PAYMENTS
                ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Unit_Val().getValue(pnd_All_Indx_Pnd_Cref_Indx_4).nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Annl_Unit_Val().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #A-INDX ) TO #C-SAVE-IA-ANNUALLY-UNIT-VAL ( #CREF-INDX-4 )
                ldaAdsl770.getPnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Unit_Val().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Annl_Unit_Val().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #A-INDX ) TO #CREF-IA-ANNUALLY-FINAL-UNIT-VAL
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Cref_Detail_Line() throws Exception                                                                                                            //Natural: WRITE-CREF-DETAIL-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //*  082012
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm871a.class));                                                                              //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM871A'
        //* *WRITE   (1) NOTITLE USING FORM 'ADSM874'   /* 082012 - ALWAYS DISPLAYS
        //*  GG050608           ZEROS SO IT IS USELESS.
        if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                                //Natural: IF #ROTH-INDICATOR-ON
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            //*  082012
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm871a.class));                                                                          //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM871A'
            //* *WRITE   (2) NOTITLE USING FORM 'ADSM874'   /* 082012
        }                                                                                                                                                                 //Natural: END-IF
        //*  082012 - COMMENTED OUT THE FOLLOWING SINCE ALL ZEROS ARE PRINTED
        //* *FOR #B-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
        //* *  IF ADC-ACCT-CDE (#B-INDX) = 'R' OR
        //* *      ADC-ACCT-CDE (#B-INDX) = 'D'        /* RS0
        //* *    MOVE ADC-ACCT-QTY (#B-INDX) TO
        //* *      #CREF-HEADER-SETTLE-AMT
        //* *    MOVE ADC-ACCT-TYP (#B-INDX) TO
        //* *      #CREF-HEADER-SETTLE-TYP
        //* *    MOVE ADC-GRD-MNTHLY-TYP (#B-INDX) TO
        //* *      #CREF-HEADER-GRD-TYP
        //* *    MOVE ADC-GRD-MNTHLY-QTY (#B-INDX) TO
        //* *      #CREF-HEADER-GRD-AMT
        //* *    IF ADC-GRD-MNTHLY-ACTL-AMT (#B-INDX) = 0
        //* *      MOVE 0 TO #CREF-HEADER-CREF-MNTHLY-UNITS
        //* *    END-IF
        //* *    EXAMINE  #PEC-ACCT-NAME-1 (*) FOR
        //* *      ADC-ACCT-CDE (#B-INDX) GIVING INDEX  #PROD-INDX
        //* *    MOVE #PEC-ACCT-NAME-4 (#PROD-INDX) TO #CREF-HEADER-ACCOUNT
        //* *    WRITE (1) NOTITLE USING FORM 'ADSM860'
        //* *    IF #ROTH-INDICATOR-ON                             /* GG050608
        //* *      WRITE (2) NOTITLE USING FORM 'ADSM860'
        //* *    END-IF
        //* *  END-IF
        //* *END-FOR
        //*  082012 END
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm863.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM863'
        //*  GG050608
        if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                                //Natural: IF #ROTH-INDICATOR-ON
        {
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm863.class));                                                                           //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM863'
        }                                                                                                                                                                 //Natural: END-IF
        FOR07:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO 80
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(80)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Method().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                //Natural: IF #C-SAVE-DA-METHOD ( #B-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl770.getPnd_Cref_Minor_Amt_Totals_Pnd_Cref_Prod().setValue(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Prod().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-CREF-PROD ( #B-INDX ) TO #CREF-PROD
            ldaAdsl770.getPnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Rate().setValue(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Rate().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-DA-RATE ( #B-INDX ) TO #CREF-DA-RATE
            ldaAdsl770.getPnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Method().setValue(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Method().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-DA-METHOD ( #B-INDX ) TO #CREF-DA-METHOD
            ldaAdsl770.getPnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Accum_Amt().setValue(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Accum_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-DA-ACCUM-AMT ( #B-INDX ) TO #CREF-DA-ACCUM-AMT
            ldaAdsl770.getPnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Amt().setValue(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-SETTLED-AMT ( #B-INDX ) TO #CREF-SETTLED-AMT
            ldaAdsl770.getPnd_Cref_Minor_Amt_Totals_Pnd_Cref_Rtb_Amt().setValue(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Rtb_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-RTB-AMT ( #B-INDX ) TO #CREF-RTB-AMT
            ldaAdsl770.getPnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Rtb_Amt().setValue(ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Rtb_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-SETTLED-RTB-AMT ( #B-INDX ) TO #CREF-SETTLED-RTB-AMT
            ldaAdsl770.getPnd_Cref_Minor_Unit_Totals_Pnd_Cref_Da_Accum_Units().setValue(ldaAdsl770.getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Da_Accum_Units().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-DA-ACCUM-UNITS ( #B-INDX ) TO #CREF-DA-ACCUM-UNITS
            ldaAdsl770.getPnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Units().setValue(ldaAdsl770.getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Settled_Units().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-SETTLED-UNITS ( #B-INDX ) TO #CREF-SETTLED-UNITS
            ldaAdsl770.getPnd_Cref_Minor_Unit_Totals_Pnd_Cref_Rtb_Units().setValue(ldaAdsl770.getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Rtb_Units().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-RTB-UNITS ( #B-INDX ) TO #CREF-RTB-UNITS
            ldaAdsl770.getPnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Rtb_Units().setValue(ldaAdsl770.getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Settled_Rtb_Units().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-SETTLED-RTB-UNITS ( #B-INDX ) TO #CREF-SETTLED-RTB-UNITS
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm878.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM878'
            //*  GG050608
            if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                            //Natural: IF #ROTH-INDICATOR-ON
            {
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm878.class));                                                                       //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM878'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Amount.setValue(ldaAdsl770.getPnd_Cref_Sub_Total_Pnd_Cref_Sub_Da_Accum_Total());                                                                              //Natural: ASSIGN #AMOUNT := #CREF-SUB-TOTAL.#CREF-SUB-DA-ACCUM-TOTAL
                                                                                                                                                                          //Natural: PERFORM PUT-ASTERISK-BESIDE-TOTAL
        sub_Put_Asterisk_Beside_Total();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm864.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM864'
        //*  GG050608
        if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                                //Natural: IF #ROTH-INDICATOR-ON
        {
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm864.class));                                                                           //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM864'
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Cref_Total_Settlement_Line() throws Exception                                                                                                  //Natural: WRITE-CREF-TOTAL-SETTLEMENT-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm975.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM975'
        //*  GG050608
        if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                                //Natural: IF #ROTH-INDICATOR-ON
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm975.class));                                                                           //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM975'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR08:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO 80
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(80)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Rate().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                            //Natural: IF #C-SAVE-DA-FINAL-RATE ( #B-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl770.getPnd_Cref_Final_Total_Pnd_Cref_Final_Da_Rate().setValue(ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Rate().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-DA-FINAL-RATE ( #B-INDX ) TO #CREF-FINAL-DA-RATE
            ldaAdsl770.getPnd_Cref_Final_Total_Pnd_Cref_Final_Prod().setValue(ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Cref_Final_Prod().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-CREF-FINAL-PROD ( #B-INDX ) TO #CREF-FINAL-PROD
            ldaAdsl770.getPnd_Cref_Final_Total_Pnd_Cref_Final_Da_Method().setValue(ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Method().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-DA-FINAL-METHOD ( #B-INDX ) TO #CREF-FINAL-DA-METHOD
            ldaAdsl770.getPnd_Cref_Final_Total_Pnd_Cref_Da_Accum_Final_Total().setValue(ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Accum_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-DA-FINAL-ACCUM-AMT ( #B-INDX ) TO #CREF-DA-ACCUM-FINAL-TOTAL
            ldaAdsl770.getPnd_Cref_Final_Total_Pnd_Cref_Settled_Final_Total().setValue(ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Settled_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-FINAL-SETTLED-AMT ( #B-INDX ) TO #CREF-SETTLED-FINAL-TOTAL
            ldaAdsl770.getPnd_Cref_Final_Total_Pnd_Cref_Rtb_Final_Total().setValue(ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Rtb_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-FINAL-RTB-AMT ( #B-INDX ) TO #CREF-RTB-FINAL-TOTAL
            ldaAdsl770.getPnd_Cref_Final_Total_Pnd_Cref_Settled_Rtb_Final_Total().setValue(ldaAdsl770.getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Settled_Rtb_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-FINAL-SETTLED-RTB-AMT ( #B-INDX ) TO #CREF-SETTLED-RTB-FINAL-TOTAL
            ldaAdsl770.getPnd_Cref_Final_Units_Pnd_Cref_Da_Accum_Final_Units().setValue(ldaAdsl770.getPnd_C_Save_Final_Total_Units_Pnd_C_Save_Da_Final_Accum_Units().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-DA-FINAL-ACCUM-UNITS ( #B-INDX ) TO #CREF-DA-ACCUM-FINAL-UNITS
            ldaAdsl770.getPnd_Cref_Final_Units_Pnd_Cref_Settled_Final_Units().setValue(ldaAdsl770.getPnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Settled_Units().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-FINAL-SETTLED-UNITS ( #B-INDX ) TO #CREF-SETTLED-FINAL-UNITS
            ldaAdsl770.getPnd_Cref_Final_Units_Pnd_Cref_Rtb_Final_Units().setValue(ldaAdsl770.getPnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Rtb_Units().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-FINAL-RTB-UNITS ( #B-INDX ) TO #CREF-RTB-FINAL-UNITS
            ldaAdsl770.getPnd_Cref_Final_Units_Pnd_Cref_Settled_Rtb_Final_Units().setValue(ldaAdsl770.getPnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Settled_Rtb_Units().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-FINAL-SETTLED-RTB-UNITS ( #B-INDX ) TO #CREF-SETTLED-RTB-FINAL-UNITS
            pnd_New_Header_Page_3.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #NEW-HEADER-PAGE-3
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm879.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM879'
            //*  GG050608
            if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                            //Natural: IF #ROTH-INDICATOR-ON
            {
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm879.class));                                                                       //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM879'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Amount.setValue(ldaAdsl770.getPnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Da_Accum_Total());                                                                         //Natural: ASSIGN #AMOUNT := #CREF-TOT-SETTLEMENT.#CREF-TOT-DA-ACCUM-TOTAL
                                                                                                                                                                          //Natural: PERFORM PUT-ASTERISK-BESIDE-TOTAL
        sub_Put_Asterisk_Beside_Total();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm866.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM866'
        //*  GG050608
        if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                                //Natural: IF #ROTH-INDICATOR-ON
        {
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm866.class));                                                                           //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM866'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_New_Header_Page_3.setValue(false);                                                                                                                            //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-3
    }
    private void sub_Write_Cref_Monthly_Method() throws Exception                                                                                                         //Natural: WRITE-CREF-MONTHLY-METHOD
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR09:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO 1
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(1)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Settled_Monthly_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).notEquals(getZero())))        //Natural: IF #C-SAVE-SETTLED-MONTHLY-AMT ( #B-INDX ) NE 0
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm867.class));                                                                       //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM867'
                //*  GG050608
                if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                        //Natural: IF #ROTH-INDICATOR-ON
                {
                    getReports().newPage(new ReportSpecification(2));                                                                                                     //Natural: NEWPAGE ( 2 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm867.class));                                                                   //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM867'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR10:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO 80
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(80)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Settled_Monthly_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).equals(getZero())))           //Natural: IF #C-SAVE-SETTLED-MONTHLY-AMT ( #B-INDX ) = 0
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl770.getPnd_Cref_Monthly_Totals_Pnd_Cref_Prod_Monthly().setValue(ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Prod_Monthly().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-PROD-MONTHLY ( #B-INDX ) TO #CREF-PROD-MONTHLY
            ldaAdsl770.getPnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Rate_Monthly().setValue(ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Rate_Monthly().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-IA-RATE-MONTHLY ( #B-INDX ) TO #CREF-IA-RATE-MONTHLY
            ldaAdsl770.getPnd_Cref_Monthly_Totals_Pnd_Cref_Da_Rate_Monthly().setValue(ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Da_Rate_Monthly().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-DA-RATE-MONTHLY ( #B-INDX ) TO #CREF-DA-RATE-MONTHLY
            ldaAdsl770.getPnd_Cref_Monthly_Totals_Pnd_Cref_Settled_Monthly_Amt().setValue(ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Settled_Monthly_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-SETTLED-MONTHLY-AMT ( #B-INDX ) TO #CREF-SETTLED-MONTHLY-AMT
            ldaAdsl770.getPnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Units().setValue(ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Units().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-IA-MONTHLY-UNITS ( #B-INDX ) TO #CREF-IA-MONTHLY-UNITS
            ldaAdsl770.getPnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Payments().setValue(ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Payments().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-IA-MONTHLY-PAYMENTS ( #B-INDX ) TO #CREF-IA-MONTHLY-PAYMENTS
            ldaAdsl770.getPnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Unit_Val().setValue(ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Unit_Val().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-IA-MONTHLY-UNIT-VAL ( #B-INDX ) TO #CREF-IA-MONTHLY-UNIT-VAL
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm880.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM880'
            //*  GG050608
            if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                            //Natural: IF #ROTH-INDICATOR-ON
            {
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm880.class));                                                                       //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM880'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_All_Indx_Pnd_B_Indx.greater(1)))                                                                                                                //Natural: IF #B-INDX GT 1
        {
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm868.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM868'
            //*  GG050608
            if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                            //Natural: IF #ROTH-INDICATOR-ON
            {
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm868.class));                                                                       //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM868'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Cref_Annually_Method() throws Exception                                                                                                        //Natural: WRITE-CREF-ANNUALLY-METHOD
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR11:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO 1
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(1)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Settled_Annually_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).notEquals(getZero())))      //Natural: IF #C-SAVE-SETTLED-ANNUALLY-AMT ( #B-INDX ) NE 0
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm869.class));                                                                       //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM869'
                //*  GG050608
                if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                        //Natural: IF #ROTH-INDICATOR-ON
                {
                    getReports().newPage(new ReportSpecification(2));                                                                                                     //Natural: NEWPAGE ( 2 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm869.class));                                                                   //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM869'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR12:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO 80
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(80)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Settled_Annually_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).equals(getZero())))         //Natural: IF #C-SAVE-SETTLED-ANNUALLY-AMT ( #B-INDX ) = 0
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl770.getPnd_Cref_Annually_Method_Totals_Pnd_Cref_Prod_Annually().setValue(ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Prod_Annually().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-PROD-ANNUALLY ( #B-INDX ) TO #CREF-PROD-ANNUALLY
            ldaAdsl770.getPnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Rate_Annually().setValue(ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Rate_Annually().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-IA-RATE-ANNUALLY ( #B-INDX ) TO #CREF-IA-RATE-ANNUALLY
            ldaAdsl770.getPnd_Cref_Annually_Method_Totals_Pnd_Cref_Da_Rate_Annually().setValue(ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Da_Rate_Annually().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-DA-RATE-ANNUALLY ( #B-INDX ) TO #CREF-DA-RATE-ANNUALLY
            ldaAdsl770.getPnd_Cref_Annually_Method_Totals_Pnd_Cref_Settled_Annually_Amt().setValue(ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Settled_Annually_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-SETTLED-ANNUALLY-AMT ( #B-INDX ) TO #CREF-SETTLED-ANNUALLY-AMT
            ldaAdsl770.getPnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Units().setValue(ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Units().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-IA-ANNUALLY-UNITS ( #B-INDX ) TO #CREF-IA-ANNUALLY-UNITS
            ldaAdsl770.getPnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Payments().setValue(ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Payments().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-IA-ANNUALLY-PAYMENTS ( #B-INDX ) TO #CREF-IA-ANNUALLY-PAYMENTS
            ldaAdsl770.getPnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Unit_Val().setValue(ldaAdsl770.getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Unit_Val().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #C-SAVE-IA-ANNUALLY-UNIT-VAL ( #B-INDX ) TO #CREF-IA-ANNUALLY-UNIT-VAL
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm881.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM881'
            //*  GG050608
            if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                            //Natural: IF #ROTH-INDICATOR-ON
            {
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm881.class));                                                                       //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM881'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_All_Indx_Pnd_B_Indx.greater(1)))                                                                                                                //Natural: IF #B-INDX GT 1
        {
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm870.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM870'
            //*  GG050608
            if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                            //Natural: IF #ROTH-INDICATOR-ON
            {
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm870.class));                                                                       //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM870'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Previous_Annuitant_Total_Rtn() throws Exception                                                                                              //Natural: PROCESS-PREVIOUS-ANNUITANT-TOTAL-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Total_Cref_Nbr.greater(1)))                                                                                                                     //Natural: IF #TOTAL-CREF-NBR > 1
        {
            //*  GG050608
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                            //Natural: IF #ROTH-INDICATOR-ON
            {
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 2 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-CREF-TOTAL-SETTLEMENT-LINE
            sub_Write_Cref_Total_Settlement_Line();
            if (condition(Global.isEscape())) {return;}
            pnd_Total_Cref_Nbr.reset();                                                                                                                                   //Natural: RESET #TOTAL-CREF-NBR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Cref_Record.getBoolean()))                                                                                                                      //Natural: IF #CREF-RECORD
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-CREF-MONTHLY-METHOD
            sub_Write_Cref_Monthly_Method();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-CREF-ANNUALLY-METHOD
            sub_Write_Cref_Annually_Method();
            if (condition(Global.isEscape())) {return;}
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            //*  082012
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm882a.class));                                                                          //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM882A'
            //*  GG050608
            if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                            //Natural: IF #ROTH-INDICATOR-ON
            {
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 2 )
                if (condition(Global.isEscape())){return;}
                //*  082012
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm882a.class));                                                                      //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM882A'
            }                                                                                                                                                             //Natural: END-IF
            pnd_Total_Cref_Nbr.reset();                                                                                                                                   //Natural: RESET #TOTAL-CREF-NBR
        }                                                                                                                                                                 //Natural: END-IF
        pnd_All_Indx.reset();                                                                                                                                             //Natural: RESET #ALL-INDX
                                                                                                                                                                          //Natural: PERFORM RESET-CREF-DETAIL-TOTAL-SAVE-AREA
        sub_Reset_Cref_Detail_Total_Save_Area();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM RESET-CREF-FINAL-TOTAL-SAVE-AREA
        sub_Reset_Cref_Final_Total_Save_Area();
        if (condition(Global.isEscape())) {return;}
        pnd_Cref_Record.setValue(false);                                                                                                                                  //Natural: MOVE FALSE TO #CREF-RECORD
    }
    private void sub_Reset_Cref_Detail_Total_Save_Area() throws Exception                                                                                                 //Natural: RESET-CREF-DETAIL-TOTAL-SAVE-AREA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaAdsl770.getPnd_Cref_Minor_Amt_Totals().reset();                                                                                                                //Natural: RESET #CREF-MINOR-AMT-TOTALS #CREF-MINOR-UNIT-TOTALS #C-SAVE-MINOR-AMT-TOTALS #C-SAVE-MINOR-UNIT-TOTALS #CREF-HEADER-SETTLE-AMOUNTS #CREF-HEADER-AMT #CREF-SUB-TOTAL
        ldaAdsl770.getPnd_Cref_Minor_Unit_Totals().reset();
        ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals().reset();
        ldaAdsl770.getPnd_C_Save_Minor_Unit_Totals().reset();
        ldaAdsl770.getPnd_Cref_Header_Settle_Amounts().reset();
        ldaAdsl770.getPnd_Cref_Header_Amt().reset();
        ldaAdsl770.getPnd_Cref_Sub_Total().reset();
    }
    private void sub_Reset_Header_Totals() throws Exception                                                                                                               //Natural: RESET-HEADER-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaAdsl770.getPnd_Cref_Header_Settle_Amounts().reset();                                                                                                           //Natural: RESET #CREF-HEADER-SETTLE-AMOUNTS #CREF-HEADER-AMT #TARGET-NUMBER ( * )
        ldaAdsl770.getPnd_Cref_Header_Amt().reset();
        ldaAdsl761.getPnd_Target_All_Pnd_Target_Number().getValue("*").reset();
    }
    private void sub_Reset_Cref_Final_Total_Save_Area() throws Exception                                                                                                  //Natural: RESET-CREF-FINAL-TOTAL-SAVE-AREA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaAdsl770.getPnd_Cref_Final_Total().reset();                                                                                                                     //Natural: RESET #CREF-FINAL-TOTAL #CREF-FINAL-UNITS #C-SAVE-MINOR-AMT-TOTALS #C-SAVE-MINOR-UNIT-TOTALS #C-SAVE-MINOR-UNIT-TOTALS #C-SAVE-FINAL-TOTAL-AMTS #C-SAVE-FINAL-TOTAL-UNITS #CREF-MONTHLY-TOTALS #C-SAVE-MONTHLY-METHOD-TOTALS #CREF-MONTHLY-METHOD-FINAL-TOT #CREF-ANNUALLY-METHOD-TOTALS #C-SAVE-ANNUALLY-METHOD-TOTALS #CREF-ANNUALLY-METHOD-FINAL-TOT #CREF-TOT-SETTLEMENT #CREF-SUB-TOTAL
        ldaAdsl770.getPnd_Cref_Final_Units().reset();
        ldaAdsl770.getPnd_C_Save_Minor_Amt_Totals().reset();
        ldaAdsl770.getPnd_C_Save_Minor_Unit_Totals().reset();
        ldaAdsl770.getPnd_C_Save_Minor_Unit_Totals().reset();
        ldaAdsl770.getPnd_C_Save_Final_Total_Amts().reset();
        ldaAdsl770.getPnd_C_Save_Final_Total_Units().reset();
        ldaAdsl770.getPnd_Cref_Monthly_Totals().reset();
        ldaAdsl770.getPnd_C_Save_Monthly_Method_Totals().reset();
        ldaAdsl770.getPnd_Cref_Monthly_Method_Final_Tot().reset();
        ldaAdsl770.getPnd_Cref_Annually_Method_Totals().reset();
        ldaAdsl770.getPnd_C_Save_Annually_Method_Totals().reset();
        ldaAdsl770.getPnd_Cref_Annually_Method_Final_Tot().reset();
        ldaAdsl770.getPnd_Cref_Tot_Settlement().reset();
        ldaAdsl770.getPnd_Cref_Sub_Total().reset();
    }
    private void sub_Put_Asterisk_Beside_Total() throws Exception                                                                                                         //Natural: PUT-ASTERISK-BESIDE-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Asterisk.reset();                                                                                                                                             //Natural: RESET #ASTERISK #COMMENT1 #COMMENT2
        pnd_Comment1.reset();
        pnd_Comment2.reset();
        if (condition(pnd_Amount.greater(getZero())))                                                                                                                     //Natural: IF #AMOUNT GT 0
        {
            pnd_Asterisk.setValue("*");                                                                                                                                   //Natural: ASSIGN #ASTERISK := '*'
            pnd_Comment1.setValue("* Opening balance amount may not reflect any financial transactions processed");                                                       //Natural: ASSIGN #COMMENT1 := '* Opening balance amount may not reflect any financial transactions processed'
            pnd_Comment2.setValue("  for this contract(s) prior to the T395.");                                                                                           //Natural: ASSIGN #COMMENT2 := '  for this contract(s) prior to the T395.'
        }                                                                                                                                                                 //Natural: END-IF
        //*  PUT-ASTERISK-BESIDE-TOTAL
    }
    private void sub_End_Of_Job_Rtn() throws Exception                                                                                                                    //Natural: END-OF-JOB-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(ldaAdsl770.getPnd_Cref_Rec_Cnt().equals(getZero())))                                                                                                //Natural: IF #CREF-REC-CNT = 0
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE," ");                                                                                                              //Natural: WRITE ( 1 ) ' '
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"**********************************************************************");                                         //Natural: WRITE ( 1 ) '**********************************************************************'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"* ADSP765 - ANNUITIZATION OF OMNIPLUS CONTRACTS REPORT - JOB RUN ENDED");                                         //Natural: WRITE ( 1 ) '* ADSP765 - ANNUITIZATION OF OMNIPLUS CONTRACTS REPORT - JOB RUN ENDED'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"* NO REAL ESTATE TRANSACTIONS PROCESSED TODAY");                                                                  //Natural: WRITE ( 1 ) '* NO REAL ESTATE TRANSACTIONS PROCESSED TODAY'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"* TOTAL RECORDS EXTRACTED FROM ADSP765 =",ldaAdsl770.getPnd_Cref_Rec_Cnt());                                      //Natural: WRITE ( 1 ) '* TOTAL RECORDS EXTRACTED FROM ADSP765 =' #CREF-REC-CNT
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"**********************************************************************");                                         //Natural: WRITE ( 1 ) '**********************************************************************'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  GG050608
        if (condition(ldaAdsl770.getPnd_Cref_Rec_Cnt_Roth().equals(getZero())))                                                                                           //Natural: IF #CREF-REC-CNT-ROTH = 0
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().write(2, ReportOption.NOTITLE," ");                                                                                                              //Natural: WRITE ( 2 ) ' '
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"**********************************************************************");                                         //Natural: WRITE ( 2 ) '**********************************************************************'
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"* ADSP765 - ANNUITIZATION OF OMNIPLUS CONTRACTS REPORT - JOB RUN ENDED");                                         //Natural: WRITE ( 2 ) '* ADSP765 - ANNUITIZATION OF OMNIPLUS CONTRACTS REPORT - JOB RUN ENDED'
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"* NO ROTH TRANSACTIONS PROCESSED TODAY");                                                                         //Natural: WRITE ( 2 ) '* NO ROTH TRANSACTIONS PROCESSED TODAY'
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"* TOTAL ROTH RECORDS EXTRACTED FROM ADSP765 =",ldaAdsl770.getPnd_Cref_Rec_Cnt_Roth());                            //Natural: WRITE ( 2 ) '* TOTAL ROTH RECORDS EXTRACTED FROM ADSP765 =' #CREF-REC-CNT-ROTH
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"**********************************************************************");                                         //Natural: WRITE ( 2 ) '**********************************************************************'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  GG050608
                    //*  RS0
                    ldaAdsl761.getPnd_Page_Number().nadd(1);                                                                                                              //Natural: ADD 1 TO #PAGE-NUMBER
                    pnd_Page_Number_Map.setValue(ldaAdsl761.getPnd_Page_Number());                                                                                        //Natural: ASSIGN #PAGE-NUMBER-MAP := #PAGE-NUMBER
                    //*  #HEADING := '       Real Estate Settlement        '
                    pnd_Heading.setValue("      Real Estate/Access Settlement         ");                                                                                 //Natural: ASSIGN #HEADING := '      Real Estate/Access Settlement         '
                    getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm862.class));                                                                   //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM862'
                    if (condition(pnd_New_Header_Page_3.getBoolean()))                                                                                                    //Natural: IF #NEW-HEADER-PAGE-3
                    {
                        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm975.class));                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM975'
                        if (condition(! (pnd_Roth_Indicator_On.getBoolean())))                                                                                            //Natural: IF NOT #ROTH-INDICATOR-ON
                        {
                            pnd_New_Header_Page_3.setValue(false);                                                                                                        //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-3
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  GG050608
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  RS0
                    ldaAdsl761.getPnd_Page_Number_Roth().nadd(1);                                                                                                         //Natural: ADD 1 TO #PAGE-NUMBER-ROTH
                    pnd_Page_Number_Map.setValue(ldaAdsl761.getPnd_Page_Number_Roth());                                                                                   //Natural: ASSIGN #PAGE-NUMBER-MAP := #PAGE-NUMBER-ROTH
                    //*  #HEADING := 'Real Estate Roth 403b/401k Settlement'
                    pnd_Heading.setValue("Real Estate/Access Roth 403b/401k Settlement");                                                                                 //Natural: ASSIGN #HEADING := 'Real Estate/Access Roth 403b/401k Settlement'
                    getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm862.class));                                                                   //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM862'
                    if (condition(pnd_New_Header_Page_3.getBoolean()))                                                                                                    //Natural: IF #NEW-HEADER-PAGE-3
                    {
                        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm975.class));                                                               //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM975'
                        pnd_New_Header_Page_3.setValue(false);                                                                                                            //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-3
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=90 PS=60");
        Global.format(1, "LS=90 PS=60");
        Global.format(2, "LS=90 PS=60");
        Global.format(3, "LS=90 PS=60");
    }
}
