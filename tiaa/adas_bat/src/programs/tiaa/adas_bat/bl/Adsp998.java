/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:06:10 PM
**        * FROM NATURAL PROGRAM : Adsp998
************************************************************
**        * FILE NAME            : Adsp998.java
**        * CLASS NAME           : Adsp998
**        * INSTANCE NAME        : Adsp998
************************************************************
************************************************************************
* PROGRAM  : ADSP998
* GENERATED: MAY 10, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : EXTRACT FOR RATE BASED SUMMARY REPORT
*
* THIS MODULE EXTRACTS ALL THE COMPLETED (CLOSED) ANNUITIZATION REQUESTS
* FROM PARTICIPANT FILE, UP TO THE CURRENT BUSINESS DATE.
************************  MAINTENANCE LOG ******************************
*  D A T E   PROGRAMMER     D E S C R I P T I O N
* 05/10/04   C. AVE         CREATED
* 07/27/04   T. SHINE       IF MONTH-END PROCESS FALLS ON WEEKEND OR
*                           HOLIDAY, USE ADS-RPT-13 (FACTOR DATE) AS
*                           REPORT DATE.
* 11/28/06   N. CVETKOVIC   MODIFY TO ADD SUBPRODUCT CODE TO FILE
* 03/17/2008 E.MELNIK   ROTH 403B/401K CHANGES.  RECOMPILED FOR
*                       NEW ADSL401/ADSL401A.
* 05/06/2008 G.GUERRERO     ADDED AND SET ROTH INDICATOR IN OUPUTTED
*                           WORK FILE.
* 11/26/2008 R.SACHARNY     EXCLUDE "TIAA Stable Return" FUND FROM CREF
*                           FUNDS PROCESSING AND ADD NEW SORT CODE (RS0)
* 03/30/2010 D.E.ANDER  ADABAS REPLATFORM PCPOP - ADD SURVIVOR INFORMA
*                           TION AND ASSOCIATED CODE    MARKED DEA
* 03/07/12   E. MELNIK RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                      UPDATED LINKAGE AREAS.  CHANGES MARKED BY
*                      EM - 030712.
* 01/18/13   E. MELNIK TNG PROJECT.  RESTOW TO INCLUDE UPDATED LINKAGE
*                      AREAS.
* 10/07/13   E. MELNIK CREF/REA REDESIGN CHANGES.
*                      MARKED BY EM - 100713.
* 02/24/2017 R. CARREON RESTOWED FOR PIN EXPANSION
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp998 extends BLNatBase
{
    // Data Areas
    private LdaAdsl401a ldaAdsl401a;
    private LdaAdsl401 ldaAdsl401;
    private PdaNeca4000 pdaNeca4000;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ads_Cntl_View;

    private DbsGroup ads_Cntl_View_Ads_Cntl_Grp;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte;

    private DbsGroup ads_Cntl_View__R_Field_1;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A;
    private DbsField ads_Cntl_View_Ads_Rpt_13;
    private DbsField pnd_Start_Date;

    private DbsGroup pnd_Start_Date__R_Field_2;
    private DbsField pnd_Start_Date_Pnd_Start_Date_A;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_3;
    private DbsField pnd_Date_Pnd_Date_N;
    private DbsField pnd_Record_Count;
    private DbsField pnd_Work_Count;
    private DbsField pnd_Rec_Complete_Cnt;
    private DbsField pnd_Rec_Delayed_Cnt;
    private DbsField pnd_Rec_Incmplt_Cnt;
    private DbsField pnd_Rec_Rejected_Cnt;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Ra_Mdo;
    private DbsField pnd_Sra_Mdo;
    private DbsField pnd_Gsra_Mdo;
    private DbsField pnd_Last_Activity_Date;

    private DbsGroup pnd_Last_Activity_Date__R_Field_4;
    private DbsField pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N;
    private DbsField pnd_Key_Cntl_Bsnss_Dte;
    private DbsField pnd_Adp_Sd2;

    private DbsGroup pnd_Adp_Sd2__R_Field_5;
    private DbsField pnd_Adp_Sd2_Pnd_Sd2_Opn_Clsd_Ind;
    private DbsField pnd_Adp_Sd2_Pnd_Sd2_Stts_Cde;
    private DbsField pnd_Work_File;

    private DbsGroup pnd_Work_File__R_Field_6;
    private DbsField pnd_Work_File_Pnd_Wk_Lst_Actvty_Dte;
    private DbsField pnd_Work_File_Pnd_Wk_Stts;
    private DbsField pnd_Work_File_Pnd_Wk_Acct;
    private DbsField pnd_Work_File_Pnd_Wk_Lob;
    private DbsField pnd_Work_File_Pnd_Wk_Sub_Seq;
    private DbsField pnd_Work_File_Pnd_Wk_Sub_Lob;
    private DbsField pnd_Work_File_Pnd_Wk_Rate;
    private DbsField pnd_Work_File_Pnd_Wk_Actl_Amt;
    private DbsField pnd_Work_File_Pnd_Wk_Roth_Ind;
    private DbsField pnd_Work_File_Pnd_Wk_Survivor_Ind;
    private DbsField pnd_Stbl_Fund;
    private DbsField pnd_Sv_Fund;
    private DbsField pnd_Max_Rates;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl401a = new LdaAdsl401a();
        registerRecord(ldaAdsl401a);
        registerRecord(ldaAdsl401a.getVw_ads_Cntrct_View());
        ldaAdsl401 = new LdaAdsl401();
        registerRecord(ldaAdsl401);
        registerRecord(ldaAdsl401.getVw_ads_Prtcpnt_View());
        localVariables = new DbsRecord();
        pdaNeca4000 = new PdaNeca4000(localVariables);

        // Local Variables

        vw_ads_Cntl_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntl_View", "ADS-CNTL-VIEW"), "ADS_CNTL", "ADS_CNTL");

        ads_Cntl_View_Ads_Cntl_Grp = vw_ads_Cntl_View.getRecord().newGroupInGroup("ADS_CNTL_VIEW_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_DTE");

        ads_Cntl_View__R_Field_1 = ads_Cntl_View_Ads_Cntl_Grp.newGroupInGroup("ads_Cntl_View__R_Field_1", "REDEFINE", ads_Cntl_View_Ads_Cntl_Bsnss_Dte);
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A = ads_Cntl_View__R_Field_1.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A", "ADS-CNTL-BSNSS-DTE-A", FieldType.STRING, 
            8);
        ads_Cntl_View_Ads_Rpt_13 = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Rpt_13", "ADS-RPT-13", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADS_RPT_13");
        registerRecord(vw_ads_Cntl_View);

        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.NUMERIC, 8);

        pnd_Start_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Start_Date__R_Field_2", "REDEFINE", pnd_Start_Date);
        pnd_Start_Date_Pnd_Start_Date_A = pnd_Start_Date__R_Field_2.newFieldInGroup("pnd_Start_Date_Pnd_Start_Date_A", "#START-DATE-A", FieldType.STRING, 
            8);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 8);

        pnd_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Date__R_Field_3", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_N = pnd_Date__R_Field_3.newFieldInGroup("pnd_Date_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        pnd_Record_Count = localVariables.newFieldInRecord("pnd_Record_Count", "#RECORD-COUNT", FieldType.NUMERIC, 12);
        pnd_Work_Count = localVariables.newFieldInRecord("pnd_Work_Count", "#WORK-COUNT", FieldType.NUMERIC, 12);
        pnd_Rec_Complete_Cnt = localVariables.newFieldInRecord("pnd_Rec_Complete_Cnt", "#REC-COMPLETE-CNT", FieldType.NUMERIC, 9);
        pnd_Rec_Delayed_Cnt = localVariables.newFieldInRecord("pnd_Rec_Delayed_Cnt", "#REC-DELAYED-CNT", FieldType.NUMERIC, 9);
        pnd_Rec_Incmplt_Cnt = localVariables.newFieldInRecord("pnd_Rec_Incmplt_Cnt", "#REC-INCMPLT-CNT", FieldType.NUMERIC, 9);
        pnd_Rec_Rejected_Cnt = localVariables.newFieldInRecord("pnd_Rec_Rejected_Cnt", "#REC-REJECTED-CNT", FieldType.NUMERIC, 9);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_Ra_Mdo = localVariables.newFieldInRecord("pnd_Ra_Mdo", "#RA-MDO", FieldType.BOOLEAN, 1);
        pnd_Sra_Mdo = localVariables.newFieldInRecord("pnd_Sra_Mdo", "#SRA-MDO", FieldType.BOOLEAN, 1);
        pnd_Gsra_Mdo = localVariables.newFieldInRecord("pnd_Gsra_Mdo", "#GSRA-MDO", FieldType.BOOLEAN, 1);
        pnd_Last_Activity_Date = localVariables.newFieldInRecord("pnd_Last_Activity_Date", "#LAST-ACTIVITY-DATE", FieldType.STRING, 8);

        pnd_Last_Activity_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Last_Activity_Date__R_Field_4", "REDEFINE", pnd_Last_Activity_Date);
        pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N = pnd_Last_Activity_Date__R_Field_4.newFieldInGroup("pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N", 
            "#LAST-ACTIVITY-DATE-N", FieldType.NUMERIC, 8);
        pnd_Key_Cntl_Bsnss_Dte = localVariables.newFieldInRecord("pnd_Key_Cntl_Bsnss_Dte", "#KEY-CNTL-BSNSS-DTE", FieldType.DATE);
        pnd_Adp_Sd2 = localVariables.newFieldInRecord("pnd_Adp_Sd2", "#ADP-SD2", FieldType.STRING, 4);

        pnd_Adp_Sd2__R_Field_5 = localVariables.newGroupInRecord("pnd_Adp_Sd2__R_Field_5", "REDEFINE", pnd_Adp_Sd2);
        pnd_Adp_Sd2_Pnd_Sd2_Opn_Clsd_Ind = pnd_Adp_Sd2__R_Field_5.newFieldInGroup("pnd_Adp_Sd2_Pnd_Sd2_Opn_Clsd_Ind", "#SD2-OPN-CLSD-IND", FieldType.STRING, 
            1);
        pnd_Adp_Sd2_Pnd_Sd2_Stts_Cde = pnd_Adp_Sd2__R_Field_5.newFieldInGroup("pnd_Adp_Sd2_Pnd_Sd2_Stts_Cde", "#SD2-STTS-CDE", FieldType.STRING, 3);
        pnd_Work_File = localVariables.newFieldInRecord("pnd_Work_File", "#WORK-FILE", FieldType.STRING, 200);

        pnd_Work_File__R_Field_6 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_6", "REDEFINE", pnd_Work_File);
        pnd_Work_File_Pnd_Wk_Lst_Actvty_Dte = pnd_Work_File__R_Field_6.newFieldInGroup("pnd_Work_File_Pnd_Wk_Lst_Actvty_Dte", "#WK-LST-ACTVTY-DTE", FieldType.STRING, 
            8);
        pnd_Work_File_Pnd_Wk_Stts = pnd_Work_File__R_Field_6.newFieldInGroup("pnd_Work_File_Pnd_Wk_Stts", "#WK-STTS", FieldType.STRING, 1);
        pnd_Work_File_Pnd_Wk_Acct = pnd_Work_File__R_Field_6.newFieldInGroup("pnd_Work_File_Pnd_Wk_Acct", "#WK-ACCT", FieldType.STRING, 1);
        pnd_Work_File_Pnd_Wk_Lob = pnd_Work_File__R_Field_6.newFieldInGroup("pnd_Work_File_Pnd_Wk_Lob", "#WK-LOB", FieldType.STRING, 10);
        pnd_Work_File_Pnd_Wk_Sub_Seq = pnd_Work_File__R_Field_6.newFieldInGroup("pnd_Work_File_Pnd_Wk_Sub_Seq", "#WK-SUB-SEQ", FieldType.STRING, 2);
        pnd_Work_File_Pnd_Wk_Sub_Lob = pnd_Work_File__R_Field_6.newFieldInGroup("pnd_Work_File_Pnd_Wk_Sub_Lob", "#WK-SUB-LOB", FieldType.STRING, 30);
        pnd_Work_File_Pnd_Wk_Rate = pnd_Work_File__R_Field_6.newFieldInGroup("pnd_Work_File_Pnd_Wk_Rate", "#WK-RATE", FieldType.STRING, 2);
        pnd_Work_File_Pnd_Wk_Actl_Amt = pnd_Work_File__R_Field_6.newFieldInGroup("pnd_Work_File_Pnd_Wk_Actl_Amt", "#WK-ACTL-AMT", FieldType.NUMERIC, 11, 
            2);
        pnd_Work_File_Pnd_Wk_Roth_Ind = pnd_Work_File__R_Field_6.newFieldInGroup("pnd_Work_File_Pnd_Wk_Roth_Ind", "#WK-ROTH-IND", FieldType.STRING, 1);
        pnd_Work_File_Pnd_Wk_Survivor_Ind = pnd_Work_File__R_Field_6.newFieldInGroup("pnd_Work_File_Pnd_Wk_Survivor_Ind", "#WK-SURVIVOR-IND", FieldType.STRING, 
            1);
        pnd_Stbl_Fund = localVariables.newFieldInRecord("pnd_Stbl_Fund", "#STBL-FUND", FieldType.BOOLEAN, 1);
        pnd_Sv_Fund = localVariables.newFieldInRecord("pnd_Sv_Fund", "#SV-FUND", FieldType.BOOLEAN, 1);
        pnd_Max_Rates = localVariables.newFieldInRecord("pnd_Max_Rates", "#MAX-RATES", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Cntl_View.reset();

        ldaAdsl401a.initializeValues();
        ldaAdsl401.initializeValues();

        localVariables.reset();
        pnd_Max_Rates.setInitialValue(250);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp998() throws Exception
    {
        super("Adsp998");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 132
        vw_ads_Cntl_View.startDatabaseRead                                                                                                                                //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "READ01",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        READ01:
        while (condition(vw_ads_Cntl_View.readNextRow("READ01")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                                     //Natural: IF ADS-RPT-13 NE 0
        {
            pnd_Date.setValueEdited(ads_Cntl_View_Ads_Rpt_13,new ReportEditMask("YYYYMMDD"));                                                                             //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE
            pnd_Start_Date.compute(new ComputeParameters(false, pnd_Start_Date), (pnd_Date_Pnd_Date_N.divide(100)).multiply(100));                                        //Natural: COMPUTE #START-DATE = ( #DATE-N / 100 ) * 100
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Start_Date.compute(new ComputeParameters(false, pnd_Start_Date), (ads_Cntl_View_Ads_Cntl_Bsnss_Dte.divide(100)).multiply(100));                           //Natural: COMPUTE #START-DATE = ( ADS-CNTL-BSNSS-DTE / 100 ) * 100
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "START DATE -",pnd_Start_Date);                                                                                                             //Natural: WRITE 'START DATE -' #START-DATE
        if (Global.isEscape()) return;
        getReports().write(0, "END DATE   -",ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                                           //Natural: WRITE 'END DATE   -' ADS-CNTL-BSNSS-DTE
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM EXTRACT-COMPLETED-TRANS
        sub_Extract_Completed_Trans();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM EXTRACT-DELAY-INCOMPLETE-TRANS
        sub_Extract_Delay_Incomplete_Trans();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM EXTRACT-REJECTED-TRANS
        sub_Extract_Rejected_Trans();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, NEWLINE,"PARTICIPANT RECORD COUNT :",NEWLINE,"   COMPLETE   : ",pnd_Rec_Complete_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"   DELAYED    : ",pnd_Rec_Delayed_Cnt,  //Natural: WRITE / 'PARTICIPANT RECORD COUNT :' / '   COMPLETE   : ' #REC-COMPLETE-CNT ( EM = ZZZ,ZZZ,ZZ9 ) / '   DELAYED    : ' #REC-DELAYED-CNT ( EM = ZZZ,ZZZ,ZZ9 ) / '   INCOMPLETE : ' #REC-INCMPLT-CNT ( EM = ZZZ,ZZZ,ZZ9 ) / '   REJECTED   : ' #REC-REJECTED-CNT ( EM = ZZZ,ZZZ,ZZ9 ) / 'TOTAL RECORDS WRITTEN TO OUTPUT -' #WORK-COUNT
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"   INCOMPLETE : ",pnd_Rec_Incmplt_Cnt, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"   REJECTED   : ",pnd_Rec_Rejected_Cnt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,"TOTAL RECORDS WRITTEN TO OUTPUT -",pnd_Work_Count);
        if (Global.isEscape()) return;
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXTRACT-COMPLETED-TRANS
        //* ***********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXTRACT-DELAY-INCOMPLETE-TRANS
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EXTRACT-REJECTED-TRANS
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CONTRACT-RECORD
        //*            #WK-SUB-LOB := 'CLASSIC'
        //*          VALUE 'SIP'
        //*            #WK-SUB-LOB := 'SIP'
        //*            #WK-SUB-SEQ := '04'
        //*            #WK-SUB-LOB := 'IRA'
        //*        WRITE 'No sub LOB for product' NECA4000.PRD-PRODUCT-CDE(1)
        //*      #WK-ACCT := 'C'
        //* *    #WK-ACTL-UNT := ADC-ACCT-ACTL-NBR-UNITS (#I)
        //*      #WK-ACCT := 'A'
    }
    private void sub_Extract_Completed_Trans() throws Exception                                                                                                           //Natural: EXTRACT-COMPLETED-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                                     //Natural: IF ADS-RPT-13 NE 0
        {
            pnd_Key_Cntl_Bsnss_Dte.setValue(ads_Cntl_View_Ads_Rpt_13);                                                                                                    //Natural: MOVE ADS-RPT-13 TO #KEY-CNTL-BSNSS-DTE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Key_Cntl_Bsnss_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A);                                                     //Natural: MOVE EDITED ADS-CNTL-BSNSS-DTE-A TO #KEY-CNTL-BSNSS-DTE ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl401.getVw_ads_Prtcpnt_View().startDatabaseRead                                                                                                             //Natural: READ ADS-PRTCPNT-VIEW BY ADP-LST-ACTVTY-DTE STARTING FROM #KEY-CNTL-BSNSS-DTE
        (
        "PND_PND_L1410",
        new Wc[] { new Wc("ADP_LST_ACTVTY_DTE", ">=", pnd_Key_Cntl_Bsnss_Dte, WcType.BY) },
        new Oc[] { new Oc("ADP_LST_ACTVTY_DTE", "ASC") }
        );
        PND_PND_L1410:
        while (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().readNextRow("PND_PND_L1410")))
        {
            pnd_Last_Activity_Date.setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                                    //Natural: MOVE EDITED ADP-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #LAST-ACTIVITY-DATE
            if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                                 //Natural: IF ADS-RPT-13 NE 0
            {
                if (condition(pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N.greater(pnd_Date_Pnd_Date_N)))                                                              //Natural: IF #LAST-ACTIVITY-DATE-N > #DATE-N
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N.greater(ads_Cntl_View_Ads_Cntl_Bsnss_Dte)))                                                 //Natural: IF #LAST-ACTIVITY-DATE-N > ADS-CNTL-BSNSS-DTE
                {
                    if (true) break PND_PND_L1410;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L1410. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,1).equals("T"))))                                                                //Natural: ACCEPT IF SUBSTR ( ADP-STTS-CDE,1,1 ) = 'T'
            {
                continue;
            }
                                                                                                                                                                          //Natural: PERFORM READ-CONTRACT-RECORD
            sub_Read_Contract_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1410"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1410"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  EXTRACT-COMPLETED-TRANS
    }
    private void sub_Extract_Delay_Incomplete_Trans() throws Exception                                                                                                    //Natural: EXTRACT-DELAY-INCOMPLETE-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************
        pnd_Adp_Sd2_Pnd_Sd2_Opn_Clsd_Ind.setValue("O");                                                                                                                   //Natural: ASSIGN #SD2-OPN-CLSD-IND = 'O'
        pnd_Adp_Sd2_Pnd_Sd2_Stts_Cde.setValue("D00");                                                                                                                     //Natural: ASSIGN #SD2-STTS-CDE = 'D00'
        ldaAdsl401.getVw_ads_Prtcpnt_View().startDatabaseRead                                                                                                             //Natural: READ ADS-PRTCPNT-VIEW BY ADP-SUPER2 STARTING FROM #ADP-SD2
        (
        "READ02",
        new Wc[] { new Wc("ADP_SUPER2", ">=", pnd_Adp_Sd2, WcType.BY) },
        new Oc[] { new Oc("ADP_SUPER2", "ASC") }
        );
        READ02:
        while (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().readNextRow("READ02")))
        {
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind().notEquals("O") || ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,1).compareTo("E")  //Natural: IF ADP-OPN-CLSD-IND NE 'O' OR SUBSTR ( ADP-STTS-CDE,1,1 ) GT 'E'
                > 0))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,1).equals("D") || ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,  //Natural: ACCEPT IF SUBSTR ( ADP-STTS-CDE,1,1 ) = 'D' OR = 'E'
                1).equals("E"))))
            {
                continue;
            }
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,1).equals("D")))                                                                   //Natural: IF SUBSTR ( ADP-STTS-CDE,1,1 ) = 'D'
            {
                pnd_Rec_Delayed_Cnt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #REC-DELAYED-CNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rec_Incmplt_Cnt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #REC-INCMPLT-CNT
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM READ-CONTRACT-RECORD
            sub_Read_Contract_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  EXTRACT-DELAY-INCOMPLETE-TRANS
    }
    private void sub_Extract_Rejected_Trans() throws Exception                                                                                                            //Natural: EXTRACT-REJECTED-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        pnd_Adp_Sd2_Pnd_Sd2_Opn_Clsd_Ind.setValue("C");                                                                                                                   //Natural: ASSIGN #SD2-OPN-CLSD-IND = 'C'
        pnd_Adp_Sd2_Pnd_Sd2_Stts_Cde.setValue("L00");                                                                                                                     //Natural: ASSIGN #SD2-STTS-CDE = 'L00'
        ldaAdsl401.getVw_ads_Prtcpnt_View().startDatabaseRead                                                                                                             //Natural: READ ADS-PRTCPNT-VIEW BY ADP-SUPER2 STARTING FROM #ADP-SD2
        (
        "READ03",
        new Wc[] { new Wc("ADP_SUPER2", ">=", pnd_Adp_Sd2, WcType.BY) },
        new Oc[] { new Oc("ADP_SUPER2", "ASC") }
        );
        READ03:
        while (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().readNextRow("READ03")))
        {
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind().notEquals("C") || ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,1).compareTo("L")  //Natural: IF ADS-PRTCPNT-VIEW.ADP-OPN-CLSD-IND NE 'C' OR SUBSTR ( ADP-STTS-CDE,1,1 ) GT 'L'
                > 0))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,1).equals("L"))))                                                                //Natural: ACCEPT IF SUBSTR ( ADP-STTS-CDE,1,1 ) = 'L'
            {
                continue;
            }
            pnd_Rec_Rejected_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #REC-REJECTED-CNT
                                                                                                                                                                          //Natural: PERFORM READ-CONTRACT-RECORD
            sub_Read_Contract_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  EXTRACT-REJECTED-TRANS
    }
    private void sub_Read_Contract_Record() throws Exception                                                                                                              //Natural: READ-CONTRACT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pnd_Ra_Mdo.reset();                                                                                                                                               //Natural: RESET #RA-MDO #SRA-MDO #GSRA-MDO
        pnd_Sra_Mdo.reset();
        pnd_Gsra_Mdo.reset();
        ldaAdsl401a.getVw_ads_Cntrct_View().startDatabaseFind                                                                                                             //Natural: FIND ADS-CNTRCT-VIEW WITH ADS-CNTRCT-VIEW.RQST-ID = ADS-PRTCPNT-VIEW.RQST-ID
        (
        "FIND01",
        new Wc[] { new Wc("RQST_ID", "=", ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id(), WcType.WITH) }
        );
        FIND01:
        while (condition(ldaAdsl401a.getVw_ads_Cntrct_View().readNextRow("FIND01")))
        {
            ldaAdsl401a.getVw_ads_Cntrct_View().setIfNotFoundControlFlag(false);
            pnd_Work_File.reset();                                                                                                                                        //Natural: RESET #WORK-FILE NECA4000
            pdaNeca4000.getNeca4000().reset();
            pnd_Work_File_Pnd_Wk_Lst_Actvty_Dte.setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                       //Natural: MOVE EDITED ADP-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #WK-LST-ACTVTY-DTE
            //* *MOVE EDITED ADP-EFFCTV-DTE (EM=YYYYMMDD) TO #WK-EFFCTV-DTE
            //*  COMPLETE
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,1).equals("T")))                                                                   //Natural: IF SUBSTR ( ADP-STTS-CDE,1,1 ) = 'T'
            {
                pnd_Work_File_Pnd_Wk_Stts.setValue("1");                                                                                                                  //Natural: ASSIGN #WK-STTS := '1'
                //*  DELAYED, INCOMPLETE, REJECTED
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Work_File_Pnd_Wk_Stts.setValue("2");                                                                                                                  //Natural: ASSIGN #WK-STTS := '2'
            }                                                                                                                                                             //Natural: END-IF
            pdaNeca4000.getNeca4000_Function_Cde().setValue("PRD");                                                                                                       //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'PRD'
            pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("04");                                                                                                 //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '04'
            pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr());                                                          //Natural: ASSIGN NECA4000.PRD-KEY4-ACCT-NBR := ADC-TIAA-NBR
            DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                        //Natural: CALLNAT 'NECN4000' NECA4000
            if (condition(Global.isEscape())) return;
            if (condition(pdaNeca4000.getNeca4000_Output_Cnt().equals(1)))                                                                                                //Natural: IF NECA4000.OUTPUT-CNT = 1
            {
                pnd_Work_File_Pnd_Wk_Lob.setValue(pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1));                                                                 //Natural: ASSIGN #WK-LOB := NECA4000.PRD-PRODUCT-CDE ( 1 )
                //*  WRITE '='  NECA4000.PRD-SUB-PRODUCT-CDE(1)
                short decideConditionsMet1592 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUES OF NECA4000.PRD-PRODUCT-CDE ( 1 );//Natural: VALUE 'RA'
                if (condition((pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1).equals("RA"))))
                {
                    decideConditionsMet1592++;
                    DbsUtil.examine(new ExamineSource(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1)), new ExamineSearch("GRA MDO"), new ExamineGivingNumber(pnd_I)); //Natural: EXAMINE NECA4000.PRD-SUB-PRODUCT-CDE ( 1 ) FOR 'GRA MDO' GIVING #I
                    if (condition(pnd_I.notEquals(getZero())))                                                                                                            //Natural: IF #I NE 0
                    {
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("GRA MDO");                                                                                                 //Natural: ASSIGN #WK-SUB-LOB := 'GRA MDO'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("03");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '03'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        DbsUtil.examine(new ExamineSource(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1)), new ExamineSearch("MDO"), new ExamineGivingNumber(pnd_I)); //Natural: EXAMINE NECA4000.PRD-SUB-PRODUCT-CDE ( 1 ) FOR 'MDO' GIVING #I
                        if (condition(pnd_I.notEquals(getZero())))                                                                                                        //Natural: IF #I NE 0
                        {
                            pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("RA MDO");                                                                                              //Natural: ASSIGN #WK-SUB-LOB := 'RA MDO'
                            pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("02");                                                                                                  //Natural: ASSIGN #WK-SUB-SEQ := '02'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("Non MDO");                                                                                             //Natural: ASSIGN #WK-SUB-LOB := 'Non MDO'
                            pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("01");                                                                                                  //Natural: ASSIGN #WK-SUB-SEQ := '01'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 'SRA'
                else if (condition((pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1).equals("SRA"))))
                {
                    decideConditionsMet1592++;
                    DbsUtil.examine(new ExamineSource(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1)), new ExamineSearch("GRA MDO"), new ExamineGivingNumber(pnd_I)); //Natural: EXAMINE NECA4000.PRD-SUB-PRODUCT-CDE ( 1 ) FOR 'GRA MDO' GIVING #I
                    if (condition(pnd_I.notEquals(getZero())))                                                                                                            //Natural: IF #I NE 0
                    {
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("GSRA MDO");                                                                                                //Natural: ASSIGN #WK-SUB-LOB := 'GSRA MDO'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("03");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '03'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        DbsUtil.examine(new ExamineSource(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1)), new ExamineSearch("MDO"), new ExamineGivingNumber(pnd_I)); //Natural: EXAMINE NECA4000.PRD-SUB-PRODUCT-CDE ( 1 ) FOR 'MDO' GIVING #I
                        if (condition(pnd_I.notEquals(getZero())))                                                                                                        //Natural: IF #I NE 0
                        {
                            pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("SRA DO");                                                                                              //Natural: ASSIGN #WK-SUB-LOB := 'SRA DO'
                            pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("02");                                                                                                  //Natural: ASSIGN #WK-SUB-SEQ := '02'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("Non MDO");                                                                                             //Natural: ASSIGN #WK-SUB-LOB := 'Non MDO'
                            pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("01");                                                                                                  //Natural: ASSIGN #WK-SUB-SEQ := '01'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 'IA'
                else if (condition((pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1).equals("IA"))))
                {
                    decideConditionsMet1592++;
                    //*  PER ED MELNIK
                    short decideConditionsMet1625 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE OF NECA4000.PRD-SUB-PRODUCT-CDE ( 1 );//Natural: VALUE 'IPRO'
                    if (condition((pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1).equals("IPRO"))))
                    {
                        decideConditionsMet1625++;
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("IPRO");                                                                                                    //Natural: ASSIGN #WK-SUB-LOB := 'IPRO'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("01");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '01'
                        pnd_Work_File_Pnd_Wk_Lob.setValue("IPRO");                                                                                                        //Natural: ASSIGN #WK-LOB := 'IPRO'
                    }                                                                                                                                                     //Natural: VALUE 'IA-RET'
                    else if (condition((pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1).equals("IA-RET"))))
                    {
                        decideConditionsMet1625++;
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("TPA");                                                                                                     //Natural: ASSIGN #WK-SUB-LOB := 'TPA'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("01");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '01'
                        pnd_Work_File_Pnd_Wk_Lob.setValue("TPA");                                                                                                         //Natural: ASSIGN #WK-LOB := 'TPA'
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        getReports().write(0, "Unknown Sub Product",pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1),"For Product",pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1)); //Natural: WRITE 'Unknown Sub Product' NECA4000.PRD-SUB-PRODUCT-CDE ( 1 ) 'For Product' NECA4000.PRD-PRODUCT-CDE ( 1 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("UNKNOWN");                                                                                                 //Natural: ASSIGN #WK-SUB-LOB := 'UNKNOWN'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("99");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '99'
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: VALUE 'IP7'
                else if (condition((pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1).equals("IP7"))))
                {
                    decideConditionsMet1592++;
                    short decideConditionsMet1640 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE OF NECA4000.PRD-SUB-PRODUCT-CDE ( 1 );//Natural: VALUE 'IP7'
                    if (condition((pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1).equals("IP7"))))
                    {
                        decideConditionsMet1640++;
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("IPRO(84)");                                                                                                //Natural: ASSIGN #WK-SUB-LOB := 'IPRO(84)'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("02");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '02'
                        pnd_Work_File_Pnd_Wk_Lob.setValue("IPRO");                                                                                                        //Natural: ASSIGN #WK-LOB := 'IPRO'
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        getReports().write(0, "Unknown Sub Product",pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1),"For Product",pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1)); //Natural: WRITE 'Unknown Sub Product' NECA4000.PRD-SUB-PRODUCT-CDE ( 1 ) 'For Product' NECA4000.PRD-PRODUCT-CDE ( 1 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("UNKNOWN");                                                                                                 //Natural: ASSIGN #WK-SUB-LOB := 'UNKNOWN'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("99");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '99'
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: VALUE 'IP8'
                else if (condition((pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1).equals("IP8"))))
                {
                    decideConditionsMet1592++;
                    short decideConditionsMet1651 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE OF NECA4000.PRD-SUB-PRODUCT-CDE ( 1 );//Natural: VALUE 'IP8'
                    if (condition((pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1).equals("IP8"))))
                    {
                        decideConditionsMet1651++;
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("IPRO(60)");                                                                                                //Natural: ASSIGN #WK-SUB-LOB := 'IPRO(60)'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("03");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '03'
                        pnd_Work_File_Pnd_Wk_Lob.setValue("IPRO");                                                                                                        //Natural: ASSIGN #WK-LOB := 'IPRO'
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        getReports().write(0, "Unknown Sub Product",pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1),"For Product",pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1)); //Natural: WRITE 'Unknown Sub Product' NECA4000.PRD-SUB-PRODUCT-CDE ( 1 ) 'For Product' NECA4000.PRD-PRODUCT-CDE ( 1 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("UNKNOWN");                                                                                                 //Natural: ASSIGN #WK-SUB-LOB := 'UNKNOWN'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("99");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '99'
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: VALUE 'IP9'
                else if (condition((pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1).equals("IP9"))))
                {
                    decideConditionsMet1592++;
                    short decideConditionsMet1662 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE OF NECA4000.PRD-SUB-PRODUCT-CDE ( 1 );//Natural: VALUE 'IP9'
                    if (condition((pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1).equals("IP9"))))
                    {
                        decideConditionsMet1662++;
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("IPRO(5)");                                                                                                 //Natural: ASSIGN #WK-SUB-LOB := 'IPRO(5)'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("04");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '04'
                        pnd_Work_File_Pnd_Wk_Lob.setValue("IPRO");                                                                                                        //Natural: ASSIGN #WK-LOB := 'IPRO'
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        getReports().write(0, "Unknown Sub Product",pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1),"For Product",pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1)); //Natural: WRITE 'Unknown Sub Product' NECA4000.PRD-SUB-PRODUCT-CDE ( 1 ) 'For Product' NECA4000.PRD-PRODUCT-CDE ( 1 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("UNKNOWN");                                                                                                 //Natural: ASSIGN #WK-SUB-LOB := 'UNKNOWN'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("99");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '99'
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: VALUE 'TP7'
                else if (condition((pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1).equals("TP7"))))
                {
                    decideConditionsMet1592++;
                    short decideConditionsMet1673 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE OF NECA4000.PRD-SUB-PRODUCT-CDE ( 1 );//Natural: VALUE 'TP7'
                    if (condition((pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1).equals("TP7"))))
                    {
                        decideConditionsMet1673++;
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("TPA(84)");                                                                                                 //Natural: ASSIGN #WK-SUB-LOB := 'TPA(84)'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("02");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '02'
                        pnd_Work_File_Pnd_Wk_Lob.setValue("TPA");                                                                                                         //Natural: ASSIGN #WK-LOB := 'TPA'
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        getReports().write(0, "Unknown Sub Product",pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1),"For Product",pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1)); //Natural: WRITE 'Unknown Sub Product' NECA4000.PRD-SUB-PRODUCT-CDE ( 1 ) 'For Product' NECA4000.PRD-PRODUCT-CDE ( 1 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("UNKNOWN");                                                                                                 //Natural: ASSIGN #WK-SUB-LOB := 'UNKNOWN'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("99");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '99'
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: VALUE 'TP8'
                else if (condition((pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1).equals("TP8"))))
                {
                    decideConditionsMet1592++;
                    short decideConditionsMet1684 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE OF NECA4000.PRD-SUB-PRODUCT-CDE ( 1 );//Natural: VALUE 'TP8'
                    if (condition((pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1).equals("TP8"))))
                    {
                        decideConditionsMet1684++;
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("TPA(60)");                                                                                                 //Natural: ASSIGN #WK-SUB-LOB := 'TPA(60)'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("03");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '03'
                        pnd_Work_File_Pnd_Wk_Lob.setValue("TPA");                                                                                                         //Natural: ASSIGN #WK-LOB := 'TPA'
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        getReports().write(0, "Unknown Sub Product",pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1),"For Product",pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1)); //Natural: WRITE 'Unknown Sub Product' NECA4000.PRD-SUB-PRODUCT-CDE ( 1 ) 'For Product' NECA4000.PRD-PRODUCT-CDE ( 1 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("UNKNOWN");                                                                                                 //Natural: ASSIGN #WK-SUB-LOB := 'UNKNOWN'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("99");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '99'
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: VALUE 'TP9'
                else if (condition((pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1).equals("TP9"))))
                {
                    decideConditionsMet1592++;
                    //*  PER ED MELNIK
                    short decideConditionsMet1696 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE OF NECA4000.PRD-SUB-PRODUCT-CDE ( 1 );//Natural: VALUE 'TP9'
                    if (condition((pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1).equals("TP9"))))
                    {
                        decideConditionsMet1696++;
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("TPA(5)");                                                                                                  //Natural: ASSIGN #WK-SUB-LOB := 'TPA(5)'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("04");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '04'
                        pnd_Work_File_Pnd_Wk_Lob.setValue("TPA");                                                                                                         //Natural: ASSIGN #WK-LOB := 'TPA'
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        getReports().write(0, "Unknown Sub Product",pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1),"For Product",pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1)); //Natural: WRITE 'Unknown Sub Product' NECA4000.PRD-SUB-PRODUCT-CDE ( 1 ) 'For Product' NECA4000.PRD-PRODUCT-CDE ( 1 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("UNKNOWN");                                                                                                 //Natural: ASSIGN #WK-SUB-LOB := 'UNKNOWN'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("99");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '99'
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: VALUE 'IRA'
                else if (condition((pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1).equals("IRA"))))
                {
                    decideConditionsMet1592++;
                    //*  PER ED MELNIK
                    //*  PER ED MELNIK
                    short decideConditionsMet1709 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE OF NECA4000.PRD-SUB-PRODUCT-CDE ( 1 );//Natural: VALUE 'TRADITIONAL'
                    if (condition((pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1).equals("TRADITIONAL"))))
                    {
                        decideConditionsMet1709++;
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("TRADITIONAL");                                                                                             //Natural: ASSIGN #WK-SUB-LOB := 'TRADITIONAL'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("03");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '03'
                    }                                                                                                                                                     //Natural: VALUE 'MDO'
                    else if (condition((pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1).equals("MDO"))))
                    {
                        decideConditionsMet1709++;
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("MDO");                                                                                                     //Natural: ASSIGN #WK-SUB-LOB := 'MDO'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("05");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '05'
                    }                                                                                                                                                     //Natural: VALUE 'SEP'
                    else if (condition((pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1).equals("SEP"))))
                    {
                        decideConditionsMet1709++;
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("SEP");                                                                                                     //Natural: ASSIGN #WK-SUB-LOB := 'SEP'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("04");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '04'
                    }                                                                                                                                                     //Natural: VALUE 'ROTH'
                    else if (condition((pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1).equals("ROTH"))))
                    {
                        decideConditionsMet1709++;
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("ROTH");                                                                                                    //Natural: ASSIGN #WK-SUB-LOB := 'ROTH'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("01");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '01'
                    }                                                                                                                                                     //Natural: VALUE 'IRA'
                    else if (condition((pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1).equals("IRA"))))
                    {
                        decideConditionsMet1709++;
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("CLASSIC");                                                                                                 //Natural: ASSIGN #WK-SUB-LOB := 'CLASSIC'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("02");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '02'
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        getReports().write(0, "Unknown Sub Product",pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1),"For Product",pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1)); //Natural: WRITE 'Unknown Sub Product' NECA4000.PRD-SUB-PRODUCT-CDE ( 1 ) 'For Product' NECA4000.PRD-PRODUCT-CDE ( 1 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("UNKNOWN");                                                                                                 //Natural: ASSIGN #WK-SUB-LOB := 'UNKNOWN'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("99");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '99'
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: VALUE 'IRAIDX'
                else if (condition((pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1).equals("IRAIDX"))))
                {
                    decideConditionsMet1592++;
                    short decideConditionsMet1731 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE OF NECA4000.PRD-SUB-PRODUCT-CDE ( 1 );//Natural: VALUE 'ROTH INDEXED'
                    if (condition((pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1).equals("ROTH INDEXED"))))
                    {
                        decideConditionsMet1731++;
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("ROTH IDX");                                                                                                //Natural: ASSIGN #WK-SUB-LOB := 'ROTH IDX'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("01");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '01'
                    }                                                                                                                                                     //Natural: VALUE 'SEP INDEXED'
                    else if (condition((pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1).equals("SEP INDEXED"))))
                    {
                        decideConditionsMet1731++;
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("SEP IDX");                                                                                                 //Natural: ASSIGN #WK-SUB-LOB := 'SEP IDX'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("02");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '02'
                    }                                                                                                                                                     //Natural: VALUE 'TRD INDEXED'
                    else if (condition((pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1).equals("TRD INDEXED"))))
                    {
                        decideConditionsMet1731++;
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("TRAD IDX");                                                                                                //Natural: ASSIGN #WK-SUB-LOB := 'TRAD IDX'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("03");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '03'
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        getReports().write(0, "Unknown Sub Product",pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1),"For Product",pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1)); //Natural: WRITE 'Unknown Sub Product' NECA4000.PRD-SUB-PRODUCT-CDE ( 1 ) 'For Product' NECA4000.PRD-PRODUCT-CDE ( 1 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("UNKNOWN");                                                                                                 //Natural: ASSIGN #WK-SUB-LOB := 'UNKNOWN'
                        pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("99");                                                                                                      //Natural: ASSIGN #WK-SUB-SEQ := '99'
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("  ");                                                                                                          //Natural: ASSIGN #WK-SUB-SEQ := '  '
                    pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("       ");                                                                                                     //Natural: ASSIGN #WK-SUB-LOB := '       '
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "Product counter ne 1",pdaNeca4000.getNeca4000_Output_Cnt(),"Product ",pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1),       //Natural: WRITE 'Product counter ne 1' NECA4000.OUTPUT-CNT 'Product ' NECA4000.PRD-PRODUCT-CDE ( 1 ) 'for contract=' ADC-TIAA-NBR / '=' ADS-PRTCPNT-VIEW.RQST-ID
                    "for contract=",ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr(),NEWLINE,"=",ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Work_File_Pnd_Wk_Lob.setValue("UNKNOWN");                                                                                                             //Natural: ASSIGN #WK-LOB := 'UNKNOWN'
                pnd_Work_File_Pnd_Wk_Sub_Lob.setValue("       ");                                                                                                         //Natural: ASSIGN #WK-SUB-LOB := '       '
                pnd_Work_File_Pnd_Wk_Sub_Seq.setValue("  ");                                                                                                              //Natural: ASSIGN #WK-SUB-SEQ := '  '
                //*  RS0 FOR TESTING ONLY
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_I).equals("Y")))                                                                 //Natural: IF ADC-ACCT-CDE ( #I ) = 'Y'
                {
                    getReports().write(0, "ADSP998 - FOUND STABLE RETURN FUND ",ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                //Natural: WRITE 'ADSP998 - FOUND STABLE RETURN FUND ' ADS-PRTCPNT-VIEW.RQST-ID
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  A REQUEST WILL HAVE EATHER A TIAA OR STABLE FUND
            //*  RS0
            pnd_Stbl_Fund.setValue(false);                                                                                                                                //Natural: MOVE FALSE TO #STBL-FUND
            pnd_Sv_Fund.setValue(false);                                                                                                                                  //Natural: MOVE FALSE TO #SV-FUND
            FOR02:                                                                                                                                                        //Natural: FOR #I 1 TO 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                //*  RS0
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_I).equals("Y")))                                                                 //Natural: IF ADC-ACCT-CDE ( #I ) = 'Y'
                {
                    pnd_Stbl_Fund.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #STBL-FUND
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_I).equals("T")))                                                                 //Natural: IF ADC-ACCT-CDE ( #I ) = 'T'
                {
                    pnd_J.reset();                                                                                                                                        //Natural: RESET #J
                    //*   EM - 100713 - CHANGE TO USE IC654
                    //*      EXAMINE ADC-T395-FUND-ID(*) FOR 'SA' GIVING #J
                    DbsUtil.examine(new ExamineSource(ldaAdsl401a.getAds_Cntrct_View_Adc_Invstmnt_Grpng_Id().getValue("*")), new ExamineSearch("TSV0"),                   //Natural: EXAMINE ADC-INVSTMNT-GRPNG-ID ( * ) FOR 'TSV0' GIVING #J
                        new ExamineGivingNumber(pnd_J));
                    if (condition(pnd_J.notEquals(getZero())))                                                                                                            //Natural: IF #J NE 0
                    {
                        pnd_Sv_Fund.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO #SV-FUND
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  EXCLUDE STABLE RETURN FUND RS0
                //*  REPRESENTS CREF (FOR SORTING PURPOSES)
                //*  DEA
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_I).notEquals(" ") && ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_I).notEquals("T")  //Natural: IF ADC-ACCT-CDE ( #I ) NE ' ' AND ADC-ACCT-CDE ( #I ) NE 'T' AND ADC-ACCT-CDE ( #I ) NE 'Y'
                    && ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_I).notEquals("Y")))
                {
                    pnd_Work_File_Pnd_Wk_Acct.setValue("D");                                                                                                              //Natural: ASSIGN #WK-ACCT := 'D'
                    pnd_Work_File_Pnd_Wk_Rate.setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cref_Rate_Cde().getValue(pnd_I));                                          //Natural: ASSIGN #WK-RATE := ADC-ACCT-CREF-RATE-CDE ( #I )
                    pnd_Work_File_Pnd_Wk_Actl_Amt.setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_I));                                           //Natural: ASSIGN #WK-ACTL-AMT := ADC-ACCT-ACTL-AMT ( #I )
                    pnd_Work_File_Pnd_Wk_Survivor_Ind.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Srvvr_Ind());                                                           //Natural: ASSIGN #WK-SURVIVOR-IND := ADP-SRVVR-IND
                    //*  GG050608
                    if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Roth_Rqst_Ind().equals("Y")))                                                                        //Natural: IF ADP-ROTH-RQST-IND = 'Y'
                    {
                        pnd_Work_File_Pnd_Wk_Roth_Ind.setValue("Y");                                                                                                      //Natural: ASSIGN #WK-ROTH-IND := 'Y'
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Work_Count.nadd(1);                                                                                                                               //Natural: ADD 1 TO #WORK-COUNT
                    getWorkFiles().write(1, false, pnd_Work_File);                                                                                                        //Natural: WRITE WORK FILE 1 #WORK-FILE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  FINISHED PROCESSING CREF
            //*  RS0
            //*  REPRESENTS STABLE (FOR SORTING PURPOSES)
            if (condition(pnd_Stbl_Fund.getBoolean()))                                                                                                                    //Natural: IF #STBL-FUND
            {
                pnd_Work_File_Pnd_Wk_Acct.setValue("B");                                                                                                                  //Natural: ASSIGN #WK-ACCT := 'B'
                //*  REPRESENTS TIAA (FOR SORTING PURPOSES)
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Work_File_Pnd_Wk_Acct.setValue("A");                                                                                                                  //Natural: ASSIGN #WK-ACCT := 'A'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Sv_Fund.getBoolean()))                                                                                                                      //Natural: IF #SV-FUND
            {
                pnd_Work_File_Pnd_Wk_Acct.setValue("C");                                                                                                                  //Natural: ASSIGN #WK-ACCT := 'C'
                //*  EM - 030712
            }                                                                                                                                                             //Natural: END-IF
            FOR03:                                                                                                                                                        //Natural: FOR #I 1 TO #MAX-RATES
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Rates)); pnd_I.nadd(1))
            {
                //*  DEA
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_I).notEquals(" ")))                                                     //Natural: IF ADC-DTL-TIAA-RATE-CDE ( #I ) NE ' '
                {
                    pnd_Work_File_Pnd_Wk_Rate.setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_I));                                           //Natural: ASSIGN #WK-RATE := ADC-DTL-TIAA-RATE-CDE ( #I )
                    pnd_Work_File_Pnd_Wk_Actl_Amt.setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_I));                                       //Natural: ASSIGN #WK-ACTL-AMT := ADC-DTL-TIAA-ACTL-AMT ( #I )
                    pnd_Work_File_Pnd_Wk_Survivor_Ind.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Srvvr_Ind());                                                           //Natural: ASSIGN #WK-SURVIVOR-IND := ADP-SRVVR-IND
                    //*  GG050608
                    if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Roth_Rqst_Ind().equals("Y")))                                                                        //Natural: IF ADP-ROTH-RQST-IND = 'Y'
                    {
                        pnd_Work_File_Pnd_Wk_Roth_Ind.setValue("Y");                                                                                                      //Natural: ASSIGN #WK-ROTH-IND := 'Y'
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Work_Count.nadd(1);                                                                                                                               //Natural: ADD 1 TO #WORK-COUNT
                    getWorkFiles().write(1, false, pnd_Work_File);                                                                                                        //Natural: WRITE WORK FILE 1 #WORK-FILE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  READ-CONTRACT-RECORD
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=132");
    }
}
