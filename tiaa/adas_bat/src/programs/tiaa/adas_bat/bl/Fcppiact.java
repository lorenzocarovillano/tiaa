/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:21:47 PM
**        * FROM NATURAL PROGRAM : Fcppiact
************************************************************
**        * FILE NAME            : Fcppiact.java
**        * CLASS NAME           : Fcppiact
**        * INSTANCE NAME        : Fcppiact
************************************************************
************************************************************************
* PROGRAM  : FCPPIACT
*
* SYSTEM   : CPS
* TITLE    : FETCH IAA-CNTRCT TO UPDATE S462 FILE
* FUNCTION : IT WILL EXTRACT FOLLOWING FROM IAA-CONRCT TO UPDATE IN
*            S462 FILE:
*            PLAN-NMBR
*            SUB-PLAN-NMBR
*            CNTRCT-ORIG-DA-CNTRCT-NB
*            ORGNTNG-SUB-PLAN-NMBR
*
* DATE     : 05/15/2020
* CHANGES  : CTS CPS SUNSET (CHG694525)
* HISTORY
* MODIFICATIONS :
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcppiact extends BLNatBase
{
    // Data Areas
    private LdaIaal200a ldaIaal200a;
    private LdaFcpls462 ldaFcpls462;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Error_Msg;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaal200a = new LdaIaal200a();
        registerRecord(ldaIaal200a);
        registerRecord(ldaIaal200a.getVw_iaa_Cntrct());
        ldaFcpls462 = new LdaFcpls462();
        registerRecord(ldaFcpls462);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cntrct_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 70);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaIaal200a.initializeValues();
        ldaFcpls462.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcppiact() throws Exception
    {
        super("Fcppiact");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPPIACT", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 20
        //*                                                                                                                                                               //Natural: ON ERROR
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 WF-PYMNT-ADDR-GRP1 ( * )
        while (condition(getWorkFiles().read(1, ldaFcpls462.getWf_Pymnt_Addr_Grp1().getValue("*"))))
        {
            pnd_Cntrct_Ppcn_Nbr.setValue(ldaFcpls462.getWf_Pymnt_Addr_Grp1_Cntrct_Ppcn_Nbr());                                                                            //Natural: ASSIGN #CNTRCT-PPCN-NBR := WF-PYMNT-ADDR-GRP1.CNTRCT-PPCN-NBR
                                                                                                                                                                          //Natural: PERFORM PROCESS-IAA-CNTRCT
            sub_Process_Iaa_Cntrct();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getWorkFiles().write(2, false, ldaFcpls462.getWf_Pymnt_Addr_Grp1().getValue("*"));                                                                            //Natural: WRITE WORK FILE 2 WF-PYMNT-ADDR-GRP1 ( * )
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* ********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-IAA-CNTRCT
    }
    private void sub_Process_Iaa_Cntrct() throws Exception                                                                                                                //Natural: PROCESS-IAA-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************
        ldaIaal200a.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT WITH IAA-CNTRCT.CNTRCT-PPCN-NBR = #CNTRCT-PPCN-NBR
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Cntrct_Ppcn_Nbr, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(ldaIaal200a.getVw_iaa_Cntrct().readNextRow("FIND01", true)))
        {
            ldaIaal200a.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal200a.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                      //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "NO CONTRACT FOUND FOR ",pnd_Cntrct_Ppcn_Nbr);                                                                                      //Natural: WRITE 'NO CONTRACT FOUND FOR ' #CNTRCT-PPCN-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            ldaFcpls462.getWf_Pymnt_Addr_Grp1_Plan_Number().setValue(ldaIaal200a.getIaa_Cntrct_Plan_Nmbr());                                                              //Natural: ASSIGN WF-PYMNT-ADDR-GRP1.PLAN-NUMBER := IAA-CNTRCT.PLAN-NMBR
            ldaFcpls462.getWf_Pymnt_Addr_Grp1_Sub_Plan().setValue(ldaIaal200a.getIaa_Cntrct_Sub_Plan_Nmbr());                                                             //Natural: ASSIGN WF-PYMNT-ADDR-GRP1.SUB-PLAN := IAA-CNTRCT.SUB-PLAN-NMBR
            ldaFcpls462.getWf_Pymnt_Addr_Grp1_Orig_Cntrct_Nbr().setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr());                                          //Natural: ASSIGN WF-PYMNT-ADDR-GRP1.ORIG-CNTRCT-NBR := IAA-CNTRCT.CNTRCT-ORIG-DA-CNTRCT-NBR
            ldaFcpls462.getWf_Pymnt_Addr_Grp1_Orig_Sub_Plan().setValue(ldaIaal200a.getIaa_Cntrct_Orgntng_Sub_Plan_Nmbr());                                                //Natural: ASSIGN WF-PYMNT-ADDR-GRP1.ORIG-SUB-PLAN := IAA-CNTRCT.ORGNTNG-SUB-PLAN-NMBR
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //* PROCESS-IAA-CNTRCT
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pnd_Error_Msg.setValue(DbsUtil.compress(Global.getPROGRAM(), "- ERROR ON LINE", Global.getERROR_LINE(), "ERROR NBR", Global.getERROR_NR()));                      //Natural: COMPRESS *PROGRAM '- ERROR ON LINE' *ERROR-LINE 'ERROR NBR' *ERROR-NR INTO #ERROR-MSG
        getReports().write(0, "ERROR :",pnd_Error_Msg);                                                                                                                   //Natural: WRITE 'ERROR :' #ERROR-MSG
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE IMMEDIATE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=20");
    }
}
