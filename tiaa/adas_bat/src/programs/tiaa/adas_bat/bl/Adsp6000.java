/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:03:27 PM
**        * FROM NATURAL PROGRAM : Adsp6000
************************************************************
**        * FILE NAME            : Adsp6000.java
**        * CLASS NAME           : Adsp6000
**        * INSTANCE NAME        : Adsp6000
************************************************************
**********************************************************************
* HISTORY
*
* 04/06/2004 HK CHANGED LOCAL TO USE ADSL6003.  CHANGED REPEAT TO FOR
*            LOOP FOR DTL DATA.
*
* 11/18/2004 TS CHECK FOR END OF MONTH PROCESSING DATE
* 02/20/2008 OS COMMENTED-OUT UNNECESSARY WRITES. SC 022008.
* 03/17/2008 E.MELNIK   ROTH 403B/401K CHANGES.  RECOMPILED FOR
*                       NEW ADSL401/ADSL450/ADSLCNTL.
* 12/04/2008 R.SACHARNY SUB TITLE (REA) CHANGED TO (REA/ACS/IND) RS0
* 03/02/2009 R.SACHARNY ADD ACCESS FUND TO "Income Benefits" (RS1)
* 7/19/2010  C. MASON  RESTOW DUE TO LDA CHANGES
* 03/05/12   E. MELNIK RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                      UPDATED LINKAGE AREAS.
* 03/30/12   O. SOTTO  ADDITONAL RATE BASE EXP CHANGES. SC 033012.
* 01/18/13   E. MELNIK TNG PROJECT.  RESTOW TO INCLUDE UPDATED LINKAGE
*                      AREAS.
* 02/22/2017 R.CARREON PIN EXPANSION 02222017
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp6000 extends BLNatBase
{
    // Data Areas
    private LdaAdsl401 ldaAdsl401;
    private LdaAdsl450 ldaAdsl450;
    private LdaAdsl6005 ldaAdsl6005;
    private LdaAdslcntl ldaAdslcntl;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cis_Prtcpnt_File_View;
    private DbsField cis_Prtcpnt_File_View_Cis_Pin_Nbr;
    private DbsField cis_Prtcpnt_File_View_Cis_Rqst_Id_Key;
    private DbsField pnd_Tiaa_Only;
    private DbsField pnd_Tiaa_Ind;
    private DbsField pnd_Tpa_Io_Ind;
    private DbsField pnd_Ipro_Sw;
    private DbsField pnd_End_Date;
    private DbsField pnd_Counter_Start;
    private DbsField pnd_Total_Processed;
    private DbsField pnd_Error_Ind;
    private DbsField pnd_Cntrl_Total;
    private DbsField pnd_Cntrl_Ok;
    private DbsField pnd_Cis_Super;
    private DbsField pnd_Date_Conv;

    private DbsGroup pnd_Date_Conv__R_Field_1;
    private DbsField pnd_Date_Conv_Pnd_Date_Conv_Ccyy;
    private DbsField pnd_Date_Conv_Pnd_Date_Conv_Mm;
    private DbsField pnd_Date_Conv_Pnd_Date_Conv_Dd;
    private DbsField pnd_Print_Date;

    private DbsGroup pnd_Print_Date__R_Field_2;
    private DbsField pnd_Print_Date_Pnd_Print_Date_Mm;
    private DbsField pnd_Print_Date_Pnd_Print_Date_Dd;
    private DbsField pnd_Print_Date_Pnd_Print_Date_Ccyy;
    private DbsField pnd_Da_Tiaa_Grd_Amt;
    private DbsField pnd_Da_Tiaa_Std_Amt;
    private DbsField pnd_Ia_Tiaa_Grd_Amt;
    private DbsField pnd_Ia_Tiaa_Std_Amt;
    private DbsField pnd_Part_Date;

    private DbsGroup pnd_Part_Date__R_Field_3;
    private DbsField pnd_Part_Date_Pnd_Part_Date_Numeric;
    private DbsField pnd_Adi_Super1;

    private DbsGroup pnd_Adi_Super1__R_Field_4;
    private DbsField pnd_Adi_Super1_Pnd_Rqst_Id_Ia;
    private DbsField pnd_Adi_Super1_Pnd_Adi_Record_Type;
    private DbsField pnd_Adi_Super1_Pnd_Adi_Sqnce_Nbr;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_5;
    private DbsField pnd_Date_A_Pnd_Date_N;
    private DbsField pnd_Temp_Date_A;

    private DbsGroup pnd_Temp_Date_A__R_Field_6;
    private DbsField pnd_Temp_Date_A_Pnd_Temp_Date_N;
    private DbsField pnd_Max_Rslt;
    private DbsField pnd_Adi_Super2;

    private DbsGroup pnd_Adi_Super2__R_Field_7;
    private DbsField pnd_Adi_Super2_Pnd_Rqst_Id_Ia_2;
    private DbsField pnd_Adi_Super2_Pnd_Adi_Record_Type_2;
    private DbsField pnd_Adi_Super2_Pnd_Adi_Sqnce_Nbr_2;

    private DataAccessProgramView vw_ads_Ia_Rsl2;
    private DbsField ads_Ia_Rsl2_Count_Castadi_Dtl_Tiaa_Data;

    private DbsGroup ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Rate_Cd;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Acct_Cd;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Eff_Rte_Intrst;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl401 = new LdaAdsl401();
        registerRecord(ldaAdsl401);
        registerRecord(ldaAdsl401.getVw_ads_Prtcpnt_View());
        ldaAdsl450 = new LdaAdsl450();
        registerRecord(ldaAdsl450);
        registerRecord(ldaAdsl450.getVw_ads_Ia_Rslt_View());
        ldaAdsl6005 = new LdaAdsl6005();
        registerRecord(ldaAdsl6005);
        ldaAdslcntl = new LdaAdslcntl();
        registerRecord(ldaAdslcntl);
        registerRecord(ldaAdslcntl.getVw_ads_Cntl_View());

        // Local Variables
        localVariables = new DbsRecord();

        vw_cis_Prtcpnt_File_View = new DataAccessProgramView(new NameInfo("vw_cis_Prtcpnt_File_View", "CIS-PRTCPNT-FILE-VIEW"), "CIS_PRTCPNT_FILE_12", 
            "CIS_PRTCPNT_FILE");
        cis_Prtcpnt_File_View_Cis_Pin_Nbr = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Pin_Nbr", "CIS-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "CIS_PIN_NBR");
        cis_Prtcpnt_File_View_Cis_Rqst_Id_Key = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Rqst_Id_Key", "CIS-RQST-ID-KEY", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "CIS_RQST_ID_KEY");
        registerRecord(vw_cis_Prtcpnt_File_View);

        pnd_Tiaa_Only = localVariables.newFieldInRecord("pnd_Tiaa_Only", "#TIAA-ONLY", FieldType.STRING, 1);
        pnd_Tiaa_Ind = localVariables.newFieldInRecord("pnd_Tiaa_Ind", "#TIAA-IND", FieldType.STRING, 1);
        pnd_Tpa_Io_Ind = localVariables.newFieldInRecord("pnd_Tpa_Io_Ind", "#TPA-IO-IND", FieldType.STRING, 1);
        pnd_Ipro_Sw = localVariables.newFieldInRecord("pnd_Ipro_Sw", "#IPRO-SW", FieldType.STRING, 1);
        pnd_End_Date = localVariables.newFieldInRecord("pnd_End_Date", "#END-DATE", FieldType.DATE);
        pnd_Counter_Start = localVariables.newFieldInRecord("pnd_Counter_Start", "#COUNTER-START", FieldType.NUMERIC, 3);
        pnd_Total_Processed = localVariables.newFieldInRecord("pnd_Total_Processed", "#TOTAL-PROCESSED", FieldType.NUMERIC, 7);
        pnd_Error_Ind = localVariables.newFieldInRecord("pnd_Error_Ind", "#ERROR-IND", FieldType.STRING, 1);
        pnd_Cntrl_Total = localVariables.newFieldInRecord("pnd_Cntrl_Total", "#CNTRL-TOTAL", FieldType.NUMERIC, 7);
        pnd_Cntrl_Ok = localVariables.newFieldInRecord("pnd_Cntrl_Ok", "#CNTRL-OK", FieldType.STRING, 1);
        pnd_Cis_Super = localVariables.newFieldInRecord("pnd_Cis_Super", "#CIS-SUPER", FieldType.STRING, 35);
        pnd_Date_Conv = localVariables.newFieldInRecord("pnd_Date_Conv", "#DATE-CONV", FieldType.NUMERIC, 8);

        pnd_Date_Conv__R_Field_1 = localVariables.newGroupInRecord("pnd_Date_Conv__R_Field_1", "REDEFINE", pnd_Date_Conv);
        pnd_Date_Conv_Pnd_Date_Conv_Ccyy = pnd_Date_Conv__R_Field_1.newFieldInGroup("pnd_Date_Conv_Pnd_Date_Conv_Ccyy", "#DATE-CONV-CCYY", FieldType.NUMERIC, 
            4);
        pnd_Date_Conv_Pnd_Date_Conv_Mm = pnd_Date_Conv__R_Field_1.newFieldInGroup("pnd_Date_Conv_Pnd_Date_Conv_Mm", "#DATE-CONV-MM", FieldType.NUMERIC, 
            2);
        pnd_Date_Conv_Pnd_Date_Conv_Dd = pnd_Date_Conv__R_Field_1.newFieldInGroup("pnd_Date_Conv_Pnd_Date_Conv_Dd", "#DATE-CONV-DD", FieldType.NUMERIC, 
            2);
        pnd_Print_Date = localVariables.newFieldInRecord("pnd_Print_Date", "#PRINT-DATE", FieldType.NUMERIC, 8);

        pnd_Print_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Print_Date__R_Field_2", "REDEFINE", pnd_Print_Date);
        pnd_Print_Date_Pnd_Print_Date_Mm = pnd_Print_Date__R_Field_2.newFieldInGroup("pnd_Print_Date_Pnd_Print_Date_Mm", "#PRINT-DATE-MM", FieldType.NUMERIC, 
            2);
        pnd_Print_Date_Pnd_Print_Date_Dd = pnd_Print_Date__R_Field_2.newFieldInGroup("pnd_Print_Date_Pnd_Print_Date_Dd", "#PRINT-DATE-DD", FieldType.NUMERIC, 
            2);
        pnd_Print_Date_Pnd_Print_Date_Ccyy = pnd_Print_Date__R_Field_2.newFieldInGroup("pnd_Print_Date_Pnd_Print_Date_Ccyy", "#PRINT-DATE-CCYY", FieldType.NUMERIC, 
            4);
        pnd_Da_Tiaa_Grd_Amt = localVariables.newFieldInRecord("pnd_Da_Tiaa_Grd_Amt", "#DA-TIAA-GRD-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Da_Tiaa_Std_Amt = localVariables.newFieldInRecord("pnd_Da_Tiaa_Std_Amt", "#DA-TIAA-STD-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Ia_Tiaa_Grd_Amt = localVariables.newFieldInRecord("pnd_Ia_Tiaa_Grd_Amt", "#IA-TIAA-GRD-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Ia_Tiaa_Std_Amt = localVariables.newFieldInRecord("pnd_Ia_Tiaa_Std_Amt", "#IA-TIAA-STD-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Part_Date = localVariables.newFieldInRecord("pnd_Part_Date", "#PART-DATE", FieldType.STRING, 8);

        pnd_Part_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Part_Date__R_Field_3", "REDEFINE", pnd_Part_Date);
        pnd_Part_Date_Pnd_Part_Date_Numeric = pnd_Part_Date__R_Field_3.newFieldInGroup("pnd_Part_Date_Pnd_Part_Date_Numeric", "#PART-DATE-NUMERIC", FieldType.NUMERIC, 
            8);
        pnd_Adi_Super1 = localVariables.newFieldInRecord("pnd_Adi_Super1", "#ADI-SUPER1", FieldType.STRING, 41);

        pnd_Adi_Super1__R_Field_4 = localVariables.newGroupInRecord("pnd_Adi_Super1__R_Field_4", "REDEFINE", pnd_Adi_Super1);
        pnd_Adi_Super1_Pnd_Rqst_Id_Ia = pnd_Adi_Super1__R_Field_4.newFieldInGroup("pnd_Adi_Super1_Pnd_Rqst_Id_Ia", "#RQST-ID-IA", FieldType.STRING, 35);
        pnd_Adi_Super1_Pnd_Adi_Record_Type = pnd_Adi_Super1__R_Field_4.newFieldInGroup("pnd_Adi_Super1_Pnd_Adi_Record_Type", "#ADI-RECORD-TYPE", FieldType.STRING, 
            3);
        pnd_Adi_Super1_Pnd_Adi_Sqnce_Nbr = pnd_Adi_Super1__R_Field_4.newFieldInGroup("pnd_Adi_Super1_Pnd_Adi_Sqnce_Nbr", "#ADI-SQNCE-NBR", FieldType.NUMERIC, 
            3);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_5 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_5", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_5.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        pnd_Temp_Date_A = localVariables.newFieldInRecord("pnd_Temp_Date_A", "#TEMP-DATE-A", FieldType.STRING, 8);

        pnd_Temp_Date_A__R_Field_6 = localVariables.newGroupInRecord("pnd_Temp_Date_A__R_Field_6", "REDEFINE", pnd_Temp_Date_A);
        pnd_Temp_Date_A_Pnd_Temp_Date_N = pnd_Temp_Date_A__R_Field_6.newFieldInGroup("pnd_Temp_Date_A_Pnd_Temp_Date_N", "#TEMP-DATE-N", FieldType.NUMERIC, 
            8);
        pnd_Max_Rslt = localVariables.newFieldInRecord("pnd_Max_Rslt", "#MAX-RSLT", FieldType.PACKED_DECIMAL, 3);
        pnd_Adi_Super2 = localVariables.newFieldInRecord("pnd_Adi_Super2", "#ADI-SUPER2", FieldType.STRING, 41);

        pnd_Adi_Super2__R_Field_7 = localVariables.newGroupInRecord("pnd_Adi_Super2__R_Field_7", "REDEFINE", pnd_Adi_Super2);
        pnd_Adi_Super2_Pnd_Rqst_Id_Ia_2 = pnd_Adi_Super2__R_Field_7.newFieldInGroup("pnd_Adi_Super2_Pnd_Rqst_Id_Ia_2", "#RQST-ID-IA-2", FieldType.STRING, 
            35);
        pnd_Adi_Super2_Pnd_Adi_Record_Type_2 = pnd_Adi_Super2__R_Field_7.newFieldInGroup("pnd_Adi_Super2_Pnd_Adi_Record_Type_2", "#ADI-RECORD-TYPE-2", 
            FieldType.STRING, 3);
        pnd_Adi_Super2_Pnd_Adi_Sqnce_Nbr_2 = pnd_Adi_Super2__R_Field_7.newFieldInGroup("pnd_Adi_Super2_Pnd_Adi_Sqnce_Nbr_2", "#ADI-SQNCE-NBR-2", FieldType.NUMERIC, 
            3);

        vw_ads_Ia_Rsl2 = new DataAccessProgramView(new NameInfo("vw_ads_Ia_Rsl2", "ADS-IA-RSL2"), "ADS_IA_RSLT", "ADS_IA_RSLT", DdmPeriodicGroups.getInstance().getGroups("ADS_IA_RSLT"));
        ads_Ia_Rsl2_Count_Castadi_Dtl_Tiaa_Data = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Count_Castadi_Dtl_Tiaa_Data", "C*ADI-DTL-TIAA-DATA", 
            RepeatingFieldStrategy.CAsteriskVariable, "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");

        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data = vw_ads_Ia_Rsl2.getRecord().newGroupInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data", "ADI-DTL-TIAA-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Rate_Cd = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Rate_Cd", "ADI-DTL-TIAA-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt", "ADI-DTL-TIAA-DA-STD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt", "ADI-DTL-TIAA-DA-GRD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Acct_Cd = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Acct_Cd", "ADI-DTL-TIAA-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_ACCT_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd", "ADI-DTL-TIAA-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_IA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Eff_Rte_Intrst = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Eff_Rte_Intrst", "ADI-DTL-TIAA-EFF-RTE-INTRST", 
            FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_EFF_RTE_INTRST", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt", "ADI-DTL-TIAA-GRD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt", "ADI-DTL-TIAA-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt", "ADI-DTL-FNL-GRD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt", "ADI-DTL-FNL-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt", "ADI-DTL-TIAA-STD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt", "ADI-DTL-TIAA-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt", "ADI-DTL-FNL-STD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt", "ADI-DTL-FNL-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cis_Prtcpnt_File_View.reset();
        vw_ads_Ia_Rsl2.reset();

        ldaAdsl401.initializeValues();
        ldaAdsl450.initializeValues();
        ldaAdsl6005.initializeValues();
        ldaAdslcntl.initializeValues();

        localVariables.reset();
        pnd_Max_Rslt.setInitialValue(125);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp6000() throws Exception
    {
        super("Adsp6000");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) LS = 132 PS = 55;//Natural: FORMAT ( 02 ) LS = 132 PS = 55;//Natural: FORMAT ( 03 ) LS = 132 PS = 55;//Natural: FORMAT ( 04 ) LS = 136 PS = 55
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 01 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 02 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 03 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 04 )
                                                                                                                                                                          //Natural: PERFORM ADS-CONTROL
        sub_Ads_Control();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Cntrl_Ok.notEquals("Y")))                                                                                                                       //Natural: IF #CNTRL-OK NE 'Y'
        {
            getReports().write(0, "ERROR IN CONTROL RECORD SETUP ");                                                                                                      //Natural: WRITE 'ERROR IN CONTROL RECORD SETUP '
            if (Global.isEscape()) return;
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl401.getVw_ads_Prtcpnt_View().startDatabaseRead                                                                                                             //Natural: READ ADS-PRTCPNT-VIEW BY ADP-SUPER1 STARTING FROM 'O'
        (
        "PART",
        new Wc[] { new Wc("ADP_SUPER1", ">=", "O", WcType.BY) },
        new Oc[] { new Oc("ADP_SUPER1", "ASC") }
        );
        PART:
        while (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().readNextRow("PART")))
        {
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind().notEquals("O")))                                                                              //Natural: IF ADS-PRTCPNT-VIEW.ADP-OPN-CLSD-IND NE 'O'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  WRITE 'OPEN '    /* 022008
            pnd_Part_Date.setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                                             //Natural: MOVE EDITED ADS-PRTCPNT-VIEW.ADP-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #PART-DATE
            //*  WRITE 'RQST-ID        ' ADS-PRTCPNT-DDM-VIEW.RQST-ID
            //*  WRITE 'STATUS-CODE    ' ADS-PRTCPNT-DDM-VIEW.ADP-STTS-CDE-1
            //*  WRITE 'LAST-ACT-DATE  ' ADS-PRTCPNT-DDM-VIEW.ADP-LST-ACTVTY-DTE
            //*  WRITE 'CIS-INT-DATE   ' ADS-CNTRL-DDM-VIEW.ADS-CIS-INTRFCE-DTE
            //*  WRITE 'PART-DATE      ' #PART-DATE-NUMERIC
            //*  WRITE 'CNTL-BUSS-DATE ' ADS-CNTRL-DDM-VIEW.CCT-CNTL-BSNSS-DTE
            //*  WRITE 'ANNT-TYPE-CODE ' ADS-PRTCPNT-DDM-VIEW.ADP-ANNT-TYP-CDE
            //*                                              /* TS 111804
            if (condition(ldaAdslcntl.getAds_Cntl_View_Ads_Rpt_13().notEquals(getZero())))                                                                                //Natural: IF ADS-RPT-13 NE 0
            {
                pnd_Temp_Date_A.setValueEdited(ldaAdslcntl.getAds_Cntl_View_Ads_Rpt_13(),new ReportEditMask("YYYYMMDD"));                                                 //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #TEMP-DATE-A
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Temp_Date_A_Pnd_Temp_Date_N.setValue(ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Dte());                                                              //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #TEMP-DATE-N
            }                                                                                                                                                             //Natural: END-IF
            //*                                              /* TS 111804
            //*  TS 111804
            if (condition(!((ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde_1().equals("T")) && (ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte().greater(ldaAdslcntl.getAds_Cntl_View_Ads_Cis_Intrfce_Dte()))  //Natural: ACCEPT IF ( ADS-PRTCPNT-VIEW.ADP-STTS-CDE-1 = 'T' ) AND ( ADS-PRTCPNT-VIEW.ADP-LST-ACTVTY-DTE > ADS-CNTL-VIEW.ADS-CIS-INTRFCE-DTE ) AND #PART-DATE-NUMERIC LE #TEMP-DATE-N AND ( ADS-PRTCPNT-VIEW.ADP-ANNT-TYP-CDE = 'M' )
                && pnd_Part_Date_Pnd_Part_Date_Numeric.lessOrEqual(pnd_Temp_Date_A_Pnd_Temp_Date_N) && (ldaAdsl401.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde().equals("M")))))
            {
                continue;
            }
            //* *    #PART-DATE-NUMERIC LE ADS-CNTL-VIEW.ADS-CNTL-BSNSS-DTE
            //*  WRITE 'IM HERE  '   /* 022008
            pnd_Error_Ind.reset();                                                                                                                                        //Natural: RESET #ERROR-IND
            //*  WRITE ' PART RQST ID' ADS-PRTCPNT-DDM-VIEW.RQST-ID
            pnd_Cis_Super.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                                                             //Natural: MOVE ADS-PRTCPNT-VIEW.RQST-ID TO #CIS-SUPER
            vw_cis_Prtcpnt_File_View.startDatabaseFind                                                                                                                    //Natural: FIND CIS-PRTCPNT-FILE-VIEW WITH CIS-RQST-ID-KEY = #CIS-SUPER
            (
            "CIS",
            new Wc[] { new Wc("CIS_RQST_ID_KEY", "=", pnd_Cis_Super, WcType.WITH) }
            );
            CIS:
            while (condition(vw_cis_Prtcpnt_File_View.readNextRow("CIS", true)))
            {
                vw_cis_Prtcpnt_File_View.setIfNotFoundControlFlag(false);
                if (condition(vw_cis_Prtcpnt_File_View.getAstCOUNTER().equals(0)))                                                                                        //Natural: IF NO RECORD FOUND
                {
                    getReports().write(0, "NO RECORDS FOUND FOR  ",pnd_Cis_Super);                                                                                        //Natural: WRITE 'NO RECORDS FOUND FOR  ' #CIS-SUPER
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("CIS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  WRITE ' FIND CIS  RQST ID ' CIS-RQST-ID-KEY
                    //*  WRITE ' #CIS-SUPER        ' #CIS-SUPER
                    pnd_Error_Ind.setValue("Y");                                                                                                                          //Natural: MOVE 'Y' TO #ERROR-IND
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-NOREC
                pnd_Total_Processed.nadd(1);                                                                                                                              //Natural: ADD 1 TO #TOTAL-PROCESSED
                                                                                                                                                                          //Natural: PERFORM IA-RESULT-LOOKUP
                sub_Ia_Result_Lookup();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("CIS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Error_Ind.equals("Y")))                                                                                                                 //Natural: IF #ERROR-IND = 'Y'
                {
                    pnd_Total_Processed.nsubtract(1);                                                                                                                     //Natural: SUBTRACT 1 FROM #TOTAL-PROCESSED
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //*  REMOVED 3 LETTERS 02222017
                    //*  REMOVED 2 LETTERS 02222017
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(1, "REQUEST/ID",                                                                                                                     //Natural: DISPLAY ( 01 ) 'REQUEST/ID' RQST-ID 'MIT/LOG-DATE' ADP-MIT-LOG-DTE-TME 'EFFECT/DATE' ADP-EFFCTV-DTE 'OPT/CDE' ADP-ANNTY-OPTN 'GUAR/PER' ADP-GRNTEE-PERIOD 'START/DATE' ADI-ANNTY-STRT-DTE 'END/DATE' #END-DATE 'DA/TIAA/NUMBER' ADI-TIAA-NBRS ( * ) 'IA/TIAA/NUMBER' ADI-IA-TIAA-NBR 'DA/CREF/NUMBER' ADI-CREF-NBRS ( * ) 'IA/CREF/NUMBER' ADI-IA-CREF-NBR
                		ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id(),"MIT/LOG-DATE",
                		ldaAdsl401.getAds_Prtcpnt_View_Adp_Mit_Log_Dte_Tme(),"EFFECT/DATE",
                		ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte(),"OPT/CDE",
                		ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Optn(),"GUAR/PER",
                		ldaAdsl401.getAds_Prtcpnt_View_Adp_Grntee_Period(),"START/DATE",
                		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Annty_Strt_Dte(),"END/DATE",
                		pnd_End_Date,"DA/TIAA/NUMBER",
                		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Nbrs().getValue("*"),"IA/TIAA/NUMBER",
                		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Tiaa_Nbr(),"DA/CREF/NUMBER",
                		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Cref_Nbrs().getValue("*"),"IA/CREF/NUMBER",
                		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Cref_Nbr());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("CIS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PART"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PART"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM SUMMARY-PRINT
        sub_Summary_Print();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //* ***********************************************************************
        //* **                   PRINT CONTROL REPORT                           ***
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-PRINT
        //* ***********************************************************************
        //* ***********************************************************************
        //* **     READ ITS CONTROL RECORD TO EXTRACT DATES                     ***
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADS-CONTROL
        //* ***********************************************************************
        //* ***********************************************************************
        //* **     FIND IA RESULT RECORDS FROM A PARTICIPANT RECORD             ***
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-RESULT-LOOKUP
        //*    'TIAA/IND'                   ADI-TIAA-STTLMNT
        //*    'REA/IND'                    ADI-TIAA-RE-STTLMNT
        //* *************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-ALL-OTHER-PAYMENTS
        //* ***********************************************************************
        //* ***********************************************************************
        //* **     MOVE IA RESULTS FIELDS TO CIS KDO FILE                       ***
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-IA-TIAA-RESULTS-FIELDS
        //* *****************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TIAA-MATURITIES
        //* ***********************************************************************
        //* ***********************************************************************
        //* ***********************************************************************
        //* **     MOVE IA CREF RESULT FIELDS TO CIS KDO FILE                   ***
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-IA-CREF-RESULTS-FIELDS
        //*    'REA/IND'         ADI-TIAA-RE-STTLMNT
    }
    private void sub_Summary_Print() throws Exception                                                                                                                     //Natural: SUMMARY-PRINT
    {
        if (BLNatReinput.isReinput()) return;

        //* *MOVE ADS-CNTL-BSNSS-TMRRW-DTE TO #DATE-CONV
        if (condition(ldaAdslcntl.getAds_Cntl_View_Ads_Rpt_13().notEquals(getZero())))                                                                                    //Natural: IF ADS-RPT-13 NE 0
        {
            pnd_Date_A.setValueEdited(ldaAdslcntl.getAds_Cntl_View_Ads_Rpt_13(),new ReportEditMask("YYYYMMDD"));                                                          //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE-A
            pnd_Date_Conv.setValue(pnd_Date_A_Pnd_Date_N);                                                                                                                //Natural: MOVE #DATE-N TO #DATE-CONV
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Date_Conv.setValue(ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Dte());                                                                                    //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #DATE-CONV
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Print_Date_Pnd_Print_Date_Ccyy.setValue(pnd_Date_Conv_Pnd_Date_Conv_Ccyy);                                                                                    //Natural: MOVE #DATE-CONV-CCYY TO #PRINT-DATE-CCYY
        pnd_Print_Date_Pnd_Print_Date_Mm.setValue(pnd_Date_Conv_Pnd_Date_Conv_Mm);                                                                                        //Natural: MOVE #DATE-CONV-MM TO #PRINT-DATE-MM
        pnd_Print_Date_Pnd_Print_Date_Dd.setValue(pnd_Date_Conv_Pnd_Date_Conv_Dd);                                                                                        //Natural: MOVE #DATE-CONV-DD TO #PRINT-DATE-DD
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(35),"CONTRACT PRODUCTION CONTROL TOTAL - PAYOUT ANNUITES",new ColumnSpacing(15),"Accounting Date: ",pnd_Print_Date,  //Natural: WRITE ( 02 ) 35X 'CONTRACT PRODUCTION CONTROL TOTAL - PAYOUT ANNUITES' 15X 'Accounting Date: ' #PRINT-DATE ( EM = 99/99/9999 ) /
            new ReportEditMask ("99/99/9999"),NEWLINE);
        if (Global.isEscape()) return;
        //*  RS1
        //*  RS1
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(38),"Monthly",new ColumnSpacing(12),"Quarterly",new ColumnSpacing(12),"Semi-Annual",new              //Natural: WRITE ( 02 ) 38X 'Monthly' 12X 'Quarterly' 12X 'Semi-Annual' 12X 'Annual' 12X 'All Modes'
            ColumnSpacing(12),"Annual",new ColumnSpacing(12),"All Modes");
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"I N C O M E  B E N E F I T S",new ColumnSpacing(8),"$ or Units",new ColumnSpacing(11),"$ or Units",new                //Natural: WRITE ( 02 ) 'I N C O M E  B E N E F I T S' 08X '$ or Units' 11X '$ or Units' 12X '$ or Units' 10X '$ or Units' 10X '$ or Units'
            ColumnSpacing(12),"$ or Units",new ColumnSpacing(10),"$ or Units",new ColumnSpacing(10),"$ or Units");
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"----------------------------",new ColumnSpacing(6),"--------------",new ColumnSpacing(7),"--------------",new         //Natural: WRITE ( 02 ) '----------------------------' 06X '--------------' 07X '--------------' 08X '--------------' 06X '--------------' 06X '--------------'
            ColumnSpacing(8),"--------------",new ColumnSpacing(6),"--------------",new ColumnSpacing(6),"--------------");
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Traditional Annuities         ");                                                                                     //Natural: WRITE ( 02 ) 'Traditional Annuities         '
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Standard Guaranteed -         ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_T_Std_Guar_M(), new                     //Natural: WRITE ( 02 ) 'Standard Guaranteed -         ' 4X #TOTAL-T-STD-GUAR-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #TOTAL-T-STD-GUAR-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 8X #TOTAL-T-STD-GUAR-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-T-STD-GUAR-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-T-STD-GUAR-T ( EM = ZZZ,ZZZ,ZZ9.99 )
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_T_Std_Guar_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_T_Std_Guar_S(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_T_Std_Guar_A(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(6),ldaAdsl6005.getPnd_Total_T_Std_Guar_T(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Standard Dividend   -         ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_T_Std_Div_M(), new ReportEditMask       //Natural: WRITE ( 02 ) 'Standard Dividend   -         ' 4X #TOTAL-T-STD-DIV-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #TOTAL-T-STD-DIV-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 8X #TOTAL-T-STD-DIV-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-T-STD-DIV-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-T-STD-DIV-T ( EM = ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_T_Std_Div_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_T_Std_Div_S(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_T_Std_Div_A(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(6),ldaAdsl6005.getPnd_Total_T_Std_Div_T(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Standard Total      -         ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_T_Std_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'Standard Total      -         ' 4X #TOTAL-T-STD-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #TOTAL-T-STD-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 8X #TOTAL-T-STD-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-T-STD-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-T-STD-T ( EM = ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_T_Std_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_T_Std_S(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_T_Std_A(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_T_Std_T(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Graded   Guaranteed -         ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_T_Grd_Guar_M(), new                     //Natural: WRITE ( 02 ) 'Graded   Guaranteed -         ' 4X #TOTAL-T-GRD-GUAR-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #TOTAL-T-GRD-GUAR-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 8X #TOTAL-T-GRD-GUAR-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-T-GRD-GUAR-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-T-GRD-GUAR-T ( EM = ZZZ,ZZZ,ZZ9.99 )
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_T_Grd_Guar_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_T_Grd_Guar_S(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_T_Grd_Guar_A(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(6),ldaAdsl6005.getPnd_Total_T_Grd_Guar_T(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Graded   Dividend   -         ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_T_Grd_Div_M(), new ReportEditMask       //Natural: WRITE ( 02 ) 'Graded   Dividend   -         ' 4X #TOTAL-T-GRD-DIV-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #TOTAL-T-GRD-DIV-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 8X #TOTAL-T-GRD-DIV-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-T-GRD-DIV-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-T-GRD-DIV-T ( EM = ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_T_Grd_Div_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_T_Grd_Div_S(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_T_Grd_Div_A(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(6),ldaAdsl6005.getPnd_Total_T_Grd_Div_T(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Graded   Total                ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_T_Grd_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'Graded   Total                ' 4X #TOTAL-T-GRD-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #TOTAL-T-GRD-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 8X #TOTAL-T-GRD-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-T-GRD-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-T-GRD-T ( EM = ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_T_Grd_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_T_Grd_S(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_T_Grd_A(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_T_Grd_T(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"TPA Guaranteed      -         ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_Tpa_Std_Guar_M(), new                   //Natural: WRITE ( 02 ) 'TPA Guaranteed      -         ' 4X #TOTAL-TPA-STD-GUAR-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #TOTAL-TPA-STD-GUAR-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 8X #TOTAL-TPA-STD-GUAR-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-TPA-STD-GUAR-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-TPA-STD-GUAR-T ( EM = ZZZ,ZZZ,ZZ9.99 )
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_Tpa_Std_Guar_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(8),ldaAdsl6005.getPnd_Total_Tpa_Std_Guar_S(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_Tpa_Std_Guar_A(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_Tpa_Std_Guar_T(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"TPA Dividend        -         ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_Tpa_Std_Div_M(), new                    //Natural: WRITE ( 02 ) 'TPA Dividend        -         ' 4X #TOTAL-TPA-STD-DIV-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #TOTAL-TPA-STD-DIV-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 8X #TOTAL-TPA-STD-DIV-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-TPA-STD-DIV-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-TPA-STD-DIV-T ( EM = ZZZ,ZZZ,ZZ9.99 )
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_Tpa_Std_Div_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_Tpa_Std_Div_S(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_Tpa_Std_Div_A(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(6),ldaAdsl6005.getPnd_Total_Tpa_Std_Div_T(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"TPA Total           -         ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_Tpa_Std_M(), new ReportEditMask         //Natural: WRITE ( 02 ) 'TPA Total           -         ' 4X #TOTAL-TPA-STD-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #TOTAL-TPA-STD-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 8X #TOTAL-TPA-STD-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-TPA-STD-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-TPA-STD-T ( EM = ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_Tpa_Std_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_Tpa_Std_S(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_Tpa_Std_A(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_Tpa_Std_T(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"IPRO Guaranteed     -         ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_Io_Std_Guar_M(), new                    //Natural: WRITE ( 02 ) 'IPRO Guaranteed     -         ' 4X #TOTAL-IO-STD-GUAR-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #TOTAL-IO-STD-GUAR-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 8X #TOTAL-IO-STD-GUAR-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-IO-STD-GUAR-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-IO-STD-GUAR-T ( EM = ZZZ,ZZZ,ZZ9.99 )
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_Io_Std_Guar_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_Io_Std_Guar_S(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_Io_Std_Guar_A(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(6),ldaAdsl6005.getPnd_Total_Io_Std_Guar_T(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"IPRO Dividend       -         ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_Io_Std_Div_M(), new                     //Natural: WRITE ( 02 ) 'IPRO Dividend       -         ' 4X #TOTAL-IO-STD-DIV-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #TOTAL-IO-STD-DIV-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 8X #TOTAL-IO-STD-DIV-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-IO-STD-DIV-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-IO-STD-DIV-T ( EM = ZZZ,ZZZ,ZZ9.99 )
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_Io_Std_Div_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_Io_Std_Div_S(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_Io_Std_Div_A(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(6),ldaAdsl6005.getPnd_Total_Io_Std_Div_T(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"IPRO Total          -         ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_Io_Std_M(), new ReportEditMask          //Natural: WRITE ( 02 ) 'IPRO Total          -         ' 4X #TOTAL-IO-STD-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 7X #TOTAL-IO-STD-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 8X #TOTAL-IO-STD-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-IO-STD-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 6X #TOTAL-IO-STD-T ( EM = ZZZ,ZZZ,ZZ9.99 )
            ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_Io_Std_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_Io_Std_S(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_Io_Std_A(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_Io_Std_T(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"ANNUAL REVALUATION");                                                                                                 //Natural: WRITE ( 02 ) 'ANNUAL REVALUATION'
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"------------------");                                                                                                 //Natural: WRITE ( 02 ) '------------------'
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"TIAA REA                      ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_R_Ann_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'TIAA REA                      ' 4X #TOTAL-R-ANN-M ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #TOTAL-R-ANN-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 8X #TOTAL-R-ANN-S ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-R-ANN-A ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-R-ANN-T ( EM = ZZ,ZZZ,ZZ9.999 ) /
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_R_Ann_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_R_Ann_S(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_R_Ann_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_R_Ann_T(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"TIAA ACCESS                   ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_A_Ann_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'TIAA ACCESS                   ' 4X #TOTAL-A-ANN-M ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #TOTAL-A-ANN-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 8X #TOTAL-A-ANN-S ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-A-ANN-A ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-A-ANN-T ( EM = ZZ,ZZZ,ZZ9.999 ) /
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_A_Ann_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_A_Ann_S(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_A_Ann_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_A_Ann_T(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"CREF Stock                    ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_C_Ann_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'CREF Stock                    ' 4X #TOTAL-C-ANN-M ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #TOTAL-C-ANN-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 8X #TOTAL-C-ANN-S ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-C-ANN-A ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-C-ANN-T ( EM = ZZ,ZZZ,ZZ9.999 )
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_C_Ann_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_C_Ann_S(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_C_Ann_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_C_Ann_T(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"CREF MMA                      ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_M_Ann_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'CREF MMA                      ' 4X #TOTAL-M-ANN-M ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #TOTAL-M-ANN-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 8X #TOTAL-M-ANN-S ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-M-ANN-A ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-M-ANN-T ( EM = ZZ,ZZZ,ZZ9.999 )
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_M_Ann_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_M_Ann_S(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_M_Ann_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_M_Ann_T(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"CREF Social Choice            ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_S_Ann_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'CREF Social Choice            ' 4X #TOTAL-S-ANN-M ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #TOTAL-S-ANN-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 8X #TOTAL-S-ANN-S ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-S-ANN-A ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-S-ANN-T ( EM = ZZ,ZZZ,ZZ9.999 )
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_S_Ann_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_S_Ann_S(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_S_Ann_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_S_Ann_T(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"CREF Bond                     ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_B_Ann_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'CREF Bond                     ' 4X #TOTAL-B-ANN-M ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #TOTAL-B-ANN-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 8X #TOTAL-B-ANN-S ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-B-ANN-A ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-B-ANN-T ( EM = ZZ,ZZZ,ZZ9.999 )
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_B_Ann_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_B_Ann_S(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_B_Ann_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_B_Ann_T(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"CREF Global                   ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_W_Ann_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'CREF Global                   ' 4X #TOTAL-W-ANN-M ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #TOTAL-W-ANN-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 8X #TOTAL-W-ANN-S ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-W-ANN-A ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-W-ANN-T ( EM = ZZ,ZZZ,ZZ9.999 )
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_W_Ann_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_W_Ann_S(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_W_Ann_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_W_Ann_T(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"CREF Growth                   ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_L_Ann_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'CREF Growth                   ' 4X #TOTAL-L-ANN-M ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #TOTAL-L-ANN-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 8X #TOTAL-L-ANN-S ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-L-ANN-A ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-L-ANN-T ( EM = ZZ,ZZZ,ZZ9.999 )
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_L_Ann_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_L_Ann_S(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_L_Ann_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_L_Ann_T(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"CREF Equity Index             ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_E_Ann_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'CREF Equity Index             ' 4X #TOTAL-E-ANN-M ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #TOTAL-E-ANN-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 8X #TOTAL-E-ANN-S ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-E-ANN-A ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-E-ANN-T ( EM = ZZ,ZZZ,ZZ9.999 )
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_E_Ann_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_E_Ann_S(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_E_Ann_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_E_Ann_T(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"CREF ILB                      ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_I_Ann_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'CREF ILB                      ' 4X #TOTAL-I-ANN-M ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #TOTAL-I-ANN-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 8X #TOTAL-I-ANN-S ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-I-ANN-A ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-I-ANN-T ( EM = ZZ,ZZZ,ZZ9.999 )
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_I_Ann_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_I_Ann_S(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_I_Ann_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_I_Ann_T(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"MONTHLY REVALUATION");                                                                                                //Natural: WRITE ( 02 ) 'MONTHLY REVALUATION'
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"------------------");                                                                                                 //Natural: WRITE ( 02 ) '------------------'
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"TIAA REA                      ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_R_Mth_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'TIAA REA                      ' 4X #TOTAL-R-MTH-M ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #TOTAL-R-MTH-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 8X #TOTAL-R-MTH-S ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-R-MTH-A ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-R-MTH-T ( EM = ZZ,ZZZ,ZZ9.999 )
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_R_Mth_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_R_Mth_S(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_R_Mth_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_R_Mth_T(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"TIAA ACCESS                   ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_A_Mth_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'TIAA ACCESS                   ' 4X #TOTAL-A-MTH-M ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #TOTAL-A-MTH-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 8X #TOTAL-A-MTH-S ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-A-MTH-A ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-A-MTH-T ( EM = ZZ,ZZZ,ZZ9.999 )
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_A_Mth_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_A_Mth_S(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_A_Mth_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_A_Mth_T(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"CREF Stock                    ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_C_Mth_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'CREF Stock                    ' 4X #TOTAL-C-MTH-M ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #TOTAL-C-MTH-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 8X #TOTAL-C-MTH-S ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-C-MTH-A ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-C-MTH-T ( EM = ZZ,ZZZ,ZZ9.999 )
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_C_Mth_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_C_Mth_S(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_C_Mth_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_C_Mth_T(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"CREF MMA                      ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_M_Mth_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'CREF MMA                      ' 4X #TOTAL-M-MTH-M ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #TOTAL-M-MTH-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 8X #TOTAL-M-MTH-S ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-M-MTH-A ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-M-MTH-T ( EM = ZZ,ZZZ,ZZ9.999 )
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_M_Mth_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_M_Mth_S(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_M_Mth_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_M_Mth_T(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"CREF Social Choice            ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_S_Mth_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'CREF Social Choice            ' 4X #TOTAL-S-MTH-M ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #TOTAL-S-MTH-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 8X #TOTAL-S-MTH-S ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-S-MTH-A ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-S-MTH-T ( EM = ZZ,ZZZ,ZZ9.999 )
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_S_Mth_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_S_Mth_S(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_S_Mth_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_S_Mth_T(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"CREF Bond                     ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_B_Mth_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'CREF Bond                     ' 4X #TOTAL-B-MTH-M ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #TOTAL-B-MTH-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 8X #TOTAL-B-MTH-S ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-B-MTH-A ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-B-MTH-T ( EM = ZZ,ZZZ,ZZ9.999 )
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_B_Mth_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_B_Mth_S(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_B_Mth_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_B_Mth_T(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"CREF Global                   ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_W_Mth_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'CREF Global                   ' 4X #TOTAL-W-MTH-M ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #TOTAL-W-MTH-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 8X #TOTAL-W-MTH-S ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-W-MTH-A ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-W-MTH-T ( EM = ZZ,ZZZ,ZZ9.999 )
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_W_Mth_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_W_Mth_S(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_W_Mth_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_W_Mth_T(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"CREF Growth                   ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_L_Mth_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'CREF Growth                   ' 4X #TOTAL-L-MTH-M ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #TOTAL-L-MTH-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 8X #TOTAL-L-MTH-S ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-L-MTH-A ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-L-MTH-T ( EM = ZZ,ZZZ,ZZ9.999 )
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_L_Mth_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_L_Mth_S(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_L_Mth_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_L_Mth_T(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"CREF Equity Index             ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_E_Mth_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'CREF Equity Index             ' 4X #TOTAL-E-MTH-M ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #TOTAL-E-MTH-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 8X #TOTAL-E-MTH-S ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-E-MTH-A ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-E-MTH-T ( EM = ZZ,ZZZ,ZZ9.999 )
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_E_Mth_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_E_Mth_S(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_E_Mth_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_E_Mth_T(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"CREF ILB                      ",new ColumnSpacing(4),ldaAdsl6005.getPnd_Total_I_Mth_M(), new ReportEditMask           //Natural: WRITE ( 02 ) 'CREF ILB                      ' 4X #TOTAL-I-MTH-M ( EM = ZZ,ZZZ,ZZ9.999 ) 7X #TOTAL-I-MTH-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 8X #TOTAL-I-MTH-S ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-I-MTH-A ( EM = ZZ,ZZZ,ZZ9.999 ) 6X #TOTAL-I-MTH-T ( EM = ZZ,ZZZ,ZZ9.999 ) /
            ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_I_Mth_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(8),ldaAdsl6005.getPnd_Total_I_Mth_S(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_I_Mth_A(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_I_Mth_T(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE);
        if (Global.isEscape()) return;
        //*  RSXXX
        //*  RS0
        //*  RS0
        //*  RS0
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 02 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, ReportOption.NOTITLE,"S U M M A R Y   T O T A L S",new ColumnSpacing(82),"G R A N D  T O T A L S");                                         //Natural: WRITE ( 02 ) 'S U M M A R Y   T O T A L S' 82X 'G R A N D  T O T A L S'
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"---------------------------",new ColumnSpacing(82),"----------------------");                                         //Natural: WRITE ( 02 ) '---------------------------' 82X '----------------------'
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"Traditional Annuity:      ");                                                                                         //Natural: WRITE ( 02 ) 'Traditional Annuity:      '
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(2),"Contracts Issued       ",new ColumnSpacing(12),ldaAdsl6005.getPnd_Total_T_Issued_M(),            //Natural: WRITE ( 02 ) 2X 'Contracts Issued       ' 12X #TOTAL-T-ISSUED-M ( EM = ZZZ,ZZZ,ZZ9 ) 10X #TOTAL-T-ISSUED-Q ( EM = ZZZ,ZZZ,ZZ9 ) 10X #TOTAL-T-ISSUED-S ( EM = ZZZ,ZZZ,ZZ9 ) 10X #TOTAL-T-ISSUED-A ( EM = ZZZ,ZZZ,ZZ9 ) 09X #TOTAL-T-ISSUED-T ( EM = ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(10),ldaAdsl6005.getPnd_Total_T_Issued_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(10),ldaAdsl6005.getPnd_Total_T_Issued_S(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(10),ldaAdsl6005.getPnd_Total_T_Issued_A(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(9),ldaAdsl6005.getPnd_Total_T_Issued_T(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(2),"Accumulation Applied   ",new ColumnSpacing(9),ldaAdsl6005.getPnd_Total_T_Applied_M(),            //Natural: WRITE ( 02 ) 2X 'Accumulation Applied   ' 09X #TOTAL-T-APPLIED-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-T-APPLIED-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-T-APPLIED-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-T-APPLIED-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 06X #TOTAL-T-APPLIED-T ( EM = ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_T_Applied_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(7),ldaAdsl6005.getPnd_Total_T_Applied_S(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_T_Applied_A(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_T_Applied_T(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(2),"Annuity Income(dollars)",new ColumnSpacing(9),ldaAdsl6005.getPnd_Total_T_Income_M(),             //Natural: WRITE ( 02 ) 2X 'Annuity Income(dollars)' 09X #TOTAL-T-INCOME-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-T-INCOME-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-T-INCOME-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-T-INCOME-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 06X #TOTAL-T-INCOME-T ( EM = ZZZ,ZZZ,ZZ9.99 ) /
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_T_Income_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(7),ldaAdsl6005.getPnd_Total_T_Income_S(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_T_Income_A(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_T_Income_T(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"TPA:                      ");                                                                                         //Natural: WRITE ( 02 ) 'TPA:                      '
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(2),"Contracts Issued       ",new ColumnSpacing(12),ldaAdsl6005.getPnd_Total_Tpa_Issued_M(),          //Natural: WRITE ( 02 ) 2X 'Contracts Issued       ' 12X #TOTAL-TPA-ISSUED-M ( EM = ZZZ,ZZZ,ZZ9 ) 10X #TOTAL-TPA-ISSUED-Q ( EM = ZZZ,ZZZ,ZZ9 ) 10X #TOTAL-TPA-ISSUED-S ( EM = ZZZ,ZZZ,ZZ9 ) 10X #TOTAL-TPA-ISSUED-A ( EM = ZZZ,ZZZ,ZZ9 ) 09X #TOTAL-TPA-ISSUED-T ( EM = ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(10),ldaAdsl6005.getPnd_Total_Tpa_Issued_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(10),ldaAdsl6005.getPnd_Total_Tpa_Issued_S(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(10),ldaAdsl6005.getPnd_Total_Tpa_Issued_A(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(9),ldaAdsl6005.getPnd_Total_Tpa_Issued_T(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(2),"Accumulation Applied   ",new ColumnSpacing(9),ldaAdsl6005.getPnd_Total_Tpa_Applied_M(),          //Natural: WRITE ( 02 ) 2X 'Accumulation Applied   ' 09X #TOTAL-TPA-APPLIED-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-TPA-APPLIED-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-TPA-APPLIED-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-TPA-APPLIED-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 06X #TOTAL-TPA-APPLIED-T ( EM = ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_Tpa_Applied_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(7),ldaAdsl6005.getPnd_Total_Tpa_Applied_S(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_Tpa_Applied_A(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_Tpa_Applied_T(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(2),"Annuity Income(dollars)",new ColumnSpacing(9),ldaAdsl6005.getPnd_Total_Tpa_Income_M(),           //Natural: WRITE ( 02 ) 2X 'Annuity Income(dollars)' 09X #TOTAL-TPA-INCOME-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-TPA-INCOME-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-TPA-INCOME-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-TPA-INCOME-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 06X #TOTAL-TPA-INCOME-T ( EM = ZZZ,ZZZ,ZZ9.99 ) /
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_Tpa_Income_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(7),ldaAdsl6005.getPnd_Total_Tpa_Income_S(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_Tpa_Income_A(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_Tpa_Income_T(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"IPRO:                     ");                                                                                         //Natural: WRITE ( 02 ) 'IPRO:                     '
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(2),"Contracts Issued       ",new ColumnSpacing(12),ldaAdsl6005.getPnd_Total_Io_Issued_M(),           //Natural: WRITE ( 02 ) 2X 'Contracts Issued       ' 12X #TOTAL-IO-ISSUED-M ( EM = ZZZ,ZZZ,ZZ9 ) 10X #TOTAL-IO-ISSUED-Q ( EM = ZZZ,ZZZ,ZZ9 ) 10X #TOTAL-IO-ISSUED-S ( EM = ZZZ,ZZZ,ZZ9 ) 10X #TOTAL-IO-ISSUED-A ( EM = ZZZ,ZZZ,ZZ9 ) 09X #TOTAL-IO-ISSUED-T ( EM = ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(10),ldaAdsl6005.getPnd_Total_Io_Issued_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(10),ldaAdsl6005.getPnd_Total_Io_Issued_S(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(10),ldaAdsl6005.getPnd_Total_Io_Issued_A(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(9),ldaAdsl6005.getPnd_Total_Io_Issued_T(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(2),"Accumulation Applied   ",new ColumnSpacing(9),ldaAdsl6005.getPnd_Total_Io_Applied_M(),           //Natural: WRITE ( 02 ) 2X 'Accumulation Applied   ' 09X #TOTAL-IO-APPLIED-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-IO-APPLIED-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-IO-APPLIED-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-IO-APPLIED-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 06X #TOTAL-IO-APPLIED-T ( EM = ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_Io_Applied_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(7),ldaAdsl6005.getPnd_Total_Io_Applied_S(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_Io_Applied_A(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_Io_Applied_T(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(2),"Annuity Income(dollars)",new ColumnSpacing(9),ldaAdsl6005.getPnd_Total_Io_Income_M(),            //Natural: WRITE ( 02 ) 2X 'Annuity Income(dollars)' 09X #TOTAL-IO-INCOME-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-IO-INCOME-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-IO-INCOME-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-IO-INCOME-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 06X #TOTAL-IO-INCOME-T ( EM = ZZZ,ZZZ,ZZ9.99 ) /
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_Io_Income_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(7),ldaAdsl6005.getPnd_Total_Io_Income_S(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_Io_Income_A(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_Io_Income_T(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"TIAA Real Estate Account: ");                                                                                         //Natural: WRITE ( 02 ) 'TIAA Real Estate Account: '
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(2),"Contracts Issued       ",new ColumnSpacing(12),ldaAdsl6005.getPnd_Total_R_Issued_M(),            //Natural: WRITE ( 02 ) 2X 'Contracts Issued       ' 12X #TOTAL-R-ISSUED-M ( EM = ZZZ,ZZZ,ZZ9 ) 10X #TOTAL-R-ISSUED-Q ( EM = ZZZ,ZZZ,ZZ9 ) 10X #TOTAL-R-ISSUED-S ( EM = ZZZ,ZZZ,ZZ9 ) 10X #TOTAL-R-ISSUED-A ( EM = ZZZ,ZZZ,ZZ9 ) 09X #TOTAL-R-ISSUED-T ( EM = ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(10),ldaAdsl6005.getPnd_Total_R_Issued_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(10),ldaAdsl6005.getPnd_Total_R_Issued_S(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(10),ldaAdsl6005.getPnd_Total_R_Issued_A(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(9),ldaAdsl6005.getPnd_Total_R_Issued_T(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(2),"Accumulation Applied   ",new ColumnSpacing(9),ldaAdsl6005.getPnd_Total_R_Applied_M(),            //Natural: WRITE ( 02 ) 2X 'Accumulation Applied   ' 09X #TOTAL-R-APPLIED-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-R-APPLIED-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-R-APPLIED-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-R-APPLIED-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 06X #TOTAL-R-APPLIED-T ( EM = ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_R_Applied_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(7),ldaAdsl6005.getPnd_Total_R_Applied_S(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_R_Applied_A(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_R_Applied_T(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(2),"Annuity Income(units)  ",new ColumnSpacing(9),ldaAdsl6005.getPnd_Total_R_Income_M(),             //Natural: WRITE ( 02 ) 2X 'Annuity Income(units)  ' 09X #TOTAL-R-INCOME-M ( EM = ZZ,ZZZ,ZZ9.999 ) 07X #TOTAL-R-INCOME-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 07X #TOTAL-R-INCOME-S ( EM = ZZ,ZZZ,ZZ9.999 ) 07X #TOTAL-R-INCOME-A ( EM = ZZ,ZZZ,ZZ9.999 ) 06X #TOTAL-R-INCOME-T ( EM = ZZ,ZZZ,ZZ9.999 ) /
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_R_Income_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(7),ldaAdsl6005.getPnd_Total_R_Income_S(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_R_Income_A(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_R_Income_T(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"TIAA Access Account: ");                                                                                              //Natural: WRITE ( 02 ) 'TIAA Access Account: '
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(2),"Contracts Issued       ",new ColumnSpacing(12),ldaAdsl6005.getPnd_Total_A_Issued_M(),            //Natural: WRITE ( 02 ) 2X 'Contracts Issued       ' 12X #TOTAL-A-ISSUED-M ( EM = ZZZ,ZZZ,ZZ9 ) 10X #TOTAL-A-ISSUED-Q ( EM = ZZZ,ZZZ,ZZ9 ) 10X #TOTAL-A-ISSUED-S ( EM = ZZZ,ZZZ,ZZ9 ) 10X #TOTAL-A-ISSUED-A ( EM = ZZZ,ZZZ,ZZ9 ) 09X #TOTAL-A-ISSUED-T ( EM = ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(10),ldaAdsl6005.getPnd_Total_A_Issued_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(10),ldaAdsl6005.getPnd_Total_A_Issued_S(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(10),ldaAdsl6005.getPnd_Total_A_Issued_A(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(9),ldaAdsl6005.getPnd_Total_A_Issued_T(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(2),"Accumulation Applied   ",new ColumnSpacing(9),ldaAdsl6005.getPnd_Total_A_Applied_M(),            //Natural: WRITE ( 02 ) 2X 'Accumulation Applied   ' 09X #TOTAL-A-APPLIED-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-A-APPLIED-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-A-APPLIED-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-A-APPLIED-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 06X #TOTAL-A-APPLIED-T ( EM = ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_A_Applied_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(7),ldaAdsl6005.getPnd_Total_A_Applied_S(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_A_Applied_A(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_A_Applied_T(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(2),"Annuity Income(units)  ",new ColumnSpacing(9),ldaAdsl6005.getPnd_Total_A_Income_M(),             //Natural: WRITE ( 02 ) 2X 'Annuity Income(units)  ' 09X #TOTAL-A-INCOME-M ( EM = ZZ,ZZZ,ZZ9.999 ) 07X #TOTAL-A-INCOME-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 07X #TOTAL-A-INCOME-S ( EM = ZZ,ZZZ,ZZ9.999 ) 07X #TOTAL-A-INCOME-A ( EM = ZZ,ZZZ,ZZ9.999 ) 06X #TOTAL-A-INCOME-T ( EM = ZZ,ZZZ,ZZ9.999 ) /
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_A_Income_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(7),ldaAdsl6005.getPnd_Total_A_Income_S(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_A_Income_A(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_A_Income_T(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"CREF:                     ");                                                                                         //Natural: WRITE ( 02 ) 'CREF:                     '
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(2),"Contracts Issued       ",new ColumnSpacing(12),ldaAdsl6005.getPnd_Total_C_Issued_M(),            //Natural: WRITE ( 02 ) 2X 'Contracts Issued       ' 12X #TOTAL-C-ISSUED-M ( EM = ZZZ,ZZZ,ZZ9 ) 10X #TOTAL-C-ISSUED-Q ( EM = ZZZ,ZZZ,ZZ9 ) 10X #TOTAL-C-ISSUED-S ( EM = ZZZ,ZZZ,ZZ9 ) 10X #TOTAL-C-ISSUED-A ( EM = ZZZ,ZZZ,ZZ9 ) 09X #TOTAL-C-ISSUED-T ( EM = ZZZ,ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(10),ldaAdsl6005.getPnd_Total_C_Issued_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(10),ldaAdsl6005.getPnd_Total_C_Issued_S(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(10),ldaAdsl6005.getPnd_Total_C_Issued_A(), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new ColumnSpacing(9),ldaAdsl6005.getPnd_Total_C_Issued_T(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(2),"Accumulation Applied   ",new ColumnSpacing(9),ldaAdsl6005.getPnd_Total_C_Applied_M(),            //Natural: WRITE ( 02 ) 2X 'Accumulation Applied   ' 09X #TOTAL-C-APPLIED-M ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-C-APPLIED-Q ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-C-APPLIED-S ( EM = ZZZ,ZZZ,ZZ9.99 ) 07X #TOTAL-C-APPLIED-A ( EM = ZZZ,ZZZ,ZZ9.99 ) 06X #TOTAL-C-APPLIED-T ( EM = ZZZ,ZZZ,ZZ9.99 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_C_Applied_Q(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
            ColumnSpacing(7),ldaAdsl6005.getPnd_Total_C_Applied_S(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_C_Applied_A(), 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_C_Applied_T(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(2),"Annuity Income(units)  ",new ColumnSpacing(9),ldaAdsl6005.getPnd_Total_C_Income_M(),             //Natural: WRITE ( 02 ) 2X 'Annuity Income(units)  ' 09X #TOTAL-C-INCOME-M ( EM = ZZ,ZZZ,ZZ9.999 ) 07X #TOTAL-C-INCOME-Q ( EM = ZZ,ZZZ,ZZ9.999 ) 07X #TOTAL-C-INCOME-S ( EM = ZZ,ZZZ,ZZ9.999 ) 07X #TOTAL-C-INCOME-A ( EM = ZZ,ZZZ,ZZ9.999 ) 06X #TOTAL-C-INCOME-T ( EM = ZZ,ZZZ,ZZ9.999 ) /
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_C_Income_Q(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new 
            ColumnSpacing(7),ldaAdsl6005.getPnd_Total_C_Income_S(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(7),ldaAdsl6005.getPnd_Total_C_Income_A(), 
            new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),new ColumnSpacing(6),ldaAdsl6005.getPnd_Total_C_Income_T(), new ReportEditMask ("ZZ,ZZZ,ZZ9.999"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(107),"Printed on ",Global.getDATU());                                                                //Natural: WRITE ( 02 ) 107X 'Printed on ' *DATU
        if (Global.isEscape()) return;
    }
    private void sub_Ads_Control() throws Exception                                                                                                                       //Natural: ADS-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        ldaAdslcntl.getVw_ads_Cntl_View().startDatabaseRead                                                                                                               //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "CNTRL",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        CNTRL:
        while (condition(ldaAdslcntl.getVw_ads_Cntl_View().readNextRow("CNTRL")))
        {
            if (condition(ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde().notEquals("F")))                                                                           //Natural: IF ADS-CNTL-RCRD-TYP-CDE NE 'F'
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cntrl_Total.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #CNTRL-TOTAL
            pnd_Cntrl_Ok.setValue("Y");                                                                                                                                   //Natural: MOVE 'Y' TO #CNTRL-OK
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Ia_Result_Lookup() throws Exception                                                                                                                  //Natural: IA-RESULT-LOOKUP
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Error_Ind.reset();                                                                                                                                            //Natural: RESET #ERROR-IND
        pnd_Adi_Super1_Pnd_Rqst_Id_Ia.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                                                 //Natural: MOVE ADS-PRTCPNT-VIEW.RQST-ID TO #RQST-ID-IA
        pnd_Adi_Super1_Pnd_Adi_Record_Type.setValue("ACT");                                                                                                               //Natural: MOVE 'ACT' TO #ADI-RECORD-TYPE
        pnd_Adi_Super1_Pnd_Adi_Sqnce_Nbr.setValue(1);                                                                                                                     //Natural: MOVE 1 TO #ADI-SQNCE-NBR
        ldaAdsl450.getVw_ads_Ia_Rslt_View().startDatabaseFind                                                                                                             //Natural: FIND ADS-IA-RSLT-VIEW WITH ADI-SUPER1 = #ADI-SUPER1
        (
        "FIND01",
        new Wc[] { new Wc("ADI_SUPER1", "=", pnd_Adi_Super1, WcType.WITH) }
        );
        FIND01:
        while (condition(ldaAdsl450.getVw_ads_Ia_Rslt_View().readNextRow("FIND01", true)))
        {
            ldaAdsl450.getVw_ads_Ia_Rslt_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAdsl450.getVw_ads_Ia_Rslt_View().getAstCOUNTER().equals(0)))                                                                                 //Natural: IF NO RECORD FOUND
            {
                pnd_Error_Ind.setValue("Y");                                                                                                                              //Natural: MOVE 'Y' TO #ERROR-IND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Re_Sttlmnt().equals(" ") && ldaAdsl450.getAds_Ia_Rslt_View_Adi_Cref_Sttlmnt().equals(" ")))             //Natural: IF ADI-TIAA-RE-STTLMNT = ' ' AND ADI-CREF-STTLMNT = ' '
            {
                pnd_Tiaa_Only.setValue("Y");                                                                                                                              //Natural: MOVE 'Y' TO #TIAA-ONLY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tiaa_Only.setValue("N");                                                                                                                              //Natural: MOVE 'N' TO #TIAA-ONLY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Finl_Periodic_Py_Dte().greater(getZero())))                                                                  //Natural: IF ADI-FINL-PERIODIC-PY-DTE > 0
            {
                pnd_End_Date.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Finl_Periodic_Py_Dte());                                                                         //Natural: MOVE ADI-FINL-PERIODIC-PY-DTE TO #END-DATE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_End_Date.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Finl_Prtl_Py_Dte());                                                                             //Natural: MOVE ADI-FINL-PRTL-PY-DTE TO #END-DATE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ADD-ALL-OTHER-PAYMENTS
            sub_Add_All_Other_Payments();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM MOVE-IA-TIAA-RESULTS-FIELDS
            sub_Move_Ia_Tiaa_Results_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Tpa_Io_Ind.notEquals(" ")))                                                                                                                 //Natural: IF #TPA-IO-IND NOT = ' '
            {
                pnd_Tiaa_Ind.setValue(pnd_Tpa_Io_Ind);                                                                                                                    //Natural: MOVE #TPA-IO-IND TO #TIAA-IND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Tiaa_Ind.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Sttlmnt());                                                                                 //Natural: MOVE ADI-TIAA-STTLMNT TO #TIAA-IND
                //*  RS0
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(3, new ColumnSpacing(5),"UNIQUE/ID",                                                                                                     //Natural: DISPLAY ( 03 ) 5X 'UNIQUE/ID' ADS-PRTCPNT-VIEW.ADP-UNIQUE-ID 'IA/TIAA/NUMBER' ADI-IA-TIAA-NBR 5X 'PAYMENT/MODE' ADP-PYMNT-MODE 5X 'TIAA/IND' #TIAA-ONLY 'REA/ACS/IND' ADI-TIAA-RE-STTLMNT 'CREF/IND' ADI-CREF-STTLMNT 'TIAA/STANDARD/TPA-IPRO' #IA-TIAA-STD-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) 5X 'TIAA/GRADED' #IA-TIAA-GRD-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) 5X 'TIAA/DA/STANDARD/TPA-IPRO' #DA-TIAA-STD-AMT ( EM = ZZZ,ZZZ,ZZ9.99 ) 5X 'TIAA/DA/GRADED' #DA-TIAA-GRD-AMT ( EM = ZZZ,ZZZ,ZZ9.99 )
            		ldaAdsl401.getAds_Prtcpnt_View_Adp_Unique_Id(),"IA/TIAA/NUMBER",
            		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Tiaa_Nbr(),new ColumnSpacing(5),"PAYMENT/MODE",
            		ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode(),new ColumnSpacing(5),"TIAA/IND",
            		pnd_Tiaa_Only,"REA/ACS/IND",
            		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Re_Sttlmnt(),"CREF/IND",
            		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Cref_Sttlmnt(),"TIAA/STANDARD/TPA-IPRO",
            		pnd_Ia_Tiaa_Std_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(5),"TIAA/GRADED",
            		pnd_Ia_Tiaa_Grd_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(5),"TIAA/DA/STANDARD/TPA-IPRO",
            		pnd_Da_Tiaa_Std_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(5),"TIAA/DA/GRADED",
            		pnd_Da_Tiaa_Grd_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM MOVE-IA-CREF-RESULTS-FIELDS
            sub_Move_Ia_Cref_Results_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Add_All_Other_Payments() throws Exception                                                                                                            //Natural: ADD-ALL-OTHER-PAYMENTS
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************************
        //*  WRITE 'IN ALL OTHER PAYMENTS'
        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode_1().equals(1)))                                                                                       //Natural: IF ADI-PYMNT-MODE-1 = 1
        {
            if (condition(pnd_Tiaa_Only.equals("Y")))                                                                                                                     //Natural: IF #TIAA-ONLY = 'Y'
            {
                ldaAdsl6005.getPnd_Total_T_Issued_M().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-T-ISSUED-M
                ldaAdsl6005.getPnd_Total_T_Issued_T().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-T-ISSUED-T
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl6005.getPnd_Total_R_Issued_M().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-R-ISSUED-M
                ldaAdsl6005.getPnd_Total_R_Issued_T().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-R-ISSUED-T
                //*  RS0
                ldaAdsl6005.getPnd_Total_A_Issued_M().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-A-ISSUED-M
                //*  RS0
                ldaAdsl6005.getPnd_Total_A_Issued_T().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-A-ISSUED-T
                ldaAdsl6005.getPnd_Total_T_Issued_M().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-T-ISSUED-M
                ldaAdsl6005.getPnd_Total_T_Issued_T().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-T-ISSUED-T
                ldaAdsl6005.getPnd_Total_C_Issued_M().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-C-ISSUED-M
                ldaAdsl6005.getPnd_Total_C_Issued_T().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-C-ISSUED-T
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode_1().equals(6)))                                                                                       //Natural: IF ADI-PYMNT-MODE-1 = 6
        {
            if (condition(pnd_Tiaa_Only.equals("Y")))                                                                                                                     //Natural: IF #TIAA-ONLY = 'Y'
            {
                ldaAdsl6005.getPnd_Total_T_Issued_Q().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-T-ISSUED-Q
                ldaAdsl6005.getPnd_Total_T_Issued_T().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-T-ISSUED-T
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl6005.getPnd_Total_R_Issued_Q().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-R-ISSUED-Q
                ldaAdsl6005.getPnd_Total_R_Issued_T().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-R-ISSUED-T
                //*  RS0
                ldaAdsl6005.getPnd_Total_A_Issued_Q().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-A-ISSUED-Q
                //*  RS0
                ldaAdsl6005.getPnd_Total_A_Issued_T().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-A-ISSUED-T
                ldaAdsl6005.getPnd_Total_T_Issued_Q().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-T-ISSUED-Q
                ldaAdsl6005.getPnd_Total_T_Issued_T().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-T-ISSUED-T
                ldaAdsl6005.getPnd_Total_C_Issued_Q().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-C-ISSUED-Q
                ldaAdsl6005.getPnd_Total_C_Issued_T().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-C-ISSUED-T
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode_1().equals(7)))                                                                                       //Natural: IF ADI-PYMNT-MODE-1 = 7
        {
            if (condition(pnd_Tiaa_Only.equals("Y")))                                                                                                                     //Natural: IF #TIAA-ONLY = 'Y'
            {
                ldaAdsl6005.getPnd_Total_T_Issued_S().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-T-ISSUED-S
                ldaAdsl6005.getPnd_Total_T_Issued_T().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-T-ISSUED-T
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl6005.getPnd_Total_R_Issued_S().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-R-ISSUED-S
                ldaAdsl6005.getPnd_Total_R_Issued_T().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-R-ISSUED-T
                //*  RS0
                ldaAdsl6005.getPnd_Total_A_Issued_S().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-A-ISSUED-S
                //*  RS0
                ldaAdsl6005.getPnd_Total_A_Issued_T().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-A-ISSUED-T
                ldaAdsl6005.getPnd_Total_T_Issued_S().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-T-ISSUED-S
                ldaAdsl6005.getPnd_Total_T_Issued_T().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-T-ISSUED-T
                ldaAdsl6005.getPnd_Total_C_Issued_S().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-C-ISSUED-S
                ldaAdsl6005.getPnd_Total_C_Issued_T().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-C-ISSUED-T
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode_1().equals(8)))                                                                                       //Natural: IF ADI-PYMNT-MODE-1 = 8
        {
            if (condition(pnd_Tiaa_Only.equals("Y")))                                                                                                                     //Natural: IF #TIAA-ONLY = 'Y'
            {
                ldaAdsl6005.getPnd_Total_T_Issued_A().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-T-ISSUED-A
                ldaAdsl6005.getPnd_Total_T_Issued_T().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-T-ISSUED-T
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl6005.getPnd_Total_R_Issued_A().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-R-ISSUED-A
                ldaAdsl6005.getPnd_Total_R_Issued_T().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-R-ISSUED-T
                //*  RS0
                ldaAdsl6005.getPnd_Total_A_Issued_A().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-A-ISSUED-A
                //*  RS0
                ldaAdsl6005.getPnd_Total_A_Issued_T().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-A-ISSUED-T
                ldaAdsl6005.getPnd_Total_T_Issued_A().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-T-ISSUED-A
                ldaAdsl6005.getPnd_Total_T_Issued_T().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-T-ISSUED-T
                ldaAdsl6005.getPnd_Total_C_Issued_A().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-C-ISSUED-A
                ldaAdsl6005.getPnd_Total_C_Issued_T().nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-C-ISSUED-T
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Move_Ia_Tiaa_Results_Fields() throws Exception                                                                                                       //Natural: MOVE-IA-TIAA-RESULTS-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Counter_Start.reset();                                                                                                                                        //Natural: RESET #COUNTER-START #DA-TIAA-GRD-AMT #DA-TIAA-STD-AMT #IA-TIAA-GRD-AMT #IA-TIAA-STD-AMT #TPA-IO-IND
        pnd_Da_Tiaa_Grd_Amt.reset();
        pnd_Da_Tiaa_Std_Amt.reset();
        pnd_Ia_Tiaa_Grd_Amt.reset();
        pnd_Ia_Tiaa_Std_Amt.reset();
        pnd_Tpa_Io_Ind.reset();
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-MATURITIES
        sub_Calculate_Tiaa_Maturities();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Calculate_Tiaa_Maturities() throws Exception                                                                                                         //Natural: CALCULATE-TIAA-MATURITIES
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************
        //*  WRITE 'IN MATURITIES '
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Counter_Start.equals(ldaAdsl450.getAds_Ia_Rslt_View_Count_Castadi_Dtl_Tiaa_Data()))) {break;}                                               //Natural: UNTIL #COUNTER-START = ADS-IA-RSLT-VIEW.C*ADI-DTL-TIAA-DATA
            //* *FOR #COUNTER-START 1 TO 99
            pnd_Counter_Start.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #COUNTER-START
            //*  033012 - ADDED ADS-IA-RSLT-VIEW TO ALL TIAA FIELDS FOR UNIQUENESS
            ldaAdsl6005.getPnd_Total_T_Grd_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Grd_T()), ldaAdsl6005.getPnd_Total_T_Grd_T().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-T
            ldaAdsl6005.getPnd_Total_T_Grd_Guar_T().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Counter_Start));                        //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-GUAR-T
            ldaAdsl6005.getPnd_Total_T_Grd_Div_T().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Counter_Start));                         //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-DIV-T
            ldaAdsl6005.getPnd_Total_T_Std_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Std_T()), ldaAdsl6005.getPnd_Total_T_Std_T().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-T
            ldaAdsl6005.getPnd_Total_T_Std_Guar_T().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Counter_Start));                        //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-GUAR-T
            ldaAdsl6005.getPnd_Total_T_Std_Div_T().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Counter_Start));                         //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-DIV-T
            ldaAdsl6005.getPnd_Total_T_Income_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Income_T()), ldaAdsl6005.getPnd_Total_T_Income_T().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-INCOME-T
            pnd_Ia_Tiaa_Grd_Amt.compute(new ComputeParameters(false, pnd_Ia_Tiaa_Grd_Amt), pnd_Ia_Tiaa_Grd_Amt.add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #IA-TIAA-GRD-AMT
            pnd_Ia_Tiaa_Std_Amt.compute(new ComputeParameters(false, pnd_Ia_Tiaa_Std_Amt), pnd_Ia_Tiaa_Std_Amt.add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #IA-TIAA-STD-AMT
            //*  DA SIDE
            ldaAdsl6005.getPnd_Total_T_Applied_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Applied_T()), ldaAdsl6005.getPnd_Total_T_Applied_T().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-GRD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-STD-AMT ( #COUNTER-START ) TO #TOTAL-T-APPLIED-T
            pnd_Da_Tiaa_Grd_Amt.nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_Counter_Start));                                               //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-GRD-AMT ( #COUNTER-START ) TO #DA-TIAA-GRD-AMT
            pnd_Da_Tiaa_Std_Amt.nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(pnd_Counter_Start));                                               //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-STD-AMT ( #COUNTER-START ) TO #DA-TIAA-STD-AMT
            //*  MONTHLY
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode_1().equals(1)))                                                                                   //Natural: IF ADI-PYMNT-MODE-1 = 1
            {
                ldaAdsl6005.getPnd_Total_T_Grd_M().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Grd_M()), ldaAdsl6005.getPnd_Total_T_Grd_M().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-M
                ldaAdsl6005.getPnd_Total_T_Grd_Guar_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Counter_Start));                    //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-GUAR-M
                ldaAdsl6005.getPnd_Total_T_Grd_Div_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Counter_Start));                     //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-DIV-M
                ldaAdsl6005.getPnd_Total_T_Std_M().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Std_M()), ldaAdsl6005.getPnd_Total_T_Std_M().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-M
                ldaAdsl6005.getPnd_Total_T_Std_Guar_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Counter_Start));                    //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-GUAR-M
                ldaAdsl6005.getPnd_Total_T_Std_Div_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Counter_Start));                     //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-DIV-M
                ldaAdsl6005.getPnd_Total_T_Income_M().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Income_M()), ldaAdsl6005.getPnd_Total_T_Income_M().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-INCOME-M
                ldaAdsl6005.getPnd_Total_T_Applied_M().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Applied_M()), ldaAdsl6005.getPnd_Total_T_Applied_M().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-GRD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-STD-AMT ( #COUNTER-START ) TO #TOTAL-T-APPLIED-M
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  QUARTERLY
                if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode_1().equals(6)))                                                                               //Natural: IF ADI-PYMNT-MODE-1 = 6
                {
                    ldaAdsl6005.getPnd_Total_T_Grd_Q().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Grd_Q()), ldaAdsl6005.getPnd_Total_T_Grd_Q().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-Q
                    ldaAdsl6005.getPnd_Total_T_Grd_Guar_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Counter_Start));                //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-GUAR-Q
                    ldaAdsl6005.getPnd_Total_T_Grd_Div_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Counter_Start));                 //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-DIV-Q
                    ldaAdsl6005.getPnd_Total_T_Std_Q().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Std_Q()), ldaAdsl6005.getPnd_Total_T_Std_Q().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-Q
                    ldaAdsl6005.getPnd_Total_T_Std_Guar_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Counter_Start));                //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-GUAR-Q
                    ldaAdsl6005.getPnd_Total_T_Std_Div_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Counter_Start));                 //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-DIV-Q
                    ldaAdsl6005.getPnd_Total_T_Income_Q().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Income_Q()), ldaAdsl6005.getPnd_Total_T_Income_Q().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-INCOME-Q
                    ldaAdsl6005.getPnd_Total_T_Applied_Q().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Applied_Q()), ldaAdsl6005.getPnd_Total_T_Applied_Q().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-GRD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-STD-AMT ( #COUNTER-START ) TO #TOTAL-T-APPLIED-Q
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  SEMI
                    if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode_1().equals(7)))                                                                           //Natural: IF ADI-PYMNT-MODE-1 = 7
                    {
                        ldaAdsl6005.getPnd_Total_T_Grd_S().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Grd_S()), ldaAdsl6005.getPnd_Total_T_Grd_S().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-S
                        ldaAdsl6005.getPnd_Total_T_Grd_Guar_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Counter_Start));            //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-GUAR-S
                        ldaAdsl6005.getPnd_Total_T_Grd_Div_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Counter_Start));             //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-DIV-S
                        ldaAdsl6005.getPnd_Total_T_Std_S().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Std_S()), ldaAdsl6005.getPnd_Total_T_Std_S().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-S
                        ldaAdsl6005.getPnd_Total_T_Std_Guar_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Counter_Start));            //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-GUAR-S
                        ldaAdsl6005.getPnd_Total_T_Std_Div_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Counter_Start));             //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-DIV-S
                        ldaAdsl6005.getPnd_Total_T_Income_S().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Income_S()), ldaAdsl6005.getPnd_Total_T_Income_S().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-INCOME-S
                        ldaAdsl6005.getPnd_Total_T_Applied_S().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Applied_S()), ldaAdsl6005.getPnd_Total_T_Applied_S().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-GRD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-STD-AMT ( #COUNTER-START ) TO #TOTAL-T-APPLIED-S
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  ANNUAL
                        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode_1().equals(8)))                                                                       //Natural: IF ADI-PYMNT-MODE-1 = 8
                        {
                            ldaAdsl6005.getPnd_Total_T_Grd_A().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Grd_A()), ldaAdsl6005.getPnd_Total_T_Grd_A().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-A
                            ldaAdsl6005.getPnd_Total_T_Grd_Guar_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Counter_Start));        //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-GUAR-A
                            ldaAdsl6005.getPnd_Total_T_Grd_Div_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Counter_Start));         //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-DIV-A
                            ldaAdsl6005.getPnd_Total_T_Std_A().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Std_A()), ldaAdsl6005.getPnd_Total_T_Std_A().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-A
                            ldaAdsl6005.getPnd_Total_T_Std_Guar_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Counter_Start));        //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-GUAR-A
                            ldaAdsl6005.getPnd_Total_T_Std_Div_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Counter_Start));         //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-DIV-A
                            ldaAdsl6005.getPnd_Total_T_Income_A().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Income_A()), ldaAdsl6005.getPnd_Total_T_Income_A().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-INCOME-A
                            ldaAdsl6005.getPnd_Total_T_Applied_A().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Applied_A()), ldaAdsl6005.getPnd_Total_T_Applied_A().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-GRD-AMT ( #COUNTER-START ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-STD-AMT ( #COUNTER-START ) TO #TOTAL-T-APPLIED-A
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  033012 START
        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Count_Castadi_Dtl_Tiaa_Data().equals(pnd_Max_Rslt)))                                                                 //Natural: IF ADS-IA-RSLT-VIEW.C*ADI-DTL-TIAA-DATA = #MAX-RSLT
        {
            pnd_Adi_Super2_Pnd_Rqst_Id_Ia_2.setValue(pnd_Adi_Super1_Pnd_Rqst_Id_Ia);                                                                                      //Natural: ASSIGN #RQST-ID-IA-2 := #RQST-ID-IA
            pnd_Adi_Super2_Pnd_Adi_Record_Type_2.setValue(pnd_Adi_Super1_Pnd_Adi_Record_Type);                                                                            //Natural: ASSIGN #ADI-RECORD-TYPE-2 := #ADI-RECORD-TYPE
            pnd_Adi_Super2_Pnd_Adi_Sqnce_Nbr_2.setValue(2);                                                                                                               //Natural: ASSIGN #ADI-SQNCE-NBR-2 := 2
            vw_ads_Ia_Rsl2.startDatabaseFind                                                                                                                              //Natural: FIND ADS-IA-RSL2 WITH ADI-SUPER1 = #ADI-SUPER2
            (
            "FIND02",
            new Wc[] { new Wc("ADI_SUPER1", "=", pnd_Adi_Super2, WcType.WITH) }
            );
            FIND02:
            while (condition(vw_ads_Ia_Rsl2.readNextRow("FIND02")))
            {
                vw_ads_Ia_Rsl2.setIfNotFoundControlFlag(false);
                FOR01:                                                                                                                                                    //Natural: FOR #COUNTER-START 1 ADS-IA-RSL2.C*ADI-DTL-TIAA-DATA
                for (pnd_Counter_Start.setValue(1); condition(pnd_Counter_Start.lessOrEqual(ads_Ia_Rsl2_Count_Castadi_Dtl_Tiaa_Data)); pnd_Counter_Start.nadd(1))
                {
                    ldaAdsl6005.getPnd_Total_T_Grd_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Grd_T()), ldaAdsl6005.getPnd_Total_T_Grd_T().add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-T
                    ldaAdsl6005.getPnd_Total_T_Grd_Guar_T().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Counter_Start));                                     //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-GUAR-T
                    ldaAdsl6005.getPnd_Total_T_Grd_Div_T().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_Counter_Start));                                      //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-DIV-T
                    ldaAdsl6005.getPnd_Total_T_Std_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Std_T()), ldaAdsl6005.getPnd_Total_T_Std_T().add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-T
                    ldaAdsl6005.getPnd_Total_T_Std_Guar_T().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Counter_Start));                                     //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-GUAR-T
                    ldaAdsl6005.getPnd_Total_T_Std_Div_T().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Counter_Start));                                      //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-DIV-T
                    ldaAdsl6005.getPnd_Total_T_Income_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Income_T()), ldaAdsl6005.getPnd_Total_T_Income_T().add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-INCOME-T
                    pnd_Ia_Tiaa_Grd_Amt.compute(new ComputeParameters(false, pnd_Ia_Tiaa_Grd_Amt), pnd_Ia_Tiaa_Grd_Amt.add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #IA-TIAA-GRD-AMT
                    pnd_Ia_Tiaa_Std_Amt.compute(new ComputeParameters(false, pnd_Ia_Tiaa_Std_Amt), pnd_Ia_Tiaa_Std_Amt.add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #IA-TIAA-STD-AMT
                    //*  DA SIDE
                    ldaAdsl6005.getPnd_Total_T_Applied_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Applied_T()), ldaAdsl6005.getPnd_Total_T_Applied_T().add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt.getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-DA-GRD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-DA-STD-AMT ( #COUNTER-START ) TO #TOTAL-T-APPLIED-T
                    pnd_Da_Tiaa_Grd_Amt.nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt.getValue(pnd_Counter_Start));                                                            //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-DA-GRD-AMT ( #COUNTER-START ) TO #DA-TIAA-GRD-AMT
                    pnd_Da_Tiaa_Std_Amt.nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt.getValue(pnd_Counter_Start));                                                            //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-DA-STD-AMT ( #COUNTER-START ) TO #DA-TIAA-STD-AMT
                    //*  MONTHLY
                    if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode_1().equals(1)))                                                                           //Natural: IF ADI-PYMNT-MODE-1 = 1
                    {
                        ldaAdsl6005.getPnd_Total_T_Grd_M().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Grd_M()), ldaAdsl6005.getPnd_Total_T_Grd_M().add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-M
                        ldaAdsl6005.getPnd_Total_T_Grd_Guar_M().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Counter_Start));                                 //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-GUAR-M
                        ldaAdsl6005.getPnd_Total_T_Grd_Div_M().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_Counter_Start));                                  //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-DIV-M
                        ldaAdsl6005.getPnd_Total_T_Std_M().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Std_M()), ldaAdsl6005.getPnd_Total_T_Std_M().add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-M
                        ldaAdsl6005.getPnd_Total_T_Std_Guar_M().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Counter_Start));                                 //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-GUAR-M
                        ldaAdsl6005.getPnd_Total_T_Std_Div_M().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Counter_Start));                                  //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-DIV-M
                        ldaAdsl6005.getPnd_Total_T_Income_M().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Income_M()), ldaAdsl6005.getPnd_Total_T_Income_M().add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-INCOME-M
                        ldaAdsl6005.getPnd_Total_T_Applied_M().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Applied_M()), ldaAdsl6005.getPnd_Total_T_Applied_M().add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt.getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-DA-GRD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-DA-STD-AMT ( #COUNTER-START ) TO #TOTAL-T-APPLIED-M
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  QUARTERLY
                        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode_1().equals(6)))                                                                       //Natural: IF ADI-PYMNT-MODE-1 = 6
                        {
                            ldaAdsl6005.getPnd_Total_T_Grd_Q().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Grd_Q()), ldaAdsl6005.getPnd_Total_T_Grd_Q().add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-Q
                            ldaAdsl6005.getPnd_Total_T_Grd_Guar_Q().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Counter_Start));                             //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-GUAR-Q
                            ldaAdsl6005.getPnd_Total_T_Grd_Div_Q().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_Counter_Start));                              //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-DIV-Q
                            ldaAdsl6005.getPnd_Total_T_Std_Q().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Std_Q()), ldaAdsl6005.getPnd_Total_T_Std_Q().add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-Q
                            ldaAdsl6005.getPnd_Total_T_Std_Guar_Q().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Counter_Start));                             //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-GUAR-Q
                            ldaAdsl6005.getPnd_Total_T_Std_Div_Q().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Counter_Start));                              //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-DIV-Q
                            ldaAdsl6005.getPnd_Total_T_Income_Q().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Income_Q()), ldaAdsl6005.getPnd_Total_T_Income_Q().add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-INCOME-Q
                            ldaAdsl6005.getPnd_Total_T_Applied_Q().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Applied_Q()), ldaAdsl6005.getPnd_Total_T_Applied_Q().add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt.getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-DA-GRD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-DA-STD-AMT ( #COUNTER-START ) TO #TOTAL-T-APPLIED-Q
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  SEMI
                            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode_1().equals(7)))                                                                   //Natural: IF ADI-PYMNT-MODE-1 = 7
                            {
                                ldaAdsl6005.getPnd_Total_T_Grd_S().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Grd_S()), ldaAdsl6005.getPnd_Total_T_Grd_S().add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-S
                                ldaAdsl6005.getPnd_Total_T_Grd_Guar_S().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Counter_Start));                         //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-GUAR-S
                                ldaAdsl6005.getPnd_Total_T_Grd_Div_S().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_Counter_Start));                          //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-DIV-S
                                ldaAdsl6005.getPnd_Total_T_Std_S().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Std_S()), ldaAdsl6005.getPnd_Total_T_Std_S().add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-S
                                ldaAdsl6005.getPnd_Total_T_Std_Guar_S().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Counter_Start));                         //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-GUAR-S
                                ldaAdsl6005.getPnd_Total_T_Std_Div_S().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Counter_Start));                          //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-DIV-S
                                ldaAdsl6005.getPnd_Total_T_Income_S().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Income_S()), ldaAdsl6005.getPnd_Total_T_Income_S().add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-INCOME-S
                                ldaAdsl6005.getPnd_Total_T_Applied_S().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Applied_S()), ldaAdsl6005.getPnd_Total_T_Applied_S().add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt.getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-DA-GRD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-DA-STD-AMT ( #COUNTER-START ) TO #TOTAL-T-APPLIED-S
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                //*  ANNUAL
                                if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode_1().equals(8)))                                                               //Natural: IF ADI-PYMNT-MODE-1 = 8
                                {
                                    ldaAdsl6005.getPnd_Total_T_Grd_A().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Grd_A()), ldaAdsl6005.getPnd_Total_T_Grd_A().add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-A
                                    ldaAdsl6005.getPnd_Total_T_Grd_Guar_A().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Counter_Start));                     //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-GUAR-A
                                    ldaAdsl6005.getPnd_Total_T_Grd_Div_A().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_Counter_Start));                      //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-GRD-DIV-A
                                    ldaAdsl6005.getPnd_Total_T_Std_A().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Std_A()), ldaAdsl6005.getPnd_Total_T_Std_A().add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Counter_Start))); //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-A
                                    ldaAdsl6005.getPnd_Total_T_Std_Guar_A().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Counter_Start));                     //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-GUAR-A
                                    ldaAdsl6005.getPnd_Total_T_Std_Div_A().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Counter_Start));                      //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-STD-DIV-A
                                    ldaAdsl6005.getPnd_Total_T_Income_A().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Income_A()),                    //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #COUNTER-START ) TO #TOTAL-T-INCOME-A
                                        ldaAdsl6005.getPnd_Total_T_Income_A().add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Counter_Start)));
                                    ldaAdsl6005.getPnd_Total_T_Applied_A().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_T_Applied_A()),                  //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-DA-GRD-AMT ( #COUNTER-START ) ADS-IA-RSL2.ADI-DTL-TIAA-DA-STD-AMT ( #COUNTER-START ) TO #TOTAL-T-APPLIED-A
                                        ldaAdsl6005.getPnd_Total_T_Applied_A().add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt.getValue(pnd_Counter_Start)).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt.getValue(pnd_Counter_Start)));
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
            //*  033012 END
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Move_Ia_Cref_Results_Fields() throws Exception                                                                                                       //Natural: MOVE-IA-CREF-RESULTS-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Counter_Start.reset();                                                                                                                                        //Natural: RESET #COUNTER-START
        REPEAT02:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Counter_Start.equals(ldaAdsl450.getAds_Ia_Rslt_View_Count_Castadi_Dtl_Cref_Data()))) {break;}                                               //Natural: UNTIL #COUNTER-START = C*ADI-DTL-CREF-DATA
            pnd_Counter_Start.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #COUNTER-START
            //*  MONTHLY
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode_1().equals(1)))                                                                                   //Natural: IF ADI-PYMNT-MODE-1 = 1
            {
                if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("R")))                                             //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'R'
                {
                    ldaAdsl6005.getPnd_Total_R_Applied_M().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_R_Applied_M()), ldaAdsl6005.getPnd_Total_R_Applied_M().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADI-DTL-CREF-ANNL-AMT ( #COUNTER-START ) ADI-DTL-CREF-MNTHLY-AMT ( #COUNTER-START ) TO #TOTAL-R-APPLIED-M
                    ldaAdsl6005.getPnd_Total_R_Income_M().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_R_Income_M()), ldaAdsl6005.getPnd_Total_R_Income_M().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start))); //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-R-INCOME-M
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  RS0
                    if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("D")))                                         //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'D'
                    {
                        ldaAdsl6005.getPnd_Total_A_Applied_M().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_A_Applied_M()), ldaAdsl6005.getPnd_Total_A_Applied_M().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADI-DTL-CREF-ANNL-AMT ( #COUNTER-START ) ADI-DTL-CREF-MNTHLY-AMT ( #COUNTER-START ) TO #TOTAL-A-APPLIED-M
                        ldaAdsl6005.getPnd_Total_A_Income_M().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_A_Income_M()), ldaAdsl6005.getPnd_Total_A_Income_M().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start))); //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-A-INCOME-M
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl6005.getPnd_Total_C_Applied_M().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_C_Applied_M()), ldaAdsl6005.getPnd_Total_C_Applied_M().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADI-DTL-CREF-ANNL-AMT ( #COUNTER-START ) ADI-DTL-CREF-MNTHLY-AMT ( #COUNTER-START ) TO #TOTAL-C-APPLIED-M
                        ldaAdsl6005.getPnd_Total_C_Income_M().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_C_Income_M()), ldaAdsl6005.getPnd_Total_C_Income_M().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start))); //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-C-INCOME-M
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("C")))                                             //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'C'
                {
                    ldaAdsl6005.getPnd_Total_C_Mth_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));                  //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-C-MTH-M
                    ldaAdsl6005.getPnd_Total_C_Ann_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));                    //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-C-ANN-M
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("M")))                                             //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'M'
                {
                    ldaAdsl6005.getPnd_Total_M_Mth_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));                  //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-M-MTH-M
                    ldaAdsl6005.getPnd_Total_M_Ann_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));                    //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-M-ANN-M
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("S")))                                             //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'S'
                {
                    ldaAdsl6005.getPnd_Total_S_Mth_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));                  //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-S-MTH-M
                    ldaAdsl6005.getPnd_Total_S_Ann_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));                    //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-S-ANN-M
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("B")))                                             //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'B'
                {
                    ldaAdsl6005.getPnd_Total_B_Mth_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));                  //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-B-MTH-M
                    ldaAdsl6005.getPnd_Total_B_Ann_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));                    //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-B-ANN-M
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("W")))                                             //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'W'
                {
                    ldaAdsl6005.getPnd_Total_W_Mth_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));                  //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-W-MTH-M
                    ldaAdsl6005.getPnd_Total_W_Ann_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));                    //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-W-ANN-M
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("L")))                                             //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'L'
                {
                    ldaAdsl6005.getPnd_Total_L_Mth_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));                  //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-L-MTH-M
                    ldaAdsl6005.getPnd_Total_L_Ann_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));                    //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-L-ANN-M
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("E")))                                             //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'E'
                {
                    ldaAdsl6005.getPnd_Total_E_Mth_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));                  //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-E-MTH-M
                    ldaAdsl6005.getPnd_Total_E_Ann_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));                    //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-E-ANN-M
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("R")))                                             //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'R'
                {
                    ldaAdsl6005.getPnd_Total_R_Mth_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));                  //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-R-MTH-M
                    ldaAdsl6005.getPnd_Total_R_Ann_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));                    //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-R-ANN-M
                }                                                                                                                                                         //Natural: END-IF
                //*  RS0
                if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("D")))                                             //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'D'
                {
                    ldaAdsl6005.getPnd_Total_A_Mth_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));                  //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-A-MTH-M
                    ldaAdsl6005.getPnd_Total_A_Ann_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));                    //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-A-ANN-M
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("I")))                                             //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'I'
                {
                    ldaAdsl6005.getPnd_Total_I_Mth_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));                  //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-I-MTH-M
                    ldaAdsl6005.getPnd_Total_I_Ann_M().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));                    //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-I-ANN-M
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  QUARTERLY
                if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode_1().equals(6)))                                                                               //Natural: IF ADI-PYMNT-MODE-1 = 6
                {
                    if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("R")))                                         //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'R'
                    {
                        ldaAdsl6005.getPnd_Total_R_Applied_Q().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_R_Applied_Q()), ldaAdsl6005.getPnd_Total_R_Applied_Q().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADI-DTL-CREF-ANNL-AMT ( #COUNTER-START ) ADI-DTL-CREF-MNTHLY-AMT ( #COUNTER-START ) TO #TOTAL-R-APPLIED-Q
                        ldaAdsl6005.getPnd_Total_R_Income_Q().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_R_Income_Q()), ldaAdsl6005.getPnd_Total_R_Income_Q().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start))); //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-R-INCOME-Q
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  RS0
                        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("D")))                                     //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'D'
                        {
                            ldaAdsl6005.getPnd_Total_A_Applied_Q().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_A_Applied_Q()), ldaAdsl6005.getPnd_Total_A_Applied_Q().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADI-DTL-CREF-ANNL-AMT ( #COUNTER-START ) ADI-DTL-CREF-MNTHLY-AMT ( #COUNTER-START ) TO #TOTAL-A-APPLIED-Q
                            ldaAdsl6005.getPnd_Total_A_Income_Q().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_A_Income_Q()), ldaAdsl6005.getPnd_Total_A_Income_Q().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start))); //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-A-INCOME-Q
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaAdsl6005.getPnd_Total_C_Applied_Q().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_C_Applied_Q()), ldaAdsl6005.getPnd_Total_C_Applied_Q().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADI-DTL-CREF-ANNL-AMT ( #COUNTER-START ) ADI-DTL-CREF-MNTHLY-AMT ( #COUNTER-START ) TO #TOTAL-C-APPLIED-Q
                            ldaAdsl6005.getPnd_Total_C_Income_Q().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_C_Income_Q()), ldaAdsl6005.getPnd_Total_C_Income_Q().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start))); //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-C-INCOME-Q
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("C")))                                         //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'C'
                    {
                        ldaAdsl6005.getPnd_Total_C_Mth_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));              //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-C-MTH-Q
                        ldaAdsl6005.getPnd_Total_C_Ann_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));                //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-C-ANN-Q
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("M")))                                         //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'M'
                    {
                        ldaAdsl6005.getPnd_Total_M_Mth_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));              //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-M-MTH-Q
                        ldaAdsl6005.getPnd_Total_M_Ann_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));                //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-M-ANN-Q
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("S")))                                         //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'S'
                    {
                        ldaAdsl6005.getPnd_Total_S_Mth_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));              //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-S-MTH-Q
                        ldaAdsl6005.getPnd_Total_S_Ann_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));                //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-S-ANN-Q
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("B")))                                         //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'B'
                    {
                        ldaAdsl6005.getPnd_Total_B_Mth_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));              //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-B-MTH-Q
                        ldaAdsl6005.getPnd_Total_B_Ann_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));                //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-B-ANN-Q
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("W")))                                         //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'W'
                    {
                        ldaAdsl6005.getPnd_Total_W_Mth_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));              //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-W-MTH-Q
                        ldaAdsl6005.getPnd_Total_W_Ann_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));                //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-W-ANN-Q
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("L")))                                         //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'L'
                    {
                        ldaAdsl6005.getPnd_Total_L_Mth_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));              //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-L-MTH-Q
                        ldaAdsl6005.getPnd_Total_L_Ann_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));                //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-L-ANN-Q
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("E")))                                         //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'E'
                    {
                        ldaAdsl6005.getPnd_Total_E_Mth_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));              //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-E-MTH-Q
                        ldaAdsl6005.getPnd_Total_E_Ann_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));                //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-E-ANN-Q
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("R")))                                         //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'R'
                    {
                        ldaAdsl6005.getPnd_Total_R_Mth_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));              //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-R-MTH-Q
                        ldaAdsl6005.getPnd_Total_R_Ann_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));                //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-R-ANN-Q
                    }                                                                                                                                                     //Natural: END-IF
                    //*  RS1
                    if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("D")))                                         //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'D'
                    {
                        ldaAdsl6005.getPnd_Total_A_Mth_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));              //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-A-MTH-Q
                        ldaAdsl6005.getPnd_Total_A_Ann_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));                //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-A-ANN-Q
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("I")))                                         //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'I'
                    {
                        ldaAdsl6005.getPnd_Total_I_Mth_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));              //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-I-MTH-Q
                        ldaAdsl6005.getPnd_Total_I_Ann_Q().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));                //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-I-ANN-Q
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  SEMI
                    if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode_1().equals(7)))                                                                           //Natural: IF ADI-PYMNT-MODE-1 = 7
                    {
                        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("R")))                                     //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'R'
                        {
                            ldaAdsl6005.getPnd_Total_R_Applied_S().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_R_Applied_S()), ldaAdsl6005.getPnd_Total_R_Applied_S().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADI-DTL-CREF-ANNL-AMT ( #COUNTER-START ) ADI-DTL-CREF-MNTHLY-AMT ( #COUNTER-START ) TO #TOTAL-R-APPLIED-S
                            ldaAdsl6005.getPnd_Total_R_Income_S().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_R_Income_S()), ldaAdsl6005.getPnd_Total_R_Income_S().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start))); //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-R-INCOME-S
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  RS0
                            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("D")))                                 //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'D'
                            {
                                ldaAdsl6005.getPnd_Total_A_Applied_S().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_A_Applied_S()), ldaAdsl6005.getPnd_Total_A_Applied_S().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADI-DTL-CREF-ANNL-AMT ( #COUNTER-START ) ADI-DTL-CREF-MNTHLY-AMT ( #COUNTER-START ) TO #TOTAL-A-APPLIED-S
                                ldaAdsl6005.getPnd_Total_A_Income_S().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_A_Income_S()), ldaAdsl6005.getPnd_Total_A_Income_S().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start))); //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-A-INCOME-S
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                ldaAdsl6005.getPnd_Total_C_Applied_S().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_C_Applied_S()), ldaAdsl6005.getPnd_Total_C_Applied_S().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADI-DTL-CREF-ANNL-AMT ( #COUNTER-START ) ADI-DTL-CREF-MNTHLY-AMT ( #COUNTER-START ) TO #TOTAL-C-APPLIED-S
                                ldaAdsl6005.getPnd_Total_C_Income_S().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_C_Income_S()), ldaAdsl6005.getPnd_Total_C_Income_S().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start))); //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-C-INCOME-S
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("C")))                                     //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'C'
                        {
                            ldaAdsl6005.getPnd_Total_C_Mth_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));          //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-C-MTH-S
                            ldaAdsl6005.getPnd_Total_C_Ann_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));            //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-C-ANN-S
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("M")))                                     //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'M'
                        {
                            ldaAdsl6005.getPnd_Total_M_Mth_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));          //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-M-MTH-S
                            ldaAdsl6005.getPnd_Total_M_Ann_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));            //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-M-ANN-S
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("S")))                                     //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'S'
                        {
                            ldaAdsl6005.getPnd_Total_S_Mth_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));          //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-S-MTH-S
                            ldaAdsl6005.getPnd_Total_S_Ann_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));            //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-S-ANN-S
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("B")))                                     //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'B'
                        {
                            ldaAdsl6005.getPnd_Total_B_Mth_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));          //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-B-MTH-S
                            ldaAdsl6005.getPnd_Total_B_Ann_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));            //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-B-ANN-S
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("W")))                                     //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'W'
                        {
                            ldaAdsl6005.getPnd_Total_W_Mth_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));          //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-W-MTH-S
                            ldaAdsl6005.getPnd_Total_W_Ann_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));            //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-W-ANN-S
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("L")))                                     //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'L'
                        {
                            ldaAdsl6005.getPnd_Total_L_Mth_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));          //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-L-MTH-S
                            ldaAdsl6005.getPnd_Total_L_Ann_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));            //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-L-ANN-S
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("E")))                                     //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'E'
                        {
                            ldaAdsl6005.getPnd_Total_E_Mth_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));          //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-E-MTH-S
                            ldaAdsl6005.getPnd_Total_E_Ann_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));            //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-E-ANN-S
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("R")))                                     //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'R'
                        {
                            ldaAdsl6005.getPnd_Total_R_Mth_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));          //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-R-MTH-S
                            ldaAdsl6005.getPnd_Total_R_Ann_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));            //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-R-ANN-S
                        }                                                                                                                                                 //Natural: END-IF
                        //*  RS1
                        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("D")))                                     //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'D'
                        {
                            ldaAdsl6005.getPnd_Total_A_Mth_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));          //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-A-MTH-S
                            ldaAdsl6005.getPnd_Total_A_Ann_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));            //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-A-ANN-S
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("I")))                                     //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'I'
                        {
                            ldaAdsl6005.getPnd_Total_I_Mth_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));          //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-I-MTH-S
                            ldaAdsl6005.getPnd_Total_I_Ann_S().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));            //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-I-ANN-S
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  ANNUAL
                        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode_1().equals(8)))                                                                       //Natural: IF ADI-PYMNT-MODE-1 = 8
                        {
                            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("R")))                                 //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'R'
                            {
                                ldaAdsl6005.getPnd_Total_R_Applied_A().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_R_Applied_A()), ldaAdsl6005.getPnd_Total_R_Applied_A().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt().getValue(pnd_Counter_Start))); //Natural: ADD ADI-DTL-CREF-ANNL-AMT ( #COUNTER-START ) ADI-DTL-CREF-MNTHLY-AMT ( #COUNTER-START ) TO #TOTAL-R-APPLIED-A
                                ldaAdsl6005.getPnd_Total_R_Income_A().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_R_Income_A()), ldaAdsl6005.getPnd_Total_R_Income_A().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start))); //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-R-INCOME-A
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                //*  RS0
                                if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("D")))                             //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'D'
                                {
                                    ldaAdsl6005.getPnd_Total_A_Applied_A().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_A_Applied_A()),                  //Natural: ADD ADI-DTL-CREF-ANNL-AMT ( #COUNTER-START ) ADI-DTL-CREF-MNTHLY-AMT ( #COUNTER-START ) TO #TOTAL-A-APPLIED-A
                                        ldaAdsl6005.getPnd_Total_A_Applied_A().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt().getValue(pnd_Counter_Start)));
                                    ldaAdsl6005.getPnd_Total_A_Income_A().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_A_Income_A()),                    //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-A-INCOME-A
                                        ldaAdsl6005.getPnd_Total_A_Income_A().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start)));
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    ldaAdsl6005.getPnd_Total_C_Applied_A().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_C_Applied_A()),                  //Natural: ADD ADI-DTL-CREF-ANNL-AMT ( #COUNTER-START ) ADI-DTL-CREF-MNTHLY-AMT ( #COUNTER-START ) TO #TOTAL-C-APPLIED-A
                                        ldaAdsl6005.getPnd_Total_C_Applied_A().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt().getValue(pnd_Counter_Start)));
                                    ldaAdsl6005.getPnd_Total_C_Income_A().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_C_Income_A()),                    //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-C-INCOME-A
                                        ldaAdsl6005.getPnd_Total_C_Income_A().add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start)));
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("C")))                                 //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'C'
                            {
                                ldaAdsl6005.getPnd_Total_C_Mth_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));      //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-C-MTH-A
                                ldaAdsl6005.getPnd_Total_C_Ann_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));        //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-C-ANN-A
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("M")))                                 //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'M'
                            {
                                ldaAdsl6005.getPnd_Total_M_Mth_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));      //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-M-MTH-A
                                ldaAdsl6005.getPnd_Total_M_Ann_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));        //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-M-ANN-A
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("S")))                                 //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'S'
                            {
                                ldaAdsl6005.getPnd_Total_S_Mth_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));      //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-S-MTH-A
                                ldaAdsl6005.getPnd_Total_S_Ann_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));        //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-S-ANN-A
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("B")))                                 //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'B'
                            {
                                ldaAdsl6005.getPnd_Total_B_Mth_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));      //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-B-MTH-A
                                ldaAdsl6005.getPnd_Total_B_Ann_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));        //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-B-ANN-A
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("W")))                                 //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'W'
                            {
                                ldaAdsl6005.getPnd_Total_W_Mth_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));      //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-W-MTH-A
                                ldaAdsl6005.getPnd_Total_W_Ann_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));        //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-W-ANN-A
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("L")))                                 //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'L'
                            {
                                ldaAdsl6005.getPnd_Total_L_Mth_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));      //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-L-MTH-A
                                ldaAdsl6005.getPnd_Total_L_Ann_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));        //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-L-ANN-A
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("E")))                                 //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'E'
                            {
                                ldaAdsl6005.getPnd_Total_E_Mth_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));      //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-E-MTH-A
                                ldaAdsl6005.getPnd_Total_E_Ann_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));        //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-E-ANN-A
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("R")))                                 //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'R'
                            {
                                ldaAdsl6005.getPnd_Total_R_Mth_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));      //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-R-MTH-A
                                ldaAdsl6005.getPnd_Total_R_Ann_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));        //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-R-ANN-A
                            }                                                                                                                                             //Natural: END-IF
                            //*  RS0
                            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("D")))                                 //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'D'
                            {
                                ldaAdsl6005.getPnd_Total_A_Mth_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));      //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-A-MTH-A
                                ldaAdsl6005.getPnd_Total_A_Ann_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));        //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-A-ANN-A
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start).equals("I")))                                 //Natural: IF ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) = 'I'
                            {
                                ldaAdsl6005.getPnd_Total_I_Mth_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start));      //Natural: ADD ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-I-MTH-A
                                ldaAdsl6005.getPnd_Total_I_Ann_A().nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start));        //Natural: ADD ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) TO #TOTAL-I-ANN-A
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  RS0
                //*    1X
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(4, new ColumnSpacing(1),"UNIQUE/ID",                                                                                                     //Natural: DISPLAY ( 04 ) 1X 'UNIQUE/ID' ADS-PRTCPNT-VIEW.ADP-UNIQUE-ID 1X 'IA/CREF/NUMBER' ADI-IA-CREF-NBR 1X 'PAYMENT/MODE' ADP-PYMNT-MODE 1X 'PRDCT/CODE' ADI-DTL-CREF-ACCT-CD ( #COUNTER-START ) 1X 'TIAA/IND' ADI-TIAA-STTLMNT 'REA/ACS/IND' ADI-TIAA-RE-STTLMNT 'CREF/IND' ADI-CREF-STTLMNT 'MONTHLY/UNITS' ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #COUNTER-START ) 1X 'ANNUAL/UNITS' ADI-DTL-CREF-ANNL-NBR-UNITS ( #COUNTER-START ) 'MONTHLY/DOLLARS' ADI-DTL-CREF-DA-MNTHLY-AMT ( #COUNTER-START ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 'ANNUAL/DOLLARS' ADI-DTL-CREF-DA-ANNL-AMT ( #COUNTER-START ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 'MONTHLY/DA/DOLLARS' ADI-DTL-CREF-MNTHLY-AMT ( #COUNTER-START ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 'ANNUAL/DA/DOLLARS' ADI-DTL-CREF-ANNL-AMT ( #COUNTER-START ) ( EM = ZZZ,ZZZ,ZZ9.99 )
            		ldaAdsl401.getAds_Prtcpnt_View_Adp_Unique_Id(),new ColumnSpacing(1),"IA/CREF/NUMBER",
            		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Cref_Nbr(),new ColumnSpacing(1),"PAYMENT/MODE",
            		ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode(),new ColumnSpacing(1),"PRDCT/CODE",
            		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Counter_Start),new ColumnSpacing(1),"TIAA/IND",
            		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Sttlmnt(),"REA/ACS/IND",
            		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Re_Sttlmnt(),"CREF/IND",
            		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Cref_Sttlmnt(),"MONTHLY/UNITS",
            		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Counter_Start),new ColumnSpacing(1),"ANNUAL/UNITS",
            		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Counter_Start),"MONTHLY/DOLLARS",
            		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_Counter_Start), new ReportEditMask ("Z,ZZZ,ZZ9.99"),"ANNUAL/DOLLARS",
                
            		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_Counter_Start), new ReportEditMask ("Z,ZZZ,ZZ9.99"),"MONTHLY/DA/DOLLARS",
                
            		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt().getValue(pnd_Counter_Start), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),"ANNUAL/DA/DOLLARS",
                
            		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt().getValue(pnd_Counter_Start), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getReports().skip(4, 1);                                                                                                                                          //Natural: SKIP ( 04 ) 1
        ldaAdsl6005.getPnd_Total_C_Mth_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_C_Mth_T()), (ldaAdsl6005.getPnd_Total_C_Mth_M().add(ldaAdsl6005.getPnd_Total_C_Mth_Q()).add(ldaAdsl6005.getPnd_Total_C_Mth_S()).add(ldaAdsl6005.getPnd_Total_C_Mth_A()))); //Natural: COMPUTE #TOTAL-C-MTH-T = ( #TOTAL-C-MTH-M + #TOTAL-C-MTH-Q + #TOTAL-C-MTH-S + #TOTAL-C-MTH-A )
        ldaAdsl6005.getPnd_Total_M_Mth_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_M_Mth_T()), (ldaAdsl6005.getPnd_Total_M_Mth_M().add(ldaAdsl6005.getPnd_Total_M_Mth_Q()).add(ldaAdsl6005.getPnd_Total_M_Mth_S()).add(ldaAdsl6005.getPnd_Total_M_Mth_A()))); //Natural: COMPUTE #TOTAL-M-MTH-T = ( #TOTAL-M-MTH-M + #TOTAL-M-MTH-Q + #TOTAL-M-MTH-S + #TOTAL-M-MTH-A )
        ldaAdsl6005.getPnd_Total_S_Mth_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_S_Mth_T()), (ldaAdsl6005.getPnd_Total_S_Mth_M().add(ldaAdsl6005.getPnd_Total_S_Mth_Q()).add(ldaAdsl6005.getPnd_Total_S_Mth_S()).add(ldaAdsl6005.getPnd_Total_S_Mth_A()))); //Natural: COMPUTE #TOTAL-S-MTH-T = ( #TOTAL-S-MTH-M + #TOTAL-S-MTH-Q + #TOTAL-S-MTH-S + #TOTAL-S-MTH-A )
        ldaAdsl6005.getPnd_Total_B_Mth_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_B_Mth_T()), (ldaAdsl6005.getPnd_Total_B_Mth_M().add(ldaAdsl6005.getPnd_Total_B_Mth_Q()).add(ldaAdsl6005.getPnd_Total_B_Mth_S()).add(ldaAdsl6005.getPnd_Total_B_Mth_A()))); //Natural: COMPUTE #TOTAL-B-MTH-T = ( #TOTAL-B-MTH-M + #TOTAL-B-MTH-Q + #TOTAL-B-MTH-S + #TOTAL-B-MTH-A )
        ldaAdsl6005.getPnd_Total_W_Mth_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_W_Mth_T()), (ldaAdsl6005.getPnd_Total_W_Mth_M().add(ldaAdsl6005.getPnd_Total_W_Mth_Q()).add(ldaAdsl6005.getPnd_Total_W_Mth_S()).add(ldaAdsl6005.getPnd_Total_W_Mth_A()))); //Natural: COMPUTE #TOTAL-W-MTH-T = ( #TOTAL-W-MTH-M + #TOTAL-W-MTH-Q + #TOTAL-W-MTH-S + #TOTAL-W-MTH-A )
        ldaAdsl6005.getPnd_Total_L_Mth_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_L_Mth_T()), (ldaAdsl6005.getPnd_Total_L_Mth_M().add(ldaAdsl6005.getPnd_Total_L_Mth_Q()).add(ldaAdsl6005.getPnd_Total_L_Mth_S()).add(ldaAdsl6005.getPnd_Total_L_Mth_A()))); //Natural: COMPUTE #TOTAL-L-MTH-T = ( #TOTAL-L-MTH-M + #TOTAL-L-MTH-Q + #TOTAL-L-MTH-S + #TOTAL-L-MTH-A )
        ldaAdsl6005.getPnd_Total_E_Mth_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_E_Mth_T()), (ldaAdsl6005.getPnd_Total_E_Mth_M().add(ldaAdsl6005.getPnd_Total_E_Mth_Q()).add(ldaAdsl6005.getPnd_Total_E_Mth_S()).add(ldaAdsl6005.getPnd_Total_E_Mth_A()))); //Natural: COMPUTE #TOTAL-E-MTH-T = ( #TOTAL-E-MTH-M + #TOTAL-E-MTH-Q + #TOTAL-E-MTH-S + #TOTAL-E-MTH-A )
        ldaAdsl6005.getPnd_Total_R_Mth_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_R_Mth_T()), (ldaAdsl6005.getPnd_Total_R_Mth_M().add(ldaAdsl6005.getPnd_Total_R_Mth_Q()).add(ldaAdsl6005.getPnd_Total_R_Mth_S()).add(ldaAdsl6005.getPnd_Total_R_Mth_A()))); //Natural: COMPUTE #TOTAL-R-MTH-T = ( #TOTAL-R-MTH-M + #TOTAL-R-MTH-Q + #TOTAL-R-MTH-S + #TOTAL-R-MTH-A )
        //*  RS1
        ldaAdsl6005.getPnd_Total_A_Mth_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_A_Mth_T()), (ldaAdsl6005.getPnd_Total_A_Mth_M().add(ldaAdsl6005.getPnd_Total_A_Mth_Q()).add(ldaAdsl6005.getPnd_Total_A_Mth_S()).add(ldaAdsl6005.getPnd_Total_A_Mth_A()))); //Natural: COMPUTE #TOTAL-A-MTH-T = ( #TOTAL-A-MTH-M + #TOTAL-A-MTH-Q + #TOTAL-A-MTH-S + #TOTAL-A-MTH-A )
        ldaAdsl6005.getPnd_Total_I_Mth_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_I_Mth_T()), (ldaAdsl6005.getPnd_Total_I_Mth_M().add(ldaAdsl6005.getPnd_Total_I_Mth_Q()).add(ldaAdsl6005.getPnd_Total_I_Mth_S()).add(ldaAdsl6005.getPnd_Total_I_Mth_A()))); //Natural: COMPUTE #TOTAL-I-MTH-T = ( #TOTAL-I-MTH-M + #TOTAL-I-MTH-Q + #TOTAL-I-MTH-S + #TOTAL-I-MTH-A )
        ldaAdsl6005.getPnd_Total_C_Ann_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_C_Ann_T()), (ldaAdsl6005.getPnd_Total_C_Ann_M().add(ldaAdsl6005.getPnd_Total_C_Ann_Q()).add(ldaAdsl6005.getPnd_Total_C_Ann_S()).add(ldaAdsl6005.getPnd_Total_C_Ann_A()))); //Natural: COMPUTE #TOTAL-C-ANN-T = ( #TOTAL-C-ANN-M + #TOTAL-C-ANN-Q + #TOTAL-C-ANN-S + #TOTAL-C-ANN-A )
        ldaAdsl6005.getPnd_Total_M_Ann_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_M_Ann_T()), (ldaAdsl6005.getPnd_Total_M_Ann_M().add(ldaAdsl6005.getPnd_Total_M_Ann_Q()).add(ldaAdsl6005.getPnd_Total_M_Ann_S()).add(ldaAdsl6005.getPnd_Total_M_Ann_A()))); //Natural: COMPUTE #TOTAL-M-ANN-T = ( #TOTAL-M-ANN-M + #TOTAL-M-ANN-Q + #TOTAL-M-ANN-S + #TOTAL-M-ANN-A )
        ldaAdsl6005.getPnd_Total_S_Ann_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_S_Ann_T()), (ldaAdsl6005.getPnd_Total_S_Ann_M().add(ldaAdsl6005.getPnd_Total_S_Ann_Q()).add(ldaAdsl6005.getPnd_Total_S_Ann_S()).add(ldaAdsl6005.getPnd_Total_S_Ann_A()))); //Natural: COMPUTE #TOTAL-S-ANN-T = ( #TOTAL-S-ANN-M + #TOTAL-S-ANN-Q + #TOTAL-S-ANN-S + #TOTAL-S-ANN-A )
        ldaAdsl6005.getPnd_Total_B_Ann_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_B_Ann_T()), (ldaAdsl6005.getPnd_Total_B_Ann_M().add(ldaAdsl6005.getPnd_Total_B_Ann_Q()).add(ldaAdsl6005.getPnd_Total_B_Ann_S()).add(ldaAdsl6005.getPnd_Total_B_Ann_A()))); //Natural: COMPUTE #TOTAL-B-ANN-T = ( #TOTAL-B-ANN-M + #TOTAL-B-ANN-Q + #TOTAL-B-ANN-S + #TOTAL-B-ANN-A )
        ldaAdsl6005.getPnd_Total_W_Ann_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_W_Ann_T()), (ldaAdsl6005.getPnd_Total_W_Ann_M().add(ldaAdsl6005.getPnd_Total_W_Ann_Q()).add(ldaAdsl6005.getPnd_Total_W_Ann_S()).add(ldaAdsl6005.getPnd_Total_W_Ann_A()))); //Natural: COMPUTE #TOTAL-W-ANN-T = ( #TOTAL-W-ANN-M + #TOTAL-W-ANN-Q + #TOTAL-W-ANN-S + #TOTAL-W-ANN-A )
        ldaAdsl6005.getPnd_Total_L_Ann_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_L_Ann_T()), (ldaAdsl6005.getPnd_Total_L_Ann_M().add(ldaAdsl6005.getPnd_Total_L_Ann_Q()).add(ldaAdsl6005.getPnd_Total_L_Ann_S()).add(ldaAdsl6005.getPnd_Total_L_Ann_A()))); //Natural: COMPUTE #TOTAL-L-ANN-T = ( #TOTAL-L-ANN-M + #TOTAL-L-ANN-Q + #TOTAL-L-ANN-S + #TOTAL-L-ANN-A )
        ldaAdsl6005.getPnd_Total_E_Ann_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_E_Ann_T()), (ldaAdsl6005.getPnd_Total_E_Ann_M().add(ldaAdsl6005.getPnd_Total_E_Ann_Q()).add(ldaAdsl6005.getPnd_Total_E_Ann_S()).add(ldaAdsl6005.getPnd_Total_E_Ann_A()))); //Natural: COMPUTE #TOTAL-E-ANN-T = ( #TOTAL-E-ANN-M + #TOTAL-E-ANN-Q + #TOTAL-E-ANN-S + #TOTAL-E-ANN-A )
        ldaAdsl6005.getPnd_Total_R_Ann_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_R_Ann_T()), (ldaAdsl6005.getPnd_Total_R_Ann_M().add(ldaAdsl6005.getPnd_Total_R_Ann_Q()).add(ldaAdsl6005.getPnd_Total_R_Ann_S()).add(ldaAdsl6005.getPnd_Total_R_Ann_A()))); //Natural: COMPUTE #TOTAL-R-ANN-T = ( #TOTAL-R-ANN-M + #TOTAL-R-ANN-Q + #TOTAL-R-ANN-S + #TOTAL-R-ANN-A )
        //*  RS1
        ldaAdsl6005.getPnd_Total_A_Ann_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_A_Ann_T()), (ldaAdsl6005.getPnd_Total_A_Ann_M().add(ldaAdsl6005.getPnd_Total_A_Ann_Q()).add(ldaAdsl6005.getPnd_Total_A_Ann_S()).add(ldaAdsl6005.getPnd_Total_A_Ann_A()))); //Natural: COMPUTE #TOTAL-A-ANN-T = ( #TOTAL-A-ANN-M + #TOTAL-A-ANN-Q + #TOTAL-A-ANN-S + #TOTAL-A-ANN-A )
        ldaAdsl6005.getPnd_Total_I_Ann_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_I_Ann_T()), (ldaAdsl6005.getPnd_Total_I_Ann_M().add(ldaAdsl6005.getPnd_Total_I_Ann_Q()).add(ldaAdsl6005.getPnd_Total_I_Ann_S()).add(ldaAdsl6005.getPnd_Total_I_Ann_A()))); //Natural: COMPUTE #TOTAL-I-ANN-T = ( #TOTAL-I-ANN-M + #TOTAL-I-ANN-Q + #TOTAL-I-ANN-S + #TOTAL-I-ANN-A )
        ldaAdsl6005.getPnd_Total_R_Applied_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_R_Applied_T()), (ldaAdsl6005.getPnd_Total_R_Applied_M().add(ldaAdsl6005.getPnd_Total_R_Applied_Q()).add(ldaAdsl6005.getPnd_Total_R_Applied_S()).add(ldaAdsl6005.getPnd_Total_R_Applied_A()))); //Natural: COMPUTE #TOTAL-R-APPLIED-T = ( #TOTAL-R-APPLIED-M + #TOTAL-R-APPLIED-Q + #TOTAL-R-APPLIED-S + #TOTAL-R-APPLIED-A )
        //*  RS0
        ldaAdsl6005.getPnd_Total_A_Applied_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_A_Applied_T()), (ldaAdsl6005.getPnd_Total_A_Applied_M().add(ldaAdsl6005.getPnd_Total_A_Applied_Q()).add(ldaAdsl6005.getPnd_Total_A_Applied_S()).add(ldaAdsl6005.getPnd_Total_A_Applied_A()))); //Natural: COMPUTE #TOTAL-A-APPLIED-T = ( #TOTAL-A-APPLIED-M + #TOTAL-A-APPLIED-Q + #TOTAL-A-APPLIED-S + #TOTAL-A-APPLIED-A )
        ldaAdsl6005.getPnd_Total_C_Applied_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_C_Applied_T()), (ldaAdsl6005.getPnd_Total_C_Applied_M().add(ldaAdsl6005.getPnd_Total_C_Applied_Q()).add(ldaAdsl6005.getPnd_Total_C_Applied_S()).add(ldaAdsl6005.getPnd_Total_C_Applied_A()))); //Natural: COMPUTE #TOTAL-C-APPLIED-T = ( #TOTAL-C-APPLIED-M + #TOTAL-C-APPLIED-Q + #TOTAL-C-APPLIED-S + #TOTAL-C-APPLIED-A )
        ldaAdsl6005.getPnd_Total_C_Income_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_C_Income_T()), (ldaAdsl6005.getPnd_Total_C_Income_M().add(ldaAdsl6005.getPnd_Total_C_Income_Q()).add(ldaAdsl6005.getPnd_Total_C_Income_S()).add(ldaAdsl6005.getPnd_Total_C_Income_A()))); //Natural: COMPUTE #TOTAL-C-INCOME-T = ( #TOTAL-C-INCOME-M + #TOTAL-C-INCOME-Q + #TOTAL-C-INCOME-S + #TOTAL-C-INCOME-A )
        ldaAdsl6005.getPnd_Total_R_Income_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_R_Income_T()), (ldaAdsl6005.getPnd_Total_R_Income_M().add(ldaAdsl6005.getPnd_Total_R_Income_Q()).add(ldaAdsl6005.getPnd_Total_R_Income_S()).add(ldaAdsl6005.getPnd_Total_R_Income_A()))); //Natural: COMPUTE #TOTAL-R-INCOME-T = ( #TOTAL-R-INCOME-M + #TOTAL-R-INCOME-Q + #TOTAL-R-INCOME-S + #TOTAL-R-INCOME-A )
        //*  RS0
        ldaAdsl6005.getPnd_Total_A_Income_T().compute(new ComputeParameters(false, ldaAdsl6005.getPnd_Total_A_Income_T()), (ldaAdsl6005.getPnd_Total_A_Income_M().add(ldaAdsl6005.getPnd_Total_A_Income_Q()).add(ldaAdsl6005.getPnd_Total_A_Income_S()).add(ldaAdsl6005.getPnd_Total_A_Income_A()))); //Natural: COMPUTE #TOTAL-A-INCOME-T = ( #TOTAL-A-INCOME-M + #TOTAL-A-INCOME-Q + #TOTAL-A-INCOME-S + #TOTAL-A-INCOME-A )
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(64),"TIAA/CREF",NEWLINE,new ColumnSpacing(10),"RUN DATE  : ",Global.getDATU(),new  //Natural: WRITE ( 01 ) NOTITLE NOHDR 64X 'TIAA/CREF' / 10X 'RUN DATE  : ' *DATU 21X 'PENSIONS AND ANNUITIES PROJECT' 27X 'PROGRAM ID: ' *PROGRAM / 10X 'RUN TIME  : ' *TIMX 19X 'REPORT LISTING : CIS INTERFACE REPORT ' 26X 'PAGE : ' *PAGE-NUMBER ( 01 ) ///
                        ColumnSpacing(21),"PENSIONS AND ANNUITIES PROJECT",new ColumnSpacing(27),"PROGRAM ID: ",Global.getPROGRAM(),NEWLINE,new ColumnSpacing(10),"RUN TIME  : ",Global.getTIMX(),new 
                        ColumnSpacing(19),"REPORT LISTING : CIS INTERFACE REPORT ",new ColumnSpacing(26),"PAGE : ",getReports().getPageNumberDbs(1),NEWLINE,
                        NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR," ");                                                                                   //Natural: WRITE ( 02 ) NOTITLE NOHDR ' '
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(64),"TIAA/CREF",NEWLINE,new ColumnSpacing(10),"RUN DATE  : ",Global.getDATU(),new  //Natural: WRITE ( 03 ) NOTITLE NOHDR 64X 'TIAA/CREF' / 10X 'RUN DATE  : ' *DATU 21X 'PENSIONS AND ANNUITIES PROJECT' 27X 'PROGRAM ID: ' *PROGRAM / 10X 'RUN TIME  : ' *TIMX 19X 'REPORT LISTING : TIAA CIS FINANCIAL  ' 27X 'PAGE : ' *PAGE-NUMBER ( 03 ) ///
                        ColumnSpacing(21),"PENSIONS AND ANNUITIES PROJECT",new ColumnSpacing(27),"PROGRAM ID: ",Global.getPROGRAM(),NEWLINE,new ColumnSpacing(10),"RUN TIME  : ",Global.getTIMX(),new 
                        ColumnSpacing(19),"REPORT LISTING : TIAA CIS FINANCIAL  ",new ColumnSpacing(27),"PAGE : ",getReports().getPageNumberDbs(3),NEWLINE,
                        NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(4, ReportOption.NOTITLE,ReportOption.NOHDR,new ColumnSpacing(64),"TIAA/CREF",NEWLINE,new ColumnSpacing(10),"RUN DATE  : ",Global.getDATU(),new  //Natural: WRITE ( 04 ) NOTITLE NOHDR 64X 'TIAA/CREF' / 10X 'RUN DATE  : ' *DATU 21X 'PENSIONS AND ANNUITIES PROJECT' 27X 'PROGRAM ID: ' *PROGRAM / 10X 'RUN TIME  : ' *TIMX 19X 'REPORT LISTING : CREF CIS FINANCIAL  ' 27X 'PAGE : ' *PAGE-NUMBER ( 04 ) ///
                        ColumnSpacing(21),"PENSIONS AND ANNUITIES PROJECT",new ColumnSpacing(27),"PROGRAM ID: ",Global.getPROGRAM(),NEWLINE,new ColumnSpacing(10),"RUN TIME  : ",Global.getTIMX(),new 
                        ColumnSpacing(19),"REPORT LISTING : CREF CIS FINANCIAL  ",new ColumnSpacing(27),"PAGE : ",getReports().getPageNumberDbs(4),NEWLINE,
                        NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=55");
        Global.format(2, "LS=132 PS=55");
        Global.format(3, "LS=132 PS=55");
        Global.format(4, "LS=136 PS=55");

        getReports().setDisplayColumns(1, "REQUEST/ID",
        		ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id(),"MIT/LOG-DATE",
        		ldaAdsl401.getAds_Prtcpnt_View_Adp_Mit_Log_Dte_Tme(),"EFFECT/DATE",
        		ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte(),"OPT/CDE",
        		ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Optn(),"GUAR/PER",
        		ldaAdsl401.getAds_Prtcpnt_View_Adp_Grntee_Period(),"START/DATE",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Annty_Strt_Dte(),"END/DATE",
        		pnd_End_Date,"DA/TIAA/NUMBER",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Nbrs(),"IA/TIAA/NUMBER",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Tiaa_Nbr(),"DA/CREF/NUMBER",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Cref_Nbrs(),"IA/CREF/NUMBER",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Cref_Nbr());
        getReports().setDisplayColumns(3, new ColumnSpacing(5),"UNIQUE/ID",
        		ldaAdsl401.getAds_Prtcpnt_View_Adp_Unique_Id(),"IA/TIAA/NUMBER",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Tiaa_Nbr(),new ColumnSpacing(5),"PAYMENT/MODE",
        		ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode(),new ColumnSpacing(5),"TIAA/IND",
        		pnd_Tiaa_Only,"REA/ACS/IND",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Re_Sttlmnt(),"CREF/IND",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Cref_Sttlmnt(),"TIAA/STANDARD/TPA-IPRO",
        		pnd_Ia_Tiaa_Std_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(5),"TIAA/GRADED",
        		pnd_Ia_Tiaa_Grd_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(5),"TIAA/DA/STANDARD/TPA-IPRO",
        		pnd_Da_Tiaa_Std_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(5),"TIAA/DA/GRADED",
        		pnd_Da_Tiaa_Grd_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
        getReports().setDisplayColumns(4, new ColumnSpacing(1),"UNIQUE/ID",
        		ldaAdsl401.getAds_Prtcpnt_View_Adp_Unique_Id(),new ColumnSpacing(1),"IA/CREF/NUMBER",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Cref_Nbr(),new ColumnSpacing(1),"PAYMENT/MODE",
        		ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode(),new ColumnSpacing(1),"PRDCT/CODE",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd(),new ColumnSpacing(1),"TIAA/IND",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Sttlmnt(),"REA/ACS/IND",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Re_Sttlmnt(),"CREF/IND",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Cref_Sttlmnt(),"MONTHLY/UNITS",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units(),new ColumnSpacing(1),"ANNUAL/UNITS",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units(),"MONTHLY/DOLLARS",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Mnthly_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),"ANNUAL/DOLLARS",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Annl_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),"MONTHLY/DA/DOLLARS",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),"ANNUAL/DA/DOLLARS",
        		ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
    }
}
