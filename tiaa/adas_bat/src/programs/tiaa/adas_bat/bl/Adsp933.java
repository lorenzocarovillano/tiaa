/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:05:41 PM
**        * FROM NATURAL PROGRAM : Adsp933
************************************************************
**        * FILE NAME            : Adsp933.java
**        * CLASS NAME           : Adsp933
**        * INSTANCE NAME        : Adsp933
************************************************************
************************************************************************
* PROGRAM  : ADSP933
* GENERATED: APRIL 16, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : READS PARTICIPANT RECORDS AND WRITES WORKFILE OF PENDING
*            SETTLEMENT REQUESTS.
*
* REMARKS  : CLONED FROM ADSP930
************************************************************************
* MOD DATE   MOD BY      DESCRIPTION OF CHANGES
* 04/16/04   C. AVE      CLONED FROM ADSP930
* 03/10/10   D.E.ANDER   CHANGE FOR ADABAS REPLATFORM ADD SURVIVOR-IND
*                        PCPOP                MARKED DEA
* 06/24/14   O. SOTTO    RECOMPILED FOR LASTEST ADS-CNTRCT DDM.
* 02/23/2017 R. CARREON  PIN EXPANSION 02232017
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp933 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_partrec;
    private DbsField partrec_Rqst_Id;
    private DbsField partrec_Adp_Bps_Unit;
    private DbsField partrec_Adp_Unique_Id;
    private DbsField partrec_Adp_Effctv_Dte;
    private DbsField partrec_Adp_Opn_Clsd_Ind;
    private DbsField partrec_Adp_Stts_Cde;

    private DbsGroup partrec__R_Field_1;
    private DbsField partrec_Pnd_Adp_Stts_Cde_1st_Byte;
    private DbsField partrec_Pnd_Adp_Stts_Cde_Rest;
    private DbsField partrec_Adp_Ia_Tiaa_Nbr;
    private DbsField partrec_Adp_Rqst_Last_Updtd_Dte;
    private DbsField partrec_Adp_Annty_Strt_Dte;
    private DbsField partrec_Adp_Annt_Typ_Cde;
    private DbsField partrec_Adp_Srvvr_Unique_Id;

    private DataAccessProgramView vw_contrec;
    private DbsField contrec_Rqst_Id;
    private DbsField contrec_Adc_Tiaa_Nbr;
    private DbsField contrec_Adc_Stts_Cde;

    private DbsGroup contrec__R_Field_2;
    private DbsField contrec_Pnd_Adc_Stts_Cde_1st_Byte;
    private DbsField contrec_Pnd_Adc_Stts_Cde_Rest;
    private DbsField pnd_Adp_Sd2;

    private DbsGroup pnd_Adp_Sd2__R_Field_3;
    private DbsField pnd_Adp_Sd2_Pnd_Sd2_Opn_Clsd_Ind;
    private DbsField pnd_Adp_Sd2_Pnd_Sd2_Stts_Cde;
    private DbsField pnd_Dte_Ccyymmdd;

    private DbsGroup pnd_Dte_Ccyymmdd__R_Field_4;
    private DbsField pnd_Dte_Ccyymmdd_Pnd_Dte_Ccyymmdd_Alpha;
    private DbsField pnd_Wkf;

    private DbsGroup pnd_Wkf__R_Field_5;
    private DbsField pnd_Wkf_Pnd_Wkf_Bps_Unit;
    private DbsField pnd_Wkf_Pnd_Wkf_Cntrct;
    private DbsField pnd_Wkf_Pnd_Wkf_Unique_Id;
    private DbsField pnd_Wkf_Pnd_Wkf_Eff_Dte;
    private DbsField pnd_Wkf_Pnd_Wkf_Adp_Stts_Cde;
    private DbsField pnd_Wkf_Pnd_Wkf_Adc_Stts_Cde;
    private DbsField pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr;
    private DbsField pnd_Wkf_Pnd_Wkf_Lst_Updte;
    private DbsField pnd_Wkf_Pnd_Wkf_Annty_Strt_Dte;
    private DbsField pnd_Wkf_Pnd_Wkf_Repl_Ind;
    private DbsField pnd_X;
    private DbsField pnd_Y;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_partrec = new DataAccessProgramView(new NameInfo("vw_partrec", "PARTREC"), "ADS_PRTCPNT", "ADS_PRTCPNT");
        partrec_Rqst_Id = vw_partrec.getRecord().newFieldInGroup("partrec_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, "RQST_ID");
        partrec_Adp_Bps_Unit = vw_partrec.getRecord().newFieldInGroup("partrec_Adp_Bps_Unit", "ADP-BPS-UNIT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADP_BPS_UNIT");
        partrec_Adp_Unique_Id = vw_partrec.getRecord().newFieldInGroup("partrec_Adp_Unique_Id", "ADP-UNIQUE-ID", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "ADP_UNIQUE_ID");
        partrec_Adp_Effctv_Dte = vw_partrec.getRecord().newFieldInGroup("partrec_Adp_Effctv_Dte", "ADP-EFFCTV-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADP_EFFCTV_DTE");
        partrec_Adp_Opn_Clsd_Ind = vw_partrec.getRecord().newFieldInGroup("partrec_Adp_Opn_Clsd_Ind", "ADP-OPN-CLSD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ADP_OPN_CLSD_IND");
        partrec_Adp_Stts_Cde = vw_partrec.getRecord().newFieldInGroup("partrec_Adp_Stts_Cde", "ADP-STTS-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "ADP_STTS_CDE");

        partrec__R_Field_1 = vw_partrec.getRecord().newGroupInGroup("partrec__R_Field_1", "REDEFINE", partrec_Adp_Stts_Cde);
        partrec_Pnd_Adp_Stts_Cde_1st_Byte = partrec__R_Field_1.newFieldInGroup("partrec_Pnd_Adp_Stts_Cde_1st_Byte", "#ADP-STTS-CDE-1ST-BYTE", FieldType.STRING, 
            1);
        partrec_Pnd_Adp_Stts_Cde_Rest = partrec__R_Field_1.newFieldInGroup("partrec_Pnd_Adp_Stts_Cde_Rest", "#ADP-STTS-CDE-REST", FieldType.STRING, 2);
        partrec_Adp_Ia_Tiaa_Nbr = vw_partrec.getRecord().newFieldInGroup("partrec_Adp_Ia_Tiaa_Nbr", "ADP-IA-TIAA-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "ADP_IA_TIAA_NBR");
        partrec_Adp_Rqst_Last_Updtd_Dte = vw_partrec.getRecord().newFieldInGroup("partrec_Adp_Rqst_Last_Updtd_Dte", "ADP-RQST-LAST-UPDTD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADP_RQST_LAST_UPDTD_DTE");
        partrec_Adp_Annty_Strt_Dte = vw_partrec.getRecord().newFieldInGroup("partrec_Adp_Annty_Strt_Dte", "ADP-ANNTY-STRT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADP_ANNTY_STRT_DTE");
        partrec_Adp_Annt_Typ_Cde = vw_partrec.getRecord().newFieldInGroup("partrec_Adp_Annt_Typ_Cde", "ADP-ANNT-TYP-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ADP_ANNT_TYP_CDE");
        partrec_Adp_Srvvr_Unique_Id = vw_partrec.getRecord().newFieldInGroup("partrec_Adp_Srvvr_Unique_Id", "ADP-SRVVR-UNIQUE-ID", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "ADP_SRVVR_UNIQUE_ID");
        registerRecord(vw_partrec);

        vw_contrec = new DataAccessProgramView(new NameInfo("vw_contrec", "CONTREC"), "ADS_CNTRCT", "DS_CNTRCT");
        contrec_Rqst_Id = vw_contrec.getRecord().newFieldInGroup("contrec_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, "RQST_ID");
        contrec_Adc_Tiaa_Nbr = vw_contrec.getRecord().newFieldInGroup("contrec_Adc_Tiaa_Nbr", "ADC-TIAA-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "ADC_TIAA_NBR");
        contrec_Adc_Stts_Cde = vw_contrec.getRecord().newFieldInGroup("contrec_Adc_Stts_Cde", "ADC-STTS-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "ADC_STTS_CDE");

        contrec__R_Field_2 = vw_contrec.getRecord().newGroupInGroup("contrec__R_Field_2", "REDEFINE", contrec_Adc_Stts_Cde);
        contrec_Pnd_Adc_Stts_Cde_1st_Byte = contrec__R_Field_2.newFieldInGroup("contrec_Pnd_Adc_Stts_Cde_1st_Byte", "#ADC-STTS-CDE-1ST-BYTE", FieldType.STRING, 
            1);
        contrec_Pnd_Adc_Stts_Cde_Rest = contrec__R_Field_2.newFieldInGroup("contrec_Pnd_Adc_Stts_Cde_Rest", "#ADC-STTS-CDE-REST", FieldType.STRING, 2);
        registerRecord(vw_contrec);

        pnd_Adp_Sd2 = localVariables.newFieldInRecord("pnd_Adp_Sd2", "#ADP-SD2", FieldType.STRING, 4);

        pnd_Adp_Sd2__R_Field_3 = localVariables.newGroupInRecord("pnd_Adp_Sd2__R_Field_3", "REDEFINE", pnd_Adp_Sd2);
        pnd_Adp_Sd2_Pnd_Sd2_Opn_Clsd_Ind = pnd_Adp_Sd2__R_Field_3.newFieldInGroup("pnd_Adp_Sd2_Pnd_Sd2_Opn_Clsd_Ind", "#SD2-OPN-CLSD-IND", FieldType.STRING, 
            1);
        pnd_Adp_Sd2_Pnd_Sd2_Stts_Cde = pnd_Adp_Sd2__R_Field_3.newFieldInGroup("pnd_Adp_Sd2_Pnd_Sd2_Stts_Cde", "#SD2-STTS-CDE", FieldType.STRING, 3);
        pnd_Dte_Ccyymmdd = localVariables.newFieldInRecord("pnd_Dte_Ccyymmdd", "#DTE-CCYYMMDD", FieldType.NUMERIC, 8);

        pnd_Dte_Ccyymmdd__R_Field_4 = localVariables.newGroupInRecord("pnd_Dte_Ccyymmdd__R_Field_4", "REDEFINE", pnd_Dte_Ccyymmdd);
        pnd_Dte_Ccyymmdd_Pnd_Dte_Ccyymmdd_Alpha = pnd_Dte_Ccyymmdd__R_Field_4.newFieldInGroup("pnd_Dte_Ccyymmdd_Pnd_Dte_Ccyymmdd_Alpha", "#DTE-CCYYMMDD-ALPHA", 
            FieldType.STRING, 8);
        pnd_Wkf = localVariables.newFieldInRecord("pnd_Wkf", "#WKF", FieldType.STRING, 71);

        pnd_Wkf__R_Field_5 = localVariables.newGroupInRecord("pnd_Wkf__R_Field_5", "REDEFINE", pnd_Wkf);
        pnd_Wkf_Pnd_Wkf_Bps_Unit = pnd_Wkf__R_Field_5.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Bps_Unit", "#WKF-BPS-UNIT", FieldType.STRING, 8);
        pnd_Wkf_Pnd_Wkf_Cntrct = pnd_Wkf__R_Field_5.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Cntrct", "#WKF-CNTRCT", FieldType.STRING, 10);
        pnd_Wkf_Pnd_Wkf_Unique_Id = pnd_Wkf__R_Field_5.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Unique_Id", "#WKF-UNIQUE-ID", FieldType.NUMERIC, 12);
        pnd_Wkf_Pnd_Wkf_Eff_Dte = pnd_Wkf__R_Field_5.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Eff_Dte", "#WKF-EFF-DTE", FieldType.STRING, 8);
        pnd_Wkf_Pnd_Wkf_Adp_Stts_Cde = pnd_Wkf__R_Field_5.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Adp_Stts_Cde", "#WKF-ADP-STTS-CDE", FieldType.STRING, 3);
        pnd_Wkf_Pnd_Wkf_Adc_Stts_Cde = pnd_Wkf__R_Field_5.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Adc_Stts_Cde", "#WKF-ADC-STTS-CDE", FieldType.STRING, 3);
        pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr = pnd_Wkf__R_Field_5.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr", "#WKF-IA-TIAA-NBR", FieldType.STRING, 10);
        pnd_Wkf_Pnd_Wkf_Lst_Updte = pnd_Wkf__R_Field_5.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Lst_Updte", "#WKF-LST-UPDTE", FieldType.NUMERIC, 8);
        pnd_Wkf_Pnd_Wkf_Annty_Strt_Dte = pnd_Wkf__R_Field_5.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Annty_Strt_Dte", "#WKF-ANNTY-STRT-DTE", FieldType.NUMERIC, 
            8);
        pnd_Wkf_Pnd_Wkf_Repl_Ind = pnd_Wkf__R_Field_5.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Repl_Ind", "#WKF-REPL-IND", FieldType.STRING, 1);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.INTEGER, 4);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.INTEGER, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_partrec.reset();
        vw_contrec.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp933() throws Exception
    {
        super("Adsp933");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* ******************************************************************
        //*                                                                                                                                                               //Natural: FORMAT LS = 133 PS = 60
        pnd_Adp_Sd2_Pnd_Sd2_Opn_Clsd_Ind.setValue("O");                                                                                                                   //Natural: ASSIGN #SD2-OPN-CLSD-IND = 'O'
        pnd_Adp_Sd2_Pnd_Sd2_Stts_Cde.setValue("F00");                                                                                                                     //Natural: ASSIGN #SD2-STTS-CDE = 'F00'
        vw_partrec.startDatabaseRead                                                                                                                                      //Natural: READ PARTREC BY ADP-SUPER2 STARTING FROM #ADP-SD2
        (
        "READ01",
        new Wc[] { new Wc("ADP_SUPER2", ">=", pnd_Adp_Sd2, WcType.BY) },
        new Oc[] { new Oc("ADP_SUPER2", "ASC") }
        );
        READ01:
        while (condition(vw_partrec.readNextRow("READ01")))
        {
            if (condition(partrec_Adp_Opn_Clsd_Ind.notEquals("O") || partrec_Pnd_Adp_Stts_Cde_1st_Byte.greater("M")))                                                     //Natural: IF ADP-OPN-CLSD-IND NE 'O' OR #ADP-STTS-CDE-1ST-BYTE GT 'M'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(partrec_Pnd_Adp_Stts_Cde_1st_Byte.equals("F") || partrec_Pnd_Adp_Stts_Cde_1st_Byte.equals("M"))))                                             //Natural: ACCEPT IF #ADP-STTS-CDE-1ST-BYTE = 'F' OR = 'M'
            {
                continue;
            }
            pnd_Dte_Ccyymmdd_Pnd_Dte_Ccyymmdd_Alpha.setValueEdited(partrec_Adp_Effctv_Dte,new ReportEditMask("YYYYMMDD"));                                                //Natural: MOVE EDITED ADP-EFFCTV-DTE ( EM = YYYYMMDD ) TO #DTE-CCYYMMDD-ALPHA
            pnd_Wkf_Pnd_Wkf_Adp_Stts_Cde.setValue(partrec_Adp_Stts_Cde);                                                                                                  //Natural: ASSIGN #WKF-ADP-STTS-CDE = ADP-STTS-CDE
                                                                                                                                                                          //Natural: PERFORM WRITE-WKF-RCDS
            sub_Write_Wkf_Rcds();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WKF-RCDS
    }
    private void sub_Write_Wkf_Rcds() throws Exception                                                                                                                    //Natural: WRITE-WKF-RCDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************
        vw_contrec.startDatabaseFind                                                                                                                                      //Natural: FIND CONTREC WITH RQST-ID = PARTREC.RQST-ID
        (
        "FIND01",
        new Wc[] { new Wc("RQST_ID", "=", partrec_Rqst_Id, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_contrec.readNextRow("FIND01")))
        {
            vw_contrec.setIfNotFoundControlFlag(false);
            //* *IF #ADP-STTS-CDE-1ST-BYTE = 'H' OR
            //* *  ADP-STTS-CDE = 'F16'
            //* *  ASSIGN #WKF-BPS-UNIT = 'ACTRB   '
            //* *ELSE
            pnd_Wkf_Pnd_Wkf_Bps_Unit.setValue(partrec_Adp_Bps_Unit);                                                                                                      //Natural: ASSIGN #WKF-BPS-UNIT = ADP-BPS-UNIT
            //* *END-IF
            pnd_Wkf_Pnd_Wkf_Cntrct.setValue(contrec_Adc_Tiaa_Nbr);                                                                                                        //Natural: ASSIGN #WKF-CNTRCT = ADC-TIAA-NBR
            pnd_Wkf_Pnd_Wkf_Unique_Id.setValue(partrec_Adp_Unique_Id);                                                                                                    //Natural: ASSIGN #WKF-UNIQUE-ID = ADP-UNIQUE-ID
            pnd_Wkf_Pnd_Wkf_Eff_Dte.setValueEdited(partrec_Adp_Effctv_Dte,new ReportEditMask("YYYYMMDD"));                                                                //Natural: MOVE EDITED ADP-EFFCTV-DTE ( EM = YYYYMMDD ) TO #WKF-EFF-DTE
            pnd_Wkf_Pnd_Wkf_Adc_Stts_Cde.setValue(contrec_Adc_Stts_Cde);                                                                                                  //Natural: ASSIGN #WKF-ADC-STTS-CDE = ADC-STTS-CDE
            pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr.setValue(partrec_Adp_Ia_Tiaa_Nbr);                                                                                                //Natural: ASSIGN #WKF-IA-TIAA-NBR = ADP-IA-TIAA-NBR
            pnd_Dte_Ccyymmdd_Pnd_Dte_Ccyymmdd_Alpha.setValueEdited(partrec_Adp_Rqst_Last_Updtd_Dte,new ReportEditMask("YYYYMMDD"));                                       //Natural: MOVE EDITED ADP-RQST-LAST-UPDTD-DTE ( EM = YYYYMMDD ) TO #DTE-CCYYMMDD-ALPHA
            pnd_Wkf_Pnd_Wkf_Lst_Updte.setValue(pnd_Dte_Ccyymmdd);                                                                                                         //Natural: ASSIGN #WKF-LST-UPDTE = #DTE-CCYYMMDD
            pnd_Dte_Ccyymmdd_Pnd_Dte_Ccyymmdd_Alpha.setValueEdited(partrec_Adp_Rqst_Last_Updtd_Dte,new ReportEditMask("YYYYMMDD"));                                       //Natural: MOVE EDITED ADP-RQST-LAST-UPDTD-DTE ( EM = YYYYMMDD ) TO #DTE-CCYYMMDD-ALPHA
            pnd_Wkf_Pnd_Wkf_Lst_Updte.setValue(pnd_Dte_Ccyymmdd);                                                                                                         //Natural: ASSIGN #WKF-LST-UPDTE = #DTE-CCYYMMDD
            pnd_Dte_Ccyymmdd_Pnd_Dte_Ccyymmdd_Alpha.setValueEdited(partrec_Adp_Annty_Strt_Dte,new ReportEditMask("YYYYMMDD"));                                            //Natural: MOVE EDITED ADP-ANNTY-STRT-DTE ( EM = YYYYMMDD ) TO #DTE-CCYYMMDD-ALPHA
            pnd_Wkf_Pnd_Wkf_Annty_Strt_Dte.setValue(pnd_Dte_Ccyymmdd);                                                                                                    //Natural: ASSIGN #WKF-ANNTY-STRT-DTE = #DTE-CCYYMMDD
            if (condition(partrec_Adp_Annt_Typ_Cde.equals("R")))                                                                                                          //Natural: IF ADP-ANNT-TYP-CDE = 'R'
            {
                pnd_Wkf_Pnd_Wkf_Repl_Ind.setValue("Y");                                                                                                                   //Natural: ASSIGN #WKF-REPL-IND = 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Wkf_Pnd_Wkf_Repl_Ind.setValue(" ");                                                                                                                   //Natural: ASSIGN #WKF-REPL-IND = ' '
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(1, false, pnd_Wkf);                                                                                                                      //Natural: WRITE WORK FILE 01 #WKF
            //*  DEA START
            if (condition(partrec_Adp_Srvvr_Unique_Id.notEquals(getZero())))                                                                                              //Natural: IF ADP-SRVVR-UNIQUE-ID NE 0
            {
                getWorkFiles().write(2, false, pnd_Wkf, partrec_Adp_Srvvr_Unique_Id);                                                                                     //Natural: WRITE WORK FILE 02 #WKF ADP-SRVVR-UNIQUE-ID
                //*  DEA END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60");
    }
}
