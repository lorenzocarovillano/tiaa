/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:00:23 PM
**        * FROM NATURAL PROGRAM : Acipsas7
************************************************************
**        * FILE NAME            : Acipsas7.java
**        * CLASS NAME           : Acipsas7
**        * INSTANCE NAME        : Acipsas7
************************************************************
************************************************************************
* PROGRAM  : ACIPSAS7
* FUNCTION : READ ACIS SECURITY AND PRODUCE A FILE FOR SAS70
*            ENTITLEMENT REVIEW.
*
* HISTORY
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Acipsas7 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Start;
    private DbsField pnd_Start_Pnd_Ky;

    private DbsGroup pnd_Start__R_Field_1;
    private DbsField pnd_Start_Pnd_Table_Id_Nbr;
    private DbsField pnd_Start_Pnd_Table_Sub_Id;
    private DbsField pnd_Start_Pnd_Entry_Cde;

    private DataAccessProgramView vw_app_Table_Entry;
    private DbsField app_Table_Entry_Entry_Actve_Ind;
    private DbsField app_Table_Entry_Entry_Table_Id_Nbr;
    private DbsField app_Table_Entry_Entry_Table_Sub_Id;
    private DbsField app_Table_Entry_Entry_Cde;
    private DbsField app_Table_Entry_Entry_Dscrptn_Txt;
    private DbsField app_Table_Entry_Entry_User_Id;
    private DbsField app_Table_Entry_Update_By;
    private DbsField app_Table_Entry_Update_Date;
    private DbsGroup app_Table_Entry_Entry_Trnsctn_Type_CdeMuGroup;
    private DbsField app_Table_Entry_Entry_Trnsctn_Type_Cde;
    private DbsGroup app_Table_Entry_Entry_Mit_Status_CdeMuGroup;
    private DbsField app_Table_Entry_Entry_Mit_Status_Cde;
    private DbsGroup app_Table_Entry_Addtnl_Dscrptn_TxtMuGroup;
    private DbsField app_Table_Entry_Addtnl_Dscrptn_Txt;
    private DbsGroup app_Table_Entry_Entry_Prgam_CdeMuGroup;
    private DbsField app_Table_Entry_Entry_Prgam_Cde;
    private DbsField app_Table_Entry_Hold_Cde;

    private DataAccessProgramView vw_cwf_Org_Empl_Tbl;
    private DbsField cwf_Org_Empl_Tbl_Empl_Unit_Cde;
    private DbsField cwf_Org_Empl_Tbl_Empl_Racf_Id;
    private DbsField cwf_Org_Empl_Tbl_Empl_Nme;

    private DbsGroup cwf_Org_Empl_Tbl__R_Field_2;
    private DbsField cwf_Org_Empl_Tbl_Empl_First_Nme;
    private DbsField cwf_Org_Empl_Tbl_Empl_Last_Nme;
    private DbsField cwf_Org_Empl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Org_Empl_Tbl_Default_Unit_Ind;

    private DbsGroup pnd_Print_File;
    private DbsField pnd_Print_File_Pnd_User_Id;
    private DbsField pnd_Print_File_Pnd_Lname;
    private DbsField pnd_Print_File_Pnd_Fname;
    private DbsField pnd_Print_File_Pnd_Edesc1;
    private DbsField pnd_Work_File;
    private DbsField pnd_Comma;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Start = localVariables.newGroupInRecord("pnd_Start", "#START");
        pnd_Start_Pnd_Ky = pnd_Start.newFieldInGroup("pnd_Start_Pnd_Ky", "#KY", FieldType.STRING, 28);

        pnd_Start__R_Field_1 = pnd_Start.newGroupInGroup("pnd_Start__R_Field_1", "REDEFINE", pnd_Start_Pnd_Ky);
        pnd_Start_Pnd_Table_Id_Nbr = pnd_Start__R_Field_1.newFieldInGroup("pnd_Start_Pnd_Table_Id_Nbr", "#TABLE-ID-NBR", FieldType.NUMERIC, 6);
        pnd_Start_Pnd_Table_Sub_Id = pnd_Start__R_Field_1.newFieldInGroup("pnd_Start_Pnd_Table_Sub_Id", "#TABLE-SUB-ID", FieldType.STRING, 2);
        pnd_Start_Pnd_Entry_Cde = pnd_Start__R_Field_1.newFieldInGroup("pnd_Start_Pnd_Entry_Cde", "#ENTRY-CDE", FieldType.STRING, 20);

        vw_app_Table_Entry = new DataAccessProgramView(new NameInfo("vw_app_Table_Entry", "APP-TABLE-ENTRY"), "APP_TABLE_ENTRY", "ACIS_APP_TBL_ENT");
        app_Table_Entry_Entry_Actve_Ind = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        app_Table_Entry_Entry_Table_Id_Nbr = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        app_Table_Entry_Entry_Table_Sub_Id = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ENTRY_TABLE_SUB_ID");
        app_Table_Entry_Entry_Cde = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "ENTRY_CDE");
        app_Table_Entry_Entry_Dscrptn_Txt = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ENTRY_DSCRPTN_TXT");
        app_Table_Entry_Entry_User_Id = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Entry_User_Id", "ENTRY-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ENTRY_USER_ID");
        app_Table_Entry_Update_By = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Update_By", "UPDATE-BY", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UPDATE_BY");
        app_Table_Entry_Update_Date = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Update_Date", "UPDATE-DATE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "UPDATE_DATE");
        app_Table_Entry_Entry_Trnsctn_Type_CdeMuGroup = vw_app_Table_Entry.getRecord().newGroupInGroup("APP_TABLE_ENTRY_ENTRY_TRNSCTN_TYPE_CDEMuGroup", 
            "ENTRY_TRNSCTN_TYPE_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "ACIS_APP_TBL_ENT_ENTRY_TRNSCTN_TYPE_CDE");
        app_Table_Entry_Entry_Trnsctn_Type_Cde = app_Table_Entry_Entry_Trnsctn_Type_CdeMuGroup.newFieldArrayInGroup("app_Table_Entry_Entry_Trnsctn_Type_Cde", 
            "ENTRY-TRNSCTN-TYPE-CDE", FieldType.STRING, 1, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArray, "ENTRY_TRNSCTN_TYPE_CDE");
        app_Table_Entry_Entry_Mit_Status_CdeMuGroup = vw_app_Table_Entry.getRecord().newGroupInGroup("APP_TABLE_ENTRY_ENTRY_MIT_STATUS_CDEMuGroup", "ENTRY_MIT_STATUS_CDEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ACIS_APP_TBL_ENT_ENTRY_MIT_STATUS_CDE");
        app_Table_Entry_Entry_Mit_Status_Cde = app_Table_Entry_Entry_Mit_Status_CdeMuGroup.newFieldArrayInGroup("app_Table_Entry_Entry_Mit_Status_Cde", 
            "ENTRY-MIT-STATUS-CDE", FieldType.STRING, 4, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArray, "ENTRY_MIT_STATUS_CDE");
        app_Table_Entry_Addtnl_Dscrptn_TxtMuGroup = vw_app_Table_Entry.getRecord().newGroupInGroup("APP_TABLE_ENTRY_ADDTNL_DSCRPTN_TXTMuGroup", "ADDTNL_DSCRPTN_TXTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ACIS_APP_TBL_ENT_ADDTNL_DSCRPTN_TXT");
        app_Table_Entry_Addtnl_Dscrptn_Txt = app_Table_Entry_Addtnl_Dscrptn_TxtMuGroup.newFieldArrayInGroup("app_Table_Entry_Addtnl_Dscrptn_Txt", "ADDTNL-DSCRPTN-TXT", 
            FieldType.STRING, 60, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADDTNL_DSCRPTN_TXT");
        app_Table_Entry_Entry_Prgam_CdeMuGroup = vw_app_Table_Entry.getRecord().newGroupInGroup("APP_TABLE_ENTRY_ENTRY_PRGAM_CDEMuGroup", "ENTRY_PRGAM_CDEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ACIS_APP_TBL_ENT_ENTRY_PRGAM_CDE");
        app_Table_Entry_Entry_Prgam_Cde = app_Table_Entry_Entry_Prgam_CdeMuGroup.newFieldArrayInGroup("app_Table_Entry_Entry_Prgam_Cde", "ENTRY-PRGAM-CDE", 
            FieldType.STRING, 8, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ENTRY_PRGAM_CDE");
        app_Table_Entry_Hold_Cde = vw_app_Table_Entry.getRecord().newFieldInGroup("app_Table_Entry_Hold_Cde", "HOLD-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "HOLD_CDE");
        registerRecord(vw_app_Table_Entry);

        vw_cwf_Org_Empl_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Org_Empl_Tbl", "CWF-ORG-EMPL-TBL"), "CWF_ORG_EMPL_TBL", "CWF_ASSIGN_RULE");
        cwf_Org_Empl_Tbl_Empl_Unit_Cde = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Unit_Cde", "EMPL-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "EMPL_UNIT_CDE");
        cwf_Org_Empl_Tbl_Empl_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Org_Empl_Tbl_Empl_Racf_Id = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "EMPL_RACF_ID");
        cwf_Org_Empl_Tbl_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF  ID");
        cwf_Org_Empl_Tbl_Empl_Nme = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Nme", "EMPL-NME", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "EMPL_NME");
        cwf_Org_Empl_Tbl_Empl_Nme.setDdmHeader("EMPLOYEE NAME");

        cwf_Org_Empl_Tbl__R_Field_2 = vw_cwf_Org_Empl_Tbl.getRecord().newGroupInGroup("cwf_Org_Empl_Tbl__R_Field_2", "REDEFINE", cwf_Org_Empl_Tbl_Empl_Nme);
        cwf_Org_Empl_Tbl_Empl_First_Nme = cwf_Org_Empl_Tbl__R_Field_2.newFieldInGroup("cwf_Org_Empl_Tbl_Empl_First_Nme", "EMPL-FIRST-NME", FieldType.STRING, 
            20);
        cwf_Org_Empl_Tbl_Empl_Last_Nme = cwf_Org_Empl_Tbl__R_Field_2.newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Last_Nme", "EMPL-LAST-NME", FieldType.STRING, 
            20);
        cwf_Org_Empl_Tbl_Dlte_Dte_Tme = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "DLTE_DTE_TME");
        cwf_Org_Empl_Tbl_Dlte_Dte_Tme.setDdmHeader("DELETE/DATE-TIME");
        cwf_Org_Empl_Tbl_Default_Unit_Ind = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Default_Unit_Ind", "DEFAULT-UNIT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DEFAULT_UNIT_IND");
        registerRecord(vw_cwf_Org_Empl_Tbl);

        pnd_Print_File = localVariables.newGroupInRecord("pnd_Print_File", "#PRINT-FILE");
        pnd_Print_File_Pnd_User_Id = pnd_Print_File.newFieldInGroup("pnd_Print_File_Pnd_User_Id", "#USER-ID", FieldType.STRING, 10);
        pnd_Print_File_Pnd_Lname = pnd_Print_File.newFieldInGroup("pnd_Print_File_Pnd_Lname", "#LNAME", FieldType.STRING, 20);
        pnd_Print_File_Pnd_Fname = pnd_Print_File.newFieldInGroup("pnd_Print_File_Pnd_Fname", "#FNAME", FieldType.STRING, 20);
        pnd_Print_File_Pnd_Edesc1 = pnd_Print_File.newFieldInGroup("pnd_Print_File_Pnd_Edesc1", "#EDESC1", FieldType.STRING, 20);
        pnd_Work_File = localVariables.newFieldInRecord("pnd_Work_File", "#WORK-FILE", FieldType.STRING, 80);
        pnd_Comma = localVariables.newFieldInRecord("pnd_Comma", "#COMMA", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_app_Table_Entry.reset();
        vw_cwf_Org_Empl_Tbl.reset();

        localVariables.reset();
        pnd_Comma.setInitialValue(",");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Acipsas7() throws Exception
    {
        super("Acipsas7");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 60 LS = 133
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        pnd_Start_Pnd_Table_Id_Nbr.setValue(100);                                                                                                                         //Natural: ASSIGN #TABLE-ID-NBR = 000100
        pnd_Start_Pnd_Table_Sub_Id.setValue("VR");                                                                                                                        //Natural: ASSIGN #TABLE-SUB-ID = 'VR'
        pnd_Start_Pnd_Entry_Cde.setValue(" ");                                                                                                                            //Natural: ASSIGN #ENTRY-CDE = ' '
        vw_app_Table_Entry.startDatabaseRead                                                                                                                              //Natural: READ APP-TABLE-ENTRY BY TABLE-ID-ENTRY-CDE STARTING FROM #START.#KY
        (
        "READ01",
        new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_Start_Pnd_Ky, WcType.BY) },
        new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") }
        );
        READ01:
        while (condition(vw_app_Table_Entry.readNextRow("READ01")))
        {
            if (condition(app_Table_Entry_Entry_Table_Sub_Id.greater("VR") || app_Table_Entry_Entry_Table_Id_Nbr.notEquals(100)))                                         //Natural: IF APP-TABLE-ENTRY.ENTRY-TABLE-SUB-ID GT 'VR' OR APP-TABLE-ENTRY.ENTRY-TABLE-ID-NBR NE 000100
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(app_Table_Entry_Entry_Table_Sub_Id.notEquals("VR")))                                                                                            //Natural: REJECT IF APP-TABLE-ENTRY.ENTRY-TABLE-SUB-ID NE 'VR'
            {
                continue;
            }
            pnd_Print_File_Pnd_User_Id.setValue(app_Table_Entry_Entry_Cde.getSubstring(1,8));                                                                             //Natural: ASSIGN #USER-ID := SUBSTR ( ENTRY-CDE,1,8 )
                                                                                                                                                                          //Natural: PERFORM GET-NAME
            sub_Get_Name();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Print_File_Pnd_Edesc1.setValue(app_Table_Entry_Entry_Dscrptn_Txt);                                                                                        //Natural: ASSIGN #EDESC1 := ENTRY-DSCRPTN-TXT
            pnd_Work_File.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Print_File_Pnd_User_Id, pnd_Comma, pnd_Print_File_Pnd_Lname, pnd_Comma,            //Natural: COMPRESS #USER-ID #COMMA #LNAME #COMMA #FNAME #COMMA #EDESC1 #COMMA INTO #WORK-FILE LEAVE NO
                pnd_Print_File_Pnd_Fname, pnd_Comma, pnd_Print_File_Pnd_Edesc1, pnd_Comma));
            getWorkFiles().write(1, false, pnd_Work_File);                                                                                                                //Natural: WRITE WORK FILE 1 #WORK-FILE
            getReports().display(1, "USERID",                                                                                                                             //Natural: DISPLAY ( 1 ) 'USERID' #USER-ID 'LNAME' #LNAME 'FNAME' #FNAME 'ROLE' #EDESC1
            		pnd_Print_File_Pnd_User_Id,"LNAME",
            		pnd_Print_File_Pnd_Lname,"FNAME",
            		pnd_Print_File_Pnd_Fname,"ROLE",
            		pnd_Print_File_Pnd_Edesc1);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME
    }
    private void sub_Get_Name() throws Exception                                                                                                                          //Natural: GET-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Print_File_Pnd_Lname.setValue("N/A");                                                                                                                         //Natural: MOVE 'N/A' TO #LNAME #FNAME
        pnd_Print_File_Pnd_Fname.setValue("N/A");
        vw_cwf_Org_Empl_Tbl.startDatabaseRead                                                                                                                             //Natural: READ CWF-ORG-EMPL-TBL WITH EMPL-RACF-ID-KEY = #USER-ID
        (
        "READ02",
        new Wc[] { new Wc("EMPL_RACF_ID_KEY", ">=", pnd_Print_File_Pnd_User_Id, WcType.BY) },
        new Oc[] { new Oc("EMPL_RACF_ID_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_cwf_Org_Empl_Tbl.readNextRow("READ02")))
        {
            if (condition(cwf_Org_Empl_Tbl_Empl_Racf_Id.notEquals(pnd_Print_File_Pnd_User_Id)))                                                                           //Natural: IF EMPL-RACF-ID NE #USER-ID
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(cwf_Org_Empl_Tbl_Default_Unit_Ind.equals("Y"))))                                                                                              //Natural: ACCEPT IF DEFAULT-UNIT-IND = 'Y'
            {
                continue;
            }
            pnd_Print_File_Pnd_Lname.setValue(cwf_Org_Empl_Tbl_Empl_Last_Nme);                                                                                            //Natural: ASSIGN #LNAME := EMPL-LAST-NME
            pnd_Print_File_Pnd_Fname.setValue(cwf_Org_Empl_Tbl_Empl_First_Nme);                                                                                           //Natural: ASSIGN #FNAME := EMPL-FIRST-NME
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, new ColumnSpacing(10),"ACIS ENTITLEMENTS REPORT",new ColumnSpacing(10),Global.getDATX(), new ReportEditMask                     //Natural: WRITE ( 1 ) 10X 'ACIS ENTITLEMENTS REPORT' 10X *DATX ( EM = MM/DD/YYYY )
                        ("MM/DD/YYYY"));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133");

        getReports().setDisplayColumns(1, "USERID",
        		pnd_Print_File_Pnd_User_Id,"LNAME",
        		pnd_Print_File_Pnd_Lname,"FNAME",
        		pnd_Print_File_Pnd_Fname,"ROLE",
        		pnd_Print_File_Pnd_Edesc1);
    }
}
