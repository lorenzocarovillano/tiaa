/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:05:55 PM
**        * FROM NATURAL PROGRAM : Adsp965
************************************************************
**        * FILE NAME            : Adsp965.java
**        * CLASS NAME           : Adsp965
**        * INSTANCE NAME        : Adsp965
************************************************************
************************************************************************
* PROGRAM  : ADSP965
* GENERATED: APRIL 13, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : READ PARTICIPANT RECORDS AND WRITE  WORKFILE OF REJECTED
*            REQUESTS
*
* REMARKS  : CLONED FROM NAZP965 (ADAM SYSTEM)
************************************************************************
* MOD DATE   MOD BY      DESCRIPTION OF CHANGES
* 12-09-98  C SCHNEIDER CHANGE UNIT SZ FROM 6 TO 8 FOR REGIONALIZATION
* 05-99     E MELNIK    RQSTS WITH STATUS CODES 'W20' AND 'W25' ALSO
*                       WILL BE ACCEPTED AND WRITTEN TO THE WORK FILE.
* 04/13/04  C AVE       MODIFIED FOR ADAS (ANNUITIZATION SUNGUARD)
* 03/10/10  D.E.ANDER   MODIFIED FOR ADABAS REPLATFORM FOR ADDING
*                       SURVIVOR PIN.          MARKED DEA
* 06/24/14  O. SOTTO    RECOMPILED FOR LATEST ADS-CNTRCT DDM.
* 02/23/2017 R.CARREON  PIN EXPANSION 02232017
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp965 extends BLNatBase
{
    // Data Areas
    private PdaAdspda_M pdaAdspda_M;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ads_Prtcpnt_View;
    private DbsField ads_Prtcpnt_View_Rqst_Id;
    private DbsField ads_Prtcpnt_View_Adp_Bps_Unit;
    private DbsField ads_Prtcpnt_View_Adp_Unique_Id;
    private DbsField ads_Prtcpnt_View_Adp_Effctv_Dte;
    private DbsField ads_Prtcpnt_View_Adp_Opn_Clsd_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Stts_Cde;

    private DbsGroup ads_Prtcpnt_View__R_Field_1;
    private DbsField ads_Prtcpnt_View_Pnd_Adp_Stts_Cde_1st_Byte;
    private DbsField ads_Prtcpnt_View_Pnd_Adp_Stts_Cde_Rest;
    private DbsField ads_Prtcpnt_View_Adp_Ia_Tiaa_Nbr;
    private DbsField ads_Prtcpnt_View_Adp_Rqst_Last_Updtd_Dte;
    private DbsField ads_Prtcpnt_View_Adp_Annty_Strt_Dte;
    private DbsField ads_Prtcpnt_View_Adp_Annt_Typ_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Srvvr_Unique_Id;

    private DataAccessProgramView vw_ads_Cntrct_View;
    private DbsField ads_Cntrct_View_Rqst_Id;
    private DbsField ads_Cntrct_View_Adc_Tiaa_Nbr;
    private DbsField ads_Cntrct_View_Adc_Stts_Cde;

    private DbsGroup ads_Cntrct_View__R_Field_2;
    private DbsField ads_Cntrct_View_Pnd_Adc_Stts_Cde_1st_Byte;
    private DbsField ads_Cntrct_View_Pnd_Adc_Stts_Cde_Rest;
    private DbsField pnd_Adp_Sd2;

    private DbsGroup pnd_Adp_Sd2__R_Field_3;
    private DbsField pnd_Adp_Sd2_Pnd_Sd2_Opn_Clsd_Ind;
    private DbsField pnd_Adp_Sd2_Pnd_Sd2_Stts_Cde;

    private DbsGroup pnd_Factor_Interface;
    private DbsField pnd_Factor_Interface_Pnd_Factor_Date;

    private DbsGroup pnd_Factor_Interface__R_Field_4;
    private DbsField pnd_Factor_Interface_Pnd_Factor_Date_N;

    private DbsGroup pnd_Factor_Interface__R_Field_5;
    private DbsField pnd_Factor_Interface_Pnd_Factor_Mm;
    private DbsField pnd_Factor_Interface_Pnd_Factor_Dd;
    private DbsField pnd_Factor_Interface_Pnd_Factor_Yyyy;
    private DbsField pnd_Factor_Interface_Pnd_Factor_Error_Message;
    private DbsField pnd_Factor_Avail_Date;

    private DbsGroup pnd_Factor_Avail_Date__R_Field_6;
    private DbsField pnd_Factor_Avail_Date_Pnd_Factor_Avail_Ccyy;
    private DbsField pnd_Factor_Avail_Date_Pnd_Factor_Avail_Mm;
    private DbsField pnd_Factor_Avail_Date_Pnd_Factor_Avail_Dd;
    private DbsField pnd_Dte_Ccyymmdd;

    private DbsGroup pnd_Dte_Ccyymmdd__R_Field_7;
    private DbsField pnd_Dte_Ccyymmdd_Pnd_Dte_Ccyymmdd_Alpha;
    private DbsField pnd_Wkf;

    private DbsGroup pnd_Wkf__R_Field_8;
    private DbsField pnd_Wkf_Pnd_Wkf_Bps_Unit;
    private DbsField pnd_Wkf_Pnd_Wkf_Cntrct;
    private DbsField pnd_Wkf_Pnd_Wkf_Unique_Id;
    private DbsField pnd_Wkf_Pnd_Wkf_Eff_Dte;
    private DbsField pnd_Wkf_Pnd_Wkf_Adp_Stts_Cde;
    private DbsField pnd_Wkf_Pnd_Wkf_Adc_Stts_Cde;
    private DbsField pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr;
    private DbsField pnd_Wkf_Pnd_Wkf_Lst_Updte;
    private DbsField pnd_Wkf_Pnd_Wkf_Annty_Strt_Dte;
    private DbsField pnd_Wkf_Pnd_Wkf_Repl_Ind;
    private DbsField pnd_Wkf_Survivor_Pin;
    private DbsField pnd_X;
    private DbsField pnd_Y;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAdspda_M = new PdaAdspda_M(localVariables);

        // Local Variables

        vw_ads_Prtcpnt_View = new DataAccessProgramView(new NameInfo("vw_ads_Prtcpnt_View", "ADS-PRTCPNT-VIEW"), "ADS_PRTCPNT", "ADS_PRTCPNT");
        ads_Prtcpnt_View_Rqst_Id = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "RQST_ID");
        ads_Prtcpnt_View_Adp_Bps_Unit = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Bps_Unit", "ADP-BPS-UNIT", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADP_BPS_UNIT");
        ads_Prtcpnt_View_Adp_Unique_Id = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Unique_Id", "ADP-UNIQUE-ID", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "ADP_UNIQUE_ID");
        ads_Prtcpnt_View_Adp_Effctv_Dte = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Effctv_Dte", "ADP-EFFCTV-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADP_EFFCTV_DTE");
        ads_Prtcpnt_View_Adp_Opn_Clsd_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Opn_Clsd_Ind", "ADP-OPN-CLSD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_OPN_CLSD_IND");
        ads_Prtcpnt_View_Adp_Stts_Cde = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Stts_Cde", "ADP-STTS-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "ADP_STTS_CDE");

        ads_Prtcpnt_View__R_Field_1 = vw_ads_Prtcpnt_View.getRecord().newGroupInGroup("ads_Prtcpnt_View__R_Field_1", "REDEFINE", ads_Prtcpnt_View_Adp_Stts_Cde);
        ads_Prtcpnt_View_Pnd_Adp_Stts_Cde_1st_Byte = ads_Prtcpnt_View__R_Field_1.newFieldInGroup("ads_Prtcpnt_View_Pnd_Adp_Stts_Cde_1st_Byte", "#ADP-STTS-CDE-1ST-BYTE", 
            FieldType.STRING, 1);
        ads_Prtcpnt_View_Pnd_Adp_Stts_Cde_Rest = ads_Prtcpnt_View__R_Field_1.newFieldInGroup("ads_Prtcpnt_View_Pnd_Adp_Stts_Cde_Rest", "#ADP-STTS-CDE-REST", 
            FieldType.STRING, 2);
        ads_Prtcpnt_View_Adp_Ia_Tiaa_Nbr = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Ia_Tiaa_Nbr", "ADP-IA-TIAA-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "ADP_IA_TIAA_NBR");
        ads_Prtcpnt_View_Adp_Rqst_Last_Updtd_Dte = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Rqst_Last_Updtd_Dte", "ADP-RQST-LAST-UPDTD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_RQST_LAST_UPDTD_DTE");
        ads_Prtcpnt_View_Adp_Annty_Strt_Dte = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Annty_Strt_Dte", "ADP-ANNTY-STRT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_ANNTY_STRT_DTE");
        ads_Prtcpnt_View_Adp_Annt_Typ_Cde = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Annt_Typ_Cde", "ADP-ANNT-TYP-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_ANNT_TYP_CDE");
        ads_Prtcpnt_View_Adp_Srvvr_Unique_Id = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Srvvr_Unique_Id", "ADP-SRVVR-UNIQUE-ID", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "ADP_SRVVR_UNIQUE_ID");
        registerRecord(vw_ads_Prtcpnt_View);

        vw_ads_Cntrct_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntrct_View", "ADS-CNTRCT-VIEW"), "ADS_CNTRCT", "DS_CNTRCT");
        ads_Cntrct_View_Rqst_Id = vw_ads_Cntrct_View.getRecord().newFieldInGroup("ads_Cntrct_View_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "RQST_ID");
        ads_Cntrct_View_Adc_Tiaa_Nbr = vw_ads_Cntrct_View.getRecord().newFieldInGroup("ads_Cntrct_View_Adc_Tiaa_Nbr", "ADC-TIAA-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "ADC_TIAA_NBR");
        ads_Cntrct_View_Adc_Stts_Cde = vw_ads_Cntrct_View.getRecord().newFieldInGroup("ads_Cntrct_View_Adc_Stts_Cde", "ADC-STTS-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "ADC_STTS_CDE");

        ads_Cntrct_View__R_Field_2 = vw_ads_Cntrct_View.getRecord().newGroupInGroup("ads_Cntrct_View__R_Field_2", "REDEFINE", ads_Cntrct_View_Adc_Stts_Cde);
        ads_Cntrct_View_Pnd_Adc_Stts_Cde_1st_Byte = ads_Cntrct_View__R_Field_2.newFieldInGroup("ads_Cntrct_View_Pnd_Adc_Stts_Cde_1st_Byte", "#ADC-STTS-CDE-1ST-BYTE", 
            FieldType.STRING, 1);
        ads_Cntrct_View_Pnd_Adc_Stts_Cde_Rest = ads_Cntrct_View__R_Field_2.newFieldInGroup("ads_Cntrct_View_Pnd_Adc_Stts_Cde_Rest", "#ADC-STTS-CDE-REST", 
            FieldType.STRING, 2);
        registerRecord(vw_ads_Cntrct_View);

        pnd_Adp_Sd2 = localVariables.newFieldInRecord("pnd_Adp_Sd2", "#ADP-SD2", FieldType.STRING, 4);

        pnd_Adp_Sd2__R_Field_3 = localVariables.newGroupInRecord("pnd_Adp_Sd2__R_Field_3", "REDEFINE", pnd_Adp_Sd2);
        pnd_Adp_Sd2_Pnd_Sd2_Opn_Clsd_Ind = pnd_Adp_Sd2__R_Field_3.newFieldInGroup("pnd_Adp_Sd2_Pnd_Sd2_Opn_Clsd_Ind", "#SD2-OPN-CLSD-IND", FieldType.STRING, 
            1);
        pnd_Adp_Sd2_Pnd_Sd2_Stts_Cde = pnd_Adp_Sd2__R_Field_3.newFieldInGroup("pnd_Adp_Sd2_Pnd_Sd2_Stts_Cde", "#SD2-STTS-CDE", FieldType.STRING, 3);

        pnd_Factor_Interface = localVariables.newGroupInRecord("pnd_Factor_Interface", "#FACTOR-INTERFACE");
        pnd_Factor_Interface_Pnd_Factor_Date = pnd_Factor_Interface.newFieldInGroup("pnd_Factor_Interface_Pnd_Factor_Date", "#FACTOR-DATE", FieldType.STRING, 
            8);

        pnd_Factor_Interface__R_Field_4 = pnd_Factor_Interface.newGroupInGroup("pnd_Factor_Interface__R_Field_4", "REDEFINE", pnd_Factor_Interface_Pnd_Factor_Date);
        pnd_Factor_Interface_Pnd_Factor_Date_N = pnd_Factor_Interface__R_Field_4.newFieldInGroup("pnd_Factor_Interface_Pnd_Factor_Date_N", "#FACTOR-DATE-N", 
            FieldType.NUMERIC, 8);

        pnd_Factor_Interface__R_Field_5 = pnd_Factor_Interface__R_Field_4.newGroupInGroup("pnd_Factor_Interface__R_Field_5", "REDEFINE", pnd_Factor_Interface_Pnd_Factor_Date_N);
        pnd_Factor_Interface_Pnd_Factor_Mm = pnd_Factor_Interface__R_Field_5.newFieldInGroup("pnd_Factor_Interface_Pnd_Factor_Mm", "#FACTOR-MM", FieldType.NUMERIC, 
            2);
        pnd_Factor_Interface_Pnd_Factor_Dd = pnd_Factor_Interface__R_Field_5.newFieldInGroup("pnd_Factor_Interface_Pnd_Factor_Dd", "#FACTOR-DD", FieldType.NUMERIC, 
            2);
        pnd_Factor_Interface_Pnd_Factor_Yyyy = pnd_Factor_Interface__R_Field_5.newFieldInGroup("pnd_Factor_Interface_Pnd_Factor_Yyyy", "#FACTOR-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Factor_Interface_Pnd_Factor_Error_Message = pnd_Factor_Interface.newFieldInGroup("pnd_Factor_Interface_Pnd_Factor_Error_Message", "#FACTOR-ERROR-MESSAGE", 
            FieldType.STRING, 79);
        pnd_Factor_Avail_Date = localVariables.newFieldInRecord("pnd_Factor_Avail_Date", "#FACTOR-AVAIL-DATE", FieldType.NUMERIC, 8);

        pnd_Factor_Avail_Date__R_Field_6 = localVariables.newGroupInRecord("pnd_Factor_Avail_Date__R_Field_6", "REDEFINE", pnd_Factor_Avail_Date);
        pnd_Factor_Avail_Date_Pnd_Factor_Avail_Ccyy = pnd_Factor_Avail_Date__R_Field_6.newFieldInGroup("pnd_Factor_Avail_Date_Pnd_Factor_Avail_Ccyy", 
            "#FACTOR-AVAIL-CCYY", FieldType.NUMERIC, 4);
        pnd_Factor_Avail_Date_Pnd_Factor_Avail_Mm = pnd_Factor_Avail_Date__R_Field_6.newFieldInGroup("pnd_Factor_Avail_Date_Pnd_Factor_Avail_Mm", "#FACTOR-AVAIL-MM", 
            FieldType.NUMERIC, 2);
        pnd_Factor_Avail_Date_Pnd_Factor_Avail_Dd = pnd_Factor_Avail_Date__R_Field_6.newFieldInGroup("pnd_Factor_Avail_Date_Pnd_Factor_Avail_Dd", "#FACTOR-AVAIL-DD", 
            FieldType.NUMERIC, 2);
        pnd_Dte_Ccyymmdd = localVariables.newFieldInRecord("pnd_Dte_Ccyymmdd", "#DTE-CCYYMMDD", FieldType.NUMERIC, 8);

        pnd_Dte_Ccyymmdd__R_Field_7 = localVariables.newGroupInRecord("pnd_Dte_Ccyymmdd__R_Field_7", "REDEFINE", pnd_Dte_Ccyymmdd);
        pnd_Dte_Ccyymmdd_Pnd_Dte_Ccyymmdd_Alpha = pnd_Dte_Ccyymmdd__R_Field_7.newFieldInGroup("pnd_Dte_Ccyymmdd_Pnd_Dte_Ccyymmdd_Alpha", "#DTE-CCYYMMDD-ALPHA", 
            FieldType.STRING, 8);
        pnd_Wkf = localVariables.newFieldInRecord("pnd_Wkf", "#WKF", FieldType.STRING, 71);

        pnd_Wkf__R_Field_8 = localVariables.newGroupInRecord("pnd_Wkf__R_Field_8", "REDEFINE", pnd_Wkf);
        pnd_Wkf_Pnd_Wkf_Bps_Unit = pnd_Wkf__R_Field_8.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Bps_Unit", "#WKF-BPS-UNIT", FieldType.STRING, 8);
        pnd_Wkf_Pnd_Wkf_Cntrct = pnd_Wkf__R_Field_8.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Cntrct", "#WKF-CNTRCT", FieldType.STRING, 10);
        pnd_Wkf_Pnd_Wkf_Unique_Id = pnd_Wkf__R_Field_8.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Unique_Id", "#WKF-UNIQUE-ID", FieldType.NUMERIC, 12);
        pnd_Wkf_Pnd_Wkf_Eff_Dte = pnd_Wkf__R_Field_8.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Eff_Dte", "#WKF-EFF-DTE", FieldType.STRING, 8);
        pnd_Wkf_Pnd_Wkf_Adp_Stts_Cde = pnd_Wkf__R_Field_8.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Adp_Stts_Cde", "#WKF-ADP-STTS-CDE", FieldType.STRING, 3);
        pnd_Wkf_Pnd_Wkf_Adc_Stts_Cde = pnd_Wkf__R_Field_8.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Adc_Stts_Cde", "#WKF-ADC-STTS-CDE", FieldType.STRING, 3);
        pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr = pnd_Wkf__R_Field_8.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr", "#WKF-IA-TIAA-NBR", FieldType.STRING, 10);
        pnd_Wkf_Pnd_Wkf_Lst_Updte = pnd_Wkf__R_Field_8.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Lst_Updte", "#WKF-LST-UPDTE", FieldType.NUMERIC, 8);
        pnd_Wkf_Pnd_Wkf_Annty_Strt_Dte = pnd_Wkf__R_Field_8.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Annty_Strt_Dte", "#WKF-ANNTY-STRT-DTE", FieldType.NUMERIC, 
            8);
        pnd_Wkf_Pnd_Wkf_Repl_Ind = pnd_Wkf__R_Field_8.newFieldInGroup("pnd_Wkf_Pnd_Wkf_Repl_Ind", "#WKF-REPL-IND", FieldType.STRING, 1);
        pnd_Wkf_Survivor_Pin = localVariables.newFieldInRecord("pnd_Wkf_Survivor_Pin", "#WKF-SURVIVOR-PIN", FieldType.NUMERIC, 12);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.INTEGER, 4);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.INTEGER, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Prtcpnt_View.reset();
        vw_ads_Cntrct_View.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp965() throws Exception
    {
        super("Adsp965");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* ******************************************************************
        //*                                                                                                                                                               //Natural: FORMAT LS = 133 PS = 60
        pnd_Adp_Sd2_Pnd_Sd2_Opn_Clsd_Ind.setValue("C");                                                                                                                   //Natural: ASSIGN #SD2-OPN-CLSD-IND = 'C'
        pnd_Adp_Sd2_Pnd_Sd2_Stts_Cde.setValue("L00");                                                                                                                     //Natural: ASSIGN #SD2-STTS-CDE = 'L00'
        vw_ads_Prtcpnt_View.startDatabaseRead                                                                                                                             //Natural: READ ADS-PRTCPNT-VIEW BY ADP-SUPER2 STARTING FROM #ADP-SD2
        (
        "READ01",
        new Wc[] { new Wc("ADP_SUPER2", ">=", pnd_Adp_Sd2, WcType.BY) },
        new Oc[] { new Oc("ADP_SUPER2", "ASC") }
        );
        READ01:
        while (condition(vw_ads_Prtcpnt_View.readNextRow("READ01")))
        {
            if (condition(ads_Prtcpnt_View_Adp_Opn_Clsd_Ind.notEquals("C") || ads_Prtcpnt_View_Pnd_Adp_Stts_Cde_1st_Byte.greater("L")))                                   //Natural: IF ADS-PRTCPNT-VIEW.ADP-OPN-CLSD-IND NE 'C' OR #ADP-STTS-CDE-1ST-BYTE GT 'L'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(ads_Prtcpnt_View_Pnd_Adp_Stts_Cde_1st_Byte.equals("L"))))                                                                                     //Natural: ACCEPT IF #ADP-STTS-CDE-1ST-BYTE = 'L'
            {
                continue;
            }
            pnd_Dte_Ccyymmdd_Pnd_Dte_Ccyymmdd_Alpha.setValueEdited(ads_Prtcpnt_View_Adp_Effctv_Dte,new ReportEditMask("YYYYMMDD"));                                       //Natural: MOVE EDITED ADS-PRTCPNT-VIEW.ADP-EFFCTV-DTE ( EM = YYYYMMDD ) TO #DTE-CCYYMMDD-ALPHA
            pnd_Wkf_Pnd_Wkf_Adp_Stts_Cde.setValue(ads_Prtcpnt_View_Adp_Stts_Cde);                                                                                         //Natural: ASSIGN #WKF-ADP-STTS-CDE = ADS-PRTCPNT-VIEW.ADP-STTS-CDE
            pnd_Wkf_Survivor_Pin.setValue(ads_Prtcpnt_View_Adp_Srvvr_Unique_Id);                                                                                          //Natural: ASSIGN #WKF-SURVIVOR-PIN := ADP-SRVVR-UNIQUE-ID
                                                                                                                                                                          //Natural: PERFORM WRITE-WKF-RCDS
            sub_Write_Wkf_Rcds();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WKF-RCDS
    }
    private void sub_Write_Wkf_Rcds() throws Exception                                                                                                                    //Natural: WRITE-WKF-RCDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************************
        vw_ads_Cntrct_View.startDatabaseFind                                                                                                                              //Natural: FIND ADS-CNTRCT-VIEW WITH RQST-ID = ADS-PRTCPNT-VIEW.RQST-ID
        (
        "FIND01",
        new Wc[] { new Wc("RQST_ID", "=", ads_Prtcpnt_View_Rqst_Id, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_ads_Cntrct_View.readNextRow("FIND01")))
        {
            vw_ads_Cntrct_View.setIfNotFoundControlFlag(false);
            pnd_Wkf_Pnd_Wkf_Bps_Unit.setValue(ads_Prtcpnt_View_Adp_Bps_Unit);                                                                                             //Natural: ASSIGN #WKF-BPS-UNIT = ADS-PRTCPNT-VIEW.ADP-BPS-UNIT
            pnd_Wkf_Pnd_Wkf_Cntrct.setValue(ads_Cntrct_View_Adc_Tiaa_Nbr);                                                                                                //Natural: ASSIGN #WKF-CNTRCT = ADS-CNTRCT-VIEW.ADC-TIAA-NBR
            pnd_Wkf_Pnd_Wkf_Unique_Id.setValue(ads_Prtcpnt_View_Adp_Unique_Id);                                                                                           //Natural: ASSIGN #WKF-UNIQUE-ID = ADS-PRTCPNT-VIEW.ADP-UNIQUE-ID
            pnd_Wkf_Pnd_Wkf_Eff_Dte.setValueEdited(ads_Prtcpnt_View_Adp_Effctv_Dte,new ReportEditMask("YYYYMMDD"));                                                       //Natural: MOVE EDITED ADS-PRTCPNT-VIEW.ADP-EFFCTV-DTE ( EM = YYYYMMDD ) TO #WKF-EFF-DTE
            pnd_Wkf_Pnd_Wkf_Adc_Stts_Cde.setValue(ads_Cntrct_View_Adc_Stts_Cde);                                                                                          //Natural: ASSIGN #WKF-ADC-STTS-CDE = ADS-CNTRCT-VIEW.ADC-STTS-CDE
            pnd_Wkf_Pnd_Wkf_Ia_Tiaa_Nbr.setValue(ads_Prtcpnt_View_Adp_Ia_Tiaa_Nbr);                                                                                       //Natural: ASSIGN #WKF-IA-TIAA-NBR = ADS-PRTCPNT-VIEW.ADP-IA-TIAA-NBR
            pnd_Dte_Ccyymmdd_Pnd_Dte_Ccyymmdd_Alpha.setValueEdited(ads_Prtcpnt_View_Adp_Rqst_Last_Updtd_Dte,new ReportEditMask("YYYYMMDD"));                              //Natural: MOVE EDITED ADS-PRTCPNT-VIEW.ADP-RQST-LAST-UPDTD-DTE ( EM = YYYYMMDD ) TO #DTE-CCYYMMDD-ALPHA
            pnd_Wkf_Pnd_Wkf_Lst_Updte.setValue(pnd_Dte_Ccyymmdd);                                                                                                         //Natural: ASSIGN #WKF-LST-UPDTE = #DTE-CCYYMMDD
            pnd_Dte_Ccyymmdd_Pnd_Dte_Ccyymmdd_Alpha.setValueEdited(ads_Prtcpnt_View_Adp_Rqst_Last_Updtd_Dte,new ReportEditMask("YYYYMMDD"));                              //Natural: MOVE EDITED ADS-PRTCPNT-VIEW.ADP-RQST-LAST-UPDTD-DTE ( EM = YYYYMMDD ) TO #DTE-CCYYMMDD-ALPHA
            pnd_Wkf_Pnd_Wkf_Lst_Updte.setValue(pnd_Dte_Ccyymmdd);                                                                                                         //Natural: ASSIGN #WKF-LST-UPDTE = #DTE-CCYYMMDD
            pnd_Dte_Ccyymmdd_Pnd_Dte_Ccyymmdd_Alpha.setValueEdited(ads_Prtcpnt_View_Adp_Annty_Strt_Dte,new ReportEditMask("YYYYMMDD"));                                   //Natural: MOVE EDITED ADS-PRTCPNT-VIEW.ADP-ANNTY-STRT-DTE ( EM = YYYYMMDD ) TO #DTE-CCYYMMDD-ALPHA
            pnd_Wkf_Pnd_Wkf_Annty_Strt_Dte.setValue(pnd_Dte_Ccyymmdd);                                                                                                    //Natural: ASSIGN #WKF-ANNTY-STRT-DTE = #DTE-CCYYMMDD
            if (condition(ads_Prtcpnt_View_Adp_Annt_Typ_Cde.equals("R")))                                                                                                 //Natural: IF ADS-PRTCPNT-VIEW.ADP-ANNT-TYP-CDE = 'R'
            {
                pnd_Wkf_Pnd_Wkf_Repl_Ind.setValue("Y");                                                                                                                   //Natural: ASSIGN #WKF-REPL-IND = 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Wkf_Pnd_Wkf_Repl_Ind.setValue(" ");                                                                                                                   //Natural: ASSIGN #WKF-REPL-IND = ' '
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(1, false, pnd_Wkf);                                                                                                                      //Natural: WRITE WORK FILE 01 #WKF
            //*  DEA START
            if (condition(pnd_Wkf_Survivor_Pin.greater(getZero())))                                                                                                       //Natural: IF #WKF-SURVIVOR-PIN GT 0
            {
                getWorkFiles().write(2, false, pnd_Wkf, pnd_Wkf_Survivor_Pin);                                                                                            //Natural: WRITE WORK FILE 02 #WKF #WKF-SURVIVOR-PIN
                //*  DEA END
            }                                                                                                                                                             //Natural: END-IF
            pnd_X.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #X
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60");
    }
}
