/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:01:50 PM
**        * FROM NATURAL PROGRAM : Adsp1331
************************************************************
**        * FILE NAME            : Adsp1331.java
**        * CLASS NAME           : Adsp1331
**        * INSTANCE NAME        : Adsp1331
************************************************************
************************************************************************
* PROGRAM  : ADSP1331
* GENERATED: MARCH 15, 2010
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : THIS MODULE READS WORK FILE AND REPORTS A GRAND TOTAL OF
*            ALL MODE WITHIN ADI-ANNTY-START-DT USING PP 999 RECORD
*            (MATURITIES TO IAA INTERFACE)
*
* REMARKS  : CLONED FROM ADSP1330   (DEA)
*********************  MAINTENANCE LOG *********************************
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* 11/16/00  F.TWAHIR TPA AND IO CHANGES
* 11/18/03  O SOTTO  99 RATES CHANGES.
* 04/15/04  C. AVE   MODIFIED FOR ADAS (ANNUITIZATION SUNGUARD)
* 07/27/04  T. SHINE     IF MONTH-END PROCESS FALLS ON WEEKEND OR
*                        HOLIDAY, USE ADS-RPT-13 (FACTOR DATE) AS
*                        REPORT DATE.
* 11/18/04  T.SHINE CHECK FOR END OF MONTH PROCESSING
* 05/06/08  GUERRERO ADDED NEW ROTH ONLY REPORT.
* 01/13/09  SACHARNY EXPAND PARM AREA FOR ADSN888 (RS0)
* 03/15/10 D.E.ANDER ADABAS REPLATFORM PCPOP - ADD SURVIVOR INFO DEA
* 03/05/12 E. MELNIK RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                    UPDATED LINKAGE AREAS.  CHANGES MARKED BY
*                    EM - 030512.
* 10/2013   O. SOTTO  CREF REA CHANGES MARKED WITH OS-102013.
* 02/24/2017 R. CARREON PIN EXPANSION 02242017
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp1331 extends BLNatBase
{
    // Data Areas
    private LdaAdsl1330 ldaAdsl1330;
    private LdaAdsl450 ldaAdsl450;
    private LdaAdsl450a ldaAdsl450a;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ads_Ia_Rslt2;
    private DbsField ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data;

    private DbsGroup ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt;

    private DbsGroup ads_Ia_Rslt2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt;
    private DbsGroup ads_Ia_Rslt2_Adi_Contract_IdMuGroup;
    private DbsField ads_Ia_Rslt2_Adi_Contract_Id;
    private DbsField pnd_Rec_Type;

    private DbsGroup pnd_Rec_Type__R_Field_1;
    private DbsField pnd_Rec_Type_Pnd_Filler;
    private DbsField pnd_Rec_Type_Pnd_Z_Indx;
    private DbsField pnd_Nap_Pymnt_Mode;

    private DbsGroup pnd_Pas_Ext_Cntrl_02_Work_Area;

    private DbsGroup pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Table;
    private DbsField pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1;

    private DataAccessProgramView vw_ads_Cntl_View;

    private DbsGroup ads_Cntl_View_Ads_Cntl_Grp;
    private DbsField ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte;
    private DbsField ads_Cntl_View_Ads_Daily_Actvty_Dte;
    private DbsField ads_Cntl_View_Ads_Rpt_13;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1_View;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte;

    private DbsGroup pnd_All_Indx;
    private DbsField pnd_All_Indx_Pnd_A;
    private DbsField pnd_All_Indx_Pnd_B;
    private DbsField pnd_All_Indx_Pnd_C_Indx;
    private DbsField pnd_All_Indx_Pnd_Next_Indx;
    private DbsField pnd_All_Indx_Pnd_Prod_Indx;
    private DbsField pnd_Total_Number_Rec;
    private DbsField pnd_Total_Number_Rec_Roth;
    private DbsField pnd_First_Time;
    private DbsField pnd_Tiaa_Prod_Code;
    private DbsField pnd_Cref_Prod_Found;
    private DbsField pnd_Roth_Only_Run;
    private DbsField pnd_Prev_Bsnss_Dte_D;
    private DbsField pnd_Prev_Bsnss_Dte_N;

    private DbsGroup pnd_Prev_Bsnss_Dte_N__R_Field_2;
    private DbsField pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A;
    private DbsField pnd_Payment_Due_Date_Yyyymmdd;

    private DbsGroup pnd_Payment_Due_Date_Yyyymmdd__R_Field_3;
    private DbsField pnd_Payment_Due_Date_Yyyymmdd_Pnd_Payment_Due_Date_Dte_Ccyy;
    private DbsField pnd_Payment_Due_Date_Yyyymmdd_Pnd_Payment_Due_Date_Dte_Mmdd;
    private DbsField pnd_Today_Business_Date;

    private DbsGroup pnd_Today_Business_Date__R_Field_4;
    private DbsField pnd_Today_Business_Date_Pnd_Today_Business_Date_Cc;
    private DbsField pnd_Today_Business_Date_Pnd_Today_Business_Date_Yy;
    private DbsField pnd_Today_Business_Date_Pnd_Today_Business_Date_Mm;
    private DbsField pnd_Today_Business_Date_Pnd_Today_Business_Date_Dd;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd;

    private DbsGroup pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_5;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd;
    private DbsField pnd_Curr_Accounting_Dte_Yyyymmdd;

    private DbsGroup pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_6;
    private DbsField pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Cc;
    private DbsField pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Yy;
    private DbsField pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Mm;
    private DbsField pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Dd;
    private DbsField pnd_Check_Cycle_Date_Yyyymmdd;

    private DbsGroup pnd_Check_Cycle_Date_Yyyymmdd__R_Field_7;
    private DbsField pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Dte_Cc;
    private DbsField pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Dte_Yy;
    private DbsField pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Dte_Mm;
    private DbsField pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Dte_Dd;
    private DbsField pnd_Rqst_Id;

    private DbsGroup pnd_Rqst_Id__R_Field_8;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Unique_Id;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte;

    private DbsGroup pnd_Rqst_Id__R_Field_9;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Cc;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Yy;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Mm;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Dd;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Entry_Dte;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Entry_Tme;
    private DbsField pnd_Payment_Mode;

    private DbsGroup pnd_Payment_Mode__R_Field_10;
    private DbsField pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1;
    private DbsField pnd_Payment_Mode_Pnd_Payment_Mode_Byte_2;
    private DbsField pnd_Pec_Nbr_Active_Acct;
    private DbsField pnd_Heading;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_11;
    private DbsField pnd_Date_A_Pnd_Date_N;
    private DbsField pnd_Ads_Ia_Super1;

    private DbsGroup pnd_Ads_Ia_Super1__R_Field_12;
    private DbsField pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rqst_Id;
    private DbsField pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde;
    private DbsField pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr;
    private DbsField pnd_Max_Rslt;
    private DbsField pnd_Max_Rates;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl1330 = new LdaAdsl1330();
        registerRecord(ldaAdsl1330);
        ldaAdsl450 = new LdaAdsl450();
        registerRecord(ldaAdsl450);
        registerRecord(ldaAdsl450.getVw_ads_Ia_Rslt_View());
        ldaAdsl450a = new LdaAdsl450a();
        registerRecord(ldaAdsl450a);

        // Local Variables
        localVariables = new DbsRecord();

        vw_ads_Ia_Rslt2 = new DataAccessProgramView(new NameInfo("vw_ads_Ia_Rslt2", "ADS-IA-RSLT2"), "ADS_IA_RSLT", "ADS_IA_RSLT", DdmPeriodicGroups.getInstance().getGroups("ADS_IA_RSLT"));
        ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data = vw_ads_Ia_Rslt2.getRecord().newFieldInGroup("ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data", "C*ADI-DTL-TIAA-DATA", 
            RepeatingFieldStrategy.CAsteriskVariable, "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");

        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data = vw_ads_Ia_Rslt2.getRecord().newGroupInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data", "ADI-DTL-TIAA-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd", "ADI-DTL-TIAA-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt", "ADI-DTL-TIAA-DA-STD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt", "ADI-DTL-TIAA-DA-GRD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd", "ADI-DTL-TIAA-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_ACCT_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd", "ADI-DTL-TIAA-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_IA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst", "ADI-DTL-TIAA-EFF-RTE-INTRST", 
            FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_EFF_RTE_INTRST", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt", "ADI-DTL-TIAA-GRD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt", "ADI-DTL-TIAA-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt", "ADI-DTL-FNL-GRD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt", "ADI-DTL-FNL-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt", "ADI-DTL-TIAA-STD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt", "ADI-DTL-TIAA-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt", "ADI-DTL-FNL-STD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt", "ADI-DTL-FNL-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");

        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat = vw_ads_Ia_Rslt2.getRecord().newGroupInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat", "ADI-DTL-TIAA-TPA-GUAR-COMMUT-DAT", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt", 
            "ADI-DTL-TIAA-GUAR-COMMUT-STD-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_TIAA_GUAR_COMMUT_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt", 
            "ADI-DTL-TIAA-GUAR-COMMUT-GRD-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_TIAA_GUAR_COMMUT_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        ads_Ia_Rslt2_Adi_Contract_IdMuGroup = vw_ads_Ia_Rslt2.getRecord().newGroupInGroup("ADS_IA_RSLT2_ADI_CONTRACT_IDMuGroup", "ADI_CONTRACT_IDMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_IA_RSLT_ADI_CONTRACT_ID");
        ads_Ia_Rslt2_Adi_Contract_Id = ads_Ia_Rslt2_Adi_Contract_IdMuGroup.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Contract_Id", "ADI-CONTRACT-ID", FieldType.NUMERIC, 
            11, new DbsArrayController(1, 125), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_CONTRACT_ID");
        registerRecord(vw_ads_Ia_Rslt2);

        pnd_Rec_Type = localVariables.newFieldInRecord("pnd_Rec_Type", "#REC-TYPE", FieldType.NUMERIC, 3);

        pnd_Rec_Type__R_Field_1 = localVariables.newGroupInRecord("pnd_Rec_Type__R_Field_1", "REDEFINE", pnd_Rec_Type);
        pnd_Rec_Type_Pnd_Filler = pnd_Rec_Type__R_Field_1.newFieldInGroup("pnd_Rec_Type_Pnd_Filler", "#FILLER", FieldType.NUMERIC, 2);
        pnd_Rec_Type_Pnd_Z_Indx = pnd_Rec_Type__R_Field_1.newFieldInGroup("pnd_Rec_Type_Pnd_Z_Indx", "#Z-INDX", FieldType.NUMERIC, 1);
        pnd_Nap_Pymnt_Mode = localVariables.newFieldInRecord("pnd_Nap_Pymnt_Mode", "#NAP-PYMNT-MODE", FieldType.NUMERIC, 3);

        pnd_Pas_Ext_Cntrl_02_Work_Area = localVariables.newGroupInRecord("pnd_Pas_Ext_Cntrl_02_Work_Area", "#PAS-EXT-CNTRL-02-WORK-AREA");

        pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Table = pnd_Pas_Ext_Cntrl_02_Work_Area.newGroupArrayInGroup("pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Table", 
            "#PEC-ACCT-TABLE", new DbsArrayController(1, 20));
        pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1 = pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Table.newFieldInGroup("pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1", 
            "#PEC-ACCT-NAME-1", FieldType.STRING, 1);

        vw_ads_Cntl_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntl_View", "ADS-CNTL-VIEW"), "ADS_CNTL", "ADS_CNTL");

        ads_Cntl_View_Ads_Cntl_Grp = vw_ads_Cntl_View.getRecord().newGroupInGroup("ADS_CNTL_VIEW_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde", "ADS-CNTL-RCRD-TYP-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADS_CNTL_RCRD_TYP_CDE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte", "ADS-CNTL-BSNSS-RCPRCL-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_RCPRCL_DTE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_DTE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte", "ADS-CNTL-BSNSS-TMRRW-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_TMRRW_DTE");
        ads_Cntl_View_Ads_Daily_Actvty_Dte = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Daily_Actvty_Dte", "ADS-DAILY-ACTVTY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADS_DAILY_ACTVTY_DTE");
        ads_Cntl_View_Ads_Rpt_13 = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Rpt_13", "ADS-RPT-13", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADS_RPT_13");
        registerRecord(vw_ads_Cntl_View);

        vw_iaa_Cntrl_Rcrd_1_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1_View", "IAA-CNTRL-RCRD-1-VIEW"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1_View);

        pnd_All_Indx = localVariables.newGroupInRecord("pnd_All_Indx", "#ALL-INDX");
        pnd_All_Indx_Pnd_A = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_B = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_B", "#B", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_C_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_C_Indx", "#C-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Next_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Next_Indx", "#NEXT-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Prod_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Prod_Indx", "#PROD-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Total_Number_Rec = localVariables.newFieldInRecord("pnd_Total_Number_Rec", "#TOTAL-NUMBER-REC", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Number_Rec_Roth = localVariables.newFieldInRecord("pnd_Total_Number_Rec_Roth", "#TOTAL-NUMBER-REC-ROTH", FieldType.PACKED_DECIMAL, 7);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Prod_Code = localVariables.newFieldInRecord("pnd_Tiaa_Prod_Code", "#TIAA-PROD-CODE", FieldType.BOOLEAN, 1);
        pnd_Cref_Prod_Found = localVariables.newFieldInRecord("pnd_Cref_Prod_Found", "#CREF-PROD-FOUND", FieldType.BOOLEAN, 1);
        pnd_Roth_Only_Run = localVariables.newFieldInRecord("pnd_Roth_Only_Run", "#ROTH-ONLY-RUN", FieldType.BOOLEAN, 1);
        pnd_Prev_Bsnss_Dte_D = localVariables.newFieldInRecord("pnd_Prev_Bsnss_Dte_D", "#PREV-BSNSS-DTE-D", FieldType.DATE);
        pnd_Prev_Bsnss_Dte_N = localVariables.newFieldInRecord("pnd_Prev_Bsnss_Dte_N", "#PREV-BSNSS-DTE-N", FieldType.NUMERIC, 8);

        pnd_Prev_Bsnss_Dte_N__R_Field_2 = localVariables.newGroupInRecord("pnd_Prev_Bsnss_Dte_N__R_Field_2", "REDEFINE", pnd_Prev_Bsnss_Dte_N);
        pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A = pnd_Prev_Bsnss_Dte_N__R_Field_2.newFieldInGroup("pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A", "#PREV-BSNSS-DTE-A", 
            FieldType.STRING, 8);
        pnd_Payment_Due_Date_Yyyymmdd = localVariables.newFieldInRecord("pnd_Payment_Due_Date_Yyyymmdd", "#PAYMENT-DUE-DATE-YYYYMMDD", FieldType.STRING, 
            8);

        pnd_Payment_Due_Date_Yyyymmdd__R_Field_3 = localVariables.newGroupInRecord("pnd_Payment_Due_Date_Yyyymmdd__R_Field_3", "REDEFINE", pnd_Payment_Due_Date_Yyyymmdd);
        pnd_Payment_Due_Date_Yyyymmdd_Pnd_Payment_Due_Date_Dte_Ccyy = pnd_Payment_Due_Date_Yyyymmdd__R_Field_3.newFieldInGroup("pnd_Payment_Due_Date_Yyyymmdd_Pnd_Payment_Due_Date_Dte_Ccyy", 
            "#PAYMENT-DUE-DATE-DTE-CCYY", FieldType.NUMERIC, 4);
        pnd_Payment_Due_Date_Yyyymmdd_Pnd_Payment_Due_Date_Dte_Mmdd = pnd_Payment_Due_Date_Yyyymmdd__R_Field_3.newFieldInGroup("pnd_Payment_Due_Date_Yyyymmdd_Pnd_Payment_Due_Date_Dte_Mmdd", 
            "#PAYMENT-DUE-DATE-DTE-MMDD", FieldType.NUMERIC, 4);
        pnd_Today_Business_Date = localVariables.newFieldInRecord("pnd_Today_Business_Date", "#TODAY-BUSINESS-DATE", FieldType.STRING, 8);

        pnd_Today_Business_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Today_Business_Date__R_Field_4", "REDEFINE", pnd_Today_Business_Date);
        pnd_Today_Business_Date_Pnd_Today_Business_Date_Cc = pnd_Today_Business_Date__R_Field_4.newFieldInGroup("pnd_Today_Business_Date_Pnd_Today_Business_Date_Cc", 
            "#TODAY-BUSINESS-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Today_Business_Date_Pnd_Today_Business_Date_Yy = pnd_Today_Business_Date__R_Field_4.newFieldInGroup("pnd_Today_Business_Date_Pnd_Today_Business_Date_Yy", 
            "#TODAY-BUSINESS-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Today_Business_Date_Pnd_Today_Business_Date_Mm = pnd_Today_Business_Date__R_Field_4.newFieldInGroup("pnd_Today_Business_Date_Pnd_Today_Business_Date_Mm", 
            "#TODAY-BUSINESS-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Today_Business_Date_Pnd_Today_Business_Date_Dd = pnd_Today_Business_Date__R_Field_4.newFieldInGroup("pnd_Today_Business_Date_Pnd_Today_Business_Date_Dd", 
            "#TODAY-BUSINESS-DATE-DD", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd = localVariables.newFieldInRecord("pnd_Prev_Accounting_Dte_Yyyymmdd", "#PREV-ACCOUNTING-DTE-YYYYMMDD", FieldType.STRING, 
            8);

        pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_5 = localVariables.newGroupInRecord("pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_5", "REDEFINE", pnd_Prev_Accounting_Dte_Yyyymmdd);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_5.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc", 
            "#PREV-ACCOUNT-DTE-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_5.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy", 
            "#PREV-ACCOUNT-DTE-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_5.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm", 
            "#PREV-ACCOUNT-DTE-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_5.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd", 
            "#PREV-ACCOUNT-DTE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Curr_Accounting_Dte_Yyyymmdd = localVariables.newFieldInRecord("pnd_Curr_Accounting_Dte_Yyyymmdd", "#CURR-ACCOUNTING-DTE-YYYYMMDD", FieldType.STRING, 
            8);

        pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_6 = localVariables.newGroupInRecord("pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_6", "REDEFINE", pnd_Curr_Accounting_Dte_Yyyymmdd);
        pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Cc = pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_6.newFieldInGroup("pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Cc", 
            "#CURR-ACCOUNT-DTE-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Yy = pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_6.newFieldInGroup("pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Yy", 
            "#CURR-ACCOUNT-DTE-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Mm = pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_6.newFieldInGroup("pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Mm", 
            "#CURR-ACCOUNT-DTE-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Dd = pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_6.newFieldInGroup("pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Dd", 
            "#CURR-ACCOUNT-DTE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Check_Cycle_Date_Yyyymmdd = localVariables.newFieldInRecord("pnd_Check_Cycle_Date_Yyyymmdd", "#CHECK-CYCLE-DATE-YYYYMMDD", FieldType.STRING, 
            8);

        pnd_Check_Cycle_Date_Yyyymmdd__R_Field_7 = localVariables.newGroupInRecord("pnd_Check_Cycle_Date_Yyyymmdd__R_Field_7", "REDEFINE", pnd_Check_Cycle_Date_Yyyymmdd);
        pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Dte_Cc = pnd_Check_Cycle_Date_Yyyymmdd__R_Field_7.newFieldInGroup("pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Dte_Cc", 
            "#CHECK-CYCLE-DATE-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Dte_Yy = pnd_Check_Cycle_Date_Yyyymmdd__R_Field_7.newFieldInGroup("pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Dte_Yy", 
            "#CHECK-CYCLE-DATE-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Dte_Mm = pnd_Check_Cycle_Date_Yyyymmdd__R_Field_7.newFieldInGroup("pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Dte_Mm", 
            "#CHECK-CYCLE-DATE-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Dte_Dd = pnd_Check_Cycle_Date_Yyyymmdd__R_Field_7.newFieldInGroup("pnd_Check_Cycle_Date_Yyyymmdd_Pnd_Check_Cycle_Date_Dte_Dd", 
            "#CHECK-CYCLE-DATE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Rqst_Id = localVariables.newFieldInRecord("pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 35);

        pnd_Rqst_Id__R_Field_8 = localVariables.newGroupInRecord("pnd_Rqst_Id__R_Field_8", "REDEFINE", pnd_Rqst_Id);
        pnd_Rqst_Id_Pnd_Nap_Unique_Id = pnd_Rqst_Id__R_Field_8.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Unique_Id", "#NAP-UNIQUE-ID", FieldType.NUMERIC, 12);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte = pnd_Rqst_Id__R_Field_8.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte", "#NAP-EFFCTV-DTE", FieldType.NUMERIC, 
            8);

        pnd_Rqst_Id__R_Field_9 = pnd_Rqst_Id__R_Field_8.newGroupInGroup("pnd_Rqst_Id__R_Field_9", "REDEFINE", pnd_Rqst_Id_Pnd_Nap_Effctv_Dte);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Cc = pnd_Rqst_Id__R_Field_9.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Cc", "#NAP-EFFCTV-DTE-CC", FieldType.NUMERIC, 
            2);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Yy = pnd_Rqst_Id__R_Field_9.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Yy", "#NAP-EFFCTV-DTE-YY", FieldType.NUMERIC, 
            2);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Mm = pnd_Rqst_Id__R_Field_9.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Mm", "#NAP-EFFCTV-DTE-MM", FieldType.NUMERIC, 
            2);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Dd = pnd_Rqst_Id__R_Field_9.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Dd", "#NAP-EFFCTV-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_Rqst_Id_Pnd_Nap_Entry_Dte = pnd_Rqst_Id__R_Field_8.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Entry_Dte", "#NAP-ENTRY-DTE", FieldType.NUMERIC, 8);
        pnd_Rqst_Id_Pnd_Nap_Entry_Tme = pnd_Rqst_Id__R_Field_8.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Entry_Tme", "#NAP-ENTRY-TME", FieldType.NUMERIC, 7);
        pnd_Payment_Mode = localVariables.newFieldInRecord("pnd_Payment_Mode", "#PAYMENT-MODE", FieldType.NUMERIC, 3);

        pnd_Payment_Mode__R_Field_10 = localVariables.newGroupInRecord("pnd_Payment_Mode__R_Field_10", "REDEFINE", pnd_Payment_Mode);
        pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1 = pnd_Payment_Mode__R_Field_10.newFieldInGroup("pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1", "#PAYMENT-MODE-BYTE-1", 
            FieldType.NUMERIC, 1);
        pnd_Payment_Mode_Pnd_Payment_Mode_Byte_2 = pnd_Payment_Mode__R_Field_10.newFieldInGroup("pnd_Payment_Mode_Pnd_Payment_Mode_Byte_2", "#PAYMENT-MODE-BYTE-2", 
            FieldType.NUMERIC, 2);
        pnd_Pec_Nbr_Active_Acct = localVariables.newFieldInRecord("pnd_Pec_Nbr_Active_Acct", "#PEC-NBR-ACTIVE-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Heading = localVariables.newFieldInRecord("pnd_Heading", "#HEADING", FieldType.STRING, 50);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_11 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_11", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_11.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        pnd_Ads_Ia_Super1 = localVariables.newFieldInRecord("pnd_Ads_Ia_Super1", "#ADS-IA-SUPER1", FieldType.STRING, 41);

        pnd_Ads_Ia_Super1__R_Field_12 = localVariables.newGroupInRecord("pnd_Ads_Ia_Super1__R_Field_12", "REDEFINE", pnd_Ads_Ia_Super1);
        pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rqst_Id = pnd_Ads_Ia_Super1__R_Field_12.newFieldInGroup("pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rqst_Id", "#ADS-IA-RQST-ID", 
            FieldType.STRING, 35);
        pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde = pnd_Ads_Ia_Super1__R_Field_12.newFieldInGroup("pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde", "#ADS-IA-RCRD-CDE", 
            FieldType.STRING, 3);
        pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr = pnd_Ads_Ia_Super1__R_Field_12.newFieldInGroup("pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr", "#ADS-IA-RCRD-SQNCE-NBR", 
            FieldType.NUMERIC, 3);
        pnd_Max_Rslt = localVariables.newFieldInRecord("pnd_Max_Rslt", "#MAX-RSLT", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Rates = localVariables.newFieldInRecord("pnd_Max_Rates", "#MAX-RATES", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Ia_Rslt2.reset();
        vw_ads_Cntl_View.reset();
        vw_iaa_Cntrl_Rcrd_1_View.reset();

        ldaAdsl1330.initializeValues();
        ldaAdsl450.initializeValues();
        ldaAdsl450a.initializeValues();

        localVariables.reset();
        pnd_All_Indx_Pnd_A.setInitialValue(0);
        pnd_All_Indx_Pnd_B.setInitialValue(0);
        pnd_All_Indx_Pnd_C_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Next_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Prod_Indx.setInitialValue(0);
        pnd_Total_Number_Rec.setInitialValue(0);
        pnd_Total_Number_Rec_Roth.setInitialValue(0);
        pnd_First_Time.setInitialValue(true);
        pnd_Tiaa_Prod_Code.setInitialValue(true);
        pnd_Cref_Prod_Found.setInitialValue(true);
        pnd_Roth_Only_Run.setInitialValue(false);
        pnd_Pec_Nbr_Active_Acct.setInitialValue(20);
        pnd_Max_Rslt.setInitialValue(125);
        pnd_Max_Rates.setInitialValue(250);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp1331() throws Exception
    {
        super("Adsp1331");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  OS-102013 START                                                                                                                                              //Natural: FORMAT LS = 79 PS = 60;//Natural: FORMAT ( 1 ) LS = 79 PS = 60;//Natural: FORMAT ( 2 ) LS = 79 PS = 60
        //* *CALLNAT 'ADSN888'     /* NEW EXTERNALIZATION + SEQUENCING OF PROD CDE
        //* *  #PAS-EXT-CNTRL-02-WORK-AREA
        //* *  #PEC-NBR-ACTIVE-ACCT
        DbsUtil.callnat(Adsn889.class , getCurrentProcessState(), pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue("*"));                                      //Natural: CALLNAT 'ADSN889' #PEC-ACCT-NAME-1 ( * )
        if (condition(Global.isEscape())) return;
        //*  OS-102013 END
                                                                                                                                                                          //Natural: PERFORM READ-CONTROL-RECORD
        sub_Read_Control_Record();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM READ-IAA-CONTROL-RECORD
        sub_Read_Iaa_Control_Record();
        if (condition(Global.isEscape())) {return;}
        PND_PND_L1910:                                                                                                                                                    //Natural: READ WORK FILE 1 ADS-IA-RSLT-VIEW #MORE
        while (condition(getWorkFiles().read(1, ldaAdsl450.getVw_ads_Ia_Rslt_View(), ldaAdsl450.getPnd_More())))
        {
            getReports().write(0, ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde(),ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr(),ldaAdsl450.getAds_Ia_Rslt_View_Rqst_Id(), //Natural: WRITE ADS-IA-RSLT-VIEW.ADI-IA-RCRD-CDE ADS-IA-RSLT-VIEW.ADI-SQNCE-NBR ADS-IA-RSLT-VIEW.RQST-ID ADS-IA-RSLT-VIEW.ADI-ANNTY-STRT-DTE ADS-IA-RSLT-VIEW.ADI-LST-ACTVTY-DTE ADS-IA-RSLT-VIEW.ADI-PYMNT-MODE ADS-IA-RSLT-VIEW.ADI-OPTN-CDE ADS-IA-RSLT-VIEW.ADI-SRVVR-IND ADS-IA-RSLT-VIEW.ADI-ROTH-RQST-IND
                ldaAdsl450.getAds_Ia_Rslt_View_Adi_Annty_Strt_Dte(),ldaAdsl450.getAds_Ia_Rslt_View_Adi_Lst_Actvty_Dte(),ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode(),
                ldaAdsl450.getAds_Ia_Rslt_View_Adi_Optn_Cde(),ldaAdsl450.getAds_Ia_Rslt_View_Adi_Srvvr_Ind(),ldaAdsl450.getAds_Ia_Rslt_View_Adi_Roth_Rqst_Ind());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1910"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1910"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                                                                                                                                           //Natural: AT END OF DATA
            //*  DEA
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Srvvr_Ind().notEquals("Y")))                                                                                 //Natural: IF ADS-IA-RSLT-VIEW.ADI-SRVVR-IND NE 'Y'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  DEA
            }                                                                                                                                                             //Natural: END-IF
            //*  GG050608
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Roth_Rqst_Ind().equals("Y")))                                                                                //Natural: IF ADS-IA-RSLT-VIEW.ADI-ROTH-RQST-IND = 'Y'
            {
                pnd_Roth_Only_Run.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #ROTH-ONLY-RUN
            }                                                                                                                                                             //Natural: END-IF
            pnd_Curr_Accounting_Dte_Yyyymmdd.setValueEdited(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                          //Natural: MOVE EDITED ADS-IA-RSLT-VIEW.ADI-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #CURR-ACCOUNTING-DTE-YYYYMMDD
            pnd_Payment_Due_Date_Yyyymmdd.setValueEdited(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Annty_Strt_Dte(),new ReportEditMask("YYYYMMDD"));                             //Natural: MOVE EDITED ADS-IA-RSLT-VIEW.ADI-ANNTY-STRT-DTE ( EM = YYYYMMDD ) TO #PAYMENT-DUE-DATE-YYYYMMDD
            if (condition(pnd_Curr_Accounting_Dte_Yyyymmdd.greater(pnd_Today_Business_Date)))                                                                             //Natural: IF #CURR-ACCOUNTING-DTE-YYYYMMDD GT #TODAY-BUSINESS-DATE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                                 //Natural: IF ADS-RPT-13 NE 0
            {
                if (condition(!(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Lst_Actvty_Dte().equals(ads_Cntl_View_Ads_Rpt_13) && ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde().equals("PP ")  //Natural: ACCEPT IF ADS-IA-RSLT-VIEW.ADI-LST-ACTVTY-DTE EQ ADS-RPT-13 AND ADS-IA-RSLT-VIEW.ADI-IA-RCRD-CDE = 'PP ' AND ADS-IA-RSLT-VIEW.ADI-SQNCE-NBR = 999
                    && ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr().equals(999))))
                {
                    continue;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(!(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Lst_Actvty_Dte().greater(ads_Cntl_View_Ads_Daily_Actvty_Dte) && ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde().equals("PP ")  //Natural: ACCEPT IF ADS-IA-RSLT-VIEW.ADI-LST-ACTVTY-DTE GT ADS-DAILY-ACTVTY-DTE AND ADS-IA-RSLT-VIEW.ADI-IA-RCRD-CDE = 'PP ' AND ADS-IA-RSLT-VIEW.ADI-SQNCE-NBR = 999
                    && ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr().equals(999))))
                {
                    continue;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  EM - 030512
                                                                                                                                                                          //Natural: PERFORM POPULATE-ADS-IA-RSLT-AREA
            sub_Populate_Ads_Ia_Rslt_Area();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1910"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1910"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_All_Indx_Pnd_A.reset();                                                                                                                                   //Natural: RESET #A
            PND_PND_L2350:                                                                                                                                                //Natural: FOR #A 1 TO 1
            for (pnd_All_Indx_Pnd_A.setValue(1); condition(pnd_All_Indx_Pnd_A.lessOrEqual(1)); pnd_All_Indx_Pnd_A.nadd(1))
            {
                if (condition(pnd_All_Indx_Pnd_A.greater(1)))                                                                                                             //Natural: IF #A > 1
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals(" ")))                                      //Natural: IF #ADI-DTL-TIAA-IA-RATE-CD ( #A ) NE ' '
                {
                    //*  GG050608
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Roth_Rqst_Ind().equals("Y")))                                                                            //Natural: IF ADS-IA-RSLT.ADI-ROTH-RQST-IND = 'Y'
                    {
                        pnd_Total_Number_Rec_Roth.nadd(1);                                                                                                                //Natural: ADD 1 TO #TOTAL-NUMBER-REC-ROTH
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Total_Number_Rec.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-NUMBER-REC
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-ACCUMULATIONS
                    sub_Calculate_Tiaa_Accumulations();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L2350"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L2350"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (true) break PND_PND_L2350;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L2350. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1910"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1910"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR01:                                                                                                                                                        //Natural: FOR #A 1 TO #PEC-NBR-ACTIVE-ACCT
            for (pnd_All_Indx_Pnd_A.setValue(1); condition(pnd_All_Indx_Pnd_A.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_A.nadd(1))
            {
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals(" ")))                                             //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE ' '
                {
                    //*  GG050608
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Roth_Rqst_Ind().equals("Y")))                                                                            //Natural: IF ADS-IA-RSLT.ADI-ROTH-RQST-IND = 'Y'
                    {
                        pnd_Total_Number_Rec_Roth.nadd(1);                                                                                                                //Natural: ADD 1 TO #TOTAL-NUMBER-REC-ROTH
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Total_Number_Rec.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-NUMBER-REC
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-ACCUMULATIONS
                    sub_Calculate_Cref_Accumulations();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  GG050608
                //*  DEA
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1910"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1910"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 1 )
        }                                                                                                                                                                 //Natural: AT TOP OF PAGE ( 2 );//Natural: END-WORK
        PND_PND_L1910_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom, "pnd_pnd_L1910");                                                                             //Natural: ESCAPE BOTTOM ( ##L1910. )
            if (true) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM BREAK-OF-ANNTY-START-DATE-RTN
        sub_Break_Of_Annty_Start_Date_Rtn();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB-RTN
        sub_End_Of_Job_Rtn();
        if (condition(Global.isEscape())) {return;}
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CONTROL-RECORD
        //* *******************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IAA-CONTROL-RECORD
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-ADS-IA-RSLT-AREA
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TIAA-ACCUMULATIONS
        //* **********************************************************************
        //*  M001
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TIAA-STD-ACCUMULATIONS
        //*  M001
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TIAA-IPRO-ACCUMULATIONS
        //*  M001
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TIAA-TPA-ACCUMULATIONS
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-CREF-ACCUMULATIONS
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-CREF-MONTHLY-ACCUMULATIONS
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-CREF-ANNUAL-ACCUMULATIONS
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BREAK-OF-ANNTY-START-DATE-RTN
        //*  M001
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BREAK-OF-SETTLEMENT-TYPE
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-JOB-RTN
    }
    private void sub_Read_Control_Record() throws Exception                                                                                                               //Natural: READ-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        vw_ads_Cntl_View.startDatabaseRead                                                                                                                                //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "PND_PND_L2810",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        PND_PND_L2810:
        while (condition(vw_ads_Cntl_View.readNextRow("PND_PND_L2810")))
        {
            //*    MOVE 20011031 TO ADS-CNTL-BSNSS-DTE
            //*    MOVE 20011101 TO ADS-CNTL-BSNSS-TMRRW-DTE
            if (condition(vw_ads_Cntl_View.getAstCOUNTER().equals(2)))                                                                                                    //Natural: IF *COUNTER ( ##L2810. ) = 2
            {
                if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                             //Natural: IF ADS-RPT-13 NE 0
                {
                    pnd_Date_A.setValueEdited(ads_Cntl_View_Ads_Rpt_13,new ReportEditMask("YYYYMMDD"));                                                                   //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE-A
                    pnd_Prev_Bsnss_Dte_N.setValue(pnd_Date_A_Pnd_Date_N);                                                                                                 //Natural: MOVE #DATE-N TO #PREV-BSNSS-DTE-N
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Prev_Bsnss_Dte_N.setValue(ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                                      //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #PREV-BSNSS-DTE-N
                }                                                                                                                                                         //Natural: END-IF
                pnd_Prev_Bsnss_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A);                                            //Natural: MOVE EDITED #PREV-BSNSS-DTE-A TO #PREV-BSNSS-DTE-D ( EM = YYYYMMDD )
                pnd_Today_Business_Date.setValueEdited(pnd_Prev_Bsnss_Dte_D,new ReportEditMask("YYYYMMDD"));                                                              //Natural: MOVE EDITED #PREV-BSNSS-DTE-D ( EM = YYYYMMDD ) TO #TODAY-BUSINESS-DATE
                if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                             //Natural: IF ADS-RPT-13 NE 0
                {
                    pnd_Prev_Accounting_Dte_Yyyymmdd.setValue(pnd_Date_A_Pnd_Date_N);                                                                                     //Natural: MOVE #DATE-N TO #PREV-ACCOUNTING-DTE-YYYYMMDD
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Prev_Accounting_Dte_Yyyymmdd.setValue(ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                          //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #PREV-ACCOUNTING-DTE-YYYYMMDD
                }                                                                                                                                                         //Natural: END-IF
                //* *  MOVE ADS-CNTL-BSNSS-TMRRW-DTE TO #PREV-ACCOUNTING-DTE-YYYYMMDD
                ldaAdsl1330.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc);                  //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-CC TO #ACCOUNTING-DATE-MCDY-CC
                ldaAdsl1330.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy);                  //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-YY TO #ACCOUNTING-DATE-MCDY-YY
                ldaAdsl1330.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm);                  //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-MM TO #ACCOUNTING-DATE-MCDY-MM
                ldaAdsl1330.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd);                  //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-DD TO #ACCOUNTING-DATE-MCDY-DD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_Iaa_Control_Record() throws Exception                                                                                                           //Natural: READ-IAA-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        vw_iaa_Cntrl_Rcrd_1_View.startDatabaseRead                                                                                                                        //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1-VIEW BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "READ01",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        READ01:
        while (condition(vw_iaa_Cntrl_Rcrd_1_View.readNextRow("READ01")))
        {
            pnd_Check_Cycle_Date_Yyyymmdd.setValueEdited(iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                           //Natural: MOVE EDITED CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-CYCLE-DATE-YYYYMMDD
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    //*  EM - 030512 START
    private void sub_Populate_Ads_Ia_Rslt_Area() throws Exception                                                                                                         //Natural: POPULATE-ADS-IA-RSLT-AREA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaAdsl450a.getAds_Ia_Rslt().setValuesByName(ldaAdsl450.getVw_ads_Ia_Rslt_View());                                                                                //Natural: MOVE BY NAME ADS-IA-RSLT-VIEW TO ADS-IA-RSLT
        ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Data().reset();                                                                                                           //Natural: RESET ADS-IA-RSLT.ADI-DTL-TIAA-DATA ADS-IA-RSLT.#ADI-DTL-TIA-TPA-GUAR-COMMUT-DAT ADS-IA-RSLT.#ADI-CONTRACT-ID ( * )
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Tpa_Guar_Commut_Dat().reset();
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Contract_Id().getValue("*").reset();
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-RATE-CD ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-RATE-CD ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-STD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-STD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-GRD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-GRD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Acct_Cd().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Acct_Cd().getValue(1,     //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-ACCT-CD ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-ACCT-CD ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Eff_Rte_Intrst().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Eff_Rte_Intrst().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-EFF-RTE-INTRST ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-EFF-RTE-INTRST ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-GRD-GRNTD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-GRD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-GRD-PAY-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-FNL-GRD-PAY-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-GRD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-FNL-GRD-DVDND-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-STD-PAY-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-FNL-STD-PAY-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-STD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-FNL-STD-DVDND-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Std_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIA-GUAR-COMMUT-STD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GUAR-COMMUT-STD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Grd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIA-GUAR-COMMUT-GRD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GUAR-COMMUT-GRD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Contract_Id().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Contract_Id().getValue(1,               //Natural: ASSIGN ADS-IA-RSLT.#ADI-CONTRACT-ID ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-CONTRACT-ID ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        if (condition(ldaAdsl450.getPnd_More().equals("Y")))                                                                                                              //Natural: IF #MORE = 'Y'
        {
            pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rqst_Id.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Rqst_Id());                                                                      //Natural: ASSIGN #ADS-IA-RQST-ID := ADS-IA-RSLT-VIEW.RQST-ID
            pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde(),              //Natural: COMPRESS ADS-IA-RSLT-VIEW.ADI-IA-RCRD-CDE '1' INTO #ADS-IA-RCRD-CDE LEAVING NO SPACE
                "1"));
            pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr());                                                         //Natural: ASSIGN #ADS-IA-RCRD-SQNCE-NBR := ADS-IA-RSLT-VIEW.ADI-SQNCE-NBR
            vw_ads_Ia_Rslt2.startDatabaseFind                                                                                                                             //Natural: FIND ADS-IA-RSLT2 WITH ADI-SUPER1 = #ADS-IA-SUPER1
            (
            "FIND01",
            new Wc[] { new Wc("ADI_SUPER1", "=", pnd_Ads_Ia_Super1, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_ads_Ia_Rslt2.readNextRow("FIND01")))
            {
                vw_ads_Ia_Rslt2.setIfNotFoundControlFlag(false);
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-RATE-CD ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-DA-RATE-CD ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-STD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-DA-STD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-GRD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-DA-GRD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Acct_Cd().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-ACCT-CD ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-ACCT-CD ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-IA-RATE-CD ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Eff_Rte_Intrst().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-EFF-RTE-INTRST ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-EFF-RTE-INTRST ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-GRD-GRNTD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-GRD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-DVDND-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-GRD-PAY-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-FNL-GRD-PAY-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-GRD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-FNL-GRD-DVDND-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-STD-GRNTD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-STD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-STD-PAY-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-FNL-STD-PAY-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-STD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-FNL-STD-DVDND-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Std_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIA-GUAR-COMMUT-STD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-GUAR-COMMUT-STD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Grd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIA-GUAR-COMMUT-GRD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-GUAR-COMMUT-GRD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Contract_Id().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Contract_Id.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-CONTRACT-ID ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-CONTRACT-ID ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  EM - 030512 END
    }
    //*  111803
    private void sub_Calculate_Tiaa_Accumulations() throws Exception                                                                                                      //Natural: CALCULATE-TIAA-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #B 1 TO #MAX-RATES
        for (pnd_All_Indx_Pnd_B.setValue(1); condition(pnd_All_Indx_Pnd_B.lessOrEqual(pnd_Max_Rates)); pnd_All_Indx_Pnd_B.nadd(1))
        {
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B).equals(" ")))                                             //Natural: IF #ADI-DTL-TIAA-IA-RATE-CD ( #B ) = ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(pnd_All_Indx_Pnd_B).notEquals(getZero())))                                    //Natural: IF #ADI-DTL-TIAA-DA-STD-AMT ( #B ) NE 0
            {
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-IPRO-ACCUMULATIONS
                    sub_Calculate_Tiaa_Ipro_Accumulations();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                    {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-TPA-ACCUMULATIONS
                        sub_Calculate_Tiaa_Tpa_Accumulations();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-STD-ACCUMULATIONS
                        sub_Calculate_Tiaa_Std_Accumulations();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_All_Indx_Pnd_B).notEquals(getZero())))                                    //Natural: IF #ADI-DTL-TIAA-DA-GRD-AMT ( #B ) NE 0
            {
                ldaAdsl1330.getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Pay_Cnt().nadd(1);                                                                                   //Natural: ADD 1 TO #F-TIAA-GRD-PAY-CNT
                ldaAdsl1330.getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #F-TIAA-SUB-PAY-CNT
                ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt().nadd(1);                                                                                 //Natural: ADD 1 TO #F-TOTAL-PAY-CNT
                ldaAdsl1330.getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B ) TO #F-TIAA-GRD-GTD-AMT
                ldaAdsl1330.getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B ) TO #F-TIAA-SUB-GTD-AMT
                ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B ) TO #F-TOTAL-GTD-AMT
                ldaAdsl1330.getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B ) TO #F-TIAA-GRD-DIV-AMT
                ldaAdsl1330.getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B ) TO #F-TIAA-SUB-DIVID-AMT
                ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B ) TO #F-TOTAL-DIV-AMT
                ldaAdsl1330.getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B ) TO #F-TIAA-GRD-FIN-GTD-AMT
                ldaAdsl1330.getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B ) TO #F-TIAA-SUB-FIN-GTD-AMT
                ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B ) TO #F-TOTAL-FIN-GTD-AMT
                ldaAdsl1330.getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B ) TO #F-TIAA-GRD-FIN-DIV-AMT
                ldaAdsl1330.getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B ) TO #F-TIAA-SUB-FIN-DIV-AMT
                ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B ) TO #F-TOTAL-FIN-DIV-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Calculate_Tiaa_Std_Accumulations() throws Exception                                                                                                  //Natural: CALCULATE-TIAA-STD-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        ldaAdsl1330.getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Pay_Cnt().nadd(1);                                                                                           //Natural: ADD 1 TO #F-TIAA-STD-PAY-CNT
        ldaAdsl1330.getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                                             //Natural: ADD 1 TO #F-TIAA-SUB-PAY-CNT
        ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt().nadd(1);                                                                                         //Natural: ADD 1 TO #F-TOTAL-PAY-CNT
        ldaAdsl1330.getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B));    //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B ) TO #F-TIAA-STD-GTD-AMT
        ldaAdsl1330.getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B));      //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B ) TO #F-TIAA-SUB-GTD-AMT
        ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B));  //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B ) TO #F-TOTAL-GTD-AMT
        ldaAdsl1330.getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B));    //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B ) TO #F-TIAA-STD-DIV-AMT
        ldaAdsl1330.getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B));    //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B ) TO #F-TIAA-SUB-DIVID-AMT
        ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B));  //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B ) TO #F-TOTAL-DIV-AMT
        ldaAdsl1330.getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B));   //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B ) TO #F-TIAA-STD-FIN-GTD-AMT
        ldaAdsl1330.getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B));     //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B ) TO #F-TIAA-SUB-FIN-GTD-AMT
        ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B ) TO #F-TOTAL-FIN-GTD-AMT
        ldaAdsl1330.getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B ) TO #F-TIAA-STD-FIN-DIV-AMT
        ldaAdsl1330.getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B));   //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B ) TO #F-TIAA-SUB-FIN-DIV-AMT
        ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B ) TO #F-TOTAL-FIN-DIV-AMT
    }
    private void sub_Calculate_Tiaa_Ipro_Accumulations() throws Exception                                                                                                 //Natural: CALCULATE-TIAA-IPRO-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        ldaAdsl1330.getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Pay_Cnt().nadd(1);                                                                                               //Natural: ADD 1 TO #F-IPRO-PAY-CNT
        ldaAdsl1330.getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                                             //Natural: ADD 1 TO #F-TIAA-SUB-PAY-CNT
        ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt().nadd(1);                                                                                         //Natural: ADD 1 TO #F-TOTAL-PAY-CNT
        ldaAdsl1330.getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B));        //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B ) TO #F-IPRO-GTD-AMT
        ldaAdsl1330.getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B));      //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B ) TO #F-TIAA-SUB-GTD-AMT
        ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B));  //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B ) TO #F-TOTAL-GTD-AMT
        ldaAdsl1330.getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B));        //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B ) TO #F-IPRO-DIV-AMT
        ldaAdsl1330.getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B));    //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B ) TO #F-TIAA-SUB-DIVID-AMT
        ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B));  //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B ) TO #F-TOTAL-DIV-AMT
        ldaAdsl1330.getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Final_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B));     //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B ) TO #F-IPRO-FINAL-GTD-AMT
        ldaAdsl1330.getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B));     //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B ) TO #F-TIAA-SUB-FIN-GTD-AMT
        ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B ) TO #F-TOTAL-FIN-GTD-AMT
    }
    private void sub_Calculate_Tiaa_Tpa_Accumulations() throws Exception                                                                                                  //Natural: CALCULATE-TIAA-TPA-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        ldaAdsl1330.getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Pay_Cnt().nadd(1);                                                                                                  //Natural: ADD 1 TO #F-TPA-PAY-CNT
        ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt().nadd(1);                                                                                         //Natural: ADD 1 TO #F-TOTAL-PAY-CNT
        ldaAdsl1330.getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B));           //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B ) TO #F-TPA-GTD-AMT
        ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B));  //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B ) TO #F-TOTAL-GTD-AMT
        ldaAdsl1330.getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B));           //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B ) TO #F-TPA-DIV-AMT
        ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B));  //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B ) TO #F-TOTAL-DIV-AMT
    }
    private void sub_Calculate_Cref_Accumulations() throws Exception                                                                                                      //Natural: CALCULATE-CREF-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Cref_Prod_Found.setValue(false);                                                                                                                              //Natural: MOVE FALSE TO #CREF-PROD-FOUND
        FOR03:                                                                                                                                                            //Natural: FOR #B 1 TO #PEC-NBR-ACTIVE-ACCT
        for (pnd_All_Indx_Pnd_B.setValue(1); condition(pnd_All_Indx_Pnd_B.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_B.nadd(1))
        {
            if (condition(pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue(pnd_All_Indx_Pnd_B).equals(" ")))                                                   //Natural: IF #PEC-ACCT-NAME-1 ( #B ) = ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue(pnd_All_Indx_Pnd_B).equals(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A)))) //Natural: IF #PEC-ACCT-NAME-1 ( #B ) = ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A )
            {
                pnd_Cref_Prod_Found.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO #CREF-PROD-FOUND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cref_Prod_Found.setValue(false);                                                                                                                      //Natural: MOVE FALSE TO #CREF-PROD-FOUND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A).notEquals(getZero())))                                 //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A ) NE 0
                {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-MONTHLY-ACCUMULATIONS
                    sub_Calculate_Cref_Monthly_Accumulations();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A).notEquals(getZero())))                                   //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A ) NE 0
                {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-ANNUAL-ACCUMULATIONS
                    sub_Calculate_Cref_Annual_Accumulations();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Calculate_Cref_Monthly_Accumulations() throws Exception                                                                                              //Natural: CALCULATE-CREF-MONTHLY-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        //*  RS0
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'D'
        {
            ldaAdsl1330.getPnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Pay_Cnt().nadd(1);                                                                           //Natural: ADD 1 TO #F-CREF-MTH-SUB-PAY-CNT
            ldaAdsl1330.getPnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A ) TO #F-CREF-MTH-SUB-UNITS
            ldaAdsl1330.getPnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A ) TO #F-CREF-MTH-SUB-DOLLARS
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl1330.getPnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt().getValue(pnd_All_Indx_Pnd_B).nadd(1);                                                          //Natural: ADD 1 TO #F-CREF-MTH-PAY-CNT ( #B )
        ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt().nadd(1);                                                                                         //Natural: ADD 1 TO #F-TOTAL-PAY-CNT
        ldaAdsl1330.getPnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units().getValue(pnd_All_Indx_Pnd_B).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A ) TO #F-CREF-MTH-UNITS ( #B )
        ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A));     //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A ) TO #F-TOTAL-UNITS
        ldaAdsl1330.getPnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars().getValue(pnd_All_Indx_Pnd_B).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A ) TO #F-CREF-MTH-DOLLARS ( #B )
        ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A));      //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A ) TO #F-TOTAL-DOLLARS
    }
    private void sub_Calculate_Cref_Annual_Accumulations() throws Exception                                                                                               //Natural: CALCULATE-CREF-ANNUAL-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        //*  RS0
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'D'
        {
            ldaAdsl1330.getPnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Pay_Cnt().nadd(1);                                                                           //Natural: ADD 1 TO #F-CREF-ANN-SUB-PAY-CNT
            ldaAdsl1330.getPnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A ) TO #F-CREF-ANN-SUB-UNITS
            ldaAdsl1330.getPnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A ) TO #F-CREF-ANN-SUB-DOLLARS
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl1330.getPnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt().getValue(pnd_All_Indx_Pnd_B).nadd(1);                                                          //Natural: ADD 1 TO #F-CREF-ANN-PAY-CNT ( #B )
        ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt().nadd(1);                                                                                         //Natural: ADD 1 TO #F-TOTAL-PAY-CNT
        ldaAdsl1330.getPnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units().getValue(pnd_All_Indx_Pnd_B).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A ) TO #F-CREF-ANN-UNITS ( #B )
        ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A));       //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A ) TO #F-TOTAL-UNITS
        ldaAdsl1330.getPnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars().getValue(pnd_All_Indx_Pnd_B).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A ) TO #F-CREF-ANN-DOLLARS ( #B )
        ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A));        //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A ) TO #F-TOTAL-DOLLARS
    }
    private void sub_Break_Of_Annty_Start_Date_Rtn() throws Exception                                                                                                     //Natural: BREAK-OF-ANNTY-START-DATE-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  GG050608
        if (condition(! (pnd_Roth_Only_Run.getBoolean())))                                                                                                                //Natural: IF NOT #ROTH-ONLY-RUN
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm1332.class));                                                                          //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM1332'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm1332.class));                                                                          //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM1332'
        }                                                                                                                                                                 //Natural: END-IF
        //* **********************************************************************
        ldaAdsl1330.getPnd_F_Tiaa_Minor_Accum().reset();                                                                                                                  //Natural: RESET #F-TIAA-MINOR-ACCUM #F-TIAA-GRD-PAY-CNT #F-TIAA-SUB-ACCUM #F-CREF-MTH-MINOR-ACCUM ( * ) #F-CREF-ANN-MINOR-ACCUM ( * ) #F-CREF-MTH-SUB-TOTAL-ACCUM #F-CREF-ANN-SUB-TOTAL-ACCUM #F-TOTAL-TIAA-CREF-ACCUM
        ldaAdsl1330.getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Pay_Cnt().reset();
        ldaAdsl1330.getPnd_F_Tiaa_Sub_Accum().reset();
        ldaAdsl1330.getPnd_F_Cref_Mth_Minor_Accum().getValue("*").reset();
        ldaAdsl1330.getPnd_F_Cref_Ann_Minor_Accum().getValue("*").reset();
        ldaAdsl1330.getPnd_F_Cref_Mth_Sub_Total_Accum().reset();
        ldaAdsl1330.getPnd_F_Cref_Ann_Sub_Total_Accum().reset();
        ldaAdsl1330.getPnd_F_Total_Tiaa_Cref_Accum().reset();
    }
    private void sub_Break_Of_Settlement_Type() throws Exception                                                                                                          //Natural: BREAK-OF-SETTLEMENT-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  GG050608
        if (condition(! (pnd_Roth_Only_Run.getBoolean())))                                                                                                                //Natural: IF NOT #ROTH-ONLY-RUN
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm1332.class));                                                                          //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM1332'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm1332.class));                                                                          //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM1332'
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_End_Of_Job_Rtn() throws Exception                                                                                                                    //Natural: END-OF-JOB-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  GG050608
        if (condition(! (pnd_Roth_Only_Run.getBoolean())))                                                                                                                //Natural: IF NOT #ROTH-ONLY-RUN
        {
            if (condition(pnd_Total_Number_Rec.equals(getZero())))                                                                                                        //Natural: IF #TOTAL-NUMBER-REC = 0
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
                getReports().write(1, ReportOption.NOTITLE,"*******************************************************");                                                    //Natural: WRITE ( 1 ) '*******************************************************'
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,"NO DATA FOR MATURITIES TO TIAA INTERFACE              *");                                                    //Natural: WRITE ( 1 ) 'NO DATA FOR MATURITIES TO TIAA INTERFACE              *'
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,"PROCESSED TODAY                                       *");                                                    //Natural: WRITE ( 1 ) 'PROCESSED TODAY                                       *'
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,"*******************************************************");                                                    //Natural: WRITE ( 1 ) '*******************************************************'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  GG050608
            if (condition(pnd_Total_Number_Rec_Roth.equals(getZero())))                                                                                                   //Natural: IF #TOTAL-NUMBER-REC-ROTH = 0
            {
                //*  DEA
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 2 )
                if (condition(Global.isEscape())){return;}
                getReports().write(2, ReportOption.NOTITLE,"*******************************************************");                                                    //Natural: WRITE ( 2 ) '*******************************************************'
                if (Global.isEscape()) return;
                getReports().write(2, ReportOption.NOTITLE,"NO SURV DATA                                          *");                                                    //Natural: WRITE ( 2 ) 'NO SURV DATA                                          *'
                if (Global.isEscape()) return;
                getReports().write(2, ReportOption.NOTITLE,"PROCESSED TODAY                                       *");                                                    //Natural: WRITE ( 2 ) 'PROCESSED TODAY                                       *'
                if (Global.isEscape()) return;
                getReports().write(2, ReportOption.NOTITLE,"*******************************************************");                                                    //Natural: WRITE ( 2 ) '*******************************************************'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Heading.setValue("Annuitization of OmniPlus Survivor Contracts        ");                                                                         //Natural: ASSIGN #HEADING := 'Annuitization of OmniPlus Survivor Contracts        '
                    if (condition(! (pnd_Roth_Only_Run.getBoolean())))                                                                                                    //Natural: IF NOT #ROTH-ONLY-RUN
                    {
                        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm1332.class));                                                              //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM1332'
                    }                                                                                                                                                     //Natural: END-IF
                    //*  GG050608
                    //*  DEA
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Heading.setValue("Annuitization of OmniPlus Survivor 403b/401k Contracts");                                                                       //Natural: ASSIGN #HEADING := 'Annuitization of OmniPlus Survivor 403b/401k Contracts'
                    if (condition(pnd_Roth_Only_Run.getBoolean()))                                                                                                        //Natural: IF #ROTH-ONLY-RUN
                    {
                        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm1332.class));                                                              //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM1332'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=79 PS=60");
        Global.format(1, "LS=79 PS=60");
        Global.format(2, "LS=79 PS=60");
    }
}
