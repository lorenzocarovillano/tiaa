/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:04:49 PM
**        * FROM NATURAL PROGRAM : Adsp762
************************************************************
**        * FILE NAME            : Adsp762.java
**        * CLASS NAME           : Adsp762
**        * INSTANCE NAME        : Adsp762
************************************************************
************************************************************************
* PROGRAM  : ADSP762
* GENERATED: MARCH 15, 2010
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : THIS MODULE REPORTS REQUESTS FOR TIAA  WHICH WERE
*            PROCESSED BY ADAS
*
* REMARKS  : CLONED FROM ADSP760 (DEA)
*********************  MAINTENANCE LOG  ********************************
*  D A T E   PROGRAMMER  D E S C R I P T I O N
* 12/22/97   ZAFAR KHAN  MAP NAZM762 CORRECTED
* 03/19/98   ZAFAR KHAN  MAPS NAZM766 NAZM768 NAZM775 NAZM777 CHANGED TO
*                        SHOW -VE SIGNS. TIAA GRADED REPORT WAS ALSO
*                        FIXED (CHECK CHANGED FROM AMT TO CDE).
* 05/22/98   ZAFAR KHAN  ROLLOVER CERTIFICATE NUMBER AND TOTAL ROLLOVER
*                        AMOUNT ADDED IN PROGRAM AS WELL AS MAP NAZM761
* 01/27/99   ZAFAR KHAN  ADP-BPS-UNIT EXTENDED FROM 6 TO 8 BYTES
* 01/2001    C SCHNEIDER TPA/IPRO CHANGES
* 11/26/01   C SCHNEIDER RE-STOWED UNDER NAT 3.1.4
* 11/17/03   C SCHNEIDER RE-STOWED NAZLIARS  ALLOW FOR 80 TIAA RATES
* 04/12/04   E MELNIK    MODIFIED FOR ADAS - ANNUITIZATION SUNGUARD
* 05/19/04   C. AVE      ADDED ASTERISK BESIDE DA ACCUM TOTAL &
*                        DISCLAIMER MESSAGE
* 08222005 M.NACHBER   REL 6.5
*                        ADDED GUARANTEED COMMUTED VALUES TO THE REPORT
* 02/22/08   O SOTTO TIAA RATE EXPANSION TO 99 OCCURS. SC 022208.
* 03/17/2008 E.MELNIK   ROTH 403B/401K CHANGES.  RECOMPILED FOR
*                       NEW ADSL401/ADSL401A/ADSL450.
* 05/06/2008 G.GUERRERO ADDED THE CREATION A ROTH ONLY REPORT.
* 11/28/2008 R.SACHARNY ADDED (/STABLE) TO REPORT TITLE. (RS0)
* 03/15/2010 D.E.ANDER  COPY PROGRAM FOR ADAS REPLATFORM  MARKED DEA
* 03/06/12   E. MELNIK  RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                       UPDATED LINKAGE AREAS.  CHANGES MARKED BY
*                       EM - 030612.
* 01/18/13   E. MELNIK  TNG PROJECT.  RESTOW TO INCLUDE UPDATED LINKAGE
*                       AREAS.
* 10/22/13  E. MELNIK CREF/REA REDESIGN CHANGES.
*                     MARKED BY EM - 102213.
* 02/27/2017 R.CARREON PIN EXPANSION 02272017
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp762 extends BLNatBase
{
    // Data Areas
    private GdaAdsg870 gdaAdsg870;
    private LdaAdsl760 ldaAdsl760;
    private LdaAdsl761 ldaAdsl761;
    private LdaAdsl763 ldaAdsl763;
    private LdaAdsl450 ldaAdsl450;
    private LdaAdsl450a ldaAdsl450a;
    private LdaAdsl401a ldaAdsl401a;
    private LdaAdsl401 ldaAdsl401;
    private PdaAdspda_M pdaAdspda_M;
    private PdaNeca4000 pdaNeca4000;
    private PdaAdsa888 pdaAdsa888;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ads_Ia_Rslt2;
    private DbsField ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data;

    private DbsGroup ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt;

    private DbsGroup ads_Ia_Rslt2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt;
    private DbsGroup ads_Ia_Rslt2_Adi_Contract_IdMuGroup;
    private DbsField ads_Ia_Rslt2_Adi_Contract_Id;
    private DbsField pnd_Type_Of_Call;
    private DbsField pnd_Code;
    private DbsField pnd_Desc;
    private DbsField pnd_Rec_Type;

    private DbsGroup pnd_Rec_Type__R_Field_1;
    private DbsField pnd_Rec_Type_Pnd_Filler;
    private DbsField pnd_Rec_Type_Pnd_Z_Indx;
    private DbsField pnd_Nac_Ppg_Code;
    private DbsField pnd_Nac_Ppg_Ind;
    private DbsField pnd_Tiaa_Nbr;
    private DbsField pnd_Tiaa_Ind;
    private DbsField pnd_Tiaa_Stlmnt_Amt;
    private DbsField pnd_Cref_Nbr;
    private DbsField pnd_Cref_Ind;
    private DbsField pnd_Cref_Stlmnt_Amt;
    private DbsField pnd_Real_Nbr;
    private DbsField pnd_Real_Ind;
    private DbsField pnd_Real_Stlmnt_Amt;
    private DbsField pnd_Tiaa_Rlvr_A;

    private DbsGroup pnd_Tiaa_Rlvr_A__R_Field_2;
    private DbsField pnd_Tiaa_Rlvr_A_Pnd_Tiaa_Rlvr;
    private DbsField pnd_Cref_Rlvr_A;

    private DbsGroup pnd_Cref_Rlvr_A__R_Field_3;
    private DbsField pnd_Cref_Rlvr_A_Pnd_Cref_Rlvr;
    private DbsField pnd_Real_Rlvr_A;

    private DbsGroup pnd_Real_Rlvr_A__R_Field_4;
    private DbsField pnd_Real_Rlvr_A_Pnd_Real_Rlvr;
    private DbsField pnd_Payment_Mode;

    private DbsGroup pnd_Payment_Mode__R_Field_5;
    private DbsField pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1;
    private DbsField pnd_Payment_Mode_Pnd_Payment_Mode_Byte_2;
    private DbsField pnd_Payment_Mode_All;
    private DbsField pnd_Time;

    private DbsGroup pnd_Time__R_Field_6;
    private DbsField pnd_Time_Pnd_Time_Hh;
    private DbsField pnd_Time_Pnd_Time_Filler_1;
    private DbsField pnd_Time_Pnd_Time_Mm;
    private DbsField pnd_Time_Pnd_Time_Filler_2;
    private DbsField pnd_Time_Pnd_Time_Ss;

    private DbsGroup pnd_All_Indx;
    private DbsField pnd_All_Indx_Pnd_Z;
    private DbsField pnd_All_Indx_Pnd_A;
    private DbsField pnd_All_Indx_Pnd_A_Indx;
    private DbsField pnd_All_Indx_Pnd_B_Indx;
    private DbsField pnd_All_Indx_Pnd_C_Indx;
    private DbsField pnd_All_Indx_Pnd_X_Indx;
    private DbsField pnd_All_Indx_Pnd_Y_Indx;
    private DbsField pnd_All_Indx_Pnd_Next_Indx;
    private DbsField pnd_All_Indx_Pnd_Tiaa_Indx_1;
    private DbsField pnd_All_Indx_Pnd_Tiaa_Indx_2;
    private DbsField pnd_All_Indx_Pnd_Tiaa_Indx_3;
    private DbsField pnd_All_Indx_Pnd_Tiaa_Indx_4;
    private DbsField pnd_All_Indx_Pnd_Tiaa_Indx_5;
    private DbsField pnd_All_Indx_Pnd_Tiaa_Indx_6;
    private DbsField pnd_All_Indx_Pnd_Prod_Indx;
    private DbsField pnd_All_Indx_Pnd_I;
    private DbsField pnd_All_Indx_Pnd_Tot_Da;
    private DbsField pnd_Total_Tiaa_Nbr;
    private DbsField pnd_Total_Stable_Nbr;
    private DbsField pnd_New_Header_Page_1;
    private DbsField pnd_New_Header_Page_2;
    private DbsField pnd_New_Header_Page_3;
    private DbsField pnd_New_Header_Page_4;
    private DbsField pnd_New_Header_Page_5;
    private DbsField pnd_New_Header_Page_6;
    private DbsField pnd_First_Time;
    private DbsField pnd_Tiaa_New_Record;
    private DbsField pnd_Tiaa_Record;
    private DbsField pnd_Cref_Record;
    private DbsField pnd_Real_Record;
    private DbsField pnd_Tiaa_Prod_Code;
    private DbsField pnd_Tiaa_Rec_Found;
    private DbsField pnd_Record_Written;
    private DbsField pnd_Tiaa_Rec_Not_Found;
    private DbsField pnd_End_Of_Ia_File;
    private DbsField pnd_Roth_Indicator_On;
    private DbsField pnd_Tpa_Io_Lit_774;
    private DbsField pnd_Tiaa_Tpa_Io_665;
    private DbsField pnd_Heading;
    private DbsField pnd_Page_Number_Map;
    private DbsField pnd_Pec_Nbr_Active_Acct;
    private DbsField pnd_Asterisk;
    private DbsField pnd_Comment1;
    private DbsField pnd_Comment2;
    private DbsField pnd_Amount;
    private DbsField pnd_Ads_Ia_Super1;

    private DbsGroup pnd_Ads_Ia_Super1__R_Field_7;
    private DbsField pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rqst_Id;
    private DbsField pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde;
    private DbsField pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr;
    private DbsField pnd_Max_Rslt;
    private DbsField pnd_Max_Rates;
    private DbsField pnd_Work_Rqst;

    private DbsGroup pnd_Work_Rqst__R_Field_8;
    private DbsField pnd_Work_Rqst_Pnd_Work_Rqst_30;
    private DbsField pnd_Work_Rqst_Pnd_Work_Rqst_5;

    private DbsGroup pnd_Work_Rqst__R_Field_9;
    private DbsField pnd_Work_Rqst_Pnd_Work_Rqst_Pin7;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaAdsg870 = GdaAdsg870.getInstance(getCallnatLevel());
        registerRecord(gdaAdsg870);
        if (gdaOnly) return;

        ldaAdsl760 = new LdaAdsl760();
        registerRecord(ldaAdsl760);
        ldaAdsl761 = new LdaAdsl761();
        registerRecord(ldaAdsl761);
        ldaAdsl763 = new LdaAdsl763();
        registerRecord(ldaAdsl763);
        ldaAdsl450 = new LdaAdsl450();
        registerRecord(ldaAdsl450);
        registerRecord(ldaAdsl450.getVw_ads_Ia_Rslt_View());
        ldaAdsl450a = new LdaAdsl450a();
        registerRecord(ldaAdsl450a);
        ldaAdsl401a = new LdaAdsl401a();
        registerRecord(ldaAdsl401a);
        registerRecord(ldaAdsl401a.getVw_ads_Cntrct_View());
        ldaAdsl401 = new LdaAdsl401();
        registerRecord(ldaAdsl401);
        registerRecord(ldaAdsl401.getVw_ads_Prtcpnt_View());
        localVariables = new DbsRecord();
        pdaAdspda_M = new PdaAdspda_M(localVariables);
        pdaNeca4000 = new PdaNeca4000(localVariables);
        pdaAdsa888 = new PdaAdsa888(localVariables);

        // Local Variables

        vw_ads_Ia_Rslt2 = new DataAccessProgramView(new NameInfo("vw_ads_Ia_Rslt2", "ADS-IA-RSLT2"), "ADS_IA_RSLT", "ADS_IA_RSLT", DdmPeriodicGroups.getInstance().getGroups("ADS_IA_RSLT"));
        ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data = vw_ads_Ia_Rslt2.getRecord().newFieldInGroup("ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data", "C*ADI-DTL-TIAA-DATA", 
            RepeatingFieldStrategy.CAsteriskVariable, "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");

        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data = vw_ads_Ia_Rslt2.getRecord().newGroupInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data", "ADI-DTL-TIAA-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd", "ADI-DTL-TIAA-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt", "ADI-DTL-TIAA-DA-STD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt", "ADI-DTL-TIAA-DA-GRD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd", "ADI-DTL-TIAA-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_ACCT_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd", "ADI-DTL-TIAA-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_IA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst", "ADI-DTL-TIAA-EFF-RTE-INTRST", 
            FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_EFF_RTE_INTRST", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt", "ADI-DTL-TIAA-GRD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt", "ADI-DTL-TIAA-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt", "ADI-DTL-FNL-GRD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt", "ADI-DTL-FNL-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt", "ADI-DTL-TIAA-STD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt", "ADI-DTL-TIAA-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt", "ADI-DTL-FNL-STD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt", "ADI-DTL-FNL-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");

        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat = vw_ads_Ia_Rslt2.getRecord().newGroupInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat", "ADI-DTL-TIAA-TPA-GUAR-COMMUT-DAT", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt", 
            "ADI-DTL-TIAA-GUAR-COMMUT-STD-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_TIAA_GUAR_COMMUT_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt", 
            "ADI-DTL-TIAA-GUAR-COMMUT-GRD-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_TIAA_GUAR_COMMUT_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        ads_Ia_Rslt2_Adi_Contract_IdMuGroup = vw_ads_Ia_Rslt2.getRecord().newGroupInGroup("ADS_IA_RSLT2_ADI_CONTRACT_IDMuGroup", "ADI_CONTRACT_IDMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_IA_RSLT_ADI_CONTRACT_ID");
        ads_Ia_Rslt2_Adi_Contract_Id = ads_Ia_Rslt2_Adi_Contract_IdMuGroup.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Contract_Id", "ADI-CONTRACT-ID", FieldType.NUMERIC, 
            11, new DbsArrayController(1, 125), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_CONTRACT_ID");
        registerRecord(vw_ads_Ia_Rslt2);

        pnd_Type_Of_Call = localVariables.newFieldInRecord("pnd_Type_Of_Call", "#TYPE-OF-CALL", FieldType.STRING, 1);
        pnd_Code = localVariables.newFieldInRecord("pnd_Code", "#CODE", FieldType.STRING, 2);
        pnd_Desc = localVariables.newFieldInRecord("pnd_Desc", "#DESC", FieldType.STRING, 20);
        pnd_Rec_Type = localVariables.newFieldInRecord("pnd_Rec_Type", "#REC-TYPE", FieldType.NUMERIC, 3);

        pnd_Rec_Type__R_Field_1 = localVariables.newGroupInRecord("pnd_Rec_Type__R_Field_1", "REDEFINE", pnd_Rec_Type);
        pnd_Rec_Type_Pnd_Filler = pnd_Rec_Type__R_Field_1.newFieldInGroup("pnd_Rec_Type_Pnd_Filler", "#FILLER", FieldType.NUMERIC, 2);
        pnd_Rec_Type_Pnd_Z_Indx = pnd_Rec_Type__R_Field_1.newFieldInGroup("pnd_Rec_Type_Pnd_Z_Indx", "#Z-INDX", FieldType.NUMERIC, 1);
        pnd_Nac_Ppg_Code = localVariables.newFieldArrayInRecord("pnd_Nac_Ppg_Code", "#NAC-PPG-CODE", FieldType.STRING, 6, new DbsArrayController(1, 15));
        pnd_Nac_Ppg_Ind = localVariables.newFieldArrayInRecord("pnd_Nac_Ppg_Ind", "#NAC-PPG-IND", FieldType.STRING, 5, new DbsArrayController(1, 15));
        pnd_Tiaa_Nbr = localVariables.newFieldArrayInRecord("pnd_Tiaa_Nbr", "#TIAA-NBR", FieldType.STRING, 10, new DbsArrayController(1, 12));
        pnd_Tiaa_Ind = localVariables.newFieldArrayInRecord("pnd_Tiaa_Ind", "#TIAA-IND", FieldType.STRING, 6, new DbsArrayController(1, 12));
        pnd_Tiaa_Stlmnt_Amt = localVariables.newFieldInRecord("pnd_Tiaa_Stlmnt_Amt", "#TIAA-STLMNT-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Cref_Nbr = localVariables.newFieldArrayInRecord("pnd_Cref_Nbr", "#CREF-NBR", FieldType.STRING, 10, new DbsArrayController(1, 12));
        pnd_Cref_Ind = localVariables.newFieldArrayInRecord("pnd_Cref_Ind", "#CREF-IND", FieldType.STRING, 6, new DbsArrayController(1, 12));
        pnd_Cref_Stlmnt_Amt = localVariables.newFieldInRecord("pnd_Cref_Stlmnt_Amt", "#CREF-STLMNT-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Real_Nbr = localVariables.newFieldArrayInRecord("pnd_Real_Nbr", "#REAL-NBR", FieldType.STRING, 10, new DbsArrayController(1, 12));
        pnd_Real_Ind = localVariables.newFieldArrayInRecord("pnd_Real_Ind", "#REAL-IND", FieldType.STRING, 6, new DbsArrayController(1, 12));
        pnd_Real_Stlmnt_Amt = localVariables.newFieldInRecord("pnd_Real_Stlmnt_Amt", "#REAL-STLMNT-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Tiaa_Rlvr_A = localVariables.newFieldInRecord("pnd_Tiaa_Rlvr_A", "#TIAA-RLVR-A", FieldType.STRING, 11);

        pnd_Tiaa_Rlvr_A__R_Field_2 = localVariables.newGroupInRecord("pnd_Tiaa_Rlvr_A__R_Field_2", "REDEFINE", pnd_Tiaa_Rlvr_A);
        pnd_Tiaa_Rlvr_A_Pnd_Tiaa_Rlvr = pnd_Tiaa_Rlvr_A__R_Field_2.newFieldInGroup("pnd_Tiaa_Rlvr_A_Pnd_Tiaa_Rlvr", "#TIAA-RLVR", FieldType.NUMERIC, 11, 
            2);
        pnd_Cref_Rlvr_A = localVariables.newFieldInRecord("pnd_Cref_Rlvr_A", "#CREF-RLVR-A", FieldType.STRING, 11);

        pnd_Cref_Rlvr_A__R_Field_3 = localVariables.newGroupInRecord("pnd_Cref_Rlvr_A__R_Field_3", "REDEFINE", pnd_Cref_Rlvr_A);
        pnd_Cref_Rlvr_A_Pnd_Cref_Rlvr = pnd_Cref_Rlvr_A__R_Field_3.newFieldInGroup("pnd_Cref_Rlvr_A_Pnd_Cref_Rlvr", "#CREF-RLVR", FieldType.NUMERIC, 11, 
            2);
        pnd_Real_Rlvr_A = localVariables.newFieldInRecord("pnd_Real_Rlvr_A", "#REAL-RLVR-A", FieldType.STRING, 11);

        pnd_Real_Rlvr_A__R_Field_4 = localVariables.newGroupInRecord("pnd_Real_Rlvr_A__R_Field_4", "REDEFINE", pnd_Real_Rlvr_A);
        pnd_Real_Rlvr_A_Pnd_Real_Rlvr = pnd_Real_Rlvr_A__R_Field_4.newFieldInGroup("pnd_Real_Rlvr_A_Pnd_Real_Rlvr", "#REAL-RLVR", FieldType.NUMERIC, 11, 
            2);
        pnd_Payment_Mode = localVariables.newFieldInRecord("pnd_Payment_Mode", "#PAYMENT-MODE", FieldType.NUMERIC, 3);

        pnd_Payment_Mode__R_Field_5 = localVariables.newGroupInRecord("pnd_Payment_Mode__R_Field_5", "REDEFINE", pnd_Payment_Mode);
        pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1 = pnd_Payment_Mode__R_Field_5.newFieldInGroup("pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1", "#PAYMENT-MODE-BYTE-1", 
            FieldType.NUMERIC, 1);
        pnd_Payment_Mode_Pnd_Payment_Mode_Byte_2 = pnd_Payment_Mode__R_Field_5.newFieldInGroup("pnd_Payment_Mode_Pnd_Payment_Mode_Byte_2", "#PAYMENT-MODE-BYTE-2", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Mode_All = localVariables.newFieldInRecord("pnd_Payment_Mode_All", "#PAYMENT-MODE-ALL", FieldType.STRING, 20);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.NUMERIC, 8);

        pnd_Time__R_Field_6 = localVariables.newGroupInRecord("pnd_Time__R_Field_6", "REDEFINE", pnd_Time);
        pnd_Time_Pnd_Time_Hh = pnd_Time__R_Field_6.newFieldInGroup("pnd_Time_Pnd_Time_Hh", "#TIME-HH", FieldType.NUMERIC, 2);
        pnd_Time_Pnd_Time_Filler_1 = pnd_Time__R_Field_6.newFieldInGroup("pnd_Time_Pnd_Time_Filler_1", "#TIME-FILLER-1", FieldType.STRING, 1);
        pnd_Time_Pnd_Time_Mm = pnd_Time__R_Field_6.newFieldInGroup("pnd_Time_Pnd_Time_Mm", "#TIME-MM", FieldType.NUMERIC, 2);
        pnd_Time_Pnd_Time_Filler_2 = pnd_Time__R_Field_6.newFieldInGroup("pnd_Time_Pnd_Time_Filler_2", "#TIME-FILLER-2", FieldType.STRING, 1);
        pnd_Time_Pnd_Time_Ss = pnd_Time__R_Field_6.newFieldInGroup("pnd_Time_Pnd_Time_Ss", "#TIME-SS", FieldType.NUMERIC, 2);

        pnd_All_Indx = localVariables.newGroupInRecord("pnd_All_Indx", "#ALL-INDX");
        pnd_All_Indx_Pnd_Z = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Z", "#Z", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_A = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_A_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_A_Indx", "#A-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_B_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_B_Indx", "#B-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_C_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_C_Indx", "#C-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_X_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_X_Indx", "#X-INDX", FieldType.PACKED_DECIMAL, 2);
        pnd_All_Indx_Pnd_Y_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Y_Indx", "#Y-INDX", FieldType.PACKED_DECIMAL, 2);
        pnd_All_Indx_Pnd_Next_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Next_Indx", "#NEXT-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Tiaa_Indx_1 = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Tiaa_Indx_1", "#TIAA-INDX-1", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Tiaa_Indx_2 = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Tiaa_Indx_2", "#TIAA-INDX-2", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Tiaa_Indx_3 = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Tiaa_Indx_3", "#TIAA-INDX-3", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Tiaa_Indx_4 = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Tiaa_Indx_4", "#TIAA-INDX-4", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Tiaa_Indx_5 = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Tiaa_Indx_5", "#TIAA-INDX-5", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Tiaa_Indx_6 = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Tiaa_Indx_6", "#TIAA-INDX-6", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Prod_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Prod_Indx", "#PROD-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_I = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_All_Indx_Pnd_Tot_Da = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Tot_Da", "#TOT-DA", FieldType.PACKED_DECIMAL, 3);
        pnd_Total_Tiaa_Nbr = localVariables.newFieldInRecord("pnd_Total_Tiaa_Nbr", "#TOTAL-TIAA-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Stable_Nbr = localVariables.newFieldInRecord("pnd_Total_Stable_Nbr", "#TOTAL-STABLE-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_New_Header_Page_1 = localVariables.newFieldInRecord("pnd_New_Header_Page_1", "#NEW-HEADER-PAGE-1", FieldType.BOOLEAN, 1);
        pnd_New_Header_Page_2 = localVariables.newFieldInRecord("pnd_New_Header_Page_2", "#NEW-HEADER-PAGE-2", FieldType.BOOLEAN, 1);
        pnd_New_Header_Page_3 = localVariables.newFieldInRecord("pnd_New_Header_Page_3", "#NEW-HEADER-PAGE-3", FieldType.BOOLEAN, 1);
        pnd_New_Header_Page_4 = localVariables.newFieldInRecord("pnd_New_Header_Page_4", "#NEW-HEADER-PAGE-4", FieldType.BOOLEAN, 1);
        pnd_New_Header_Page_5 = localVariables.newFieldInRecord("pnd_New_Header_Page_5", "#NEW-HEADER-PAGE-5", FieldType.BOOLEAN, 1);
        pnd_New_Header_Page_6 = localVariables.newFieldInRecord("pnd_New_Header_Page_6", "#NEW-HEADER-PAGE-6", FieldType.BOOLEAN, 1);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Tiaa_New_Record = localVariables.newFieldInRecord("pnd_Tiaa_New_Record", "#TIAA-NEW-RECORD", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Record = localVariables.newFieldInRecord("pnd_Tiaa_Record", "#TIAA-RECORD", FieldType.BOOLEAN, 1);
        pnd_Cref_Record = localVariables.newFieldInRecord("pnd_Cref_Record", "#CREF-RECORD", FieldType.BOOLEAN, 1);
        pnd_Real_Record = localVariables.newFieldInRecord("pnd_Real_Record", "#REAL-RECORD", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Prod_Code = localVariables.newFieldInRecord("pnd_Tiaa_Prod_Code", "#TIAA-PROD-CODE", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Rec_Found = localVariables.newFieldInRecord("pnd_Tiaa_Rec_Found", "#TIAA-REC-FOUND", FieldType.BOOLEAN, 1);
        pnd_Record_Written = localVariables.newFieldInRecord("pnd_Record_Written", "#RECORD-WRITTEN", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Rec_Not_Found = localVariables.newFieldInRecord("pnd_Tiaa_Rec_Not_Found", "#TIAA-REC-NOT-FOUND", FieldType.BOOLEAN, 1);
        pnd_End_Of_Ia_File = localVariables.newFieldInRecord("pnd_End_Of_Ia_File", "#END-OF-IA-FILE", FieldType.BOOLEAN, 1);
        pnd_Roth_Indicator_On = localVariables.newFieldInRecord("pnd_Roth_Indicator_On", "#ROTH-INDICATOR-ON", FieldType.BOOLEAN, 1);
        pnd_Tpa_Io_Lit_774 = localVariables.newFieldInRecord("pnd_Tpa_Io_Lit_774", "#TPA-IO-LIT-774", FieldType.STRING, 4);
        pnd_Tiaa_Tpa_Io_665 = localVariables.newFieldInRecord("pnd_Tiaa_Tpa_Io_665", "#TIAA-TPA-IO-665", FieldType.STRING, 4);
        pnd_Heading = localVariables.newFieldInRecord("pnd_Heading", "#HEADING", FieldType.STRING, 37);
        pnd_Page_Number_Map = localVariables.newFieldInRecord("pnd_Page_Number_Map", "#PAGE-NUMBER-MAP", FieldType.NUMERIC, 8);
        pnd_Pec_Nbr_Active_Acct = localVariables.newFieldInRecord("pnd_Pec_Nbr_Active_Acct", "#PEC-NBR-ACTIVE-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Asterisk = localVariables.newFieldInRecord("pnd_Asterisk", "#ASTERISK", FieldType.STRING, 1);
        pnd_Comment1 = localVariables.newFieldInRecord("pnd_Comment1", "#COMMENT1", FieldType.STRING, 79);
        pnd_Comment2 = localVariables.newFieldInRecord("pnd_Comment2", "#COMMENT2", FieldType.STRING, 79);
        pnd_Amount = localVariables.newFieldInRecord("pnd_Amount", "#AMOUNT", FieldType.NUMERIC, 14, 2);
        pnd_Ads_Ia_Super1 = localVariables.newFieldInRecord("pnd_Ads_Ia_Super1", "#ADS-IA-SUPER1", FieldType.STRING, 41);

        pnd_Ads_Ia_Super1__R_Field_7 = localVariables.newGroupInRecord("pnd_Ads_Ia_Super1__R_Field_7", "REDEFINE", pnd_Ads_Ia_Super1);
        pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rqst_Id = pnd_Ads_Ia_Super1__R_Field_7.newFieldInGroup("pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rqst_Id", "#ADS-IA-RQST-ID", 
            FieldType.STRING, 35);
        pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde = pnd_Ads_Ia_Super1__R_Field_7.newFieldInGroup("pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde", "#ADS-IA-RCRD-CDE", 
            FieldType.STRING, 3);
        pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr = pnd_Ads_Ia_Super1__R_Field_7.newFieldInGroup("pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr", "#ADS-IA-RCRD-SQNCE-NBR", 
            FieldType.NUMERIC, 3);
        pnd_Max_Rslt = localVariables.newFieldInRecord("pnd_Max_Rslt", "#MAX-RSLT", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Rates = localVariables.newFieldInRecord("pnd_Max_Rates", "#MAX-RATES", FieldType.PACKED_DECIMAL, 3);
        pnd_Work_Rqst = localVariables.newFieldInRecord("pnd_Work_Rqst", "#WORK-RQST", FieldType.STRING, 35);

        pnd_Work_Rqst__R_Field_8 = localVariables.newGroupInRecord("pnd_Work_Rqst__R_Field_8", "REDEFINE", pnd_Work_Rqst);
        pnd_Work_Rqst_Pnd_Work_Rqst_30 = pnd_Work_Rqst__R_Field_8.newFieldInGroup("pnd_Work_Rqst_Pnd_Work_Rqst_30", "#WORK-RQST-30", FieldType.STRING, 
            30);
        pnd_Work_Rqst_Pnd_Work_Rqst_5 = pnd_Work_Rqst__R_Field_8.newFieldInGroup("pnd_Work_Rqst_Pnd_Work_Rqst_5", "#WORK-RQST-5", FieldType.STRING, 5);

        pnd_Work_Rqst__R_Field_9 = localVariables.newGroupInRecord("pnd_Work_Rqst__R_Field_9", "REDEFINE", pnd_Work_Rqst);
        pnd_Work_Rqst_Pnd_Work_Rqst_Pin7 = pnd_Work_Rqst__R_Field_9.newFieldInGroup("pnd_Work_Rqst_Pnd_Work_Rqst_Pin7", "#WORK-RQST-PIN7", FieldType.NUMERIC, 
            7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Ia_Rslt2.reset();

        ldaAdsl760.initializeValues();
        ldaAdsl761.initializeValues();
        ldaAdsl763.initializeValues();
        ldaAdsl450.initializeValues();
        ldaAdsl450a.initializeValues();
        ldaAdsl401a.initializeValues();
        ldaAdsl401.initializeValues();

        localVariables.reset();
        pnd_Type_Of_Call.setInitialValue("R");
        pnd_All_Indx_Pnd_Z.setInitialValue(0);
        pnd_All_Indx_Pnd_A.setInitialValue(0);
        pnd_All_Indx_Pnd_A_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_B_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_C_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Next_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Tiaa_Indx_1.setInitialValue(0);
        pnd_All_Indx_Pnd_Tiaa_Indx_2.setInitialValue(0);
        pnd_All_Indx_Pnd_Tiaa_Indx_3.setInitialValue(0);
        pnd_All_Indx_Pnd_Tiaa_Indx_4.setInitialValue(0);
        pnd_All_Indx_Pnd_Tiaa_Indx_5.setInitialValue(0);
        pnd_All_Indx_Pnd_Tiaa_Indx_6.setInitialValue(0);
        pnd_All_Indx_Pnd_Prod_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_I.setInitialValue(0);
        pnd_Total_Tiaa_Nbr.setInitialValue(0);
        pnd_Total_Stable_Nbr.setInitialValue(0);
        pnd_New_Header_Page_1.setInitialValue(true);
        pnd_New_Header_Page_2.setInitialValue(true);
        pnd_New_Header_Page_3.setInitialValue(true);
        pnd_New_Header_Page_4.setInitialValue(true);
        pnd_New_Header_Page_5.setInitialValue(true);
        pnd_New_Header_Page_6.setInitialValue(true);
        pnd_First_Time.setInitialValue(true);
        pnd_Tiaa_New_Record.setInitialValue(true);
        pnd_Tiaa_Record.setInitialValue(true);
        pnd_Cref_Record.setInitialValue(true);
        pnd_Real_Record.setInitialValue(true);
        pnd_Tiaa_Prod_Code.setInitialValue(true);
        pnd_Tiaa_Rec_Found.setInitialValue(true);
        pnd_Record_Written.setInitialValue(true);
        pnd_Tiaa_Rec_Not_Found.setInitialValue(true);
        pnd_End_Of_Ia_File.setInitialValue(true);
        pnd_Roth_Indicator_On.setInitialValue(true);
        pnd_Pec_Nbr_Active_Acct.setInitialValue(20);
        pnd_Max_Rslt.setInitialValue(125);
        pnd_Max_Rates.setInitialValue(250);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp762() throws Exception
    {
        super("Adsp762");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 79 PS = 60;//Natural: FORMAT ( 1 ) LS = 100 PS = 60;//Natural: FORMAT ( 2 ) LS = 100 PS = 60
        pnd_Tpa_Io_Lit_774.setValue("Grd");                                                                                                                               //Natural: ASSIGN #TPA-IO-LIT-774 := 'Grd'
        pnd_Tiaa_Tpa_Io_665.setValue("TIAA");                                                                                                                             //Natural: ASSIGN #TIAA-TPA-IO-665 := 'TIAA'
        //*  NEW EXTERNALIZATION + SEQUENCING OF PROD CDE
        DbsUtil.callnat(Adsn888.class , getCurrentProcessState(), pdaAdsa888.getPnd_Parm_Area(), pdaAdsa888.getPnd_Nbr_Acct());                                           //Natural: CALLNAT 'ADSN888' #PARM-AREA #NBR-ACCT
        if (condition(Global.isEscape())) return;
        PND_PND_L1070:                                                                                                                                                    //Natural: REPEAT
        while (condition(whileTrue))
        {
            getWorkFiles().read(5, pnd_Rec_Type, ldaAdsl401.getVw_ads_Prtcpnt_View(), pnd_Tiaa_Rlvr_A, pnd_Cref_Rlvr_A, pnd_Real_Rlvr_A);                                 //Natural: READ WORK FILE 5 ONCE #REC-TYPE ADS-PRTCPNT-VIEW #TIAA-RLVR-A #CREF-RLVR-A #REAL-RLVR-A
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                if (true) break PND_PND_L1070;                                                                                                                            //Natural: ESCAPE BOTTOM ( ##L1070. )
            }                                                                                                                                                             //Natural: END-ENDFILE
            //*  GG050608
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Roth_Rqst_Ind().equals("Y")))                                                                                //Natural: IF ADP-ROTH-RQST-IND = 'Y'
            {
                pnd_Roth_Indicator_On.setValue(true);                                                                                                                     //Natural: MOVE TRUE TO #ROTH-INDICATOR-ON
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "SHOW ME PRTCPNT:",pnd_Rec_Type,pnd_Tiaa_Rlvr_A,pnd_Cref_Rlvr_A,pnd_Real_Rlvr_A);                                                       //Natural: WRITE 'SHOW ME PRTCPNT:' #REC-TYPE #TIAA-RLVR-A #CREF-RLVR-A #REAL-RLVR-A
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1070"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1070"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde().equals("R")))                                                                                 //Natural: IF ADP-ANNT-TYP-CDE = 'R'
            {
                ldaAdsl761.getPnd_Final_Prem_Ind().setValue("FINAL PREMIUM");                                                                                             //Natural: MOVE 'FINAL PREMIUM' TO #FINAL-PREM-IND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl761.getPnd_Final_Prem_Ind().setValue(" ");                                                                                                         //Natural: MOVE ' ' TO #FINAL-PREM-IND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_First_Time.getBoolean()))                                                                                                                   //Natural: IF #FIRST-TIME
            {
                pnd_New_Header_Page_1.setValue(false);                                                                                                                    //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-1
                pnd_New_Header_Page_2.setValue(false);                                                                                                                    //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-2
                pnd_New_Header_Page_3.setValue(false);                                                                                                                    //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-3
                pnd_New_Header_Page_4.setValue(false);                                                                                                                    //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-4
                pnd_New_Header_Page_5.setValue(false);                                                                                                                    //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-5
                pnd_New_Header_Page_6.setValue(false);                                                                                                                    //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-6
            }                                                                                                                                                             //Natural: END-IF
            //*  PIN EXPANSION
            ldaAdsl761.getPnd_Rqst_Id().setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                                               //Natural: MOVE ADS-PRTCPNT-VIEW.RQST-ID TO #RQST-ID #WORK-RQST
            pnd_Work_Rqst.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());
            if (condition(pnd_Work_Rqst_Pnd_Work_Rqst_5.equals(" ")))                                                                                                     //Natural: IF #WORK-RQST-5 = ' '
            {
                ldaAdsl761.getPnd_Rqst_Id_Pnd_Unique_Id().setValue(pnd_Work_Rqst_Pnd_Work_Rqst_Pin7);                                                                     //Natural: ASSIGN #RQST-ID.#UNIQUE-ID := #WORK-RQST-PIN7
            }                                                                                                                                                             //Natural: END-IF
            //* *MOVE ADS-PRTCPNT-VIEW.RQST-ID TO #NAZA870-RQST-ID
            ldaAdsl761.getPnd_Nap_Mit_Log_Dte_Tme().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Mit_Log_Dte_Tme());                                                       //Natural: MOVE ADP-MIT-LOG-DTE-TME TO #NAP-MIT-LOG-DTE-TME
            ldaAdsl761.getPnd_Nap_Bps_Unit().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Bps_Unit());                                                                     //Natural: MOVE ADP-BPS-UNIT TO #NAP-BPS-UNIT
            //* *IF ADP-ANNT-TYP-CDE = 'R'
            //* *  MOVE 'AZ1' TO #NAZA870-TRN-OPER-NME
            //* *ELSE
            //* *  MOVE 'NAZ' TO #NAZA870-TRN-OPER-NME
            //* *END-IF
            pnd_Total_Tiaa_Nbr.setValue(0);                                                                                                                               //Natural: MOVE 0 TO #TOTAL-TIAA-NBR
            //*  RS0
            pnd_Total_Stable_Nbr.setValue(0);                                                                                                                             //Natural: MOVE 0 TO #TOTAL-STABLE-NBR
            pnd_Tiaa_New_Record.setValue(true);                                                                                                                           //Natural: MOVE TRUE TO #TIAA-NEW-RECORD
            pnd_Tiaa_Record.setValue(false);                                                                                                                              //Natural: MOVE FALSE TO #TIAA-RECORD
            pnd_Tiaa_Prod_Code.setValue(false);                                                                                                                           //Natural: MOVE FALSE TO #TIAA-PROD-CODE
            ldaAdsl761.getPnd_Target_All_Pnd_Target_Disc().getValue("*").reset();                                                                                         //Natural: RESET #TARGET-DISC ( * ) #TIAA-CERT-IND ( * )
            ldaAdsl761.getPnd_Tiaa_Cert_Ind().getValue("*").reset();
            //* *  #NAZL870-GOOD-BAD-IND   (*)
            //* *  #NAZL870-CREF-PROD      (*)
            //* *  #NAZL870-RTB-PRM-TME    (*)
                                                                                                                                                                          //Natural: PERFORM BUILD-TIAA-PARTICIPANT-HEADER-DATA
            sub_Build_Tiaa_Participant_Header_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1070"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1070"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM BUILD-DATA-ENTRY-INFORMATION
            sub_Build_Data_Entry_Information();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1070"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1070"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getWorkFiles().read(5, pnd_Rec_Type, ldaAdsl401a.getAds_Cntrct_View_Rqst_Id(), pnd_Nac_Ppg_Code.getValue("*"), pnd_Nac_Ppg_Ind.getValue("*"),                 //Natural: READ WORK FILE 5 ONCE #REC-TYPE ADS-CNTRCT-VIEW.RQST-ID #NAC-PPG-CODE ( * ) #NAC-PPG-IND ( * ) #TIAA-NBR ( * ) #TIAA-IND ( * ) #TIAA-STLMNT-AMT #CREF-NBR ( * ) #CREF-IND ( * ) #CREF-STLMNT-AMT #REAL-NBR ( * ) #REAL-IND ( * ) #REAL-STLMNT-AMT
                pnd_Tiaa_Nbr.getValue("*"), pnd_Tiaa_Ind.getValue("*"), pnd_Tiaa_Stlmnt_Amt, pnd_Cref_Nbr.getValue("*"), pnd_Cref_Ind.getValue("*"), pnd_Cref_Stlmnt_Amt, 
                pnd_Real_Nbr.getValue("*"), pnd_Real_Ind.getValue("*"), pnd_Real_Stlmnt_Amt);
            getReports().write(0, "SHOW ME CNTRCT:",pnd_Rec_Type,pnd_Rec_Type_Pnd_Z_Indx);                                                                                //Natural: WRITE 'SHOW ME CNTRCT:' #REC-TYPE #Z-INDX
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1070"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1070"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_All_Indx_Pnd_X_Indx.setValue(pnd_Rec_Type_Pnd_Z_Indx);                                                                                                    //Natural: MOVE #Z-INDX TO #X-INDX
            PND_PND_L1460:                                                                                                                                                //Natural: FOR #Y-INDX 1 TO #X-INDX
            for (pnd_All_Indx_Pnd_Y_Indx.setValue(1); condition(pnd_All_Indx_Pnd_Y_Indx.lessOrEqual(pnd_All_Indx_Pnd_X_Indx)); pnd_All_Indx_Pnd_Y_Indx.nadd(1))
            {
                getWorkFiles().read(5, pnd_Rec_Type, ldaAdsl401a.getVw_ads_Cntrct_View());                                                                                //Natural: READ WORK FILE 5 ONCE #REC-TYPE ADS-CNTRCT-VIEW
                if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                  //Natural: AT END OF FILE
                {
                    if (true) break PND_PND_L1460;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L1460. )
                }                                                                                                                                                         //Natural: END-ENDFILE
                FOR01:                                                                                                                                                    //Natural: FOR #A-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
                for (pnd_All_Indx_Pnd_A_Indx.setValue(1); condition(pnd_All_Indx_Pnd_A_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_A_Indx.nadd(1))
                {
                    //*  RS0
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).equals("T") || ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).equals("Y"))) //Natural: IF ADC-ACCT-CDE ( #A-INDX ) = 'T' OR ADC-ACCT-CDE ( #A-INDX ) = 'Y'
                    {
                        pnd_Tiaa_Record.setValue(true);                                                                                                                   //Natural: MOVE TRUE TO #TIAA-RECORD
                        pnd_Tiaa_Prod_Code.setValue(true);                                                                                                                //Natural: MOVE TRUE TO #TIAA-PROD-CODE
                        pnd_Total_Tiaa_Nbr.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TOTAL-TIAA-NBR
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L1460"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1460"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_Tiaa_New_Record.getBoolean() && pnd_Tiaa_Record.getBoolean()))                                                                          //Natural: IF #TIAA-NEW-RECORD AND #TIAA-RECORD
                {
                                                                                                                                                                          //Natural: PERFORM BUILD-TIAA-DA-RSLT-HEADER-DATA
                    sub_Build_Tiaa_Da_Rslt_Header_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L1460"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1460"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM WRITE-TIAA-PARTICIPANT-HEADER-DATA
                    sub_Write_Tiaa_Participant_Header_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L1460"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1460"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM RESET-HEADER-TOTALS
                    sub_Reset_Header_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L1460"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1460"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Tiaa_New_Record.setValue(false);                                                                                                                  //Natural: MOVE FALSE TO #TIAA-NEW-RECORD
                    ldaAdsl761.getPnd_Final_Prem_Ind().reset();                                                                                                           //Natural: RESET #FINAL-PREM-IND
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Tiaa_Record.getBoolean()))                                                                                                              //Natural: IF #TIAA-RECORD
                {
                    ldaAdsl761.getPnd_Tiaa_Contract_Nbr().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr());                                                        //Natural: MOVE ADC-TIAA-NBR TO #TIAA-CONTRACT-NBR
                    ldaAdsl761.getPnd_Cref_Certificate_Nbr().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Cref_Nbr());                                                     //Natural: MOVE ADC-CREF-NBR TO #CREF-CERTIFICATE-NBR
                    ldaAdsl760.getPnd_Tiaa_Rec_Cnt().nadd(1);                                                                                                             //Natural: ADD 1 TO #TIAA-REC-CNT
                    //*  GG050608
                    if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                    //Natural: IF #ROTH-INDICATOR-ON
                    {
                        ldaAdsl760.getPnd_Tiaa_Rec_Cnt_Surv().nadd(1);                                                                                                    //Natural: ADD 1 TO #TIAA-REC-CNT-SURV
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-TIAA-DA-RATE-BASIS-AMT
                    sub_Write_Tiaa_Da_Rate_Basis_Amt();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L1460"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1460"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM SEQUENCE-TOTAL-SETTLEMENT-FOR-ALL-TIAA-CONTRACTS
                    sub_Sequence_Total_Settlement_For_All_Tiaa_Contracts();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L1460"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1460"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //* *  IF #TIAA-RECORD
                //* *    IF ADC-GD-CNTRCT-IND
                //* *      MOVE 'Y' TO #NAZL870-GOOD-BAD-IND (#Y-INDX)
                //* *      MOVE 'T' TO #NAZL870-CREF-PROD (#Y-INDX)
                //* *      MOVE ADC-TIAA-NBR TO #NAZL870-TIAA-NBR (#Y-INDX)
                //* *      MOVE ADC-CREF-NBR TO #NAZL870-CREF-CERT-NBR (#Y-INDX)
                //* *    END-IF
                //* *  END-IF
                                                                                                                                                                          //Natural: PERFORM RESET-TIAA-DETAIL-TOTAL-SAVE-AREA
                sub_Reset_Tiaa_Detail_Total_Save_Area();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L1460"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1460"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaAdsl761.getPnd_Cref_Cert_Nbr().getValue("*").reset();                                                                                                  //Natural: RESET #CREF-CERT-NBR ( * ) #TIAA-CERT-NBR ( * )
                ldaAdsl761.getPnd_Tiaa_Cert_Nbr().getValue("*").reset();
                pnd_Tiaa_Record.setValue(false);                                                                                                                          //Natural: MOVE FALSE TO #TIAA-RECORD
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1070"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1070"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM READ-IA-RESULT-FILE
            sub_Read_Ia_Result_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1070"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1070"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(! (pnd_End_Of_Ia_File.getBoolean())))                                                                                                           //Natural: IF NOT #END-OF-IA-FILE
            {
                if (condition(pnd_Total_Tiaa_Nbr.greater(1)))                                                                                                             //Natural: IF #TOTAL-TIAA-NBR > 1
                {
                    pnd_Tiaa_Record.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO #TIAA-RECORD
                    pnd_New_Header_Page_2.setValue(true);                                                                                                                 //Natural: MOVE TRUE TO #NEW-HEADER-PAGE-2
                                                                                                                                                                          //Natural: PERFORM WRITE-TIAA-TOTAL-SETTLEMENT-LINE
                    sub_Write_Tiaa_Total_Settlement_Line();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L1070"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1070"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Amount.setValue(ldaAdsl760.getPnd_Tiaa_Final_Totals_Pnd_Tiaa_Da_Accum_Final_Total());                                                             //Natural: ASSIGN #AMOUNT := #TIAA-FINAL-TOTALS.#TIAA-DA-ACCUM-FINAL-TOTAL
                                                                                                                                                                          //Natural: PERFORM PUT-ASTERISK-BESIDE-TOTAL
                    sub_Put_Asterisk_Beside_Total();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L1070"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1070"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm776.class));                                                                   //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM776'
                    if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                    //Natural: IF #ROTH-INDICATOR-ON
                    {
                        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm776.class));                                                               //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM776'
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_New_Header_Page_2.setValue(false);                                                                                                                //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-2
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-IA-RSLT-ACCUMULATIONS
                sub_Calculate_Tiaa_Ia_Rslt_Accumulations();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L1070"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1070"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaAdsl760.getPnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Total().setValue(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                          //Natural: MOVE ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #TIAA-IVC-TOTAL
                ldaAdsl760.getPnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Annuity().compute(new ComputeParameters(true, ldaAdsl760.getPnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Annuity()), //Natural: COMPUTE ROUNDED #TIAA-IVC-ANNUITY = ADS-IA-RSLT.ADI-TIAA-IVC-AMT
                    ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());
                                                                                                                                                                          //Natural: PERFORM PROCESS-ANNUITANT-TOTAL-RTN
                sub_Process_Annuitant_Total_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L1070"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1070"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* *  PERFORM  WRITE-TESTCASE-REPORT
            }                                                                                                                                                             //Natural: END-IF
            pnd_Roth_Indicator_On.resetInitial();                                                                                                                         //Natural: RESET INITIAL #ROTH-INDICATOR-ON
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB-RTN
        sub_End_Of_Job_Rtn();
        if (condition(Global.isEscape())) {return;}
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IA-RESULT-FILE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-ADS-IA-RSLT-AREA
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-TIAA-PARTICIPANT-HEADER-DATA
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-DATA-ENTRY-INFORMATION
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-TIAA-DA-RSLT-HEADER-DATA
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TIAA-PARTICIPANT-HEADER-DATA
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TIAA-DA-RATE-BASIS-AMT
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SEQUENCE-TOTAL-SETTLEMENT-FOR-ALL-TIAA-CONTRACTS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TIAA-IA-RSLT-ACCUMULATIONS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SEQUENCE-AND-WRITE-SETTLEMENT-FOR-STANDARD-METHOD
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-WRITE-SETTLEMENT-FOR-GRADED-METHOD
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-TIAA-TOTAL-SETTLEMENT-LINE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-IA-PAYMENT-STANDARD-METHOD
        //*    #TIAA-IA-STD-GUR-PAY-RENEW (1:60)
        //*    #TIAA-IA-STD-DIV-PAY-RENEW (1:60)
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-IA-PAYMENT-GRADED-METHOD
        //*    #TIAA-IA-GRD-GUR-PAY-RENEW (1:60)
        //*    #TIAA-IA-GRD-DIV-PAY-RENEW (1:60)
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-ANNUITANT-TOTAL-RTN
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-TIAA-DETAIL-TOTAL-SAVE-AREA
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-TIAA-FINAL-TOTAL-SAVE-AREA
        //*  #TIAA-IA-STD-GUR-PAY-RENEW (1:60)
        //*  #TIAA-IA-STD-DIV-PAY-RENEW (1:60)
        //*  #TIAA-IA-GRD-DIV-PAY-RENEW (1:60)
        //*  #TIAA-IA-GRD-GUR-PAY-RENEW (1:60)
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-HEADER-TOTALS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PUT-ASTERISK-BESIDE-TOTAL
        //* * DEFINE SUBROUTINE WRITE-TESTCASE-REPORT
        //* * RESET #B-INDX
        //* * FOR #B-INDX 1 TO 10
        //* *  IF #NAZL870-GOOD-BAD-IND (#B-INDX) = 'Y'
        //* *    IF #NAZL870-CREF-PROD (#B-INDX) = 'T'
        //* *      MOVE #NAZL870-TIAA-NBR      (#B-INDX) TO #NAZA870-TIAA-NBR
        //* *      MOVE #NAZL870-CREF-CERT-NBR (#B-INDX) TO #NAZA870-CREF-CERT-NBR
        //* *      MOVE #NAZL870-NON-PRM-DTE   (#B-INDX) TO #NAZA870-NON-PRM-DTE
        //* *      MOVE #NAZL870-NON-PRM-TME   (#B-INDX) TO #NAZA870-NON-PRM-TME
        //* *      MOVE #PAGE-NUMBER TO #NAZA870-PAGE-NUMBER
        //* *      FETCH RETURN 'NAZP870'
        //* *      MOVE #NAZA870-PAGE-NUMBER TO #PAGE-NUMBER
        //* *    END-IF
        //* *  END-IF
        //* *END-FOR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-JOB-RTN
    }
    private void sub_Read_Ia_Result_File() throws Exception                                                                                                               //Natural: READ-IA-RESULT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_End_Of_Ia_File.setValue(false);                                                                                                                               //Natural: MOVE FALSE TO #END-OF-IA-FILE
        getWorkFiles().read(5, pnd_Rec_Type, ldaAdsl450.getVw_ads_Ia_Rslt_View(), ldaAdsl450.getPnd_More());                                                              //Natural: READ WORK FILE 5 ONCE #REC-TYPE ADS-IA-RSLT-VIEW #MORE
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            pnd_End_Of_Ia_File.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #END-OF-IA-FILE
        }                                                                                                                                                                 //Natural: END-ENDFILE
        //*  EM - 030612
                                                                                                                                                                          //Natural: PERFORM POPULATE-ADS-IA-RSLT-AREA
        sub_Populate_Ads_Ia_Rslt_Area();
        if (condition(Global.isEscape())) {return;}
    }
    //*  EM - 030612 START
    private void sub_Populate_Ads_Ia_Rslt_Area() throws Exception                                                                                                         //Natural: POPULATE-ADS-IA-RSLT-AREA
    {
        if (BLNatReinput.isReinput()) return;

        ldaAdsl450a.getAds_Ia_Rslt().setValuesByName(ldaAdsl450.getVw_ads_Ia_Rslt_View());                                                                                //Natural: MOVE BY NAME ADS-IA-RSLT-VIEW TO ADS-IA-RSLT
        ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Data().reset();                                                                                                           //Natural: RESET ADS-IA-RSLT.ADI-DTL-TIAA-DATA ADS-IA-RSLT.#ADI-DTL-TIA-TPA-GUAR-COMMUT-DAT ADS-IA-RSLT.#ADI-CONTRACT-ID ( * )
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Tpa_Guar_Commut_Dat().reset();
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Contract_Id().getValue("*").reset();
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-RATE-CD ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-RATE-CD ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-STD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-STD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-GRD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-GRD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Acct_Cd().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Acct_Cd().getValue(1,     //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-ACCT-CD ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-ACCT-CD ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Eff_Rte_Intrst().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Eff_Rte_Intrst().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-EFF-RTE-INTRST ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-EFF-RTE-INTRST ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-GRD-GRNTD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-GRD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-GRD-PAY-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-FNL-GRD-PAY-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-GRD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-FNL-GRD-DVDND-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-STD-PAY-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-FNL-STD-PAY-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-STD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-FNL-STD-DVDND-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Std_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIA-GUAR-COMMUT-STD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GUAR-COMMUT-STD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Grd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIA-GUAR-COMMUT-GRD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GUAR-COMMUT-GRD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Contract_Id().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Contract_Id().getValue(1,               //Natural: ASSIGN ADS-IA-RSLT.#ADI-CONTRACT-ID ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-CONTRACT-ID ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        if (condition(ldaAdsl450.getPnd_More().equals("Y")))                                                                                                              //Natural: IF #MORE = 'Y'
        {
            pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rqst_Id.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Rqst_Id());                                                                      //Natural: ASSIGN #ADS-IA-RQST-ID := ADS-IA-RSLT-VIEW.RQST-ID
            pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde());                                                             //Natural: ASSIGN #ADS-IA-RCRD-CDE := ADS-IA-RSLT-VIEW.ADI-IA-RCRD-CDE
            pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr());                                                         //Natural: ASSIGN #ADS-IA-RCRD-SQNCE-NBR := ADS-IA-RSLT-VIEW.ADI-SQNCE-NBR
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde().equals("ACT")))                                                                                //Natural: IF ADS-IA-RSLT-VIEW.ADI-IA-RCRD-CDE = 'ACT'
            {
                pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr.setValue(2);                                                                                                  //Natural: ASSIGN #ADS-IA-RCRD-SQNCE-NBR := 2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde(),          //Natural: COMPRESS ADS-IA-RSLT-VIEW.ADI-IA-RCRD-CDE '1' INTO #ADS-IA-RCRD-CDE LEAVING NO SPACE
                    "1"));
            }                                                                                                                                                             //Natural: END-IF
            vw_ads_Ia_Rslt2.startDatabaseFind                                                                                                                             //Natural: FIND ADS-IA-RSLT2 WITH ADI-SUPER1 = #ADS-IA-SUPER1
            (
            "FIND01",
            new Wc[] { new Wc("ADI_SUPER1", "=", pnd_Ads_Ia_Super1, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_ads_Ia_Rslt2.readNextRow("FIND01")))
            {
                vw_ads_Ia_Rslt2.setIfNotFoundControlFlag(false);
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-RATE-CD ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-DA-RATE-CD ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-STD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-DA-STD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-GRD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-DA-GRD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Acct_Cd().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-ACCT-CD ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-ACCT-CD ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-IA-RATE-CD ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Eff_Rte_Intrst().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-EFF-RTE-INTRST ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-EFF-RTE-INTRST ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-GRD-GRNTD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-GRD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-DVDND-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-GRD-PAY-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-FNL-GRD-PAY-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-GRD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-FNL-GRD-DVDND-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-STD-GRNTD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-STD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-STD-PAY-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-FNL-STD-PAY-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-STD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-FNL-STD-DVDND-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Std_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIA-GUAR-COMMUT-STD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-GUAR-COMMUT-STD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Grd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIA-GUAR-COMMUT-GRD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-GUAR-COMMUT-GRD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Contract_Id().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Contract_Id.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-CONTRACT-ID ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-CONTRACT-ID ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  EM - 030612 END
    }
    private void sub_Build_Tiaa_Participant_Header_Data() throws Exception                                                                                                //Natural: BUILD-TIAA-PARTICIPANT-HEADER-DATA
    {
        if (BLNatReinput.isReinput()) return;

        ldaAdsl761.getPnd_Partcipant_Name_1().setValue(DbsUtil.compress(ldaAdsl401.getAds_Prtcpnt_View_Adp_Frst_Annt_Frst_Nme(), ldaAdsl401.getAds_Prtcpnt_View_Adp_Frst_Annt_Mid_Nme(),  //Natural: COMPRESS ADP-FRST-ANNT-FRST-NME ADP-FRST-ANNT-MID-NME ADP-FRST-ANNT-LST-NME INTO #PARTCIPANT-NAME-1
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Frst_Annt_Lst_Nme()));
        ldaAdsl761.getPnd_Partcipant_Name_2().setValue(DbsUtil.compress(ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Frst_Nme(), ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Mid_Nme(),  //Natural: COMPRESS ADP-SCND-ANNT-FRST-NME ADP-SCND-ANNT-MID-NME ADP-SCND-ANNT-LST-NME INTO #PARTCIPANT-NAME-2
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Lst_Nme()));
        pnd_Code.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Frst_Annt_Rsdnc_Cde());                                                                                      //Natural: MOVE ADP-FRST-ANNT-RSDNC-CDE TO #CODE
        DbsUtil.callnat(Adsn035.class , getCurrentProcessState(), pdaAdspda_M.getMsg_Info_Sub(), pnd_Type_Of_Call, pnd_Code, pnd_Desc);                                   //Natural: CALLNAT 'ADSN035' MSG-INFO-SUB #TYPE-OF-CALL #CODE #DESC
        if (condition(Global.isEscape())) return;
        ldaAdsl761.getPnd_Residence().setValue(pnd_Desc);                                                                                                                 //Natural: MOVE #DESC TO #RESIDENCE
        ldaAdsl761.getPnd_Form_Comp_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Forms_Rcvd_Dte(),new ReportEditMask("YYYYMMDD"));                    //Natural: MOVE EDITED ADP-FORMS-RCVD-DTE ( EM = YYYYMMDD ) TO #FORM-COMP-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Mm());                       //Natural: MOVE #FORM-COMP-DTE-MM TO #FORM-COMP-DTE-MDCY-MM
        ldaAdsl761.getPnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Dd());                       //Natural: MOVE #FORM-COMP-DTE-DD TO #FORM-COMP-DTE-MDCY-DD
        ldaAdsl761.getPnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Yyyy());                   //Natural: MOVE #FORM-COMP-DTE-YYYY TO #FORM-COMP-DTE-MDCY-YYYY
        ldaAdsl761.getPnd_Frst_Perod_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Strt_Dte(),new ReportEditMask("YYYYMMDD"));                   //Natural: MOVE EDITED ADP-ANNTY-STRT-DTE ( EM = YYYYMMDD ) TO #FRST-PEROD-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Mm());                   //Natural: MOVE #FRST-PEROD-DTE-MM TO #FRST-PEROD-DTE-MDCY-MM
        ldaAdsl761.getPnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Dd());                   //Natural: MOVE #FRST-PEROD-DTE-DD TO #FRST-PEROD-DTE-MDCY-DD
        ldaAdsl761.getPnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Yyyy());               //Natural: MOVE #FRST-PEROD-DTE-YYYY TO #FRST-PEROD-DTE-MDCY-YYYY
        //* * MOVE EDITED ADP-LST-PRM-RCVD-DTE(EM=YYYYMMDD) TO
        //* *                                     #LAST-PREM-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Last_Prem_Dte_Mdcy_Pnd_Last_Prem_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Last_Prem_Dte_Yyyymmdd_Pnd_Last_Prem_Dte_Mm());                       //Natural: MOVE #LAST-PREM-DTE-MM TO #LAST-PREM-DTE-MDCY-MM
        ldaAdsl761.getPnd_Last_Prem_Dte_Mdcy_Pnd_Last_Prem_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Last_Prem_Dte_Yyyymmdd_Pnd_Last_Prem_Dte_Dd());                       //Natural: MOVE #LAST-PREM-DTE-DD TO #LAST-PREM-DTE-MDCY-DD
        ldaAdsl761.getPnd_Last_Prem_Dte_Mdcy_Pnd_Last_Prem_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Last_Prem_Dte_Yyyymmdd_Pnd_Last_Prem_Dte_Yyyy());                   //Natural: MOVE #LAST-PREM-DTE-YYYY TO #LAST-PREM-DTE-MDCY-YYYY
        ldaAdsl761.getPnd_Non_Prem_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                         //Natural: MOVE EDITED ADP-EFFCTV-DTE ( EM = YYYYMMDD ) TO #NON-PREM-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Mm());                           //Natural: MOVE #NON-PREM-DTE-MM TO #NON-PREM-DTE-MDCY-MM
        ldaAdsl761.getPnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Dd());                           //Natural: MOVE #NON-PREM-DTE-DD TO #NON-PREM-DTE-MDCY-DD
        ldaAdsl761.getPnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Yyyy());                       //Natural: MOVE #NON-PREM-DTE-YYYY TO #NON-PREM-DTE-MDCY-YYYY
        //* *MOVE EDITED ADP-EFFCTV-DTE (EM=CCYYMMDD)
        //* *  TO  #NAZA870-HOLD-SETTL-DTE
        ldaAdsl761.getPnd_Cont_Issue_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Cntr_Prt_Issue_Dte(),new ReportEditMask("YYYYMMDD"));               //Natural: MOVE EDITED ADP-CNTR-PRT-ISSUE-DTE ( EM = YYYYMMDD ) TO #CONT-ISSUE-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Mm());                   //Natural: MOVE #CONT-ISSUE-DTE-MM TO #CONT-ISSUE-DTE-MDCY-MM
        ldaAdsl761.getPnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Dd());                   //Natural: MOVE #CONT-ISSUE-DTE-DD TO #CONT-ISSUE-DTE-MDCY-DD
        ldaAdsl761.getPnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Yyyy());               //Natural: MOVE #CONT-ISSUE-DTE-YYYY TO #CONT-ISSUE-DTE-MDCY-YYYY
        ldaAdsl761.getPnd_Effective_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                        //Natural: MOVE EDITED ADP-EFFCTV-DTE ( EM = YYYYMMDD ) TO #EFFECTIVE-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Dd());                       //Natural: MOVE #EFFECTIVE-DTE-DD TO #EFFECTIVE-DTE-MDCY-DD
        ldaAdsl761.getPnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Mm());                       //Natural: MOVE #EFFECTIVE-DTE-MM TO #EFFECTIVE-DTE-MDCY-MM
        ldaAdsl761.getPnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Yyyy());                   //Natural: MOVE #EFFECTIVE-DTE-YYYY TO #EFFECTIVE-DTE-MDCY-YYYY
        ldaAdsl761.getPnd_Frst_Birth_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Frst_Annt_Dte_Of_Brth(),new ReportEditMask("YYYYMMDD"));            //Natural: MOVE EDITED ADP-FRST-ANNT-DTE-OF-BRTH ( EM = YYYYMMDD ) TO #FRST-BIRTH-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Mm());                   //Natural: MOVE #FRST-BIRTH-DTE-MM TO #FRST-BIRTH-DTE-MDCY-MM
        ldaAdsl761.getPnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Dd());                   //Natural: MOVE #FRST-BIRTH-DTE-DD TO #FRST-BIRTH-DTE-MDCY-DD
        ldaAdsl761.getPnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Yyyy());               //Natural: MOVE #FRST-BIRTH-DTE-YYYY TO #FRST-BIRTH-DTE-MDCY-YYYY
        ldaAdsl761.getPnd_Scnd_Birth_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Dte_Of_Brth(),new ReportEditMask("YYYYMMDD"));            //Natural: MOVE EDITED ADP-SCND-ANNT-DTE-OF-BRTH ( EM = YYYYMMDD ) TO #SCND-BIRTH-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Mm());                   //Natural: MOVE #SCND-BIRTH-DTE-MM TO #SCND-BIRTH-DTE-MDCY-MM
        ldaAdsl761.getPnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Dd());                   //Natural: MOVE #SCND-BIRTH-DTE-DD TO #SCND-BIRTH-DTE-MDCY-DD
        ldaAdsl761.getPnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Yyyy());               //Natural: MOVE #SCND-BIRTH-DTE-YYYY TO #SCND-BIRTH-DTE-MDCY-YYYY
        ldaAdsl761.getPnd_Annt_Start_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Strt_Dte(),new ReportEditMask("YYYYMMDD"));                   //Natural: MOVE EDITED ADP-ANNTY-STRT-DTE ( EM = YYYYMMDD ) TO #ANNT-START-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Mm());                   //Natural: MOVE #ANNT-START-DTE-MM TO #ANNT-START-DTE-MDCY-MM
        ldaAdsl761.getPnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Dd());                   //Natural: MOVE #ANNT-START-DTE-DD TO #ANNT-START-DTE-MDCY-DD
        ldaAdsl761.getPnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Yyyy());               //Natural: MOVE #ANNT-START-DTE-YYYY TO #ANNT-START-DTE-MDCY-YYYY
        ldaAdsl761.getPnd_Empl_Term_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Emplymnt_Term_Dte(),new ReportEditMask("YYYYMMDD"));                 //Natural: MOVE EDITED ADP-EMPLYMNT-TERM-DTE ( EM = YYYYMMDD ) TO #EMPL-TERM-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Mm());                       //Natural: MOVE #EMPL-TERM-DTE-MM TO #EMPL-TERM-DTE-MDCY-MM
        ldaAdsl761.getPnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Dd());                       //Natural: MOVE #EMPL-TERM-DTE-DD TO #EMPL-TERM-DTE-MDCY-DD
        ldaAdsl761.getPnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Yyyy());                   //Natural: MOVE #EMPL-TERM-DTE-YYYY TO #EMPL-TERM-DTE-MDCY-YYYY
        ldaAdsl761.getPnd_Last_Guar_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Crrspndnce_Addr_Lst_Chg_Dte(),new ReportEditMask("YYYYMMDD"));       //Natural: MOVE EDITED ADP-CRRSPNDNCE-ADDR-LST-CHG-DTE ( EM = YYYYMMDD ) TO #LAST-GUAR-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Mm());                       //Natural: MOVE #LAST-GUAR-DTE-MM TO #LAST-GUAR-DTE-MDCY-MM
        ldaAdsl761.getPnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Dd());                       //Natural: MOVE #LAST-GUAR-DTE-DD TO #LAST-GUAR-DTE-MDCY-DD
        ldaAdsl761.getPnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Yyyy());                   //Natural: MOVE #LAST-GUAR-DTE-YYYY TO #LAST-GUAR-DTE-MDCY-YYYY
        ldaAdsl761.getPnd_Da_Accum_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                         //Natural: MOVE EDITED ADP-EFFCTV-DTE ( EM = YYYYMMDD ) TO #DA-ACCUM-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Mm());                               //Natural: MOVE #DA-ACCUM-DTE-MM TO #DA-ACCUM-DTE-MDCY-MM
        ldaAdsl761.getPnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Dd());                               //Natural: MOVE #DA-ACCUM-DTE-DD TO #DA-ACCUM-DTE-MDCY-DD
        ldaAdsl761.getPnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Yy().setValue(ldaAdsl761.getPnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Yy());                               //Natural: MOVE #DA-ACCUM-DTE-YY TO #DA-ACCUM-DTE-MDCY-YY
        ldaAdsl761.getPnd_Ia_Number().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Ia_Tiaa_Nbr());                                                                         //Natural: MOVE ADP-IA-TIAA-NBR TO #IA-NUMBER
        ldaAdsl761.getPnd_Social_Sec_Nbr_1().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Frst_Annt_Ssn());                                                                //Natural: MOVE ADP-FRST-ANNT-SSN TO #SOCIAL-SEC-NBR-1
        ldaAdsl761.getPnd_Social_Sec_Nbr_2().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Ssn());                                                                //Natural: MOVE ADP-SCND-ANNT-SSN TO #SOCIAL-SEC-NBR-2
        ldaAdsl761.getPnd_Work_Rqst_Id().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Wpid());                                                                             //Natural: MOVE ADP-WPID TO #WORK-RQST-ID
        ldaAdsl761.getPnd_Sex_1().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Frst_Annt_Sex_Cde());                                                                       //Natural: MOVE ADP-FRST-ANNT-SEX-CDE TO #SEX-1
        ldaAdsl761.getPnd_Sex_2().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Sex_Cde());                                                                       //Natural: MOVE ADP-SCND-ANNT-SEX-CDE TO #SEX-2
        ldaAdsl761.getPnd_Frst_Citizenship().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Frst_Annt_Ctznshp());                                                            //Natural: MOVE ADP-FRST-ANNT-CTZNSHP TO #FRST-CITIZENSHIP
        ldaAdsl761.getPnd_Scnd_Spouse().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Scnd_Annt_Rltnshp());                                                                 //Natural: MOVE ADP-SCND-ANNT-RLTNSHP TO #SCND-SPOUSE
        ldaAdsl761.getPnd_Nap_Annty_Optn().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Optn());                                                                     //Natural: MOVE ADP-ANNTY-OPTN TO #NAP-ANNTY-OPTN
        pnd_Payment_Mode.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode());                                                                                       //Natural: MOVE ADP-PYMNT-MODE TO #PAYMENT-MODE
        ldaAdsl761.getPnd_Nap_Grntee_Period().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Grntee_Period());                                                               //Natural: MOVE ADP-GRNTEE-PERIOD TO #NAP-GRNTEE-PERIOD
        ldaAdsl761.getPnd_Nap_Mit_Log_Dte_Tme().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Mit_Log_Dte_Tme());                                                           //Natural: MOVE ADP-MIT-LOG-DTE-TME TO #NAP-MIT-LOG-DTE-TME
        ldaAdsl761.getPnd_Rollover_Amount().setValue(pnd_Tiaa_Rlvr_A_Pnd_Tiaa_Rlvr);                                                                                      //Natural: MOVE #TIAA-RLVR TO #ROLLOVER-AMOUNT
        pnd_All_Indx_Pnd_A.reset();                                                                                                                                       //Natural: RESET #A #ROLLOVER-CONTRACT-NBR ( * )
        ldaAdsl761.getPnd_Rollover_Contract_Nbr().getValue("*").reset();
        if (condition(pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(1)))                                                                                                //Natural: IF #PAYMENT-MODE-BYTE-1 = 1
        {
            pnd_Payment_Mode_All.setValue("MONTHLY");                                                                                                                     //Natural: MOVE 'MONTHLY' TO #PAYMENT-MODE-ALL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(6)))                                                                                            //Natural: IF #PAYMENT-MODE-BYTE-1 = 6
            {
                pnd_Payment_Mode_All.setValue("QUARTLY");                                                                                                                 //Natural: MOVE 'QUARTLY' TO #PAYMENT-MODE-ALL
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(7)))                                                                                        //Natural: IF #PAYMENT-MODE-BYTE-1 = 7
                {
                    pnd_Payment_Mode_All.setValue("SEMI ANNUAL");                                                                                                         //Natural: MOVE 'SEMI ANNUAL' TO #PAYMENT-MODE-ALL
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(8)))                                                                                    //Natural: IF #PAYMENT-MODE-BYTE-1 = 8
                    {
                        pnd_Payment_Mode_All.setValue("ANNUAL ");                                                                                                         //Natural: MOVE 'ANNUAL ' TO #PAYMENT-MODE-ALL
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl761.getPnd_Nap_Pymnt_Mode_All().setValue(DbsUtil.compress(CompressOption.WithDelimiters, '/', pnd_Payment_Mode, pnd_Payment_Mode_All));                    //Natural: COMPRESS #PAYMENT-MODE #PAYMENT-MODE-ALL INTO #NAP-PYMNT-MODE-ALL WITH DELIMITER '/'
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX #DESTINATION-CDE ( * )
        ldaAdsl761.getPnd_Destination_Code_Pnd_Destination_Cde().getValue("*").reset();
    }
    private void sub_Build_Data_Entry_Information() throws Exception                                                                                                      //Natural: BUILD-DATA-ENTRY-INFORMATION
    {
        if (BLNatReinput.isReinput()) return;

        ldaAdsl761.getPnd_First_Entered_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Entry_Dte(),new ReportEditMask("YYYYMMDD"));                         //Natural: MOVE EDITED ADP-ENTRY-DTE ( EM = YYYYMMDD ) TO #FIRST-ENTERED-YYYYMMDD
        ldaAdsl761.getPnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Cc().setValue(ldaAdsl761.getPnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Cc());                   //Natural: MOVE #FIRST-ENTERED-CC TO #FIRST-ENTERED-DTE-MDCY-CC
        ldaAdsl761.getPnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Yy().setValue(ldaAdsl761.getPnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Yy());                   //Natural: MOVE #FIRST-ENTERED-YY TO #FIRST-ENTERED-DTE-MDCY-YY
        ldaAdsl761.getPnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Mm());                   //Natural: MOVE #FIRST-ENTERED-MM TO #FIRST-ENTERED-DTE-MDCY-MM
        ldaAdsl761.getPnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Dd());                   //Natural: MOVE #FIRST-ENTERED-DD TO #FIRST-ENTERED-DTE-MDCY-DD
        ldaAdsl761.getPnd_First_Verified_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Rqst_1st_Vrfd_Dte(),new ReportEditMask("YYYYMMDD"));                //Natural: MOVE EDITED ADP-RQST-1ST-VRFD-DTE ( EM = YYYYMMDD ) TO #FIRST-VERIFIED-YYYYMMDD
        ldaAdsl761.getPnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Cc().setValue(ldaAdsl761.getPnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Cc());               //Natural: MOVE #FIRST-VERIFIED-CC TO #FIRST-VERIFIED-DTE-MDCY-CC
        ldaAdsl761.getPnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Yy().setValue(ldaAdsl761.getPnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Yy());               //Natural: MOVE #FIRST-VERIFIED-YY TO #FIRST-VERIFIED-DTE-MDCY-YY
        ldaAdsl761.getPnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Mm());               //Natural: MOVE #FIRST-VERIFIED-MM TO #FIRST-VERIFIED-DTE-MDCY-MM
        ldaAdsl761.getPnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Dd());               //Natural: MOVE #FIRST-VERIFIED-DD TO #FIRST-VERIFIED-DTE-MDCY-DD
        ldaAdsl761.getPnd_Last_Updated_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Rqst_Last_Updtd_Dte(),new ReportEditMask("YYYYMMDD"));                //Natural: MOVE EDITED ADP-RQST-LAST-UPDTD-DTE ( EM = YYYYMMDD ) TO #LAST-UPDATED-YYYYMMDD
        ldaAdsl761.getPnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Cc().setValue(ldaAdsl761.getPnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Cc());                       //Natural: MOVE #LAST-UPDATED-CC TO #LAST-UPDATED-DTE-MDCY-CC
        ldaAdsl761.getPnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Yy().setValue(ldaAdsl761.getPnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Yy());                       //Natural: MOVE #LAST-UPDATED-YY TO #LAST-UPDATED-DTE-MDCY-YY
        ldaAdsl761.getPnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Mm());                       //Natural: MOVE #LAST-UPDATED-MM TO #LAST-UPDATED-DTE-MDCY-MM
        ldaAdsl761.getPnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Dd());                       //Natural: MOVE #LAST-UPDATED-DD TO #LAST-UPDATED-DTE-MDCY-DD
        ldaAdsl761.getPnd_Last_Verified_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Rqst_Last_Vrfd_Dte(),new ReportEditMask("YYYYMMDD"));                //Natural: MOVE EDITED ADP-RQST-LAST-VRFD-DTE ( EM = YYYYMMDD ) TO #LAST-VERIFIED-YYYYMMDD
        ldaAdsl761.getPnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Cc().setValue(ldaAdsl761.getPnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Cc());                   //Natural: MOVE #LAST-VERIFIED-CC TO #LAST-VERIFIED-DTE-MDCY-CC
        ldaAdsl761.getPnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Yy().setValue(ldaAdsl761.getPnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Yy());                   //Natural: MOVE #LAST-VERIFIED-YY TO #LAST-VERIFIED-DTE-MDCY-YY
        ldaAdsl761.getPnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Mm());                   //Natural: MOVE #LAST-VERIFIED-MM TO #LAST-VERIFIED-DTE-MDCY-MM
        ldaAdsl761.getPnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Dd());                   //Natural: MOVE #LAST-VERIFIED-DD TO #LAST-VERIFIED-DTE-MDCY-DD
        ldaAdsl763.getPnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Verified_By().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Rqst_Last_Vrfd_Id());                  //Natural: MOVE ADP-RQST-LAST-VRFD-ID TO #CREF-RQST-LAST-VERIFIED-BY
        ldaAdsl763.getPnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Updated_By().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Rqst_Last_Updtd_Id());                  //Natural: MOVE ADP-RQST-LAST-UPDTD-ID TO #CREF-RQST-LAST-UPDATED-BY
        ldaAdsl763.getPnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Verified_By().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Rqst_1st_Vrfd_Id());                  //Natural: MOVE ADP-RQST-1ST-VRFD-ID TO #CREF-RQST-FIRST-VERIFIED-BY
        ldaAdsl763.getPnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Entered_By().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Entry_User_Id());                      //Natural: MOVE ADP-ENTRY-USER-ID TO #CREF-RQST-FIRST-ENTERED-BY
    }
    private void sub_Build_Tiaa_Da_Rslt_Header_Data() throws Exception                                                                                                    //Natural: BUILD-TIAA-DA-RSLT-HEADER-DATA
    {
        if (BLNatReinput.isReinput()) return;

        ldaAdsl761.getPnd_Tiaa_Contract_Nbr().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr());                                                                    //Natural: MOVE ADC-TIAA-NBR TO #TIAA-CONTRACT-NBR
        ldaAdsl761.getPnd_Account_Dte_Yyyymmdd().setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                      //Natural: MOVE EDITED ADP-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #ACCOUNT-DTE-YYYYMMDD
        ldaAdsl761.getPnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Mm().setValue(ldaAdsl761.getPnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Mm());                               //Natural: MOVE #ACCOUNT-DTE-MM TO #ACCOUNT-DTE-MDCY-MM
        ldaAdsl761.getPnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Dd().setValue(ldaAdsl761.getPnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Dd());                               //Natural: MOVE #ACCOUNT-DTE-DD TO #ACCOUNT-DTE-MDCY-DD
        ldaAdsl761.getPnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Yyyy().setValue(ldaAdsl761.getPnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Yyyy());                           //Natural: MOVE #ACCOUNT-DTE-YYYY TO #ACCOUNT-DTE-MDCY-YYYY
        ldaAdsl761.getPnd_Target_All_Pnd_Target_Number().getValue("*").setValue(pnd_Nac_Ppg_Code.getValue("*"));                                                          //Natural: MOVE #NAC-PPG-CODE ( * ) TO #TARGET-NUMBER ( * )
        ldaAdsl761.getPnd_Target_All_Pnd_Target_Disc().getValue("*").setValue(pnd_Nac_Ppg_Ind.getValue("*"));                                                             //Natural: MOVE #NAC-PPG-IND ( * ) TO #TARGET-DISC ( * )
        FOR02:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            //*  RS0
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_B_Indx).equals("T") || ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_B_Indx).equals("Y"))) //Natural: IF ADC-ACCT-CDE ( #B-INDX ) = 'T' OR ADC-ACCT-CDE ( #B-INDX ) = 'Y'
            {
                ldaAdsl760.getPnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Settle_Amount().setValue(pnd_Tiaa_Stlmnt_Amt);                                               //Natural: MOVE #TIAA-STLMNT-AMT TO #TIAA-HEADER-SETTLE-AMOUNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaAdsl761.getPnd_Rtb().setValue("N");                                                                                                                            //Natural: MOVE 'N' TO #RTB
        ldaAdsl761.getPnd_Rtb_Settle_Dte_Mdcy().reset();                                                                                                                  //Natural: RESET #RTB-SETTLE-DTE-MDCY
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR03:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO 10
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(10)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(pnd_Tiaa_Nbr.getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                                                                    //Natural: IF #TIAA-NBR ( #B-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl761.getPnd_Tiaa_Cert_Nbr().getValue(pnd_All_Indx_Pnd_B_Indx).setValueEdited(pnd_Tiaa_Nbr.getValue(pnd_All_Indx_Pnd_B_Indx),new ReportEditMask("XXXXXXX-XXX")); //Natural: MOVE EDITED #TIAA-NBR ( #B-INDX ) ( EM = XXXXXXX-XXX ) TO #TIAA-CERT-NBR ( #B-INDX )
            ldaAdsl761.getPnd_Tiaa_Cert_Ind().getValue(pnd_All_Indx_Pnd_B_Indx).setValue(pnd_Tiaa_Ind.getValue(pnd_All_Indx_Pnd_B_Indx));                                 //Natural: MOVE #TIAA-IND ( #B-INDX ) TO #TIAA-CERT-IND ( #B-INDX )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Tiaa_Participant_Header_Data() throws Exception                                                                                                //Natural: WRITE-TIAA-PARTICIPANT-HEADER-DATA
    {
        if (BLNatReinput.isReinput()) return;

        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm761.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM761'
        if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                                //Natural: IF #ROTH-INDICATOR-ON
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm761.class));                                                                           //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM761'
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Tiaa_Da_Rate_Basis_Amt() throws Exception                                                                                                      //Natural: WRITE-TIAA-DA-RATE-BASIS-AMT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        pnd_New_Header_Page_1.setValue(false);                                                                                                                            //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-1
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm763.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM763'
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm774.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM774'
        if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                                //Natural: IF #ROTH-INDICATOR-ON
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm763.class));                                                                           //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM763'
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm774.class));                                                                           //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM774'
        }                                                                                                                                                                 //Natural: END-IF
        FOR04:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                                   //Natural: IF ADC-ACCT-CDE ( #B-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  RS0
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_B_Indx).equals("T") || ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_B_Indx).equals("Y"))) //Natural: IF ADC-ACCT-CDE ( #B-INDX ) = 'T' OR ADC-ACCT-CDE ( #B-INDX ) = 'Y'
            {
                ldaAdsl760.getPnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Amt().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Qty().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE ADC-ACCT-QTY ( #B-INDX ) TO #TIAA-HEADER-SETTLE-AMT
                ldaAdsl760.getPnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Typ().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Typ().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE ADC-ACCT-TYP ( #B-INDX ) TO #TIAA-HEADER-SETTLE-TYP
                ldaAdsl760.getPnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Amt().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Qty().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE ADC-GRD-MNTHLY-QTY ( #B-INDX ) TO #TIAA-HEADER-GRD-AMT
                ldaAdsl760.getPnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Typ().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Typ().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE ADC-GRD-MNTHLY-TYP ( #B-INDX ) TO #TIAA-HEADER-GRD-TYP
                //*    EXAMINE #PEC-ACCT-NAME-1 (*) FOR            /* EM - 102213 START
                //*      ADC-ACCT-CDE (#B-INDX)
                //*  EM - 102213 END
                DbsUtil.examine(new ExamineSource(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Ticker().getValue("*")), new ExamineSearch(ldaAdsl401a.getAds_Cntrct_View_Adc_Tkr_Symbl().getValue(pnd_All_Indx_Pnd_B_Indx)),  //Natural: EXAMINE #ACCT-TICKER ( * ) FOR ADC-TKR-SYMBL ( #B-INDX ) GIVING INDEX #PROD-INDX
                    new ExamineGivingIndex(pnd_All_Indx_Pnd_Prod_Indx));
                ldaAdsl760.getPnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Account().setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Name_4().getValue(pnd_All_Indx_Pnd_Prod_Indx)); //Natural: MOVE #ACCT-NAME-4 ( #PROD-INDX ) TO #TIAA-HEADER-ACCOUNT
                //*  RS0
                ldaAdsl760.getPnd_Tiaa_Graded_Totals_Pnd_Tiaa_Prod_Grd().setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Name_1().getValue(pnd_All_Indx_Pnd_Prod_Indx));    //Natural: MOVE #ACCT-NAME-1 ( #PROD-INDX ) TO #TIAA-PROD-GRD
                //*  RS0
                ldaAdsl760.getPnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Prod_Std().setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Name_1().getValue(pnd_All_Indx_Pnd_Prod_Indx));  //Natural: MOVE #ACCT-NAME-1 ( #PROD-INDX ) TO #TIAA-PROD-STD
                getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm760.class));                                                                       //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM760'
                if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                        //Natural: IF #ROTH-INDICATOR-ON
                {
                    getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm760.class));                                                                   //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM760'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        pnd_All_Indx_Pnd_Tiaa_Indx_1.reset();                                                                                                                             //Natural: RESET #TIAA-INDX-1
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm778.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM778'
        if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                                //Natural: IF #ROTH-INDICATOR-ON
        {
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm778.class));                                                                           //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM778'
            //*  022208
        }                                                                                                                                                                 //Natural: END-IF
        FOR05:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #MAX-RATES
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rates)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                          //Natural: IF ADC-DTL-TIAA-RATE-CDE ( #B-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl760.getPnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Rate().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_All_Indx_Pnd_B_Indx));    //Natural: MOVE ADC-DTL-TIAA-RATE-CDE ( #B-INDX ) TO #TIAA-DA-RATE
            ldaAdsl760.getPnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Accum_Amtfslash_Units().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Opn_Accum_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE ADC-DTL-TIAA-OPN-ACCUM-AMT ( #B-INDX ) TO #TIAA-DA-ACCUM-AMT/UNITS
            ldaAdsl760.getPnd_Tiaa_Sub_Totals_Pnd_Tiaa_Da_Accum_Sub_Total().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Opn_Accum_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-OPN-ACCUM-AMT ( #B-INDX ) TO #TIAA-DA-ACCUM-SUB-TOTAL
            ldaAdsl760.getPnd_Tiaa_Final_Totals_Pnd_Tiaa_Da_Accum_Final_Total().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Opn_Accum_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-OPN-ACCUM-AMT ( #B-INDX ) TO #TIAA-DA-ACCUM-FINAL-TOTAL
            //*  06/22/04 ADDED BY C. AVE
            ldaAdsl760.getPnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Rtb_Sub_Total().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-ACTL-AMT ( #B-INDX ) TO #TIAA-SETTLED-RTB-SUB-TOTAL
            ldaAdsl760.getPnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Amtfslash_Units().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE ADC-DTL-TIAA-ACTL-AMT ( #B-INDX ) TO #TIAA-SETTLED-AMT/UNITS
            ldaAdsl760.getPnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Sub_Total().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-ACTL-AMT ( #B-INDX ) TO #TIAA-SETTLED-SUB-TOTAL
            ldaAdsl760.getPnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Final_Total().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-ACTL-AMT ( #B-INDX ) TO #TIAA-SETTLED-FINAL-TOTAL
            ldaAdsl760.getPnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Rtb_Amtfslash_Unit().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ASSIGN #TIAA-SETTLED-RTB-AMT/UNIT := ADC-DTL-TIAA-ACTL-AMT ( #B-INDX )
            pnd_New_Header_Page_1.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #NEW-HEADER-PAGE-1
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm775.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM775'
            //*  GG050608
            if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                            //Natural: IF #ROTH-INDICATOR-ON
            {
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm775.class));                                                                       //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM775'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Amount.setValue(ldaAdsl760.getPnd_Tiaa_Sub_Totals_Pnd_Tiaa_Da_Accum_Sub_Total());                                                                             //Natural: ASSIGN #AMOUNT := #TIAA-SUB-TOTALS.#TIAA-DA-ACCUM-SUB-TOTAL
                                                                                                                                                                          //Natural: PERFORM PUT-ASTERISK-BESIDE-TOTAL
        sub_Put_Asterisk_Beside_Total();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm764.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM764'
        //*  GG050608
        if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                                //Natural: IF #ROTH-INDICATOR-ON
        {
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm764.class));                                                                           //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM764'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_New_Header_Page_1.setValue(false);                                                                                                                            //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-1
    }
    private void sub_Sequence_Total_Settlement_For_All_Tiaa_Contracts() throws Exception                                                                                  //Natural: SEQUENCE-TOTAL-SETTLEMENT-FOR-ALL-TIAA-CONTRACTS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        ldaAdsl761.getPnd_Tiaa_Contract_Nbr().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr());                                                                    //Natural: MOVE ADC-TIAA-NBR TO #TIAA-CONTRACT-NBR
        FOR06:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO 120
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(120)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                          //Natural: IF ADC-DTL-TIAA-RATE-CDE ( #B-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_All_Indx_Pnd_Prod_Indx.reset();                                                                                                                           //Natural: RESET #PROD-INDX
            DbsUtil.examine(new ExamineSource(ldaAdsl760.getPnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Rate().getValue("*")), new ExamineSearch(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_All_Indx_Pnd_B_Indx)),  //Natural: EXAMINE #T-SUB-DA-RATE ( * ) FOR ADC-DTL-TIAA-RATE-CDE ( #B-INDX ) GIVING INDEX #PROD-INDX
                new ExamineGivingIndex(pnd_All_Indx_Pnd_Prod_Indx));
            if (condition(pnd_All_Indx_Pnd_Prod_Indx.equals(getZero())))                                                                                                  //Natural: IF #PROD-INDX = 0
            {
                pnd_All_Indx_Pnd_Tiaa_Indx_2.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TIAA-INDX-2
                ldaAdsl760.getPnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Rate().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_2).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE ADC-DTL-TIAA-RATE-CDE ( #B-INDX ) TO #T-SUB-DA-RATE ( #TIAA-INDX-2 )
                ldaAdsl760.getPnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Accum_Amtfslash_Units().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_2).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Opn_Accum_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-OPN-ACCUM-AMT ( #B-INDX ) TO #T-SUB-DA-ACCUM-AMT/UNITS ( #TIAA-INDX-2 )
                ldaAdsl760.getPnd_T_Save_Sub_Totals_Pnd_T_Sub_Settled_Amtfslash_Units().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_2).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-ACTL-AMT ( #B-INDX ) TO #T-SUB-SETTLED-AMT/UNITS ( #TIAA-INDX-2 )
                ldaAdsl760.getPnd_T_Save_Sub_Totals_Pnd_T_Sub_Settled_Rtb_Amtfslash_Units().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_2).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ASSIGN #T-SUB-SETTLED-RTB-AMT/UNITS ( #TIAA-INDX-2 ) := ADC-DTL-TIAA-ACTL-AMT ( #B-INDX )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl760.getPnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Rate().getValue(pnd_All_Indx_Pnd_Prod_Indx).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE ADC-DTL-TIAA-RATE-CDE ( #B-INDX ) TO #T-SUB-DA-RATE ( #PROD-INDX )
                ldaAdsl760.getPnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Accum_Amtfslash_Units().getValue(pnd_All_Indx_Pnd_Prod_Indx).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Opn_Accum_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-OPN-ACCUM-AMT ( #B-INDX ) TO #T-SUB-DA-ACCUM-AMT/UNITS ( #PROD-INDX )
                ldaAdsl760.getPnd_T_Save_Sub_Totals_Pnd_T_Sub_Settled_Amtfslash_Units().getValue(pnd_All_Indx_Pnd_Prod_Indx).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-ACTL-AMT ( #B-INDX ) TO #T-SUB-SETTLED-AMT/UNITS ( #PROD-INDX )
                ldaAdsl760.getPnd_T_Sub_Settled_Rtb_Amounts().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx));         //Natural: ASSIGN #T-SUB-SETTLED-RTB-AMOUNTS := ADC-DTL-TIAA-ACTL-AMT ( #B-INDX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Calculate_Tiaa_Ia_Rslt_Accumulations() throws Exception                                                                                              //Natural: CALCULATE-TIAA-IA-RSLT-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_All_Indx_Pnd_A_Indx.reset();                                                                                                                                  //Natural: RESET #A-INDX #B-INDX #PROD-INDX
        pnd_All_Indx_Pnd_B_Indx.reset();
        pnd_All_Indx_Pnd_Prod_Indx.reset();
        if (condition(pnd_Tiaa_Prod_Code.getBoolean()))                                                                                                                   //Natural: IF #TIAA-PROD-CODE
        {
            ldaAdsl761.getPnd_Tiaa_Contract_Nbr().setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr());                                                                //Natural: MOVE ADC-TIAA-NBR TO #TIAA-CONTRACT-NBR
                                                                                                                                                                          //Natural: PERFORM SEQUENCE-AND-WRITE-SETTLEMENT-FOR-STANDARD-METHOD
            sub_Sequence_And_Write_Settlement_For_Standard_Method();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALCULATE-WRITE-SETTLEMENT-FOR-GRADED-METHOD
            sub_Calculate_Write_Settlement_For_Graded_Method();
            if (condition(Global.isEscape())) {return;}
            gdaAdsg870.getAdsg870_Pnd_Nai_Rslt_Rqst_Id().setValue(ldaAdsl450a.getAds_Ia_Rslt_Rqst_Id());                                                                  //Natural: MOVE ADS-IA-RSLT.RQST-ID TO #NAI-RSLT-RQST-ID
            DbsUtil.invokeMain(DbsUtil.getBlType("ADSP761"), getCurrentProcessState());                                                                                   //Natural: FETCH RETURN 'ADSP761'
            if (condition(Global.isEscape())) return;
            pnd_Tiaa_Prod_Code.setValue(false);                                                                                                                           //Natural: MOVE FALSE TO #TIAA-PROD-CODE
            pnd_Tiaa_Record.setValue(true);                                                                                                                               //Natural: MOVE TRUE TO #TIAA-RECORD
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Sequence_And_Write_Settlement_For_Standard_Method() throws Exception                                                                                 //Natural: SEQUENCE-AND-WRITE-SETTLEMENT-FOR-STANDARD-METHOD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_New_Header_Page_3.setValue(true);                                                                                                                             //Natural: MOVE TRUE TO #NEW-HEADER-PAGE-3
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                                //Natural: IF #ROTH-INDICATOR-ON
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        pnd_All_Indx_Pnd_Tiaa_Indx_3.reset();                                                                                                                             //Natural: RESET #TIAA-INDX-3
        //* *******************************************************
        //*  THIS PARA WILL SEQUENCE SETTLEMENT FOR STANDARD METHOD
        //* ********************************************************
        //*  R #B-INDX 1  TO 60
        //* *R #B-INDX 1  TO 80                                  /* 022208
        //*  022208
        //*  022208      /* EM - 030612
        FOR1:                                                                                                                                                             //Natural: FOR #B-INDX 1 TO #MAX-RATES
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rates)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                        //Natural: IF #ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) = ' '
            {
                //*    ESCAPE BOTTOM (4650)                              /* 022208
                //*  022208
                if (true) break FOR1;                                                                                                                                     //Natural: ESCAPE BOTTOM ( FOR1. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tiaa_Rec_Found.setValue(false);                                                                                                                           //Natural: MOVE FALSE TO #TIAA-REC-FOUND
            pnd_Tiaa_Rec_Not_Found.setValue(false);                                                                                                                       //Natural: MOVE FALSE TO #TIAA-REC-NOT-FOUND
            pnd_All_Indx_Pnd_C_Indx.reset();                                                                                                                              //Natural: RESET #C-INDX
            //*  022208
            //*  022208      /* EM - 030612
            FOR2:                                                                                                                                                         //Natural: FOR #C-INDX 1 TO #MAX-RATES
            for (pnd_All_Indx_Pnd_C_Indx.setValue(1); condition(pnd_All_Indx_Pnd_C_Indx.lessOrEqual(pnd_Max_Rates)); pnd_All_Indx_Pnd_C_Indx.nadd(1))
            {
                if (condition(ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Std_Payment_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).equals(" ")))           //Natural: IF #TIAA-IA-RATE-STD-PAYMENT-AMT ( #C-INDX ) = ' '
                {
                    pnd_Tiaa_Rec_Not_Found.setValue(true);                                                                                                                //Natural: MOVE TRUE TO #TIAA-REC-NOT-FOUND
                    //*      ESCAPE BOTTOM (4715)                            /* 022208
                    //*  022208
                    if (true) break FOR2;                                                                                                                                 //Natural: ESCAPE BOTTOM ( FOR2. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Std_Payment_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).equals(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx))  //Natural: IF #TIAA-IA-RATE-STD-PAYMENT-AMT ( #C-INDX ) = #ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) AND #TIAA-DA-RATE-STD-PAYMENT-AMT ( #C-INDX ) = #ADI-DTL-TIAA-DA-RATE-CD ( #B-INDX )
                    && ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Da_Rate_Std_Payment_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).equals(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx))))
                {
                    pnd_Tiaa_Rec_Found.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #TIAA-REC-FOUND
                    //*      ESCAPE BOTTOM (4715)                            /* 022208
                    //*  022208
                    if (true) break FOR2;                                                                                                                                 //Natural: ESCAPE BOTTOM ( FOR2. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FOR1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FOR1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ????
            if (condition(pnd_Tiaa_Rec_Found.getBoolean()))                                                                                                               //Natural: IF #TIAA-REC-FOUND
            {
                ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Std_Payment_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).setValue(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) TO #TIAA-IA-RATE-STD-PAYMENT-AMT ( #C-INDX )
                ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Da_Rate_Std_Payment_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).setValue(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #ADI-DTL-TIAA-DA-RATE-CD ( #B-INDX ) TO #TIAA-DA-RATE-STD-PAYMENT-AMT ( #C-INDX )
                ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Da_Payment_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-DA-STD-AMT ( #B-INDX ) TO #TIAA-IA-STD-DA-PAYMENT-AMT ( #C-INDX )
                ldaAdsl760.getPnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Settled_Std_Sub_Total().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-DA-STD-AMT ( #B-INDX ) TO #TIAA-SETTLED-STD-SUB-TOTAL
                ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Gur_Payment_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GUR-PAYMENT-AMT ( #C-INDX )
                ldaAdsl760.getPnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Sub_Total().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GURANTEED-SUB-TOTAL
                ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Div_Payment_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIV-PAYMENT-AMT ( #C-INDX )
                ldaAdsl760.getPnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Dividend_Sub_Total().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIVIDEND-SUB-TOTAL
                //* * 08222005 MN START
                ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Guar_Commut_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Std_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIA-GUAR-COMMUT-STD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GUAR-COMMUT-AMT ( #B-INDX )
                ldaAdsl760.getPnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Guar_Commut_Sub_Tot().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Std_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIA-GUAR-COMMUT-STD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GUAR-COMMUT-SUB-TOT
                //* * 08222005 MN END
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Tiaa_Rec_Not_Found.getBoolean()))                                                                                                       //Natural: IF #TIAA-REC-NOT-FOUND
                {
                    pnd_All_Indx_Pnd_Tiaa_Indx_3.nadd(1);                                                                                                                 //Natural: ADD 1 TO #TIAA-INDX-3
                    ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Std_Payment_Amt().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_3).setValue(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) TO #TIAA-IA-RATE-STD-PAYMENT-AMT ( #TIAA-INDX-3 )
                    ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Da_Rate_Std_Payment_Amt().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_3).setValue(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #ADI-DTL-TIAA-DA-RATE-CD ( #B-INDX ) TO #TIAA-DA-RATE-STD-PAYMENT-AMT ( #TIAA-INDX-3 )
                    ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Da_Payment_Amt().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_3).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-DA-STD-AMT ( #B-INDX ) TO #TIAA-IA-STD-DA-PAYMENT-AMT ( #TIAA-INDX-3 )
                    ldaAdsl760.getPnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Settled_Std_Sub_Total().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-DA-STD-AMT ( #B-INDX ) TO #TIAA-SETTLED-STD-SUB-TOTAL
                    ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Gur_Payment_Amt().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_3).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GUR-PAYMENT-AMT ( #TIAA-INDX-3 )
                    ldaAdsl760.getPnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Sub_Total().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GURANTEED-SUB-TOTAL
                    ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Div_Payment_Amt().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_3).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIV-PAYMENT-AMT ( #TIAA-INDX-3 )
                    ldaAdsl760.getPnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Dividend_Sub_Total().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIVIDEND-SUB-TOTAL
                    //* * 08222005 MN START
                    ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Guar_Commut_Amt().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_3).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Std_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIA-GUAR-COMMUT-STD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GUAR-COMMUT-AMT ( #TIAA-INDX-3 )
                    ldaAdsl760.getPnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Guar_Commut_Sub_Tot().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Std_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIA-GUAR-COMMUT-STD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GUAR-COMMUT-SUB-TOT
                    //* * 08222005 MN END
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *******************************************************
        //*  THIS PARA WILL WRITE  SETTLEMENT FOR STANDARD METHOD
        //* ********************************************************
        //*  022208
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR07:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #MAX-RATES
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rates)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Std_Payment_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))               //Natural: IF #TIAA-IA-RATE-STD-PAYMENT-AMT ( #B-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  MOVE #PEC-ACCT-NAME-1 (1) TO #TIAA-PROD-STD    /* RS0
            ldaAdsl760.getPnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Da_Rate_Std().setValue(ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Da_Rate_Std_Payment_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-DA-RATE-STD-PAYMENT-AMT ( #B-INDX ) TO #TIAA-DA-RATE-STD
            ldaAdsl760.getPnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Rate_Std().setValue(ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Std_Payment_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-RATE-STD-PAYMENT-AMT ( #B-INDX ) TO #TIAA-IA-RATE-STD
            ldaAdsl760.getPnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Settled_Std_Amt().setValue(ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Da_Payment_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-STD-DA-PAYMENT-AMT ( #B-INDX ) TO #TIAA-SETTLED-STD-AMT
            ldaAdsl760.getPnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Amt().setValue(ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Gur_Payment_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-STD-GUR-PAYMENT-AMT ( #B-INDX ) TO #TIAA-IA-STD-GURANTEED-AMT
            ldaAdsl760.getPnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Dividend_Amt().setValue(ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Div_Payment_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-STD-DIV-PAYMENT-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIVIDEND-AMT
            //* * 08222005 MN START
            //*  REPORT AMOUNT
            ldaAdsl760.getPnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Guar_Commut_Rep_Amt().setValue(ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Guar_Commut_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-STD-GUAR-COMMUT-AMT ( #B-INDX ) TO #TIAA-IA-STD-GUAR-COMMUT-REP-AMT
            //* * 08222005 MN END
            pnd_New_Header_Page_3.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #NEW-HEADER-PAGE-3
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm766.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM766'
            if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                            //Natural: IF #ROTH-INDICATOR-ON
            {
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm766.class));                                                                       //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM766'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* **********************************************************************
        //*  THIS PARA WILL SEQUENCE IA PAYMENTS/RENEWAL INFOR FOR STANDARD METHOD
        //* ***********************************************************************
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        //*  R #B-INDX 1  TO 60
        //* *R #B-INDX 1  TO 80                                  /* 022208
        //*  022208
        //*  022208      /* EM - 030612
        FOR3:                                                                                                                                                             //Natural: FOR #B-INDX 1 TO #MAX-RATES
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rates)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                        //Natural: IF #ADI-DTL-TIAA-DA-RATE-CD ( #B-INDX ) = ' '
            {
                //* *  ESCAPE BOTTOM (5255)                              /* 022208
                //*  022208
                if (true) break FOR3;                                                                                                                                     //Natural: ESCAPE BOTTOM ( FOR3. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_All_Indx_Pnd_Prod_Indx.reset();                                                                                                                           //Natural: RESET #PROD-INDX
            DbsUtil.examine(new ExamineSource(ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Std_Payment_Save().getValue("*")), new                     //Natural: EXAMINE #TIAA-IA-RATE-STD-PAYMENT-SAVE ( * ) FOR #ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) GIVING INDEX #PROD-INDX
                ExamineSearch(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx)), new ExamineGivingIndex(pnd_All_Indx_Pnd_Prod_Indx));
            if (condition(pnd_All_Indx_Pnd_Prod_Indx.equals(getZero())))                                                                                                  //Natural: IF #PROD-INDX = 0
            {
                pnd_All_Indx_Pnd_Tiaa_Indx_4.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TIAA-INDX-4
                ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Std_Payment_Save().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_4).setValue(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) TO #TIAA-IA-RATE-STD-PAYMENT-SAVE ( #TIAA-INDX-4 )
                ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Gur_Payment_Save().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_4).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GUR-PAYMENT-SAVE ( #TIAA-INDX-4 )
                ldaAdsl760.getPnd_Tiaa_Ia_Std_Guranteed_Final().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx));      //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GURANTEED-FINAL
                ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Div_Payment_Save().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_4).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIV-PAYMENT-SAVE ( #TIAA-INDX-4 )
                ldaAdsl760.getPnd_Tiaa_Ia_Std_Dividend_Final().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx));       //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIVIDEND-FINAL
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Std_Payment_Save().getValue(pnd_All_Indx_Pnd_Prod_Indx).setValue(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) TO #TIAA-IA-RATE-STD-PAYMENT-SAVE ( #PROD-INDX )
                ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Gur_Payment_Save().getValue(pnd_All_Indx_Pnd_Prod_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GUR-PAYMENT-SAVE ( #PROD-INDX )
                ldaAdsl760.getPnd_Tiaa_Ia_Std_Guranteed_Final().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx));      //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-STD-GURANTEED-FINAL
                ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Div_Payment_Save().getValue(pnd_All_Indx_Pnd_Prod_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIV-PAYMENT-SAVE ( #PROD-INDX )
                ldaAdsl760.getPnd_Tiaa_Ia_Std_Dividend_Final().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx));       //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-STD-DIVIDEND-FINAL
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_All_Indx_Pnd_B_Indx.equals(1)))                                                                                                                 //Natural: IF #B-INDX = 1
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm767.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM767'
            if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                            //Natural: IF #ROTH-INDICATOR-ON
            {
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm767.class));                                                                       //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM767'
            }                                                                                                                                                             //Natural: END-IF
            pnd_New_Header_Page_3.setValue(false);                                                                                                                        //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-3
        }                                                                                                                                                                 //Natural: END-IF
        pnd_New_Header_Page_3.setValue(false);                                                                                                                            //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-3
    }
    private void sub_Calculate_Write_Settlement_For_Graded_Method() throws Exception                                                                                      //Natural: CALCULATE-WRITE-SETTLEMENT-FOR-GRADED-METHOD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_New_Header_Page_4.setValue(true);                                                                                                                             //Natural: MOVE TRUE TO #NEW-HEADER-PAGE-4
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                                //Natural: IF #ROTH-INDICATOR-ON
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*    THIS PARA WILL SEQUENCE THE IA GRADED METHOD
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        pnd_All_Indx_Pnd_Tiaa_Indx_6.reset();                                                                                                                             //Natural: RESET #TIAA-INDX-6
        //*  022208
        //*  022208      /* EM - 030612
        FOR4:                                                                                                                                                             //Natural: FOR #B-INDX 1 TO #MAX-RATES
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rates)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ") && ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).equals(getZero()))) //Natural: IF #ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) = ' ' AND #ADI-DTL-TIAA-DA-GRD-AMT ( #B-INDX ) = 0
            {
                //*  022208
                if (true) break FOR4;                                                                                                                                     //Natural: ESCAPE BOTTOM ( FOR4. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tiaa_Rec_Found.setValue(false);                                                                                                                           //Natural: MOVE FALSE TO #TIAA-REC-FOUND
            pnd_Tiaa_Rec_Not_Found.setValue(false);                                                                                                                       //Natural: MOVE FALSE TO #TIAA-REC-NOT-FOUND
            pnd_All_Indx_Pnd_C_Indx.reset();                                                                                                                              //Natural: RESET #C-INDX
            //*  022208
            //*  022208      /* EM - 030612
            FOR5:                                                                                                                                                         //Natural: FOR #C-INDX 1 TO #MAX-RATES
            for (pnd_All_Indx_Pnd_C_Indx.setValue(1); condition(pnd_All_Indx_Pnd_C_Indx.lessOrEqual(pnd_Max_Rates)); pnd_All_Indx_Pnd_C_Indx.nadd(1))
            {
                if (condition(ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Grd_Payment_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).equals(" ")))           //Natural: IF #TIAA-IA-RATE-GRD-PAYMENT-AMT ( #C-INDX ) = ' '
                {
                    pnd_Tiaa_Rec_Not_Found.setValue(true);                                                                                                                //Natural: MOVE TRUE TO #TIAA-REC-NOT-FOUND
                    //*  022208
                    if (true) break FOR5;                                                                                                                                 //Natural: ESCAPE BOTTOM ( FOR5. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Grd_Payment_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).equals(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx))  //Natural: IF #TIAA-IA-RATE-GRD-PAYMENT-AMT ( #C-INDX ) = #ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) AND #TIAA-DA-RATE-GRD-PAYMENT-AMT ( #C-INDX ) = #ADI-DTL-TIAA-DA-RATE-CD ( #B-INDX )
                    && ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Da_Rate_Grd_Payment_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).equals(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx))))
                {
                    pnd_Tiaa_Rec_Found.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #TIAA-REC-FOUND
                    //*  022208
                    if (true) break FOR5;                                                                                                                                 //Natural: ESCAPE BOTTOM ( FOR5. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FOR4"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FOR4"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Tiaa_Rec_Found.getBoolean()))                                                                                                               //Natural: IF #TIAA-REC-FOUND
            {
                ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Grd_Payment_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).setValue(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) TO #TIAA-IA-RATE-GRD-PAYMENT-AMT ( #C-INDX )
                ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Da_Rate_Grd_Payment_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).setValue(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #ADI-DTL-TIAA-DA-RATE-CD ( #B-INDX ) TO #TIAA-DA-RATE-GRD-PAYMENT-AMT ( #C-INDX )
                ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Accum_Payment_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-DA-GRD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-ACCUM-PAYMENT-AMT ( #C-INDX )
                ldaAdsl760.getPnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Settled_Grd_Sub_Total().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-DA-GRD-AMT ( #B-INDX ) TO #TIAA-SETTLED-GRD-SUB-TOTAL
                ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Gur_Payment_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GUR-PAYMENT-AMT ( #C-INDX )
                ldaAdsl760.getPnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Sub_Total().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GURANTEED-SUB-TOTAL
                ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Div_Payment_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIV-PAYMENT-AMT ( #C-INDX )
                ldaAdsl760.getPnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Sub_Total().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIVIDEND-SUB-TOTAL
                //* * 08222005 MN START
                ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Guar_Commut_Amt().getValue(pnd_All_Indx_Pnd_C_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Grd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIA-GUAR-COMMUT-GRD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GUAR-COMMUT-AMT ( #C-INDX )
                ldaAdsl760.getPnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Ia_Grd_Guar_Commut_Sub_Tot().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Grd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIA-GUAR-COMMUT-GRD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GUAR-COMMUT-SUB-TOT
                //* * 08222005 MN END
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Tiaa_Rec_Not_Found.getBoolean()))                                                                                                       //Natural: IF #TIAA-REC-NOT-FOUND
                {
                    pnd_All_Indx_Pnd_Tiaa_Indx_6.nadd(1);                                                                                                                 //Natural: ADD 1 TO #TIAA-INDX-6
                    ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Grd_Payment_Amt().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_6).setValue(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) TO #TIAA-IA-RATE-GRD-PAYMENT-AMT ( #TIAA-INDX-6 )
                    ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Da_Rate_Grd_Payment_Amt().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_6).setValue(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #ADI-DTL-TIAA-DA-RATE-CD ( #B-INDX ) TO #TIAA-DA-RATE-GRD-PAYMENT-AMT ( #TIAA-INDX-6 )
                    ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Accum_Payment_Amt().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_6).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-DA-GRD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-ACCUM-PAYMENT-AMT ( #TIAA-INDX-6 )
                    ldaAdsl760.getPnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Settled_Grd_Sub_Total().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-DA-GRD-AMT ( #B-INDX ) TO #TIAA-SETTLED-GRD-SUB-TOTAL
                    ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Gur_Payment_Amt().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_6).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GUR-PAYMENT-AMT ( #TIAA-INDX-6 )
                    ldaAdsl760.getPnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Sub_Total().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GURANTEED-SUB-TOTAL
                    ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Div_Payment_Amt().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_6).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIV-PAYMENT-AMT ( #TIAA-INDX-6 )
                    ldaAdsl760.getPnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Sub_Total().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIVIDEND-SUB-TOTAL
                    //* * 08222005 MN START
                    ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Guar_Commut_Amt().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_6).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Grd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIA-GUAR-COMMUT-GRD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GUAR-COMMUT-AMT ( #TIAA-INDX-6 )
                    ldaAdsl760.getPnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Ia_Grd_Guar_Commut_Sub_Tot().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Grd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIA-GUAR-COMMUT-GRD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GUAR-COMMUT-SUB-TOT
                    //* * 08222005 MN END
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ****************************************************************
        //*   THIS PARA WILL WRITE GRADED METHOD SETTLEMENT
        //* ****************************************************************
        //*  022208
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR08:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #MAX-RATES
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rates)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Grd_Payment_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))               //Natural: IF #TIAA-IA-RATE-GRD-PAYMENT-AMT ( #B-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  MOVE #PEC-ACCT-NAME-1 (1) TO #TIAA-PROD-GRD    /* RS0
            ldaAdsl760.getPnd_Tiaa_Graded_Totals_Pnd_Tiaa_Da_Rate_Grd().setValue(ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Da_Rate_Grd_Payment_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-DA-RATE-GRD-PAYMENT-AMT ( #B-INDX ) TO #TIAA-DA-RATE-GRD
            ldaAdsl760.getPnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Rate_Grd().setValue(ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Grd_Payment_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-RATE-GRD-PAYMENT-AMT ( #B-INDX ) TO #TIAA-IA-RATE-GRD
            ldaAdsl760.getPnd_Tiaa_Graded_Totals_Pnd_Tiaa_Settled_Grd_Amt().setValue(ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Accum_Payment_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-GRD-ACCUM-PAYMENT-AMT ( #B-INDX ) TO #TIAA-SETTLED-GRD-AMT
            ldaAdsl760.getPnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Amt().setValue(ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Gur_Payment_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-GRD-GUR-PAYMENT-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GURANTEED-AMT
            ldaAdsl760.getPnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Amt().setValue(ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Div_Payment_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-GRD-DIV-PAYMENT-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIVIDEND-AMT
            //* * 08222005 MN START
            ldaAdsl760.getPnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Guar_Commut_Rep_Amt().setValue(ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Guar_Commut_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-GRD-GUAR-COMMUT-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GUAR-COMMUT-REP-AMT
            //* * 08222005 MN END
            pnd_New_Header_Page_4.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #NEW-HEADER-PAGE-4
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm768.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM768'
            if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                            //Natural: IF #ROTH-INDICATOR-ON
            {
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm768.class));                                                                       //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM768'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *********************************************************
        //*      THIS PARA WILL GET FINAL PAYMENT FOR GRADED-METHOD *
        //* *********************************************************
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        //*  022208        /* EM - 030612
        pnd_All_Indx_Pnd_Tiaa_Indx_6.reset();                                                                                                                             //Natural: RESET #TIAA-INDX-6
        FOR09:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #MAX-RATES
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rates)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).equals(getZero())))                                  //Natural: IF #ADI-DTL-TIAA-DA-GRD-AMT ( #B-INDX ) = 0
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_All_Indx_Pnd_Prod_Indx.reset();                                                                                                                           //Natural: RESET #PROD-INDX
            DbsUtil.examine(new ExamineSource(ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Grd_Payment_Save().getValue("*")), new                     //Natural: EXAMINE #TIAA-IA-RATE-GRD-PAYMENT-SAVE ( * ) FOR #ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) GIVING INDEX #PROD-INDX
                ExamineSearch(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx)), new ExamineGivingIndex(pnd_All_Indx_Pnd_Prod_Indx));
            if (condition(pnd_All_Indx_Pnd_Prod_Indx.equals(getZero())))                                                                                                  //Natural: IF #PROD-INDX = 0
            {
                pnd_All_Indx_Pnd_Tiaa_Indx_6.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TIAA-INDX-6
                ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Grd_Payment_Save().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_6).setValue(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) TO #TIAA-IA-RATE-GRD-PAYMENT-SAVE ( #TIAA-INDX-6 )
                ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Gur_Payment_Save().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_6).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GUR-PAYMENT-SAVE ( #TIAA-INDX-6 )
                ldaAdsl760.getPnd_Tiaa_Ia_Grd_Guranteed_Final().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx));      //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GURANTEED-FINAL
                ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Div_Payment_Save().getValue(pnd_All_Indx_Pnd_Tiaa_Indx_6).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIV-PAYMENT-SAVE ( #TIAA-INDX-6 )
                ldaAdsl760.getPnd_Tiaa_Ia_Grd_Dividend_Final().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx));       //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIVIDEND-FINAL
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Grd_Payment_Save().getValue(pnd_All_Indx_Pnd_Prod_Indx).setValue(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) TO #TIAA-IA-RATE-GRD-PAYMENT-SAVE ( #PROD-INDX )
                ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Gur_Payment_Save().getValue(pnd_All_Indx_Pnd_Prod_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GUR-PAYMENT-SAVE ( #PROD-INDX )
                ldaAdsl760.getPnd_Tiaa_Ia_Grd_Guranteed_Final().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx));      //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-IA-GRD-GURANTEED-FINAL
                ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Div_Payment_Save().getValue(pnd_All_Indx_Pnd_Prod_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIV-PAYMENT-SAVE ( #PROD-INDX )
                ldaAdsl760.getPnd_Tiaa_Ia_Grd_Dividend_Final().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx));       //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-IA-GRD-DIVIDEND-FINAL
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_All_Indx_Pnd_B_Indx.equals(1)))                                                                                                                 //Natural: IF #B-INDX = 1
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm769.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM769'
            if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                            //Natural: IF #ROTH-INDICATOR-ON
            {
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm769.class));                                                                       //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM769'
            }                                                                                                                                                             //Natural: END-IF
            pnd_New_Header_Page_4.setValue(false);                                                                                                                        //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-4
        }                                                                                                                                                                 //Natural: END-IF
        pnd_New_Header_Page_4.setValue(false);                                                                                                                            //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-4
    }
    private void sub_Write_Tiaa_Total_Settlement_Line() throws Exception                                                                                                  //Natural: WRITE-TIAA-TOTAL-SETTLEMENT-LINE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        pnd_New_Header_Page_2.setValue(true);                                                                                                                             //Natural: MOVE TRUE TO #NEW-HEADER-PAGE-2
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                                //Natural: IF #ROTH-INDICATOR-ON
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        FOR10:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO 120
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(120)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl760.getPnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Rate().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                         //Natural: IF #T-SUB-DA-RATE ( #B-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl760.getPnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Rate_Total_Settle().setValue(ldaAdsl760.getPnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Rate().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #T-SUB-DA-RATE ( #B-INDX ) TO #TIAA-DA-RATE-TOTAL-SETTLE
            ldaAdsl760.getPnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Prod_Total_Settle().setValue(ldaAdsl760.getPnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Prod().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #T-SUB-DA-PROD ( #B-INDX ) TO #TIAA-DA-PROD-TOTAL-SETTLE
            ldaAdsl760.getPnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Accum_Total_Settle().setValue(ldaAdsl760.getPnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Accum_Amtfslash_Units().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #T-SUB-DA-ACCUM-AMT/UNITS ( #B-INDX ) TO #TIAA-DA-ACCUM-TOTAL-SETTLE
            ldaAdsl760.getPnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Total_Settle().setValue(ldaAdsl760.getPnd_T_Save_Sub_Totals_Pnd_T_Sub_Settled_Amtfslash_Units().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #T-SUB-SETTLED-AMT/UNITS ( #B-INDX ) TO #TIAA-SETTLED-TOTAL-SETTLE
            pnd_New_Header_Page_2.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #NEW-HEADER-PAGE-2
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm777.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM777'
            if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                            //Natural: IF #ROTH-INDICATOR-ON
            {
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm777.class));                                                                       //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM777'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Ia_Payment_Standard_Method() throws Exception                                                                                                  //Natural: WRITE-IA-PAYMENT-STANDARD-METHOD
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaAdsl760.getPnd_Tiaa_Ia_Std_Guranteed_Final().equals(gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Gur_Fin_Renew()) && ldaAdsl760.getPnd_Tiaa_Ia_Std_Dividend_Final().equals(gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Div_Fin_Renew()))) //Natural: IF #TIAA-IA-STD-GURANTEED-FINAL = #TIAA-IA-STD-GUR-FIN-RENEW AND #TIAA-IA-STD-DIVIDEND-FINAL = #TIAA-IA-STD-DIV-FIN-RENEW
        {
            //*  022208
            //*  022208
            gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Gur_Fin_Renew().reset();                                                                                                //Natural: RESET #TIAA-IA-STD-GUR-FIN-RENEW #TIAA-IA-STD-DIV-FIN-RENEW #TIAA-IA-STD-GUR-PAY-RENEW ( * ) #TIAA-IA-STD-DIV-PAY-RENEW ( * )
            gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Div_Fin_Renew().reset();
            gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Gur_Pay_Renew().getValue("*").reset();
            gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Div_Pay_Renew().getValue("*").reset();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAdsl760.getPnd_Tiaa_Ia_Std_Guranteed_Fin_Renew().setValue(gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Gur_Fin_Renew());                                          //Natural: MOVE #TIAA-IA-STD-GUR-FIN-RENEW TO #TIAA-IA-STD-GURANTEED-FIN-RENEW
            ldaAdsl760.getPnd_Tiaa_Ia_Std_Dividend_Fin_Renew().setValue(gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Div_Fin_Renew());                                           //Natural: MOVE #TIAA-IA-STD-DIV-FIN-RENEW TO #TIAA-IA-STD-DIVIDEND-FIN-RENEW
        }                                                                                                                                                                 //Natural: END-IF
        pnd_New_Header_Page_5.setValue(true);                                                                                                                             //Natural: MOVE TRUE TO #NEW-HEADER-PAGE-5
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                                //Natural: IF #ROTH-INDICATOR-ON
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  022208
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR11:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #MAX-RATES
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rates)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Std_Payment_Save().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))             //Natural: IF #TIAA-IA-RATE-STD-PAYMENT-SAVE ( #B-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl760.getPnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Std_Payment().setValue(ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Std_Payment_Save().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-RATE-STD-PAYMENT-SAVE ( #B-INDX ) TO #TIAA-IA-RATE-STD-PAYMENT
            ldaAdsl760.getPnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Payment().setValue(ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Gur_Payment_Save().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-STD-GUR-PAYMENT-SAVE ( #B-INDX ) TO #TIAA-IA-STD-GURANTEED-PAYMENT
            ldaAdsl760.getPnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Dividend_Payment().setValue(ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Div_Payment_Save().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-STD-DIV-PAYMENT-SAVE ( #B-INDX ) TO #TIAA-IA-STD-DIVIDEND-PAYMENT
            ldaAdsl760.getPnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Pay_Renew().setValue(gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Gur_Pay_Renew().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-STD-GUR-PAY-RENEW ( #B-INDX ) TO #TIAA-IA-STD-GURANTEED-PAY-RENEW
            ldaAdsl760.getPnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Dividend_Pay_Renew().setValue(gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Div_Pay_Renew().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-STD-DIV-PAY-RENEW ( #B-INDX ) TO #TIAA-IA-STD-DIVIDEND-PAY-RENEW
            pnd_New_Header_Page_5.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #NEW-HEADER-PAGE-5
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm770.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM770'
            if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                            //Natural: IF #ROTH-INDICATOR-ON
            {
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm770.class));                                                                       //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM770'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_All_Indx_Pnd_B_Indx.equals(1)))                                                                                                                 //Natural: IF #B-INDX = 1
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm771.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM771'
            if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                            //Natural: IF #ROTH-INDICATOR-ON
            {
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm771.class));                                                                       //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM771'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_New_Header_Page_5.setValue(false);                                                                                                                            //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-5
    }
    private void sub_Write_Ia_Payment_Graded_Method() throws Exception                                                                                                    //Natural: WRITE-IA-PAYMENT-GRADED-METHOD
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaAdsl760.getPnd_Tiaa_Ia_Grd_Guranteed_Final().equals(gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Gur_Fin_Renew()) && ldaAdsl760.getPnd_Tiaa_Ia_Grd_Dividend_Final().equals(gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Div_Fin_Renew()))) //Natural: IF #TIAA-IA-GRD-GURANTEED-FINAL = #TIAA-IA-GRD-GUR-FIN-RENEW AND #TIAA-IA-GRD-DIVIDEND-FINAL = #TIAA-IA-GRD-DIV-FIN-RENEW
        {
            //*  022208
            //*  022208
            gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Gur_Fin_Renew().reset();                                                                                                //Natural: RESET #TIAA-IA-GRD-GUR-FIN-RENEW #TIAA-IA-GRD-DIV-FIN-RENEW #TIAA-IA-GRD-GUR-PAY-RENEW ( * ) #TIAA-IA-GRD-DIV-PAY-RENEW ( * )
            gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Div_Fin_Renew().reset();
            gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Gur_Pay_Renew().getValue("*").reset();
            gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Div_Pay_Renew().getValue("*").reset();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAdsl760.getPnd_Tiaa_Ia_Grd_Guranteed_Fin_Renew().setValue(gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Gur_Fin_Renew());                                          //Natural: MOVE #TIAA-IA-GRD-GUR-FIN-RENEW TO #TIAA-IA-GRD-GURANTEED-FIN-RENEW
            ldaAdsl760.getPnd_Tiaa_Ia_Grd_Dividend_Fin_Renew().setValue(gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Div_Fin_Renew());                                           //Natural: MOVE #TIAA-IA-GRD-DIV-FIN-RENEW TO #TIAA-IA-GRD-DIVIDEND-FIN-RENEW
        }                                                                                                                                                                 //Natural: END-IF
        pnd_New_Header_Page_6.setValue(true);                                                                                                                             //Natural: MOVE TRUE TO #NEW-HEADER-PAGE-6
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                                //Natural: IF #ROTH-INDICATOR-ON
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  022208
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR12:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #MAX-RATES
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rates)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Grd_Payment_Save().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))             //Natural: IF #TIAA-IA-RATE-GRD-PAYMENT-SAVE ( #B-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl760.getPnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Grd_Payment().setValue(ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Grd_Payment_Save().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-RATE-GRD-PAYMENT-SAVE ( #B-INDX ) TO #TIAA-IA-RATE-GRD-PAYMENT
            ldaAdsl760.getPnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Payment().setValue(ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Gur_Payment_Save().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-GRD-GUR-PAYMENT-SAVE ( #B-INDX ) TO #TIAA-IA-GRD-GURANTEED-PAYMENT
            ldaAdsl760.getPnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Pay_Renew().setValue(gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Gur_Pay_Renew().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-GRD-GUR-PAY-RENEW ( #B-INDX ) TO #TIAA-IA-GRD-GURANTEED-PAY-RENEW
            ldaAdsl760.getPnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Payment().setValue(ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Div_Payment_Save().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-GRD-DIV-PAYMENT-SAVE ( #B-INDX ) TO #TIAA-IA-GRD-DIVIDEND-PAYMENT
            ldaAdsl760.getPnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Pay_Renew().setValue(gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Div_Pay_Renew().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: MOVE #TIAA-IA-GRD-DIV-PAY-RENEW ( #B-INDX ) TO #TIAA-IA-GRD-DIVIDEND-PAY-RENEW
            pnd_New_Header_Page_6.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #NEW-HEADER-PAGE-6
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm772.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM772'
            if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                            //Natural: IF #ROTH-INDICATOR-ON
            {
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm772.class));                                                                       //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM772'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_All_Indx_Pnd_B_Indx.equals(1)))                                                                                                                 //Natural: IF #B-INDX = 1
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm773.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM773'
            if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                            //Natural: IF #ROTH-INDICATOR-ON
            {
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm773.class));                                                                       //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM773'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_New_Header_Page_6.setValue(false);                                                                                                                            //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-6
    }
    private void sub_Process_Annuitant_Total_Rtn() throws Exception                                                                                                       //Natural: PROCESS-ANNUITANT-TOTAL-RTN
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Total_Tiaa_Nbr.equals(getZero())))                                                                                                              //Natural: IF #TOTAL-TIAA-NBR = 0
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-IA-PAYMENT-STANDARD-METHOD
            sub_Write_Ia_Payment_Standard_Method();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-IA-PAYMENT-GRADED-METHOD
            sub_Write_Ia_Payment_Graded_Method();
            if (condition(Global.isEscape())) {return;}
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm883.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM883'
            if (condition(pnd_Roth_Indicator_On.getBoolean()))                                                                                                            //Natural: IF #ROTH-INDICATOR-ON
            {
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 2 )
                if (condition(Global.isEscape())){return;}
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm883.class));                                                                       //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM883'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_All_Indx.reset();                                                                                                                                             //Natural: RESET #ALL-INDX
                                                                                                                                                                          //Natural: PERFORM RESET-TIAA-DETAIL-TOTAL-SAVE-AREA
        sub_Reset_Tiaa_Detail_Total_Save_Area();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM RESET-TIAA-FINAL-TOTAL-SAVE-AREA
        sub_Reset_Tiaa_Final_Total_Save_Area();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Reset_Tiaa_Detail_Total_Save_Area() throws Exception                                                                                                 //Natural: RESET-TIAA-DETAIL-TOTAL-SAVE-AREA
    {
        if (BLNatReinput.isReinput()) return;

        ldaAdsl760.getPnd_Tiaa_Minor_Totals().reset();                                                                                                                    //Natural: RESET #TIAA-MINOR-TOTALS #TIAA-HEADER-SETTLE-AMOUNTS #TIAA-HEADER-AMT #TIAA-SUB-TOTALS #TARGET-NUMBER ( * )
        ldaAdsl760.getPnd_Tiaa_Header_Settle_Amounts().reset();
        ldaAdsl760.getPnd_Tiaa_Header_Amt().reset();
        ldaAdsl760.getPnd_Tiaa_Sub_Totals().reset();
        ldaAdsl761.getPnd_Target_All_Pnd_Target_Number().getValue("*").reset();
    }
    private void sub_Reset_Tiaa_Final_Total_Save_Area() throws Exception                                                                                                  //Natural: RESET-TIAA-FINAL-TOTAL-SAVE-AREA
    {
        if (BLNatReinput.isReinput()) return;

        //*  022208
        //*  022208
        //*  022208
        //*  022208
        ldaAdsl760.getPnd_Tiaa_Final_Totals().reset();                                                                                                                    //Natural: RESET #TIAA-FINAL-TOTALS #TIAA-STANDATD-TOTALS #TIAA-STANDARD-SUB-TOTALS #TIAA-GRADED-TOTALS #T-SAVE-SUB-TOTALS #TIAA-GRADED-SUB-TOTALS #TIAA-STD-METHOD-SAVE-AMOUNTS #TIAA-STD-METHOD-SAVE-PAYMENTS #TIAA-STD-METHOD-PAYMENT-TOTALS #TIAA-IA-STD-GURANTEED-FINAL #TIAA-IA-STD-GURANTEED-FIN-RENEW #TIAA-IA-STD-DIVIDEND-FINAL #TIAA-IA-STD-DIVIDEND-FIN-RENEW #TIAA-IA-STD-GUR-PAY-RENEW ( * ) #TIAA-IA-STD-DIV-PAY-RENEW ( * ) #TIAA-GRD-METHOD-SAVE-PAYMENTS #TIAA-GRD-METHOD-SAVE-AMOUNTS #TIAA-GRD-METHOD-PAYMENT-TOTALS #TIAA-IA-GRD-GURANTEED-FINAL #TIAA-IA-GRD-DIVIDEND-FINAL #TIAA-IA-GRD-GURANTEED-FIN-RENEW #TIAA-IA-GRD-DIVIDEND-FIN-RENEW #TIAA-IA-STD-GURANTEED-FIN-RENEW #TIAA-IA-STD-DIVIDEND-FIN-RENEW #TIAA-IA-GRD-DIV-PAY-RENEW ( * ) #TIAA-IA-GRD-GUR-PAY-RENEW ( * ) #TOTAL-TIAA-NBR #TIAA-IA-STD-GUR-FIN-RENEW #TIAA-IA-STD-DIV-FIN-RENEW #TIAA-IA-GRD-GUR-FIN-RENEW #TIAA-IA-GRD-DIV-FIN-RENEW
        ldaAdsl760.getPnd_Tiaa_Standatd_Totals().reset();
        ldaAdsl760.getPnd_Tiaa_Standard_Sub_Totals().reset();
        ldaAdsl760.getPnd_Tiaa_Graded_Totals().reset();
        ldaAdsl760.getPnd_T_Save_Sub_Totals().reset();
        ldaAdsl760.getPnd_Tiaa_Graded_Sub_Totals().reset();
        ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Amounts().reset();
        ldaAdsl760.getPnd_Tiaa_Std_Method_Save_Payments().reset();
        ldaAdsl760.getPnd_Tiaa_Std_Method_Payment_Totals().reset();
        ldaAdsl760.getPnd_Tiaa_Ia_Std_Guranteed_Final().reset();
        ldaAdsl760.getPnd_Tiaa_Ia_Std_Guranteed_Fin_Renew().reset();
        ldaAdsl760.getPnd_Tiaa_Ia_Std_Dividend_Final().reset();
        ldaAdsl760.getPnd_Tiaa_Ia_Std_Dividend_Fin_Renew().reset();
        gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Gur_Pay_Renew().getValue("*").reset();
        gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Div_Pay_Renew().getValue("*").reset();
        ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Payments().reset();
        ldaAdsl760.getPnd_Tiaa_Grd_Method_Save_Amounts().reset();
        ldaAdsl760.getPnd_Tiaa_Grd_Method_Payment_Totals().reset();
        ldaAdsl760.getPnd_Tiaa_Ia_Grd_Guranteed_Final().reset();
        ldaAdsl760.getPnd_Tiaa_Ia_Grd_Dividend_Final().reset();
        ldaAdsl760.getPnd_Tiaa_Ia_Grd_Guranteed_Fin_Renew().reset();
        ldaAdsl760.getPnd_Tiaa_Ia_Grd_Dividend_Fin_Renew().reset();
        ldaAdsl760.getPnd_Tiaa_Ia_Std_Guranteed_Fin_Renew().reset();
        ldaAdsl760.getPnd_Tiaa_Ia_Std_Dividend_Fin_Renew().reset();
        gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Div_Pay_Renew().getValue("*").reset();
        gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Gur_Pay_Renew().getValue("*").reset();
        pnd_Total_Tiaa_Nbr.reset();
        gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Gur_Fin_Renew().reset();
        gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Std_Div_Fin_Renew().reset();
        gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Gur_Fin_Renew().reset();
        gdaAdsg870.getAdsg870_Pnd_Tiaa_Ia_Grd_Div_Fin_Renew().reset();
    }
    private void sub_Reset_Header_Totals() throws Exception                                                                                                               //Natural: RESET-HEADER-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        ldaAdsl760.getPnd_Tiaa_Header_Settle_Amounts().reset();                                                                                                           //Natural: RESET #TIAA-HEADER-SETTLE-AMOUNTS
        ldaAdsl760.getPnd_Tiaa_Header_Amt().reset();                                                                                                                      //Natural: RESET #TIAA-HEADER-AMT
        ldaAdsl761.getPnd_Target_All_Pnd_Target_Number().getValue("*").reset();                                                                                           //Natural: RESET #TARGET-NUMBER ( * )
    }
    private void sub_Put_Asterisk_Beside_Total() throws Exception                                                                                                         //Natural: PUT-ASTERISK-BESIDE-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Asterisk.reset();                                                                                                                                             //Natural: RESET #ASTERISK #COMMENT1 #COMMENT2
        pnd_Comment1.reset();
        pnd_Comment2.reset();
        if (condition(pnd_Amount.greater(getZero())))                                                                                                                     //Natural: IF #AMOUNT GT 0
        {
            pnd_Asterisk.setValue("*");                                                                                                                                   //Natural: ASSIGN #ASTERISK := '*'
            pnd_Comment1.setValue("* Opening balance amount may not reflect any financial transactions processed");                                                       //Natural: ASSIGN #COMMENT1 := '* Opening balance amount may not reflect any financial transactions processed'
            pnd_Comment2.setValue("  for this contract(s) prior to the T395.");                                                                                           //Natural: ASSIGN #COMMENT2 := '  for this contract(s) prior to the T395.'
        }                                                                                                                                                                 //Natural: END-IF
        //*  PUT-ASTERISK-BESIDE-TOTAL
    }
    private void sub_End_Of_Job_Rtn() throws Exception                                                                                                                    //Natural: END-OF-JOB-RTN
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaAdsl760.getPnd_Tiaa_Rec_Cnt().equals(getZero())))                                                                                                //Natural: IF #TIAA-REC-CNT = 0
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE," ");                                                                                                              //Natural: WRITE ( 1 ) ' '
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"**********************************************************************");                                         //Natural: WRITE ( 1 ) '**********************************************************************'
            if (Global.isEscape()) return;
            //* DEA
            getReports().write(1, ReportOption.NOTITLE,"* ADSP762 - ANNUITIZATION OF OMNIPLUS CONTRACTS REPORT - JOB RUN ENDED");                                         //Natural: WRITE ( 1 ) '* ADSP762 - ANNUITIZATION OF OMNIPLUS CONTRACTS REPORT - JOB RUN ENDED'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"* NO TIAA TRANSACTIONS PROCESSED TODAY");                                                                         //Natural: WRITE ( 1 ) '* NO TIAA TRANSACTIONS PROCESSED TODAY'
            if (Global.isEscape()) return;
            //*  DEA
            getReports().write(1, ReportOption.NOTITLE,"* TOTAL RECORDS EXTRACTED FROM ADSP762  = ",ldaAdsl760.getPnd_Tiaa_Rec_Cnt(),"**********************************************************************"); //Natural: WRITE ( 1 ) '* TOTAL RECORDS EXTRACTED FROM ADSP762  = ' #TIAA-REC-CNT '**********************************************************************'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAdsl760.getPnd_Tiaa_Rec_Cnt_Surv().equals(getZero())))                                                                                           //Natural: IF #TIAA-REC-CNT-SURV = 0
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().write(2, ReportOption.NOTITLE," ");                                                                                                              //Natural: WRITE ( 2 ) ' '
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"**********************************************************************");                                         //Natural: WRITE ( 2 ) '**********************************************************************'
            if (Global.isEscape()) return;
            //*  DEA
            getReports().write(2, ReportOption.NOTITLE,"* ADSP762 - ANNUITIZATION OF OMNIPLUS CONTRACTS REPORT - JOB RUN ENDED");                                         //Natural: WRITE ( 2 ) '* ADSP762 - ANNUITIZATION OF OMNIPLUS CONTRACTS REPORT - JOB RUN ENDED'
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"* NO SURV ROTH TRANSACTIONS PROCESSED TODAY");                                                                    //Natural: WRITE ( 2 ) '* NO SURV ROTH TRANSACTIONS PROCESSED TODAY'
            if (Global.isEscape()) return;
            //*  DEA
            getReports().write(2, ReportOption.NOTITLE,"* TOTAL SURV ROTH RECORDS EXTRACTED FROM ADSP762  = ",ldaAdsl760.getPnd_Tiaa_Rec_Cnt_Surv());                     //Natural: WRITE ( 2 ) '* TOTAL SURV ROTH RECORDS EXTRACTED FROM ADSP762  = ' #TIAA-REC-CNT-SURV
            if (Global.isEscape()) return;
            getReports().write(2, ReportOption.NOTITLE,"**********************************************************************");                                         //Natural: WRITE ( 2 ) '**********************************************************************'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  GG050608
                    //*  RS0 DEA
                    ldaAdsl761.getPnd_Page_Number().nadd(1);                                                                                                              //Natural: ADD 1 TO #PAGE-NUMBER
                    pnd_Page_Number_Map.setValue(ldaAdsl761.getPnd_Page_Number());                                                                                        //Natural: ASSIGN #PAGE-NUMBER-MAP := #PAGE-NUMBER
                    //*  #HEADING := '       TIAA Settlement        '
                    pnd_Heading.setValue("   TIAA/Stable Settlement/SVSAA      ");                                                                                        //Natural: ASSIGN #HEADING := '   TIAA/Stable Settlement/SVSAA      '
                    getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm762a.class));                                                                  //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM762A'
                    if (condition(pnd_New_Header_Page_1.getBoolean()))                                                                                                    //Natural: IF #NEW-HEADER-PAGE-1
                    {
                        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm763.class));                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM763'
                        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm778.class));                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM778'
                        if (condition(! (pnd_Roth_Indicator_On.getBoolean())))                                                                                            //Natural: IF NOT #ROTH-INDICATOR-ON
                        {
                            pnd_New_Header_Page_1.setValue(false);                                                                                                        //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-1
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_New_Header_Page_2.getBoolean()))                                                                                                    //Natural: IF #NEW-HEADER-PAGE-2
                    {
                        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm765.class));                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM765'
                        if (condition(! (pnd_Roth_Indicator_On.getBoolean())))                                                                                            //Natural: IF NOT #ROTH-INDICATOR-ON
                        {
                            pnd_New_Header_Page_2.setValue(false);                                                                                                        //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-2
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_New_Header_Page_3.getBoolean()))                                                                                                    //Natural: IF #NEW-HEADER-PAGE-3
                    {
                        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm666.class));                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM666'
                        if (condition(! (pnd_Roth_Indicator_On.getBoolean())))                                                                                            //Natural: IF NOT #ROTH-INDICATOR-ON
                        {
                            pnd_New_Header_Page_3.setValue(false);                                                                                                        //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-3
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_New_Header_Page_4.getBoolean()))                                                                                                    //Natural: IF #NEW-HEADER-PAGE-4
                    {
                        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm668.class));                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM668'
                        if (condition(! (pnd_Roth_Indicator_On.getBoolean())))                                                                                            //Natural: IF NOT #ROTH-INDICATOR-ON
                        {
                            pnd_New_Header_Page_4.setValue(false);                                                                                                        //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-4
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_New_Header_Page_5.getBoolean()))                                                                                                    //Natural: IF #NEW-HEADER-PAGE-5
                    {
                        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm665.class));                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM665'
                        if (condition(! (pnd_Roth_Indicator_On.getBoolean())))                                                                                            //Natural: IF NOT #ROTH-INDICATOR-ON
                        {
                            pnd_New_Header_Page_5.setValue(false);                                                                                                        //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-5
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_New_Header_Page_6.getBoolean()))                                                                                                    //Natural: IF #NEW-HEADER-PAGE-6
                    {
                        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm662.class));                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM662'
                        if (condition(! (pnd_Roth_Indicator_On.getBoolean())))                                                                                            //Natural: IF NOT #ROTH-INDICATOR-ON
                        {
                            pnd_New_Header_Page_6.setValue(false);                                                                                                        //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-6
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  RS0
                    ldaAdsl761.getPnd_Page_Number_Surv().nadd(1);                                                                                                         //Natural: ADD 1 TO #PAGE-NUMBER-SURV
                    pnd_Page_Number_Map.setValue(ldaAdsl761.getPnd_Page_Number_Surv());                                                                                   //Natural: ASSIGN #PAGE-NUMBER-MAP := #PAGE-NUMBER-SURV
                    pnd_Heading.setValue("TIAA/STABLE Surv 403B/401K Settlement");                                                                                        //Natural: ASSIGN #HEADING := 'TIAA/STABLE Surv 403B/401K Settlement'
                    getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm762a.class));                                                                  //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM762A'
                    if (condition(pnd_New_Header_Page_1.getBoolean()))                                                                                                    //Natural: IF #NEW-HEADER-PAGE-1
                    {
                        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm763.class));                                                               //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM763'
                        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm778.class));                                                               //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM778'
                        pnd_New_Header_Page_1.setValue(false);                                                                                                            //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-1
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_New_Header_Page_2.getBoolean()))                                                                                                    //Natural: IF #NEW-HEADER-PAGE-2
                    {
                        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm765.class));                                                               //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM765'
                        pnd_New_Header_Page_2.setValue(false);                                                                                                            //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-2
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_New_Header_Page_3.getBoolean()))                                                                                                    //Natural: IF #NEW-HEADER-PAGE-3
                    {
                        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm666.class));                                                               //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM666'
                        pnd_New_Header_Page_3.setValue(false);                                                                                                            //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-3
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_New_Header_Page_4.getBoolean()))                                                                                                    //Natural: IF #NEW-HEADER-PAGE-4
                    {
                        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm668.class));                                                               //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM668'
                        pnd_New_Header_Page_4.setValue(false);                                                                                                            //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-4
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_New_Header_Page_5.getBoolean()))                                                                                                    //Natural: IF #NEW-HEADER-PAGE-5
                    {
                        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm665.class));                                                               //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM665'
                        pnd_New_Header_Page_5.setValue(false);                                                                                                            //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-5
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_New_Header_Page_6.getBoolean()))                                                                                                    //Natural: IF #NEW-HEADER-PAGE-6
                    {
                        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm662.class));                                                               //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM662'
                        pnd_New_Header_Page_6.setValue(false);                                                                                                            //Natural: MOVE FALSE TO #NEW-HEADER-PAGE-6
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=79 PS=60");
        Global.format(1, "LS=100 PS=60");
        Global.format(2, "LS=100 PS=60");
    }
}
