/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:02:46 PM
**        * FROM NATURAL PROGRAM : Adsp400
************************************************************
**        * FILE NAME            : Adsp400.java
**        * CLASS NAME           : Adsp400
**        * INSTANCE NAME        : Adsp400
************************************************************
***********************************************************************
* PROGRAM  : ADSP400
* SYSTEM   : ADAS - ANNUITIZATION SUNGUARD
* TITLE    : ADAS BATCH (STEP 1) - OMNI PLUS AND ADAS KDO MATCHING
*          :                       PROCESS
* GENERATED: MARCH 31, 2004
* AUTHOR   : EDWINA VILLANUEVA
* FUNCTION : THIS PROGRAM WILL MATCH SETTLEMENT RECORDS IN OMNI PLUS
*          : (T966 INPUT FILE) WITH ADAS REQUESTS ENTERED ONLINE.
*          : ADAS RECORDS WILL BE WITH UPDATED WITH CORRESPONDING OMNI
*          : DATA AND THE FINAL REQUEST'S STATUS WILL BE SET DEPENDING
*          : THE FF. CRITERIA.
*          :
*          : 1.  IF 100% MATCH IS FOUND, ADAS IS UPDATED AND RECORD
*          :     STATUS WILL BE CHANGED TO 'PENDING - READY TO PROCESS'.
*          :     THESE RECORDS WILL THEN BE PICKED-UP FOR PROCESSING
*          :     IN ADAS BATCH STEP 2.
*          :
*          : 2.  IF A PARTIAL MATCH IS FOUND, OR ORIGINAL REQUEST IS
*          :     INCOMPLETE OR AWAITING 2ND SIGHT, ADAS IS UPDATED AND
*          :     RECORD STATUS WILL BE CHANGED TO 'DELAYED'. MIT RECORD
*          :     WILL BE UPDATED TO REFLECT DELAYED STATUS FOR USER'S
*          :     REVIEW THE NEXT DAY.
*          :
*          : 3.  IF NO MATCH IS FOUND, A REJECT REPORT IS GENERATED FOR
*          :     RESETTLEMENTS REQUESTS.  FOR REGULAR REQUEST, AN ADAS
*          :     RECORD IS CREATED USING OMNI INFORMATION WITH 'DELAYED'
*          :     STATUS.  A SYSTEM GENERATED REQUEST WILL BE LOGGED IN
*          :     MIT FOR USER'SREVIEW THE NEXT DAY.
*          :
*********************  MAINTENANCE LOG ******************************
*
*  D A T E   PROGRAMMER     D E S C R I P T I O N
*
*  06-15-04  E.VILLANUEVA   REPLACE FUND-QTY IN OMNI FILE LAYOUT WITH
*                             CONTRACT-ID.  FOR TIAA FUNDS, CALL ADSN501
*                             TO CONVERT CONTRACT-ID WITH 2 BYTE RATE
*                             CODE.  FOR CREF FUNDS, CALL EXTERNALIZATIO
*                             TO GET CREF RATE CODES.
* 06-21-04  E.VILLANUEVA   REMOVE UPDATE MIT IF REQUEST IS PROCESSED
*                             SUCCESSFULLY
* 06-30-04  E.VILLANUEVA   DO NOT DISPLAY ASSIGN JOB RETURN CODE IF
*                             T966 FILE IS EMPTY
* 07-13-04  E.VILLANUEVA   REMOVE CHECK FOR MONDAY WHEN VALIDATING
*                             CREATION DATE IN T966 FILE PER E.MELNIK
* 07-16-04  E.VILLANUEVA   INCLUDE PLAN-NO WHEN CALLING MIT
* 08-23-04  E.VILLANUEVA   FOR TIAA FUNDS, ACCUMULATE #OMNI-FUND-SETTL-
*                          AMT OF ALL TIAA HEADER RECORDS ASSOCIATED
*                          WITH EACH FUND-SOURCE
* 12-07-04  T.SHINE        TNT CHANGES FOR ADAS
* 07062005  M.NACHBER      TPA & IPRO CHANGES:
*                        - ADDED GUARANTEED COMMUTED VALUE FOR TPA
*                          REQUESTS
*                        - #OMNI-FUND-RATE-ACCUM-AMT IS DELETED FROM
*                          OMNI EXTRACT AND COMMENTED OUT IN THE PGM
*                        - 3 NEW FIELDS ARE ADDED TO OMNI EXTRACT
*                              #OMNI-TPA-GUARANT-COMMUT-AMT
*                              #OMNI-TPA-IO-ORIG-CNTRCT-LOB-IND
*                              #OMNI-TPA-PAYMENT-CNT
* 09272005 M.NACHBER     PROD. PROBLEM.  FOUND DURING INTEGRATION
*                        TEST OF TPA's and IPRO'S
* 10062005 M.NACHBER     ADDED INPUT RECORD NUMBER TO THE ERROR REPORT
* 01122006 M.NACHBER     REL 8.
*                        CORRECTION TO THE RESETTLEMENT PROCESS
* 03282006 M.NACHBER     PROD. ISSUE
*                        MODIFIED TO HANDLE MULTIPLE REQUESTS WITH THE
*                        SAME CONTRACT LOCATED ONE AFTER ANOTHER ON
*                        THE OMNI EXTRACT
* 05152006 M.NACHBER     ICAP/TPA CHANGE
*                        MODIFIED TO TREAT ICAP/TPA REQUEST AS ICAP
*                        REQUESTS AND NOT TO CHECK IF
*                        #OMNI-TPA-GUARANT-COMMUT-AMT IS POPULATED
* 06302006 M.NACHBER     ACKNOWLEDGMENT LETTERS
*                        MODIFIED TO BYPASS ACKL RECORDS ON OMNI FILE
* 11092006 M.NACHBER     ADAS-INTRA
*                        ADDED 3 NEW STATUS E26, E27, E28 TO
*                        #ORIGINAL-STATUSES
* 01112007 O. SOTTO      FIXED MATCHING PROBLEM. SC 011107.
* 03072007 M. NACHBER    ATRA/KEOGH
* 07172007 O. SOTTO      FIXED RESETTLEMENT MATCHING PROBLEM. SC 071707.
* 12102007 O. SOTTO    FIX TO INC122534 (RESETTLEMENT ISSUE). SC 121007.
* 01082008 O. SOTTO    STREAMLINE PROCESSING. WHEN CALLING ADAN501,
*                      THE PDA MUST NOT BE INITIALIZED. ALSO,
*                      CALL TO NECN4000 TO GET LOB MUST ONLY BE DONE
*                      ONCE FOR EACH CONTRACT. SC 010808.
* 01102008 O. SOTTO    FIXED RESETTLEMENT PROBLEM WHENEVER
*                      THE RESETTLEMENT HAS LESS FUNDS THAN THE
*                      ORIGINAL. SC 011008.
* 04182008 R. SACHARNY PROCESS ROTH REQUESTS (MARKED RS0)
* 12102008 E. MELNIK   TIAA ACCESS/STABLE RETURN ENHANCEMENTS.  MARKED
*                      BY EM - 121008.
* 01012009 O SOTTO     INCREASE TOTAL COUNTERS TO P9 TO PREVENT
*                      OVERFLOW. INC537609 AND INC537609.
* 01/12/09 R SACHARNY  CALL ADSN888 FOR INFO FROM EXTERNALIZATION (RS1)
* 04/07/09 E. MELNIK   INCLUDED PROD FIX.  MARKED BY EM - 040709.
* 07/08/09 C. MASON    CHANGES FOR MDM CONVERSION
* 12/22/09 O. SOTTO    DISPLAY RC FROM MDM. SC 122209.
* 03/04/10 C. MASON    ADD OPEN AND CLOSE OF MQ; CHANGE CALL FROM
*                      MDMN100 TO MDMN100A
* 10/11/10 O. SOTTO    INITIALIZE MDMA100.
* 11/15/10 E. MELNIK   SVSAA ENHANCEMENTS.  MARKED
*                      BY EM - 111510.
* 01/21/11 O. SOTTO    FIX RESETTLEMENT ISSUE. SC 012111.
* 07/28/11 E. MELNIK   OMNI FUND ID REMEDIATION PROJECT.  MARKED
*                      BY EM - 072811.
* 03/01/12 E. MELNIK   RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                      UPDATED LINKAGE AREAS.  CHANGES MARKED BY
*                      EM - 030112.
* 12/17/12 O. SOTTO    TNG CHANGES. SC TNG0613.
* 01/17/13 O. SOTTO    CHECK COR WHENEVER MDM RETURN CODE IS NOT ZERO.
*                      SC 011713.
* 06/20/13 O. SOTTO    ADD A CHECK FOR RESETTLEMENT TO MAKE SURE THE
*                      ORIGINAL REQUEST HAS 100% IN THE SETTLED FUND.
*                      SC 062013.
* 07/16/13 O. SOTTO    FIX FOR RESETTLEMENT CHANGES MADE ABOVE.
*                      SC 071613.
* 10/21/13 O. SOTTO    CREF REA CHANGES MARKED WITH OS-102013.
*                      COMMENTED-OUT CALL TO COR AMONG THE
*                      CHANGES.
* 04/13/15 O. SOTTO    PROD FIX TO DUPLICATE PROBLEM. CHANGES
*                      MARKED 041315.
* 02/22/2017 R.CARREON PIN EXPANSION 02222017
* 08/16/17 E. MELNIK  PIN EXPANSION RESETTLEMENT FIX.  MARKED BY
*                     PIN EXPANSION EM 0816.
*
*********************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp400 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private LdaAdsl400 ldaAdsl400;
    private LdaAdsl400a ldaAdsl400a;
    private PdaMdma101 pdaMdma101;
    private LdaAdsl401 ldaAdsl401;
    private LdaAdsl401a ldaAdsl401a;
    private PdaAdsa480 pdaAdsa480;
    private PdaNeca4000 pdaNeca4000;
    private PdaAdaa501a pdaAdaa501a;
    private PdaAdspda_M pdaAdspda_M;
    private PdaAdsa888 pdaAdsa888;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Rtb_Occurs;

    private DbsGroup pnd_Constants;
    private DbsField pnd_Constants_Pnd_Open;
    private DbsField pnd_Constants_Pnd_Close;
    private DbsField pnd_Constants_Pnd_Tiaa;
    private DbsField pnd_Constants_Pnd_Stable;
    private DbsField pnd_Constants_Pnd_Svsaa;
    private DbsField pnd_Constants_Pnd_Rex1;
    private DbsField pnd_Constants_Pnd_Rea1;
    private DbsField pnd_Constants_Pnd_Batchads;
    private DbsField pnd_Constants_Pnd_Mit_Add;
    private DbsField pnd_Constants_Pnd_Mit_Sub_Rqst;
    private DbsField pnd_Constants_Pnd_Mit_Update;
    private DbsField pnd_Constants_Pnd_Rstl_Usage_Cde;
    private DbsField pnd_Constants_Pnd_R_Annty_Typ;
    private DbsField pnd_Constants_Pnd_M_Annty_Typ;
    private DbsField pnd_Constants_Pnd_After_Tax_Fsrce;

    private DbsGroup pnd_Constants__R_Field_1;
    private DbsField pnd_Constants_Pnd_After_Tax_Fsrce_Array;

    private DbsGroup pnd_Constants_Pnd_Statuses;
    private DbsField pnd_Constants_Pnd_Original_Statuses;

    private DbsGroup pnd_Constants__R_Field_2;
    private DbsField pnd_Constants_Pnd_Orig_2nd_Sight;
    private DbsField pnd_Constants_Pnd_Orig_Incmplte;
    private DbsField pnd_Constants_Pnd_Orig_Incmplte_Mit1;
    private DbsField pnd_Constants_Pnd_Orig_Incmplte_Mit2;
    private DbsField pnd_Constants_Pnd_Orig_Incmplte_Mit3;
    private DbsField pnd_Constants_Pnd_Await_Omni;

    private DbsGroup pnd_Constants__R_Field_3;
    private DbsField pnd_Constants_Pnd_Original_Stat_Array;
    private DbsField pnd_Constants_Pnd_Invalid_Rsttlmnt;
    private DbsField pnd_Constants_Pnd_No_Match;
    private DbsField pnd_Constants_Pnd_Await_Process;
    private DbsField pnd_Constants_Pnd_Incomplete_Stats;

    private DbsGroup pnd_Constants__R_Field_4;
    private DbsField pnd_Constants_Pnd_Incmplte;
    private DbsField pnd_Constants_Pnd_Inc_Eff_Dte;
    private DbsField pnd_Constants_Pnd_Inc_Cntrct;
    private DbsField pnd_Constants_Pnd_Inc_Funds;

    private DbsGroup pnd_Constants__R_Field_5;
    private DbsField pnd_Constants_Pnd_Incomplete_Array;
    private DbsField pnd_Constants_Pnd_Await_Stats;

    private DbsGroup pnd_Constants__R_Field_6;
    private DbsField pnd_Constants_Pnd_Await_2nd_Sight;
    private DbsField pnd_Constants_Pnd_Awt_Eff_Dte;
    private DbsField pnd_Constants_Pnd_Awt_Cntrct;
    private DbsField pnd_Constants_Pnd_Awt_Funds;

    private DbsGroup pnd_Constants__R_Field_7;
    private DbsField pnd_Constants_Pnd_Await_2nd_Sight_Array;
    private DbsField pnd_Constants_Pnd_Delayed_Stats;

    private DbsGroup pnd_Constants__R_Field_8;
    private DbsField pnd_Constants_Pnd_Delayed_Eff_Dte;
    private DbsField pnd_Constants_Pnd_Delayed_Cntrct;
    private DbsField pnd_Constants_Pnd_Delayed_Funds;

    private DbsGroup pnd_Constants__R_Field_9;
    private DbsField pnd_Constants_Pnd_Delayed_Stat_Array;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_I;
    private DbsField pnd_Counters_Pnd_J;
    private DbsField pnd_Counters_Pnd_K;
    private DbsField pnd_Counters_Pnd_X;
    private DbsField pnd_Counters_Pnd_A;
    private DbsField pnd_Counters_Pnd_Indx;
    private DbsField pnd_Counters_Pnd_Total_Input_Recs;
    private DbsField pnd_Counters_Pnd_Total_Omni_Recs;
    private DbsField pnd_Counters_Pnd_Total_Recs;
    private DbsField pnd_Counters_Pnd_Total_Ackl_Recs;
    private DbsField pnd_Counters_Pnd_Bad_Recs;
    private DbsField pnd_Counters_Pnd_Total_Omni_Rqst;
    private DbsField pnd_Counters_Pnd_Total_Omni_Err_Cntrct;
    private DbsField pnd_Counters_Pnd_Total_Omni_Cntrct;

    private DbsGroup pnd_Counters_Pnd_Status_Count;
    private DbsField pnd_Counters_Pnd_No_Match_Cnt;
    private DbsField pnd_Counters_Pnd_Full_Match_Cnt;
    private DbsField pnd_Counters_Pnd_Delayed_Cnt;
    private DbsField pnd_Counters_Pnd_Orig_Incomplete;
    private DbsField pnd_Counters_Pnd_Orig_Await_2site;

    private DbsGroup pnd_Keys;
    private DbsField pnd_Keys_Pnd_Adp_Key;

    private DbsGroup pnd_Keys__R_Field_10;
    private DbsField pnd_Keys_Pnd_Key_Opn_Clsd_Ind;
    private DbsField pnd_Keys_Pnd_Key_Group;

    private DbsGroup pnd_Keys__R_Field_11;
    private DbsField pnd_Keys_Pnd_Key_Pin;
    private DbsField pnd_Keys_Pnd_Key_Frst_Annt_Ssn;
    private DbsField pnd_Keys_Pnd_Key_Plan_Nbr;
    private DbsField pnd_Keys_Pnd_Key_Annty_Optn;
    private DbsField pnd_Keys_Pnd_Key_Roth_Rqst_Ind;
    private DbsField pnd_Keys_Pnd_Adc_Key;

    private DbsGroup pnd_Keys__R_Field_12;
    private DbsField pnd_Keys_Pnd_Key_Rqst_Id;
    private DbsField pnd_Keys_Pnd_Key_Tiaa_Nbr;
    private DbsField pnd_Keys_Pnd_Cor_Cntrct_Key1;

    private DbsGroup pnd_Keys__R_Field_13;
    private DbsField pnd_Keys_Pnd_Cor_Key1_Pin_Nbr;
    private DbsField pnd_Keys_Pnd_Cor_Key1_Rec_Typ;
    private DbsField pnd_Keys_Pnd_Key_Tiaa_Cntrct;
    private DbsField pnd_Keys_Pnd_Key_Usage_Cde;

    private DbsGroup pnd_Terminate;
    private DbsField pnd_Terminate_Pnd_Terminate_No;
    private DbsField pnd_Terminate_Pnd_Terminate_Msg;

    private DbsGroup pnd_Terminate__R_Field_14;
    private DbsField pnd_Terminate_Pnd_Terminate_Msg1;
    private DbsField pnd_Terminate_Pnd_Terminate_Msg2;
    private DbsField pnd_Terminate_Pnd_Terminate_Msg3;

    private DbsGroup pnd_Adsn998_Error_Handler;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Calling_Program;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Unique_Id;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Step_Name;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Appl_Id;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Error_Msg;
    private DbsField pnd_Roth_Request;
    private DbsField pnd_Tiaa_Type;
    private DbsField pnd_Rex1_Type;

    private DbsGroup pnd_Per_Request_Info;
    private DbsField pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found;
    private DbsField pnd_Per_Request_Info_Pnd_New_Request;
    private DbsField pnd_Per_Request_Info_Pnd_Unmatched_Rstlmt;
    private DbsField pnd_Per_Request_Info_Pnd_Resettlement;
    private DbsField pnd_Per_Request_Info_Pnd_Processed_Omni_Rqst;
    private DbsField pnd_Per_Request_Info_Pnd_Processed_Rsttlmnt;
    private DbsField pnd_Per_Request_Info_Pnd_Amt_More_Than_Orig;
    private DbsField pnd_Per_Request_Info_Pnd_Rstlmnt_Ctr;
    private DbsField pnd_Per_Request_Info_Pnd_Original_Amt;
    private DbsField pnd_Per_Request_Info_Pnd_Error_Cntrct;
    private DbsField pnd_Per_Request_Info_Pnd_Omni_Cntrct;
    private DbsField pnd_Per_Request_Info_Pnd_Adas_Cntrct;
    private DbsField pnd_Per_Request_Info_Pnd_Overall_Stat;
    private DbsField pnd_Per_Request_Info_Pnd_Rstl_Cnt;
    private DbsField pnd_Per_Request_Info_Pnd_All_Tiaa_Grd_Pct;
    private DbsField pnd_Per_Request_Info_Pnd_All_Cref_Mon_Pct;
    private DbsField pnd_Per_Request_Info_Pnd_Last_Cntrct_Seq;
    private DbsField pnd_Per_Request_Info_Pnd_Total_Pin_Rqst;
    private DbsField pnd_Per_Request_Info_Pnd_Plan_Nbr;
    private DbsField pnd_Per_Request_Info_Pnd_Rqst_Id;

    private DbsGroup pnd_Per_Request_Info__R_Field_15;
    private DbsField pnd_Per_Request_Info_Pnd_Rqst_Pin;
    private DbsField pnd_Per_Request_Info_Pnd_Rqst_Effctv_Dte;
    private DbsField pnd_Per_Request_Info_Pnd_Rqst_Entry_Dte;
    private DbsField pnd_Per_Request_Info_Pnd_Rqst_Entry_Tme;

    private DbsGroup pnd_Per_Request_Info__R_Field_16;
    private DbsField pnd_Per_Request_Info_Pnd_Rqst_Entry_Tme_N;
    private DbsField pnd_Per_Request_Info_Pnd_Resettl_Orig_Rqstid;

    private DbsGroup pnd_Per_Request_Info_Pnd_Match_Conditions;
    private DbsField pnd_Per_Request_Info_Pnd_Full_Match;
    private DbsField pnd_Per_Request_Info_Pnd_Partial_Match;
    private DbsField pnd_Per_Request_Info_Pnd_Unmatched;

    private DbsGroup pnd_Adsn401;
    private DbsField pnd_Adsn401_Pnd_Cntl_Bsnss_Prev_Dte;
    private DbsField pnd_Adsn401_Pnd_Cntl_Bsnss_Dte;
    private DbsField pnd_Adsn401_Pnd_Cntl_Bsnss_Tmrrw_Dte;
    private DbsField pnd_Adsn401_Pnd_Cct_Rcrd_Cde;

    private DbsGroup pnd_Misc_Variables;
    private DbsField pnd_Misc_Variables_Pnd_Eff_Date_D;
    private DbsField pnd_Misc_Variables_Pnd_File_Date_D;
    private DbsField pnd_Misc_Variables_Pnd_Wk_Date;

    private DbsGroup pnd_Misc_Variables__R_Field_17;
    private DbsField pnd_Misc_Variables_Pnd_Wk_Date_A;

    private DbsGroup pnd_Misc_Variables__R_Field_18;
    private DbsField pnd_Misc_Variables_Pnd_Wk_Dte_Cc;
    private DbsField pnd_Misc_Variables_Pnd_Wk_Dte_Yy;
    private DbsField pnd_Misc_Variables_Pnd_Wk_Dte_Mm;
    private DbsField pnd_Misc_Variables_Pnd_Wk_Dte_Dd;
    private DbsField pnd_Misc_Variables_Pnd_Process_Date;

    private DbsGroup pnd_Misc_Variables__R_Field_19;
    private DbsField pnd_Misc_Variables_Pnd_Process_Date_D;
    private DbsField pnd_Misc_Variables_Pnd_Flip_Date;

    private DbsGroup pnd_Misc_Variables__R_Field_20;
    private DbsField pnd_Misc_Variables_Pnd_Flip_Date_D;
    private DbsField pnd_Misc_Variables_Pnd_Bad_Roth_Data;
    private DbsField pnd_Misc_Variables_Pnd_Day_Of_Week;
    private DbsField pnd_Misc_Variables_Pnd_Invalid_File_Date;
    private DbsField pnd_Misc_Variables_Pnd_Acct_Cde_Space;
    private DbsField pnd_Misc_Variables_Pnd_Contract_Space;
    private DbsField pnd_Misc_Variables_Pnd_Omni_Rate_Code;
    private DbsField pnd_Misc_Variables_Pnd_Omni_Lob;
    private DbsField pnd_Misc_Variables_Pnd_Rb_Return_Code;
    private DbsField pnd_Misc_Variables_Pnd_Mit_Audit_Trail;
    private DbsField pnd_Misc_Variables_Pnd_Adas_Funds_Exist;
    private DbsField pnd_Misc_Variables_Pnd_Unmatched_Funds;
    private DbsField pnd_Misc_Variables_Pnd_Pin_Okay;
    private DbsField pnd_Misc_Variables_Pnd_Omni_Format_Ok;
    private DbsField pnd_Misc_Variables_Pnd_Report_Msg;
    private DbsField pnd_Misc_Variables_Pnd_Report_Rqst_Type;
    private DbsField pnd_Misc_Variables_Pnd_Debug_On;
    private DbsField pnd_S_Err_Cntrct;
    private DbsField pnd_Rqst_Found;
    private DbsField pnd_Acct_Ndx;
    private DbsField pnd_Acct_Ndx1;
    private DbsField pnd_Number;
    private DbsField pnd_S_Omni_Contract;
    private DbsField pnd_Rec_Has_Data;

    private DataAccessProgramView vw_ads_Part;
    private DbsField ads_Part_Rqst_Id;
    private DbsField ads_Part_Adp_Annt_Typ_Cde;
    private DbsField pnd_Rqst_Cnt;
    private DbsField pnd_Rc;
    private DbsField pnd_Pr_Plans;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        ldaAdsl400 = new LdaAdsl400();
        registerRecord(ldaAdsl400);
        ldaAdsl400a = new LdaAdsl400a();
        registerRecord(ldaAdsl400a);
        localVariables = new DbsRecord();
        pdaMdma101 = new PdaMdma101(localVariables);
        ldaAdsl401 = new LdaAdsl401();
        registerRecord(ldaAdsl401);
        registerRecord(ldaAdsl401.getVw_ads_Prtcpnt_View());
        ldaAdsl401a = new LdaAdsl401a();
        registerRecord(ldaAdsl401a);
        registerRecord(ldaAdsl401a.getVw_ads_Cntrct_View());
        pdaAdsa480 = new PdaAdsa480(localVariables);
        pdaNeca4000 = new PdaNeca4000(localVariables);
        pdaAdaa501a = new PdaAdaa501a(localVariables);
        pdaAdspda_M = new PdaAdspda_M(localVariables);
        pdaAdsa888 = new PdaAdsa888(localVariables);

        // Local Variables
        pnd_Rtb_Occurs = localVariables.newFieldInRecord("pnd_Rtb_Occurs", "#RTB-OCCURS", FieldType.PACKED_DECIMAL, 3);

        pnd_Constants = localVariables.newGroupInRecord("pnd_Constants", "#CONSTANTS");
        pnd_Constants_Pnd_Open = pnd_Constants.newFieldInGroup("pnd_Constants_Pnd_Open", "#OPEN", FieldType.STRING, 1);
        pnd_Constants_Pnd_Close = pnd_Constants.newFieldInGroup("pnd_Constants_Pnd_Close", "#CLOSE", FieldType.STRING, 1);
        pnd_Constants_Pnd_Tiaa = pnd_Constants.newFieldInGroup("pnd_Constants_Pnd_Tiaa", "#TIAA", FieldType.STRING, 10);
        pnd_Constants_Pnd_Stable = pnd_Constants.newFieldArrayInGroup("pnd_Constants_Pnd_Stable", "#STABLE", FieldType.STRING, 10, new DbsArrayController(1, 
            2));
        pnd_Constants_Pnd_Svsaa = pnd_Constants.newFieldInGroup("pnd_Constants_Pnd_Svsaa", "#SVSAA", FieldType.STRING, 10);
        pnd_Constants_Pnd_Rex1 = pnd_Constants.newFieldInGroup("pnd_Constants_Pnd_Rex1", "#REX1", FieldType.STRING, 10);
        pnd_Constants_Pnd_Rea1 = pnd_Constants.newFieldInGroup("pnd_Constants_Pnd_Rea1", "#REA1", FieldType.STRING, 10);
        pnd_Constants_Pnd_Batchads = pnd_Constants.newFieldInGroup("pnd_Constants_Pnd_Batchads", "#BATCHADS", FieldType.STRING, 8);
        pnd_Constants_Pnd_Mit_Add = pnd_Constants.newFieldInGroup("pnd_Constants_Pnd_Mit_Add", "#MIT-ADD", FieldType.STRING, 2);
        pnd_Constants_Pnd_Mit_Sub_Rqst = pnd_Constants.newFieldInGroup("pnd_Constants_Pnd_Mit_Sub_Rqst", "#MIT-SUB-RQST", FieldType.STRING, 2);
        pnd_Constants_Pnd_Mit_Update = pnd_Constants.newFieldInGroup("pnd_Constants_Pnd_Mit_Update", "#MIT-UPDATE", FieldType.STRING, 2);
        pnd_Constants_Pnd_Rstl_Usage_Cde = pnd_Constants.newFieldArrayInGroup("pnd_Constants_Pnd_Rstl_Usage_Cde", "#RSTL-USAGE-CDE", FieldType.STRING, 
            1, new DbsArrayController(1, 6));
        pnd_Constants_Pnd_R_Annty_Typ = pnd_Constants.newFieldInGroup("pnd_Constants_Pnd_R_Annty_Typ", "#R-ANNTY-TYP", FieldType.STRING, 1);
        pnd_Constants_Pnd_M_Annty_Typ = pnd_Constants.newFieldInGroup("pnd_Constants_Pnd_M_Annty_Typ", "#M-ANNTY-TYP", FieldType.STRING, 1);
        pnd_Constants_Pnd_After_Tax_Fsrce = pnd_Constants.newFieldInGroup("pnd_Constants_Pnd_After_Tax_Fsrce", "#AFTER-TAX-FSRCE", FieldType.STRING, 5);

        pnd_Constants__R_Field_1 = pnd_Constants.newGroupInGroup("pnd_Constants__R_Field_1", "REDEFINE", pnd_Constants_Pnd_After_Tax_Fsrce);
        pnd_Constants_Pnd_After_Tax_Fsrce_Array = pnd_Constants__R_Field_1.newFieldArrayInGroup("pnd_Constants_Pnd_After_Tax_Fsrce_Array", "#AFTER-TAX-FSRCE-ARRAY", 
            FieldType.STRING, 1, new DbsArrayController(1, 5));

        pnd_Constants_Pnd_Statuses = pnd_Constants.newGroupInGroup("pnd_Constants_Pnd_Statuses", "#STATUSES");
        pnd_Constants_Pnd_Original_Statuses = pnd_Constants_Pnd_Statuses.newFieldInGroup("pnd_Constants_Pnd_Original_Statuses", "#ORIGINAL-STATUSES", 
            FieldType.STRING, 18);

        pnd_Constants__R_Field_2 = pnd_Constants_Pnd_Statuses.newGroupInGroup("pnd_Constants__R_Field_2", "REDEFINE", pnd_Constants_Pnd_Original_Statuses);
        pnd_Constants_Pnd_Orig_2nd_Sight = pnd_Constants__R_Field_2.newFieldInGroup("pnd_Constants_Pnd_Orig_2nd_Sight", "#ORIG-2ND-SIGHT", FieldType.STRING, 
            3);
        pnd_Constants_Pnd_Orig_Incmplte = pnd_Constants__R_Field_2.newFieldInGroup("pnd_Constants_Pnd_Orig_Incmplte", "#ORIG-INCMPLTE", FieldType.STRING, 
            3);
        pnd_Constants_Pnd_Orig_Incmplte_Mit1 = pnd_Constants__R_Field_2.newFieldInGroup("pnd_Constants_Pnd_Orig_Incmplte_Mit1", "#ORIG-INCMPLTE-MIT1", 
            FieldType.STRING, 3);
        pnd_Constants_Pnd_Orig_Incmplte_Mit2 = pnd_Constants__R_Field_2.newFieldInGroup("pnd_Constants_Pnd_Orig_Incmplte_Mit2", "#ORIG-INCMPLTE-MIT2", 
            FieldType.STRING, 3);
        pnd_Constants_Pnd_Orig_Incmplte_Mit3 = pnd_Constants__R_Field_2.newFieldInGroup("pnd_Constants_Pnd_Orig_Incmplte_Mit3", "#ORIG-INCMPLTE-MIT3", 
            FieldType.STRING, 3);
        pnd_Constants_Pnd_Await_Omni = pnd_Constants__R_Field_2.newFieldInGroup("pnd_Constants_Pnd_Await_Omni", "#AWAIT-OMNI", FieldType.STRING, 3);

        pnd_Constants__R_Field_3 = pnd_Constants_Pnd_Statuses.newGroupInGroup("pnd_Constants__R_Field_3", "REDEFINE", pnd_Constants_Pnd_Original_Statuses);
        pnd_Constants_Pnd_Original_Stat_Array = pnd_Constants__R_Field_3.newFieldArrayInGroup("pnd_Constants_Pnd_Original_Stat_Array", "#ORIGINAL-STAT-ARRAY", 
            FieldType.STRING, 3, new DbsArrayController(1, 6));
        pnd_Constants_Pnd_Invalid_Rsttlmnt = pnd_Constants_Pnd_Statuses.newFieldInGroup("pnd_Constants_Pnd_Invalid_Rsttlmnt", "#INVALID-RSTTLMNT", FieldType.STRING, 
            3);
        pnd_Constants_Pnd_No_Match = pnd_Constants_Pnd_Statuses.newFieldInGroup("pnd_Constants_Pnd_No_Match", "#NO-MATCH", FieldType.STRING, 3);
        pnd_Constants_Pnd_Await_Process = pnd_Constants_Pnd_Statuses.newFieldInGroup("pnd_Constants_Pnd_Await_Process", "#AWAIT-PROCESS", FieldType.STRING, 
            3);
        pnd_Constants_Pnd_Incomplete_Stats = pnd_Constants_Pnd_Statuses.newFieldInGroup("pnd_Constants_Pnd_Incomplete_Stats", "#INCOMPLETE-STATS", FieldType.STRING, 
            12);

        pnd_Constants__R_Field_4 = pnd_Constants_Pnd_Statuses.newGroupInGroup("pnd_Constants__R_Field_4", "REDEFINE", pnd_Constants_Pnd_Incomplete_Stats);
        pnd_Constants_Pnd_Incmplte = pnd_Constants__R_Field_4.newFieldInGroup("pnd_Constants_Pnd_Incmplte", "#INCMPLTE", FieldType.STRING, 3);
        pnd_Constants_Pnd_Inc_Eff_Dte = pnd_Constants__R_Field_4.newFieldInGroup("pnd_Constants_Pnd_Inc_Eff_Dte", "#INC-EFF-DTE", FieldType.STRING, 3);
        pnd_Constants_Pnd_Inc_Cntrct = pnd_Constants__R_Field_4.newFieldInGroup("pnd_Constants_Pnd_Inc_Cntrct", "#INC-CNTRCT", FieldType.STRING, 3);
        pnd_Constants_Pnd_Inc_Funds = pnd_Constants__R_Field_4.newFieldInGroup("pnd_Constants_Pnd_Inc_Funds", "#INC-FUNDS", FieldType.STRING, 3);

        pnd_Constants__R_Field_5 = pnd_Constants_Pnd_Statuses.newGroupInGroup("pnd_Constants__R_Field_5", "REDEFINE", pnd_Constants_Pnd_Incomplete_Stats);
        pnd_Constants_Pnd_Incomplete_Array = pnd_Constants__R_Field_5.newFieldArrayInGroup("pnd_Constants_Pnd_Incomplete_Array", "#INCOMPLETE-ARRAY", 
            FieldType.STRING, 3, new DbsArrayController(1, 4));
        pnd_Constants_Pnd_Await_Stats = pnd_Constants_Pnd_Statuses.newFieldInGroup("pnd_Constants_Pnd_Await_Stats", "#AWAIT-STATS", FieldType.STRING, 
            12);

        pnd_Constants__R_Field_6 = pnd_Constants_Pnd_Statuses.newGroupInGroup("pnd_Constants__R_Field_6", "REDEFINE", pnd_Constants_Pnd_Await_Stats);
        pnd_Constants_Pnd_Await_2nd_Sight = pnd_Constants__R_Field_6.newFieldInGroup("pnd_Constants_Pnd_Await_2nd_Sight", "#AWAIT-2ND-SIGHT", FieldType.STRING, 
            3);
        pnd_Constants_Pnd_Awt_Eff_Dte = pnd_Constants__R_Field_6.newFieldInGroup("pnd_Constants_Pnd_Awt_Eff_Dte", "#AWT-EFF-DTE", FieldType.STRING, 3);
        pnd_Constants_Pnd_Awt_Cntrct = pnd_Constants__R_Field_6.newFieldInGroup("pnd_Constants_Pnd_Awt_Cntrct", "#AWT-CNTRCT", FieldType.STRING, 3);
        pnd_Constants_Pnd_Awt_Funds = pnd_Constants__R_Field_6.newFieldInGroup("pnd_Constants_Pnd_Awt_Funds", "#AWT-FUNDS", FieldType.STRING, 3);

        pnd_Constants__R_Field_7 = pnd_Constants_Pnd_Statuses.newGroupInGroup("pnd_Constants__R_Field_7", "REDEFINE", pnd_Constants_Pnd_Await_Stats);
        pnd_Constants_Pnd_Await_2nd_Sight_Array = pnd_Constants__R_Field_7.newFieldArrayInGroup("pnd_Constants_Pnd_Await_2nd_Sight_Array", "#AWAIT-2ND-SIGHT-ARRAY", 
            FieldType.STRING, 3, new DbsArrayController(1, 4));
        pnd_Constants_Pnd_Delayed_Stats = pnd_Constants_Pnd_Statuses.newFieldInGroup("pnd_Constants_Pnd_Delayed_Stats", "#DELAYED-STATS", FieldType.STRING, 
            9);

        pnd_Constants__R_Field_8 = pnd_Constants_Pnd_Statuses.newGroupInGroup("pnd_Constants__R_Field_8", "REDEFINE", pnd_Constants_Pnd_Delayed_Stats);
        pnd_Constants_Pnd_Delayed_Eff_Dte = pnd_Constants__R_Field_8.newFieldInGroup("pnd_Constants_Pnd_Delayed_Eff_Dte", "#DELAYED-EFF-DTE", FieldType.STRING, 
            3);
        pnd_Constants_Pnd_Delayed_Cntrct = pnd_Constants__R_Field_8.newFieldInGroup("pnd_Constants_Pnd_Delayed_Cntrct", "#DELAYED-CNTRCT", FieldType.STRING, 
            3);
        pnd_Constants_Pnd_Delayed_Funds = pnd_Constants__R_Field_8.newFieldInGroup("pnd_Constants_Pnd_Delayed_Funds", "#DELAYED-FUNDS", FieldType.STRING, 
            3);

        pnd_Constants__R_Field_9 = pnd_Constants_Pnd_Statuses.newGroupInGroup("pnd_Constants__R_Field_9", "REDEFINE", pnd_Constants_Pnd_Delayed_Stats);
        pnd_Constants_Pnd_Delayed_Stat_Array = pnd_Constants__R_Field_9.newFieldArrayInGroup("pnd_Constants_Pnd_Delayed_Stat_Array", "#DELAYED-STAT-ARRAY", 
            FieldType.STRING, 3, new DbsArrayController(1, 3));

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_I = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 5);
        pnd_Counters_Pnd_J = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 5);
        pnd_Counters_Pnd_K = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_K", "#K", FieldType.PACKED_DECIMAL, 5);
        pnd_Counters_Pnd_X = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_X", "#X", FieldType.PACKED_DECIMAL, 5);
        pnd_Counters_Pnd_A = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_A", "#A", FieldType.PACKED_DECIMAL, 5);
        pnd_Counters_Pnd_Indx = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Indx", "#INDX", FieldType.PACKED_DECIMAL, 5);
        pnd_Counters_Pnd_Total_Input_Recs = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Input_Recs", "#TOTAL-INPUT-RECS", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Omni_Recs = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Omni_Recs", "#TOTAL-OMNI-RECS", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Recs = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Recs", "#TOTAL-RECS", FieldType.PACKED_DECIMAL, 9);
        pnd_Counters_Pnd_Total_Ackl_Recs = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Ackl_Recs", "#TOTAL-ACKL-RECS", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Bad_Recs = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Bad_Recs", "#BAD-RECS", FieldType.PACKED_DECIMAL, 9);
        pnd_Counters_Pnd_Total_Omni_Rqst = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Omni_Rqst", "#TOTAL-OMNI-RQST", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Omni_Err_Cntrct = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Omni_Err_Cntrct", "#TOTAL-OMNI-ERR-CNTRCT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Total_Omni_Cntrct = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Total_Omni_Cntrct", "#TOTAL-OMNI-CNTRCT", FieldType.PACKED_DECIMAL, 
            9);

        pnd_Counters_Pnd_Status_Count = pnd_Counters.newGroupInGroup("pnd_Counters_Pnd_Status_Count", "#STATUS-COUNT");
        pnd_Counters_Pnd_No_Match_Cnt = pnd_Counters_Pnd_Status_Count.newFieldInGroup("pnd_Counters_Pnd_No_Match_Cnt", "#NO-MATCH-CNT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Full_Match_Cnt = pnd_Counters_Pnd_Status_Count.newFieldInGroup("pnd_Counters_Pnd_Full_Match_Cnt", "#FULL-MATCH-CNT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Delayed_Cnt = pnd_Counters_Pnd_Status_Count.newFieldInGroup("pnd_Counters_Pnd_Delayed_Cnt", "#DELAYED-CNT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Orig_Incomplete = pnd_Counters_Pnd_Status_Count.newFieldInGroup("pnd_Counters_Pnd_Orig_Incomplete", "#ORIG-INCOMPLETE", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Counters_Pnd_Orig_Await_2site = pnd_Counters_Pnd_Status_Count.newFieldInGroup("pnd_Counters_Pnd_Orig_Await_2site", "#ORIG-AWAIT-2SITE", FieldType.PACKED_DECIMAL, 
            9);

        pnd_Keys = localVariables.newGroupInRecord("pnd_Keys", "#KEYS");
        pnd_Keys_Pnd_Adp_Key = pnd_Keys.newFieldInGroup("pnd_Keys_Pnd_Adp_Key", "#ADP-KEY", FieldType.STRING, 31);

        pnd_Keys__R_Field_10 = pnd_Keys.newGroupInGroup("pnd_Keys__R_Field_10", "REDEFINE", pnd_Keys_Pnd_Adp_Key);
        pnd_Keys_Pnd_Key_Opn_Clsd_Ind = pnd_Keys__R_Field_10.newFieldInGroup("pnd_Keys_Pnd_Key_Opn_Clsd_Ind", "#KEY-OPN-CLSD-IND", FieldType.STRING, 1);
        pnd_Keys_Pnd_Key_Group = pnd_Keys__R_Field_10.newFieldInGroup("pnd_Keys_Pnd_Key_Group", "#KEY-GROUP", FieldType.STRING, 30);

        pnd_Keys__R_Field_11 = pnd_Keys__R_Field_10.newGroupInGroup("pnd_Keys__R_Field_11", "REDEFINE", pnd_Keys_Pnd_Key_Group);
        pnd_Keys_Pnd_Key_Pin = pnd_Keys__R_Field_11.newFieldInGroup("pnd_Keys_Pnd_Key_Pin", "#KEY-PIN", FieldType.NUMERIC, 12);
        pnd_Keys_Pnd_Key_Frst_Annt_Ssn = pnd_Keys__R_Field_11.newFieldInGroup("pnd_Keys_Pnd_Key_Frst_Annt_Ssn", "#KEY-FRST-ANNT-SSN", FieldType.NUMERIC, 
            9);
        pnd_Keys_Pnd_Key_Plan_Nbr = pnd_Keys__R_Field_11.newFieldInGroup("pnd_Keys_Pnd_Key_Plan_Nbr", "#KEY-PLAN-NBR", FieldType.STRING, 6);
        pnd_Keys_Pnd_Key_Annty_Optn = pnd_Keys__R_Field_11.newFieldInGroup("pnd_Keys_Pnd_Key_Annty_Optn", "#KEY-ANNTY-OPTN", FieldType.STRING, 2);
        pnd_Keys_Pnd_Key_Roth_Rqst_Ind = pnd_Keys__R_Field_11.newFieldInGroup("pnd_Keys_Pnd_Key_Roth_Rqst_Ind", "#KEY-ROTH-RQST-IND", FieldType.STRING, 
            1);
        pnd_Keys_Pnd_Adc_Key = pnd_Keys.newFieldInGroup("pnd_Keys_Pnd_Adc_Key", "#ADC-KEY", FieldType.STRING, 45);

        pnd_Keys__R_Field_12 = pnd_Keys.newGroupInGroup("pnd_Keys__R_Field_12", "REDEFINE", pnd_Keys_Pnd_Adc_Key);
        pnd_Keys_Pnd_Key_Rqst_Id = pnd_Keys__R_Field_12.newFieldInGroup("pnd_Keys_Pnd_Key_Rqst_Id", "#KEY-RQST-ID", FieldType.STRING, 35);
        pnd_Keys_Pnd_Key_Tiaa_Nbr = pnd_Keys__R_Field_12.newFieldInGroup("pnd_Keys_Pnd_Key_Tiaa_Nbr", "#KEY-TIAA-NBR", FieldType.STRING, 10);
        pnd_Keys_Pnd_Cor_Cntrct_Key1 = pnd_Keys.newFieldInGroup("pnd_Keys_Pnd_Cor_Cntrct_Key1", "#COR-CNTRCT-KEY1", FieldType.STRING, 14);

        pnd_Keys__R_Field_13 = pnd_Keys.newGroupInGroup("pnd_Keys__R_Field_13", "REDEFINE", pnd_Keys_Pnd_Cor_Cntrct_Key1);
        pnd_Keys_Pnd_Cor_Key1_Pin_Nbr = pnd_Keys__R_Field_13.newFieldInGroup("pnd_Keys_Pnd_Cor_Key1_Pin_Nbr", "#COR-KEY1-PIN-NBR", FieldType.NUMERIC, 
            12);
        pnd_Keys_Pnd_Cor_Key1_Rec_Typ = pnd_Keys__R_Field_13.newFieldInGroup("pnd_Keys_Pnd_Cor_Key1_Rec_Typ", "#COR-KEY1-REC-TYP", FieldType.NUMERIC, 
            2);
        pnd_Keys_Pnd_Key_Tiaa_Cntrct = pnd_Keys.newFieldInGroup("pnd_Keys_Pnd_Key_Tiaa_Cntrct", "#KEY-TIAA-CNTRCT", FieldType.STRING, 10);
        pnd_Keys_Pnd_Key_Usage_Cde = pnd_Keys.newFieldInGroup("pnd_Keys_Pnd_Key_Usage_Cde", "#KEY-USAGE-CDE", FieldType.STRING, 1);

        pnd_Terminate = localVariables.newGroupInRecord("pnd_Terminate", "#TERMINATE");
        pnd_Terminate_Pnd_Terminate_No = pnd_Terminate.newFieldInGroup("pnd_Terminate_Pnd_Terminate_No", "#TERMINATE-NO", FieldType.PACKED_DECIMAL, 3);
        pnd_Terminate_Pnd_Terminate_Msg = pnd_Terminate.newFieldInGroup("pnd_Terminate_Pnd_Terminate_Msg", "#TERMINATE-MSG", FieldType.STRING, 177);

        pnd_Terminate__R_Field_14 = pnd_Terminate.newGroupInGroup("pnd_Terminate__R_Field_14", "REDEFINE", pnd_Terminate_Pnd_Terminate_Msg);
        pnd_Terminate_Pnd_Terminate_Msg1 = pnd_Terminate__R_Field_14.newFieldInGroup("pnd_Terminate_Pnd_Terminate_Msg1", "#TERMINATE-MSG1", FieldType.STRING, 
            59);
        pnd_Terminate_Pnd_Terminate_Msg2 = pnd_Terminate__R_Field_14.newFieldInGroup("pnd_Terminate_Pnd_Terminate_Msg2", "#TERMINATE-MSG2", FieldType.STRING, 
            59);
        pnd_Terminate_Pnd_Terminate_Msg3 = pnd_Terminate__R_Field_14.newFieldInGroup("pnd_Terminate_Pnd_Terminate_Msg3", "#TERMINATE-MSG3", FieldType.STRING, 
            59);

        pnd_Adsn998_Error_Handler = localVariables.newGroupInRecord("pnd_Adsn998_Error_Handler", "#ADSN998-ERROR-HANDLER");
        pnd_Adsn998_Error_Handler_Pnd_Calling_Program = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Calling_Program", "#CALLING-PROGRAM", 
            FieldType.STRING, 8);
        pnd_Adsn998_Error_Handler_Pnd_Unique_Id = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Unique_Id", "#UNIQUE-ID", FieldType.NUMERIC, 
            12);
        pnd_Adsn998_Error_Handler_Pnd_Step_Name = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Step_Name", "#STEP-NAME", FieldType.STRING, 
            8);
        pnd_Adsn998_Error_Handler_Pnd_Appl_Id = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Appl_Id", "#APPL-ID", FieldType.STRING, 
            8);
        pnd_Adsn998_Error_Handler_Pnd_Error_Msg = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 
            79);
        pnd_Roth_Request = localVariables.newFieldInRecord("pnd_Roth_Request", "#ROTH-REQUEST", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Type = localVariables.newFieldInRecord("pnd_Tiaa_Type", "#TIAA-TYPE", FieldType.BOOLEAN, 1);
        pnd_Rex1_Type = localVariables.newFieldInRecord("pnd_Rex1_Type", "#REX1-TYPE", FieldType.BOOLEAN, 1);

        pnd_Per_Request_Info = localVariables.newGroupInRecord("pnd_Per_Request_Info", "#PER-REQUEST-INFO");
        pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found", "#PRTCPNT-MATCH-FOUND", 
            FieldType.BOOLEAN, 1);
        pnd_Per_Request_Info_Pnd_New_Request = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_New_Request", "#NEW-REQUEST", FieldType.BOOLEAN, 
            1);
        pnd_Per_Request_Info_Pnd_Unmatched_Rstlmt = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_Unmatched_Rstlmt", "#UNMATCHED-RSTLMT", 
            FieldType.BOOLEAN, 1);
        pnd_Per_Request_Info_Pnd_Resettlement = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_Resettlement", "#RESETTLEMENT", FieldType.BOOLEAN, 
            1);
        pnd_Per_Request_Info_Pnd_Processed_Omni_Rqst = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_Processed_Omni_Rqst", "#PROCESSED-OMNI-RQST", 
            FieldType.BOOLEAN, 1);
        pnd_Per_Request_Info_Pnd_Processed_Rsttlmnt = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_Processed_Rsttlmnt", "#PROCESSED-RSTTLMNT", 
            FieldType.BOOLEAN, 1);
        pnd_Per_Request_Info_Pnd_Amt_More_Than_Orig = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_Amt_More_Than_Orig", "#AMT-MORE-THAN-ORIG", 
            FieldType.BOOLEAN, 1);
        pnd_Per_Request_Info_Pnd_Rstlmnt_Ctr = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_Rstlmnt_Ctr", "#RSTLMNT-CTR", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Per_Request_Info_Pnd_Original_Amt = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_Original_Amt", "#ORIGINAL-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Per_Request_Info_Pnd_Error_Cntrct = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_Error_Cntrct", "#ERROR-CNTRCT", FieldType.STRING, 
            10);
        pnd_Per_Request_Info_Pnd_Omni_Cntrct = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_Omni_Cntrct", "#OMNI-CNTRCT", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Per_Request_Info_Pnd_Adas_Cntrct = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_Adas_Cntrct", "#ADAS-CNTRCT", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Per_Request_Info_Pnd_Overall_Stat = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_Overall_Stat", "#OVERALL-STAT", FieldType.STRING, 
            3);
        pnd_Per_Request_Info_Pnd_Rstl_Cnt = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_Rstl_Cnt", "#RSTL-CNT", FieldType.STRING, 1);
        pnd_Per_Request_Info_Pnd_All_Tiaa_Grd_Pct = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_All_Tiaa_Grd_Pct", "#ALL-TIAA-GRD-PCT", 
            FieldType.NUMERIC, 3);
        pnd_Per_Request_Info_Pnd_All_Cref_Mon_Pct = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_All_Cref_Mon_Pct", "#ALL-CREF-MON-PCT", 
            FieldType.NUMERIC, 3);
        pnd_Per_Request_Info_Pnd_Last_Cntrct_Seq = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_Last_Cntrct_Seq", "#LAST-CNTRCT-SEQ", 
            FieldType.NUMERIC, 2);
        pnd_Per_Request_Info_Pnd_Total_Pin_Rqst = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_Total_Pin_Rqst", "#TOTAL-PIN-RQST", FieldType.NUMERIC, 
            3);
        pnd_Per_Request_Info_Pnd_Plan_Nbr = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_Plan_Nbr", "#PLAN-NBR", FieldType.STRING, 6);
        pnd_Per_Request_Info_Pnd_Rqst_Id = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 35);

        pnd_Per_Request_Info__R_Field_15 = pnd_Per_Request_Info.newGroupInGroup("pnd_Per_Request_Info__R_Field_15", "REDEFINE", pnd_Per_Request_Info_Pnd_Rqst_Id);
        pnd_Per_Request_Info_Pnd_Rqst_Pin = pnd_Per_Request_Info__R_Field_15.newFieldInGroup("pnd_Per_Request_Info_Pnd_Rqst_Pin", "#RQST-PIN", FieldType.NUMERIC, 
            12);
        pnd_Per_Request_Info_Pnd_Rqst_Effctv_Dte = pnd_Per_Request_Info__R_Field_15.newFieldInGroup("pnd_Per_Request_Info_Pnd_Rqst_Effctv_Dte", "#RQST-EFFCTV-DTE", 
            FieldType.STRING, 8);
        pnd_Per_Request_Info_Pnd_Rqst_Entry_Dte = pnd_Per_Request_Info__R_Field_15.newFieldInGroup("pnd_Per_Request_Info_Pnd_Rqst_Entry_Dte", "#RQST-ENTRY-DTE", 
            FieldType.STRING, 8);
        pnd_Per_Request_Info_Pnd_Rqst_Entry_Tme = pnd_Per_Request_Info__R_Field_15.newFieldInGroup("pnd_Per_Request_Info_Pnd_Rqst_Entry_Tme", "#RQST-ENTRY-TME", 
            FieldType.STRING, 7);

        pnd_Per_Request_Info__R_Field_16 = pnd_Per_Request_Info__R_Field_15.newGroupInGroup("pnd_Per_Request_Info__R_Field_16", "REDEFINE", pnd_Per_Request_Info_Pnd_Rqst_Entry_Tme);
        pnd_Per_Request_Info_Pnd_Rqst_Entry_Tme_N = pnd_Per_Request_Info__R_Field_16.newFieldInGroup("pnd_Per_Request_Info_Pnd_Rqst_Entry_Tme_N", "#RQST-ENTRY-TME-N", 
            FieldType.NUMERIC, 7);
        pnd_Per_Request_Info_Pnd_Resettl_Orig_Rqstid = pnd_Per_Request_Info.newFieldInGroup("pnd_Per_Request_Info_Pnd_Resettl_Orig_Rqstid", "#RESETTL-ORIG-RQSTID", 
            FieldType.STRING, 35);

        pnd_Per_Request_Info_Pnd_Match_Conditions = pnd_Per_Request_Info.newGroupInGroup("pnd_Per_Request_Info_Pnd_Match_Conditions", "#MATCH-CONDITIONS");
        pnd_Per_Request_Info_Pnd_Full_Match = pnd_Per_Request_Info_Pnd_Match_Conditions.newFieldInGroup("pnd_Per_Request_Info_Pnd_Full_Match", "#FULL-MATCH", 
            FieldType.NUMERIC, 3);
        pnd_Per_Request_Info_Pnd_Partial_Match = pnd_Per_Request_Info_Pnd_Match_Conditions.newFieldInGroup("pnd_Per_Request_Info_Pnd_Partial_Match", "#PARTIAL-MATCH", 
            FieldType.NUMERIC, 3);
        pnd_Per_Request_Info_Pnd_Unmatched = pnd_Per_Request_Info_Pnd_Match_Conditions.newFieldInGroup("pnd_Per_Request_Info_Pnd_Unmatched", "#UNMATCHED", 
            FieldType.NUMERIC, 3);

        pnd_Adsn401 = localVariables.newGroupInRecord("pnd_Adsn401", "#ADSN401");
        pnd_Adsn401_Pnd_Cntl_Bsnss_Prev_Dte = pnd_Adsn401.newFieldInGroup("pnd_Adsn401_Pnd_Cntl_Bsnss_Prev_Dte", "#CNTL-BSNSS-PREV-DTE", FieldType.DATE);
        pnd_Adsn401_Pnd_Cntl_Bsnss_Dte = pnd_Adsn401.newFieldInGroup("pnd_Adsn401_Pnd_Cntl_Bsnss_Dte", "#CNTL-BSNSS-DTE", FieldType.DATE);
        pnd_Adsn401_Pnd_Cntl_Bsnss_Tmrrw_Dte = pnd_Adsn401.newFieldInGroup("pnd_Adsn401_Pnd_Cntl_Bsnss_Tmrrw_Dte", "#CNTL-BSNSS-TMRRW-DTE", FieldType.DATE);
        pnd_Adsn401_Pnd_Cct_Rcrd_Cde = pnd_Adsn401.newFieldInGroup("pnd_Adsn401_Pnd_Cct_Rcrd_Cde", "#CCT-RCRD-CDE", FieldType.STRING, 2);

        pnd_Misc_Variables = localVariables.newGroupInRecord("pnd_Misc_Variables", "#MISC-VARIABLES");
        pnd_Misc_Variables_Pnd_Eff_Date_D = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Eff_Date_D", "#EFF-DATE-D", FieldType.DATE);
        pnd_Misc_Variables_Pnd_File_Date_D = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_File_Date_D", "#FILE-DATE-D", FieldType.DATE);
        pnd_Misc_Variables_Pnd_Wk_Date = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Wk_Date", "#WK-DATE", FieldType.NUMERIC, 8);

        pnd_Misc_Variables__R_Field_17 = pnd_Misc_Variables.newGroupInGroup("pnd_Misc_Variables__R_Field_17", "REDEFINE", pnd_Misc_Variables_Pnd_Wk_Date);
        pnd_Misc_Variables_Pnd_Wk_Date_A = pnd_Misc_Variables__R_Field_17.newFieldInGroup("pnd_Misc_Variables_Pnd_Wk_Date_A", "#WK-DATE-A", FieldType.STRING, 
            8);

        pnd_Misc_Variables__R_Field_18 = pnd_Misc_Variables.newGroupInGroup("pnd_Misc_Variables__R_Field_18", "REDEFINE", pnd_Misc_Variables_Pnd_Wk_Date);
        pnd_Misc_Variables_Pnd_Wk_Dte_Cc = pnd_Misc_Variables__R_Field_18.newFieldInGroup("pnd_Misc_Variables_Pnd_Wk_Dte_Cc", "#WK-DTE-CC", FieldType.STRING, 
            2);
        pnd_Misc_Variables_Pnd_Wk_Dte_Yy = pnd_Misc_Variables__R_Field_18.newFieldInGroup("pnd_Misc_Variables_Pnd_Wk_Dte_Yy", "#WK-DTE-YY", FieldType.STRING, 
            2);
        pnd_Misc_Variables_Pnd_Wk_Dte_Mm = pnd_Misc_Variables__R_Field_18.newFieldInGroup("pnd_Misc_Variables_Pnd_Wk_Dte_Mm", "#WK-DTE-MM", FieldType.STRING, 
            2);
        pnd_Misc_Variables_Pnd_Wk_Dte_Dd = pnd_Misc_Variables__R_Field_18.newFieldInGroup("pnd_Misc_Variables_Pnd_Wk_Dte_Dd", "#WK-DTE-DD", FieldType.STRING, 
            2);
        pnd_Misc_Variables_Pnd_Process_Date = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Process_Date", "#PROCESS-DATE", FieldType.STRING, 
            8);

        pnd_Misc_Variables__R_Field_19 = pnd_Misc_Variables.newGroupInGroup("pnd_Misc_Variables__R_Field_19", "REDEFINE", pnd_Misc_Variables_Pnd_Process_Date);
        pnd_Misc_Variables_Pnd_Process_Date_D = pnd_Misc_Variables__R_Field_19.newFieldInGroup("pnd_Misc_Variables_Pnd_Process_Date_D", "#PROCESS-DATE-D", 
            FieldType.DATE);
        pnd_Misc_Variables_Pnd_Flip_Date = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Flip_Date", "#FLIP-DATE", FieldType.STRING, 10);

        pnd_Misc_Variables__R_Field_20 = pnd_Misc_Variables.newGroupInGroup("pnd_Misc_Variables__R_Field_20", "REDEFINE", pnd_Misc_Variables_Pnd_Flip_Date);
        pnd_Misc_Variables_Pnd_Flip_Date_D = pnd_Misc_Variables__R_Field_20.newFieldInGroup("pnd_Misc_Variables_Pnd_Flip_Date_D", "#FLIP-DATE-D", FieldType.DATE);
        pnd_Misc_Variables_Pnd_Bad_Roth_Data = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Bad_Roth_Data", "#BAD-ROTH-DATA", FieldType.STRING, 
            1);
        pnd_Misc_Variables_Pnd_Day_Of_Week = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Day_Of_Week", "#DAY-OF-WEEK", FieldType.STRING, 
            9);
        pnd_Misc_Variables_Pnd_Invalid_File_Date = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Invalid_File_Date", "#INVALID-FILE-DATE", 
            FieldType.BOOLEAN, 1);
        pnd_Misc_Variables_Pnd_Acct_Cde_Space = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Acct_Cde_Space", "#ACCT-CDE-SPACE", FieldType.STRING, 
            1);
        pnd_Misc_Variables_Pnd_Contract_Space = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Contract_Space", "#CONTRACT-SPACE", FieldType.STRING, 
            10);
        pnd_Misc_Variables_Pnd_Omni_Rate_Code = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Omni_Rate_Code", "#OMNI-RATE-CODE", FieldType.STRING, 
            2);
        pnd_Misc_Variables_Pnd_Omni_Lob = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Omni_Lob", "#OMNI-LOB", FieldType.STRING, 10);
        pnd_Misc_Variables_Pnd_Rb_Return_Code = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Rb_Return_Code", "#RB-RETURN-CODE", FieldType.STRING, 
            2);
        pnd_Misc_Variables_Pnd_Mit_Audit_Trail = pnd_Misc_Variables.newFieldArrayInGroup("pnd_Misc_Variables_Pnd_Mit_Audit_Trail", "#MIT-AUDIT-TRAIL", 
            FieldType.STRING, 80, new DbsArrayController(1, 60));
        pnd_Misc_Variables_Pnd_Adas_Funds_Exist = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Adas_Funds_Exist", "#ADAS-FUNDS-EXIST", FieldType.BOOLEAN, 
            1);
        pnd_Misc_Variables_Pnd_Unmatched_Funds = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Unmatched_Funds", "#UNMATCHED-FUNDS", FieldType.BOOLEAN, 
            1);
        pnd_Misc_Variables_Pnd_Pin_Okay = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Pin_Okay", "#PIN-OKAY", FieldType.BOOLEAN, 1);
        pnd_Misc_Variables_Pnd_Omni_Format_Ok = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Omni_Format_Ok", "#OMNI-FORMAT-OK", FieldType.BOOLEAN, 
            1);
        pnd_Misc_Variables_Pnd_Report_Msg = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Report_Msg", "#REPORT-MSG", FieldType.STRING, 73);
        pnd_Misc_Variables_Pnd_Report_Rqst_Type = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Report_Rqst_Type", "#REPORT-RQST-TYPE", FieldType.STRING, 
            1);
        pnd_Misc_Variables_Pnd_Debug_On = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Debug_On", "#DEBUG-ON", FieldType.BOOLEAN, 1);
        pnd_S_Err_Cntrct = localVariables.newFieldInRecord("pnd_S_Err_Cntrct", "#S-ERR-CNTRCT", FieldType.STRING, 10);
        pnd_Rqst_Found = localVariables.newFieldInRecord("pnd_Rqst_Found", "#RQST-FOUND", FieldType.BOOLEAN, 1);
        pnd_Acct_Ndx = localVariables.newFieldInRecord("pnd_Acct_Ndx", "#ACCT-NDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Acct_Ndx1 = localVariables.newFieldInRecord("pnd_Acct_Ndx1", "#ACCT-NDX1", FieldType.PACKED_DECIMAL, 3);
        pnd_Number = localVariables.newFieldInRecord("pnd_Number", "#NUMBER", FieldType.PACKED_DECIMAL, 3);
        pnd_S_Omni_Contract = localVariables.newFieldInRecord("pnd_S_Omni_Contract", "#S-OMNI-CONTRACT", FieldType.STRING, 10);
        pnd_Rec_Has_Data = localVariables.newFieldInRecord("pnd_Rec_Has_Data", "#REC-HAS-DATA", FieldType.BOOLEAN, 1);

        vw_ads_Part = new DataAccessProgramView(new NameInfo("vw_ads_Part", "ADS-PART"), "ADS_PRTCPNT", "ADS_PRTCPNT");
        ads_Part_Rqst_Id = vw_ads_Part.getRecord().newFieldInGroup("ads_Part_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, "RQST_ID");
        ads_Part_Adp_Annt_Typ_Cde = vw_ads_Part.getRecord().newFieldInGroup("ads_Part_Adp_Annt_Typ_Cde", "ADP-ANNT-TYP-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ADP_ANNT_TYP_CDE");
        registerRecord(vw_ads_Part);

        pnd_Rqst_Cnt = localVariables.newFieldInRecord("pnd_Rqst_Cnt", "#RQST-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_Pr_Plans = localVariables.newFieldArrayInRecord("pnd_Pr_Plans", "#PR-PLANS", FieldType.STRING, 7, new DbsArrayController(1, 8));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Part.reset();

        ldaAdsl400.initializeValues();
        ldaAdsl400a.initializeValues();
        ldaAdsl401.initializeValues();
        ldaAdsl401a.initializeValues();

        localVariables.reset();
        pnd_Rtb_Occurs.setInitialValue(14);
        pnd_Constants_Pnd_Open.setInitialValue("O");
        pnd_Constants_Pnd_Close.setInitialValue("C");
        pnd_Constants_Pnd_Tiaa.setInitialValue("TIA0");
        pnd_Constants_Pnd_Stable.getValue(1).setInitialValue("TSA0");
        pnd_Constants_Pnd_Stable.getValue(2).setInitialValue("TSV0");
        pnd_Constants_Pnd_Svsaa.setInitialValue("TSV0");
        pnd_Constants_Pnd_Rex1.setInitialValue("REX1");
        pnd_Constants_Pnd_Rea1.setInitialValue("REA1");
        pnd_Constants_Pnd_Batchads.setInitialValue("BATCHADS");
        pnd_Constants_Pnd_Mit_Add.setInitialValue("AR");
        pnd_Constants_Pnd_Mit_Sub_Rqst.setInitialValue("AS");
        pnd_Constants_Pnd_Mit_Update.setInitialValue("MO");
        pnd_Constants_Pnd_Rstl_Usage_Cde.getValue(1).setInitialValue(" ");
        pnd_Constants_Pnd_Rstl_Usage_Cde.getValue(2).setInitialValue("1");
        pnd_Constants_Pnd_Rstl_Usage_Cde.getValue(3).setInitialValue("2");
        pnd_Constants_Pnd_Rstl_Usage_Cde.getValue(4).setInitialValue("3");
        pnd_Constants_Pnd_Rstl_Usage_Cde.getValue(5).setInitialValue("4");
        pnd_Constants_Pnd_Rstl_Usage_Cde.getValue(6).setInitialValue("C");
        pnd_Constants_Pnd_R_Annty_Typ.setInitialValue("R");
        pnd_Constants_Pnd_M_Annty_Typ.setInitialValue("M");
        pnd_Constants_Pnd_After_Tax_Fsrce.setInitialValue("GHKQ3");
        pnd_Constants_Pnd_Original_Statuses.setInitialValue("F30E20E26E27E28M05");
        pnd_Constants_Pnd_Invalid_Rsttlmnt.setInitialValue("L70");
        pnd_Constants_Pnd_No_Match.setInitialValue("E25");
        pnd_Constants_Pnd_Await_Process.setInitialValue("M10");
        pnd_Constants_Pnd_Incomplete_Stats.setInitialValue("E21E22E23E24");
        pnd_Constants_Pnd_Await_Stats.setInitialValue("D01D02D03D04");
        pnd_Constants_Pnd_Delayed_Stats.setInitialValue("D05D06D07");
        pnd_Pr_Plans.getValue(1).setInitialValue("151628");
        pnd_Pr_Plans.getValue(2).setInitialValue("151630");
        pnd_Pr_Plans.getValue(3).setInitialValue("151632");
        pnd_Pr_Plans.getValue(4).setInitialValue("151633");
        pnd_Pr_Plans.getValue(5).setInitialValue("151635");
        pnd_Pr_Plans.getValue(6).setInitialValue("151636");
        pnd_Pr_Plans.getValue(7).setInitialValue("329757");
        pnd_Pr_Plans.getValue(8).setInitialValue("356017");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp400() throws Exception
    {
        super("Adsp400");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Adsp400|Main");
        OnErrorManager.pushEvent("ADSP400", onError);
        setupReports();
        while(true)
        {
            try
            {
                //*  >>> TO RUN JOBS WITH DE-BUG DISPLAYS, SET DEBUG-ON PARM IN JCL <<<
                //*  ==================================================================
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Misc_Variables_Pnd_Debug_On);                                                                      //Natural: INPUT #DEBUG-ON
                //*  ==================================================================
                if (Global.isEscape()) return;                                                                                                                            //Natural: FORMAT ( 1 ) PS = 60 LS = 132 ZP = OFF SG = OFF;//Natural: WRITE ( 1 ) TITLE LEFT JUSTIFIED 001T 'ADSP400' 050T 'ANNUITIZATION OF OMNI PLUS CONTRACTS' 110T 'Run Date:' ( BLI ) 120T *DATX ( AD = OD CD = NE ) / 050T 'OMNI-ADAS MATCH PROCESS ERROR REPORT' 110T 'Page No :' 120T *PAGE-NUMBER ( AD = OD CD = NE ZP = ON ) // 003T '  OMNI REC' 017T '  PIN  ' 032T '  TIAA   ' 045T 'EFFECTIVE' 055T 'REQUEST' 064T '                 ERROR MESSAGES' / 003T '   NUMBER' 032T 'CONTRACT' 048T '  DATE' 055T ' TYPE ' //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 1 ) TRAILER LEFT JUSTIFIED //// 35T ' >>>> TOTAL CONTRACTS WITH ERRORS : ' #TOTAL-OMNI-ERR-CNTRCT '   <<<<'
                //*  CM -FOR CALLS TO MDM
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
                sub_Open_Mq();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //* *RESET #EXTERNAL-DATA(*)  /* OS-102013
                                                                                                                                                                          //Natural: PERFORM GET-FUND-TICKERS
                sub_Get_Fund_Tickers();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADSN402-READ-CONTROL-REC
                sub_Adsn402_Read_Control_Rec();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //* *********************************************************************
                //*                      M A I N   R O U T I N E
                //* *********************************************************************
                pnd_Keys_Pnd_Key_Opn_Clsd_Ind.setValue(pnd_Constants_Pnd_Open);                                                                                           //Natural: MOVE #OPEN TO #KEY-OPN-CLSD-IND
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 1 #OMNI-INPUT ( * )
                while (condition(getWorkFiles().read(1, ldaAdsl400.getPnd_Omni_Input().getValue("*"))))
                {
                    //*  06302006 MN  START
                    pnd_Counters_Pnd_Total_Input_Recs.nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTAL-INPUT-RECS
                    //*  IF #OMNI-ACKL-REC-ID = 'ACKL'           /*  PIN EXPANSION 02222017
                    //*    ADD 1 TO #TOTAL-ACKL-RECS
                    //*    ESCAPE TOP
                    //*  END-IF
                    //*  06302006 MN  END
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Pin_A());                                                                             //Natural: WRITE '=' #OMNI-PIN-A
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Ssn());                                                                               //Natural: WRITE '=' #OMNI-SSN
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Eff_Dte());                                                                           //Natural: WRITE '=' #OMNI-EFF-DTE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Annty_Optn());                                                                        //Natural: WRITE '=' #OMNI-ANNTY-OPTN
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Plan_Nbr());                                                                          //Natural: WRITE '=' #OMNI-PLAN-NBR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Emp_Term_Dte());                                                                      //Natural: WRITE '=' #OMNI-EMP-TERM-DTE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct());                                                                       //Natural: WRITE '=' #OMNI-TIAA-CNTRCT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Cref_Cntrct());                                                                       //Natural: WRITE '=' #OMNI-CREF-CNTRCT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Issue_State());                                                                       //Natural: WRITE '=' #OMNI-ISSUE-STATE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Issue_Dte());                                                                         //Natural: WRITE '=' #OMNI-ISSUE-DTE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Ticker());                                                                       //Natural: WRITE '=' #OMNI-FUND-TICKER
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Typ());                                                                          //Natural: WRITE '=' #OMNI-FUND-TYP
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Contract_Id());                                                                       //Natural: WRITE '=' #OMNI-CONTRACT-ID
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt());                                                                //Natural: WRITE '=' #OMNI-FUND-OPN-ACCUM-AMT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt());                                                                    //Natural: WRITE '=' #OMNI-FUND-SETTL-AMT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt());                                                                      //Natural: WRITE '=' #OMNI-FUND-IVC-AMT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts());                                                               //Natural: WRITE '=' #OMNI-FUND-OPN-ACCUM-UNTS
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts());                                                                   //Natural: WRITE '=' #OMNI-FUND-SETTL-UNTS
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt());                                                               //Natural: WRITE '=' #OMNI-FUND-RATE-SETTL-AMT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Usage_Cde1());                                                                        //Natural: WRITE '=' #OMNI-USAGE-CDE1
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Usage_Cde2());                                                                        //Natural: WRITE '=' #OMNI-USAGE-CDE2
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Source());                                                                       //Natural: WRITE '=' #OMNI-FUND-SOURCE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Sub_Plan_Nbr());                                                                      //Natural: WRITE '=' #OMNI-SUB-PLAN-NBR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_File_Dte());                                                                          //Natural: WRITE '=' #OMNI-FILE-DTE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt());                                                            //Natural: WRITE '=' #OMNI-TPA-GUARANT-COMMUT-AMT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Orig_Cntrct_Lob_Ind());                                                               //Natural: WRITE '=' #OMNI-ORIG-CNTRCT-LOB-IND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt());                                                                   //Natural: WRITE '=' #OMNI-TPA-PAYMENT-CNT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  EM 072811
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Inv_Short_Name());                                                                    //Natural: WRITE '=' #OMNI-INV-SHORT-NAME
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  RS0 START
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Rqst_Ind());                                                                     //Natural: WRITE '=' #OMNI-ROTH-RQST-IND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Plan_Type());                                                                    //Natural: WRITE '=' #OMNI-ROTH-PLAN-TYPE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte());                                                             //Natural: WRITE '=' #OMNI-ROTH-FRST-CONTRIB-DTE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte());                                                                   //Natural: WRITE '=' #OMNI-ROTH-DSBLTY-DTE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Tot_Contrib_Amt());                                                              //Natural: WRITE '=' #OMNI-ROTH-TOT-CONTRIB-AMT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  RS0 END
                    getReports().write(0, "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt());                                                            //Natural: WRITE '=' #OMNI-ROTH-CONTRIB-SETTL-AMT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  RS0
                    if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Rqst_Ind().equals("Y")))                                                                     //Natural: IF #OMNI-ROTH-RQST-IND = 'Y'
                    {
                        pnd_Roth_Request.setValue(true);                                                                                                                  //Natural: MOVE TRUE TO #ROTH-REQUEST
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Roth_Request.setValue(false);                                                                                                                 //Natural: MOVE FALSE TO #ROTH-REQUEST
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Counters_Pnd_Total_Omni_Recs.nadd(1);                                                                                                             //Natural: ADD 1 TO #TOTAL-OMNI-RECS
                    //*  RS0
                    pnd_Counters_Pnd_Total_Recs.nadd(1);                                                                                                                  //Natural: ADD 1 TO #TOTAL-RECS
                                                                                                                                                                          //Natural: PERFORM CHECK-OMNI-DATA-FORMAT
                    sub_Check_Omni_Data_Format();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*     >> DO NOT PROCESS OMNI RECORDS WITH INVALID FORMAT <<
                    //*     --------------------------------------------------
                    if (condition(! (pnd_Misc_Variables_Pnd_Omni_Format_Ok.getBoolean())))                                                                                //Natural: IF NOT #OMNI-FORMAT-OK
                    {
                        if (condition(pnd_Misc_Variables_Pnd_Debug_On.getBoolean()))                                                                                      //Natural: IF #DEBUG-ON
                        {
                            getReports().write(0, NEWLINE,"*","....AFTER CHECK-OMNI-DATA-FORMAT ","=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Pin_A(),"=",                  //Natural: WRITE / '*' '....AFTER CHECK-OMNI-DATA-FORMAT ' '=' #OMNI-PIN-A '=' #OMNI-TIAA-CNTRCT '=' #OMNI-CONTRACT-ID '=' #OMNI-LOB '=' #OMNI-RATE-CODE '=' #RB-RETURN-CODE '=' ADAA501A-RETURN-CODE '=' NECA4000.RETURN-CDE '=' #OMNI-FORMAT-OK '=' #TOTAL-OMNI-RECS
                                ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct(),"=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Contract_Id(),"=",pnd_Misc_Variables_Pnd_Omni_Lob,
                                "=",pnd_Misc_Variables_Pnd_Omni_Rate_Code,"=",pnd_Misc_Variables_Pnd_Rb_Return_Code,"=",pdaAdaa501a.getAdaa501a_Linkage_Adaa501a_Return_Code(),
                                "=",pdaNeca4000.getNeca4000_Return_Cde(),"=",pnd_Misc_Variables_Pnd_Omni_Format_Ok,"=",pnd_Counters_Pnd_Total_Omni_Recs);
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        //*   09272005 MN
                        pnd_Counters_Pnd_Bad_Recs.nadd(1);                                                                                                                //Natural: ADD 1 TO #BAD-RECS
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR-REPORT
                        sub_Print_Error_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*  IF THE FIRST RECORD IS REJECTED
                        //*  RS0
                        if (condition(pnd_Counters_Pnd_Total_Omni_Recs.equals(1)))                                                                                        //Natural: IF #TOTAL-OMNI-RECS = 1
                        {
                            pnd_Counters_Pnd_Total_Omni_Recs.nsubtract(1);                                                                                                //Natural: SUBTRACT 1 FROM #TOTAL-OMNI-RECS
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                        //*   PIN EXPANSION 02222017
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Keys_Pnd_Key_Pin.compute(new ComputeParameters(false, pnd_Keys_Pnd_Key_Pin), ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Pin_A().val());                //Natural: ASSIGN #KEY-PIN := VAL ( #OMNI-PIN-A )
                    pnd_Keys_Pnd_Key_Frst_Annt_Ssn.setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Ssn());                                                                 //Natural: MOVE #OMNI-SSN TO #KEY-FRST-ANNT-SSN
                    pnd_Keys_Pnd_Key_Plan_Nbr.setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Plan_Nbr());                                                                 //Natural: MOVE #OMNI-PLAN-NBR TO #KEY-PLAN-NBR
                    pnd_Keys_Pnd_Key_Annty_Optn.setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Annty_Optn());                                                             //Natural: MOVE #OMNI-ANNTY-OPTN TO #KEY-ANNTY-OPTN
                    pnd_Keys_Pnd_Key_Tiaa_Cntrct.setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct());                                                           //Natural: MOVE #OMNI-TIAA-CNTRCT TO #KEY-TIAA-CNTRCT
                    pnd_Keys_Pnd_Key_Usage_Cde.setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Usage_Cde2());                                                              //Natural: MOVE #OMNI-USAGE-CDE2 TO #KEY-USAGE-CDE
                    //*  RS0
                    pnd_Keys_Pnd_Key_Roth_Rqst_Ind.setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Rqst_Ind());                                                       //Natural: MOVE #OMNI-ROTH-RQST-IND TO #KEY-ROTH-RQST-IND
                    //*     >> IDENTIFY NEW REQUEST <<
                    //*     --------------------------
                    if (condition(((pnd_Keys_Pnd_Key_Group.isBreak() || pnd_Keys_Pnd_Key_Usage_Cde.isBreak()) || ((pnd_Counters_Pnd_Total_Omni_Recs.equals(1)             //Natural: IF ( BREAK OF #KEY-GROUP ) OR ( BREAK OF #KEY-USAGE-CDE ) OR ( #TOTAL-OMNI-RECS EQ 1 ) OR ( BREAK OF #KEY-TIAA-CNTRCT AND #KEY-USAGE-CDE EQ #RSTL-USAGE-CDE ( 1:3 ) ) OR ( BREAK OF #KEY-TIAA-CNTRCT AND #KEY-USAGE-CDE EQ #RSTL-USAGE-CDE ( 3:6 ) AND #PRTCPNT-MATCH-FOUND AND #TOTAL-PIN-RQST GT 1 AND NOT #KEY-TIAA-CNTRCT EQ #PRTCPNT.ADP-CNTRCTS-IN-RQST ( * ) )
                        || (pnd_Counters_Pnd_Total_Omni_Recs.equals(pnd_Keys_Pnd_Key_Tiaa_Cntrct.isBreak()) && pnd_Keys_Pnd_Key_Usage_Cde.equals(pnd_Constants_Pnd_Rstl_Usage_Cde.getValue(1,":",3)))) 
                        || ((((pnd_Keys_Pnd_Key_Usage_Cde.equals(pnd_Keys_Pnd_Key_Tiaa_Cntrct.isBreak()) && pnd_Keys_Pnd_Key_Usage_Cde.equals(pnd_Constants_Pnd_Rstl_Usage_Cde.getValue(3,":",6))) 
                        && pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found.getBoolean()) && pnd_Per_Request_Info_Pnd_Total_Pin_Rqst.greater(1)) && ! (pnd_Keys_Pnd_Key_Tiaa_Cntrct.equals(ldaAdsl400a.getPnd_Prtcpnt_Adp_Cntrcts_In_Rqst().getValue("*"))))))))
                    {
                        //*        >>> THE FF. LOGIC HANDLES MULTIPLE REQUESTS FOR SAME PIN ON THE
                        //*                          SAME DAY WITH DIFFERENT SETS OF CONTRACTS <<<<
                        //*        ----------------------------------------------------------------
                        pnd_Per_Request_Info_Pnd_New_Request.setValue(true);                                                                                              //Natural: MOVE TRUE TO #NEW-REQUEST
                        pnd_Counters_Pnd_Total_Omni_Rqst.nadd(1);                                                                                                         //Natural: ADD 1 TO #TOTAL-OMNI-RQST
                        if (condition(pnd_Counters_Pnd_Total_Omni_Recs.equals(1)))                                                                                        //Natural: IF #TOTAL-OMNI-RECS EQ 1
                        {
                            pnd_Counters_Pnd_Total_Omni_Cntrct.nadd(1);                                                                                                   //Natural: ADD 1 TO #TOTAL-OMNI-CNTRCT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Misc_Variables_Pnd_Debug_On.getBoolean() && pnd_Per_Request_Info_Pnd_New_Request.getBoolean()))                                     //Natural: IF #DEBUG-ON AND #NEW-REQUEST
                    {
                        getReports().write(0, NEWLINE,"=",new RepeatItem(15),">>>>> NEW REQUEST <<<<<","=",new RepeatItem(15));                                           //Natural: WRITE / '=' ( 15 ) '>>>>> NEW REQUEST <<<<<' '=' ( 15 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, NEWLINE,"PIN : ",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Pin_A(),"SSN : ",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Ssn(),        //Natural: WRITE / 'PIN : ' #OMNI-PIN-A 'SSN : ' #OMNI-SSN 'PLAN # : ' #OMNI-PLAN-NBR 'OPNT: ' #OMNI-ANNTY-OPTN 'OMNI CONTRACT : ' #OMNI-TIAA-CNTRCT 'OMNI-USAGE2 : ' #OMNI-USAGE-CDE2
                            "PLAN # : ",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Plan_Nbr(),"OPNT: ",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Annty_Optn(),"OMNI CONTRACT : ",
                            ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct(),"OMNI-USAGE2 : ",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Usage_Cde2());
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*     >> UPDATE CONTRACT RECORDS <<
                    //*     -----------------------------
                    if (condition((pnd_Keys_Pnd_Key_Tiaa_Cntrct.isBreak() || (pnd_Per_Request_Info_Pnd_New_Request.getBoolean() && pnd_Counters_Pnd_Total_Omni_Recs.notEquals(1))))) //Natural: IF BREAK OF #KEY-TIAA-CNTRCT OR ( #NEW-REQUEST AND #TOTAL-OMNI-RECS NE 1 )
                    {
                        //* *  MN 03282006  START
                        //* *  MN 03282006  END
                        if (condition(pnd_Misc_Variables_Pnd_Debug_On.getBoolean()))                                                                                      //Natural: IF #DEBUG-ON
                        {
                            getReports().write(0, NEWLINE,"*");                                                                                                           //Natural: WRITE / '*'
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, NEWLINE,"....BREAK OF CONTRACT","RQST COMPLETE ?",ldaAdsl400a.getPnd_Prtcpnt_Adp_Rqst_Complete(),"OLD CONT :",          //Natural: WRITE / '....BREAK OF CONTRACT' 'RQST COMPLETE ?' #PRTCPNT.ADP-RQST-COMPLETE 'OLD CONT :' #CNTRCT.ADC-TIAA-NBR 'NEW CONT :' #OMNI-TIAA-CNTRCT 'OVERALL-STAT :' #OVERALL-STAT 'OLD SUB PLAN :' #CNTRCT.ADC-SUB-PLAN-NBR
                                ldaAdsl400a.getPnd_Cntrct_Adc_Tiaa_Nbr(),"NEW CONT :",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct(),"OVERALL-STAT :",
                                pnd_Per_Request_Info_Pnd_Overall_Stat,"OLD SUB PLAN :",ldaAdsl400a.getPnd_Cntrct_Adc_Sub_Plan_Nbr());
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(! (ldaAdsl400a.getPnd_Prtcpnt_Adp_Rqst_Complete().getBoolean())))                                                                   //Natural: IF NOT #PRTCPNT.ADP-RQST-COMPLETE
                        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-PREV-ADS-CNTRCT
                            sub_Update_Prev_Ads_Cntrct();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Counters_Pnd_Total_Omni_Cntrct.nadd(1);                                                                                                       //Natural: ADD 1 TO #TOTAL-OMNI-CNTRCT
                        ldaAdsl400a.getPnd_Cntrct().reset();                                                                                                              //Natural: RESET #CNTRCT
                        ldaAdsl400a.getPnd_Cntrct_New_Cntrct().setValue(true);                                                                                            //Natural: MOVE TRUE TO NEW-CNTRCT
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Per_Request_Info_Pnd_New_Request.getBoolean()))                                                                                     //Natural: IF #NEW-REQUEST
                    {
                        if (condition(pnd_Misc_Variables_Pnd_Debug_On.getBoolean()))                                                                                      //Natural: IF #DEBUG-ON
                        {
                            getReports().write(0, NEWLINE,"*",NEWLINE,"....UPDATE PREVIOUS PARTICIPANT RECORD","PREV PRTCPT RECORD FOUND ?",pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found); //Natural: WRITE / '*' / '....UPDATE PREVIOUS PARTICIPANT RECORD' 'PREV PRTCPT RECORD FOUND ?' #PRTCPNT-MATCH-FOUND
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        //*       >> COMPLETE PROCESS OF PRIOR RQST: UPDATE OVERALL STAT,MIT ETC.<<
                        //*       -----------------------------------------------------------------
                        if (condition(! (ldaAdsl400a.getPnd_Prtcpnt_Adp_Rqst_Complete().getBoolean()) && pnd_Counters_Pnd_Total_Omni_Recs.notEquals(1)))                  //Natural: IF NOT #PRTCPNT.ADP-RQST-COMPLETE AND #TOTAL-OMNI-RECS NE 1
                        {
                            if (condition(pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found.getBoolean()))                                                                     //Natural: IF #PRTCPNT-MATCH-FOUND
                            {
                                                                                                                                                                          //Natural: PERFORM COMPARE-ADAS-VS-OMNI-CONT-IN-RQST
                                sub_Compare_Adas_Vs_Omni_Cont_In_Rqst();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(Map.getDoInput())) {return;}
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                ldaAdsl400a.getPnd_Prtcpnt_Adp_Cntrcts_In_Rqst().getValue("*").setValue(ldaAdsl400a.getPnd_Prtcpnt_Omni_Cntrcts_In_Rqst().getValue("*")); //Natural: MOVE #PRTCPNT.OMNI-CNTRCTS-IN-RQST ( * ) TO #PRTCPNT.ADP-CNTRCTS-IN-RQST ( * )
                            }                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM UPDATE-PREV-RQST-MIT
                            sub_Update_Prev_Rqst_Mit();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-PREV-RQST-OVERALL-STAT
                            sub_Update_Prev_Rqst_Overall_Stat();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            ldaAdsl400a.getPnd_Prtcpnt_Adp_Rqst_Complete().setValue(true);                                                                                //Natural: MOVE TRUE TO #PRTCPNT.ADP-RQST-COMPLETE
                        }                                                                                                                                                 //Natural: END-IF
                        //*        ================================================
                        //*        >> UPDATE PARTICIPANT RECORDS FOR NEW REQUEST <<
                        //*        ================================================
                        ldaAdsl400a.getPnd_Prtcpnt().reset();                                                                                                             //Natural: RESET #PRTCPNT #PER-REQUEST-INFO #ADC-KEY #ADSA480-MIT-UPDATE
                        pnd_Per_Request_Info.reset();
                        pnd_Keys_Pnd_Adc_Key.reset();
                        pdaAdsa480.getPnd_Adsa480_Mit_Update().reset();
                        ldaAdsl400a.getPnd_Cntrct_New_Cntrct().setValue(true);                                                                                            //Natural: MOVE TRUE TO NEW-CNTRCT
                        //*  ESV 07-16-04
                        pnd_Per_Request_Info_Pnd_Plan_Nbr.setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Plan_Nbr());                                                     //Natural: MOVE #OMNI-PLAN-NBR TO #PLAN-NBR
                        if (condition(pnd_Keys_Pnd_Key_Pin.isBreak() || pnd_Counters_Pnd_Total_Omni_Recs.equals(1)))                                                      //Natural: IF BREAK OF #KEY-PIN OR #TOTAL-OMNI-RECS EQ 1
                        {
                                                                                                                                                                          //Natural: PERFORM VERIFY-PIN
                            sub_Verify_Pin();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Misc_Variables_Pnd_Pin_Okay.getBoolean()))                                                                                      //Natural: IF #PIN-OKAY
                        {
                                                                                                                                                                          //Natural: PERFORM FIND-ADS-PRTCPNT-MATCH
                            sub_Find_Ads_Prtcpnt_Match();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            //*  121007
                            if (condition(! (pnd_Per_Request_Info_Pnd_Processed_Rsttlmnt.getBoolean())))                                                                  //Natural: IF NOT #PROCESSED-RSTTLMNT
                            {
                                                                                                                                                                          //Natural: PERFORM CREATE-OR-UPDATE-ADS-PRTCPNT
                                sub_Create_Or_Update_Ads_Prtcpnt();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(Map.getDoInput())) {return;}
                                //*  121007
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaAdsl400a.getPnd_Prtcpnt_Adp_Rqst_Complete().setValue(true);                                                                                //Natural: MOVE TRUE TO #PRTCPNT.ADP-RQST-COMPLETE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*     >> PRINT ERROR REPORT FOR REJECTED OMNI RECORDS <<
                    //*     --------------------------------------------------
                    //*  121007 START
                    if (condition(pnd_Per_Request_Info_Pnd_Unmatched_Rstlmt.getBoolean() || ! (pnd_Misc_Variables_Pnd_Pin_Okay.getBoolean()) || pnd_Per_Request_Info_Pnd_Processed_Omni_Rqst.getBoolean()  //Natural: IF #UNMATCHED-RSTLMT OR NOT #PIN-OKAY OR #PROCESSED-OMNI-RQST OR #PROCESSED-RSTTLMNT
                        || pnd_Per_Request_Info_Pnd_Processed_Rsttlmnt.getBoolean()))
                    {
                        if (condition(pnd_Per_Request_Info_Pnd_Processed_Rsttlmnt.getBoolean()))                                                                          //Natural: IF #PROCESSED-RSTTLMNT
                        {
                            ldaAdsl400a.getPnd_Prtcpnt_Adp_Rqst_Complete().setValue(true);                                                                                //Natural: MOVE TRUE TO #PRTCPNT.ADP-RQST-COMPLETE
                            //*  121007 END
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(! (ldaAdsl400a.getPnd_Cntrct_Err_Report_Printed().getBoolean())))                                                                   //Natural: IF NOT ERR-REPORT-PRINTED
                        {
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR-REPORT
                            sub_Print_Error_Report();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FILLUP-CNTRCT-LINKAGE
                    sub_Fillup_Cntrct_Linkage();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                if (condition(pnd_Counters_Pnd_Total_Omni_Recs.equals(getZero())))                                                                                        //Natural: IF #TOTAL-OMNI-RECS EQ 0
                {
                    //*   MOVE 91  TO #TERMINATE-NO                     /* ESV 06-30-04
                    getCurrentProcessState().getDbConv().dbRollback();                                                                                                    //Natural: BACKOUT TRANSACTION
                    getReports().write(0, NEWLINE," ================================================================= ",NEWLINE,"|*****************************************************************|", //Natural: WRITE /' ================================================================= ' /'|*****************************************************************|' /'|*****************************************************************|' /'|** TERMINATING ADAS BATCH PROCESS.....                         **|' /'|**                                                             **|' /'|**      >>>> NO RECORDS FOUND IN OMNI T966 FILE <<<<           **|' /'|*****************************************************************|' /'|*****************************************************************|' /' ================================================================= '
                        NEWLINE,"|*****************************************************************|",NEWLINE,"|** TERMINATING ADAS BATCH PROCESS.....                         **|",
                        NEWLINE,"|**                                                             **|",NEWLINE,"|**      >>>> NO RECORDS FOUND IN OMNI T966 FILE <<<<           **|",
                        NEWLINE,"|*****************************************************************|",NEWLINE,"|*****************************************************************|",
                        NEWLINE," ================================================================= ");
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB-MESSAGE
                    sub_End_Of_Job_Message();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate();  if (true) return;                                                                                                               //Natural: TERMINATE
                }                                                                                                                                                         //Natural: END-IF
                //* * 09272005 MN START
                //* * IF #TOTAL-OMNI-RECS = #BAD-RECS   /*
                getReports().write(0, "=",pnd_Counters_Pnd_Total_Recs,"=",pnd_Counters_Pnd_Bad_Recs);                                                                     //Natural: WRITE '=' #TOTAL-RECS '=' #BAD-RECS
                if (Global.isEscape()) return;
                //*  RS0
                if (condition(pnd_Counters_Pnd_Total_Recs.equals(pnd_Counters_Pnd_Bad_Recs)))                                                                             //Natural: IF #TOTAL-RECS = #BAD-RECS
                {
                    pnd_Terminate_Pnd_Terminate_No.setValue(93);                                                                                                          //Natural: MOVE 93 TO #TERMINATE-NO
                    pnd_Terminate_Pnd_Terminate_Msg.setValue(DbsUtil.compress("ALL RECORDS IN OMNI T966 FILE HAVE BAD FORMAT"));                                          //Natural: COMPRESS 'ALL RECORDS IN OMNI T966 FILE HAVE BAD FORMAT' TO #TERMINATE-MSG
                    pnd_Adsn998_Error_Handler_Pnd_Error_Msg.setValue(pnd_Terminate_Pnd_Terminate_Msg);                                                                    //Natural: MOVE #TERMINATE-MSG TO #ERROR-MSG
                                                                                                                                                                          //Natural: PERFORM TERMINATE-ROUTINE
                    sub_Terminate_Routine();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                //* * 09272005 MN END
                //*   >> COMPLETE PROCESS OF LAST REQUEST <<
                //*   --------------------------------------
                if (condition(! (ldaAdsl400a.getPnd_Prtcpnt_Adp_Rqst_Complete().getBoolean())))                                                                           //Natural: IF NOT #PRTCPNT.ADP-RQST-COMPLETE
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-PREV-ADS-CNTRCT
                    sub_Update_Prev_Ads_Cntrct();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    if (condition(pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found.getBoolean()))                                                                             //Natural: IF #PRTCPNT-MATCH-FOUND
                    {
                                                                                                                                                                          //Natural: PERFORM COMPARE-ADAS-VS-OMNI-CONT-IN-RQST
                        sub_Compare_Adas_Vs_Omni_Cont_In_Rqst();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl400a.getPnd_Prtcpnt_Adp_Cntrcts_In_Rqst().getValue("*").setValue(ldaAdsl400a.getPnd_Prtcpnt_Omni_Cntrcts_In_Rqst().getValue("*"));         //Natural: MOVE #PRTCPNT.OMNI-CNTRCTS-IN-RQST ( * ) TO #PRTCPNT.ADP-CNTRCTS-IN-RQST ( * )
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM UPDATE-PREV-RQST-MIT
                    sub_Update_Prev_Rqst_Mit();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM UPDATE-PREV-RQST-OVERALL-STAT
                    sub_Update_Prev_Rqst_Overall_Stat();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    ldaAdsl400a.getPnd_Prtcpnt_Adp_Rqst_Complete().setValue(true);                                                                                        //Natural: MOVE TRUE TO #PRTCPNT.ADP-RQST-COMPLETE
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB-MESSAGE
                sub_End_Of_Job_Message();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //* *********************************************************************
                //*                    O N   E R R O R   R O U T I N E
                //* *********************************************************************
                //*                                                                                                                                                       //Natural: ON ERROR
                //* ********************* S U B R O U T I N E S *************************
                //*  -------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FUND-TICKERS
                //*  ---------------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADSN402-READ-CONTROL-REC
                //*  -------------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-OMNI-DATA-FORMAT
                //* * 07062005 MN START
                //* * 07062005 MN START
                //* * 07062005 MN END
                //* * WHEN #OMNI-FUND-RATE-ACCUM-AMT-A EQ ' '
                //* *     MOVE 0 TO #OMNI-FUND-RATE-ACCUM-AMT
                //* * RS0 START
                //* * RS0 END
                //*  -------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VERIFY-PIN
                //*  --------------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-ADS-PRTCPNT-MATCH
                //* * 11092006 MN START
                //* * 11092006 MN END
                //* ******************************************** 071707 *******************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-CONTRACT
                //* ***********************************************************************
                //*  OS - 071613 END
                //*  ------------------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-OR-UPDATE-ADS-PRTCPNT
                //*    MOVE #OMNI-PIN                  TO #RQST-PIN
                //*  -------------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-PREV-ADS-CNTRCT
                //*    MOVE ADS-CNTRCT-VIEW.ADC-GRD-MNTHLY-QTY(*) TO
                //*      #CNTRCT.ADC-GRD-MNTHLY-QTY(*)
                //*    MOVE ADS-CNTRCT-VIEW.ADC-GRD-MNTHLY-TYP(*) TO
                //*      #CNTRCT.ADC-GRD-MNTHLY-TYP(*)
                //*    '#CNTRCT.ADC-GRD-MNTHLY-QTY: ' #CNTRCT.ADC-GRD-MNTHLY-QTY(1:5)
                //*    'ADS.ADC-GRD-MNTHLY-QTY: ' ADS-CNTRCT-VIEW.ADC-GRD-MNTHLY-QTY(1:5)
                //*    '#CNTRCT.ADC-GRD-MNTHLY-TYP: ' #CNTRCT.ADC-GRD-MNTHLY-TYP(1:5)
                //*    'ADS.ADC-GRD-MNTHLY-TYP: ' ADS-CNTRCT-VIEW.ADC-GRD-MNTHLY-TYP(1:5)
                //*  -------------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FILLUP-CNTRCT-LINKAGE
                //*      '=' #OMNI-FUND-TICKER '=' #EXT-TICKER-SYMBOL(*)
                //*  ------------------------------------------------ *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPARE-ADAS-VS-OMNI-CONT-IN-RQST
                //*  >>> IF NOT SYNCH, UPDATE ADP-CNTRCTS-IN-RQST WITH OMNI CONTRACTS <<<
                //*  --------------------------------------------------------------------
                //*  --------------------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-PREV-RQST-OVERALL-STAT
                //*  --------------------------------------------- *
                //*  ------------------------------------ *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-PREV-RQST-MIT
                //*  ---------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-ERROR-REPORT
                //* * 07062005 MN START
                //* * 07062005 MN END
                //* * RS0 START
                //* *    VALUE '85'
                //* *      MOVE 'Roth Contribution Amount LE zero ' TO #REPORT-MSG
                //* * RS0 END
                //*  --------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-LOG
                //*  --------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TERMINATE-ROUTINE
                //*  ---------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-JOB-MESSAGE
                //*  /'|**                                                             **|'
                //*  /'|**                                                             **|'
                //* **************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
                //* ****************************************
                //* CM - CLOSE MQ
                DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                              //Natural: FETCH RETURN 'MDMP0012'
                if (condition(Global.isEscape())) return;
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Get_Fund_Tickers() throws Exception                                                                                                                  //Natural: GET-FUND-TICKERS
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------- *
        //*  RS1
        DbsUtil.callnat(Adsn888.class , getCurrentProcessState(), pdaAdsa888.getPnd_Parm_Area(), pdaAdsa888.getPnd_Nbr_Acct());                                           //Natural: CALLNAT 'ADSN888' #PARM-AREA #NBR-ACCT
        if (condition(Global.isEscape())) return;
        //* *#EXT-TICKER-SYMBOL(1:20) := #ACCT-TICKER(1:20)           /* OS-102013
        //* *#EXT-ALPHA-FUND-CDE(1:20) := #ACCT-ALPHA-FUND-CDE(1:20)  /* OS-102013
        //*  RESET NECA4000                      /* RS1
        //*  NECA4000.FUNCTION-CDE        := 'CFN'
        //*  NECA4000.INPT-KEY-OPTION-CDE := '01'
        //*  NECA4000.CFN-KEY-COMPANY-CDE := '001'
        //*  CALLNAT 'NECN4000' NECA4000
        //*  IF NECA4000.OUTPUT-CNT NE 0
        //*  MOVE NECA4000.CFN-TICKER-SYMBOL(*)      TO #EXT-TICKER-SYMBOL(*)
        //*  MOVE NECA4000.CFN-FND-ALPHA-FUND-CDE(*) TO #EXT-ALPHA-FUND-CDE(*)
        //*  ELSE
        //*   MOVE 90  TO #TERMINATE-NO
        //*   COMPRESS 'ERROR IN RETRIEVING FUND TICKER INFORMATION. '
        //*     ' NECN4000 RETURN CODE : ' NECA4000.RETURN-CDE
        //*     ' NECN4000 RETURN MSG : '  NECA4000.RETURN-MSG
        //*     '....VERIFY IF THE EXTERNALIZATION FILE 3/220 IS AVAILABLE'
        //*     TO #TERMINATE-MSG
        //*   MOVE 'NEC-CALL'     TO #STEP-NAME
        //*   MOVE #TERMINATE-MSG TO #ERROR-MSG
        //*   PERFORM TERMINATE-ROUTINE
        //*  END-IF
    }
    private void sub_Adsn402_Read_Control_Rec() throws Exception                                                                                                          //Natural: ADSN402-READ-CONTROL-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------------- *
        DbsUtil.callnat(Adsn402.class , getCurrentProcessState(), pnd_Adsn401_Pnd_Cntl_Bsnss_Prev_Dte, pnd_Adsn401_Pnd_Cntl_Bsnss_Dte, pnd_Adsn401_Pnd_Cntl_Bsnss_Tmrrw_Dte,  //Natural: CALLNAT 'ADSN402' #CNTL-BSNSS-PREV-DTE #CNTL-BSNSS-DTE #CNTL-BSNSS-TMRRW-DTE #CCT-RCRD-CDE
            pnd_Adsn401_Pnd_Cct_Rcrd_Cde);
        if (condition(Global.isEscape())) return;
        getReports().write(0, "=",pnd_Adsn401_Pnd_Cntl_Bsnss_Prev_Dte,"=",pnd_Adsn401_Pnd_Cntl_Bsnss_Dte);                                                                //Natural: WRITE '=' #CNTL-BSNSS-PREV-DTE '=' #CNTL-BSNSS-DTE
        if (Global.isEscape()) return;
        //*  FLIP DATE FOR COMPEARING TO ROTH DATE
        //*  MOVE EDITED #CNTL-BSNSS-DTE  TO #FILE-DTE-D (EM=YYYYMMDD)
    }
    private void sub_Check_Omni_Data_Format() throws Exception                                                                                                            //Natural: CHECK-OMNI-DATA-FORMAT
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------- *
        //*  >> CHECK IF CORRECT OMNI FILE IS BEING PROCESSED <<
        //*  ---------------------------------------------------
        if (condition(pnd_Counters_Pnd_Total_Omni_Recs.equals(1)))                                                                                                        //Natural: IF #TOTAL-OMNI-RECS EQ 1
        {
            pnd_Misc_Variables_Pnd_Invalid_File_Date.reset();                                                                                                             //Natural: RESET #INVALID-FILE-DATE
            if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_File_Dte().notEquals(getZero())))                                                                         //Natural: IF #OMNI-FILE-DTE NE 0
            {
                pnd_Misc_Variables_Pnd_File_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_File_Dte_A());                     //Natural: MOVE EDITED #OMNI-FILE-DTE-A TO #FILE-DATE-D ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Adsn401_Pnd_Cntl_Bsnss_Dte.notEquals(getZero())))                                                                                           //Natural: IF #CNTL-BSNSS-DTE NE 0
            {
                pnd_Misc_Variables_Pnd_Day_Of_Week.setValueEdited(pnd_Adsn401_Pnd_Cntl_Bsnss_Dte,new ReportEditMask("NNNNNNNNN"));                                        //Natural: MOVE EDITED #CNTL-BSNSS-DTE ( EM = N ( 9 ) ) TO #DAY-OF-WEEK
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "=",pnd_Misc_Variables_Pnd_File_Date_D,"CONTROL-DAY-OF-WEEK",pnd_Misc_Variables_Pnd_Day_Of_Week);                                       //Natural: WRITE '=' #FILE-DATE-D 'CONTROL-DAY-OF-WEEK' #DAY-OF-WEEK
            if (Global.isEscape()) return;
            if (condition(pnd_Misc_Variables_Pnd_File_Date_D.notEquals(pnd_Adsn401_Pnd_Cntl_Bsnss_Dte)))                                                                  //Natural: IF #FILE-DATE-D NE #CNTL-BSNSS-DTE
            {
                //*      IF #DAY-OF-WEEK = 'Monday'                        /* ESV 7-13-04
                if (condition(pnd_Misc_Variables_Pnd_File_Date_D.lessOrEqual(pnd_Adsn401_Pnd_Cntl_Bsnss_Prev_Dte) || pnd_Misc_Variables_Pnd_File_Date_D.greater(pnd_Adsn401_Pnd_Cntl_Bsnss_Dte))) //Natural: IF #FILE-DATE-D LE #CNTL-BSNSS-PREV-DTE OR #FILE-DATE-D GT #CNTL-BSNSS-DTE
                {
                    pnd_Misc_Variables_Pnd_Invalid_File_Date.setValue(true);                                                                                              //Natural: MOVE TRUE TO #INVALID-FILE-DATE
                }                                                                                                                                                         //Natural: END-IF
                //*      ELSE                                              /* COMMENT OUT
                //*         MOVE TRUE TO #INVALID-FILE-DATE                /* PER E. MELNIK
                //*      END-IF                                            /*
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Misc_Variables_Pnd_Invalid_File_Date.getBoolean()))                                                                                         //Natural: IF #INVALID-FILE-DATE
            {
                pnd_Terminate_Pnd_Terminate_No.setValue(92);                                                                                                              //Natural: MOVE 92 TO #TERMINATE-NO
                pnd_Terminate_Pnd_Terminate_Msg.setValue(DbsUtil.compress("OMNI FILE CREATION DATE (", pnd_Misc_Variables_Pnd_File_Date_D, ") DOES NOT",                  //Natural: COMPRESS 'OMNI FILE CREATION DATE (' #FILE-DATE-D ') DOES NOT' ' MATCH WITH ADAS CONTROL BUSINESS DATE (' #CNTL-BSNSS-DTE ') PLEASE VERIFY THAT THE OMNI UNIFIED BATCH PROCESS' 'RAN SUCCESSFULLY......' TO #TERMINATE-MSG
                    " MATCH WITH ADAS CONTROL BUSINESS DATE (", pnd_Adsn401_Pnd_Cntl_Bsnss_Dte, ") PLEASE VERIFY THAT THE OMNI UNIFIED BATCH PROCESS", "RAN SUCCESSFULLY......"));
                pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("CHKDATE");                                                                                              //Natural: MOVE 'CHKDATE' TO #STEP-NAME
                pnd_Adsn998_Error_Handler_Pnd_Error_Msg.setValue(pnd_Terminate_Pnd_Terminate_Msg);                                                                        //Natural: MOVE #TERMINATE-MSG TO #ERROR-MSG
                                                                                                                                                                          //Natural: PERFORM TERMINATE-ROUTINE
                sub_Terminate_Routine();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  RS0
        pnd_Misc_Variables_Pnd_Bad_Roth_Data.reset();                                                                                                                     //Natural: RESET #BAD-ROTH-DATA #RB-RETURN-CODE
        pnd_Misc_Variables_Pnd_Rb_Return_Code.reset();
        pnd_Misc_Variables_Pnd_Omni_Format_Ok.setValue(true);                                                                                                             //Natural: MOVE TRUE TO #OMNI-FORMAT-OK
        //*  RS0 START
        pnd_Rec_Has_Data.setValue(false);                                                                                                                                 //Natural: MOVE FALSE TO #REC-HAS-DATA
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 299
        for (pnd_Counters_Pnd_I.setValue(1); condition(pnd_Counters_Pnd_I.lessOrEqual(299)); pnd_Counters_Pnd_I.nadd(1))
        {
            if (condition(ldaAdsl400.getPnd_Omni_Input().getValue(pnd_Counters_Pnd_I).notEquals(" ")))                                                                    //Natural: IF #OMNI-INPUT ( #I ) NE ' '
            {
                pnd_Rec_Has_Data.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #REC-HAS-DATA
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  RS0 END
        //*  IF #OMNI-INPUT(*) EQ ' '               OR    /* RS0
        //*  RS0
        if (condition(((((((((((((((((! (pnd_Rec_Has_Data.getBoolean()) || (ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Pin_A().notEquals(" ") && ! (DbsUtil.maskMatches(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Pin_A(),"999999999999"))))  //Natural: IF NOT #REC-HAS-DATA OR ( #OMNI-PIN-A NE ' ' AND VAL ( #OMNI-PIN-A ) NE MASK ( 999999999999 ) ) OR #OMNI-SSN-A NE MASK ( 999999999 ) OR #OMNI-EFF-DTE-A NE MASK ( YYYYMMDD ) OR ( #OMNI-EMP-TERM-DTE NE 0 AND #OMNI-EMP-TERM-DTE-A NE MASK ( YYYYMMDD ) ) OR ( #OMNI-ISSUE-DTE NE 0 AND #OMNI-ISSUE-DTE-A NE MASK ( YYYYMMDD ) ) OR ( #OMNI-FILE-DTE NE 0 AND #OMNI-FILE-DTE-A NE MASK ( YYYYMMDD ) ) OR ( #OMNI-CONTRACT-ID-A NE ' ' AND #OMNI-CONTRACT-ID-A NE MASK ( 99999999999 ) ) OR ( #OMNI-FUND-OPN-ACCUM-AMT-A NE ' ' AND #OMNI-FUND-OPN-ACCUM-AMT-A NE MASK ( 99999999999 ) ) OR ( #OMNI-FUND-SETTL-AMT-A NE ' ' AND #OMNI-FUND-SETTL-AMT-A NE MASK ( 99999999999 ) ) OR ( #OMNI-FUND-IVC-AMT-A NE ' ' AND #OMNI-FUND-IVC-AMT-A NE MASK ( 999999999 ) ) OR ( #OMNI-FUND-OPN-ACCUM-UNTS-A NE ' ' AND #OMNI-FUND-OPN-ACCUM-UNTS-A NE MASK ( 9999999999 ) ) OR ( #OMNI-FUND-SETTL-UNTS-A NE ' ' AND #OMNI-FUND-SETTL-UNTS-A NE MASK ( 9999999999 ) ) OR ( #OMNI-FUND-RATE-SETTL-AMT-A NE ' ' AND #OMNI-FUND-RATE-SETTL-AMT-A NE MASK ( 99999999999 ) ) OR ( #OMNI-ORIG-CNTRCT-LOB-IND NE ' ' AND #OMNI-ORIG-CNTRCT-LOB-IND NE MASK ( 9 ) ) OR ( #OMNI-TPA-GUARANT-COMMUT-AMT-A NE ' ' AND #OMNI-TPA-GUARANT-COMMUT-AMT-A NE MASK ( 99999999999 ) ) OR ( #OMNI-TPA-PAYMENT-CNT-A NE ' ' AND #OMNI-TPA-PAYMENT-CNT-A NE MASK ( 99 ) )
            || ! (DbsUtil.maskMatches(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Ssn_A(),"999999999"))) || ! (DbsUtil.maskMatches(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Eff_Dte_A(),"YYYYMMDD"))) 
            || (ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Emp_Term_Dte().notEquals(getZero()) && ! (DbsUtil.maskMatches(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Emp_Term_Dte_A(),"YYYYMMDD")))) 
            || (ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Issue_Dte().notEquals(getZero()) && ! (DbsUtil.maskMatches(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Issue_Dte_A(),"YYYYMMDD")))) 
            || (ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_File_Dte().notEquals(getZero()) && ! (DbsUtil.maskMatches(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_File_Dte_A(),"YYYYMMDD")))) 
            || (ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Contract_Id_A().notEquals(" ") && ! (DbsUtil.maskMatches(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Contract_Id_A(),"99999999999")))) 
            || (ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt_A().notEquals(" ") && ! (DbsUtil.maskMatches(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt_A(),"99999999999")))) 
            || (ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt_A().notEquals(" ") && ! (DbsUtil.maskMatches(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt_A(),"99999999999")))) 
            || (ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt_A().notEquals(" ") && ! (DbsUtil.maskMatches(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt_A(),"999999999")))) 
            || (ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts_A().notEquals(" ") && ! (DbsUtil.maskMatches(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts_A(),"9999999999")))) 
            || (ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts_A().notEquals(" ") && ! (DbsUtil.maskMatches(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts_A(),"9999999999")))) 
            || (ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt_A().notEquals(" ") && ! (DbsUtil.maskMatches(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt_A(),"99999999999")))) 
            || (ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Orig_Cntrct_Lob_Ind().notEquals(" ") && ! (DbsUtil.maskMatches(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Orig_Cntrct_Lob_Ind(),"9")))) 
            || (ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt_A().notEquals(" ") && ! (DbsUtil.maskMatches(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt_A(),"99999999999")))) 
            || (ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt_A().notEquals(" ") && ! (DbsUtil.maskMatches(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt_A(),
            "99"))))))
        {
            //* * 07062005 MN END
            pnd_Misc_Variables_Pnd_Omni_Format_Ok.reset();                                                                                                                //Natural: RESET #OMNI-FORMAT-OK
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ROTH SPECIFIC EDITS   RS0 START
        if (condition(pnd_Roth_Request.getBoolean()))                                                                                                                     //Natural: IF #ROTH-REQUEST
        {
            if (condition(! (DbsUtil.maskMatches(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte(),"YYYYMMDD")) || ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte_A().equals(" ")  //Natural: IF ( #OMNI-ROTH-FRST-CONTRIB-DTE NE MASK ( YYYYMMDD ) OR #OMNI-ROTH-FRST-CONTRIB-DTE-A = ' ' OR #OMNI-ROTH-FRST-CONTRIB-DTE = 0 )
                || ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte().equals(getZero())))
            {
                //*  NOT POPULATED
                pnd_Misc_Variables_Pnd_Rb_Return_Code.setValue("83");                                                                                                     //Natural: MOVE '83' TO #RB-RETURN-CODE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte().less(20060101)))                                                                  //Natural: IF #OMNI-ROTH-FRST-CONTRIB-DTE < 20060101
            {
                pnd_Misc_Variables_Pnd_Rb_Return_Code.setValue("82");                                                                                                     //Natural: MOVE '82' TO #RB-RETURN-CODE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Misc_Variables_Pnd_Process_Date.setValueEdited(pnd_Adsn401_Pnd_Cntl_Bsnss_Dte,new ReportEditMask("YYYYMMDD"));                                            //Natural: MOVE EDITED #CNTL-BSNSS-DTE ( EM = YYYYMMDD ) TO #PROCESS-DATE
            if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte_A().greater(pnd_Misc_Variables_Pnd_Process_Date)))                                  //Natural: IF #OMNI-ROTH-FRST-CONTRIB-DTE-A > #PROCESS-DATE
            {
                pnd_Misc_Variables_Pnd_Rb_Return_Code.setValue("81");                                                                                                     //Natural: MOVE '81' TO #RB-RETURN-CODE
            }                                                                                                                                                             //Natural: END-IF
            //*  INVALID DSBLTY DATE
            if (condition(! (DbsUtil.maskMatches(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte(),"YYYYMMDD")) && ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte().notEquals(getZero()))) //Natural: IF ( #OMNI-ROTH-DSBLTY-DTE NE MASK ( YYYYMMDD ) AND #OMNI-ROTH-DSBLTY-DTE NE 0 )
            {
                pnd_Misc_Variables_Pnd_Rb_Return_Code.setValue("84");                                                                                                     //Natural: MOVE '84' TO #RB-RETURN-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Roth_Request.getBoolean() && pnd_Misc_Variables_Pnd_Rb_Return_Code.notEquals(" ")))                                                             //Natural: IF #ROTH-REQUEST AND #RB-RETURN-CODE NE ' '
        {
            pnd_Misc_Variables_Pnd_Omni_Format_Ok.setValue(false);                                                                                                        //Natural: MOVE FALSE TO #OMNI-FORMAT-OK
            pnd_Misc_Variables_Pnd_Bad_Roth_Data.setValue("Y");                                                                                                           //Natural: MOVE 'Y' TO #BAD-ROTH-DATA
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* * RS0 END ROTH EDITS
        short decideConditionsMet2758 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #OMNI-TPA-GUARANT-COMMUT-AMT-A = ' '
        if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt_A().equals(" ")))
        {
            decideConditionsMet2758++;
            ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt().setValue(0);                                                                                   //Natural: MOVE 0 TO #OMNI-TPA-GUARANT-COMMUT-AMT
        }                                                                                                                                                                 //Natural: WHEN #OMNI-TPA-PAYMENT-CNT-A EQ ' '
        if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt_A().equals(" ")))
        {
            decideConditionsMet2758++;
            ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt().setValue(0);                                                                                          //Natural: MOVE 0 TO #OMNI-TPA-PAYMENT-CNT
        }                                                                                                                                                                 //Natural: WHEN #OMNI-EMP-TERM-DTE-A EQ ' '
        if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Emp_Term_Dte_A().equals(" ")))
        {
            decideConditionsMet2758++;
            ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Emp_Term_Dte().setValue(0);                                                                                             //Natural: MOVE 0 TO #OMNI-EMP-TERM-DTE
        }                                                                                                                                                                 //Natural: WHEN #OMNI-ISSUE-DTE-A EQ ' '
        if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Issue_Dte_A().equals(" ")))
        {
            decideConditionsMet2758++;
            ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Issue_Dte_A().setValue(0);                                                                                              //Natural: MOVE 0 TO #OMNI-ISSUE-DTE-A
        }                                                                                                                                                                 //Natural: WHEN #OMNI-FILE-DTE-A EQ ' '
        if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_File_Dte_A().equals(" ")))
        {
            decideConditionsMet2758++;
            ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_File_Dte().setValue(0);                                                                                                 //Natural: MOVE 0 TO #OMNI-FILE-DTE
        }                                                                                                                                                                 //Natural: WHEN #OMNI-CONTRACT-ID-A EQ ' '
        if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Contract_Id_A().equals(" ")))
        {
            decideConditionsMet2758++;
            ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Contract_Id().setValue(0);                                                                                              //Natural: MOVE 0 TO #OMNI-CONTRACT-ID
        }                                                                                                                                                                 //Natural: WHEN #OMNI-FUND-OPN-ACCUM-AMT-A EQ ' '
        if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt_A().equals(" ")))
        {
            decideConditionsMet2758++;
            ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt().setValue(0);                                                                                       //Natural: MOVE 0 TO #OMNI-FUND-OPN-ACCUM-AMT
        }                                                                                                                                                                 //Natural: WHEN #OMNI-FUND-SETTL-AMT-A EQ ' '
        if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt_A().equals(" ")))
        {
            decideConditionsMet2758++;
            ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt().setValue(0);                                                                                           //Natural: MOVE 0 TO #OMNI-FUND-SETTL-AMT
        }                                                                                                                                                                 //Natural: WHEN #OMNI-FUND-IVC-AMT-A EQ ' '
        if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt_A().equals(" ")))
        {
            decideConditionsMet2758++;
            ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt().setValue(0);                                                                                             //Natural: MOVE 0 TO #OMNI-FUND-IVC-AMT
        }                                                                                                                                                                 //Natural: WHEN #OMNI-FUND-OPN-ACCUM-UNTS-A EQ ' '
        if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts_A().equals(" ")))
        {
            decideConditionsMet2758++;
            ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts().setValue(0);                                                                                      //Natural: MOVE 0 TO #OMNI-FUND-OPN-ACCUM-UNTS
        }                                                                                                                                                                 //Natural: WHEN #OMNI-FUND-SETTL-UNTS-A EQ ' '
        if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts_A().equals(" ")))
        {
            decideConditionsMet2758++;
            ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts().setValue(0);                                                                                          //Natural: MOVE 0 TO #OMNI-FUND-SETTL-UNTS
        }                                                                                                                                                                 //Natural: WHEN #OMNI-FUND-RATE-SETTL-AMT-A EQ ' '
        if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt_A().equals(" ")))
        {
            decideConditionsMet2758++;
            ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt().setValue(0);                                                                                      //Natural: MOVE 0 TO #OMNI-FUND-RATE-SETTL-AMT
        }                                                                                                                                                                 //Natural: WHEN #ROTH-REQUEST AND #OMNI-ROTH-DSBLTY-DTE-A EQ ' '
        if (condition(pnd_Roth_Request.getBoolean() && ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte_A().equals(" ")))
        {
            decideConditionsMet2758++;
            ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte().setValue(0);                                                                                          //Natural: MOVE 0 TO #OMNI-ROTH-DSBLTY-DTE
        }                                                                                                                                                                 //Natural: WHEN #ROTH-REQUEST AND #OMNI-ROTH-FRST-CONTRIB-DTE-A EQ ' '
        if (condition(pnd_Roth_Request.getBoolean() && ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte_A().equals(" ")))
        {
            decideConditionsMet2758++;
            ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte().setValue(0);                                                                                    //Natural: MOVE 0 TO #OMNI-ROTH-FRST-CONTRIB-DTE
        }                                                                                                                                                                 //Natural: WHEN #ROTH-REQUEST AND #OMNI-ROTH-TOT-CONTRIB-AMT-A EQ ' '
        if (condition(pnd_Roth_Request.getBoolean() && ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Tot_Contrib_Amt_A().equals(" ")))
        {
            decideConditionsMet2758++;
            //* **   #OMNI-ROTH-TOT-CONTRIB-AMT NE MASK (99999999999)
            ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Tot_Contrib_Amt().setValue(0);                                                                                     //Natural: MOVE 0 TO #OMNI-ROTH-TOT-CONTRIB-AMT
        }                                                                                                                                                                 //Natural: WHEN #ROTH-REQUEST AND #OMNI-ROTH-CONTRIB-SETTL-AMT-A EQ ' '
        if (condition(pnd_Roth_Request.getBoolean() && ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt_A().equals(" ")))
        {
            decideConditionsMet2758++;
            //* **   #OMNI-ROTH-CONTRIB-SETTL-AMT   NE MASK (99999999999)
            ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt().setValue(0);                                                                                   //Natural: MOVE 0 TO #OMNI-ROTH-CONTRIB-SETTL-AMT
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet2758 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  GET RATE-CODE EQUIVALENT OF #OMNI-CONTRACT-ID
        //*  ---------------------------------------------
        pnd_Misc_Variables_Pnd_Omni_Rate_Code.reset();                                                                                                                    //Natural: RESET #OMNI-RATE-CODE #RB-RETURN-CODE
        pnd_Misc_Variables_Pnd_Rb_Return_Code.reset();
        //*  OS-102013 START
        pnd_Tiaa_Type.reset();                                                                                                                                            //Natural: RESET #TIAA-TYPE
        DbsUtil.examine(new ExamineSource(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Ticker().getValue("*")), new ExamineSearch(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Ticker()),  //Natural: EXAMINE #ACCT-TICKER ( * ) FOR #OMNI-FUND-TICKER GIVING INDEX #I
            new ExamineGivingIndex(pnd_Counters_Pnd_I));
        if (condition((pnd_Counters_Pnd_I.greater(getZero()) && (pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_Counters_Pnd_I).equals(pnd_Constants_Pnd_Tiaa)  //Natural: IF #I GT 0 AND #ACCT-INVSTMNT-GRPNG-ID ( #I ) EQ #TIAA OR EQ #STABLE ( * )
            || (pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_Counters_Pnd_I).equals(pnd_Constants_Pnd_Stable.getValue(1)) || pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_Counters_Pnd_I).equals(pnd_Constants_Pnd_Stable.getValue(2)))))))
        {
            pnd_Tiaa_Type.setValue(true);                                                                                                                                 //Natural: ASSIGN #TIAA-TYPE := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        //* *IF #OMNI-FUND-TICKER EQ #TIAA OR EQ #STABLE(*)   /* EM - 121008/111510
        if (condition(pnd_Tiaa_Type.getBoolean()))                                                                                                                        //Natural: IF #TIAA-TYPE
        {
            //*  OS-102013 END
            if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Contract_Id().notEquals(getZero())))                                                                      //Natural: IF #OMNI-CONTRACT-ID NE 0
            {
                //*    RESET ADAA501A-LINKAGE /* 010808 SHOULD NOT BE INITIALIZED
                pdaAdaa501a.getAdaa501a_Linkage_Adaa501a_Type_Of_Call().setValue("S");                                                                                    //Natural: MOVE 'S' TO ADAA501A-TYPE-OF-CALL
                pdaAdaa501a.getAdaa501a_Linkage_Adaa501a_Sub_Acct().setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Contract_Id());                                        //Natural: MOVE #OMNI-CONTRACT-ID TO ADAA501A-SUB-ACCT
                if (condition(pnd_Misc_Variables_Pnd_Debug_On.getBoolean()))                                                                                              //Natural: IF #DEBUG-ON
                {
                    //*  010808
                    //*  010808
                    getReports().write(0, NEWLINE,"*","....BEFORE CALL TO ADAN501....","=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Pin_A(),"=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct(), //Natural: WRITE / '*' '....BEFORE CALL TO ADAN501....' '=' #OMNI-PIN-A '=' #OMNI-TIAA-CNTRCT '=' #OMNI-CONTRACT-ID / '=' ADAA501A-TYPE-OF-CALL '=' ADAA501A-FIRST-CALL-FLAG
                        "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Contract_Id(),NEWLINE,"=",pdaAdaa501a.getAdaa501a_Linkage_Adaa501a_Type_Of_Call(),"=",
                        pdaAdaa501a.getAdaa501a_Linkage_Adaa501a_First_Call_Flag());
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.callnat(Adan501.class , getCurrentProcessState(), pdaAdaa501a.getAdaa501a_Linkage());                                                             //Natural: CALLNAT 'ADAN501' ADAA501A-LINKAGE
                if (condition(Global.isEscape())) return;
                if (condition(pdaAdaa501a.getAdaa501a_Linkage_Adaa501a_Return_Code().equals(getZero()) && pdaAdaa501a.getAdaa501a_Linkage_Adaa501a_Rate_Basis().notEquals(" "))) //Natural: IF ADAA501A-RETURN-CODE EQ 0 AND ADAA501A-RATE-BASIS NE ' '
                {
                    pnd_Misc_Variables_Pnd_Omni_Rate_Code.setValue(pdaAdaa501a.getAdaa501a_Linkage_Adaa501a_Rate_Basis());                                                //Natural: MOVE ADAA501A-RATE-BASIS TO #OMNI-RATE-CODE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Misc_Variables_Pnd_Omni_Format_Ok.reset();                                                                                                        //Natural: RESET #OMNI-FORMAT-OK
                    pnd_Misc_Variables_Pnd_Rb_Return_Code.setValue(pdaAdaa501a.getAdaa501a_Linkage_Adaa501a_Return_Code());                                               //Natural: MOVE ADAA501A-RETURN-CODE TO #RB-RETURN-CODE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Misc_Variables_Pnd_Debug_On.getBoolean()))                                                                                              //Natural: IF #DEBUG-ON
                {
                    getReports().write(0, NEWLINE,"*","....AFTER CALL TO ADAN501....","=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Pin_A(),"=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct(), //Natural: WRITE / '*' '....AFTER CALL TO ADAN501....' '=' #OMNI-PIN-A '=' #OMNI-TIAA-CNTRCT '=' ADAA501A-RATE-BASIS '=' #OMNI-RATE-CODE '=' #RB-RETURN-CODE '=' ADAA501A-RETURN-CODE '=' #OMNI-FORMAT-OK
                        "=",pdaAdaa501a.getAdaa501a_Linkage_Adaa501a_Rate_Basis(),"=",pnd_Misc_Variables_Pnd_Omni_Rate_Code,"=",pnd_Misc_Variables_Pnd_Rb_Return_Code,
                        "=",pdaAdaa501a.getAdaa501a_Linkage_Adaa501a_Return_Code(),"=",pnd_Misc_Variables_Pnd_Omni_Format_Ok);
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //* * 07062005 MN START
                if (condition(! (pnd_Misc_Variables_Pnd_Omni_Format_Ok.getBoolean())))                                                                                    //Natural: IF NOT #OMNI-FORMAT-OK
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                //* * 07062005 MN END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  010808
            //*  010808
            if (condition(pnd_S_Omni_Contract.notEquals(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct()) || pnd_Misc_Variables_Pnd_Omni_Lob.equals(" ")))             //Natural: IF #S-OMNI-CONTRACT NE #OMNI-TIAA-CNTRCT OR #OMNI-LOB = ' '
            {
                pnd_S_Omni_Contract.setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct());                                                                        //Natural: ASSIGN #S-OMNI-CONTRACT := #OMNI-TIAA-CNTRCT
                pdaNeca4000.getNeca4000().reset();                                                                                                                        //Natural: RESET NECA4000
                pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct());                                                //Natural: ASSIGN NECA4000.PRD-KEY4-ACCT-NBR := #OMNI-TIAA-CNTRCT
                pdaNeca4000.getNeca4000_Function_Cde().setValue("PRD");                                                                                                   //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'PRD'
                pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("04");                                                                                             //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '04'
                //*  010808 START
                if (condition(pnd_Misc_Variables_Pnd_Debug_On.getBoolean()))                                                                                              //Natural: IF #DEBUG-ON
                {
                    getReports().write(0, NEWLINE,"*","....Calling NECN4000....","=",pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr(),"=",pdaNeca4000.getNeca4000_Function_Cde(), //Natural: WRITE / '*' '....Calling NECN4000....' '=' NECA4000.PRD-KEY4-ACCT-NBR '=' NECA4000.FUNCTION-CDE '=' NECA4000.INPT-KEY-OPTION-CDE
                        "=",pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde());
                    if (Global.isEscape()) return;
                    //*  010808 END
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                    //Natural: CALLNAT 'NECN4000' NECA4000
                if (condition(Global.isEscape())) return;
                if (condition(pdaNeca4000.getNeca4000_Return_Cde().equals("00") || pdaNeca4000.getNeca4000_Return_Cde().equals(" ")))                                     //Natural: IF NECA4000.RETURN-CDE = '00' OR = ' '
                {
                    pnd_Misc_Variables_Pnd_Omni_Lob.setValue(pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1));                                                      //Natural: MOVE PRD-PRODUCT-CDE ( 1 ) TO #OMNI-LOB
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Misc_Variables_Pnd_Rb_Return_Code.setValue("4");                                                                                                  //Natural: MOVE '4' TO #RB-RETURN-CODE
                    pnd_Misc_Variables_Pnd_Omni_Format_Ok.reset();                                                                                                        //Natural: RESET #OMNI-FORMAT-OK
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                //*  010808
            }                                                                                                                                                             //Natural: END-IF
            pdaNeca4000.getNeca4000().reset();                                                                                                                            //Natural: RESET NECA4000
            pdaNeca4000.getNeca4000_Request_Ind().setValue(" ");                                                                                                          //Natural: ASSIGN NECA4000.REQUEST-IND := ' '
            pdaNeca4000.getNeca4000_Function_Cde().setValue("RTE");                                                                                                       //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'RTE'
            pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("01");                                                                                                 //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '01'
            pdaNeca4000.getNeca4000_Rte_Key1_Product_Cde().setValue(pnd_Misc_Variables_Pnd_Omni_Lob);                                                                     //Natural: ASSIGN NECA4000.RTE-KEY1-PRODUCT-CDE := #OMNI-LOB
            pdaNeca4000.getNeca4000_Rte_Key1_Ticker_Symbol().setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Ticker());                                               //Natural: ASSIGN NECA4000.RTE-KEY1-TICKER-SYMBOL := #OMNI-FUND-TICKER
            DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                        //Natural: CALLNAT 'NECN4000' NECA4000
            if (condition(Global.isEscape())) return;
            if (condition(pdaNeca4000.getNeca4000_Return_Cde().equals(" ") || pdaNeca4000.getNeca4000_Return_Cde().equals("00")))                                         //Natural: IF NECA4000.RETURN-CDE = ' ' OR = '00'
            {
                pnd_Misc_Variables_Pnd_Omni_Rate_Code.setValue(pdaNeca4000.getNeca4000_Rte_Rate_Cde().getValue(1));                                                       //Natural: MOVE NECA4000.RTE-RATE-CDE ( 1 ) TO #OMNI-RATE-CODE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Misc_Variables_Pnd_Rb_Return_Code.setValue("5");                                                                                                      //Natural: MOVE '5' TO #RB-RETURN-CODE
                pnd_Misc_Variables_Pnd_Omni_Format_Ok.reset();                                                                                                            //Natural: RESET #OMNI-FORMAT-OK
                //*   07062005   MN
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* * 07062005 MN START
        //* *  CHECK IF TPA OR IPRO FROM TPA CONTRACTS HAVE GUARANTEED COMMUTED
        //* *  AMOUNT FIELD POPULATED
        //*  05152006 MN - ICAP/TPA
        //*  05152006 MN
        if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Sub_Plan_Nbr().getSubstring(1,3).equals("TP7")))                                                              //Natural: IF SUBSTR ( #OMNI-SUB-PLAN-NBR,1,3 ) = 'TP7'
        {
            ignore();
            //*  05152006 MN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Sub_Plan_Nbr().getSubstring(1,2).equals("TP") || ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Orig_Cntrct_Lob_Ind().equals("1"))) //Natural: IF SUBSTR ( #OMNI-SUB-PLAN-NBR,1,2 ) = 'TP' OR #OMNI-ORIG-CNTRCT-LOB-IND = '1'
            {
                if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt().equals(getZero()) && ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Contract_Id().greater(getZero()))) //Natural: IF #OMNI-TPA-GUARANT-COMMUT-AMT = 0 AND #OMNI-CONTRACT-ID > 0
                {
                    pnd_Misc_Variables_Pnd_Rb_Return_Code.setValue("6");                                                                                                  //Natural: MOVE '6' TO #RB-RETURN-CODE
                    pnd_Misc_Variables_Pnd_Omni_Format_Ok.reset();                                                                                                        //Natural: RESET #OMNI-FORMAT-OK
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  05152006 MN
        }                                                                                                                                                                 //Natural: END-IF
        //* * 07062005 MN END
    }
    private void sub_Verify_Pin() throws Exception                                                                                                                        //Natural: VERIFY-PIN
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------- *
        pnd_Misc_Variables_Pnd_Pin_Okay.reset();                                                                                                                          //Natural: RESET #PIN-OKAY
        pnd_Keys_Pnd_Cor_Key1_Pin_Nbr.setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Pin());                                                                              //Natural: ASSIGN #COR-KEY1-PIN-NBR := #OMNI-PIN
        pnd_Keys_Pnd_Cor_Key1_Rec_Typ.setValue(1);                                                                                                                        //Natural: ASSIGN #COR-KEY1-REC-TYP := 1
        if (condition(pnd_Misc_Variables_Pnd_Debug_On.getBoolean()))                                                                                                      //Natural: IF #DEBUG-ON
        {
            getReports().write(0, NEWLINE,"*","....INSIDE VERIFY PIN : ","=",pnd_Keys_Pnd_Cor_Cntrct_Key1);                                                               //Natural: WRITE / '*' '....INSIDE VERIFY PIN : ' '=' #COR-CNTRCT-KEY1
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* * REPLACES READ OF COR FILE PER MDM CONVERSION
        //*                                        /* START PIN EXPANSION 02222017
        //* RESET #MDMA100                                   /* 101110
        //* #I-PIN := #OMNI-PIN
        //* CALLNAT 'MDMN100A' #MDMA100                       /* CM 3/4/10
        pdaMdma101.getPnd_Mdma101().reset();                                                                                                                              //Natural: RESET #MDMA101
        pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().compute(new ComputeParameters(false, pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12()), ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Pin_A().val()); //Natural: ASSIGN #I-PIN-N12 := VAL ( #OMNI-PIN-A )
        DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                          //Natural: CALLNAT 'MDMN101A' #MDMA101
        if (condition(Global.isEscape())) return;
        //*                                        /* END  PIN EXPANSION 02222017
        if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #O-RETURN-CODE EQ '0000'
        {
            pnd_Misc_Variables_Pnd_Pin_Okay.setValue(true);                                                                                                               //Natural: ASSIGN #PIN-OKAY := TRUE
            //*  122209
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  122209
            getReports().write(0, "Bad return from MDMN100. RC=",pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code());                                                          //Natural: WRITE 'Bad return from MDMN100. RC=' #O-RETURN-CODE
            if (Global.isEscape()) return;
            //*  122209
            getReports().write(0, pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Text(), new AlphanumericLength (70));                                                            //Natural: WRITE #O-RETURN-TEXT ( AL = 70 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  011713 START
        //*  OS-102013 START
        //* *IF NOT #PIN-OKAY
        //* *  READ COR-XREF-PH-VIEW BY COR-SUPER-PIN-RCDTYPE STARTING FROM
        //* *      #COR-CNTRCT-KEY1
        //* *    IF COR-XREF-PH-VIEW.PH-UNIQUE-ID-NBR = #COR-KEY1-PIN-NBR AND
        //* *        COR-XREF-PH-VIEW.PH-RCD-TYPE-CDE  = #COR-KEY1-REC-TYP
        //* *      #PIN-OKAY := TRUE
        //* *    END-IF
        //* *    ESCAPE BOTTOM
        //* *  END-READ
        //* *END-IF
        //*  011713 END
        //*  OS-102013 END
    }
    private void sub_Find_Ads_Prtcpnt_Match() throws Exception                                                                                                            //Natural: FIND-ADS-PRTCPNT-MATCH
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------- *
        //*  >>> CHECK IF RESETTLEMENTS <<<
        //*  ------------------------------
        if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Usage_Cde2().equals(pnd_Constants_Pnd_Rstl_Usage_Cde.getValue(2,":",5))))                                     //Natural: IF #OMNI-USAGE-CDE2 = #RSTL-USAGE-CDE ( 2:5 )
        {
            pnd_Per_Request_Info_Pnd_Resettlement.setValue(true);                                                                                                         //Natural: MOVE TRUE TO #RESETTLEMENT
            if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Usage_Cde2().equals("1") || ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Usage_Cde2().equals("3")))              //Natural: IF #OMNI-USAGE-CDE2 = '1' OR EQ '3'
            {
                pnd_Per_Request_Info_Pnd_Rstl_Cnt.setValue("1");                                                                                                          //Natural: MOVE '1' TO #RSTL-CNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Per_Request_Info_Pnd_Rstl_Cnt.setValue("2");                                                                                                          //Natural: MOVE '2' TO #RSTL-CNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Per_Request_Info_Pnd_Resettlement.reset();                                                                                                                //Natural: RESET #RESETTLEMENT
        }                                                                                                                                                                 //Natural: END-IF
        //* *MOVE TRUE TO #PRTCPNT-MATCH-FOUND /* 011107
        //*  011107
        pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found.setValue(false);                                                                                                     //Natural: MOVE FALSE TO #PRTCPNT-MATCH-FOUND
        //*  121007
        pnd_Per_Request_Info_Pnd_Processed_Rsttlmnt.reset();                                                                                                              //Natural: RESET #PROCESSED-RSTTLMNT
        //*  >> IF RESETTLEMENT, FIND MATCHING ORIGINAL REQUEST.  IF NOT RSTLMNT,
        //*    CHK IF MULTIPLE REQUEST FOR SAME PIN, PLAN & ANNTY OPTION EXISTS  <<
        //*  ----------------------------------------------------------------------
        if (condition(pnd_Per_Request_Info_Pnd_Resettlement.getBoolean()))                                                                                                //Natural: IF #RESETTLEMENT
        {
            pnd_Keys_Pnd_Key_Opn_Clsd_Ind.setValue(pnd_Constants_Pnd_Close);                                                                                              //Natural: MOVE #CLOSE TO #KEY-OPN-CLSD-IND
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Keys_Pnd_Key_Opn_Clsd_Ind.setValue(pnd_Constants_Pnd_Open);                                                                                               //Natural: MOVE #OPEN TO #KEY-OPN-CLSD-IND
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Misc_Variables_Pnd_Eff_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Eff_Dte_A());                               //Natural: MOVE EDITED #OMNI-EFF-DTE-A TO #EFF-DATE-D ( EM = YYYYMMDD )
        if (condition(pnd_Misc_Variables_Pnd_Debug_On.getBoolean()))                                                                                                      //Natural: IF #DEBUG-ON
        {
            getReports().write(0, "*");                                                                                                                                   //Natural: WRITE '*'
            if (Global.isEscape()) return;
            getReports().write(0, "......BEGINING OF  FIND-ADS-PRTCPNT-MATCH","=",pnd_Keys_Pnd_Adp_Key);                                                                  //Natural: WRITE '......BEGINING OF  FIND-ADS-PRTCPNT-MATCH' '=' #ADP-KEY
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  012111
        pnd_Rqst_Cnt.reset();                                                                                                                                             //Natural: RESET #RQST-CNT
        ldaAdsl401.getVw_ads_Prtcpnt_View().startDatabaseFind                                                                                                             //Natural: FIND ADS-PRTCPNT-VIEW WITH ADP-SUPER4 EQ #ADP-KEY
        (
        "FIND_PRTCPNT",
        new Wc[] { new Wc("ADP_SUPER4", "=", pnd_Keys_Pnd_Adp_Key, WcType.WITH) }
        );
        FIND_PRTCPNT:
        while (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().readNextRow("FIND_PRTCPNT", true)))
        {
            ldaAdsl401.getVw_ads_Prtcpnt_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().getAstCOUNTER().equals(0)))                                                                                 //Natural: IF NO RECORDS FOUND
            {
                pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_No_Match);                                                                               //Natural: MOVE #NO-MATCH TO #OVERALL-STAT
                pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found.reset();                                                                                                     //Natural: RESET #PRTCPNT-MATCH-FOUND
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            //*     >> FIND EXACT MATCH OF ORIGINAL REQUESTS FOR RESETTLEMENTS <<
            //*     -------------------------------------------------------------
            if (condition(pnd_Per_Request_Info_Pnd_Resettlement.getBoolean()))                                                                                            //Natural: IF #RESETTLEMENT
            {
                if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde().equals(pnd_Constants_Pnd_M_Annty_Typ)))                                                   //Natural: IF ADS-PRTCPNT-VIEW.ADP-ANNT-TYP-CDE EQ #M-ANNTY-TYP
                {
                    pnd_Per_Request_Info_Pnd_Total_Pin_Rqst.nadd(1);                                                                                                      //Natural: ADD 1 TO #TOTAL-PIN-RQST
                    if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct().equals(ldaAdsl401.getAds_Prtcpnt_View_Adp_Cntrcts_In_Rqst().getValue("*"))))        //Natural: IF #OMNI-TIAA-CNTRCT EQ ADS-PRTCPNT-VIEW.ADP-CNTRCTS-IN-RQST ( * )
                    {
                        //* * 01122006 MN START
                        if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde_1().notEquals("T")))                                                                    //Natural: IF ADS-PRTCPNT-VIEW.ADP-STTS-CDE-1 NE 'T'
                        {
                            ignore();
                            //*  011107 END
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //* * 01122006 MN END
                            //*  071707 START /* 012111
                            if (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().getAstNUMBER().greater(1)))                                                                 //Natural: IF *NUMBER GT 1
                            {
                                //*  CHANGED *COUNTER WITH
                                                                                                                                                                          //Natural: PERFORM CHECK-CONTRACT
                                sub_Check_Contract();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom("FIND_PRTCPNT"))) break;
                                    else if (condition(Global.isEscapeBottomImmediate("FIND_PRTCPNT"))) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(Map.getDoInput())) {return;}
                                //*  *NUMBER
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Rqst_Found.setValue(true);                                                                                                            //Natural: ASSIGN #RQST-FOUND := TRUE
                            }                                                                                                                                             //Natural: END-IF
                            //*  071707 END
                            if (condition(pnd_Rqst_Found.getBoolean()))                                                                                                   //Natural: IF #RQST-FOUND
                            {
                                //*  121007 START
                                if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Rsttlmnt_Cnt().notEquals(" ")))                                                          //Natural: IF ADS-PRTCPNT-VIEW.ADP-RSTTLMNT-CNT NE ' '
                                {
                                    pnd_Per_Request_Info_Pnd_Rstlmnt_Ctr.compute(new ComputeParameters(false, pnd_Per_Request_Info_Pnd_Rstlmnt_Ctr), ldaAdsl401.getAds_Prtcpnt_View_Adp_Rsttlmnt_Cnt().val().add(1)); //Natural: ASSIGN #RSTLMNT-CTR := VAL ( ADS-PRTCPNT-VIEW.ADP-RSTTLMNT-CNT ) + 1
                                    if (condition(pnd_Per_Request_Info_Pnd_Rstlmnt_Ctr.greater(2) || ldaAdsl401.getAds_Prtcpnt_View_Adp_Rsttlmnt_Cnt().equals(pnd_Per_Request_Info_Pnd_Rstl_Cnt))) //Natural: IF #RSTLMNT-CTR GT 2 OR ADS-PRTCPNT-VIEW.ADP-RSTTLMNT-CNT = #RSTL-CNT
                                    {
                                        //*  012111 START
                                        if (condition(pnd_Rqst_Cnt.greater(1)))                                                                                           //Natural: IF #RQST-CNT GT 1
                                        {
                                            pnd_Rqst_Cnt.nsubtract(1);                                                                                                    //Natural: SUBTRACT 1 FROM #RQST-CNT
                                            if (condition(true)) continue;                                                                                                //Natural: ESCAPE TOP
                                            //*  012111 END
                                        }                                                                                                                                 //Natural: ELSE
                                        else if (condition())
                                        {
                                            pnd_Per_Request_Info_Pnd_Processed_Rsttlmnt.setValue(true);                                                                   //Natural: ASSIGN #PROCESSED-RSTTLMNT := TRUE
                                            if (true) return;                                                                                                             //Natural: ESCAPE ROUTINE
                                        }                                                                                                                                 //Natural: END-IF
                                    }                                                                                                                                     //Natural: END-IF
                                    //*  121007 END
                                }                                                                                                                                         //Natural: END-IF
                                pnd_Per_Request_Info_Pnd_Full_Match.nadd(1);                                                                                              //Natural: ADD 1 TO #FULL-MATCH
                                ldaAdsl400a.getPnd_Prtcpnt_Adp_Isn().setValue(ldaAdsl401.getVw_ads_Prtcpnt_View().getAstISN("FIND_PRTCPNT"));                             //Natural: MOVE *ISN ( FIND-PRTCPNT. ) TO #PRTCPNT.ADP-ISN
                                //*  011107
                                pnd_Per_Request_Info_Pnd_Resettl_Orig_Rqstid.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                          //Natural: MOVE ADS-PRTCPNT-VIEW.RQST-ID TO #RESETTL-ORIG-RQSTID
                                pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found.setValue(true);                                                                              //Natural: ASSIGN #PRTCPNT-MATCH-FOUND := TRUE
                                //*  071707
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                                //*  071707
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Per_Request_Info_Pnd_Total_Pin_Rqst.nadd(1);                                                                                                          //Natural: ADD 1 TO #TOTAL-PIN-RQST
                //*        >> IDENTIFY MATCH CRITERIA - HANDLES MULTIPLE PIN REQUESTS <<
                //*        -------------------------------------------------------------
                short decideConditionsMet3045 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ADS-PRTCPNT-VIEW.ADP-EFFCTV-DTE EQ #EFF-DATE-D AND #OMNI-TIAA-CNTRCT EQ ADS-PRTCPNT-VIEW.ADP-CNTRCTS-IN-RQST ( * )
                if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte().equals(pnd_Misc_Variables_Pnd_Eff_Date_D) && ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct().equals(ldaAdsl401.getAds_Prtcpnt_View_Adp_Cntrcts_In_Rqst().getValue("*"))))
                {
                    decideConditionsMet3045++;
                    pnd_Per_Request_Info_Pnd_Full_Match.nadd(1);                                                                                                          //Natural: ADD 1 TO #FULL-MATCH
                    pnd_Per_Request_Info_Pnd_Rqst_Id.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                                  //Natural: MOVE ADS-PRTCPNT-VIEW.RQST-ID TO #RQST-ID
                }                                                                                                                                                         //Natural: WHEN #OMNI-TIAA-CNTRCT EQ ADS-PRTCPNT-VIEW.ADP-CNTRCTS-IN-RQST ( * )
                else if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct().equals(ldaAdsl401.getAds_Prtcpnt_View_Adp_Cntrcts_In_Rqst().getValue("*"))))
                {
                    decideConditionsMet3045++;
                    pnd_Per_Request_Info_Pnd_Partial_Match.nadd(1);                                                                                                       //Natural: ADD 1 TO #PARTIAL-MATCH
                    if (condition(pnd_Per_Request_Info_Pnd_Full_Match.equals(getZero())))                                                                                 //Natural: IF #FULL-MATCH EQ 0
                    {
                        pnd_Per_Request_Info_Pnd_Rqst_Id.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                              //Natural: MOVE ADS-PRTCPNT-VIEW.RQST-ID TO #RQST-ID
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Per_Request_Info_Pnd_Unmatched.nadd(1);                                                                                                           //Natural: ADD 1 TO #UNMATCHED
                    if (condition(pnd_Per_Request_Info_Pnd_Partial_Match.equals(getZero()) && pnd_Per_Request_Info_Pnd_Full_Match.equals(getZero())))                     //Natural: IF #PARTIAL-MATCH EQ 0 AND #FULL-MATCH EQ 0
                    {
                        pnd_Per_Request_Info_Pnd_Rqst_Id.setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                              //Natural: MOVE ADS-PRTCPNT-VIEW.RQST-ID TO #RQST-ID
                    }                                                                                                                                                     //Natural: END-IF
                    //*  011107
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found.setValue(true);                                                                                              //Natural: ASSIGN #PRTCPNT-MATCH-FOUND := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(pnd_Per_Request_Info_Pnd_Resettlement.getBoolean()))                                                                                                //Natural: IF #RESETTLEMENT
        {
            if (condition(pnd_Per_Request_Info_Pnd_Full_Match.notEquals(1)))                                                                                              //Natural: IF #FULL-MATCH NE 1
            {
                pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_No_Match);                                                                               //Natural: MOVE #NO-MATCH TO #OVERALL-STAT
                pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found.reset();                                                                                                     //Natural: RESET #PRTCPNT-MATCH-FOUND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Per_Request_Info_Pnd_Full_Match.notEquals(1)))                                                                                              //Natural: IF #FULL-MATCH NE 1
            {
                if (condition((pnd_Per_Request_Info_Pnd_Partial_Match.greater(1)) || (pnd_Per_Request_Info_Pnd_Partial_Match.equals(getZero()) && pnd_Per_Request_Info_Pnd_Unmatched.notEquals(1)))) //Natural: IF ( #PARTIAL-MATCH GT 1 ) OR ( #PARTIAL-MATCH EQ 0 AND #UNMATCHED NE 1 )
                {
                    pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_No_Match);                                                                           //Natural: MOVE #NO-MATCH TO #OVERALL-STAT
                    pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found.reset();                                                                                                 //Natural: RESET #PRTCPNT-MATCH-FOUND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  011107 START
        //* * NOTE: I DON't know the purpose of the check above (#FULL-MATCH NE 1)
        //* *       SO I HAVE TO ADD THE FOLLOWING IN ORDER TO IDENTIFY
        //* *       UNMATCHED REQUESTS.
        if (condition(! (pnd_Per_Request_Info_Pnd_Resettlement.getBoolean())))                                                                                            //Natural: IF NOT #RESETTLEMENT
        {
            if (condition(pnd_Per_Request_Info_Pnd_Full_Match.equals(getZero()) && pnd_Per_Request_Info_Pnd_Partial_Match.equals(getZero()) && pnd_Per_Request_Info_Pnd_Unmatched.greater(getZero()))) //Natural: IF #FULL-MATCH = 0 AND #PARTIAL-MATCH = 0 AND #UNMATCHED GT 0
            {
                pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_No_Match);                                                                               //Natural: MOVE #NO-MATCH TO #OVERALL-STAT
                //*  THIS WILL CREATE ADAS TRANSACTION
                pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found.reset();                                                                                                     //Natural: RESET #PRTCPNT-MATCH-FOUND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  011107 END
        if (condition(pnd_Misc_Variables_Pnd_Debug_On.getBoolean()))                                                                                                      //Natural: IF #DEBUG-ON
        {
            getReports().write(0, "*");                                                                                                                                   //Natural: WRITE '*'
            if (Global.isEscape()) return;
            getReports().write(0, "......INSIDE FIND-ADS-PRTCPNT-MATCH","=",pnd_Per_Request_Info_Pnd_Rqst_Id,"=",pnd_Per_Request_Info_Pnd_Resettl_Orig_Rqstid,            //Natural: WRITE '......INSIDE FIND-ADS-PRTCPNT-MATCH' '=' #RQST-ID '=' #RESETTL-ORIG-RQSTID '=' #PRTCPNT-MATCH-FOUND '=' #FULL-MATCH '=' #PARTIAL-MATCH '=' #UNMATCHED '=' #EFF-DATE-D '=' #RESETTLEMENT '=' #OVERALL-STAT '=' #TOTAL-PIN-RQST
                "=",pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found,"=",pnd_Per_Request_Info_Pnd_Full_Match,"=",pnd_Per_Request_Info_Pnd_Partial_Match,"=",
                pnd_Per_Request_Info_Pnd_Unmatched,"=",pnd_Misc_Variables_Pnd_Eff_Date_D,"=",pnd_Per_Request_Info_Pnd_Resettlement,"=",pnd_Per_Request_Info_Pnd_Overall_Stat,
                "=",pnd_Per_Request_Info_Pnd_Total_Pin_Rqst);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  >> CHECK IF RESETTLEMENT IS ALREADY PROCESSED-HANDLES BATCH RESTART <<
        //*  ----------------------------------------------------------------------
        if (condition(pnd_Per_Request_Info_Pnd_Resettlement.getBoolean()))                                                                                                //Natural: IF #RESETTLEMENT
        {
            pnd_Keys_Pnd_Key_Opn_Clsd_Ind.setValue(pnd_Constants_Pnd_Open);                                                                                               //Natural: MOVE #OPEN TO #KEY-OPN-CLSD-IND
            ldaAdsl401.getVw_ads_Prtcpnt_View().startDatabaseFind                                                                                                         //Natural: FIND ADS-PRTCPNT-VIEW WITH ADP-SUPER4 EQ #ADP-KEY
            (
            "FIND_PRTCPNT1",
            new Wc[] { new Wc("ADP_SUPER4", "=", pnd_Keys_Pnd_Adp_Key, WcType.WITH) }
            );
            FIND_PRTCPNT1:
            while (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().readNextRow("FIND_PRTCPNT1", true)))
            {
                ldaAdsl401.getVw_ads_Prtcpnt_View().setIfNotFoundControlFlag(false);
                if (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().getAstCOUNTER().equals(0)))                                                                             //Natural: IF NO RECORDS FOUND
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
                if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte().equals(pnd_Misc_Variables_Pnd_Eff_Date_D) && ldaAdsl401.getAds_Prtcpnt_View_Adp_Rsttlmnt_Cnt().equals(pnd_Per_Request_Info_Pnd_Rstl_Cnt)  //Natural: IF ADS-PRTCPNT-VIEW.ADP-EFFCTV-DTE EQ #EFF-DATE-D AND ADS-PRTCPNT-VIEW.ADP-RSTTLMNT-CNT EQ #RSTL-CNT AND #OMNI-TIAA-CNTRCT EQ ADS-PRTCPNT-VIEW.ADP-CNTRCTS-IN-RQST ( * )
                    && ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct().equals(ldaAdsl401.getAds_Prtcpnt_View_Adp_Cntrcts_In_Rqst().getValue("*"))))
                {
                    pnd_Per_Request_Info_Pnd_Processed_Omni_Rqst.setValue(true);                                                                                          //Natural: MOVE TRUE TO #PROCESSED-OMNI-RQST
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found.getBoolean()))                                                                                     //Natural: IF #PRTCPNT-MATCH-FOUND
            {
                ldaAdsl401.getVw_ads_Prtcpnt_View().startDatabaseFind                                                                                                     //Natural: FIND ADS-PRTCPNT-VIEW WITH RQST-ID EQ #RQST-ID
                (
                "FIND_PRTCPNT2",
                new Wc[] { new Wc("RQST_ID", "=", pnd_Per_Request_Info_Pnd_Rqst_Id, WcType.WITH) }
                );
                FIND_PRTCPNT2:
                while (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().readNextRow("FIND_PRTCPNT2", true)))
                {
                    ldaAdsl401.getVw_ads_Prtcpnt_View().setIfNotFoundControlFlag(false);
                    if (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().getAstCOUNTER().equals(0)))                                                                         //Natural: IF NO RECORDS FOUND
                    {
                        if (true) break FIND_PRTCPNT2;                                                                                                                    //Natural: ESCAPE BOTTOM ( FIND-PRTCPNT2. )
                    }                                                                                                                                                     //Natural: END-NOREC
                    //*          >> CHK ALREADY PROCESSED OMNI RQST (FOR BATCH RESTART) <<
                    //*          ----------------------------------------------------------
                    if (condition(! (ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().equals(pnd_Constants_Pnd_Original_Stat_Array.getValue("*")))))                         //Natural: IF NOT ADS-PRTCPNT-VIEW.ADP-STTS-CDE EQ #ORIGINAL-STAT-ARRAY ( * )
                    {
                        pnd_Per_Request_Info_Pnd_Processed_Omni_Rqst.setValue(true);                                                                                      //Natural: MOVE TRUE TO #PROCESSED-OMNI-RQST
                        if (true) break FIND_PRTCPNT2;                                                                                                                    //Natural: ESCAPE BOTTOM ( FIND-PRTCPNT2. )
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl400a.getPnd_Prtcpnt().setValuesByName(ldaAdsl401.getVw_ads_Prtcpnt_View());                                                                    //Natural: MOVE BY NAME ADS-PRTCPNT-VIEW TO #PRTCPNT
                    ldaAdsl400a.getPnd_Prtcpnt_Adp_Isn().setValue(ldaAdsl401.getVw_ads_Prtcpnt_View().getAstISN("FIND_PRTCPNT2"));                                        //Natural: MOVE *ISN ( FIND-PRTCPNT2. ) TO #PRTCPNT.ADP-ISN
                    pnd_Per_Request_Info_Pnd_All_Tiaa_Grd_Pct.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Settl_All_Tiaa_Grd_Pct());                                      //Natural: MOVE ADP-SETTL-ALL-TIAA-GRD-PCT TO #ALL-TIAA-GRD-PCT
                    pnd_Per_Request_Info_Pnd_All_Cref_Mon_Pct.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Settl_All_Cref_Mon_Pct());                                      //Natural: MOVE ADP-SETTL-ALL-CREF-MON-PCT TO #ALL-CREF-MON-PCT
                    short decideConditionsMet3129 = 0;                                                                                                                    //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ADS-PRTCPNT-VIEW.ADP-STTS-CDE EQ #ORIG-2ND-SIGHT
                    if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().equals(pnd_Constants_Pnd_Orig_2nd_Sight)))
                    {
                        decideConditionsMet3129++;
                        pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_Await_2nd_Sight);                                                                //Natural: MOVE #AWAIT-2ND-SIGHT TO #OVERALL-STAT
                    }                                                                                                                                                     //Natural: WHEN ADS-PRTCPNT-VIEW.ADP-STTS-CDE EQ #ORIG-INCMPLTE
                    else if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().equals(pnd_Constants_Pnd_Orig_Incmplte)))
                    {
                        decideConditionsMet3129++;
                        pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_Incmplte);                                                                       //Natural: MOVE #INCMPLTE TO #OVERALL-STAT
                    }                                                                                                                                                     //Natural: WHEN ADS-PRTCPNT-VIEW.ADP-STTS-CDE EQ #ORIG-INCMPLTE-MIT1
                    else if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().equals(pnd_Constants_Pnd_Orig_Incmplte_Mit1)))
                    {
                        decideConditionsMet3129++;
                        pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_Incmplte);                                                                       //Natural: MOVE #INCMPLTE TO #OVERALL-STAT
                    }                                                                                                                                                     //Natural: WHEN ADS-PRTCPNT-VIEW.ADP-STTS-CDE EQ #ORIG-INCMPLTE-MIT2
                    else if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().equals(pnd_Constants_Pnd_Orig_Incmplte_Mit2)))
                    {
                        decideConditionsMet3129++;
                        pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_Incmplte);                                                                       //Natural: MOVE #INCMPLTE TO #OVERALL-STAT
                    }                                                                                                                                                     //Natural: WHEN ADS-PRTCPNT-VIEW.ADP-STTS-CDE EQ #ORIG-INCMPLTE-MIT3
                    else if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().equals(pnd_Constants_Pnd_Orig_Incmplte_Mit3)))
                    {
                        decideConditionsMet3129++;
                        pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_Incmplte);                                                                       //Natural: MOVE #INCMPLTE TO #OVERALL-STAT
                    }                                                                                                                                                     //Natural: WHEN ADS-PRTCPNT-VIEW.ADP-STTS-CDE NE #AWAIT-OMNI
                    else if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().notEquals(pnd_Constants_Pnd_Await_Omni)))
                    {
                        decideConditionsMet3129++;
                        pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_No_Match);                                                                       //Natural: MOVE #NO-MATCH TO #OVERALL-STAT
                        pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found.reset();                                                                                             //Natural: RESET #PRTCPNT-MATCH-FOUND
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    //*          >>> CHECK IF EFFECT DATE DO NOT MATCH <<<
                    //*          ------------------------------------------
                    if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte().notEquals(pnd_Misc_Variables_Pnd_Eff_Date_D)))                                          //Natural: IF ADS-PRTCPNT-VIEW.ADP-EFFCTV-DTE NE #EFF-DATE-D
                    {
                        short decideConditionsMet3151 = 0;                                                                                                                //Natural: DECIDE ON FIRST VALUE #OVERALL-STAT;//Natural: VALUE #AWAIT-2ND-SIGHT-ARRAY ( * )
                        if (condition((pnd_Per_Request_Info_Pnd_Overall_Stat.equals(pnd_Constants_Pnd_Await_2nd_Sight_Array.getValue("*")))))
                        {
                            decideConditionsMet3151++;
                            pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_Awt_Eff_Dte);                                                                //Natural: MOVE #AWT-EFF-DTE TO #OVERALL-STAT
                        }                                                                                                                                                 //Natural: VALUE #INCOMPLETE-ARRAY ( * )
                        else if (condition((pnd_Per_Request_Info_Pnd_Overall_Stat.equals(pnd_Constants_Pnd_Incomplete_Array.getValue("*")))))
                        {
                            decideConditionsMet3151++;
                            pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_Inc_Eff_Dte);                                                                //Natural: MOVE #INC-EFF-DTE TO #OVERALL-STAT
                        }                                                                                                                                                 //Natural: NONE
                        else if (condition())
                        {
                            pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_Delayed_Eff_Dte);                                                            //Natural: MOVE #DELAYED-EFF-DTE TO #OVERALL-STAT
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FIND
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_Contract() throws Exception                                                                                                                    //Natural: CHECK-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Rqst_Found.setValue(false);                                                                                                                                   //Natural: ASSIGN #RQST-FOUND := FALSE
        //*  012111 START
        if (condition(pnd_Rqst_Cnt.equals(getZero())))                                                                                                                    //Natural: IF #RQST-CNT = 0
        {
            vw_ads_Part.startDatabaseFind                                                                                                                                 //Natural: FIND ADS-PART WITH ADP-SUPER4 = #ADP-KEY
            (
            "FIND01",
            new Wc[] { new Wc("ADP_SUPER4", "=", pnd_Keys_Pnd_Adp_Key, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_ads_Part.readNextRow("FIND01")))
            {
                vw_ads_Part.setIfNotFoundControlFlag(false);
                if (condition(!(ads_Part_Adp_Annt_Typ_Cde.equals(pnd_Constants_Pnd_M_Annty_Typ))))                                                                        //Natural: ACCEPT IF ADS-PART.ADP-ANNT-TYP-CDE EQ #M-ANNTY-TYP
                {
                    continue;
                }
                pnd_Rqst_Cnt.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #RQST-CNT
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
            //*  012111 END
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl401a.getVw_ads_Cntrct_View().startDatabaseFind                                                                                                             //Natural: FIND ADS-CNTRCT-VIEW WITH ADS-CNTRCT-VIEW.RQST-ID = ADS-PRTCPNT-VIEW.RQST-ID
        (
        "FIND02",
        new Wc[] { new Wc("RQST_ID", "=", ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id(), WcType.WITH) }
        );
        FIND02:
        while (condition(ldaAdsl401a.getVw_ads_Cntrct_View().readNextRow("FIND02", true)))
        {
            ldaAdsl401a.getVw_ads_Cntrct_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAdsl401a.getVw_ads_Cntrct_View().getAstCOUNTER().equals(0)))                                                                                 //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "IN CHECK-CONTRACT");                                                                                                               //Natural: WRITE 'IN CHECK-CONTRACT'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "NO CONTRACT RECORD FOUND FOR RQST-ID",ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                   //Natural: WRITE 'NO CONTRACT RECORD FOUND FOR RQST-ID' ADS-PRTCPNT-VIEW.RQST-ID
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            //*  TNG0613 START                   /* OS-102013 START
            //*  IF #OMNI-FUND-TICKER = ADS-CNTRCT-VIEW.ADC-TKR-SYMBL (*)
            //*    EXAMINE ADS-CNTRCT-VIEW.ADC-TKR-SYMBL (*) FOR
            //*      #OMNI-FUND-TICKER GIVING INDEX #ACCT-NDX
            //*  IF #OMNI-INVSTMT-ID  = ADS-CNTRCT-VIEW.ADC-INVSTMT-ID (*)
            //*    EXAMINE ADS-CNTRCT-VIEW.ADC-INVSTMT-ID (*) FOR
            //*      #OMNI-INVSTMT-ID GIVING INDEX #ACCT-NDX
            //*  TNG0613 END
            //*    IF ADS-CNTRCT-VIEW.ADC-ACCT-ACTL-AMT (#ACCT-NDX) GT 0
            //*  OS - 071613 START: THE FOLLOWING FIELDS ARE NOT FILLED SO WE HAVE TO
            //*       USE ADC-T395 ONES.
            //*        AND ADS-CNTRCT-VIEW.ADC-ACCT-TYP (#ACCT-NDX) = 'P' /* 062013
            //*        AND ADS-CNTRCT-VIEW.ADC-ACCT-QTY (#ACCT-NDX) = 100 /* 062013
            DbsUtil.examine(new ExamineSource(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Ticker().getValue("*")), new ExamineSearch(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Ticker()),  //Natural: EXAMINE #ACCT-TICKER ( * ) FOR #OMNI-FUND-TICKER GIVING INDEX #ACCT-NDX
                new ExamineGivingIndex(pnd_Acct_Ndx));
            if (condition(pnd_Acct_Ndx.greater(getZero())))                                                                                                               //Natural: IF #ACCT-NDX > 0
            {
                DbsUtil.examine(new ExamineSource(ldaAdsl401a.getAds_Cntrct_View_Adc_Invstmnt_Grpng_Id().getValue("*")), new ExamineSearch(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_Acct_Ndx)),  //Natural: EXAMINE ADS-CNTRCT-VIEW.ADC-INVSTMNT-GRPNG-ID ( * ) FOR #ACCT-INVSTMNT-GRPNG-ID ( #ACCT-NDX ) GIVING INDEX #ACCT-NDX1
                    new ExamineGivingIndex(pnd_Acct_Ndx1));
                if (condition(pnd_Acct_Ndx1.greater(getZero())))                                                                                                          //Natural: IF #ACCT-NDX1 > 0
                {
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_Acct_Ndx1).greater(getZero()) && ldaAdsl401a.getAds_Cntrct_View_Adc_T395_Percent_Dollar().getValue(pnd_Acct_Ndx1).equals("P")  //Natural: IF ADS-CNTRCT-VIEW.ADC-ACCT-ACTL-AMT ( #ACCT-NDX1 ) GT 0 AND ADC-T395-PERCENT-DOLLAR ( #ACCT-NDX1 ) = 'P' AND ADC-T395-QUANTITY ( #ACCT-NDX1 ) = 100
                        && ldaAdsl401a.getAds_Cntrct_View_Adc_T395_Quantity().getValue(pnd_Acct_Ndx1).equals(100)))
                    {
                        pnd_Rqst_Found.setValue(true);                                                                                                                    //Natural: ASSIGN #RQST-FOUND := TRUE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  OS-102013 END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  071707
    }
    private void sub_Create_Or_Update_Ads_Prtcpnt() throws Exception                                                                                                      //Natural: CREATE-OR-UPDATE-ADS-PRTCPNT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------------- *
        if (condition(pnd_Misc_Variables_Pnd_Debug_On.getBoolean()))                                                                                                      //Natural: IF #DEBUG-ON
        {
            getReports().write(0, "*");                                                                                                                                   //Natural: WRITE '*'
            if (Global.isEscape()) return;
            getReports().write(0, "......INSIDE CREATE-OR-UPDATE-ADS-PRTCPNT","KEY USED TO FIND MATCH : ",pnd_Keys_Pnd_Adp_Key,"=",pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found, //Natural: WRITE '......INSIDE CREATE-OR-UPDATE-ADS-PRTCPNT' 'KEY USED TO FIND MATCH : ' #ADP-KEY '=' #PRTCPNT-MATCH-FOUND '=' #PROCESSED-OMNI-RQST '=' #OVERALL-STAT '=' #PRTCPNT.ADP-ISN
                "=",pnd_Per_Request_Info_Pnd_Processed_Omni_Rqst,"=",pnd_Per_Request_Info_Pnd_Overall_Stat,"=",ldaAdsl400a.getPnd_Prtcpnt_Adp_Isn());
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  >>> UPDATE OR STORE PARTICIPANT RECORD <<<
        //*  ------------------------------------------
        short decideConditionsMet3216 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #PROCESSED-OMNI-RQST
        if (condition(pnd_Per_Request_Info_Pnd_Processed_Omni_Rqst.getBoolean()))
        {
            decideConditionsMet3216++;
            ldaAdsl400a.getPnd_Prtcpnt_Adp_Rqst_Complete().setValue(true);                                                                                                //Natural: MOVE TRUE TO #PRTCPNT.ADP-RQST-COMPLETE
        }                                                                                                                                                                 //Natural: WHEN #PRTCPNT-MATCH-FOUND AND NOT #RESETTLEMENT
        else if (condition(pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found.getBoolean() && ! (pnd_Per_Request_Info_Pnd_Resettlement.getBoolean())))
        {
            decideConditionsMet3216++;
            PND_PND_L2734:                                                                                                                                                //Natural: GET ADS-PRTCPNT-VIEW #PRTCPNT.ADP-ISN
            ldaAdsl401.getVw_ads_Prtcpnt_View().readByID(ldaAdsl400a.getPnd_Prtcpnt_Adp_Isn().getLong(), "PND_PND_L2734");
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde().setValue(pnd_Constants_Pnd_M_Annty_Typ);                                                                    //Natural: MOVE #M-ANNTY-TYP TO ADS-PRTCPNT-VIEW.ADP-ANNT-TYP-CDE
            ldaAdsl401.getVw_ads_Prtcpnt_View().updateDBRow("PND_PND_L2734");                                                                                             //Natural: UPDATE ( ##L2734. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: WHEN NOT #PRTCPNT-MATCH-FOUND AND #RESETTLEMENT
        else if (condition(! ((pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found.getBoolean()) && pnd_Per_Request_Info_Pnd_Resettlement.getBoolean())))
        {
            decideConditionsMet3216++;
            pnd_Per_Request_Info_Pnd_Unmatched_Rstlmt.setValue(true);                                                                                                     //Natural: MOVE TRUE TO #UNMATCHED-RSTLMT
            ldaAdsl400a.getPnd_Prtcpnt_Adp_Rqst_Complete().setValue(true);                                                                                                //Natural: MOVE TRUE TO #PRTCPNT.ADP-RQST-COMPLETE
        }                                                                                                                                                                 //Natural: WHEN NOT #PRTCPNT-MATCH-FOUND AND NOT #RESETTLEMENT OR #PRTCPNT-MATCH-FOUND AND #RESETTLEMENT
        else if (condition(((! (pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found.getBoolean()) && ! (pnd_Per_Request_Info_Pnd_Resettlement.getBoolean())) 
            || (pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found.getBoolean() && pnd_Per_Request_Info_Pnd_Resettlement.getBoolean()))))
        {
            decideConditionsMet3216++;
            //*   >>> IF RESETTLEMENT, COPY ORIGINAL REQUEST PARTICIPANT INFO <<<
            //*   ---------------------------------------------------------------
            if (condition(pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found.getBoolean() && pnd_Per_Request_Info_Pnd_Resettlement.getBoolean()))                               //Natural: IF #PRTCPNT-MATCH-FOUND AND #RESETTLEMENT
            {
                //*  121007
                GT:                                                                                                                                                       //Natural: GET ADS-PRTCPNT-VIEW #PRTCPNT.ADP-ISN
                ldaAdsl401.getVw_ads_Prtcpnt_View().readByID(ldaAdsl400a.getPnd_Prtcpnt_Adp_Isn().getLong(), "GT");
                //*  121007
                ldaAdsl401.getAds_Prtcpnt_View_Adp_Rsttlmnt_Cnt().setValue(pnd_Per_Request_Info_Pnd_Rstl_Cnt);                                                            //Natural: MOVE #RSTL-CNT TO ADS-PRTCPNT-VIEW.ADP-RSTTLMNT-CNT
                //*  121007
                ldaAdsl401.getVw_ads_Prtcpnt_View().updateDBRow("GT");                                                                                                    //Natural: UPDATE ( GT. )
                //*  121007
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                ldaAdsl401.getAds_Prtcpnt_View_Adp_Orig_Rqst_Id().setValue(ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                     //Natural: MOVE ADS-PRTCPNT-VIEW.RQST-ID TO ADS-PRTCPNT-VIEW.ADP-ORIG-RQST-ID
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl401.getVw_ads_Prtcpnt_View().reset();                                                                                                              //Natural: RESET ADS-PRTCPNT-VIEW
            }                                                                                                                                                             //Natural: END-IF
            pnd_Per_Request_Info_Pnd_Rqst_Pin.compute(new ComputeParameters(false, pnd_Per_Request_Info_Pnd_Rqst_Pin), ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Pin_A().val()); //Natural: ASSIGN #RQST-PIN := VAL ( #OMNI-PIN-A )
            pnd_Per_Request_Info_Pnd_Rqst_Effctv_Dte.setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Eff_Dte());                                                           //Natural: MOVE #OMNI-EFF-DTE TO #RQST-EFFCTV-DTE
            pnd_Per_Request_Info_Pnd_Rqst_Entry_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                      //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO #RQST-ENTRY-DTE
            pnd_Per_Request_Info_Pnd_Rqst_Entry_Tme.setValueEdited(Global.getTIMX(),new ReportEditMask("HHIISST"));                                                       //Natural: MOVE EDITED *TIMX ( EM = HHIISST ) TO #RQST-ENTRY-TME
            //*   PIN EXPANSION 02222017 START
            //*    MOVE #RQST-ID                   TO ADS-PRTCPNT-VIEW.RQST-ID
            ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Per_Request_Info_Pnd_Rqst_Pin, pnd_Per_Request_Info_Pnd_Rqst_Effctv_Dte,  //Natural: COMPRESS #RQST-PIN #RQST-EFFCTV-DTE #RQST-ENTRY-DTE #RQST-ENTRY-TME INTO ADS-PRTCPNT-VIEW.RQST-ID LEAVING NO
                pnd_Per_Request_Info_Pnd_Rqst_Entry_Dte, pnd_Per_Request_Info_Pnd_Rqst_Entry_Tme));
            //*   PIN EXPANSION 02222017 END
            //*  TNG0613 START
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Group_Rqst_Id().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Per_Request_Info_Pnd_Rqst_Pin,                //Natural: COMPRESS #RQST-PIN #RQST-ENTRY-DTE #RQST-ENTRY-TME INTO ADS-PRTCPNT-VIEW.ADP-GROUP-RQST-ID LEAVING NO
                pnd_Per_Request_Info_Pnd_Rqst_Entry_Dte, pnd_Per_Request_Info_Pnd_Rqst_Entry_Tme));
            //*  TNG0613 END
            if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Emp_Term_Dte().notEquals(getZero())))                                                                     //Natural: IF #OMNI-EMP-TERM-DTE NE 0
            {
                ldaAdsl401.getAds_Prtcpnt_View_Adp_Emplymnt_Term_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Emp_Term_Dte_A()); //Natural: MOVE EDITED #OMNI-EMP-TERM-DTE-A TO ADS-PRTCPNT-VIEW.ADP-EMPLYMNT-TERM-DTE ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte().setValue(pnd_Adsn401_Pnd_Cntl_Bsnss_Dte);                                                                 //Natural: MOVE #CNTL-BSNSS-DTE TO ADS-PRTCPNT-VIEW.ADP-LST-ACTVTY-DTE
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Rsttlmnt_Cnt().setValue(pnd_Per_Request_Info_Pnd_Rstl_Cnt);                                                                //Natural: MOVE #RSTL-CNT TO ADS-PRTCPNT-VIEW.ADP-RSTTLMNT-CNT
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Unique_Id().compute(new ComputeParameters(false, ldaAdsl401.getAds_Prtcpnt_View_Adp_Unique_Id()), ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Pin_A().val()); //Natural: ASSIGN ADS-PRTCPNT-VIEW.ADP-UNIQUE-ID := VAL ( #OMNI-PIN-A )
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Frst_Annt_Ssn().setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Ssn());                                                     //Natural: MOVE #OMNI-SSN TO ADS-PRTCPNT-VIEW.ADP-FRST-ANNT-SSN
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Eff_Dte_A());             //Natural: MOVE EDITED #OMNI-EFF-DTE-A TO ADS-PRTCPNT-VIEW.ADP-EFFCTV-DTE ( EM = YYYYMMDD )
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind().setValue(pnd_Constants_Pnd_Open);                                                                           //Natural: MOVE #OPEN TO ADS-PRTCPNT-VIEW.ADP-OPN-CLSD-IND
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Optn().setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Annty_Optn());                                                 //Natural: MOVE #OMNI-ANNTY-OPTN TO ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Plan_Nbr().setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Plan_Nbr());                                                     //Natural: MOVE #OMNI-PLAN-NBR TO ADS-PRTCPNT-VIEW.ADP-PLAN-NBR
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Rcprcl_Dte().compute(new ComputeParameters(false, ldaAdsl401.getAds_Prtcpnt_View_Adp_Rcprcl_Dte()), DbsField.subtract(99999999, //Natural: COMPUTE ADS-PRTCPNT-VIEW.ADP-RCPRCL-DTE = 99999999 - #OMNI-EFF-DTE
                ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Eff_Dte()));
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Entry_Dte().setValue(Global.getDATX());                                                                                    //Natural: MOVE *DATX TO ADS-PRTCPNT-VIEW.ADP-ENTRY-DTE
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Entry_Tme().setValue(Global.getTIMX());                                                                                    //Natural: MOVE *TIMX TO ADS-PRTCPNT-VIEW.ADP-ENTRY-TME
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Entry_User_Id().setValue(pnd_Constants_Pnd_Batchads);                                                                      //Natural: MOVE #BATCHADS TO ADS-PRTCPNT-VIEW.ADP-ENTRY-USER-ID
            //*   START RS0
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Roth_Plan_Type().setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Plan_Type());                                         //Natural: MOVE #OMNI-ROTH-PLAN-TYPE TO ADS-PRTCPNT-VIEW.ADP-ROTH-PLAN-TYPE
            if (condition(pnd_Roth_Request.getBoolean()))                                                                                                                 //Natural: IF #ROTH-REQUEST
            {
                ldaAdsl401.getAds_Prtcpnt_View_Adp_Roth_Rqst_Ind().setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Rqst_Ind());                                       //Natural: MOVE #OMNI-ROTH-RQST-IND TO ADS-PRTCPNT-VIEW.ADP-ROTH-RQST-IND
                if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte_A().greater("00000000")))                                                             //Natural: IF #OMNI-ROTH-DSBLTY-DTE-A > '00000000'
                {
                    ldaAdsl401.getAds_Prtcpnt_View_Adp_Roth_Dsblty_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte_A()); //Natural: MOVE EDITED #OMNI-ROTH-DSBLTY-DTE-A TO ADS-PRTCPNT-VIEW.ADP-ROTH-DSBLTY-DTE ( EM = YYYYMMDD )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaAdsl401.getAds_Prtcpnt_View_Adp_Roth_Dsblty_Dte().reset();                                                                                         //Natural: RESET ADS-PRTCPNT-VIEW.ADP-ROTH-DSBLTY-DTE
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl401.getAds_Prtcpnt_View_Adp_Roth_Frst_Cntrbtn_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte_A()); //Natural: MOVE EDITED #OMNI-ROTH-FRST-CONTRIB-DTE-A TO ADS-PRTCPNT-VIEW.ADP-ROTH-FRST-CNTRBTN-DTE ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-IF
            //*   END   RS0
            if (condition(pnd_Per_Request_Info_Pnd_Resettlement.getBoolean()))                                                                                            //Natural: IF #RESETTLEMENT
            {
                ldaAdsl401.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde().setValue(pnd_Constants_Pnd_R_Annty_Typ);                                                                //Natural: MOVE #R-ANNTY-TYP TO ADS-PRTCPNT-VIEW.ADP-ANNT-TYP-CDE
                ldaAdsl401.getAds_Prtcpnt_View_Adp_In_Prgrss_Ind().reset();                                                                                               //Natural: RESET ADS-PRTCPNT-VIEW.ADP-IN-PRGRSS-IND ADS-PRTCPNT-VIEW.ADP-HOLD-CDE ADS-PRTCPNT-VIEW.ADP-STTS-CDE
                ldaAdsl401.getAds_Prtcpnt_View_Adp_Hold_Cde().reset();
                ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().reset();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl401.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde().setValue(pnd_Constants_Pnd_M_Annty_Typ);                                                                //Natural: MOVE #M-ANNTY-TYP TO ADS-PRTCPNT-VIEW.ADP-ANNT-TYP-CDE
                ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Strt_Dte().compute(new ComputeParameters(false, ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Strt_Dte()),            //Natural: COMPUTE ADS-PRTCPNT-VIEW.ADP-ANNTY-STRT-DTE = ADS-PRTCPNT-VIEW.ADP-EFFCTV-DTE + 1
                    ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte().add(1));
            }                                                                                                                                                             //Natural: END-IF
            //*  CHECK IF EXISTING                  RS0
            //*  041315
            //*  041315
            RPT:                                                                                                                                                          //Natural: REPEAT
            while (condition(whileTrue))
            {
                vw_ads_Part.startDatabaseFind                                                                                                                             //Natural: FIND ( 1 ) ADS-PART WITH ADS-PART.RQST-ID = ADS-PRTCPNT-VIEW.RQST-ID
                (
                "FIND03",
                new Wc[] { new Wc("RQST_ID", "=", ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id(), WcType.WITH) },
                1
                );
                FIND03:
                while (condition(vw_ads_Part.readNextRow("FIND03", true)))
                {
                    vw_ads_Part.setIfNotFoundControlFlag(false);
                    //*  041315
                    if (condition(vw_ads_Part.getAstCOUNTER().equals(0)))                                                                                                 //Natural: IF NO RECORD FOUND
                    {
                        //*  041315
                        if (true) break RPT;                                                                                                                              //Natural: ESCAPE BOTTOM ( RPT. )
                        //*  041315
                    }                                                                                                                                                     //Natural: END-NOREC
                    getReports().write(0, "DUPLICATE FOUND","=",ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id());                                                                //Natural: WRITE 'DUPLICATE FOUND' '=' ADS-PRTCPNT-VIEW.RQST-ID
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Per_Request_Info_Pnd_Rqst_Entry_Tme_N.nadd(1);                                                                                                    //Natural: ADD 1 TO #RQST-ENTRY-TME-N
                    //*   PIN EXPANSION EM 08/16
                    //*        MOVE #RQST-ID                   TO ADS-PRTCPNT-VIEW.RQST-ID
                    ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Per_Request_Info_Pnd_Rqst_Pin,                  //Natural: COMPRESS #RQST-PIN #RQST-EFFCTV-DTE #RQST-ENTRY-DTE #RQST-ENTRY-TME INTO ADS-PRTCPNT-VIEW.RQST-ID LEAVING NO
                        pnd_Per_Request_Info_Pnd_Rqst_Effctv_Dte, pnd_Per_Request_Info_Pnd_Rqst_Entry_Dte, pnd_Per_Request_Info_Pnd_Rqst_Entry_Tme));
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RPT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RPT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  041315
            }                                                                                                                                                             //Natural: END-REPEAT
            if (Global.isEscape()) return;
            ldaAdsl400a.getPnd_Prtcpnt().setValuesByName(ldaAdsl401.getVw_ads_Prtcpnt_View());                                                                            //Natural: MOVE BY NAME ADS-PRTCPNT-VIEW TO #PRTCPNT
            PND_PND_L2936:                                                                                                                                                //Natural: STORE ADS-PRTCPNT-VIEW
            ldaAdsl401.getVw_ads_Prtcpnt_View().insertDBRow("PND_PND_L2936");
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            ldaAdsl400a.getPnd_Prtcpnt_Adp_Isn().setValue(ldaAdsl401.getVw_ads_Prtcpnt_View().getAstISN("PND_PND_L2936"));                                                //Natural: MOVE *ISN ( ##L2936. ) TO #PRTCPNT.ADP-ISN
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Misc_Variables_Pnd_Debug_On.getBoolean()))                                                                                                      //Natural: IF #DEBUG-ON
        {
            getReports().write(0, "*");                                                                                                                                   //Natural: WRITE '*'
            if (Global.isEscape()) return;
            getReports().write(0, "..... END OF UPDATE-ADS-PRTCPNT-NEW-RQST","=",pnd_Per_Request_Info_Pnd_Processed_Omni_Rqst,"=",pnd_Per_Request_Info_Pnd_Unmatched_Rstlmt, //Natural: WRITE '..... END OF UPDATE-ADS-PRTCPNT-NEW-RQST' '=' #PROCESSED-OMNI-RQST '=' #UNMATCHED-RSTLMT '=' #PIN-OKAY '=' #OVERALL-STAT '=' #PRTCPNT-MATCH-FOUND '=' #PRTCPNT.ADP-ISN
                "=",pnd_Misc_Variables_Pnd_Pin_Okay,"=",pnd_Per_Request_Info_Pnd_Overall_Stat,"=",pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found,"=",ldaAdsl400a.getPnd_Prtcpnt_Adp_Isn());
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_Prev_Ads_Cntrct() throws Exception                                                                                                            //Natural: UPDATE-PREV-ADS-CNTRCT
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------- *
        getReports().write(0, "*********UPDATE-PREV-ADS-CNTRCT***********");                                                                                              //Natural: WRITE '*********UPDATE-PREV-ADS-CNTRCT***********'
        if (Global.isEscape()) return;
        ldaAdsl400a.getPnd_Prtcpnt_Omni_Cntrcts_In_Rqst().getValue(pnd_Per_Request_Info_Pnd_Omni_Cntrct).setValue(ldaAdsl400a.getPnd_Cntrct_Adc_Tiaa_Nbr());              //Natural: MOVE #CNTRCT.ADC-TIAA-NBR TO #PRTCPNT.OMNI-CNTRCTS-IN-RQST ( #OMNI-CNTRCT )
        pnd_Keys_Pnd_Key_Tiaa_Nbr.setValue(ldaAdsl400a.getPnd_Cntrct_Adc_Tiaa_Nbr());                                                                                     //Natural: MOVE #CNTRCT.ADC-TIAA-NBR TO #KEY-TIAA-NBR
        if (condition(pnd_Per_Request_Info_Pnd_Resettlement.getBoolean()))                                                                                                //Natural: IF #RESETTLEMENT
        {
            pnd_Keys_Pnd_Key_Rqst_Id.setValue(pnd_Per_Request_Info_Pnd_Resettl_Orig_Rqstid);                                                                              //Natural: MOVE #RESETTL-ORIG-RQSTID TO #KEY-RQST-ID
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Keys_Pnd_Key_Rqst_Id.setValue(pnd_Per_Request_Info_Pnd_Rqst_Id);                                                                                          //Natural: MOVE #RQST-ID TO #KEY-RQST-ID
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl401a.getVw_ads_Cntrct_View().startDatabaseFind                                                                                                             //Natural: FIND ADS-CNTRCT-VIEW WITH ADC-SUPER1 EQ #ADC-KEY
        (
        "FIND_CNTRCT",
        new Wc[] { new Wc("ADC_SUPER1", "=", pnd_Keys_Pnd_Adc_Key, WcType.WITH) }
        );
        FIND_CNTRCT:
        while (condition(ldaAdsl401a.getVw_ads_Cntrct_View().readNextRow("FIND_CNTRCT", true)))
        {
            ldaAdsl401a.getVw_ads_Cntrct_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAdsl401a.getVw_ads_Cntrct_View().getAstCOUNTER().equals(0)))                                                                                 //Natural: IF NO RECORDS FOUND
            {
                if (condition(pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found.getBoolean()))                                                                                 //Natural: IF #PRTCPNT-MATCH-FOUND
                {
                    short decideConditionsMet3341 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE #OVERALL-STAT;//Natural: VALUE #INCOMPLETE-ARRAY ( 1 )
                    if (condition((pnd_Per_Request_Info_Pnd_Overall_Stat.equals(pnd_Constants_Pnd_Incomplete_Array.getValue(1)))))
                    {
                        decideConditionsMet3341++;
                        pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_Inc_Cntrct);                                                                     //Natural: MOVE #INC-CNTRCT TO #OVERALL-STAT
                    }                                                                                                                                                     //Natural: VALUE #AWAIT-2ND-SIGHT-ARRAY ( 1 )
                    else if (condition((pnd_Per_Request_Info_Pnd_Overall_Stat.equals(pnd_Constants_Pnd_Await_2nd_Sight_Array.getValue(1)))))
                    {
                        decideConditionsMet3341++;
                        pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_Awt_Cntrct);                                                                     //Natural: MOVE #AWT-CNTRCT TO #OVERALL-STAT
                    }                                                                                                                                                     //Natural: VALUE ' '
                    else if (condition((pnd_Per_Request_Info_Pnd_Overall_Stat.equals(" "))))
                    {
                        decideConditionsMet3341++;
                        pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_Delayed_Cntrct);                                                                 //Natural: MOVE #DELAYED-CNTRCT TO #OVERALL-STAT
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    pnd_Per_Request_Info_Pnd_Error_Cntrct.setValue(ldaAdsl400a.getPnd_Cntrct_Adc_Tiaa_Nbr());                                                             //Natural: MOVE #CNTRCT.ADC-TIAA-NBR TO #ERROR-CNTRCT
                }                                                                                                                                                         //Natural: END-IF
                if (true) break FIND_CNTRCT;                                                                                                                              //Natural: ESCAPE BOTTOM ( FIND-CNTRCT. )
            }                                                                                                                                                             //Natural: END-NOREC
            //*  OS-102013 START - SYNCHRONIZE THE ADAS DATA WITH OMNI WHEN OMNI
            //* *  IS UPDATED WITH THE NEW FUND SHARE CLASSES.
            pnd_Rex1_Type.reset();                                                                                                                                        //Natural: RESET #REX1-TYPE
            DbsUtil.examine(new ExamineSource(ldaAdsl401a.getAds_Cntrct_View_Adc_Invstmnt_Grpng_Id().getValue("*")), new ExamineSearch(pnd_Constants_Pnd_Rex1),           //Natural: EXAMINE ADS-CNTRCT-VIEW.ADC-INVSTMNT-GRPNG-ID ( * ) FOR #REX1 REPLACE WITH #REA1 GIVING NUMBER #NUMBER
                new ExamineReplace(pnd_Constants_Pnd_Rea1), new ExamineGivingNumber(pnd_Number));
            if (condition(pnd_Number.greater(getZero())))                                                                                                                 //Natural: IF #NUMBER > 0
            {
                pnd_Rex1_Type.setValue(true);                                                                                                                             //Natural: ASSIGN #REX1-TYPE := TRUE
            }                                                                                                                                                             //Natural: END-IF
            FOR02:                                                                                                                                                        //Natural: FOR #X 1 TO FND-CNT
            for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt())); pnd_Counters_Pnd_X.nadd(1))
            {
                if (condition(ldaAdsl400a.getPnd_Cntrct_Adc_Tkr_Symbl().getValue(pnd_Counters_Pnd_X).equals(" ")))                                                        //Natural: IF #CNTRCT.ADC-TKR-SYMBL ( #X ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.examine(new ExamineSource(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Ticker().getValue("*")), new ExamineSearch(ldaAdsl400a.getPnd_Cntrct_Adc_Tkr_Symbl().getValue(pnd_Counters_Pnd_X)),  //Natural: EXAMINE #ACCT-TICKER ( * ) FOR #CNTRCT.ADC-TKR-SYMBL ( #X ) GIVING INDEX #I
                    new ExamineGivingIndex(pnd_Counters_Pnd_I));
                if (condition(pnd_Counters_Pnd_I.notEquals(getZero())))                                                                                                   //Natural: IF #I NE 0
                {
                    FOR03:                                                                                                                                                //Natural: FOR #INDX = 1 TO FND-CNT
                    for (pnd_Counters_Pnd_Indx.setValue(1); condition(pnd_Counters_Pnd_Indx.lessOrEqual(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt())); pnd_Counters_Pnd_Indx.nadd(1))
                    {
                        if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Invstmnt_Grpng_Id().getValue(pnd_Counters_Pnd_Indx).equals(" ")))                                //Natural: IF ADS-CNTRCT-VIEW.ADC-INVSTMNT-GRPNG-ID ( #INDX ) = ' '
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Invstmnt_Grpng_Id().getValue(pnd_Counters_Pnd_Indx).equals(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_Counters_Pnd_I)))) //Natural: IF ADS-CNTRCT-VIEW.ADC-INVSTMNT-GRPNG-ID ( #INDX ) = #ACCT-INVSTMNT-GRPNG-ID ( #I )
                        {
                            //*  GET THE RTB OCCURRENCE BEFORE UPDATING
                            DbsUtil.examine(new ExamineSource(ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Ticker().getValue("*")), new ExamineSearch(ldaAdsl401a.getAds_Cntrct_View_Adc_T395_Ticker().getValue(pnd_Counters_Pnd_Indx)),  //Natural: EXAMINE ADS-CNTRCT-VIEW.ADC-RTB-TICKER ( * ) FOR ADS-CNTRCT-VIEW.ADC-T395-TICKER ( #INDX ) GIVING INDEX #A
                                new ExamineGivingIndex(pnd_Counters_Pnd_A));
                            ldaAdsl401a.getAds_Cntrct_View_Adc_T395_Ticker().getValue(pnd_Counters_Pnd_Indx).setValue(ldaAdsl400a.getPnd_Cntrct_Adc_Tkr_Symbl().getValue(pnd_Counters_Pnd_X)); //Natural: ASSIGN ADS-CNTRCT-VIEW.ADC-T395-TICKER ( #INDX ) := #CNTRCT.ADC-TKR-SYMBL ( #X )
                            ldaAdsl401a.getAds_Cntrct_View_Adc_T395_Fund_Id().getValue(pnd_Counters_Pnd_Indx).setValue(ldaAdsl400a.getPnd_Cntrct_Adc_Invstmt_Id().getValue(pnd_Counters_Pnd_X)); //Natural: ASSIGN ADS-CNTRCT-VIEW.ADC-T395-FUND-ID ( #INDX ) := #CNTRCT.ADC-INVSTMT-ID ( #X )
                            //*  UPDATE THE RTB TICKER AND FUND ID
                            if (condition(pnd_Counters_Pnd_A.greater(getZero())))                                                                                         //Natural: IF #A GT 0
                            {
                                ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Ticker().getValue(pnd_Counters_Pnd_A).setValue(ldaAdsl400a.getPnd_Cntrct_Adc_Tkr_Symbl().getValue(pnd_Counters_Pnd_X)); //Natural: ASSIGN ADS-CNTRCT-VIEW.ADC-RTB-TICKER ( #A ) := #CNTRCT.ADC-TKR-SYMBL ( #X )
                                ldaAdsl401a.getAds_Cntrct_View_Adc_Rtb_Fund_Id().getValue(pnd_Counters_Pnd_A).setValue(ldaAdsl400a.getPnd_Cntrct_Adc_Invstmt_Id().getValue(pnd_Counters_Pnd_X)); //Natural: ASSIGN ADS-CNTRCT-VIEW.ADC-RTB-FUND-ID ( #A ) := #CNTRCT.ADC-INVSTMT-ID ( #X )
                            }                                                                                                                                             //Natural: END-IF
                            ldaAdsl401a.getAds_Cntrct_View_Adc_Ackl_Lttr_Tkr_Symbl().getValue(pnd_Counters_Pnd_Indx).setValue(ldaAdsl400a.getPnd_Cntrct_Adc_Tkr_Symbl().getValue(pnd_Counters_Pnd_X)); //Natural: ASSIGN ADS-CNTRCT-VIEW.ADC-ACKL-LTTR-TKR-SYMBL ( #INDX ) := #CNTRCT.ADC-TKR-SYMBL ( #X )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_CNTRCT"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_CNTRCT"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Rex1_Type.equals(true)))                                                                                                                    //Natural: IF #REX1-TYPE = TRUE
            {
                DbsUtil.examine(new ExamineSource(ldaAdsl401a.getAds_Cntrct_View_Adc_Invstmnt_Grpng_Id().getValue("*")), new ExamineSearch(pnd_Constants_Pnd_Rea1),       //Natural: EXAMINE ADS-CNTRCT-VIEW.ADC-INVSTMNT-GRPNG-ID ( * ) FOR #REA1 REPLACE WITH #REX1
                    new ExamineReplace(pnd_Constants_Pnd_Rex1));
                pnd_Rex1_Type.reset();                                                                                                                                    //Natural: RESET #REX1-TYPE
            }                                                                                                                                                             //Natural: END-IF
            //*  OS-102013 END
            //*  121007
            pnd_Per_Request_Info_Pnd_Amt_More_Than_Orig.reset();                                                                                                          //Natural: RESET #AMT-MORE-THAN-ORIG
            //*  121007 START
            if (condition(pnd_Per_Request_Info_Pnd_Resettlement.getBoolean()))                                                                                            //Natural: IF #RESETTLEMENT
            {
                FOR04:                                                                                                                                                    //Natural: FOR #X 1 TO FND-CNT
                for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt())); pnd_Counters_Pnd_X.nadd(1))
                {
                    if (condition(ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).notEquals(" ")))                                                  //Natural: IF #CNTRCT.ADC-ACCT-CDE ( #X ) NE ' '
                    {
                        DbsUtil.examine(new ExamineSource(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue("*")), new ExamineSearch(ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X)),  //Natural: EXAMINE ADS-CNTRCT-VIEW.ADC-ACCT-CDE ( * ) FOR #CNTRCT.ADC-ACCT-CDE ( #X ) GIVING INDEX #I
                            new ExamineGivingIndex(pnd_Counters_Pnd_I));
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Counters_Pnd_I.greater(getZero())))                                                                                                 //Natural: IF #I GT 0
                    {
                        //*  011008 START
                        ldaAdsl400a.getPnd_Cntrct_Adc_Grd_Mnthly_Qty().getValue(pnd_Counters_Pnd_X).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Qty().getValue(pnd_Counters_Pnd_I)); //Natural: MOVE ADS-CNTRCT-VIEW.ADC-GRD-MNTHLY-QTY ( #I ) TO #CNTRCT.ADC-GRD-MNTHLY-QTY ( #X )
                        //*  011008 END
                        ldaAdsl400a.getPnd_Cntrct_Adc_Grd_Mnthly_Typ().getValue(pnd_Counters_Pnd_X).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Typ().getValue(pnd_Counters_Pnd_I)); //Natural: MOVE ADS-CNTRCT-VIEW.ADC-GRD-MNTHLY-TYP ( #I ) TO #CNTRCT.ADC-GRD-MNTHLY-TYP ( #X )
                        pnd_Per_Request_Info_Pnd_Original_Amt.compute(new ComputeParameters(true, pnd_Per_Request_Info_Pnd_Original_Amt), ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_Counters_Pnd_I).multiply(new  //Natural: COMPUTE ROUNDED #ORIGINAL-AMT = ADS-CNTRCT-VIEW.ADC-ACCT-ACTL-AMT ( #I ) * .90
                            DbsDecimal(".90")));
                        //*  XXX
                        if (condition(ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Actl_Amt().getValue(pnd_Counters_Pnd_X).greater(pnd_Per_Request_Info_Pnd_Original_Amt)))         //Natural: IF #CNTRCT.ADC-ACCT-ACTL-AMT ( #X ) GT #ORIGINAL-AMT
                        {
                            pnd_Per_Request_Info_Pnd_Amt_More_Than_Orig.setValue(true);                                                                                   //Natural: ASSIGN #AMT-MORE-THAN-ORIG := TRUE
                            getReports().write(0, "***********************************");                                                                                 //Natural: WRITE '***********************************'
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, "*** INVALID RESETTLEMENT AMOUNT ***");                                                                                 //Natural: WRITE '*** INVALID RESETTLEMENT AMOUNT ***'
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, "RESETTLEMENT AMT",ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Actl_Amt().getValue(pnd_Counters_Pnd_X));                         //Natural: WRITE 'RESETTLEMENT AMT' #CNTRCT.ADC-ACCT-ACTL-AMT ( #X )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, "ORIGINAL AMT",ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_Counters_Pnd_I));                        //Natural: WRITE 'ORIGINAL AMT' ADS-CNTRCT-VIEW.ADC-ACCT-ACTL-AMT ( #I )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, "90% OF ORIGINAL AMT",pnd_Per_Request_Info_Pnd_Original_Amt);                                                           //Natural: WRITE '90% OF ORIGINAL AMT' #ORIGINAL-AMT
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, "***********************************");                                                                                 //Natural: WRITE '***********************************'
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  121007 END
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND_CNTRCT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND_CNTRCT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) break FIND_CNTRCT;                                                                                                                              //Natural: ESCAPE BOTTOM ( FIND-CNTRCT. )
            }                                                                                                                                                             //Natural: END-IF
            //*     >> IF BATCH RE-START, CHK IF ALREADY PROCESSED OMNI CONTRACT <<
            //*     ---------------------------------------------------------------
            //*  XXX
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue("*").notEquals(getZero())))                                                         //Natural: IF ADS-CNTRCT-VIEW.ADC-ACCT-ACTL-AMT ( * ) NE 0
            {
                if (condition(! (ldaAdsl401a.getAds_Cntrct_View_Adc_Stts_Cde().equals(pnd_Constants_Pnd_Original_Stat_Array.getValue("*"))) && pnd_Per_Request_Info_Pnd_Overall_Stat.equals(" "))) //Natural: IF NOT ADS-CNTRCT-VIEW.ADC-STTS-CDE EQ #ORIGINAL-STAT-ARRAY ( * ) AND #OVERALL-STAT EQ ' '
                {
                    pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Stts_Cde());                                                        //Natural: MOVE ADS-CNTRCT-VIEW.ADC-STTS-CDE TO #OVERALL-STAT
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl400a.getPnd_Prtcpnt_Adp_Cntrct_Isn().getValue(pnd_Per_Request_Info_Pnd_Omni_Cntrct).setValue(ldaAdsl401a.getVw_ads_Cntrct_View().getAstISN("FIND_CNTRCT")); //Natural: MOVE *ISN ( FIND-CNTRCT. ) TO #PRTCPNT.ADP-CNTRCT-ISN ( #OMNI-CNTRCT )
                if (true) break FIND_CNTRCT;                                                                                                                              //Natural: ESCAPE BOTTOM ( FIND-CNTRCT. )
            }                                                                                                                                                             //Natural: END-IF
            //*     >>> CHECK IF ADAS FUNDS DO NOT MATCH WITH OMNI FUNDS <<<
            //*     --------------------------------------------------------
            pnd_Counters_Pnd_J.reset();                                                                                                                                   //Natural: RESET #J #K #UNMATCHED-FUNDS #ADAS-FUNDS-EXIST
            pnd_Counters_Pnd_K.reset();
            pnd_Misc_Variables_Pnd_Unmatched_Funds.reset();
            pnd_Misc_Variables_Pnd_Adas_Funds_Exist.reset();
            DbsUtil.examine(new ExamineSource(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue("*"),true), new ExamineSearch(pnd_Misc_Variables_Pnd_Acct_Cde_Space, //Natural: EXAMINE FULL ADS-CNTRCT-VIEW.ADC-ACCT-CDE ( * ) FULL #ACCT-CDE-SPACE GIVING INDEX #J
                true), new ExamineGivingIndex(pnd_Counters_Pnd_J));
            DbsUtil.examine(new ExamineSource(ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Cde().getValue("*"),true), new ExamineSearch(pnd_Misc_Variables_Pnd_Acct_Cde_Space,      //Natural: EXAMINE FULL #CNTRCT.ADC-ACCT-CDE ( * ) FULL #ACCT-CDE-SPACE GIVING INDEX #K
                true), new ExamineGivingIndex(pnd_Counters_Pnd_K));
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue("*").notEquals(" ")))                                                                    //Natural: IF ADS-CNTRCT-VIEW.ADC-ACCT-CDE ( * ) NE ' '
            {
                pnd_Misc_Variables_Pnd_Adas_Funds_Exist.setValue(true);                                                                                                   //Natural: MOVE TRUE TO #ADAS-FUNDS-EXIST
                if (condition(pnd_Counters_Pnd_K.notEquals(pnd_Counters_Pnd_J)))                                                                                          //Natural: IF #K NE #J
                {
                    pnd_Misc_Variables_Pnd_Unmatched_Funds.setValue(true);                                                                                                //Natural: MOVE TRUE TO #UNMATCHED-FUNDS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Per_Request_Info_Pnd_Last_Cntrct_Seq.setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Sqnce_Nbr());                                                            //Natural: MOVE ADS-CNTRCT-VIEW.ADC-SQNCE-NBR TO #LAST-CNTRCT-SEQ
            //*     >>> UPDATE ADAS FUND INFO WITH OMNI DATA <<<
            //*     --------------------------------------------
            if (condition(pnd_Misc_Variables_Pnd_Debug_On.getBoolean()))                                                                                                  //Natural: IF #DEBUG-ON
            {
                getReports().write(0, "*");                                                                                                                               //Natural: WRITE '*'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND_CNTRCT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND_CNTRCT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  EM 072811
                getReports().write(0, "..... INSIDE UPDATE-PREV-ADS-CNTRCT","=",ldaAdsl400a.getPnd_Cntrct_Adc_Tkr_Symbl().getValue("*"),"=",ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Cde().getValue("*"), //Natural: WRITE '..... INSIDE UPDATE-PREV-ADS-CNTRCT' '=' #CNTRCT.ADC-TKR-SYMBL ( * ) '=' #CNTRCT.ADC-ACCT-CDE ( * ) '=' ADS-CNTRCT-VIEW.ADC-ACCT-CDE ( * ) '=' #CNTRCT.ADC-ACCT-CDE ( * ) '=' #K '=' #J '=' #ADAS-FUNDS-EXIST '=' FND-CNT '=' #CNTRCT.ADC-ACCT-ACTL-AMT ( 1:10 ) '=' #CNTRCT.ADC-ACCT-ACTL-NBR-UNITS ( 1:10 ) '=' #CNTRCT.ADC-ACCT-CREF-RATE-CDE ( 1:10 ) '=' #CNTRCT.ADC-ACCT-OPN-ACCUM-AMT ( 1:10 ) '=' #CNTRCT.ADC-ACCT-OPN-NBR-UNITS ( 1:10 ) '=' #CNTRCT.ADC-ACCT-AFTR-TAX-AMT ( 1:10 ) '=' #CNTRCT.ADC-DTL-TIAA-RATE-CDE ( 1:10 ) '=' #CNTRCT.ADC-DTL-TIAA-ACTL-AMT ( 1:10 ) '=' #CNTRCT.ADC-INVSTMT-ID ( 1:10 ) '=' #CNTRCT.ADC-INV-SHORT-NAME ( 1:10 )
                    "=",ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue("*"),"=",ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Cde().getValue("*"),"=",pnd_Counters_Pnd_K,
                    "=",pnd_Counters_Pnd_J,"=",pnd_Misc_Variables_Pnd_Adas_Funds_Exist,"=",ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt(),"=",ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Actl_Amt().getValue(1,
                    ":",10),"=",ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Actl_Nbr_Units().getValue(1,":",10),"=",ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Cref_Rate_Cde().getValue(1,
                    ":",10),"=",ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Opn_Accum_Amt().getValue(1,":",10),"=",ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Opn_Nbr_Units().getValue(1,
                    ":",10),"=",ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Aftr_Tax_Amt().getValue(1,":",10),"=",ldaAdsl400a.getPnd_Cntrct_Adc_Dtl_Tiaa_Rate_Cde().getValue(1,
                    ":",10),"=",ldaAdsl400a.getPnd_Cntrct_Adc_Dtl_Tiaa_Actl_Amt().getValue(1,":",10),"=",ldaAdsl400a.getPnd_Cntrct_Adc_Invstmt_Id().getValue(1,
                    ":",10),"=",ldaAdsl400a.getPnd_Cntrct_Adc_Inv_Short_Name().getValue(1,":",10));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND_CNTRCT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND_CNTRCT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  EM 030112
                getReports().write(0, NEWLINE,"=",ldaAdsl400a.getPnd_Cntrct_Adc_Contract_Id().getValue(1,":",10));                                                        //Natural: WRITE / '=' #CNTRCT.ADC-CONTRACT-ID ( 1:10 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND_CNTRCT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND_CNTRCT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            FOR05:                                                                                                                                                        //Natural: FOR #X 1 TO FND-CNT
            for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt())); pnd_Counters_Pnd_X.nadd(1))
            {
                if (condition(ldaAdsl400a.getPnd_Cntrct_Adc_Tkr_Symbl().getValue(pnd_Counters_Pnd_X).equals(" ")))                                                        //Natural: IF #CNTRCT.ADC-TKR-SYMBL ( #X ) EQ ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Counters_Pnd_I.reset();                                                                                                                               //Natural: RESET #I
                if (condition(ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).notEquals(" ")))                                                      //Natural: IF #CNTRCT.ADC-ACCT-CDE ( #X ) NE ' '
                {
                    DbsUtil.examine(new ExamineSource(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue("*")), new ExamineSearch(ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X)),  //Natural: EXAMINE ADS-CNTRCT-VIEW.ADC-ACCT-CDE ( * ) FOR #CNTRCT.ADC-ACCT-CDE ( #X ) GIVING INDEX #I
                        new ExamineGivingIndex(pnd_Counters_Pnd_I));
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Counters_Pnd_I.equals(getZero())))                                                                                                      //Natural: IF #I EQ 0
                {
                    if (condition(pnd_Misc_Variables_Pnd_Adas_Funds_Exist.getBoolean()))                                                                                  //Natural: IF #ADAS-FUNDS-EXIST
                    {
                        pnd_Misc_Variables_Pnd_Unmatched_Funds.setValue(true);                                                                                            //Natural: MOVE TRUE TO #UNMATCHED-FUNDS
                    }                                                                                                                                                     //Natural: END-IF
                    DbsUtil.examine(new ExamineSource(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue("*"),true), new ExamineSearch(pnd_Misc_Variables_Pnd_Acct_Cde_Space,  //Natural: EXAMINE FULL ADS-CNTRCT-VIEW.ADC-ACCT-CDE ( * ) FULL #ACCT-CDE-SPACE GIVING INDEX #I
                        true), new ExamineGivingIndex(pnd_Counters_Pnd_I));
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Counters_Pnd_I.equals(getZero())))                                                                                                      //Natural: IF #I EQ 0
                {
                    if (condition(pnd_Misc_Variables_Pnd_Adas_Funds_Exist.getBoolean()))                                                                                  //Natural: IF #ADAS-FUNDS-EXIST
                    {
                        pnd_Misc_Variables_Pnd_Unmatched_Funds.setValue(true);                                                                                            //Natural: MOVE TRUE TO #UNMATCHED-FUNDS
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaAdsl401a.getAds_Cntrct_View_Adc_Rqst_Info().getValue(pnd_Counters_Pnd_I).setValuesByName(ldaAdsl400a.getPnd_Cntrct_Adc_Rqst_Info().getValue(pnd_Counters_Pnd_X)); //Natural: MOVE BY NAME #CNTRCT.ADC-RQST-INFO ( #X ) TO ADS-CNTRCT-VIEW.ADC-RQST-INFO ( #I )
                    ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Info().getValue(pnd_Counters_Pnd_I).setValuesByName(ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Info().getValue(pnd_Counters_Pnd_X)); //Natural: MOVE BY NAME #CNTRCT.ADC-ACCT-INFO ( #X ) TO ADS-CNTRCT-VIEW.ADC-ACCT-INFO ( #I )
                    ldaAdsl401a.getAds_Cntrct_View_Adc_Invstmt_Info().getValue(pnd_Counters_Pnd_I).setValuesByName(ldaAdsl400a.getPnd_Cntrct_Adc_Invstmt_Info().getValue(pnd_Counters_Pnd_X)); //Natural: MOVE BY NAME #CNTRCT.ADC-INVSTMT-INFO ( #X ) TO ADS-CNTRCT-VIEW.ADC-INVSTMT-INFO ( #I )
                    //*  EM - 072811
                    //*  EM - 072811
                    ldaAdsl401a.getAds_Cntrct_View_Adc_Inv_Short_Name().getValue(pnd_Counters_Pnd_I).setValue(ldaAdsl400a.getPnd_Cntrct_Adc_Inv_Short_Name().getValue(pnd_Counters_Pnd_X)); //Natural: MOVE #CNTRCT.ADC-INV-SHORT-NAME ( #X ) TO ADS-CNTRCT-VIEW.ADC-INV-SHORT-NAME ( #I )
                    //* * EM - 040709 START
                    //*      IF ADS-CNTRCT-VIEW.ADC-GRD-MNTHLY-TYP(#I) EQ ' ' AND
                    //*          ADS-CNTRCT-VIEW.ADC-GRD-MNTHLY-QTY(#I) EQ 0
                    //* * EM - 040709 END
                    ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Typ().getValue(pnd_Counters_Pnd_I).setValue(ldaAdsl400a.getPnd_Cntrct_Adc_Grd_Mnthly_Typ().getValue(pnd_Counters_Pnd_X)); //Natural: MOVE #CNTRCT.ADC-GRD-MNTHLY-TYP ( #X ) TO ADS-CNTRCT-VIEW.ADC-GRD-MNTHLY-TYP ( #I )
                    ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Qty().getValue(pnd_Counters_Pnd_I).setValue(ldaAdsl400a.getPnd_Cntrct_Adc_Grd_Mnthly_Qty().getValue(pnd_Counters_Pnd_X)); //Natural: MOVE #CNTRCT.ADC-GRD-MNTHLY-QTY ( #X ) TO ADS-CNTRCT-VIEW.ADC-GRD-MNTHLY-QTY ( #I )
                    //* *    END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND_CNTRCT"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND_CNTRCT"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*     >>> ASSIGN STATUS FOR UN-MATCHED FUNDS <<<
            //*   ------------------------------------------
            if (condition(pnd_Misc_Variables_Pnd_Unmatched_Funds.getBoolean()))                                                                                           //Natural: IF #UNMATCHED-FUNDS
            {
                short decideConditionsMet3501 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE #OVERALL-STAT;//Natural: VALUE #INCOMPLETE-ARRAY ( 1 )
                if (condition((pnd_Per_Request_Info_Pnd_Overall_Stat.equals(pnd_Constants_Pnd_Incomplete_Array.getValue(1)))))
                {
                    decideConditionsMet3501++;
                    pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_Inc_Funds);                                                                          //Natural: MOVE #INC-FUNDS TO #OVERALL-STAT
                }                                                                                                                                                         //Natural: VALUE #AWAIT-2ND-SIGHT-ARRAY ( 1 )
                else if (condition((pnd_Per_Request_Info_Pnd_Overall_Stat.equals(pnd_Constants_Pnd_Await_2nd_Sight_Array.getValue(1)))))
                {
                    decideConditionsMet3501++;
                    pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_Awt_Funds);                                                                          //Natural: MOVE #AWT-FUNDS TO #OVERALL-STAT
                }                                                                                                                                                         //Natural: VALUE ' '
                else if (condition((pnd_Per_Request_Info_Pnd_Overall_Stat.equals(" "))))
                {
                    decideConditionsMet3501++;
                    pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_Delayed_Funds);                                                                      //Natural: MOVE #DELAYED-FUNDS TO #OVERALL-STAT
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl400a.getPnd_Prtcpnt_Adp_Cntrct_Isn().getValue(pnd_Per_Request_Info_Pnd_Omni_Cntrct).setValue(ldaAdsl401a.getVw_ads_Cntrct_View().getAstISN("FIND_CNTRCT")); //Natural: MOVE *ISN ( FIND-CNTRCT. ) TO #PRTCPNT.ADP-CNTRCT-ISN ( #OMNI-CNTRCT )
            ldaAdsl401a.getAds_Cntrct_View_Adc_Stts_Cde().setValue(pnd_Per_Request_Info_Pnd_Overall_Stat);                                                                //Natural: MOVE #OVERALL-STAT TO ADS-CNTRCT-VIEW.ADC-STTS-CDE
            ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Data().getValue("*").setValuesByName(ldaAdsl400a.getPnd_Cntrct_Adc_Dtl_Tiaa_Data().getValue("*"));                //Natural: MOVE BY NAME #CNTRCT.ADC-DTL-TIAA-DATA ( * ) TO ADS-CNTRCT-VIEW.ADC-DTL-TIAA-DATA ( * )
            //* *  07062005 MN START
            ldaAdsl401a.getAds_Cntrct_View_Adc_Tpa_Guarant_Commut_Amt().getValue("*").setValue(ldaAdsl400a.getPnd_Cntrct_Adc_Tpa_Guarant_Commut_Amt().getValue("*"));     //Natural: MOVE #CNTRCT.ADC-TPA-GUARANT-COMMUT-AMT ( * ) TO ADS-CNTRCT-VIEW.ADC-TPA-GUARANT-COMMUT-AMT ( * )
            //*  EM - 030112
            //*  EM - 030112
            ldaAdsl401a.getAds_Cntrct_View_Adc_Contract_Id().getValue("*").setValue(ldaAdsl400a.getPnd_Cntrct_Adc_Contract_Id().getValue("*"));                           //Natural: MOVE #CNTRCT.ADC-CONTRACT-ID ( * ) TO ADS-CNTRCT-VIEW.ADC-CONTRACT-ID ( * )
            ldaAdsl401a.getAds_Cntrct_View_Adc_Orig_Lob_Ind().setValue(ldaAdsl400a.getPnd_Cntrct_Adc_Orig_Lob_Ind());                                                     //Natural: MOVE #CNTRCT.ADC-ORIG-LOB-IND TO ADS-CNTRCT-VIEW.ADC-ORIG-LOB-IND
            ldaAdsl401a.getAds_Cntrct_View_Adc_Tpa_Paymnt_Cnt().setValue(ldaAdsl400a.getPnd_Cntrct_Adc_Tpa_Paymnt_Cnt());                                                 //Natural: MOVE #CNTRCT.ADC-TPA-PAYMNT-CNT TO ADS-CNTRCT-VIEW.ADC-TPA-PAYMNT-CNT
            //* *  07062005 MN END
            ldaAdsl401a.getAds_Cntrct_View_Adc_Sttl_Ivc_Amt().setValue(ldaAdsl400a.getPnd_Cntrct_Adc_Sttl_Ivc_Amt());                                                     //Natural: MOVE #CNTRCT.ADC-STTL-IVC-AMT TO ADS-CNTRCT-VIEW.ADC-STTL-IVC-AMT
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Cntrct_Issue_State().equals(" ")))                                                                           //Natural: IF ADS-CNTRCT-VIEW.ADC-CNTRCT-ISSUE-STATE EQ ' '
            {
                ldaAdsl401a.getAds_Cntrct_View_Adc_Cntrct_Issue_State().setValue(ldaAdsl400a.getPnd_Cntrct_Adc_Cntrct_Issue_State());                                     //Natural: MOVE #CNTRCT.ADC-CNTRCT-ISSUE-STATE TO ADS-CNTRCT-VIEW.ADC-CNTRCT-ISSUE-STATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Cntrct_Issue_Dte().equals(getZero())))                                                                       //Natural: IF ADS-CNTRCT-VIEW.ADC-CNTRCT-ISSUE-DTE EQ 0
            {
                ldaAdsl401a.getAds_Cntrct_View_Adc_Cntrct_Issue_Dte().setValue(ldaAdsl400a.getPnd_Cntrct_Adc_Cntrct_Issue_Dte());                                         //Natural: MOVE #CNTRCT.ADC-CNTRCT-ISSUE-DTE TO ADS-CNTRCT-VIEW.ADC-CNTRCT-ISSUE-DTE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Sub_Plan_Nbr().equals(" ")))                                                                                 //Natural: IF ADS-CNTRCT-VIEW.ADC-SUB-PLAN-NBR EQ ' '
            {
                ldaAdsl401a.getAds_Cntrct_View_Adc_Sub_Plan_Nbr().setValue(ldaAdsl400a.getPnd_Cntrct_Adc_Sub_Plan_Nbr());                                                 //Natural: MOVE #CNTRCT.ADC-SUB-PLAN-NBR TO ADS-CNTRCT-VIEW.ADC-SUB-PLAN-NBR
            }                                                                                                                                                             //Natural: END-IF
            pnd_Per_Request_Info_Pnd_Last_Cntrct_Seq.setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Sqnce_Nbr());                                                            //Natural: MOVE ADS-CNTRCT-VIEW.ADC-SQNCE-NBR TO #LAST-CNTRCT-SEQ
            ldaAdsl401a.getVw_ads_Cntrct_View().updateDBRow("FIND_CNTRCT");                                                                                               //Natural: UPDATE ( FIND-CNTRCT. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  121007 START
        if (condition(pnd_Per_Request_Info_Pnd_Resettlement.getBoolean() && pnd_Per_Request_Info_Pnd_Amt_More_Than_Orig.getBoolean()))                                    //Natural: IF #RESETTLEMENT AND #AMT-MORE-THAN-ORIG
        {
            pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_Invalid_Rsttlmnt);                                                                           //Natural: ASSIGN #OVERALL-STAT := #INVALID-RSTTLMNT
            //*  121007 END
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Misc_Variables_Pnd_Debug_On.getBoolean()))                                                                                                      //Natural: IF #DEBUG-ON
        {
            getReports().write(0, "*");                                                                                                                                   //Natural: WRITE '*'
            if (Global.isEscape()) return;
            //*  EM - 072811
            //*  EM - 072811
            //*  EM - 072811
            //*  EM - 072811
            getReports().write(0, "..... AFTER FIND UPDATE-PREV-ADS-CNTRCT","SUPER1 KEY USED      : ",pnd_Keys_Pnd_Adc_Key,"NO. OF CNTRCTS FOUND : ",ldaAdsl401a.getVw_ads_Cntrct_View().getAstNUMBER(), //Natural: WRITE '..... AFTER FIND UPDATE-PREV-ADS-CNTRCT' 'SUPER1 KEY USED      : ' #ADC-KEY 'NO. OF CNTRCTS FOUND : ' *NUMBER ( FIND-CNTRCT. ) '#CNTRCT.RQST-ID      : ' #CNTRCT.RQST-ID '#CNTRCT.ADC-ACCT-CDE : ' #CNTRCT.ADC-ACCT-CDE ( * ) 'ADS.ADC-ACCT-CDE     : ' ADS-CNTRCT-VIEW.ADC-ACCT-CDE ( * ) '#CNTRCT.ADC-TKR-SYMBL: ' #CNTRCT.ADC-TKR-SYMBL ( * ) 'ADS.ADC-TKR-SYMBL    : ' ADS-CNTRCT-VIEW.ADC-TKR-SYMBL ( * ) '#CNTRCT.ADC-INVSTMT-ID: '#CNTRCT.ADC-INVSTMT-ID ( * ) 'ADS.ADC-INVSTMT-ID   : ' ADS-CNTRCT-VIEW.ADC-INVSTMT-ID ( * ) '#CNTRCT.ADC-INV-SHORT-NAME   : ' #CNTRCT.ADC-INV-SHORT-NAME ( * ) 'ADS.ADC-INV-SHORT-NAME   : ' ADS-CNTRCT-VIEW.ADC-INV-SHORT-NAME ( * ) '#CNTRCT.ADC-SUB-PLAN-NBR: '#CNTRCT.ADC-SUB-PLAN-NBR 'ADS.ADC-SUB-PLAN-NBR: 'ADS-CNTRCT-VIEW.ADC-SUB-PLAN-NBR '#CNTRCT.ADC-DTL-TIAA-RATE-CDE: ' #CNTRCT.ADC-DTL-TIAA-RATE-CDE ( 1:5 ) 'ADS.ADC-DTL-TIAA-RATE-CDE: ' ADS-CNTRCT-VIEW.ADC-DTL-TIAA-RATE-CDE ( 1:5 )
                "#CNTRCT.RQST-ID      : ",ldaAdsl400a.getPnd_Cntrct_Rqst_Id(),"#CNTRCT.ADC-ACCT-CDE : ",ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Cde().getValue("*"),
                "ADS.ADC-ACCT-CDE     : ",ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue("*"),"#CNTRCT.ADC-TKR-SYMBL: ",ldaAdsl400a.getPnd_Cntrct_Adc_Tkr_Symbl().getValue("*"),
                "ADS.ADC-TKR-SYMBL    : ",ldaAdsl401a.getAds_Cntrct_View_Adc_Tkr_Symbl().getValue("*"),"#CNTRCT.ADC-INVSTMT-ID: ",ldaAdsl400a.getPnd_Cntrct_Adc_Invstmt_Id().getValue("*"),
                "ADS.ADC-INVSTMT-ID   : ",ldaAdsl401a.getAds_Cntrct_View_Adc_Invstmt_Id().getValue("*"),"#CNTRCT.ADC-INV-SHORT-NAME   : ",ldaAdsl400a.getPnd_Cntrct_Adc_Inv_Short_Name().getValue("*"),
                "ADS.ADC-INV-SHORT-NAME   : ",ldaAdsl401a.getAds_Cntrct_View_Adc_Inv_Short_Name().getValue("*"),"#CNTRCT.ADC-SUB-PLAN-NBR: ",ldaAdsl400a.getPnd_Cntrct_Adc_Sub_Plan_Nbr(),
                "ADS.ADC-SUB-PLAN-NBR: ",ldaAdsl401a.getAds_Cntrct_View_Adc_Sub_Plan_Nbr(),"#CNTRCT.ADC-DTL-TIAA-RATE-CDE: ",ldaAdsl400a.getPnd_Cntrct_Adc_Dtl_Tiaa_Rate_Cde().getValue(1,
                ":",5),"ADS.ADC-DTL-TIAA-RATE-CDE: ",ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Rate_Cde().getValue(1,":",5));
            if (Global.isEscape()) return;
            //*  EM - 030112
            //*  EM - 030112
            getReports().write(0, NEWLINE,"#CNTRCT.ADC-CONTRACT-ID: ",ldaAdsl400a.getPnd_Cntrct_Adc_Contract_Id().getValue(1,":",5),"ADS.ADC-CONTRACT-ID: ",              //Natural: WRITE / '#CNTRCT.ADC-CONTRACT-ID: ' #CNTRCT.ADC-CONTRACT-ID ( 1:5 ) 'ADS.ADC-CONTRACT-ID: ' ADS-CNTRCT-VIEW.ADC-CONTRACT-ID ( 1:5 ) '#CNTRCT.ADC-ACCT-CREF-RATE-CDE: ' #CNTRCT.ADC-ACCT-CREF-RATE-CDE ( 1:5 ) 'ADS.ADC-ACCT-CREF-RATE-CDE: ' ADS-CNTRCT-VIEW.ADC-ACCT-CREF-RATE-CDE ( 1:5 ) '=' #UNMATCHED-FUNDS '=' #OVERALL-STAT
                ldaAdsl401a.getAds_Cntrct_View_Adc_Contract_Id().getValue(1,":",5),"#CNTRCT.ADC-ACCT-CREF-RATE-CDE: ",ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Cref_Rate_Cde().getValue(1,
                ":",5),"ADS.ADC-ACCT-CREF-RATE-CDE: ",ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cref_Rate_Cde().getValue(1,":",5),"=",pnd_Misc_Variables_Pnd_Unmatched_Funds,
                "=",pnd_Per_Request_Info_Pnd_Overall_Stat);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAdsl401a.getVw_ads_Cntrct_View().getAstNUMBER().equals(getZero()) || pnd_Per_Request_Info_Pnd_Resettlement.getBoolean()))                        //Natural: IF *NUMBER ( FIND-CNTRCT. ) EQ 0 OR #RESETTLEMENT
        {
            ldaAdsl401a.getVw_ads_Cntrct_View().setValuesByName(ldaAdsl400a.getPnd_Cntrct());                                                                             //Natural: MOVE BY NAME #CNTRCT TO ADS-CNTRCT-VIEW
            ldaAdsl401a.getAds_Cntrct_View_Adc_Stts_Cde().setValue(pnd_Per_Request_Info_Pnd_Overall_Stat);                                                                //Natural: MOVE #OVERALL-STAT TO ADS-CNTRCT-VIEW.ADC-STTS-CDE
            ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue("*").reset();                                                                               //Natural: RESET ADS-CNTRCT-VIEW.ADC-GRD-MNTHLY-ACTL-AMT ( * ) ADS-CNTRCT-VIEW.ADC-STNDRD-ANNL-ACTL-AMT ( * ) ADS-CNTRCT-VIEW.ADC-DTL-TIAA-GRD-ACTL-AMT ( * ) ADS-CNTRCT-VIEW.ADC-DTL-TIAA-STNDRD-ACTL-AMT ( * ) ADS-CNTRCT-VIEW.ADC-HOLD-CDE
            ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue("*").reset();
            ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue("*").reset();
            ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue("*").reset();
            ldaAdsl401a.getAds_Cntrct_View_Adc_Hold_Cde().reset();
            //*  TNG0613 START
            //*   PIN EXPANSION 02222017 START
            //*  COMPRESS SUBSTR (#CNTRCT.RQST-ID,1,7)
            //*    SUBSTR (#CNTRCT.RQST-ID,16,15)
            //*    INTO ADS-CNTRCT-VIEW.ADC-GROUP-RQST-ID LEAVING NO
            ldaAdsl401a.getAds_Cntrct_View_Adc_Group_Rqst_Id().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAdsl400a.getPnd_Cntrct_Rqst_Pin(),             //Natural: COMPRESS #CNTRCT.RQST-PIN #CNTRCT.RQST-ENTRY-DTE #CNTRCT.RQST-ENTRY-TME INTO ADS-CNTRCT-VIEW.ADC-GROUP-RQST-ID LEAVING NO
                ldaAdsl400a.getPnd_Cntrct_Rqst_Entry_Dte(), ldaAdsl400a.getPnd_Cntrct_Rqst_Entry_Tme()));
            PND_PND_L3616:                                                                                                                                                //Natural: STORE ADS-CNTRCT-VIEW
            ldaAdsl401a.getVw_ads_Cntrct_View().insertDBRow("PND_PND_L3616");
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            ldaAdsl400a.getPnd_Prtcpnt_Adp_Cntrct_Isn().getValue(pnd_Per_Request_Info_Pnd_Omni_Cntrct).setValue(ldaAdsl401a.getVw_ads_Cntrct_View().getAstISN("PND_PND_L3616")); //Natural: MOVE *ISN ( ##L3616. ) TO #PRTCPNT.ADP-CNTRCT-ISN ( #OMNI-CNTRCT )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Fillup_Cntrct_Linkage() throws Exception                                                                                                             //Natural: FILLUP-CNTRCT-LINKAGE
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------- *
        if (condition(ldaAdsl400a.getPnd_Cntrct_New_Cntrct().getBoolean()))                                                                                               //Natural: IF NEW-CNTRCT
        {
            //*   PIN EXPANSION 02222017 START
            //*  MOVE #RQST-ID            TO #CNTRCT.RQST-ID
            ldaAdsl400a.getPnd_Cntrct_Rqst_Id().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Per_Request_Info_Pnd_Rqst_Pin, pnd_Per_Request_Info_Pnd_Rqst_Effctv_Dte,  //Natural: COMPRESS #RQST-PIN #RQST-EFFCTV-DTE #RQST-ENTRY-DTE #RQST-ENTRY-TME INTO #CNTRCT.RQST-ID LEAVING NO
                pnd_Per_Request_Info_Pnd_Rqst_Entry_Dte, pnd_Per_Request_Info_Pnd_Rqst_Entry_Tme));
            //*   PIN EXPANSION 02222017 END
            ldaAdsl400a.getPnd_Cntrct_Adc_Tiaa_Nbr().setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct());                                                       //Natural: MOVE #OMNI-TIAA-CNTRCT TO #CNTRCT.ADC-TIAA-NBR
            ldaAdsl400a.getPnd_Cntrct_Adc_Cref_Nbr().setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Cref_Cntrct());                                                       //Natural: MOVE #OMNI-CREF-CNTRCT TO #CNTRCT.ADC-CREF-NBR
            ldaAdsl400a.getPnd_Cntrct_Adc_Cntrct_Issue_State().setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Issue_State());                                             //Natural: MOVE #OMNI-ISSUE-STATE TO #CNTRCT.ADC-CNTRCT-ISSUE-STATE
            ldaAdsl400a.getPnd_Cntrct_Adc_Sub_Plan_Nbr().setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Sub_Plan_Nbr());                                                  //Natural: MOVE #OMNI-SUB-PLAN-NBR TO #CNTRCT.ADC-SUB-PLAN-NBR
            ldaAdsl400a.getPnd_Cntrct_Adc_Lst_Actvty_Dte().setValue(pnd_Adsn401_Pnd_Cntl_Bsnss_Dte);                                                                      //Natural: MOVE #CNTL-BSNSS-DTE TO #CNTRCT.ADC-LST-ACTVTY-DTE
            if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Issue_Dte().notEquals(getZero())))                                                                        //Natural: IF #OMNI-ISSUE-DTE NE 0
            {
                ldaAdsl400a.getPnd_Cntrct_Adc_Cntrct_Issue_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Issue_Dte_A());      //Natural: MOVE EDITED #OMNI-ISSUE-DTE-A TO #CNTRCT.ADC-CNTRCT-ISSUE-DTE ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl400a.getPnd_Cntrct_Adc_Sttl_Ivc_Amt().setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt());                                                  //Natural: MOVE #OMNI-FUND-IVC-AMT TO #CNTRCT.ADC-STTL-IVC-AMT
            pnd_Per_Request_Info_Pnd_Omni_Cntrct.nadd(1);                                                                                                                 //Natural: ADD 1 TO #OMNI-CNTRCT
            if (condition(pnd_Misc_Variables_Pnd_Debug_On.getBoolean()))                                                                                                  //Natural: IF #DEBUG-ON
            {
                getReports().write(0, "*");                                                                                                                               //Natural: WRITE '*'
                if (Global.isEscape()) return;
                getReports().write(0, "....INSIDE FILLUP-CNTRCT-LINKAGE","=",pnd_Per_Request_Info_Pnd_Rqst_Id,"=",ldaAdsl400a.getPnd_Cntrct_Rqst_Id(),                    //Natural: WRITE '....INSIDE FILLUP-CNTRCT-LINKAGE' '=' #RQST-ID '=' #CNTRCT.RQST-ID '=' NEW-CNTRCT '=' #CNTRCT.ADC-TIAA-NBR '=' #CNTRCT.ADC-TKR-SYMBL ( * ) '=' #OMNI-FUND-TICKER
                    "=",ldaAdsl400a.getPnd_Cntrct_New_Cntrct(),"=",ldaAdsl400a.getPnd_Cntrct_Adc_Tiaa_Nbr(),"=",ldaAdsl400a.getPnd_Cntrct_Adc_Tkr_Symbl().getValue("*"),
                    "=",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Ticker());
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl400a.getPnd_Cntrct_New_Cntrct().reset();                                                                                                               //Natural: RESET NEW-CNTRCT
        }                                                                                                                                                                 //Natural: END-IF
        //*  OS-102013
        pnd_Counters_Pnd_X.reset();                                                                                                                                       //Natural: RESET #X #TIAA-TYPE
        pnd_Tiaa_Type.reset();
        DbsUtil.examine(new ExamineSource(ldaAdsl400a.getPnd_Cntrct_Adc_Tkr_Symbl().getValue("*")), new ExamineSearch(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Ticker()),  //Natural: EXAMINE #CNTRCT.ADC-TKR-SYMBL ( * ) FOR #OMNI-FUND-TICKER GIVING INDEX #X
            new ExamineGivingIndex(pnd_Counters_Pnd_X));
        if (condition(pnd_Counters_Pnd_X.equals(getZero())))                                                                                                              //Natural: IF #X EQ 0
        {
            ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt().nadd(1);                                                                                                                  //Natural: ADD 1 TO FND-CNT
            //*  EXAMINE #EXT-TICKER-SYMBOL(*) FOR #OMNI-FUND-TICKER GIVING OS-102013
            //*  OS-102013
            DbsUtil.examine(new ExamineSource(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Ticker().getValue("*")), new ExamineSearch(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Ticker()),  //Natural: EXAMINE #ACCT-TICKER ( * ) FOR #OMNI-FUND-TICKER GIVING INDEX #I
                new ExamineGivingIndex(pnd_Counters_Pnd_I));
            if (condition(pnd_Counters_Pnd_I.notEquals(getZero())))                                                                                                       //Natural: IF #I NE 0
            {
                //*  OS-102013 START
                //*    IF #OMNI-FUND-TICKER NE #SVSAA                 /* EM - 111510
                if (condition(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_Counters_Pnd_I).equals(pnd_Constants_Pnd_Tiaa) ||                     //Natural: IF #ACCT-INVSTMNT-GRPNG-ID ( #I ) EQ #TIAA OR EQ #STABLE ( * )
                    pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_Counters_Pnd_I).equals(pnd_Constants_Pnd_Stable.getValue(1)) || 
                    pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_Counters_Pnd_I).equals(pnd_Constants_Pnd_Stable.getValue(2))))
                {
                    pnd_Tiaa_Type.setValue(true);                                                                                                                         //Natural: ASSIGN #TIAA-TYPE := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_Counters_Pnd_I).notEquals(pnd_Constants_Pnd_Svsaa)))                  //Natural: IF #ACCT-INVSTMNT-GRPNG-ID ( #I ) NE #SVSAA
                {
                    //*      MOVE #EXT-ALPHA-FUND-CDE(#I) TO #CNTRCT.ADC-ACCT-CDE(FND-CNT)
                    ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Cde().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Alpha_Fund_Cde().getValue(pnd_Counters_Pnd_I)); //Natural: MOVE #ACCT-ALPHA-FUND-CDE ( #I ) TO #CNTRCT.ADC-ACCT-CDE ( FND-CNT )
                    //*  OS-102013 END
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Cde().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).setValue("T");                                                 //Natural: MOVE 'T' TO #CNTRCT.ADC-ACCT-CDE ( FND-CNT )
                    //*  EM - 111510
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "OMNI TICKER DOES NOT EXIST IN EXTERNAL :",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Ticker(),"FOR PIN : ",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Pin_A(), //Natural: WRITE 'OMNI TICKER DOES NOT EXIST IN EXTERNAL :' #OMNI-FUND-TICKER 'FOR PIN : ' #OMNI-PIN-A ' CONTRACT :' #OMNI-TIAA-CNTRCT
                    " CONTRACT :",ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct());
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl400a.getPnd_Cntrct_Adc_Tkr_Symbl().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Ticker());        //Natural: MOVE #OMNI-FUND-TICKER TO #CNTRCT.ADC-TKR-SYMBL ( FND-CNT )
            ldaAdsl400a.getPnd_Cntrct_Adc_Invstmt_Id().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Invstmt_Id());        //Natural: MOVE #OMNI-INVSTMT-ID TO #CNTRCT.ADC-INVSTMT-ID ( FND-CNT )
            //* * EM - 072811 START
            ldaAdsl400a.getPnd_Cntrct_Adc_Inv_Short_Name().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Inv_Short_Name()); //Natural: MOVE #OMNI-INV-SHORT-NAME TO #CNTRCT.ADC-INV-SHORT-NAME ( FND-CNT )
            //* * EM - 072811 END
            ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Typ().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Typ());            //Natural: MOVE #OMNI-FUND-TYP TO #CNTRCT.ADC-ACCT-TYP ( FND-CNT )
            ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Opn_Accum_Amt().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt()); //Natural: MOVE #OMNI-FUND-OPN-ACCUM-AMT TO #CNTRCT.ADC-ACCT-OPN-ACCUM-AMT ( FND-CNT )
            ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Opn_Nbr_Units().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts()); //Natural: MOVE #OMNI-FUND-OPN-ACCUM-UNTS TO #CNTRCT.ADC-ACCT-OPN-NBR-UNITS ( FND-CNT )
            //*  XXX
            if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt().notEquals(getZero())))                                                                   //Natural: IF #OMNI-FUND-SETTL-AMT NE 0
            {
                ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Actl_Amt().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt()); //Natural: MOVE #OMNI-FUND-SETTL-AMT TO #CNTRCT.ADC-ACCT-ACTL-AMT ( FND-CNT )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts().notEquals(getZero())))                                                                  //Natural: IF #OMNI-FUND-SETTL-UNTS NE 0
            {
                ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Actl_Nbr_Units().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts()); //Natural: MOVE #OMNI-FUND-SETTL-UNTS TO #CNTRCT.ADC-ACCT-ACTL-NBR-UNITS ( FND-CNT )
            }                                                                                                                                                             //Natural: END-IF
            //*  CM #PR-PLANS
            if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Plan_Nbr().equals(pnd_Pr_Plans.getValue("*"))))                                                           //Natural: IF #OMNI-PLAN-NBR EQ #PR-PLANS ( * )
            {
                //*  OS-102013 START
                //*    IF #OMNI-FUND-TICKER  EQ #TIAA OR EQ #STABLE(*)  /* EM - 121008
                if (condition(pnd_Tiaa_Type.getBoolean()))                                                                                                                //Natural: IF #TIAA-TYPE
                {
                    //*  OS-102013 END
                    ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Aftr_Tax_Amt().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt()); //Natural: MOVE #OMNI-FUND-RATE-SETTL-AMT TO #CNTRCT.ADC-ACCT-AFTR-TAX-AMT ( FND-CNT )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Aftr_Tax_Amt().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt()); //Natural: MOVE #OMNI-FUND-SETTL-AMT TO #CNTRCT.ADC-ACCT-AFTR-TAX-AMT ( FND-CNT )
                }                                                                                                                                                         //Natural: END-IF
                //*                           /* RS0 START
                if (condition(pnd_Roth_Request.getBoolean()))                                                                                                             //Natural: IF #ROTH-REQUEST
                {
                    ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Aftr_Tax_Amt().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt()); //Natural: MOVE #OMNI-ROTH-CONTRIB-SETTL-AMT TO #CNTRCT.ADC-ACCT-AFTR-TAX-AMT ( FND-CNT )
                    //*  RS0 END
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Source().equals(pnd_Constants_Pnd_After_Tax_Fsrce_Array.getValue("*"))))                         //Natural: IF #OMNI-FUND-SOURCE EQ #AFTER-TAX-FSRCE-ARRAY ( * )
                {
                    //*  OS-102013 START
                    //*      IF #OMNI-FUND-TICKER  EQ #TIAA OR EQ #STABLE(*)  /* EM - 121008
                    if (condition(pnd_Tiaa_Type.getBoolean()))                                                                                                            //Natural: IF #TIAA-TYPE
                    {
                        //*  OS-102013 END
                        ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Aftr_Tax_Amt().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt()); //Natural: MOVE #OMNI-FUND-RATE-SETTL-AMT TO #CNTRCT.ADC-ACCT-AFTR-TAX-AMT ( FND-CNT )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Aftr_Tax_Amt().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt()); //Natural: MOVE #OMNI-FUND-SETTL-AMT TO #CNTRCT.ADC-ACCT-AFTR-TAX-AMT ( FND-CNT )
                    }                                                                                                                                                     //Natural: END-IF
                    //*  RS0 START
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Roth_Request.getBoolean()))                                                                                                         //Natural: IF #ROTH-REQUEST
                    {
                        ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Aftr_Tax_Amt().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt()); //Natural: MOVE #OMNI-ROTH-CONTRIB-SETTL-AMT TO #CNTRCT.ADC-ACCT-AFTR-TAX-AMT ( FND-CNT )
                        //*  RS0 END
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  CM- END #PR-PLANS
            }                                                                                                                                                             //Natural: END-IF
            //*  OS-102013 START
            //*  IF (#OMNI-FUND-TICKER NE #TIAA) AND           /* EM - 121008/111510
            //*      (NOT (#OMNI-FUND-TICKER EQ #STABLE(*)))
            if (condition(! (pnd_Tiaa_Type.getBoolean())))                                                                                                                //Natural: IF NOT #TIAA-TYPE
            {
                //*  OS-102013 END
                //*      (#OMNI-FUND-TICKER NE #STABLE)
                ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Cref_Rate_Cde().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).setValue(pnd_Misc_Variables_Pnd_Omni_Rate_Code);         //Natural: MOVE #OMNI-RATE-CODE TO #CNTRCT.ADC-ACCT-CREF-RATE-CDE ( FND-CNT )
                ldaAdsl400a.getPnd_Cntrct_Adc_Grd_Mnthly_Qty().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).setValue(pnd_Per_Request_Info_Pnd_All_Cref_Mon_Pct);         //Natural: MOVE #ALL-CREF-MON-PCT TO #CNTRCT.ADC-GRD-MNTHLY-QTY ( FND-CNT )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl400a.getPnd_Cntrct_Adc_Grd_Mnthly_Qty().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).setValue(pnd_Per_Request_Info_Pnd_All_Tiaa_Grd_Pct);         //Natural: MOVE #ALL-TIAA-GRD-PCT TO #CNTRCT.ADC-GRD-MNTHLY-QTY ( FND-CNT )
                //* * 07062005 MN START
                ldaAdsl400a.getPnd_Cntrct_Adc_Orig_Lob_Ind().setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Orig_Cntrct_Lob_Ind());                                       //Natural: MOVE #OMNI-ORIG-CNTRCT-LOB-IND TO #CNTRCT.ADC-ORIG-LOB-IND
                ldaAdsl400a.getPnd_Cntrct_Adc_Tpa_Paymnt_Cnt().setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt());                                         //Natural: MOVE #OMNI-TPA-PAYMENT-CNT TO #CNTRCT.ADC-TPA-PAYMNT-CNT
                //* * 07062005 MN END
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl400a.getPnd_Cntrct_Adc_Grd_Mnthly_Qty().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).notEquals(getZero())))                             //Natural: IF #CNTRCT.ADC-GRD-MNTHLY-QTY ( FND-CNT ) NE 0
            {
                ldaAdsl400a.getPnd_Cntrct_Adc_Grd_Mnthly_Typ().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).setValue("P");                                               //Natural: MOVE 'P' TO #CNTRCT.ADC-GRD-MNTHLY-TYP ( FND-CNT )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl400a.getPnd_Cntrct_Adc_Grd_Mnthly_Typ().getValue(ldaAdsl400a.getPnd_Cntrct_Fnd_Cnt()).reset();                                                     //Natural: RESET #CNTRCT.ADC-GRD-MNTHLY-TYP ( FND-CNT )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*   >>> ACCUMULATE AMOUNTS AT FUND-SOURCE LEVEL FOR EACH TICKER <<<
            //*   ---------------------------------------------------------------
            //*  OS-102013 START
            DbsUtil.examine(new ExamineSource(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Ticker().getValue("*")), new ExamineSearch(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Ticker()),  //Natural: EXAMINE #ACCT-TICKER ( * ) FOR #OMNI-FUND-TICKER GIVING INDEX #I
                new ExamineGivingIndex(pnd_Counters_Pnd_I));
            if (condition((pnd_Counters_Pnd_I.notEquals(getZero()) && (pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_Counters_Pnd_I).equals(pnd_Constants_Pnd_Tiaa)  //Natural: IF #I NE 0 AND #ACCT-INVSTMNT-GRPNG-ID ( #I ) EQ #TIAA OR EQ #STABLE ( * )
                || (pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_Counters_Pnd_I).equals(pnd_Constants_Pnd_Stable.getValue(1)) || 
                pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue(pnd_Counters_Pnd_I).equals(pnd_Constants_Pnd_Stable.getValue(2)))))))
            {
                pnd_Tiaa_Type.setValue(true);                                                                                                                             //Natural: ASSIGN #TIAA-TYPE := TRUE
            }                                                                                                                                                             //Natural: END-IF
            //*  OS-102013 END
            //*  CM - #PR-PLANS
            if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Plan_Nbr().equals(pnd_Pr_Plans.getValue("*"))))                                                           //Natural: IF #OMNI-PLAN-NBR EQ #PR-PLANS ( * )
            {
                //*  OS-102013 START
                //*    IF #OMNI-FUND-TICKER EQ #TIAA OR EQ #STABLE(*) /* EM - 121008/111510
                if (condition(pnd_Tiaa_Type.getBoolean()))                                                                                                                //Natural: IF #TIAA-TYPE
                {
                    //*  OS-102013 END
                    ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Aftr_Tax_Amt().getValue(pnd_Counters_Pnd_X).nadd(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt());     //Natural: ADD #OMNI-FUND-RATE-SETTL-AMT TO #CNTRCT.ADC-ACCT-AFTR-TAX-AMT ( #X )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Aftr_Tax_Amt().getValue(pnd_Counters_Pnd_X).nadd(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt());          //Natural: ADD #OMNI-FUND-SETTL-AMT TO #CNTRCT.ADC-ACCT-AFTR-TAX-AMT ( #X )
                }                                                                                                                                                         //Natural: END-IF
                //*                               /* RS0 START
                if (condition(pnd_Roth_Request.getBoolean()))                                                                                                             //Natural: IF #ROTH-REQUEST
                {
                    ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Aftr_Tax_Amt().getValue(pnd_Counters_Pnd_X).nadd(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt());  //Natural: ADD #OMNI-ROTH-CONTRIB-SETTL-AMT TO #CNTRCT.ADC-ACCT-AFTR-TAX-AMT ( #X )
                    //*  RS0 END
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Source().equals(pnd_Constants_Pnd_After_Tax_Fsrce_Array.getValue("*"))))                         //Natural: IF #OMNI-FUND-SOURCE EQ #AFTER-TAX-FSRCE-ARRAY ( * )
                {
                    //*  OS-102013 START
                    //*      IF #OMNI-FUND-TICKER EQ #TIAA OR EQ #STABLE(*)   /* EM - 121008
                    if (condition(pnd_Tiaa_Type.getBoolean()))                                                                                                            //Natural: IF #TIAA-TYPE
                    {
                        //*  OS-102013 END
                        ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Aftr_Tax_Amt().getValue(pnd_Counters_Pnd_X).nadd(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt()); //Natural: ADD #OMNI-FUND-RATE-SETTL-AMT TO #CNTRCT.ADC-ACCT-AFTR-TAX-AMT ( #X )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Aftr_Tax_Amt().getValue(pnd_Counters_Pnd_X).nadd(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt());      //Natural: ADD #OMNI-FUND-SETTL-AMT TO #CNTRCT.ADC-ACCT-AFTR-TAX-AMT ( #X )
                    }                                                                                                                                                     //Natural: END-IF
                    //*  RS0 START
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Roth_Request.getBoolean()))                                                                                                         //Natural: IF #ROTH-REQUEST
                    {
                        ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Aftr_Tax_Amt().getValue(pnd_Counters_Pnd_X).nadd(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt()); //Natural: ADD #OMNI-ROTH-CONTRIB-SETTL-AMT TO #CNTRCT.ADC-ACCT-AFTR-TAX-AMT ( #X )
                        //*  RS0 END
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  CM - END #PR-PLANS
            }                                                                                                                                                             //Natural: END-IF
            //*  OS-102013 START
            //*  IF (#OMNI-FUND-TICKER NE #TIAA)  AND           /* EM - 121008/111510
            //*      (NOT (#OMNI-FUND-TICKER EQ #STABLE(*)))
            if (condition(! (pnd_Tiaa_Type.getBoolean())))                                                                                                                //Natural: IF NOT #TIAA-TYPE
            {
                //*  OS-102013 END
                //*      (#OMNI-FUND-TICKER NE #STABLE)
                //*  XXX
                ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Actl_Amt().getValue(pnd_Counters_Pnd_X).nadd(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt());                  //Natural: ADD #OMNI-FUND-SETTL-AMT TO #CNTRCT.ADC-ACCT-ACTL-AMT ( #X )
                ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Actl_Nbr_Units().getValue(pnd_Counters_Pnd_X).nadd(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts());           //Natural: ADD #OMNI-FUND-SETTL-UNTS TO #CNTRCT.ADC-ACCT-ACTL-NBR-UNITS ( #X )
                ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Opn_Accum_Amt().getValue(pnd_Counters_Pnd_X).nadd(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt());         //Natural: ADD #OMNI-FUND-OPN-ACCUM-AMT TO #CNTRCT.ADC-ACCT-OPN-ACCUM-AMT ( #X )
                ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Opn_Nbr_Units().getValue(pnd_Counters_Pnd_X).nadd(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts());        //Natural: ADD #OMNI-FUND-OPN-ACCUM-UNTS TO #CNTRCT.ADC-ACCT-OPN-NBR-UNITS ( #X )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  ACCUM TIAA HDR FUND-SETTL AMT ESV 8-23-04
                if (condition(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Contract_Id().equals(getZero())))                                                                     //Natural: IF #OMNI-CONTRACT-ID EQ 0
                {
                    //*  XXX
                    ldaAdsl400a.getPnd_Cntrct_Adc_Acct_Actl_Amt().getValue(pnd_Counters_Pnd_X).nadd(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt());              //Natural: ADD #OMNI-FUND-SETTL-AMT TO #CNTRCT.ADC-ACCT-ACTL-AMT ( #X )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  OS-102013 START
        //* *IF (#OMNI-FUND-TICKER EQ #TIAA OR EQ #STABLE(*))
        //*  EM - 121008/111510
        if (condition(pnd_Tiaa_Type.getBoolean() && (pnd_Misc_Variables_Pnd_Omni_Rate_Code.notEquals(" "))))                                                              //Natural: IF #TIAA-TYPE AND ( #OMNI-RATE-CODE NE ' ' )
        {
            pnd_Counters_Pnd_X.reset();                                                                                                                                   //Natural: RESET #X
            DbsUtil.examine(new ExamineSource(ldaAdsl400a.getPnd_Cntrct_Adc_Dtl_Tiaa_Rate_Cde().getValue("*")), new ExamineSearch(pnd_Misc_Variables_Pnd_Omni_Rate_Code), //Natural: EXAMINE #CNTRCT.ADC-DTL-TIAA-RATE-CDE ( * ) FOR #OMNI-RATE-CODE GIVING INDEX #X
                new ExamineGivingIndex(pnd_Counters_Pnd_X));
            if (condition(pnd_Counters_Pnd_X.equals(getZero())))                                                                                                          //Natural: IF #X EQ 0
            {
                ldaAdsl400a.getPnd_Cntrct_Rte_Cnt().nadd(1);                                                                                                              //Natural: ADD 1 TO RTE-CNT
                ldaAdsl400a.getPnd_Cntrct_Adc_Dtl_Tiaa_Rate_Cde().getValue(ldaAdsl400a.getPnd_Cntrct_Rte_Cnt()).setValue(pnd_Misc_Variables_Pnd_Omni_Rate_Code);          //Natural: MOVE #OMNI-RATE-CODE TO #CNTRCT.ADC-DTL-TIAA-RATE-CDE ( RTE-CNT )
                ldaAdsl400a.getPnd_Cntrct_Adc_Dtl_Tiaa_Actl_Amt().getValue(ldaAdsl400a.getPnd_Cntrct_Rte_Cnt()).setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt()); //Natural: MOVE #OMNI-FUND-RATE-SETTL-AMT TO #CNTRCT.ADC-DTL-TIAA-ACTL-AMT ( RTE-CNT )
                //* * 07062005 MN START
                //* *     MOVE #OMNI-FUND-RATE-ACCUM-AMT TO  /* COMMENTED OUT ON OMNI EXTR
                //* *         #CNTRCT.ADC-DTL-TIAA-OPN-ACCUM-AMT(RTE-CNT)
                ldaAdsl400a.getPnd_Cntrct_Adc_Tpa_Guarant_Commut_Amt().getValue(ldaAdsl400a.getPnd_Cntrct_Rte_Cnt()).setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt()); //Natural: MOVE #OMNI-TPA-GUARANT-COMMUT-AMT TO #CNTRCT.ADC-TPA-GUARANT-COMMUT-AMT ( RTE-CNT )
                //* * 07062005 MN END
                //*  EM - 030112
                //*  EM - 030112
                ldaAdsl400a.getPnd_Cntrct_Adc_Contract_Id().getValue(ldaAdsl400a.getPnd_Cntrct_Rte_Cnt()).setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Contract_Id());  //Natural: MOVE #OMNI-CONTRACT-ID TO #CNTRCT.ADC-CONTRACT-ID ( RTE-CNT )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl400a.getPnd_Cntrct_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_Counters_Pnd_X).nadd(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt());         //Natural: ADD #OMNI-FUND-RATE-SETTL-AMT TO #CNTRCT.ADC-DTL-TIAA-ACTL-AMT ( #X )
                //* * 07062005 MN START
                //* *     ADD #OMNI-FUND-RATE-ACCUM-AMT TO /* COMMENTED OUT ON OMNI EXTR
                //* *              #CNTRCT.ADC-DTL-TIAA-OPN-ACCUM-AMT(#X)
                ldaAdsl400a.getPnd_Cntrct_Adc_Tpa_Guarant_Commut_Amt().getValue(pnd_Counters_Pnd_X).nadd(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt()); //Natural: ADD #OMNI-TPA-GUARANT-COMMUT-AMT TO #CNTRCT.ADC-TPA-GUARANT-COMMUT-AMT ( #X )
                //* * 07062005 MN END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Compare_Adas_Vs_Omni_Cont_In_Rqst() throws Exception                                                                                                 //Natural: COMPARE-ADAS-VS-OMNI-CONT-IN-RQST
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------------------ *
        //*  >>> CHECK IF ADAS CNTRCTS IS MORE THAN OMNI <<<
        //*  -----------------------------------------------
        //*   PIN EXPANSION 02222017 EM 0816
        //*  FIND ADS-CNTRCT-VIEW WITH RQST-ID EQ #RQST-ID
        ldaAdsl401a.getVw_ads_Cntrct_View().startDatabaseFind                                                                                                             //Natural: FIND ADS-CNTRCT-VIEW WITH RQST-ID EQ ADS-CNTRCT-VIEW.RQST-ID
        (
        "FIND04",
        new Wc[] { new Wc("RQST_ID", "=", ldaAdsl401a.getAds_Cntrct_View_Rqst_Id(), WcType.WITH) }
        );
        FIND04:
        while (condition(ldaAdsl401a.getVw_ads_Cntrct_View().readNextRow("FIND04")))
        {
            ldaAdsl401a.getVw_ads_Cntrct_View().setIfNotFoundControlFlag(false);
            pnd_Per_Request_Info_Pnd_Adas_Cntrct.nadd(1);                                                                                                                 //Natural: ADD 1 TO #ADAS-CNTRCT
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(pnd_Per_Request_Info_Pnd_Adas_Cntrct.notEquals(pnd_Per_Request_Info_Pnd_Omni_Cntrct)))                                                              //Natural: IF #ADAS-CNTRCT NE #OMNI-CNTRCT
        {
            short decideConditionsMet3804 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #OVERALL-STAT;//Natural: VALUE #INCOMPLETE-ARRAY ( 1 )
            if (condition((pnd_Per_Request_Info_Pnd_Overall_Stat.equals(pnd_Constants_Pnd_Incomplete_Array.getValue(1)))))
            {
                decideConditionsMet3804++;
                pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_Inc_Cntrct);                                                                             //Natural: MOVE #INC-CNTRCT TO #OVERALL-STAT
            }                                                                                                                                                             //Natural: VALUE #AWAIT-2ND-SIGHT-ARRAY ( 1 )
            else if (condition((pnd_Per_Request_Info_Pnd_Overall_Stat.equals(pnd_Constants_Pnd_Await_2nd_Sight_Array.getValue(1)))))
            {
                decideConditionsMet3804++;
                pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_Awt_Cntrct);                                                                             //Natural: MOVE #AWT-CNTRCT TO #OVERALL-STAT
            }                                                                                                                                                             //Natural: VALUE ' '
            else if (condition((pnd_Per_Request_Info_Pnd_Overall_Stat.equals(" "))))
            {
                decideConditionsMet3804++;
                pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_Delayed_Cntrct);                                                                         //Natural: MOVE #DELAYED-CNTRCT TO #OVERALL-STAT
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Misc_Variables_Pnd_Debug_On.getBoolean()))                                                                                                      //Natural: IF #DEBUG-ON
        {
            getReports().write(0, "*");                                                                                                                                   //Natural: WRITE '*'
            if (Global.isEscape()) return;
            getReports().write(0, ".......INSIDE COMPARE-ADAS-VS-OMNI-CONT-IN-RQST",NEWLINE,"=",ldaAdsl400a.getPnd_Prtcpnt_Omni_Cntrcts_In_Rqst().getValue("*"),          //Natural: WRITE '.......INSIDE COMPARE-ADAS-VS-OMNI-CONT-IN-RQST' / '=' #PRTCPNT.OMNI-CNTRCTS-IN-RQST ( * ) / '=' #PRTCPNT.ADP-CNTRCTS-IN-RQST ( * )
                NEWLINE,"=",ldaAdsl400a.getPnd_Prtcpnt_Adp_Cntrcts_In_Rqst().getValue("*"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        FOR06:                                                                                                                                                            //Natural: FOR #X 1 TO #OMNI-CNTRCT
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pnd_Per_Request_Info_Pnd_Omni_Cntrct)); pnd_Counters_Pnd_X.nadd(1))
        {
            if (condition(ldaAdsl400a.getPnd_Prtcpnt_Omni_Cntrcts_In_Rqst().getValue(pnd_Counters_Pnd_X).equals(" ")))                                                    //Natural: IF #PRTCPNT.OMNI-CNTRCTS-IN-RQST ( #X ) EQ ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(ldaAdsl400a.getPnd_Prtcpnt_Adp_Cntrcts_In_Rqst().getValue("*")), new ExamineSearch(ldaAdsl400a.getPnd_Prtcpnt_Omni_Cntrcts_In_Rqst().getValue(pnd_Counters_Pnd_X)),  //Natural: EXAMINE #PRTCPNT.ADP-CNTRCTS-IN-RQST ( * ) FOR #PRTCPNT.OMNI-CNTRCTS-IN-RQST ( #X ) GIVING INDEX #I
                new ExamineGivingIndex(pnd_Counters_Pnd_I));
            if (condition(pnd_Counters_Pnd_I.equals(getZero())))                                                                                                          //Natural: IF #I EQ 0
            {
                DbsUtil.examine(new ExamineSource(ldaAdsl400a.getPnd_Prtcpnt_Adp_Cntrcts_In_Rqst().getValue("*"),true), new ExamineSearch(pnd_Misc_Variables_Pnd_Contract_Space,  //Natural: EXAMINE FULL #PRTCPNT.ADP-CNTRCTS-IN-RQST ( * ) FOR FULL #CONTRACT-SPACE GIVING INDEX #I
                    true), new ExamineGivingIndex(pnd_Counters_Pnd_I));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Counters_Pnd_I.notEquals(getZero())))                                                                                                       //Natural: IF #I NE 0
            {
                ldaAdsl400a.getPnd_Prtcpnt_Adp_Cntrcts_In_Rqst().getValue(pnd_Counters_Pnd_I).setValue(ldaAdsl400a.getPnd_Prtcpnt_Omni_Cntrcts_In_Rqst().getValue(pnd_Counters_Pnd_X)); //Natural: MOVE #PRTCPNT.OMNI-CNTRCTS-IN-RQST ( #X ) TO #PRTCPNT.ADP-CNTRCTS-IN-RQST ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Misc_Variables_Pnd_Debug_On.getBoolean()))                                                                                                      //Natural: IF #DEBUG-ON
        {
            getReports().write(0, ".......AFTER COMPARE-ADAS-VS-OMNI-CONT-IN-RQST",NEWLINE,"=",ldaAdsl400a.getPnd_Prtcpnt_Omni_Cntrcts_In_Rqst().getValue("*"),           //Natural: WRITE '.......AFTER COMPARE-ADAS-VS-OMNI-CONT-IN-RQST' / '=' #PRTCPNT.OMNI-CNTRCTS-IN-RQST ( * ) / '=' #PRTCPNT.ADP-CNTRCTS-IN-RQST ( * )
                NEWLINE,"=",ldaAdsl400a.getPnd_Prtcpnt_Adp_Cntrcts_In_Rqst().getValue("*"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_Prev_Rqst_Overall_Stat() throws Exception                                                                                                     //Natural: UPDATE-PREV-RQST-OVERALL-STAT
    {
        if (BLNatReinput.isReinput()) return;

        PND_PND_L4222:                                                                                                                                                    //Natural: GET ADS-PRTCPNT-VIEW #PRTCPNT.ADP-ISN
        ldaAdsl401.getVw_ads_Prtcpnt_View().readByID(ldaAdsl400a.getPnd_Prtcpnt_Adp_Isn().getLong(), "PND_PND_L4222");
        ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().setValue(pnd_Per_Request_Info_Pnd_Overall_Stat);                                                                    //Natural: MOVE #OVERALL-STAT TO ADS-PRTCPNT-VIEW.ADP-STTS-CDE
        ldaAdsl401.getAds_Prtcpnt_View_Adp_Rqst_Last_Updtd_Dte().setValue(Global.getDATX());                                                                              //Natural: MOVE *DATX TO ADS-PRTCPNT-VIEW.ADP-RQST-LAST-UPDTD-DTE
        ldaAdsl401.getAds_Prtcpnt_View_Adp_Rqst_1st_Vrfd_Id().setValue(pnd_Constants_Pnd_Batchads);                                                                       //Natural: MOVE #BATCHADS TO ADS-PRTCPNT-VIEW.ADP-RQST-1ST-VRFD-ID
        if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Wpid().equals(" ")))                                                                                             //Natural: IF ADS-PRTCPNT-VIEW.ADP-WPID EQ ' '
        {
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Wpid().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Wpid());                                                          //Natural: MOVE #ADSA480-MIT-UPDATE.ADP-WPID TO ADS-PRTCPNT-VIEW.ADP-WPID
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Mit_Log_Dte_Tme().equals(" ")))                                                                                  //Natural: IF ADS-PRTCPNT-VIEW.ADP-MIT-LOG-DTE-TME EQ ' '
        {
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Mit_Log_Dte_Tme().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Mit_Log_Dte_Tme());                                    //Natural: MOVE #ADSA480-MIT-UPDATE.ADP-MIT-LOG-DTE-TME TO ADS-PRTCPNT-VIEW.ADP-MIT-LOG-DTE-TME
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Bps_Unit().equals(" ")))                                                                                         //Natural: IF ADS-PRTCPNT-VIEW.ADP-BPS-UNIT EQ ' '
        {
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Bps_Unit().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Bps_Unit());                                                  //Natural: MOVE #ADSA480-MIT-UPDATE.ADP-BPS-UNIT TO ADS-PRTCPNT-VIEW.ADP-BPS-UNIT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (ldaAdsl400a.getPnd_Prtcpnt_Adp_Cntrcts_In_Rqst().getValue("*").equals(ldaAdsl401.getAds_Prtcpnt_View_Adp_Cntrcts_In_Rqst().getValue("*")))))     //Natural: IF NOT #PRTCPNT.ADP-CNTRCTS-IN-RQST ( * ) EQ ADS-PRTCPNT-VIEW.ADP-CNTRCTS-IN-RQST ( * )
        {
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Cntrcts_In_Rqst().getValue("*").setValue(ldaAdsl400a.getPnd_Prtcpnt_Adp_Cntrcts_In_Rqst().getValue("*"));                  //Natural: MOVE #PRTCPNT.ADP-CNTRCTS-IN-RQST ( * ) TO ADS-PRTCPNT-VIEW.ADP-CNTRCTS-IN-RQST ( * )
        }                                                                                                                                                                 //Natural: END-IF
        //*  121007
        //*  121007
        if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().equals(pnd_Constants_Pnd_Invalid_Rsttlmnt)))                                                          //Natural: IF ADS-PRTCPNT-VIEW.ADP-STTS-CDE = #INVALID-RSTTLMNT
        {
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind().setValue(pnd_Constants_Pnd_Close);                                                                          //Natural: ASSIGN ADS-PRTCPNT-VIEW.ADP-OPN-CLSD-IND := #CLOSE
            //*  121007
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl401.getVw_ads_Prtcpnt_View().updateDBRow("PND_PND_L4222");                                                                                                 //Natural: UPDATE ( ##L4222. )
        FOR07:                                                                                                                                                            //Natural: FOR #X 1 TO #OMNI-CNTRCT
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pnd_Per_Request_Info_Pnd_Omni_Cntrct)); pnd_Counters_Pnd_X.nadd(1))
        {
            if (condition(ldaAdsl400a.getPnd_Prtcpnt_Adp_Cntrct_Isn().getValue(pnd_Counters_Pnd_X).equals(getZero())))                                                    //Natural: IF #PRTCPNT.ADP-CNTRCT-ISN ( #X ) EQ 0
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            PND_PND_L4280:                                                                                                                                                //Natural: GET ADS-CNTRCT-VIEW #PRTCPNT.ADP-CNTRCT-ISN ( #X )
            ldaAdsl401a.getVw_ads_Cntrct_View().readByID(ldaAdsl400a.getPnd_Prtcpnt_Adp_Cntrct_Isn().getValue(pnd_Counters_Pnd_X).getLong(), "PND_PND_L4280");
            ldaAdsl401a.getAds_Cntrct_View_Adc_Stts_Cde().setValue(pnd_Per_Request_Info_Pnd_Overall_Stat);                                                                //Natural: MOVE #OVERALL-STAT TO ADS-CNTRCT-VIEW.ADC-STTS-CDE
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Sqnce_Nbr().equals(getZero())))                                                                              //Natural: IF ADS-CNTRCT-VIEW.ADC-SQNCE-NBR EQ 0
            {
                pnd_Per_Request_Info_Pnd_Last_Cntrct_Seq.nadd(1);                                                                                                         //Natural: ADD 1 TO #LAST-CNTRCT-SEQ
                ldaAdsl401a.getAds_Cntrct_View_Adc_Sqnce_Nbr().setValue(pnd_Per_Request_Info_Pnd_Last_Cntrct_Seq);                                                        //Natural: MOVE #LAST-CNTRCT-SEQ TO ADS-CNTRCT-VIEW.ADC-SQNCE-NBR
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl401a.getVw_ads_Cntrct_View().updateDBRow("PND_PND_L4280");                                                                                             //Natural: UPDATE ( ##L4280. )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        short decideConditionsMet3876 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #OVERALL-STAT;//Natural: VALUE #NO-MATCH
        if (condition((pnd_Per_Request_Info_Pnd_Overall_Stat.equals(pnd_Constants_Pnd_No_Match))))
        {
            decideConditionsMet3876++;
            pnd_Counters_Pnd_No_Match_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #NO-MATCH-CNT
        }                                                                                                                                                                 //Natural: VALUE #AWAIT-PROCESS
        else if (condition((pnd_Per_Request_Info_Pnd_Overall_Stat.equals(pnd_Constants_Pnd_Await_Process))))
        {
            decideConditionsMet3876++;
            pnd_Counters_Pnd_Full_Match_Cnt.nadd(1);                                                                                                                      //Natural: ADD 1 TO #FULL-MATCH-CNT
        }                                                                                                                                                                 //Natural: VALUE #INCOMPLETE-ARRAY ( * )
        else if (condition((pnd_Per_Request_Info_Pnd_Overall_Stat.equals(pnd_Constants_Pnd_Incomplete_Array.getValue("*")))))
        {
            decideConditionsMet3876++;
            pnd_Counters_Pnd_Orig_Incomplete.nadd(1);                                                                                                                     //Natural: ADD 1 TO #ORIG-INCOMPLETE
        }                                                                                                                                                                 //Natural: VALUE #AWAIT-2ND-SIGHT-ARRAY ( * )
        else if (condition((pnd_Per_Request_Info_Pnd_Overall_Stat.equals(pnd_Constants_Pnd_Await_2nd_Sight_Array.getValue("*")))))
        {
            decideConditionsMet3876++;
            pnd_Counters_Pnd_Orig_Await_2site.nadd(1);                                                                                                                    //Natural: ADD 1 TO #ORIG-AWAIT-2SITE
        }                                                                                                                                                                 //Natural: VALUE #DELAYED-STAT-ARRAY ( * )
        else if (condition((pnd_Per_Request_Info_Pnd_Overall_Stat.equals(pnd_Constants_Pnd_Delayed_Stat_Array.getValue("*")))))
        {
            decideConditionsMet3876++;
            pnd_Counters_Pnd_Delayed_Cnt.nadd(1);                                                                                                                         //Natural: ADD 1 TO #DELAYED-CNT
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Update_Prev_Rqst_Mit() throws Exception                                                                                                              //Natural: UPDATE-PREV-RQST-MIT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------ *
        if (condition(pnd_Per_Request_Info_Pnd_Overall_Stat.equals(" ")))                                                                                                 //Natural: IF #OVERALL-STAT EQ ' '
        {
            pnd_Per_Request_Info_Pnd_Overall_Stat.setValue(pnd_Constants_Pnd_Await_Process);                                                                              //Natural: MOVE #AWAIT-PROCESS TO #OVERALL-STAT
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl400a.getPnd_Prtcpnt_Adp_Stts_Cde().setValue(pnd_Per_Request_Info_Pnd_Overall_Stat);                                                                        //Natural: MOVE #OVERALL-STAT TO #PRTCPNT.ADP-STTS-CDE
        pdaAdsa480.getPnd_Adsa480_Mit_Update().setValuesByName(ldaAdsl400a.getPnd_Prtcpnt());                                                                             //Natural: MOVE BY NAME #PRTCPNT TO #ADSA480-MIT-UPDATE
        //*  ESV 071604
        pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Plan_Nbr().setValue(pnd_Per_Request_Info_Pnd_Plan_Nbr);                                                                  //Natural: MOVE #PLAN-NBR TO #ADSA480-MIT-UPDATE.ADP-PLAN-NBR
        if (condition(pnd_Per_Request_Info_Pnd_Prtcpnt_Match_Found.getBoolean()))                                                                                         //Natural: IF #PRTCPNT-MATCH-FOUND
        {
            if (condition(pnd_Per_Request_Info_Pnd_Resettlement.getBoolean()))                                                                                            //Natural: IF #RESETTLEMENT
            {
                pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Action_Cde().setValue(pnd_Constants_Pnd_Mit_Sub_Rqst);                                                           //Natural: MOVE #MIT-SUB-RQST TO #ACTION-CDE
                //*  IF RESETTLEMENT, GET WPID
                pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Wpid().reset();                                                                                                  //Natural: RESET #ADSA480-MIT-UPDATE.ADP-WPID
                //*     FROM NAZ TABLE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*                                           /* DO NOT CALL MIT IF SUCCESS
                //*  FUL PROCESS ESV 06-21-04
                if (condition(pnd_Per_Request_Info_Pnd_Overall_Stat.equals(pnd_Constants_Pnd_Await_Process)))                                                             //Natural: IF #OVERALL-STAT EQ #AWAIT-PROCESS
                {
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Entry_User_Id().setValue(ldaAdsl400a.getPnd_Prtcpnt_Adp_Rqst_Last_Updtd_Id());                                   //Natural: MOVE #PRTCPNT.ADP-RQST-LAST-UPDTD-ID TO #ADSA480-MIT-UPDATE.ADP-ENTRY-USER-ID #USER-ID
                pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_User_Id().setValue(ldaAdsl400a.getPnd_Prtcpnt_Adp_Rqst_Last_Updtd_Id());
                pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Action_Cde().setValue(pnd_Constants_Pnd_Mit_Update);                                                             //Natural: MOVE #MIT-UPDATE TO #ACTION-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_User_Id().setValue(ldaAdsl400a.getPnd_Prtcpnt_Adp_Entry_User_Id());                                                  //Natural: MOVE #PRTCPNT.ADP-ENTRY-USER-ID TO #USER-ID
            pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Action_Cde().setValue(pnd_Constants_Pnd_Mit_Add);                                                                    //Natural: MOVE #MIT-ADD TO #ACTION-CDE
        }                                                                                                                                                                 //Natural: END-IF
        pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Type_Of_Process().setValue(pnd_Constants_Pnd_Batchads);                                                                  //Natural: MOVE #BATCHADS TO #TYPE-OF-PROCESS
        pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Debug_On().setValue(pnd_Misc_Variables_Pnd_Debug_On.getBoolean());                                                   //Natural: MOVE #DEBUG-ON TO #MIT-DEBUG-ON
        DbsUtil.callnat(Adsn480.class , getCurrentProcessState(), pdaAdsa480.getPnd_Adsa480_Mit_Update());                                                                //Natural: CALLNAT 'ADSN480' #ADSA480-MIT-UPDATE
        if (condition(Global.isEscape())) return;
        if (condition(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Msg().notEquals(" ")))                                                                                 //Natural: IF #MIT-MSG NE ' '
        {
            pnd_Adsn998_Error_Handler.reset();                                                                                                                            //Natural: RESET #ADSN998-ERROR-HANDLER
            pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("MIT");                                                                                                      //Natural: ASSIGN #STEP-NAME := 'MIT'
            pnd_Adsn998_Error_Handler_Pnd_Error_Msg.setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Msg());                                                         //Natural: ASSIGN #ERROR-MSG := #MIT-MSG
            pnd_Adsn998_Error_Handler_Pnd_Unique_Id.setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Unique_Id());                                                       //Natural: ASSIGN #UNIQUE-ID := #ADSA480-MIT-UPDATE.ADP-UNIQUE-ID
                                                                                                                                                                          //Natural: PERFORM ERROR-LOG
            sub_Error_Log();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Error_Report() throws Exception                                                                                                                //Natural: PRINT-ERROR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------- *
        pnd_Misc_Variables_Pnd_Report_Msg.reset();                                                                                                                        //Natural: RESET #REPORT-MSG #REPORT-RQST-TYPE
        pnd_Misc_Variables_Pnd_Report_Rqst_Type.reset();
        short decideConditionsMet3938 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN NOT #OMNI-FORMAT-OK
        if (condition(! ((pnd_Misc_Variables_Pnd_Omni_Format_Ok.getBoolean()))))
        {
            decideConditionsMet3938++;
            short decideConditionsMet3940 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #RB-RETURN-CODE;//Natural: VALUE '1'
            if (condition((pnd_Misc_Variables_Pnd_Rb_Return_Code.equals("1"))))
            {
                decideConditionsMet3940++;
                pnd_Misc_Variables_Pnd_Report_Msg.setValue(DbsUtil.compress("RATE-BASE CODE ERROR (ADAN501 - ", "INVALID SUB-ACCOUNT ID) : ", ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Contract_Id())); //Natural: COMPRESS 'RATE-BASE CODE ERROR (ADAN501 - ' 'INVALID SUB-ACCOUNT ID) : ' #OMNI-CONTRACT-ID TO #REPORT-MSG
            }                                                                                                                                                             //Natural: VALUE '2'
            else if (condition((pnd_Misc_Variables_Pnd_Rb_Return_Code.equals("2"))))
            {
                decideConditionsMet3940++;
                pnd_Misc_Variables_Pnd_Report_Msg.setValue(DbsUtil.compress("RATE-BASE CODE ERROR (ADAN501 - ", "INVALID RATE BASIS CODE) : ", ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Contract_Id())); //Natural: COMPRESS 'RATE-BASE CODE ERROR (ADAN501 - ' 'INVALID RATE BASIS CODE) : ' #OMNI-CONTRACT-ID TO #REPORT-MSG
            }                                                                                                                                                             //Natural: VALUE '99'
            else if (condition((pnd_Misc_Variables_Pnd_Rb_Return_Code.equals("99"))))
            {
                decideConditionsMet3940++;
                pnd_Misc_Variables_Pnd_Report_Msg.setValue(DbsUtil.compress("RATE-BASE CODE ERROR (ADAN501 - ", "INVALID TYPE OF CALL ) : ", ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Contract_Id())); //Natural: COMPRESS 'RATE-BASE CODE ERROR (ADAN501 - ' 'INVALID TYPE OF CALL ) : ' #OMNI-CONTRACT-ID TO #REPORT-MSG
            }                                                                                                                                                             //Natural: VALUE '4'
            else if (condition((pnd_Misc_Variables_Pnd_Rb_Return_Code.equals("4"))))
            {
                decideConditionsMet3940++;
                pnd_Misc_Variables_Pnd_Report_Msg.setValue(DbsUtil.compress("RATE-BASE CODE ERROR (NECN4000 - ", "PRODUCT CODE NOT FOUND) : ", ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct())); //Natural: COMPRESS 'RATE-BASE CODE ERROR (NECN4000 - ' 'PRODUCT CODE NOT FOUND) : ' #OMNI-TIAA-CNTRCT TO #REPORT-MSG
            }                                                                                                                                                             //Natural: VALUE '5'
            else if (condition((pnd_Misc_Variables_Pnd_Rb_Return_Code.equals("5"))))
            {
                decideConditionsMet3940++;
                pnd_Misc_Variables_Pnd_Report_Msg.setValue(DbsUtil.compress("RATE-BASE CODE ERROR (NECN4000 - ", "RATE CODE NOT FOUND) : ", ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct())); //Natural: COMPRESS 'RATE-BASE CODE ERROR (NECN4000 - ' 'RATE CODE NOT FOUND) : ' #OMNI-TIAA-CNTRCT TO #REPORT-MSG
            }                                                                                                                                                             //Natural: VALUE '6'
            else if (condition((pnd_Misc_Variables_Pnd_Rb_Return_Code.equals("6"))))
            {
                decideConditionsMet3940++;
                pnd_Misc_Variables_Pnd_Report_Msg.setValue(DbsUtil.compress("Guaranteed Commuted value for TPA", "/IPRO from TPA is 0 : ", ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct())); //Natural: COMPRESS 'Guaranteed Commuted value for TPA' '/IPRO from TPA is 0 : ' #OMNI-TIAA-CNTRCT TO #REPORT-MSG
            }                                                                                                                                                             //Natural: VALUE '81'
            else if (condition((pnd_Misc_Variables_Pnd_Rb_Return_Code.equals("81"))))
            {
                decideConditionsMet3940++;
                pnd_Misc_Variables_Pnd_Report_Msg.setValue("First Roth Contribution date GT current date");                                                               //Natural: MOVE 'First Roth Contribution date GT current date' TO #REPORT-MSG
            }                                                                                                                                                             //Natural: VALUE '82'
            else if (condition((pnd_Misc_Variables_Pnd_Rb_Return_Code.equals("82"))))
            {
                decideConditionsMet3940++;
                pnd_Misc_Variables_Pnd_Report_Msg.setValue("First Roth Contribution date LT 20060101");                                                                   //Natural: MOVE 'First Roth Contribution date LT 20060101' TO #REPORT-MSG
            }                                                                                                                                                             //Natural: VALUE '83'
            else if (condition((pnd_Misc_Variables_Pnd_Rb_Return_Code.equals("83"))))
            {
                decideConditionsMet3940++;
                pnd_Misc_Variables_Pnd_Report_Msg.setValue("First Roth Contribution date not populated");                                                                 //Natural: MOVE 'First Roth Contribution date not populated' TO #REPORT-MSG
            }                                                                                                                                                             //Natural: VALUE '84'
            else if (condition((pnd_Misc_Variables_Pnd_Rb_Return_Code.equals("84"))))
            {
                decideConditionsMet3940++;
                pnd_Misc_Variables_Pnd_Report_Msg.setValue("Invalid Roth Dissability Date");                                                                              //Natural: MOVE 'Invalid Roth Dissability Date' TO #REPORT-MSG
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Misc_Variables_Pnd_Report_Msg.setValue("RECORD NOT PROCESSED, INVALID FIELD FORMAT FOUND");                                                           //Natural: MOVE 'RECORD NOT PROCESSED, INVALID FIELD FORMAT FOUND' TO #REPORT-MSG
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN NOT #PIN-OKAY
        else if (condition(! ((pnd_Misc_Variables_Pnd_Pin_Okay.getBoolean()))))
        {
            decideConditionsMet3938++;
            pnd_Misc_Variables_Pnd_Report_Msg.setValue("OMNI PIN DOES NOT EXIST IN COR");                                                                                 //Natural: MOVE 'OMNI PIN DOES NOT EXIST IN COR' TO #REPORT-MSG
        }                                                                                                                                                                 //Natural: WHEN #UNMATCHED-RSTLMT
        else if (condition(pnd_Per_Request_Info_Pnd_Unmatched_Rstlmt.getBoolean()))
        {
            decideConditionsMet3938++;
            pnd_Misc_Variables_Pnd_Report_Msg.setValue(DbsUtil.compress("NO MATCHING ORIGINAL ADAS REQUEST FOUND FOR", "OMNI RESETTLEMENT REQUEST"));                     //Natural: COMPRESS 'NO MATCHING ORIGINAL ADAS REQUEST FOUND FOR' 'OMNI RESETTLEMENT REQUEST' TO #REPORT-MSG
        }                                                                                                                                                                 //Natural: WHEN #PROCESSED-OMNI-RQST
        else if (condition(pnd_Per_Request_Info_Pnd_Processed_Omni_Rqst.getBoolean()))
        {
            decideConditionsMet3938++;
            //*  121007
            pnd_Misc_Variables_Pnd_Report_Msg.setValue("OMNI REQUEST ALREADY WENT THROUGH MATCHING PROCESS");                                                             //Natural: MOVE 'OMNI REQUEST ALREADY WENT THROUGH MATCHING PROCESS' TO #REPORT-MSG
        }                                                                                                                                                                 //Natural: WHEN #PROCESSED-RSTTLMNT
        else if (condition(pnd_Per_Request_Info_Pnd_Processed_Rsttlmnt.getBoolean()))
        {
            decideConditionsMet3938++;
            //*  121007
            //*  121007
            pnd_Misc_Variables_Pnd_Report_Msg.setValue("RESETTLEMENT HAS BEEN PREVIOUSLY PROCESSED");                                                                     //Natural: MOVE 'RESETTLEMENT HAS BEEN PREVIOUSLY PROCESSED' TO #REPORT-MSG
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Misc_Variables_Pnd_Report_Msg.setValue("UN-IDENTIFIED ERROR");                                                                                            //Natural: MOVE 'UN-IDENTIFIED ERROR' TO #REPORT-MSG
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Per_Request_Info_Pnd_Resettlement.getBoolean()))                                                                                                //Natural: IF #RESETTLEMENT
        {
            pnd_Misc_Variables_Pnd_Report_Rqst_Type.setValue(pnd_Constants_Pnd_R_Annty_Typ);                                                                              //Natural: MOVE #R-ANNTY-TYP TO #REPORT-RQST-TYPE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Misc_Variables_Pnd_Report_Rqst_Type.setValue(pnd_Constants_Pnd_M_Annty_Typ);                                                                              //Natural: MOVE #M-ANNTY-TYP TO #REPORT-RQST-TYPE
        }                                                                                                                                                                 //Natural: END-IF
        //*  06302006 MN 010109
        //*  +5
        //*  +8
        //*  +5
        //*  +5
        getReports().write(1, new TabSetting(5),pnd_Counters_Pnd_Total_Input_Recs, new ReportEditMask ("ZZZZZZZZ9"),new TabSetting(17),ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Pin_A(),new  //Natural: WRITE ( 1 ) 005T #TOTAL-INPUT-RECS ( EM = ZZZZZZZZ9 ) 017T #OMNI-PIN-A 037T #OMNI-TIAA-CNTRCT 048T #OMNI-EFF-DTE 058T #REPORT-RQST-TYPE 064T #REPORT-MSG
            TabSetting(37),ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct(),new TabSetting(48),ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Eff_Dte(),new TabSetting(58),pnd_Misc_Variables_Pnd_Report_Rqst_Type,new 
            TabSetting(64),pnd_Misc_Variables_Pnd_Report_Msg);
        if (Global.isEscape()) return;
        //*  END PIN EXPANSION 02222017
        //*  MOVE TRUE TO ERR-REPORT-PRINTED /* 01122006 MN - ACCORDING TO ED
        //*  011107
        //*  011107
        if (condition(pnd_S_Err_Cntrct.notEquals(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct())))                                                                   //Natural: IF #S-ERR-CNTRCT NE #OMNI-TIAA-CNTRCT
        {
            pnd_S_Err_Cntrct.setValue(ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct());                                                                               //Natural: ASSIGN #S-ERR-CNTRCT := #OMNI-TIAA-CNTRCT
            pnd_Counters_Pnd_Total_Omni_Err_Cntrct.nadd(1);                                                                                                               //Natural: ADD 1 TO #TOTAL-OMNI-ERR-CNTRCT
            //*  011107
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Log() throws Exception                                                                                                                         //Natural: ERROR-LOG
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------- *
        pnd_Adsn998_Error_Handler_Pnd_Appl_Id.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "BATCH", Global.getINIT_ID()));                                    //Natural: COMPRESS 'BATCH' *INIT-ID INTO #APPL-ID LEAVING NO SPACE
        pnd_Adsn998_Error_Handler_Pnd_Calling_Program.setValue(Global.getPROGRAM());                                                                                      //Natural: ASSIGN #CALLING-PROGRAM := *PROGRAM
        //*   PIN EXPANSION 02222017
        if (condition(pnd_Adsn998_Error_Handler_Pnd_Unique_Id.equals(getZero())))                                                                                         //Natural: IF #UNIQUE-ID EQ 0
        {
            pnd_Adsn998_Error_Handler_Pnd_Unique_Id.compute(new ComputeParameters(false, pnd_Adsn998_Error_Handler_Pnd_Unique_Id), ldaAdsl400.getPnd_Omni_Input_Pnd_Omni_Pin_A().val()); //Natural: ASSIGN #UNIQUE-ID := VAL ( #OMNI-PIN-A )
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Adsn998.class , getCurrentProcessState(), pdaAdspda_M.getMsg_Info_Sub(), pnd_Adsn998_Error_Handler);                                              //Natural: CALLNAT 'ADSN998' MSG-INFO-SUB #ADSN998-ERROR-HANDLER
        if (condition(Global.isEscape())) return;
    }
    private void sub_Terminate_Routine() throws Exception                                                                                                                 //Natural: TERMINATE-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------- *
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
                                                                                                                                                                          //Natural: PERFORM ERROR-LOG
        sub_Error_Log();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        getReports().write(0, NEWLINE,NEWLINE,NEWLINE,NEWLINE," ================================================================= ",NEWLINE,"|*****************************************************************|", //Natural: WRITE /// /' ================================================================= ' /'|*****************************************************************|' /'|*****************************************************************|' /'|** TERMINATING ADAS BATCH PROCESS.....                         **|' /'|**                                                             **|' /'|**' #TERMINATE-MSG1 '**|' /'|**' #TERMINATE-MSG2 '**|' /'|**' #TERMINATE-MSG3 '**|' /'|**                                                             **|' /'|**  >>> IF NECESSARY, PLEASE CALL - ADAS SYSTEM SUPPORT <<<    **|' /'|*****************************************************************|' /'|*****************************************************************|' /' ================================================================= '
            NEWLINE,"|*****************************************************************|",NEWLINE,"|** TERMINATING ADAS BATCH PROCESS.....                         **|",
            NEWLINE,"|**                                                             **|",NEWLINE,"|**",pnd_Terminate_Pnd_Terminate_Msg1,"**|",NEWLINE,"|**",
            pnd_Terminate_Pnd_Terminate_Msg2,"**|",NEWLINE,"|**",pnd_Terminate_Pnd_Terminate_Msg3,"**|",NEWLINE,"|**                                                             **|",
            NEWLINE,"|**  >>> IF NECESSARY, PLEASE CALL - ADAS SYSTEM SUPPORT <<<    **|",NEWLINE,"|*****************************************************************|",
            NEWLINE,"|*****************************************************************|",NEWLINE," ================================================================= ");
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB-MESSAGE
        sub_End_Of_Job_Message();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        DbsUtil.terminate(pnd_Terminate_Pnd_Terminate_No);  if (true) return;                                                                                             //Natural: TERMINATE #TERMINATE-NO
    }
    private void sub_End_Of_Job_Message() throws Exception                                                                                                                //Natural: END-OF-JOB-MESSAGE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------- *
        getReports().write(0, NEWLINE,NEWLINE,NEWLINE,NEWLINE," ================================================================= ",NEWLINE,"|**            ANNUITIZATION OF OMNI PLUS CONTRACTS             **|", //Natural: WRITE /// /' ================================================================= ' /'|**            ANNUITIZATION OF OMNI PLUS CONTRACTS             **|' /'|**                                                             **|' /'|**   ADAS - OMNI MATCHING BATCH PROCESS ---- JOB RUN ENDED     **|' /'|**                                                             **|' /'|** TOTAL INPUT RECORDS READ       : ' #TOTAL-INPUT-RECS '               **|' /'|** TOTAL OMNI RECORDS READ        : ' #TOTAL-OMNI-RECS '               **|' /'|** TOTAL ACKL RECORDS BYPASSED    : ' #TOTAL-ACKL-RECS '               **|' /'|** TOTAL OMNI REQUEST PROCESSED   : ' #TOTAL-OMNI-RQST '               **|' /'|** TOTAL OMNI CONTRACTS PROCESSED : ' #TOTAL-OMNI-CNTRCT '               **|' /'|** TOTAL OMNI CONTRACTS W/ ERRORS : ' #TOTAL-OMNI-ERR-CNTRCT '               **|' /'|**                                                             **|' /'|**            >> REQUESTS RESULT-STATUS COUNT <<               **|' /'|** FULL MATCH REQUESTS                : ' #FULL-MATCH-CNT '           **|' /'|** NO MATCH REQUESTS                  : ' #NO-MATCH-CNT '           **|' /'|** PARTIAL MATCH-MISMATCH ADAS DATA   : ' #DELAYED-CNT '           **|' /'|** PARTIAL MATCH-ADAS RQST AWAIT 2SITE: ' #ORIG-AWAIT-2SITE '           **|' /'|** PARTIAL MATCH-ADAS RQST INCOMPLETE : ' #ORIG-INCOMPLETE '           **|' /' ================================================================= '
            NEWLINE,"|**                                                             **|",NEWLINE,"|**   ADAS - OMNI MATCHING BATCH PROCESS ---- JOB RUN ENDED     **|",
            NEWLINE,"|**                                                             **|",NEWLINE,"|** TOTAL INPUT RECORDS READ       : ",pnd_Counters_Pnd_Total_Input_Recs,
            "               **|",NEWLINE,"|** TOTAL OMNI RECORDS READ        : ",pnd_Counters_Pnd_Total_Omni_Recs,"               **|",NEWLINE,"|** TOTAL ACKL RECORDS BYPASSED    : ",
            pnd_Counters_Pnd_Total_Ackl_Recs,"               **|",NEWLINE,"|** TOTAL OMNI REQUEST PROCESSED   : ",pnd_Counters_Pnd_Total_Omni_Rqst,"               **|",
            NEWLINE,"|** TOTAL OMNI CONTRACTS PROCESSED : ",pnd_Counters_Pnd_Total_Omni_Cntrct,"               **|",NEWLINE,"|** TOTAL OMNI CONTRACTS W/ ERRORS : ",
            pnd_Counters_Pnd_Total_Omni_Err_Cntrct,"               **|",NEWLINE,"|**                                                             **|",NEWLINE,
            "|**            >> REQUESTS RESULT-STATUS COUNT <<               **|",NEWLINE,"|** FULL MATCH REQUESTS                : ",pnd_Counters_Pnd_Full_Match_Cnt,
            "           **|",NEWLINE,"|** NO MATCH REQUESTS                  : ",pnd_Counters_Pnd_No_Match_Cnt,"           **|",NEWLINE,"|** PARTIAL MATCH-MISMATCH ADAS DATA   : ",
            pnd_Counters_Pnd_Delayed_Cnt,"           **|",NEWLINE,"|** PARTIAL MATCH-ADAS RQST AWAIT 2SITE: ",pnd_Counters_Pnd_Orig_Await_2site,"           **|",
            NEWLINE,"|** PARTIAL MATCH-ADAS RQST INCOMPLETE : ",pnd_Counters_Pnd_Orig_Incomplete,"           **|",NEWLINE," ================================================================= ");
        if (Global.isEscape()) return;
    }
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        //* CM - OPEN MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0")))                                                                   //Natural: IF ##DATA-RESPONSE ( 1 ) = '0'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR08:                                                                                                                                                            //Natural: FOR #I = 1 TO 60
        for (pnd_Counters_Pnd_I.setValue(1); condition(pnd_Counters_Pnd_I.lessOrEqual(60)); pnd_Counters_Pnd_I.nadd(1))
        {
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_Counters_Pnd_I))); //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Rc);                                                                                                                                //Natural: WRITE '=' #RC
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        pnd_Adsn998_Error_Handler.reset();                                                                                                                                //Natural: RESET #ADSN998-ERROR-HANDLER
        pnd_Terminate_Pnd_Terminate_Msg.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "INTERNAL ERROR ENCOUNTERED IN PROGRAM :", Global.getINIT_PROGRAM(),     //Natural: COMPRESS 'INTERNAL ERROR ENCOUNTERED IN PROGRAM :' *INIT-PROGRAM ' ERR NO : ' *ERROR-NR ' ERR LINE :' *ERROR-LINE TO #TERMINATE-MSG LEAVING NO SPACE
            " ERR NO : ", Global.getERROR_NR(), " ERR LINE :", Global.getERROR_LINE()));
        pnd_Terminate_Pnd_Terminate_No.setValue(99);                                                                                                                      //Natural: MOVE 99 TO #TERMINATE-NO
        pnd_Adsn998_Error_Handler_Pnd_Error_Msg.setValue(pnd_Terminate_Pnd_Terminate_Msg);                                                                                //Natural: MOVE #TERMINATE-MSG TO #ERROR-MSG
                                                                                                                                                                          //Natural: PERFORM TERMINATE-ROUTINE
        sub_Terminate_Routine();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132 ZP=OFF SG=OFF");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(1),"ADSP400",new TabSetting(50),"ANNUITIZATION OF OMNI PLUS CONTRACTS",new 
            TabSetting(110),"Run Date:", Color.blue, new FieldAttributes ("AD=I"),new TabSetting(120),Global.getDATX(), new FieldAttributes ("AD=OD"), Color.white,NEWLINE,new 
            TabSetting(50),"OMNI-ADAS MATCH PROCESS ERROR REPORT",new TabSetting(110),"Page No :",new TabSetting(120),getReports().getPageNumberDbs(0), 
            new FieldAttributes ("AD=OD"), Color.white, new ReportZeroPrint (true),NEWLINE,NEWLINE,new TabSetting(3),"  OMNI REC",new TabSetting(17),"  PIN  ",new 
            TabSetting(32),"  TIAA   ",new TabSetting(45),"EFFECTIVE",new TabSetting(55),"REQUEST",new TabSetting(64),"                 ERROR MESSAGES",NEWLINE,new 
            TabSetting(3),"   NUMBER",new TabSetting(32),"CONTRACT",new TabSetting(48),"  DATE",new TabSetting(55)," TYPE ",NEWLINE,NEWLINE);
        getReports().write(1, ReportOption.TRAILER,ReportTitleOptions.LEFTJUSTIFIED,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(35)," >>>> TOTAL CONTRACTS WITH ERRORS : ",
            pnd_Counters_Pnd_Total_Omni_Err_Cntrct,"   <<<<");
    }
}
