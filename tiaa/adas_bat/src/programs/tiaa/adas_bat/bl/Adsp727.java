/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:04:28 PM
**        * FROM NATURAL PROGRAM : Adsp727
************************************************************
**        * FILE NAME            : Adsp727.java
**        * CLASS NAME           : Adsp727
**        * INSTANCE NAME        : Adsp727
************************************************************
************************************************************************
* PROGRAM  : ADSP727
* GENERATED: APRIL 15, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : THIS MODULE EXTRACTS ALL THE ANNUITIZATION TRANSACTIONS
*            FROM THE KDO FILE FOR A GIVEN DATE.
*            KDO FILE FOR A GIVEN DATE.  SURVIVOR INFORMATION ONLY
*
* REMARKS  : CLONED FROM ADSP722  (ADAM SYSTEM)
*********************  MAINTENANCE LOG *********************************
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* 11/17/00  F TWAHIR   TPA AND IO CHANGES
* 04/15/04  C. AVE     MODIFIED FOR ADAS (ANNUITIZATION SUNGUARD)
* 07/27/04  T. SHINE     IF MONTH-END PROCESS FALLS ON WEEKEND OR
*                        HOLIDAY, USE ADS-RPT-13 (FACTOR DATE) AS
*                        REPORT DATE.
* 05/06/08   GUERRERO  RE-STOW TO PICKUP NEW ADSL450 LAYOUT.
*                      ADDED 2ND WORK FILE WRITE FOR ROTH ONLY.
* 03/23/10   D.E.ANDER ADABAS REPLATFORM PCPOP SURVIVOR INFORMATION ONLY
* 05/14/10   C.MASON   REPLACE READ PHYSICAL WITH READY BY SUPER DESC.
* 03/05/12   E. MELNIK RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                      UPDATED LINKAGE AREAS.
* 04/03/12  O. SOTTO  ADDITIONAL RATE BASE CHANGES.  SC 040312.
* 02/27/2017 R.CARREON PIN EXPANSION 02272017
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp727 extends BLNatBase
{
    // Data Areas
    private LdaAdsl450 ldaAdsl450;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ads_Cntl_View;

    private DbsGroup ads_Cntl_View_Ads_Cntl_Grp;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte;
    private DbsField ads_Cntl_View_Ads_Rpt_13;

    private DataAccessProgramView vw_ads_Prtcpnt_View;
    private DbsField ads_Prtcpnt_View_Rqst_Id;
    private DbsField ads_Prtcpnt_View_Adp_Alt_Dest_Rlvr_Dest;
    private DbsField pnd_Rlvr_Dest;
    private DbsField pnd_Start_Date;
    private DbsField pnd_Record_Count;
    private DbsField pnd_Record_Count_Roth;
    private DbsField pnd_Last_Activity_Date;

    private DbsGroup pnd_Last_Activity_Date__R_Field_1;
    private DbsField pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_2;
    private DbsField pnd_Date_A_Pnd_Date_N;

    private DataAccessProgramView vw_ads_Ia_Rsl2;
    private DbsField pnd_Max_Rslt;
    private DbsField pnd_Adi_Super1;

    private DbsGroup pnd_Adi_Super1__R_Field_3;
    private DbsField pnd_Adi_Super1_Pnd_Rqst_Id_Ia;
    private DbsField pnd_Adi_Super1_Pnd_Adi_Record_Type;
    private DbsField pnd_Adi_Super1_Pnd_Adi_Sqnce_Nbr;
    private DbsField pnd_Super4_Key;

    private DbsGroup pnd_Super4_Key__R_Field_4;
    private DbsField pnd_Super4_Key_Pnd_Rcrd_Cde;
    private DbsField pnd_Super4_Key_Pnd_Actvty_Date;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl450 = new LdaAdsl450();
        registerRecord(ldaAdsl450);
        registerRecord(ldaAdsl450.getVw_ads_Ia_Rslt_View());

        // Local Variables
        localVariables = new DbsRecord();

        vw_ads_Cntl_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntl_View", "ADS-CNTL-VIEW"), "ADS_CNTL", "ADS_CNTL");

        ads_Cntl_View_Ads_Cntl_Grp = vw_ads_Cntl_View.getRecord().newGroupInGroup("ADS_CNTL_VIEW_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_DTE");
        ads_Cntl_View_Ads_Rpt_13 = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Rpt_13", "ADS-RPT-13", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADS_RPT_13");
        registerRecord(vw_ads_Cntl_View);

        vw_ads_Prtcpnt_View = new DataAccessProgramView(new NameInfo("vw_ads_Prtcpnt_View", "ADS-PRTCPNT-VIEW"), "ADS_PRTCPNT", "ADS_PRTCPNT");
        ads_Prtcpnt_View_Rqst_Id = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "RQST_ID");
        ads_Prtcpnt_View_Adp_Alt_Dest_Rlvr_Dest = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Alt_Dest_Rlvr_Dest", "ADP-ALT-DEST-RLVR-DEST", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADP_ALT_DEST_RLVR_DEST");
        registerRecord(vw_ads_Prtcpnt_View);

        pnd_Rlvr_Dest = localVariables.newFieldInRecord("pnd_Rlvr_Dest", "#RLVR-DEST", FieldType.STRING, 4);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.NUMERIC, 8);
        pnd_Record_Count = localVariables.newFieldInRecord("pnd_Record_Count", "#RECORD-COUNT", FieldType.PACKED_DECIMAL, 8);
        pnd_Record_Count_Roth = localVariables.newFieldInRecord("pnd_Record_Count_Roth", "#RECORD-COUNT-ROTH", FieldType.PACKED_DECIMAL, 8);
        pnd_Last_Activity_Date = localVariables.newFieldInRecord("pnd_Last_Activity_Date", "#LAST-ACTIVITY-DATE", FieldType.STRING, 8);

        pnd_Last_Activity_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Last_Activity_Date__R_Field_1", "REDEFINE", pnd_Last_Activity_Date);
        pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N = pnd_Last_Activity_Date__R_Field_1.newFieldInGroup("pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N", 
            "#LAST-ACTIVITY-DATE-N", FieldType.NUMERIC, 8);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_2", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_2.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);

        vw_ads_Ia_Rsl2 = new DataAccessProgramView(new NameInfo("vw_ads_Ia_Rsl2", "ADS-IA-RSL2"), "ADS_IA_RSLT", "ADS_IA_RSLT");
        registerRecord(vw_ads_Ia_Rsl2);

        pnd_Max_Rslt = localVariables.newFieldInRecord("pnd_Max_Rslt", "#MAX-RSLT", FieldType.PACKED_DECIMAL, 3);
        pnd_Adi_Super1 = localVariables.newFieldInRecord("pnd_Adi_Super1", "#ADI-SUPER1", FieldType.STRING, 41);

        pnd_Adi_Super1__R_Field_3 = localVariables.newGroupInRecord("pnd_Adi_Super1__R_Field_3", "REDEFINE", pnd_Adi_Super1);
        pnd_Adi_Super1_Pnd_Rqst_Id_Ia = pnd_Adi_Super1__R_Field_3.newFieldInGroup("pnd_Adi_Super1_Pnd_Rqst_Id_Ia", "#RQST-ID-IA", FieldType.STRING, 35);
        pnd_Adi_Super1_Pnd_Adi_Record_Type = pnd_Adi_Super1__R_Field_3.newFieldInGroup("pnd_Adi_Super1_Pnd_Adi_Record_Type", "#ADI-RECORD-TYPE", FieldType.STRING, 
            3);
        pnd_Adi_Super1_Pnd_Adi_Sqnce_Nbr = pnd_Adi_Super1__R_Field_3.newFieldInGroup("pnd_Adi_Super1_Pnd_Adi_Sqnce_Nbr", "#ADI-SQNCE-NBR", FieldType.NUMERIC, 
            3);
        pnd_Super4_Key = localVariables.newFieldInRecord("pnd_Super4_Key", "#SUPER4-KEY", FieldType.STRING, 7);

        pnd_Super4_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Super4_Key__R_Field_4", "REDEFINE", pnd_Super4_Key);
        pnd_Super4_Key_Pnd_Rcrd_Cde = pnd_Super4_Key__R_Field_4.newFieldInGroup("pnd_Super4_Key_Pnd_Rcrd_Cde", "#RCRD-CDE", FieldType.STRING, 3);
        pnd_Super4_Key_Pnd_Actvty_Date = pnd_Super4_Key__R_Field_4.newFieldInGroup("pnd_Super4_Key_Pnd_Actvty_Date", "#ACTVTY-DATE", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Cntl_View.reset();
        vw_ads_Prtcpnt_View.reset();
        vw_ads_Ia_Rsl2.reset();

        ldaAdsl450.initializeValues();

        localVariables.reset();
        pnd_Max_Rslt.setInitialValue(125);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp727() throws Exception
    {
        super("Adsp727");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        vw_ads_Cntl_View.startDatabaseRead                                                                                                                                //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "READ01",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        READ01:
        while (condition(vw_ads_Cntl_View.readNextRow("READ01")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, "=",ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                                                      //Natural: WRITE '=' ADS-CNTL-BSNSS-DTE
        if (Global.isEscape()) return;
        getReports().write(0, "=",ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                                                      //Natural: WRITE '=' ADS-CNTL-BSNSS-DTE
        if (Global.isEscape()) return;
        if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                                     //Natural: IF ADS-RPT-13 NE 0
        {
            pnd_Date_A.setValueEdited(ads_Cntl_View_Ads_Rpt_13,new ReportEditMask("YYYYMMDD"));                                                                           //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE-A
            pnd_Start_Date.compute(new ComputeParameters(false, pnd_Start_Date), (pnd_Date_A_Pnd_Date_N.divide(100)).multiply(100).add(1));                               //Natural: COMPUTE #START-DATE = ( #DATE-N / 100 ) * 100 + 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Start_Date.compute(new ComputeParameters(false, pnd_Start_Date), (ads_Cntl_View_Ads_Cntl_Bsnss_Dte.divide(100)).multiply(100).add(1));                    //Natural: COMPUTE #START-DATE = ( ADS-CNTL-BSNSS-DTE / 100 ) * 100 + 1
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "START DATE -",pnd_Start_Date);                                                                                                             //Natural: WRITE 'START DATE -' #START-DATE
        if (Global.isEscape()) return;
        //*  CM - INITIALIZE SUPER4 KEY FIELDS
        getReports().write(0, "END DATE   -",ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                                           //Natural: WRITE 'END DATE   -' ADS-CNTL-BSNSS-DTE
        if (Global.isEscape()) return;
        //*  READ ADS-IA-RSLT-VIEW PHYSICAL
        pnd_Super4_Key_Pnd_Rcrd_Cde.setValue("PP ");                                                                                                                      //Natural: ASSIGN #RCRD-CDE := 'PP '
        pnd_Super4_Key_Pnd_Actvty_Date.reset();                                                                                                                           //Natural: RESET #ACTVTY-DATE
        ldaAdsl450.getVw_ads_Ia_Rslt_View().startDatabaseRead                                                                                                             //Natural: READ ADS-IA-RSLT-VIEW BY ADI-SUPER4 STARTING FROM #SUPER4-KEY
        (
        "READ02",
        new Wc[] { new Wc("ADI_SUPER4", ">=", pnd_Super4_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("ADI_SUPER4", "ASC") }
        );
        READ02:
        while (condition(ldaAdsl450.getVw_ads_Ia_Rslt_View().readNextRow("READ02")))
        {
            //*  CM - ESCAPE IF END OF 'PP ' RECS
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde().notEquals("PP ")))                                                                             //Natural: IF ADI-IA-RCRD-CDE NE 'PP '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*   ACCEPT IF ADI-IA-RCRD-CDE = 'PP '  /* CM - LIMTED TO 'PP'  BY SUPER
            //*     AND ADI-SQNCE-NBR GT 0
            //*  CM - REPLACE PREVIOUS ACCEPT LOGIC
            //*  DEA
            if (condition(!(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr().greater(getZero()) && ldaAdsl450.getAds_Ia_Rslt_View_Adi_Srvvr_Ind().equals("Y"))))            //Natural: ACCEPT IF ADI-SQNCE-NBR GT 0 AND ADI-SRVVR-IND = 'Y'
            {
                continue;
            }
            pnd_Last_Activity_Date.setValueEdited(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                                    //Natural: MOVE EDITED ADI-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #LAST-ACTIVITY-DATE
            if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                                 //Natural: IF ADS-RPT-13 NE 0
            {
                if (condition(pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N.greater(pnd_Date_A_Pnd_Date_N)))                                                            //Natural: IF #LAST-ACTIVITY-DATE-N > #DATE-N
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N.greater(ads_Cntl_View_Ads_Cntl_Bsnss_Dte)))                                                 //Natural: IF #LAST-ACTIVITY-DATE-N > ADS-CNTL-BSNSS-DTE
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N.less(pnd_Start_Date)))                                                                          //Natural: IF #LAST-ACTIVITY-DATE-N < #START-DATE
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            vw_ads_Prtcpnt_View.startDatabaseFind                                                                                                                         //Natural: FIND ADS-PRTCPNT-VIEW WITH ADS-PRTCPNT-VIEW.RQST-ID = ADS-IA-RSLT-VIEW.RQST-ID
            (
            "FIND01",
            new Wc[] { new Wc("RQST_ID", "=", ldaAdsl450.getAds_Ia_Rslt_View_Rqst_Id(), WcType.WITH) }
            );
            FIND01:
            while (condition(vw_ads_Prtcpnt_View.readNextRow("FIND01")))
            {
                vw_ads_Prtcpnt_View.setIfNotFoundControlFlag(false);
                pnd_Rlvr_Dest.setValue(ads_Prtcpnt_View_Adp_Alt_Dest_Rlvr_Dest);                                                                                          //Natural: ASSIGN #RLVR-DEST := ADP-ALT-DEST-RLVR-DEST
                getReports().display(0, " AA ",                                                                                                                           //Natural: DISPLAY ' AA ' ADS-PRTCPNT-VIEW.RQST-ID ' XX ' #RLVR-DEST
                		ads_Prtcpnt_View_Rqst_Id," XX ",
                		pnd_Rlvr_Dest);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Record_Count.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #RECORD-COUNT
            ldaAdsl450.getPnd_More().setValue("N");                                                                                                                       //Natural: ASSIGN #MORE := 'N'
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Count_Castadi_Dtl_Tiaa_Data().equals(pnd_Max_Rslt)))                                                             //Natural: IF ADS-IA-RSLT-VIEW.C*ADI-DTL-TIAA-DATA = #MAX-RSLT
            {
                pnd_Adi_Super1_Pnd_Rqst_Id_Ia.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Rqst_Id());                                                                         //Natural: ASSIGN #RQST-ID-IA := ADS-IA-RSLT-VIEW.RQST-ID
                pnd_Adi_Super1_Pnd_Adi_Record_Type.setValue("PP1");                                                                                                       //Natural: ASSIGN #ADI-RECORD-TYPE := 'PP1'
                pnd_Adi_Super1_Pnd_Adi_Sqnce_Nbr.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr());                                                                //Natural: ASSIGN #ADI-SQNCE-NBR := ADI-SQNCE-NBR
                vw_ads_Ia_Rsl2.startDatabaseFind                                                                                                                          //Natural: FIND ADS-IA-RSL2 WITH ADI-SUPER1 = #ADI-SUPER1
                (
                "FIND02",
                new Wc[] { new Wc("ADI_SUPER1", "=", pnd_Adi_Super1, WcType.WITH) }
                );
                FIND02:
                while (condition(vw_ads_Ia_Rsl2.readNextRow("FIND02")))
                {
                    vw_ads_Ia_Rsl2.setIfNotFoundControlFlag(false);
                    ldaAdsl450.getPnd_More().setValue("Y");                                                                                                               //Natural: ASSIGN #MORE := 'Y'
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  040312 END
            //*  DEA
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Roth_Rqst_Ind().equals("Y")))                                                                                //Natural: IF ADI-ROTH-RQST-IND = 'Y'
            {
                pnd_Record_Count_Roth.nadd(1);                                                                                                                            //Natural: ADD 1 TO #RECORD-COUNT-ROTH
                //*  040312
                getWorkFiles().write(2, false, ldaAdsl450.getVw_ads_Ia_Rslt_View(), pnd_Rlvr_Dest, ldaAdsl450.getPnd_More());                                             //Natural: WRITE WORK FILE 2 ADS-IA-RSLT-VIEW #RLVR-DEST #MORE
                ldaAdsl450.getAds_Ia_Rslt_View_Adi_Roth_Rqst_Ind().setValue(" ");                                                                                         //Natural: ASSIGN ADI-ROTH-RQST-IND := ' '
            }                                                                                                                                                             //Natural: END-IF
            //*  040312
            getWorkFiles().write(1, false, ldaAdsl450.getVw_ads_Ia_Rslt_View(), pnd_Rlvr_Dest, ldaAdsl450.getPnd_More());                                                 //Natural: WRITE WORK FILE 1 ADS-IA-RSLT-VIEW #RLVR-DEST #MORE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  WHEN NO SURV RECS ARE PRESENT, THIS NULL WRITE WILL SEND THE SURV
        //*  INDICATOR TO THE REPORT MODULE TO CREATE THE SURV REPORT.
        //*  GG050608
        if (condition(pnd_Record_Count_Roth.equals(getZero())))                                                                                                           //Natural: IF #RECORD-COUNT-ROTH = 0
        {
            //*  NULL VALUES
            //*  040312
            ldaAdsl450.getVw_ads_Ia_Rslt_View().reset();                                                                                                                  //Natural: RESET ADS-IA-RSLT-VIEW
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Roth_Rqst_Ind().setValue("Y");                                                                                             //Natural: ASSIGN ADI-ROTH-RQST-IND := 'Y'
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Srvvr_Ind().setValue("Y");                                                                                                 //Natural: ASSIGN ADI-SRVVR-IND := 'Y'
            ldaAdsl450.getPnd_More().setValue("N");                                                                                                                       //Natural: ASSIGN #MORE := 'N'
            //*  040312
            getWorkFiles().write(2, false, ldaAdsl450.getVw_ads_Ia_Rslt_View(), pnd_Rlvr_Dest, ldaAdsl450.getPnd_More());                                                 //Natural: WRITE WORK FILE 2 ADS-IA-RSLT-VIEW #RLVR-DEST #MORE
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, NEWLINE,"TOTAL IA RESULT RECORDS WRITTEN TO OUTPUT -",pnd_Record_Count);                                                                    //Natural: WRITE / 'TOTAL IA RESULT RECORDS WRITTEN TO OUTPUT -' #RECORD-COUNT
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"TOT SURV IA RESULT RECS WRITTEN TO OUTPUT -",pnd_Record_Count_Roth);                                                               //Natural: WRITE / 'TOT SURV IA RESULT RECS WRITTEN TO OUTPUT -' #RECORD-COUNT-ROTH
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        getReports().setDisplayColumns(0, " AA ",
        		ads_Prtcpnt_View_Rqst_Id," XX ",
        		pnd_Rlvr_Dest);
    }
}
