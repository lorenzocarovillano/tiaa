/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:03:03 PM
**        * FROM NATURAL PROGRAM : Adsp402
************************************************************
**        * FILE NAME            : Adsp402.java
**        * CLASS NAME           : Adsp402
**        * INSTANCE NAME        : Adsp402
************************************************************
***********************************************************************
* PROGRAM  : ADSP401
* SYSTEM   : ADAS - ANNUITIZATION SUNGUARD
* GENERATED: MARCH 2015
* PURPOSE  : THIS PROGRAM GENERATES THE CTLS FILE FOR ALL REQUESTS
*          : PROCESSED BY PAN2005D FOR CURRENT BUSINESS DAY.
*
*********************  MAINTENANCE LOG ******************************
*
*  D A T E   PROGRAMMER     D E S C R I P T I O N
* 5/07/2015  E. MELNIK   BENE IN THE PLAN CHANGES.
*                        MARKED BY EM - 050715.
* 2/22/2017R.CARREON   PIN EXPANSION 02222017
* 2/22/2019 E. MELNIK  RESTOWED FOR UPDATED ADSA401 WITH IRC CDE.
*********************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp402 extends BLNatBase
{
    // Data Areas
    private LdaAdsl401p ldaAdsl401p;
    private LdaAdsl401a ldaAdsl401a;
    private PdaAdsa401 pdaAdsa401;
    private PdaNeca4000 pdaNeca4000;
    private PdaAdspda_M pdaAdspda_M;
    private PdaAdsa888 pdaAdsa888;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ads_Cntl_View;
    private DbsField ads_Cntl_View_Ads_Rcrd_Cde;

    private DbsGroup ads_Cntl_View_Ads_Cntl_Grp;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte;

    private DbsGroup ads_Cntl_View__R_Field_1;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A;

    private DbsGroup pnd_Constant_Variables;
    private DbsField pnd_Constant_Variables_Pnd_Open_Ind;
    private DbsField pnd_Constant_Variables_Pnd_Successful;
    private DbsField pnd_Constant_Variables_Pnd_Batchads;
    private DbsField pnd_Constant_Variables_Pnd_Max_Cntrct;
    private DbsField pnd_Constant_Variables_Pnd_Database_Error;

    private DbsGroup pnd_Keys;
    private DbsField pnd_Keys_Pnd_Ads_Cntl_Super_De_1;

    private DbsGroup pnd_Keys__R_Field_2;
    private DbsField pnd_Keys_Pnd_Ads_Cntl_Rcrd_Typ;
    private DbsField pnd_Keys_Pnd_Ads_Cntl_Bsnss_Dt;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_Cntrct_Cnt;
    private DbsField pnd_Counters_Pnd_I;
    private DbsField pnd_Counters_Pnd_P;

    private DbsGroup pnd_Adsn998_Error_Handler;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Calling_Program;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Unique_Id;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Step_Name;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Appl_Id;
    private DbsField pnd_Adsn998_Error_Handler_Pnd_Error_Msg;

    private DbsGroup pnd_Get_Lob;
    private DbsField pnd_Get_Lob_Pnd_Lob_Cntrct;
    private DbsField pnd_Get_Lob_Pnd_Lob;
    private DbsField pnd_Get_Lob_Pnd_Sub_Lob;

    private DbsGroup pnd_Misc_Variables;
    private DbsField pnd_Misc_Variables_Pnd_Terminate_No;
    private DbsField pnd_Misc_Variables_Pnd_Terminate_Msg;

    private DbsGroup pnd_Misc_Variables__R_Field_3;
    private DbsField pnd_Misc_Variables_Pnd_Terminate_Msg1;
    private DbsField pnd_Misc_Variables_Pnd_Terminate_Msg2;
    private DbsField pnd_Misc_Variables_Pnd_Process_Step;
    private DbsField pnd_Misc_Variables_Pnd_Display_Adsa401;
    private DbsField pnd_Misc_Variables_Pnd_L;
    private DbsField pnd_Misc_Variables_Pnd_Wk_Cntrct_Lob;
    private DbsField pnd_Misc_Variables_Pnd_Cntl_Bsnss_Date;
    private DbsField pnd_Misc_Variables_Pnd_Bene_Accptnce_Ind;
    private DbsField pls_Debug_On;
    private DbsField pls_Tot_System_Errors;
    private DbsField pls_Tot_Prtcpnt_Rqst;
    private DbsField pls_Tot_Cntrct_Recs;
    private DbsField pls_Tot_Rqst_Errors;
    private DbsField pls_Tot_Rqst_Successful;
    private DbsField pls_Ticker;
    private DbsField pls_Invstmnt_Grpng_Id;
    private DbsField pls_Ctls_Sqnce_Nbr;
    private DbsField pls_Ctls_Trlr_Rcrd;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl401p = new LdaAdsl401p();
        registerRecord(ldaAdsl401p);
        registerRecord(ldaAdsl401p.getVw_ads_Prtcpnt_View());
        ldaAdsl401a = new LdaAdsl401a();
        registerRecord(ldaAdsl401a);
        registerRecord(ldaAdsl401a.getVw_ads_Cntrct_View());
        localVariables = new DbsRecord();
        pdaAdsa401 = new PdaAdsa401(localVariables);
        pdaNeca4000 = new PdaNeca4000(localVariables);
        pdaAdspda_M = new PdaAdspda_M(localVariables);
        pdaAdsa888 = new PdaAdsa888(localVariables);

        // Local Variables

        vw_ads_Cntl_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntl_View", "ADS-CNTL-VIEW"), "ADS_CNTL", "ADS_CNTL");
        ads_Cntl_View_Ads_Rcrd_Cde = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Rcrd_Cde", "ADS-RCRD-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ADS_RCRD_CDE");

        ads_Cntl_View_Ads_Cntl_Grp = vw_ads_Cntl_View.getRecord().newGroupInGroup("ADS_CNTL_VIEW_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_DTE");

        ads_Cntl_View__R_Field_1 = ads_Cntl_View_Ads_Cntl_Grp.newGroupInGroup("ads_Cntl_View__R_Field_1", "REDEFINE", ads_Cntl_View_Ads_Cntl_Bsnss_Dte);
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A = ads_Cntl_View__R_Field_1.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A", "ADS-CNTL-BSNSS-DTE-A", FieldType.STRING, 
            8);
        registerRecord(vw_ads_Cntl_View);

        pnd_Constant_Variables = localVariables.newGroupInRecord("pnd_Constant_Variables", "#CONSTANT-VARIABLES");
        pnd_Constant_Variables_Pnd_Open_Ind = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Open_Ind", "#OPEN-IND", FieldType.STRING, 
            1);
        pnd_Constant_Variables_Pnd_Successful = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Successful", "#SUCCESSFUL", FieldType.STRING, 
            1);
        pnd_Constant_Variables_Pnd_Batchads = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Batchads", "#BATCHADS", FieldType.STRING, 
            8);
        pnd_Constant_Variables_Pnd_Max_Cntrct = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Max_Cntrct", "#MAX-CNTRCT", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Constant_Variables_Pnd_Database_Error = pnd_Constant_Variables.newFieldArrayInGroup("pnd_Constant_Variables_Pnd_Database_Error", "#DATABASE-ERROR", 
            FieldType.NUMERIC, 7, new DbsArrayController(1, 2));

        pnd_Keys = localVariables.newGroupInRecord("pnd_Keys", "#KEYS");
        pnd_Keys_Pnd_Ads_Cntl_Super_De_1 = pnd_Keys.newFieldInGroup("pnd_Keys_Pnd_Ads_Cntl_Super_De_1", "#ADS-CNTL-SUPER-DE-1", FieldType.STRING, 9);

        pnd_Keys__R_Field_2 = pnd_Keys.newGroupInGroup("pnd_Keys__R_Field_2", "REDEFINE", pnd_Keys_Pnd_Ads_Cntl_Super_De_1);
        pnd_Keys_Pnd_Ads_Cntl_Rcrd_Typ = pnd_Keys__R_Field_2.newFieldInGroup("pnd_Keys_Pnd_Ads_Cntl_Rcrd_Typ", "#ADS-CNTL-RCRD-TYP", FieldType.STRING, 
            1);
        pnd_Keys_Pnd_Ads_Cntl_Bsnss_Dt = pnd_Keys__R_Field_2.newFieldInGroup("pnd_Keys_Pnd_Ads_Cntl_Bsnss_Dt", "#ADS-CNTL-BSNSS-DT", FieldType.NUMERIC, 
            8);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_Cntrct_Cnt = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Cntrct_Cnt", "#CNTRCT-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Counters_Pnd_I = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Counters_Pnd_P = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_P", "#P", FieldType.PACKED_DECIMAL, 3);

        pnd_Adsn998_Error_Handler = localVariables.newGroupInRecord("pnd_Adsn998_Error_Handler", "#ADSN998-ERROR-HANDLER");
        pnd_Adsn998_Error_Handler_Pnd_Calling_Program = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Calling_Program", "#CALLING-PROGRAM", 
            FieldType.STRING, 8);
        pnd_Adsn998_Error_Handler_Pnd_Unique_Id = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Unique_Id", "#UNIQUE-ID", FieldType.NUMERIC, 
            12);
        pnd_Adsn998_Error_Handler_Pnd_Step_Name = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Step_Name", "#STEP-NAME", FieldType.STRING, 
            8);
        pnd_Adsn998_Error_Handler_Pnd_Appl_Id = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Appl_Id", "#APPL-ID", FieldType.STRING, 
            8);
        pnd_Adsn998_Error_Handler_Pnd_Error_Msg = pnd_Adsn998_Error_Handler.newFieldInGroup("pnd_Adsn998_Error_Handler_Pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 
            79);

        pnd_Get_Lob = localVariables.newGroupInRecord("pnd_Get_Lob", "#GET-LOB");
        pnd_Get_Lob_Pnd_Lob_Cntrct = pnd_Get_Lob.newFieldInGroup("pnd_Get_Lob_Pnd_Lob_Cntrct", "#LOB-CNTRCT", FieldType.STRING, 10);
        pnd_Get_Lob_Pnd_Lob = pnd_Get_Lob.newFieldInGroup("pnd_Get_Lob_Pnd_Lob", "#LOB", FieldType.STRING, 10);
        pnd_Get_Lob_Pnd_Sub_Lob = pnd_Get_Lob.newFieldInGroup("pnd_Get_Lob_Pnd_Sub_Lob", "#SUB-LOB", FieldType.STRING, 12);

        pnd_Misc_Variables = localVariables.newGroupInRecord("pnd_Misc_Variables", "#MISC-VARIABLES");
        pnd_Misc_Variables_Pnd_Terminate_No = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Terminate_No", "#TERMINATE-NO", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Misc_Variables_Pnd_Terminate_Msg = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Terminate_Msg", "#TERMINATE-MSG", FieldType.STRING, 
            118);

        pnd_Misc_Variables__R_Field_3 = pnd_Misc_Variables.newGroupInGroup("pnd_Misc_Variables__R_Field_3", "REDEFINE", pnd_Misc_Variables_Pnd_Terminate_Msg);
        pnd_Misc_Variables_Pnd_Terminate_Msg1 = pnd_Misc_Variables__R_Field_3.newFieldInGroup("pnd_Misc_Variables_Pnd_Terminate_Msg1", "#TERMINATE-MSG1", 
            FieldType.STRING, 59);
        pnd_Misc_Variables_Pnd_Terminate_Msg2 = pnd_Misc_Variables__R_Field_3.newFieldInGroup("pnd_Misc_Variables_Pnd_Terminate_Msg2", "#TERMINATE-MSG2", 
            FieldType.STRING, 59);
        pnd_Misc_Variables_Pnd_Process_Step = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Process_Step", "#PROCESS-STEP", FieldType.STRING, 
            35);
        pnd_Misc_Variables_Pnd_Display_Adsa401 = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Display_Adsa401", "#DISPLAY-ADSA401", FieldType.BOOLEAN, 
            1);
        pnd_Misc_Variables_Pnd_L = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_L", "#L", FieldType.PACKED_DECIMAL, 3);
        pnd_Misc_Variables_Pnd_Wk_Cntrct_Lob = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Wk_Cntrct_Lob", "#WK-CNTRCT-LOB", FieldType.STRING, 
            10);
        pnd_Misc_Variables_Pnd_Cntl_Bsnss_Date = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Cntl_Bsnss_Date", "#CNTL-BSNSS-DATE", FieldType.DATE);
        pnd_Misc_Variables_Pnd_Bene_Accptnce_Ind = pnd_Misc_Variables.newFieldInGroup("pnd_Misc_Variables_Pnd_Bene_Accptnce_Ind", "#BENE-ACCPTNCE-IND", 
            FieldType.NUMERIC, 1);
        pls_Debug_On = WsIndependent.getInstance().newFieldInRecord("pls_Debug_On", "+DEBUG-ON", FieldType.BOOLEAN, 1);
        pls_Tot_System_Errors = WsIndependent.getInstance().newFieldInRecord("pls_Tot_System_Errors", "+TOT-SYSTEM-ERRORS", FieldType.PACKED_DECIMAL, 8);
        pls_Tot_Prtcpnt_Rqst = WsIndependent.getInstance().newFieldInRecord("pls_Tot_Prtcpnt_Rqst", "+TOT-PRTCPNT-RQST", FieldType.PACKED_DECIMAL, 8);
        pls_Tot_Cntrct_Recs = WsIndependent.getInstance().newFieldInRecord("pls_Tot_Cntrct_Recs", "+TOT-CNTRCT-RECS", FieldType.PACKED_DECIMAL, 8);
        pls_Tot_Rqst_Errors = WsIndependent.getInstance().newFieldInRecord("pls_Tot_Rqst_Errors", "+TOT-RQST-ERRORS", FieldType.PACKED_DECIMAL, 8);
        pls_Tot_Rqst_Successful = WsIndependent.getInstance().newFieldInRecord("pls_Tot_Rqst_Successful", "+TOT-RQST-SUCCESSFUL", FieldType.PACKED_DECIMAL, 
            8);
        pls_Ticker = WsIndependent.getInstance().newFieldArrayInRecord("pls_Ticker", "+TICKER", FieldType.STRING, 10, new DbsArrayController(1, 100));
        pls_Invstmnt_Grpng_Id = WsIndependent.getInstance().newFieldArrayInRecord("pls_Invstmnt_Grpng_Id", "+INVSTMNT-GRPNG-ID", FieldType.STRING, 4, 
            new DbsArrayController(1, 100));
        pls_Ctls_Sqnce_Nbr = WsIndependent.getInstance().newFieldInRecord("pls_Ctls_Sqnce_Nbr", "+CTLS-SQNCE-NBR", FieldType.NUMERIC, 9);
        pls_Ctls_Trlr_Rcrd = WsIndependent.getInstance().newFieldInRecord("pls_Ctls_Trlr_Rcrd", "+CTLS-TRLR-RCRD", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Cntl_View.reset();

        ldaAdsl401p.initializeValues();
        ldaAdsl401a.initializeValues();

        localVariables.reset();
        pnd_Constant_Variables_Pnd_Open_Ind.setInitialValue("O");
        pnd_Constant_Variables_Pnd_Successful.setInitialValue("T");
        pnd_Constant_Variables_Pnd_Batchads.setInitialValue("BATCHADS");
        pnd_Constant_Variables_Pnd_Max_Cntrct.setInitialValue(12);
        pnd_Constant_Variables_Pnd_Database_Error.getValue(1).setInitialValue(3017);
        pnd_Constant_Variables_Pnd_Database_Error.getValue(2).setInitialValue(3018);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp402() throws Exception
    {
        super("Adsp402");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Adsp402|Main");
        OnErrorManager.pushEvent("ADSP402", onError);
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT LS = 120 PS = 100
                //*  >> GET STATIC VARIABLE INFO TO BE USED LATER IN THE PROCESS <<
                //*  --------------------------------------------------------------
                //*  >>> TO RUN JOBS WITH DEBUG DISPLAYS, SET DEBUG-ON PARM IN JCL  <<<
                //*  ==================================================================
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pls_Debug_On);                                                                                         //Natural: INPUT +DEBUG-ON
                //*  ==================================================================
                                                                                                                                                                          //Natural: PERFORM READ-CONTROL-REC
                sub_Read_Control_Rec();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-TOTAL-FUND-COUNT
                sub_Get_Total_Fund_Count();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pls_Ctls_Sqnce_Nbr.setValue(100000000);                                                                                                                   //Natural: ASSIGN +CTLS-SQNCE-NBR := 100000000
                pls_Ctls_Trlr_Rcrd.reset();                                                                                                                               //Natural: RESET +CTLS-TRLR-RCRD
                pdaAdsa401.getPnd_Adsa401_Pnd_Cics_System_Name().setValue("BATCH");                                                                                       //Natural: MOVE 'BATCH' TO #ADSA401.#CICS-SYSTEM-NAME
                pdaAdsa401.getPnd_Adsa401_Pnd_Fund_Cnt().setValue(20);                                                                                                    //Natural: MOVE 20 TO #ADSA401.#FUND-CNT
                pdaAdsa401.getPnd_Adsa401_Pnd_Cntl_Bsnss_Dte().setValue(pnd_Misc_Variables_Pnd_Cntl_Bsnss_Date);                                                          //Natural: MOVE #CNTL-BSNSS-DATE TO #ADSA401.#CNTL-BSNSS-DTE
                if (condition(pls_Debug_On.getBoolean()))                                                                                                                 //Natural: IF +DEBUG-ON
                {
                    getReports().write(0, "*************** STARTING PROCESS ****************");                                                                           //Natural: WRITE '*************** STARTING PROCESS ****************'
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",pdaAdsa401.getPnd_Adsa401_Pnd_Cntl_Bsnss_Dte());                                                                            //Natural: WRITE '=' #ADSA401.#CNTL-BSNSS-DTE
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //* *********************************************************************
                //*          >> MAIN READ LOOP (READ ADS PARTICIPANT FILE) <<
                //* *********************************************************************
                if (condition(pls_Debug_On.getBoolean()))                                                                                                                 //Natural: IF +DEBUG-ON
                {
                    getReports().write(0, ">>> BEFORE MAIN READ LOOP",NEWLINE,"*",new RepeatItem(100));                                                                   //Natural: WRITE '>>> BEFORE MAIN READ LOOP' / '*' ( 100 )
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("MAINREAD");                                                                                             //Natural: ASSIGN #STEP-NAME := 'MAINREAD'
                ldaAdsl401p.getVw_ads_Prtcpnt_View().startDatabaseRead                                                                                                    //Natural: READ ADS-PRTCPNT-VIEW BY ADP-LST-ACTVTY-DTE = #CNTL-BSNSS-DATE
                (
                "READ_PRTCPNT",
                new Wc[] { new Wc("ADP_LST_ACTVTY_DTE", ">=", pnd_Misc_Variables_Pnd_Cntl_Bsnss_Date, WcType.BY) },
                new Oc[] { new Oc("ADP_LST_ACTVTY_DTE", "ASC") }
                );
                READ_PRTCPNT:
                while (condition(ldaAdsl401p.getVw_ads_Prtcpnt_View().readNextRow("READ_PRTCPNT")))
                {
                    if (condition(ldaAdsl401p.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte().notEquals(pnd_Misc_Variables_Pnd_Cntl_Bsnss_Date)))                                //Natural: IF ADS-PRTCPNT-VIEW.ADP-LST-ACTVTY-DTE NE #CNTL-BSNSS-DATE
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(!((ldaAdsl401p.getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind().equals(pnd_Constant_Variables_Pnd_Open_Ind)) && (ldaAdsl401p.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1, //Natural: ACCEPT IF ( ADS-PRTCPNT-VIEW.ADP-OPN-CLSD-IND EQ #OPEN-IND ) AND ( SUBSTRING ( ADS-PRTCPNT-VIEW.ADP-STTS-CDE,1,1 ) EQ #SUCCESSFUL )
                        1).equals(pnd_Constant_Variables_Pnd_Successful)))))
                    {
                        continue;
                    }
                    getReports().write(0, "*",new RepeatItem(100),NEWLINE);                                                                                               //Natural: WRITE '*' ( 100 ) /
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ">>>> NEW REQUEST <<<<",NEWLINE,"STATUS : ",ldaAdsl401p.getAds_Prtcpnt_View_Adp_Stts_Cde(),"RQST-ID :",ldaAdsl401p.getAds_Prtcpnt_View_Rqst_Id(),"PIN : ",ldaAdsl401p.getAds_Prtcpnt_View_Adp_Unique_Id(),NEWLINE,"EFF-DATE : ",ldaAdsl401p.getAds_Prtcpnt_View_Adp_Effctv_Dte(),  //Natural: WRITE '>>>> NEW REQUEST <<<<' / 'STATUS : ' ADS-PRTCPNT-VIEW.ADP-STTS-CDE 'RQST-ID :' ADS-PRTCPNT-VIEW.RQST-ID 'PIN : ' ADS-PRTCPNT-VIEW.ADP-UNIQUE-ID / 'EFF-DATE : ' ADS-PRTCPNT-VIEW.ADP-EFFCTV-DTE ( EM = YYYYMMDD ) 'ANNTY-OPTN : ' ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN
                        new ReportEditMask ("YYYYMMDD"),"ANNTY-OPTN : ",ldaAdsl401p.getAds_Prtcpnt_View_Adp_Annty_Optn());
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, "*",new RepeatItem(100));                                                                                                       //Natural: WRITE '*' ( 100 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*     >> RESET VARIABLES FOR EVERY NEW PARTICIPANT <<
                    //*     -----------------------------------------------
                    //*  EM 050715
                    pdaAdsa401.getPnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd().reset();                                                                                         //Natural: RESET #ADSA401-PRTCPNT-RCRD #ADSA401-CNTRCT-RCRD #ADSA401-VARIABLES #COUNTERS #BENE-ACCPTNCE-IND
                    pdaAdsa401.getPnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd().reset();
                    pdaAdsa401.getPnd_Adsa401_Pnd_Adsa401_Variables().reset();
                    pnd_Counters.reset();
                    pnd_Misc_Variables_Pnd_Bene_Accptnce_Ind.reset();
                    pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Prtcpnt_Isn().setValue(ldaAdsl401p.getVw_ads_Prtcpnt_View().getAstISN("READ_PRTCPNT"));                             //Natural: MOVE *ISN ( READ-PRTCPNT. ) TO #ADSA401.#CUR-PRTCPNT-ISN
                    pls_Tot_Prtcpnt_Rqst.nadd(1);                                                                                                                         //Natural: ADD 1 TO +TOT-PRTCPNT-RQST
                    pdaAdsa401.getPnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd().setValuesByName(ldaAdsl401p.getVw_ads_Prtcpnt_View());                                           //Natural: MOVE BY NAME ADS-PRTCPNT-VIEW TO #ADSA401-PRTCPNT-RCRD
                    //*     >> RETRIEVE IA CONTRACT LOB/SUB-LOB  <<
                    //*     ---------------------------------------
                    pnd_Get_Lob.reset();                                                                                                                                  //Natural: RESET #GET-LOB
                    pnd_Get_Lob_Pnd_Lob_Cntrct.setValue(pdaAdsa401.getPnd_Adsa401_Adp_Ia_Tiaa_Nbr());                                                                     //Natural: MOVE #ADSA401.ADP-IA-TIAA-NBR TO #LOB-CNTRCT
                                                                                                                                                                          //Natural: PERFORM GET-CNTRCT-LOB
                    sub_Get_Cntrct_Lob();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    pdaAdsa401.getPnd_Adsa401_Pnd_Ia_Cntrct_Lob().setValue(pnd_Get_Lob_Pnd_Lob);                                                                          //Natural: MOVE #LOB TO #ADSA401.#IA-CNTRCT-LOB
                    pdaAdsa401.getPnd_Adsa401_Pnd_Ia_Cntrct_Sub_Lob().setValue(pnd_Get_Lob_Pnd_Sub_Lob);                                                                  //Natural: MOVE #SUB-LOB TO #ADSA401.#IA-CNTRCT-SUB-LOB
                    //* * EM - 050715
                    pnd_Misc_Variables_Pnd_Bene_Accptnce_Ind.setValue(ldaAdsl401p.getAds_Prtcpnt_View_Adp_Bene_Accptnce_Ind());                                           //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-BENE-ACCPTNCE-IND TO #BENE-ACCPTNCE-IND
                    //*     >> FIND MATCHING CONTRACT RECORD IN ADS-CNTRCT FILE <<
                    //*     --------------------------------------------------------
                    //*  08312005 MN
                    pnd_Misc_Variables_Pnd_L.reset();                                                                                                                     //Natural: RESET #L
                    pnd_Misc_Variables_Pnd_Process_Step.setValue("FIND ADS CNTRCT");                                                                                      //Natural: MOVE 'FIND ADS CNTRCT' TO #PROCESS-STEP
                    pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("FINDCNTR");                                                                                         //Natural: ASSIGN #STEP-NAME := 'FINDCNTR'
                    ldaAdsl401a.getVw_ads_Cntrct_View().startDatabaseFind                                                                                                 //Natural: FIND ADS-CNTRCT-VIEW WITH ADS-CNTRCT-VIEW.RQST-ID = #ADSA401.RQST-ID
                    (
                    "FIND_CNTRCT",
                    new Wc[] { new Wc("RQST_ID", "=", pdaAdsa401.getPnd_Adsa401_Rqst_Id(), WcType.WITH) }
                    );
                    FIND_CNTRCT:
                    while (condition(ldaAdsl401a.getVw_ads_Cntrct_View().readNextRow("FIND_CNTRCT", true)))
                    {
                        ldaAdsl401a.getVw_ads_Cntrct_View().setIfNotFoundControlFlag(false);
                        if (condition(ldaAdsl401a.getVw_ads_Cntrct_View().getAstCOUNTER().equals(0)))                                                                     //Natural: IF NO RECORDS FOUND
                        {
                            pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L03");                                                                                 //Natural: MOVE 'L03' TO #ERROR-STATUS
                            pls_Tot_Rqst_Errors.nadd(1);                                                                                                                  //Natural: ADD 1 TO +TOT-RQST-ERRORS
                                                                                                                                                                          //Natural: PERFORM DISPLAY-#ADSA401
                            sub_Display_Pnd_Adsa401();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("FIND_CNTRCT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("FIND_CNTRCT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            pnd_Misc_Variables_Pnd_Terminate_No.setValue(98);                                                                                             //Natural: MOVE 98 TO #TERMINATE-NO
                            pnd_Misc_Variables_Pnd_Terminate_Msg.setValue(DbsUtil.compress("ERROR ", pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status(), "IN ADSP402",          //Natural: COMPRESS 'ERROR ' #ERROR-STATUS 'IN ADSP402' 'FOR PRTCPNT RQST-ID : ' ADS-PRTCPNT-VIEW.RQST-ID TO #TERMINATE-MSG
                                "FOR PRTCPNT RQST-ID : ", ldaAdsl401p.getAds_Prtcpnt_View_Rqst_Id()));
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESS
                            sub_Terminate_Process();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("FIND_CNTRCT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("FIND_CNTRCT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            //*      ESCAPE BOTTOM (1680)
                        }                                                                                                                                                 //Natural: END-NOREC
                        pls_Tot_Cntrct_Recs.nadd(1);                                                                                                                      //Natural: ADD 1 TO +TOT-CNTRCT-RECS
                        pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Cntrct_Isn().setValue(ldaAdsl401a.getVw_ads_Cntrct_View().getAstISN("FIND_CNTRCT"));                            //Natural: MOVE *ISN ( FIND-CNTRCT. ) TO #ADSA401.#CUR-CNTRCT-ISN
                        pdaAdsa401.getPnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd().setValuesByName(ldaAdsl401a.getVw_ads_Cntrct_View());                                         //Natural: MOVE BY NAME ADS-CNTRCT-VIEW TO #ADSA401-CNTRCT-RCRD
                        pdaAdsa401.getPnd_Adsa401_Adc_Rqst_Id().setValue(ldaAdsl401a.getAds_Cntrct_View_Rqst_Id());                                                       //Natural: MOVE ADS-CNTRCT-VIEW.RQST-ID TO #ADSA401.ADC-RQST-ID
                        //*          >>> RETRIEVE DA CONTRACT LOB/SUB-LOB  <<<
                        //*          -----------------------------------------
                        pnd_Get_Lob_Pnd_Lob_Cntrct.setValue(pdaAdsa401.getPnd_Adsa401_Adc_Tiaa_Nbr());                                                                    //Natural: MOVE #ADSA401.ADC-TIAA-NBR TO #LOB-CNTRCT
                                                                                                                                                                          //Natural: PERFORM GET-CNTRCT-LOB
                        sub_Get_Cntrct_Lob();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FIND_CNTRCT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FIND_CNTRCT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Lob().setValue(pnd_Get_Lob_Pnd_Lob);                                                                         //Natural: MOVE #LOB TO #ADSA401.#CNTRCT-LOB
                        pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Sub_Lob().setValue(pnd_Get_Lob_Pnd_Sub_Lob);                                                                 //Natural: MOVE #SUB-LOB TO #ADSA401.#CNTRCT-SUB-LOB
                        //*          >>> CTLS INTERFACE <<<
                        //*          ----------------------
                        pnd_Misc_Variables_Pnd_Process_Step.setValue("CTLS INTERFACE-ADSN430");                                                                           //Natural: MOVE 'CTLS INTERFACE-ADSN430' TO #PROCESS-STEP
                        pnd_Adsn998_Error_Handler_Pnd_Calling_Program.setValue("ADSN430");                                                                                //Natural: ASSIGN #CALLING-PROGRAM := 'ADSN430'
                        pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("CALLCTLS");                                                                                     //Natural: ASSIGN #STEP-NAME := 'CALLCTLS'
                        if (condition(pls_Debug_On.getBoolean()))                                                                                                         //Natural: IF +DEBUG-ON
                        {
                            pnd_Misc_Variables_Pnd_Display_Adsa401.setValue(true);                                                                                        //Natural: MOVE TRUE TO #DISPLAY-ADSA401
                        }                                                                                                                                                 //Natural: END-IF
                        getReports().write(0, "CALL TO ADSN430");                                                                                                         //Natural: WRITE 'CALL TO ADSN430'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("FIND_CNTRCT"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("FIND_CNTRCT"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  EM - 050715
                        DbsUtil.callnat(Adsn430.class , getCurrentProcessState(), pdaAdsa401.getPnd_Adsa401(), pnd_Misc_Variables_Pnd_Bene_Accptnce_Ind);                 //Natural: CALLNAT 'ADSN430' #ADSA401 #BENE-ACCPTNCE-IND
                        if (condition(Global.isEscape())) return;
                        if (condition(pls_Debug_On.getBoolean()))                                                                                                         //Natural: IF +DEBUG-ON
                        {
                            pnd_Misc_Variables_Pnd_Display_Adsa401.reset();                                                                                               //Natural: RESET #DISPLAY-ADSA401
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().notEquals(" ")))                                                                       //Natural: IF #ERROR-STATUS NE ' '
                        {
                            pls_Tot_Rqst_Errors.nadd(1);                                                                                                                  //Natural: ADD 1 TO +TOT-RQST-ERRORS
                                                                                                                                                                          //Natural: PERFORM DISPLAY-#ADSA401
                            sub_Display_Pnd_Adsa401();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("FIND_CNTRCT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("FIND_CNTRCT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            pnd_Misc_Variables_Pnd_Terminate_No.setValue(93);                                                                                             //Natural: MOVE 93 TO #TERMINATE-NO
                            pnd_Misc_Variables_Pnd_Terminate_Msg.setValue(DbsUtil.compress("ERROR ", pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status(), "IN",                  //Natural: COMPRESS 'ERROR ' #ERROR-STATUS 'IN' #CALLING-PROGRAM 'FOR PRTCPNT RQST-ID : ' ADS-PRTCPNT-VIEW.RQST-ID TO #TERMINATE-MSG
                                pnd_Adsn998_Error_Handler_Pnd_Calling_Program, "FOR PRTCPNT RQST-ID : ", ldaAdsl401p.getAds_Prtcpnt_View_Rqst_Id()));
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESS
                            sub_Terminate_Process();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("FIND_CNTRCT"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("FIND_CNTRCT"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            //*      ESCAPE BOTTOM (1680)
                        }                                                                                                                                                 //Natural: END-IF
                        //*  END OF CONTRACT LOOP
                    }                                                                                                                                                     //Natural: END-FIND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRTCPNT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  END PARTICIPANT READ
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(pls_Tot_Cntrct_Recs.greater(getZero())))                                                                                                    //Natural: IF +TOT-CNTRCT-RECS > 0
                {
                    pls_Ctls_Trlr_Rcrd.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO +CTLS-TRLR-RCRD
                    //*          >>> CTLS INTERFACE TO WRITE TRAILER RECORD <<<
                    //*          ----------------------
                    pnd_Misc_Variables_Pnd_Process_Step.setValue("CTLS INTERFACE TRLR RCRD-ADSN430");                                                                     //Natural: MOVE 'CTLS INTERFACE TRLR RCRD-ADSN430' TO #PROCESS-STEP
                    pnd_Adsn998_Error_Handler_Pnd_Calling_Program.setValue("ADSN430");                                                                                    //Natural: ASSIGN #CALLING-PROGRAM := 'ADSN430'
                    pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("CALLCTLS");                                                                                         //Natural: ASSIGN #STEP-NAME := 'CALLCTLS'
                    getReports().write(0, "CALL TO ADSN430 FOR TRAILER RECORD");                                                                                          //Natural: WRITE 'CALL TO ADSN430 FOR TRAILER RECORD'
                    if (Global.isEscape()) return;
                    //*  EM - 050715
                    DbsUtil.callnat(Adsn430.class , getCurrentProcessState(), pdaAdsa401.getPnd_Adsa401(), pnd_Misc_Variables_Pnd_Bene_Accptnce_Ind);                     //Natural: CALLNAT 'ADSN430' #ADSA401 #BENE-ACCPTNCE-IND
                    if (condition(Global.isEscape())) return;
                    if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().notEquals(" ")))                                                                           //Natural: IF #ERROR-STATUS NE ' '
                    {
                        pls_Tot_Rqst_Errors.nadd(1);                                                                                                                      //Natural: ADD 1 TO +TOT-RQST-ERRORS
                                                                                                                                                                          //Natural: PERFORM DISPLAY-#ADSA401
                        sub_Display_Pnd_Adsa401();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Misc_Variables_Pnd_Terminate_No.setValue(93);                                                                                                 //Natural: MOVE 93 TO #TERMINATE-NO
                        pnd_Misc_Variables_Pnd_Terminate_Msg.setValue(DbsUtil.compress("ERROR ", pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status(), "IN", pnd_Adsn998_Error_Handler_Pnd_Calling_Program,  //Natural: COMPRESS 'ERROR ' #ERROR-STATUS 'IN' #CALLING-PROGRAM 'FOR PRTCPNT RQST-ID : ' ADS-PRTCPNT-VIEW.RQST-ID TO #TERMINATE-MSG
                            "FOR PRTCPNT RQST-ID : ", ldaAdsl401p.getAds_Prtcpnt_View_Rqst_Id()));
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESS
                        sub_Terminate_Process();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB-MESSAGE
                sub_End_Of_Job_Message();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //* ***************** O N   E R R O R   R O U T I N E *********************
                //*                                                                                                                                                       //Natural: ON ERROR
                //* *********************** S U B R O U T I N E S ************************
                //*  -------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CONTROL-REC
                //*  OS-102013 START
                //*  ------------------------------------ *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-TOTAL-FUND-COUNT
                //*  ----------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CNTRCT-LOB
                //*  ----------------------------- *
                //* * 08312005 MN START
                //*  ------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-LOG
                //*  ----------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: HANDLE-SYSTEM-ERROR
                //*  -------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-#ADSA401
                //* * 08312005 MN START
                //* * 08312005 MN END
                //*  ---------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-JOB-MESSAGE
                //*  --------------------------------- *
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TERMINATE-PROCESS
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Read_Control_Rec() throws Exception                                                                                                                  //Natural: READ-CONTROL-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------- *
        vw_ads_Cntl_View.startDatabaseRead                                                                                                                                //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "READ01",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        READ01:
        while (condition(vw_ads_Cntl_View.readNextRow("READ01")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Misc_Variables_Pnd_Cntl_Bsnss_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A);                                         //Natural: MOVE EDITED ADS-CNTL-BSNSS-DTE-A TO #CNTL-BSNSS-DATE ( EM = YYYYMMDD )
    }
    private void sub_Get_Total_Fund_Count() throws Exception                                                                                                              //Natural: GET-TOTAL-FUND-COUNT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------ *
        DbsUtil.callnat(Adsn888.class , getCurrentProcessState(), pdaAdsa888.getPnd_Parm_Area(), pdaAdsa888.getPnd_Nbr_Acct());                                           //Natural: CALLNAT 'ADSN888' #PARM-AREA #NBR-ACCT
        if (condition(Global.isEscape())) return;
        pls_Ticker.getValue("*").setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Ticker().getValue("*"));                                                                   //Natural: MOVE #ACCT-TICKER ( * ) TO +TICKER ( * )
        pls_Invstmnt_Grpng_Id.getValue("*").setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Invstmnt_Grpng_Id().getValue("*"));                                             //Natural: MOVE #ACCT-INVSTMNT-GRPNG-ID ( * ) TO +INVSTMNT-GRPNG-ID ( * )
    }
    private void sub_Get_Cntrct_Lob() throws Exception                                                                                                                    //Natural: GET-CNTRCT-LOB
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Adsn998_Error_Handler_Pnd_Calling_Program.setValue("NECN4000");                                                                                               //Natural: ASSIGN #CALLING-PROGRAM := 'NECN4000'
        pnd_Adsn998_Error_Handler_Pnd_Step_Name.setValue("GET-LOB");                                                                                                      //Natural: ASSIGN #STEP-NAME := 'GET-LOB'
        pdaNeca4000.getNeca4000().reset();                                                                                                                                //Natural: RESET NECA4000
        pdaNeca4000.getNeca4000_Prd_Key4_Acct_Nbr().setValue(pnd_Get_Lob_Pnd_Lob_Cntrct);                                                                                 //Natural: ASSIGN NECA4000.PRD-KEY4-ACCT-NBR := #LOB-CNTRCT
        pdaNeca4000.getNeca4000_Function_Cde().setValue("PRD");                                                                                                           //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'PRD'
        pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("04");                                                                                                     //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '04'
        DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                            //Natural: CALLNAT 'NECN4000' NECA4000
        if (condition(Global.isEscape())) return;
        if (condition(pdaNeca4000.getNeca4000_Return_Cde().equals("00") || pdaNeca4000.getNeca4000_Return_Cde().equals(" ")))                                             //Natural: IF NECA4000.RETURN-CDE = '00' OR = ' '
        {
            pnd_Get_Lob_Pnd_Sub_Lob.setValue(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1));                                                                  //Natural: ASSIGN #SUB-LOB := PRD-SUB-PRODUCT-CDE ( 1 )
            pnd_Get_Lob_Pnd_Lob.setValue(pdaNeca4000.getNeca4000_Prd_Product_Cde().getValue(1));                                                                          //Natural: ASSIGN #LOB := PRD-PRODUCT-CDE ( 1 )
            pnd_Misc_Variables_Pnd_Wk_Cntrct_Lob.setValue(pnd_Get_Lob_Pnd_Lob);                                                                                           //Natural: ASSIGN #WK-CNTRCT-LOB := #LOB
            if (condition(pnd_Misc_Variables_Pnd_Wk_Cntrct_Lob.getSubstring(1,3).equals("TPA")))                                                                          //Natural: IF SUBSTR ( #WK-CNTRCT-LOB,1,3 ) EQ 'TPA'
            {
                pnd_Get_Lob_Pnd_Lob.setValue(pnd_Misc_Variables_Pnd_Wk_Cntrct_Lob.getSubstring(1,3));                                                                     //Natural: ASSIGN #LOB := SUBSTR ( #WK-CNTRCT-LOB,1,3 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Get_Lob_Pnd_Lob.equals("IA")))                                                                                                              //Natural: IF #LOB EQ 'IA'
            {
                if (condition(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1).equals("IPRO")))                                                                  //Natural: IF PRD-SUB-PRODUCT-CDE ( 1 ) EQ 'IPRO'
                {
                    pnd_Get_Lob_Pnd_Lob.setValue(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1));                                                              //Natural: ASSIGN #LOB := PRD-SUB-PRODUCT-CDE ( 1 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaNeca4000.getNeca4000_Prd_Sub_Product_Cde().getValue(1).equals("IA-RET")))                                                            //Natural: IF PRD-SUB-PRODUCT-CDE ( 1 ) EQ 'IA-RET'
                    {
                        pnd_Get_Lob_Pnd_Lob.setValue("TPA");                                                                                                              //Natural: ASSIGN #LOB := 'TPA'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* * 08312005 MN END
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "PRD RECORD NOT FOUND IN EXTERNAL CALL NECN4000 FOR CONTRACT :",pnd_Get_Lob_Pnd_Lob_Cntrct);                                            //Natural: WRITE 'PRD RECORD NOT FOUND IN EXTERNAL CALL NECN4000 FOR CONTRACT :' #LOB-CNTRCT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Log() throws Exception                                                                                                                         //Natural: ERROR-LOG
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------- *
        pnd_Adsn998_Error_Handler_Pnd_Appl_Id.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaAdsa401.getPnd_Adsa401_Pnd_Cics_System_Name(),                  //Natural: COMPRESS #ADSA401.#CICS-SYSTEM-NAME *INIT-ID INTO #APPL-ID LEAVING NO SPACE
            Global.getINIT_ID()));
        if (condition(pnd_Adsn998_Error_Handler_Pnd_Calling_Program.equals(" ")))                                                                                         //Natural: IF #CALLING-PROGRAM EQ ' '
        {
            pnd_Adsn998_Error_Handler_Pnd_Calling_Program.setValue(Global.getPROGRAM());                                                                                  //Natural: ASSIGN #CALLING-PROGRAM := *PROGRAM
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Adsn998_Error_Handler_Pnd_Unique_Id.setValue(pdaAdsa401.getPnd_Adsa401_Adp_Unique_Id());                                                                      //Natural: ASSIGN #UNIQUE-ID := #ADSA401.ADP-UNIQUE-ID
        DbsUtil.callnat(Adsn998.class , getCurrentProcessState(), pdaAdspda_M.getMsg_Info_Sub(), pnd_Adsn998_Error_Handler);                                              //Natural: CALLNAT 'ADSN998' MSG-INFO-SUB #ADSN998-ERROR-HANDLER
        if (condition(Global.isEscape())) return;
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
            getReports().write(0, ">>>>INSIDE ERROR-LOG ");                                                                                                               //Natural: WRITE '>>>>INSIDE ERROR-LOG '
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Handle_System_Error() throws Exception                                                                                                               //Natural: HANDLE-SYSTEM-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------- *
        pls_Tot_System_Errors.nadd(1);                                                                                                                                    //Natural: ADD 1 TO +TOT-SYSTEM-ERRORS
        getReports().write(0, ">>> ON ERROR FOR RQST-ID",ldaAdsl401p.getAds_Prtcpnt_View_Rqst_Id(),"TPGM: ",Global.getINIT_PROGRAM(),"PROG: ",Global.getPROGRAM(),"ERR: ",Global.getERROR_NR(),"ELIN: ",Global.getERROR_LINE(),"=",pnd_Misc_Variables_Pnd_Process_Step,"=",pnd_Adsn998_Error_Handler_Pnd_Calling_Program,NEWLINE,"*",new  //Natural: WRITE '>>> ON ERROR FOR RQST-ID' ADS-PRTCPNT-VIEW.RQST-ID '=' *INIT-PROGRAM '=' *PROGRAM '=' *ERROR-NR '=' *ERROR-LINE '=' #PROCESS-STEP '=' #CALLING-PROGRAM / '*' ( 100 )
            RepeatItem(100));
        if (Global.isEscape()) return;
        //*  -------------------------------------------------------------------
        pnd_Adsn998_Error_Handler_Pnd_Error_Msg.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "INTERNAL ERROR ENCOUNTERED IN PROGRAM :", Global.getINIT_PROGRAM(),  //Natural: COMPRESS 'INTERNAL ERROR ENCOUNTERED IN PROGRAM :' *INIT-PROGRAM ' ERR NO : ' *ERROR-NR ' ERR LINE :' *ERROR-LINE TO #ERROR-MSG LEAVING NO SPACE
            " ERR NO : ", Global.getERROR_NR(), " ERR LINE :", Global.getERROR_LINE()));
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
            short decideConditionsMet1969 = 0;                                                                                                                            //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #DISPLAY-ADSA401
            if (condition(pnd_Misc_Variables_Pnd_Display_Adsa401.getBoolean()))
            {
                decideConditionsMet1969++;
                                                                                                                                                                          //Natural: PERFORM DISPLAY-#ADSA401
                sub_Display_Pnd_Adsa401();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet1969 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ERROR-LOG
        sub_Error_Log();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (condition(Global.getERROR_NR().equals(pnd_Constant_Variables_Pnd_Database_Error.getValue("*"))))                                                              //Natural: IF *ERROR-NR EQ #DATABASE-ERROR ( * )
        {
            pnd_Misc_Variables_Pnd_Terminate_No.setValue(99);                                                                                                             //Natural: MOVE 99 TO #TERMINATE-NO
            pnd_Misc_Variables_Pnd_Terminate_Msg.setValue(DbsUtil.compress(pnd_Adsn998_Error_Handler_Pnd_Error_Msg, " PRTCPNT RQST-ID : ", ldaAdsl401p.getAds_Prtcpnt_View_Rqst_Id())); //Natural: COMPRESS #ERROR-MSG ' PRTCPNT RQST-ID : ' ADS-PRTCPNT-VIEW.RQST-ID TO #TERMINATE-MSG
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESS
            sub_Terminate_Process();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Misc_Variables_Pnd_Terminate_Msg.setValue(DbsUtil.compress(pnd_Adsn998_Error_Handler_Pnd_Error_Msg, " PRTCPNT RQST-ID : "));                              //Natural: COMPRESS #ERROR-MSG ' PRTCPNT RQST-ID : ' TO #TERMINATE-MSG
            pnd_Misc_Variables_Pnd_Terminate_No.setValue(94);                                                                                                             //Natural: MOVE 94 TO #TERMINATE-NO
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESS
            sub_Terminate_Process();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Display_Pnd_Adsa401() throws Exception                                                                                                               //Natural: DISPLAY-#ADSA401
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------- *
        getReports().write(0, NEWLINE,"*",new RepeatItem(100));                                                                                                           //Natural: WRITE / '*' ( 100 )
        if (Global.isEscape()) return;
        getReports().write(0, ">>> PROCESS STEP : ",pnd_Misc_Variables_Pnd_Process_Step);                                                                                 //Natural: WRITE '>>> PROCESS STEP : ' #PROCESS-STEP
        if (Global.isEscape()) return;
        //*  RS0
        //*  RS0
        //*  RS0
        //*  RS0
        getReports().write(0, " =====> VALUES IN #ADSA401 PARM < =====",NEWLINE,"** #ADSA401-PRTCPNT-RCRD **","=",pdaAdsa401.getPnd_Adsa401_Rqst_Id(),NEWLINE,new         //Natural: WRITE ' =====> VALUES IN #ADSA401 PARM < =====' / '** #ADSA401-PRTCPNT-RCRD **' '=' #ADSA401.RQST-ID /5T '=' #ADSA401.ADP-IA-TIAA-NBR 40T '=' #ADSA401.ADP-IA-TIAA-REA-NBR /5T '=' #ADSA401.ADP-IA-TIAA-PAYEE-CD 40T '=' #ADSA401.ADP-IA-CREF-NBR /5T '=' #ADSA401.ADP-IA-CREF-PAYEE-CD 40T '=' #ADSA401.ADP-BPS-UNIT /5T '=' #ADSA401.ADP-ANNT-TYP-CDE 40T '=' #ADSA401.ADP-EMPLYMNT-TERM-DTE /5T '=' #ADSA401.ADP-CNTRCTS-IN-RQST ( * ) /5T '=' #ADSA401.ADP-UNIQUE-ID /5T '=' #ADSA401.ADP-EFFCTV-DTE 40T '=' #ADSA401.ADP-CNTR-PRT-ISSUE-DTE /5T '=' #ADSA401.ADP-ENTRY-DTE 40T '=' #ADSA401.ADP-ENTRY-TME /5T '=' #ADSA401.ADP-ENTRY-USER-ID 40T '=' #ADSA401.ADP-REP-NME /5T '=' #ADSA401.ADP-LST-ACTVTY-DTE 40T '=' #ADSA401.ADP-WPID /3T '=' #ADSA401.ADP-MIT-LOG-DTE-TME 40T '=' #ADSA401.ADP-OPN-CLSD-IND /5T '=' #ADSA401.ADP-IN-PRGRSS-IND 40T '=' #ADSA401.ADP-RCPRCL-DTE /5T '=' #ADSA401.ADP-STTS-CDE 40T '=' #ADSA401.ADP-FRST-ANNT-SSN /5T '=' #ADSA401.ADP-SCND-ANNT-SSN 40T '=' #ADSA401.ADP-ANNTY-OPTN /5T '=' #ADSA401.ADP-GRNTEE-PERIOD 40T '=' #ADSA401.ADP-GRNTEE-PERIOD-MNTHS /5T '=' #ADSA401.ADP-ANNTY-STRT-DTE 40T '=' #ADSA401.ADP-PYMNT-MODE /5T '=' #ADSA401.ADP-SETTL-ALL-TIAA-GRD-PCT 40T '=' #ADSA401.ADP-SETTL-ALL-CREF-MON-PCT /5T '=' #ADSA401.ADP-RQST-LAST-UPDTD-ID 40T '=' #ADSA401.ADP-RQST-LAST-UPDTD-DTE /5T '=' #ADSA401.ADP-RSTTLMNT-CNT 40T '=' #ADSA401.ADP-STATE-OF-ISSUE /5T '=' #ADSA401.ADP-ORGNL-ISSUE-STATE 40T '=' #ADSA401.ADP-IVC-RLVR-CDE /5T '=' #ADSA401.ADP-IVC-PYMNT-RULE 40T '=' #ADSA401.ADP-FRM-IRC-CDE ( * ) /5T '=' #ADSA401.ADP-TO-IRC-CDE 40T '=' #ADSA401.ADP-ROTH-PLAN-TYPE /5T '=' #ADSA401.ADP-ROTH-FRST-CNTRBTN-DTE 2X '=' #ADSA401.ADP-ROTH-DSBLTY-DTE /5T '=' #ADSA401.ADP-ROTH-RQST-IND / '** #ADSA401-CNTRCT-RCRD **' /5T '=' #ADSA401.ADC-RQST-ID /5T '=' #ADSA401.ADC-SQNCE-NBR /5T '=' #ADSA401.ADC-STTS-CDE 40T '=' #ADSA401.ADC-LST-ACTVTY-DTE / '=' #ADSA401.ADC-TIAA-NBR 40T '=' #ADSA401.ADC-CREF-NBR /5T '=' #ADSA401.ADC-GD-CNTRCT-IND 40T '=' #ADSA401.ADC-TIAA-IVC-GD-IND /5T '=' #ADSA401.ADC-CREF-IVC-GD-IND 40T '=' #ADSA401.ADC-CNTRCT-ISSUE-STATE /5T '=' #ADSA401.ADC-CNTRCT-ISSUE-DTE 40T '=' #ADSA401.ADC-HOLD-CDE /5T '=' #ADSA401.ADC-ORIG-LOB-IND / '**** ADC-RQST-INFO ****'
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Ia_Tiaa_Nbr(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Ia_Tiaa_Rea_Nbr(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Ia_Tiaa_Payee_Cd(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Ia_Cref_Nbr(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Ia_Cref_Payee_Cd(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Bps_Unit(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Annt_Typ_Cde(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Emplymnt_Term_Dte(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Cntrcts_In_Rqst().getValue("*"),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Unique_Id(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Effctv_Dte(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Cntr_Prt_Issue_Dte(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Entry_Dte(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Entry_Tme(),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Entry_User_Id(),new 
            TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Rep_Nme(),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Lst_Actvty_Dte(),new 
            TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Wpid(),NEWLINE,new TabSetting(3),"=",pdaAdsa401.getPnd_Adsa401_Adp_Mit_Log_Dte_Tme(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Opn_Clsd_Ind(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_In_Prgrss_Ind(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Rcprcl_Dte(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Stts_Cde(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Frst_Annt_Ssn(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Scnd_Annt_Ssn(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Annty_Optn(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Grntee_Period(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Grntee_Period_Mnths(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Annty_Strt_Dte(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Pymnt_Mode(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Settl_All_Tiaa_Grd_Pct(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Settl_All_Cref_Mon_Pct(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Rqst_Last_Updtd_Id(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Rqst_Last_Updtd_Dte(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Rsttlmnt_Cnt(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_State_Of_Issue(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Orgnl_Issue_State(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Ivc_Rlvr_Cde(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Ivc_Pymnt_Rule(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Frm_Irc_Cde().getValue("*"),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_To_Irc_Cde(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adp_Roth_Plan_Type(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Roth_Frst_Cntrbtn_Dte(),new ColumnSpacing(2),"=",pdaAdsa401.getPnd_Adsa401_Adp_Roth_Dsblty_Dte(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adp_Roth_Rqst_Ind(),NEWLINE,"** #ADSA401-CNTRCT-RCRD **",NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Rqst_Id(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Sqnce_Nbr(),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Stts_Cde(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Lst_Actvty_Dte(),NEWLINE,"=",pdaAdsa401.getPnd_Adsa401_Adc_Tiaa_Nbr(),new 
            TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Cref_Nbr(),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Gd_Cntrct_Ind(),new 
            TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Tiaa_Ivc_Gd_Ind(),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Cref_Ivc_Gd_Ind(),new 
            TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Cntrct_Issue_State(),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Cntrct_Issue_Dte(),new 
            TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Hold_Cde(),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Orig_Lob_Ind(),NEWLINE,
            "**** ADC-RQST-INFO ****");
        if (Global.isEscape()) return;
        FOR01:                                                                                                                                                            //Natural: FOR #P 1 TO 20
        for (pnd_Counters_Pnd_P.setValue(1); condition(pnd_Counters_Pnd_P.lessOrEqual(20)); pnd_Counters_Pnd_P.nadd(1))
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_P).equals(" ") && pdaAdsa401.getPnd_Adsa401_Adc_Tkr_Symbl().getValue(pnd_Counters_Pnd_P).equals(" "))) //Natural: IF #ADSA401.ADC-ACCT-CDE ( #P ) EQ ' ' AND #ADSA401.ADC-TKR-SYMBL ( #P ) EQ ' '
            {
                getReports().write(0, ".....END OF ADC-RQST-INFO");                                                                                                       //Natural: WRITE '.....END OF ADC-RQST-INFO'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "=",pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_P),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Tkr_Symbl().getValue(pnd_Counters_Pnd_P),NEWLINE,"=",pdaAdsa401.getPnd_Adsa401_Adc_Acct_Stts_Cde().getValue(pnd_Counters_Pnd_P),new  //Natural: WRITE '=' #ADSA401.ADC-ACCT-CDE ( #P ) 40T '=' #ADSA401.ADC-TKR-SYMBL ( #P ) / '=' #ADSA401.ADC-ACCT-STTS-CDE ( #P ) 40T '=' #ADSA401.ADC-ACCT-TYP ( #P ) / '=' #ADSA401.ADC-ACCT-QTY ( #P ) 40T '=' #ADSA401.ADC-ACCT-ACTL-AMT ( #P ) / '=' #ADSA401.ADC-ACCT-ACTL-NBR-UNITS ( #P ) 40T '=' #ADSA401.ADC-GRD-MNTHLY-TYP ( #P ) / '=' #ADSA401.ADC-GRD-MNTHLY-QTY ( #P ) 40T '=' #ADSA401.ADC-GRD-MNTHLY-ACTL-AMT ( #P ) / '=' #ADSA401.ADC-STNDRD-ANNL-ACTL-AMT ( #P ) 50T '=' #ADSA401.ADC-ACCT-CREF-RATE-CDE ( #P ) / '=' #ADSA401.ADC-IVC-AMT ( #P ) 40T '=' #ADSA401.ADC-ACCT-OPN-ACCUM-AMT ( #P ) / '=' #ADSA401.ADC-ACCT-OPN-NBR-UNITS ( #P )
                TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Acct_Typ().getValue(pnd_Counters_Pnd_P),NEWLINE,"=",pdaAdsa401.getPnd_Adsa401_Adc_Acct_Qty().getValue(pnd_Counters_Pnd_P),new 
                TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Acct_Actl_Amt().getValue(pnd_Counters_Pnd_P),NEWLINE,"=",pdaAdsa401.getPnd_Adsa401_Adc_Acct_Actl_Nbr_Units().getValue(pnd_Counters_Pnd_P),new 
                TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Typ().getValue(pnd_Counters_Pnd_P),NEWLINE,"=",pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Qty().getValue(pnd_Counters_Pnd_P),new 
                TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_Counters_Pnd_P),NEWLINE,"=",pdaAdsa401.getPnd_Adsa401_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_Counters_Pnd_P),new 
                TabSetting(50),"=",pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cref_Rate_Cde().getValue(pnd_Counters_Pnd_P),NEWLINE,"=",pdaAdsa401.getPnd_Adsa401_Adc_Ivc_Amt().getValue(pnd_Counters_Pnd_P),new 
                TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Acct_Opn_Accum_Amt().getValue(pnd_Counters_Pnd_P),NEWLINE,"=",pdaAdsa401.getPnd_Adsa401_Adc_Acct_Opn_Nbr_Units().getValue(pnd_Counters_Pnd_P));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAdsa401.getPnd_Adsa401_Adc_Err_Cde().getValue(1),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Adc_Err_Acct_Cde().getValue(1),    //Natural: WRITE '=' #ADSA401.ADC-ERR-CDE ( 1 ) 40T '=' #ADSA401.ADC-ERR-ACCT-CDE ( 1 ) / '=' #ADSA401.ADC-ERR-MSG ( 1 ) / '=' #ADSA401.ADC-TRK-RO-IVC / '**** ADC-TIAA-DATA ****'
            NEWLINE,"=",pdaAdsa401.getPnd_Adsa401_Adc_Err_Msg().getValue(1),NEWLINE,"=",pdaAdsa401.getPnd_Adsa401_Adc_Trk_Ro_Ivc(),NEWLINE,"**** ADC-TIAA-DATA ****");
        if (Global.isEscape()) return;
        getReports().write(0, "*** #CURRENT-CNTRCT-PRTCPNT-INFO ***",NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Cntrct_Isn(),new                     //Natural: WRITE '*** #CURRENT-CNTRCT-PRTCPNT-INFO ***' /5T '=' #ADSA401.#CUR-CNTRCT-ISN 40T '=' #ADSA401.#CUR-PRTCPNT-ISN /5T '=' #ADSA401.#CUR-IARSLT-ISN ( 1 ) 40T '=' #ADSA401.#CUR-IARSLT-ISN ( 2 ) /5T '=' #ADSA401.#IA-CNTRCT-LOB /5T '=' #ADSA401.#IA-CNTRCT-SUB-LOB 40T '=' #ADSA401.#CNTRCT-LOB /5T '=' #ADSA401.#CNTRCT-SUB-LOB 40T '=' #ADSA401.#ERROR-STATUS /5T '=' #ADSA401.#CHECK-RULE-ELGBLTY 40T '=' #ADSA401.#TOTAL-TIAA-IVC-AMOUNT /5T '=' #ADSA401.#TOTAL-CREF-IVC-AMOUNT /5T '=' #ADSA401.#IA-ORIGIN-CODE
            TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Prtcpnt_Isn(),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Iarslt_Isn().getValue(1),new 
            TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Iarslt_Isn().getValue(2),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Ia_Cntrct_Lob(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Ia_Cntrct_Sub_Lob(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Lob(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Sub_Lob(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Check_Rule_Elgblty(),new TabSetting(40),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Total_Tiaa_Ivc_Amount(),NEWLINE,new 
            TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Total_Cref_Ivc_Amount(),NEWLINE,new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Pnd_Ia_Origin_Code());
        if (Global.isEscape()) return;
    }
    private void sub_End_Of_Job_Message() throws Exception                                                                                                                //Natural: END-OF-JOB-MESSAGE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------- *
        getReports().write(0, NEWLINE,NEWLINE,NEWLINE,NEWLINE," ================================================================= ",NEWLINE,"|**                                                             **|", //Natural: WRITE /// /' ================================================================= ' /'|**                                                             **|' /'|**            ADAS/CTLS INTERFACE PROCESSING                   **|' /'|**                                                             **|' /'|** TOTAL PRTCPNT RECORDS READ       : ' +TOT-PRTCPNT-RQST '              **|' /'|** TOTAL CONTRACTS READ             : ' +TOT-CNTRCT-RECS '              **|' /'|** TOTAL RQSTS WITH VALIDATION ERROR: ' +TOT-RQST-ERRORS '              **|' /'|** TOTAL REQUESTS WITH SYSTEM ERROR : ' +TOT-SYSTEM-ERRORS '              **|' /'|**                                                             **|' /'|**                                                             **|' /' ================================================================= '
            NEWLINE,"|**            ADAS/CTLS INTERFACE PROCESSING                   **|",NEWLINE,"|**                                                             **|",
            NEWLINE,"|** TOTAL PRTCPNT RECORDS READ       : ",pls_Tot_Prtcpnt_Rqst,"              **|",NEWLINE,"|** TOTAL CONTRACTS READ             : ",
            pls_Tot_Cntrct_Recs,"              **|",NEWLINE,"|** TOTAL RQSTS WITH VALIDATION ERROR: ",pls_Tot_Rqst_Errors,"              **|",NEWLINE,"|** TOTAL REQUESTS WITH SYSTEM ERROR : ",
            pls_Tot_System_Errors,"              **|",NEWLINE,"|**                                                             **|",NEWLINE,"|**                                                             **|",
            NEWLINE," ================================================================= ");
        if (Global.isEscape()) return;
    }
    private void sub_Terminate_Process() throws Exception                                                                                                                 //Natural: TERMINATE-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------- *
        getReports().write(0, NEWLINE,NEWLINE,NEWLINE,NEWLINE," ================================================================= ",NEWLINE,"|*****************************************************************|", //Natural: WRITE /// /' ================================================================= ' /'|*****************************************************************|' /'|*****************************************************************|' /'|** TERMINATING ADAS BATCH PROCESS.....                         **|' /'|**                                                             **|' /'|**' #TERMINATE-MSG1 '**|' /'|**' #TERMINATE-MSG2 '**|' /'|**                                                             **|' /'|** >>> PLEASE CONTACT ANNUITIZATION - ADAS SYSTEM SUPPORT <<<  **|' /'|*****************************************************************|' /'|*****************************************************************|' /' ================================================================= '
            NEWLINE,"|*****************************************************************|",NEWLINE,"|** TERMINATING ADAS BATCH PROCESS.....                         **|",
            NEWLINE,"|**                                                             **|",NEWLINE,"|**",pnd_Misc_Variables_Pnd_Terminate_Msg1,"**|",NEWLINE,
            "|**",pnd_Misc_Variables_Pnd_Terminate_Msg2,"**|",NEWLINE,"|**                                                             **|",NEWLINE,"|** >>> PLEASE CONTACT ANNUITIZATION - ADAS SYSTEM SUPPORT <<<  **|",
            NEWLINE,"|*****************************************************************|",NEWLINE,"|*****************************************************************|",
            NEWLINE," ================================================================= ");
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB-MESSAGE
        sub_End_Of_Job_Message();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        DbsUtil.terminate(pnd_Misc_Variables_Pnd_Terminate_No);  if (true) return;                                                                                        //Natural: TERMINATE #TERMINATE-NO
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
                                                                                                                                                                          //Natural: PERFORM HANDLE-SYSTEM-ERROR
        sub_Handle_System_Error();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=120 PS=100");
    }
}
