/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:00:56 PM
**        * FROM NATURAL PROGRAM : Adsp1030
************************************************************
**        * FILE NAME            : Adsp1030.java
**        * CLASS NAME           : Adsp1030
**        * INSTANCE NAME        : Adsp1030
************************************************************
************************************************************************
* PROGRAM  : ADSP1030
* GENERATED: APRIL 14, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : THIS MODULE READS A  WORK FILE AND REPORT ADDL DA TO IA
*            FINAL PREMIUM ADJUSTED PAYMENTS BY MODE WITHIN
*            PAYMENT DUE DATE.
*
* REMARKS  : CLONED FROM NAZP1030 (ADAM SYSTEM)
*********************  MAINTENANCE LOG *********************************
*  D A T E   PROGRAMMER     D E S C R I P T I O N
* 03/23/98   ZAFAR KHAN     CHECK ON UNITS ADDED FOR CREF MONTHLY TOTALS
* 12/26/00   C SCHENIDER    ADDED TPA AND IPRO
* 11/18/03   O SOTTO        99 RATES CHANGES.
* 04/14/04   C. AVE         MODIFIED ADAS (ANNUITIZATION SUNGUARD)
* 07/27/04   T. SHINE       IF MONTH-END PROCESS FALLS ON WEEKEND OR
*                           HOLIDAY, USE ADS-RPT-13 (FACTOR DATE) AS
*                           REPORT DATE.
* 12/12/08   R. SACHARNY    ADD MTH/ANN "Rea Access" FUND  RS0
* 9/07/2010  D.E.ANDER  CHG #MAX-RATE FROM 80 TO 99 OCCURS, PRD FX DEA
* 03/01/12 E. MELNIK   RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                      UPDATED LINKAGE AREAS.  CHANGES MARKED BY
*                      EM - 030112.
* 09/2013   O. SOTTO  CREF REA CHANGES MARKED WITH OS-092013.
* 02/24/2017 R.CARREON PIN EXPANSION 02242017
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp1030 extends BLNatBase
{
    // Data Areas
    private LdaAdsl1030 ldaAdsl1030;
    private LdaAdsl1031 ldaAdsl1031;
    private LdaAdsl1032 ldaAdsl1032;
    private LdaAdsl1033 ldaAdsl1033;
    private LdaAdsl1034 ldaAdsl1034;
    private LdaAdsl450 ldaAdsl450;
    private LdaAdsl450a ldaAdsl450a;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ads_Ia_Rslt2;
    private DbsField ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data;

    private DbsGroup ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt;

    private DbsGroup ads_Ia_Rslt2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt;
    private DbsGroup ads_Ia_Rslt2_Adi_Contract_IdMuGroup;
    private DbsField ads_Ia_Rslt2_Adi_Contract_Id;
    private DbsField pnd_Rec_Type;

    private DbsGroup pnd_Rec_Type__R_Field_1;
    private DbsField pnd_Rec_Type_Pnd_Filler;
    private DbsField pnd_Rec_Type_Pnd_Z_Indx;
    private DbsField pnd_Nap_Pymnt_Mode;

    private DbsGroup pnd_Pas_Ext_Cntrl_02_Work_Area;

    private DbsGroup pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Table;
    private DbsField pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1;

    private DataAccessProgramView vw_ads_Cntl_View;

    private DbsGroup ads_Cntl_View_Ads_Cntl_Grp;
    private DbsField ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte;
    private DbsField ads_Cntl_View_Ads_Fnl_Prm_Accum_Dte;
    private DbsField ads_Cntl_View_Ads_Rpt_13;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1_View;
    private DbsField iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte;

    private DbsGroup pnd_All_Indx;
    private DbsField pnd_All_Indx_Pnd_A_Indx;
    private DbsField pnd_All_Indx_Pnd_B_Indx;
    private DbsField pnd_All_Indx_Pnd_C_Indx;
    private DbsField pnd_All_Indx_Pnd_Next_Indx;
    private DbsField pnd_All_Indx_Pnd_Prod_Indx;
    private DbsField pnd_Total_Number_Rec;
    private DbsField pnd_Monthly_Number_Rec;
    private DbsField pnd_Quartly_Number_Rec;
    private DbsField pnd_Semi_Annuall_Number_Rec;
    private DbsField pnd_Annuall_Number_Rec;
    private DbsField pnd_First_Time;
    private DbsField pnd_Tiaa_Prod_Code;
    private DbsField pnd_Cref_Prod_Found;
    private DbsField pnd_Prev_Bsnss_Dte_D;
    private DbsField pnd_Prev_Bsnss_Dte_N;

    private DbsGroup pnd_Prev_Bsnss_Dte_N__R_Field_2;
    private DbsField pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A;
    private DbsField pnd_Today_Accounting_Date;
    private DbsField pnd_Today_Business_Date;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd;

    private DbsGroup pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_3;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd;
    private DbsField pnd_Curr_Accounting_Dte_Yyyymmdd;

    private DbsGroup pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_4;
    private DbsField pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Cc;
    private DbsField pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Yy;
    private DbsField pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Mm;
    private DbsField pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Dd;
    private DbsField pnd_Rqst_Id;

    private DbsGroup pnd_Rqst_Id__R_Field_5;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Unique_Id;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte;

    private DbsGroup pnd_Rqst_Id__R_Field_6;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Cc;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Yy;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Mm;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Dd;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Entry_Dte;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Entry_Tme;
    private DbsField pnd_Prev_Payment_Due_Date;

    private DbsGroup pnd_Prev_Payment_Due_Date__R_Field_7;
    private DbsField pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Cc;
    private DbsField pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Yy;
    private DbsField pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Mm;
    private DbsField pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Dd;
    private DbsField pnd_Curr_Payment_Due_Date;

    private DbsGroup pnd_Curr_Payment_Due_Date__R_Field_8;
    private DbsField pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Cc;
    private DbsField pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Yy;
    private DbsField pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Mm;
    private DbsField pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Dd;
    private DbsField pnd_Payment_Mode;

    private DbsGroup pnd_Payment_Mode__R_Field_9;
    private DbsField pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1;
    private DbsField pnd_Payment_Mode_Pnd_Payment_Mode_Byte_2;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_10;
    private DbsField pnd_Date_A_Pnd_Date_N;
    private DbsField pnd_Pec_Nbr_Active_Acct;
    private DbsField pnd_Ads_Ia_Super1;

    private DbsGroup pnd_Ads_Ia_Super1__R_Field_11;
    private DbsField pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rqst_Id;
    private DbsField pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde;
    private DbsField pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr;
    private DbsField pnd_Max_Rslt;
    private DbsField pnd_Max_Rates;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl1030 = new LdaAdsl1030();
        registerRecord(ldaAdsl1030);
        ldaAdsl1031 = new LdaAdsl1031();
        registerRecord(ldaAdsl1031);
        ldaAdsl1032 = new LdaAdsl1032();
        registerRecord(ldaAdsl1032);
        ldaAdsl1033 = new LdaAdsl1033();
        registerRecord(ldaAdsl1033);
        ldaAdsl1034 = new LdaAdsl1034();
        registerRecord(ldaAdsl1034);
        ldaAdsl450 = new LdaAdsl450();
        registerRecord(ldaAdsl450);
        registerRecord(ldaAdsl450.getVw_ads_Ia_Rslt_View());
        ldaAdsl450a = new LdaAdsl450a();
        registerRecord(ldaAdsl450a);

        // Local Variables
        localVariables = new DbsRecord();

        vw_ads_Ia_Rslt2 = new DataAccessProgramView(new NameInfo("vw_ads_Ia_Rslt2", "ADS-IA-RSLT2"), "ADS_IA_RSLT", "ADS_IA_RSLT", DdmPeriodicGroups.getInstance().getGroups("ADS_IA_RSLT"));
        ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data = vw_ads_Ia_Rslt2.getRecord().newFieldInGroup("ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data", "C*ADI-DTL-TIAA-DATA", 
            RepeatingFieldStrategy.CAsteriskVariable, "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");

        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data = vw_ads_Ia_Rslt2.getRecord().newGroupInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data", "ADI-DTL-TIAA-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd", "ADI-DTL-TIAA-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt", "ADI-DTL-TIAA-DA-STD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt", "ADI-DTL-TIAA-DA-GRD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd", "ADI-DTL-TIAA-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_ACCT_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd", "ADI-DTL-TIAA-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_IA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst", "ADI-DTL-TIAA-EFF-RTE-INTRST", 
            FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_EFF_RTE_INTRST", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt", "ADI-DTL-TIAA-GRD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt", "ADI-DTL-TIAA-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt", "ADI-DTL-FNL-GRD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt", "ADI-DTL-FNL-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt", "ADI-DTL-TIAA-STD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt", "ADI-DTL-TIAA-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt", "ADI-DTL-FNL-STD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt", "ADI-DTL-FNL-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");

        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat = vw_ads_Ia_Rslt2.getRecord().newGroupInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat", "ADI-DTL-TIAA-TPA-GUAR-COMMUT-DAT", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt", 
            "ADI-DTL-TIAA-GUAR-COMMUT-STD-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_TIAA_GUAR_COMMUT_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt", 
            "ADI-DTL-TIAA-GUAR-COMMUT-GRD-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_TIAA_GUAR_COMMUT_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        ads_Ia_Rslt2_Adi_Contract_IdMuGroup = vw_ads_Ia_Rslt2.getRecord().newGroupInGroup("ADS_IA_RSLT2_ADI_CONTRACT_IDMuGroup", "ADI_CONTRACT_IDMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_IA_RSLT_ADI_CONTRACT_ID");
        ads_Ia_Rslt2_Adi_Contract_Id = ads_Ia_Rslt2_Adi_Contract_IdMuGroup.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Contract_Id", "ADI-CONTRACT-ID", FieldType.NUMERIC, 
            11, new DbsArrayController(1, 125), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_CONTRACT_ID");
        registerRecord(vw_ads_Ia_Rslt2);

        pnd_Rec_Type = localVariables.newFieldInRecord("pnd_Rec_Type", "#REC-TYPE", FieldType.NUMERIC, 3);

        pnd_Rec_Type__R_Field_1 = localVariables.newGroupInRecord("pnd_Rec_Type__R_Field_1", "REDEFINE", pnd_Rec_Type);
        pnd_Rec_Type_Pnd_Filler = pnd_Rec_Type__R_Field_1.newFieldInGroup("pnd_Rec_Type_Pnd_Filler", "#FILLER", FieldType.NUMERIC, 2);
        pnd_Rec_Type_Pnd_Z_Indx = pnd_Rec_Type__R_Field_1.newFieldInGroup("pnd_Rec_Type_Pnd_Z_Indx", "#Z-INDX", FieldType.NUMERIC, 1);
        pnd_Nap_Pymnt_Mode = localVariables.newFieldInRecord("pnd_Nap_Pymnt_Mode", "#NAP-PYMNT-MODE", FieldType.NUMERIC, 3);

        pnd_Pas_Ext_Cntrl_02_Work_Area = localVariables.newGroupInRecord("pnd_Pas_Ext_Cntrl_02_Work_Area", "#PAS-EXT-CNTRL-02-WORK-AREA");

        pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Table = pnd_Pas_Ext_Cntrl_02_Work_Area.newGroupArrayInGroup("pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Table", 
            "#PEC-ACCT-TABLE", new DbsArrayController(1, 20));
        pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1 = pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Table.newFieldInGroup("pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1", 
            "#PEC-ACCT-NAME-1", FieldType.STRING, 1);

        vw_ads_Cntl_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntl_View", "ADS-CNTL-VIEW"), "ADS_CNTL", "ADS_CNTL");

        ads_Cntl_View_Ads_Cntl_Grp = vw_ads_Cntl_View.getRecord().newGroupInGroup("ADS_CNTL_VIEW_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde", "ADS-CNTL-RCRD-TYP-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADS_CNTL_RCRD_TYP_CDE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte", "ADS-CNTL-BSNSS-RCPRCL-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_RCPRCL_DTE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_DTE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte", "ADS-CNTL-BSNSS-TMRRW-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_TMRRW_DTE");
        ads_Cntl_View_Ads_Fnl_Prm_Accum_Dte = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Fnl_Prm_Accum_Dte", "ADS-FNL-PRM-ACCUM-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADS_FNL_PRM_ACCUM_DTE");
        ads_Cntl_View_Ads_Rpt_13 = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Rpt_13", "ADS-RPT-13", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADS_RPT_13");
        registerRecord(vw_ads_Cntl_View);

        vw_iaa_Cntrl_Rcrd_1_View = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1_View", "IAA-CNTRL-RCRD-1-VIEW"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1_View.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_View_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1_View);

        pnd_All_Indx = localVariables.newGroupInRecord("pnd_All_Indx", "#ALL-INDX");
        pnd_All_Indx_Pnd_A_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_A_Indx", "#A-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_B_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_B_Indx", "#B-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_C_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_C_Indx", "#C-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Next_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Next_Indx", "#NEXT-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Prod_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Prod_Indx", "#PROD-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Total_Number_Rec = localVariables.newFieldInRecord("pnd_Total_Number_Rec", "#TOTAL-NUMBER-REC", FieldType.PACKED_DECIMAL, 7);
        pnd_Monthly_Number_Rec = localVariables.newFieldInRecord("pnd_Monthly_Number_Rec", "#MONTHLY-NUMBER-REC", FieldType.PACKED_DECIMAL, 7);
        pnd_Quartly_Number_Rec = localVariables.newFieldInRecord("pnd_Quartly_Number_Rec", "#QUARTLY-NUMBER-REC", FieldType.PACKED_DECIMAL, 7);
        pnd_Semi_Annuall_Number_Rec = localVariables.newFieldInRecord("pnd_Semi_Annuall_Number_Rec", "#SEMI-ANNUALL-NUMBER-REC", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Annuall_Number_Rec = localVariables.newFieldInRecord("pnd_Annuall_Number_Rec", "#ANNUALL-NUMBER-REC", FieldType.PACKED_DECIMAL, 7);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Prod_Code = localVariables.newFieldInRecord("pnd_Tiaa_Prod_Code", "#TIAA-PROD-CODE", FieldType.BOOLEAN, 1);
        pnd_Cref_Prod_Found = localVariables.newFieldInRecord("pnd_Cref_Prod_Found", "#CREF-PROD-FOUND", FieldType.BOOLEAN, 1);
        pnd_Prev_Bsnss_Dte_D = localVariables.newFieldInRecord("pnd_Prev_Bsnss_Dte_D", "#PREV-BSNSS-DTE-D", FieldType.DATE);
        pnd_Prev_Bsnss_Dte_N = localVariables.newFieldInRecord("pnd_Prev_Bsnss_Dte_N", "#PREV-BSNSS-DTE-N", FieldType.NUMERIC, 8);

        pnd_Prev_Bsnss_Dte_N__R_Field_2 = localVariables.newGroupInRecord("pnd_Prev_Bsnss_Dte_N__R_Field_2", "REDEFINE", pnd_Prev_Bsnss_Dte_N);
        pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A = pnd_Prev_Bsnss_Dte_N__R_Field_2.newFieldInGroup("pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A", "#PREV-BSNSS-DTE-A", 
            FieldType.STRING, 8);
        pnd_Today_Accounting_Date = localVariables.newFieldInRecord("pnd_Today_Accounting_Date", "#TODAY-ACCOUNTING-DATE", FieldType.STRING, 8);
        pnd_Today_Business_Date = localVariables.newFieldInRecord("pnd_Today_Business_Date", "#TODAY-BUSINESS-DATE", FieldType.STRING, 8);
        pnd_Prev_Accounting_Dte_Yyyymmdd = localVariables.newFieldInRecord("pnd_Prev_Accounting_Dte_Yyyymmdd", "#PREV-ACCOUNTING-DTE-YYYYMMDD", FieldType.STRING, 
            8);

        pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_3 = localVariables.newGroupInRecord("pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_3", "REDEFINE", pnd_Prev_Accounting_Dte_Yyyymmdd);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_3.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc", 
            "#PREV-ACCOUNT-DTE-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_3.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy", 
            "#PREV-ACCOUNT-DTE-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_3.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm", 
            "#PREV-ACCOUNT-DTE-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_3.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd", 
            "#PREV-ACCOUNT-DTE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Curr_Accounting_Dte_Yyyymmdd = localVariables.newFieldInRecord("pnd_Curr_Accounting_Dte_Yyyymmdd", "#CURR-ACCOUNTING-DTE-YYYYMMDD", FieldType.STRING, 
            8);

        pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_4 = localVariables.newGroupInRecord("pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_4", "REDEFINE", pnd_Curr_Accounting_Dte_Yyyymmdd);
        pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Cc = pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_4.newFieldInGroup("pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Cc", 
            "#CURR-ACCOUNT-DTE-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Yy = pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_4.newFieldInGroup("pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Yy", 
            "#CURR-ACCOUNT-DTE-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Mm = pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_4.newFieldInGroup("pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Mm", 
            "#CURR-ACCOUNT-DTE-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Dd = pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_4.newFieldInGroup("pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Dd", 
            "#CURR-ACCOUNT-DTE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Rqst_Id = localVariables.newFieldInRecord("pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 35);

        pnd_Rqst_Id__R_Field_5 = localVariables.newGroupInRecord("pnd_Rqst_Id__R_Field_5", "REDEFINE", pnd_Rqst_Id);
        pnd_Rqst_Id_Pnd_Nap_Unique_Id = pnd_Rqst_Id__R_Field_5.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Unique_Id", "#NAP-UNIQUE-ID", FieldType.NUMERIC, 12);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte = pnd_Rqst_Id__R_Field_5.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte", "#NAP-EFFCTV-DTE", FieldType.NUMERIC, 
            8);

        pnd_Rqst_Id__R_Field_6 = pnd_Rqst_Id__R_Field_5.newGroupInGroup("pnd_Rqst_Id__R_Field_6", "REDEFINE", pnd_Rqst_Id_Pnd_Nap_Effctv_Dte);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Cc = pnd_Rqst_Id__R_Field_6.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Cc", "#NAP-EFFCTV-DTE-CC", FieldType.NUMERIC, 
            2);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Yy = pnd_Rqst_Id__R_Field_6.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Yy", "#NAP-EFFCTV-DTE-YY", FieldType.NUMERIC, 
            2);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Mm = pnd_Rqst_Id__R_Field_6.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Mm", "#NAP-EFFCTV-DTE-MM", FieldType.NUMERIC, 
            2);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Dd = pnd_Rqst_Id__R_Field_6.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Dd", "#NAP-EFFCTV-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_Rqst_Id_Pnd_Nap_Entry_Dte = pnd_Rqst_Id__R_Field_5.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Entry_Dte", "#NAP-ENTRY-DTE", FieldType.NUMERIC, 8);
        pnd_Rqst_Id_Pnd_Nap_Entry_Tme = pnd_Rqst_Id__R_Field_5.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Entry_Tme", "#NAP-ENTRY-TME", FieldType.NUMERIC, 7);
        pnd_Prev_Payment_Due_Date = localVariables.newFieldInRecord("pnd_Prev_Payment_Due_Date", "#PREV-PAYMENT-DUE-DATE", FieldType.STRING, 8);

        pnd_Prev_Payment_Due_Date__R_Field_7 = localVariables.newGroupInRecord("pnd_Prev_Payment_Due_Date__R_Field_7", "REDEFINE", pnd_Prev_Payment_Due_Date);
        pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Cc = pnd_Prev_Payment_Due_Date__R_Field_7.newFieldInGroup("pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Cc", 
            "#PREV-PAYMENT-DUE-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Yy = pnd_Prev_Payment_Due_Date__R_Field_7.newFieldInGroup("pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Yy", 
            "#PREV-PAYMENT-DUE-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Mm = pnd_Prev_Payment_Due_Date__R_Field_7.newFieldInGroup("pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Mm", 
            "#PREV-PAYMENT-DUE-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Dd = pnd_Prev_Payment_Due_Date__R_Field_7.newFieldInGroup("pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Dd", 
            "#PREV-PAYMENT-DUE-DATE-DD", FieldType.NUMERIC, 2);
        pnd_Curr_Payment_Due_Date = localVariables.newFieldInRecord("pnd_Curr_Payment_Due_Date", "#CURR-PAYMENT-DUE-DATE", FieldType.STRING, 8);

        pnd_Curr_Payment_Due_Date__R_Field_8 = localVariables.newGroupInRecord("pnd_Curr_Payment_Due_Date__R_Field_8", "REDEFINE", pnd_Curr_Payment_Due_Date);
        pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Cc = pnd_Curr_Payment_Due_Date__R_Field_8.newFieldInGroup("pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Cc", 
            "#CURR-PAYMENT-DUE-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Yy = pnd_Curr_Payment_Due_Date__R_Field_8.newFieldInGroup("pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Yy", 
            "#CURR-PAYMENT-DUE-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Mm = pnd_Curr_Payment_Due_Date__R_Field_8.newFieldInGroup("pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Mm", 
            "#CURR-PAYMENT-DUE-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Dd = pnd_Curr_Payment_Due_Date__R_Field_8.newFieldInGroup("pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Dd", 
            "#CURR-PAYMENT-DUE-DATE-DD", FieldType.NUMERIC, 2);
        pnd_Payment_Mode = localVariables.newFieldInRecord("pnd_Payment_Mode", "#PAYMENT-MODE", FieldType.NUMERIC, 3);

        pnd_Payment_Mode__R_Field_9 = localVariables.newGroupInRecord("pnd_Payment_Mode__R_Field_9", "REDEFINE", pnd_Payment_Mode);
        pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1 = pnd_Payment_Mode__R_Field_9.newFieldInGroup("pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1", "#PAYMENT-MODE-BYTE-1", 
            FieldType.NUMERIC, 1);
        pnd_Payment_Mode_Pnd_Payment_Mode_Byte_2 = pnd_Payment_Mode__R_Field_9.newFieldInGroup("pnd_Payment_Mode_Pnd_Payment_Mode_Byte_2", "#PAYMENT-MODE-BYTE-2", 
            FieldType.NUMERIC, 2);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_10 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_10", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_10.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        pnd_Pec_Nbr_Active_Acct = localVariables.newFieldInRecord("pnd_Pec_Nbr_Active_Acct", "#PEC-NBR-ACTIVE-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Ads_Ia_Super1 = localVariables.newFieldInRecord("pnd_Ads_Ia_Super1", "#ADS-IA-SUPER1", FieldType.STRING, 36);

        pnd_Ads_Ia_Super1__R_Field_11 = localVariables.newGroupInRecord("pnd_Ads_Ia_Super1__R_Field_11", "REDEFINE", pnd_Ads_Ia_Super1);
        pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rqst_Id = pnd_Ads_Ia_Super1__R_Field_11.newFieldInGroup("pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rqst_Id", "#ADS-IA-RQST-ID", 
            FieldType.STRING, 30);
        pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde = pnd_Ads_Ia_Super1__R_Field_11.newFieldInGroup("pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde", "#ADS-IA-RCRD-CDE", 
            FieldType.STRING, 3);
        pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr = pnd_Ads_Ia_Super1__R_Field_11.newFieldInGroup("pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr", "#ADS-IA-RCRD-SQNCE-NBR", 
            FieldType.NUMERIC, 3);
        pnd_Max_Rslt = localVariables.newFieldInRecord("pnd_Max_Rslt", "#MAX-RSLT", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Rates = localVariables.newFieldInRecord("pnd_Max_Rates", "#MAX-RATES", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Ia_Rslt2.reset();
        vw_ads_Cntl_View.reset();
        vw_iaa_Cntrl_Rcrd_1_View.reset();

        ldaAdsl1030.initializeValues();
        ldaAdsl1031.initializeValues();
        ldaAdsl1032.initializeValues();
        ldaAdsl1033.initializeValues();
        ldaAdsl1034.initializeValues();
        ldaAdsl450.initializeValues();
        ldaAdsl450a.initializeValues();

        localVariables.reset();
        pnd_All_Indx_Pnd_A_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_B_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_C_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Next_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Prod_Indx.setInitialValue(0);
        pnd_Total_Number_Rec.setInitialValue(0);
        pnd_Monthly_Number_Rec.setInitialValue(0);
        pnd_Quartly_Number_Rec.setInitialValue(0);
        pnd_Semi_Annuall_Number_Rec.setInitialValue(0);
        pnd_Annuall_Number_Rec.setInitialValue(0);
        pnd_First_Time.setInitialValue(true);
        pnd_Tiaa_Prod_Code.setInitialValue(true);
        pnd_Cref_Prod_Found.setInitialValue(true);
        pnd_Pec_Nbr_Active_Acct.setInitialValue(20);
        pnd_Max_Rslt.setInitialValue(125);
        pnd_Max_Rates.setInitialValue(250);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp1030() throws Exception
    {
        super("Adsp1030");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  OS-092013 START                                                                                                                                              //Natural: FORMAT LS = 79 PS = 60;//Natural: FORMAT ( 1 ) LS = 79 PS = 60;//Natural: FORMAT ( 2 ) LS = 79 PS = 60
        //* *CALLNAT 'ADSN888'      /* NEW EXTERNALIZATION + SEQUENCING OF PROD CDE
        //* *  #PAS-EXT-CNTRL-02-WORK-AREA
        //* *  #PEC-NBR-ACTIVE-ACCT
        DbsUtil.callnat(Adsn889.class , getCurrentProcessState(), pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue("*"));                                      //Natural: CALLNAT 'ADSN889' #PEC-ACCT-NAME-1 ( * )
        if (condition(Global.isEscape())) return;
        //*  OS-092013 END
                                                                                                                                                                          //Natural: PERFORM READ-CONTROL-RECORD
        sub_Read_Control_Record();
        if (condition(Global.isEscape())) {return;}
        PND_PND_L0380:                                                                                                                                                    //Natural: READ WORK FILE 1 ADS-IA-RSLT-VIEW #MORE
        while (condition(getWorkFiles().read(1, ldaAdsl450.getVw_ads_Ia_Rslt_View(), ldaAdsl450.getPnd_More())))
        {
            //*                                                                                                                                                           //Natural: AT END OF DATA
            pnd_Today_Accounting_Date.setValueEdited(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                                 //Natural: MOVE EDITED ADS-IA-RSLT-VIEW.ADI-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #TODAY-ACCOUNTING-DATE
            if (condition(pnd_Today_Accounting_Date.greater(pnd_Today_Business_Date)))                                                                                    //Natural: IF #TODAY-ACCOUNTING-DATE GT #TODAY-BUSINESS-DATE
            {
                if (true) break PND_PND_L0380;                                                                                                                            //Natural: ESCAPE BOTTOM ( ##L0380. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!((ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr().equals(12) || ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr().equals(22)) &&                   //Natural: ACCEPT IF ( ADS-IA-RSLT-VIEW.ADI-SQNCE-NBR = 12 OR ADS-IA-RSLT-VIEW.ADI-SQNCE-NBR = 22 ) AND ( ADS-IA-RSLT-VIEW.ADI-LST-ACTVTY-DTE GT ADS-FNL-PRM-ACCUM-DTE )
                (ldaAdsl450.getAds_Ia_Rslt_View_Adi_Lst_Actvty_Dte().greater(ads_Cntl_View_Ads_Fnl_Prm_Accum_Dte)))))
            {
                continue;
            }
            //*  EM - 030112
                                                                                                                                                                          //Natural: PERFORM POPULATE-ADS-IA-RSLT-AREA
            sub_Populate_Ads_Ia_Rslt_Area();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L0380"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0380"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_First_Time.getBoolean()))                                                                                                                   //Natural: IF #FIRST-TIME
            {
                pnd_Rqst_Id.setValue(ldaAdsl450a.getAds_Ia_Rslt_Rqst_Id());                                                                                               //Natural: MOVE ADS-IA-RSLT.RQST-ID TO #RQST-ID
                pnd_Prev_Payment_Due_Date.setValueEdited(ldaAdsl450a.getAds_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte(),new ReportEditMask("YYYYMMDD"));                             //Natural: MOVE EDITED ADS-IA-RSLT.ADI-FRST-PYMNT-DUE-DTE ( EM = YYYYMMDD ) TO #PREV-PAYMENT-DUE-DATE
                pnd_First_Time.setValue(false);                                                                                                                           //Natural: MOVE FALSE TO #FIRST-TIME
            }                                                                                                                                                             //Natural: END-IF
            pnd_Rqst_Id.setValue(ldaAdsl450a.getAds_Ia_Rslt_Rqst_Id());                                                                                                   //Natural: MOVE ADS-IA-RSLT.RQST-ID TO #RQST-ID
            pnd_Payment_Mode.setValue(ldaAdsl450a.getAds_Ia_Rslt_Adi_Pymnt_Mode());                                                                                       //Natural: MOVE ADS-IA-RSLT.ADI-PYMNT-MODE TO #PAYMENT-MODE
            pnd_Curr_Payment_Due_Date.setValueEdited(ldaAdsl450a.getAds_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte(),new ReportEditMask("YYYYMMDD"));                                 //Natural: MOVE EDITED ADS-IA-RSLT.ADI-FRST-PYMNT-DUE-DTE ( EM = YYYYMMDD ) TO #CURR-PAYMENT-DUE-DATE
            if (condition(pnd_Curr_Payment_Due_Date.notEquals(pnd_Prev_Payment_Due_Date)))                                                                                //Natural: IF #CURR-PAYMENT-DUE-DATE NE #PREV-PAYMENT-DUE-DATE
            {
                                                                                                                                                                          //Natural: PERFORM BREAK-OF-PAYMENT-DUE-DATE-RTN
                sub_Break_Of_Payment_Due_Date_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L0380"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0380"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Prev_Payment_Due_Date.setValue(pnd_Curr_Payment_Due_Date);                                                                                            //Natural: MOVE #CURR-PAYMENT-DUE-DATE TO #PREV-PAYMENT-DUE-DATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Monthly_Number_Rec.reset();                                                                                                                               //Natural: RESET #MONTHLY-NUMBER-REC #QUARTLY-NUMBER-REC #SEMI-ANNUALL-NUMBER-REC #ANNUALL-NUMBER-REC
            pnd_Quartly_Number_Rec.reset();
            pnd_Semi_Annuall_Number_Rec.reset();
            pnd_Annuall_Number_Rec.reset();
            pnd_All_Indx_Pnd_A_Indx.reset();                                                                                                                              //Natural: RESET #A-INDX
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(1).notEquals(" ")))                                                           //Natural: IF #ADI-DTL-TIAA-DA-RATE-CD ( 1 ) NE ' '
            {
                short decideConditionsMet971 = 0;                                                                                                                         //Natural: DECIDE ON FIRST #PAYMENT-MODE-BYTE-1;//Natural: VALUE 1
                if (condition((pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(1))))
                {
                    decideConditionsMet971++;
                    pnd_Total_Number_Rec.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-NUMBER-REC
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-MONTHLY-ACCUMULATIONS
                    sub_Calculate_Tiaa_Monthly_Accumulations();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L0380"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0380"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 6
                else if (condition((pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(6))))
                {
                    decideConditionsMet971++;
                    pnd_Total_Number_Rec.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-NUMBER-REC
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-QUARTLY-ACCUMULATIONS
                    sub_Calculate_Tiaa_Quartly_Accumulations();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L0380"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0380"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 7
                else if (condition((pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(7))))
                {
                    decideConditionsMet971++;
                    pnd_Total_Number_Rec.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-NUMBER-REC
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-SEMI-ANNUAL-ACCUMULATIONS
                    sub_Calculate_Tiaa_Semi_Annual_Accumulations();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L0380"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0380"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 8
                else if (condition((pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(8))))
                {
                    decideConditionsMet971++;
                    pnd_Total_Number_Rec.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-NUMBER-REC
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-ANNUAL-ACCUMULATIONS
                    sub_Calculate_Tiaa_Annual_Accumulations();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PND_PND_L0380"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0380"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #A-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
            for (pnd_All_Indx_Pnd_A_Indx.setValue(1); condition(pnd_All_Indx_Pnd_A_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_A_Indx.nadd(1))
            {
                //*  STBLE RTRN RS0
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("T") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("Y"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'T' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'Y'
                {
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(" ")))                                    //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE ' '
                    {
                        short decideConditionsMet993 = 0;                                                                                                                 //Natural: DECIDE ON FIRST #PAYMENT-MODE-BYTE-1;//Natural: VALUE 1
                        if (condition((pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(1))))
                        {
                            decideConditionsMet993++;
                            pnd_Total_Number_Rec.nadd(1);                                                                                                                 //Natural: ADD 1 TO #TOTAL-NUMBER-REC
                            pnd_Monthly_Number_Rec.nadd(1);                                                                                                               //Natural: ADD 1 TO #MONTHLY-NUMBER-REC
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-MONTHLY-ACCUMULATIONS
                            sub_Calculate_Cref_Monthly_Accumulations();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: VALUE 6
                        else if (condition((pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(6))))
                        {
                            decideConditionsMet993++;
                            pnd_Total_Number_Rec.nadd(1);                                                                                                                 //Natural: ADD 1 TO #TOTAL-NUMBER-REC
                            pnd_Quartly_Number_Rec.nadd(1);                                                                                                               //Natural: ADD 1 TO #QUARTLY-NUMBER-REC
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-QUARTLY-ACCUMULATIONS
                            sub_Calculate_Cref_Quartly_Accumulations();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: VALUE 7
                        else if (condition((pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(7))))
                        {
                            decideConditionsMet993++;
                            pnd_Total_Number_Rec.nadd(1);                                                                                                                 //Natural: ADD 1 TO #TOTAL-NUMBER-REC
                            pnd_Semi_Annuall_Number_Rec.nadd(1);                                                                                                          //Natural: ADD 1 TO #SEMI-ANNUALL-NUMBER-REC
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-SEMI-ANNUAL-ACCUMULATIONS
                            sub_Calculate_Cref_Semi_Annual_Accumulations();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: VALUE 8
                        else if (condition((pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(8))))
                        {
                            decideConditionsMet993++;
                            pnd_Total_Number_Rec.nadd(1);                                                                                                                 //Natural: ADD 1 TO #TOTAL-NUMBER-REC
                            pnd_Annuall_Number_Rec.nadd(1);                                                                                                               //Natural: ADD 1 TO #ANNUALL-NUMBER-REC
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-ANNUAL-ACCUMULATIONS
                            sub_Calculate_Cref_Annual_Accumulations();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: NONE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L0380"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0380"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Monthly_Number_Rec.greater(getZero())))                                                                                                     //Natural: IF #MONTHLY-NUMBER-REC > 0
            {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-MONTHLY-IVC
                sub_Calculate_Monthly_Ivc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L0380"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0380"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Quartly_Number_Rec.greater(getZero())))                                                                                                     //Natural: IF #QUARTLY-NUMBER-REC > 0
            {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-QUARTLY-IVC
                sub_Calculate_Quartly_Ivc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L0380"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0380"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Semi_Annuall_Number_Rec.greater(getZero())))                                                                                                //Natural: IF #SEMI-ANNUALL-NUMBER-REC > 0
            {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-SEMI-ANNUALL-IVC
                sub_Calculate_Semi_Annuall_Ivc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L0380"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0380"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Annuall_Number_Rec.greater(getZero())))                                                                                                     //Natural: IF #ANNUALL-NUMBER-REC > 0
            {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-ANNUALL-IVC
                sub_Calculate_Annuall_Ivc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L0380"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0380"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: AT TOP OF PAGE ( 1 );//Natural: END-WORK
        PND_PND_L0380_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom, "pnd_pnd_L0380");                                                                             //Natural: ESCAPE BOTTOM ( ##L0380. )
            if (true) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //*    PERFORM BREAK-OF-ACCOUNTING-DATE-RTN
                                                                                                                                                                          //Natural: PERFORM BREAK-OF-PAYMENT-DUE-DATE-RTN
        sub_Break_Of_Payment_Due_Date_Rtn();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB-RTN
        sub_End_Of_Job_Rtn();
        if (condition(Global.isEscape())) {return;}
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-ADS-IA-RSLT-AREA
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CONTROL-RECORD
        //* *******************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TIAA-MONTHLY-ACCUMULATIONS
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TIAA-QUARTLY-ACCUMULATIONS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TIAA-SEMI-ANNUAL-ACCUMULATIONS
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TIAA-ANNUAL-ACCUMULATIONS
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-CREF-MONTHLY-ACCUMULATIONS
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-MONTHLY-IVC
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-CREF-QUARTLY-ACCUMULATIONS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-QUARTLY-IVC
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-CREF-SEMI-ANNUAL-ACCUMULATIONS
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-SEMI-ANNUALL-IVC
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-CREF-ANNUAL-ACCUMULATIONS
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-ANNUALL-IVC
        //* *********************************************************************
        //* **********************************************************************
        //*  DEFINE SUBROUTINE  BREAK-OF-ACCOUNTING-DATE-RTN
        //* **********************************************************************
        //*   NEWPAGE (1)
        //*   WRITE   (1) NOTITLE USING FORM 'NAZM472'
        //*   WRITE   (1) NOTITLE USING FORM 'NAZM473'
        //*  END-SUBROUTINE
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BREAK-OF-PAYMENT-DUE-DATE-RTN
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-JOB-RTN
    }
    //*  EM 030112 START
    private void sub_Populate_Ads_Ia_Rslt_Area() throws Exception                                                                                                         //Natural: POPULATE-ADS-IA-RSLT-AREA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaAdsl450a.getAds_Ia_Rslt().setValuesByName(ldaAdsl450.getVw_ads_Ia_Rslt_View());                                                                                //Natural: MOVE BY NAME ADS-IA-RSLT-VIEW TO ADS-IA-RSLT
        ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Data().reset();                                                                                                           //Natural: RESET ADS-IA-RSLT.ADI-DTL-TIAA-DATA ADS-IA-RSLT.#ADI-DTL-TIA-TPA-GUAR-COMMUT-DAT ADS-IA-RSLT.#ADI-CONTRACT-ID ( * )
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Tpa_Guar_Commut_Dat().reset();
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Contract_Id().getValue("*").reset();
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-RATE-CD ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-RATE-CD ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-STD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-STD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-GRD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-GRD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Acct_Cd().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Acct_Cd().getValue(1,     //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-ACCT-CD ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-ACCT-CD ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Eff_Rte_Intrst().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Eff_Rte_Intrst().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-EFF-RTE-INTRST ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-EFF-RTE-INTRST ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-GRD-GRNTD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-GRD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-GRD-PAY-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-FNL-GRD-PAY-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-GRD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-FNL-GRD-DVDND-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-STD-PAY-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-FNL-STD-PAY-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-STD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-FNL-STD-DVDND-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Std_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIA-GUAR-COMMUT-STD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GUAR-COMMUT-STD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Grd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIA-GUAR-COMMUT-GRD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GUAR-COMMUT-GRD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Contract_Id().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Contract_Id().getValue(1,               //Natural: ASSIGN ADS-IA-RSLT.#ADI-CONTRACT-ID ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-CONTRACT-ID ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        if (condition(ldaAdsl450.getPnd_More().equals("Y")))                                                                                                              //Natural: IF #MORE = 'Y'
        {
            pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rqst_Id.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Rqst_Id());                                                                      //Natural: ASSIGN #ADS-IA-RQST-ID := ADS-IA-RSLT-VIEW.RQST-ID
            pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde(),              //Natural: COMPRESS ADS-IA-RSLT-VIEW.ADI-IA-RCRD-CDE '1' INTO #ADS-IA-RCRD-CDE LEAVING NO SPACE
                "1"));
            pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr());                                                         //Natural: ASSIGN #ADS-IA-RCRD-SQNCE-NBR := ADS-IA-RSLT-VIEW.ADI-SQNCE-NBR
            vw_ads_Ia_Rslt2.startDatabaseFind                                                                                                                             //Natural: FIND ADS-IA-RSLT2 WITH ADI-SUPER1 = #ADS-IA-SUPER1
            (
            "FIND01",
            new Wc[] { new Wc("ADI_SUPER1", "=", pnd_Ads_Ia_Super1, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_ads_Ia_Rslt2.readNextRow("FIND01")))
            {
                vw_ads_Ia_Rslt2.setIfNotFoundControlFlag(false);
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-RATE-CD ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-DA-RATE-CD ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-STD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-DA-STD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-GRD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-DA-GRD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Acct_Cd().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-ACCT-CD ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-ACCT-CD ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-IA-RATE-CD ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Eff_Rte_Intrst().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-EFF-RTE-INTRST ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-EFF-RTE-INTRST ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-GRD-GRNTD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-GRD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-DVDND-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-GRD-PAY-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-FNL-GRD-PAY-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-GRD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-FNL-GRD-DVDND-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-STD-GRNTD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-STD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-STD-PAY-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-FNL-STD-PAY-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-STD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-FNL-STD-DVDND-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Std_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIA-GUAR-COMMUT-STD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-GUAR-COMMUT-STD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Grd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIA-GUAR-COMMUT-GRD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-GUAR-COMMUT-GRD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Contract_Id().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Contract_Id.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-CONTRACT-ID ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-CONTRACT-ID ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  EM - 030112 END
    }
    private void sub_Read_Control_Record() throws Exception                                                                                                               //Natural: READ-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        vw_ads_Cntl_View.startDatabaseRead                                                                                                                                //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "PND_PND_L0786",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        PND_PND_L0786:
        while (condition(vw_ads_Cntl_View.readNextRow("PND_PND_L0786")))
        {
            if (condition(vw_ads_Cntl_View.getAstCOUNTER().equals(2)))                                                                                                    //Natural: IF *COUNTER ( ##L0786. ) = 2
            {
                if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                             //Natural: IF ADS-RPT-13 NE 0
                {
                    pnd_Date_A.setValueEdited(ads_Cntl_View_Ads_Rpt_13,new ReportEditMask("YYYYMMDD"));                                                                   //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE-A
                    pnd_Prev_Bsnss_Dte_N.setValue(pnd_Date_A_Pnd_Date_N);                                                                                                 //Natural: MOVE #DATE-N TO #PREV-BSNSS-DTE-N
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Prev_Bsnss_Dte_N.setValue(ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                                      //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #PREV-BSNSS-DTE-N
                }                                                                                                                                                         //Natural: END-IF
                pnd_Prev_Bsnss_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A);                                            //Natural: MOVE EDITED #PREV-BSNSS-DTE-A TO #PREV-BSNSS-DTE-D ( EM = YYYYMMDD )
                pnd_Today_Business_Date.setValueEdited(pnd_Prev_Bsnss_Dte_D,new ReportEditMask("YYYYMMDD"));                                                              //Natural: MOVE EDITED #PREV-BSNSS-DTE-D ( EM = YYYYMMDD ) TO #TODAY-BUSINESS-DATE
                if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                             //Natural: IF ADS-RPT-13 NE 0
                {
                    pnd_Prev_Accounting_Dte_Yyyymmdd.setValue(pnd_Date_A_Pnd_Date_N);                                                                                     //Natural: MOVE #DATE-N TO #PREV-ACCOUNTING-DTE-YYYYMMDD
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Prev_Accounting_Dte_Yyyymmdd.setValue(ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                          //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #PREV-ACCOUNTING-DTE-YYYYMMDD
                }                                                                                                                                                         //Natural: END-IF
                //* *  MOVE ADS-CNTL-BSNSS-TMRRW-DTE
                //* *    TO #PREV-ACCOUNTING-DTE-YYYYMMDD
                ldaAdsl1030.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc);                  //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-CC TO #ACCOUNTING-DATE-MCDY-CC
                ldaAdsl1030.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy);                  //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-YY TO #ACCOUNTING-DATE-MCDY-YY
                ldaAdsl1030.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm);                  //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-MM TO #ACCOUNTING-DATE-MCDY-MM
                ldaAdsl1030.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd);                  //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-DD TO #ACCOUNTING-DATE-MCDY-DD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Calculate_Tiaa_Monthly_Accumulations() throws Exception                                                                                              //Natural: CALCULATE-TIAA-MONTHLY-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  111803
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR02:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #MAX-RATES
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rates)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                        //Natural: IF #ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).notEquals(getZero())))                            //Natural: IF #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) NE 0
            {
                //*  TPA
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                {
                    ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Pay_Cnt().nadd(1);                                                                           //Natural: ADD 1 TO #M-TIAA-TPA-STD-PAY-CNT
                    ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Pay_Cnt().nadd(1);                                                                           //Natural: ADD 1 TO #G-TIAA-TPA-STD-PAY-CNT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                    {
                        ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Pay_Cnt().nadd(1);                                                                      //Natural: ADD 1 TO #M-TIAA-IPRO-STD-PAY-CNT
                        ldaAdsl1030.getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #M-TIAA-SUB-PAY-CNT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Pay_Cnt().nadd(1);                                                                      //Natural: ADD 1 TO #G-TIAA-IPRO-STD-PAY-CNT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #G-TIAA-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Pay_Cnt().nadd(1);                                                                           //Natural: ADD 1 TO #M-TIAA-STD-PAY-CNT
                        ldaAdsl1030.getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #M-TIAA-SUB-PAY-CNT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Pay_Cnt().nadd(1);                                                                           //Natural: ADD 1 TO #G-TIAA-STD-PAY-CNT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #G-TIAA-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1030.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Pay_Cnt().nadd(1);                                                                                 //Natural: ADD 1 TO #M-TOTAL-PAY-CNT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt().nadd(1);                                                                                 //Natural: ADD 1 TO #G-TOTAL-PAY-CNT
                //*  TPA
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                {
                    ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #M-TIAA-TPA-STD-GTD-AMT
                    ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-TPA-STD-GTD-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                    {
                        ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #M-TIAA-IPRO-STD-GTD-AMT
                        ldaAdsl1030.getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #M-TIAA-SUB-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-SUB-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-IPRO-STD-GTD-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #M-TIAA-STD-GTD-AMT
                        ldaAdsl1030.getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #M-TIAA-SUB-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-SUB-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-STD-GTD-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1030.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #M-TOTAL-GTD-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TOTAL-GTD-AMT
                //*  TPA
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                {
                    ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #M-TIAA-TPA-STD-DIV-AMT
                    ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-TPA-STD-DIV-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                    {
                        ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #M-TIAA-IPRO-STD-DIV-AMT
                        ldaAdsl1030.getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #M-TIAA-SUB-DIVID-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-DIVID-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-IPRO-STD-DIV-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #M-TIAA-STD-DIV-AMT
                        ldaAdsl1030.getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #M-TIAA-SUB-DIVID-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-DIVID-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-STD-DIV-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1030.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #M-TOTAL-DIV-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TOTAL-DIV-AMT
                //*  TPA
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                {
                    ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #M-TIAA-TPA-STD-FIN-GTD-AMT
                    ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TIAA-TPA-STD-FIN-GTD-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                    {
                        ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #M-TIAA-IPRO-STD-FIN-GTD-AMT
                        ldaAdsl1030.getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #M-TIAA-SUB-FIN-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TIAA-IPRO-STD-FIN-GTD-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #M-TIAA-STD-FIN-GTD-AMT
                        ldaAdsl1030.getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #M-TIAA-SUB-FIN-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TIAA-STD-FIN-GTD-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1030.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #M-TOTAL-FIN-GTD-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TOTAL-FIN-GTD-AMT
                //*  TPA
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                {
                    ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #M-TIAA-TPA-STD-FIN-DIV-AMT
                    ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-TPA-STD-FIN-DIV-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                    {
                        ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #M-TIAA-IPRO-STD-FIN-DIV-AMT
                        ldaAdsl1030.getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #M-TIAA-SUB-FIN-DIV-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-IPRO-STD-FIN-DIV-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #M-TIAA-STD-FIN-DIV-AMT
                        ldaAdsl1030.getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #M-TIAA-SUB-FIN-DIV-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-DIV-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-STD-FIN-DIV-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1030.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #M-TOTAL-FIN-DIV-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TOTAL-FIN-DIV-AMT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).notEquals(getZero())))                            //Natural: IF #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) NE 0
            {
                ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Pay_Cnt().nadd(1);                                                                                   //Natural: ADD 1 TO #M-TIAA-GRD-PAY-CNT
                ldaAdsl1030.getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #M-TIAA-SUB-PAY-CNT
                ldaAdsl1030.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Pay_Cnt().nadd(1);                                                                                 //Natural: ADD 1 TO #M-TOTAL-PAY-CNT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Pay_Cnt().nadd(1);                                                                                   //Natural: ADD 1 TO #G-TIAA-GRD-PAY-CNT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #G-TIAA-SUB-PAY-CNT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt().nadd(1);                                                                                 //Natural: ADD 1 TO #G-TOTAL-PAY-CNT
                ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #M-TIAA-GRD-GTD-AMT
                ldaAdsl1030.getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #M-TIAA-SUB-GTD-AMT
                ldaAdsl1030.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #M-TOTAL-GTD-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-GRD-GTD-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-SUB-GTD-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #G-TOTAL-GTD-AMT
                ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #M-TIAA-GRD-DIV-AMT
                ldaAdsl1030.getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #M-TIAA-SUB-DIVID-AMT
                ldaAdsl1030.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #M-TOTAL-DIV-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-GRD-DIV-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-DIVID-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #G-TOTAL-DIV-AMT
                ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #M-TIAA-GRD-FIN-GTD-AMT
                ldaAdsl1030.getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #M-TIAA-SUB-FIN-GTD-AMT
                ldaAdsl1030.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #M-TOTAL-FIN-GTD-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #G-TIAA-GRD-FIN-GTD-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-GTD-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #G-TOTAL-FIN-GTD-AMT
                ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #M-TIAA-GRD-FIN-DIV-AMT
                ldaAdsl1030.getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #M-TIAA-SUB-FIN-DIV-AMT
                ldaAdsl1030.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #M-TOTAL-FIN-DIV-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-GRD-FIN-DIV-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-DIV-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #G-TOTAL-FIN-DIV-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  TPA
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                                      //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
        {
            ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                   //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #M-TIAA-TPA-STD-FIN-IVC-AMT
            ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                   //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TIAA-TPA-STD-FIN-IVC-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  IPRO
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                                  //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
            {
                ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                              //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #M-TIAA-IPRO-STD-FIN-IVC-AMT
                ldaAdsl1030.getPnd_M_Tiaa_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                                          //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #M-TIAA-SUB-FIN-IVC-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                     //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TIAA-SUB-FIN-IVC-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                              //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TIAA-IPRO-STD-FIN-IVC-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                   //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #M-TIAA-STD-FIN-IVC-AMT
                ldaAdsl1030.getPnd_M_Tiaa_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                                          //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #M-TIAA-SUB-FIN-IVC-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                     //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TIAA-SUB-FIN-IVC-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                   //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TIAA-STD-FIN-IVC-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl1030.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                         //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #M-TOTAL-FIN-IVC-AMT
        ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                         //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TOTAL-FIN-IVC-AMT
    }
    private void sub_Calculate_Tiaa_Quartly_Accumulations() throws Exception                                                                                              //Natural: CALCULATE-TIAA-QUARTLY-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  111803
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR03:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #MAX-RATES
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rates)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                        //Natural: IF #ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).notEquals(getZero())))                            //Natural: IF #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) NE 0
            {
                //*  TPA
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                {
                    ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Pay_Cnt().nadd(1);                                                                           //Natural: ADD 1 TO #Q-TIAA-TPA-STD-PAY-CNT
                    ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Pay_Cnt().nadd(1);                                                                           //Natural: ADD 1 TO #G-TIAA-TPA-STD-PAY-CNT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                    {
                        ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Pay_Cnt().nadd(1);                                                                      //Natural: ADD 1 TO #Q-TIAA-IPRO-STD-PAY-CNT
                        ldaAdsl1031.getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #Q-TIAA-SUB-PAY-CNT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #G-TIAA-SUB-PAY-CNT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Pay_Cnt().nadd(1);                                                                      //Natural: ADD 1 TO #G-TIAA-IPRO-STD-PAY-CNT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Pay_Cnt().nadd(1);                                                                           //Natural: ADD 1 TO #Q-TIAA-STD-PAY-CNT
                        ldaAdsl1031.getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #Q-TIAA-SUB-PAY-CNT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #G-TIAA-SUB-PAY-CNT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Pay_Cnt().nadd(1);                                                                           //Natural: ADD 1 TO #G-TIAA-STD-PAY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1031.getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Pay_Cnt().nadd(1);                                                                                 //Natural: ADD 1 TO #Q-TOTAL-PAY-CNT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt().nadd(1);                                                                                 //Natural: ADD 1 TO #G-TOTAL-PAY-CNT
                //*  TPA
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                {
                    ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #Q-TIAA-TPA-STD-GTD-AMT
                    ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-TPA-STD-GTD-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                    {
                        ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #Q-TIAA-IPRO-STD-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-IPRO-STD-GTD-AMT
                        ldaAdsl1031.getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #Q-TIAA-SUB-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-SUB-GTD-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #Q-TIAA-STD-GTD-AMT
                        ldaAdsl1031.getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #Q-TIAA-SUB-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-SUB-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-STD-GTD-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1031.getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #Q-TOTAL-GTD-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TOTAL-GTD-AMT
                //*  TPA
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                {
                    ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #Q-TIAA-TPA-STD-DIV-AMT
                    ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-TPA-STD-DIV-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                    {
                        ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #Q-TIAA-IPRO-STD-DIV-AMT
                        ldaAdsl1031.getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #Q-TIAA-SUB-DIVID-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-DIVID-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-IPRO-STD-DIV-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #Q-TIAA-STD-DIV-AMT
                        ldaAdsl1031.getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #Q-TIAA-SUB-DIVID-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-DIVID-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-STD-DIV-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1031.getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #Q-TOTAL-DIV-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TOTAL-DIV-AMT
                //*  TPA
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                {
                    ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #Q-TIAA-TPA-STD-FIN-GTD-AMT
                    ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TIAA-TPA-STD-FIN-GTD-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                    {
                        ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #Q-TIAA-IPRO-STD-FIN-GTD-AMT
                        ldaAdsl1031.getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #Q-TIAA-SUB-FIN-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TIAA-IPRO-STD-FIN-GTD-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #Q-TIAA-STD-FIN-GTD-AMT
                        ldaAdsl1031.getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #Q-TIAA-SUB-FIN-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TIAA-STD-FIN-GTD-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1031.getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #Q-TOTAL-FIN-GTD-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TOTAL-FIN-GTD-AMT
                //*  TPA
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                {
                    ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #Q-TIAA-TPA-STD-FIN-DIV-AMT
                    ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-TPA-STD-FIN-DIV-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                    {
                        ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #Q-TIAA-IPRO-STD-FIN-DIV-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-IPRO-STD-FIN-DIV-AMT
                        ldaAdsl1031.getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #Q-TIAA-SUB-FIN-DIV-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-DIV-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #Q-TIAA-STD-FIN-DIV-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-STD-FIN-DIV-AMT
                        ldaAdsl1031.getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #Q-TIAA-SUB-FIN-DIV-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-DIV-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1031.getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #Q-TOTAL-FIN-DIV-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TOTAL-FIN-DIV-AMT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).notEquals(getZero())))                            //Natural: IF #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) NE 0
            {
                ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Pay_Cnt().nadd(1);                                                                                   //Natural: ADD 1 TO #Q-TIAA-GRD-PAY-CNT
                ldaAdsl1031.getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #Q-TIAA-SUB-PAY-CNT
                ldaAdsl1031.getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Pay_Cnt().nadd(1);                                                                                 //Natural: ADD 1 TO #Q-TOTAL-PAY-CNT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Pay_Cnt().nadd(1);                                                                                   //Natural: ADD 1 TO #G-TIAA-GRD-PAY-CNT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #G-TIAA-SUB-PAY-CNT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt().nadd(1);                                                                                 //Natural: ADD 1 TO #G-TOTAL-PAY-CNT
                ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #Q-TIAA-GRD-GTD-AMT
                ldaAdsl1031.getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #Q-TIAA-SUB-GTD-AMT
                ldaAdsl1031.getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #Q-TOTAL-GTD-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-GRD-GTD-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-SUB-GTD-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #G-TOTAL-GTD-AMT
                ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #Q-TIAA-GRD-DIV-AMT
                ldaAdsl1031.getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #Q-TIAA-SUB-DIVID-AMT
                ldaAdsl1031.getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #Q-TOTAL-DIV-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-GRD-DIV-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-DIVID-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #G-TOTAL-DIV-AMT
                ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #Q-TIAA-GRD-FIN-GTD-AMT
                ldaAdsl1031.getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #Q-TIAA-SUB-FIN-GTD-AMT
                ldaAdsl1031.getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #Q-TOTAL-FIN-GTD-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #G-TIAA-GRD-FIN-GTD-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-GTD-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #G-TOTAL-FIN-GTD-AMT
                ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #Q-TIAA-GRD-FIN-DIV-AMT
                ldaAdsl1031.getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #Q-TIAA-SUB-FIN-DIV-AMT
                ldaAdsl1031.getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #Q-TOTAL-FIN-DIV-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-GRD-FIN-DIV-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-DIV-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #G-TOTAL-FIN-DIV-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  TPA
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                                      //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
        {
            ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                   //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #Q-TIAA-TPA-STD-FIN-IVC-AMT
            ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                   //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TIAA-TPA-STD-FIN-IVC-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  IPRO
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                                  //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
            {
                ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                              //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #Q-TIAA-IPRO-STD-FIN-IVC-AMT
                ldaAdsl1031.getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                     //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #Q-TIAA-SUB-FIN-IVC-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                     //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TIAA-SUB-FIN-IVC-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                              //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TIAA-IPRO-STD-FIN-IVC-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                   //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #Q-TIAA-STD-FIN-IVC-AMT
                ldaAdsl1031.getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                     //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #Q-TIAA-SUB-FIN-IVC-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                     //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TIAA-SUB-FIN-IVC-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                   //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TIAA-STD-FIN-IVC-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                         //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TOTAL-FIN-IVC-AMT
        ldaAdsl1031.getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                         //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #Q-TOTAL-FIN-IVC-AMT
    }
    private void sub_Calculate_Tiaa_Semi_Annual_Accumulations() throws Exception                                                                                          //Natural: CALCULATE-TIAA-SEMI-ANNUAL-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  111803
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR04:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #MAX-RATES
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rates)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                        //Natural: IF #ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).notEquals(getZero())))                            //Natural: IF #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) NE 0
            {
                //*  TPA
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                {
                    ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Pay_Cnt().nadd(1);                                                                           //Natural: ADD 1 TO #S-TIAA-TPA-STD-PAY-CNT
                    ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Pay_Cnt().nadd(1);                                                                           //Natural: ADD 1 TO #G-TIAA-TPA-STD-PAY-CNT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                    {
                        ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Pay_Cnt().nadd(1);                                                                      //Natural: ADD 1 TO #S-TIAA-IPRO-STD-PAY-CNT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Pay_Cnt().nadd(1);                                                                      //Natural: ADD 1 TO #G-TIAA-IPRO-STD-PAY-CNT
                        ldaAdsl1032.getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #S-TIAA-SUB-PAY-CNT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #G-TIAA-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Pay_Cnt().nadd(1);                                                                           //Natural: ADD 1 TO #G-TIAA-STD-PAY-CNT
                        ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Pay_Cnt().nadd(1);                                                                           //Natural: ADD 1 TO #S-TIAA-STD-PAY-CNT
                        ldaAdsl1032.getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #S-TIAA-SUB-PAY-CNT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #G-TIAA-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1032.getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Pay_Cnt().nadd(1);                                                                                 //Natural: ADD 1 TO #S-TOTAL-PAY-CNT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt().nadd(1);                                                                                 //Natural: ADD 1 TO #G-TOTAL-PAY-CNT
                //*  TPA
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                {
                    ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #S-TIAA-TPA-STD-GTD-AMT
                    ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-TPA-STD-GTD-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                    {
                        ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #S-TIAA-IPRO-STD-GTD-AMT
                        ldaAdsl1032.getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #S-TIAA-SUB-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-SUB-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-IPRO-STD-GTD-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #S-TIAA-STD-GTD-AMT
                        ldaAdsl1032.getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #S-TIAA-SUB-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-SUB-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-STD-GTD-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1032.getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #S-TOTAL-GTD-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TOTAL-GTD-AMT
                //*  TPA
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                {
                    ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #S-TIAA-TPA-STD-DIV-AMT
                    ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-TPA-STD-DIV-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                    {
                        ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #S-TIAA-IPRO-STD-DIV-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-IPRO-STD-DIV-AMT
                        ldaAdsl1032.getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #S-TIAA-SUB-DIVID-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-DIVID-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #S-TIAA-STD-DIV-AMT
                        ldaAdsl1032.getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #S-TIAA-SUB-DIVID-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-STD-DIV-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-DIVID-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1032.getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #S-TOTAL-DIV-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TOTAL-DIV-AMT
                //*  TPA
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                {
                    ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #S-TIAA-TPA-STD-FIN-GTD-AMT
                    ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TIAA-TPA-STD-FIN-GTD-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                    {
                        ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #S-TIAA-IPRO-STD-FIN-GTD-AMT
                        ldaAdsl1032.getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #S-TIAA-SUB-FIN-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TIAA-TPA-STD-FIN-GTD-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #S-TIAA-STD-FIN-GTD-AMT
                        ldaAdsl1032.getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #S-TIAA-SUB-FIN-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TIAA-STD-FIN-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-GTD-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1032.getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #S-TOTAL-FIN-GTD-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TOTAL-FIN-GTD-AMT
                //*  TPA
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                {
                    ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #S-TIAA-TPA-STD-FIN-DIV-AMT
                    ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-TPA-STD-FIN-DIV-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                    {
                        ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #S-TIAA-TPA-STD-FIN-DIV-AMT
                        ldaAdsl1032.getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #S-TIAA-SUB-FIN-DIV-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-DIV-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-IPRO-STD-FIN-DIV-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #S-TIAA-STD-FIN-DIV-AMT
                        ldaAdsl1032.getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #S-TIAA-SUB-FIN-DIV-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-STD-FIN-DIV-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-DIV-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1032.getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #S-TOTAL-FIN-DIV-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TOTAL-FIN-DIV-AMT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).notEquals(getZero())))                            //Natural: IF #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) NE 0
            {
                ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Pay_Cnt().nadd(1);                                                                                   //Natural: ADD 1 TO #S-TIAA-GRD-PAY-CNT
                ldaAdsl1032.getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #S-TIAA-SUB-PAY-CNT
                ldaAdsl1032.getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Pay_Cnt().nadd(1);                                                                                 //Natural: ADD 1 TO #S-TOTAL-PAY-CNT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Pay_Cnt().nadd(1);                                                                                   //Natural: ADD 1 TO #G-TIAA-GRD-PAY-CNT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #G-TIAA-SUB-PAY-CNT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt().nadd(1);                                                                                 //Natural: ADD 1 TO #G-TOTAL-PAY-CNT
                ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #S-TIAA-GRD-GTD-AMT
                ldaAdsl1032.getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #S-TIAA-SUB-GTD-AMT
                ldaAdsl1032.getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #S-TOTAL-GTD-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-GRD-GTD-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-SUB-GTD-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #G-TOTAL-GTD-AMT
                ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #S-TIAA-GRD-DIV-AMT
                ldaAdsl1032.getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #S-TIAA-SUB-DIVID-AMT
                ldaAdsl1032.getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #S-TOTAL-DIV-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-GRD-DIV-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-DIVID-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #G-TOTAL-DIV-AMT
                ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #S-TIAA-GRD-FIN-GTD-AMT
                ldaAdsl1032.getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #S-TIAA-SUB-FIN-GTD-AMT
                ldaAdsl1032.getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #S-TOTAL-FIN-GTD-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #G-TIAA-GRD-FIN-GTD-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-GTD-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #G-TOTAL-FIN-GTD-AMT
                ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #S-TIAA-GRD-FIN-DIV-AMT
                ldaAdsl1032.getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #S-TIAA-SUB-FIN-DIV-AMT
                ldaAdsl1032.getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #S-TOTAL-FIN-DIV-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-GRD-FIN-DIV-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-DIV-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #G-TOTAL-FIN-DIV-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  TPA
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                                      //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
        {
            ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                   //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #S-TIAA-TPA-STD-FIN-IVC-AMT
            ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                   //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TIAA-TPA-STD-FIN-IVC-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  IPRO
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                                  //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
            {
                ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                              //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #S-TIAA-IPRO-STD-FIN-IVC-AMT
                ldaAdsl1032.getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                     //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #S-TIAA-SUB-FIN-IVC-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                     //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TIAA-SUB-FIN-IVC-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                              //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TIAA-IPRO-STD-FIN-IVC-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                   //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #S-TIAA-STD-FIN-IVC-AMT
                ldaAdsl1032.getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                     //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #S-TIAA-SUB-FIN-IVC-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                     //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TIAA-SUB-FIN-IVC-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                   //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TIAA-STD-FIN-IVC-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl1032.getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                         //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #S-TOTAL-FIN-IVC-AMT
        ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                         //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TOTAL-FIN-IVC-AMT
    }
    private void sub_Calculate_Tiaa_Annual_Accumulations() throws Exception                                                                                               //Natural: CALCULATE-TIAA-ANNUAL-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  111803
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR05:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #MAX-RATES
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rates)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                        //Natural: IF #ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).notEquals(getZero())))                            //Natural: IF #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) NE 0
            {
                //*  TPA
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                {
                    ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Pay_Cnt().nadd(1);                                                                           //Natural: ADD 1 TO #A-TIAA-TPA-STD-PAY-CNT
                    ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Pay_Cnt().nadd(1);                                                                           //Natural: ADD 1 TO #G-TIAA-TPA-STD-PAY-CNT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                    {
                        ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Pay_Cnt().nadd(1);                                                                      //Natural: ADD 1 TO #A-TIAA-IPRO-STD-PAY-CNT
                        ldaAdsl1033.getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #A-TIAA-SUB-PAY-CNT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Pay_Cnt().nadd(1);                                                                      //Natural: ADD 1 TO #G-TIAA-IPRO-STD-PAY-CNT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #G-TIAA-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Pay_Cnt().nadd(1);                                                                           //Natural: ADD 1 TO #A-TIAA-STD-PAY-CNT
                        ldaAdsl1033.getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #A-TIAA-SUB-PAY-CNT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #G-TIAA-SUB-PAY-CNT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Pay_Cnt().nadd(1);                                                                           //Natural: ADD 1 TO #G-TIAA-STD-PAY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1033.getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Pay_Cnt().nadd(1);                                                                                 //Natural: ADD 1 TO #A-TOTAL-PAY-CNT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt().nadd(1);                                                                                 //Natural: ADD 1 TO #G-TOTAL-PAY-CNT
                //*  TPA
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                {
                    ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #A-TIAA-TPA-STD-GTD-AMT
                    ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-TPA-STD-GTD-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                    {
                        ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #A-TIAA-IPRO-STD-GTD-AMT
                        ldaAdsl1033.getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #A-TIAA-SUB-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-SUB-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-TPA-STD-GTD-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #A-TIAA-STD-GTD-AMT
                        ldaAdsl1033.getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #A-TIAA-SUB-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-SUB-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-STD-GTD-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1033.getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #A-TOTAL-GTD-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #G-TOTAL-GTD-AMT
                //*  TPA
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                {
                    ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #A-TIAA-TPA-STD-DIV-AMT
                    ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-TPA-STD-DIV-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                    {
                        ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #A-TIAA-IPRO-STD-DIV-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-IPRO-STD-DIV-AMT
                        ldaAdsl1033.getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #A-TIAA-SUB-DIVID-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-DIVID-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #A-TIAA-STD-DIV-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-STD-DIV-AMT
                        ldaAdsl1033.getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #A-TIAA-SUB-DIVID-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-DIVID-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1033.getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #A-TOTAL-DIV-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #G-TOTAL-DIV-AMT
                //*  TPA
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                {
                    ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #A-TIAA-TPA-STD-FIN-GTD-AMT
                    ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TIAA-TPA-STD-FIN-GTD-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                    {
                        ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #A-TIAA-IPRO-STD-FIN-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TIAA-IPRO-STD-FIN-GTD-AMT
                        ldaAdsl1033.getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #A-TIAA-SUB-FIN-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-GTD-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #A-TIAA-STD-FIN-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TIAA-STD-FIN-GTD-AMT
                        ldaAdsl1033.getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #A-TIAA-SUB-FIN-GTD-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-GTD-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1033.getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #A-TOTAL-FIN-GTD-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #G-TOTAL-FIN-GTD-AMT
                //*  TPA
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                {
                    ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #A-TIAA-TPA-STD-FIN-DIV-AMT
                    ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-TPA-STD-FIN-DIV-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                    {
                        ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #A-TIAA-IPRO-STD-FIN-DIV-AMT
                        ldaAdsl1033.getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #A-TIAA-SUB-FIN-DIV-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-DIV-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-IPRO-STD-FIN-DIV-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #A-TIAA-STD-FIN-DIV-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-DIV-AMT
                        ldaAdsl1033.getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #A-TIAA-SUB-FIN-DIV-AMT
                        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-STD-FIN-DIV-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1033.getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #A-TOTAL-FIN-DIV-AMT
                ldaAdsl1033.getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #A-TOTAL-FIN-DIV-AMT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).notEquals(getZero())))                            //Natural: IF #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) NE 0
            {
                ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Pay_Cnt().nadd(1);                                                                                   //Natural: ADD 1 TO #A-TIAA-GRD-PAY-CNT
                ldaAdsl1033.getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #A-TIAA-SUB-PAY-CNT
                ldaAdsl1033.getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Pay_Cnt().nadd(1);                                                                                 //Natural: ADD 1 TO #A-TOTAL-PAY-CNT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Pay_Cnt().nadd(1);                                                                                   //Natural: ADD 1 TO #G-TIAA-GRD-PAY-CNT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Pay_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #G-TIAA-SUB-PAY-CNT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt().nadd(1);                                                                                 //Natural: ADD 1 TO #G-TOTAL-PAY-CNT
                ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #A-TIAA-GRD-GTD-AMT
                ldaAdsl1033.getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #A-TIAA-SUB-GTD-AMT
                ldaAdsl1033.getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #A-TOTAL-GTD-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-GRD-GTD-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #G-TIAA-SUB-GTD-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #G-TOTAL-GTD-AMT
                ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #A-TIAA-GRD-DIV-AMT
                ldaAdsl1033.getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #A-TIAA-SUB-DIVID-AMT
                ldaAdsl1033.getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #A-TOTAL-DIV-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-GRD-DIV-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Divid_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-DIVID-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #G-TOTAL-DIV-AMT
                ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #A-TIAA-GRD-FIN-GTD-AMT
                ldaAdsl1033.getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #A-TIAA-SUB-FIN-GTD-AMT
                ldaAdsl1033.getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #A-TOTAL-FIN-GTD-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #G-TIAA-GRD-FIN-GTD-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-GTD-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Gtd_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #G-TOTAL-FIN-GTD-AMT
                ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #A-TIAA-GRD-FIN-DIV-AMT
                ldaAdsl1033.getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #A-TIAA-SUB-FIN-DIV-AMT
                ldaAdsl1033.getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #A-TOTAL-FIN-DIV-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-GRD-FIN-DIV-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #G-TIAA-SUB-FIN-DIV-AMT
                ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Div_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #G-TOTAL-FIN-DIV-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  TPA
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                                      //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
        {
            ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                   //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #A-TIAA-TPA-STD-FIN-IVC-AMT
            ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                   //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TIAA-TPA-STD-FIN-IVC-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  IPRO
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                                  //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
            {
                ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                               //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #A-TIAA-TPA-STD-FIN-IVC-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                               //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TIAA-TPA-STD-FIN-IVC-AMT
                ldaAdsl1033.getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                     //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #A-TIAA-SUB-FIN-IVC-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                     //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TIAA-SUB-FIN-IVC-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                   //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #A-TIAA-STD-FIN-IVC-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                   //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TIAA-STD-FIN-IVC-AMT
                ldaAdsl1033.getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                     //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #A-TIAA-SUB-FIN-IVC-AMT
                ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                     //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TIAA-SUB-FIN-IVC-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl1033.getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                         //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #A-TOTAL-FIN-IVC-AMT
        ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                         //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO #G-TOTAL-FIN-IVC-AMT
    }
    private void sub_Calculate_Cref_Monthly_Accumulations() throws Exception                                                                                              //Natural: CALCULATE-CREF-MONTHLY-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Cref_Prod_Found.setValue(false);                                                                                                                              //Natural: MOVE FALSE TO #CREF-PROD-FOUND
        FOR06:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                              //Natural: IF #PEC-ACCT-NAME-1 ( #B-INDX ) = ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue(pnd_All_Indx_Pnd_B_Indx).equals(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx)))) //Natural: IF #PEC-ACCT-NAME-1 ( #B-INDX ) = ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX )
            {
                pnd_Cref_Prod_Found.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO #CREF-PROD-FOUND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cref_Prod_Found.setValue(false);                                                                                                                      //Natural: MOVE FALSE TO #CREF-PROD-FOUND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (condition((ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero()) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero())))) //Natural: IF ( ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) NE 0 OR ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) NE 0 )
                {
                    ldaAdsl1030.getPnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                               //Natural: ADD 1 TO #M-C-MTH-PAY-CNT ( #B-INDX )
                    //*   ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1030.getPnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Pay_Cnt().nadd(1);                                                               //Natural: ADD 1 TO #M-CREF-MTH-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1030.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #M-TOTAL-PAY-CNT
                    ldaAdsl1034.getPnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Pay_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                               //Natural: ADD 1 TO #G-C-MTH-PAY-CNT ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Pay_Cnt().nadd(1);                                                               //Natural: ADD 1 TO #G-CREF-MTH-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #G-TOTAL-PAY-CNT
                    ldaAdsl1030.getPnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #M-C-MTH-UNITS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1030.getPnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #M-CREF-MTH-SUB-UNITS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1030.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #M-TOTAL-UNITS
                    ldaAdsl1034.getPnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Units().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #G-C-MTH-UNITS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #G-CREF-MTH-SUB-UNITS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #G-TOTAL-UNITS
                    ldaAdsl1030.getPnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #M-C-MTH-DOLLARS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1030.getPnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #M-CREF-MTH-SUB-DOLLARS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1030.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #M-TOTAL-DOLLARS
                    ldaAdsl1034.getPnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Dollars().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #G-C-MTH-DOLLARS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #G-CREF-MTH-SUB-DOLLARS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #G-TOTAL-DOLLARS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (condition((ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero()) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero())))) //Natural: IF ( ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) NE 0 OR ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) NE 0 )
                {
                    ldaAdsl1030.getPnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                               //Natural: ADD 1 TO #M-C-ANN-PAY-CNT ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1030.getPnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Pay_Cnt().nadd(1);                                                               //Natural: ADD 1 TO #M-CREF-ANN-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1030.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #M-TOTAL-PAY-CNT
                    ldaAdsl1034.getPnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Pay_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                               //Natural: ADD 1 TO #G-C-ANN-PAY-CNT ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Pay_Cnt().nadd(1);                                                               //Natural: ADD 1 TO #G-CREF-ANN-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #G-TOTAL-PAY-CNT
                    ldaAdsl1030.getPnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #M-C-ANN-UNITS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1030.getPnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #M-CREF-ANN-SUB-UNITS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1030.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #M-TOTAL-UNITS
                    ldaAdsl1034.getPnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Units().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #G-C-ANN-UNITS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #G-CREF-ANN-SUB-UNITS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #G-TOTAL-UNITS
                    ldaAdsl1030.getPnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #M-C-ANN-DOLLARS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1030.getPnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #M-CREF-ANN-SUB-DOLLARS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1030.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #M-TOTAL-DOLLARS
                    ldaAdsl1034.getPnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Dollars().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #G-C-ANN-DOLLARS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #G-CREF-ANN-SUB-DOLLARS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #G-TOTAL-DOLLARS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Calculate_Monthly_Ivc() throws Exception                                                                                                             //Natural: CALCULATE-MONTHLY-IVC
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaAdsl1030.getPnd_M_C_Mth_Fin_Ivc_Amount_Pnd_M_C_Mth_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                                          //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #M-C-MTH-FIN-IVC-AMT
        ldaAdsl1030.getPnd_M_Cref_Mth_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                                                              //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #M-CREF-MTH-SUB-FIN-IVC-AMT
        ldaAdsl1030.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                                         //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #M-TOTAL-FIN-IVC-AMT
        ldaAdsl1034.getPnd_G_C_Mth_Fin_Ivc_Amount_Pnd_G_C_Mth_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                                          //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #G-C-MTH-FIN-IVC-AMT
        ldaAdsl1034.getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                               //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #G-CREF-MTH-SUB-FIN-IVC-AMT
        ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                                         //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #G-TOTAL-FIN-IVC-AMT
    }
    private void sub_Calculate_Cref_Quartly_Accumulations() throws Exception                                                                                              //Natural: CALCULATE-CREF-QUARTLY-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Cref_Prod_Found.setValue(false);                                                                                                                              //Natural: MOVE FALSE TO #CREF-PROD-FOUND
        FOR07:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                              //Natural: IF #PEC-ACCT-NAME-1 ( #B-INDX ) = ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue(pnd_All_Indx_Pnd_B_Indx).equals(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx)))) //Natural: IF #PEC-ACCT-NAME-1 ( #B-INDX ) = ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX )
            {
                pnd_Cref_Prod_Found.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO #CREF-PROD-FOUND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cref_Prod_Found.setValue(false);                                                                                                                      //Natural: MOVE FALSE TO #CREF-PROD-FOUND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (condition((ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero()) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero())))) //Natural: IF ( ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) NE 0 OR ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) NE 0 )
                {
                    ldaAdsl1031.getPnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Pay_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                               //Natural: ADD 1 TO #Q-C-MTH-PAY-CNT ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1031.getPnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Pay_Cnt().nadd(1);                                                               //Natural: ADD 1 TO #Q-CREF-MTH-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1031.getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #Q-TOTAL-PAY-CNT
                    ldaAdsl1034.getPnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Pay_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                               //Natural: ADD 1 TO #G-C-MTH-PAY-CNT ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Pay_Cnt().nadd(1);                                                               //Natural: ADD 1 TO #G-CREF-MTH-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #G-TOTAL-PAY-CNT
                    ldaAdsl1031.getPnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Units().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #Q-C-MTH-UNITS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1031.getPnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #Q-CREF-MTH-SUB-UNITS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1031.getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #Q-TOTAL-UNITS
                    ldaAdsl1034.getPnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Units().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #G-C-MTH-UNITS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #G-CREF-MTH-SUB-UNITS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #G-TOTAL-UNITS
                    ldaAdsl1031.getPnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Dollars().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #Q-C-MTH-DOLLARS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1031.getPnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #Q-CREF-MTH-SUB-DOLLARS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1031.getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #Q-TOTAL-DOLLARS
                    ldaAdsl1034.getPnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Dollars().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #G-C-MTH-DOLLARS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #G-CREF-MTH-SUB-DOLLARS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #G-TOTAL-DOLLARS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (condition((ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero()) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero())))) //Natural: IF ( ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) NE 0 OR ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) NE 0 )
                {
                    ldaAdsl1031.getPnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Pay_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                               //Natural: ADD 1 TO #Q-C-ANN-PAY-CNT ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1031.getPnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Pay_Cnt().nadd(1);                                                               //Natural: ADD 1 TO #Q-CREF-ANN-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1031.getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #Q-TOTAL-PAY-CNT
                    ldaAdsl1034.getPnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Pay_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                               //Natural: ADD 1 TO #G-C-ANN-PAY-CNT ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Pay_Cnt().nadd(1);                                                               //Natural: ADD 1 TO #G-CREF-ANN-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #G-TOTAL-PAY-CNT
                    ldaAdsl1031.getPnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Units().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #Q-C-ANN-UNITS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1031.getPnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #Q-CREF-ANN-SUB-UNITS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1031.getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #Q-TOTAL-UNITS
                    ldaAdsl1034.getPnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Units().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #G-C-ANN-UNITS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #G-CREF-ANN-SUB-UNITS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #G-TOTAL-UNITS
                    ldaAdsl1031.getPnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Dollars().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #Q-C-ANN-DOLLARS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1031.getPnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #Q-CREF-ANN-SUB-DOLLARS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1031.getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #Q-TOTAL-DOLLARS
                    ldaAdsl1034.getPnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Dollars().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #G-C-ANN-DOLLARS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #G-CREF-ANN-SUB-DOLLARS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #G-TOTAL-DOLLARS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Calculate_Quartly_Ivc() throws Exception                                                                                                             //Natural: CALCULATE-QUARTLY-IVC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaAdsl1031.getPnd_Q_C_Mth_Fin_Ivc_Amount_Pnd_Q_C_Mth_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                                          //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #Q-C-MTH-FIN-IVC-AMT
        ldaAdsl1031.getPnd_Q_Cref_Mth_Sub_Fin_Ivc_Amount_Pnd_Q_Cref_Mth_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                            //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #Q-CREF-MTH-SUB-FIN-IVC-AMT
        ldaAdsl1031.getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                                         //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #Q-TOTAL-FIN-IVC-AMT
        ldaAdsl1034.getPnd_G_C_Mth_Fin_Ivc_Amount_Pnd_G_C_Mth_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                                          //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #G-C-MTH-FIN-IVC-AMT
        ldaAdsl1034.getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                               //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #G-CREF-MTH-SUB-FIN-IVC-AMT
        ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                                         //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #G-TOTAL-FIN-IVC-AMT
    }
    private void sub_Calculate_Cref_Semi_Annual_Accumulations() throws Exception                                                                                          //Natural: CALCULATE-CREF-SEMI-ANNUAL-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Cref_Prod_Found.setValue(false);                                                                                                                              //Natural: MOVE FALSE TO #CREF-PROD-FOUND
        FOR08:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                              //Natural: IF #PEC-ACCT-NAME-1 ( #B-INDX ) = ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue(pnd_All_Indx_Pnd_B_Indx).equals(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx)))) //Natural: IF #PEC-ACCT-NAME-1 ( #B-INDX ) = ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX )
            {
                pnd_Cref_Prod_Found.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO #CREF-PROD-FOUND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cref_Prod_Found.setValue(false);                                                                                                                      //Natural: MOVE FALSE TO #CREF-PROD-FOUND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (condition((ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero()) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero())))) //Natural: IF ( ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) NE 0 OR ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) NE 0 )
                {
                    ldaAdsl1032.getPnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                               //Natural: ADD 1 TO #S-C-MTH-PAY-CNT ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1032.getPnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Pay_Cnt().nadd(1);                                                               //Natural: ADD 1 TO #S-CREF-MTH-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1032.getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #S-TOTAL-PAY-CNT
                    ldaAdsl1034.getPnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Pay_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                               //Natural: ADD 1 TO #G-C-MTH-PAY-CNT ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Pay_Cnt().nadd(1);                                                               //Natural: ADD 1 TO #G-CREF-MTH-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #G-TOTAL-PAY-CNT
                    ldaAdsl1032.getPnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #S-C-MTH-UNITS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1032.getPnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #S-CREF-MTH-SUB-UNITS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1032.getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #S-TOTAL-UNITS
                    ldaAdsl1034.getPnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Units().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #G-C-MTH-UNITS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #G-CREF-MTH-SUB-UNITS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #G-TOTAL-UNITS
                    ldaAdsl1032.getPnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #S-C-MTH-DOLLARS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1032.getPnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #S-CREF-MTH-SUB-DOLLARS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1032.getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #S-TOTAL-DOLLARS
                    ldaAdsl1034.getPnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Dollars().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #G-C-MTH-DOLLARS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #G-CREF-MTH-SUB-DOLLARS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #G-TOTAL-DOLLARS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (condition((ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero()) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero())))) //Natural: IF ( ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) NE 0 OR ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) NE 0 )
                {
                    ldaAdsl1032.getPnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                               //Natural: ADD 1 TO #S-C-ANN-PAY-CNT ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1032.getPnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Pay_Cnt().nadd(1);                                                               //Natural: ADD 1 TO #S-CREF-ANN-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1032.getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #S-TOTAL-PAY-CNT
                    ldaAdsl1034.getPnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Pay_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                               //Natural: ADD 1 TO #G-C-ANN-PAY-CNT ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Pay_Cnt().nadd(1);                                                               //Natural: ADD 1 TO #G-CREF-ANN-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #G-TOTAL-PAY-CNT
                    ldaAdsl1032.getPnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #S-C-ANN-UNITS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1032.getPnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #S-CREF-ANN-SUB-UNITS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1032.getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #S-TOTAL-UNITS
                    ldaAdsl1034.getPnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Units().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #G-C-ANN-UNITS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #G-CREF-ANN-SUB-UNITS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #G-TOTAL-UNITS
                    ldaAdsl1032.getPnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #S-C-ANN-DOLLARS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1032.getPnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #S-CREF-ANN-SUB-DOLLARS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1032.getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #S-TOTAL-DOLLARS
                    ldaAdsl1034.getPnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Dollars().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #G-C-ANN-DOLLARS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #G-CREF-ANN-SUB-DOLLARS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #G-TOTAL-DOLLARS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Calculate_Semi_Annuall_Ivc() throws Exception                                                                                                        //Natural: CALCULATE-SEMI-ANNUALL-IVC
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        ldaAdsl1032.getPnd_S_C_Mth_Fin_Ivc_Amount_Pnd_S_C_Mth_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                                          //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #S-C-MTH-FIN-IVC-AMT
        ldaAdsl1032.getPnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                               //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #S-CREF-MTH-SUB-FIN-IVC-AMT
        ldaAdsl1032.getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                                         //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #S-TOTAL-FIN-IVC-AMT
        ldaAdsl1034.getPnd_G_C_Mth_Fin_Ivc_Amount_Pnd_G_C_Mth_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                                          //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #G-C-MTH-FIN-IVC-AMT
        ldaAdsl1034.getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                               //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #G-CREF-MTH-SUB-FIN-IVC-AMT
        ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                                         //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #G-TOTAL-FIN-IVC-AMT
    }
    private void sub_Calculate_Cref_Annual_Accumulations() throws Exception                                                                                               //Natural: CALCULATE-CREF-ANNUAL-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Cref_Prod_Found.setValue(false);                                                                                                                              //Natural: MOVE FALSE TO #CREF-PROD-FOUND
        FOR09:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                              //Natural: IF #PEC-ACCT-NAME-1 ( #B-INDX ) = ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue(pnd_All_Indx_Pnd_B_Indx).equals(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx)))) //Natural: IF #PEC-ACCT-NAME-1 ( #B-INDX ) = ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX )
            {
                pnd_Cref_Prod_Found.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO #CREF-PROD-FOUND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cref_Prod_Found.setValue(false);                                                                                                                      //Natural: MOVE FALSE TO #CREF-PROD-FOUND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (condition((ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero()) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero())))) //Natural: IF ( ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) NE 0 OR ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) NE 0 )
                {
                    ldaAdsl1033.getPnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                               //Natural: ADD 1 TO #A-C-MTH-PAY-CNT ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1033.getPnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Pay_Cnt().nadd(1);                                                               //Natural: ADD 1 TO #A-CREF-MTH-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1033.getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #A-TOTAL-PAY-CNT
                    ldaAdsl1034.getPnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Pay_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                               //Natural: ADD 1 TO #G-C-MTH-PAY-CNT ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Pay_Cnt().nadd(1);                                                               //Natural: ADD 1 TO #G-CREF-MTH-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #G-TOTAL-PAY-CNT
                    ldaAdsl1033.getPnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #A-C-MTH-UNITS ( #B-INDX )
                    //*   ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1033.getPnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #A-CREF-MTH-SUB-UNITS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1033.getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #A-TOTAL-UNITS
                    ldaAdsl1034.getPnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Units().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #G-C-MTH-UNITS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #G-CREF-MTH-SUB-UNITS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A-INDX ) TO #G-TOTAL-UNITS
                    ldaAdsl1033.getPnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #A-C-MTH-DOLLARS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1033.getPnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #A-CREF-MTH-SUB-DOLLARS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1033.getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #A-TOTAL-DOLLARS
                    ldaAdsl1034.getPnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Dollars().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #G-C-MTH-DOLLARS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #G-CREF-MTH-SUB-DOLLARS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A-INDX ) TO #G-TOTAL-DOLLARS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (condition((ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero()) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero())))) //Natural: IF ( ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) NE 0 OR ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) NE 0 )
                {
                    ldaAdsl1033.getPnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                               //Natural: ADD 1 TO #A-C-ANN-PAY-CNT ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1033.getPnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Pay_Cnt().nadd(1);                                                               //Natural: ADD 1 TO #A-CREF-ANN-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1033.getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #A-TOTAL-PAY-CNT
                    ldaAdsl1034.getPnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Pay_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                               //Natural: ADD 1 TO #G-C-ANN-PAY-CNT ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Pay_Cnt().nadd(1);                                                               //Natural: ADD 1 TO #G-CREF-ANN-SUB-PAY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt().nadd(1);                                                                             //Natural: ADD 1 TO #G-TOTAL-PAY-CNT
                    ldaAdsl1033.getPnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #A-C-ANN-UNITS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1033.getPnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #A-CREF-ANN-SUB-UNITS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1033.getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #A-TOTAL-UNITS
                    ldaAdsl1034.getPnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Units().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #G-C-ANN-UNITS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #G-CREF-ANN-SUB-UNITS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Units().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A-INDX ) TO #G-TOTAL-UNITS
                    ldaAdsl1033.getPnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #A-C-ANN-DOLLARS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1033.getPnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #A-CREF-ANN-SUB-DOLLARS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1033.getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #A-TOTAL-DOLLARS
                    ldaAdsl1034.getPnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Dollars().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #G-C-ANN-DOLLARS ( #B-INDX )
                    //*  ACCESS RS0
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1034.getPnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #G-CREF-ANN-SUB-DOLLARS
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Dollars().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A-INDX ) TO #G-TOTAL-DOLLARS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Calculate_Annuall_Ivc() throws Exception                                                                                                             //Natural: CALCULATE-ANNUALL-IVC
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaAdsl1033.getPnd_A_C_Mth_Fin_Ivc_Amount_Pnd_A_C_Mth_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                                          //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #A-C-MTH-FIN-IVC-AMT
        ldaAdsl1033.getPnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                               //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #A-CREF-MTH-SUB-FIN-IVC-AMT
        ldaAdsl1033.getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                                         //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #A-TOTAL-FIN-IVC-AMT
        ldaAdsl1034.getPnd_G_C_Mth_Fin_Ivc_Amount_Pnd_G_C_Mth_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                                          //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #G-C-MTH-FIN-IVC-AMT
        ldaAdsl1034.getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                               //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #G-CREF-MTH-SUB-FIN-IVC-AMT
        ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Ivc_Amt().nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                                         //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO #G-TOTAL-FIN-IVC-AMT
    }
    private void sub_Break_Of_Payment_Due_Date_Rtn() throws Exception                                                                                                     //Natural: BREAK-OF-PAYMENT-DUE-DATE-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        ldaAdsl1030.getPnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Mm().setValue(pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Mm);                                  //Natural: MOVE #PREV-PAYMENT-DUE-DATE-MM TO #PAYMENT-DATE-MCDY-MM
        ldaAdsl1030.getPnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Dd().setValue(pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Dd);                                  //Natural: MOVE #PREV-PAYMENT-DUE-DATE-DD TO #PAYMENT-DATE-MCDY-DD
        ldaAdsl1030.getPnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Cc().setValue(pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Cc);                                  //Natural: MOVE #PREV-PAYMENT-DUE-DATE-CC TO #PAYMENT-DATE-MCDY-CC
        ldaAdsl1030.getPnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Yy().setValue(pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Yy);                                  //Natural: MOVE #PREV-PAYMENT-DUE-DATE-YY TO #PAYMENT-DATE-MCDY-YY
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm791.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM791'
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm792.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM792'
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm793.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM793'
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm794.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM794'
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm795.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM795'
        ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum().reset();                                                                                                                  //Natural: RESET #M-TIAA-MINOR-ACCUM #M-TIAA-GRD-PAY-CNT #M-TIAA-SUB-ACCUM #M-C-MTH-MINOR-ACCUM ( * ) #M-C-ANN-MINOR-ACCUM ( * ) #M-CREF-MTH-SUB-TOTAL-ACCUM #M-CREF-ANN-SUB-TOTAL-ACCUM #M-TOTAL-TIAA-CREF-ACCUM #S-TIAA-MINOR-ACCUM #S-TIAA-GRD-PAY-CNT #S-TIAA-SUB-ACCUM #S-C-MTH-MINOR-ACCUM ( * ) #S-C-ANN-MINOR-ACCUM ( * ) #S-CREF-MTH-SUB-TOTAL-ACCUM #S-CREF-ANN-SUB-TOTAL-ACCUM #S-TOTAL-TIAA-CREF-ACCUM #Q-TIAA-MINOR-ACCUM #Q-TIAA-GRD-PAY-CNT #Q-TIAA-SUB-ACCUM #Q-C-MTH-MINOR-ACCUM ( * ) #Q-C-ANN-MINOR-ACCUM ( * ) #Q-CREF-MTH-SUB-TOTAL-ACCUM #Q-CREF-ANN-SUB-TOTAL-ACCUM #Q-TOTAL-TIAA-CREF-ACCUM #A-TIAA-MINOR-ACCUM #A-TIAA-GRD-PAY-CNT #A-TIAA-SUB-ACCUM #A-C-MTH-MINOR-ACCUM ( * ) #A-C-ANN-MINOR-ACCUM ( * ) #A-CREF-MTH-SUB-TOTAL-ACCUM #A-CREF-ANN-SUB-TOTAL-ACCUM #A-TOTAL-TIAA-CREF-ACCUM #G-TIAA-MINOR-ACCUM #G-TIAA-GRD-PAY-CNT #G-TIAA-SUB-ACCUM #G-C-MTH-MINOR-ACCUM ( * ) #G-C-ANN-MINOR-ACCUM ( * ) #G-CREF-MTH-SUB-TOTAL-ACCUM #G-CREF-ANN-SUB-TOTAL-ACCUM #G-TOTAL-TIAA-CREF-ACCUM
        ldaAdsl1030.getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Pay_Cnt().reset();
        ldaAdsl1030.getPnd_M_Tiaa_Sub_Accum().reset();
        ldaAdsl1030.getPnd_M_C_Mth_Minor_Accum().getValue("*").reset();
        ldaAdsl1030.getPnd_M_C_Ann_Minor_Accum().getValue("*").reset();
        ldaAdsl1030.getPnd_M_Cref_Mth_Sub_Total_Accum().reset();
        ldaAdsl1030.getPnd_M_Cref_Ann_Sub_Total_Accum().reset();
        ldaAdsl1030.getPnd_M_Total_Tiaa_Cref_Accum().reset();
        ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum().reset();
        ldaAdsl1032.getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Pay_Cnt().reset();
        ldaAdsl1032.getPnd_S_Tiaa_Sub_Accum().reset();
        ldaAdsl1032.getPnd_S_C_Mth_Minor_Accum().getValue("*").reset();
        ldaAdsl1032.getPnd_S_C_Ann_Minor_Accum().getValue("*").reset();
        ldaAdsl1032.getPnd_S_Cref_Mth_Sub_Total_Accum().reset();
        ldaAdsl1032.getPnd_S_Cref_Ann_Sub_Total_Accum().reset();
        ldaAdsl1032.getPnd_S_Total_Tiaa_Cref_Accum().reset();
        ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum().reset();
        ldaAdsl1031.getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Pay_Cnt().reset();
        ldaAdsl1031.getPnd_Q_Tiaa_Sub_Accum().reset();
        ldaAdsl1031.getPnd_Q_C_Mth_Minor_Accum().getValue("*").reset();
        ldaAdsl1031.getPnd_Q_C_Ann_Minor_Accum().getValue("*").reset();
        ldaAdsl1031.getPnd_Q_Cref_Mth_Sub_Total_Accum().reset();
        ldaAdsl1031.getPnd_Q_Cref_Ann_Sub_Total_Accum().reset();
        ldaAdsl1031.getPnd_Q_Total_Tiaa_Cref_Accum().reset();
        ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum().reset();
        ldaAdsl1033.getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Pay_Cnt().reset();
        ldaAdsl1033.getPnd_A_Tiaa_Sub_Accum().reset();
        ldaAdsl1033.getPnd_A_C_Mth_Minor_Accum().getValue("*").reset();
        ldaAdsl1033.getPnd_A_C_Ann_Minor_Accum().getValue("*").reset();
        ldaAdsl1033.getPnd_A_Cref_Mth_Sub_Total_Accum().reset();
        ldaAdsl1033.getPnd_A_Cref_Ann_Sub_Total_Accum().reset();
        ldaAdsl1033.getPnd_A_Total_Tiaa_Cref_Accum().reset();
        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum().reset();
        ldaAdsl1034.getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Pay_Cnt().reset();
        ldaAdsl1034.getPnd_G_Tiaa_Sub_Accum().reset();
        ldaAdsl1034.getPnd_G_C_Mth_Minor_Accum().getValue("*").reset();
        ldaAdsl1034.getPnd_G_C_Ann_Minor_Accum().getValue("*").reset();
        ldaAdsl1034.getPnd_G_Cref_Mth_Sub_Total_Accum().reset();
        ldaAdsl1034.getPnd_G_Cref_Ann_Sub_Total_Accum().reset();
        ldaAdsl1034.getPnd_G_Total_Tiaa_Cref_Accum().reset();
    }
    private void sub_End_Of_Job_Rtn() throws Exception                                                                                                                    //Natural: END-OF-JOB-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        if (condition(pnd_Total_Number_Rec.equals(getZero())))                                                                                                            //Natural: IF #TOTAL-NUMBER-REC = 0
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE);                                                                                  //Natural: WRITE ( 1 ) ////
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"******************************************************");                                                         //Natural: WRITE ( 1 ) '******************************************************'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"NO DATA PROCESSED BY MODE WITHIN PAYMENT DUE DATE     ");                                                         //Natural: WRITE ( 1 ) 'NO DATA PROCESSED BY MODE WITHIN PAYMENT DUE DATE     '
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"******************************************************");                                                         //Natural: WRITE ( 1 ) '******************************************************'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm797.class));                                                                   //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM797'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=79 PS=60");
        Global.format(1, "LS=79 PS=60");
        Global.format(2, "LS=79 PS=60");
    }
}
