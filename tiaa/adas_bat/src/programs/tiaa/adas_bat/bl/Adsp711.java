/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:04:14 PM
**        * FROM NATURAL PROGRAM : Adsp711
************************************************************
**        * FILE NAME            : Adsp711.java
**        * CLASS NAME           : Adsp711
**        * INSTANCE NAME        : Adsp711
************************************************************
************************************************************************
* PROGRAM  : ADSP711
* GENERATED: MARCH 15, 2010
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : THIS MODULE EXTRACTS ALL THE ANNUITIZATION TRANSACTIONS
*            FROM THE KDO FILE FOR A GIVEN DATE.
*
* REMARKS  : CLONED FROM ADSP710
**********************  MAINTENANCE LOG ********************************
*  D A T E     PROGRAMMER     D E S C R I P T I O N
* 05/28/1998   ZAFAR KHAN     ROTH/CLASSIC ROLLOVER PROCESS ADDED
* 01/07/1999   ZAFAR KHAN     DESTINATION CHECK ADDED FOR ROLLOVER
* 01/27/1999   ZAFAR KHAN     NAP-BPS-UNIT EXTENDED FROM 6 TO 8 BYTES
* 02/18/1999   ZAFAR KHAN     ROLLOVER AMOUNT FIXED
* 07/12/1999   ZAFAR KHAN     SETTLEMENTS WITH ALT2 WERE NOT PROCESSED
*                             CORRECTLY.
* 11/13/2000   FAZAL TWAHIR   TPA/ IO CHANGES M001
* 01/26/2004   OSCAR SOTTO    99 RATES CHANGES. RECOMPILED TO GET
*                             UPDATED NAZLDARC.
* 04/15/2004   C. AVE         MODIFIED ADAS (ANNUITIZATION SUNGUARD)
* 07/27/04   T. SHINE       IF MONTH-END PROCESS FALLS ON WEEKEND OR
*                           HOLIDAY, USE ADS-RPT-13 (FACTOR DATE) AS
*                           REPORT DATE.
* 03/17/2008 E.MELNIK   ROTH 403B/401K CHANGES.  RECOMPILED FOR
*                       NEW ADSL401/ADSL401A.
* 05/06/2008 G.GUERRERO ADDED A ROTH INDICATOR TO WORK FILE.
*                       ADDED 2ND WORK FILE WRITE FOR ROTH ONLY.
* 01/13/2009 R.SACHARNY COMMENT OUT CALL TO ADSN102(NOT USED) RS0
* 05/13/2010 D.E.ANDER  ADD SURVIVOR INFORMATION MARKED DEA
* 09/25/2010 D.E.ANDER  ADD EXTRACT INFORMATION BASED ON ED MARKED DEA
* 03/05/12  E. MELNIK RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                     UPDATED LINKAGE AREAS.
* 01/18/13  E. MELNIK TNG PROJECT.  RESTOW TO INCLUDE UPDATED LINKAGE
*                      AREAS.
* 10/15/13  E. MELNIK RECOMPILED FOR NEW ADSL401A.  CREF/REA REDESIGN
*                     CHANGES.
* 02/27/2017 R.CARREON RESTOWED FOR PIN EXPANSION
*********************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp711 extends BLNatBase
{
    // Data Areas
    private LdaAdsl401a ldaAdsl401a;
    private LdaAdsl401 ldaAdsl401;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ads_Cntl_View;

    private DbsGroup ads_Cntl_View_Ads_Cntl_Grp;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte;

    private DbsGroup ads_Cntl_View__R_Field_1;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A;
    private DbsField ads_Cntl_View_Ads_Rpt_13;
    private DbsField pnd_Nai_Super1_F;

    private DbsGroup pnd_Nai_Super1_F__R_Field_2;
    private DbsField pnd_Nai_Super1_F_Pnd_Nai_Rqst_Id_F;
    private DbsField pnd_Nai_Super1_F_Pnd_Nai_Rcrd_Cde_F;
    private DbsField pnd_Nai_Super1_F_Pnd_Nai_Sqnc_Nbr_F;
    private DbsField pnd_Nai_Super1_T;

    private DbsGroup pnd_Nai_Super1_T__R_Field_3;
    private DbsField pnd_Nai_Super1_T_Pnd_Nai_Rqst_Id_T;
    private DbsField pnd_Nai_Super1_T_Pnd_Nai_Rcrd_Cde_T;
    private DbsField pnd_Nai_Super1_T_Pnd_Nai_Sqnc_Nbr_T;
    private DbsField pnd_Start_Date;

    private DbsGroup pnd_Start_Date__R_Field_4;
    private DbsField pnd_Start_Date_Pnd_Start_Date_A;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_5;
    private DbsField pnd_Date_Pnd_Date_N;
    private DbsField pnd_X;
    private DbsField pnd_Saved_Prod_Cde;
    private DbsField pnd_A;
    private DbsField pnd_Pos;
    private DbsField pnd_Record_Count;
    private DbsField pnd_Record_Count_Roth;
    private DbsField pnd_Prod_Cde;
    private DbsField pnd_Da_Count;
    private DbsField pnd_Rlvr_Amt;
    private DbsField pnd_Dest;
    private DbsField pnd_Surv_Ind;
    private DbsField pnd_Roth_Ind;
    private DbsField pnd_Last_Activity_Date;

    private DbsGroup pnd_Last_Activity_Date__R_Field_6;
    private DbsField pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N;

    private DbsGroup adsn102_Data_Area;
    private DbsField adsn102_Data_Area_Pnd_Prod_Ct;
    private DbsField adsn102_Data_Area_Pnd_Prod_Cd;
    private DbsField adsn102_Data_Area_Pnd_Acct_Nme_5;
    private DbsField adsn102_Data_Area_Pnd_Acct_Effctve_Dte;
    private DbsField adsn102_Data_Area_Pnd_Acct_Unit_Rte_Ind;
    private DbsField adsn102_Data_Area_Pnd_Prod_Cd_N;
    private DbsField adsn102_Data_Area_Pnd_Acct_Rpt_Seq;
    private DbsField pnd_Hold_Rqst_Id;
    private DbsField pnd_Filler_8;
    private DbsField pnd_Key_Cntl_Bsnss_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl401a = new LdaAdsl401a();
        registerRecord(ldaAdsl401a);
        registerRecord(ldaAdsl401a.getVw_ads_Cntrct_View());
        ldaAdsl401 = new LdaAdsl401();
        registerRecord(ldaAdsl401);
        registerRecord(ldaAdsl401.getVw_ads_Prtcpnt_View());

        // Local Variables
        localVariables = new DbsRecord();

        vw_ads_Cntl_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntl_View", "ADS-CNTL-VIEW"), "ADS_CNTL", "ADS_CNTL");

        ads_Cntl_View_Ads_Cntl_Grp = vw_ads_Cntl_View.getRecord().newGroupInGroup("ADS_CNTL_VIEW_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_DTE");

        ads_Cntl_View__R_Field_1 = ads_Cntl_View_Ads_Cntl_Grp.newGroupInGroup("ads_Cntl_View__R_Field_1", "REDEFINE", ads_Cntl_View_Ads_Cntl_Bsnss_Dte);
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A = ads_Cntl_View__R_Field_1.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A", "ADS-CNTL-BSNSS-DTE-A", FieldType.STRING, 
            8);
        ads_Cntl_View_Ads_Rpt_13 = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Rpt_13", "ADS-RPT-13", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADS_RPT_13");
        registerRecord(vw_ads_Cntl_View);

        pnd_Nai_Super1_F = localVariables.newFieldInRecord("pnd_Nai_Super1_F", "#NAI-SUPER1-F", FieldType.STRING, 36);

        pnd_Nai_Super1_F__R_Field_2 = localVariables.newGroupInRecord("pnd_Nai_Super1_F__R_Field_2", "REDEFINE", pnd_Nai_Super1_F);
        pnd_Nai_Super1_F_Pnd_Nai_Rqst_Id_F = pnd_Nai_Super1_F__R_Field_2.newFieldInGroup("pnd_Nai_Super1_F_Pnd_Nai_Rqst_Id_F", "#NAI-RQST-ID-F", FieldType.STRING, 
            30);
        pnd_Nai_Super1_F_Pnd_Nai_Rcrd_Cde_F = pnd_Nai_Super1_F__R_Field_2.newFieldInGroup("pnd_Nai_Super1_F_Pnd_Nai_Rcrd_Cde_F", "#NAI-RCRD-CDE-F", FieldType.STRING, 
            3);
        pnd_Nai_Super1_F_Pnd_Nai_Sqnc_Nbr_F = pnd_Nai_Super1_F__R_Field_2.newFieldInGroup("pnd_Nai_Super1_F_Pnd_Nai_Sqnc_Nbr_F", "#NAI-SQNC-NBR-F", FieldType.NUMERIC, 
            3);
        pnd_Nai_Super1_T = localVariables.newFieldInRecord("pnd_Nai_Super1_T", "#NAI-SUPER1-T", FieldType.STRING, 36);

        pnd_Nai_Super1_T__R_Field_3 = localVariables.newGroupInRecord("pnd_Nai_Super1_T__R_Field_3", "REDEFINE", pnd_Nai_Super1_T);
        pnd_Nai_Super1_T_Pnd_Nai_Rqst_Id_T = pnd_Nai_Super1_T__R_Field_3.newFieldInGroup("pnd_Nai_Super1_T_Pnd_Nai_Rqst_Id_T", "#NAI-RQST-ID-T", FieldType.STRING, 
            30);
        pnd_Nai_Super1_T_Pnd_Nai_Rcrd_Cde_T = pnd_Nai_Super1_T__R_Field_3.newFieldInGroup("pnd_Nai_Super1_T_Pnd_Nai_Rcrd_Cde_T", "#NAI-RCRD-CDE-T", FieldType.STRING, 
            3);
        pnd_Nai_Super1_T_Pnd_Nai_Sqnc_Nbr_T = pnd_Nai_Super1_T__R_Field_3.newFieldInGroup("pnd_Nai_Super1_T_Pnd_Nai_Sqnc_Nbr_T", "#NAI-SQNC-NBR-T", FieldType.NUMERIC, 
            3);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.NUMERIC, 8);

        pnd_Start_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Start_Date__R_Field_4", "REDEFINE", pnd_Start_Date);
        pnd_Start_Date_Pnd_Start_Date_A = pnd_Start_Date__R_Field_4.newFieldInGroup("pnd_Start_Date_Pnd_Start_Date_A", "#START-DATE-A", FieldType.STRING, 
            8);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 8);

        pnd_Date__R_Field_5 = localVariables.newGroupInRecord("pnd_Date__R_Field_5", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_N = pnd_Date__R_Field_5.newFieldInGroup("pnd_Date_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 2);
        pnd_Saved_Prod_Cde = localVariables.newFieldArrayInRecord("pnd_Saved_Prod_Cde", "#SAVED-PROD-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 2);
        pnd_Pos = localVariables.newFieldInRecord("pnd_Pos", "#POS", FieldType.PACKED_DECIMAL, 2);
        pnd_Record_Count = localVariables.newFieldInRecord("pnd_Record_Count", "#RECORD-COUNT", FieldType.PACKED_DECIMAL, 8);
        pnd_Record_Count_Roth = localVariables.newFieldInRecord("pnd_Record_Count_Roth", "#RECORD-COUNT-ROTH", FieldType.PACKED_DECIMAL, 8);
        pnd_Prod_Cde = localVariables.newFieldArrayInRecord("pnd_Prod_Cde", "#PROD-CDE", FieldType.STRING, 1, new DbsArrayController(1, 20));
        pnd_Da_Count = localVariables.newFieldArrayInRecord("pnd_Da_Count", "#DA-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Rlvr_Amt = localVariables.newFieldArrayInRecord("pnd_Rlvr_Amt", "#RLVR-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20));
        pnd_Dest = localVariables.newFieldArrayInRecord("pnd_Dest", "#DEST", FieldType.STRING, 4, new DbsArrayController(1, 3));
        pnd_Surv_Ind = localVariables.newFieldInRecord("pnd_Surv_Ind", "#SURV-IND", FieldType.STRING, 1);
        pnd_Roth_Ind = localVariables.newFieldInRecord("pnd_Roth_Ind", "#ROTH-IND", FieldType.STRING, 1);
        pnd_Last_Activity_Date = localVariables.newFieldInRecord("pnd_Last_Activity_Date", "#LAST-ACTIVITY-DATE", FieldType.STRING, 8);

        pnd_Last_Activity_Date__R_Field_6 = localVariables.newGroupInRecord("pnd_Last_Activity_Date__R_Field_6", "REDEFINE", pnd_Last_Activity_Date);
        pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N = pnd_Last_Activity_Date__R_Field_6.newFieldInGroup("pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N", 
            "#LAST-ACTIVITY-DATE-N", FieldType.NUMERIC, 8);

        adsn102_Data_Area = localVariables.newGroupInRecord("adsn102_Data_Area", "ADSN102-DATA-AREA");
        adsn102_Data_Area_Pnd_Prod_Ct = adsn102_Data_Area.newFieldInGroup("adsn102_Data_Area_Pnd_Prod_Ct", "#PROD-CT", FieldType.PACKED_DECIMAL, 3);
        adsn102_Data_Area_Pnd_Prod_Cd = adsn102_Data_Area.newFieldArrayInGroup("adsn102_Data_Area_Pnd_Prod_Cd", "#PROD-CD", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        adsn102_Data_Area_Pnd_Acct_Nme_5 = adsn102_Data_Area.newFieldArrayInGroup("adsn102_Data_Area_Pnd_Acct_Nme_5", "#ACCT-NME-5", FieldType.STRING, 
            6, new DbsArrayController(1, 20));
        adsn102_Data_Area_Pnd_Acct_Effctve_Dte = adsn102_Data_Area.newFieldArrayInGroup("adsn102_Data_Area_Pnd_Acct_Effctve_Dte", "#ACCT-EFFCTVE-DTE", 
            FieldType.NUMERIC, 8, new DbsArrayController(1, 20));
        adsn102_Data_Area_Pnd_Acct_Unit_Rte_Ind = adsn102_Data_Area.newFieldArrayInGroup("adsn102_Data_Area_Pnd_Acct_Unit_Rte_Ind", "#ACCT-UNIT-RTE-IND", 
            FieldType.PACKED_DECIMAL, 3, new DbsArrayController(1, 20));
        adsn102_Data_Area_Pnd_Prod_Cd_N = adsn102_Data_Area.newFieldArrayInGroup("adsn102_Data_Area_Pnd_Prod_Cd_N", "#PROD-CD-N", FieldType.NUMERIC, 2, 
            new DbsArrayController(1, 20));
        adsn102_Data_Area_Pnd_Acct_Rpt_Seq = adsn102_Data_Area.newFieldArrayInGroup("adsn102_Data_Area_Pnd_Acct_Rpt_Seq", "#ACCT-RPT-SEQ", FieldType.NUMERIC, 
            2, new DbsArrayController(1, 20));
        pnd_Hold_Rqst_Id = localVariables.newFieldInRecord("pnd_Hold_Rqst_Id", "#HOLD-RQST-ID", FieldType.STRING, 30);
        pnd_Filler_8 = localVariables.newFieldInRecord("pnd_Filler_8", "#FILLER-8", FieldType.STRING, 8);
        pnd_Key_Cntl_Bsnss_Dte = localVariables.newFieldInRecord("pnd_Key_Cntl_Bsnss_Dte", "#KEY-CNTL-BSNSS-DTE", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Cntl_View.reset();

        ldaAdsl401a.initializeValues();
        ldaAdsl401.initializeValues();

        localVariables.reset();
        pnd_Hold_Rqst_Id.setInitialValue(" ");
        pnd_Filler_8.setInitialValue(" ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp711() throws Exception
    {
        super("Adsp711");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //*  CALLNAT 'ADSN102' ADSN102-DATA-AREA        /* RS0
                                                                                                                                                                          //Natural: PERFORM READ-THE-CONTROL-RECORD
        sub_Read_The_Control_Record();
        if (condition(Global.isEscape())) {return;}
        pnd_Key_Cntl_Bsnss_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Start_Date_Pnd_Start_Date_A);                                                            //Natural: MOVE EDITED #START-DATE-A TO #KEY-CNTL-BSNSS-DTE ( EM = YYYYMMDD )
        ldaAdsl401.getVw_ads_Prtcpnt_View().startDatabaseRead                                                                                                             //Natural: READ ADS-PRTCPNT-VIEW BY ADP-LST-ACTVTY-DTE STARTING FROM #KEY-CNTL-BSNSS-DTE
        (
        "PND_PND_L1140",
        new Wc[] { new Wc("ADP_LST_ACTVTY_DTE", ">=", pnd_Key_Cntl_Bsnss_Dte, WcType.BY) },
        new Oc[] { new Oc("ADP_LST_ACTVTY_DTE", "ASC") }
        );
        PND_PND_L1140:
        while (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().readNextRow("PND_PND_L1140")))
        {
            pnd_Last_Activity_Date.setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                                    //Natural: MOVE EDITED ADP-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #LAST-ACTIVITY-DATE
            if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                                 //Natural: IF ADS-RPT-13 NE 0
            {
                pnd_Date.setValueEdited(ads_Cntl_View_Ads_Rpt_13,new ReportEditMask("YYYYMMDD"));                                                                         //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE
                if (condition(pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N.greater(pnd_Date_Pnd_Date_N)))                                                              //Natural: IF #LAST-ACTIVITY-DATE-N > #DATE-N
                {
                    if (true) break PND_PND_L1140;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L1140. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N.greater(ads_Cntl_View_Ads_Cntl_Bsnss_Dte)))                                                 //Natural: IF #LAST-ACTIVITY-DATE-N > ADS-CNTL-BSNSS-DTE
                {
                    if (true) break PND_PND_L1140;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L1140. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  COMPLETE
            //*  DEA
            if (condition(!(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,1).equals("T") && ldaAdsl401.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde().equals("M")  //Natural: ACCEPT IF SUBSTR ( ADP-STTS-CDE,1,1 ) = 'T' AND ADP-ANNT-TYP-CDE EQ 'M' AND ADP-SRVVR-IND = 'Y'
                && ldaAdsl401.getAds_Prtcpnt_View_Adp_Srvvr_Ind().equals("Y"))))
            {
                continue;
            }
            ldaAdsl401a.getVw_ads_Cntrct_View().startDatabaseFind                                                                                                         //Natural: FIND ADS-CNTRCT-VIEW WITH ADS-CNTRCT-VIEW.RQST-ID = ADS-PRTCPNT-VIEW.RQST-ID
            (
            "FIND01",
            new Wc[] { new Wc("RQST_ID", "=", ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id(), WcType.WITH) }
            );
            FIND01:
            while (condition(ldaAdsl401a.getVw_ads_Cntrct_View().readNextRow("FIND01")))
            {
                ldaAdsl401a.getVw_ads_Cntrct_View().setIfNotFoundControlFlag(false);
                //*  DEA
                pnd_Record_Count.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #RECORD-COUNT
                pnd_Roth_Ind.setValue(" ");                                                                                                                               //Natural: ASSIGN #ROTH-IND := ' '
                pnd_Surv_Ind.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Srvvr_Ind());                                                                                    //Natural: ASSIGN #SURV-IND := ADP-SRVVR-IND
                //*  NEWLY ADDED
                //*  NAP-RTB-ALT-DEST-RLVR-DEST-CDE (*)
                //*  DEA
                getWorkFiles().write(1, false, ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Optn(), ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte(), ldaAdsl401a.getVw_ads_Cntrct_View(),  //Natural: WRITE WORK FILE 1 ADP-ANNTY-OPTN ADP-EFFCTV-DTE ADS-CNTRCT-VIEW #DA-COUNT ( * ) #RLVR-AMT ( * ) #PROD-CDE ( * ) #DEST ( * ) #ROTH-IND #SURV-IND
                    pnd_Da_Count.getValue("*"), pnd_Rlvr_Amt.getValue("*"), pnd_Prod_Cde.getValue("*"), pnd_Dest.getValue("*"), pnd_Roth_Ind, pnd_Surv_Ind);
                if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Roth_Rqst_Ind().equals("Y")))                                                                            //Natural: IF ADP-ROTH-RQST-IND = 'Y'
                {
                    pnd_Record_Count_Roth.nadd(1);                                                                                                                        //Natural: ADD 1 TO #RECORD-COUNT-ROTH
                    pnd_Roth_Ind.setValue("Y");                                                                                                                           //Natural: ASSIGN #ROTH-IND := 'Y'
                    //*  NAP-RTB-ALT-DEST-RLVR-DEST-CDE (*)
                    //*  GG050608
                    //*  DEA
                    getWorkFiles().write(2, false, ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Optn(), ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte(), ldaAdsl401a.getVw_ads_Cntrct_View(),  //Natural: WRITE WORK FILE 2 ADP-ANNTY-OPTN ADP-EFFCTV-DTE ADS-CNTRCT-VIEW #DA-COUNT ( * ) #RLVR-AMT ( * ) #PROD-CDE ( * ) #DEST ( * ) #ROTH-IND #SURV-IND
                        pnd_Da_Count.getValue("*"), pnd_Rlvr_Amt.getValue("*"), pnd_Prod_Cde.getValue("*"), pnd_Dest.getValue("*"), pnd_Roth_Ind, pnd_Surv_Ind);
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1140"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1140"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  WHEN NO ROTH RECS ARE PRESENT, THIS NULL WRITE WILL SEND THE ROTH
        //*  INDICATOR TO THE REPORT MODULE TO CREATE THE SURV ROTH REPORT.  DEA
        //*  GG050608
        if (condition(pnd_Record_Count_Roth.equals(getZero())))                                                                                                           //Natural: IF #RECORD-COUNT-ROTH = 0
        {
            pnd_Roth_Ind.setValue("Y");                                                                                                                                   //Natural: ASSIGN #ROTH-IND := 'Y'
            //*  NULL VALUES
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Optn().reset();                                                                                                      //Natural: RESET ADP-ANNTY-OPTN ADP-EFFCTV-DTE ADS-CNTRCT-VIEW #DA-COUNT ( * ) #RLVR-AMT ( * ) #PROD-CDE ( * ) #DEST ( * )
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte().reset();
            ldaAdsl401a.getVw_ads_Cntrct_View().reset();
            pnd_Da_Count.getValue("*").reset();
            pnd_Rlvr_Amt.getValue("*").reset();
            pnd_Prod_Cde.getValue("*").reset();
            pnd_Dest.getValue("*").reset();
            //*  DEA
            getWorkFiles().write(2, false, ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Optn(), ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte(), ldaAdsl401a.getVw_ads_Cntrct_View(),  //Natural: WRITE WORK FILE 2 ADP-ANNTY-OPTN ADP-EFFCTV-DTE ADS-CNTRCT-VIEW #DA-COUNT ( * ) #RLVR-AMT ( * ) #PROD-CDE ( * ) #DEST ( * ) #ROTH-IND #SURV-IND
                pnd_Da_Count.getValue("*"), pnd_Rlvr_Amt.getValue("*"), pnd_Prod_Cde.getValue("*"), pnd_Dest.getValue("*"), pnd_Roth_Ind, pnd_Surv_Ind);
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, NEWLINE,"TOTAL DA RESULT RECORDS WRITTEN TO OUTPUT -",pnd_Record_Count);                                                                    //Natural: WRITE / 'TOTAL DA RESULT RECORDS WRITTEN TO OUTPUT -' #RECORD-COUNT
        if (Global.isEscape()) return;
        //*  DEA
        getReports().write(0, NEWLINE,"TOT SURV DA RESULT RECS WRITTEN TO OUTPUT -",pnd_Record_Count_Roth);                                                               //Natural: WRITE / 'TOT SURV DA RESULT RECS WRITTEN TO OUTPUT -' #RECORD-COUNT-ROTH
        if (Global.isEscape()) return;
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-THE-CONTROL-RECORD
        //* ********************************
        //* *DEFINE SUBROUTINE GET-IA-ROLLOVER-DATA
        //* ********************************
        //* *RESET #DA-COUNT (*)
        //* *  #RLVR-AMT (*) #PROD-CDE (*)
        //* *IF NAP-RTB-ALT-DEST-TYP (*) = 'ALT1' OR = 'ALT2'
        //* *  MOVE NAZ-PRTCPNT-DDM-VIEW.RQST-ID TO #NAI-RQST-ID-F
        //* *    #NAI-RQST-ID-T
        //* *  MOVE 'ALT' TO #NAI-RCRD-CDE-F
        //* *    #NAI-RCRD-CDE-T
        //* *  MOVE 1             TO #NAI-SQNC-NBR-F
        //* *  MOVE 99    TO #NAI-SQNC-NBR-T
        //* *  FIND NAZ-IA-RSLT-RTB-VIEW WITH NAI-SUPER1 = #NAI-SUPER1-F
        //* *      THRU #NAI-SUPER1-T
        //* *    IF NO RECORD
        //* *       WRITE 'NO RECORD FOUND-' #NAI-SUPER1-F #NAI-SUPER1-T
        //* *       ESCAPE ROUTINE
        //* *    END-NOREC
        //*  01/07/1999
        //* *    IF NAI-RTB-DEST-CDE = 'RTHC' OR = 'RTHT' OR = 'IRAC' OR = 'IRAT'
        //*  CURTIS NEW CODES EGGTRA
        //* *            OR = 'O3BT' OR = 'O3BC'
        //* *            OR = 'QPLT' OR = 'QPLC'
        //*            OR = '57BT' OR = '57BC'
        //* *      IF (NAD-ACCT-CDE (*) = 'T' AND
        //* *          NAI-DTL-TIAA-DA-STD-AMT (1) > 0)
        //* *        IF #SAVED-PROD-CDE (*) = 'T'
        //* *            AND NAI-SQNCE-NBR < 2    /* 07/12/99
        //* *          IGNORE
        //* *        ELSE
        //* *          ADD 1 TO #X
        //* *          MOVE 'T' TO #SAVED-PROD-CDE (#X)
        //* *          ADD 1 TO #DA-COUNT (1)
        //* *          ADD NAI-DTL-TIAA-DA-STD-AMT (1) TO #RLVR-AMT (1)
        //* *        END-IF
        //* *      END-IF
        //* *      FOR #A 1 20
        //* *        IF NAI-DTL-CREF-ACCT-CD (#A) = ' '
        //* *          ESCAPE BOTTOM (2500)
        //* *        ELSE
        //* *          IF #SAVED-PROD-CDE (*) = NAI-DTL-CREF-ACCT-CD (#A)
        //* *              AND NAI-SQNCE-NBR < 2   /* 07/12/99
        //* *            ESCAPE TOP
        //* *          END-IF
        //* *          EXAMINE #PROD-CD (*) FOR NAI-DTL-CREF-ACCT-CD (#A)
        //* *            GIVING INDEX #POS
        //* *          ADD 1 TO #X
        //* *          MOVE NAI-DTL-CREF-ACCT-CD (#A) TO #PROD-CDE (#POS)
        //* *            #SAVED-PROD-CDE (#X)
        //* *          ADD 1 TO #DA-COUNT (#POS)
        //* *          ADD NAI-DTL-CREF-DA-ANNL-AMT (#A) TO #RLVR-AMT (#POS)
        //* *        END-IF
        //* *      END-FOR
        //*     END-IF
        //* *  END-FIND
        //* *END-IF
        //* *END-SUBROUTINE
    }
    private void sub_Read_The_Control_Record() throws Exception                                                                                                           //Natural: READ-THE-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        vw_ads_Cntl_View.startDatabaseRead                                                                                                                                //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "READ01",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        READ01:
        while (condition(vw_ads_Cntl_View.readNextRow("READ01")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                                     //Natural: IF ADS-RPT-13 NE 0
        {
            pnd_Date.setValueEdited(ads_Cntl_View_Ads_Rpt_13,new ReportEditMask("YYYYMMDD"));                                                                             //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE
            pnd_Start_Date.compute(new ComputeParameters(false, pnd_Start_Date), (pnd_Date_Pnd_Date_N.divide(100)).multiply(100).add(1));                                 //Natural: COMPUTE #START-DATE = ( #DATE-N / 100 ) * 100 + 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Start_Date.compute(new ComputeParameters(false, pnd_Start_Date), (ads_Cntl_View_Ads_Cntl_Bsnss_Dte.divide(100)).multiply(100).add(1));                    //Natural: COMPUTE #START-DATE = ( ADS-CNTL-BSNSS-DTE / 100 ) * 100 + 1
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "START DATE -",pnd_Start_Date);                                                                                                             //Natural: WRITE 'START DATE -' #START-DATE
        if (Global.isEscape()) return;
        getReports().write(0, "END DATE   -",ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                                           //Natural: WRITE 'END DATE   -' ADS-CNTL-BSNSS-DTE
        if (Global.isEscape()) return;
    }

    //
}
