/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:00:44 PM
**        * FROM NATURAL PROGRAM : Adsp1005
************************************************************
**        * FILE NAME            : Adsp1005.java
**        * CLASS NAME           : Adsp1005
**        * INSTANCE NAME        : Adsp1005
************************************************************
************************************************************************
* PROGRAM  : ADSP1005
* GENERATED: APRIL 16, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : THIS MODULE READS WORK FILE TAND REPORTS A GRAND TOTAL
*          : OF THE DA TO  IA  FINAL PREMIUM ACCUMULATIONS
*            FOR MONTH-TO-DATE BY EFFECTIVE DATE.
*
* REMARKS  : CLONED FROM NAZP1005 (ADAM SYSTEM)
*********************  MAINTENANCE LOG *********************************
* 01 02 01  C SCHNEIDER ADDED OPTN-CD TO WORK FILE TO SUPPORT REPORT
*                       CHANGES
* 01 26 04  O SOTTO     99 RATES CHANGES. SCAN FOR 012604.
* 04 16 04  C. AVE      MODIFIED FOR ADAS - ANNUITIZATION SUNGUARD
* 07/27/04   T. SHINE       IF MONTH-END PROCESS FALLS ON WEEKEND OR
*                           HOLIDAY, USE ADS-RPT-13 (FACTOR DATE) AS
*                           REPORT DATE.
* 12/10/08 R.SACHARNY   ADD "TIAA STABLE" AND "REA ACCESS" FUNDS RS0
* 7/19/2010  C. MASON   RESTOW DUE TO LDA CHANGES
* 9/07/2010  D.E.ANDER  CHG #MAX-RATE FROM 80 TO 99 OCCURS, PRD FX DEA
* 03/01/12 E. MELNIK   RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                      UPDATED LINKAGE AREAS.  CHANGES MARKED BY
*                      EM - 030112.
* 09/2013   O. SOTTO  CREF REA CHANGES MARKED WITH OS-092013.
* 02/24/2017 R.CARREON RESTOWED FOR PIN EXPANSION 02242017
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp1005 extends BLNatBase
{
    // Data Areas
    private LdaAdsl1005 ldaAdsl1005;
    private LdaAdsl401a ldaAdsl401a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cnvrt_Annty_Optn;
    private DbsField pnd_Rec_Type;

    private DbsGroup pnd_Rec_Type__R_Field_1;
    private DbsField pnd_Rec_Type_Pnd_Filler;
    private DbsField pnd_Rec_Type_Pnd_Z_Indx;

    private DbsGroup pnd_Pas_Ext_Cntrl_02_Work_Area;

    private DbsGroup pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Table;
    private DbsField pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1;

    private DataAccessProgramView vw_ads_Cntl_View;

    private DbsGroup ads_Cntl_View_Ads_Cntl_Grp;
    private DbsField ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte;
    private DbsField ads_Cntl_View_Ads_Fnl_Prm_Accum_Dte;
    private DbsField ads_Cntl_View_Ads_Rpt_13;
    private DbsField pnd_Prev_Bsnss_Dte_N;

    private DbsGroup pnd_Prev_Bsnss_Dte_N__R_Field_2;
    private DbsField pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A;
    private DbsField pnd_Today_Business_Date;

    private DbsGroup pnd_Today_Business_Date__R_Field_3;
    private DbsField pnd_Today_Business_Date_Pnd_Today_Business_Date_Ccyy;
    private DbsField pnd_Today_Business_Date_Pnd_Today_Business_Date_Mmdd;

    private DbsGroup pnd_Today_Business_Date__R_Field_4;
    private DbsField pnd_Today_Business_Date_Pnd_Today_Business_Date_Mm;
    private DbsField pnd_Today_Business_Date_Pnd_Today_Business_Date_Dd;
    private DbsField pnd_Current_Accounting_Date;

    private DbsGroup pnd_Current_Accounting_Date__R_Field_5;
    private DbsField pnd_Current_Accounting_Date_Pnd_Current_Accounting_Date_Ccyy;
    private DbsField pnd_Current_Accounting_Date_Pnd_Current_Accounting_Date_Mmdd;
    private DbsField pnd_Curr_Month_Date;

    private DbsGroup pnd_Curr_Month_Date__R_Field_6;
    private DbsField pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Ccyy;
    private DbsField pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Mmdd;

    private DbsGroup pnd_Curr_Month_Date__R_Field_7;
    private DbsField pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Mm;
    private DbsField pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Dd;
    private DbsField pnd_Prev_Bsnss_Dte_D;
    private DbsField pnd_Nad_Super_2;

    private DbsGroup pnd_Nad_Super_2__R_Field_8;
    private DbsField pnd_Nad_Super_2_Pnd_Nad_Lst_Actvty_Dte;
    private DbsField pnd_Nad_Super_2_Pnd_Nad_Stts_Cde;

    private DbsGroup pnd_All_Indx;
    private DbsField pnd_All_Indx_Pnd_A_Indx;
    private DbsField pnd_All_Indx_Pnd_B_Indx;
    private DbsField pnd_All_Indx_Pnd_C_Indx;
    private DbsField pnd_All_Indx_Pnd_Next_Indx;
    private DbsField pnd_All_Indx_Pnd_Prod_Indx;
    private DbsField pnd_Cntl_Isn;
    private DbsField pnd_Max_Rate;
    private DbsField pnd_Total_Number_Rec;
    private DbsField pnd_First_Time;
    private DbsField pnd_Tiaa_Prod_Code;
    private DbsField pnd_Cref_Prod_Found;
    private DbsField pnd_Stbl_Fund;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd;

    private DbsGroup pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_9;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd;
    private DbsField pnd_Prev_Lst_Actvty_Dte_Yyyymmdd;

    private DbsGroup pnd_Prev_Lst_Actvty_Dte_Yyyymmdd__R_Field_10;
    private DbsField pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Cc;
    private DbsField pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Yy;
    private DbsField pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Mm;
    private DbsField pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Dd;
    private DbsField pnd_Prev_Effctv_Dte;

    private DbsGroup pnd_Prev_Effctv_Dte__R_Field_11;
    private DbsField pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Cc;
    private DbsField pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Yy;
    private DbsField pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Mm;
    private DbsField pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Dd;
    private DbsField pnd_Pec_Nbr_Active_Acct;
    private DbsField pnd_Adp_Effctv_Date;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_12;
    private DbsField pnd_Date_A_Pnd_Date_N;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl1005 = new LdaAdsl1005();
        registerRecord(ldaAdsl1005);
        ldaAdsl401a = new LdaAdsl401a();
        registerRecord(ldaAdsl401a);
        registerRecord(ldaAdsl401a.getVw_ads_Cntrct_View());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cnvrt_Annty_Optn = localVariables.newFieldInRecord("pnd_Cnvrt_Annty_Optn", "#CNVRT-ANNTY-OPTN", FieldType.STRING, 1);
        pnd_Rec_Type = localVariables.newFieldInRecord("pnd_Rec_Type", "#REC-TYPE", FieldType.NUMERIC, 3);

        pnd_Rec_Type__R_Field_1 = localVariables.newGroupInRecord("pnd_Rec_Type__R_Field_1", "REDEFINE", pnd_Rec_Type);
        pnd_Rec_Type_Pnd_Filler = pnd_Rec_Type__R_Field_1.newFieldInGroup("pnd_Rec_Type_Pnd_Filler", "#FILLER", FieldType.NUMERIC, 2);
        pnd_Rec_Type_Pnd_Z_Indx = pnd_Rec_Type__R_Field_1.newFieldInGroup("pnd_Rec_Type_Pnd_Z_Indx", "#Z-INDX", FieldType.NUMERIC, 1);

        pnd_Pas_Ext_Cntrl_02_Work_Area = localVariables.newGroupInRecord("pnd_Pas_Ext_Cntrl_02_Work_Area", "#PAS-EXT-CNTRL-02-WORK-AREA");

        pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Table = pnd_Pas_Ext_Cntrl_02_Work_Area.newGroupArrayInGroup("pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Table", 
            "#PEC-ACCT-TABLE", new DbsArrayController(1, 20));
        pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1 = pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Table.newFieldInGroup("pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1", 
            "#PEC-ACCT-NAME-1", FieldType.STRING, 1);

        vw_ads_Cntl_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntl_View", "ADS-CNTL-VIEW"), "ADS_CNTL", "ADS_CNTL");

        ads_Cntl_View_Ads_Cntl_Grp = vw_ads_Cntl_View.getRecord().newGroupInGroup("ADS_CNTL_VIEW_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde", "ADS-CNTL-RCRD-TYP-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADS_CNTL_RCRD_TYP_CDE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte", "ADS-CNTL-BSNSS-RCPRCL-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_RCPRCL_DTE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_DTE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte", "ADS-CNTL-BSNSS-TMRRW-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_TMRRW_DTE");
        ads_Cntl_View_Ads_Fnl_Prm_Accum_Dte = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Fnl_Prm_Accum_Dte", "ADS-FNL-PRM-ACCUM-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADS_FNL_PRM_ACCUM_DTE");
        ads_Cntl_View_Ads_Rpt_13 = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Rpt_13", "ADS-RPT-13", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADS_RPT_13");
        registerRecord(vw_ads_Cntl_View);

        pnd_Prev_Bsnss_Dte_N = localVariables.newFieldInRecord("pnd_Prev_Bsnss_Dte_N", "#PREV-BSNSS-DTE-N", FieldType.NUMERIC, 8);

        pnd_Prev_Bsnss_Dte_N__R_Field_2 = localVariables.newGroupInRecord("pnd_Prev_Bsnss_Dte_N__R_Field_2", "REDEFINE", pnd_Prev_Bsnss_Dte_N);
        pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A = pnd_Prev_Bsnss_Dte_N__R_Field_2.newFieldInGroup("pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A", "#PREV-BSNSS-DTE-A", 
            FieldType.STRING, 8);
        pnd_Today_Business_Date = localVariables.newFieldInRecord("pnd_Today_Business_Date", "#TODAY-BUSINESS-DATE", FieldType.STRING, 8);

        pnd_Today_Business_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Today_Business_Date__R_Field_3", "REDEFINE", pnd_Today_Business_Date);
        pnd_Today_Business_Date_Pnd_Today_Business_Date_Ccyy = pnd_Today_Business_Date__R_Field_3.newFieldInGroup("pnd_Today_Business_Date_Pnd_Today_Business_Date_Ccyy", 
            "#TODAY-BUSINESS-DATE-CCYY", FieldType.NUMERIC, 4);
        pnd_Today_Business_Date_Pnd_Today_Business_Date_Mmdd = pnd_Today_Business_Date__R_Field_3.newFieldInGroup("pnd_Today_Business_Date_Pnd_Today_Business_Date_Mmdd", 
            "#TODAY-BUSINESS-DATE-MMDD", FieldType.STRING, 4);

        pnd_Today_Business_Date__R_Field_4 = pnd_Today_Business_Date__R_Field_3.newGroupInGroup("pnd_Today_Business_Date__R_Field_4", "REDEFINE", pnd_Today_Business_Date_Pnd_Today_Business_Date_Mmdd);
        pnd_Today_Business_Date_Pnd_Today_Business_Date_Mm = pnd_Today_Business_Date__R_Field_4.newFieldInGroup("pnd_Today_Business_Date_Pnd_Today_Business_Date_Mm", 
            "#TODAY-BUSINESS-DATE-MM", FieldType.STRING, 2);
        pnd_Today_Business_Date_Pnd_Today_Business_Date_Dd = pnd_Today_Business_Date__R_Field_4.newFieldInGroup("pnd_Today_Business_Date_Pnd_Today_Business_Date_Dd", 
            "#TODAY-BUSINESS-DATE-DD", FieldType.STRING, 2);
        pnd_Current_Accounting_Date = localVariables.newFieldInRecord("pnd_Current_Accounting_Date", "#CURRENT-ACCOUNTING-DATE", FieldType.STRING, 8);

        pnd_Current_Accounting_Date__R_Field_5 = localVariables.newGroupInRecord("pnd_Current_Accounting_Date__R_Field_5", "REDEFINE", pnd_Current_Accounting_Date);
        pnd_Current_Accounting_Date_Pnd_Current_Accounting_Date_Ccyy = pnd_Current_Accounting_Date__R_Field_5.newFieldInGroup("pnd_Current_Accounting_Date_Pnd_Current_Accounting_Date_Ccyy", 
            "#CURRENT-ACCOUNTING-DATE-CCYY", FieldType.NUMERIC, 4);
        pnd_Current_Accounting_Date_Pnd_Current_Accounting_Date_Mmdd = pnd_Current_Accounting_Date__R_Field_5.newFieldInGroup("pnd_Current_Accounting_Date_Pnd_Current_Accounting_Date_Mmdd", 
            "#CURRENT-ACCOUNTING-DATE-MMDD", FieldType.STRING, 4);
        pnd_Curr_Month_Date = localVariables.newFieldInRecord("pnd_Curr_Month_Date", "#CURR-MONTH-DATE", FieldType.STRING, 8);

        pnd_Curr_Month_Date__R_Field_6 = localVariables.newGroupInRecord("pnd_Curr_Month_Date__R_Field_6", "REDEFINE", pnd_Curr_Month_Date);
        pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Ccyy = pnd_Curr_Month_Date__R_Field_6.newFieldInGroup("pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Ccyy", 
            "#CURR-MONTH-DATE-CCYY", FieldType.NUMERIC, 4);
        pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Mmdd = pnd_Curr_Month_Date__R_Field_6.newFieldInGroup("pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Mmdd", 
            "#CURR-MONTH-DATE-MMDD", FieldType.STRING, 4);

        pnd_Curr_Month_Date__R_Field_7 = pnd_Curr_Month_Date__R_Field_6.newGroupInGroup("pnd_Curr_Month_Date__R_Field_7", "REDEFINE", pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Mmdd);
        pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Mm = pnd_Curr_Month_Date__R_Field_7.newFieldInGroup("pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Mm", "#CURR-MONTH-DATE-MM", 
            FieldType.STRING, 2);
        pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Dd = pnd_Curr_Month_Date__R_Field_7.newFieldInGroup("pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Dd", "#CURR-MONTH-DATE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Prev_Bsnss_Dte_D = localVariables.newFieldInRecord("pnd_Prev_Bsnss_Dte_D", "#PREV-BSNSS-DTE-D", FieldType.DATE);
        pnd_Nad_Super_2 = localVariables.newFieldInRecord("pnd_Nad_Super_2", "#NAD-SUPER-2", FieldType.STRING, 7);

        pnd_Nad_Super_2__R_Field_8 = localVariables.newGroupInRecord("pnd_Nad_Super_2__R_Field_8", "REDEFINE", pnd_Nad_Super_2);
        pnd_Nad_Super_2_Pnd_Nad_Lst_Actvty_Dte = pnd_Nad_Super_2__R_Field_8.newFieldInGroup("pnd_Nad_Super_2_Pnd_Nad_Lst_Actvty_Dte", "#NAD-LST-ACTVTY-DTE", 
            FieldType.DATE);
        pnd_Nad_Super_2_Pnd_Nad_Stts_Cde = pnd_Nad_Super_2__R_Field_8.newFieldInGroup("pnd_Nad_Super_2_Pnd_Nad_Stts_Cde", "#NAD-STTS-CDE", FieldType.STRING, 
            3);

        pnd_All_Indx = localVariables.newGroupInRecord("pnd_All_Indx", "#ALL-INDX");
        pnd_All_Indx_Pnd_A_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_A_Indx", "#A-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_B_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_B_Indx", "#B-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_C_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_C_Indx", "#C-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Next_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Next_Indx", "#NEXT-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Prod_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Prod_Indx", "#PROD-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Cntl_Isn = localVariables.newFieldInRecord("pnd_Cntl_Isn", "#CNTL-ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Max_Rate = localVariables.newFieldInRecord("pnd_Max_Rate", "#MAX-RATE", FieldType.PACKED_DECIMAL, 3);
        pnd_Total_Number_Rec = localVariables.newFieldInRecord("pnd_Total_Number_Rec", "#TOTAL-NUMBER-REC", FieldType.PACKED_DECIMAL, 7);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Prod_Code = localVariables.newFieldInRecord("pnd_Tiaa_Prod_Code", "#TIAA-PROD-CODE", FieldType.BOOLEAN, 1);
        pnd_Cref_Prod_Found = localVariables.newFieldInRecord("pnd_Cref_Prod_Found", "#CREF-PROD-FOUND", FieldType.BOOLEAN, 1);
        pnd_Stbl_Fund = localVariables.newFieldInRecord("pnd_Stbl_Fund", "#STBL-FUND", FieldType.BOOLEAN, 1);
        pnd_Prev_Accounting_Dte_Yyyymmdd = localVariables.newFieldInRecord("pnd_Prev_Accounting_Dte_Yyyymmdd", "#PREV-ACCOUNTING-DTE-YYYYMMDD", FieldType.STRING, 
            8);

        pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_9 = localVariables.newGroupInRecord("pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_9", "REDEFINE", pnd_Prev_Accounting_Dte_Yyyymmdd);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_9.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc", 
            "#PREV-ACCOUNT-DTE-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_9.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy", 
            "#PREV-ACCOUNT-DTE-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_9.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm", 
            "#PREV-ACCOUNT-DTE-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_9.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd", 
            "#PREV-ACCOUNT-DTE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Prev_Lst_Actvty_Dte_Yyyymmdd = localVariables.newFieldInRecord("pnd_Prev_Lst_Actvty_Dte_Yyyymmdd", "#PREV-LST-ACTVTY-DTE-YYYYMMDD", FieldType.STRING, 
            8);

        pnd_Prev_Lst_Actvty_Dte_Yyyymmdd__R_Field_10 = localVariables.newGroupInRecord("pnd_Prev_Lst_Actvty_Dte_Yyyymmdd__R_Field_10", "REDEFINE", pnd_Prev_Lst_Actvty_Dte_Yyyymmdd);
        pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Cc = pnd_Prev_Lst_Actvty_Dte_Yyyymmdd__R_Field_10.newFieldInGroup("pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Cc", 
            "#PREV-ACTVITY-DTE-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Yy = pnd_Prev_Lst_Actvty_Dte_Yyyymmdd__R_Field_10.newFieldInGroup("pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Yy", 
            "#PREV-ACTVITY-DTE-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Mm = pnd_Prev_Lst_Actvty_Dte_Yyyymmdd__R_Field_10.newFieldInGroup("pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Mm", 
            "#PREV-ACTVITY-DTE-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Dd = pnd_Prev_Lst_Actvty_Dte_Yyyymmdd__R_Field_10.newFieldInGroup("pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Dd", 
            "#PREV-ACTVITY-DTE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Prev_Effctv_Dte = localVariables.newFieldInRecord("pnd_Prev_Effctv_Dte", "#PREV-EFFCTV-DTE", FieldType.NUMERIC, 8);

        pnd_Prev_Effctv_Dte__R_Field_11 = localVariables.newGroupInRecord("pnd_Prev_Effctv_Dte__R_Field_11", "REDEFINE", pnd_Prev_Effctv_Dte);
        pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Cc = pnd_Prev_Effctv_Dte__R_Field_11.newFieldInGroup("pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Cc", "#PREV-EFFCTV-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Yy = pnd_Prev_Effctv_Dte__R_Field_11.newFieldInGroup("pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Yy", "#PREV-EFFCTV-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Mm = pnd_Prev_Effctv_Dte__R_Field_11.newFieldInGroup("pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Mm", "#PREV-EFFCTV-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Dd = pnd_Prev_Effctv_Dte__R_Field_11.newFieldInGroup("pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Dd", "#PREV-EFFCTV-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Pec_Nbr_Active_Acct = localVariables.newFieldInRecord("pnd_Pec_Nbr_Active_Acct", "#PEC-NBR-ACTIVE-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Adp_Effctv_Date = localVariables.newFieldInRecord("pnd_Adp_Effctv_Date", "#ADP-EFFCTV-DATE", FieldType.DATE);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_12 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_12", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_12.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Cntl_View.reset();

        ldaAdsl1005.initializeValues();
        ldaAdsl401a.initializeValues();

        localVariables.reset();
        pnd_All_Indx_Pnd_A_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_B_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_C_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Next_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Prod_Indx.setInitialValue(0);
        pnd_Max_Rate.setInitialValue(250);
        pnd_Total_Number_Rec.setInitialValue(0);
        pnd_First_Time.setInitialValue(true);
        pnd_Tiaa_Prod_Code.setInitialValue(true);
        pnd_Cref_Prod_Found.setInitialValue(true);
        pnd_Stbl_Fund.setInitialValue(false);
        pnd_Pec_Nbr_Active_Acct.setInitialValue(20);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp1005() throws Exception
    {
        super("Adsp1005");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  OS-092013 START                                                                                                                                              //Natural: FORMAT LS = 79 PS = 60;//Natural: FORMAT ( 1 ) LS = 79 PS = 60;//Natural: FORMAT ( 2 ) LS = 79 PS = 60
        //* *
        //* *  #PAS-EXT-CNTRL-02-WORK-AREA
        //* *  #PEC-NBR-ACTIVE-ACCT
        DbsUtil.callnat(Adsn889.class , getCurrentProcessState(), pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue("*"));                                      //Natural: CALLNAT 'ADSN889' #PEC-ACCT-NAME-1 ( * )
        if (condition(Global.isEscape())) return;
        //*  OS-092013 END
                                                                                                                                                                          //Natural: PERFORM READ-CONTROL-RECORD
        sub_Read_Control_Record();
        if (condition(Global.isEscape())) {return;}
        PND_PND_L1620:                                                                                                                                                    //Natural: READ WORK FILE 1 ADS-CNTRCT-VIEW #CNVRT-ANNTY-OPTN #ADP-EFFCTV-DATE
        while (condition(getWorkFiles().read(1, ldaAdsl401a.getVw_ads_Cntrct_View(), pnd_Cnvrt_Annty_Optn, pnd_Adp_Effctv_Date)))
        {
            //*                                                                                                                                                           //Natural: AT END OF DATA
            pnd_Current_Accounting_Date.setValueEdited(pnd_Adp_Effctv_Date,new ReportEditMask("YYYYMMDD"));                                                               //Natural: MOVE EDITED #ADP-EFFCTV-DATE ( EM = YYYYMMDD ) TO #CURRENT-ACCOUNTING-DATE
            FOR01:                                                                                                                                                        //Natural: FOR #A-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
            for (pnd_All_Indx_Pnd_A_Indx.setValue(1); condition(pnd_All_Indx_Pnd_A_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_A_Indx.nadd(1))
            {
                //*    IF ADC-ACCT-CDE (#A-INDX) = 'T'
                //*  RS0
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).equals("T") || ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).equals("Y"))) //Natural: IF ADC-ACCT-CDE ( #A-INDX ) = 'T' OR = 'Y'
                {
                    pnd_Total_Number_Rec.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-NUMBER-REC
                    //*  RS0
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).equals("Y")))                                           //Natural: IF ADC-ACCT-CDE ( #A-INDX ) = 'Y'
                    {
                        pnd_Stbl_Fund.setValue(true);                                                                                                                     //Natural: MOVE TRUE TO #STBL-FUND
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Stbl_Fund.setValue(false);                                                                                                                    //Natural: MOVE FALSE TO #STBL-FUND
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-ACCUMULATIONS
                    sub_Calculate_Tiaa_Accumulations();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  RS0
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("T") && ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("Y")  //Natural: IF ADC-ACCT-CDE ( #A-INDX ) NE 'T' AND ADC-ACCT-CDE ( #A-INDX ) NE 'Y' AND ADC-ACCT-CDE ( #A-INDX ) NE ' '
                    && ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(" ")))
                {
                    pnd_Total_Number_Rec.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-NUMBER-REC
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-PRODUCT-ACCUMULATION
                    sub_Calculate_Cref_Product_Accumulation();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1620"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1620"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: AT TOP OF PAGE ( 1 );//Natural: END-WORK
        PND_PND_L1620_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom, "pnd_pnd_L1620");                                                                             //Natural: ESCAPE BOTTOM ( ##L1620. )
            if (true) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM BREAK-OF-ACCOUNTING-DATE-RTN
        sub_Break_Of_Accounting_Date_Rtn();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB-RTN
        sub_End_Of_Job_Rtn();
        if (condition(Global.isEscape())) {return;}
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CONTROL-RECORD
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TIAA-ACCUMULATIONS
        //* *R #B-INDX 1   TO 60
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-CREF-PRODUCT-ACCUMULATION
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BREAK-OF-ACCOUNTING-DATE-RTN
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-JOB-RTN
    }
    private void sub_Read_Control_Record() throws Exception                                                                                                               //Natural: READ-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        vw_ads_Cntl_View.startDatabaseRead                                                                                                                                //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "READ01",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        READ01:
        while (condition(vw_ads_Cntl_View.readNextRow("READ01")))
        {
            if (condition(vw_ads_Cntl_View.getAstCOUNTER().equals(2)))                                                                                                    //Natural: IF *COUNTER = 2
            {
                if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                             //Natural: IF ADS-RPT-13 NE 0
                {
                    pnd_Date_A.setValueEdited(ads_Cntl_View_Ads_Rpt_13,new ReportEditMask("YYYYMMDD"));                                                                   //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE-A
                    pnd_Prev_Bsnss_Dte_N.setValue(pnd_Date_A_Pnd_Date_N);                                                                                                 //Natural: MOVE #DATE-N TO #PREV-BSNSS-DTE-N
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Prev_Bsnss_Dte_N.setValue(ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                                      //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #PREV-BSNSS-DTE-N
                }                                                                                                                                                         //Natural: END-IF
                pnd_Prev_Bsnss_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A);                                            //Natural: MOVE EDITED #PREV-BSNSS-DTE-A TO #PREV-BSNSS-DTE-D ( EM = YYYYMMDD )
                pnd_Today_Business_Date.setValueEdited(pnd_Prev_Bsnss_Dte_D,new ReportEditMask("YYYYMMDD"));                                                              //Natural: MOVE EDITED #PREV-BSNSS-DTE-D ( EM = YYYYMMDD ) TO #TODAY-BUSINESS-DATE
                pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Mm.setValue(pnd_Today_Business_Date_Pnd_Today_Business_Date_Mm);                                                  //Natural: MOVE #TODAY-BUSINESS-DATE-MM TO #CURR-MONTH-DATE-MM
                pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Dd.setValue(0);                                                                                                   //Natural: MOVE 00 TO #CURR-MONTH-DATE-DD
                //* *  MOVE ADS-CNTL-BSNSS-TMRRW-DTE                       TO
                //* *    #PREV-ACCOUNTING-DTE-YYYYMMDD
                if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                             //Natural: IF ADS-RPT-13 NE 0
                {
                    pnd_Prev_Accounting_Dte_Yyyymmdd.setValue(pnd_Date_A_Pnd_Date_N);                                                                                     //Natural: MOVE #DATE-N TO #PREV-ACCOUNTING-DTE-YYYYMMDD
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Prev_Accounting_Dte_Yyyymmdd.setValue(ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                          //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #PREV-ACCOUNTING-DTE-YYYYMMDD
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1005.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc);                  //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-CC TO #ACCOUNTING-DATE-MCDY-CC
                ldaAdsl1005.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy);                  //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-YY TO #ACCOUNTING-DATE-MCDY-YY
                ldaAdsl1005.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm);                  //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-MM TO #ACCOUNTING-DATE-MCDY-MM
                ldaAdsl1005.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd);                  //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-DD TO #ACCOUNTING-DATE-MCDY-DD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Calculate_Tiaa_Accumulations() throws Exception                                                                                                      //Natural: CALCULATE-TIAA-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  012604
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR02:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #MAX-RATE
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rate)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                          //Natural: IF ADC-DTL-TIAA-RATE-CDE ( #B-INDX ) = ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).notEquals(getZero())))                          //Natural: IF ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) NE 0
            {
                //*  TPA
                if (condition(pnd_Cnvrt_Annty_Optn.equals("1")))                                                                                                          //Natural: IF #CNVRT-ANNTY-OPTN = '1'
                {
                    ldaAdsl1005.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Tpa_Grn_Std_Maturity_Cnt().nadd(1);                                                                      //Natural: ADD 1 TO #TIAA-TPA-GRN-STD-MATURITY-CNT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(pnd_Cnvrt_Annty_Optn.equals("2")))                                                                                                      //Natural: IF #CNVRT-ANNTY-OPTN = '2'
                    {
                        ldaAdsl1005.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Cnt().nadd(1);                                                                 //Natural: ADD 1 TO #TIAA-IPRO-GRN-STD-MATURITY-CNT
                        ldaAdsl1005.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt().nadd(1);                                                                    //Natural: ADD 1 TO #TIAA-SUB-GRN-MATURITY-CNT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  STABLE RS0
                        if (condition(pnd_Stbl_Fund.getBoolean()))                                                                                                        //Natural: IF #STBL-FUND
                        {
                            ldaAdsl1005.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Cnt().nadd(1);                                                             //Natural: ADD 1 TO #TIAA-STBL-GRN-STD-MATURITY-CNT
                            ldaAdsl1005.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt().nadd(1);                                                                //Natural: ADD 1 TO #TIAA-SUB-GRN-MATURITY-CNT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaAdsl1005.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Cnt().nadd(1);                                                                  //Natural: ADD 1 TO #TIAA-GRN-STD-MATURITY-CNT
                            ldaAdsl1005.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt().nadd(1);                                                                //Natural: ADD 1 TO #TIAA-SUB-GRN-MATURITY-CNT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1005.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                       //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
                //*  TPA
                if (condition(pnd_Cnvrt_Annty_Optn.equals("1")))                                                                                                          //Natural: IF #CNVRT-ANNTY-OPTN = '1'
                {
                    ldaAdsl1005.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Tpa_Grn_Std_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-TPA-GRN-STD-MATURITY-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(pnd_Cnvrt_Annty_Optn.equals("2")))                                                                                                      //Natural: IF #CNVRT-ANNTY-OPTN = '2'
                    {
                        ldaAdsl1005.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-IPRO-GRN-STD-MATURITY-AMT
                        ldaAdsl1005.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-GRN-MATURITY-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  STABLE RS0
                        if (condition(pnd_Stbl_Fund.getBoolean()))                                                                                                        //Natural: IF #STBL-FUND
                        {
                            ldaAdsl1005.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-STBL-GRN-STD-MATURITY-AMT
                            ldaAdsl1005.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-GRN-MATURITY-AMT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaAdsl1005.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-STD-MATURITY-AMT
                            ldaAdsl1005.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-GRN-MATURITY-AMT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1005.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #GRAND-MATURITY-AMT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).notEquals(getZero())))                             //Natural: IF ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) NE 0
            {
                //*  RS0
                if (condition(pnd_Stbl_Fund.getBoolean()))                                                                                                                //Natural: IF #STBL-FUND
                {
                    ldaAdsl1005.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Cnt().nadd(1);                                                                     //Natural: ADD 1 TO #TIAA-STBL-GRN-GRD-MATURITY-CNT
                    ldaAdsl1005.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt().nadd(1);                                                                        //Natural: ADD 1 TO #TIAA-SUB-GRN-MATURITY-CNT
                    ldaAdsl1005.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                   //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaAdsl1005.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Cnt().nadd(1);                                                                          //Natural: ADD 1 TO #TIAA-GRN-GRD-MATURITY-CNT
                    ldaAdsl1005.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt().nadd(1);                                                                        //Natural: ADD 1 TO #TIAA-SUB-GRN-MATURITY-CNT
                    ldaAdsl1005.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                   //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl1005.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-GRN-MATURITY-AMT
                ldaAdsl1005.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #GRAND-MATURITY-AMT
                //*  RS0
                if (condition(pnd_Stbl_Fund.getBoolean()))                                                                                                                //Natural: IF #STBL-FUND
                {
                    ldaAdsl1005.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-STBL-GRN-GRD-MATURITY-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaAdsl1005.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-GRD-MATURITY-AMT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Calculate_Cref_Product_Accumulation() throws Exception                                                                                               //Natural: CALCULATE-CREF-PRODUCT-ACCUMULATION
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Cref_Prod_Found.setValue(false);                                                                                                                              //Natural: MOVE FALSE TO #CREF-PROD-FOUND
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR03:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                              //Natural: IF #PEC-ACCT-NAME-1 ( #B-INDX ) = ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue(pnd_All_Indx_Pnd_B_Indx).equals(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx)))) //Natural: IF #PEC-ACCT-NAME-1 ( #B-INDX ) = ADC-ACCT-CDE ( #A-INDX )
            {
                pnd_Cref_Prod_Found.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO #CREF-PROD-FOUND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cref_Prod_Found.setValue(false);                                                                                                                      //Natural: MOVE FALSE TO #CREF-PROD-FOUND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero())))                           //Natural: IF ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) NE 0
                {
                    ldaAdsl1005.getPnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                     //Natural: ADD 1 TO #CREF-GRN-MTH-MAT-CNT ( #B-INDX )
                    //*  RS0 EXCLUDE FROM TOTALS
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADC-ACCT-CDE ( #A-INDX ) NE 'R' AND ADC-ACCT-CDE ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1005.getPnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Cnt().nadd(1);                                                      //Natural: ADD 1 TO #CREF-SUB-GRN-MTH-MATURITY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1005.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                   //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
                    ldaAdsl1005.getPnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) TO #CREF-GRN-MTH-MAT-AMT ( #B-INDX )
                    //*  RS0 EXCLUDE FROM TOTALS
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADC-ACCT-CDE ( #A-INDX ) NE 'R' AND ADC-ACCT-CDE ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1005.getPnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) TO #CREF-SUB-GRN-MTH-MATURITY-AMT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1005.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) TO #GRAND-MATURITY-AMT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero())))                          //Natural: IF ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) NE 0
                {
                    ldaAdsl1005.getPnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                    //Natural: ADD 1 TO #CREF-GRN-ANN-MAT-CNT ( #B-INDX )
                    //*  RS0 EXCLUDE FROM TOTALS
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADC-ACCT-CDE ( #A-INDX ) NE 'R' AND ADC-ACCT-CDE ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1005.getPnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Cnt().nadd(1);                                                     //Natural: ADD 1 TO #CREF-SUB-GRN-ANN-MATURITY-CNT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1005.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                   //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
                    ldaAdsl1005.getPnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) TO #CREF-GRN-ANN-MAT-AMT ( #B-INDX )
                    //*  RS0 EXCLUDE FROM TOTALS
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADC-ACCT-CDE ( #A-INDX ) NE 'R' AND ADC-ACCT-CDE ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl1005.getPnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) TO #CREF-SUB-GRN-ANN-MATURITY-AMT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl1005.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) TO #GRAND-MATURITY-AMT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Break_Of_Accounting_Date_Rtn() throws Exception                                                                                                      //Natural: BREAK-OF-ACCOUNTING-DATE-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm783.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM783'
        ldaAdsl1005.getPnd_Tiaa_Grand_Accum().reset();                                                                                                                    //Natural: RESET #TIAA-GRAND-ACCUM #TIAA-SUB-GRN-ACCUM #CREF-MONTHLY-GRAND-ACCUM ( * ) #CREF-ANNUALLY-GRAND-ACCUM ( * ) #CREF-MONTHLY-SUB-GRAND-ACCUM #CREF-ANNUALLY-SUB-GRAND-ACCUM #CREF-AND-TIAA-GRAND-ACCUM
        ldaAdsl1005.getPnd_Tiaa_Sub_Grn_Accum().reset();
        ldaAdsl1005.getPnd_Cref_Monthly_Grand_Accum().getValue("*").reset();
        ldaAdsl1005.getPnd_Cref_Annually_Grand_Accum().getValue("*").reset();
        ldaAdsl1005.getPnd_Cref_Monthly_Sub_Grand_Accum().reset();
        ldaAdsl1005.getPnd_Cref_Annually_Sub_Grand_Accum().reset();
        ldaAdsl1005.getPnd_Cref_And_Tiaa_Grand_Accum().reset();
    }
    private void sub_End_Of_Job_Rtn() throws Exception                                                                                                                    //Natural: END-OF-JOB-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        if (condition(pnd_Total_Number_Rec.equals(getZero())))                                                                                                            //Natural: IF #TOTAL-NUMBER-REC = 0
        {
            getReports().write(1, ReportOption.NOTITLE,"*********************************************************");                                                      //Natural: WRITE ( 1 ) '*********************************************************'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"* MONTH TO DATE FINAL PREMIUM ACCUMULATION - JOB  ENDED *");                                                      //Natural: WRITE ( 1 ) '* MONTH TO DATE FINAL PREMIUM ACCUMULATION - JOB  ENDED *'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"*            NO TRANSACTIONS PROCESSED TODAY            *");                                                      //Natural: WRITE ( 1 ) '*            NO TRANSACTIONS PROCESSED TODAY            *'
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,"*********************************************************");                                                      //Natural: WRITE ( 1 ) '*********************************************************'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm782.class));                                                                   //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM782'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=79 PS=60");
        Global.format(1, "LS=79 PS=60");
        Global.format(2, "LS=79 PS=60");
    }
}
