/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:03:20 PM
**        * FROM NATURAL PROGRAM : Adsp590
************************************************************
**        * FILE NAME            : Adsp590.java
**        * CLASS NAME           : Adsp590
**        * INSTANCE NAME        : Adsp590
************************************************************
***************************************************************
* ADSP590
*
* FINAL PREMIUM CONFIRMATION LETTER
*
* WRITTEN AUGUST 1997 BY DON MEADE
* REVISED SEPT 1997 BY DON MEADE TO SKIP SETTLEMENTS WITH MISSING
*    DA OR IA RESULT RECORDS
* REVISED OCT 22, 1997 TO USE NAZ-FNL-PRM-CNTRL-DTE INSTEAD OF
*    NAZ-FNL-PRM-LTTR-DTE (WHICH IS NOT UPDATED IN NAZP595)
* REVISED NOV 4, 1997 TO FIX START DATE PROBLEM
* REVISED MARCH, 1998 FOR CREF/REA MONTHLY INCOME UNITS
*   MAR 20 - DON'T CALL POST FOR CREF ANNL INC IF NO ANNL INCOME!
*   MAR 20 - CHECK FOR ANNL OR MNTHLY INCOME
* REVISED APRIL, 2000 TO READ ONLY MOST RECENT IAA TRANS RECORD
*   IN ORDER TO CORRECT PRINTING OF NEXT PYT DATE FOR PARTICIPANTS
*   WITH 19- AND 70-DAY RESETTLEMENTS.
* 10/23/2003 OS 99 RATES CHANGES. SCAN FOR 102303.
* 04/26/2004 H. KAKADIA MODIFIED FOR ANNUITIZATION SUNGUARD
* 03/17/2008 E.MELNIK   ROTH 403B/401K. RECOMPILED FOR ADSL401A.
* 03/17/2008 E.MELNIK   ROTH 403B/401K CHANGSS.  RECOMPILED FOR
*                       NEW ADSL401/ADSL450/ADSLCNTL.
* 01/14/2009 R. SACHARNY CALL ADSN888 INSTEAD OF NECN4000(CFN) RS0
* 01/27/2009 E. MELNIK   TIAA ACCESS/STABLE RETURN ENHANCEMENTS.
*                        MARKED BY EM - 012709.
* 04/13/2010 D.E.ANDER   IN TIAA-WRITE ADD CODE FOR ADABAS REPLAT-
*                        FORM PCPOP MARKED BY - DEA.
* 9/07/2010  D.E.ANDER  CHG #MAX-RATE FROM 80 TO 99 OCCURS, PRD FX DEA
* 03/01/12 E. MELNIK   RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                      UPDATED LINKAGE AREAS.  CHANGES MARKED BY
*                      EM - 030112.
* 01/18/13 E. MELNIK   TNG PROJECT.  RESTOW TO INCLUDE UPDATED LINKAGE
*                      AREAS.
* 10/04/2013 E. MELNIK CREF/REA REDESIGN CHANGES.
*                      MARKED BY EM - 101413.
* 05/24/2016 E. MELNIK CHANGE FOR INC324505.  MARKED BY EM - 052416.
* 08/22/2016 E. MELNIK CHANGE FOR INC3364348/INC3364357
*                      MARKED BY EM - 082216.
* 08/2016   E. MELNIK PIN EXPANSION CHANGES.  MARKED BY EM - 083016.
* 03/03/2017 R.CARREON  PIN EXPANSION 03032017
* 11/14/2018 E. MELNIK  CORRECTION TO DATE MOVED TO MS-MESSAGE-DATE (2)
*                       FOR QUARTERLY REQUESTS.  MARKED BY 111418.
***************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp590 extends BLNatBase
{
    // Data Areas
    private LdaAdsl401a ldaAdsl401a;
    private LdaAdslcntl ldaAdslcntl;
    private LdaAdsl401 ldaAdsl401;
    private LdaAdsl450 ldaAdsl450;
    private PdaPsta9500 pdaPsta9500;
    private PdaPsta9501 pdaPsta9501;
    private PdaPsta9610 pdaPsta9610;
    private PdaPsta9612 pdaPsta9612;
    private PdaPsta9533 pdaPsta9533;
    private LdaPstl6075 ldaPstl6075;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaCwfpdaun pdaCwfpdaun;
    private PdaPdqa650 pdaPdqa650;
    private PdaAdsa888 pdaAdsa888;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_trn;
    private DbsField trn_Trans_Check_Dte;

    private DbsGroup trn__R_Field_1;
    private DbsField trn_Pnd_Chk_Dte_Yyyy;
    private DbsField trn_Pnd_Chk_Dte_Mm;
    private DbsField trn_Pnd_Chk_Dte_Dd;
    private DbsField trn_Trans_Cde;
    private DbsField trn_Trans_Ppcn_Nbr;

    private DataAccessProgramView vw_ads_Ia_Rslt2;
    private DbsField ads_Ia_Rslt2_Rqst_Id;
    private DbsField ads_Ia_Rslt2_Adi_Ia_Rcrd_Cde;
    private DbsField ads_Ia_Rslt2_Adi_Sqnce_Nbr;
    private DbsField ads_Ia_Rslt2_Adi_Stts_Cde;
    private DbsField ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data;

    private DbsGroup ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt;
    private DbsField pnd_Adi_Sd1;

    private DbsGroup pnd_Adi_Sd1__R_Field_2;
    private DbsField pnd_Adi_Sd1_Pnd_Adi_Sd1_Rqst_Id;
    private DbsField pnd_Adi_Sd1_Pnd_Adi_Sd1_Ia_Rcrd_Cde;
    private DbsField pnd_Adi_Sd1_Pnd_Adi_Sd1_Sqnce_Nbr;
    private DbsField pnd_Trn_Cntrct_Key;

    private DbsGroup pnd_Trn_Cntrct_Key__R_Field_3;
    private DbsField pnd_Trn_Cntrct_Key_Pnd_Trn_Key_Ia_Ppcn;
    private DbsField pnd_Trn_Cntrct_Key_Pnd_Trn_Key_Payee_Cde;
    private DbsField pnd_Trn_Cntrct_Key_Pnd_Trn_Key_Inv_Dte;
    private DbsField pnd_Trn_Cntrct_Key_Pnd_Trn_Key_Trn_Cde;

    private DbsGroup pnd_Storage;
    private DbsField pnd_Storage_Pnd_Ia_Tiaa_Contract;
    private DbsField pnd_Storage_Pnd_Ia_Cref_Contract;
    private DbsField pnd_Storage_Pnd_Effctv_Dte;

    private DbsGroup pnd_Storage__R_Field_4;
    private DbsField pnd_Storage_Pnd_Effctv_Dte_Mm;
    private DbsField pnd_Storage_Pnd_Effctv_Dte_Dd;
    private DbsField pnd_Storage_Pnd_Effctv_Dte_Yyyy;
    private DbsField pnd_Storage_Pnd_Issue_Dte;
    private DbsField pnd_Storage_Pnd_Nxt_Pyt_Dte;

    private DbsGroup pnd_Storage__R_Field_5;
    private DbsField pnd_Storage_Pnd_Nxt_Pyt_Mm;
    private DbsField pnd_Storage_Pnd_Nxt_Pyt_Dd;
    private DbsField pnd_Storage_Pnd_Nxt_Pyt_Yyyy;
    private DbsField pnd_Storage_Pnd_Nad_Acct_Stts_Cde;

    private DbsGroup pnd_Storage__R_Field_6;
    private DbsField pnd_Storage_Pnd_Nad_Stts_Byte_1;
    private DbsField pnd_Storage_Pnd_Nad_Stts_Rest;
    private DbsField pnd_Storage_Pnd_Da_Tiaa_Nbr;
    private DbsField pnd_Storage_Pnd_Da_Cref_Nbr;
    private DbsField pnd_Storage_Pnd_Tiaa_Addit_Accum;
    private DbsField pnd_Storage_Pnd_Rea_Addit_Accum;
    private DbsField pnd_Storage_Pnd_Cref_Addit_Accum;
    private DbsField pnd_Storage_Pnd_Tiaa_Grd_Guar_Inc;
    private DbsField pnd_Storage_Pnd_Tiaa_Std_Guar_Inc;
    private DbsField pnd_Storage_Pnd_Tiaa_Grd_Est_Inc;
    private DbsField pnd_Storage_Pnd_Tiaa_Std_Est_Inc;
    private DbsField pnd_Storage_Pnd_Cref_Inc_Acct_Cde;
    private DbsField pnd_Storage_Pnd_Cref_Inc_Tkr_Symbl;
    private DbsField pnd_Storage_Pnd_Cref_Inc_Annl_Units;
    private DbsField pnd_Storage_Pnd_Cref_Inc_Annl_Unit_Val;
    private DbsField pnd_Storage_Pnd_Cref_Inc_Annl_Est_Inc;
    private DbsField pnd_Storage_Pnd_Cref_Inc_Mnthly_Units;
    private DbsField pnd_Storage_Pnd_Cref_Inc_Mnthly_Unit_Val;
    private DbsField pnd_Storage_Pnd_Cref_Inc_Mnthly_Est_Inc;
    private DbsField pnd_Storage_Pnd_Rea_Annl_Units;
    private DbsField pnd_Storage_Pnd_Rea_Annl_Unit_Val;
    private DbsField pnd_Storage_Pnd_Rea_Annl_Add_Inc;
    private DbsField pnd_Storage_Pnd_Rea_Mnthly_Units;
    private DbsField pnd_Storage_Pnd_Rea_Mnthly_Unit_Val;
    private DbsField pnd_Storage_Pnd_Rea_Mnthly_Add_Inc;
    private DbsField pnd_Storage_Pnd_Acc_Annl_Units;
    private DbsField pnd_Storage_Pnd_Acc_Annl_Unit_Val;
    private DbsField pnd_Storage_Pnd_Acc_Annl_Add_Inc;
    private DbsField pnd_Storage_Pnd_Acc_Mnthly_Units;
    private DbsField pnd_Storage_Pnd_Acc_Mnthly_Unit_Val;
    private DbsField pnd_Storage_Pnd_Acc_Mnthly_Add_Inc;
    private DbsField pnd_Storage_Pnd_Tot_Rea_Add_Accum;
    private DbsField pnd_Storage_Pnd_Tot_Tiaa_Add_Accum;
    private DbsField pnd_Storage_Pnd_Tot_Cref_Add_Accum;
    private DbsField pnd_Storage_Pnd_Tot_Tiaa_Guar_Inc;
    private DbsField pnd_Storage_Pnd_Tot_Tiaa_Est_Inc;
    private DbsField pnd_Storage_Pnd_Tot_Cref_Add_Inc;
    private DbsField pnd_Storage_Pnd_Tot_Rea_Add_Inc;
    private DbsField pnd_Storage_Pnd_Tot_Acc_Add_Inc;
    private DbsField pnd_Storage_Pnd_Frequency;
    private DbsField pnd_Storage_Pnd_Found_Nad;
    private DbsField pnd_Storage_Pnd_Found_Nai;
    private DbsField pnd_Storage_Pnd_Found_Ia_Rslt_Fund;
    private DbsField pnd_Storage_Pnd_Contract_Cnt;
    private DbsField pnd_Storage_Pnd_Cref_Rea_Ind;
    private DbsField pnd_Tiebreak;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_7;
    private DbsField pnd_Date_A_Pnd_Date_Mmddyyyy;

    private DbsGroup pnd_Date_A__R_Field_8;
    private DbsField pnd_Date_A_Pnd_Date_Mm;
    private DbsField pnd_Date_A_Pnd_Date_Dd;
    private DbsField pnd_Date_A_Pnd_Date_Yyyy;
    private DbsField pnd_Strt_Dte;
    private DbsField pnd_I;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Max_Rates;
    private DbsField pnd_Tckr_Symbl;
    private DbsField pnd_Indx;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl401a = new LdaAdsl401a();
        registerRecord(ldaAdsl401a);
        registerRecord(ldaAdsl401a.getVw_ads_Cntrct_View());
        ldaAdslcntl = new LdaAdslcntl();
        registerRecord(ldaAdslcntl);
        registerRecord(ldaAdslcntl.getVw_ads_Cntl_View());
        ldaAdsl401 = new LdaAdsl401();
        registerRecord(ldaAdsl401);
        registerRecord(ldaAdsl401.getVw_ads_Prtcpnt_View());
        ldaAdsl450 = new LdaAdsl450();
        registerRecord(ldaAdsl450);
        registerRecord(ldaAdsl450.getVw_ads_Ia_Rslt_View());
        localVariables = new DbsRecord();
        pdaPsta9500 = new PdaPsta9500(localVariables);
        pdaPsta9501 = new PdaPsta9501(localVariables);
        pdaPsta9610 = new PdaPsta9610(localVariables);
        pdaPsta9612 = new PdaPsta9612(localVariables);
        pdaPsta9533 = new PdaPsta9533(localVariables);
        ldaPstl6075 = new LdaPstl6075();
        registerRecord(ldaPstl6075);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfpdaus = new PdaCwfpdaus(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);
        pdaPdqa650 = new PdaPdqa650(localVariables);
        pdaAdsa888 = new PdaAdsa888(localVariables);

        // Local Variables

        vw_trn = new DataAccessProgramView(new NameInfo("vw_trn", "TRN"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        trn_Trans_Check_Dte = vw_trn.getRecord().newFieldInGroup("trn_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "TRANS_CHECK_DTE");

        trn__R_Field_1 = vw_trn.getRecord().newGroupInGroup("trn__R_Field_1", "REDEFINE", trn_Trans_Check_Dte);
        trn_Pnd_Chk_Dte_Yyyy = trn__R_Field_1.newFieldInGroup("trn_Pnd_Chk_Dte_Yyyy", "#CHK-DTE-YYYY", FieldType.NUMERIC, 4);
        trn_Pnd_Chk_Dte_Mm = trn__R_Field_1.newFieldInGroup("trn_Pnd_Chk_Dte_Mm", "#CHK-DTE-MM", FieldType.NUMERIC, 2);
        trn_Pnd_Chk_Dte_Dd = trn__R_Field_1.newFieldInGroup("trn_Pnd_Chk_Dte_Dd", "#CHK-DTE-DD", FieldType.NUMERIC, 2);
        trn_Trans_Cde = vw_trn.getRecord().newFieldInGroup("trn_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "TRANS_CDE");
        trn_Trans_Ppcn_Nbr = vw_trn.getRecord().newFieldInGroup("trn_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "TRANS_PPCN_NBR");
        registerRecord(vw_trn);

        vw_ads_Ia_Rslt2 = new DataAccessProgramView(new NameInfo("vw_ads_Ia_Rslt2", "ADS-IA-RSLT2"), "ADS_IA_RSLT", "ADS_IA_RSLT", DdmPeriodicGroups.getInstance().getGroups("ADS_IA_RSLT"));
        ads_Ia_Rslt2_Rqst_Id = vw_ads_Ia_Rslt2.getRecord().newFieldInGroup("ads_Ia_Rslt2_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "RQST_ID");
        ads_Ia_Rslt2_Adi_Ia_Rcrd_Cde = vw_ads_Ia_Rslt2.getRecord().newFieldInGroup("ads_Ia_Rslt2_Adi_Ia_Rcrd_Cde", "ADI-IA-RCRD-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "ADI_IA_RCRD_CDE");
        ads_Ia_Rslt2_Adi_Sqnce_Nbr = vw_ads_Ia_Rslt2.getRecord().newFieldInGroup("ads_Ia_Rslt2_Adi_Sqnce_Nbr", "ADI-SQNCE-NBR", FieldType.NUMERIC, 3, 
            RepeatingFieldStrategy.None, "ADI_SQNCE_NBR");
        ads_Ia_Rslt2_Adi_Stts_Cde = vw_ads_Ia_Rslt2.getRecord().newFieldInGroup("ads_Ia_Rslt2_Adi_Stts_Cde", "ADI-STTS-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "ADI_STTS_CDE");
        ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data = vw_ads_Ia_Rslt2.getRecord().newFieldInGroup("ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data", "C*ADI-DTL-TIAA-DATA", 
            RepeatingFieldStrategy.CAsteriskVariable, "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");

        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data = vw_ads_Ia_Rslt2.getRecord().newGroupInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data", "ADI-DTL-TIAA-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd", "ADI-DTL-TIAA-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt", "ADI-DTL-TIAA-DA-STD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt", "ADI-DTL-TIAA-DA-GRD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd", "ADI-DTL-TIAA-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_ACCT_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd", "ADI-DTL-TIAA-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_IA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst", "ADI-DTL-TIAA-EFF-RTE-INTRST", 
            FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_EFF_RTE_INTRST", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt", "ADI-DTL-TIAA-GRD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt", "ADI-DTL-TIAA-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt", "ADI-DTL-FNL-GRD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt", "ADI-DTL-FNL-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt", "ADI-DTL-TIAA-STD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt", "ADI-DTL-TIAA-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt", "ADI-DTL-FNL-STD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt", "ADI-DTL-FNL-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        registerRecord(vw_ads_Ia_Rslt2);

        pnd_Adi_Sd1 = localVariables.newFieldInRecord("pnd_Adi_Sd1", "#ADI-SD1", FieldType.STRING, 41);

        pnd_Adi_Sd1__R_Field_2 = localVariables.newGroupInRecord("pnd_Adi_Sd1__R_Field_2", "REDEFINE", pnd_Adi_Sd1);
        pnd_Adi_Sd1_Pnd_Adi_Sd1_Rqst_Id = pnd_Adi_Sd1__R_Field_2.newFieldInGroup("pnd_Adi_Sd1_Pnd_Adi_Sd1_Rqst_Id", "#ADI-SD1-RQST-ID", FieldType.STRING, 
            35);
        pnd_Adi_Sd1_Pnd_Adi_Sd1_Ia_Rcrd_Cde = pnd_Adi_Sd1__R_Field_2.newFieldInGroup("pnd_Adi_Sd1_Pnd_Adi_Sd1_Ia_Rcrd_Cde", "#ADI-SD1-IA-RCRD-CDE", FieldType.STRING, 
            3);
        pnd_Adi_Sd1_Pnd_Adi_Sd1_Sqnce_Nbr = pnd_Adi_Sd1__R_Field_2.newFieldInGroup("pnd_Adi_Sd1_Pnd_Adi_Sd1_Sqnce_Nbr", "#ADI-SD1-SQNCE-NBR", FieldType.NUMERIC, 
            3);
        pnd_Trn_Cntrct_Key = localVariables.newFieldInRecord("pnd_Trn_Cntrct_Key", "#TRN-CNTRCT-KEY", FieldType.STRING, 27);

        pnd_Trn_Cntrct_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Trn_Cntrct_Key__R_Field_3", "REDEFINE", pnd_Trn_Cntrct_Key);
        pnd_Trn_Cntrct_Key_Pnd_Trn_Key_Ia_Ppcn = pnd_Trn_Cntrct_Key__R_Field_3.newFieldInGroup("pnd_Trn_Cntrct_Key_Pnd_Trn_Key_Ia_Ppcn", "#TRN-KEY-IA-PPCN", 
            FieldType.STRING, 10);
        pnd_Trn_Cntrct_Key_Pnd_Trn_Key_Payee_Cde = pnd_Trn_Cntrct_Key__R_Field_3.newFieldInGroup("pnd_Trn_Cntrct_Key_Pnd_Trn_Key_Payee_Cde", "#TRN-KEY-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Trn_Cntrct_Key_Pnd_Trn_Key_Inv_Dte = pnd_Trn_Cntrct_Key__R_Field_3.newFieldInGroup("pnd_Trn_Cntrct_Key_Pnd_Trn_Key_Inv_Dte", "#TRN-KEY-INV-DTE", 
            FieldType.NUMERIC, 12);
        pnd_Trn_Cntrct_Key_Pnd_Trn_Key_Trn_Cde = pnd_Trn_Cntrct_Key__R_Field_3.newFieldInGroup("pnd_Trn_Cntrct_Key_Pnd_Trn_Key_Trn_Cde", "#TRN-KEY-TRN-CDE", 
            FieldType.NUMERIC, 3);

        pnd_Storage = localVariables.newGroupInRecord("pnd_Storage", "#STORAGE");
        pnd_Storage_Pnd_Ia_Tiaa_Contract = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Ia_Tiaa_Contract", "#IA-TIAA-CONTRACT", FieldType.STRING, 10);
        pnd_Storage_Pnd_Ia_Cref_Contract = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Ia_Cref_Contract", "#IA-CREF-CONTRACT", FieldType.STRING, 10);
        pnd_Storage_Pnd_Effctv_Dte = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Effctv_Dte", "#EFFCTV-DTE", FieldType.NUMERIC, 8);

        pnd_Storage__R_Field_4 = pnd_Storage.newGroupInGroup("pnd_Storage__R_Field_4", "REDEFINE", pnd_Storage_Pnd_Effctv_Dte);
        pnd_Storage_Pnd_Effctv_Dte_Mm = pnd_Storage__R_Field_4.newFieldInGroup("pnd_Storage_Pnd_Effctv_Dte_Mm", "#EFFCTV-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Storage_Pnd_Effctv_Dte_Dd = pnd_Storage__R_Field_4.newFieldInGroup("pnd_Storage_Pnd_Effctv_Dte_Dd", "#EFFCTV-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Storage_Pnd_Effctv_Dte_Yyyy = pnd_Storage__R_Field_4.newFieldInGroup("pnd_Storage_Pnd_Effctv_Dte_Yyyy", "#EFFCTV-DTE-YYYY", FieldType.NUMERIC, 
            2);
        pnd_Storage_Pnd_Issue_Dte = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.NUMERIC, 8);
        pnd_Storage_Pnd_Nxt_Pyt_Dte = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Nxt_Pyt_Dte", "#NXT-PYT-DTE", FieldType.NUMERIC, 8);

        pnd_Storage__R_Field_5 = pnd_Storage.newGroupInGroup("pnd_Storage__R_Field_5", "REDEFINE", pnd_Storage_Pnd_Nxt_Pyt_Dte);
        pnd_Storage_Pnd_Nxt_Pyt_Mm = pnd_Storage__R_Field_5.newFieldInGroup("pnd_Storage_Pnd_Nxt_Pyt_Mm", "#NXT-PYT-MM", FieldType.NUMERIC, 2);
        pnd_Storage_Pnd_Nxt_Pyt_Dd = pnd_Storage__R_Field_5.newFieldInGroup("pnd_Storage_Pnd_Nxt_Pyt_Dd", "#NXT-PYT-DD", FieldType.NUMERIC, 2);
        pnd_Storage_Pnd_Nxt_Pyt_Yyyy = pnd_Storage__R_Field_5.newFieldInGroup("pnd_Storage_Pnd_Nxt_Pyt_Yyyy", "#NXT-PYT-YYYY", FieldType.NUMERIC, 4);
        pnd_Storage_Pnd_Nad_Acct_Stts_Cde = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Nad_Acct_Stts_Cde", "#NAD-ACCT-STTS-CDE", FieldType.STRING, 3);

        pnd_Storage__R_Field_6 = pnd_Storage.newGroupInGroup("pnd_Storage__R_Field_6", "REDEFINE", pnd_Storage_Pnd_Nad_Acct_Stts_Cde);
        pnd_Storage_Pnd_Nad_Stts_Byte_1 = pnd_Storage__R_Field_6.newFieldInGroup("pnd_Storage_Pnd_Nad_Stts_Byte_1", "#NAD-STTS-BYTE-1", FieldType.STRING, 
            1);
        pnd_Storage_Pnd_Nad_Stts_Rest = pnd_Storage__R_Field_6.newFieldInGroup("pnd_Storage_Pnd_Nad_Stts_Rest", "#NAD-STTS-REST", FieldType.STRING, 2);
        pnd_Storage_Pnd_Da_Tiaa_Nbr = pnd_Storage.newFieldArrayInGroup("pnd_Storage_Pnd_Da_Tiaa_Nbr", "#DA-TIAA-NBR", FieldType.STRING, 8, new DbsArrayController(1, 
            6));
        pnd_Storage_Pnd_Da_Cref_Nbr = pnd_Storage.newFieldArrayInGroup("pnd_Storage_Pnd_Da_Cref_Nbr", "#DA-CREF-NBR", FieldType.STRING, 8, new DbsArrayController(1, 
            6));
        pnd_Storage_Pnd_Tiaa_Addit_Accum = pnd_Storage.newFieldArrayInGroup("pnd_Storage_Pnd_Tiaa_Addit_Accum", "#TIAA-ADDIT-ACCUM", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 6));
        pnd_Storage_Pnd_Rea_Addit_Accum = pnd_Storage.newFieldArrayInGroup("pnd_Storage_Pnd_Rea_Addit_Accum", "#REA-ADDIT-ACCUM", FieldType.NUMERIC, 9, 
            2, new DbsArrayController(1, 6));
        pnd_Storage_Pnd_Cref_Addit_Accum = pnd_Storage.newFieldArrayInGroup("pnd_Storage_Pnd_Cref_Addit_Accum", "#CREF-ADDIT-ACCUM", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 6));
        pnd_Storage_Pnd_Tiaa_Grd_Guar_Inc = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Tiaa_Grd_Guar_Inc", "#TIAA-GRD-GUAR-INC", FieldType.NUMERIC, 
            9, 2);
        pnd_Storage_Pnd_Tiaa_Std_Guar_Inc = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Tiaa_Std_Guar_Inc", "#TIAA-STD-GUAR-INC", FieldType.NUMERIC, 
            9, 2);
        pnd_Storage_Pnd_Tiaa_Grd_Est_Inc = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Tiaa_Grd_Est_Inc", "#TIAA-GRD-EST-INC", FieldType.NUMERIC, 9, 
            2);
        pnd_Storage_Pnd_Tiaa_Std_Est_Inc = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Tiaa_Std_Est_Inc", "#TIAA-STD-EST-INC", FieldType.NUMERIC, 9, 
            2);
        pnd_Storage_Pnd_Cref_Inc_Acct_Cde = pnd_Storage.newFieldArrayInGroup("pnd_Storage_Pnd_Cref_Inc_Acct_Cde", "#CREF-INC-ACCT-CDE", FieldType.STRING, 
            1, new DbsArrayController(1, 18));
        pnd_Storage_Pnd_Cref_Inc_Tkr_Symbl = pnd_Storage.newFieldArrayInGroup("pnd_Storage_Pnd_Cref_Inc_Tkr_Symbl", "#CREF-INC-TKR-SYMBL", FieldType.STRING, 
            10, new DbsArrayController(1, 18));
        pnd_Storage_Pnd_Cref_Inc_Annl_Units = pnd_Storage.newFieldArrayInGroup("pnd_Storage_Pnd_Cref_Inc_Annl_Units", "#CREF-INC-ANNL-UNITS", FieldType.NUMERIC, 
            8, 3, new DbsArrayController(1, 18));
        pnd_Storage_Pnd_Cref_Inc_Annl_Unit_Val = pnd_Storage.newFieldArrayInGroup("pnd_Storage_Pnd_Cref_Inc_Annl_Unit_Val", "#CREF-INC-ANNL-UNIT-VAL", 
            FieldType.NUMERIC, 9, 4, new DbsArrayController(1, 18));
        pnd_Storage_Pnd_Cref_Inc_Annl_Est_Inc = pnd_Storage.newFieldArrayInGroup("pnd_Storage_Pnd_Cref_Inc_Annl_Est_Inc", "#CREF-INC-ANNL-EST-INC", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 18));
        pnd_Storage_Pnd_Cref_Inc_Mnthly_Units = pnd_Storage.newFieldArrayInGroup("pnd_Storage_Pnd_Cref_Inc_Mnthly_Units", "#CREF-INC-MNTHLY-UNITS", FieldType.NUMERIC, 
            8, 3, new DbsArrayController(1, 18));
        pnd_Storage_Pnd_Cref_Inc_Mnthly_Unit_Val = pnd_Storage.newFieldArrayInGroup("pnd_Storage_Pnd_Cref_Inc_Mnthly_Unit_Val", "#CREF-INC-MNTHLY-UNIT-VAL", 
            FieldType.NUMERIC, 9, 4, new DbsArrayController(1, 18));
        pnd_Storage_Pnd_Cref_Inc_Mnthly_Est_Inc = pnd_Storage.newFieldArrayInGroup("pnd_Storage_Pnd_Cref_Inc_Mnthly_Est_Inc", "#CREF-INC-MNTHLY-EST-INC", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 18));
        pnd_Storage_Pnd_Rea_Annl_Units = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Rea_Annl_Units", "#REA-ANNL-UNITS", FieldType.NUMERIC, 9, 3);
        pnd_Storage_Pnd_Rea_Annl_Unit_Val = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Rea_Annl_Unit_Val", "#REA-ANNL-UNIT-VAL", FieldType.NUMERIC, 
            7, 4);
        pnd_Storage_Pnd_Rea_Annl_Add_Inc = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Rea_Annl_Add_Inc", "#REA-ANNL-ADD-INC", FieldType.NUMERIC, 9, 
            2);
        pnd_Storage_Pnd_Rea_Mnthly_Units = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Rea_Mnthly_Units", "#REA-MNTHLY-UNITS", FieldType.NUMERIC, 9, 
            3);
        pnd_Storage_Pnd_Rea_Mnthly_Unit_Val = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Rea_Mnthly_Unit_Val", "#REA-MNTHLY-UNIT-VAL", FieldType.NUMERIC, 
            7, 4);
        pnd_Storage_Pnd_Rea_Mnthly_Add_Inc = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Rea_Mnthly_Add_Inc", "#REA-MNTHLY-ADD-INC", FieldType.NUMERIC, 
            9, 2);
        pnd_Storage_Pnd_Acc_Annl_Units = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Acc_Annl_Units", "#ACC-ANNL-UNITS", FieldType.NUMERIC, 9, 3);
        pnd_Storage_Pnd_Acc_Annl_Unit_Val = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Acc_Annl_Unit_Val", "#ACC-ANNL-UNIT-VAL", FieldType.NUMERIC, 
            7, 4);
        pnd_Storage_Pnd_Acc_Annl_Add_Inc = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Acc_Annl_Add_Inc", "#ACC-ANNL-ADD-INC", FieldType.NUMERIC, 9, 
            2);
        pnd_Storage_Pnd_Acc_Mnthly_Units = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Acc_Mnthly_Units", "#ACC-MNTHLY-UNITS", FieldType.NUMERIC, 9, 
            3);
        pnd_Storage_Pnd_Acc_Mnthly_Unit_Val = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Acc_Mnthly_Unit_Val", "#ACC-MNTHLY-UNIT-VAL", FieldType.NUMERIC, 
            7, 4);
        pnd_Storage_Pnd_Acc_Mnthly_Add_Inc = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Acc_Mnthly_Add_Inc", "#ACC-MNTHLY-ADD-INC", FieldType.NUMERIC, 
            9, 2);
        pnd_Storage_Pnd_Tot_Rea_Add_Accum = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Tot_Rea_Add_Accum", "#TOT-REA-ADD-ACCUM", FieldType.NUMERIC, 
            9, 2);
        pnd_Storage_Pnd_Tot_Tiaa_Add_Accum = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Tot_Tiaa_Add_Accum", "#TOT-TIAA-ADD-ACCUM", FieldType.NUMERIC, 
            9, 2);
        pnd_Storage_Pnd_Tot_Cref_Add_Accum = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Tot_Cref_Add_Accum", "#TOT-CREF-ADD-ACCUM", FieldType.NUMERIC, 
            9, 2);
        pnd_Storage_Pnd_Tot_Tiaa_Guar_Inc = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Tot_Tiaa_Guar_Inc", "#TOT-TIAA-GUAR-INC", FieldType.NUMERIC, 
            9, 2);
        pnd_Storage_Pnd_Tot_Tiaa_Est_Inc = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Tot_Tiaa_Est_Inc", "#TOT-TIAA-EST-INC", FieldType.NUMERIC, 9, 
            2);
        pnd_Storage_Pnd_Tot_Cref_Add_Inc = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Tot_Cref_Add_Inc", "#TOT-CREF-ADD-INC", FieldType.NUMERIC, 9, 
            2);
        pnd_Storage_Pnd_Tot_Rea_Add_Inc = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Tot_Rea_Add_Inc", "#TOT-REA-ADD-INC", FieldType.NUMERIC, 9, 2);
        pnd_Storage_Pnd_Tot_Acc_Add_Inc = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Tot_Acc_Add_Inc", "#TOT-ACC-ADD-INC", FieldType.NUMERIC, 9, 2);
        pnd_Storage_Pnd_Frequency = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Frequency", "#FREQUENCY", FieldType.STRING, 15);
        pnd_Storage_Pnd_Found_Nad = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Found_Nad", "#FOUND-NAD", FieldType.BOOLEAN, 1);
        pnd_Storage_Pnd_Found_Nai = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Found_Nai", "#FOUND-NAI", FieldType.BOOLEAN, 1);
        pnd_Storage_Pnd_Found_Ia_Rslt_Fund = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Found_Ia_Rslt_Fund", "#FOUND-IA-RSLT-FUND", FieldType.BOOLEAN, 
            1);
        pnd_Storage_Pnd_Contract_Cnt = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Contract_Cnt", "#CONTRACT-CNT", FieldType.NUMERIC, 1);
        pnd_Storage_Pnd_Cref_Rea_Ind = pnd_Storage.newFieldInGroup("pnd_Storage_Pnd_Cref_Rea_Ind", "#CREF-REA-IND", FieldType.STRING, 1);
        pnd_Tiebreak = localVariables.newFieldInRecord("pnd_Tiebreak", "#TIEBREAK", FieldType.NUMERIC, 7);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_7 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_7", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_Mmddyyyy = pnd_Date_A__R_Field_7.newFieldInGroup("pnd_Date_A_Pnd_Date_Mmddyyyy", "#DATE-MMDDYYYY", FieldType.NUMERIC, 8);

        pnd_Date_A__R_Field_8 = pnd_Date_A__R_Field_7.newGroupInGroup("pnd_Date_A__R_Field_8", "REDEFINE", pnd_Date_A_Pnd_Date_Mmddyyyy);
        pnd_Date_A_Pnd_Date_Mm = pnd_Date_A__R_Field_8.newFieldInGroup("pnd_Date_A_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);
        pnd_Date_A_Pnd_Date_Dd = pnd_Date_A__R_Field_8.newFieldInGroup("pnd_Date_A_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);
        pnd_Date_A_Pnd_Date_Yyyy = pnd_Date_A__R_Field_8.newFieldInGroup("pnd_Date_A_Pnd_Date_Yyyy", "#DATE-YYYY", FieldType.NUMERIC, 4);
        pnd_Strt_Dte = localVariables.newFieldInRecord("pnd_Strt_Dte", "#STRT-DTE", FieldType.DATE);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 2);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 2);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Rates = localVariables.newFieldInRecord("pnd_Max_Rates", "#MAX-RATES", FieldType.PACKED_DECIMAL, 3);
        pnd_Tckr_Symbl = localVariables.newFieldArrayInRecord("pnd_Tckr_Symbl", "#TCKR-SYMBL", FieldType.STRING, 10, new DbsArrayController(1, 20));
        pnd_Indx = localVariables.newFieldInRecord("pnd_Indx", "#INDX", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_trn.reset();
        vw_ads_Ia_Rslt2.reset();

        ldaAdsl401a.initializeValues();
        ldaAdslcntl.initializeValues();
        ldaAdsl401.initializeValues();
        ldaAdsl450.initializeValues();
        ldaPstl6075.initializeValues();

        localVariables.reset();
        pnd_Storage_Pnd_Found_Nad.setInitialValue(true);
        pnd_Storage_Pnd_Found_Nai.setInitialValue(true);
        pnd_Storage_Pnd_Found_Ia_Rslt_Fund.setInitialValue(true);
        pnd_Storage_Pnd_Contract_Cnt.setInitialValue(0);
        pnd_Storage_Pnd_Cref_Rea_Ind.setInitialValue(" ");
        pnd_Tiebreak.setInitialValue(0);
        pnd_Max_Rates.setInitialValue(125);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp590() throws Exception
    {
        super("Adsp590");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //*  NEW EXTERNALIZATION ROUTINE
                                                                                                                                                                          //Natural: PERFORM CALL-EXT-MODULE
        sub_Call_Ext_Module();
        if (condition(Global.isEscape())) {return;}
        ldaAdslcntl.getVw_ads_Cntl_View().startDatabaseRead                                                                                                               //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "READ01",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        READ01:
        while (condition(ldaAdslcntl.getVw_ads_Cntl_View().readNextRow("READ01")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Strt_Dte.setValue(ldaAdslcntl.getAds_Cntl_View_Ads_Fnl_Prm_Cntrl_Dte());                                                                                      //Natural: MOVE ADS-FNL-PRM-CNTRL-DTE TO #STRT-DTE
        pnd_Strt_Dte.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #STRT-DTE
        ldaAdsl401.getVw_ads_Prtcpnt_View().startDatabaseRead                                                                                                             //Natural: READ ADS-PRTCPNT-VIEW BY ADP-LST-ACTVTY-DTE STARTING FROM #STRT-DTE
        (
        "READ02",
        new Wc[] { new Wc("ADP_LST_ACTVTY_DTE", ">=", pnd_Strt_Dte, WcType.BY) },
        new Oc[] { new Oc("ADP_LST_ACTVTY_DTE", "ASC") }
        );
        READ02:
        while (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().readNextRow("READ02")))
        {
            if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte().greater(ldaAdslcntl.getAds_Cntl_View_Ads_Cntl_Bsnss_Dte())))                                //Natural: IF ADP-LST-ACTVTY-DTE GT ADS-CNTL-BSNSS-DTE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde().equals("R") && ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde_1().equals("T"))))               //Natural: ACCEPT IF ADP-ANNT-TYP-CDE = 'R' AND ADP-STTS-CDE-1 = 'T'
            {
                continue;
            }
            pnd_Storage.resetInitial();                                                                                                                                   //Natural: RESET INITIAL #STORAGE
            pnd_Storage_Pnd_Ia_Tiaa_Contract.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Ia_Tiaa_Nbr());                                                                  //Natural: MOVE ADP-IA-TIAA-NBR TO #IA-TIAA-CONTRACT
            pnd_Storage_Pnd_Ia_Cref_Contract.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Ia_Cref_Nbr());                                                                  //Natural: MOVE ADP-IA-CREF-NBR TO #IA-CREF-CONTRACT
            pnd_Date_A.setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte(),new ReportEditMask("MMDDYYYY"));                                                    //Natural: MOVE EDITED ADP-EFFCTV-DTE ( EM = MMDDYYYY ) TO #DATE-A
            pnd_Storage_Pnd_Effctv_Dte.setValue(pnd_Date_A_Pnd_Date_Mmddyyyy);                                                                                            //Natural: MOVE #DATE-MMDDYYYY TO #EFFCTV-DTE
            pnd_Date_A.setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Strt_Dte(),new ReportEditMask("MMDDYYYY"));                                                //Natural: MOVE EDITED ADP-ANNTY-STRT-DTE ( EM = MMDDYYYY ) TO #DATE-A
            pnd_Storage_Pnd_Issue_Dte.setValue(pnd_Date_A_Pnd_Date_Mmddyyyy);                                                                                             //Natural: MOVE #DATE-MMDDYYYY TO #ISSUE-DTE
                                                                                                                                                                          //Natural: PERFORM GET-CNTRCT-RESULT-DATA
            sub_Get_Cntrct_Result_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Storage_Pnd_Found_Nad.getBoolean()))                                                                                                        //Natural: IF #FOUND-NAD
            {
                                                                                                                                                                          //Natural: PERFORM GET-IA-RESULT-DATA
                sub_Get_Ia_Result_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  EM - 101413
                if (condition(pnd_Storage_Pnd_Found_Nai.getBoolean() && pnd_Storage_Pnd_Found_Ia_Rslt_Fund.getBoolean()))                                                 //Natural: IF #FOUND-NAI AND #FOUND-IA-RSLT-FUND
                {
                                                                                                                                                                          //Natural: PERFORM SET-UP-POST-CALLS
                    sub_Set_Up_Post_Calls();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CNTRCT-RESULT-DATA
        //* ****************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-CNTRCT-RSLT-DATA
        //* **************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-IA-RESULT-DATA
        //* **************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-TIAA-IA-RSLT-DATA
        //* *FOR #Y 1 TO 60
        //*  ADDED ADS-IA-RSLT-VIEW FOR UNIQUENESS -
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-CREF-REA-IA-RSLT-DATA
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-UP-POST-CALLS
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TIAA-CT-WRITE
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TIAA-AC-WRITE
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TIAA-IN-WRITE
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TIAA-MS-WRITE
        //*  "EST ADD INCOME SHOULD BE ADDED TO NXT PYT - STTMNT WILL FOLLOW"
        //*  "NEXT PYT SCHEDULED FOR ..."
        //*  "TIAA DIVIDENDS EFFECTIVE..." APR 1 FOR ANNTY CERTAIN, ELSE JAN 1
        //*  "THIS IS AN IMPORTANT DOCUMENT"
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FREQUENCY
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NXT-PYT-DTE
        //* ******************************
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-NEXT-QRTRLY-PYT
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-NEXT-SEMIANN-PYT
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREF-CT-WRITE
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREF-AC-WRITE
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREF-ANNL-IN-WRITE
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREF-MNTHLY-IN-WRITE
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREF-REA-MS-WRITE
        //*  "EST ADD INCOME SHOULD BE ADDED TO NXT PYT - STTMNT WILL FOLLOW"
        //*  "NEXT PYT SCHEDULED FOR ..."
        //*  UNIT VALUE EFFECTIVE...
        //*  "THIS IS AN IMPORTANT DOCUMENT"
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REA-CT-WRITE
        //*  FILL IN SORT-KEY-STRUCT FIELDS HERE
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REA-AC-WRITE
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REA-ANNL-IN-WRITE
        //*  ADDED ADS-IA-RSLT-VIEW FOR UNIQUENESS -
        //*        IN-FUND-NAME (#Y)       := 'TIAA REAL ESTATE'
        //*        IN-FUND-NAME (#Y)       := 'ACCESS LIFECYCLE RET. INCOME'
        //*  IN-DATA-OCCURS          := 1
        //*  IN-FUND-NAME (1)        := 'TIAA REAL ESTATE'
        //*  IN-UNITS (1)            := #REA-ANNL-UNITS
        //*  IN-UNIT-VALUE (1)       := #REA-ANNL-UNIT-VAL
        //*  IN-ESTIMATED-INCOME (1) := #REA-ANNL-ADD-INC
        //*  IN-CHANGE-METHOD (1)    := 'A'
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REA-MNTHLY-IN-WRITE
        //*  ADDED ADS-IA-RSLT-VIEW FOR UNIQUENESS -
        //*        IN-FUND-NAME (#Y)       := 'TIAA REAL ESTATE'
        //*        IN-FUND-NAME (#Y)       := 'ACCESS LIFECYCLE RET. INCOME'
        //*  IN-DATA-OCCURS          := 2
        //*  IN-FUND-NAME (1)        := 'TIAA REAL ESTATE'
        //*  IN-UNITS (1)            := #REA-MNTHLY-UNITS
        //*  IN-UNIT-VALUE (1)       := #REA-MNTHLY-UNIT-VAL
        //*  IN-ESTIMATED-INCOME (1) := #REA-MNTHLY-ADD-INC
        //*  IN-CHANGE-METHOD (1)    := 'M'
        //*  IN-FUND-NAME (2)        := ' '
        //*  IN-CHANGE-METHOD (2)    := 'M'
        //*  IN-ESTIMATED-INCOME (2) := #TOT-REA-ADD-INC
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST-FOR-OPEN
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-POST-FOR-CLOSE
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-EXT-MODULE
    }
    private void sub_Get_Cntrct_Result_Data() throws Exception                                                                                                            //Natural: GET-CNTRCT-RESULT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        pnd_X.setValue(1);                                                                                                                                                //Natural: ASSIGN #X := 1
        pnd_Storage_Pnd_Tot_Tiaa_Add_Accum.setValue(0);                                                                                                                   //Natural: MOVE 0 TO #TOT-TIAA-ADD-ACCUM #TOT-REA-ADD-ACCUM #TOT-CREF-ADD-ACCUM
        pnd_Storage_Pnd_Tot_Rea_Add_Accum.setValue(0);
        pnd_Storage_Pnd_Tot_Cref_Add_Accum.setValue(0);
        ldaAdsl401a.getVw_ads_Cntrct_View().startDatabaseFind                                                                                                             //Natural: FIND ADS-CNTRCT-VIEW WITH RQST-ID = ADS-PRTCPNT-VIEW.RQST-ID
        (
        "FIND01",
        new Wc[] { new Wc("RQST_ID", "=", ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id(), WcType.WITH) }
        );
        FIND01:
        while (condition(ldaAdsl401a.getVw_ads_Cntrct_View().readNextRow("FIND01", true)))
        {
            ldaAdsl401a.getVw_ads_Cntrct_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAdsl401a.getVw_ads_Cntrct_View().getAstCOUNTER().equals(0)))                                                                                 //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "*******************************************");                                                                                     //Natural: WRITE '*******************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "NO DA RESULT RECORD TO MATCH PRTCPNT RECORD");                                                                                     //Natural: WRITE 'NO DA RESULT RECORD TO MATCH PRTCPNT RECORD'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "FOR RQST-ID ",ldaAdsl401a.getAds_Cntrct_View_Rqst_Id());                                                                           //Natural: WRITE 'FOR RQST-ID ' ADS-CNTRCT-VIEW.RQST-ID
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "*******************************************");                                                                                     //Natural: WRITE '*******************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Storage_Pnd_Found_Nad.setValue(false);                                                                                                                //Natural: MOVE FALSE TO #FOUND-NAD
            }                                                                                                                                                             //Natural: END-NOREC
                                                                                                                                                                          //Natural: PERFORM MOVE-CNTRCT-RSLT-DATA
            sub_Move_Cntrct_Rslt_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_X.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #X
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Move_Cntrct_Rslt_Data() throws Exception                                                                                                             //Natural: MOVE-CNTRCT-RSLT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //*  THERE MAY BE UP TO SIX DA RESULT RECORDS - ONE FOR EACH CONTRACT
        //*  FOR EACH CONTRACT THERE MAY BE TIAA, REA OR CREF ADD'L INCOME
        //* ******************************************************************
        pnd_Storage_Pnd_Da_Tiaa_Nbr.getValue(pnd_X).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tiaa_Nbr());                                                              //Natural: MOVE ADC-TIAA-NBR TO #DA-TIAA-NBR ( #X )
        pnd_Storage_Pnd_Da_Cref_Nbr.getValue(pnd_X).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Cref_Nbr());                                                              //Natural: MOVE ADC-CREF-NBR TO #DA-CREF-NBR ( #X )
        pnd_Storage_Pnd_Contract_Cnt.nadd(1);                                                                                                                             //Natural: ADD 1 TO #CONTRACT-CNT
        FOR01:                                                                                                                                                            //Natural: FOR #Y 1 TO 20
        for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(20)); pnd_Y.nadd(1))
        {
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_Y).equals(" ")))                                                                     //Natural: IF ADC-ACCT-CDE ( #Y ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Storage_Pnd_Nad_Acct_Stts_Cde.setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Stts_Cde().getValue(pnd_Y));                                           //Natural: MOVE ADC-ACCT-STTS-CDE ( #Y ) TO #NAD-ACCT-STTS-CDE
                //*  COMPLETED STATUS
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Stts_Cde_1().equals("T")))                                                                               //Natural: IF ADC-STTS-CDE-1 = 'T'
                {
                    //*  TIAA/STABLE /* EM - 012709
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_Y).equals("T") || ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_Y).equals("Y"))) //Natural: IF ADC-ACCT-CDE ( #Y ) = 'T' OR = 'Y'
                    {
                        pnd_Storage_Pnd_Tiaa_Addit_Accum.getValue(pnd_X).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_Y));                    //Natural: MOVE ADC-ACCT-ACTL-AMT ( #Y ) TO #TIAA-ADDIT-ACCUM ( #X )
                        pnd_Storage_Pnd_Tot_Tiaa_Add_Accum.nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_Y));                                      //Natural: ADD ADC-ACCT-ACTL-AMT ( #Y ) TO #TOT-TIAA-ADD-ACCUM
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  REA/ACC   /* EM - 012709
                        if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_Y).equals("R") || ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_Y).equals("D"))) //Natural: IF ADC-ACCT-CDE ( #Y ) = 'R' OR = 'D'
                        {
                            pnd_Storage_Pnd_Rea_Addit_Accum.getValue(pnd_X).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_Y));                     //Natural: ADD ADC-ACCT-ACTL-AMT ( #Y ) TO #REA-ADDIT-ACCUM ( #X )
                            pnd_Storage_Pnd_Tot_Rea_Add_Accum.nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_Y));                                   //Natural: ADD ADC-ACCT-ACTL-AMT ( #Y ) TO #TOT-REA-ADD-ACCUM
                            //*  CREF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Storage_Pnd_Cref_Addit_Accum.getValue(pnd_X).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_Y));                    //Natural: ADD ADC-ACCT-ACTL-AMT ( #Y ) TO #CREF-ADDIT-ACCUM ( #X )
                            pnd_Storage_Pnd_Tot_Cref_Add_Accum.nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_Y));                                  //Natural: ADD ADC-ACCT-ACTL-AMT ( #Y ) TO #TOT-CREF-ADD-ACCUM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Get_Ia_Result_Data() throws Exception                                                                                                                //Natural: GET-IA-RESULT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //*  LOOK FIRST FOR 19-DAY REPLICATION RECORD, THEN FOR 70-DAY
        //*  THERE WILL ONLY BE ONE OR THE OTHER
        //* **************************************************************
        pnd_Storage_Pnd_Found_Nai.setValue(false);                                                                                                                        //Natural: MOVE FALSE TO #FOUND-NAI
        pnd_Adi_Sd1_Pnd_Adi_Sd1_Rqst_Id.setValue(ldaAdsl401a.getAds_Cntrct_View_Rqst_Id());                                                                               //Natural: ASSIGN #ADI-SD1-RQST-ID := ADS-CNTRCT-VIEW.RQST-ID
        pnd_Adi_Sd1_Pnd_Adi_Sd1_Ia_Rcrd_Cde.setValue("RS ");                                                                                                              //Natural: ASSIGN #ADI-SD1-IA-RCRD-CDE := 'RS '
        if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Rsttlmnt_Cnt().equals("1")))                                                                                     //Natural: IF ADP-RSTTLMNT-CNT = '1'
        {
            pnd_Adi_Sd1_Pnd_Adi_Sd1_Sqnce_Nbr.setValue(12);                                                                                                               //Natural: ASSIGN #ADI-SD1-SQNCE-NBR := 12
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Adi_Sd1_Pnd_Adi_Sd1_Sqnce_Nbr.setValue(22);                                                                                                               //Natural: ASSIGN #ADI-SD1-SQNCE-NBR := 22
        }                                                                                                                                                                 //Natural: END-IF
        //*  READ ADS-IA-RSLT-VIEW BY ADI-SUPER1 STARTING FROM #ADI-SD1
        //*  IF ADS-IA-RSLT-VIEW.RQST-ID NE ADS-CNTRCT-VIEW.RQST-ID
        //*    ESCAPE BOTTOM
        //*  END-IF
        //*  ACCEPT IF ADI-IA-RCRD-CDE = 'RS ' AND
        //*    ADI-SQNCE-NBR = 12 OR = 22
        //*  MOVE TRUE TO #FOUND-NAI
        //*  ESCAPE BOTTOM
        //*  END-READ
        ldaAdsl450.getVw_ads_Ia_Rslt_View().startDatabaseFind                                                                                                             //Natural: FIND ADS-IA-RSLT-VIEW WITH ADI-SUPER1 = #ADI-SD1
        (
        "FIND02",
        new Wc[] { new Wc("ADI_SUPER1", "=", pnd_Adi_Sd1, WcType.WITH) }
        );
        FIND02:
        while (condition(ldaAdsl450.getVw_ads_Ia_Rslt_View().readNextRow("FIND02", true)))
        {
            ldaAdsl450.getVw_ads_Ia_Rslt_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAdsl450.getVw_ads_Ia_Rslt_View().getAstCOUNTER().equals(0)))                                                                                 //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "*******************************************");                                                                                     //Natural: WRITE '*******************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "NO IA RESULT RECORD TO MATCH PRTCPNT RECORD");                                                                                     //Natural: WRITE 'NO IA RESULT RECORD TO MATCH PRTCPNT RECORD'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "FOR RQST-ID ",ldaAdsl401a.getAds_Cntrct_View_Rqst_Id());                                                                           //Natural: WRITE 'FOR RQST-ID ' ADS-CNTRCT-VIEW.RQST-ID
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "*******************************************");                                                                                     //Natural: WRITE '*******************************************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Storage_Pnd_Found_Nai.setValue(true);                                                                                                                     //Natural: MOVE TRUE TO #FOUND-NAI
            //*  IF #FOUND-NAI
                                                                                                                                                                          //Natural: PERFORM MOVE-TIAA-IA-RSLT-DATA
            sub_Move_Tiaa_Ia_Rslt_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM MOVE-CREF-REA-IA-RSLT-DATA
            sub_Move_Cref_Rea_Ia_Rslt_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  ELSE
        //*   WRITE '*******************************************'
        //*   WRITE 'NO IA RESULT RECORD TO MATCH PRTCPNT RECORD'
        //*   WRITE 'FOR RQST-ID ' ADS-CNTRCT-VIEW.RQST-ID
        //*   WRITE '*******************************************'
        //*  END-IF
    }
    private void sub_Move_Tiaa_Ia_Rslt_Data() throws Exception                                                                                                            //Natural: MOVE-TIAA-IA-RSLT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************************************
        //*  102303
        pnd_Storage_Pnd_Tiaa_Grd_Guar_Inc.setValue(0);                                                                                                                    //Natural: MOVE 0 TO #TIAA-GRD-GUAR-INC #TIAA-GRD-EST-INC #TIAA-STD-GUAR-INC #TIAA-STD-EST-INC
        pnd_Storage_Pnd_Tiaa_Grd_Est_Inc.setValue(0);
        pnd_Storage_Pnd_Tiaa_Std_Guar_Inc.setValue(0);
        pnd_Storage_Pnd_Tiaa_Std_Est_Inc.setValue(0);
        FOR02:                                                                                                                                                            //Natural: FOR #Y 1 TO #MAX-RATES
        for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(pnd_Max_Rates)); pnd_Y.nadd(1))
        {
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(pnd_Y).equals("  ")))                                                         //Natural: IF ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-RATE-CD ( #Y ) = '  '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Storage_Pnd_Tiaa_Grd_Guar_Inc.nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Y));                                      //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #Y ) TO #TIAA-GRD-GUAR-INC
                pnd_Storage_Pnd_Tiaa_Grd_Est_Inc.compute(new ComputeParameters(false, pnd_Storage_Pnd_Tiaa_Grd_Est_Inc), pnd_Storage_Pnd_Tiaa_Grd_Est_Inc.add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Y)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Y))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #Y ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( #Y ) TO #TIAA-GRD-EST-INC
                pnd_Storage_Pnd_Tiaa_Std_Guar_Inc.nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Y));                                      //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #Y ) TO #TIAA-STD-GUAR-INC
                pnd_Storage_Pnd_Tiaa_Std_Est_Inc.compute(new ComputeParameters(false, pnd_Storage_Pnd_Tiaa_Std_Est_Inc), pnd_Storage_Pnd_Tiaa_Std_Est_Inc.add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Y)).add(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Y))); //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( #Y ) ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( #Y ) TO #TIAA-STD-EST-INC
            }                                                                                                                                                             //Natural: END-IF
            //*  EM - 030112 END
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  READ THE SECOND IA RSLT RECORD IF EXISTS /* EM - 030112 START
        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Count_Castadi_Dtl_Tiaa_Data().equals(pnd_Max_Rates)))                                                                //Natural: IF ADS-IA-RSLT-VIEW.C*ADI-DTL-TIAA-DATA = #MAX-RATES
        {
            pnd_Adi_Sd1_Pnd_Adi_Sd1_Ia_Rcrd_Cde.setValue("RS1");                                                                                                          //Natural: ASSIGN #ADI-SD1-IA-RCRD-CDE := 'RS1'
            vw_ads_Ia_Rslt2.startDatabaseFind                                                                                                                             //Natural: FIND ADS-IA-RSLT2 WITH ADI-SUPER1 = #ADI-SD1
            (
            "FIND03",
            new Wc[] { new Wc("ADI_SUPER1", "=", pnd_Adi_Sd1, WcType.WITH) }
            );
            FIND03:
            while (condition(vw_ads_Ia_Rslt2.readNextRow("FIND03")))
            {
                vw_ads_Ia_Rslt2.setIfNotFoundControlFlag(false);
                FOR03:                                                                                                                                                    //Natural: FOR #Y = 1 TO ADS-IA-RSLT2.C*ADI-DTL-TIAA-DATA
                for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data)); pnd_Y.nadd(1))
                {
                    pnd_Storage_Pnd_Tiaa_Grd_Guar_Inc.nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Y));                                                      //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #Y ) TO #TIAA-GRD-GUAR-INC
                    pnd_Storage_Pnd_Tiaa_Grd_Est_Inc.compute(new ComputeParameters(false, pnd_Storage_Pnd_Tiaa_Grd_Est_Inc), pnd_Storage_Pnd_Tiaa_Grd_Est_Inc.add(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Y)).add(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_Y))); //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #Y ) ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #Y ) TO #TIAA-GRD-EST-INC
                    pnd_Storage_Pnd_Tiaa_Std_Guar_Inc.nadd(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Y));                                                      //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #Y ) TO #TIAA-STD-GUAR-INC
                    pnd_Storage_Pnd_Tiaa_Std_Est_Inc.compute(new ComputeParameters(false, pnd_Storage_Pnd_Tiaa_Std_Est_Inc), pnd_Storage_Pnd_Tiaa_Std_Est_Inc.add(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Y)).add(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Y))); //Natural: ADD ADS-IA-RSLT2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #Y ) ADS-IA-RSLT2.ADI-DTL-TIAA-STD-DVDND-AMT ( #Y ) TO #TIAA-STD-EST-INC
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
            //*  EM - 030112 END
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Storage_Pnd_Tot_Tiaa_Guar_Inc.compute(new ComputeParameters(false, pnd_Storage_Pnd_Tot_Tiaa_Guar_Inc), pnd_Storage_Pnd_Tiaa_Grd_Guar_Inc.add(pnd_Storage_Pnd_Tiaa_Std_Guar_Inc)); //Natural: ADD #TIAA-GRD-GUAR-INC #TIAA-STD-GUAR-INC GIVING #TOT-TIAA-GUAR-INC
        pnd_Storage_Pnd_Tot_Tiaa_Est_Inc.compute(new ComputeParameters(false, pnd_Storage_Pnd_Tot_Tiaa_Est_Inc), pnd_Storage_Pnd_Tiaa_Grd_Est_Inc.add(pnd_Storage_Pnd_Tiaa_Std_Est_Inc)); //Natural: ADD #TIAA-GRD-EST-INC #TIAA-STD-EST-INC GIVING #TOT-TIAA-EST-INC
    }
    private void sub_Move_Cref_Rea_Ia_Rslt_Data() throws Exception                                                                                                        //Natural: MOVE-CREF-REA-IA-RSLT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        //*  ADDED ADS-IA-RSLT-VIEW FOR UNIQUENESS - /* EM - 030112 START
        //*  EM - 101413
        pnd_Storage_Pnd_Found_Ia_Rslt_Fund.setValue(true);                                                                                                                //Natural: MOVE TRUE TO #FOUND-IA-RSLT-FUND
        pnd_X.setValue(1);                                                                                                                                                //Natural: ASSIGN #X := 1
        FOR04:                                                                                                                                                            //Natural: FOR #Y 1 TO 20
        for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(20)); pnd_Y.nadd(1))
        {
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Y).equals(" ")))                                                             //Natural: IF ADS-IA-RSLT-VIEW.ADI-DTL-CREF-ACCT-CD ( #Y ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Y).equals("R")))                                                         //Natural: IF ADS-IA-RSLT-VIEW.ADI-DTL-CREF-ACCT-CD ( #Y ) = 'R'
                {
                    pnd_Storage_Pnd_Rea_Annl_Units.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Y));                                //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-CREF-ANNL-NBR-UNITS ( #Y ) TO #REA-ANNL-UNITS
                    pnd_Storage_Pnd_Rea_Annl_Unit_Val.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Annl_Unit_Val().getValue(pnd_Y));                           //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #Y ) TO #REA-ANNL-UNIT-VAL
                    pnd_Storage_Pnd_Rea_Annl_Add_Inc.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_Y));                                 //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-CREF-DA-ANNL-AMT ( #Y ) TO #REA-ANNL-ADD-INC
                    pnd_Storage_Pnd_Tot_Rea_Add_Inc.nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_Y));                                      //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-CREF-DA-ANNL-AMT ( #Y ) TO #TOT-REA-ADD-INC
                    pnd_Storage_Pnd_Rea_Mnthly_Units.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Y));                            //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #Y ) TO #REA-MNTHLY-UNITS
                    pnd_Storage_Pnd_Rea_Mnthly_Unit_Val.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val().getValue(pnd_Y));                       //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL ( #Y ) TO #REA-MNTHLY-UNIT-VAL
                    pnd_Storage_Pnd_Rea_Mnthly_Add_Inc.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_Y));                             //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-CREF-DA-MNTHLY-AMT ( #Y ) TO #REA-MNTHLY-ADD-INC
                    pnd_Storage_Pnd_Tot_Rea_Add_Inc.nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_Y));                                    //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-CREF-DA-MNTHLY-AMT ( #Y ) TO #TOT-REA-ADD-INC
                    //*  EM - 012709 START
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Y).equals("D")))                                                     //Natural: IF ADS-IA-RSLT-VIEW.ADI-DTL-CREF-ACCT-CD ( #Y ) = 'D'
                    {
                        pnd_Storage_Pnd_Acc_Annl_Units.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Y));                            //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-CREF-ANNL-NBR-UNITS ( #Y ) TO #ACC-ANNL-UNITS
                        pnd_Storage_Pnd_Acc_Annl_Unit_Val.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Annl_Unit_Val().getValue(pnd_Y));                       //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #Y ) TO #ACC-ANNL-UNIT-VAL
                        pnd_Storage_Pnd_Acc_Annl_Add_Inc.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_Y));                             //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-CREF-DA-ANNL-AMT ( #Y ) TO #ACC-ANNL-ADD-INC
                        pnd_Storage_Pnd_Tot_Acc_Add_Inc.nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_Y));                                  //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-CREF-DA-ANNL-AMT ( #Y ) TO #TOT-ACC-ADD-INC
                        pnd_Storage_Pnd_Acc_Mnthly_Units.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Y));                        //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #Y ) TO #ACC-MNTHLY-UNITS
                        pnd_Storage_Pnd_Acc_Mnthly_Unit_Val.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val().getValue(pnd_Y));                   //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL ( #Y ) TO #ACC-MNTHLY-UNIT-VAL
                        pnd_Storage_Pnd_Acc_Mnthly_Add_Inc.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_Y));                         //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-CREF-DA-MNTHLY-AMT ( #Y ) TO #ACC-MNTHLY-ADD-INC
                        pnd_Storage_Pnd_Tot_Acc_Add_Inc.nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_Y));                                //Natural: ADD ADS-IA-RSLT-VIEW.ADI-DTL-CREF-DA-MNTHLY-AMT ( #Y ) TO #TOT-ACC-ADD-INC
                        //*  EM - 012709 END
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Y).notEquals(getZero()) || ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Y).notEquals(getZero()))) //Natural: IF ADS-IA-RSLT-VIEW.ADI-DTL-CREF-ANNL-NBR-UNITS ( #Y ) NE 0 OR ADS-IA-RSLT-VIEW.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #Y ) NE 0
                        {
                            pnd_Storage_Pnd_Cref_Inc_Acct_Cde.getValue(pnd_X).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Y));            //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-CREF-ACCT-CD ( #Y ) TO #CREF-INC-ACCT-CDE ( #X )
                            //*  EM - 101413 START
                            pnd_Indx.reset();                                                                                                                             //Natural: RESET #INDX
                            DbsUtil.examine(new ExamineSource(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue("*")), new ExamineSearch(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Y)),  //Natural: EXAMINE ADS-CNTRCT-VIEW.ADC-ACCT-CDE ( * ) FOR ADS-IA-RSLT-VIEW.ADI-DTL-CREF-ACCT-CD ( #Y ) GIVING INDEX #INDX
                                new ExamineGivingIndex(pnd_Indx));
                            if (condition(pnd_Indx.greater(getZero())))                                                                                                   //Natural: IF #INDX > 0
                            {
                                pnd_Storage_Pnd_Cref_Inc_Tkr_Symbl.getValue(pnd_X).setValue(ldaAdsl401a.getAds_Cntrct_View_Adc_Tkr_Symbl().getValue(pnd_Indx));           //Natural: MOVE ADS-CNTRCT-VIEW.ADC-TKR-SYMBL ( #INDX ) TO #CREF-INC-TKR-SYMBL ( #X )
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                getReports().write(0, "*******************************************");                                                                     //Natural: WRITE '*******************************************'
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                getReports().write(0, "IA RESULT RECORD ACCT CDE ",ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_Y));                //Natural: WRITE 'IA RESULT RECORD ACCT CDE ' ADI-DTL-CREF-ACCT-CD ( #Y )
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                getReports().write(0, "DOES NOT EXIST ON CONTRACT RECORD ACCT CDE TABLE");                                                                //Natural: WRITE 'DOES NOT EXIST ON CONTRACT RECORD ACCT CDE TABLE'
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                getReports().write(0, "FOR RQST-ID ",ldaAdsl401a.getAds_Cntrct_View_Rqst_Id());                                                           //Natural: WRITE 'FOR RQST-ID ' ADS-CNTRCT-VIEW.RQST-ID
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                getReports().write(0, "*******************************************");                                                                     //Natural: WRITE '*******************************************'
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                pnd_Storage_Pnd_Found_Ia_Rslt_Fund.setValue(false);                                                                                       //Natural: MOVE FALSE TO #FOUND-IA-RSLT-FUND
                                if (true) return;                                                                                                                         //Natural: ESCAPE ROUTINE
                                //*  EM - 101413 END
                            }                                                                                                                                             //Natural: END-IF
                            pnd_Storage_Pnd_Cref_Inc_Annl_Units.getValue(pnd_X).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_Y));   //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-CREF-ANNL-NBR-UNITS ( #Y ) TO #CREF-INC-ANNL-UNITS ( #X )
                            pnd_Storage_Pnd_Cref_Inc_Annl_Unit_Val.getValue(pnd_X).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Annl_Unit_Val().getValue(pnd_Y)); //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #Y ) TO #CREF-INC-ANNL-UNIT-VAL ( #X )
                            pnd_Storage_Pnd_Cref_Inc_Annl_Est_Inc.getValue(pnd_X).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_Y));    //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-CREF-DA-ANNL-AMT ( #Y ) TO #CREF-INC-ANNL-EST-INC ( #X )
                            pnd_Storage_Pnd_Tot_Cref_Add_Inc.nadd(pnd_Storage_Pnd_Cref_Inc_Annl_Est_Inc.getValue(pnd_X));                                                 //Natural: ADD #CREF-INC-ANNL-EST-INC ( #X ) TO #TOT-CREF-ADD-INC
                            pnd_Storage_Pnd_Cref_Inc_Mnthly_Units.getValue(pnd_X).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_Y)); //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #Y ) TO #CREF-INC-MNTHLY-UNITS ( #X )
                            pnd_Storage_Pnd_Cref_Inc_Mnthly_Unit_Val.getValue(pnd_X).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val().getValue(pnd_Y)); //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL ( #Y ) TO #CREF-INC-MNTHLY-UNIT-VAL ( #X )
                            pnd_Storage_Pnd_Cref_Inc_Mnthly_Est_Inc.getValue(pnd_X).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_Y)); //Natural: MOVE ADS-IA-RSLT-VIEW.ADI-DTL-CREF-DA-MNTHLY-AMT ( #Y ) TO #CREF-INC-MNTHLY-EST-INC ( #X )
                            pnd_Storage_Pnd_Tot_Cref_Add_Inc.nadd(pnd_Storage_Pnd_Cref_Inc_Mnthly_Est_Inc.getValue(pnd_X));                                               //Natural: ADD #CREF-INC-MNTHLY-EST-INC ( #X ) TO #TOT-CREF-ADD-INC
                            pnd_X.nadd(1);                                                                                                                                //Natural: ADD 1 TO #X
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  EM - 030112 END
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Set_Up_Post_Calls() throws Exception                                                                                                                 //Natural: SET-UP-POST-CALLS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        pnd_Tiebreak.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #TIEBREAK
                                                                                                                                                                          //Natural: PERFORM CALL-POST-FOR-OPEN
        sub_Call_Post_For_Open();
        if (condition(Global.isEscape())) {return;}
        ldaPstl6075.getIn_Income_Struct().reset();                                                                                                                        //Natural: RESET IN-INCOME-STRUCT AC-ACCUMULATION-INFO-STRUCT MS-MESSAGES-STRUCT CT-CONTRACT-STRUCT
        ldaPstl6075.getAc_Accumulation_Info_Struct().reset();
        ldaPstl6075.getMs_Messages_Struct().reset();
        ldaPstl6075.getCt_Contract_Struct().reset();
        if (condition(pnd_Storage_Pnd_Tot_Tiaa_Add_Accum.greater(getZero())))                                                                                             //Natural: IF #TOT-TIAA-ADD-ACCUM GT 0
        {
                                                                                                                                                                          //Natural: PERFORM TIAA-CT-WRITE
            sub_Tiaa_Ct_Write();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM TIAA-AC-WRITE
            sub_Tiaa_Ac_Write();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM TIAA-IN-WRITE
            sub_Tiaa_In_Write();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM TIAA-MS-WRITE
            sub_Tiaa_Ms_Write();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Storage_Pnd_Tot_Cref_Add_Accum.greater(getZero())))                                                                                             //Natural: IF #TOT-CREF-ADD-ACCUM GT 0
        {
            ldaPstl6075.getIn_Income_Struct().reset();                                                                                                                    //Natural: RESET IN-INCOME-STRUCT AC-ACCUMULATION-INFO-STRUCT MS-MESSAGES-STRUCT CT-CONTRACT-STRUCT
            ldaPstl6075.getAc_Accumulation_Info_Struct().reset();
            ldaPstl6075.getMs_Messages_Struct().reset();
            ldaPstl6075.getCt_Contract_Struct().reset();
                                                                                                                                                                          //Natural: PERFORM CREF-CT-WRITE
            sub_Cref_Ct_Write();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREF-AC-WRITE
            sub_Cref_Ac_Write();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREF-ANNL-IN-WRITE
            sub_Cref_Annl_In_Write();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREF-MNTHLY-IN-WRITE
            sub_Cref_Mnthly_In_Write();
            if (condition(Global.isEscape())) {return;}
            pnd_Storage_Pnd_Cref_Rea_Ind.setValue("C");                                                                                                                   //Natural: ASSIGN #CREF-REA-IND := 'C'
                                                                                                                                                                          //Natural: PERFORM CREF-REA-MS-WRITE
            sub_Cref_Rea_Ms_Write();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Storage_Pnd_Tot_Rea_Add_Accum.greater(getZero())))                                                                                              //Natural: IF #TOT-REA-ADD-ACCUM GT 0
        {
            ldaPstl6075.getIn_Income_Struct().reset();                                                                                                                    //Natural: RESET IN-INCOME-STRUCT AC-ACCUMULATION-INFO-STRUCT MS-MESSAGES-STRUCT CT-CONTRACT-STRUCT
            ldaPstl6075.getAc_Accumulation_Info_Struct().reset();
            ldaPstl6075.getMs_Messages_Struct().reset();
            ldaPstl6075.getCt_Contract_Struct().reset();
                                                                                                                                                                          //Natural: PERFORM REA-CT-WRITE
            sub_Rea_Ct_Write();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM REA-AC-WRITE
            sub_Rea_Ac_Write();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM REA-ANNL-IN-WRITE
            sub_Rea_Annl_In_Write();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM REA-MNTHLY-IN-WRITE
            sub_Rea_Mnthly_In_Write();
            if (condition(Global.isEscape())) {return;}
            pnd_Storage_Pnd_Cref_Rea_Ind.setValue("R");                                                                                                                   //Natural: ASSIGN #CREF-REA-IND := 'R'
                                                                                                                                                                          //Natural: PERFORM CREF-REA-MS-WRITE
            sub_Cref_Rea_Ms_Write();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALL-POST-FOR-CLOSE
        sub_Call_Post_For_Close();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Tiaa_Ct_Write() throws Exception                                                                                                                     //Natural: TIAA-CT-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        ldaPstl6075.getCt_Contract_Struct().resetInitial();                                                                                                               //Natural: RESET INITIAL CT-CONTRACT-STRUCT
        ldaPstl6075.getSort_Key_Struct_Sort_Key_Data().getValue("*").moveAll("H'00'");                                                                                    //Natural: MOVE ALL H'00' TO SORT-KEY-DATA ( * )
        ldaPstl6075.getSort_Key_Struct_Key_L3_Lob().setValue("T");                                                                                                        //Natural: MOVE 'T' TO KEY-L3-LOB
        ldaPstl6075.getSort_Key_Struct_Key_L3_Tiebreak().setValue(pnd_Tiebreak);                                                                                          //Natural: MOVE #TIEBREAK TO KEY-L3-TIEBREAK
        ldaPstl6075.getCt_Contract_Struct_Ct_Contract_Number().setValue(pnd_Storage_Pnd_Ia_Tiaa_Contract);                                                                //Natural: ASSIGN CT-CONTRACT-NUMBER := #IA-TIAA-CONTRACT
        ldaPstl6075.getCt_Contract_Struct_Ct_Effective_Date().setValue(pnd_Storage_Pnd_Effctv_Dte);                                                                       //Natural: ASSIGN CT-EFFECTIVE-DATE := #EFFCTV-DTE
        ldaPstl6075.getCt_Contract_Struct_Ct_Issue_Date().setValue(pnd_Storage_Pnd_Issue_Dte);                                                                            //Natural: ASSIGN CT-ISSUE-DATE := #ISSUE-DTE
        ldaPstl6075.getCt_Contract_Struct_Ct_Data_Occurs().setValue(1);                                                                                                   //Natural: ASSIGN CT-DATA-OCCURS := 1
        ldaPstl6075.getCt_Contract_Struct_Ct_Line_Of_Business().setValue("T");                                                                                            //Natural: ASSIGN CT-LINE-OF-BUSINESS := 'T'
        //*  EM - 012709 START
        DbsUtil.examine(new ExamineSource(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue("*")), new ExamineSearch("Y"), new ExamineGivingIndex(pnd_Y));           //Natural: EXAMINE ADC-ACCT-CDE ( * ) FOR 'Y' GIVING INDEX #Y
        if (condition(pnd_Y.greater(getZero())))                                                                                                                          //Natural: IF #Y > 0
        {
            ldaPstl6075.getCt_Contract_Struct_Ct_Ind_1().setValue("Y");                                                                                                   //Natural: ASSIGN CT-IND-1 := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  EM - 101413 START
            //*  EXAMINE ADC-T395-FUND-ID (*)  FOR 'SA' GIVING INDEX #Y /* DEA
            DbsUtil.examine(new ExamineSource(ldaAdsl401a.getAds_Cntrct_View_Adc_Invstmnt_Grpng_Id().getValue("*")), new ExamineSearch("TSV0"), new ExamineGivingIndex(pnd_Y)); //Natural: EXAMINE ADC-INVSTMNT-GRPNG-ID ( * ) FOR 'TSV0' GIVING INDEX #Y
            //*  EM - 101413 END
            //*  DEA
            //*  DEA
            if (condition(pnd_Y.greater(getZero())))                                                                                                                      //Natural: IF #Y > 0
            {
                ldaPstl6075.getCt_Contract_Struct_Ct_Ind_1().setValue("V");                                                                                               //Natural: ASSIGN CT-IND-1 := 'V'
                //*  DEA
            }                                                                                                                                                             //Natural: END-IF
            //*  EM - 012709 END
        }                                                                                                                                                                 //Natural: END-IF
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT CT-CONTRACT-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6075.getSort_Key_Struct(), ldaPstl6075.getCt_Contract_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Tiaa_Ac_Write() throws Exception                                                                                                                     //Natural: TIAA-AC-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        ldaPstl6075.getAc_Accumulation_Info_Struct().resetInitial();                                                                                                      //Natural: RESET INITIAL AC-ACCUMULATION-INFO-STRUCT
        ldaPstl6075.getSort_Key_Struct_Sort_Key_Data().getValue("*").moveAll("H'00'");                                                                                    //Natural: MOVE ALL H'00' TO SORT-KEY-DATA ( * )
        ldaPstl6075.getSort_Key_Struct_Key_L3_Lob().setValue("T");                                                                                                        //Natural: MOVE 'T' TO KEY-L3-LOB
        ldaPstl6075.getSort_Key_Struct_Key_L3_Tiebreak().setValue(pnd_Tiebreak);                                                                                          //Natural: MOVE #TIEBREAK TO KEY-L3-TIEBREAK
        pnd_Y.setValue(1);                                                                                                                                                //Natural: ASSIGN #Y := 1
        FOR05:                                                                                                                                                            //Natural: FOR #X 1 TO #CONTRACT-CNT
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pnd_Storage_Pnd_Contract_Cnt)); pnd_X.nadd(1))
        {
            if (condition(pnd_Storage_Pnd_Tiaa_Addit_Accum.getValue(pnd_X).notEquals(getZero())))                                                                         //Natural: IF #TIAA-ADDIT-ACCUM ( #X ) NE 0
            {
                ldaPstl6075.getAc_Accumulation_Info_Struct_Ac_Contract_Number().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Da_Tiaa_Nbr.getValue(pnd_X));                    //Natural: ASSIGN AC-CONTRACT-NUMBER ( #Y ) := #DA-TIAA-NBR ( #X )
                ldaPstl6075.getAc_Accumulation_Info_Struct_Ac_Accumulation().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Tiaa_Addit_Accum.getValue(pnd_X));                  //Natural: ASSIGN AC-ACCUMULATION ( #Y ) := #TIAA-ADDIT-ACCUM ( #X )
                pnd_Y.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #Y
            }                                                                                                                                                             //Natural: END-IF
            //*  TOTALS BUCKET
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaPstl6075.getAc_Accumulation_Info_Struct_Ac_Contract_Number().getValue(pnd_Y).setValue(" ");                                                                    //Natural: ASSIGN AC-CONTRACT-NUMBER ( #Y ) := ' '
        ldaPstl6075.getAc_Accumulation_Info_Struct_Ac_Accumulation().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Tot_Tiaa_Add_Accum);                                        //Natural: ASSIGN AC-ACCUMULATION ( #Y ) := #TOT-TIAA-ADD-ACCUM
        ldaPstl6075.getAc_Accumulation_Info_Struct_Ac_Data_Occurs().setValue(pnd_Y);                                                                                      //Natural: ASSIGN AC-DATA-OCCURS := #Y
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT AC-ACCUMULATION-INFO-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6075.getSort_Key_Struct(), ldaPstl6075.getAc_Accumulation_Info_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Tiaa_In_Write() throws Exception                                                                                                                     //Natural: TIAA-IN-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        ldaPstl6075.getIn_Income_Struct().resetInitial();                                                                                                                 //Natural: RESET INITIAL IN-INCOME-STRUCT
        ldaPstl6075.getSort_Key_Struct_Sort_Key_Data().getValue("*").moveAll("H'00'");                                                                                    //Natural: MOVE ALL H'00' TO SORT-KEY-DATA ( * )
        ldaPstl6075.getSort_Key_Struct_Key_L3_Lob().setValue("T");                                                                                                        //Natural: MOVE 'T' TO KEY-L3-LOB
        //*  TOTALS BUCKET
        ldaPstl6075.getSort_Key_Struct_Key_L3_Tiebreak().setValue(pnd_Tiebreak);                                                                                          //Natural: MOVE #TIEBREAK TO KEY-L3-TIEBREAK
        ldaPstl6075.getIn_Income_Struct_In_Tiaa_Payment_Method().getValue(1).setValue("GRADED");                                                                          //Natural: ASSIGN IN-TIAA-PAYMENT-METHOD ( 1 ) := 'GRADED'
        ldaPstl6075.getIn_Income_Struct_In_Guaranteed_Income().getValue(1).setValue(pnd_Storage_Pnd_Tiaa_Grd_Guar_Inc);                                                   //Natural: ASSIGN IN-GUARANTEED-INCOME ( 1 ) := #TIAA-GRD-GUAR-INC
        ldaPstl6075.getIn_Income_Struct_In_Estimated_Income().getValue(1).setValue(pnd_Storage_Pnd_Tiaa_Grd_Est_Inc);                                                     //Natural: ASSIGN IN-ESTIMATED-INCOME ( 1 ) := #TIAA-GRD-EST-INC
        ldaPstl6075.getIn_Income_Struct_In_Tiaa_Payment_Method().getValue(2).setValue("STANDARD");                                                                        //Natural: ASSIGN IN-TIAA-PAYMENT-METHOD ( 2 ) := 'STANDARD'
        ldaPstl6075.getIn_Income_Struct_In_Guaranteed_Income().getValue(2).setValue(pnd_Storage_Pnd_Tiaa_Std_Guar_Inc);                                                   //Natural: ASSIGN IN-GUARANTEED-INCOME ( 2 ) := #TIAA-STD-GUAR-INC
        ldaPstl6075.getIn_Income_Struct_In_Estimated_Income().getValue(2).setValue(pnd_Storage_Pnd_Tiaa_Std_Est_Inc);                                                     //Natural: ASSIGN IN-ESTIMATED-INCOME ( 2 ) := #TIAA-STD-EST-INC
        ldaPstl6075.getIn_Income_Struct_In_Tiaa_Payment_Method().getValue(3).setValue(" ");                                                                               //Natural: ASSIGN IN-TIAA-PAYMENT-METHOD ( 3 ) := ' '
        ldaPstl6075.getIn_Income_Struct_In_Guaranteed_Income().getValue(3).setValue(pnd_Storage_Pnd_Tot_Tiaa_Guar_Inc);                                                   //Natural: ASSIGN IN-GUARANTEED-INCOME ( 3 ) := #TOT-TIAA-GUAR-INC
        ldaPstl6075.getIn_Income_Struct_In_Estimated_Income().getValue(3).setValue(pnd_Storage_Pnd_Tot_Tiaa_Est_Inc);                                                     //Natural: ASSIGN IN-ESTIMATED-INCOME ( 3 ) := #TOT-TIAA-EST-INC
        ldaPstl6075.getIn_Income_Struct_In_Data_Occurs().setValue(3);                                                                                                     //Natural: ASSIGN IN-DATA-OCCURS := 3
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT IN-INCOME-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6075.getSort_Key_Struct(), ldaPstl6075.getIn_Income_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Tiaa_Ms_Write() throws Exception                                                                                                                     //Natural: TIAA-MS-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        ldaPstl6075.getMs_Messages_Struct().resetInitial();                                                                                                               //Natural: RESET INITIAL MS-MESSAGES-STRUCT
        ldaPstl6075.getSort_Key_Struct_Sort_Key_Data().getValue("*").moveAll("H'00'");                                                                                    //Natural: MOVE ALL H'00' TO SORT-KEY-DATA ( * )
        ldaPstl6075.getSort_Key_Struct_Key_L3_Lob().setValue("T");                                                                                                        //Natural: MOVE 'T' TO KEY-L3-LOB
        ldaPstl6075.getSort_Key_Struct_Key_L3_Tiebreak().setValue(pnd_Tiebreak);                                                                                          //Natural: MOVE #TIEBREAK TO KEY-L3-TIEBREAK
        ldaPstl6075.getMs_Messages_Struct_Ms_Data_Occurs().setValue(4);                                                                                                   //Natural: ASSIGN MS-DATA-OCCURS := 4
        ldaPstl6075.getMs_Messages_Struct_Ms_Message_Id().getValue(1).setValue("1");                                                                                      //Natural: ASSIGN MS-MESSAGE-ID ( 1 ) := '1'
                                                                                                                                                                          //Natural: PERFORM GET-FREQUENCY
        sub_Get_Frequency();
        if (condition(Global.isEscape())) {return;}
        ldaPstl6075.getMs_Messages_Struct_Ms_Payment_Frequency().getValue(1).setValue(pnd_Storage_Pnd_Frequency);                                                         //Natural: ASSIGN MS-PAYMENT-FREQUENCY ( 1 ) := #FREQUENCY
        ldaPstl6075.getMs_Messages_Struct_Ms_Message_Id().getValue(2).setValue("2");                                                                                      //Natural: ASSIGN MS-MESSAGE-ID ( 2 ) := '2'
        pnd_Trn_Cntrct_Key_Pnd_Trn_Key_Ia_Ppcn.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Ia_Tiaa_Nbr());                                                                //Natural: ASSIGN #TRN-KEY-IA-PPCN := ADP-IA-TIAA-NBR
                                                                                                                                                                          //Natural: PERFORM GET-NXT-PYT-DTE
        sub_Get_Nxt_Pyt_Dte();
        if (condition(Global.isEscape())) {return;}
        ldaPstl6075.getMs_Messages_Struct_Ms_Message_Date().getValue(2).setValue(pnd_Storage_Pnd_Nxt_Pyt_Dte);                                                            //Natural: ASSIGN MS-MESSAGE-DATE ( 2 ) := #NXT-PYT-DTE
        ldaPstl6075.getMs_Messages_Struct_Ms_Message_Id().getValue(3).setValue("3");                                                                                      //Natural: ASSIGN MS-MESSAGE-ID ( 3 ) := '3'
        pnd_Date_A_Pnd_Date_Mmddyyyy.setValue(pnd_Storage_Pnd_Effctv_Dte);                                                                                                //Natural: MOVE #EFFCTV-DTE TO #DATE-MMDDYYYY
        pnd_Date_A_Pnd_Date_Yyyy.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #DATE-YYYY
        //*  EM 030112
        if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Optn_Cde().equals(21)))                                                                                          //Natural: IF ADS-IA-RSLT-VIEW.ADI-OPTN-CDE = 21
        {
            pnd_Date_A_Pnd_Date_Mm.setValue(4);                                                                                                                           //Natural: ASSIGN #DATE-MM := 4
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Date_A_Pnd_Date_Mm.setValue(1);                                                                                                                           //Natural: ASSIGN #DATE-MM := 1
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Date_A_Pnd_Date_Dd.setValue(1);                                                                                                                               //Natural: MOVE 1 TO #DATE-DD
        ldaPstl6075.getMs_Messages_Struct_Ms_Message_Date().getValue(3).setValue(pnd_Date_A_Pnd_Date_Mmddyyyy);                                                           //Natural: MOVE #DATE-MMDDYYYY TO MS-MESSAGE-DATE ( 3 )
        ldaPstl6075.getMs_Messages_Struct_Ms_Message_Id().getValue(4).setValue("5");                                                                                      //Natural: ASSIGN MS-MESSAGE-ID ( 4 ) := '5'
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT MS-MESSAGES-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6075.getSort_Key_Struct(), ldaPstl6075.getMs_Messages_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Frequency() throws Exception                                                                                                                     //Natural: GET-FREQUENCY
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        short decideConditionsMet1859 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF ADP-PYMNT-MODE-1;//Natural: VALUE 1
        if (condition((ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_1().equals(1))))
        {
            decideConditionsMet1859++;
            pnd_Storage_Pnd_Frequency.setValue("MONTHLY");                                                                                                                //Natural: ASSIGN #FREQUENCY := 'MONTHLY'
        }                                                                                                                                                                 //Natural: VALUE 6
        else if (condition((ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_1().equals(6))))
        {
            decideConditionsMet1859++;
            pnd_Storage_Pnd_Frequency.setValue("QUARTERLY");                                                                                                              //Natural: ASSIGN #FREQUENCY := 'QUARTERLY'
        }                                                                                                                                                                 //Natural: VALUE 7
        else if (condition((ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_1().equals(7))))
        {
            decideConditionsMet1859++;
            pnd_Storage_Pnd_Frequency.setValue("SEMIANNUAL");                                                                                                             //Natural: ASSIGN #FREQUENCY := 'SEMIANNUAL'
        }                                                                                                                                                                 //Natural: VALUE 8
        else if (condition((ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_1().equals(8))))
        {
            decideConditionsMet1859++;
            pnd_Storage_Pnd_Frequency.setValue("ANNUAL");                                                                                                                 //Natural: ASSIGN #FREQUENCY := 'ANNUAL'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Nxt_Pyt_Dte() throws Exception                                                                                                                   //Natural: GET-NXT-PYT-DTE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Trn_Cntrct_Key_Pnd_Trn_Key_Payee_Cde.setValue(1);                                                                                                             //Natural: ASSIGN #TRN-KEY-PAYEE-CDE := 01
        pnd_Trn_Cntrct_Key_Pnd_Trn_Key_Inv_Dte.setValue(0);                                                                                                               //Natural: ASSIGN #TRN-KEY-INV-DTE := 0
        pnd_Trn_Cntrct_Key_Pnd_Trn_Key_Trn_Cde.setValue(0);                                                                                                               //Natural: ASSIGN #TRN-KEY-TRN-CDE := 0
        vw_trn.startDatabaseRead                                                                                                                                          //Natural: READ ( 1 ) TRN BY TRANS-CNTRCT-KEY STARTING FROM #TRN-CNTRCT-KEY
        (
        "READ03",
        new Wc[] { new Wc("TRANS_CNTRCT_KEY", ">=", pnd_Trn_Cntrct_Key, WcType.BY) },
        new Oc[] { new Oc("TRANS_CNTRCT_KEY", "ASC") },
        1
        );
        READ03:
        while (condition(vw_trn.readNextRow("READ03")))
        {
            if (condition(trn_Trans_Ppcn_Nbr.notEquals(pnd_Trn_Cntrct_Key_Pnd_Trn_Key_Ia_Ppcn)))                                                                          //Natural: IF TRANS-PPCN-NBR NE #TRN-KEY-IA-PPCN
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(trn_Trans_Cde.equals(60))))                                                                                                                   //Natural: ACCEPT IF TRANS-CDE = 060
            {
                continue;
            }
            pnd_Storage_Pnd_Nxt_Pyt_Dd.setValue(trn_Pnd_Chk_Dte_Dd);                                                                                                      //Natural: ASSIGN #NXT-PYT-DD := #CHK-DTE-DD
            pnd_Storage_Pnd_Nxt_Pyt_Yyyy.setValue(trn_Pnd_Chk_Dte_Yyyy);                                                                                                  //Natural: ASSIGN #NXT-PYT-YYYY := #CHK-DTE-YYYY
            //*  MONTHLY
            short decideConditionsMet1884 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF ADP-PYMNT-MODE-1;//Natural: VALUE 1
            if (condition((ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_1().equals(1))))
            {
                decideConditionsMet1884++;
                pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(trn_Pnd_Chk_Dte_Mm);                                                                                                  //Natural: ASSIGN #NXT-PYT-MM := #CHK-DTE-MM
            }                                                                                                                                                             //Natural: VALUE 6
            else if (condition((ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_1().equals(6))))
            {
                decideConditionsMet1884++;
                                                                                                                                                                          //Natural: PERFORM FIND-NEXT-QRTRLY-PYT
                sub_Find_Next_Qrtrly_Pyt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 7
            else if (condition((ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_1().equals(7))))
            {
                decideConditionsMet1884++;
                                                                                                                                                                          //Natural: PERFORM FIND-NEXT-SEMIANN-PYT
                sub_Find_Next_Semiann_Pyt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 8
            else if (condition((ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_1().equals(8))))
            {
                decideConditionsMet1884++;
                pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_2());                                                                   //Natural: ASSIGN #NXT-PYT-MM := ADP-PYMNT-MODE-2
                if (condition(trn_Pnd_Chk_Dte_Mm.greater(ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_2())))                                                             //Natural: IF #CHK-DTE-MM GT ADP-PYMNT-MODE-2
                {
                    pnd_Storage_Pnd_Nxt_Pyt_Yyyy.nadd(1);                                                                                                                 //Natural: ADD 1 TO #NXT-PYT-YYYY
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Find_Next_Qrtrly_Pyt() throws Exception                                                                                                              //Natural: FIND-NEXT-QRTRLY-PYT
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        short decideConditionsMet1903 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF ADP-PYMNT-MODE-2;//Natural: VALUE 1
        if (condition((ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_2().equals(1))))
        {
            decideConditionsMet1903++;
            if (condition(trn_Pnd_Chk_Dte_Mm.less(4)))                                                                                                                    //Natural: IF #CHK-DTE-MM LT 4
            {
                pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(4);                                                                                                                   //Natural: ASSIGN #NXT-PYT-MM := 4
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(trn_Pnd_Chk_Dte_Mm.less(7)))                                                                                                                //Natural: IF #CHK-DTE-MM LT 7
                {
                    pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(7);                                                                                                               //Natural: ASSIGN #NXT-PYT-MM := 7
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(trn_Pnd_Chk_Dte_Mm.less(10)))                                                                                                           //Natural: IF #CHK-DTE-MM LT 10
                    {
                        pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(10);                                                                                                          //Natural: ASSIGN #NXT-PYT-MM := 10
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(1);                                                                                                           //Natural: ASSIGN #NXT-PYT-MM := 1
                        pnd_Storage_Pnd_Nxt_Pyt_Yyyy.nadd(1);                                                                                                             //Natural: ADD 1 TO #NXT-PYT-YYYY
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_2().equals(2))))
        {
            decideConditionsMet1903++;
            if (condition(trn_Pnd_Chk_Dte_Mm.less(2)))                                                                                                                    //Natural: IF #CHK-DTE-MM LT 2
            {
                pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(2);                                                                                                                   //Natural: ASSIGN #NXT-PYT-MM := 2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(trn_Pnd_Chk_Dte_Mm.less(5)))                                                                                                                //Natural: IF #CHK-DTE-MM LT 5
                {
                    pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(5);                                                                                                               //Natural: ASSIGN #NXT-PYT-MM := 5
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(trn_Pnd_Chk_Dte_Mm.less(8)))                                                                                                            //Natural: IF #CHK-DTE-MM LT 8
                    {
                        pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(8);                                                                                                           //Natural: ASSIGN #NXT-PYT-MM := 8
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(trn_Pnd_Chk_Dte_Mm.less(11)))                                                                                                       //Natural: IF #CHK-DTE-MM LT 11
                        {
                            pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(11);                                                                                                      //Natural: ASSIGN #NXT-PYT-MM := 11
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(2);                                                                                                       //Natural: ASSIGN #NXT-PYT-MM := 2
                            pnd_Storage_Pnd_Nxt_Pyt_Yyyy.nadd(1);                                                                                                         //Natural: ADD 1 TO #NXT-PYT-YYYY
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  EM - 052416
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_2().equals(3))))
        {
            decideConditionsMet1903++;
            if (condition(trn_Pnd_Chk_Dte_Mm.less(3)))                                                                                                                    //Natural: IF #CHK-DTE-MM LT 3
            {
                pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(3);                                                                                                                   //Natural: ASSIGN #NXT-PYT-MM := 3
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(trn_Pnd_Chk_Dte_Mm.less(6)))                                                                                                                //Natural: IF #CHK-DTE-MM LT 6
                {
                    pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(6);                                                                                                               //Natural: ASSIGN #NXT-PYT-MM := 6
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(trn_Pnd_Chk_Dte_Mm.less(9)))                                                                                                            //Natural: IF #CHK-DTE-MM LT 9
                    {
                        pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(9);                                                                                                           //Natural: ASSIGN #NXT-PYT-MM := 9
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(trn_Pnd_Chk_Dte_Mm.less(12)))                                                                                                       //Natural: IF #CHK-DTE-MM LT 12
                        {
                            pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(12);                                                                                                      //Natural: ASSIGN #NXT-PYT-MM := 12
                            //*  111418 - START
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(3);                                                                                                       //Natural: ASSIGN #NXT-PYT-MM := 3
                            //*  111418 - END
                            pnd_Storage_Pnd_Nxt_Pyt_Yyyy.nadd(1);                                                                                                         //Natural: ADD 1 TO #NXT-PYT-YYYY
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Find_Next_Semiann_Pyt() throws Exception                                                                                                             //Natural: FIND-NEXT-SEMIANN-PYT
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        short decideConditionsMet1966 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF ADP-PYMNT-MODE-2;//Natural: VALUE 1
        if (condition((ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_2().equals(1))))
        {
            decideConditionsMet1966++;
            if (condition(trn_Pnd_Chk_Dte_Mm.less(7)))                                                                                                                    //Natural: IF #CHK-DTE-MM LT 7
            {
                pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(7);                                                                                                                   //Natural: ASSIGN #NXT-PYT-MM := 7
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(1);                                                                                                                   //Natural: ASSIGN #NXT-PYT-MM := 1
                pnd_Storage_Pnd_Nxt_Pyt_Yyyy.nadd(1);                                                                                                                     //Natural: ADD 1 TO #NXT-PYT-YYYY
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_2().equals(2))))
        {
            decideConditionsMet1966++;
            if (condition(trn_Pnd_Chk_Dte_Mm.less(2)))                                                                                                                    //Natural: IF #CHK-DTE-MM LT 2
            {
                pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(2);                                                                                                                   //Natural: ASSIGN #NXT-PYT-MM := 2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(trn_Pnd_Chk_Dte_Mm.less(8)))                                                                                                                //Natural: IF #CHK-DTE-MM LT 8
                {
                    pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(2);                                                                                                               //Natural: ASSIGN #NXT-PYT-MM := 2
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(2);                                                                                                               //Natural: ASSIGN #NXT-PYT-MM := 2
                    pnd_Storage_Pnd_Nxt_Pyt_Yyyy.nadd(1);                                                                                                                 //Natural: ADD 1 TO #NXT-PYT-YYYY
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_2().equals(3))))
        {
            decideConditionsMet1966++;
            if (condition(trn_Pnd_Chk_Dte_Mm.less(3)))                                                                                                                    //Natural: IF #CHK-DTE-MM LT 3
            {
                pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(3);                                                                                                                   //Natural: ASSIGN #NXT-PYT-MM := 3
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(trn_Pnd_Chk_Dte_Mm.less(9)))                                                                                                                //Natural: IF #CHK-DTE-MM LT 9
                {
                    pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(9);                                                                                                               //Natural: ASSIGN #NXT-PYT-MM := 9
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(3);                                                                                                               //Natural: ASSIGN #NXT-PYT-MM := 3
                    pnd_Storage_Pnd_Nxt_Pyt_Yyyy.nadd(1);                                                                                                                 //Natural: ADD 1 TO #NXT-PYT-YYYY
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_2().equals(4))))
        {
            decideConditionsMet1966++;
            if (condition(trn_Pnd_Chk_Dte_Mm.less(4)))                                                                                                                    //Natural: IF #CHK-DTE-MM LT 4
            {
                pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(4);                                                                                                                   //Natural: ASSIGN #NXT-PYT-MM := 4
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(trn_Pnd_Chk_Dte_Mm.less(10)))                                                                                                               //Natural: IF #CHK-DTE-MM LT 10
                {
                    pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(10);                                                                                                              //Natural: ASSIGN #NXT-PYT-MM := 10
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(4);                                                                                                               //Natural: ASSIGN #NXT-PYT-MM := 4
                    pnd_Storage_Pnd_Nxt_Pyt_Yyyy.nadd(1);                                                                                                                 //Natural: ADD 1 TO #NXT-PYT-YYYY
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 5
        else if (condition((ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_2().equals(5))))
        {
            decideConditionsMet1966++;
            if (condition(trn_Pnd_Chk_Dte_Mm.less(5)))                                                                                                                    //Natural: IF #CHK-DTE-MM LT 5
            {
                pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(5);                                                                                                                   //Natural: ASSIGN #NXT-PYT-MM := 5
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(trn_Pnd_Chk_Dte_Mm.less(11)))                                                                                                               //Natural: IF #CHK-DTE-MM LT 11
                {
                    pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(11);                                                                                                              //Natural: ASSIGN #NXT-PYT-MM := 11
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(5);                                                                                                               //Natural: ASSIGN #NXT-PYT-MM := 5
                    pnd_Storage_Pnd_Nxt_Pyt_Yyyy.nadd(1);                                                                                                                 //Natural: ADD 1 TO #NXT-PYT-YYYY
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 6
        else if (condition((ldaAdsl401.getAds_Prtcpnt_View_Adp_Pymnt_Mode_2().equals(6))))
        {
            decideConditionsMet1966++;
            if (condition(trn_Pnd_Chk_Dte_Mm.less(6)))                                                                                                                    //Natural: IF #CHK-DTE-MM LT 6
            {
                pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(6);                                                                                                                   //Natural: ASSIGN #NXT-PYT-MM := 6
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Storage_Pnd_Nxt_Pyt_Mm.setValue(12);                                                                                                                  //Natural: ASSIGN #NXT-PYT-MM := 12
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Cref_Ct_Write() throws Exception                                                                                                                     //Natural: CREF-CT-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        ldaPstl6075.getCt_Contract_Struct().resetInitial();                                                                                                               //Natural: RESET INITIAL CT-CONTRACT-STRUCT
        ldaPstl6075.getSort_Key_Struct_Sort_Key_Data().getValue("*").moveAll("H'00'");                                                                                    //Natural: MOVE ALL H'00' TO SORT-KEY-DATA ( * )
        ldaPstl6075.getSort_Key_Struct_Key_L3_Lob().setValue("C");                                                                                                        //Natural: MOVE 'C' TO KEY-L3-LOB
        ldaPstl6075.getSort_Key_Struct_Key_L3_Tiebreak().setValue(pnd_Tiebreak);                                                                                          //Natural: MOVE #TIEBREAK TO KEY-L3-TIEBREAK
        ldaPstl6075.getCt_Contract_Struct_Ct_Contract_Number().setValue(pnd_Storage_Pnd_Ia_Cref_Contract);                                                                //Natural: ASSIGN CT-CONTRACT-NUMBER := #IA-CREF-CONTRACT
        ldaPstl6075.getCt_Contract_Struct_Ct_Effective_Date().setValue(pnd_Storage_Pnd_Effctv_Dte);                                                                       //Natural: ASSIGN CT-EFFECTIVE-DATE := #EFFCTV-DTE
        ldaPstl6075.getCt_Contract_Struct_Ct_Issue_Date().setValue(pnd_Storage_Pnd_Issue_Dte);                                                                            //Natural: ASSIGN CT-ISSUE-DATE := #ISSUE-DTE
        ldaPstl6075.getCt_Contract_Struct_Ct_Data_Occurs().setValue(1);                                                                                                   //Natural: ASSIGN CT-DATA-OCCURS := 1
        ldaPstl6075.getCt_Contract_Struct_Ct_Line_Of_Business().setValue("C");                                                                                            //Natural: ASSIGN CT-LINE-OF-BUSINESS := 'C'
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT CT-CONTRACT-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6075.getSort_Key_Struct(), ldaPstl6075.getCt_Contract_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Cref_Ac_Write() throws Exception                                                                                                                     //Natural: CREF-AC-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        ldaPstl6075.getAc_Accumulation_Info_Struct().resetInitial();                                                                                                      //Natural: RESET INITIAL AC-ACCUMULATION-INFO-STRUCT
        ldaPstl6075.getSort_Key_Struct_Sort_Key_Data().getValue("*").moveAll("H'00'");                                                                                    //Natural: MOVE ALL H'00' TO SORT-KEY-DATA ( * )
        ldaPstl6075.getSort_Key_Struct_Key_L3_Lob().setValue("C");                                                                                                        //Natural: MOVE 'C' TO KEY-L3-LOB
        ldaPstl6075.getSort_Key_Struct_Key_L3_Tiebreak().setValue(pnd_Tiebreak);                                                                                          //Natural: MOVE #TIEBREAK TO KEY-L3-TIEBREAK
        pnd_Y.setValue(1);                                                                                                                                                //Natural: ASSIGN #Y := 1
        FOR06:                                                                                                                                                            //Natural: FOR #X 1 TO #CONTRACT-CNT
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pnd_Storage_Pnd_Contract_Cnt)); pnd_X.nadd(1))
        {
            if (condition(pnd_Storage_Pnd_Cref_Addit_Accum.getValue(pnd_X).notEquals(getZero())))                                                                         //Natural: IF #CREF-ADDIT-ACCUM ( #X ) NE 0
            {
                if (condition(pnd_Storage_Pnd_Da_Cref_Nbr.getValue(pnd_X).equals("        ")))                                                                            //Natural: IF #DA-CREF-NBR ( #X ) = '        '
                {
                    ldaPstl6075.getAc_Accumulation_Info_Struct_Ac_Contract_Number().getValue(pnd_Y).setValue("MISSING");                                                  //Natural: ASSIGN AC-CONTRACT-NUMBER ( #Y ) := 'MISSING'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaPstl6075.getAc_Accumulation_Info_Struct_Ac_Contract_Number().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Da_Cref_Nbr.getValue(pnd_X));                //Natural: ASSIGN AC-CONTRACT-NUMBER ( #Y ) := #DA-CREF-NBR ( #X )
                }                                                                                                                                                         //Natural: END-IF
                ldaPstl6075.getAc_Accumulation_Info_Struct_Ac_Accumulation().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Cref_Addit_Accum.getValue(pnd_X));                  //Natural: ASSIGN AC-ACCUMULATION ( #Y ) := #CREF-ADDIT-ACCUM ( #X )
                pnd_Y.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #Y
            }                                                                                                                                                             //Natural: END-IF
            //*  TOTALS BUCKET
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaPstl6075.getAc_Accumulation_Info_Struct_Ac_Contract_Number().getValue(pnd_Y).setValue(" ");                                                                    //Natural: ASSIGN AC-CONTRACT-NUMBER ( #Y ) := ' '
        ldaPstl6075.getAc_Accumulation_Info_Struct_Ac_Accumulation().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Tot_Cref_Add_Accum);                                        //Natural: ASSIGN AC-ACCUMULATION ( #Y ) := #TOT-CREF-ADD-ACCUM
        ldaPstl6075.getAc_Accumulation_Info_Struct_Ac_Data_Occurs().setValue(pnd_Y);                                                                                      //Natural: ASSIGN AC-DATA-OCCURS := #Y
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT AC-ACCUMULATION-INFO-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6075.getSort_Key_Struct(), ldaPstl6075.getAc_Accumulation_Info_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Cref_Annl_In_Write() throws Exception                                                                                                                //Natural: CREF-ANNL-IN-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        ldaPstl6075.getIn_Income_Struct().resetInitial();                                                                                                                 //Natural: RESET INITIAL IN-INCOME-STRUCT
        ldaPstl6075.getSort_Key_Struct_Sort_Key_Data().getValue("*").moveAll("H'00'");                                                                                    //Natural: MOVE ALL H'00' TO SORT-KEY-DATA ( * )
        ldaPstl6075.getSort_Key_Struct_Key_L3_Lob().setValue("C");                                                                                                        //Natural: MOVE 'C' TO KEY-L3-LOB
        ldaPstl6075.getSort_Key_Struct_Key_L3_Tiebreak().setValue(pnd_Tiebreak);                                                                                          //Natural: MOVE #TIEBREAK TO KEY-L3-TIEBREAK
        FOR07:                                                                                                                                                            //Natural: FOR #X 1 TO 18
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(18)); pnd_X.nadd(1))
        {
            if (condition(pnd_Storage_Pnd_Cref_Inc_Acct_Cde.getValue(pnd_X).equals(" ")))                                                                                 //Natural: IF #CREF-INC-ACCT-CDE ( #X ) = ' '
            {
                ldaPstl6075.getIn_Income_Struct_In_Data_Occurs().compute(new ComputeParameters(false, ldaPstl6075.getIn_Income_Struct_In_Data_Occurs()),                  //Natural: SUBTRACT 1 FROM #X GIVING IN-DATA-OCCURS
                    pnd_X.subtract(1));
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  EM - 101413 START
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                FOR08:                                                                                                                                                    //Natural: FOR #Y 1 TO #NBR-ACCT
                for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(pdaAdsa888.getPnd_Nbr_Acct())); pnd_Y.nadd(1))
                {
                    //*      IF #ACCT-ALPHA-FUND-CDE (#Y) = #CREF-INC-ACCT-CDE (#X)
                    //*        MOVE #ACCT-NAME-10 (#Y) TO IN-FUND-NAME (#X)
                    if (condition(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Alpha_Fund_Cde().getValue(pnd_Y).equals(pnd_Storage_Pnd_Cref_Inc_Acct_Cde.getValue(pnd_X))         //Natural: IF #ACCT-ALPHA-FUND-CDE ( #Y ) = #CREF-INC-ACCT-CDE ( #X ) AND #ACCT-ACT-CLASS-CDE ( #Y ) = #IA-FUND-CLASS
                        && pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Act_Class_Cde().getValue(pnd_Y).equals(pdaAdsa888.getPnd_Parm_Area_Pnd_Ia_Fund_Class())))
                    {
                        ldaPstl6075.getIn_Income_Struct_In_Fund_Name().getValue(pnd_X).setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Name_18().getValue(pnd_Y));          //Natural: MOVE #ACCT-NAME-18 ( #Y ) TO IN-FUND-NAME ( #X )
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    //*  EM - 101413 END
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaPstl6075.getIn_Income_Struct_In_Units().getValue(pnd_X).setValue(pnd_Storage_Pnd_Cref_Inc_Annl_Units.getValue(pnd_X));                                 //Natural: ASSIGN IN-UNITS ( #X ) := #CREF-INC-ANNL-UNITS ( #X )
                ldaPstl6075.getIn_Income_Struct_In_Unit_Value().getValue(pnd_X).setValue(pnd_Storage_Pnd_Cref_Inc_Annl_Unit_Val.getValue(pnd_X));                         //Natural: ASSIGN IN-UNIT-VALUE ( #X ) := #CREF-INC-ANNL-UNIT-VAL ( #X )
                ldaPstl6075.getIn_Income_Struct_In_Estimated_Income().getValue(pnd_X).setValue(pnd_Storage_Pnd_Cref_Inc_Annl_Est_Inc.getValue(pnd_X));                    //Natural: ASSIGN IN-ESTIMATED-INCOME ( #X ) := #CREF-INC-ANNL-EST-INC ( #X )
                ldaPstl6075.getIn_Income_Struct_In_Change_Method().getValue(pnd_X).setValue("A");                                                                         //Natural: ASSIGN IN-CHANGE-METHOD ( #X ) := 'A'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(ldaPstl6075.getIn_Income_Struct_In_Data_Occurs().greater(getZero())))                                                                               //Natural: IF IN-DATA-OCCURS GT 0
        {
            pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                         //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
            DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT IN-INCOME-STRUCT
                pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
                ldaPstl6075.getSort_Key_Struct(), ldaPstl6075.getIn_Income_Struct());
            if (condition(Global.isEscape())) return;
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                              //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
            {
                                                                                                                                                                          //Natural: PERFORM ERROR
                sub_Error();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Cref_Mnthly_In_Write() throws Exception                                                                                                              //Natural: CREF-MNTHLY-IN-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        ldaPstl6075.getIn_Income_Struct().resetInitial();                                                                                                                 //Natural: RESET INITIAL IN-INCOME-STRUCT
        ldaPstl6075.getSort_Key_Struct_Sort_Key_Data().getValue("*").moveAll("H'00'");                                                                                    //Natural: MOVE ALL H'00' TO SORT-KEY-DATA ( * )
        ldaPstl6075.getSort_Key_Struct_Key_L3_Lob().setValue("C");                                                                                                        //Natural: MOVE 'C' TO KEY-L3-LOB
        ldaPstl6075.getSort_Key_Struct_Key_L3_Tiebreak().setValue(pnd_Tiebreak);                                                                                          //Natural: MOVE #TIEBREAK TO KEY-L3-TIEBREAK
        FOR09:                                                                                                                                                            //Natural: FOR #X 1 TO 18
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(18)); pnd_X.nadd(1))
        {
            //*  TOTALS BUCKET
            if (condition(pnd_Storage_Pnd_Cref_Inc_Acct_Cde.getValue(pnd_X).equals(" ")))                                                                                 //Natural: IF #CREF-INC-ACCT-CDE ( #X ) = ' '
            {
                ldaPstl6075.getIn_Income_Struct_In_Fund_Name().getValue(pnd_X).setValue(" ");                                                                             //Natural: ASSIGN IN-FUND-NAME ( #X ) := ' '
                ldaPstl6075.getIn_Income_Struct_In_Change_Method().getValue(pnd_X).setValue("M");                                                                         //Natural: ASSIGN IN-CHANGE-METHOD ( #X ) := 'M'
                ldaPstl6075.getIn_Income_Struct_In_Estimated_Income().getValue(pnd_X).setValue(pnd_Storage_Pnd_Tot_Cref_Add_Inc);                                         //Natural: ASSIGN IN-ESTIMATED-INCOME ( #X ) := #TOT-CREF-ADD-INC
                ldaPstl6075.getIn_Income_Struct_In_Data_Occurs().setValue(pnd_X);                                                                                         //Natural: MOVE #X TO IN-DATA-OCCURS
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  EM - 101413 START
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                FOR10:                                                                                                                                                    //Natural: FOR #Y 1 TO #NBR-ACCT
                for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(pdaAdsa888.getPnd_Nbr_Acct())); pnd_Y.nadd(1))
                {
                    //*      IF #ACCT-ALPHA-FUND-CDE (#Y) = #CREF-INC-ACCT-CDE (#X)
                    //*        MOVE #ACCT-NAME-10 (#Y) TO IN-FUND-NAME (#X)
                    if (condition(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Alpha_Fund_Cde().getValue(pnd_Y).equals(pnd_Storage_Pnd_Cref_Inc_Acct_Cde.getValue(pnd_X))         //Natural: IF #ACCT-ALPHA-FUND-CDE ( #Y ) = #CREF-INC-ACCT-CDE ( #X ) AND #ACCT-ACT-CLASS-CDE ( #Y ) = #IA-FUND-CLASS
                        && pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Act_Class_Cde().getValue(pnd_Y).equals(pdaAdsa888.getPnd_Parm_Area_Pnd_Ia_Fund_Class())))
                    {
                        ldaPstl6075.getIn_Income_Struct_In_Fund_Name().getValue(pnd_X).setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Name_18().getValue(pnd_Y));          //Natural: MOVE #ACCT-NAME-18 ( #Y ) TO IN-FUND-NAME ( #X )
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    //*  EM - 101413 END
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaPstl6075.getIn_Income_Struct_In_Units().getValue(pnd_X).setValue(pnd_Storage_Pnd_Cref_Inc_Mnthly_Units.getValue(pnd_X));                               //Natural: ASSIGN IN-UNITS ( #X ) := #CREF-INC-MNTHLY-UNITS ( #X )
                ldaPstl6075.getIn_Income_Struct_In_Unit_Value().getValue(pnd_X).setValue(pnd_Storage_Pnd_Cref_Inc_Mnthly_Unit_Val.getValue(pnd_X));                       //Natural: ASSIGN IN-UNIT-VALUE ( #X ) := #CREF-INC-MNTHLY-UNIT-VAL ( #X )
                ldaPstl6075.getIn_Income_Struct_In_Estimated_Income().getValue(pnd_X).setValue(pnd_Storage_Pnd_Cref_Inc_Mnthly_Est_Inc.getValue(pnd_X));                  //Natural: ASSIGN IN-ESTIMATED-INCOME ( #X ) := #CREF-INC-MNTHLY-EST-INC ( #X )
                ldaPstl6075.getIn_Income_Struct_In_Change_Method().getValue(pnd_X).setValue("M");                                                                         //Natural: ASSIGN IN-CHANGE-METHOD ( #X ) := 'M'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT IN-INCOME-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6075.getSort_Key_Struct(), ldaPstl6075.getIn_Income_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Cref_Rea_Ms_Write() throws Exception                                                                                                                 //Natural: CREF-REA-MS-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        ldaPstl6075.getMs_Messages_Struct().resetInitial();                                                                                                               //Natural: RESET INITIAL MS-MESSAGES-STRUCT
        ldaPstl6075.getSort_Key_Struct_Sort_Key_Data().getValue("*").moveAll("H'00'");                                                                                    //Natural: MOVE ALL H'00' TO SORT-KEY-DATA ( * )
        if (condition(pnd_Storage_Pnd_Cref_Rea_Ind.equals("C")))                                                                                                          //Natural: IF #CREF-REA-IND = 'C'
        {
            ldaPstl6075.getSort_Key_Struct_Key_L3_Lob().setValue("C");                                                                                                    //Natural: MOVE 'C' TO KEY-L3-LOB
            ldaPstl6075.getMs_Messages_Struct_Ms_Data_Occurs().setValue(4);                                                                                               //Natural: ASSIGN MS-DATA-OCCURS := 4
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaPstl6075.getSort_Key_Struct_Key_L3_Lob().setValue("R");                                                                                                    //Natural: MOVE 'R' TO KEY-L3-LOB
            ldaPstl6075.getMs_Messages_Struct_Ms_Data_Occurs().setValue(5);                                                                                               //Natural: ASSIGN MS-DATA-OCCURS := 5
        }                                                                                                                                                                 //Natural: END-IF
        ldaPstl6075.getSort_Key_Struct_Key_L3_Tiebreak().setValue(pnd_Tiebreak);                                                                                          //Natural: MOVE #TIEBREAK TO KEY-L3-TIEBREAK
        ldaPstl6075.getMs_Messages_Struct_Ms_Message_Id().getValue(1).setValue("1");                                                                                      //Natural: ASSIGN MS-MESSAGE-ID ( 1 ) := '1'
        if (condition(pnd_Storage_Pnd_Frequency.equals("               ")))                                                                                               //Natural: IF #FREQUENCY = '               '
        {
                                                                                                                                                                          //Natural: PERFORM GET-FREQUENCY
            sub_Get_Frequency();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        ldaPstl6075.getMs_Messages_Struct_Ms_Payment_Frequency().getValue(1).setValue(pnd_Storage_Pnd_Frequency);                                                         //Natural: ASSIGN MS-PAYMENT-FREQUENCY ( 1 ) := #FREQUENCY
        ldaPstl6075.getMs_Messages_Struct_Ms_Message_Id().getValue(2).setValue("2");                                                                                      //Natural: ASSIGN MS-MESSAGE-ID ( 2 ) := '2'
        if (condition(pnd_Storage_Pnd_Cref_Rea_Ind.equals("C")))                                                                                                          //Natural: IF #CREF-REA-IND = 'C'
        {
            pnd_Trn_Cntrct_Key_Pnd_Trn_Key_Ia_Ppcn.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Ia_Cref_Nbr());                                                            //Natural: ASSIGN #TRN-KEY-IA-PPCN := ADP-IA-CREF-NBR
            //*  EM - 012709
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Trn_Cntrct_Key_Pnd_Trn_Key_Ia_Ppcn.setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Ia_Tiaa_Nbr());                                                            //Natural: ASSIGN #TRN-KEY-IA-PPCN := ADP-IA-TIAA-NBR
            ldaPstl6075.getMs_Messages_Struct_Ms_Message_Id().getValue(5).setValue("8");                                                                                  //Natural: ASSIGN MS-MESSAGE-ID ( 5 ) := '8'
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-NXT-PYT-DTE
        sub_Get_Nxt_Pyt_Dte();
        if (condition(Global.isEscape())) {return;}
        ldaPstl6075.getMs_Messages_Struct_Ms_Message_Date().getValue(2).setValue(pnd_Storage_Pnd_Nxt_Pyt_Dte);                                                            //Natural: ASSIGN MS-MESSAGE-DATE ( 2 ) := #NXT-PYT-DTE
        ldaPstl6075.getMs_Messages_Struct_Ms_Message_Id().getValue(3).setValue("4");                                                                                      //Natural: ASSIGN MS-MESSAGE-ID ( 3 ) := '4'
        ldaPstl6075.getMs_Messages_Struct_Ms_Message_Date().getValue(3).setValue(pnd_Storage_Pnd_Effctv_Dte);                                                             //Natural: MOVE #EFFCTV-DTE TO MS-MESSAGE-DATE ( 3 )
        ldaPstl6075.getMs_Messages_Struct_Ms_Message_Id().getValue(4).setValue("5");                                                                                      //Natural: ASSIGN MS-MESSAGE-ID ( 4 ) := '5'
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT MS-MESSAGES-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6075.getSort_Key_Struct(), ldaPstl6075.getMs_Messages_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Rea_Ct_Write() throws Exception                                                                                                                      //Natural: REA-CT-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        ldaPstl6075.getCt_Contract_Struct().resetInitial();                                                                                                               //Natural: RESET INITIAL CT-CONTRACT-STRUCT
        ldaPstl6075.getSort_Key_Struct_Sort_Key_Data().getValue("*").moveAll("H'00'");                                                                                    //Natural: MOVE ALL H'00' TO SORT-KEY-DATA ( * )
        ldaPstl6075.getSort_Key_Struct_Key_L3_Lob().setValue("R");                                                                                                        //Natural: MOVE 'R' TO KEY-L3-LOB
        ldaPstl6075.getSort_Key_Struct_Key_L3_Tiebreak().setValue(pnd_Tiebreak);                                                                                          //Natural: MOVE #TIEBREAK TO KEY-L3-TIEBREAK
        ldaPstl6075.getCt_Contract_Struct_Ct_Contract_Number().setValue(pnd_Storage_Pnd_Ia_Tiaa_Contract);                                                                //Natural: ASSIGN CT-CONTRACT-NUMBER := #IA-TIAA-CONTRACT
        ldaPstl6075.getCt_Contract_Struct_Ct_Effective_Date().setValue(pnd_Storage_Pnd_Effctv_Dte);                                                                       //Natural: ASSIGN CT-EFFECTIVE-DATE := #EFFCTV-DTE
        ldaPstl6075.getCt_Contract_Struct_Ct_Issue_Date().setValue(pnd_Storage_Pnd_Issue_Dte);                                                                            //Natural: ASSIGN CT-ISSUE-DATE := #ISSUE-DTE
        ldaPstl6075.getCt_Contract_Struct_Ct_Data_Occurs().setValue(1);                                                                                                   //Natural: ASSIGN CT-DATA-OCCURS := 1
        ldaPstl6075.getCt_Contract_Struct_Ct_Line_Of_Business().setValue("R");                                                                                            //Natural: ASSIGN CT-LINE-OF-BUSINESS := 'R'
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT CT-CONTRACT-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6075.getSort_Key_Struct(), ldaPstl6075.getCt_Contract_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Rea_Ac_Write() throws Exception                                                                                                                      //Natural: REA-AC-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        ldaPstl6075.getAc_Accumulation_Info_Struct().resetInitial();                                                                                                      //Natural: RESET INITIAL AC-ACCUMULATION-INFO-STRUCT
        ldaPstl6075.getSort_Key_Struct_Sort_Key_Data().getValue("*").moveAll("H'00'");                                                                                    //Natural: MOVE ALL H'00' TO SORT-KEY-DATA ( * )
        ldaPstl6075.getSort_Key_Struct_Key_L3_Lob().setValue("R");                                                                                                        //Natural: MOVE 'R' TO KEY-L3-LOB
        ldaPstl6075.getSort_Key_Struct_Key_L3_Tiebreak().setValue(pnd_Tiebreak);                                                                                          //Natural: MOVE #TIEBREAK TO KEY-L3-TIEBREAK
        pnd_Y.setValue(1);                                                                                                                                                //Natural: ASSIGN #Y := 1
        FOR11:                                                                                                                                                            //Natural: FOR #X 1 TO #CONTRACT-CNT
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pnd_Storage_Pnd_Contract_Cnt)); pnd_X.nadd(1))
        {
            if (condition(pnd_Storage_Pnd_Rea_Addit_Accum.getValue(pnd_X).notEquals(getZero())))                                                                          //Natural: IF #REA-ADDIT-ACCUM ( #X ) NE 0
            {
                ldaPstl6075.getAc_Accumulation_Info_Struct_Ac_Contract_Number().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Da_Tiaa_Nbr.getValue(pnd_X));                    //Natural: ASSIGN AC-CONTRACT-NUMBER ( #Y ) := #DA-TIAA-NBR ( #X )
                ldaPstl6075.getAc_Accumulation_Info_Struct_Ac_Accumulation().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Rea_Addit_Accum.getValue(pnd_X));                   //Natural: ASSIGN AC-ACCUMULATION ( #Y ) := #REA-ADDIT-ACCUM ( #X )
                pnd_Y.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #Y
            }                                                                                                                                                             //Natural: END-IF
            //*  TOTALS BUCKET
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaPstl6075.getAc_Accumulation_Info_Struct_Ac_Contract_Number().getValue(pnd_Y).setValue(" ");                                                                    //Natural: ASSIGN AC-CONTRACT-NUMBER ( #Y ) := ' '
        ldaPstl6075.getAc_Accumulation_Info_Struct_Ac_Accumulation().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Tot_Rea_Add_Accum);                                         //Natural: ASSIGN AC-ACCUMULATION ( #Y ) := #TOT-REA-ADD-ACCUM
        ldaPstl6075.getAc_Accumulation_Info_Struct_Ac_Data_Occurs().setValue(pnd_Y);                                                                                      //Natural: ASSIGN AC-DATA-OCCURS := #Y
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT AC-ACCUMULATION-INFO-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6075.getSort_Key_Struct(), ldaPstl6075.getAc_Accumulation_Info_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Rea_Annl_In_Write() throws Exception                                                                                                                 //Natural: REA-ANNL-IN-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        ldaPstl6075.getIn_Income_Struct().resetInitial();                                                                                                                 //Natural: RESET INITIAL IN-INCOME-STRUCT #Y
        pnd_Y.resetInitial();
        ldaPstl6075.getSort_Key_Struct_Sort_Key_Data().getValue("*").moveAll("H'00'");                                                                                    //Natural: MOVE ALL H'00' TO SORT-KEY-DATA ( * )
        ldaPstl6075.getSort_Key_Struct_Key_L3_Lob().setValue("R");                                                                                                        //Natural: MOVE 'R' TO KEY-L3-LOB
        ldaPstl6075.getSort_Key_Struct_Key_L3_Tiebreak().setValue(pnd_Tiebreak);                                                                                          //Natural: MOVE #TIEBREAK TO KEY-L3-TIEBREAK
        //*  EM 012709 - START
        ldaPstl6075.getIn_Income_Struct_In_Data_Occurs().setValue(0);                                                                                                     //Natural: MOVE 0 TO IN-DATA-OCCURS
        FOR12:                                                                                                                                                            //Natural: FOR #X 1 TO 18
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(18)); pnd_X.nadd(1))
        {
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_X).equals(" ")))                                                             //Natural: IF ADS-IA-RSLT-VIEW.ADI-DTL-CREF-ACCT-CD ( #X ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                short decideConditionsMet2242 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ADS-IA-RSLT-VIEW.ADI-DTL-CREF-ACCT-CD ( #X ) = 'R'
                if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_X).equals("R")))
                {
                    decideConditionsMet2242++;
                    pnd_Y.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #Y
                    //*                                                    /* EM - 101413 START
                    pnd_Indx.reset();                                                                                                                                     //Natural: RESET #INDX
                    DbsUtil.examine(new ExamineSource(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Alpha_Fund_Cde().getValue("*")), new ExamineSearch(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_X)),  //Natural: EXAMINE #ACCT-ALPHA-FUND-CDE ( * ) FOR ADS-IA-RSLT-VIEW.ADI-DTL-CREF-ACCT-CD ( #X ) GIVING INDEX #INDX
                        new ExamineGivingIndex(pnd_Indx));
                    if (condition(pnd_Indx.greater(getZero())))                                                                                                           //Natural: IF #INDX > 0
                    {
                        ldaPstl6075.getIn_Income_Struct_In_Fund_Name().getValue(pnd_Y).setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Name_18().getValue(pnd_Indx));       //Natural: MOVE #ACCT-NAME-18 ( #INDX ) TO IN-FUND-NAME ( #Y )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(0, "*******************************************");                                                                             //Natural: WRITE '*******************************************'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "IA RESULT RECORD ACCT CDE ",ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_X));                        //Natural: WRITE 'IA RESULT RECORD ACCT CDE ' ADI-DTL-CREF-ACCT-CD ( #X )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "DOES NOT EXIST ON THE EXTERNALIZATION TABLE");                                                                             //Natural: WRITE 'DOES NOT EXIST ON THE EXTERNALIZATION TABLE'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "FOR REQUEST ID ",ldaAdsl450.getAds_Ia_Rslt_View_Rqst_Id());                                                                //Natural: WRITE 'FOR REQUEST ID ' ADS-IA-RSLT-VIEW.RQST-ID
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "*******************************************");                                                                             //Natural: WRITE '*******************************************'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    ldaPstl6075.getIn_Income_Struct_In_Units().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Rea_Annl_Units);                                                  //Natural: ASSIGN IN-UNITS ( #Y ) := #REA-ANNL-UNITS
                    ldaPstl6075.getIn_Income_Struct_In_Unit_Value().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Rea_Annl_Unit_Val);                                          //Natural: ASSIGN IN-UNIT-VALUE ( #Y ) := #REA-ANNL-UNIT-VAL
                    ldaPstl6075.getIn_Income_Struct_In_Estimated_Income().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Rea_Annl_Add_Inc);                                     //Natural: ASSIGN IN-ESTIMATED-INCOME ( #Y ) := #REA-ANNL-ADD-INC
                    ldaPstl6075.getIn_Income_Struct_In_Change_Method().getValue(pnd_Y).setValue("A");                                                                     //Natural: ASSIGN IN-CHANGE-METHOD ( #Y ) := 'A'
                    ldaPstl6075.getIn_Income_Struct_In_Data_Occurs().nadd(1);                                                                                             //Natural: ADD 1 TO IN-DATA-OCCURS
                }                                                                                                                                                         //Natural: WHEN ADS-IA-RSLT-VIEW.ADI-DTL-CREF-ACCT-CD ( #X ) = 'D'
                else if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_X).equals("D")))
                {
                    decideConditionsMet2242++;
                    pnd_Y.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #Y
                    //*                                                    /* EM - 101413 START
                    pnd_Indx.reset();                                                                                                                                     //Natural: RESET #INDX
                    DbsUtil.examine(new ExamineSource(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Alpha_Fund_Cde().getValue("*")), new ExamineSearch(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_X)),  //Natural: EXAMINE #ACCT-ALPHA-FUND-CDE ( * ) FOR ADS-IA-RSLT-VIEW.ADI-DTL-CREF-ACCT-CD ( #X ) GIVING INDEX #INDX
                        new ExamineGivingIndex(pnd_Indx));
                    if (condition(pnd_Indx.greater(getZero())))                                                                                                           //Natural: IF #INDX > 0
                    {
                        ldaPstl6075.getIn_Income_Struct_In_Fund_Name().getValue(pnd_Y).setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Name_18().getValue(pnd_Indx));       //Natural: MOVE #ACCT-NAME-18 ( #INDX ) TO IN-FUND-NAME ( #Y )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(0, "*******************************************");                                                                             //Natural: WRITE '*******************************************'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "IA RESULT RECORD ACCT CDE ",ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_X));                        //Natural: WRITE 'IA RESULT RECORD ACCT CDE ' ADI-DTL-CREF-ACCT-CD ( #X )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "DOES NOT EXIST ON THE EXTERNALIZATION TABLE");                                                                             //Natural: WRITE 'DOES NOT EXIST ON THE EXTERNALIZATION TABLE'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "FOR REQUEST ID ",ldaAdsl450.getAds_Ia_Rslt_View_Rqst_Id());                                                                //Natural: WRITE 'FOR REQUEST ID ' ADS-IA-RSLT-VIEW.RQST-ID
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "*******************************************");                                                                             //Natural: WRITE '*******************************************'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    ldaPstl6075.getIn_Income_Struct_In_Units().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Acc_Annl_Units);                                                  //Natural: ASSIGN IN-UNITS ( #Y ) := #ACC-ANNL-UNITS
                    ldaPstl6075.getIn_Income_Struct_In_Unit_Value().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Acc_Annl_Unit_Val);                                          //Natural: ASSIGN IN-UNIT-VALUE ( #Y ) := #ACC-ANNL-UNIT-VAL
                    ldaPstl6075.getIn_Income_Struct_In_Estimated_Income().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Acc_Annl_Add_Inc);                                     //Natural: ASSIGN IN-ESTIMATED-INCOME ( #Y ) := #ACC-ANNL-ADD-INC
                    ldaPstl6075.getIn_Income_Struct_In_Change_Method().getValue(pnd_Y).setValue("A");                                                                     //Natural: ASSIGN IN-CHANGE-METHOD ( #Y ) := 'A'
                    ldaPstl6075.getIn_Income_Struct_In_Data_Occurs().nadd(1);                                                                                             //Natural: ADD 1 TO IN-DATA-OCCURS
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            //*  EM - 012709 END
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT IN-INCOME-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6075.getSort_Key_Struct(), ldaPstl6075.getIn_Income_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Rea_Mnthly_In_Write() throws Exception                                                                                                               //Natural: REA-MNTHLY-IN-WRITE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        ldaPstl6075.getIn_Income_Struct().resetInitial();                                                                                                                 //Natural: RESET INITIAL IN-INCOME-STRUCT #Y
        pnd_Y.resetInitial();
        ldaPstl6075.getSort_Key_Struct_Sort_Key_Data().getValue("*").moveAll("H'00'");                                                                                    //Natural: MOVE ALL H'00' TO SORT-KEY-DATA ( * )
        ldaPstl6075.getSort_Key_Struct_Key_L3_Lob().setValue("R");                                                                                                        //Natural: MOVE 'R' TO KEY-L3-LOB
        ldaPstl6075.getSort_Key_Struct_Key_L3_Tiebreak().setValue(pnd_Tiebreak);                                                                                          //Natural: MOVE #TIEBREAK TO KEY-L3-TIEBREAK
        //*  EM 012709 - START
        ldaPstl6075.getIn_Income_Struct_In_Data_Occurs().setValue(0);                                                                                                     //Natural: MOVE 0 TO IN-DATA-OCCURS
        FOR13:                                                                                                                                                            //Natural: FOR #X 1 TO 18
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(18)); pnd_X.nadd(1))
        {
            //*  TOTALS
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_X).equals(" ")))                                                             //Natural: IF ADS-IA-RSLT-VIEW.ADI-DTL-CREF-ACCT-CD ( #X ) = ' '
            {
                ldaPstl6075.getIn_Income_Struct_In_Fund_Name().getValue(2).setValue(" ");                                                                                 //Natural: ASSIGN IN-FUND-NAME ( 2 ) := ' '
                ldaPstl6075.getIn_Income_Struct_In_Change_Method().getValue(2).setValue("M");                                                                             //Natural: ASSIGN IN-CHANGE-METHOD ( 2 ) := 'M'
                ldaPstl6075.getIn_Income_Struct_In_Estimated_Income().getValue(2).compute(new ComputeParameters(false, ldaPstl6075.getIn_Income_Struct_In_Estimated_Income().getValue(2)),  //Natural: ASSIGN IN-ESTIMATED-INCOME ( 2 ) := #TOT-REA-ADD-INC + #TOT-ACC-ADD-INC
                    pnd_Storage_Pnd_Tot_Rea_Add_Inc.add(pnd_Storage_Pnd_Tot_Acc_Add_Inc));
                ldaPstl6075.getIn_Income_Struct_In_Data_Occurs().nadd(1);                                                                                                 //Natural: ADD 1 TO IN-DATA-OCCURS
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                short decideConditionsMet2311 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ADS-IA-RSLT-VIEW.ADI-DTL-CREF-ACCT-CD ( #X ) = 'R'
                if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_X).equals("R")))
                {
                    decideConditionsMet2311++;
                    pnd_Y.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #Y
                    //*                                                    /* EM - 101413 START
                    pnd_Indx.reset();                                                                                                                                     //Natural: RESET #INDX
                    DbsUtil.examine(new ExamineSource(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Alpha_Fund_Cde().getValue("*")), new ExamineSearch(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_X)),  //Natural: EXAMINE #ACCT-ALPHA-FUND-CDE ( * ) FOR ADS-IA-RSLT-VIEW.ADI-DTL-CREF-ACCT-CD ( #X ) GIVING INDEX #INDX
                        new ExamineGivingIndex(pnd_Indx));
                    if (condition(pnd_Indx.greater(getZero())))                                                                                                           //Natural: IF #INDX > 0
                    {
                        ldaPstl6075.getIn_Income_Struct_In_Fund_Name().getValue(pnd_Y).setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Name_18().getValue(pnd_Indx));       //Natural: MOVE #ACCT-NAME-18 ( #INDX ) TO IN-FUND-NAME ( #Y )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(0, "*******************************************");                                                                             //Natural: WRITE '*******************************************'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "IA RESULT RECORD ACCT CDE ",ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_X));                        //Natural: WRITE 'IA RESULT RECORD ACCT CDE ' ADI-DTL-CREF-ACCT-CD ( #X )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "DOES NOT EXIST ON THE EXTERNALIZATION TABLE");                                                                             //Natural: WRITE 'DOES NOT EXIST ON THE EXTERNALIZATION TABLE'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "FOR REQUEST ID ",ldaAdsl450.getAds_Ia_Rslt_View_Rqst_Id());                                                                //Natural: WRITE 'FOR REQUEST ID ' ADS-IA-RSLT-VIEW.RQST-ID
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "*******************************************");                                                                             //Natural: WRITE '*******************************************'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    ldaPstl6075.getIn_Income_Struct_In_Units().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Rea_Mnthly_Units);                                                //Natural: ASSIGN IN-UNITS ( #Y ) := #REA-MNTHLY-UNITS
                    ldaPstl6075.getIn_Income_Struct_In_Unit_Value().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Rea_Mnthly_Unit_Val);                                        //Natural: ASSIGN IN-UNIT-VALUE ( #Y ) := #REA-MNTHLY-UNIT-VAL
                    ldaPstl6075.getIn_Income_Struct_In_Estimated_Income().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Rea_Mnthly_Add_Inc);                                   //Natural: ASSIGN IN-ESTIMATED-INCOME ( #Y ) := #REA-MNTHLY-ADD-INC
                    ldaPstl6075.getIn_Income_Struct_In_Change_Method().getValue(pnd_Y).setValue("M");                                                                     //Natural: ASSIGN IN-CHANGE-METHOD ( #Y ) := 'M'
                    ldaPstl6075.getIn_Income_Struct_In_Data_Occurs().nadd(1);                                                                                             //Natural: ADD 1 TO IN-DATA-OCCURS
                }                                                                                                                                                         //Natural: WHEN ADS-IA-RSLT-VIEW.ADI-DTL-CREF-ACCT-CD ( #X ) = 'D'
                else if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_X).equals("D")))
                {
                    decideConditionsMet2311++;
                    pnd_Y.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #Y
                    //*                                                    /* EM - 101413 START
                    pnd_Indx.reset();                                                                                                                                     //Natural: RESET #INDX
                    DbsUtil.examine(new ExamineSource(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Alpha_Fund_Cde().getValue("*")), new ExamineSearch(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_X)),  //Natural: EXAMINE #ACCT-ALPHA-FUND-CDE ( * ) FOR ADS-IA-RSLT-VIEW.ADI-DTL-CREF-ACCT-CD ( #X ) GIVING INDEX #INDX
                        new ExamineGivingIndex(pnd_Indx));
                    if (condition(pnd_Indx.greater(getZero())))                                                                                                           //Natural: IF #INDX > 0
                    {
                        ldaPstl6075.getIn_Income_Struct_In_Fund_Name().getValue(pnd_Y).setValue(pdaAdsa888.getPnd_Parm_Area_Pnd_Acct_Name_18().getValue(pnd_Indx));       //Natural: MOVE #ACCT-NAME-18 ( #INDX ) TO IN-FUND-NAME ( #Y )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(0, "*******************************************");                                                                             //Natural: WRITE '*******************************************'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "IA RESULT RECORD ACCT CDE ",ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_X));                        //Natural: WRITE 'IA RESULT RECORD ACCT CDE ' ADI-DTL-CREF-ACCT-CD ( #X )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "DOES NOT EXIST ON THE EXTERNALIZATION TABLE");                                                                             //Natural: WRITE 'DOES NOT EXIST ON THE EXTERNALIZATION TABLE'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "FOR REQUEST ID ",ldaAdsl450.getAds_Ia_Rslt_View_Rqst_Id());                                                                //Natural: WRITE 'FOR REQUEST ID ' ADS-IA-RSLT-VIEW.RQST-ID
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, "*******************************************");                                                                             //Natural: WRITE '*******************************************'
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    ldaPstl6075.getIn_Income_Struct_In_Units().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Acc_Mnthly_Units);                                                //Natural: ASSIGN IN-UNITS ( #Y ) := #ACC-MNTHLY-UNITS
                    ldaPstl6075.getIn_Income_Struct_In_Unit_Value().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Acc_Mnthly_Unit_Val);                                        //Natural: ASSIGN IN-UNIT-VALUE ( #Y ) := #ACC-MNTHLY-UNIT-VAL
                    ldaPstl6075.getIn_Income_Struct_In_Estimated_Income().getValue(pnd_Y).setValue(pnd_Storage_Pnd_Acc_Mnthly_Add_Inc);                                   //Natural: ASSIGN IN-ESTIMATED-INCOME ( #Y ) := #ACC-MNTHLY-ADD-INC
                    ldaPstl6075.getIn_Income_Struct_In_Change_Method().getValue(pnd_Y).setValue("M");                                                                     //Natural: ASSIGN IN-CHANGE-METHOD ( #Y ) := 'M'
                    ldaPstl6075.getIn_Income_Struct_In_Data_Occurs().nadd(1);                                                                                             //Natural: ADD 1 TO IN-DATA-OCCURS
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            //*  EM - 012709 END
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        DbsUtil.callnat(Pstn9500.class , getCurrentProcessState(), pdaPsta9500.getPsta9500(), pdaPsta9500.getPsta9500_Id(), pdaPsta9501.getPsta9501(),                    //Natural: CALLNAT 'PSTN9500' PSTA9500 PSTA9500-ID PSTA9501 CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB PSTA9610 SORT-KEY-STRUCT IN-INCOME-STRUCT
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaPsta9610.getPsta9610(), 
            ldaPstl6075.getSort_Key_Struct(), ldaPstl6075.getIn_Income_Struct());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Call_Post_For_Open() throws Exception                                                                                                                //Natural: CALL-POST-FOR-OPEN
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        pdaPsta9500.getPsta9500().reset();                                                                                                                                //Natural: RESET PSTA9500
        //*  RESET PSTA9611                                  /* EM 083016 - START
        pdaPsta9612.getPsta9612().reset();                                                                                                                                //Natural: RESET PSTA9612
        pdaPsta9612.getPsta9612_Systm_Id_Cde().setValue("ADS ");                                                                                                          //Natural: MOVE 'ADS ' TO SYSTM-ID-CDE
        pdaPsta9612.getPsta9612_Pckge_Cde().setValue("PTNAZPRM");                                                                                                         //Natural: MOVE 'PTNAZPRM' TO PCKGE-CDE
        pdaPsta9612.getPsta9612_Prntr_Id_Cde().setValue(" ");                                                                                                             //Natural: MOVE ' ' TO PRNTR-ID-CDE
        pdaPsta9612.getPsta9612_Pckge_Dlvry_Typ().setValue("   ");                                                                                                        //Natural: MOVE '   ' TO PCKGE-DLVRY-TYP
        //*  MOVE ADP-UNIQUE-ID       TO PIN-NBR
        pdaPsta9612.getPsta9612_Univ_Id().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Unique_Id());                                                                       //Natural: MOVE ADP-UNIQUE-ID TO UNIV-ID
        pdaPsta9612.getPsta9612_Univ_Id_Typ().setValue("P");                                                                                                              //Natural: MOVE 'P' TO UNIV-ID-TYP
        pdaPsta9612.getPsta9612_No_Updte_To_Mit_Ind().setValue(true);                                                                                                     //Natural: MOVE TRUE TO NO-UPDTE-TO-MIT-IND
        pdaPsta9612.getPsta9612_Tiaa_Cntrct_Nbr().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Ia_Tiaa_Nbr());                                                             //Natural: MOVE ADP-IA-TIAA-NBR TO TIAA-CNTRCT-NBR
        pdaPsta9612.getPsta9612_Rqst_Log_Dte_Tme().getValue(1).setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Mit_Log_Dte_Tme());                                            //Natural: MOVE ADP-MIT-LOG-DTE-TME TO PSTA9612.RQST-LOG-DTE-TME ( 1 )
        //*  CALLNAT 'PSTN9610'
        //*   PSTA9610
        //*   PSTA9611
        //*   MSG-INFO-SUB
        //*   PARM-EMPLOYEE-INFO-SUB
        //*   PARM-UNIT-INFO-SUB
        //*  EM - 083016 END
        DbsUtil.callnat(Pstn9612.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9612' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Call_Post_For_Close() throws Exception                                                                                                               //Natural: CALL-POST-FOR-CLOSE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        //*  CALLNAT 'PSTN9680'                              /* EM - 083016 START
        //*   PSTA9610
        //*   PSTA9611
        //*   MSG-INFO-SUB
        //*   PARM-EMPLOYEE-INFO-SUB
        //*   PARM-UNIT-INFO-SUB
        //*  EM - 083016 END
        DbsUtil.callnat(Pstn9685.class , getCurrentProcessState(), pdaPsta9610.getPsta9610(), pdaPsta9612.getPsta9612(), pdaCwfpda_M.getMsg_Info_Sub(),                   //Natural: CALLNAT 'PSTN9685' PSTA9610 PSTA9612 MSG-INFO-SUB PARM-EMPLOYEE-INFO-SUB PARM-UNIT-INFO-SUB
            pdaCwfpdaus.getParm_Employee_Info_Sub(), pdaCwfpdaun.getParm_Unit_Info_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals(" ")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NOT = ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR
            sub_Error();
            if (condition(Global.isEscape())) {return;}
            Global.setEscapeCode(EscapeType.Top); if (true) return;                                                                                                       //Natural: ESCAPE TOP
        }                                                                                                                                                                 //Natural: END-IF
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Error() throws Exception                                                                                                                             //Natural: ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        DbsUtil.callnat(Pstn9980.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub());                                                                        //Natural: CALLNAT 'PSTN9980' MSG-INFO-SUB
        if (condition(Global.isEscape())) return;
        getReports().write(0, pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                                 //Natural: WRITE MSG-INFO-SUB.##MSG
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        DbsUtil.terminate(8);  if (true) return;                                                                                                                          //Natural: TERMINATE 8
    }
    private void sub_Call_Ext_Module() throws Exception                                                                                                                   //Natural: CALL-EXT-MODULE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  RESET NECA4000      /* RS0
        //*  NECA4000.FUNCTION-CDE := 'CFN'
        //*  NECA4000.INPT-KEY-OPTION-CDE := '01'  /* BY COMPANY FUND
        //*  NECA4000.CFN-KEY-COMPANY-CDE := '001' /* TIAA-CREF FUNDS ONLY
        //*  CALLNAT 'NECN4000' NECA4000
        //*  IF NECA4000.RETURN-CDE = '00' OR = '  '
        //*  RS0
        DbsUtil.callnat(Adsn888.class , getCurrentProcessState(), pdaAdsa888.getPnd_Parm_Area(), pdaAdsa888.getPnd_Nbr_Acct());                                           //Natural: CALLNAT 'ADSN888' #PARM-AREA #NBR-ACCT
        if (condition(Global.isEscape())) return;
        //*  FOR #X 1 TO 20 /* EM - 101413 START - FIELDS BELOW PASSED  BY ADSN888
        //*   IF #PARM-AREA.#ACCT-ALPHA-FUND-CDE (#X) = ' '
        //*     ESCAPE BOTTOM
        //*   END-IF
        //*   ADD 1 TO PEC-NBR-ACTIVE-ACCT
        //*   PEC-ACCT-STD-ALPHA-CD (#X) := #PARM-AREA.#ACCT-ALPHA-FUND-CDE (#X)
        //*   #TCKR-SYMBL (#X)           := #PARM-AREA.#ACCT-TICKER (#X)    /* RS0
        //*   PEC-ACCT-NAME-10(#X)       := #PARM-AREA.#ACCT-NAME-10(#X)
        //*   IF #PARM-AREA.#ACCT-NAME-10(#X) = ' '
        //*     PEC-ACCT-NAME-10(#X)     := #PARM-AREA.#ACCT-TICKER(#X)
        //*   END-IF
        //*  END-FOR        /* EM - 101413 - END
    }

    //
}
