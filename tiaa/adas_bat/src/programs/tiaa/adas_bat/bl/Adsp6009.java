/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:04:05 PM
**        * FROM NATURAL PROGRAM : Adsp6009
************************************************************
**        * FILE NAME            : Adsp6009.java
**        * CLASS NAME           : Adsp6009
**        * INSTANCE NAME        : Adsp6009
************************************************************
***********************************************************
* ADSP6009
*
* UPDATE CIS PARTICIPANT FILE WITH OIPA VARIABLE AND ADAS FIXED REQUEST
* DATA
*
* 10/03/16   E. MELNIK IA REPLATFORM CHANGES TO WRITE CIS DATA TO
*                      A DATASET.
* 11/14/2017 E. MELNIK IA REPLATFORM PROJECT. REMOVE ZIP FROM CRRSPNDNCE
*                        ADDRESS LINES.  MARKED BY EM - IA1117.
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp6009 extends BLNatBase
{
    // Data Areas
    private LdaAdsl6009 ldaAdsl6009;
    private PdaCisa1000 pdaCisa1000;
    private PdaAdspda_M pdaAdspda_M;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cis_Prtcpnt_File_View;
    private DbsField cis_Prtcpnt_File_View_Cis_Pin_Nbr;
    private DbsField cis_Prtcpnt_File_View_Cis_Tiaa_Doi;
    private DbsField cis_Prtcpnt_File_View_Cis_Rqst_Id_Key;
    private DbsField cis_Prtcpnt_File_View_Cis_Status_Cd;
    private DbsField cis_Prtcpnt_File_View_Cis_Cref_Doi;
    private DbsField cis_Prtcpnt_File_View_Cis_Rqst_Id;
    private DbsField cis_Prtcpnt_File_View_Cis_Mit_Rqst_Log_Dte_Tme;
    private DbsField cis_Prtcpnt_File_View_Cis_Opn_Clsd_Ind;
    private DbsField cis_Prtcpnt_File_View_Cis_Cntrct_Type;
    private DbsField cis_Prtcpnt_File_View_Cis_Cntrct_Print_Dte;
    private DbsField cis_Prtcpnt_File_View_Cis_Appl_Rcvd_Dte;
    private DbsField cis_Prtcpnt_File_View_Cis_Appl_Rcvd_User_Id;
    private DbsField cis_Prtcpnt_File_View_Cis_Appl_Entry_User_Id;
    private DbsField cis_Prtcpnt_File_View_Cis_Annty_Option;
    private DbsField cis_Prtcpnt_File_View_Cis_Pymnt_Mode;
    private DbsField cis_Prtcpnt_File_View_Cis_Annty_Start_Dte;
    private DbsField cis_Prtcpnt_File_View_Cis_Annty_End_Dte;
    private DbsField cis_Prtcpnt_File_View_Cis_Grnted_Period_Yrs;
    private DbsField cis_Prtcpnt_File_View_Cis_Grnted_Period_Dys;
    private DbsField cis_Prtcpnt_File_View_Cis_Scnd_Annt_Prfx;
    private DbsField cis_Prtcpnt_File_View_Cis_Scnd_Annt_Frst_Nme;
    private DbsField cis_Prtcpnt_File_View_Cis_Scnd_Annt_Mid_Nme;
    private DbsField cis_Prtcpnt_File_View_Cis_Scnd_Annt_Lst_Nme;
    private DbsField cis_Prtcpnt_File_View_Cis_Scnd_Annt_Sffx;
    private DbsField cis_Prtcpnt_File_View_Cis_Scnd_Annt_Ssn;
    private DbsField cis_Prtcpnt_File_View_Cis_Scnd_Annt_Dob;
    private DbsField cis_Prtcpnt_File_View_Cis_Scnd_Annt_Sex_Cde;
    private DbsField cis_Prtcpnt_File_View_Cis_Grnted_Grd_Amt;
    private DbsField cis_Prtcpnt_File_View_Cis_Grnted_Std_Amt;
    private DbsField cis_Prtcpnt_File_View_Count_Castcis_Tiaa_Commuted_Info;

    private DbsGroup cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info;
    private DbsField cis_Prtcpnt_File_View_Cis_Comut_Grnted_Amt;
    private DbsField cis_Prtcpnt_File_View_Cis_Comut_Int_Rate;
    private DbsField cis_Prtcpnt_File_View_Cis_Comut_Pymt_Method;
    private DbsField cis_Prtcpnt_File_View_Cis_Grnted_Int_Rate;
    private DbsField cis_Prtcpnt_File_View_Cis_Surv_Redct_Amt;
    private DbsField cis_Prtcpnt_File_View_Count_Castcis_Da_Tiaa_Cntrcts_Rqst;

    private DbsGroup cis_Prtcpnt_File_View_Cis_Da_Tiaa_Cntrcts_Rqst;
    private DbsField cis_Prtcpnt_File_View_Cis_Da_Tiaa_Nbr;
    private DbsField cis_Prtcpnt_File_View_Cis_Da_Tiaa_Proceeds_Amt;
    private DbsField cis_Prtcpnt_File_View_Cis_Da_Rea_Proceeds_Amt;
    private DbsField cis_Prtcpnt_File_View_Count_Castcis_Da_Cref_Cntrcts_Rqst;

    private DbsGroup cis_Prtcpnt_File_View_Cis_Da_Cref_Cntrcts_Rqst;
    private DbsField cis_Prtcpnt_File_View_Cis_Da_Cert_Nbr;
    private DbsField cis_Prtcpnt_File_View_Cis_Da_Cref_Proceeds_Amt;
    private DbsField cis_Prtcpnt_File_View_Count_Castcis_Cref_Annty_Pymnt;

    private DbsGroup cis_Prtcpnt_File_View_Cis_Cref_Annty_Pymnt;
    private DbsField cis_Prtcpnt_File_View_Cis_Cref_Acct_Cde;
    private DbsField cis_Prtcpnt_File_View_Cis_Cref_Mnthly_Nbr_Units;
    private DbsField cis_Prtcpnt_File_View_Cis_Cref_Annual_Nbr_Units;
    private DbsField cis_Prtcpnt_File_View_Cis_Cref_Annty_Amt;
    private DbsField cis_Prtcpnt_File_View_Cis_Rea_Annty_Amt;
    private DbsField cis_Prtcpnt_File_View_Cis_Rea_Surv_Nbr_Units;
    private DbsField cis_Prtcpnt_File_View_Cis_Rea_Mnthly_Nbr_Units;
    private DbsField cis_Prtcpnt_File_View_Cis_Rea_Annual_Nbr_Units;
    private DbsField cis_Prtcpnt_File_View_Cis_Cntrct_Apprvl_Ind;
    private DbsField cis_Prtcpnt_File_View_Cis_Orig_Issue_State;
    private DbsField cis_Prtcpnt_File_View_Cis_Issue_State_Cd;
    private DbsField cis_Prtcpnt_File_View_Cis_Lob;
    private DbsField cis_Prtcpnt_File_View_Cis_Lob_Type;
    private DbsField cis_Prtcpnt_File_View_Cis_Mail_Instructions;
    private DbsField cis_Prtcpnt_File_View_Cis_Pull_Code;

    private DbsGroup cis_Prtcpnt_File_View_Cis_Address_Info;
    private DbsField cis_Prtcpnt_File_View_Cis_Address_Chg_Ind;
    private DbsField cis_Prtcpnt_File_View_Cis_Address_Dest_Name;
    private DbsGroup cis_Prtcpnt_File_View_Cis_Address_TxtMuGroup;
    private DbsField cis_Prtcpnt_File_View_Cis_Address_Txt;
    private DbsField cis_Prtcpnt_File_View_Cis_Zip_Code;
    private DbsField cis_Prtcpnt_File_View_Cis_Bank_Pymnt_Acct_Nmbr;
    private DbsField cis_Prtcpnt_File_View_Cis_Bank_Aba_Acct_Nmbr;
    private DbsField cis_Prtcpnt_File_View_Cis_Stndrd_Trn_Cd;
    private DbsField cis_Prtcpnt_File_View_Cis_Finalist_Reason_Cd;
    private DbsField cis_Prtcpnt_File_View_Cis_Addr_Usage_Code;
    private DbsField cis_Prtcpnt_File_View_Cis_Checking_Saving_Cd;
    private DbsField cis_Prtcpnt_File_View_Cis_Addr_Stndrd_Code;
    private DbsField cis_Prtcpnt_File_View_Cis_Stndrd_Overide;
    private DbsField cis_Prtcpnt_File_View_Cis_Postal_Data_Fields;
    private DbsField cis_Prtcpnt_File_View_Cis_Geographic_Cd;
    private DbsField cis_Prtcpnt_File_View_Cis_Rtb_Pct;
    private DbsField cis_Prtcpnt_File_View_Cis_Rtb_Amt;

    private DbsGroup cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data;
    private DbsField cis_Prtcpnt_File_View_Cis_Rtb_Dest_Pct;
    private DbsField cis_Prtcpnt_File_View_Cis_Rtb_Dest_Amt;
    private DbsField cis_Prtcpnt_File_View_Cis_Rtb_Dest_Nme;
    private DbsGroup cis_Prtcpnt_File_View_Cis_Rtb_Dest_AddrMuGroup;
    private DbsField cis_Prtcpnt_File_View_Cis_Rtb_Dest_Addr;
    private DbsField cis_Prtcpnt_File_View_Cis_Rtb_Bank_Accnt_Nbr;
    private DbsField cis_Prtcpnt_File_View_Cis_Rtb_Trnst_Nbr;
    private DbsField cis_Prtcpnt_File_View_Cis_Rtb_Dest_Zip;
    private DbsField cis_Prtcpnt_File_View_Cis_Rtb_Dest_Type_Cd;
    private DbsField cis_Prtcpnt_File_View_Cis_Trnsf_Flag;
    private DbsField cis_Prtcpnt_File_View_Cis_Trnsf_Cert_Nbr;
    private DbsField cis_Prtcpnt_File_View_Cis_Trnsf_Tiaa_Rea_Nbr;
    private DbsField cis_Prtcpnt_File_View_Cis_Mdo_Traditional_Amt;

    private DbsGroup cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info_2;
    private DbsField cis_Prtcpnt_File_View_Cis_Comut_Grnted_Amt_2;
    private DbsField cis_Prtcpnt_File_View_Cis_Comut_Int_Rate_2;
    private DbsField cis_Prtcpnt_File_View_Cis_Comut_Pymt_Method_2;
    private DbsField cis_Prtcpnt_File_View_Cis_Comut_Mortality_Basis;
    private DbsField cis_Prtcpnt_File_View_Cis_Sg_Text_Udf_1;

    private DbsGroup cis_Prtcpnt_File_View_Cis_Sg_Fund_Identifier;
    private DbsField cis_Prtcpnt_File_View_Cis_Sg_Fund_Ticker;
    private DbsField cis_Prtcpnt_File_View_Cis_Sg_Text_Udf_3;
    private DbsField cis_Prtcpnt_File_View_Cis_Tacc_Annty_Amt;

    private DbsGroup cis_Prtcpnt_File_View_Cis_Tacc_Fund_Info;
    private DbsField cis_Prtcpnt_File_View_Cis_Tacc_Ind;
    private DbsField cis_Prtcpnt_File_View_Cis_Tacc_Account;
    private DbsField cis_Prtcpnt_File_View_Cis_Tacc_Mnthly_Nbr_Units;
    private DbsField cis_Prtcpnt_File_View_Cis_Tacc_Annual_Nbr_Units;
    private DbsField cis_Prtcpnt_File_View_Cis_Institution_Name;
    private DbsField cis_Prtcpnt_File_View_Cis_Four_Fifty_Seven_Ind;
    private DbsField pnd_Ok_Sw;
    private DbsField pnd_Counter;
    private DbsField pnd_Counter_Read;
    private DbsField pnd_Counter_Compress;
    private DbsField pnd_Counter_Start;
    private DbsField pnd_Counter_End;
    private DbsField pnd_Counter_Cis;
    private DbsField pnd_Error_Ind;
    private DbsField pnd_Cntrl_Total;
    private DbsField pnd_Cntrl_Ok;
    private DbsField pnd_A;
    private DbsField pnd_C;
    private DbsField pnd_D;
    private DbsField pnd_X;
    private DbsField pnd_Cis_Super;
    private DbsField pnd_Adc_Super2_From;

    private DbsGroup pnd_Adc_Super2_From__R_Field_1;
    private DbsField pnd_Adc_Super2_From_Pnd_Rqst_Id_F;
    private DbsField pnd_Adc_Super2_From_Pnd_Adc_Sqnce_Nbr_F;
    private DbsField testing_Mode;
    private DbsField pnd_Max_Cis;
    private DbsField pnd_Max_Funds;
    private DbsField pnd_Cnt;
    private DbsField pnd_B;
    private DbsField pnd_Ws_Lob_Nmbr;
    private DbsField pnd_I;
    private DbsField pnd_Indx;
    private DbsField pnd_Zip_Sub;
    private DbsField pnd_Zip_Pos;
    private DbsField pnd_Wrk_Add;
    private DbsField pnd_Desc;
    private DbsField pnd_Type_Call;
    private DbsField pnd_Code;
    private DbsField pnd_Error_Msg;

    private DbsGroup pnd_Totals;
    private DbsField pnd_Totals_Pnd_Totl_Rqsts_Read;
    private DbsField pnd_Totals_Pnd_Totl_Rqsts_Written;
    private DbsField pnd_Totals_Pnd_Totl_Rqst_Errs;
    private DbsField pnd_Rpt_Msg;
    private DbsField pnd_Ia_Mdm_Cntrct;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl6009 = new LdaAdsl6009();
        registerRecord(ldaAdsl6009);
        localVariables = new DbsRecord();
        pdaCisa1000 = new PdaCisa1000(localVariables);
        pdaAdspda_M = new PdaAdspda_M(localVariables);

        // Local Variables

        vw_cis_Prtcpnt_File_View = new DataAccessProgramView(new NameInfo("vw_cis_Prtcpnt_File_View", "CIS-PRTCPNT-FILE-VIEW"), "CIS_PRTCPNT_FILE_12", 
            "CIS_PRTCPNT_FILE", DdmPeriodicGroups.getInstance().getGroups("CIS_PRTCPNT_FILE_12"));
        cis_Prtcpnt_File_View_Cis_Pin_Nbr = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Pin_Nbr", "CIS-PIN-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "CIS_PIN_NBR");
        cis_Prtcpnt_File_View_Cis_Tiaa_Doi = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Tiaa_Doi", "CIS-TIAA-DOI", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_TIAA_DOI");
        cis_Prtcpnt_File_View_Cis_Rqst_Id_Key = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Rqst_Id_Key", "CIS-RQST-ID-KEY", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "CIS_RQST_ID_KEY");
        cis_Prtcpnt_File_View_Cis_Status_Cd = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Status_Cd", "CIS-STATUS-CD", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_STATUS_CD");
        cis_Prtcpnt_File_View_Cis_Cref_Doi = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Cref_Doi", "CIS-CREF-DOI", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_CREF_DOI");
        cis_Prtcpnt_File_View_Cis_Rqst_Id = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Rqst_Id", "CIS-RQST-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CIS_RQST_ID");
        cis_Prtcpnt_File_View_Cis_Mit_Rqst_Log_Dte_Tme = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Mit_Rqst_Log_Dte_Tme", 
            "CIS-MIT-RQST-LOG-DTE-TME", FieldType.STRING, 15, RepeatingFieldStrategy.None, "CIS_MIT_RQST_LOG_DTE_TME");
        cis_Prtcpnt_File_View_Cis_Opn_Clsd_Ind = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Opn_Clsd_Ind", "CIS-OPN-CLSD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_OPN_CLSD_IND");
        cis_Prtcpnt_File_View_Cis_Cntrct_Type = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Cntrct_Type", "CIS-CNTRCT-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_CNTRCT_TYPE");
        cis_Prtcpnt_File_View_Cis_Cntrct_Print_Dte = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Cntrct_Print_Dte", 
            "CIS-CNTRCT-PRINT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CIS_CNTRCT_PRINT_DTE");
        cis_Prtcpnt_File_View_Cis_Appl_Rcvd_Dte = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Appl_Rcvd_Dte", "CIS-APPL-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_APPL_RCVD_DTE");
        cis_Prtcpnt_File_View_Cis_Appl_Rcvd_User_Id = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Appl_Rcvd_User_Id", 
            "CIS-APPL-RCVD-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_APPL_RCVD_USER_ID");
        cis_Prtcpnt_File_View_Cis_Appl_Entry_User_Id = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Appl_Entry_User_Id", 
            "CIS-APPL-ENTRY-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_APPL_ENTRY_USER_ID");
        cis_Prtcpnt_File_View_Cis_Annty_Option = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Annty_Option", "CIS-ANNTY-OPTION", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_ANNTY_OPTION");
        cis_Prtcpnt_File_View_Cis_Pymnt_Mode = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Pymnt_Mode", "CIS-PYMNT-MODE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_PYMNT_MODE");
        cis_Prtcpnt_File_View_Cis_Annty_Start_Dte = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Annty_Start_Dte", 
            "CIS-ANNTY-START-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CIS_ANNTY_START_DTE");
        cis_Prtcpnt_File_View_Cis_Annty_End_Dte = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Annty_End_Dte", "CIS-ANNTY-END-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_ANNTY_END_DTE");
        cis_Prtcpnt_File_View_Cis_Grnted_Period_Yrs = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Grnted_Period_Yrs", 
            "CIS-GRNTED-PERIOD-YRS", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CIS_GRNTED_PERIOD_YRS");
        cis_Prtcpnt_File_View_Cis_Grnted_Period_Dys = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Grnted_Period_Dys", 
            "CIS-GRNTED-PERIOD-DYS", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CIS_GRNTED_PERIOD_DYS");
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Prfx = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Scnd_Annt_Prfx", "CIS-SCND-ANNT-PRFX", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_PRFX");
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Frst_Nme = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Scnd_Annt_Frst_Nme", 
            "CIS-SCND-ANNT-FRST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_FRST_NME");
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Mid_Nme = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Scnd_Annt_Mid_Nme", 
            "CIS-SCND-ANNT-MID-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_MID_NME");
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Lst_Nme = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Scnd_Annt_Lst_Nme", 
            "CIS-SCND-ANNT-LST-NME", FieldType.STRING, 30, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_LST_NME");
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Sffx = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Scnd_Annt_Sffx", "CIS-SCND-ANNT-SFFX", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_SFFX");
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Ssn = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Scnd_Annt_Ssn", "CIS-SCND-ANNT-SSN", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_SSN");
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Dob = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Scnd_Annt_Dob", "CIS-SCND-ANNT-DOB", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_DOB");
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Sex_Cde = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Scnd_Annt_Sex_Cde", 
            "CIS-SCND-ANNT-SEX-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_SCND_ANNT_SEX_CDE");
        cis_Prtcpnt_File_View_Cis_Grnted_Grd_Amt = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Grnted_Grd_Amt", "CIS-GRNTED-GRD-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_GRNTED_GRD_AMT");
        cis_Prtcpnt_File_View_Cis_Grnted_Std_Amt = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Grnted_Std_Amt", "CIS-GRNTED-STD-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_GRNTED_STD_AMT");
        cis_Prtcpnt_File_View_Count_Castcis_Tiaa_Commuted_Info = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Count_Castcis_Tiaa_Commuted_Info", 
            "C*CIS-TIAA-COMMUTED-INFO", RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO");

        cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info = vw_cis_Prtcpnt_File_View.getRecord().newGroupInGroup("cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info", 
            "CIS-TIAA-COMMUTED-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO");
        cis_Prtcpnt_File_View_Cis_Comut_Grnted_Amt = cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Comut_Grnted_Amt", 
            "CIS-COMUT-GRNTED-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_GRNTED_AMT", 
            "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO");
        cis_Prtcpnt_File_View_Cis_Comut_Int_Rate = cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Comut_Int_Rate", 
            "CIS-COMUT-INT-RATE", FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_INT_RATE", 
            "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO");
        cis_Prtcpnt_File_View_Cis_Comut_Pymt_Method = cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Comut_Pymt_Method", 
            "CIS-COMUT-PYMT-METHOD", FieldType.STRING, 8, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_PYMT_METHOD", 
            "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO");
        cis_Prtcpnt_File_View_Cis_Grnted_Int_Rate = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Grnted_Int_Rate", 
            "CIS-GRNTED-INT-RATE", FieldType.NUMERIC, 5, 3, RepeatingFieldStrategy.None, "CIS_GRNTED_INT_RATE");
        cis_Prtcpnt_File_View_Cis_Surv_Redct_Amt = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Surv_Redct_Amt", "CIS-SURV-REDCT-AMT", 
            FieldType.NUMERIC, 5, 2, RepeatingFieldStrategy.None, "CIS_SURV_REDCT_AMT");
        cis_Prtcpnt_File_View_Count_Castcis_Da_Tiaa_Cntrcts_Rqst = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Count_Castcis_Da_Tiaa_Cntrcts_Rqst", 
            "C*CIS-DA-TIAA-CNTRCTS-RQST", RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");

        cis_Prtcpnt_File_View_Cis_Da_Tiaa_Cntrcts_Rqst = vw_cis_Prtcpnt_File_View.getRecord().newGroupInGroup("cis_Prtcpnt_File_View_Cis_Da_Tiaa_Cntrcts_Rqst", 
            "CIS-DA-TIAA-CNTRCTS-RQST", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Prtcpnt_File_View_Cis_Da_Tiaa_Nbr = cis_Prtcpnt_File_View_Cis_Da_Tiaa_Cntrcts_Rqst.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Da_Tiaa_Nbr", 
            "CIS-DA-TIAA-NBR", FieldType.STRING, 10, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_TIAA_NBR", 
            "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Prtcpnt_File_View_Cis_Da_Tiaa_Proceeds_Amt = cis_Prtcpnt_File_View_Cis_Da_Tiaa_Cntrcts_Rqst.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Da_Tiaa_Proceeds_Amt", 
            "CIS-DA-TIAA-PROCEEDS-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_TIAA_PROCEEDS_AMT", 
            "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Prtcpnt_File_View_Cis_Da_Rea_Proceeds_Amt = cis_Prtcpnt_File_View_Cis_Da_Tiaa_Cntrcts_Rqst.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Da_Rea_Proceeds_Amt", 
            "CIS-DA-REA-PROCEEDS-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_REA_PROCEEDS_AMT", 
            "CIS_PRTCPNT_FILE_CIS_DA_TIAA_CNTRCTS_RQST");
        cis_Prtcpnt_File_View_Count_Castcis_Da_Cref_Cntrcts_Rqst = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Count_Castcis_Da_Cref_Cntrcts_Rqst", 
            "C*CIS-DA-CREF-CNTRCTS-RQST", RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_DA_CREF_CNTRCTS_RQST");

        cis_Prtcpnt_File_View_Cis_Da_Cref_Cntrcts_Rqst = vw_cis_Prtcpnt_File_View.getRecord().newGroupInGroup("cis_Prtcpnt_File_View_Cis_Da_Cref_Cntrcts_Rqst", 
            "CIS-DA-CREF-CNTRCTS-RQST", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_DA_CREF_CNTRCTS_RQST");
        cis_Prtcpnt_File_View_Cis_Da_Cert_Nbr = cis_Prtcpnt_File_View_Cis_Da_Cref_Cntrcts_Rqst.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Da_Cert_Nbr", 
            "CIS-DA-CERT-NBR", FieldType.STRING, 10, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_CERT_NBR", 
            "CIS_PRTCPNT_FILE_CIS_DA_CREF_CNTRCTS_RQST");
        cis_Prtcpnt_File_View_Cis_Da_Cref_Proceeds_Amt = cis_Prtcpnt_File_View_Cis_Da_Cref_Cntrcts_Rqst.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Da_Cref_Proceeds_Amt", 
            "CIS-DA-CREF-PROCEEDS-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_DA_CREF_PROCEEDS_AMT", 
            "CIS_PRTCPNT_FILE_CIS_DA_CREF_CNTRCTS_RQST");
        cis_Prtcpnt_File_View_Count_Castcis_Cref_Annty_Pymnt = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Count_Castcis_Cref_Annty_Pymnt", 
            "C*CIS-CREF-ANNTY-PYMNT", RepeatingFieldStrategy.CAsteriskVariable, "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");

        cis_Prtcpnt_File_View_Cis_Cref_Annty_Pymnt = vw_cis_Prtcpnt_File_View.getRecord().newGroupInGroup("cis_Prtcpnt_File_View_Cis_Cref_Annty_Pymnt", 
            "CIS-CREF-ANNTY-PYMNT", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Prtcpnt_File_View_Cis_Cref_Acct_Cde = cis_Prtcpnt_File_View_Cis_Cref_Annty_Pymnt.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Cref_Acct_Cde", 
            "CIS-CREF-ACCT-CDE", FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CREF_ACCT_CDE", 
            "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Prtcpnt_File_View_Cis_Cref_Mnthly_Nbr_Units = cis_Prtcpnt_File_View_Cis_Cref_Annty_Pymnt.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Cref_Mnthly_Nbr_Units", 
            "CIS-CREF-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CREF_MNTHLY_NBR_UNITS", 
            "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Prtcpnt_File_View_Cis_Cref_Annual_Nbr_Units = cis_Prtcpnt_File_View_Cis_Cref_Annty_Pymnt.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Cref_Annual_Nbr_Units", 
            "CIS-CREF-ANNUAL-NBR-UNITS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CREF_ANNUAL_NBR_UNITS", 
            "CIS_PRTCPNT_FILE_CIS_CREF_ANNTY_PYMNT");
        cis_Prtcpnt_File_View_Cis_Cref_Annty_Amt = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Cref_Annty_Amt", "CIS-CREF-ANNTY-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_CREF_ANNTY_AMT");
        cis_Prtcpnt_File_View_Cis_Rea_Annty_Amt = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Rea_Annty_Amt", "CIS-REA-ANNTY-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_REA_ANNTY_AMT");
        cis_Prtcpnt_File_View_Cis_Rea_Surv_Nbr_Units = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Rea_Surv_Nbr_Units", 
            "CIS-REA-SURV-NBR-UNITS", FieldType.NUMERIC, 11, 3, RepeatingFieldStrategy.None, "CIS_REA_SURV_NBR_UNITS");
        cis_Prtcpnt_File_View_Cis_Rea_Mnthly_Nbr_Units = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Rea_Mnthly_Nbr_Units", 
            "CIS-REA-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 11, 3, RepeatingFieldStrategy.None, "CIS_REA_MNTHLY_NBR_UNITS");
        cis_Prtcpnt_File_View_Cis_Rea_Annual_Nbr_Units = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Rea_Annual_Nbr_Units", 
            "CIS-REA-ANNUAL-NBR-UNITS", FieldType.NUMERIC, 11, 3, RepeatingFieldStrategy.None, "CIS_REA_ANNUAL_NBR_UNITS");
        cis_Prtcpnt_File_View_Cis_Cntrct_Apprvl_Ind = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Cntrct_Apprvl_Ind", 
            "CIS-CNTRCT-APPRVL-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_CNTRCT_APPRVL_IND");
        cis_Prtcpnt_File_View_Cis_Orig_Issue_State = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Orig_Issue_State", 
            "CIS-ORIG-ISSUE-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_ORIG_ISSUE_STATE");
        cis_Prtcpnt_File_View_Cis_Issue_State_Cd = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Issue_State_Cd", "CIS-ISSUE-STATE-CD", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_ISSUE_STATE_CD");
        cis_Prtcpnt_File_View_Cis_Lob = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Lob", "CIS-LOB", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CIS_LOB");
        cis_Prtcpnt_File_View_Cis_Lob_Type = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Lob_Type", "CIS-LOB-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_LOB_TYPE");
        cis_Prtcpnt_File_View_Cis_Mail_Instructions = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Mail_Instructions", 
            "CIS-MAIL-INSTRUCTIONS", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_MAIL_INSTRUCTIONS");
        cis_Prtcpnt_File_View_Cis_Pull_Code = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Pull_Code", "CIS-PULL-CODE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CIS_PULL_CODE");

        cis_Prtcpnt_File_View_Cis_Address_Info = vw_cis_Prtcpnt_File_View.getRecord().newGroupInGroup("cis_Prtcpnt_File_View_Cis_Address_Info", "CIS-ADDRESS-INFO", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Address_Chg_Ind = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Address_Chg_Ind", 
            "CIS-ADDRESS-CHG-IND", FieldType.STRING, 1, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_ADDRESS_CHG_IND", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Address_Dest_Name = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Address_Dest_Name", 
            "CIS-ADDRESS-DEST-NAME", FieldType.STRING, 35, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_ADDRESS_DEST_NAME", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Address_TxtMuGroup = cis_Prtcpnt_File_View_Cis_Address_Info.newGroupInGroup("CIS_PRTCPNT_FILE_VIEW_CIS_ADDRESS_TXTMuGroup", 
            "CIS_ADDRESS_TXTMuGroup", RepeatingFieldStrategy.PeriodicGroupSubTableFieldArray, "CIS_PRTCPNT_FILE_CIS_ADDRESS_TXT", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Address_Txt = cis_Prtcpnt_File_View_Cis_Address_TxtMuGroup.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Address_Txt", 
            "CIS-ADDRESS-TXT", FieldType.STRING, 35, new DbsArrayController(1, 3, 1, 5) , RepeatingFieldStrategy.SubTableFieldArray, "CIS_ADDRESS_TXT", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Zip_Code = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Zip_Code", "CIS-ZIP-CODE", 
            FieldType.STRING, 9, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_ZIP_CODE", "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Bank_Pymnt_Acct_Nmbr = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Bank_Pymnt_Acct_Nmbr", 
            "CIS-BANK-PYMNT-ACCT-NMBR", FieldType.STRING, 21, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_BANK_PYMNT_ACCT_NMBR", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Bank_Aba_Acct_Nmbr = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Bank_Aba_Acct_Nmbr", 
            "CIS-BANK-ABA-ACCT-NMBR", FieldType.STRING, 9, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_BANK_ABA_ACCT_NMBR", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Stndrd_Trn_Cd = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Stndrd_Trn_Cd", 
            "CIS-STNDRD-TRN-CD", FieldType.STRING, 2, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_STNDRD_TRN_CD", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Finalist_Reason_Cd = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Finalist_Reason_Cd", 
            "CIS-FINALIST-REASON-CD", FieldType.STRING, 10, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_FINALIST_REASON_CD", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Addr_Usage_Code = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Addr_Usage_Code", 
            "CIS-ADDR-USAGE-CODE", FieldType.STRING, 1, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_ADDR_USAGE_CODE", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Checking_Saving_Cd = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Checking_Saving_Cd", 
            "CIS-CHECKING-SAVING-CD", FieldType.STRING, 1, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CHECKING_SAVING_CD", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Addr_Stndrd_Code = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Addr_Stndrd_Code", 
            "CIS-ADDR-STNDRD-CODE", FieldType.STRING, 1, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_ADDR_STNDRD_CODE", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Stndrd_Overide = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Stndrd_Overide", 
            "CIS-STNDRD-OVERIDE", FieldType.STRING, 1, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_STNDRD_OVERIDE", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Postal_Data_Fields = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Postal_Data_Fields", 
            "CIS-POSTAL-DATA-FIELDS", FieldType.STRING, 44, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_POSTAL_DATA_FIELDS", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Geographic_Cd = cis_Prtcpnt_File_View_Cis_Address_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Geographic_Cd", 
            "CIS-GEOGRAPHIC-CD", FieldType.STRING, 2, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_GEOGRAPHIC_CD", 
            "CIS_PRTCPNT_FILE_CIS_ADDRESS_INFO");
        cis_Prtcpnt_File_View_Cis_Rtb_Pct = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Pct", "CIS-RTB-PCT", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CIS_RTB_PCT");
        cis_Prtcpnt_File_View_Cis_Rtb_Amt = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Amt", "CIS-RTB-AMT", FieldType.NUMERIC, 
            11, 2, RepeatingFieldStrategy.None, "CIS_RTB_AMT");

        cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data = vw_cis_Prtcpnt_File_View.getRecord().newGroupInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data", "CIS-RTB-DEST-DATA", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_View_Cis_Rtb_Dest_Pct = cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Dest_Pct", 
            "CIS-RTB-DEST-PCT", FieldType.NUMERIC, 3, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_DEST_PCT", 
            "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_View_Cis_Rtb_Dest_Amt = cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Dest_Amt", 
            "CIS-RTB-DEST-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_DEST_AMT", 
            "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_View_Cis_Rtb_Dest_Nme = cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Dest_Nme", 
            "CIS-RTB-DEST-NME", FieldType.STRING, 35, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_DEST_NME", 
            "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_View_Cis_Rtb_Dest_AddrMuGroup = cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data.newGroupInGroup("CIS_PRTCPNT_FILE_VIEW_CIS_RTB_DEST_ADDRMuGroup", 
            "CIS_RTB_DEST_ADDRMuGroup", RepeatingFieldStrategy.PeriodicGroupSubTableFieldArray, "CIS_PRTCPNT_FILE_CIS_RTB_DEST_ADDR", "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_View_Cis_Rtb_Dest_Addr = cis_Prtcpnt_File_View_Cis_Rtb_Dest_AddrMuGroup.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Dest_Addr", 
            "CIS-RTB-DEST-ADDR", FieldType.STRING, 35, new DbsArrayController(1, 3, 1, 5) , RepeatingFieldStrategy.SubTableFieldArray, "CIS_RTB_DEST_ADDR", 
            "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_View_Cis_Rtb_Bank_Accnt_Nbr = cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Bank_Accnt_Nbr", 
            "CIS-RTB-BANK-ACCNT-NBR", FieldType.STRING, 21, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_BANK_ACCNT_NBR", 
            "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_View_Cis_Rtb_Trnst_Nbr = cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Trnst_Nbr", 
            "CIS-RTB-TRNST-NBR", FieldType.NUMERIC, 9, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_TRNST_NBR", 
            "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_View_Cis_Rtb_Dest_Zip = cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Dest_Zip", 
            "CIS-RTB-DEST-ZIP", FieldType.STRING, 9, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_DEST_ZIP", 
            "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_View_Cis_Rtb_Dest_Type_Cd = cis_Prtcpnt_File_View_Cis_Rtb_Dest_Data.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Rtb_Dest_Type_Cd", 
            "CIS-RTB-DEST-TYPE-CD", FieldType.STRING, 1, new DbsArrayController(1, 3) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_RTB_DEST_TYPE_CD", 
            "CIS_PRTCPNT_FILE_CIS_RTB_DEST_DATA");
        cis_Prtcpnt_File_View_Cis_Trnsf_Flag = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Trnsf_Flag", "CIS-TRNSF-FLAG", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_TRNSF_FLAG");
        cis_Prtcpnt_File_View_Cis_Trnsf_Cert_Nbr = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Trnsf_Cert_Nbr", "CIS-TRNSF-CERT-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_TRNSF_CERT_NBR");
        cis_Prtcpnt_File_View_Cis_Trnsf_Tiaa_Rea_Nbr = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Trnsf_Tiaa_Rea_Nbr", 
            "CIS-TRNSF-TIAA-REA-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_TRNSF_TIAA_REA_NBR");
        cis_Prtcpnt_File_View_Cis_Mdo_Traditional_Amt = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Mdo_Traditional_Amt", 
            "CIS-MDO-TRADITIONAL-AMT", FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_MDO_TRADITIONAL_AMT");

        cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info_2 = vw_cis_Prtcpnt_File_View.getRecord().newGroupInGroup("cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info_2", 
            "CIS-TIAA-COMMUTED-INFO-2", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_File_View_Cis_Comut_Grnted_Amt_2 = cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info_2.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Comut_Grnted_Amt_2", 
            "CIS-COMUT-GRNTED-AMT-2", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_GRNTED_AMT_2", 
            "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_File_View_Cis_Comut_Int_Rate_2 = cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info_2.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Comut_Int_Rate_2", 
            "CIS-COMUT-INT-RATE-2", FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_INT_RATE_2", 
            "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_File_View_Cis_Comut_Pymt_Method_2 = cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info_2.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Comut_Pymt_Method_2", 
            "CIS-COMUT-PYMT-METHOD-2", FieldType.STRING, 8, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_PYMT_METHOD_2", 
            "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_File_View_Cis_Comut_Mortality_Basis = cis_Prtcpnt_File_View_Cis_Tiaa_Commuted_Info_2.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Comut_Mortality_Basis", 
            "CIS-COMUT-MORTALITY-BASIS", FieldType.STRING, 30, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_COMUT_MORTALITY_BASIS", 
            "CIS_PRTCPNT_FILE_CIS_TIAA_COMMUTED_INFO_2");
        cis_Prtcpnt_File_View_Cis_Sg_Text_Udf_1 = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Sg_Text_Udf_1", "CIS-SG-TEXT-UDF-1", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_SG_TEXT_UDF_1");

        cis_Prtcpnt_File_View_Cis_Sg_Fund_Identifier = vw_cis_Prtcpnt_File_View.getRecord().newGroupInGroup("cis_Prtcpnt_File_View_Cis_Sg_Fund_Identifier", 
            "CIS-SG-FUND-IDENTIFIER", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_SG_FUND_IDENTIFIER");
        cis_Prtcpnt_File_View_Cis_Sg_Fund_Ticker = cis_Prtcpnt_File_View_Cis_Sg_Fund_Identifier.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Sg_Fund_Ticker", 
            "CIS-SG-FUND-TICKER", FieldType.STRING, 10, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_SG_FUND_TICKER", 
            "CIS_PRTCPNT_FILE_CIS_SG_FUND_IDENTIFIER");
        cis_Prtcpnt_File_View_Cis_Sg_Text_Udf_3 = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Sg_Text_Udf_3", "CIS-SG-TEXT-UDF-3", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CIS_SG_TEXT_UDF_3");
        cis_Prtcpnt_File_View_Cis_Tacc_Annty_Amt = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Tacc_Annty_Amt", "CIS-TACC-ANNTY-AMT", 
            FieldType.NUMERIC, 11, 2, RepeatingFieldStrategy.None, "CIS_TACC_ANNTY_AMT");

        cis_Prtcpnt_File_View_Cis_Tacc_Fund_Info = vw_cis_Prtcpnt_File_View.getRecord().newGroupInGroup("cis_Prtcpnt_File_View_Cis_Tacc_Fund_Info", "CIS-TACC-FUND-INFO", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Prtcpnt_File_View_Cis_Tacc_Ind = cis_Prtcpnt_File_View_Cis_Tacc_Fund_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Tacc_Ind", "CIS-TACC-IND", 
            FieldType.STRING, 4, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TACC_IND", "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Prtcpnt_File_View_Cis_Tacc_Account = cis_Prtcpnt_File_View_Cis_Tacc_Fund_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Tacc_Account", 
            "CIS-TACC-ACCOUNT", FieldType.STRING, 10, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TACC_ACCOUNT", 
            "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Prtcpnt_File_View_Cis_Tacc_Mnthly_Nbr_Units = cis_Prtcpnt_File_View_Cis_Tacc_Fund_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Tacc_Mnthly_Nbr_Units", 
            "CIS-TACC-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TACC_MNTHLY_NBR_UNITS", 
            "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Prtcpnt_File_View_Cis_Tacc_Annual_Nbr_Units = cis_Prtcpnt_File_View_Cis_Tacc_Fund_Info.newFieldArrayInGroup("cis_Prtcpnt_File_View_Cis_Tacc_Annual_Nbr_Units", 
            "CIS-TACC-ANNUAL-NBR-UNITS", FieldType.NUMERIC, 11, 3, new DbsArrayController(1, 100) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_TACC_ANNUAL_NBR_UNITS", 
            "CIS_PRTCPNT_FILE_CIS_TACC_FUND_INFO");
        cis_Prtcpnt_File_View_Cis_Institution_Name = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Institution_Name", 
            "CIS-INSTITUTION-NAME", FieldType.STRING, 72, RepeatingFieldStrategy.None, "CIS_INSTITUTION_NAME");
        cis_Prtcpnt_File_View_Cis_Four_Fifty_Seven_Ind = vw_cis_Prtcpnt_File_View.getRecord().newFieldInGroup("cis_Prtcpnt_File_View_Cis_Four_Fifty_Seven_Ind", 
            "CIS-FOUR-FIFTY-SEVEN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_FOUR_FIFTY_SEVEN_IND");
        registerRecord(vw_cis_Prtcpnt_File_View);

        pnd_Ok_Sw = localVariables.newFieldInRecord("pnd_Ok_Sw", "#OK-SW", FieldType.STRING, 1);
        pnd_Counter = localVariables.newFieldInRecord("pnd_Counter", "#COUNTER", FieldType.NUMERIC, 3);
        pnd_Counter_Read = localVariables.newFieldInRecord("pnd_Counter_Read", "#COUNTER-READ", FieldType.NUMERIC, 3);
        pnd_Counter_Compress = localVariables.newFieldInRecord("pnd_Counter_Compress", "#COUNTER-COMPRESS", FieldType.NUMERIC, 3);
        pnd_Counter_Start = localVariables.newFieldInRecord("pnd_Counter_Start", "#COUNTER-START", FieldType.NUMERIC, 3);
        pnd_Counter_End = localVariables.newFieldInRecord("pnd_Counter_End", "#COUNTER-END", FieldType.NUMERIC, 3);
        pnd_Counter_Cis = localVariables.newFieldInRecord("pnd_Counter_Cis", "#COUNTER-CIS", FieldType.NUMERIC, 3);
        pnd_Error_Ind = localVariables.newFieldInRecord("pnd_Error_Ind", "#ERROR-IND", FieldType.STRING, 1);
        pnd_Cntrl_Total = localVariables.newFieldInRecord("pnd_Cntrl_Total", "#CNTRL-TOTAL", FieldType.NUMERIC, 7);
        pnd_Cntrl_Ok = localVariables.newFieldInRecord("pnd_Cntrl_Ok", "#CNTRL-OK", FieldType.STRING, 1);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.PACKED_DECIMAL, 3);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.PACKED_DECIMAL, 3);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);
        pnd_Cis_Super = localVariables.newFieldInRecord("pnd_Cis_Super", "#CIS-SUPER", FieldType.STRING, 35);
        pnd_Adc_Super2_From = localVariables.newFieldInRecord("pnd_Adc_Super2_From", "#ADC-SUPER2-FROM", FieldType.STRING, 37);

        pnd_Adc_Super2_From__R_Field_1 = localVariables.newGroupInRecord("pnd_Adc_Super2_From__R_Field_1", "REDEFINE", pnd_Adc_Super2_From);
        pnd_Adc_Super2_From_Pnd_Rqst_Id_F = pnd_Adc_Super2_From__R_Field_1.newFieldInGroup("pnd_Adc_Super2_From_Pnd_Rqst_Id_F", "#RQST-ID-F", FieldType.STRING, 
            35);
        pnd_Adc_Super2_From_Pnd_Adc_Sqnce_Nbr_F = pnd_Adc_Super2_From__R_Field_1.newFieldInGroup("pnd_Adc_Super2_From_Pnd_Adc_Sqnce_Nbr_F", "#ADC-SQNCE-NBR-F", 
            FieldType.NUMERIC, 2);
        testing_Mode = localVariables.newFieldInRecord("testing_Mode", "TESTING-MODE", FieldType.BOOLEAN, 1);
        pnd_Max_Cis = localVariables.newFieldInRecord("pnd_Max_Cis", "#MAX-CIS", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Funds = localVariables.newFieldInRecord("pnd_Max_Funds", "#MAX-FUNDS", FieldType.PACKED_DECIMAL, 3);
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Lob_Nmbr = localVariables.newFieldInRecord("pnd_Ws_Lob_Nmbr", "#WS-LOB-NMBR", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Indx = localVariables.newFieldInRecord("pnd_Indx", "#INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Zip_Sub = localVariables.newFieldInRecord("pnd_Zip_Sub", "#ZIP-SUB", FieldType.STRING, 5);
        pnd_Zip_Pos = localVariables.newFieldInRecord("pnd_Zip_Pos", "#ZIP-POS", FieldType.PACKED_DECIMAL, 3);
        pnd_Wrk_Add = localVariables.newFieldInRecord("pnd_Wrk_Add", "#WRK-ADD", FieldType.STRING, 35);
        pnd_Desc = localVariables.newFieldInRecord("pnd_Desc", "#DESC", FieldType.STRING, 20);
        pnd_Type_Call = localVariables.newFieldInRecord("pnd_Type_Call", "#TYPE-CALL", FieldType.STRING, 1);
        pnd_Code = localVariables.newFieldInRecord("pnd_Code", "#CODE", FieldType.STRING, 2);
        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 70);

        pnd_Totals = localVariables.newGroupInRecord("pnd_Totals", "#TOTALS");
        pnd_Totals_Pnd_Totl_Rqsts_Read = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Totl_Rqsts_Read", "#TOTL-RQSTS-READ", FieldType.NUMERIC, 9);
        pnd_Totals_Pnd_Totl_Rqsts_Written = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Totl_Rqsts_Written", "#TOTL-RQSTS-WRITTEN", FieldType.NUMERIC, 
            9);
        pnd_Totals_Pnd_Totl_Rqst_Errs = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Totl_Rqst_Errs", "#TOTL-RQST-ERRS", FieldType.NUMERIC, 9);
        pnd_Rpt_Msg = localVariables.newFieldInRecord("pnd_Rpt_Msg", "#RPT-MSG", FieldType.STRING, 70);
        pnd_Ia_Mdm_Cntrct = localVariables.newFieldInRecord("pnd_Ia_Mdm_Cntrct", "#IA-MDM-CNTRCT", FieldType.STRING, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cis_Prtcpnt_File_View.reset();

        ldaAdsl6009.initializeValues();

        localVariables.reset();
        pnd_Max_Cis.setInitialValue(20);
        pnd_Max_Funds.setInitialValue(100);
        pnd_Error_Msg.setInitialValue("CIS Participant Record not found");
        pnd_Ia_Mdm_Cntrct.setInitialValue("MDM");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp6009() throws Exception
    {
        super("Adsp6009");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("ADSP6009", onError);
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 1 ) PS = 60 LS = 132 ZP = OFF SG = OFF;//Natural: FORMAT ( 2 ) PS = 60 LS = 132 ZP = OFF SG = OFF;//Natural: WRITE ( 1 ) TITLE LEFT JUSTIFIED 001T 'ADSP6009' 050T 'ADAS ERROR REQUESTS ' 110T 'Run Date:' ( BLI ) 120T *DATX ( AD = OD CD = NE ) / 050T '  EXCEPTION REPORT ' 110T 'Page No :' 120T *PAGE-NUMBER ( AD = OD CD = NE ZP = ON ) / 050T '====================' // 02T 'PIN' 11X ' PART ' 04X 'DA CNTRCT' 03X 'IA CNTRCT' 02X 'RQST' / 01T 'NUMBER' 08X ' NUMBER ' 04X 'NUMBR' 07X 'NUMBR' 05X 'ID' // //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT JUSTIFIED 001T 'ADSP6009' 050T 'ADAS WRITTEN REQUESTS ' 110T 'Run Date:' ( BLI ) 120T *DATX ( AD = OD CD = NE ) / 050T '============================= ' 110T 'Page No :' 120T *PAGE-NUMBER ( AD = OD CD = NE ZP = ON ) // 02T 'PIN' 11X ' PART ' 04X 'DA CNTRCT' 03X 'IA CNTRCT' 02X 'RQST' / 01T 'NUMBER' 08X ' NUMBER ' 04X 'NUMBR' 07X 'NUMBR' 05X 'ID' // //
        testing_Mode.setValue(false);                                                                                                                                     //Natural: ASSIGN TESTING-MODE := FALSE
        if (condition(testing_Mode.getBoolean()))                                                                                                                         //Natural: IF TESTING-MODE
        {
            getReports().write(0, "PROG: ",Global.getPROGRAM());                                                                                                          //Natural: WRITE '=' *PROGRAM
            if (Global.isEscape()) return;
            getReports().write(0, "LIB: ",Global.getLIBRARY_ID());                                                                                                        //Natural: WRITE '=' *LIBRARY-ID
            if (Global.isEscape()) return;
            getReports().write(0, "DATU: ",Global.getDATU());                                                                                                             //Natural: WRITE '=' *DATU
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 ADSL6009
        while (condition(getWorkFiles().read(1, ldaAdsl6009.getAdsl6009())))
        {
            pnd_Totals_Pnd_Totl_Rqsts_Read.nadd(1);                                                                                                                       //Natural: ADD 1 TO #TOTL-RQSTS-READ
            pnd_Cis_Super.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Entry_Date_Time_Key());                                                                                //Natural: MOVE ADSL6009.#CIS-ENTRY-DATE-TIME-KEY TO #CIS-SUPER
            vw_cis_Prtcpnt_File_View.startDatabaseFind                                                                                                                    //Natural: FIND CIS-PRTCPNT-FILE-VIEW WITH CIS-RQST-ID-KEY = #CIS-SUPER
            (
            "CIS",
            new Wc[] { new Wc("CIS_RQST_ID_KEY", "=", pnd_Cis_Super, WcType.WITH) }
            );
            CIS:
            while (condition(vw_cis_Prtcpnt_File_View.readNextRow("CIS", true)))
            {
                vw_cis_Prtcpnt_File_View.setIfNotFoundControlFlag(false);
                if (condition(vw_cis_Prtcpnt_File_View.getAstCOUNTER().equals(0)))                                                                                        //Natural: IF NO RECORD FOUND
                {
                    getReports().write(0, "DID NOT FIND CIS PRTCPNT RECORD FOR RQST"," ",ldaAdsl6009.getAdsl6009_Pnd_Cis_Entry_Date_Time_Key());                          //Natural: WRITE 'DID NOT FIND CIS PRTCPNT RECORD FOR RQST' ' ' ADSL6009.#CIS-ENTRY-DATE-TIME-KEY
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("CIS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Error_Ind.setValue("Y");                                                                                                                          //Natural: MOVE 'Y' TO #ERROR-IND
                    pnd_Totals_Pnd_Totl_Rqst_Errs.nadd(1);                                                                                                                //Natural: ADD 1 TO #TOTL-RQST-ERRS
                    pnd_Rpt_Msg.setValue(pnd_Error_Msg);                                                                                                                  //Natural: MOVE #ERROR-MSG TO #RPT-MSG
                                                                                                                                                                          //Natural: PERFORM WRITE-EXCEPTION
                    sub_Write_Exception();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("CIS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
                                                                                                                                                                          //Natural: PERFORM MOVE-CIS-PARTICIPANT-FILE-FIELDS
                sub_Move_Cis_Participant_File_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("CIS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    ADD 1 TO #TOTAL-PROCESSED
                //* ** MOVE 'C' TO ADS-PRTCPNT-VIEW.ADP-OPN-CLSD-IND
                //* ** IF TESTING-MODE
                //* **   WRITE ' CIS-GRNTED-GRD-AMT BEFORE UPDT   ' #CIS-GRNTED-GRD-AMT
                //* **   WRITE ' CIS-GRNTED-STD-AMT BEFORE UPDT   ' #CIS-GRNTED-STD-AMT
                //* **   WRITE ' CIS-REA-ANNTY-AMT  BEFORE UPDT   ' #CIS-REA-ANNTY-AMT
                //* **   WRITE ' CIS-CREF-ANNTY-AMT BEFORE UPDT   ' #CIS-CREF-ANNTY-AMT
                //* ** END-IF
                vw_cis_Prtcpnt_File_View.updateDBRow("CIS");                                                                                                              //Natural: UPDATE ( CIS. )
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM MOVE-CISN1000-FIELDS
                sub_Move_Cisn1000_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("CIS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "BEFORE FUNCTION ",pdaCisa1000.getCisa1000_Pnd_Cis_Msg_Number().getValue("*"));                                                     //Natural: WRITE 'BEFORE FUNCTION ' #CIS-MSG-NUMBER ( * )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("CIS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.callnat(Cisn1000.class , getCurrentProcessState(), pdaCisa1000.getCisa1000(), pdaCisa1000.getPnd_Cis_Misc());                                     //Natural: CALLNAT 'CISN1000' USING CISA1000 #CIS-MISC
                if (condition(Global.isEscape())) return;
                getReports().write(0, "MESSAGE FROM CISN1000 ",pdaCisa1000.getCisa1000_Pnd_Cis_Msg_Text());                                                               //Natural: WRITE 'MESSAGE FROM CISN1000 ' #CIS-MSG-TEXT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("CIS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "Error from  CISN1000  ",pdaCisa1000.getCisa1000_Pnd_Cis_Msg_Number().getValue("*"));                                               //Natural: WRITE 'Error from  CISN1000  ' #CIS-MSG-NUMBER ( * )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("CIS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pdaCisa1000.getCisa1000_Pnd_Cis_Msg_Text().notEquals(" ")))                                                                                 //Natural: IF #CIS-MSG-TEXT NE ' '
                {
                    pnd_Totals_Pnd_Totl_Rqst_Errs.nadd(1);                                                                                                                //Natural: ADD 1 TO #TOTL-RQST-ERRS
                    pnd_Rpt_Msg.setValue(pdaCisa1000.getCisa1000_Pnd_Cis_Msg_Text());                                                                                     //Natural: MOVE #CIS-MSG-TEXT TO #RPT-MSG
                                                                                                                                                                          //Natural: PERFORM WRITE-EXCEPTION
                    sub_Write_Exception();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("CIS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Totals_Pnd_Totl_Rqsts_Written.nadd(1);                                                                                                            //Natural: ADD 1 TO #TOTL-RQSTS-WRITTEN
                                                                                                                                                                          //Natural: PERFORM WRITE-PROCESSED
                    sub_Write_Processed();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("CIS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("CIS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,NEWLINE,"  TOTAL EXCEPTIONS   ",pnd_Totals_Pnd_Totl_Rqst_Errs);                                                                     //Natural: WRITE ( 1 ) // '  TOTAL EXCEPTIONS   ' #TOTL-RQST-ERRS
        if (Global.isEscape()) return;
        getReports().write(2, NEWLINE,NEWLINE,"  TOTAL PROCESSED ",pnd_Totals_Pnd_Totl_Rqsts_Written);                                                                    //Natural: WRITE ( 2 ) // '  TOTAL PROCESSED ' #TOTL-RQSTS-WRITTEN
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM EOJ-PROCESS
        sub_Eoj_Process();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //* ***********************************************************************
        //* **     MOVE CIS PARTICIPANT FILE DATA                               ***
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-CIS-PARTICIPANT-FILE-FIELDS
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REMOVE-ZIP-FROM-CRRSPNDNCE-ADDR
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-COUNTRY-NAME
        //* ***********************************************************************
        //* ***********************************************************************
        //* **     MOVE CISN1000 DATA                                           ***
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-CISN1000-FIELDS
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-EXCEPTION
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PROCESSED
        //* *********************************************************************
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EOJ-PROCESS
        //* ***********************************************************************
        //*                    O N   E R R O R   R O U T I N E
        //* ***********************************************************************
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Move_Cis_Participant_File_Fields() throws Exception                                                                                                  //Natural: MOVE-CIS-PARTICIPANT-FILE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        cis_Prtcpnt_File_View_Cis_Trnsf_Flag.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Trnsf_Flag());                                                                      //Natural: MOVE #CIS-TRNSF-FLAG TO CIS-TRNSF-FLAG
        cis_Prtcpnt_File_View_Cis_Sg_Text_Udf_1.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Sg_Text_Udf_1());                                                                //Natural: MOVE #CIS-SG-TEXT-UDF-1 TO CIS-SG-TEXT-UDF-1
        cis_Prtcpnt_File_View_Cis_Sg_Text_Udf_3.setValue(pnd_Ia_Mdm_Cntrct);                                                                                              //Natural: MOVE #IA-MDM-CNTRCT TO CIS-SG-TEXT-UDF-3
        cis_Prtcpnt_File_View_Cis_Cntrct_Print_Dte.setValueEdited(new ReportEditMask("YYMMDD"),ldaAdsl6009.getAdsl6009_Pnd_Cis_Cntrct_Print_Dte());                       //Natural: MOVE EDITED #CIS-CNTRCT-PRINT-DTE TO CIS-CNTRCT-PRINT-DTE ( EM = YYMMDD )
        cis_Prtcpnt_File_View_Cis_Annty_Option.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Annty_Option());                                                                  //Natural: MOVE #CIS-ANNTY-OPTION TO CIS-ANNTY-OPTION
        cis_Prtcpnt_File_View_Cis_Appl_Rcvd_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaAdsl6009.getAdsl6009_Pnd_Cis_Appl_Rcvd_Dte());                           //Natural: MOVE EDITED #CIS-APPL-RCVD-DTE TO CIS-APPL-RCVD-DTE ( EM = YYYYMMDD )
        cis_Prtcpnt_File_View_Cis_Appl_Rcvd_User_Id.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Appl_Rcvd_User_Id());                                                        //Natural: MOVE #CIS-APPL-RCVD-USER-ID TO CIS-APPL-RCVD-USER-ID
        cis_Prtcpnt_File_View_Cis_Pymnt_Mode.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Pymnt_Mode());                                                                      //Natural: MOVE #CIS-PYMNT-MODE TO CIS-PYMNT-MODE
        cis_Prtcpnt_File_View_Cis_Annty_Start_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaAdsl6009.getAdsl6009_Pnd_Cis_Annty_Start_Dte());                       //Natural: MOVE EDITED #CIS-ANNTY-START-DTE TO CIS-ANNTY-START-DTE ( EM = YYYYMMDD )
        cis_Prtcpnt_File_View_Cis_Grnted_Period_Yrs.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Grnted_Period_Yrs());                                                        //Natural: MOVE #CIS-GRNTED-PERIOD-YRS TO CIS-GRNTED-PERIOD-YRS
        cis_Prtcpnt_File_View_Cis_Grnted_Period_Dys.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Grnted_Period_Dys());                                                        //Natural: MOVE #CIS-GRNTED-PERIOD-DYS TO CIS-GRNTED-PERIOD-DYS
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Frst_Nme.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Scnd_Annt_Frst_Nme());                                                      //Natural: MOVE #CIS-SCND-ANNT-FRST-NME TO CIS-SCND-ANNT-FRST-NME
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Mid_Nme.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Scnd_Annt_Mid_Nme());                                                        //Natural: MOVE #CIS-SCND-ANNT-MID-NME TO CIS-SCND-ANNT-MID-NME
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Lst_Nme.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Scnd_Annt_Lst_Nme());                                                        //Natural: MOVE #CIS-SCND-ANNT-LST-NME TO CIS-SCND-ANNT-LST-NME
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Sffx.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Scnd_Annt_Sffx());                                                              //Natural: MOVE #CIS-SCND-ANNT-SFFX TO CIS-SCND-ANNT-SFFX
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Ssn.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Scnd_Annt_Ssn());                                                                //Natural: MOVE #CIS-SCND-ANNT-SSN TO CIS-SCND-ANNT-SSN
        if (condition(ldaAdsl6009.getAdsl6009_Pnd_Cis_Scnd_Annt_Dob().notEquals(" ")))                                                                                    //Natural: IF #CIS-SCND-ANNT-DOB NE ' '
        {
            cis_Prtcpnt_File_View_Cis_Scnd_Annt_Dob.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaAdsl6009.getAdsl6009_Pnd_Cis_Scnd_Annt_Dob());                       //Natural: MOVE EDITED #CIS-SCND-ANNT-DOB TO CIS-SCND-ANNT-DOB ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        cis_Prtcpnt_File_View_Cis_Scnd_Annt_Sex_Cde.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Scnd_Annt_Sex_Cde());                                                        //Natural: MOVE #CIS-SCND-ANNT-SEX-CDE TO CIS-SCND-ANNT-SEX-CDE
        cis_Prtcpnt_File_View_Cis_Orig_Issue_State.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Orig_Issue_State());                                                          //Natural: MOVE #CIS-ORIG-ISSUE-STATE TO CIS-ORIG-ISSUE-STATE
        cis_Prtcpnt_File_View_Cis_Issue_State_Cd.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Issue_State_Cd());                                                              //Natural: MOVE #CIS-ISSUE-STATE-CD TO CIS-ISSUE-STATE-CD
        cis_Prtcpnt_File_View_Cis_Cntrct_Apprvl_Ind.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Cntrct_Apprvl_Ind());                                                        //Natural: MOVE ADSL6009.#CIS-CNTRCT-APPRVL-IND TO CIS-CNTRCT-APPRVL-IND
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 2
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(2)); pnd_I.nadd(1))
        {
            //*  EM - IA1117 START
            if (condition(pnd_I.equals(1) && ldaAdsl6009.getAdsl6009_Pnd_Cis_Zip_Code().getValue(pnd_I).notEquals(" ")))                                                  //Natural: IF #I = 1 AND #CIS-ZIP-CODE ( #I ) NE ' '
            {
                                                                                                                                                                          //Natural: PERFORM REMOVE-ZIP-FROM-CRRSPNDNCE-ADDR
                sub_Remove_Zip_From_Crrspndnce_Addr();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                FOR02:                                                                                                                                                    //Natural: FOR #A = 1 TO 5
                for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(5)); pnd_A.nadd(1))
                {
                    cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(pnd_I,pnd_A).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Address_Txt().getValue(pnd_I,                    //Natural: MOVE #CIS-ADDRESS-TXT ( #I,#A ) TO CIS-ADDRESS-TXT ( #I,#A )
                        pnd_A));
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  EM - IA1117 END
            }                                                                                                                                                             //Natural: END-IF
            cis_Prtcpnt_File_View_Cis_Addr_Usage_Code.getValue(pnd_I).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Addr_Usage_Code().getValue(pnd_I));                        //Natural: MOVE #CIS-ADDR-USAGE-CODE ( #I ) TO CIS-ADDR-USAGE-CODE ( #I )
            cis_Prtcpnt_File_View_Cis_Zip_Code.getValue(pnd_I).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Zip_Code().getValue(pnd_I));                                      //Natural: MOVE #CIS-ZIP-CODE ( #I ) TO CIS-ZIP-CODE ( #I )
            cis_Prtcpnt_File_View_Cis_Address_Chg_Ind.getValue(pnd_I).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Address_Chg_Ind().getValue(pnd_I));                        //Natural: MOVE #CIS-ADDRESS-CHG-IND ( #I ) TO CIS-ADDRESS-CHG-IND ( #I )
            cis_Prtcpnt_File_View_Cis_Address_Dest_Name.getValue(pnd_I).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Address_Dest_Name().getValue(pnd_I));                    //Natural: MOVE #CIS-ADDRESS-DEST-NAME ( #I ) TO CIS-ADDRESS-DEST-NAME ( #I )
            cis_Prtcpnt_File_View_Cis_Bank_Pymnt_Acct_Nmbr.getValue(pnd_I).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Bank_Pymnt_Acct_Nmbr().getValue(pnd_I));              //Natural: MOVE #CIS-BANK-PYMNT-ACCT-NMBR ( #I ) TO CIS-BANK-PYMNT-ACCT-NMBR ( #I )
            cis_Prtcpnt_File_View_Cis_Bank_Aba_Acct_Nmbr.getValue(pnd_I).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Bank_Aba_Acct_Nmbr().getValue(pnd_I));                  //Natural: MOVE #CIS-BANK-ABA-ACCT-NMBR ( #I ) TO CIS-BANK-ABA-ACCT-NMBR ( #I )
            cis_Prtcpnt_File_View_Cis_Checking_Saving_Cd.getValue(pnd_I).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Checking_Saving_Cd().getValue(pnd_I));                  //Natural: MOVE #CIS-CHECKING-SAVING-CD ( #I ) TO CIS-CHECKING-SAVING-CD ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        cis_Prtcpnt_File_View_Cis_Mail_Instructions.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Mail_Instructions());                                                        //Natural: MOVE #CIS-MAIL-INSTRUCTIONS TO CIS-MAIL-INSTRUCTIONS
        cis_Prtcpnt_File_View_Cis_Pull_Code.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Pull_Code());                                                                        //Natural: MOVE #CIS-PULL-CODE TO CIS-PULL-CODE
        cis_Prtcpnt_File_View_Cis_Four_Fifty_Seven_Ind.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Four_Fifty_Seven_Ind());                                                  //Natural: MOVE #CIS-FOUR-FIFTY-SEVEN-IND TO CIS-FOUR-FIFTY-SEVEN-IND
        cis_Prtcpnt_File_View_Cis_Institution_Name.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Institution_Name());                                                          //Natural: MOVE #CIS-INSTITUTION-NAME TO CIS-INSTITUTION-NAME
        pnd_Counter_Cis.setValue(0);                                                                                                                                      //Natural: MOVE 0 TO #COUNTER-CIS
        pnd_Counter_Cis.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #COUNTER-CIS
        cis_Prtcpnt_File_View_Cis_Da_Tiaa_Nbr.getValue(pnd_Counter_Cis).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Da_Tiaa_Nbr());                                          //Natural: MOVE #CIS-DA-TIAA-NBR TO CIS-DA-TIAA-NBR ( #COUNTER-CIS )
        cis_Prtcpnt_File_View_Cis_Da_Rea_Proceeds_Amt.getValue(pnd_Counter_Cis).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Da_Rea_Proceeds_Amt());                          //Natural: MOVE #CIS-DA-REA-PROCEEDS-AMT TO CIS-DA-REA-PROCEEDS-AMT ( #COUNTER-CIS )
        cis_Prtcpnt_File_View_Cis_Da_Tiaa_Proceeds_Amt.getValue(pnd_Counter_Cis).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Da_Tiaa_Proceeds_Amt());                        //Natural: MOVE #CIS-DA-TIAA-PROCEEDS-AMT TO CIS-DA-TIAA-PROCEEDS-AMT ( #COUNTER-CIS )
        cis_Prtcpnt_File_View_Cis_Da_Cert_Nbr.getValue(pnd_Counter_Cis).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Da_Cert_Nbr());                                          //Natural: MOVE #CIS-DA-CERT-NBR TO CIS-DA-CERT-NBR ( #COUNTER-CIS )
        cis_Prtcpnt_File_View_Cis_Da_Cref_Proceeds_Amt.getValue(pnd_Counter_Cis).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Da_Cref_Proceeds_Amt());                        //Natural: MOVE #CIS-DA-CREF-PROCEEDS-AMT TO CIS-DA-CREF-PROCEEDS-AMT ( #COUNTER-CIS )
        cis_Prtcpnt_File_View_Cis_Tiaa_Doi.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaAdsl6009.getAdsl6009_Pnd_Cis_Tiaa_Doi());                                     //Natural: MOVE EDITED #CIS-TIAA-DOI TO CIS-TIAA-DOI ( EM = YYYYMMDD )
        if (condition(ldaAdsl6009.getAdsl6009_Pnd_Cis_Cref_Doi().notEquals(" ")))                                                                                         //Natural: IF #CIS-CREF-DOI NE ' '
        {
            cis_Prtcpnt_File_View_Cis_Cref_Doi.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaAdsl6009.getAdsl6009_Pnd_Cis_Cref_Doi());                                 //Natural: MOVE EDITED #CIS-CREF-DOI TO CIS-CREF-DOI ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        cis_Prtcpnt_File_View_Cis_Cntrct_Type.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Cntrct_Type1());                                                                   //Natural: MOVE #CIS-CNTRCT-TYPE1 TO CIS-CNTRCT-TYPE
        if (condition(ldaAdsl6009.getAdsl6009_Pnd_Cis_Annty_End_Dte().notEquals(" ")))                                                                                    //Natural: IF #CIS-ANNTY-END-DTE NE ' '
        {
            cis_Prtcpnt_File_View_Cis_Annty_End_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaAdsl6009.getAdsl6009_Pnd_Cis_Annty_End_Dte());                       //Natural: MOVE EDITED #CIS-ANNTY-END-DTE TO CIS-ANNTY-END-DTE ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        cis_Prtcpnt_File_View_Cis_Grnted_Grd_Amt.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Grnted_Grd_Amt());                                                              //Natural: MOVE #CIS-GRNTED-GRD-AMT TO CIS-GRNTED-GRD-AMT
        cis_Prtcpnt_File_View_Cis_Grnted_Std_Amt.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Grnted_Std_Amt());                                                              //Natural: MOVE #CIS-GRNTED-STD-AMT TO CIS-GRNTED-STD-AMT
        ldaAdsl6009.getAdsl6009_Pnd_Cis_Mdo_Traditional_Amt().setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Mdo_Traditional_Amt());                                            //Natural: MOVE #CIS-MDO-TRADITIONAL-AMT TO #CIS-MDO-TRADITIONAL-AMT
        cis_Prtcpnt_File_View_Cis_Comut_Grnted_Amt.getValue("*").setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Comut_Grnted_Amt().getValue("*"));                              //Natural: MOVE #CIS-COMUT-GRNTED-AMT ( * ) TO CIS-COMUT-GRNTED-AMT ( * )
        cis_Prtcpnt_File_View_Cis_Comut_Int_Rate.getValue("*").setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Comut_Int_Rate().getValue("*"));                                  //Natural: MOVE #CIS-COMUT-INT-RATE ( * ) TO CIS-COMUT-INT-RATE ( * )
        cis_Prtcpnt_File_View_Cis_Comut_Pymt_Method.getValue("*").setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Comut_Pymt_Method().getValue("*"));                            //Natural: MOVE #CIS-COMUT-PYMT-METHOD ( * ) TO CIS-COMUT-PYMT-METHOD ( * )
        cis_Prtcpnt_File_View_Cis_Lob_Type.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Lob_Type());                                                                          //Natural: MOVE #CIS-LOB-TYPE TO CIS-LOB-TYPE
        cis_Prtcpnt_File_View_Cis_Lob.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Lob());                                                                                    //Natural: MOVE #CIS-LOB TO CIS-LOB
        cis_Prtcpnt_File_View_Cis_Comut_Grnted_Amt_2.getValue("*").setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Comut_Grnted_Amt_2().getValue("*"));                          //Natural: MOVE #CIS-COMUT-GRNTED-AMT-2 ( * ) TO CIS-COMUT-GRNTED-AMT-2 ( * )
        cis_Prtcpnt_File_View_Cis_Comut_Pymt_Method_2.getValue("*").setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Comut_Pymt_Method_2().getValue("*"));                        //Natural: MOVE #CIS-COMUT-PYMT-METHOD-2 ( * ) TO CIS-COMUT-PYMT-METHOD-2 ( * )
        cis_Prtcpnt_File_View_Cis_Comut_Int_Rate_2.getValue("*").setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Comut_Int_Rate_2().getValue("*"));                              //Natural: MOVE #CIS-COMUT-INT-RATE-2 ( * ) TO CIS-COMUT-INT-RATE-2 ( * )
        cis_Prtcpnt_File_View_Cis_Comut_Mortality_Basis.getValue("*").setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Comut_Mortality_Basis().getValue("*"));                    //Natural: MOVE #CIS-COMUT-MORTALITY-BASIS ( * ) TO CIS-COMUT-MORTALITY-BASIS ( * )
        cis_Prtcpnt_File_View_Cis_Rea_Annual_Nbr_Units.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Rea_Annual_Nbr_Units());                                                  //Natural: MOVE #CIS-REA-ANNUAL-NBR-UNITS TO CIS-REA-ANNUAL-NBR-UNITS
        cis_Prtcpnt_File_View_Cis_Rea_Mnthly_Nbr_Units.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Rea_Mnthly_Nbr_Units());                                                  //Natural: MOVE #CIS-REA-MNTHLY-NBR-UNITS TO CIS-REA-MNTHLY-NBR-UNITS
        cis_Prtcpnt_File_View_Cis_Rea_Annty_Amt.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Rea_Annl_Annty_Amt());                                                           //Natural: MOVE #CIS-REA-ANNL-ANNTY-AMT TO CIS-REA-ANNTY-AMT
        cis_Prtcpnt_File_View_Cis_Tacc_Annual_Nbr_Units.getValue(1).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Tacc_Annual_Nbr_Units());                                    //Natural: MOVE #CIS-TACC-ANNUAL-NBR-UNITS TO CIS-TACC-ANNUAL-NBR-UNITS ( 1 )
        cis_Prtcpnt_File_View_Cis_Tacc_Mnthly_Nbr_Units.getValue(1).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Tacc_Mnthly_Nbr_Units());                                    //Natural: MOVE #CIS-TACC-MNTHLY-NBR-UNITS TO CIS-TACC-MNTHLY-NBR-UNITS ( 1 )
        cis_Prtcpnt_File_View_Cis_Tacc_Annty_Amt.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Tacc_Annl_Annty_Amt());                                                         //Natural: MOVE #CIS-TACC-ANNL-ANNTY-AMT TO CIS-TACC-ANNTY-AMT
        cis_Prtcpnt_File_View_Cis_Tacc_Ind.getValue(1).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Tacc_Ind());                                                              //Natural: MOVE #CIS-TACC-IND TO CIS-TACC-IND ( 1 )
        cis_Prtcpnt_File_View_Cis_Tacc_Account.getValue(1).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Tacc_Account());                                                      //Natural: MOVE #CIS-TACC-ACCOUNT TO CIS-TACC-ACCOUNT ( 1 )
        cis_Prtcpnt_File_View_Cis_Cref_Mnthly_Nbr_Units.getValue("*").setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Cref_Mnthly_Nbr_Units().getValue("*"));                    //Natural: MOVE #CIS-CREF-MNTHLY-NBR-UNITS ( * ) TO CIS-CREF-MNTHLY-NBR-UNITS ( * )
        cis_Prtcpnt_File_View_Cis_Cref_Annual_Nbr_Units.getValue("*").setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Cref_Annual_Nbr_Units().getValue("*"));                    //Natural: MOVE #CIS-CREF-ANNUAL-NBR-UNITS ( * ) TO CIS-CREF-ANNUAL-NBR-UNITS ( * )
        cis_Prtcpnt_File_View_Cis_Cref_Acct_Cde.getValue("*").setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Cref_Acct_Cde().getValue("*"));                                    //Natural: MOVE #CIS-CREF-ACCT-CDE ( * ) TO CIS-CREF-ACCT-CDE ( * )
        cis_Prtcpnt_File_View_Cis_Cref_Annty_Amt.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Cref_Annl_Annty_Amt());                                                         //Natural: MOVE #CIS-CREF-ANNL-ANNTY-AMT TO CIS-CREF-ANNTY-AMT
        cis_Prtcpnt_File_View_Cis_Sg_Fund_Ticker.getValue(1,":",20).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Sg_Fund_Ticker().getValue("*"));                             //Natural: MOVE #CIS-SG-FUND-TICKER ( * ) TO CIS-SG-FUND-TICKER ( 1:20 )
    }
    //*  EM - IA1117 START
    private void sub_Remove_Zip_From_Crrspndnce_Addr() throws Exception                                                                                                   //Natural: REMOVE-ZIP-FROM-CRRSPNDNCE-ADDR
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(pnd_I,"*").reset();                                                                                                //Natural: RESET CIS-ADDRESS-TXT ( #I,* )
        FOR03:                                                                                                                                                            //Natural: FOR #X 1 TO 5
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(5)); pnd_X.nadd(1))
        {
            cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(pnd_I,pnd_X).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Address_Txt().getValue(pnd_I,pnd_X));                    //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,#X ) := #CIS-ADDRESS-TXT ( #I,#X )
            if (condition(pnd_X.less(5)))                                                                                                                                 //Natural: IF #X LT 5
            {
                if (condition(ldaAdsl6009.getAdsl6009_Pnd_Cis_Address_Txt().getValue(pnd_I,pnd_X.getDec().add(1)).equals(" ")))                                           //Natural: IF #CIS-ADDRESS-TXT ( #I, #X + 1 ) = ' '
                {
                    pnd_Zip_Sub.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Zip_Code().getValue(pnd_I));                                                                     //Natural: ASSIGN #ZIP-SUB := #CIS-ZIP-CODE ( #I )
                    DbsUtil.examine(new ExamineSource(ldaAdsl6009.getAdsl6009_Pnd_Cis_Address_Txt().getValue(pnd_I,pnd_X),true), new ExamineSearch(pnd_Zip_Sub),          //Natural: EXAMINE FULL #CIS-ADDRESS-TXT ( #I,#X ) FOR #ZIP-SUB GIVING POSITION #ZIP-POS
                        new ExamineGivingPosition(pnd_Zip_Pos));
                    short decideConditionsMet659 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ZIP-POS GT 1
                    if (condition(pnd_Zip_Pos.greater(1)))
                    {
                        decideConditionsMet659++;
                        pnd_Zip_Pos.nsubtract(1);                                                                                                                         //Natural: SUBTRACT 1 FROM #ZIP-POS
                        //*  MAKE SURE THIS IS NOT THE ADDRESS NUMBER
                        pnd_Wrk_Add.reset();                                                                                                                              //Natural: RESET #WRK-ADD
                        pnd_Wrk_Add.setValue(cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(pnd_I,pnd_X).getSubstring(1,pnd_Zip_Pos.getInt()));                           //Natural: ASSIGN #WRK-ADD := SUBSTR ( CIS-ADDRESS-TXT ( #I,#X ) ,1,#ZIP-POS )
                        ldaAdsl6009.getAdsl6009_Pnd_Cis_Address_Txt().getValue(pnd_I,pnd_X).setValue(pnd_Wrk_Add);                                                        //Natural: ASSIGN #CIS-ADDRESS-TXT ( #I,#X ) := CIS-ADDRESS-TXT ( #I,#X ) := #WRK-ADD
                        cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(pnd_I,pnd_X).setValue(pnd_Wrk_Add);
                    }                                                                                                                                                     //Natural: WHEN #ZIP-POS = 1
                    else if (condition(pnd_Zip_Pos.equals(1)))
                    {
                        decideConditionsMet659++;
                        pnd_Zip_Pos.nadd(5);                                                                                                                              //Natural: ADD 5 TO #ZIP-POS
                        if (condition(cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(pnd_I,pnd_X).getSubstring(pnd_Zip_Pos.getInt(),10).equals(" ")))                     //Natural: IF SUBSTR ( CIS-ADDRESS-TXT ( #I,#X ) ,#ZIP-POS,10 ) EQ ' '
                        {
                            cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(pnd_I,pnd_X).reset();                                                                          //Natural: RESET CIS-ADDRESS-TXT ( #I,#X ) #CIS-ADDRESS-TXT ( #I,#X )
                            ldaAdsl6009.getAdsl6009_Pnd_Cis_Address_Txt().getValue(pnd_I,pnd_X).reset();
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaAdsl6009.getAdsl6009_Pnd_Cis_Address_Txt().getValue(pnd_I,pnd_X).notEquals(" ")))                                                        //Natural: IF #CIS-ADDRESS-TXT ( #I,#X ) NE ' '
                {
                    pnd_Zip_Sub.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Zip_Code().getValue(pnd_I));                                                                     //Natural: ASSIGN #ZIP-SUB := #CIS-ZIP-CODE ( #I )
                    DbsUtil.examine(new ExamineSource(ldaAdsl6009.getAdsl6009_Pnd_Cis_Address_Txt().getValue(pnd_I,pnd_X),true), new ExamineSearch(pnd_Zip_Sub),          //Natural: EXAMINE FULL #CIS-ADDRESS-TXT ( #I,#X ) FOR #ZIP-SUB GIVING POSITION #ZIP-POS
                        new ExamineGivingPosition(pnd_Zip_Pos));
                    short decideConditionsMet680 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ZIP-POS GT 1
                    if (condition(pnd_Zip_Pos.greater(1)))
                    {
                        decideConditionsMet680++;
                        pnd_Zip_Pos.nsubtract(1);                                                                                                                         //Natural: SUBTRACT 1 FROM #ZIP-POS
                        //*  MAKE SURE THIS IS NOT THE ADDRESS NUMBER
                        pnd_Wrk_Add.reset();                                                                                                                              //Natural: RESET #WRK-ADD
                        pnd_Wrk_Add.setValue(cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(pnd_I,pnd_X).getSubstring(1,pnd_Zip_Pos.getInt()));                           //Natural: ASSIGN #WRK-ADD := SUBSTR ( CIS-ADDRESS-TXT ( #I,#X ) ,1,#ZIP-POS )
                        ldaAdsl6009.getAdsl6009_Pnd_Cis_Address_Txt().getValue(pnd_I,pnd_X).setValue(pnd_Wrk_Add);                                                        //Natural: ASSIGN #CIS-ADDRESS-TXT ( #I,#X ) := CIS-ADDRESS-TXT ( #I,#X ) := #WRK-ADD
                        cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(pnd_I,pnd_X).setValue(pnd_Wrk_Add);
                    }                                                                                                                                                     //Natural: WHEN #ZIP-POS = 1
                    else if (condition(pnd_Zip_Pos.equals(1)))
                    {
                        decideConditionsMet680++;
                        pnd_Zip_Pos.nadd(5);                                                                                                                              //Natural: ADD 5 TO #ZIP-POS
                        if (condition(cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(pnd_I,pnd_X).getSubstring(pnd_Zip_Pos.getInt(),10).equals(" ")))                     //Natural: IF SUBSTR ( CIS-ADDRESS-TXT ( #I,#X ) ,#ZIP-POS,10 ) EQ ' '
                        {
                            cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(pnd_I,pnd_X).reset();                                                                          //Natural: RESET CIS-ADDRESS-TXT ( #I,#X ) #CIS-ADDRESS-TXT ( #I,#X )
                            ldaAdsl6009.getAdsl6009_Pnd_Cis_Address_Txt().getValue(pnd_I,pnd_X).reset();
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(ldaAdsl6009.getAdsl6009_Pnd_Cis_Zip_Code().getValue(pnd_I).equals("CANAD") || ldaAdsl6009.getAdsl6009_Pnd_Cis_Zip_Code().getValue(pnd_I).equals("FORGN"))) //Natural: IF #CIS-ZIP-CODE ( #I ) = 'CANAD' OR = 'FORGN'
        {
                                                                                                                                                                          //Natural: PERFORM GET-COUNTRY-NAME
            sub_Get_Country_Name();
            if (condition(Global.isEscape())) {return;}
            FOR04:                                                                                                                                                        //Natural: FOR #X 1 TO 5
            for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(5)); pnd_X.nadd(1))
            {
                if (condition(cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(pnd_I,pnd_X).notEquals(" ") &&cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(pnd_I           //Natural: IF CIS-ADDRESS-TXT ( #I,#X ) NE ' ' AND CIS-ADDRESS-TXT ( #I,#X ) = SCAN ( #DESC )
                    ,pnd_X).contains (pnd_Desc)))
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(pnd_I,pnd_X).equals(" ")))                                                                   //Natural: IF CIS-ADDRESS-TXT ( #I,#X ) = ' '
                {
                    cis_Prtcpnt_File_View_Cis_Address_Txt.getValue(pnd_I,pnd_X).setValue(pnd_Desc);                                                                       //Natural: ASSIGN CIS-ADDRESS-TXT ( #I,#X ) := #DESC
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Country_Name() throws Exception                                                                                                                  //Natural: GET-COUNTRY-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Desc.reset();                                                                                                                                                 //Natural: RESET #DESC MSG-INFO-SUB
        pdaAdspda_M.getMsg_Info_Sub().reset();
        pnd_Type_Call.setValue("R");                                                                                                                                      //Natural: ASSIGN #TYPE-CALL := 'R'
        pnd_Code.setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Issue_State_Cd());                                                                                              //Natural: ASSIGN #CODE := #CIS-ISSUE-STATE-CD
        DbsUtil.callnat(Adsn035.class , getCurrentProcessState(), pdaAdspda_M.getMsg_Info_Sub(), pnd_Type_Call, pnd_Code, pnd_Desc);                                      //Natural: CALLNAT 'ADSN035' MSG-INFO-SUB #TYPE-CALL #CODE #DESC
        if (condition(Global.isEscape())) return;
        DbsUtil.examine(new ExamineSource(pnd_Desc), new ExamineTranslate(TranslateOption.Upper));                                                                        //Natural: EXAMINE #DESC AND TRANSLATE INTO UPPER CASE
        //*  EM - IA1117 END
    }
    private void sub_Move_Cisn1000_Fields() throws Exception                                                                                                              //Natural: MOVE-CISN1000-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        pdaCisa1000.getCisa1000_Pnd_Cis_Entry_Date_Time_Key().setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Entry_Date_Time_Key());                                            //Natural: MOVE ADSL6009.#CIS-ENTRY-DATE-TIME-KEY TO CISA1000.#CIS-ENTRY-DATE-TIME-KEY
        pdaCisa1000.getCisa1000_Pnd_Cis_Requestor().setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Requestor());                                                                //Natural: MOVE ADSL6009.#CIS-REQUESTOR TO CISA1000.#CIS-REQUESTOR
        pdaCisa1000.getCisa1000_Pnd_Cis_Pin_Number().compute(new ComputeParameters(false, pdaCisa1000.getCisa1000_Pnd_Cis_Pin_Number()), ldaAdsl6009.getAdsl6009_Pnd_Cis_Pin_Number().val()); //Natural: ASSIGN CISA1000.#CIS-PIN-NUMBER := VAL ( ADSL6009.#CIS-PIN-NUMBER )
        pdaCisa1000.getCisa1000_Pnd_Cis_Soc_Sec_Number().setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Soc_Sec_Number());                                                      //Natural: MOVE ADSL6009.#CIS-SOC-SEC-NUMBER TO CISA1000.#CIS-SOC-SEC-NUMBER
        if (condition(ldaAdsl6009.getAdsl6009_Pnd_Cis_Dob().notEquals(" ")))                                                                                              //Natural: IF ADSL6009.#CIS-DOB NE ' '
        {
            pdaCisa1000.getCisa1000_Pnd_Cis_Dob().setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Dob_N());                                                                      //Natural: MOVE ADSL6009.#CIS-DOB-N TO CISA1000.#CIS-DOB
        }                                                                                                                                                                 //Natural: END-IF
        pdaCisa1000.getCisa1000_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue(1).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Tiaa_Cntrct_Nbr());                                        //Natural: MOVE ADSL6009.#CIS-TIAA-CNTRCT-NBR TO CISA1000.#CIS-TIAA-CNTRCT-NBR ( 1 )
        pdaCisa1000.getCisa1000_Pnd_Cis_Cref_Cntrct_Nbr().setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Cref_Cntrct_Nbr());                                                    //Natural: MOVE ADSL6009.#CIS-CREF-CNTRCT-NBR TO CISA1000.#CIS-CREF-CNTRCT-NBR
        pdaCisa1000.getCisa1000_Pnd_Cis_Rqst_Log_Dte_Tme().setValue(cis_Prtcpnt_File_View_Cis_Mit_Rqst_Log_Dte_Tme);                                                      //Natural: MOVE CIS-PRTCPNT-FILE-VIEW.CIS-MIT-RQST-LOG-DTE-TME TO CISA1000.#CIS-RQST-LOG-DTE-TME
        pdaCisa1000.getCisa1000_Pnd_Cis_Assign_Issue_Cntrct_Ind().setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Assign_Issue_Cntrct_Ind());                                    //Natural: MOVE ADSL6009.#CIS-ASSIGN-ISSUE-CNTRCT-IND TO CISA1000.#CIS-ASSIGN-ISSUE-CNTRCT-IND
        pdaCisa1000.getCisa1000_Pnd_Cis_Product_Cde().setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Product_Cde());                                                            //Natural: MOVE ADSL6009.#CIS-PRODUCT-CDE TO CISA1000.#CIS-PRODUCT-CDE
        pdaCisa1000.getCisa1000_Pnd_Cis_Cntrct_Type().setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Cntrct_Type());                                                            //Natural: MOVE ADSL6009.#CIS-CNTRCT-TYPE TO CISA1000.#CIS-CNTRCT-TYPE
        pdaCisa1000.getCisa1000_Pnd_Cis_Msg_Text().setValue(" ");                                                                                                         //Natural: MOVE ' ' TO #CIS-MSG-TEXT
        pdaCisa1000.getCisa1000_Pnd_Cis_Msg_Number().getValue("*").setValue(" ");                                                                                         //Natural: MOVE ' ' TO #CIS-MSG-NUMBER ( * )
        pdaCisa1000.getCisa1000_Pnd_Cis_Functions().getValue(6).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Functions());                                                    //Natural: MOVE ADSL6009.#CIS-FUNCTIONS TO CISA1000.#CIS-FUNCTIONS ( 6 ) CISA1000.#CIS-FUNCTIONS ( 4 ) CISA1000.#CIS-FUNCTIONS ( 9 )
        pdaCisa1000.getCisa1000_Pnd_Cis_Functions().getValue(4).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Functions());
        pdaCisa1000.getCisa1000_Pnd_Cis_Functions().getValue(9).setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Functions());
        pdaCisa1000.getCisa1000_Pnd_Cis_Mit_Function_Code().setValue(ldaAdsl6009.getAdsl6009_Pnd_Cis_Mit_Function_Code());                                                //Natural: MOVE ADSL6009.#CIS-MIT-FUNCTION-CODE TO CISA1000.#CIS-MIT-FUNCTION-CODE
    }
    private void sub_Write_Exception() throws Exception                                                                                                                   //Natural: WRITE-EXCEPTION
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(getReports().getAstLinesLeft(1).less(5)))                                                                                                           //Natural: NEWPAGE ( 1 ) WHEN LESS THAN 5 LINES
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        getReports().write(1, ldaAdsl6009.getAdsl6009_Pnd_Cis_Pin_Number(),new ColumnSpacing(1),ldaAdsl6009.getAdsl6009_Pnd_Cis_Soc_Sec_Number(),new ColumnSpacing(3),ldaAdsl6009.getAdsl6009_Pnd_Cis_Da_Tiaa_Nbr(),new  //Natural: WRITE ( 1 ) ADSL6009.#CIS-PIN-NUMBER 1X ADSL6009.#CIS-SOC-SEC-NUMBER 3X ADSL6009.#CIS-DA-TIAA-NBR 2X ADSL6009.#CIS-TIAA-CNTRCT-NBR ADSL6009.#CIS-ENTRY-DATE-TIME-KEY / 23X #RPT-MSG
            ColumnSpacing(2),ldaAdsl6009.getAdsl6009_Pnd_Cis_Tiaa_Cntrct_Nbr(),ldaAdsl6009.getAdsl6009_Pnd_Cis_Entry_Date_Time_Key(),NEWLINE,new ColumnSpacing(23),
            pnd_Rpt_Msg);
        if (Global.isEscape()) return;
    }
    private void sub_Write_Processed() throws Exception                                                                                                                   //Natural: WRITE-PROCESSED
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(getReports().getAstLinesLeft(2).less(5)))                                                                                                           //Natural: NEWPAGE ( 2 ) WHEN LESS THAN 5 LINES
        {
            getReports().newPage(2);
            if (condition(Global.isEscape())){return;}
        }
        getReports().write(2, ldaAdsl6009.getAdsl6009_Pnd_Cis_Pin_Number(),new ColumnSpacing(1),ldaAdsl6009.getAdsl6009_Pnd_Cis_Soc_Sec_Number(),new ColumnSpacing(3),ldaAdsl6009.getAdsl6009_Pnd_Cis_Da_Tiaa_Nbr(),new  //Natural: WRITE ( 2 ) ADSL6009.#CIS-PIN-NUMBER 1X ADSL6009.#CIS-SOC-SEC-NUMBER 3X ADSL6009.#CIS-DA-TIAA-NBR 2X ADSL6009.#CIS-TIAA-CNTRCT-NBR ADSL6009.#CIS-ENTRY-DATE-TIME-KEY
            ColumnSpacing(2),ldaAdsl6009.getAdsl6009_Pnd_Cis_Tiaa_Cntrct_Nbr(),ldaAdsl6009.getAdsl6009_Pnd_Cis_Entry_Date_Time_Key());
        if (Global.isEscape()) return;
    }
    private void sub_Eoj_Process() throws Exception                                                                                                                       //Natural: EOJ-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        getReports().write(0, "***  ADSP6009 RUN STATISTICS  ***");                                                                                                       //Natural: WRITE '***  ADSP6009 RUN STATISTICS  ***'
        if (Global.isEscape()) return;
        getReports().write(0, "   ");                                                                                                                                     //Natural: WRITE '   '
        if (Global.isEscape()) return;
        getReports().write(0, "Total requests read             ",pnd_Totals_Pnd_Totl_Rqsts_Read);                                                                         //Natural: WRITE 'Total requests read             ' #TOTL-RQSTS-READ
        if (Global.isEscape()) return;
        getReports().write(0, "Total requests written to CIS   ",pnd_Totals_Pnd_Totl_Rqsts_Written);                                                                      //Natural: WRITE 'Total requests written to CIS   ' #TOTL-RQSTS-WRITTEN
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL request errors            ",pnd_Totals_Pnd_Totl_Rqst_Errs);                                                                          //Natural: WRITE 'TOTAL request errors            ' #TOTL-RQST-ERRS
        if (Global.isEscape()) return;
        getReports().write(0, "   ");                                                                                                                                     //Natural: WRITE '   '
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, "INTERNAL ERROR ENCOUNTERED IN PROGRAM :",Global.getINIT_PROGRAM()," ERR NO : ",Global.getERROR_NR()," ERR LINE :",Global.getERROR_LINE()); //Natural: WRITE 'INTERNAL ERROR ENCOUNTERED IN PROGRAM :' *INIT-PROGRAM ' ERR NO : ' *ERROR-NR ' ERR LINE :' *ERROR-LINE
        DbsUtil.terminate(16);  if (true) return;                                                                                                                         //Natural: TERMINATE 16
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132 ZP=OFF SG=OFF");
        Global.format(2, "PS=60 LS=132 ZP=OFF SG=OFF");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(1),"ADSP6009",new TabSetting(50),"ADAS ERROR REQUESTS ",new 
            TabSetting(110),"Run Date:", Color.blue, new FieldAttributes ("AD=I"),new TabSetting(120),Global.getDATX(), new FieldAttributes ("AD=OD"), Color.white,NEWLINE,new 
            TabSetting(50),"  EXCEPTION REPORT ",new TabSetting(110),"Page No :",new TabSetting(120),getReports().getPageNumberDbs(0), new FieldAttributes 
            ("AD=OD"), Color.white, new ReportZeroPrint (true),NEWLINE,new TabSetting(50),"====================",NEWLINE,NEWLINE,new TabSetting(2),"PIN",new 
            ColumnSpacing(11)," PART ",new ColumnSpacing(4),"DA CNTRCT",new ColumnSpacing(3),"IA CNTRCT",new ColumnSpacing(2),"RQST",NEWLINE,new TabSetting(1),"NUMBER",new 
            ColumnSpacing(8)," NUMBER ",new ColumnSpacing(4),"NUMBR",new ColumnSpacing(7),"NUMBR",new ColumnSpacing(5),"ID",NEWLINE,NEWLINE,NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(1),"ADSP6009",new TabSetting(50),"ADAS WRITTEN REQUESTS ",new 
            TabSetting(110),"Run Date:", Color.blue, new FieldAttributes ("AD=I"),new TabSetting(120),Global.getDATX(), new FieldAttributes ("AD=OD"), Color.white,NEWLINE,new 
            TabSetting(50),"============================= ",new TabSetting(110),"Page No :",new TabSetting(120),getReports().getPageNumberDbs(0), new FieldAttributes 
            ("AD=OD"), Color.white, new ReportZeroPrint (true),NEWLINE,NEWLINE,new TabSetting(2),"PIN",new ColumnSpacing(11)," PART ",new ColumnSpacing(4),"DA CNTRCT",new 
            ColumnSpacing(3),"IA CNTRCT",new ColumnSpacing(2),"RQST",NEWLINE,new TabSetting(1),"NUMBER",new ColumnSpacing(8)," NUMBER ",new ColumnSpacing(4),"NUMBR",new 
            ColumnSpacing(7),"NUMBR",new ColumnSpacing(5),"ID",NEWLINE,NEWLINE,NEWLINE,NEWLINE);
    }
}
