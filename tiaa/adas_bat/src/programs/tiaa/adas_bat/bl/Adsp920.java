/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:05:35 PM
**        * FROM NATURAL PROGRAM : Adsp920
************************************************************
**        * FILE NAME            : Adsp920.java
**        * CLASS NAME           : Adsp920
**        * INSTANCE NAME        : Adsp920
************************************************************
************************************************************************
* PROGRAM  : ADSP920
* GENERATED: APRIL 02, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : READS KDO '1' RECORDS TO INCREMENT COUNTS ON DAILY, MTD &
*            YTD CONTROL RECORDS OF TRANSFERS ENTERED
*            (BUT NOT NECESSARILY PROCESSED) ON A GIVEN BUSINESS DATE.
*            THIS PROGRAM IS RUN AFTER THE CONTROL RECORD FOR THE NEXT
*            DAY HAS BEEN WRITTEN, AND SO MUST ACCESS THE PREVIOUS DAILY
*            CONTROL RECORD FOR UPDATE.
*
* REMARKS  : CLONED FROM NAZP920
************************  MAINTENANCE LOG ******************************
*  D A T E   PROGRAMMER     D E S C R I P T I O N
* 01/27/99   ZAFAR KHAN  NAP-BPS-UNIT EXTENDED FROM 6 TO 8 BYTES
* 04/02/04   E. MELNIK   MODIFIED FOR ADAS (ANNUITIZATION SUNGUARD)
* 07/27/04   T. SHINE       IF MONTH-END PROCESS FALLS ON WEEKEND OR
*                           HOLIDAY, USE ADS-RPT-13 (FACTOR DATE) AS
*                           REPORT DATE.
* 03/17/2008 E.MELNIK   ROTH 403B/401K CHANGES.  RECOMPILED FOR
*                       NEW ADSL401.
* 7/19/2010   C. MASON   RESTOW DUE TO LDA CHANGES
* 01/18/13   E. MELNIK  TNG PROJECT.  RESTOW TO INCLUDE UPDATED LINKAGE
*                       AREAS.
* 03/23/17 E. MELNIK   RESTOWED FOR PIN EXPANSION.
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp920 extends BLNatBase
{
    // Data Areas
    private LdaAdsl401 ldaAdsl401;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ads_Cntl_View;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte;
    private DbsField ads_Cntl_View_Ads_Tiaa_Nmbr_Cases_Cnt;
    private DbsField ads_Cntl_View_Ads_Rpt_13;
    private DbsField pnd_Record_Count;
    private DbsField pnd_Cntl_Isn;
    private DbsField pnd_Prev_Bsnss_Dte_D;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_1;
    private DbsField pnd_Date_A_Pnd_Date_N;
    private DbsField pnd_Prev_Bsnss_Dte_N;

    private DbsGroup pnd_Prev_Bsnss_Dte_N__R_Field_2;
    private DbsField pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl401 = new LdaAdsl401();
        registerRecord(ldaAdsl401);
        registerRecord(ldaAdsl401.getVw_ads_Prtcpnt_View());

        // Local Variables
        localVariables = new DbsRecord();

        vw_ads_Cntl_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntl_View", "ADS-CNTL-VIEW"), "ADS_CNTL", "ADS_CNTL");
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_DTE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte", "ADS-CNTL-BSNSS-TMRRW-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_TMRRW_DTE");
        ads_Cntl_View_Ads_Tiaa_Nmbr_Cases_Cnt = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Tiaa_Nmbr_Cases_Cnt", "ADS-TIAA-NMBR-CASES-CNT", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "ADS_TIAA_NMBR_CASES_CNT");
        ads_Cntl_View_Ads_Rpt_13 = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Rpt_13", "ADS-RPT-13", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADS_RPT_13");
        registerRecord(vw_ads_Cntl_View);

        pnd_Record_Count = localVariables.newFieldInRecord("pnd_Record_Count", "#RECORD-COUNT", FieldType.PACKED_DECIMAL, 8);
        pnd_Cntl_Isn = localVariables.newFieldInRecord("pnd_Cntl_Isn", "#CNTL-ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Prev_Bsnss_Dte_D = localVariables.newFieldInRecord("pnd_Prev_Bsnss_Dte_D", "#PREV-BSNSS-DTE-D", FieldType.DATE);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_1 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_1", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_1.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        pnd_Prev_Bsnss_Dte_N = localVariables.newFieldInRecord("pnd_Prev_Bsnss_Dte_N", "#PREV-BSNSS-DTE-N", FieldType.NUMERIC, 8);

        pnd_Prev_Bsnss_Dte_N__R_Field_2 = localVariables.newGroupInRecord("pnd_Prev_Bsnss_Dte_N__R_Field_2", "REDEFINE", pnd_Prev_Bsnss_Dte_N);
        pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A = pnd_Prev_Bsnss_Dte_N__R_Field_2.newFieldInGroup("pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A", "#PREV-BSNSS-DTE-A", 
            FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Cntl_View.reset();

        ldaAdsl401.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp920() throws Exception
    {
        super("Adsp920");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        vw_ads_Cntl_View.startDatabaseRead                                                                                                                                //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "PND_PND_L0530",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        PND_PND_L0530:
        while (condition(vw_ads_Cntl_View.readNextRow("PND_PND_L0530")))
        {
            if (condition(vw_ads_Cntl_View.getAstCOUNTER().equals(2)))                                                                                                    //Natural: IF *COUNTER ( ##L0530. ) = 2
            {
                pnd_Cntl_Isn.setValue(vw_ads_Cntl_View.getAstISN("PND_PND_L0530"));                                                                                       //Natural: MOVE *ISN TO #CNTL-ISN
                if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                             //Natural: IF ADS-RPT-13 NE 0
                {
                    pnd_Date_A.setValueEdited(ads_Cntl_View_Ads_Rpt_13,new ReportEditMask("YYYYMMDD"));                                                                   //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE-A
                    pnd_Prev_Bsnss_Dte_N.setValue(pnd_Date_A_Pnd_Date_N);                                                                                                 //Natural: MOVE #DATE-N TO #PREV-BSNSS-DTE-N
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Prev_Bsnss_Dte_N.setValue(ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                                      //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #PREV-BSNSS-DTE-N
                }                                                                                                                                                         //Natural: END-IF
                pnd_Prev_Bsnss_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A);                                            //Natural: MOVE EDITED #PREV-BSNSS-DTE-A TO #PREV-BSNSS-DTE-D ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaAdsl401.getVw_ads_Prtcpnt_View().startDatabaseRead                                                                                                             //Natural: READ ADS-PRTCPNT-VIEW WITH ADP-LST-ACTVTY-DTE = #PREV-BSNSS-DTE-D
        (
        "READ01",
        new Wc[] { new Wc("ADP_LST_ACTVTY_DTE", ">=", pnd_Prev_Bsnss_Dte_D, WcType.BY) },
        new Oc[] { new Oc("ADP_LST_ACTVTY_DTE", "ASC") }
        );
        READ01:
        while (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().readNextRow("READ01")))
        {
            pnd_Record_Count.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #RECORD-COUNT
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_ads_Cntl_View.startDatabaseRead                                                                                                                                //Natural: READ ( 1 ) ADS-CNTL-VIEW WITH ISN = #CNTL-ISN
        (
        "PND_PND_L0750",
        new Wc[] { new Wc("ISN", ">=", pnd_Cntl_Isn, WcType.BY) },
        new Oc[] { new Oc("ISN", "ASC") },
        1
        );
        PND_PND_L0750:
        while (condition(vw_ads_Cntl_View.readNextRow("PND_PND_L0750")))
        {
            ads_Cntl_View_Ads_Tiaa_Nmbr_Cases_Cnt.setValue(pnd_Record_Count);                                                                                             //Natural: MOVE #RECORD-COUNT TO ADS-TIAA-NMBR-CASES-CNT
            vw_ads_Cntl_View.updateDBRow("PND_PND_L0750");                                                                                                                //Natural: UPDATE ( ##L0750. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
