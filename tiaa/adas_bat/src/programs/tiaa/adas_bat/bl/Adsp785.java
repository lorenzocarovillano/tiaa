/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:05:27 PM
**        * FROM NATURAL PROGRAM : Adsp785
************************************************************
**        * FILE NAME            : Adsp785.java
**        * CLASS NAME           : Adsp785
**        * INSTANCE NAME        : Adsp785
************************************************************
************************************************************************
* PROGRAM  : ADSP785
* GENERATED: APRIL 16, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : THIS MODULE READS WORKFILE TAD.P05.PVT.NAZP710 AND
*          : REPORTS A GRAND TOTAL OF THE DA TO  IA  MAUTRITIES AND RTBS
*            FOR MONTH-TO-DATE BY EFFECTIVE DATE.
*
* REMARKS  : CLONED FROM NAZP785  (ADAM SYSTEM)
************************************************************************
* MOD DATE   MOD BY      DESCRIPTION OF CHANGES
* 02/24/98  ZAFAR KHAN   MONTH-TO-DATE RTB TOTALS CORRECTED
* 05/28/98  ZAFAR KHAN   ROTH/CLASSIC ROLLOVER PROCESS ADDED
* 11/17/98  ZAFAR KHAN   CHECK ADDED FOR DESTINATION CODES
*                        'IRAC', 'IRAT', 'RTHC' & 'RTHT' FOR ROLLOVER
* 11/14/00  FAZAL TWAHIR TPA / IO REPORTING  MARKED M001
* 10/12/01  FAZAL TWAHIR BUCKET IO/TPA ACCUMS ONLY (NO INT-ROLLOVER)
* 01/26/04  OSCAR SOTTO  99 RATES CHANGES. SCAN FOR 012604.
* 04/16/04  C. AVE       MODIFIED FOR ADAS - ANNUITIZATION SUNGUARD
* 07/27/04  T. SHINE       IF MONTH-END PROCESS FALLS ON WEEKEND OR
*                          HOLIDAY, USE ADS-RPT-13 (FACTOR DATE) AS
*                          REPORT DATE.
* 05/06/08  GUERRERO     CHECKED ROTH IND TO PRINT ROTH REPORT.
* 01/14/09  SACHARNY     EXPAND PARM AREA FOR ADSN888 (RS0)
*                        INCORPORATE STABLE RETURN FUND INTO PROCESS
* 05/14/10  D.E.ANDER    SURVIVOR CHANGES FOR SVSAA  (DEA)
* 01/05/11  O. SOTTO     REPLACED O3 WITH 03. SC 010511.
* 03/07/12  E. MELNIK RATE BAE EXPANSION PROJECT.  RESTOW TO INCLUDE
*                     UPDATED LINKAGE AREAS.  CHANGES MARKED BY
*                     EM - 030712.
* 01/18/13  E. MELNIK TNG PROJECT.  RESTOW TO INCLUDE UPDATED LINKAGE
*                     AREAS.
* 10/23/13   E. MELNIK CREF/REA REDESIGN CHANGES.
*                      MARKED BY EM - 102313.
* 02/28/2017 R.CARREON RESTOWED FOR PIN EXPANSION
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp785 extends BLNatBase
{
    // Data Areas
    private LdaAdsl780 ldaAdsl780;
    private LdaAdsl401a ldaAdsl401a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Nap_Annty_Optn;
    private DbsField pnd_Rec_Type;

    private DbsGroup pnd_Rec_Type__R_Field_1;
    private DbsField pnd_Rec_Type_Pnd_Filler;
    private DbsField pnd_Rec_Type_Pnd_Z_Indx;
    private DbsField pnd_Alpha_Fund_Cde;

    private DataAccessProgramView vw_ads_Cntl_View;

    private DbsGroup ads_Cntl_View_Ads_Cntl_Grp;
    private DbsField ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte;
    private DbsField ads_Cntl_View_Ads_Daily_Actvty_Dte;
    private DbsField ads_Cntl_View_Ads_Rpt_13;
    private DbsField pnd_Prev_Bsnss_Dte_N;

    private DbsGroup pnd_Prev_Bsnss_Dte_N__R_Field_2;
    private DbsField pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A;
    private DbsField pnd_Today_Business_Date;

    private DbsGroup pnd_Today_Business_Date__R_Field_3;
    private DbsField pnd_Today_Business_Date_Pnd_Today_Business_Date_Ccyy;
    private DbsField pnd_Today_Business_Date_Pnd_Today_Business_Date_Mmdd;

    private DbsGroup pnd_Today_Business_Date__R_Field_4;
    private DbsField pnd_Today_Business_Date_Pnd_Today_Business_Date_Mm;
    private DbsField pnd_Today_Business_Date_Pnd_Today_Business_Date_Dd;
    private DbsField pnd_Current_Accounting_Date;

    private DbsGroup pnd_Current_Accounting_Date__R_Field_5;
    private DbsField pnd_Current_Accounting_Date_Pnd_Current_Accounting_Date_Ccyy;
    private DbsField pnd_Current_Accounting_Date_Pnd_Current_Accounting_Date_Mmdd;
    private DbsField pnd_Curr_Month_Date;

    private DbsGroup pnd_Curr_Month_Date__R_Field_6;
    private DbsField pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Ccyy;
    private DbsField pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Mmdd;

    private DbsGroup pnd_Curr_Month_Date__R_Field_7;
    private DbsField pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Mm;
    private DbsField pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Dd;
    private DbsField pnd_Prev_Bsnss_Dte_D;
    private DbsField pnd_Nad_Super_2;

    private DbsGroup pnd_Nad_Super_2__R_Field_8;
    private DbsField pnd_Nad_Super_2_Pnd_Nad_Lst_Actvty_Dte;
    private DbsField pnd_Nad_Super_2_Pnd_Nad_Stts_Cde;

    private DbsGroup pnd_All_Indx;
    private DbsField pnd_All_Indx_Pnd_A_Indx;
    private DbsField pnd_All_Indx_Pnd_B_Indx;
    private DbsField pnd_All_Indx_Pnd_C_Indx;
    private DbsField pnd_All_Indx_Pnd_Next_Indx;
    private DbsField pnd_All_Indx_Pnd_Prod_Indx;
    private DbsField pnd_All_Indx_Pnd_Pos;
    private DbsField pnd_Dest;
    private DbsField pnd_Roth_Ind;
    private DbsField pnd_X;
    private DbsField pnd_J;
    private DbsField pnd_Prod_Cde;
    private DbsField pnd_Da_Count;
    private DbsField pnd_Rlvr_Amt;
    private DbsField pnd_Max_Rate;
    private DbsField pnd_Cntl_Isn;
    private DbsField pnd_Total_Number_Rec;
    private DbsField pnd_Total_Number_Rec_Roth;
    private DbsField pnd_First_Time;
    private DbsField pnd_Tiaa_Prod_Code;
    private DbsField pnd_Cref_Prod_Found;
    private DbsField pnd_Roth_Only_Run;
    private DbsField pnd_Stbl_Fund;
    private DbsField pnd_Svsaa_Fund;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd;

    private DbsGroup pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_9;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd;
    private DbsField pnd_Prev_Lst_Actvty_Dte_Yyyymmdd;

    private DbsGroup pnd_Prev_Lst_Actvty_Dte_Yyyymmdd__R_Field_10;
    private DbsField pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Cc;
    private DbsField pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Yy;
    private DbsField pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Mm;
    private DbsField pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Dd;
    private DbsField pnd_Rqst_Id;

    private DbsGroup pnd_Rqst_Id__R_Field_11;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Unique_Id;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte;

    private DbsGroup pnd_Rqst_Id__R_Field_12;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Cc;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Yy;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Mm;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Dd;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Entry_Dte;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Entry_Tme;
    private DbsField pnd_Prev_Effctv_Dte;

    private DbsGroup pnd_Prev_Effctv_Dte__R_Field_13;
    private DbsField pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Cc;
    private DbsField pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Yy;
    private DbsField pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Mm;
    private DbsField pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Dd;
    private DbsField pnd_Pec_Nbr_Active_Acct;
    private DbsField pnd_Adp_Effctv_Date;
    private DbsField pnd_Heading;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_14;
    private DbsField pnd_Date_A_Pnd_Date_N;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl780 = new LdaAdsl780();
        registerRecord(ldaAdsl780);
        ldaAdsl401a = new LdaAdsl401a();
        registerRecord(ldaAdsl401a);
        registerRecord(ldaAdsl401a.getVw_ads_Cntrct_View());

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Nap_Annty_Optn = localVariables.newFieldInRecord("pnd_Nap_Annty_Optn", "#NAP-ANNTY-OPTN", FieldType.STRING, 2);
        pnd_Rec_Type = localVariables.newFieldInRecord("pnd_Rec_Type", "#REC-TYPE", FieldType.NUMERIC, 3);

        pnd_Rec_Type__R_Field_1 = localVariables.newGroupInRecord("pnd_Rec_Type__R_Field_1", "REDEFINE", pnd_Rec_Type);
        pnd_Rec_Type_Pnd_Filler = pnd_Rec_Type__R_Field_1.newFieldInGroup("pnd_Rec_Type_Pnd_Filler", "#FILLER", FieldType.NUMERIC, 2);
        pnd_Rec_Type_Pnd_Z_Indx = pnd_Rec_Type__R_Field_1.newFieldInGroup("pnd_Rec_Type_Pnd_Z_Indx", "#Z-INDX", FieldType.NUMERIC, 1);
        pnd_Alpha_Fund_Cde = localVariables.newFieldArrayInRecord("pnd_Alpha_Fund_Cde", "#ALPHA-FUND-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20));

        vw_ads_Cntl_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntl_View", "ADS-CNTL-VIEW"), "ADS_CNTL", "ADS_CNTL");

        ads_Cntl_View_Ads_Cntl_Grp = vw_ads_Cntl_View.getRecord().newGroupInGroup("ADS_CNTL_VIEW_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde", "ADS-CNTL-RCRD-TYP-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADS_CNTL_RCRD_TYP_CDE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte", "ADS-CNTL-BSNSS-RCPRCL-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_RCPRCL_DTE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_DTE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte", "ADS-CNTL-BSNSS-TMRRW-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_TMRRW_DTE");
        ads_Cntl_View_Ads_Daily_Actvty_Dte = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Daily_Actvty_Dte", "ADS-DAILY-ACTVTY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADS_DAILY_ACTVTY_DTE");
        ads_Cntl_View_Ads_Rpt_13 = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Rpt_13", "ADS-RPT-13", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADS_RPT_13");
        registerRecord(vw_ads_Cntl_View);

        pnd_Prev_Bsnss_Dte_N = localVariables.newFieldInRecord("pnd_Prev_Bsnss_Dte_N", "#PREV-BSNSS-DTE-N", FieldType.NUMERIC, 8);

        pnd_Prev_Bsnss_Dte_N__R_Field_2 = localVariables.newGroupInRecord("pnd_Prev_Bsnss_Dte_N__R_Field_2", "REDEFINE", pnd_Prev_Bsnss_Dte_N);
        pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A = pnd_Prev_Bsnss_Dte_N__R_Field_2.newFieldInGroup("pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A", "#PREV-BSNSS-DTE-A", 
            FieldType.STRING, 8);
        pnd_Today_Business_Date = localVariables.newFieldInRecord("pnd_Today_Business_Date", "#TODAY-BUSINESS-DATE", FieldType.STRING, 8);

        pnd_Today_Business_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Today_Business_Date__R_Field_3", "REDEFINE", pnd_Today_Business_Date);
        pnd_Today_Business_Date_Pnd_Today_Business_Date_Ccyy = pnd_Today_Business_Date__R_Field_3.newFieldInGroup("pnd_Today_Business_Date_Pnd_Today_Business_Date_Ccyy", 
            "#TODAY-BUSINESS-DATE-CCYY", FieldType.NUMERIC, 4);
        pnd_Today_Business_Date_Pnd_Today_Business_Date_Mmdd = pnd_Today_Business_Date__R_Field_3.newFieldInGroup("pnd_Today_Business_Date_Pnd_Today_Business_Date_Mmdd", 
            "#TODAY-BUSINESS-DATE-MMDD", FieldType.STRING, 4);

        pnd_Today_Business_Date__R_Field_4 = pnd_Today_Business_Date__R_Field_3.newGroupInGroup("pnd_Today_Business_Date__R_Field_4", "REDEFINE", pnd_Today_Business_Date_Pnd_Today_Business_Date_Mmdd);
        pnd_Today_Business_Date_Pnd_Today_Business_Date_Mm = pnd_Today_Business_Date__R_Field_4.newFieldInGroup("pnd_Today_Business_Date_Pnd_Today_Business_Date_Mm", 
            "#TODAY-BUSINESS-DATE-MM", FieldType.STRING, 2);
        pnd_Today_Business_Date_Pnd_Today_Business_Date_Dd = pnd_Today_Business_Date__R_Field_4.newFieldInGroup("pnd_Today_Business_Date_Pnd_Today_Business_Date_Dd", 
            "#TODAY-BUSINESS-DATE-DD", FieldType.STRING, 2);
        pnd_Current_Accounting_Date = localVariables.newFieldInRecord("pnd_Current_Accounting_Date", "#CURRENT-ACCOUNTING-DATE", FieldType.STRING, 8);

        pnd_Current_Accounting_Date__R_Field_5 = localVariables.newGroupInRecord("pnd_Current_Accounting_Date__R_Field_5", "REDEFINE", pnd_Current_Accounting_Date);
        pnd_Current_Accounting_Date_Pnd_Current_Accounting_Date_Ccyy = pnd_Current_Accounting_Date__R_Field_5.newFieldInGroup("pnd_Current_Accounting_Date_Pnd_Current_Accounting_Date_Ccyy", 
            "#CURRENT-ACCOUNTING-DATE-CCYY", FieldType.NUMERIC, 4);
        pnd_Current_Accounting_Date_Pnd_Current_Accounting_Date_Mmdd = pnd_Current_Accounting_Date__R_Field_5.newFieldInGroup("pnd_Current_Accounting_Date_Pnd_Current_Accounting_Date_Mmdd", 
            "#CURRENT-ACCOUNTING-DATE-MMDD", FieldType.STRING, 4);
        pnd_Curr_Month_Date = localVariables.newFieldInRecord("pnd_Curr_Month_Date", "#CURR-MONTH-DATE", FieldType.STRING, 8);

        pnd_Curr_Month_Date__R_Field_6 = localVariables.newGroupInRecord("pnd_Curr_Month_Date__R_Field_6", "REDEFINE", pnd_Curr_Month_Date);
        pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Ccyy = pnd_Curr_Month_Date__R_Field_6.newFieldInGroup("pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Ccyy", 
            "#CURR-MONTH-DATE-CCYY", FieldType.NUMERIC, 4);
        pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Mmdd = pnd_Curr_Month_Date__R_Field_6.newFieldInGroup("pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Mmdd", 
            "#CURR-MONTH-DATE-MMDD", FieldType.STRING, 4);

        pnd_Curr_Month_Date__R_Field_7 = pnd_Curr_Month_Date__R_Field_6.newGroupInGroup("pnd_Curr_Month_Date__R_Field_7", "REDEFINE", pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Mmdd);
        pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Mm = pnd_Curr_Month_Date__R_Field_7.newFieldInGroup("pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Mm", "#CURR-MONTH-DATE-MM", 
            FieldType.STRING, 2);
        pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Dd = pnd_Curr_Month_Date__R_Field_7.newFieldInGroup("pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Dd", "#CURR-MONTH-DATE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Prev_Bsnss_Dte_D = localVariables.newFieldInRecord("pnd_Prev_Bsnss_Dte_D", "#PREV-BSNSS-DTE-D", FieldType.DATE);
        pnd_Nad_Super_2 = localVariables.newFieldInRecord("pnd_Nad_Super_2", "#NAD-SUPER-2", FieldType.STRING, 7);

        pnd_Nad_Super_2__R_Field_8 = localVariables.newGroupInRecord("pnd_Nad_Super_2__R_Field_8", "REDEFINE", pnd_Nad_Super_2);
        pnd_Nad_Super_2_Pnd_Nad_Lst_Actvty_Dte = pnd_Nad_Super_2__R_Field_8.newFieldInGroup("pnd_Nad_Super_2_Pnd_Nad_Lst_Actvty_Dte", "#NAD-LST-ACTVTY-DTE", 
            FieldType.DATE);
        pnd_Nad_Super_2_Pnd_Nad_Stts_Cde = pnd_Nad_Super_2__R_Field_8.newFieldInGroup("pnd_Nad_Super_2_Pnd_Nad_Stts_Cde", "#NAD-STTS-CDE", FieldType.STRING, 
            3);

        pnd_All_Indx = localVariables.newGroupInRecord("pnd_All_Indx", "#ALL-INDX");
        pnd_All_Indx_Pnd_A_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_A_Indx", "#A-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_B_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_B_Indx", "#B-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_C_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_C_Indx", "#C-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Next_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Next_Indx", "#NEXT-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Prod_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Prod_Indx", "#PROD-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Pos = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Pos", "#POS", FieldType.PACKED_DECIMAL, 3);
        pnd_Dest = localVariables.newFieldArrayInRecord("pnd_Dest", "#DEST", FieldType.STRING, 4, new DbsArrayController(1, 3));
        pnd_Roth_Ind = localVariables.newFieldInRecord("pnd_Roth_Ind", "#ROTH-IND", FieldType.STRING, 1);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 1);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 1);
        pnd_Prod_Cde = localVariables.newFieldArrayInRecord("pnd_Prod_Cde", "#PROD-CDE", FieldType.STRING, 1, new DbsArrayController(1, 20));
        pnd_Da_Count = localVariables.newFieldArrayInRecord("pnd_Da_Count", "#DA-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Rlvr_Amt = localVariables.newFieldArrayInRecord("pnd_Rlvr_Amt", "#RLVR-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 20));
        pnd_Max_Rate = localVariables.newFieldInRecord("pnd_Max_Rate", "#MAX-RATE", FieldType.PACKED_DECIMAL, 3);
        pnd_Cntl_Isn = localVariables.newFieldInRecord("pnd_Cntl_Isn", "#CNTL-ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Total_Number_Rec = localVariables.newFieldInRecord("pnd_Total_Number_Rec", "#TOTAL-NUMBER-REC", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Number_Rec_Roth = localVariables.newFieldInRecord("pnd_Total_Number_Rec_Roth", "#TOTAL-NUMBER-REC-ROTH", FieldType.PACKED_DECIMAL, 7);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Prod_Code = localVariables.newFieldInRecord("pnd_Tiaa_Prod_Code", "#TIAA-PROD-CODE", FieldType.BOOLEAN, 1);
        pnd_Cref_Prod_Found = localVariables.newFieldInRecord("pnd_Cref_Prod_Found", "#CREF-PROD-FOUND", FieldType.BOOLEAN, 1);
        pnd_Roth_Only_Run = localVariables.newFieldInRecord("pnd_Roth_Only_Run", "#ROTH-ONLY-RUN", FieldType.BOOLEAN, 1);
        pnd_Stbl_Fund = localVariables.newFieldInRecord("pnd_Stbl_Fund", "#STBL-FUND", FieldType.BOOLEAN, 1);
        pnd_Svsaa_Fund = localVariables.newFieldInRecord("pnd_Svsaa_Fund", "#SVSAA-FUND", FieldType.BOOLEAN, 1);
        pnd_Prev_Accounting_Dte_Yyyymmdd = localVariables.newFieldInRecord("pnd_Prev_Accounting_Dte_Yyyymmdd", "#PREV-ACCOUNTING-DTE-YYYYMMDD", FieldType.STRING, 
            8);

        pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_9 = localVariables.newGroupInRecord("pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_9", "REDEFINE", pnd_Prev_Accounting_Dte_Yyyymmdd);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_9.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc", 
            "#PREV-ACCOUNT-DTE-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_9.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy", 
            "#PREV-ACCOUNT-DTE-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_9.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm", 
            "#PREV-ACCOUNT-DTE-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_9.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd", 
            "#PREV-ACCOUNT-DTE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Prev_Lst_Actvty_Dte_Yyyymmdd = localVariables.newFieldInRecord("pnd_Prev_Lst_Actvty_Dte_Yyyymmdd", "#PREV-LST-ACTVTY-DTE-YYYYMMDD", FieldType.STRING, 
            8);

        pnd_Prev_Lst_Actvty_Dte_Yyyymmdd__R_Field_10 = localVariables.newGroupInRecord("pnd_Prev_Lst_Actvty_Dte_Yyyymmdd__R_Field_10", "REDEFINE", pnd_Prev_Lst_Actvty_Dte_Yyyymmdd);
        pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Cc = pnd_Prev_Lst_Actvty_Dte_Yyyymmdd__R_Field_10.newFieldInGroup("pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Cc", 
            "#PREV-ACTVITY-DTE-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Yy = pnd_Prev_Lst_Actvty_Dte_Yyyymmdd__R_Field_10.newFieldInGroup("pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Yy", 
            "#PREV-ACTVITY-DTE-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Mm = pnd_Prev_Lst_Actvty_Dte_Yyyymmdd__R_Field_10.newFieldInGroup("pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Mm", 
            "#PREV-ACTVITY-DTE-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Dd = pnd_Prev_Lst_Actvty_Dte_Yyyymmdd__R_Field_10.newFieldInGroup("pnd_Prev_Lst_Actvty_Dte_Yyyymmdd_Pnd_Prev_Actvity_Dte_Dte_Dd", 
            "#PREV-ACTVITY-DTE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Rqst_Id = localVariables.newFieldInRecord("pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 30);

        pnd_Rqst_Id__R_Field_11 = localVariables.newGroupInRecord("pnd_Rqst_Id__R_Field_11", "REDEFINE", pnd_Rqst_Id);
        pnd_Rqst_Id_Pnd_Nap_Unique_Id = pnd_Rqst_Id__R_Field_11.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Unique_Id", "#NAP-UNIQUE-ID", FieldType.NUMERIC, 
            7);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte = pnd_Rqst_Id__R_Field_11.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte", "#NAP-EFFCTV-DTE", FieldType.NUMERIC, 
            8);

        pnd_Rqst_Id__R_Field_12 = pnd_Rqst_Id__R_Field_11.newGroupInGroup("pnd_Rqst_Id__R_Field_12", "REDEFINE", pnd_Rqst_Id_Pnd_Nap_Effctv_Dte);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Cc = pnd_Rqst_Id__R_Field_12.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Cc", "#NAP-EFFCTV-DTE-CC", FieldType.NUMERIC, 
            2);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Yy = pnd_Rqst_Id__R_Field_12.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Yy", "#NAP-EFFCTV-DTE-YY", FieldType.NUMERIC, 
            2);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Mm = pnd_Rqst_Id__R_Field_12.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Mm", "#NAP-EFFCTV-DTE-MM", FieldType.NUMERIC, 
            2);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Dd = pnd_Rqst_Id__R_Field_12.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Dd", "#NAP-EFFCTV-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_Rqst_Id_Pnd_Nap_Entry_Dte = pnd_Rqst_Id__R_Field_11.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Entry_Dte", "#NAP-ENTRY-DTE", FieldType.NUMERIC, 
            8);
        pnd_Rqst_Id_Pnd_Nap_Entry_Tme = pnd_Rqst_Id__R_Field_11.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Entry_Tme", "#NAP-ENTRY-TME", FieldType.NUMERIC, 
            7);
        pnd_Prev_Effctv_Dte = localVariables.newFieldInRecord("pnd_Prev_Effctv_Dte", "#PREV-EFFCTV-DTE", FieldType.NUMERIC, 8);

        pnd_Prev_Effctv_Dte__R_Field_13 = localVariables.newGroupInRecord("pnd_Prev_Effctv_Dte__R_Field_13", "REDEFINE", pnd_Prev_Effctv_Dte);
        pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Cc = pnd_Prev_Effctv_Dte__R_Field_13.newFieldInGroup("pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Cc", "#PREV-EFFCTV-DTE-CC", 
            FieldType.NUMERIC, 2);
        pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Yy = pnd_Prev_Effctv_Dte__R_Field_13.newFieldInGroup("pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Yy", "#PREV-EFFCTV-DTE-YY", 
            FieldType.NUMERIC, 2);
        pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Mm = pnd_Prev_Effctv_Dte__R_Field_13.newFieldInGroup("pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Mm", "#PREV-EFFCTV-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Dd = pnd_Prev_Effctv_Dte__R_Field_13.newFieldInGroup("pnd_Prev_Effctv_Dte_Pnd_Prev_Effctv_Dte_Dd", "#PREV-EFFCTV-DTE-DD", 
            FieldType.NUMERIC, 2);
        pnd_Pec_Nbr_Active_Acct = localVariables.newFieldInRecord("pnd_Pec_Nbr_Active_Acct", "#PEC-NBR-ACTIVE-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Adp_Effctv_Date = localVariables.newFieldInRecord("pnd_Adp_Effctv_Date", "#ADP-EFFCTV-DATE", FieldType.DATE);
        pnd_Heading = localVariables.newFieldInRecord("pnd_Heading", "#HEADING", FieldType.STRING, 50);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_14 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_14", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_14.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Cntl_View.reset();

        ldaAdsl780.initializeValues();
        ldaAdsl401a.initializeValues();

        localVariables.reset();
        pnd_All_Indx_Pnd_A_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_B_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_C_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Next_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Prod_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Pos.setInitialValue(0);
        pnd_Max_Rate.setInitialValue(250);
        pnd_Total_Number_Rec.setInitialValue(0);
        pnd_Total_Number_Rec_Roth.setInitialValue(0);
        pnd_First_Time.setInitialValue(true);
        pnd_Tiaa_Prod_Code.setInitialValue(true);
        pnd_Cref_Prod_Found.setInitialValue(true);
        pnd_Roth_Only_Run.setInitialValue(false);
        pnd_Svsaa_Fund.setInitialValue(false);
        pnd_Pec_Nbr_Active_Acct.setInitialValue(20);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp785() throws Exception
    {
        super("Adsp785");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 79 PS = 60;//Natural: FORMAT ( 1 ) LS = 79 PS = 60;//Natural: FORMAT ( 2 ) LS = 79 PS = 60
        //* * EM - 102313 START
        //*  CALLNAT 'ADSN888'   /* NEW EXTERNALIZATION + SEQUENCING OF PROD CDE
        //*   #PAS-EXT-CNTRL-02-WORK-AREA
        //*   #PEC-NBR-ACTIVE-ACCT
        //*  NEW EXTERNALIZATION + SEQUENCING OF PROD CDE
        DbsUtil.callnat(Adsn889.class , getCurrentProcessState(), pnd_Alpha_Fund_Cde.getValue(1,":",20));                                                                 //Natural: CALLNAT 'ADSN889' #ALPHA-FUND-CDE ( 1:20 )
        if (condition(Global.isEscape())) return;
        //* * EM - 102313 END
                                                                                                                                                                          //Natural: PERFORM READ-CONTROL-RECORD
        sub_Read_Control_Record();
        if (condition(Global.isEscape())) {return;}
        //*  M001
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #NAP-ANNTY-OPTN #ADP-EFFCTV-DATE ADS-CNTRCT-VIEW #DA-COUNT ( * ) #RLVR-AMT ( * ) #PROD-CDE ( * ) #DEST ( * ) #ROTH-IND
        while (condition(getWorkFiles().read(1, pnd_Nap_Annty_Optn, pnd_Adp_Effctv_Date, ldaAdsl401a.getVw_ads_Cntrct_View(), pnd_Da_Count.getValue("*"), 
            pnd_Rlvr_Amt.getValue("*"), pnd_Prod_Cde.getValue("*"), pnd_Dest.getValue("*"), pnd_Roth_Ind)))
        {
            getReports().write(0, ldaAdsl401a.getAds_Cntrct_View_Rqst_Id(),pnd_Nap_Annty_Optn,pnd_Dest.getValue(1),ldaAdsl401a.getAds_Cntrct_View_Adc_Lst_Actvty_Dte(),   //Natural: WRITE ADS-CNTRCT-VIEW.RQST-ID #NAP-ANNTY-OPTN #DEST ( 1 ) ADS-CNTRCT-VIEW.ADC-LST-ACTVTY-DTE #ADP-EFFCTV-DATE #ROTH-IND
                pnd_Adp_Effctv_Date,pnd_Roth_Ind);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *    NAZ-DA-RSLT-DDM-VIEW.NAD-MTRTY-NON-PRM-DTE
            //*  GG050608
            if (condition(pnd_Roth_Ind.equals("Y")))                                                                                                                      //Natural: IF #ROTH-IND = 'Y'
            {
                pnd_Roth_Only_Run.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #ROTH-ONLY-RUN
            }                                                                                                                                                             //Natural: END-IF
            pnd_Current_Accounting_Date.setValueEdited(pnd_Adp_Effctv_Date,new ReportEditMask("YYYYMMDD"));                                                               //Natural: MOVE EDITED #ADP-EFFCTV-DATE ( EM = YYYYMMDD ) TO #CURRENT-ACCOUNTING-DATE
            FOR01:                                                                                                                                                        //Natural: FOR #A-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
            for (pnd_All_Indx_Pnd_A_Indx.setValue(1); condition(pnd_All_Indx_Pnd_A_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_A_Indx.nadd(1))
            {
                //*  RS0
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).equals("T") || ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).equals("Y"))) //Natural: IF ADC-ACCT-CDE ( #A-INDX ) = 'T' OR ADC-ACCT-CDE ( #A-INDX ) = 'Y'
                {
                    pnd_Total_Number_Rec.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-NUMBER-REC
                    //*  RS0
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).equals("Y")))                                           //Natural: IF ADC-ACCT-CDE ( #A-INDX ) = 'Y'
                    {
                        pnd_Stbl_Fund.setValue(true);                                                                                                                     //Natural: MOVE TRUE TO #STBL-FUND
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Stbl_Fund.setValue(false);                                                                                                                    //Natural: MOVE FALSE TO #STBL-FUND
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-ACCUMULATIONS
                    sub_Calculate_Tiaa_Accumulations();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Total_Number_Rec.nadd(1);                                                                                                                         //Natural: ADD 1 TO #TOTAL-NUMBER-REC
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-PRODUCT-ACCUMULATION
                    sub_Calculate_Cref_Product_Accumulation();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*     EXAMINE #PEC-ACCT-NAME-1 (*) FOR #PROD-CDE (#A-INDX) /* EM - 102313
                    //*  EM - 102313
                    DbsUtil.examine(new ExamineSource(pnd_Alpha_Fund_Cde.getValue("*")), new ExamineSearch(pnd_Prod_Cde.getValue(pnd_All_Indx_Pnd_A_Indx)),               //Natural: EXAMINE #ALPHA-FUND-CDE ( * ) FOR #PROD-CDE ( #A-INDX ) GIVING INDEX #POS
                        new ExamineGivingIndex(pnd_All_Indx_Pnd_Pos));
                    if (condition(pnd_All_Indx_Pnd_Pos.greater(getZero())))                                                                                               //Natural: IF #POS > 0
                    {
                                                                                                                                                                          //Natural: PERFORM BUILT-ROLLOVER-TOTALS
                        sub_Built_Rollover_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  GG050608
                if (condition(pnd_Roth_Ind.equals("Y")))                                                                                                                  //Natural: IF #ROTH-IND = 'Y'
                {
                    pnd_Total_Number_Rec_Roth.nadd(1);                                                                                                                    //Natural: ADD 1 TO #TOTAL-NUMBER-REC-ROTH
                }                                                                                                                                                         //Natural: END-IF
                //*  GG050608
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 1 )
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 2 )
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM BREAK-OF-ACCOUNTING-DATE-RTN
        sub_Break_Of_Accounting_Date_Rtn();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-OF-JOB-RTN
        sub_End_Of_Job_Rtn();
        if (condition(Global.isEscape())) {return;}
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CONTROL-RECORD
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TIAA-ACCUMULATIONS
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUCKET-TIAA-STANDARD-ACCUMULATION
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUCKET-TIAA-STANDARD-ACCUMULA-SV
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUCKET-TPA-ROLLOVER-ACCUMULATION
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUCKET-TPA-STANDARD-ACCUMULATION
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUCKET-IPRO-ROLLOVER-ACCUMULATION
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUCKET-IPRO-STANDARD-ACCUMULATION
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUCKET-TIAA-GRADED-ACCUMULATION
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUCKET-TIAA-GRADED-ACCUMULA-SV
        //*                                                            /* DEA END
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-CREF-PRODUCT-ACCUMULATION
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILT-ROLLOVER-TOTALS
        //*  CURTIS NEW CODES EGGTRA
        //*            OR = '57BT' OR = '57BC'
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BREAK-OF-ACCOUNTING-DATE-RTN
        //*  M001
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BREAK-OF-SETTLEMENT-TYPE
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-JOB-RTN
    }
    private void sub_Read_Control_Record() throws Exception                                                                                                               //Natural: READ-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        vw_ads_Cntl_View.startDatabaseRead                                                                                                                                //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "READ02",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        READ02:
        while (condition(vw_ads_Cntl_View.readNextRow("READ02")))
        {
            //*    MOVE 20011001 TO ADS-CNTL-BSNSS-DTE
            //*    MOVE 20011002 TO ADS-CNTL-BSNSS-TMRRW-DTE
            if (condition(vw_ads_Cntl_View.getAstCOUNTER().equals(2)))                                                                                                    //Natural: IF *COUNTER = 2
            {
                if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                             //Natural: IF ADS-RPT-13 NE 0
                {
                    pnd_Date_A.setValueEdited(ads_Cntl_View_Ads_Rpt_13,new ReportEditMask("YYYYMMDD"));                                                                   //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE-A
                    pnd_Prev_Bsnss_Dte_N.setValue(pnd_Date_A_Pnd_Date_N);                                                                                                 //Natural: MOVE #DATE-N TO #PREV-BSNSS-DTE-N
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Prev_Bsnss_Dte_N.setValue(ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                                      //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #PREV-BSNSS-DTE-N
                }                                                                                                                                                         //Natural: END-IF
                pnd_Prev_Bsnss_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A);                                            //Natural: MOVE EDITED #PREV-BSNSS-DTE-A TO #PREV-BSNSS-DTE-D ( EM = YYYYMMDD )
                pnd_Today_Business_Date.setValueEdited(pnd_Prev_Bsnss_Dte_D,new ReportEditMask("YYYYMMDD"));                                                              //Natural: MOVE EDITED #PREV-BSNSS-DTE-D ( EM = YYYYMMDD ) TO #TODAY-BUSINESS-DATE
                pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Mm.setValue(pnd_Today_Business_Date_Pnd_Today_Business_Date_Mm);                                                  //Natural: MOVE #TODAY-BUSINESS-DATE-MM TO #CURR-MONTH-DATE-MM
                pnd_Curr_Month_Date_Pnd_Curr_Month_Date_Dd.setValue(0);                                                                                                   //Natural: MOVE 00 TO #CURR-MONTH-DATE-DD
                if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                             //Natural: IF ADS-RPT-13 NE 0
                {
                    pnd_Prev_Accounting_Dte_Yyyymmdd.setValue(pnd_Date_A_Pnd_Date_N);                                                                                     //Natural: MOVE #DATE-N TO #PREV-ACCOUNTING-DTE-YYYYMMDD
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Prev_Accounting_Dte_Yyyymmdd.setValue(ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                          //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #PREV-ACCOUNTING-DTE-YYYYMMDD
                }                                                                                                                                                         //Natural: END-IF
                //* *  MOVE ADS-CNTL-BSNSS-TMRRW-DTE TO #PREV-ACCOUNTING-DTE-YYYYMMDD
                ldaAdsl780.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc);                   //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-CC TO #ACCOUNTING-DATE-MCDY-CC
                ldaAdsl780.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy);                   //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-YY TO #ACCOUNTING-DATE-MCDY-YY
                ldaAdsl780.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm);                   //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-MM TO #ACCOUNTING-DATE-MCDY-MM
                ldaAdsl780.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd);                   //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-DD TO #ACCOUNTING-DATE-MCDY-DD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    //*  012604
    private void sub_Calculate_Tiaa_Accumulations() throws Exception                                                                                                      //Natural: CALCULATE-TIAA-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #MAX-RATE
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rate)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            //*  DEA
            pnd_Svsaa_Fund.resetInitial();                                                                                                                                //Natural: RESET INITIAL #SVSAA-FUND
            //*  EXAMINE ADC-T395-FUND-ID(*) FOR 'SA' GIVING #J        /* EM - 102313
            //*  EM 102313
            DbsUtil.examine(new ExamineSource(ldaAdsl401a.getAds_Cntrct_View_Adc_Invstmnt_Grpng_Id().getValue("*")), new ExamineSearch("TSV0"), new ExamineGivingNumber(pnd_J)); //Natural: EXAMINE ADC-INVSTMNT-GRPNG-ID ( * ) FOR 'TSV0' GIVING #J
            //*  DEA
            if (condition(pnd_J.notEquals(getZero())))                                                                                                                    //Natural: IF #J NE 0
            {
                //*  DEA
                pnd_Svsaa_Fund.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #SVSAA-FUND
                //*  DEA
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).notEquals(getZero())))                          //Natural: IF ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) NE 0
            {
                if (condition(pnd_Nap_Annty_Optn.equals("TC") || pnd_Nap_Annty_Optn.equals("TR") || pnd_Nap_Annty_Optn.equals("TE")))                                     //Natural: IF #NAP-ANNTY-OPTN = 'TC' OR = 'TR' OR = 'TE'
                {
                                                                                                                                                                          //Natural: PERFORM BUCKET-TPA-STANDARD-ACCUMULATION
                    sub_Bucket_Tpa_Standard_Accumulation();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Nap_Annty_Optn.equals("IO")))                                                                                                       //Natural: IF #NAP-ANNTY-OPTN = 'IO'
                    {
                                                                                                                                                                          //Natural: PERFORM BUCKET-IPRO-STANDARD-ACCUMULATION
                        sub_Bucket_Ipro_Standard_Accumulation();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  DEA START
                        if (condition(pnd_Svsaa_Fund.getBoolean()))                                                                                                       //Natural: IF #SVSAA-FUND
                        {
                                                                                                                                                                          //Natural: PERFORM BUCKET-TIAA-STANDARD-ACCUMULA-SV
                            sub_Bucket_Tiaa_Standard_Accumula_Sv();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                                                                                                                                                                          //Natural: PERFORM BUCKET-TIAA-STANDARD-ACCUMULATION
                            sub_Bucket_Tiaa_Standard_Accumulation();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*  DEA END
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  TIAA GRADED
            if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).notEquals(getZero())))                             //Natural: IF ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) NE 0
            {
                if (condition(pnd_Svsaa_Fund.getBoolean()))                                                                                                               //Natural: IF #SVSAA-FUND
                {
                                                                                                                                                                          //Natural: PERFORM BUCKET-TIAA-GRADED-ACCUMULA-SV
                    sub_Bucket_Tiaa_Graded_Accumula_Sv();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM BUCKET-TIAA-GRADED-ACCUMULATION
                    sub_Bucket_Tiaa_Graded_Accumulation();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  DEA END
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Bucket_Tiaa_Standard_Accumulation() throws Exception                                                                                                 //Natural: BUCKET-TIAA-STANDARD-ACCUMULATION
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  RS0
        if (condition(pnd_Stbl_Fund.getBoolean()))                                                                                                                        //Natural: IF #STBL-FUND
        {
            ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Cnt().nadd(1);                                                                              //Natural: ADD 1 TO #TIAA-GRN-STBL-STD-MATURITY-CNT
            ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-STBL-STD-MATURITY-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Cnt().nadd(1);                                                                                   //Natural: ADD 1 TO #TIAA-GRN-STD-MATURITY-CNT
            ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-STD-MATURITY-AMT
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #TIAA-SUB-GRN-MATURITY-CNT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-GRN-MATURITY-AMT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #GRAND-MATURITY-AMT
    }
    private void sub_Bucket_Tiaa_Standard_Accumula_Sv() throws Exception                                                                                                  //Natural: BUCKET-TIAA-STANDARD-ACCUMULA-SV
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  RS0
        if (condition(pnd_Stbl_Fund.getBoolean()))                                                                                                                        //Natural: IF #STBL-FUND
        {
            ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Csv().nadd(1);                                                                              //Natural: ADD 1 TO #TIAA-GRN-STBL-STD-MATURITY-CSV
            ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Asv().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-STBL-STD-MATURITY-ASV
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Cnt().nadd(1);                                                                                   //Natural: ADD 1 TO #TIAA-GRN-STD-MATURITY-CNT
            ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-STD-MATURITY-AMT
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #TIAA-SUB-GRN-MATURITY-CNT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-GRN-MATURITY-AMT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #GRAND-MATURITY-AMT
    }
    private void sub_Bucket_Tpa_Rollover_Accumulation() throws Exception                                                                                                  //Natural: BUCKET-TPA-ROLLOVER-ACCUMULATION
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Tpa_Rlvr_Cnt().nadd(1);                                                                                           //Natural: ADD 1 TO #TIAA-GRN-TPA-RLVR-CNT
        ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Tpa_Rlvr_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-TPA-RLVR-AMT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Rtb_Rlvr_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #GRAND-RATE-RTB-RLVR-CNT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rtb_Rlvr_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #GRAND-RTB-RLVR-AMT
    }
    private void sub_Bucket_Tpa_Standard_Accumulation() throws Exception                                                                                                  //Natural: BUCKET-TPA-STANDARD-ACCUMULATION
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Tpa_Accm_Cnt().nadd(1);                                                                                           //Natural: ADD 1 TO #TIAA-GRN-TPA-ACCM-CNT
        ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Tpa_Accm_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-TPA-ACCM-AMT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #GRAND-MATURITY-AMT
    }
    private void sub_Bucket_Ipro_Rollover_Accumulation() throws Exception                                                                                                 //Natural: BUCKET-IPRO-ROLLOVER-ACCUMULATION
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Io_Rlvr_Cnt().nadd(1);                                                                                            //Natural: ADD 1 TO #TIAA-GRN-IO-RLVR-CNT
        ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Io_Rlvr_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-IO-RLVR-AMT
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Rlvr_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #TIAA-SUB-GRN-RTB-RLVR-CNT
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Rlvr_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-GRN-RTB-RLVR-AMT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Rtb_Rlvr_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #GRAND-RATE-RTB-RLVR-CNT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rtb_Rlvr_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #GRAND-RTB-RLVR-AMT
    }
    private void sub_Bucket_Ipro_Standard_Accumulation() throws Exception                                                                                                 //Natural: BUCKET-IPRO-STANDARD-ACCUMULATION
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Io_Accm_Cnt().nadd(1);                                                                                            //Natural: ADD 1 TO #TIAA-GRN-IO-ACCM-CNT
        ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Io_Accm_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-IO-ACCM-AMT
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #TIAA-SUB-GRN-MATURITY-CNT
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-GRN-MATURITY-AMT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #B-INDX ) TO #GRAND-MATURITY-AMT
    }
    private void sub_Bucket_Tiaa_Graded_Accumulation() throws Exception                                                                                                   //Natural: BUCKET-TIAA-GRADED-ACCUMULATION
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  RS0
        if (condition(pnd_Stbl_Fund.getBoolean()))                                                                                                                        //Natural: IF #STBL-FUND
        {
            ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Cnt().nadd(1);                                                                              //Natural: ADD 1 TO #TIAA-GRN-STBL-GRD-MATURITY-CNT
            ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-STBL-GRD-MATURITY-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Cnt().nadd(1);                                                                                   //Natural: ADD 1 TO #TIAA-GRN-GRD-MATURITY-CNT
            ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-GRD-MATURITY-AMT
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #TIAA-SUB-GRN-MATURITY-CNT
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-GRN-MATURITY-AMT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #GRAND-MATURITY-AMT
    }
    //*  DEA START
    private void sub_Bucket_Tiaa_Graded_Accumula_Sv() throws Exception                                                                                                    //Natural: BUCKET-TIAA-GRADED-ACCUMULA-SV
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  RS0
        if (condition(pnd_Stbl_Fund.getBoolean()))                                                                                                                        //Natural: IF #STBL-FUND
        {
            ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Csv().nadd(1);                                                                              //Natural: ADD 1 TO #TIAA-GRN-STBL-GRD-MATURITY-CSV
            ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Asv().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-STBL-GRD-MATURITY-ASV
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Cnt().nadd(1);                                                                                   //Natural: ADD 1 TO #TIAA-GRN-GRD-MATURITY-CNT
            ldaAdsl780.getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-GRN-GRD-MATURITY-AMT
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt().nadd(1);                                                                                     //Natural: ADD 1 TO #TIAA-SUB-GRN-MATURITY-CNT
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #TIAA-SUB-GRN-MATURITY-AMT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                                //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #B-INDX ) TO #GRAND-MATURITY-AMT
    }
    private void sub_Calculate_Cref_Product_Accumulation() throws Exception                                                                                               //Natural: CALCULATE-CREF-PRODUCT-ACCUMULATION
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Cref_Prod_Found.setValue(false);                                                                                                                              //Natural: MOVE FALSE TO #CREF-PROD-FOUND
        FOR03:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            //*   IF #PEC-ACCT-NAME-1 (#B-INDX) = ' '    /* EM - 102313 START
            //*     ESCAPE ROUTINE
            //*   END-IF
            if (condition(pnd_Alpha_Fund_Cde.getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                                                              //Natural: IF #ALPHA-FUND-CDE ( #B-INDX ) = ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
                //*  EM - 102313 END
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  IF #PEC-ACCT-NAME-1 (#B-INDX) = ADC-ACCT-CDE (#A-INDX) /* EM - 102313
            //*  EM - 102313
            if (condition(pnd_Alpha_Fund_Cde.getValue(pnd_All_Indx_Pnd_B_Indx).equals(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx))))  //Natural: IF #ALPHA-FUND-CDE ( #B-INDX ) = ADC-ACCT-CDE ( #A-INDX )
            {
                pnd_Cref_Prod_Found.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO #CREF-PROD-FOUND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cref_Prod_Found.setValue(false);                                                                                                                      //Natural: MOVE FALSE TO #CREF-PROD-FOUND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero())))                           //Natural: IF ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) NE 0
                {
                    ldaAdsl780.getPnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                      //Natural: ADD 1 TO #CREF-GRN-MTH-MAT-CNT ( #B-INDX )
                    //*  RS0
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADC-ACCT-CDE ( #A-INDX ) NE 'R' AND ADC-ACCT-CDE ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl780.getPnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Cnt().nadd(1);                                                       //Natural: ADD 1 TO #CREF-SUB-GRN-MTH-MATURITY-CNT
                        ldaAdsl780.getPnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) TO #CREF-SUB-GRN-MTH-MATURITY-AMT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                    //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
                    ldaAdsl780.getPnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) TO #CREF-GRN-MTH-MAT-AMT ( #B-INDX )
                    ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-GRD-MNTHLY-ACTL-AMT ( #A-INDX ) TO #GRAND-MATURITY-AMT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals(getZero())))                          //Natural: IF ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) NE 0
                {
                    ldaAdsl780.getPnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                     //Natural: ADD 1 TO #CREF-GRN-ANN-MAT-CNT ( #B-INDX )
                    //*  RS0
                    if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("R") && ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Cde().getValue(pnd_All_Indx_Pnd_A_Indx).notEquals("D"))) //Natural: IF ADC-ACCT-CDE ( #A-INDX ) NE 'R' AND ADC-ACCT-CDE ( #A-INDX ) NE 'D'
                    {
                        ldaAdsl780.getPnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Cnt().nadd(1);                                                      //Natural: ADD 1 TO #CREF-SUB-GRN-ANN-MATURITY-CNT
                        ldaAdsl780.getPnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) TO #CREF-SUB-GRN-ANN-MATURITY-AMT
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt().nadd(1);                                                                    //Natural: ADD 1 TO #GRAND-RATE-MATURITY-CNT
                    ldaAdsl780.getPnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) TO #CREF-GRN-ANN-MAT-AMT ( #B-INDX )
                    ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt().nadd(ldaAdsl401a.getAds_Cntrct_View_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_All_Indx_Pnd_A_Indx)); //Natural: ADD ADC-STNDRD-ANNL-ACTL-AMT ( #A-INDX ) TO #GRAND-MATURITY-AMT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Built_Rollover_Totals() throws Exception                                                                                                             //Natural: BUILT-ROLLOVER-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  010511
        if (condition(pnd_Dest.getValue("*").equals("RTHC") || pnd_Dest.getValue("*").equals("RTHT") || pnd_Dest.getValue("*").equals("IRAC") || pnd_Dest.getValue("*").equals("IRAT")  //Natural: IF #DEST ( * ) = 'RTHC' OR = 'RTHT' OR = 'IRAC' OR = 'IRAT' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC'
            || pnd_Dest.getValue("*").equals("03BT") || pnd_Dest.getValue("*").equals("03BC") || pnd_Dest.getValue("*").equals("QPLT") || pnd_Dest.getValue("*").equals("QPLC")))
        {
            FOR04:                                                                                                                                                        //Natural: FOR #B-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
            for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
            {
                //*    IF #PEC-ACCT-NAME-1 (#B-INDX) = #PROD-CDE (#POS) /* EM - 102313
                //*  EM - 102313
                if (condition(pnd_Alpha_Fund_Cde.getValue(pnd_All_Indx_Pnd_B_Indx).equals(pnd_Prod_Cde.getValue(pnd_All_Indx_Pnd_Pos))))                                  //Natural: IF #ALPHA-FUND-CDE ( #B-INDX ) = #PROD-CDE ( #POS )
                {
                    if (condition(pnd_Rlvr_Amt.getValue(pnd_All_Indx_Pnd_Pos).greater(getZero())))                                                                        //Natural: IF #RLVR-AMT ( #POS ) > 0
                    {
                        ldaAdsl780.getPnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                //Natural: ADD 1 TO #CREF-ANN-RTB-RLVR-CNT ( #B-INDX )
                        //*  RS0
                        if (condition(pnd_Prod_Cde.getValue(pnd_All_Indx_Pnd_Pos).notEquals("R") && pnd_Prod_Cde.getValue(pnd_All_Indx_Pnd_Pos).notEquals("D")))          //Natural: IF #PROD-CDE ( #POS ) NE 'R' AND #PROD-CDE ( #POS ) NE 'D'
                        {
                            ldaAdsl780.getPnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Rtb_Rlvr_Cnt().nadd(1);                                                 //Natural: ADD 1 TO #CREF-SUB-RATE-ANN-RTB-RLVR-CNT
                            ldaAdsl780.getPnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Rlvr_Cnt().nadd(1);                                                  //Natural: ADD 1 TO #CREF-SUB-GRN-ANN-RTB-RLVR-CNT
                            ldaAdsl780.getPnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Rtb_Rlvr_Amt().nadd(pnd_Rlvr_Amt.getValue(pnd_All_Indx_Pnd_Pos));            //Natural: ADD #RLVR-AMT ( #POS ) TO #CREF-SUB-ANN-RTB-RLVR-AMT
                            ldaAdsl780.getPnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Rlvr_Amt().nadd(pnd_Rlvr_Amt.getValue(pnd_All_Indx_Pnd_Pos));        //Natural: ADD #RLVR-AMT ( #POS ) TO #CREF-SUB-GRN-ANN-RTB-RLVR-AMT
                        }                                                                                                                                                 //Natural: END-IF
                        ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Rtb_Rlvr_Cnt().nadd(1);                                                                //Natural: ADD 1 TO #TOTAL-RATE-RTB-RLVR-CNT
                        ldaAdsl780.getPnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(1);                                 //Natural: ADD 1 TO #CREF-GRN-ANN-RTB-RLC ( #B-INDX )
                        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Rtb_Rlvr_Cnt().nadd(1);                                                                //Natural: ADD 1 TO #GRAND-RATE-RTB-RLVR-CNT
                        ldaAdsl780.getPnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(pnd_Rlvr_Amt.getValue(pnd_All_Indx_Pnd_Pos)); //Natural: ADD #RLVR-AMT ( #POS ) TO #CREF-ANN-RTB-RLVR-AMT ( #B-INDX )
                        ldaAdsl780.getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rtb_Rlvr_Amt().nadd(pnd_Rlvr_Amt.getValue(pnd_All_Indx_Pnd_Pos));                           //Natural: ADD #RLVR-AMT ( #POS ) TO #TOTAL-RTB-RLVR-AMT
                        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rtb_Rlvr_Amt().nadd(pnd_Rlvr_Amt.getValue(pnd_All_Indx_Pnd_Pos));                           //Natural: ADD #RLVR-AMT ( #POS ) TO #GRAND-RTB-RLVR-AMT
                        ldaAdsl780.getPnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla().getValue(pnd_All_Indx_Pnd_B_Indx).nadd(pnd_Rlvr_Amt.getValue(pnd_All_Indx_Pnd_Pos)); //Natural: ADD #RLVR-AMT ( #POS ) TO #CREF-GRN-ANN-RTB-RLA ( #B-INDX )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Break_Of_Accounting_Date_Rtn() throws Exception                                                                                                      //Natural: BREAK-OF-ACCOUNTING-DATE-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  GG050608
        if (condition(! (pnd_Roth_Only_Run.getBoolean())))                                                                                                                //Natural: IF NOT #ROTH-ONLY-RUN
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm464.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM464'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  GG050608
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm464.class));                                                                           //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM464'
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl780.getPnd_Tiaa_Grand_Accum().reset();                                                                                                                     //Natural: RESET #TIAA-GRAND-ACCUM #TIAA-SUB-GRN-ACCUM #CREF-MONTHLY-GRAND-ACCUM ( * ) #CREF-ANNUALLY-GRAND-ACCUM ( * ) #CREF-MONTHLY-SUB-GRAND-ACCUM #CREF-ANNUALLY-SUB-GRAND-ACCUM #CREF-AND-TIAA-GRAND-ACCUM
        ldaAdsl780.getPnd_Tiaa_Sub_Grn_Accum().reset();
        ldaAdsl780.getPnd_Cref_Monthly_Grand_Accum().getValue("*").reset();
        ldaAdsl780.getPnd_Cref_Annually_Grand_Accum().getValue("*").reset();
        ldaAdsl780.getPnd_Cref_Monthly_Sub_Grand_Accum().reset();
        ldaAdsl780.getPnd_Cref_Annually_Sub_Grand_Accum().reset();
        ldaAdsl780.getPnd_Cref_And_Tiaa_Grand_Accum().reset();
    }
    private void sub_Break_Of_Settlement_Type() throws Exception                                                                                                          //Natural: BREAK-OF-SETTLEMENT-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  GG050608
        if (condition(! (pnd_Roth_Only_Run.getBoolean())))                                                                                                                //Natural: IF NOT #ROTH-ONLY-RUN
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm465.class));                                                                           //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM465'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm465.class));                                                                           //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM465'
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_End_Of_Job_Rtn() throws Exception                                                                                                                    //Natural: END-OF-JOB-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        if (condition(! (pnd_Roth_Only_Run.getBoolean())))                                                                                                                //Natural: IF NOT #ROTH-ONLY-RUN
        {
            getReports().write(1, ReportOption.NOTITLE,"**********************************************************",NEWLINE,"*   ADSP785 (DAILY ACTIVITY MATURITIES)- JOB RUN ENDED   *"); //Natural: WRITE ( 1 ) '**********************************************************' / '*   ADSP785 (DAILY ACTIVITY MATURITIES)- JOB RUN ENDED   *'
            if (Global.isEscape()) return;
            if (condition(pnd_Total_Number_Rec.equals(getZero())))                                                                                                        //Natural: IF #TOTAL-NUMBER-REC = 0
            {
                getReports().write(1, ReportOption.NOTITLE,"*            NO TRANSACTIONS PROCESSED TODAY             *");                                                 //Natural: WRITE ( 1 ) '*            NO TRANSACTIONS PROCESSED TODAY             *'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(1, ReportOption.NOTITLE,"*   NUMBER OF TRANSACTIONS PROCESSED TODAY :",pnd_Total_Number_Rec, new ReportEditMask ("ZZ,ZZZ,ZZ9"),        //Natural: WRITE ( 1 ) '*   NUMBER OF TRANSACTIONS PROCESSED TODAY :' #TOTAL-NUMBER-REC ( EM = ZZ,ZZZ,ZZ9 ) '  *'
                    "  *");
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,"**********************************************************");                                                     //Natural: WRITE ( 1 ) '**********************************************************'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  GG050608
            getReports().write(2, ReportOption.NOTITLE,"**********************************************************",NEWLINE,"*   ADSP785 (DAILY ACTIVITY MATURITIES)- JOB RUN ENDED   *"); //Natural: WRITE ( 2 ) '**********************************************************' / '*   ADSP785 (DAILY ACTIVITY MATURITIES)- JOB RUN ENDED   *'
            if (Global.isEscape()) return;
            if (condition(pnd_Total_Number_Rec_Roth.equals(getZero())))                                                                                                   //Natural: IF #TOTAL-NUMBER-REC-ROTH = 0
            {
                getReports().write(2, ReportOption.NOTITLE,"*       NO ROTH TRANSACTIONS PROCESSED TODAY             *");                                                 //Natural: WRITE ( 2 ) '*       NO ROTH TRANSACTIONS PROCESSED TODAY             *'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(2, ReportOption.NOTITLE,"* NUMBER OF ROTH TRANS PROCESSED TODAY:       ",pnd_Total_Number_Rec_Roth, new ReportEditMask                 //Natural: WRITE ( 2 ) '* NUMBER OF ROTH TRANS PROCESSED TODAY:       ' #TOTAL-NUMBER-REC-ROTH ( EM = ZZ,ZZZ,ZZ9 ) '*'
                    ("ZZ,ZZZ,ZZ9"),"*");
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(2, ReportOption.NOTITLE,"**********************************************************");                                                     //Natural: WRITE ( 2 ) '**********************************************************'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Heading.setValue("       Annuitization of OmniPlus Contracts        ");                                                                           //Natural: ASSIGN #HEADING := '       Annuitization of OmniPlus Contracts        '
                    if (condition(! (pnd_Roth_Only_Run.getBoolean())))                                                                                                    //Natural: IF NOT #ROTH-ONLY-RUN
                    {
                        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm465.class));                                                               //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM465'
                    }                                                                                                                                                     //Natural: END-IF
                    //*  GG050608
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Heading.setValue("Annuitization of OmniPlus Roth 403b/401k Contracts");                                                                           //Natural: ASSIGN #HEADING := 'Annuitization of OmniPlus Roth 403b/401k Contracts'
                    if (condition(pnd_Roth_Only_Run.getBoolean()))                                                                                                        //Natural: IF #ROTH-ONLY-RUN
                    {
                        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm465.class));                                                               //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM465'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=79 PS=60");
        Global.format(1, "LS=79 PS=60");
        Global.format(2, "LS=79 PS=60");
    }
}
