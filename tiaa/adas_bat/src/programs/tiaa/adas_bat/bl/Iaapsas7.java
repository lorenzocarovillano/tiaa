/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:35:32 PM
**        * FROM NATURAL PROGRAM : Iaapsas7
************************************************************
**        * FILE NAME            : Iaapsas7.java
**        * CLASS NAME           : Iaapsas7
**        * INSTANCE NAME        : Iaapsas7
************************************************************
************************************************************************
* PROGRAM  : IAAPSAS7
* FUNCTION : READ IA SECURITY AND PRODUCE A FILE FOR SAS70
*            ENTITLEMENT REVIEW.
*
* HISTORY
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Iaapsas7 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_scrty;
    private DbsField scrty_Trans_Dte;
    private DbsField scrty_Scrty_User_Id;
    private DbsField scrty_Scrty_User_Area;
    private DbsField scrty_Scrty_Verify_Cde;

    private DataAccessProgramView vw_cwf_Org_Empl_Tbl;
    private DbsField cwf_Org_Empl_Tbl_Empl_Unit_Cde;
    private DbsField cwf_Org_Empl_Tbl_Empl_Racf_Id;
    private DbsField cwf_Org_Empl_Tbl_Empl_Nme;

    private DbsGroup cwf_Org_Empl_Tbl__R_Field_1;
    private DbsField cwf_Org_Empl_Tbl_Empl_First_Nme;
    private DbsField cwf_Org_Empl_Tbl_Empl_Last_Nme;
    private DbsField cwf_Org_Empl_Tbl_Dlte_Dte_Tme;
    private DbsField cwf_Org_Empl_Tbl_Default_Unit_Ind;

    private DataAccessProgramView vw_cwf_Org_Unit_Tbl;
    private DbsField cwf_Org_Unit_Tbl_Unit_Cde;
    private DbsField cwf_Org_Unit_Tbl_Unit_Short_Nme;
    private DbsField cwf_Org_Unit_Tbl_Unit_Long_Nme;
    private DbsField cwf_Org_Unit_Tbl_Unit_Floor_Nbr;
    private DbsField cwf_Org_Unit_Tbl_Unit_Bldg_Nbr_Brnch_Loc;
    private DbsField cwf_Org_Unit_Tbl_Unit_Drop_Off_Point;
    private DbsField cwf_Org_Unit_Tbl_Unit_Bdgt_Cde;
    private DbsField cwf_Org_Unit_Tbl_Unit_Sprvsr_Racf_Id;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;
    private DbsGroup naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup;
    private DbsField naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte;
    private DbsField naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id;
    private DbsField pnd_Naz_Table_Key;

    private DbsGroup pnd_Naz_Table_Key__R_Field_2;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id;

    private DbsGroup pnd_Print_File;
    private DbsField pnd_Print_File_Pnd_User_Id;
    private DbsField pnd_Print_File_Pnd_Lname;
    private DbsField pnd_Print_File_Pnd_Fname;
    private DbsField pnd_Print_File_Pnd_Func_Grp;
    private DbsField pnd_Work_File;
    private DbsField pnd_Comma;
    private DbsField pnd_Adat_Cnt;
    private DbsField pnd_I;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_scrty = new DataAccessProgramView(new NameInfo("vw_scrty", "SCRTY"), "IAA_SCRTY_RCRD", "IA_TRANS_FILE");
        scrty_Trans_Dte = vw_scrty.getRecord().newFieldInGroup("scrty_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, "TRANS_DTE");
        scrty_Scrty_User_Id = vw_scrty.getRecord().newFieldInGroup("scrty_Scrty_User_Id", "SCRTY-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "SCRTY_USER_ID");
        scrty_Scrty_User_Area = vw_scrty.getRecord().newFieldInGroup("scrty_Scrty_User_Area", "SCRTY-USER-AREA", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SCRTY_USER_AREA");
        scrty_Scrty_Verify_Cde = vw_scrty.getRecord().newFieldInGroup("scrty_Scrty_Verify_Cde", "SCRTY-VERIFY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "SCRTY_VERIFY_CDE");
        registerRecord(vw_scrty);

        vw_cwf_Org_Empl_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Org_Empl_Tbl", "CWF-ORG-EMPL-TBL"), "CWF_ORG_EMPL_TBL", "CWF_ASSIGN_RULE");
        cwf_Org_Empl_Tbl_Empl_Unit_Cde = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Unit_Cde", "EMPL-UNIT-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "EMPL_UNIT_CDE");
        cwf_Org_Empl_Tbl_Empl_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Org_Empl_Tbl_Empl_Racf_Id = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Racf_Id", "EMPL-RACF-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "EMPL_RACF_ID");
        cwf_Org_Empl_Tbl_Empl_Racf_Id.setDdmHeader("EMPLOYEE/RACF  ID");
        cwf_Org_Empl_Tbl_Empl_Nme = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Nme", "EMPL-NME", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "EMPL_NME");
        cwf_Org_Empl_Tbl_Empl_Nme.setDdmHeader("EMPLOYEE NAME");

        cwf_Org_Empl_Tbl__R_Field_1 = vw_cwf_Org_Empl_Tbl.getRecord().newGroupInGroup("cwf_Org_Empl_Tbl__R_Field_1", "REDEFINE", cwf_Org_Empl_Tbl_Empl_Nme);
        cwf_Org_Empl_Tbl_Empl_First_Nme = cwf_Org_Empl_Tbl__R_Field_1.newFieldInGroup("cwf_Org_Empl_Tbl_Empl_First_Nme", "EMPL-FIRST-NME", FieldType.STRING, 
            20);
        cwf_Org_Empl_Tbl_Empl_Last_Nme = cwf_Org_Empl_Tbl__R_Field_1.newFieldInGroup("cwf_Org_Empl_Tbl_Empl_Last_Nme", "EMPL-LAST-NME", FieldType.STRING, 
            20);
        cwf_Org_Empl_Tbl_Dlte_Dte_Tme = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Dlte_Dte_Tme", "DLTE-DTE-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "DLTE_DTE_TME");
        cwf_Org_Empl_Tbl_Dlte_Dte_Tme.setDdmHeader("DELETE/DATE-TIME");
        cwf_Org_Empl_Tbl_Default_Unit_Ind = vw_cwf_Org_Empl_Tbl.getRecord().newFieldInGroup("cwf_Org_Empl_Tbl_Default_Unit_Ind", "DEFAULT-UNIT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "DEFAULT_UNIT_IND");
        registerRecord(vw_cwf_Org_Empl_Tbl);

        vw_cwf_Org_Unit_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Org_Unit_Tbl", "CWF-ORG-UNIT-TBL"), "CWF_ORG_UNIT_TBL", "CWF_ASSIGN_RULE");
        cwf_Org_Unit_Tbl_Unit_Cde = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Cde", "UNIT-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "UNIT_CDE");
        cwf_Org_Unit_Tbl_Unit_Cde.setDdmHeader("UNIT/CODE");
        cwf_Org_Unit_Tbl_Unit_Short_Nme = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Short_Nme", "UNIT-SHORT-NME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "UNIT_SHORT_NME");
        cwf_Org_Unit_Tbl_Unit_Short_Nme.setDdmHeader("UNIT SHORT NAME");
        cwf_Org_Unit_Tbl_Unit_Long_Nme = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Long_Nme", "UNIT-LONG-NME", FieldType.STRING, 
            45, RepeatingFieldStrategy.None, "UNIT_LONG_NME");
        cwf_Org_Unit_Tbl_Unit_Long_Nme.setDdmHeader("UNIT LONG NAME");
        cwf_Org_Unit_Tbl_Unit_Floor_Nbr = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Floor_Nbr", "UNIT-FLOOR-NBR", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "UNIT_FLOOR_NBR");
        cwf_Org_Unit_Tbl_Unit_Floor_Nbr.setDdmHeader("FLOOR/NUMBER");
        cwf_Org_Unit_Tbl_Unit_Bldg_Nbr_Brnch_Loc = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Bldg_Nbr_Brnch_Loc", "UNIT-BLDG-NBR-BRNCH-LOC", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "UNIT_BLDG_NBR_BRNCH_LOC");
        cwf_Org_Unit_Tbl_Unit_Bldg_Nbr_Brnch_Loc.setDdmHeader("BLDG NO //BR. CODE");
        cwf_Org_Unit_Tbl_Unit_Drop_Off_Point = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Drop_Off_Point", "UNIT-DROP-OFF-POINT", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "UNIT_DROP_OFF_POINT");
        cwf_Org_Unit_Tbl_Unit_Bdgt_Cde = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Bdgt_Cde", "UNIT-BDGT-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "UNIT_BDGT_CDE");
        cwf_Org_Unit_Tbl_Unit_Bdgt_Cde.setDdmHeader("BUDGET/CODE");
        cwf_Org_Unit_Tbl_Unit_Sprvsr_Racf_Id = vw_cwf_Org_Unit_Tbl.getRecord().newFieldInGroup("cwf_Org_Unit_Tbl_Unit_Sprvsr_Racf_Id", "UNIT-SPRVSR-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "UNIT_SPRVSR_RACF_ID");
        cwf_Org_Unit_Tbl_Unit_Sprvsr_Racf_Id.setDdmHeader("SUPERVISOR/RACF/ID");
        registerRecord(vw_cwf_Org_Unit_Tbl);

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup = vw_naz_Table_Ddm.getRecord().newGroupInGroup("NAZ_TABLE_DDM_NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", 
            "NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt = naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup.newFieldArrayInGroup("naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt", 
            "NAZ-TBL-SECNDRY-DSCRPTN-TXT", FieldType.STRING, 80, new DbsArrayController(1, 2), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt.setDdmHeader("CDE/2ND/DSC");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte", "NAZ-TBL-RCRD-UPDT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_UPDT_DTE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte.setDdmHeader("LST UPDT");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id", "NAZ-TBL-UPDT-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "NAZ_TBL_UPDT_RACF_ID");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id.setDdmHeader("UPDT BY");
        registerRecord(vw_naz_Table_Ddm);

        pnd_Naz_Table_Key = localVariables.newFieldInRecord("pnd_Naz_Table_Key", "#NAZ-TABLE-KEY", FieldType.STRING, 30);

        pnd_Naz_Table_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Naz_Table_Key__R_Field_2", "REDEFINE", pnd_Naz_Table_Key);
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind = pnd_Naz_Table_Key__R_Field_2.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind", "#NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id = pnd_Naz_Table_Key__R_Field_2.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id", "#NAZ-TABLE-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id = pnd_Naz_Table_Key__R_Field_2.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id", "#NAZ-TABLE-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id = pnd_Naz_Table_Key__R_Field_2.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id", "#NAZ-TABLE-LVL3-ID", 
            FieldType.STRING, 20);

        pnd_Print_File = localVariables.newGroupInRecord("pnd_Print_File", "#PRINT-FILE");
        pnd_Print_File_Pnd_User_Id = pnd_Print_File.newFieldInGroup("pnd_Print_File_Pnd_User_Id", "#USER-ID", FieldType.STRING, 10);
        pnd_Print_File_Pnd_Lname = pnd_Print_File.newFieldInGroup("pnd_Print_File_Pnd_Lname", "#LNAME", FieldType.STRING, 20);
        pnd_Print_File_Pnd_Fname = pnd_Print_File.newFieldInGroup("pnd_Print_File_Pnd_Fname", "#FNAME", FieldType.STRING, 20);
        pnd_Print_File_Pnd_Func_Grp = pnd_Print_File.newFieldInGroup("pnd_Print_File_Pnd_Func_Grp", "#FUNC-GRP", FieldType.STRING, 10);
        pnd_Work_File = localVariables.newFieldInRecord("pnd_Work_File", "#WORK-FILE", FieldType.STRING, 80);
        pnd_Comma = localVariables.newFieldInRecord("pnd_Comma", "#COMMA", FieldType.STRING, 1);
        pnd_Adat_Cnt = localVariables.newFieldInRecord("pnd_Adat_Cnt", "#ADAT-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_scrty.reset();
        vw_cwf_Org_Empl_Tbl.reset();
        vw_cwf_Org_Unit_Tbl.reset();
        vw_naz_Table_Ddm.reset();

        localVariables.reset();
        pnd_Comma.setInitialValue(",");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Iaapsas7() throws Exception
    {
        super("Iaapsas7");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 60 LS = 133;//Natural: FORMAT ( 2 ) PS = 60 LS = 133
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        vw_scrty.startDatabaseRead                                                                                                                                        //Natural: READ SCRTY BY SCRTY-USER-ID STARTING FROM ' '
        (
        "READ01",
        new Wc[] { new Wc("SCRTY_USER_ID", ">=", " ", WcType.BY) },
        new Oc[] { new Oc("SCRTY_USER_ID", "ASC") }
        );
        READ01:
        while (condition(vw_scrty.readNextRow("READ01")))
        {
            pnd_Print_File_Pnd_User_Id.setValue(scrty_Scrty_User_Id);                                                                                                     //Natural: ASSIGN #USER-ID := SCRTY-USER-ID
            pnd_Print_File_Pnd_Func_Grp.setValue(scrty_Scrty_User_Area);                                                                                                  //Natural: ASSIGN #FUNC-GRP := SCRTY-USER-AREA
                                                                                                                                                                          //Natural: PERFORM GET-NAME
            sub_Get_Name();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Work_File.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Print_File_Pnd_User_Id, pnd_Comma, pnd_Print_File_Pnd_Lname, pnd_Comma,            //Natural: COMPRESS #USER-ID #COMMA #LNAME #COMMA #FNAME #COMMA #FUNC-GRP INTO #WORK-FILE LEAVE NO
                pnd_Print_File_Pnd_Fname, pnd_Comma, pnd_Print_File_Pnd_Func_Grp));
            getWorkFiles().write(1, false, pnd_Work_File);                                                                                                                //Natural: WRITE WORK FILE 1 #WORK-FILE
            getReports().display(1, "USERID",                                                                                                                             //Natural: DISPLAY ( 1 ) 'USERID' #USER-ID 'LNAME' #LNAME 'FNAME' #FNAME 'FUNC GROUP' #FUNC-GRP
            		pnd_Print_File_Pnd_User_Id,"LNAME",
            		pnd_Print_File_Pnd_Lname,"FNAME",
            		pnd_Print_File_Pnd_Fname,"FUNC GROUP",
            		pnd_Print_File_Pnd_Func_Grp);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM READ-SUPER-TABLE
        sub_Read_Super_Table();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-SUPER-TABLE
        //* ***********************************************************************
    }
    private void sub_Get_Name() throws Exception                                                                                                                          //Natural: GET-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Print_File_Pnd_Lname.setValue("N/A");                                                                                                                         //Natural: MOVE 'N/A' TO #LNAME #FNAME
        pnd_Print_File_Pnd_Fname.setValue("N/A");
        vw_cwf_Org_Empl_Tbl.startDatabaseRead                                                                                                                             //Natural: READ CWF-ORG-EMPL-TBL WITH EMPL-RACF-ID-KEY = #USER-ID
        (
        "READ02",
        new Wc[] { new Wc("EMPL_RACF_ID_KEY", ">=", pnd_Print_File_Pnd_User_Id, WcType.BY) },
        new Oc[] { new Oc("EMPL_RACF_ID_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_cwf_Org_Empl_Tbl.readNextRow("READ02")))
        {
            if (condition(cwf_Org_Empl_Tbl_Empl_Racf_Id.notEquals(pnd_Print_File_Pnd_User_Id)))                                                                           //Natural: IF EMPL-RACF-ID NE #USER-ID
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(cwf_Org_Empl_Tbl_Default_Unit_Ind.equals("Y"))))                                                                                              //Natural: ACCEPT IF DEFAULT-UNIT-IND = 'Y'
            {
                continue;
            }
            pnd_Print_File_Pnd_Lname.setValue(cwf_Org_Empl_Tbl_Empl_Last_Nme);                                                                                            //Natural: ASSIGN #LNAME := EMPL-LAST-NME
            pnd_Print_File_Pnd_Fname.setValue(cwf_Org_Empl_Tbl_Empl_First_Nme);                                                                                           //Natural: ASSIGN #FNAME := EMPL-FIRST-NME
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_Super_Table() throws Exception                                                                                                                  //Natural: READ-SUPER-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                         //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ038");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ038'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("IAS");                                                                                                          //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'IAS'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id.setValue(" ");                                                                                                            //Natural: ASSIGN #NAZ-TABLE-LVL3-ID := ' '
        vw_naz_Table_Ddm.startDatabaseRead                                                                                                                                //Natural: READ NAZ-TABLE-DDM BY NAZ-TBL-SUPER3 = #NAZ-TABLE-KEY
        (
        "READ03",
        new Wc[] { new Wc("NAZ_TBL_SUPER3", ">=", pnd_Naz_Table_Key, WcType.BY) },
        new Oc[] { new Oc("NAZ_TBL_SUPER3", "ASC") }
        );
        READ03:
        while (condition(vw_naz_Table_Ddm.readNextRow("READ03")))
        {
            if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.notEquals("NAZ038") || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.notEquals("IAS")))                                 //Natural: IF NAZ-TBL-RCRD-LVL1-ID NE 'NAZ038' OR NAZ-TBL-RCRD-LVL2-ID NE 'IAS'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.notEquals(" ") && naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.notEquals(" ")))                                    //Natural: IF NAZ-TBL-RCRD-LVL3-ID NE ' ' AND NAZ-TBL-RCRD-DSCRPTN-TXT NE ' '
            {
                pnd_Print_File_Pnd_User_Id.setValue(naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id);                                                                                  //Natural: ASSIGN #USER-ID := NAZ-TBL-RCRD-LVL3-ID
                pnd_Print_File_Pnd_Func_Grp.setValue(naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt);                                                                             //Natural: ASSIGN #FUNC-GRP := NAZ-TBL-RCRD-DSCRPTN-TXT
                                                                                                                                                                          //Natural: PERFORM GET-NAME
                sub_Get_Name();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Work_File.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Print_File_Pnd_User_Id, pnd_Comma, pnd_Print_File_Pnd_Lname,                   //Natural: COMPRESS #USER-ID #COMMA #LNAME #COMMA #FNAME #COMMA #FUNC-GRP INTO #WORK-FILE LEAVE NO
                    pnd_Comma, pnd_Print_File_Pnd_Fname, pnd_Comma, pnd_Print_File_Pnd_Func_Grp));
                getWorkFiles().write(2, false, pnd_Work_File);                                                                                                            //Natural: WRITE WORK FILE 2 #WORK-FILE
                getReports().display(2, "USERID",                                                                                                                         //Natural: DISPLAY ( 2 ) 'USERID' #USER-ID 'LNAME' #LNAME 'FNAME' #FNAME 'FUNC GROUP' #FUNC-GRP
                		pnd_Print_File_Pnd_User_Id,"LNAME",
                		pnd_Print_File_Pnd_Lname,"FNAME",
                		pnd_Print_File_Pnd_Fname,"FUNC GROUP",
                		pnd_Print_File_Pnd_Func_Grp);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, new ColumnSpacing(10),"IAIQ ENTITLEMENTS REPORT",new ColumnSpacing(10),Global.getDATX(), new ReportEditMask                     //Natural: WRITE ( 1 ) 10X 'IAIQ ENTITLEMENTS REPORT' 10X *DATX ( EM = MM/DD/YYYY )
                        ("MM/DD/YYYY"));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, new ColumnSpacing(10),"IASM ENTITLEMENTS REPORT",new ColumnSpacing(10),Global.getDATX(), new ReportEditMask                     //Natural: WRITE ( 2 ) 10X 'IASM ENTITLEMENTS REPORT' 10X *DATX ( EM = MM/DD/YYYY )
                        ("MM/DD/YYYY"));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133");
        Global.format(2, "PS=60 LS=133");

        getReports().setDisplayColumns(1, "USERID",
        		pnd_Print_File_Pnd_User_Id,"LNAME",
        		pnd_Print_File_Pnd_Lname,"FNAME",
        		pnd_Print_File_Pnd_Fname,"FUNC GROUP",
        		pnd_Print_File_Pnd_Func_Grp);
        getReports().setDisplayColumns(2, "USERID",
        		pnd_Print_File_Pnd_User_Id,"LNAME",
        		pnd_Print_File_Pnd_Lname,"FNAME",
        		pnd_Print_File_Pnd_Fname,"FUNC GROUP",
        		pnd_Print_File_Pnd_Func_Grp);
    }
}
