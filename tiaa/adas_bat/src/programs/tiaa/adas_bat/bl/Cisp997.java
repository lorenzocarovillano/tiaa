/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:23:26 PM
**        * FROM NATURAL PROGRAM : Cisp997
************************************************************
**        * FILE NAME            : Cisp997.java
**        * CLASS NAME           : Cisp997
**        * INSTANCE NAME        : Cisp997
************************************************************
************************************************************************
* FIX THE  CALC BENE INFO FOR LEGALLY SPLIT CONTRACTS
*
*
* 05/11/17  (BABRE)   PIN EXPANSION CHANGES. (C420007)         PINE
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisp997 extends BLNatBase
{
    // Data Areas
    private PdaNmda8510 pdaNmda8510;
    private PdaNmdpda_M pdaNmdpda_M;
    private PdaNmdpda_E pdaNmdpda_E;
    private LdaCisl2021 ldaCisl2021;
    private LdaCisl2081 ldaCisl2081;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_bene_Contract;
    private DbsField bene_Contract_Bc_Pin;
    private DbsField bene_Contract_Bc_Tiaa_Cntrct;
    private DbsField bene_Contract_Bc_Cref_Cntrct;
    private DbsField bene_Contract_Bc_Tiaa_Cref_Ind;
    private DbsField bene_Contract_Bc_Stat;
    private DbsField bene_Contract_Bc_Cntrct_Type;
    private DbsField bene_Contract_Bc_Rqst_Timestamp;
    private DbsField bene_Contract_Bc_Eff_Dte;
    private DbsField bene_Contract_Bc_Mos_Ind;
    private DbsField bene_Contract_Bc_Irvcbl_Ind;
    private DbsField bene_Contract_Bc_Pymnt_Chld_Dcsd_Ind;
    private DbsField bene_Contract_Bc_Exempt_Spouse_Rights;
    private DbsField bene_Contract_Bc_Spouse_Waived_Bnfts;
    private DbsField bene_Contract_Bc_Trust_Data_Fldr;
    private DbsField bene_Contract_Bc_Bene_Addr_Fldr;
    private DbsField bene_Contract_Bc_Co_Owner_Data_Fldr;
    private DbsField bene_Contract_Bc_Fldr_Min;
    private DbsField bene_Contract_Bc_Fldr_Srce_Id;
    private DbsField bene_Contract_Bc_Fldr_Log_Dte_Tme;
    private DbsField bene_Contract_Bc_Rcrd_Last_Updt_Dte;
    private DbsField bene_Contract_Bc_Rcrd_Last_Updt_Tme;
    private DbsField bene_Contract_Bc_Rcrd_Last_Updt_Userid;
    private DbsField bene_Contract_Bc_Last_Dsgntn_Srce;
    private DbsField bene_Contract_Bc_Last_Dsgntn_System;
    private DbsField bene_Contract_Bc_Last_Dsgntn_Userid;
    private DbsField bene_Contract_Bc_Last_Dsgntn_Dte;
    private DbsField bene_Contract_Bc_Last_Dsgntn_Tme;
    private DbsField bene_Contract_Bc_Last_Vrfy_Dte;
    private DbsField bene_Contract_Bc_Last_Vrfy_Tme;
    private DbsField bene_Contract_Bc_Last_Vrfy_Userid;
    private DbsField bene_Contract_Bc_Tiaa_Cref_Chng_Dte;
    private DbsField bene_Contract_Bc_Tiaa_Cref_Chng_Tme;
    private DbsField bene_Contract_Bc_Chng_Pwr_Atty;

    private DataAccessProgramView vw_bene_Designation;
    private DbsField bene_Designation_Bd_Pin;
    private DbsField bene_Designation_Bd_Tiaa_Cntrct;
    private DbsField bene_Designation_Bd_Cref_Cntrct;
    private DbsField bene_Designation_Bd_Tiaa_Cref_Ind;
    private DbsField bene_Designation_Bd_Stat;
    private DbsField bene_Designation_Bd_Seq_Nmbr;
    private DbsField bene_Designation_Bd_Rqst_Timestamp;
    private DbsField bene_Designation_Bd_Rcrd_Last_Updt_Dte;
    private DbsField bene_Designation_Bd_Rcrd_Last_Updt_Tme;
    private DbsField bene_Designation_Bd_Rcrd_Last_Updt_Userid;
    private DbsField bene_Designation_Bd_Bene_Type;
    private DbsField bene_Designation_Bd_Rltn_Cd;
    private DbsField bene_Designation_Bd_Bene_Name1;
    private DbsField bene_Designation_Bd_Bene_Name2;
    private DbsField bene_Designation_Bd_Rltn_Free_Txt;
    private DbsField bene_Designation_Bd_Dte_Birth_Trust;
    private DbsField bene_Designation_Bd_Ss_Cd;
    private DbsField bene_Designation_Bd_Ss_Nmbr;
    private DbsField bene_Designation_Bd_Perc_Share_Ind;
    private DbsField bene_Designation_Bd_Share_Perc;
    private DbsField bene_Designation_Bd_Share_Ntor;
    private DbsField bene_Designation_Bd_Share_Dtor;
    private DbsField bene_Designation_Bd_Irvcbl_Ind;
    private DbsField bene_Designation_Bd_Stlmnt_Rstrctn;
    private DbsField bene_Designation_Bd_Spcl_Txt1;
    private DbsField bene_Designation_Bd_Spcl_Txt2;
    private DbsField bene_Designation_Bd_Spcl_Txt3;
    private DbsField bene_Designation_Bd_Dflt_Estate;
    private DbsField bene_Designation_Bd_Mdo_Calc_Bene;
    private DbsField bene_Designation_Bd_Addr1;
    private DbsField bene_Designation_Bd_Addr2;
    private DbsField bene_Designation_Bd_Addr3_City;
    private DbsField bene_Designation_Bd_State;
    private DbsField bene_Designation_Bd_Zip;
    private DbsField bene_Designation_Bd_Phone;
    private DbsField bene_Designation_Bd_Gender;
    private DbsField bene_Designation_Bd_Country;

    private DataAccessProgramView vw_bene_Mos;
    private DbsField bene_Mos_Bm_Pin;
    private DbsField bene_Mos_Bm_Tiaa_Cntrct;
    private DbsField bene_Mos_Bm_Cref_Cntrct;
    private DbsField bene_Mos_Bm_Tiaa_Cref_Ind;
    private DbsField bene_Mos_Bm_Stat;
    private DbsField bene_Mos_Bm_Rqst_Timestamp;
    private DbsField bene_Mos_Bm_Rcrd_Last_Updt_Dte;
    private DbsField bene_Mos_Bm_Rcrd_Last_Updt_Tme;
    private DbsField bene_Mos_Bm_Rcrd_Last_Updt_Userid;
    private DbsField bene_Mos_Count_Castbm_Mos_Txt_Group;

    private DbsGroup bene_Mos_Bm_Mos_Txt_Group;
    private DbsField bene_Mos_Bm_Mos_Txt;

    private DataAccessProgramView vw_cis_Updt;
    private DbsField cis_Updt_Cis_Bene_Std_Free;

    private DbsGroup cis_Updt_Cis_Clcltn_Bnfcry_Dta;
    private DbsField cis_Updt_Cis_Clcltn_Bene_Dod;
    private DbsField cis_Updt_Cis_Clcltn_Bene_Crossover;
    private DbsField cis_Updt_Cis_Clcltn_Bene_Name;
    private DbsField cis_Updt_Cis_Clcltn_Bene_Rltnshp_Cde;
    private DbsField cis_Updt_Cis_Clcltn_Bene_Ssn_Nbr;
    private DbsField cis_Updt_Cis_Clcltn_Bene_Dob;
    private DbsField cis_Updt_Cis_Clcltn_Bene_Sex_Cde;
    private DbsField cis_Updt_Cis_Clcltn_Bene_Calc_Method;

    private DbsGroup cis_Updt_Cis_Prmry_Bnfcry_Dta;
    private DbsField cis_Updt_Cis_Prmry_Bene_Std_Txt;
    private DbsField cis_Updt_Cis_Prmry_Bene_Lump_Sum;
    private DbsField cis_Updt_Cis_Prmry_Bene_Auto_Cmnt_Val;
    private DbsField cis_Updt_Cis_Prmry_Bene_Name;
    private DbsField cis_Updt_Cis_Prmry_Bene_Extndd_Nme;
    private DbsField cis_Updt_Cis_Prmry_Bene_Rltn;
    private DbsField cis_Updt_Cis_Prmry_Bene_Rltn_Cde;
    private DbsField cis_Updt_Cis_Prmry_Bene_Ssn_Nbr;
    private DbsField cis_Updt_Cis_Prmry_Bene_Dob;
    private DbsField cis_Updt_Cis_Prmry_Bene_Allctn_Pct;
    private DbsField cis_Updt_Cis_Prmry_Bene_Nmrtr_Nbr;
    private DbsField cis_Updt_Cis_Prmry_Bene_Dnmntr_Nbr;
    private DbsGroup cis_Updt_Cis_Prmry_Bene_Spcl_TxtMuGroup;
    private DbsField cis_Updt_Cis_Prmry_Bene_Spcl_Txt;
    private DbsField cis_Updt_Count_Castcis_Cntgnt_Bnfcry_Dta;

    private DbsGroup cis_Updt_Cis_Cntgnt_Bnfcry_Dta;
    private DbsField cis_Updt_Cis_Cntgnt_Bene_Std_Txt;
    private DbsField cis_Updt_Cis_Cntgnt_Bene_Lump_Sum;
    private DbsField cis_Updt_Cis_Cntgnt_Bene_Auto_Cmnt_Val;
    private DbsField cis_Updt_Cis_Cntgnt_Bene_Name;
    private DbsField cis_Updt_Cis_Cntgnt_Bene_Extndd_Nme;
    private DbsField cis_Updt_Cis_Cntgnt_Bene_Rltn;
    private DbsField cis_Updt_Cis_Cntgnt_Bene_Rltn_Cde;
    private DbsField cis_Updt_Cis_Cntgnt_Bene_Ssn_Nbr;
    private DbsField cis_Updt_Cis_Cntgnt_Bene_Dob;
    private DbsField cis_Updt_Cis_Cntgnt_Bene_Allctn_Pct;
    private DbsField cis_Updt_Cis_Cntgnt_Bene_Nmrtr_Nbr;
    private DbsField cis_Updt_Cis_Cntgnt_Bene_Dnmntr_Nbr;
    private DbsGroup cis_Updt_Cis_Cntgnt_Bene_Spcl_TxtMuGroup;
    private DbsField cis_Updt_Cis_Cntgnt_Bene_Spcl_Txt;

    private DataAccessProgramView vw_cis_Updt2;
    private DbsGroup cis_Updt2_Cis_Bene_Dsgntn_TxtMuGroup;
    private DbsField cis_Updt2_Cis_Bene_Dsgntn_Txt;

    private DataAccessProgramView vw_mstr_02;
    private DbsField mstr_02_Rcrd_Type_Cde;
    private DbsField mstr_02_Rqst_Id;
    private DbsField mstr_02_Dlt_Cde;
    private DbsField mstr_02_Pin_Nbr;
    private DbsField mstr_02_Dvrce_Leo_Ind;
    private DbsField mstr_02_Clcltn_Dob;
    private DbsField mstr_02_Dvrce_Clcltn_Mthd;

    private DataAccessProgramView vw_mstr_03;
    private DbsField mstr_03_Rcrd_Type_Cde;
    private DbsField mstr_03_Rqst_Id;
    private DbsField mstr_03_Dlt_Cde;
    private DbsField mstr_03_Pin_Nbr;
    private DbsField mstr_03_Tiaa_Nbr;
    private DbsField mstr_03_Mdo_Pin_Cntrct_Id;
    private DbsField mstr_03_Cref_Nbr;
    private DbsField mstr_03_Rqst_Stts_Cde;
    private DbsField mstr_03_Bnfcry_Stts_Cde;
    private DbsField mstr_03_Orgnl_Rqst_Id;
    private DbsField mstr_03_Pymnt_Rqst_Id;
    private DbsField mstr_03_Last_Updt_Dte;
    private DbsField mstr_03_Last_Updt_Tme;
    private DbsField mstr_03_Last_Updt_User_Id;
    private DbsField mstr_03_Rqst_Type_Cde;
    private DbsField mstr_03_Clcltn_Mthd_Cde;
    private DbsField mstr_03_Pndng_Clcltn_Mthd_Cde;
    private DbsField mstr_03_Cnslng_Ind;
    private DbsField mstr_03_Clcltn_Cross_Over_Ind;
    private DbsField mstr_03_Clcltn_Cross_Over_Yr;
    private DbsField mstr_03_Prmry_Bnfcry_Prvsn_Ind;
    private DbsField mstr_03_Cntngnt_Bnfcry_Prvsn_Ind;

    private DbsGroup mstr_03_Clcltn_Bnfcry_Dta;
    private DbsField mstr_03_Clcltn_Bnfcry_Type_Ind;
    private DbsField mstr_03_Clcltn_Bnfcry_Nme;
    private DbsField mstr_03_Clcltn_Bnfcry_Extndd_Nme;
    private DbsField mstr_03_Clcltn_Bnfcry_Birth_Dte;
    private DbsField mstr_03_Clcltn_Bnfcry_Death_Dte;
    private DbsField mstr_03_Clcltn_Bnfcry_Rltn_Cde;
    private DbsField mstr_03_Clcltn_Bnfcry_Sex_Cde;
    private DbsField mstr_03_Clcltn_Bnfcry_Ssn_Nbr;

    private DbsGroup mstr_03_Pndng_Clcltn_Bnfcry_Dta;
    private DbsField mstr_03_Pndng_Clcltn_Bnfcry_Nme;
    private DbsField mstr_03_Pndng_Clcltn_Bnfcry_Extndd_Nme;
    private DbsField mstr_03_Pndng_Clcltn_Bnfcry_Birth_Dte;
    private DbsField mstr_03_Pndng_Clcltn_Bnfcry_Death_Dte;
    private DbsField mstr_03_Pndng_Clcltn_Bnfcry_Rltn_Cde;
    private DbsField mstr_03_Pndng_Clcltn_Bnfcry_Sex_Cde;
    private DbsField mstr_03_Pndng_Clcltn_Bnfcry_Ssn_Nbr;
    private DbsField mstr_03_Count_Castprmry_Bnfcry_Dta;

    private DbsGroup mstr_03_Prmry_Bnfcry_Dta;
    private DbsField mstr_03_Prmry_Bnfcry_Nme;
    private DbsField mstr_03_Prmry_Bnfcry_Extndd_Nme;
    private DbsField mstr_03_Prmry_Bnfcry_Allctn_Pct;
    private DbsField mstr_03_Prmry_Bnfcry_Birth_Dte;
    private DbsField mstr_03_Prmry_Bnfcry_Nmrtr_Nbr;
    private DbsField mstr_03_Prmry_Bnfcry_Dnmntr_Nbr;
    private DbsField mstr_03_Prmry_Bnfcry_Rltn_Cde;
    private DbsField mstr_03_Prmry_Bnfcry_Sex_Cde;
    private DbsField mstr_03_Prmry_Bnfcry_Spcl_Ind;
    private DbsGroup mstr_03_Prmry_Bnfcry_Spcl_TxtMuGroup;
    private DbsField mstr_03_Prmry_Bnfcry_Spcl_Txt;
    private DbsField mstr_03_Prmry_Bnfcry_Ssn_Nbr;
    private DbsField mstr_03_Count_Castcntngnt_Bnfcry_Dta;

    private DbsGroup mstr_03_Cntngnt_Bnfcry_Dta;
    private DbsField mstr_03_Cntngnt_Bnfcry_Nme;
    private DbsField mstr_03_Cntngnt_Bnfcry_Extndd_Nme;
    private DbsField mstr_03_Cntngnt_Bnfcry_Allctn_Pct;
    private DbsField mstr_03_Cntngnt_Bnfcry_Birth_Dte;
    private DbsField mstr_03_Cntngnt_Bnfcry_Nmrtr_Nbr;
    private DbsField mstr_03_Cntngnt_Bnfcry_Dnmntr_Nbr;
    private DbsField mstr_03_Cntngnt_Bnfcry_Spcl_Ind;
    private DbsField mstr_03_Cntngnt_Bnfcry_Rltn_Cde;
    private DbsField mstr_03_Cntngnt_Bnfcry_Sex_Cde;
    private DbsGroup mstr_03_Cntngnt_Bnfcry_Spcl_TxtMuGroup;
    private DbsField mstr_03_Cntngnt_Bnfcry_Spcl_Txt;
    private DbsField mstr_03_Cntngnt_Bnfcry_Ssn_Nbr;

    private DataAccessProgramView vw_mstr_05;
    private DbsField mstr_05_Rcrd_Type_Cde;
    private DbsField mstr_05_Mdo_Pin_Cntrct_Id;
    private DbsField mstr_05_Rqst_Id;
    private DbsField mstr_05_Dlt_Cde;
    private DbsField mstr_05_Pin_Nbr;
    private DbsField mstr_05_Tiaa_Nbr;
    private DbsField mstr_05_Cref_Nbr;
    private DbsField mstr_05_Rqst_Stts_Cde;
    private DbsField mstr_05_Bnfcry_Stts_Cde;
    private DbsGroup mstr_05_Bnfcry_Dsgntn_TxtMuGroup;
    private DbsField mstr_05_Bnfcry_Dsgntn_Txt;
    private DbsField pnd_Mdo_Mstr_Super_2;

    private DbsGroup pnd_Mdo_Mstr_Super_2__R_Field_1;
    private DbsField pnd_Mdo_Mstr_Super_2_Pnd_S2_Rcrd_Type_Cde;
    private DbsField pnd_Mdo_Mstr_Super_2_Pnd_S2_Rqst_Id;
    private DbsField pnd_Mdo_Mstr_Super_2_Pnd_S2_Dlt_Cde;
    private DbsField pnd_Mstr_File_03_Key;

    private DbsGroup pnd_Mstr_File_03_Key__R_Field_2;
    private DbsField pnd_Mstr_File_03_Key_Pnd_Mstr3_Rcrd_Type_Cde;
    private DbsField pnd_Mstr_File_03_Key_Pnd_Mstr3_Mdo_Pin_Cntrct_Id;

    private DbsGroup pnd_Mstr_File_03_Key__R_Field_3;
    private DbsField pnd_Mstr_File_03_Key_Pnd_Mstr3_Pin;
    private DbsField pnd_Mstr_File_03_Key_Pnd_Mstr3_Cntrct;
    private DbsField pnd_Mstr_File_03_Key_Pnd_Mstr3_Dlt_Cde;
    private DbsField pnd_Cis_Super_2;
    private DbsField pnd_W_Date_N;

    private DbsGroup pnd_W_Date_N__R_Field_4;
    private DbsField pnd_W_Date_N_Pnd_W_Date_A;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_Ph_Pin;

    private DbsGroup pnd_Input__R_Field_5;
    private DbsField pnd_Input_Pnd_Ph_Pin_N;
    private DbsField pnd_Input_Pnd_Ph_Orig_Cntrct;
    private DbsField pnd_Input_Pnd_Ph_New_Cntrct;
    private DbsField pnd_Mdo_Pin_Cntrct_Id;
    private DbsField pnd_Dob;
    private DbsField pnd_Sole_Bene;
    private DbsField pnd_Mos_Found;
    private DbsField pnd_Mstr5_Found;
    private DbsField pnd_Mdo_Bene;
    private DbsField pnd_Mdo_Bene_Ssn;

    private DbsGroup pnd_Mdo_Bene_Ssn__R_Field_6;
    private DbsField pnd_Mdo_Bene_Ssn_Pnd_Mdo_Bene_Ssn_Alpha;
    private DbsField dob1_Date;

    private DbsGroup dob1_Date__R_Field_7;
    private DbsField dob1_Date_Dob1_Yr;
    private DbsField dob1_Date_Dob1_Mo;
    private DbsField dob1_Date_Dob1_Da;
    private DbsField dob2_Date;

    private DbsGroup dob2_Date__R_Field_8;
    private DbsField dob2_Date_Dob2_Yr;
    private DbsField dob2_Date_Dob2_Mo;
    private DbsField dob2_Date_Dob2_Da;
    private DbsField pnd_Num_Years;
    private DbsField pnd_Num_Months;
    private DbsField pnd_Rltn;
    private DbsField pnd_Sex;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_A;
    private DbsField pnd_B;
    private DbsField pnd_Updt_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaNmda8510 = new PdaNmda8510(localVariables);
        pdaNmdpda_M = new PdaNmdpda_M(localVariables);
        pdaNmdpda_E = new PdaNmdpda_E(localVariables);
        ldaCisl2021 = new LdaCisl2021();
        registerRecord(ldaCisl2021);
        registerRecord(ldaCisl2021.getVw_cis_Bene_File_02());
        ldaCisl2081 = new LdaCisl2081();
        registerRecord(ldaCisl2081);
        registerRecord(ldaCisl2081.getVw_cis_Bene_File_01());

        // Local Variables

        vw_bene_Contract = new DataAccessProgramView(new NameInfo("vw_bene_Contract", "BENE-CONTRACT"), "BENE_CONTRACT_12", "BENE_CONTRACT");
        bene_Contract_Bc_Pin = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Pin", "BC-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "BC_PIN");
        bene_Contract_Bc_Pin.setDdmHeader("PIN");
        bene_Contract_Bc_Tiaa_Cntrct = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Tiaa_Cntrct", "BC-TIAA-CNTRCT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "BC_TIAA_CNTRCT");
        bene_Contract_Bc_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bene_Contract_Bc_Cref_Cntrct = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Cref_Cntrct", "BC-CREF-CNTRCT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "BC_CREF_CNTRCT");
        bene_Contract_Bc_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bene_Contract_Bc_Tiaa_Cref_Ind = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Tiaa_Cref_Ind", "BC-TIAA-CREF-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BC_TIAA_CREF_IND");
        bene_Contract_Bc_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bene_Contract_Bc_Stat = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Stat", "BC-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_STAT");
        bene_Contract_Bc_Stat.setDdmHeader("STATUS");
        bene_Contract_Bc_Cntrct_Type = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Cntrct_Type", "BC-CNTRCT-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BC_CNTRCT_TYPE");
        bene_Contract_Bc_Cntrct_Type.setDdmHeader("CNTRCT/TYPE");
        bene_Contract_Bc_Rqst_Timestamp = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Rqst_Timestamp", "BC-RQST-TIMESTAMP", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "BC_RQST_TIMESTAMP");
        bene_Contract_Bc_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bene_Contract_Bc_Eff_Dte = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Eff_Dte", "BC-EFF-DTE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "BC_EFF_DTE");
        bene_Contract_Bc_Eff_Dte.setDdmHeader("EFFECTIVE/DATE");
        bene_Contract_Bc_Mos_Ind = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Mos_Ind", "BC-MOS-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BC_MOS_IND");
        bene_Contract_Bc_Mos_Ind.setDdmHeader("MOS/IND");
        bene_Contract_Bc_Irvcbl_Ind = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Irvcbl_Ind", "BC-IRVCBL-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "BC_IRVCBL_IND");
        bene_Contract_Bc_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bene_Contract_Bc_Pymnt_Chld_Dcsd_Ind = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Pymnt_Chld_Dcsd_Ind", "BC-PYMNT-CHLD-DCSD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_PYMNT_CHLD_DCSD_IND");
        bene_Contract_Bc_Pymnt_Chld_Dcsd_Ind.setDdmHeader("PYMNT/CHILD/DCSD");
        bene_Contract_Bc_Exempt_Spouse_Rights = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Exempt_Spouse_Rights", "BC-EXEMPT-SPOUSE-RIGHTS", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_EXEMPT_SPOUSE_RIGHTS");
        bene_Contract_Bc_Exempt_Spouse_Rights.setDdmHeader("EXEMPT/SPOUSAL/RIGHTS");
        bene_Contract_Bc_Spouse_Waived_Bnfts = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Spouse_Waived_Bnfts", "BC-SPOUSE-WAIVED-BNFTS", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_SPOUSE_WAIVED_BNFTS");
        bene_Contract_Bc_Spouse_Waived_Bnfts.setDdmHeader("SPOUSE/WAIVED/BENEFITS");
        bene_Contract_Bc_Trust_Data_Fldr = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Trust_Data_Fldr", "BC-TRUST-DATA-FLDR", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BC_TRUST_DATA_FLDR");
        bene_Contract_Bc_Trust_Data_Fldr.setDdmHeader("TRUST/DATA/IN FLDR");
        bene_Contract_Bc_Bene_Addr_Fldr = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Bene_Addr_Fldr", "BC-BENE-ADDR-FLDR", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BC_BENE_ADDR_FLDR");
        bene_Contract_Bc_Bene_Addr_Fldr.setDdmHeader("BENE/ADDRESS/IN FLDR");
        bene_Contract_Bc_Co_Owner_Data_Fldr = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Co_Owner_Data_Fldr", "BC-CO-OWNER-DATA-FLDR", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_CO_OWNER_DATA_FLDR");
        bene_Contract_Bc_Co_Owner_Data_Fldr.setDdmHeader("CO-OWNER/DATA FLDR");
        bene_Contract_Bc_Fldr_Min = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Fldr_Min", "BC-FLDR-MIN", FieldType.STRING, 11, RepeatingFieldStrategy.None, 
            "BC_FLDR_MIN");
        bene_Contract_Bc_Fldr_Min.setDdmHeader("FOLDER/MIN");
        bene_Contract_Bc_Fldr_Srce_Id = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Fldr_Srce_Id", "BC-FLDR-SRCE-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "BC_FLDR_SRCE_ID");
        bene_Contract_Bc_Fldr_Srce_Id.setDdmHeader("FOLDER/SOURCE/ID");
        bene_Contract_Bc_Fldr_Log_Dte_Tme = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Fldr_Log_Dte_Tme", "BC-FLDR-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "BC_FLDR_LOG_DTE_TME");
        bene_Contract_Bc_Fldr_Log_Dte_Tme.setDdmHeader("FOLDER/LOG DATE/AND TIME");
        bene_Contract_Bc_Rcrd_Last_Updt_Dte = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Rcrd_Last_Updt_Dte", "BC-RCRD-LAST-UPDT-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "BC_RCRD_LAST_UPDT_DTE");
        bene_Contract_Bc_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bene_Contract_Bc_Rcrd_Last_Updt_Tme = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Rcrd_Last_Updt_Tme", "BC-RCRD-LAST-UPDT-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "BC_RCRD_LAST_UPDT_TME");
        bene_Contract_Bc_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bene_Contract_Bc_Rcrd_Last_Updt_Userid = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Rcrd_Last_Updt_Userid", "BC-RCRD-LAST-UPDT-USERID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "BC_RCRD_LAST_UPDT_USERID");
        bene_Contract_Bc_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bene_Contract_Bc_Last_Dsgntn_Srce = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Last_Dsgntn_Srce", "BC-LAST-DSGNTN-SRCE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BC_LAST_DSGNTN_SRCE");
        bene_Contract_Bc_Last_Dsgntn_Srce.setDdmHeader("LAST/DSGNTN/SOURCE");
        bene_Contract_Bc_Last_Dsgntn_System = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Last_Dsgntn_System", "BC-LAST-DSGNTN-SYSTEM", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BC_LAST_DSGNTN_SYSTEM");
        bene_Contract_Bc_Last_Dsgntn_System.setDdmHeader("LAST/DSGNTN/SYSTEM");
        bene_Contract_Bc_Last_Dsgntn_Userid = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Last_Dsgntn_Userid", "BC-LAST-DSGNTN-USERID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "BC_LAST_DSGNTN_USERID");
        bene_Contract_Bc_Last_Dsgntn_Userid.setDdmHeader("LAST/DSGNTN/USERID");
        bene_Contract_Bc_Last_Dsgntn_Dte = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Last_Dsgntn_Dte", "BC-LAST-DSGNTN-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BC_LAST_DSGNTN_DTE");
        bene_Contract_Bc_Last_Dsgntn_Dte.setDdmHeader("LAST/DSGNTN/DATE");
        bene_Contract_Bc_Last_Dsgntn_Tme = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Last_Dsgntn_Tme", "BC-LAST-DSGNTN-TME", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "BC_LAST_DSGNTN_TME");
        bene_Contract_Bc_Last_Dsgntn_Tme.setDdmHeader("LAST/DSGNTN/TIME");
        bene_Contract_Bc_Last_Vrfy_Dte = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Last_Vrfy_Dte", "BC-LAST-VRFY-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BC_LAST_VRFY_DTE");
        bene_Contract_Bc_Last_Vrfy_Dte.setDdmHeader("LAST/VRFY/DATE");
        bene_Contract_Bc_Last_Vrfy_Tme = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Last_Vrfy_Tme", "BC-LAST-VRFY-TME", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "BC_LAST_VRFY_TME");
        bene_Contract_Bc_Last_Vrfy_Tme.setDdmHeader("LAST/VRFY/TIME");
        bene_Contract_Bc_Last_Vrfy_Userid = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Last_Vrfy_Userid", "BC-LAST-VRFY-USERID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BC_LAST_VRFY_USERID");
        bene_Contract_Bc_Last_Vrfy_Userid.setDdmHeader("LAST/VRFY/USERID");
        bene_Contract_Bc_Tiaa_Cref_Chng_Dte = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Tiaa_Cref_Chng_Dte", "BC-TIAA-CREF-CHNG-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "BC_TIAA_CREF_CHNG_DTE");
        bene_Contract_Bc_Tiaa_Cref_Chng_Dte.setDdmHeader("TIAA CREF/CHANGE/DATE");
        bene_Contract_Bc_Tiaa_Cref_Chng_Tme = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Tiaa_Cref_Chng_Tme", "BC-TIAA-CREF-CHNG-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "BC_TIAA_CREF_CHNG_TME");
        bene_Contract_Bc_Tiaa_Cref_Chng_Tme.setDdmHeader("TIAA CREF/CHANGE/TIME");
        bene_Contract_Bc_Chng_Pwr_Atty = vw_bene_Contract.getRecord().newFieldInGroup("bene_Contract_Bc_Chng_Pwr_Atty", "BC-CHNG-PWR-ATTY", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BC_CHNG_PWR_ATTY");
        bene_Contract_Bc_Chng_Pwr_Atty.setDdmHeader("CHANGE/POWER/ATTNY");
        registerRecord(vw_bene_Contract);

        vw_bene_Designation = new DataAccessProgramView(new NameInfo("vw_bene_Designation", "BENE-DESIGNATION"), "BENE_DESIGNATION_12", "BENE_DESIGNATION");
        bene_Designation_Bd_Pin = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Pin", "BD-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "BD_PIN");
        bene_Designation_Bd_Pin.setDdmHeader("PIN");
        bene_Designation_Bd_Tiaa_Cntrct = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Tiaa_Cntrct", "BD-TIAA-CNTRCT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "BD_TIAA_CNTRCT");
        bene_Designation_Bd_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bene_Designation_Bd_Cref_Cntrct = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Cref_Cntrct", "BD-CREF-CNTRCT", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "BD_CREF_CNTRCT");
        bene_Designation_Bd_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bene_Designation_Bd_Tiaa_Cref_Ind = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Tiaa_Cref_Ind", "BD-TIAA-CREF-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BD_TIAA_CREF_IND");
        bene_Designation_Bd_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bene_Designation_Bd_Stat = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Stat", "BD-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_STAT");
        bene_Designation_Bd_Stat.setDdmHeader("STAT");
        bene_Designation_Bd_Seq_Nmbr = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Seq_Nmbr", "BD-SEQ-NMBR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "BD_SEQ_NMBR");
        bene_Designation_Bd_Seq_Nmbr.setDdmHeader(" SEQ/NMBR");
        bene_Designation_Bd_Rqst_Timestamp = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Rqst_Timestamp", "BD-RQST-TIMESTAMP", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "BD_RQST_TIMESTAMP");
        bene_Designation_Bd_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bene_Designation_Bd_Rcrd_Last_Updt_Dte = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Rcrd_Last_Updt_Dte", "BD-RCRD-LAST-UPDT-DTE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "BD_RCRD_LAST_UPDT_DTE");
        bene_Designation_Bd_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bene_Designation_Bd_Rcrd_Last_Updt_Tme = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Rcrd_Last_Updt_Tme", "BD-RCRD-LAST-UPDT-TME", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "BD_RCRD_LAST_UPDT_TME");
        bene_Designation_Bd_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bene_Designation_Bd_Rcrd_Last_Updt_Userid = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Rcrd_Last_Updt_Userid", "BD-RCRD-LAST-UPDT-USERID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "BD_RCRD_LAST_UPDT_USERID");
        bene_Designation_Bd_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bene_Designation_Bd_Bene_Type = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Bene_Type", "BD-BENE-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BD_BENE_TYPE");
        bene_Designation_Bd_Bene_Type.setDdmHeader("BENE/TYPE");
        bene_Designation_Bd_Rltn_Cd = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Rltn_Cd", "BD-RLTN-CD", FieldType.STRING, 2, 
            RepeatingFieldStrategy.None, "BD_RLTN_CD");
        bene_Designation_Bd_Rltn_Cd.setDdmHeader("RELATION/CODE");
        bene_Designation_Bd_Bene_Name1 = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Bene_Name1", "BD-BENE-NAME1", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "BD_BENE_NAME1");
        bene_Designation_Bd_Bene_Name1.setDdmHeader("BENEFICIARY/NAME 1");
        bene_Designation_Bd_Bene_Name2 = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Bene_Name2", "BD-BENE-NAME2", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "BD_BENE_NAME2");
        bene_Designation_Bd_Bene_Name2.setDdmHeader("BENEFICIARY/NAME 2");
        bene_Designation_Bd_Rltn_Free_Txt = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Rltn_Free_Txt", "BD-RLTN-FREE-TXT", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "BD_RLTN_FREE_TXT");
        bene_Designation_Bd_Rltn_Free_Txt.setDdmHeader("OTHER/RELATION");
        bene_Designation_Bd_Dte_Birth_Trust = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Dte_Birth_Trust", "BD-DTE-BIRTH-TRUST", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "BD_DTE_BIRTH_TRUST");
        bene_Designation_Bd_Dte_Birth_Trust.setDdmHeader("DOB/DOT");
        bene_Designation_Bd_Ss_Cd = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Ss_Cd", "BD-SS-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_SS_CD");
        bene_Designation_Bd_Ss_Cd.setDdmHeader("SS/CODE");
        bene_Designation_Bd_Ss_Nmbr = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Ss_Nmbr", "BD-SS-NMBR", FieldType.STRING, 9, 
            RepeatingFieldStrategy.None, "BD_SS_NMBR");
        bene_Designation_Bd_Ss_Nmbr.setDdmHeader("SS/NUMBER");
        bene_Designation_Bd_Perc_Share_Ind = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Perc_Share_Ind", "BD-PERC-SHARE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_PERC_SHARE_IND");
        bene_Designation_Bd_Perc_Share_Ind.setDdmHeader("PERCNT/SHARE/IND");
        bene_Designation_Bd_Share_Perc = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Share_Perc", "BD-SHARE-PERC", FieldType.NUMERIC, 
            5, 2, RepeatingFieldStrategy.None, "BD_SHARE_PERC");
        bene_Designation_Bd_Share_Perc.setDdmHeader("SHARE/PERCENT");
        bene_Designation_Bd_Share_Ntor = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Share_Ntor", "BD-SHARE-NTOR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "BD_SHARE_NTOR");
        bene_Designation_Bd_Share_Ntor.setDdmHeader("SHARE/NUMERATOR");
        bene_Designation_Bd_Share_Dtor = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Share_Dtor", "BD-SHARE-DTOR", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "BD_SHARE_DTOR");
        bene_Designation_Bd_Share_Dtor.setDdmHeader("SHARE/DENOMINATOR");
        bene_Designation_Bd_Irvcbl_Ind = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Irvcbl_Ind", "BD-IRVCBL-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BD_IRVCBL_IND");
        bene_Designation_Bd_Irvcbl_Ind.setDdmHeader("IRVCBL/IND");
        bene_Designation_Bd_Stlmnt_Rstrctn = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Stlmnt_Rstrctn", "BD-STLMNT-RSTRCTN", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BD_STLMNT_RSTRCTN");
        bene_Designation_Bd_Stlmnt_Rstrctn.setDdmHeader("SETTLEMENT/RESTRICTION");
        bene_Designation_Bd_Spcl_Txt1 = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Spcl_Txt1", "BD-SPCL-TXT1", FieldType.STRING, 
            72, RepeatingFieldStrategy.None, "BD_SPCL_TXT1");
        bene_Designation_Bd_Spcl_Txt1.setDdmHeader("SPECIAL TEXT 1");
        bene_Designation_Bd_Spcl_Txt2 = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Spcl_Txt2", "BD-SPCL-TXT2", FieldType.STRING, 
            72, RepeatingFieldStrategy.None, "BD_SPCL_TXT2");
        bene_Designation_Bd_Spcl_Txt2.setDdmHeader("SPECIAL TEXT 2");
        bene_Designation_Bd_Spcl_Txt3 = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Spcl_Txt3", "BD-SPCL-TXT3", FieldType.STRING, 
            72, RepeatingFieldStrategy.None, "BD_SPCL_TXT3");
        bene_Designation_Bd_Spcl_Txt3.setDdmHeader("SPECIAL TEXT 3");
        bene_Designation_Bd_Dflt_Estate = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Dflt_Estate", "BD-DFLT-ESTATE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BD_DFLT_ESTATE");
        bene_Designation_Bd_Dflt_Estate.setDdmHeader("DEFAULT/ESTATE");
        bene_Designation_Bd_Mdo_Calc_Bene = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Mdo_Calc_Bene", "BD-MDO-CALC-BENE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BD_MDO_CALC_BENE");
        bene_Designation_Bd_Mdo_Calc_Bene.setDdmHeader("MDO/CALC/BENE");
        bene_Designation_Bd_Addr1 = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Addr1", "BD-ADDR1", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "BD_ADDR1");
        bene_Designation_Bd_Addr2 = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Addr2", "BD-ADDR2", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "BD_ADDR2");
        bene_Designation_Bd_Addr3_City = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Addr3_City", "BD-ADDR3-CITY", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "BD_ADDR3_CITY");
        bene_Designation_Bd_State = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_State", "BD-STATE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "BD_STATE");
        bene_Designation_Bd_Zip = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Zip", "BD-ZIP", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BD_ZIP");
        bene_Designation_Bd_Phone = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Phone", "BD-PHONE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "BD_PHONE");
        bene_Designation_Bd_Gender = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Gender", "BD-GENDER", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BD_GENDER");
        bene_Designation_Bd_Country = vw_bene_Designation.getRecord().newFieldInGroup("bene_Designation_Bd_Country", "BD-COUNTRY", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "BD_COUNTRY");
        registerRecord(vw_bene_Designation);

        vw_bene_Mos = new DataAccessProgramView(new NameInfo("vw_bene_Mos", "BENE-MOS"), "BENE_MOS_12", "BENE_MOS", DdmPeriodicGroups.getInstance().getGroups("BENE_MOS_12"));
        bene_Mos_Bm_Pin = vw_bene_Mos.getRecord().newFieldInGroup("bene_Mos_Bm_Pin", "BM-PIN", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "BM_PIN");
        bene_Mos_Bm_Pin.setDdmHeader("PIN");
        bene_Mos_Bm_Tiaa_Cntrct = vw_bene_Mos.getRecord().newFieldInGroup("bene_Mos_Bm_Tiaa_Cntrct", "BM-TIAA-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_TIAA_CNTRCT");
        bene_Mos_Bm_Tiaa_Cntrct.setDdmHeader("TIAA/CNTRCT");
        bene_Mos_Bm_Cref_Cntrct = vw_bene_Mos.getRecord().newFieldInGroup("bene_Mos_Bm_Cref_Cntrct", "BM-CREF-CNTRCT", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "BM_CREF_CNTRCT");
        bene_Mos_Bm_Cref_Cntrct.setDdmHeader("CREF/CNTRCT");
        bene_Mos_Bm_Tiaa_Cref_Ind = vw_bene_Mos.getRecord().newFieldInGroup("bene_Mos_Bm_Tiaa_Cref_Ind", "BM-TIAA-CREF-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BM_TIAA_CREF_IND");
        bene_Mos_Bm_Tiaa_Cref_Ind.setDdmHeader("TIAA/CREF/IND");
        bene_Mos_Bm_Stat = vw_bene_Mos.getRecord().newFieldInGroup("bene_Mos_Bm_Stat", "BM-STAT", FieldType.STRING, 1, RepeatingFieldStrategy.None, "BM_STAT");
        bene_Mos_Bm_Stat.setDdmHeader("STAT");
        bene_Mos_Bm_Rqst_Timestamp = vw_bene_Mos.getRecord().newFieldInGroup("bene_Mos_Bm_Rqst_Timestamp", "BM-RQST-TIMESTAMP", FieldType.STRING, 15, 
            RepeatingFieldStrategy.None, "BM_RQST_TIMESTAMP");
        bene_Mos_Bm_Rqst_Timestamp.setDdmHeader("RQST/TIMESTAMP");
        bene_Mos_Bm_Rcrd_Last_Updt_Dte = vw_bene_Mos.getRecord().newFieldInGroup("bene_Mos_Bm_Rcrd_Last_Updt_Dte", "BM-RCRD-LAST-UPDT-DTE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BM_RCRD_LAST_UPDT_DTE");
        bene_Mos_Bm_Rcrd_Last_Updt_Dte.setDdmHeader("RCRD LAST/UPDATE/DATE");
        bene_Mos_Bm_Rcrd_Last_Updt_Tme = vw_bene_Mos.getRecord().newFieldInGroup("bene_Mos_Bm_Rcrd_Last_Updt_Tme", "BM-RCRD-LAST-UPDT-TME", FieldType.STRING, 
            7, RepeatingFieldStrategy.None, "BM_RCRD_LAST_UPDT_TME");
        bene_Mos_Bm_Rcrd_Last_Updt_Tme.setDdmHeader("RCRD LAST/UPDATE/TIME");
        bene_Mos_Bm_Rcrd_Last_Updt_Userid = vw_bene_Mos.getRecord().newFieldInGroup("bene_Mos_Bm_Rcrd_Last_Updt_Userid", "BM-RCRD-LAST-UPDT-USERID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "BM_RCRD_LAST_UPDT_USERID");
        bene_Mos_Bm_Rcrd_Last_Updt_Userid.setDdmHeader("RCRD LAST/UPDATE/USER ID");
        bene_Mos_Count_Castbm_Mos_Txt_Group = vw_bene_Mos.getRecord().newFieldInGroup("bene_Mos_Count_Castbm_Mos_Txt_Group", "C*BM-MOS-TXT-GROUP", RepeatingFieldStrategy.CAsteriskVariable, 
            "BENE_MOS_BM_MOS_TXT_GROUP");

        bene_Mos_Bm_Mos_Txt_Group = vw_bene_Mos.getRecord().newGroupInGroup("bene_Mos_Bm_Mos_Txt_Group", "BM-MOS-TXT-GROUP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "BENE_MOS_BM_MOS_TXT_GROUP");
        bene_Mos_Bm_Mos_Txt = bene_Mos_Bm_Mos_Txt_Group.newFieldArrayInGroup("bene_Mos_Bm_Mos_Txt", "BM-MOS-TXT", FieldType.STRING, 72, new DbsArrayController(1, 
            60) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "BM_MOS_TXT", "BENE_MOS_BM_MOS_TXT_GROUP");
        bene_Mos_Bm_Mos_Txt.setDdmHeader("MOS TEXT");
        registerRecord(vw_bene_Mos);

        vw_cis_Updt = new DataAccessProgramView(new NameInfo("vw_cis_Updt", "CIS-UPDT"), "CIS_BENE_FILE_01_12", "CIS_BENE_FILE", DdmPeriodicGroups.getInstance().getGroups("CIS_BENE_FILE_01_12"));
        cis_Updt_Cis_Bene_Std_Free = vw_cis_Updt.getRecord().newFieldInGroup("cis_Updt_Cis_Bene_Std_Free", "CIS-BENE-STD-FREE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CIS_BENE_STD_FREE");

        cis_Updt_Cis_Clcltn_Bnfcry_Dta = vw_cis_Updt.getRecord().newGroupInGroup("CIS_UPDT_CIS_CLCLTN_BNFCRY_DTA", "CIS-CLCLTN-BNFCRY-DTA");
        cis_Updt_Cis_Clcltn_Bene_Dod = cis_Updt_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Updt_Cis_Clcltn_Bene_Dod", "CIS-CLCLTN-BENE-DOD", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_DOD");
        cis_Updt_Cis_Clcltn_Bene_Crossover = cis_Updt_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Updt_Cis_Clcltn_Bene_Crossover", "CIS-CLCLTN-BENE-CROSSOVER", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_CROSSOVER");
        cis_Updt_Cis_Clcltn_Bene_Name = cis_Updt_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Updt_Cis_Clcltn_Bene_Name", "CIS-CLCLTN-BENE-NAME", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_NAME");
        cis_Updt_Cis_Clcltn_Bene_Rltnshp_Cde = cis_Updt_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Updt_Cis_Clcltn_Bene_Rltnshp_Cde", "CIS-CLCLTN-BENE-RLTNSHP-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_RLTNSHP_CDE");
        cis_Updt_Cis_Clcltn_Bene_Ssn_Nbr = cis_Updt_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Updt_Cis_Clcltn_Bene_Ssn_Nbr", "CIS-CLCLTN-BENE-SSN-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_SSN_NBR");
        cis_Updt_Cis_Clcltn_Bene_Dob = cis_Updt_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Updt_Cis_Clcltn_Bene_Dob", "CIS-CLCLTN-BENE-DOB", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_DOB");
        cis_Updt_Cis_Clcltn_Bene_Sex_Cde = cis_Updt_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Updt_Cis_Clcltn_Bene_Sex_Cde", "CIS-CLCLTN-BENE-SEX-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_SEX_CDE");
        cis_Updt_Cis_Clcltn_Bene_Calc_Method = cis_Updt_Cis_Clcltn_Bnfcry_Dta.newFieldInGroup("cis_Updt_Cis_Clcltn_Bene_Calc_Method", "CIS-CLCLTN-BENE-CALC-METHOD", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CIS_CLCLTN_BENE_CALC_METHOD");

        cis_Updt_Cis_Prmry_Bnfcry_Dta = vw_cis_Updt.getRecord().newGroupInGroup("cis_Updt_Cis_Prmry_Bnfcry_Dta", "CIS-PRMRY-BNFCRY-DTA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Updt_Cis_Prmry_Bene_Std_Txt = cis_Updt_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Prmry_Bene_Std_Txt", "CIS-PRMRY-BENE-STD-TXT", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_STD_TXT", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Updt_Cis_Prmry_Bene_Lump_Sum = cis_Updt_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Prmry_Bene_Lump_Sum", "CIS-PRMRY-BENE-LUMP-SUM", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_LUMP_SUM", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Updt_Cis_Prmry_Bene_Auto_Cmnt_Val = cis_Updt_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Prmry_Bene_Auto_Cmnt_Val", "CIS-PRMRY-BENE-AUTO-CMNT-VAL", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_AUTO_CMNT_VAL", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Updt_Cis_Prmry_Bene_Name = cis_Updt_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Prmry_Bene_Name", "CIS-PRMRY-BENE-NAME", FieldType.STRING, 
            35, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_NAME", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Updt_Cis_Prmry_Bene_Extndd_Nme = cis_Updt_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Prmry_Bene_Extndd_Nme", "CIS-PRMRY-BENE-EXTNDD-NME", 
            FieldType.STRING, 35, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_EXTNDD_NME", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Updt_Cis_Prmry_Bene_Rltn = cis_Updt_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Prmry_Bene_Rltn", "CIS-PRMRY-BENE-RLTN", FieldType.STRING, 
            10, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_RLTN", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Updt_Cis_Prmry_Bene_Rltn_Cde = cis_Updt_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Prmry_Bene_Rltn_Cde", "CIS-PRMRY-BENE-RLTN-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_RLTN_CDE", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Updt_Cis_Prmry_Bene_Ssn_Nbr = cis_Updt_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Prmry_Bene_Ssn_Nbr", "CIS-PRMRY-BENE-SSN-NBR", 
            FieldType.NUMERIC, 9, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_SSN_NBR", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Updt_Cis_Prmry_Bene_Dob = cis_Updt_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Prmry_Bene_Dob", "CIS-PRMRY-BENE-DOB", FieldType.DATE, 
            new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_DOB", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Updt_Cis_Prmry_Bene_Allctn_Pct = cis_Updt_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Prmry_Bene_Allctn_Pct", "CIS-PRMRY-BENE-ALLCTN-PCT", 
            FieldType.PACKED_DECIMAL, 5, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_ALLCTN_PCT", 
            "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Updt_Cis_Prmry_Bene_Nmrtr_Nbr = cis_Updt_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Prmry_Bene_Nmrtr_Nbr", "CIS-PRMRY-BENE-NMRTR-NBR", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_NMRTR_NBR", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Updt_Cis_Prmry_Bene_Dnmntr_Nbr = cis_Updt_Cis_Prmry_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Prmry_Bene_Dnmntr_Nbr", "CIS-PRMRY-BENE-DNMNTR-NBR", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_PRMRY_BENE_DNMNTR_NBR", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Updt_Cis_Prmry_Bene_Spcl_TxtMuGroup = cis_Updt_Cis_Prmry_Bnfcry_Dta.newGroupInGroup("CIS_UPDT_CIS_PRMRY_BENE_SPCL_TXTMuGroup", "CIS_PRMRY_BENE_SPCL_TXTMuGroup", 
            RepeatingFieldStrategy.PeriodicGroupSubTableFieldArray, "CIS_BENE_FILE_CIS_PRMRY_BENE_SPCL_TXT", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Updt_Cis_Prmry_Bene_Spcl_Txt = cis_Updt_Cis_Prmry_Bene_Spcl_TxtMuGroup.newFieldArrayInGroup("cis_Updt_Cis_Prmry_Bene_Spcl_Txt", "CIS-PRMRY-BENE-SPCL-TXT", 
            FieldType.STRING, 72, new DbsArrayController(1, 20, 1, 3) , RepeatingFieldStrategy.SubTableFieldArray, "CIS_PRMRY_BENE_SPCL_TXT", "CIS_BENE_FILE_CIS_PRMRY_BNFCRY_DTA");
        cis_Updt_Count_Castcis_Cntgnt_Bnfcry_Dta = vw_cis_Updt.getRecord().newFieldInGroup("cis_Updt_Count_Castcis_Cntgnt_Bnfcry_Dta", "C*CIS-CNTGNT-BNFCRY-DTA", 
            RepeatingFieldStrategy.CAsteriskVariable, "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");

        cis_Updt_Cis_Cntgnt_Bnfcry_Dta = vw_cis_Updt.getRecord().newGroupInGroup("cis_Updt_Cis_Cntgnt_Bnfcry_Dta", "CIS-CNTGNT-BNFCRY-DTA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Updt_Cis_Cntgnt_Bene_Std_Txt = cis_Updt_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Cntgnt_Bene_Std_Txt", "CIS-CNTGNT-BENE-STD-TXT", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_STD_TXT", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Updt_Cis_Cntgnt_Bene_Lump_Sum = cis_Updt_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Cntgnt_Bene_Lump_Sum", "CIS-CNTGNT-BENE-LUMP-SUM", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_LUMP_SUM", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Updt_Cis_Cntgnt_Bene_Auto_Cmnt_Val = cis_Updt_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Cntgnt_Bene_Auto_Cmnt_Val", "CIS-CNTGNT-BENE-AUTO-CMNT-VAL", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_AUTO_CMNT_VAL", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Updt_Cis_Cntgnt_Bene_Name = cis_Updt_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Cntgnt_Bene_Name", "CIS-CNTGNT-BENE-NAME", FieldType.STRING, 
            35, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_NAME", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Updt_Cis_Cntgnt_Bene_Extndd_Nme = cis_Updt_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Cntgnt_Bene_Extndd_Nme", "CIS-CNTGNT-BENE-EXTNDD-NME", 
            FieldType.STRING, 35, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_EXTNDD_NME", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Updt_Cis_Cntgnt_Bene_Rltn = cis_Updt_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Cntgnt_Bene_Rltn", "CIS-CNTGNT-BENE-RLTN", FieldType.STRING, 
            10, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_RLTN", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Updt_Cis_Cntgnt_Bene_Rltn_Cde = cis_Updt_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Cntgnt_Bene_Rltn_Cde", "CIS-CNTGNT-BENE-RLTN-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_RLTN_CDE", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Updt_Cis_Cntgnt_Bene_Ssn_Nbr = cis_Updt_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Cntgnt_Bene_Ssn_Nbr", "CIS-CNTGNT-BENE-SSN-NBR", 
            FieldType.NUMERIC, 9, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_SSN_NBR", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Updt_Cis_Cntgnt_Bene_Dob = cis_Updt_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Cntgnt_Bene_Dob", "CIS-CNTGNT-BENE-DOB", FieldType.DATE, 
            new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_DOB", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Updt_Cis_Cntgnt_Bene_Allctn_Pct = cis_Updt_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Cntgnt_Bene_Allctn_Pct", "CIS-CNTGNT-BENE-ALLCTN-PCT", 
            FieldType.PACKED_DECIMAL, 5, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_ALLCTN_PCT", 
            "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Updt_Cis_Cntgnt_Bene_Nmrtr_Nbr = cis_Updt_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Cntgnt_Bene_Nmrtr_Nbr", "CIS-CNTGNT-BENE-NMRTR-NBR", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_NMRTR_NBR", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Updt_Cis_Cntgnt_Bene_Dnmntr_Nbr = cis_Updt_Cis_Cntgnt_Bnfcry_Dta.newFieldArrayInGroup("cis_Updt_Cis_Cntgnt_Bene_Dnmntr_Nbr", "CIS-CNTGNT-BENE-DNMNTR-NBR", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CIS_CNTGNT_BENE_DNMNTR_NBR", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Updt_Cis_Cntgnt_Bene_Spcl_TxtMuGroup = cis_Updt_Cis_Cntgnt_Bnfcry_Dta.newGroupInGroup("CIS_UPDT_CIS_CNTGNT_BENE_SPCL_TXTMuGroup", "CIS_CNTGNT_BENE_SPCL_TXTMuGroup", 
            RepeatingFieldStrategy.PeriodicGroupSubTableFieldArray, "CIS_BENE_FILE_CIS_CNTGNT_BENE_SPCL_TXT", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        cis_Updt_Cis_Cntgnt_Bene_Spcl_Txt = cis_Updt_Cis_Cntgnt_Bene_Spcl_TxtMuGroup.newFieldArrayInGroup("cis_Updt_Cis_Cntgnt_Bene_Spcl_Txt", "CIS-CNTGNT-BENE-SPCL-TXT", 
            FieldType.STRING, 72, new DbsArrayController(1, 20, 1, 3) , RepeatingFieldStrategy.SubTableFieldArray, "CIS_CNTGNT_BENE_SPCL_TXT", "CIS_BENE_FILE_CIS_CNTGNT_BNFCRY_DTA");
        registerRecord(vw_cis_Updt);

        vw_cis_Updt2 = new DataAccessProgramView(new NameInfo("vw_cis_Updt2", "CIS-UPDT2"), "CIS_BENE_FILE_02_12", "CIS_BENE_FILE");
        cis_Updt2_Cis_Bene_Dsgntn_TxtMuGroup = vw_cis_Updt2.getRecord().newGroupInGroup("CIS_UPDT2_CIS_BENE_DSGNTN_TXTMuGroup", "CIS_BENE_DSGNTN_TXTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "CIS_BENE_FILE_CIS_BENE_DSGNTN_TXT");
        cis_Updt2_Cis_Bene_Dsgntn_Txt = cis_Updt2_Cis_Bene_Dsgntn_TxtMuGroup.newFieldArrayInGroup("cis_Updt2_Cis_Bene_Dsgntn_Txt", "CIS-BENE-DSGNTN-TXT", 
            FieldType.STRING, 72, new DbsArrayController(1, 60), RepeatingFieldStrategy.SubTableFieldArray, "CIS_BENE_DSGNTN_TXT");
        registerRecord(vw_cis_Updt2);

        vw_mstr_02 = new DataAccessProgramView(new NameInfo("vw_mstr_02", "MSTR-02"), "NMD_MASTER_FILE_02", "SHT_TERM_CMF_VAL");
        mstr_02_Rcrd_Type_Cde = vw_mstr_02.getRecord().newFieldInGroup("mstr_02_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYPE_CDE");
        mstr_02_Rqst_Id = vw_mstr_02.getRecord().newFieldInGroup("mstr_02_Rqst_Id", "RQST-ID", FieldType.STRING, 22, RepeatingFieldStrategy.None, "RQST_ID");
        mstr_02_Dlt_Cde = vw_mstr_02.getRecord().newFieldInGroup("mstr_02_Dlt_Cde", "DLT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "DLT_CDE");
        mstr_02_Pin_Nbr = vw_mstr_02.getRecord().newFieldInGroup("mstr_02_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "PIN_NBR");
        mstr_02_Dvrce_Leo_Ind = vw_mstr_02.getRecord().newFieldInGroup("mstr_02_Dvrce_Leo_Ind", "DVRCE-LEO-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DVRCE_LEO_IND");
        mstr_02_Clcltn_Dob = vw_mstr_02.getRecord().newFieldInGroup("mstr_02_Clcltn_Dob", "CLCLTN-DOB", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CLCLTN_DOB");
        mstr_02_Dvrce_Clcltn_Mthd = vw_mstr_02.getRecord().newFieldInGroup("mstr_02_Dvrce_Clcltn_Mthd", "DVRCE-CLCLTN-MTHD", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "DVRCE_CLCLTN_MTHD");
        registerRecord(vw_mstr_02);

        vw_mstr_03 = new DataAccessProgramView(new NameInfo("vw_mstr_03", "MSTR-03"), "NMD_MASTER_FILE_03", "SHT_TERM_CMF_VAL", DdmPeriodicGroups.getInstance().getGroups("NMD_MASTER_FILE_03"));
        mstr_03_Rcrd_Type_Cde = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYPE_CDE");
        mstr_03_Rqst_Id = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Rqst_Id", "RQST-ID", FieldType.STRING, 22, RepeatingFieldStrategy.None, "RQST_ID");
        mstr_03_Dlt_Cde = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Dlt_Cde", "DLT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "DLT_CDE");
        mstr_03_Pin_Nbr = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "PIN_NBR");
        mstr_03_Tiaa_Nbr = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Tiaa_Nbr", "TIAA-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, "TIAA_NBR");
        mstr_03_Mdo_Pin_Cntrct_Id = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Mdo_Pin_Cntrct_Id", "MDO-PIN-CNTRCT-ID", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "MDO_PIN_CNTRCT_ID");
        mstr_03_Cref_Nbr = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Cref_Nbr", "CREF-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, "CREF_NBR");
        mstr_03_Rqst_Stts_Cde = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Rqst_Stts_Cde", "RQST-STTS-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_STTS_CDE");
        mstr_03_Bnfcry_Stts_Cde = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Bnfcry_Stts_Cde", "BNFCRY-STTS-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BNFCRY_STTS_CDE");
        mstr_03_Orgnl_Rqst_Id = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Orgnl_Rqst_Id", "ORGNL-RQST-ID", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "ORGNL_RQST_ID");
        mstr_03_Pymnt_Rqst_Id = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Pymnt_Rqst_Id", "PYMNT-RQST-ID", FieldType.STRING, 22, RepeatingFieldStrategy.None, 
            "PYMNT_RQST_ID");
        mstr_03_Last_Updt_Dte = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Last_Updt_Dte", "LAST-UPDT-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "LAST_UPDT_DTE");
        mstr_03_Last_Updt_Tme = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Last_Updt_Tme", "LAST-UPDT-TME", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "LAST_UPDT_TME");
        mstr_03_Last_Updt_User_Id = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Last_Updt_User_Id", "LAST-UPDT-USER-ID", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "LAST_UPDT_USER_ID");
        mstr_03_Rqst_Type_Cde = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Rqst_Type_Cde", "RQST-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_TYPE_CDE");
        mstr_03_Clcltn_Mthd_Cde = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Clcltn_Mthd_Cde", "CLCLTN-MTHD-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CLCLTN_MTHD_CDE");
        mstr_03_Pndng_Clcltn_Mthd_Cde = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Pndng_Clcltn_Mthd_Cde", "PNDNG-CLCLTN-MTHD-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PNDNG_CLCLTN_MTHD_CDE");
        mstr_03_Cnslng_Ind = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Cnslng_Ind", "CNSLNG-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNSLNG_IND");
        mstr_03_Clcltn_Cross_Over_Ind = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Clcltn_Cross_Over_Ind", "CLCLTN-CROSS-OVER-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CLCLTN_CROSS_OVER_IND");
        mstr_03_Clcltn_Cross_Over_Yr = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Clcltn_Cross_Over_Yr", "CLCLTN-CROSS-OVER-YR", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CLCLTN_CROSS_OVER_YR");
        mstr_03_Prmry_Bnfcry_Prvsn_Ind = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Prmry_Bnfcry_Prvsn_Ind", "PRMRY-BNFCRY-PRVSN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRMRY_BNFCRY_PRVSN_IND");
        mstr_03_Cntngnt_Bnfcry_Prvsn_Ind = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Cntngnt_Bnfcry_Prvsn_Ind", "CNTNGNT-BNFCRY-PRVSN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTNGNT_BNFCRY_PRVSN_IND");

        mstr_03_Clcltn_Bnfcry_Dta = vw_mstr_03.getRecord().newGroupInGroup("MSTR_03_CLCLTN_BNFCRY_DTA", "CLCLTN-BNFCRY-DTA");
        mstr_03_Clcltn_Bnfcry_Type_Ind = mstr_03_Clcltn_Bnfcry_Dta.newFieldInGroup("mstr_03_Clcltn_Bnfcry_Type_Ind", "CLCLTN-BNFCRY-TYPE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CLCLTN_BNFCRY_TYPE_IND");
        mstr_03_Clcltn_Bnfcry_Nme = mstr_03_Clcltn_Bnfcry_Dta.newFieldInGroup("mstr_03_Clcltn_Bnfcry_Nme", "CLCLTN-BNFCRY-NME", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "CLCLTN_BNFCRY_NME");
        mstr_03_Clcltn_Bnfcry_Extndd_Nme = mstr_03_Clcltn_Bnfcry_Dta.newFieldInGroup("mstr_03_Clcltn_Bnfcry_Extndd_Nme", "CLCLTN-BNFCRY-EXTNDD-NME", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "CLCLTN_BNFCRY_EXTNDD_NME");
        mstr_03_Clcltn_Bnfcry_Birth_Dte = mstr_03_Clcltn_Bnfcry_Dta.newFieldInGroup("mstr_03_Clcltn_Bnfcry_Birth_Dte", "CLCLTN-BNFCRY-BIRTH-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CLCLTN_BNFCRY_BIRTH_DTE");
        mstr_03_Clcltn_Bnfcry_Death_Dte = mstr_03_Clcltn_Bnfcry_Dta.newFieldInGroup("mstr_03_Clcltn_Bnfcry_Death_Dte", "CLCLTN-BNFCRY-DEATH-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CLCLTN_BNFCRY_DEATH_DTE");
        mstr_03_Clcltn_Bnfcry_Rltn_Cde = mstr_03_Clcltn_Bnfcry_Dta.newFieldInGroup("mstr_03_Clcltn_Bnfcry_Rltn_Cde", "CLCLTN-BNFCRY-RLTN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CLCLTN_BNFCRY_RLTN_CDE");
        mstr_03_Clcltn_Bnfcry_Sex_Cde = mstr_03_Clcltn_Bnfcry_Dta.newFieldInGroup("mstr_03_Clcltn_Bnfcry_Sex_Cde", "CLCLTN-BNFCRY-SEX-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CLCLTN_BNFCRY_SEX_CDE");
        mstr_03_Clcltn_Bnfcry_Ssn_Nbr = mstr_03_Clcltn_Bnfcry_Dta.newFieldInGroup("mstr_03_Clcltn_Bnfcry_Ssn_Nbr", "CLCLTN-BNFCRY-SSN-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CLCLTN_BNFCRY_SSN_NBR");

        mstr_03_Pndng_Clcltn_Bnfcry_Dta = vw_mstr_03.getRecord().newGroupInGroup("MSTR_03_PNDNG_CLCLTN_BNFCRY_DTA", "PNDNG-CLCLTN-BNFCRY-DTA");
        mstr_03_Pndng_Clcltn_Bnfcry_Nme = mstr_03_Pndng_Clcltn_Bnfcry_Dta.newFieldInGroup("mstr_03_Pndng_Clcltn_Bnfcry_Nme", "PNDNG-CLCLTN-BNFCRY-NME", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "PNDNG_CLCLTN_BNFCRY_NME");
        mstr_03_Pndng_Clcltn_Bnfcry_Extndd_Nme = mstr_03_Pndng_Clcltn_Bnfcry_Dta.newFieldInGroup("mstr_03_Pndng_Clcltn_Bnfcry_Extndd_Nme", "PNDNG-CLCLTN-BNFCRY-EXTNDD-NME", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "PNDNG_CLCLTN_BNFCRY_EXTNDD_NME");
        mstr_03_Pndng_Clcltn_Bnfcry_Birth_Dte = mstr_03_Pndng_Clcltn_Bnfcry_Dta.newFieldInGroup("mstr_03_Pndng_Clcltn_Bnfcry_Birth_Dte", "PNDNG-CLCLTN-BNFCRY-BIRTH-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "PNDNG_CLCLTN_BNFCRY_BIRTH_DTE");
        mstr_03_Pndng_Clcltn_Bnfcry_Death_Dte = mstr_03_Pndng_Clcltn_Bnfcry_Dta.newFieldInGroup("mstr_03_Pndng_Clcltn_Bnfcry_Death_Dte", "PNDNG-CLCLTN-BNFCRY-DEATH-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "PNDNG_CLCLTN_BNFCRY_DEATH_DTE");
        mstr_03_Pndng_Clcltn_Bnfcry_Rltn_Cde = mstr_03_Pndng_Clcltn_Bnfcry_Dta.newFieldInGroup("mstr_03_Pndng_Clcltn_Bnfcry_Rltn_Cde", "PNDNG-CLCLTN-BNFCRY-RLTN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PNDNG_CLCLTN_BNFCRY_RLTN_CDE");
        mstr_03_Pndng_Clcltn_Bnfcry_Sex_Cde = mstr_03_Pndng_Clcltn_Bnfcry_Dta.newFieldInGroup("mstr_03_Pndng_Clcltn_Bnfcry_Sex_Cde", "PNDNG-CLCLTN-BNFCRY-SEX-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PNDNG_CLCLTN_BNFCRY_SEX_CDE");
        mstr_03_Pndng_Clcltn_Bnfcry_Ssn_Nbr = mstr_03_Pndng_Clcltn_Bnfcry_Dta.newFieldInGroup("mstr_03_Pndng_Clcltn_Bnfcry_Ssn_Nbr", "PNDNG-CLCLTN-BNFCRY-SSN-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PNDNG_CLCLTN_BNFCRY_SSN_NBR");
        mstr_03_Count_Castprmry_Bnfcry_Dta = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Count_Castprmry_Bnfcry_Dta", "C*PRMRY-BNFCRY-DTA", RepeatingFieldStrategy.CAsteriskVariable, 
            "SHT_TERM_CMF_VAL_PRMRY_BNFCRY_DTA");

        mstr_03_Prmry_Bnfcry_Dta = vw_mstr_03.getRecord().newGroupInGroup("mstr_03_Prmry_Bnfcry_Dta", "PRMRY-BNFCRY-DTA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "SHT_TERM_CMF_VAL_PRMRY_BNFCRY_DTA");
        mstr_03_Prmry_Bnfcry_Nme = mstr_03_Prmry_Bnfcry_Dta.newFieldArrayInGroup("mstr_03_Prmry_Bnfcry_Nme", "PRMRY-BNFCRY-NME", FieldType.STRING, 35, 
            new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PRMRY_BNFCRY_NME", "SHT_TERM_CMF_VAL_PRMRY_BNFCRY_DTA");
        mstr_03_Prmry_Bnfcry_Extndd_Nme = mstr_03_Prmry_Bnfcry_Dta.newFieldArrayInGroup("mstr_03_Prmry_Bnfcry_Extndd_Nme", "PRMRY-BNFCRY-EXTNDD-NME", 
            FieldType.STRING, 35, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PRMRY_BNFCRY_EXTNDD_NME", "SHT_TERM_CMF_VAL_PRMRY_BNFCRY_DTA");
        mstr_03_Prmry_Bnfcry_Allctn_Pct = mstr_03_Prmry_Bnfcry_Dta.newFieldArrayInGroup("mstr_03_Prmry_Bnfcry_Allctn_Pct", "PRMRY-BNFCRY-ALLCTN-PCT", 
            FieldType.PACKED_DECIMAL, 5, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PRMRY_BNFCRY_ALLCTN_PCT", "SHT_TERM_CMF_VAL_PRMRY_BNFCRY_DTA");
        mstr_03_Prmry_Bnfcry_Birth_Dte = mstr_03_Prmry_Bnfcry_Dta.newFieldArrayInGroup("mstr_03_Prmry_Bnfcry_Birth_Dte", "PRMRY-BNFCRY-BIRTH-DTE", FieldType.NUMERIC, 
            8, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PRMRY_BNFCRY_BIRTH_DTE", "SHT_TERM_CMF_VAL_PRMRY_BNFCRY_DTA");
        mstr_03_Prmry_Bnfcry_Nmrtr_Nbr = mstr_03_Prmry_Bnfcry_Dta.newFieldArrayInGroup("mstr_03_Prmry_Bnfcry_Nmrtr_Nbr", "PRMRY-BNFCRY-NMRTR-NBR", FieldType.NUMERIC, 
            3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PRMRY_BNFCRY_NMRTR_NBR", "SHT_TERM_CMF_VAL_PRMRY_BNFCRY_DTA");
        mstr_03_Prmry_Bnfcry_Dnmntr_Nbr = mstr_03_Prmry_Bnfcry_Dta.newFieldArrayInGroup("mstr_03_Prmry_Bnfcry_Dnmntr_Nbr", "PRMRY-BNFCRY-DNMNTR-NBR", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PRMRY_BNFCRY_DNMNTR_NBR", "SHT_TERM_CMF_VAL_PRMRY_BNFCRY_DTA");
        mstr_03_Prmry_Bnfcry_Rltn_Cde = mstr_03_Prmry_Bnfcry_Dta.newFieldArrayInGroup("mstr_03_Prmry_Bnfcry_Rltn_Cde", "PRMRY-BNFCRY-RLTN-CDE", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PRMRY_BNFCRY_RLTN_CDE", "SHT_TERM_CMF_VAL_PRMRY_BNFCRY_DTA");
        mstr_03_Prmry_Bnfcry_Sex_Cde = mstr_03_Prmry_Bnfcry_Dta.newFieldArrayInGroup("mstr_03_Prmry_Bnfcry_Sex_Cde", "PRMRY-BNFCRY-SEX-CDE", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PRMRY_BNFCRY_SEX_CDE", "SHT_TERM_CMF_VAL_PRMRY_BNFCRY_DTA");
        mstr_03_Prmry_Bnfcry_Spcl_Ind = mstr_03_Prmry_Bnfcry_Dta.newFieldArrayInGroup("mstr_03_Prmry_Bnfcry_Spcl_Ind", "PRMRY-BNFCRY-SPCL-IND", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PRMRY_BNFCRY_SPCL_IND", "SHT_TERM_CMF_VAL_PRMRY_BNFCRY_DTA");
        mstr_03_Prmry_Bnfcry_Spcl_TxtMuGroup = mstr_03_Prmry_Bnfcry_Dta.newGroupInGroup("MSTR_03_PRMRY_BNFCRY_SPCL_TXTMuGroup", "PRMRY_BNFCRY_SPCL_TXTMuGroup", 
            RepeatingFieldStrategy.PeriodicGroupSubTableFieldArray, "SHT_TERM_CMF_VAL_PRMRY_BNFCRY_SPCL_TXT", "SHT_TERM_CMF_VAL_PRMRY_BNFCRY_DTA");
        mstr_03_Prmry_Bnfcry_Spcl_Txt = mstr_03_Prmry_Bnfcry_Spcl_TxtMuGroup.newFieldArrayInGroup("mstr_03_Prmry_Bnfcry_Spcl_Txt", "PRMRY-BNFCRY-SPCL-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 20, 1, 6) , RepeatingFieldStrategy.SubTableFieldArray, "PRMRY_BNFCRY_SPCL_TXT", "SHT_TERM_CMF_VAL_PRMRY_BNFCRY_DTA");
        mstr_03_Prmry_Bnfcry_Ssn_Nbr = mstr_03_Prmry_Bnfcry_Dta.newFieldArrayInGroup("mstr_03_Prmry_Bnfcry_Ssn_Nbr", "PRMRY-BNFCRY-SSN-NBR", FieldType.NUMERIC, 
            9, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PRMRY_BNFCRY_SSN_NBR", "SHT_TERM_CMF_VAL_PRMRY_BNFCRY_DTA");
        mstr_03_Count_Castcntngnt_Bnfcry_Dta = vw_mstr_03.getRecord().newFieldInGroup("mstr_03_Count_Castcntngnt_Bnfcry_Dta", "C*CNTNGNT-BNFCRY-DTA", 
            RepeatingFieldStrategy.CAsteriskVariable, "SHT_TERM_CMF_VAL_CNTNGNT_BNFCRY_DTA");

        mstr_03_Cntngnt_Bnfcry_Dta = vw_mstr_03.getRecord().newGroupInGroup("mstr_03_Cntngnt_Bnfcry_Dta", "CNTNGNT-BNFCRY-DTA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "SHT_TERM_CMF_VAL_CNTNGNT_BNFCRY_DTA");
        mstr_03_Cntngnt_Bnfcry_Nme = mstr_03_Cntngnt_Bnfcry_Dta.newFieldArrayInGroup("mstr_03_Cntngnt_Bnfcry_Nme", "CNTNGNT-BNFCRY-NME", FieldType.STRING, 
            35, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTNGNT_BNFCRY_NME", "SHT_TERM_CMF_VAL_CNTNGNT_BNFCRY_DTA");
        mstr_03_Cntngnt_Bnfcry_Extndd_Nme = mstr_03_Cntngnt_Bnfcry_Dta.newFieldArrayInGroup("mstr_03_Cntngnt_Bnfcry_Extndd_Nme", "CNTNGNT-BNFCRY-EXTNDD-NME", 
            FieldType.STRING, 35, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTNGNT_BNFCRY_EXTNDD_NME", "SHT_TERM_CMF_VAL_CNTNGNT_BNFCRY_DTA");
        mstr_03_Cntngnt_Bnfcry_Allctn_Pct = mstr_03_Cntngnt_Bnfcry_Dta.newFieldArrayInGroup("mstr_03_Cntngnt_Bnfcry_Allctn_Pct", "CNTNGNT-BNFCRY-ALLCTN-PCT", 
            FieldType.PACKED_DECIMAL, 5, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTNGNT_BNFCRY_ALLCTN_PCT", 
            "SHT_TERM_CMF_VAL_CNTNGNT_BNFCRY_DTA");
        mstr_03_Cntngnt_Bnfcry_Birth_Dte = mstr_03_Cntngnt_Bnfcry_Dta.newFieldArrayInGroup("mstr_03_Cntngnt_Bnfcry_Birth_Dte", "CNTNGNT-BNFCRY-BIRTH-DTE", 
            FieldType.NUMERIC, 8, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTNGNT_BNFCRY_BIRTH_DTE", "SHT_TERM_CMF_VAL_CNTNGNT_BNFCRY_DTA");
        mstr_03_Cntngnt_Bnfcry_Nmrtr_Nbr = mstr_03_Cntngnt_Bnfcry_Dta.newFieldArrayInGroup("mstr_03_Cntngnt_Bnfcry_Nmrtr_Nbr", "CNTNGNT-BNFCRY-NMRTR-NBR", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTNGNT_BNFCRY_NMRTR_NBR", "SHT_TERM_CMF_VAL_CNTNGNT_BNFCRY_DTA");
        mstr_03_Cntngnt_Bnfcry_Dnmntr_Nbr = mstr_03_Cntngnt_Bnfcry_Dta.newFieldArrayInGroup("mstr_03_Cntngnt_Bnfcry_Dnmntr_Nbr", "CNTNGNT-BNFCRY-DNMNTR-NBR", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTNGNT_BNFCRY_DNMNTR_NBR", "SHT_TERM_CMF_VAL_CNTNGNT_BNFCRY_DTA");
        mstr_03_Cntngnt_Bnfcry_Spcl_Ind = mstr_03_Cntngnt_Bnfcry_Dta.newFieldArrayInGroup("mstr_03_Cntngnt_Bnfcry_Spcl_Ind", "CNTNGNT-BNFCRY-SPCL-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTNGNT_BNFCRY_SPCL_IND", "SHT_TERM_CMF_VAL_CNTNGNT_BNFCRY_DTA");
        mstr_03_Cntngnt_Bnfcry_Rltn_Cde = mstr_03_Cntngnt_Bnfcry_Dta.newFieldArrayInGroup("mstr_03_Cntngnt_Bnfcry_Rltn_Cde", "CNTNGNT-BNFCRY-RLTN-CDE", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTNGNT_BNFCRY_RLTN_CDE", "SHT_TERM_CMF_VAL_CNTNGNT_BNFCRY_DTA");
        mstr_03_Cntngnt_Bnfcry_Sex_Cde = mstr_03_Cntngnt_Bnfcry_Dta.newFieldArrayInGroup("mstr_03_Cntngnt_Bnfcry_Sex_Cde", "CNTNGNT-BNFCRY-SEX-CDE", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTNGNT_BNFCRY_SEX_CDE", "SHT_TERM_CMF_VAL_CNTNGNT_BNFCRY_DTA");
        mstr_03_Cntngnt_Bnfcry_Spcl_TxtMuGroup = mstr_03_Cntngnt_Bnfcry_Dta.newGroupInGroup("MSTR_03_CNTNGNT_BNFCRY_SPCL_TXTMuGroup", "CNTNGNT_BNFCRY_SPCL_TXTMuGroup", 
            RepeatingFieldStrategy.PeriodicGroupSubTableFieldArray, "SHT_TERM_CMF_VAL_CNTNGNT_BNFCRY_SPCL_TXT", "SHT_TERM_CMF_VAL_CNTNGNT_BNFCRY_DTA");
        mstr_03_Cntngnt_Bnfcry_Spcl_Txt = mstr_03_Cntngnt_Bnfcry_Spcl_TxtMuGroup.newFieldArrayInGroup("mstr_03_Cntngnt_Bnfcry_Spcl_Txt", "CNTNGNT-BNFCRY-SPCL-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 20, 1, 6) , RepeatingFieldStrategy.SubTableFieldArray, "CNTNGNT_BNFCRY_SPCL_TXT", "SHT_TERM_CMF_VAL_CNTNGNT_BNFCRY_DTA");
        mstr_03_Cntngnt_Bnfcry_Ssn_Nbr = mstr_03_Cntngnt_Bnfcry_Dta.newFieldArrayInGroup("mstr_03_Cntngnt_Bnfcry_Ssn_Nbr", "CNTNGNT-BNFCRY-SSN-NBR", FieldType.NUMERIC, 
            9, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTNGNT_BNFCRY_SSN_NBR", "SHT_TERM_CMF_VAL_CNTNGNT_BNFCRY_DTA");
        registerRecord(vw_mstr_03);

        vw_mstr_05 = new DataAccessProgramView(new NameInfo("vw_mstr_05", "MSTR-05"), "NMD_MASTER_FILE_05", "SHT_TERM_CMF_VAL");
        mstr_05_Rcrd_Type_Cde = vw_mstr_05.getRecord().newFieldInGroup("mstr_05_Rcrd_Type_Cde", "RCRD-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYPE_CDE");
        mstr_05_Mdo_Pin_Cntrct_Id = vw_mstr_05.getRecord().newFieldInGroup("mstr_05_Mdo_Pin_Cntrct_Id", "MDO-PIN-CNTRCT-ID", FieldType.STRING, 15, RepeatingFieldStrategy.None, 
            "MDO_PIN_CNTRCT_ID");
        mstr_05_Rqst_Id = vw_mstr_05.getRecord().newFieldInGroup("mstr_05_Rqst_Id", "RQST-ID", FieldType.STRING, 22, RepeatingFieldStrategy.None, "RQST_ID");
        mstr_05_Dlt_Cde = vw_mstr_05.getRecord().newFieldInGroup("mstr_05_Dlt_Cde", "DLT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "DLT_CDE");
        mstr_05_Pin_Nbr = vw_mstr_05.getRecord().newFieldInGroup("mstr_05_Pin_Nbr", "PIN-NBR", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "PIN_NBR");
        mstr_05_Tiaa_Nbr = vw_mstr_05.getRecord().newFieldInGroup("mstr_05_Tiaa_Nbr", "TIAA-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, "TIAA_NBR");
        mstr_05_Cref_Nbr = vw_mstr_05.getRecord().newFieldInGroup("mstr_05_Cref_Nbr", "CREF-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, "CREF_NBR");
        mstr_05_Rqst_Stts_Cde = vw_mstr_05.getRecord().newFieldInGroup("mstr_05_Rqst_Stts_Cde", "RQST-STTS-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RQST_STTS_CDE");
        mstr_05_Bnfcry_Stts_Cde = vw_mstr_05.getRecord().newFieldInGroup("mstr_05_Bnfcry_Stts_Cde", "BNFCRY-STTS-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BNFCRY_STTS_CDE");
        mstr_05_Bnfcry_Dsgntn_TxtMuGroup = vw_mstr_05.getRecord().newGroupInGroup("MSTR_05_BNFCRY_DSGNTN_TXTMuGroup", "BNFCRY_DSGNTN_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "SHT_TERM_CMF_VAL_BNFCRY_DSGNTN_TXT");
        mstr_05_Bnfcry_Dsgntn_Txt = mstr_05_Bnfcry_Dsgntn_TxtMuGroup.newFieldArrayInGroup("mstr_05_Bnfcry_Dsgntn_Txt", "BNFCRY-DSGNTN-TXT", FieldType.STRING, 
            72, new DbsArrayController(1, 60), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "BNFCRY_DSGNTN_TXT");
        registerRecord(vw_mstr_05);

        pnd_Mdo_Mstr_Super_2 = localVariables.newFieldInRecord("pnd_Mdo_Mstr_Super_2", "#MDO-MSTR-SUPER-2", FieldType.STRING, 24);

        pnd_Mdo_Mstr_Super_2__R_Field_1 = localVariables.newGroupInRecord("pnd_Mdo_Mstr_Super_2__R_Field_1", "REDEFINE", pnd_Mdo_Mstr_Super_2);
        pnd_Mdo_Mstr_Super_2_Pnd_S2_Rcrd_Type_Cde = pnd_Mdo_Mstr_Super_2__R_Field_1.newFieldInGroup("pnd_Mdo_Mstr_Super_2_Pnd_S2_Rcrd_Type_Cde", "#S2-RCRD-TYPE-CDE", 
            FieldType.STRING, 1);
        pnd_Mdo_Mstr_Super_2_Pnd_S2_Rqst_Id = pnd_Mdo_Mstr_Super_2__R_Field_1.newFieldInGroup("pnd_Mdo_Mstr_Super_2_Pnd_S2_Rqst_Id", "#S2-RQST-ID", FieldType.STRING, 
            22);
        pnd_Mdo_Mstr_Super_2_Pnd_S2_Dlt_Cde = pnd_Mdo_Mstr_Super_2__R_Field_1.newFieldInGroup("pnd_Mdo_Mstr_Super_2_Pnd_S2_Dlt_Cde", "#S2-DLT-CDE", FieldType.STRING, 
            1);
        pnd_Mstr_File_03_Key = localVariables.newFieldInRecord("pnd_Mstr_File_03_Key", "#MSTR-FILE-03-KEY", FieldType.STRING, 22);

        pnd_Mstr_File_03_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Mstr_File_03_Key__R_Field_2", "REDEFINE", pnd_Mstr_File_03_Key);
        pnd_Mstr_File_03_Key_Pnd_Mstr3_Rcrd_Type_Cde = pnd_Mstr_File_03_Key__R_Field_2.newFieldInGroup("pnd_Mstr_File_03_Key_Pnd_Mstr3_Rcrd_Type_Cde", 
            "#MSTR3-RCRD-TYPE-CDE", FieldType.STRING, 1);
        pnd_Mstr_File_03_Key_Pnd_Mstr3_Mdo_Pin_Cntrct_Id = pnd_Mstr_File_03_Key__R_Field_2.newFieldInGroup("pnd_Mstr_File_03_Key_Pnd_Mstr3_Mdo_Pin_Cntrct_Id", 
            "#MSTR3-MDO-PIN-CNTRCT-ID", FieldType.STRING, 20);

        pnd_Mstr_File_03_Key__R_Field_3 = pnd_Mstr_File_03_Key__R_Field_2.newGroupInGroup("pnd_Mstr_File_03_Key__R_Field_3", "REDEFINE", pnd_Mstr_File_03_Key_Pnd_Mstr3_Mdo_Pin_Cntrct_Id);
        pnd_Mstr_File_03_Key_Pnd_Mstr3_Pin = pnd_Mstr_File_03_Key__R_Field_3.newFieldInGroup("pnd_Mstr_File_03_Key_Pnd_Mstr3_Pin", "#MSTR3-PIN", FieldType.STRING, 
            12);
        pnd_Mstr_File_03_Key_Pnd_Mstr3_Cntrct = pnd_Mstr_File_03_Key__R_Field_3.newFieldInGroup("pnd_Mstr_File_03_Key_Pnd_Mstr3_Cntrct", "#MSTR3-CNTRCT", 
            FieldType.STRING, 8);
        pnd_Mstr_File_03_Key_Pnd_Mstr3_Dlt_Cde = pnd_Mstr_File_03_Key__R_Field_2.newFieldInGroup("pnd_Mstr_File_03_Key_Pnd_Mstr3_Dlt_Cde", "#MSTR3-DLT-CDE", 
            FieldType.STRING, 1);
        pnd_Cis_Super_2 = localVariables.newFieldInRecord("pnd_Cis_Super_2", "#CIS-SUPER-2", FieldType.STRING, 11);
        pnd_W_Date_N = localVariables.newFieldInRecord("pnd_W_Date_N", "#W-DATE-N", FieldType.NUMERIC, 8);

        pnd_W_Date_N__R_Field_4 = localVariables.newGroupInRecord("pnd_W_Date_N__R_Field_4", "REDEFINE", pnd_W_Date_N);
        pnd_W_Date_N_Pnd_W_Date_A = pnd_W_Date_N__R_Field_4.newFieldInGroup("pnd_W_Date_N_Pnd_W_Date_A", "#W-DATE-A", FieldType.STRING, 8);

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_Ph_Pin = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Ph_Pin", "#PH-PIN", FieldType.STRING, 12);

        pnd_Input__R_Field_5 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_5", "REDEFINE", pnd_Input_Pnd_Ph_Pin);
        pnd_Input_Pnd_Ph_Pin_N = pnd_Input__R_Field_5.newFieldInGroup("pnd_Input_Pnd_Ph_Pin_N", "#PH-PIN-N", FieldType.NUMERIC, 12);
        pnd_Input_Pnd_Ph_Orig_Cntrct = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Ph_Orig_Cntrct", "#PH-ORIG-CNTRCT", FieldType.STRING, 8);
        pnd_Input_Pnd_Ph_New_Cntrct = pnd_Input.newFieldInGroup("pnd_Input_Pnd_Ph_New_Cntrct", "#PH-NEW-CNTRCT", FieldType.STRING, 8);
        pnd_Mdo_Pin_Cntrct_Id = localVariables.newFieldInRecord("pnd_Mdo_Pin_Cntrct_Id", "#MDO-PIN-CNTRCT-ID", FieldType.STRING, 20);
        pnd_Dob = localVariables.newFieldInRecord("pnd_Dob", "#DOB", FieldType.NUMERIC, 8);
        pnd_Sole_Bene = localVariables.newFieldInRecord("pnd_Sole_Bene", "#SOLE-BENE", FieldType.BOOLEAN, 1);
        pnd_Mos_Found = localVariables.newFieldInRecord("pnd_Mos_Found", "#MOS-FOUND", FieldType.BOOLEAN, 1);
        pnd_Mstr5_Found = localVariables.newFieldInRecord("pnd_Mstr5_Found", "#MSTR5-FOUND", FieldType.BOOLEAN, 1);
        pnd_Mdo_Bene = localVariables.newFieldInRecord("pnd_Mdo_Bene", "#MDO-BENE", FieldType.STRING, 35);
        pnd_Mdo_Bene_Ssn = localVariables.newFieldInRecord("pnd_Mdo_Bene_Ssn", "#MDO-BENE-SSN", FieldType.NUMERIC, 9);

        pnd_Mdo_Bene_Ssn__R_Field_6 = localVariables.newGroupInRecord("pnd_Mdo_Bene_Ssn__R_Field_6", "REDEFINE", pnd_Mdo_Bene_Ssn);
        pnd_Mdo_Bene_Ssn_Pnd_Mdo_Bene_Ssn_Alpha = pnd_Mdo_Bene_Ssn__R_Field_6.newFieldInGroup("pnd_Mdo_Bene_Ssn_Pnd_Mdo_Bene_Ssn_Alpha", "#MDO-BENE-SSN-ALPHA", 
            FieldType.STRING, 9);
        dob1_Date = localVariables.newFieldInRecord("dob1_Date", "DOB1-DATE", FieldType.STRING, 8);

        dob1_Date__R_Field_7 = localVariables.newGroupInRecord("dob1_Date__R_Field_7", "REDEFINE", dob1_Date);
        dob1_Date_Dob1_Yr = dob1_Date__R_Field_7.newFieldInGroup("dob1_Date_Dob1_Yr", "DOB1-YR", FieldType.NUMERIC, 4);
        dob1_Date_Dob1_Mo = dob1_Date__R_Field_7.newFieldInGroup("dob1_Date_Dob1_Mo", "DOB1-MO", FieldType.NUMERIC, 2);
        dob1_Date_Dob1_Da = dob1_Date__R_Field_7.newFieldInGroup("dob1_Date_Dob1_Da", "DOB1-DA", FieldType.NUMERIC, 2);
        dob2_Date = localVariables.newFieldInRecord("dob2_Date", "DOB2-DATE", FieldType.STRING, 8);

        dob2_Date__R_Field_8 = localVariables.newGroupInRecord("dob2_Date__R_Field_8", "REDEFINE", dob2_Date);
        dob2_Date_Dob2_Yr = dob2_Date__R_Field_8.newFieldInGroup("dob2_Date_Dob2_Yr", "DOB2-YR", FieldType.NUMERIC, 4);
        dob2_Date_Dob2_Mo = dob2_Date__R_Field_8.newFieldInGroup("dob2_Date_Dob2_Mo", "DOB2-MO", FieldType.NUMERIC, 2);
        dob2_Date_Dob2_Da = dob2_Date__R_Field_8.newFieldInGroup("dob2_Date_Dob2_Da", "DOB2-DA", FieldType.NUMERIC, 2);
        pnd_Num_Years = localVariables.newFieldInRecord("pnd_Num_Years", "#NUM-YEARS", FieldType.NUMERIC, 3);
        pnd_Num_Months = localVariables.newFieldInRecord("pnd_Num_Months", "#NUM-MONTHS", FieldType.NUMERIC, 2);
        pnd_Rltn = localVariables.newFieldInRecord("pnd_Rltn", "#RLTN", FieldType.STRING, 1);
        pnd_Sex = localVariables.newFieldInRecord("pnd_Sex", "#SEX", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 5);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.PACKED_DECIMAL, 5);
        pnd_Updt_Cnt = localVariables.newFieldInRecord("pnd_Updt_Cnt", "#UPDT-CNT", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_bene_Contract.reset();
        vw_bene_Designation.reset();
        vw_bene_Mos.reset();
        vw_cis_Updt.reset();
        vw_cis_Updt2.reset();
        vw_mstr_02.reset();
        vw_mstr_03.reset();
        vw_mstr_05.reset();

        ldaCisl2021.initializeValues();
        ldaCisl2081.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cisp997() throws Exception
    {
        super("Cisp997");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        READWORK01:                                                                                                                                                       //Natural: FORMAT ( 1 ) PS = 60 LS = 133;//Natural: READ WORK FILE 1 #INPUT
        while (condition(getWorkFiles().read(1, pnd_Input)))
        {
            pnd_Mdo_Pin_Cntrct_Id.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Input_Pnd_Ph_Pin, pnd_Input_Pnd_Ph_Orig_Cntrct));                          //Natural: COMPRESS #PH-PIN #PH-ORIG-CNTRCT INTO #MDO-PIN-CNTRCT-ID LEAVING NO
            pnd_Mstr_File_03_Key_Pnd_Mstr3_Rcrd_Type_Cde.setValue("3");                                                                                                   //Natural: ASSIGN #MSTR3-RCRD-TYPE-CDE := '3'
            pnd_Mstr_File_03_Key_Pnd_Mstr3_Mdo_Pin_Cntrct_Id.setValue(pnd_Mdo_Pin_Cntrct_Id);                                                                             //Natural: ASSIGN #MSTR3-MDO-PIN-CNTRCT-ID := #MDO-PIN-CNTRCT-ID
            pnd_Mstr_File_03_Key_Pnd_Mstr3_Dlt_Cde.setValue("N");                                                                                                         //Natural: ASSIGN #MSTR3-DLT-CDE := 'N'
            vw_mstr_03.startDatabaseFind                                                                                                                                  //Natural: FIND ( 1 ) MSTR-03 WITH MDO-MSTR-SUPER-1 = #MSTR-FILE-03-KEY
            (
            "MSTR3_ACTIVE",
            new Wc[] { new Wc("MDO_MSTR_SUPER_1", "=", pnd_Mstr_File_03_Key, WcType.WITH) },
            1
            );
            MSTR3_ACTIVE:
            while (condition(vw_mstr_03.readNextRow("MSTR3_ACTIVE")))
            {
                vw_mstr_03.setIfNotFoundControlFlag(false);
                pnd_Mstr_File_03_Key_Pnd_Mstr3_Rcrd_Type_Cde.setValue("2");                                                                                               //Natural: ASSIGN #MSTR3-RCRD-TYPE-CDE := '2'
                vw_mstr_02.startDatabaseFind                                                                                                                              //Natural: FIND MSTR-02 WITH MDO-MSTR-SUPER-1 = #MSTR-FILE-03-KEY
                (
                "FIND01",
                new Wc[] { new Wc("MDO_MSTR_SUPER_1", "=", pnd_Mstr_File_03_Key, WcType.WITH) }
                );
                FIND01:
                while (condition(vw_mstr_02.readNextRow("FIND01", true)))
                {
                    vw_mstr_02.setIfNotFoundControlFlag(false);
                    if (condition(vw_mstr_02.getAstCOUNTER().equals(0)))                                                                                                  //Natural: IF NO RECORDS FOUND
                    {
                        getReports().write(0, "NO MSTR-02 RECORDS FOUND FOR ",pnd_Mstr_File_03_Key);                                                                      //Natural: WRITE 'NO MSTR-02 RECORDS FOUND FOR ' #MSTR-FILE-03-KEY
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (true) break MSTR3_ACTIVE;                                                                                                                     //Natural: ESCAPE BOTTOM ( MSTR3-ACTIVE. )
                    }                                                                                                                                                     //Natural: END-NOREC
                                                                                                                                                                          //Natural: PERFORM DETERMINE-IF-SOLE-SPOUSE-BENE
                    sub_Determine_If_Sole_Spouse_Bene();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Sole_Bene.getBoolean()))                                                                                                            //Natural: IF #SOLE-BENE
                    {
                        if (condition(DbsUtil.maskMatches(dob1_Date,"YYYYMMDD") && DbsUtil.maskMatches(dob2_Date,"YYYYMMDD") && dob1_Date_Dob1_Yr.less(dob2_Date_Dob2_Yr))) //Natural: IF DOB1-DATE = MASK ( YYYYMMDD ) AND DOB2-DATE = MASK ( YYYYMMDD ) AND DOB1-YR LT DOB2-YR
                        {
                            pnd_Num_Years.reset();                                                                                                                        //Natural: RESET #NUM-YEARS #NUM-MONTHS
                            pnd_Num_Months.reset();
                            DbsUtil.callnat(Nazn087.class , getCurrentProcessState(), dob1_Date, dob2_Date, pnd_Num_Years, pnd_Num_Months, pdaNmdpda_M.getMsg_Info_Sub()); //Natural: CALLNAT 'NAZN087' DOB1-DATE DOB2-DATE #NUM-YEARS #NUM-MONTHS MSG-INFO-SUB
                            if (condition(Global.isEscape())) return;
                            if (condition(pnd_Num_Years.greater(10) || (pnd_Num_Years.equals(10) && pnd_Num_Months.greater(getZero()))))                                  //Natural: IF #NUM-YEARS GT 10 OR ( #NUM-YEARS = 10 AND #NUM-MONTHS GT 0 )
                            {
                                pnd_Cis_Super_2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "1", pnd_Input_Pnd_Ph_New_Cntrct));                              //Natural: COMPRESS '1' #PH-NEW-CNTRCT INTO #CIS-SUPER-2 LEAVING NO
                                ldaCisl2081.getVw_cis_Bene_File_01().startDatabaseFind                                                                                    //Natural: FIND CIS-BENE-FILE-01 WITH CIS-BEN-SUPER-2 = #CIS-SUPER-2
                                (
                                "BEN1",
                                new Wc[] { new Wc("CIS_BEN_SUPER_2", "=", pnd_Cis_Super_2, WcType.WITH) }
                                );
                                BEN1:
                                while (condition(ldaCisl2081.getVw_cis_Bene_File_01().readNextRow("BEN1")))
                                {
                                    ldaCisl2081.getVw_cis_Bene_File_01().setIfNotFoundControlFlag(false);
                                    GT1:                                                                                                                                  //Natural: GET CIS-UPDT *ISN ( BEN1. )
                                    vw_cis_Updt.readByID(ldaCisl2081.getVw_cis_Bene_File_01().getAstISN("BEN1"), "GT1");
                                    cis_Updt_Cis_Clcltn_Bnfcry_Dta.reset();                                                                                               //Natural: RESET CIS-UPDT.CIS-CLCLTN-BNFCRY-DTA
                                    cis_Updt_Cis_Clcltn_Bene_Name.setValue(pnd_Mdo_Bene);                                                                                 //Natural: ASSIGN CIS-UPDT.CIS-CLCLTN-BENE-NAME := #MDO-BENE
                                    cis_Updt_Cis_Clcltn_Bene_Rltnshp_Cde.setValue(pnd_Rltn);                                                                              //Natural: ASSIGN CIS-UPDT.CIS-CLCLTN-BENE-RLTNSHP-CDE := #RLTN
                                    cis_Updt_Cis_Clcltn_Bene_Ssn_Nbr.setValue(pnd_Mdo_Bene_Ssn);                                                                          //Natural: ASSIGN CIS-UPDT.CIS-CLCLTN-BENE-SSN-NBR := #MDO-BENE-SSN
                                    cis_Updt_Cis_Clcltn_Bene_Dob.setValueEdited(new ReportEditMask("YYYYMMDD"),dob2_Date);                                                //Natural: MOVE EDITED DOB2-DATE TO CIS-UPDT.CIS-CLCLTN-BENE-DOB ( EM = YYYYMMDD )
                                    cis_Updt_Cis_Clcltn_Bene_Sex_Cde.setValue(pnd_Sex);                                                                                   //Natural: ASSIGN CIS-UPDT.CIS-CLCLTN-BENE-SEX-CDE := #SEX
                                    if (condition(cis_Updt_Cis_Clcltn_Bene_Name.equals("See Designation Text") && cis_Updt_Cis_Bene_Std_Free.notEquals("Y")))             //Natural: IF CIS-UPDT.CIS-CLCLTN-BENE-NAME = 'See Designation Text' AND CIS-UPDT.CIS-BENE-STD-FREE NE 'Y'
                                    {
                                        cis_Updt_Cis_Bene_Std_Free.setValue("Y");                                                                                         //Natural: ASSIGN CIS-UPDT.CIS-BENE-STD-FREE := 'Y'
                                    }                                                                                                                                     //Natural: END-IF
                                    if (condition(cis_Updt_Cis_Bene_Std_Free.equals("Y")))                                                                                //Natural: IF CIS-UPDT.CIS-BENE-STD-FREE = 'Y'
                                    {
                                        pnd_Mstr_File_03_Key_Pnd_Mstr3_Rcrd_Type_Cde.setValue("5");                                                                       //Natural: ASSIGN #MSTR3-RCRD-TYPE-CDE := '5'
                                        vw_mstr_05.startDatabaseFind                                                                                                      //Natural: FIND ( 1 ) MSTR-05 WITH MDO-MSTR-SUPER-1 = #MSTR-FILE-03-KEY
                                        (
                                        "FIND02",
                                        new Wc[] { new Wc("MDO_MSTR_SUPER_1", "=", pnd_Mstr_File_03_Key, WcType.WITH) },
                                        1
                                        );
                                        FIND02:
                                        while (condition(vw_mstr_05.readNextRow("FIND02")))
                                        {
                                            vw_mstr_05.setIfNotFoundControlFlag(false);
                                            pnd_Cis_Super_2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "2", pnd_Input_Pnd_Ph_New_Cntrct));                  //Natural: COMPRESS '2' #PH-NEW-CNTRCT INTO #CIS-SUPER-2 LEAVING NO
                                            ldaCisl2021.getVw_cis_Bene_File_02().startDatabaseFind                                                                        //Natural: FIND CIS-BENE-FILE-02 WITH CIS-BEN-SUPER-2 = #CIS-SUPER-2
                                            (
                                            "FDCIS2",
                                            new Wc[] { new Wc("CIS_BEN_SUPER_2", "=", pnd_Cis_Super_2, WcType.WITH) }
                                            );
                                            FDCIS2:
                                            while (condition(ldaCisl2021.getVw_cis_Bene_File_02().readNextRow("FDCIS2")))
                                            {
                                                ldaCisl2021.getVw_cis_Bene_File_02().setIfNotFoundControlFlag(false);
                                                if (condition(ldaCisl2021.getCis_Bene_File_02_Cis_Bene_Dsgntn_Txt().getValue(1,":",60).equals(mstr_05_Bnfcry_Dsgntn_Txt.getValue(1, //Natural: IF CIS-BENE-FILE-02.CIS-BENE-DSGNTN-TXT ( 1:60 ) = MSTR-05.BNFCRY-DSGNTN-TXT ( 1:60 )
                                                    ":",60))))
                                                {
                                                    ignore();
                                                }                                                                                                                         //Natural: ELSE
                                                else if (condition())
                                                {
                                                    GTCIS2:                                                                                                               //Natural: GET CIS-UPDT2 *ISN ( FDCIS2. )
                                                    vw_cis_Updt2.readByID(ldaCisl2021.getVw_cis_Bene_File_02().getAstISN("FDCIS2"), "GTCIS2");
                                                    cis_Updt2_Cis_Bene_Dsgntn_Txt.getValue(1,":",60).setValue(mstr_05_Bnfcry_Dsgntn_Txt.getValue(1,":",                   //Natural: ASSIGN CIS-UPDT2.CIS-BENE-DSGNTN-TXT ( 1:60 ) := MSTR-05.BNFCRY-DSGNTN-TXT ( 1:60 )
                                                        60));
                                                    vw_cis_Updt2.updateDBRow("GTCIS2");                                                                                   //Natural: UPDATE ( GTCIS2. )
                                                }                                                                                                                         //Natural: END-IF
                                            }                                                                                                                             //Natural: END-FIND
                                            if (condition(Global.isEscape()))
                                            {
                                                if (condition(Global.isEscapeBottom())) break;
                                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                                else if (condition(Global.isEscapeTop())) continue;
                                                else if (condition(Global.isEscapeRoutine())) return;
                                                else break;
                                            }
                                        }                                                                                                                                 //Natural: END-FIND
                                        if (condition(Global.isEscape()))
                                        {
                                            if (condition(Global.isEscapeBottom("BEN1"))) break;
                                            else if (condition(Global.isEscapeBottomImmediate("BEN1"))) break;
                                            else if (condition(Global.isEscapeTop())) continue;
                                            else if (condition(Global.isEscapeRoutine())) return;
                                            else break;
                                        }
                                    }                                                                                                                                     //Natural: END-IF
                                    if (condition(cis_Updt_Cis_Prmry_Bene_Name.getValue(1).notEquals(cis_Updt_Cis_Clcltn_Bene_Name) && cis_Updt_Cis_Bene_Std_Free.equals("N"))) //Natural: IF CIS-UPDT.CIS-PRMRY-BENE-NAME ( 1 ) NE CIS-UPDT.CIS-CLCLTN-BENE-NAME AND CIS-UPDT.CIS-BENE-STD-FREE = 'N'
                                    {
                                        getReports().write(0, "different benes for contract",pnd_Input_Pnd_Ph_New_Cntrct,"Pbene",cis_Updt_Cis_Prmry_Bene_Name.getValue(1), //Natural: WRITE 'different benes for contract' #PH-NEW-CNTRCT 'Pbene' CIS-UPDT.CIS-PRMRY-BENE-NAME ( 1 ) / 'CBene' CIS-UPDT.CIS-CLCLTN-BENE-NAME
                                            NEWLINE,"CBene",cis_Updt_Cis_Clcltn_Bene_Name);
                                        if (condition(Global.isEscape()))
                                        {
                                            if (condition(Global.isEscapeBottom("BEN1"))) break;
                                            else if (condition(Global.isEscapeBottomImmediate("BEN1"))) break;
                                            else if (condition(Global.isEscapeTop())) continue;
                                            else if (condition(Global.isEscapeRoutine())) return;
                                            else break;
                                        }
                                                                                                                                                                          //Natural: PERFORM UPDATE-CIS-BENES-FROM-MDO
                                        sub_Update_Cis_Benes_From_Mdo();
                                        if (condition(Global.isEscape()))
                                        {
                                            if (condition(Global.isEscapeBottom("BEN1"))) break;
                                            else if (condition(Global.isEscapeBottomImmediate("BEN1"))) break;
                                            else if (condition(Global.isEscapeTop())) continue;
                                            else if (condition(Global.isEscapeRoutine())) return;
                                            else break;
                                        }
                                    }                                                                                                                                     //Natural: END-IF
                                    if (condition(cis_Updt_Cis_Clcltn_Bene_Name.notEquals(" ") && cis_Updt_Cis_Bene_Std_Free.equals(" ")))                                //Natural: IF CIS-UPDT.CIS-CLCLTN-BENE-NAME NE ' ' AND CIS-UPDT.CIS-BENE-STD-FREE = ' '
                                    {
                                        cis_Updt_Cis_Bene_Std_Free.setValue("N");                                                                                         //Natural: ASSIGN CIS-UPDT.CIS-BENE-STD-FREE := 'N'
                                    }                                                                                                                                     //Natural: END-IF
                                    vw_cis_Updt.updateDBRow("GT1");                                                                                                       //Natural: UPDATE ( GT1. )
                                    pnd_Updt_Cnt.nadd(1);                                                                                                                 //Natural: ADD 1 TO #UPDT-CNT
                                    if (condition(pnd_Updt_Cnt.greaterOrEqual(100)))                                                                                      //Natural: IF #UPDT-CNT GE 100
                                    {
                                        getCurrentProcessState().getDbConv().dbCommit();                                                                                  //Natural: END TRANSACTION
                                        pnd_Updt_Cnt.reset();                                                                                                             //Natural: RESET #UPDT-CNT
                                    }                                                                                                                                     //Natural: END-IF
                                    getReports().display(1, "PIN",                                                                                                        //Natural: DISPLAY ( 1 ) 'PIN' #PH-PIN 'NEW MDO/CONTRACT' CIS-BENE-FILE-01.CIS-BENE-TIAA-NBR 'BENE NAME' CIS-UPDT.CIS-CLCLTN-BENE-NAME 'RC' CIS-UPDT.CIS-CLCLTN-BENE-RLTNSHP-CDE 'SSN' CIS-UPDT.CIS-CLCLTN-BENE-SSN-NBR ( EM = 999999999 ) 'BENE DOB' DOB2-DATE ( EM = XXXX/XX/XX ) 'SEX' CIS-UPDT.CIS-CLCLTN-BENE-SEX-CDE
                                    		pnd_Input_Pnd_Ph_Pin,"NEW MDO/CONTRACT",
                                    		ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Tiaa_Nbr(),"BENE NAME",
                                    		cis_Updt_Cis_Clcltn_Bene_Name,"RC",
                                    		cis_Updt_Cis_Clcltn_Bene_Rltnshp_Cde,"SSN",
                                    		cis_Updt_Cis_Clcltn_Bene_Ssn_Nbr, new ReportEditMask ("999999999"),"BENE DOB",
                                    		dob2_Date, new ReportEditMask ("XXXX/XX/XX"),"SEX",
                                    		cis_Updt_Cis_Clcltn_Bene_Sex_Cde);
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom("BEN1"))) break;
                                        else if (condition(Global.isEscapeBottomImmediate("BEN1"))) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                    pnd_A.nadd(1);                                                                                                                        //Natural: ADD 1 TO #A
                                }                                                                                                                                         //Natural: END-FIND
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().write(0, "WHY THIS CASE IS REJECTED?",pnd_Input_Pnd_Ph_Pin,pnd_Input_Pnd_Ph_Orig_Cntrct);                                        //Natural: WRITE 'WHY THIS CASE IS REJECTED?' #PH-PIN #PH-ORIG-CNTRCT
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(0, "=",dob1_Date,dob2_Date);                                                                                               //Natural: WRITE '=' DOB1-DATE DOB2-DATE
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("MSTR3_ACTIVE"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("MSTR3_ACTIVE"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* MSTR3-ACTIVE.
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Updt_Cnt.greater(getZero())))                                                                                                                   //Natural: IF #UPDT-CNT GT 0
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, "TOTAL CONTRACTS UPDATED: ",pnd_A);                                                                                                         //Natural: WRITE ( 1 ) 'TOTAL CONTRACTS UPDATED: ' #A
        if (Global.isEscape()) return;
        //* *****************************************************************
        //* *****************************************************************
        //* ***********************************************************************
    }                                                                                                                                                                     //Natural: AT TOP OF PAGE ( 1 )
    private void sub_Create_Or_Update_02_Desig() throws Exception                                                                                                         //Natural: CREATE-OR-UPDATE-02-DESIG
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        pnd_Mstr5_Found.reset();                                                                                                                                          //Natural: RESET #MSTR5-FOUND #MOS-FOUND
        pnd_Mos_Found.reset();
        pnd_Mstr_File_03_Key_Pnd_Mstr3_Rcrd_Type_Cde.setValue("5");                                                                                                       //Natural: ASSIGN #MSTR3-RCRD-TYPE-CDE := '5'
        vw_mstr_05.startDatabaseFind                                                                                                                                      //Natural: FIND ( 1 ) MSTR-05 WITH MDO-MSTR-SUPER-1 = #MSTR-FILE-03-KEY
        (
        "FIND03",
        new Wc[] { new Wc("MDO_MSTR_SUPER_1", "=", pnd_Mstr_File_03_Key, WcType.WITH) },
        1
        );
        FIND03:
        while (condition(vw_mstr_05.readNextRow("FIND03", true)))
        {
            vw_mstr_05.setIfNotFoundControlFlag(false);
            if (condition(vw_mstr_05.getAstCOUNTER().equals(0)))                                                                                                          //Natural: IF NO RECORDS FOUND
            {
                vw_bene_Mos.startDatabaseFind                                                                                                                             //Natural: FIND BENE-MOS WITH BM-TIAA-CNTRCT = #PH-ORIG-CNTRCT
                (
                "FIND04",
                new Wc[] { new Wc("BM_TIAA_CNTRCT", "=", pnd_Input_Pnd_Ph_Orig_Cntrct, WcType.WITH) }
                );
                FIND04:
                while (condition(vw_bene_Mos.readNextRow("FIND04", true)))
                {
                    vw_bene_Mos.setIfNotFoundControlFlag(false);
                    if (condition(vw_bene_Mos.getAstCOUNTER().equals(0)))                                                                                                 //Natural: IF NO RECORDS FOUND
                    {
                        getReports().write(0, "No MOS found for contract",pnd_Input_Pnd_Ph_Orig_Cntrct);                                                                  //Natural: WRITE 'No MOS found for contract' #PH-ORIG-CNTRCT
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-NOREC
                    pnd_Mos_Found.setValue(true);                                                                                                                         //Natural: ASSIGN #MOS-FOUND := TRUE
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            pnd_Mstr5_Found.setValue(true);                                                                                                                               //Natural: ASSIGN #MSTR5-FOUND := TRUE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        pnd_Cis_Super_2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "2", pnd_Input_Pnd_Ph_New_Cntrct));                                                      //Natural: COMPRESS '2' #PH-NEW-CNTRCT INTO #CIS-SUPER-2 LEAVING NO
        ldaCisl2021.getVw_cis_Bene_File_02().startDatabaseFind                                                                                                            //Natural: FIND CIS-BENE-FILE-02 WITH CIS-BEN-SUPER-2 = #CIS-SUPER-2
        (
        "FD2",
        new Wc[] { new Wc("CIS_BEN_SUPER_2", "=", pnd_Cis_Super_2, WcType.WITH) }
        );
        FD2:
        while (condition(ldaCisl2021.getVw_cis_Bene_File_02().readNextRow("FD2", true)))
        {
            ldaCisl2021.getVw_cis_Bene_File_02().setIfNotFoundControlFlag(false);
            if (condition(ldaCisl2021.getVw_cis_Bene_File_02().getAstCOUNTER().equals(0)))                                                                                //Natural: IF NO RECORDS FOUND
            {
                ldaCisl2021.getVw_cis_Bene_File_02().setValuesByName(ldaCisl2081.getVw_cis_Bene_File_01());                                                               //Natural: MOVE BY NAME CIS-BENE-FILE-01 TO CIS-BENE-FILE-02
                ldaCisl2021.getCis_Bene_File_02_Cis_Rcrd_Type_Cde().setValue("2");                                                                                        //Natural: ASSIGN CIS-BENE-FILE-02.CIS-RCRD-TYPE-CDE := '2'
                if (condition(pnd_Mstr5_Found.getBoolean()))                                                                                                              //Natural: IF #MSTR5-FOUND
                {
                    ldaCisl2021.getCis_Bene_File_02_Cis_Bene_Dsgntn_Txt().getValue(1,":",60).setValue(mstr_05_Bnfcry_Dsgntn_Txt.getValue(1,":",60));                      //Natural: ASSIGN CIS-BENE-FILE-02.CIS-BENE-DSGNTN-TXT ( 1:60 ) := MSTR-05.BNFCRY-DSGNTN-TXT ( 1:60 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaCisl2021.getCis_Bene_File_02_Cis_Bene_Dsgntn_Txt().getValue(1,":",60).setValue(bene_Mos_Bm_Mos_Txt.getValue(1,":",60));                            //Natural: ASSIGN CIS-BENE-FILE-02.CIS-BENE-DSGNTN-TXT ( 1:60 ) := BM-MOS-TXT ( 1:60 )
                }                                                                                                                                                         //Natural: END-IF
                ldaCisl2021.getVw_cis_Bene_File_02().insertDBRow();                                                                                                       //Natural: STORE CIS-BENE-FILE-02
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            GT2:                                                                                                                                                          //Natural: GET CIS-UPDT2 *ISN ( FD2. )
            vw_cis_Updt2.readByID(ldaCisl2021.getVw_cis_Bene_File_02().getAstISN("FD2"), "GT2");
            if (condition(pnd_Mstr5_Found.getBoolean()))                                                                                                                  //Natural: IF #MSTR5-FOUND
            {
                cis_Updt2_Cis_Bene_Dsgntn_Txt.getValue(1,":",60).setValue(mstr_05_Bnfcry_Dsgntn_Txt.getValue(1,":",60));                                                  //Natural: ASSIGN CIS-UPDT2.CIS-BENE-DSGNTN-TXT ( 1:60 ) := MSTR-05.BNFCRY-DSGNTN-TXT ( 1:60 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                cis_Updt2_Cis_Bene_Dsgntn_Txt.getValue(1,":",60).setValue(bene_Mos_Bm_Mos_Txt.getValue(1,":",60));                                                        //Natural: ASSIGN CIS-UPDT2.CIS-BENE-DSGNTN-TXT ( 1:60 ) := BM-MOS-TXT ( 1:60 )
            }                                                                                                                                                             //Natural: END-IF
            vw_cis_Updt2.updateDBRow("GT2");                                                                                                                              //Natural: UPDATE ( GT2. )
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Determine_If_Sole_Spouse_Bene() throws Exception                                                                                                     //Natural: DETERMINE-IF-SOLE-SPOUSE-BENE
    {
        if (BLNatReinput.isReinput()) return;

        //*  USES LOGIC FROM SETUP OF CALLS TO AMDN0034, BUT WE NEED ONLY
        //*  DETERMINE IF THERE IS A SOLE BENE AND TO SET DOB'S.
        //* *****************************************************************
        dob1_Date.reset();                                                                                                                                                //Natural: RESET DOB1-DATE DOB2-DATE #SOLE-BENE #MDO-BENE #MDO-BENE-SSN #RLTN #SEX
        dob2_Date.reset();
        pnd_Sole_Bene.reset();
        pnd_Mdo_Bene.reset();
        pnd_Mdo_Bene_Ssn.reset();
        pnd_Rltn.reset();
        pnd_Sex.reset();
        pdaNmda8510.getNmda8510_Pnd_Pin_Nbr().setValue(pnd_Input_Pnd_Ph_Pin_N);                                                                                           //Natural: ASSIGN NMDA8510.#PIN-NBR := #PH-PIN-N
        DbsUtil.terminateApplication("Program NMDN8510 is missing from the collection!");                                                                                 //Natural: CALLNAT 'NMDN8510' NMDA8510 MSG-INFO-SUB ERROR-INFO-SUB
        if (condition(Global.isEscape())) return;
        dob1_Date.setValueEdited(pdaNmda8510.getNmda8510_Pnd_Prtcpnt_Date_Of_Birth(),new ReportEditMask("99999999"));                                                     //Natural: MOVE EDITED NMDA8510.#PRTCPNT-DATE-OF-BIRTH ( EM = 99999999 ) TO DOB1-DATE
        if (condition(((mstr_03_Prmry_Bnfcry_Nme.getValue(1).notEquals(" ") && (mstr_03_Prmry_Bnfcry_Allctn_Pct.getValue(1).equals(new DbsDecimal("100.00"))              //Natural: IF ( MSTR-03.PRMRY-BNFCRY-NME ( 1 ) NE ' ' AND ( MSTR-03.PRMRY-BNFCRY-ALLCTN-PCT ( 1 ) EQ 100.00 OR MSTR-03.PRMRY-BNFCRY-NME ( 2 ) EQ ' ' ) AND MSTR-03.PRMRY-BNFCRY-RLTN-CDE ( 1 ) EQ 'S' OR EQ 'K' )
            || mstr_03_Prmry_Bnfcry_Nme.getValue(2).equals(" "))) && (mstr_03_Prmry_Bnfcry_Rltn_Cde.getValue(1).equals("S") || mstr_03_Prmry_Bnfcry_Rltn_Cde.getValue(1).equals("K")))))
        {
            pnd_Sole_Bene.setValue(true);                                                                                                                                 //Natural: MOVE TRUE TO #SOLE-BENE
            //*  #MDO-BENE := MSTR-03.PRMRY-BNFCRY-NME (1)
            //*  #MDO-BENE-SSN := MSTR-03.PRMRY-BNFCRY-SSN-NBR (1)
            //*  #SEX := MSTR-03.PRMRY-BNFCRY-SEX-CDE (1)
            //*  #RLTN := MSTR-03.PRMRY-BNFCRY-RLTN-CDE (1)
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Sole_Bene.setValue(false);                                                                                                                                //Natural: MOVE FALSE TO #SOLE-BENE
        }                                                                                                                                                                 //Natural: END-IF
        //*  JOINT LIFE INDICATOR
        if (condition(mstr_03_Pndng_Clcltn_Mthd_Cde.equals("J")))                                                                                                         //Natural: IF MSTR-03.PNDNG-CLCLTN-MTHD-CDE = 'J'
        {
            pnd_Sole_Bene.setValue(true);                                                                                                                                 //Natural: MOVE TRUE TO #SOLE-BENE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Sole_Bene.getBoolean()))                                                                                                                        //Natural: IF #SOLE-BENE
        {
            dob2_Date.setValueEdited(mstr_03_Prmry_Bnfcry_Birth_Dte.getValue(1),new ReportEditMask("99999999"));                                                          //Natural: MOVE EDITED MSTR-03.PRMRY-BNFCRY-BIRTH-DTE ( 1 ) ( EM = 99999999 ) TO DOB2-DATE
            pnd_Mdo_Bene.setValue(mstr_03_Prmry_Bnfcry_Nme.getValue(1));                                                                                                  //Natural: ASSIGN #MDO-BENE := MSTR-03.PRMRY-BNFCRY-NME ( 1 )
            pnd_Mdo_Bene_Ssn.setValue(mstr_03_Prmry_Bnfcry_Ssn_Nbr.getValue(1));                                                                                          //Natural: ASSIGN #MDO-BENE-SSN := PRMRY-BNFCRY-SSN-NBR ( 1 )
            pnd_Sex.setValue(mstr_03_Prmry_Bnfcry_Sex_Cde.getValue(1));                                                                                                   //Natural: ASSIGN #SEX := MSTR-03.PRMRY-BNFCRY-SEX-CDE ( 1 )
            pnd_Rltn.setValue(mstr_03_Prmry_Bnfcry_Rltn_Cde.getValue(1));                                                                                                 //Natural: ASSIGN #RLTN := MSTR-03.PRMRY-BNFCRY-RLTN-CDE ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(mstr_02_Dvrce_Leo_Ind.equals("D")))                                                                                                                 //Natural: IF MSTR-02.DVRCE-LEO-IND = 'D'
        {
            if (condition(mstr_02_Dvrce_Clcltn_Mthd.equals("S")))                                                                                                         //Natural: IF MSTR-02.DVRCE-CLCLTN-MTHD = 'S'
            {
                pnd_Sole_Bene.setValue(true);                                                                                                                             //Natural: MOVE TRUE TO #SOLE-BENE
                pnd_Mdo_Bene.setValue("?? DVRCE-CLCLTN-MTHD 'S'");                                                                                                        //Natural: ASSIGN #MDO-BENE := '?? DVRCE-CLCLTN-MTHD "S"'
                pnd_Mdo_Bene_Ssn.setValue(999999999);                                                                                                                     //Natural: ASSIGN #MDO-BENE-SSN := 999999999
                dob2_Date.setValueEdited(pdaNmda8510.getNmda8510_Pnd_Prtcpnt_Date_Of_Birth(),new ReportEditMask("99999999"));                                             //Natural: MOVE EDITED NMDA8510.#PRTCPNT-DATE-OF-BIRTH ( EM = 99999999 ) TO DOB2-DATE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Sole_Bene.setValue(false);                                                                                                                            //Natural: MOVE FALSE TO #SOLE-BENE
            }                                                                                                                                                             //Natural: END-IF
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(mstr_02_Dvrce_Leo_Ind.equals("L")))                                                                                                                 //Natural: IF MSTR-02.DVRCE-LEO-IND = 'L'
        {
            dob2_Date.reset();                                                                                                                                            //Natural: RESET DOB2-DATE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(mstr_03_Pndng_Clcltn_Mthd_Cde.equals("J")))                                                                                                         //Natural: IF MSTR-03.PNDNG-CLCLTN-MTHD-CDE = 'J'
        {
            //*  I ADDED THIS - OS
            pnd_Sole_Bene.setValue(true);                                                                                                                                 //Natural: MOVE TRUE TO #SOLE-BENE
            pnd_Mdo_Bene.setValue("?? PNDNG-CLCTN-MTHD-CDE 'J'");                                                                                                         //Natural: ASSIGN #MDO-BENE := '?? PNDNG-CLCTN-MTHD-CDE "J"'
            pnd_Mdo_Bene.setValue(mstr_03_Prmry_Bnfcry_Nme.getValue(1));                                                                                                  //Natural: ASSIGN #MDO-BENE := MSTR-03.PRMRY-BNFCRY-NME ( 1 )
            if (condition(pnd_Mdo_Bene_Ssn.equals(getZero())))                                                                                                            //Natural: IF #MDO-BENE-SSN = 0
            {
                pnd_Mdo_Bene_Ssn.setValue(999999999);                                                                                                                     //Natural: ASSIGN #MDO-BENE-SSN := 999999999
            }                                                                                                                                                             //Natural: END-IF
            if (condition(dob2_Date.equals(" ") || dob2_Date.equals("00000000")))                                                                                         //Natural: IF DOB2-DATE = ' ' OR = '00000000'
            {
                dob2_Date.setValueEdited(mstr_03_Pndng_Clcltn_Bnfcry_Birth_Dte,new ReportEditMask("99999999"));                                                           //Natural: MOVE EDITED MSTR-03.PNDNG-CLCLTN-BNFCRY-BIRTH-DTE ( EM = 99999999 ) TO DOB2-DATE
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "CHECK THIS",pnd_Input_Pnd_Ph_Pin,pnd_Input_Pnd_Ph_Orig_Cntrct,pnd_Input_Pnd_Ph_New_Cntrct);                                            //Natural: WRITE 'CHECK THIS' #PH-PIN #PH-ORIG-CNTRCT #PH-NEW-CNTRCT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(mstr_03_Pndng_Clcltn_Mthd_Cde.equals("U")))                                                                                                         //Natural: IF MSTR-03.PNDNG-CLCLTN-MTHD-CDE = 'U'
        {
            pnd_Sole_Bene.setValue(false);                                                                                                                                //Natural: MOVE FALSE TO #SOLE-BENE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_Cis_Benes_From_Mdo() throws Exception                                                                                                         //Natural: UPDATE-CIS-BENES-FROM-MDO
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        cis_Updt_Cis_Prmry_Bene_Std_Txt.getValue(1,":",20).reset();                                                                                                       //Natural: RESET CIS-UPDT.CIS-PRMRY-BENE-STD-TXT ( 1:20 ) CIS-UPDT.CIS-PRMRY-BENE-LUMP-SUM ( 1:20 ) CIS-UPDT.CIS-PRMRY-BENE-AUTO-CMNT-VAL ( 1:20 ) CIS-UPDT.CIS-PRMRY-BENE-NAME ( 1:20 ) CIS-UPDT.CIS-PRMRY-BENE-EXTNDD-NME ( 1:20 ) CIS-UPDT.CIS-PRMRY-BENE-RLTN ( 1:20 ) CIS-UPDT.CIS-PRMRY-BENE-RLTN-CDE ( 1:20 ) CIS-UPDT.CIS-PRMRY-BENE-SSN-NBR ( 1:20 ) CIS-UPDT.CIS-PRMRY-BENE-DOB ( 1:20 ) CIS-UPDT.CIS-PRMRY-BENE-ALLCTN-PCT ( 1:20 ) CIS-UPDT.CIS-PRMRY-BENE-NMRTR-NBR ( 1:20 ) CIS-UPDT.CIS-PRMRY-BENE-DNMNTR-NBR ( 1:20 ) CIS-UPDT.CIS-PRMRY-BENE-SPCL-TXT ( 1:20,1:3 ) CIS-UPDT.CIS-CNTGNT-BENE-STD-TXT ( 1:20 ) CIS-UPDT.CIS-CNTGNT-BENE-LUMP-SUM ( 1:20 ) CIS-UPDT.CIS-CNTGNT-BENE-AUTO-CMNT-VAL ( 1:20 ) CIS-UPDT.CIS-CNTGNT-BENE-NAME ( 1:20 ) CIS-UPDT.CIS-CNTGNT-BENE-EXTNDD-NME ( 1:20 ) CIS-UPDT.CIS-CNTGNT-BENE-RLTN ( 1:20 ) CIS-UPDT.CIS-CNTGNT-BENE-RLTN-CDE ( 1:20 ) CIS-UPDT.CIS-CNTGNT-BENE-SSN-NBR ( 1:20 ) CIS-UPDT.CIS-CNTGNT-BENE-DOB ( 1:20 ) CIS-UPDT.CIS-CNTGNT-BENE-ALLCTN-PCT ( 1:20 ) CIS-UPDT.CIS-CNTGNT-BENE-NMRTR-NBR ( 1:20 ) CIS-UPDT.CIS-CNTGNT-BENE-DNMNTR-NBR ( 1:20 ) CIS-UPDT.CIS-CNTGNT-BENE-SPCL-TXT ( 1:20,1:3 )
        cis_Updt_Cis_Prmry_Bene_Lump_Sum.getValue(1,":",20).reset();
        cis_Updt_Cis_Prmry_Bene_Auto_Cmnt_Val.getValue(1,":",20).reset();
        cis_Updt_Cis_Prmry_Bene_Name.getValue(1,":",20).reset();
        cis_Updt_Cis_Prmry_Bene_Extndd_Nme.getValue(1,":",20).reset();
        cis_Updt_Cis_Prmry_Bene_Rltn.getValue(1,":",20).reset();
        cis_Updt_Cis_Prmry_Bene_Rltn_Cde.getValue(1,":",20).reset();
        cis_Updt_Cis_Prmry_Bene_Ssn_Nbr.getValue(1,":",20).reset();
        cis_Updt_Cis_Prmry_Bene_Dob.getValue(1,":",20).reset();
        cis_Updt_Cis_Prmry_Bene_Allctn_Pct.getValue(1,":",20).reset();
        cis_Updt_Cis_Prmry_Bene_Nmrtr_Nbr.getValue(1,":",20).reset();
        cis_Updt_Cis_Prmry_Bene_Dnmntr_Nbr.getValue(1,":",20).reset();
        cis_Updt_Cis_Prmry_Bene_Spcl_Txt.getValue(1,":",20,1,":",3).reset();
        cis_Updt_Cis_Cntgnt_Bene_Std_Txt.getValue(1,":",20).reset();
        cis_Updt_Cis_Cntgnt_Bene_Lump_Sum.getValue(1,":",20).reset();
        cis_Updt_Cis_Cntgnt_Bene_Auto_Cmnt_Val.getValue(1,":",20).reset();
        cis_Updt_Cis_Cntgnt_Bene_Name.getValue(1,":",20).reset();
        cis_Updt_Cis_Cntgnt_Bene_Extndd_Nme.getValue(1,":",20).reset();
        cis_Updt_Cis_Cntgnt_Bene_Rltn.getValue(1,":",20).reset();
        cis_Updt_Cis_Cntgnt_Bene_Rltn_Cde.getValue(1,":",20).reset();
        cis_Updt_Cis_Cntgnt_Bene_Ssn_Nbr.getValue(1,":",20).reset();
        cis_Updt_Cis_Cntgnt_Bene_Dob.getValue(1,":",20).reset();
        cis_Updt_Cis_Cntgnt_Bene_Allctn_Pct.getValue(1,":",20).reset();
        cis_Updt_Cis_Cntgnt_Bene_Nmrtr_Nbr.getValue(1,":",20).reset();
        cis_Updt_Cis_Cntgnt_Bene_Dnmntr_Nbr.getValue(1,":",20).reset();
        cis_Updt_Cis_Cntgnt_Bene_Spcl_Txt.getValue(1,":",20,1,":",3).reset();
        if (condition(mstr_03_Prmry_Bnfcry_Nme.getValue(1).equals("See Designation Text")))                                                                               //Natural: IF MSTR-03.PRMRY-BNFCRY-NME ( 1 ) = 'See Designation Text'
        {
            cis_Updt_Cis_Bene_Std_Free.setValue("Y");                                                                                                                     //Natural: ASSIGN CIS-UPDT.CIS-BENE-STD-FREE := 'Y'
                                                                                                                                                                          //Natural: PERFORM CREATE-OR-UPDATE-02-DESIG
            sub_Create_Or_Update_02_Desig();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            cis_Updt_Cis_Bene_Std_Free.setValue("N");                                                                                                                     //Natural: ASSIGN CIS-UPDT.CIS-BENE-STD-FREE := 'N'
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                FOR02:                                                                                                                                                    //Natural: FOR #J 1 3
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(3)); pnd_J.nadd(1))
                {
                    if (condition(mstr_03_Prmry_Bnfcry_Spcl_Txt.getValue(pnd_I,pnd_J).equals(" ")))                                                                       //Natural: IF MSTR-03.PRMRY-BNFCRY-SPCL-TXT ( #I,#J ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    cis_Updt_Cis_Prmry_Bene_Spcl_Txt.getValue(pnd_I,pnd_J).setValue(mstr_03_Prmry_Bnfcry_Spcl_Txt.getValue(pnd_I,pnd_J));                                 //Natural: ASSIGN CIS-UPDT.CIS-PRMRY-BENE-SPCL-TXT ( #I,#J ) := MSTR-03.PRMRY-BNFCRY-SPCL-TXT ( #I,#J )
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            cis_Updt_Cis_Prmry_Bene_Name.getValue(1,":",20).setValue(mstr_03_Prmry_Bnfcry_Nme.getValue(1,":",20));                                                        //Natural: ASSIGN CIS-UPDT.CIS-PRMRY-BENE-NAME ( 1:20 ) := MSTR-03.PRMRY-BNFCRY-NME ( 1:20 )
            cis_Updt_Cis_Prmry_Bene_Extndd_Nme.getValue(1,":",20).setValue(mstr_03_Prmry_Bnfcry_Extndd_Nme.getValue(1,":",20));                                           //Natural: ASSIGN CIS-UPDT.CIS-PRMRY-BENE-EXTNDD-NME ( 1:20 ) := MSTR-03.PRMRY-BNFCRY-EXTNDD-NME ( 1:20 )
            if (condition(mstr_03_Prmry_Bnfcry_Rltn_Cde.getValue(1).equals("S")))                                                                                         //Natural: IF MSTR-03.PRMRY-BNFCRY-RLTN-CDE ( 1 ) = 'S'
            {
                cis_Updt_Cis_Prmry_Bene_Rltn.getValue(1).setValue("Spouse");                                                                                              //Natural: ASSIGN CIS-UPDT.CIS-PRMRY-BENE-RLTN ( 1 ) := 'Spouse'
            }                                                                                                                                                             //Natural: END-IF
            cis_Updt_Cis_Prmry_Bene_Rltn_Cde.getValue(1,":",20).setValue(mstr_03_Prmry_Bnfcry_Rltn_Cde.getValue(1,":",20));                                               //Natural: ASSIGN CIS-UPDT.CIS-PRMRY-BENE-RLTN-CDE ( 1:20 ) := MSTR-03.PRMRY-BNFCRY-RLTN-CDE ( 1:20 )
            cis_Updt_Cis_Prmry_Bene_Ssn_Nbr.getValue(1,":",20).setValue(mstr_03_Prmry_Bnfcry_Ssn_Nbr.getValue(1,":",20));                                                 //Natural: ASSIGN CIS-UPDT.CIS-PRMRY-BENE-SSN-NBR ( 1:20 ) := MSTR-03.PRMRY-BNFCRY-SSN-NBR ( 1:20 )
            FOR03:                                                                                                                                                        //Natural: FOR #I 1 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                if (condition(mstr_03_Prmry_Bnfcry_Birth_Dte.getValue(pnd_I).equals(getZero())))                                                                          //Natural: IF MSTR-03.PRMRY-BNFCRY-BIRTH-DTE ( #I ) = 0
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_W_Date_N.setValue(mstr_03_Prmry_Bnfcry_Birth_Dte.getValue(pnd_I));                                                                                    //Natural: ASSIGN #W-DATE-N := MSTR-03.PRMRY-BNFCRY-BIRTH-DTE ( #I )
                cis_Updt_Cis_Prmry_Bene_Dob.getValue(pnd_I).setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_W_Date_N_Pnd_W_Date_A);                                     //Natural: MOVE EDITED #W-DATE-A TO CIS-UPDT.CIS-PRMRY-BENE-DOB ( #I ) ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            cis_Updt_Cis_Prmry_Bene_Allctn_Pct.getValue(1,":",20).setValue(mstr_03_Prmry_Bnfcry_Allctn_Pct.getValue(1,":",20));                                           //Natural: ASSIGN CIS-UPDT.CIS-PRMRY-BENE-ALLCTN-PCT ( 1:20 ) := MSTR-03.PRMRY-BNFCRY-ALLCTN-PCT ( 1:20 )
            cis_Updt_Cis_Prmry_Bene_Nmrtr_Nbr.getValue(1,":",20).setValue(mstr_03_Prmry_Bnfcry_Nmrtr_Nbr.getValue(1,":",20));                                             //Natural: ASSIGN CIS-UPDT.CIS-PRMRY-BENE-NMRTR-NBR ( 1:20 ) := MSTR-03.PRMRY-BNFCRY-NMRTR-NBR ( 1:20 )
            cis_Updt_Cis_Prmry_Bene_Dnmntr_Nbr.getValue(1,":",20).setValue(mstr_03_Prmry_Bnfcry_Dnmntr_Nbr.getValue(1,":",20));                                           //Natural: ASSIGN CIS-UPDT.CIS-PRMRY-BENE-DNMNTR-NBR ( 1:20 ) := MSTR-03.PRMRY-BNFCRY-DNMNTR-NBR ( 1:20 )
            cis_Updt_Cis_Cntgnt_Bene_Name.getValue(1,":",20).setValue(mstr_03_Cntngnt_Bnfcry_Nme.getValue(1,":",20));                                                     //Natural: ASSIGN CIS-UPDT.CIS-CNTGNT-BENE-NAME ( 1:20 ) := MSTR-03.CNTNGNT-BNFCRY-NME ( 1:20 )
            cis_Updt_Cis_Cntgnt_Bene_Extndd_Nme.getValue(1,":",20).setValue(mstr_03_Cntngnt_Bnfcry_Extndd_Nme.getValue(1,":",20));                                        //Natural: ASSIGN CIS-UPDT.CIS-CNTGNT-BENE-EXTNDD-NME ( 1:20 ) := MSTR-03.CNTNGNT-BNFCRY-EXTNDD-NME ( 1:20 )
            cis_Updt_Cis_Cntgnt_Bene_Rltn_Cde.getValue(1,":",20).setValue(mstr_03_Cntngnt_Bnfcry_Rltn_Cde.getValue(1,":",20));                                            //Natural: ASSIGN CIS-UPDT.CIS-CNTGNT-BENE-RLTN-CDE ( 1:20 ) := MSTR-03.CNTNGNT-BNFCRY-RLTN-CDE ( 1:20 )
            FOR04:                                                                                                                                                        //Natural: FOR #I 1 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                if (condition(cis_Updt_Cis_Cntgnt_Bene_Rltn_Cde.getValue(pnd_I).equals(" ")))                                                                             //Natural: IF CIS-UPDT.CIS-CNTGNT-BENE-RLTN-CDE ( #I ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet737 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CIS-UPDT.CIS-CNTGNT-BENE-RLTN-CDE ( #I ) = 'S'
                if (condition(cis_Updt_Cis_Cntgnt_Bene_Rltn_Cde.getValue(pnd_I).equals("S")))
                {
                    decideConditionsMet737++;
                    cis_Updt_Cis_Cntgnt_Bene_Rltn.getValue(pnd_I).setValue("Spouse");                                                                                     //Natural: ASSIGN CIS-UPDT.CIS-CNTGNT-BENE-RLTN ( #I ) := 'Spouse'
                }                                                                                                                                                         //Natural: WHEN CIS-UPDT.CIS-CNTGNT-BENE-RLTN-CDE ( #I ) = 'K' OR = 'L' OR = 'T'
                else if (condition(cis_Updt_Cis_Cntgnt_Bene_Rltn_Cde.getValue(pnd_I).equals("K") || cis_Updt_Cis_Cntgnt_Bene_Rltn_Cde.getValue(pnd_I).equals("L") 
                    || cis_Updt_Cis_Cntgnt_Bene_Rltn_Cde.getValue(pnd_I).equals("T")))
                {
                    decideConditionsMet737++;
                    cis_Updt_Cis_Cntgnt_Bene_Rltn.getValue(pnd_I).setValue("Trust");                                                                                      //Natural: ASSIGN CIS-UPDT.CIS-CNTGNT-BENE-RLTN ( #I ) := 'Trust'
                }                                                                                                                                                         //Natural: WHEN CIS-UPDT.CIS-CNTGNT-BENE-RLTN-CDE ( #I ) = 'I'
                else if (condition(cis_Updt_Cis_Cntgnt_Bene_Rltn_Cde.getValue(pnd_I).equals("I")))
                {
                    decideConditionsMet737++;
                    cis_Updt_Cis_Cntgnt_Bene_Rltn.getValue(pnd_I).setValue("Org.");                                                                                       //Natural: ASSIGN CIS-UPDT.CIS-CNTGNT-BENE-RLTN ( #I ) := 'Org.'
                }                                                                                                                                                         //Natural: WHEN CIS-UPDT.CIS-CNTGNT-BENE-RLTN-CDE ( #I ) = 'E'
                else if (condition(cis_Updt_Cis_Cntgnt_Bene_Rltn_Cde.getValue(pnd_I).equals("E")))
                {
                    decideConditionsMet737++;
                    cis_Updt_Cis_Cntgnt_Bene_Rltn.getValue(pnd_I).setValue("Estate");                                                                                     //Natural: ASSIGN CIS-UPDT.CIS-CNTGNT-BENE-RLTN ( #I ) := 'Estate'
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    cis_Updt_Cis_Cntgnt_Bene_Rltn.getValue(pnd_I).setValue("Non-Spouse");                                                                                 //Natural: ASSIGN CIS-UPDT.CIS-CNTGNT-BENE-RLTN ( #I ) := 'Non-Spouse'
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            cis_Updt_Cis_Cntgnt_Bene_Ssn_Nbr.getValue(1,":",20).setValue(mstr_03_Cntngnt_Bnfcry_Ssn_Nbr.getValue(1,":",20));                                              //Natural: ASSIGN CIS-UPDT.CIS-CNTGNT-BENE-SSN-NBR ( 1:20 ) := MSTR-03.CNTNGNT-BNFCRY-SSN-NBR ( 1:20 )
            FOR05:                                                                                                                                                        //Natural: FOR #I 1 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                if (condition(mstr_03_Cntngnt_Bnfcry_Birth_Dte.getValue(pnd_I).equals(getZero())))                                                                        //Natural: IF MSTR-03.CNTNGNT-BNFCRY-BIRTH-DTE ( #I ) = 0
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_W_Date_N.setValue(mstr_03_Cntngnt_Bnfcry_Birth_Dte.getValue(pnd_I));                                                                                  //Natural: ASSIGN #W-DATE-N := MSTR-03.CNTNGNT-BNFCRY-BIRTH-DTE ( #I )
                cis_Updt_Cis_Cntgnt_Bene_Dob.getValue(pnd_I).setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_W_Date_N_Pnd_W_Date_A);                                    //Natural: MOVE EDITED #W-DATE-A TO CIS-UPDT.CIS-CNTGNT-BENE-DOB ( #I ) ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            cis_Updt_Cis_Cntgnt_Bene_Allctn_Pct.getValue(1,":",20).setValue(mstr_03_Cntngnt_Bnfcry_Allctn_Pct.getValue(1,":",20));                                        //Natural: ASSIGN CIS-UPDT.CIS-CNTGNT-BENE-ALLCTN-PCT ( 1:20 ) := MSTR-03.CNTNGNT-BNFCRY-ALLCTN-PCT ( 1:20 )
            cis_Updt_Cis_Cntgnt_Bene_Nmrtr_Nbr.getValue(1,":",20).setValue(mstr_03_Cntngnt_Bnfcry_Nmrtr_Nbr.getValue(1,":",20));                                          //Natural: ASSIGN CIS-UPDT.CIS-CNTGNT-BENE-NMRTR-NBR ( 1:20 ) := MSTR-03.CNTNGNT-BNFCRY-NMRTR-NBR ( 1:20 )
            cis_Updt_Cis_Cntgnt_Bene_Dnmntr_Nbr.getValue(1,":",20).setValue(mstr_03_Cntngnt_Bnfcry_Dnmntr_Nbr.getValue(1,":",20));                                        //Natural: ASSIGN CIS-UPDT.CIS-CNTGNT-BENE-DNMNTR-NBR ( 1:20 ) := MSTR-03.CNTNGNT-BNFCRY-DNMNTR-NBR ( 1:20 )
            cis_Updt_Cis_Cntgnt_Bene_Spcl_Txt.getValue(1,":",20,1,":",3).setValue(mstr_03_Cntngnt_Bnfcry_Spcl_Txt.getValue(1,":",20,1,":",3));                            //Natural: ASSIGN CIS-UPDT.CIS-CNTGNT-BENE-SPCL-TXT ( 1:20,1:3 ) := MSTR-03.CNTNGNT-BNFCRY-SPCL-TXT ( 1:20,1:3 )
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, Global.getPROGRAM(),new TabSetting(20),"NEW MDO CONTRACTS WITHOUT CALC BENE");                                                  //Natural: WRITE ( 1 ) *PROGRAM 20T 'NEW MDO CONTRACTS WITHOUT CALC BENE'
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133");

        getReports().setDisplayColumns(1, "PIN",
        		pnd_Input_Pnd_Ph_Pin,"NEW MDO/CONTRACT",
        		ldaCisl2081.getCis_Bene_File_01_Cis_Bene_Tiaa_Nbr(),"BENE NAME",
        		cis_Updt_Cis_Clcltn_Bene_Name,"RC",
        		cis_Updt_Cis_Clcltn_Bene_Rltnshp_Cde,"SSN",
        		cis_Updt_Cis_Clcltn_Bene_Ssn_Nbr, new ReportEditMask ("999999999"),"BENE DOB",
        		dob2_Date, new ReportEditMask ("XXXX/XX/XX"),"SEX",
        		cis_Updt_Cis_Clcltn_Bene_Sex_Cde);
    }
}
