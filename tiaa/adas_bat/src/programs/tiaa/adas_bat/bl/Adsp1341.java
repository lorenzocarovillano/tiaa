/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:01:59 PM
**        * FROM NATURAL PROGRAM : Adsp1341
************************************************************
**        * FILE NAME            : Adsp1341.java
**        * CLASS NAME           : Adsp1341
**        * INSTANCE NAME        : Adsp1341
************************************************************
************************************************************************
* PROGRAM  : ADSP1341
* GENERATED: APRIL 15, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : THIS MODULE READS WORK FILE TAD.P05.PVT.ADSP720
*            AND REPORT THE DA TO IA MATURITY PAYMENTS BY MODE
*            WITHIN ADI-INSTALLMNT-DATE. (REC 99 AND > 0)
*            (MATURITIES TO CPS INTERFACE) SURVIVOR INFORMATION ONLY.
*
* REMARKS  : CLONED FROM ADSP1340 (ADAS SYSTEM)
*********************  MAINTENANCE LOG *********************************
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* 11/17/00  F.TWAHIR TPA AND IO CHANGES
*                ALL ACCUMULATIONS WERE TABLEIZED. THERE ARE 5
*                OCCURRENCES AND #M IS THE REFERENCE. 1 = MONTHLY
*                2 = QUARTERLT 3 = SEMI-ANNUALS 4 = ANNUALS 5
*                5 = GRAND TOTALS. #D IS USED TO REFERENCE THE FIELD IN
*                MAP NAZM1341. FOR TIAA ACCUMULATIONS #D = 1 IS THE TIAA
*                STANDARD AMOUNT. #D = 4 IS THE IPRO ROLLOVER AMOUNT
*                FOR CREF #C IS USED FOR THE MODE. #T IS FOR TOTAL LINES
*
* M002 10/10/01  F.TWAHIR....................
* THE IVC BUCKET SHOULD ONLY BE USED FOR AN INTERNAL ROLLOVER PORTION
* OF THE PAYMENT THAT IS IVC AND WILL BE PAID TO THE PARTICIPANT. THE
* PORTION THAT IS ACTUALLY ROLLED INTO THE IRA GOES INTO THE INTERNAL
* ROLLOVER BUCKET. THE SUM OF THE IVC AND INT-ROLLOVER BUCKET SHOULD
* EQUAL THE TOTAL PAYMENT AMOUNT, IN AN INTERNAL IRA W/IVC CASE.
* PUT IVC PORTIONS (OF PAYMENTS) FOR CASH AND IRAX INTO CASH,
* NOT THE IVC BUCKET. NO IVC FOR RINV OR DTRA
*
* 04/29/03  J. VLISMAS - BREAK OUT "CASH" AND "INTERNAL ROLLOVERS"
*                        FOR TIAA STANDARD REQUESTS. THIS WILL BE
*                        REFELECTED ON THE FIRST 3 LINES OF THE
*                        REPORT
*
* 11/03     E. MELNIK  - BREAK OUT IPRO EXTERNAL ROLLOVERS FROM
*                        CASH AND ADD A SEPARATE LINE FOR THEM.
*                        INCLUDE EXTERNAL ROLLOVERS (FOR ACS) AS
*                        PART OF THE 'TIAA STD CASH' LINE TOTALS.
*                        VALUES OF INDEXES WERE ADJUSTED.  OTHER
*                        CHANGES MARKED WITH 'EM - 11/03' AND
*                        'EM - 12/03'.
* 04/15/04  C. AVE       MODIFIED FOR ADAS (ANNUITIZATION SUNGUARD)
* 05/06/08  GUERRERO     ADDED NEW ROTH ONLY REPORT.
* 12/18/08  SACHARNY ADD "Rea Access" MTH/ANN ACCUMS TO REPORT RS0
* 03/24/10  D.E.ANDER    ADABAS REPLATFORM PCPOP - SURVIVOR INFO DEA
* 03/05/12 E. MELNIK   RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                      UPDATED LINKAGE AREAS.  CHANGES MARKED BY
*                      EM - 030512.
* 10/2013   O. SOTTO  CREF REA CHANGES MARKED WITH OS-102013.
* 02/24/2017 R.CARREON PIN EXPANSION 02242017
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp1341 extends BLNatBase
{
    // Data Areas
    private LdaAdsl1340 ldaAdsl1340;
    private LdaAdsl450 ldaAdsl450;
    private LdaAdsl450a ldaAdsl450a;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ads_Ia_Rslt2;
    private DbsField ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data;

    private DbsGroup ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt;

    private DbsGroup ads_Ia_Rslt2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt;
    private DbsField ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt;
    private DbsGroup ads_Ia_Rslt2_Adi_Contract_IdMuGroup;
    private DbsField ads_Ia_Rslt2_Adi_Contract_Id;
    private DbsField pnd_Rec_Type;

    private DbsGroup pnd_Rec_Type__R_Field_1;
    private DbsField pnd_Rec_Type_Pnd_Filler;
    private DbsField pnd_Rec_Type_Pnd_Z_Indx;
    private DbsField pnd_Nap_Alt_Dest_Rlvr_Dest;
    private DbsField pnd_Nap_Annty_Optn_Cde;

    private DbsGroup pnd_Pas_Ext_Cntrl_02_Work_Area;

    private DbsGroup pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Table;
    private DbsField pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1;

    private DataAccessProgramView vw_ads_Cntl_View;

    private DbsGroup ads_Cntl_View_Ads_Cntl_Grp;
    private DbsField ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte;
    private DbsField ads_Cntl_View_Ads_Daily_Actvty_Dte;
    private DbsField ads_Cntl_View_Ads_Rpt_13;

    private DbsGroup pnd_All_Indx;
    private DbsField pnd_All_Indx_Pnd_P;
    private DbsField pnd_All_Indx_Pnd_Y;
    private DbsField pnd_All_Indx_Pnd_Z;
    private DbsField pnd_All_Indx_Pnd_Z_Tot;
    private DbsField pnd_All_Indx_Pnd_A;
    private DbsField pnd_All_Indx_Pnd_B_Indx;
    private DbsField pnd_All_Indx_Pnd_C_Indx;
    private DbsField pnd_All_Indx_Pnd_Next_Indx;
    private DbsField pnd_All_Indx_Pnd_Prod_Indx;
    private DbsField pnd_All_Indx_Pnd_Ivc_Cnt;
    private DbsField pnd_C;
    private DbsField pnd_M;
    private DbsField pnd_D;
    private DbsField pnd_T;
    private DbsField pnd_X;
    private DbsField pnd_Rec_Read;
    private DbsField pnd_Total_Monthly_Rec;
    private DbsField pnd_Total_Semi_Annual_Rec;
    private DbsField pnd_Total_Quartly_Rec;
    private DbsField pnd_Total_Annual_Rec;
    private DbsField pnd_Total_Number_Rec;
    private DbsField pnd_First_Time;
    private DbsField pnd_Tiaa_Prod_Code;
    private DbsField pnd_Cref_Prod_Found;
    private DbsField pnd_Surv_Only_Run;
    private DbsField pnd_Prev_Bsnss_Dte_D;
    private DbsField pnd_Prev_Bsnss_Dte_N;

    private DbsGroup pnd_Prev_Bsnss_Dte_N__R_Field_2;
    private DbsField pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A;
    private DbsField pnd_Today_Accounting_Date;
    private DbsField pnd_Today_Business_Date;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd;

    private DbsGroup pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_3;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm;
    private DbsField pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd;
    private DbsField pnd_Curr_Accounting_Dte_Yyyymmdd;

    private DbsGroup pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_4;
    private DbsField pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Cc;
    private DbsField pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Yy;
    private DbsField pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Mm;
    private DbsField pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Dd;
    private DbsField pnd_Rqst_Id;

    private DbsGroup pnd_Rqst_Id__R_Field_5;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Unique_Id;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte;

    private DbsGroup pnd_Rqst_Id__R_Field_6;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Cc;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Yy;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Mm;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Dd;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Entry_Dte;
    private DbsField pnd_Rqst_Id_Pnd_Nap_Entry_Tme;
    private DbsField pnd_Prev_Payment_Due_Date;

    private DbsGroup pnd_Prev_Payment_Due_Date__R_Field_7;
    private DbsField pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Cc;
    private DbsField pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Yy;
    private DbsField pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Mm;
    private DbsField pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Dd;
    private DbsField pnd_Curr_Payment_Due_Date;

    private DbsGroup pnd_Curr_Payment_Due_Date__R_Field_8;
    private DbsField pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Cc;
    private DbsField pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Yy;
    private DbsField pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Mm;
    private DbsField pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Dd;
    private DbsField pnd_Payment_Mode;

    private DbsGroup pnd_Payment_Mode__R_Field_9;
    private DbsField pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1;
    private DbsField pnd_Payment_Mode_Pnd_Payment_Mode_Byte_2;
    private DbsField pnd_Mode_Desc;
    private DbsField pnd_Pec_Nbr_Active_Acct;
    private DbsField pnd_Heading;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_10;
    private DbsField pnd_Date_A_Pnd_Date_N;
    private DbsField pnd_Ads_Ia_Super1;

    private DbsGroup pnd_Ads_Ia_Super1__R_Field_11;
    private DbsField pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rqst_Id;
    private DbsField pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde;
    private DbsField pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr;
    private DbsField pnd_Max_Rslt;
    private DbsField pnd_Max_Rates;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl1340 = new LdaAdsl1340();
        registerRecord(ldaAdsl1340);
        ldaAdsl450 = new LdaAdsl450();
        registerRecord(ldaAdsl450);
        registerRecord(ldaAdsl450.getVw_ads_Ia_Rslt_View());
        ldaAdsl450a = new LdaAdsl450a();
        registerRecord(ldaAdsl450a);

        // Local Variables
        localVariables = new DbsRecord();

        vw_ads_Ia_Rslt2 = new DataAccessProgramView(new NameInfo("vw_ads_Ia_Rslt2", "ADS-IA-RSLT2"), "ADS_IA_RSLT", "ADS_IA_RSLT", DdmPeriodicGroups.getInstance().getGroups("ADS_IA_RSLT"));
        ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data = vw_ads_Ia_Rslt2.getRecord().newFieldInGroup("ads_Ia_Rslt2_Count_Castadi_Dtl_Tiaa_Data", "C*ADI-DTL-TIAA-DATA", 
            RepeatingFieldStrategy.CAsteriskVariable, "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");

        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data = vw_ads_Ia_Rslt2.getRecord().newGroupInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data", "ADI-DTL-TIAA-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd", "ADI-DTL-TIAA-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt", "ADI-DTL-TIAA-DA-STD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt", "ADI-DTL-TIAA-DA-GRD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd", "ADI-DTL-TIAA-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_ACCT_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd", "ADI-DTL-TIAA-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_IA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst", "ADI-DTL-TIAA-EFF-RTE-INTRST", 
            FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_EFF_RTE_INTRST", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt", "ADI-DTL-TIAA-GRD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt", "ADI-DTL-TIAA-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt", "ADI-DTL-FNL-GRD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt", "ADI-DTL-FNL-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt", "ADI-DTL-TIAA-STD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt", "ADI-DTL-TIAA-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt", "ADI-DTL-FNL-STD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt", "ADI-DTL-FNL-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");

        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat = vw_ads_Ia_Rslt2.getRecord().newGroupInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat", "ADI-DTL-TIAA-TPA-GUAR-COMMUT-DAT", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt", 
            "ADI-DTL-TIAA-GUAR-COMMUT-STD-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_TIAA_GUAR_COMMUT_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt = ads_Ia_Rslt2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt", 
            "ADI-DTL-TIAA-GUAR-COMMUT-GRD-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_TIAA_GUAR_COMMUT_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        ads_Ia_Rslt2_Adi_Contract_IdMuGroup = vw_ads_Ia_Rslt2.getRecord().newGroupInGroup("ADS_IA_RSLT2_ADI_CONTRACT_IDMuGroup", "ADI_CONTRACT_IDMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_IA_RSLT_ADI_CONTRACT_ID");
        ads_Ia_Rslt2_Adi_Contract_Id = ads_Ia_Rslt2_Adi_Contract_IdMuGroup.newFieldArrayInGroup("ads_Ia_Rslt2_Adi_Contract_Id", "ADI-CONTRACT-ID", FieldType.NUMERIC, 
            11, new DbsArrayController(1, 125), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_CONTRACT_ID");
        registerRecord(vw_ads_Ia_Rslt2);

        pnd_Rec_Type = localVariables.newFieldInRecord("pnd_Rec_Type", "#REC-TYPE", FieldType.NUMERIC, 3);

        pnd_Rec_Type__R_Field_1 = localVariables.newGroupInRecord("pnd_Rec_Type__R_Field_1", "REDEFINE", pnd_Rec_Type);
        pnd_Rec_Type_Pnd_Filler = pnd_Rec_Type__R_Field_1.newFieldInGroup("pnd_Rec_Type_Pnd_Filler", "#FILLER", FieldType.NUMERIC, 2);
        pnd_Rec_Type_Pnd_Z_Indx = pnd_Rec_Type__R_Field_1.newFieldInGroup("pnd_Rec_Type_Pnd_Z_Indx", "#Z-INDX", FieldType.NUMERIC, 1);
        pnd_Nap_Alt_Dest_Rlvr_Dest = localVariables.newFieldInRecord("pnd_Nap_Alt_Dest_Rlvr_Dest", "#NAP-ALT-DEST-RLVR-DEST", FieldType.STRING, 4);
        pnd_Nap_Annty_Optn_Cde = localVariables.newFieldInRecord("pnd_Nap_Annty_Optn_Cde", "#NAP-ANNTY-OPTN-CDE", FieldType.STRING, 2);

        pnd_Pas_Ext_Cntrl_02_Work_Area = localVariables.newGroupInRecord("pnd_Pas_Ext_Cntrl_02_Work_Area", "#PAS-EXT-CNTRL-02-WORK-AREA");

        pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Table = pnd_Pas_Ext_Cntrl_02_Work_Area.newGroupArrayInGroup("pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Table", 
            "#PEC-ACCT-TABLE", new DbsArrayController(1, 20));
        pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1 = pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Table.newFieldInGroup("pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1", 
            "#PEC-ACCT-NAME-1", FieldType.STRING, 1);

        vw_ads_Cntl_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntl_View", "ADS-CNTL-VIEW"), "ADS_CNTL", "ADS_CNTL");

        ads_Cntl_View_Ads_Cntl_Grp = vw_ads_Cntl_View.getRecord().newGroupInGroup("ADS_CNTL_VIEW_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde", "ADS-CNTL-RCRD-TYP-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADS_CNTL_RCRD_TYP_CDE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte", "ADS-CNTL-BSNSS-RCPRCL-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_RCPRCL_DTE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_DTE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte", "ADS-CNTL-BSNSS-TMRRW-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_TMRRW_DTE");
        ads_Cntl_View_Ads_Daily_Actvty_Dte = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Daily_Actvty_Dte", "ADS-DAILY-ACTVTY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADS_DAILY_ACTVTY_DTE");
        ads_Cntl_View_Ads_Rpt_13 = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Rpt_13", "ADS-RPT-13", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADS_RPT_13");
        registerRecord(vw_ads_Cntl_View);

        pnd_All_Indx = localVariables.newGroupInRecord("pnd_All_Indx", "#ALL-INDX");
        pnd_All_Indx_Pnd_P = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_P", "#P", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Y = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Y", "#Y", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Z = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Z", "#Z", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Z_Tot = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Z_Tot", "#Z-TOT", FieldType.NUMERIC, 11, 2);
        pnd_All_Indx_Pnd_A = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_B_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_B_Indx", "#B-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_C_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_C_Indx", "#C-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Next_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Next_Indx", "#NEXT-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Prod_Indx = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Prod_Indx", "#PROD-INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_All_Indx_Pnd_Ivc_Cnt = pnd_All_Indx.newFieldInGroup("pnd_All_Indx_Pnd_Ivc_Cnt", "#IVC-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.PACKED_DECIMAL, 3);
        pnd_M = localVariables.newFieldInRecord("pnd_M", "#M", FieldType.PACKED_DECIMAL, 3);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.PACKED_DECIMAL, 3);
        pnd_T = localVariables.newFieldInRecord("pnd_T", "#T", FieldType.PACKED_DECIMAL, 3);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);
        pnd_Rec_Read = localVariables.newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.PACKED_DECIMAL, 3);
        pnd_Total_Monthly_Rec = localVariables.newFieldInRecord("pnd_Total_Monthly_Rec", "#TOTAL-MONTHLY-REC", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Semi_Annual_Rec = localVariables.newFieldInRecord("pnd_Total_Semi_Annual_Rec", "#TOTAL-SEMI-ANNUAL-REC", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Quartly_Rec = localVariables.newFieldInRecord("pnd_Total_Quartly_Rec", "#TOTAL-QUARTLY-REC", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Annual_Rec = localVariables.newFieldInRecord("pnd_Total_Annual_Rec", "#TOTAL-ANNUAL-REC", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Number_Rec = localVariables.newFieldInRecord("pnd_Total_Number_Rec", "#TOTAL-NUMBER-REC", FieldType.PACKED_DECIMAL, 7);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Prod_Code = localVariables.newFieldInRecord("pnd_Tiaa_Prod_Code", "#TIAA-PROD-CODE", FieldType.BOOLEAN, 1);
        pnd_Cref_Prod_Found = localVariables.newFieldInRecord("pnd_Cref_Prod_Found", "#CREF-PROD-FOUND", FieldType.BOOLEAN, 1);
        pnd_Surv_Only_Run = localVariables.newFieldInRecord("pnd_Surv_Only_Run", "#SURV-ONLY-RUN", FieldType.BOOLEAN, 1);
        pnd_Prev_Bsnss_Dte_D = localVariables.newFieldInRecord("pnd_Prev_Bsnss_Dte_D", "#PREV-BSNSS-DTE-D", FieldType.DATE);
        pnd_Prev_Bsnss_Dte_N = localVariables.newFieldInRecord("pnd_Prev_Bsnss_Dte_N", "#PREV-BSNSS-DTE-N", FieldType.NUMERIC, 8);

        pnd_Prev_Bsnss_Dte_N__R_Field_2 = localVariables.newGroupInRecord("pnd_Prev_Bsnss_Dte_N__R_Field_2", "REDEFINE", pnd_Prev_Bsnss_Dte_N);
        pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A = pnd_Prev_Bsnss_Dte_N__R_Field_2.newFieldInGroup("pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A", "#PREV-BSNSS-DTE-A", 
            FieldType.STRING, 8);
        pnd_Today_Accounting_Date = localVariables.newFieldInRecord("pnd_Today_Accounting_Date", "#TODAY-ACCOUNTING-DATE", FieldType.STRING, 8);
        pnd_Today_Business_Date = localVariables.newFieldInRecord("pnd_Today_Business_Date", "#TODAY-BUSINESS-DATE", FieldType.STRING, 8);
        pnd_Prev_Accounting_Dte_Yyyymmdd = localVariables.newFieldInRecord("pnd_Prev_Accounting_Dte_Yyyymmdd", "#PREV-ACCOUNTING-DTE-YYYYMMDD", FieldType.STRING, 
            8);

        pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_3 = localVariables.newGroupInRecord("pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_3", "REDEFINE", pnd_Prev_Accounting_Dte_Yyyymmdd);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_3.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc", 
            "#PREV-ACCOUNT-DTE-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_3.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy", 
            "#PREV-ACCOUNT-DTE-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_3.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm", 
            "#PREV-ACCOUNT-DTE-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd = pnd_Prev_Accounting_Dte_Yyyymmdd__R_Field_3.newFieldInGroup("pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd", 
            "#PREV-ACCOUNT-DTE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Curr_Accounting_Dte_Yyyymmdd = localVariables.newFieldInRecord("pnd_Curr_Accounting_Dte_Yyyymmdd", "#CURR-ACCOUNTING-DTE-YYYYMMDD", FieldType.STRING, 
            8);

        pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_4 = localVariables.newGroupInRecord("pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_4", "REDEFINE", pnd_Curr_Accounting_Dte_Yyyymmdd);
        pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Cc = pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_4.newFieldInGroup("pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Cc", 
            "#CURR-ACCOUNT-DTE-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Yy = pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_4.newFieldInGroup("pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Yy", 
            "#CURR-ACCOUNT-DTE-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Mm = pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_4.newFieldInGroup("pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Mm", 
            "#CURR-ACCOUNT-DTE-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Dd = pnd_Curr_Accounting_Dte_Yyyymmdd__R_Field_4.newFieldInGroup("pnd_Curr_Accounting_Dte_Yyyymmdd_Pnd_Curr_Account_Dte_Dte_Dd", 
            "#CURR-ACCOUNT-DTE-DTE-DD", FieldType.NUMERIC, 2);
        pnd_Rqst_Id = localVariables.newFieldInRecord("pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 35);

        pnd_Rqst_Id__R_Field_5 = localVariables.newGroupInRecord("pnd_Rqst_Id__R_Field_5", "REDEFINE", pnd_Rqst_Id);
        pnd_Rqst_Id_Pnd_Nap_Unique_Id = pnd_Rqst_Id__R_Field_5.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Unique_Id", "#NAP-UNIQUE-ID", FieldType.NUMERIC, 12);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte = pnd_Rqst_Id__R_Field_5.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte", "#NAP-EFFCTV-DTE", FieldType.NUMERIC, 
            8);

        pnd_Rqst_Id__R_Field_6 = pnd_Rqst_Id__R_Field_5.newGroupInGroup("pnd_Rqst_Id__R_Field_6", "REDEFINE", pnd_Rqst_Id_Pnd_Nap_Effctv_Dte);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Cc = pnd_Rqst_Id__R_Field_6.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Cc", "#NAP-EFFCTV-DTE-CC", FieldType.NUMERIC, 
            2);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Yy = pnd_Rqst_Id__R_Field_6.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Yy", "#NAP-EFFCTV-DTE-YY", FieldType.NUMERIC, 
            2);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Mm = pnd_Rqst_Id__R_Field_6.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Mm", "#NAP-EFFCTV-DTE-MM", FieldType.NUMERIC, 
            2);
        pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Dd = pnd_Rqst_Id__R_Field_6.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Effctv_Dte_Dd", "#NAP-EFFCTV-DTE-DD", FieldType.NUMERIC, 
            2);
        pnd_Rqst_Id_Pnd_Nap_Entry_Dte = pnd_Rqst_Id__R_Field_5.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Entry_Dte", "#NAP-ENTRY-DTE", FieldType.NUMERIC, 8);
        pnd_Rqst_Id_Pnd_Nap_Entry_Tme = pnd_Rqst_Id__R_Field_5.newFieldInGroup("pnd_Rqst_Id_Pnd_Nap_Entry_Tme", "#NAP-ENTRY-TME", FieldType.NUMERIC, 7);
        pnd_Prev_Payment_Due_Date = localVariables.newFieldInRecord("pnd_Prev_Payment_Due_Date", "#PREV-PAYMENT-DUE-DATE", FieldType.STRING, 8);

        pnd_Prev_Payment_Due_Date__R_Field_7 = localVariables.newGroupInRecord("pnd_Prev_Payment_Due_Date__R_Field_7", "REDEFINE", pnd_Prev_Payment_Due_Date);
        pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Cc = pnd_Prev_Payment_Due_Date__R_Field_7.newFieldInGroup("pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Cc", 
            "#PREV-PAYMENT-DUE-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Yy = pnd_Prev_Payment_Due_Date__R_Field_7.newFieldInGroup("pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Yy", 
            "#PREV-PAYMENT-DUE-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Mm = pnd_Prev_Payment_Due_Date__R_Field_7.newFieldInGroup("pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Mm", 
            "#PREV-PAYMENT-DUE-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Dd = pnd_Prev_Payment_Due_Date__R_Field_7.newFieldInGroup("pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Dd", 
            "#PREV-PAYMENT-DUE-DATE-DD", FieldType.NUMERIC, 2);
        pnd_Curr_Payment_Due_Date = localVariables.newFieldInRecord("pnd_Curr_Payment_Due_Date", "#CURR-PAYMENT-DUE-DATE", FieldType.STRING, 8);

        pnd_Curr_Payment_Due_Date__R_Field_8 = localVariables.newGroupInRecord("pnd_Curr_Payment_Due_Date__R_Field_8", "REDEFINE", pnd_Curr_Payment_Due_Date);
        pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Cc = pnd_Curr_Payment_Due_Date__R_Field_8.newFieldInGroup("pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Cc", 
            "#CURR-PAYMENT-DUE-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Yy = pnd_Curr_Payment_Due_Date__R_Field_8.newFieldInGroup("pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Yy", 
            "#CURR-PAYMENT-DUE-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Mm = pnd_Curr_Payment_Due_Date__R_Field_8.newFieldInGroup("pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Mm", 
            "#CURR-PAYMENT-DUE-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Dd = pnd_Curr_Payment_Due_Date__R_Field_8.newFieldInGroup("pnd_Curr_Payment_Due_Date_Pnd_Curr_Payment_Due_Date_Dd", 
            "#CURR-PAYMENT-DUE-DATE-DD", FieldType.NUMERIC, 2);
        pnd_Payment_Mode = localVariables.newFieldInRecord("pnd_Payment_Mode", "#PAYMENT-MODE", FieldType.NUMERIC, 3);

        pnd_Payment_Mode__R_Field_9 = localVariables.newGroupInRecord("pnd_Payment_Mode__R_Field_9", "REDEFINE", pnd_Payment_Mode);
        pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1 = pnd_Payment_Mode__R_Field_9.newFieldInGroup("pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1", "#PAYMENT-MODE-BYTE-1", 
            FieldType.NUMERIC, 1);
        pnd_Payment_Mode_Pnd_Payment_Mode_Byte_2 = pnd_Payment_Mode__R_Field_9.newFieldInGroup("pnd_Payment_Mode_Pnd_Payment_Mode_Byte_2", "#PAYMENT-MODE-BYTE-2", 
            FieldType.NUMERIC, 2);
        pnd_Mode_Desc = localVariables.newFieldArrayInRecord("pnd_Mode_Desc", "#MODE-DESC", FieldType.STRING, 20, new DbsArrayController(1, 5));
        pnd_Pec_Nbr_Active_Acct = localVariables.newFieldInRecord("pnd_Pec_Nbr_Active_Acct", "#PEC-NBR-ACTIVE-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Heading = localVariables.newFieldInRecord("pnd_Heading", "#HEADING", FieldType.STRING, 50);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_10 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_10", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Date_N = pnd_Date_A__R_Field_10.newFieldInGroup("pnd_Date_A_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        pnd_Ads_Ia_Super1 = localVariables.newFieldInRecord("pnd_Ads_Ia_Super1", "#ADS-IA-SUPER1", FieldType.STRING, 41);

        pnd_Ads_Ia_Super1__R_Field_11 = localVariables.newGroupInRecord("pnd_Ads_Ia_Super1__R_Field_11", "REDEFINE", pnd_Ads_Ia_Super1);
        pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rqst_Id = pnd_Ads_Ia_Super1__R_Field_11.newFieldInGroup("pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rqst_Id", "#ADS-IA-RQST-ID", 
            FieldType.STRING, 35);
        pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde = pnd_Ads_Ia_Super1__R_Field_11.newFieldInGroup("pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde", "#ADS-IA-RCRD-CDE", 
            FieldType.STRING, 3);
        pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr = pnd_Ads_Ia_Super1__R_Field_11.newFieldInGroup("pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr", "#ADS-IA-RCRD-SQNCE-NBR", 
            FieldType.NUMERIC, 3);
        pnd_Max_Rslt = localVariables.newFieldInRecord("pnd_Max_Rslt", "#MAX-RSLT", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Rates = localVariables.newFieldInRecord("pnd_Max_Rates", "#MAX-RATES", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Ia_Rslt2.reset();
        vw_ads_Cntl_View.reset();

        ldaAdsl1340.initializeValues();
        ldaAdsl450.initializeValues();
        ldaAdsl450a.initializeValues();

        localVariables.reset();
        pnd_All_Indx_Pnd_P.setInitialValue(0);
        pnd_All_Indx_Pnd_Y.setInitialValue(0);
        pnd_All_Indx_Pnd_Z.setInitialValue(0);
        pnd_All_Indx_Pnd_Z_Tot.setInitialValue(0);
        pnd_All_Indx_Pnd_A.setInitialValue(0);
        pnd_All_Indx_Pnd_B_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_C_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Next_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Prod_Indx.setInitialValue(0);
        pnd_All_Indx_Pnd_Ivc_Cnt.setInitialValue(0);
        pnd_C.setInitialValue(0);
        pnd_M.setInitialValue(0);
        pnd_D.setInitialValue(0);
        pnd_T.setInitialValue(0);
        pnd_X.setInitialValue(0);
        pnd_Rec_Read.setInitialValue(0);
        pnd_Total_Monthly_Rec.setInitialValue(0);
        pnd_Total_Semi_Annual_Rec.setInitialValue(0);
        pnd_Total_Quartly_Rec.setInitialValue(0);
        pnd_Total_Annual_Rec.setInitialValue(0);
        pnd_Total_Number_Rec.setInitialValue(0);
        pnd_First_Time.setInitialValue(true);
        pnd_Tiaa_Prod_Code.setInitialValue(true);
        pnd_Cref_Prod_Found.setInitialValue(true);
        pnd_Surv_Only_Run.setInitialValue(false);
        pnd_Pec_Nbr_Active_Acct.setInitialValue(20);
        pnd_Max_Rslt.setInitialValue(125);
        pnd_Max_Rates.setInitialValue(250);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp1341() throws Exception
    {
        super("Adsp1341");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  OS-102013 START                                                                                                                                              //Natural: FORMAT LS = 79 PS = 60;//Natural: FORMAT ( 1 ) LS = 79 PS = 60;//Natural: FORMAT ( 2 ) LS = 79 PS = 60
        //* *CALLNAT 'ADSN888'     /* NEW EXTERNALIZATION + SEQUENCING OF PROD CDE
        //* *  #PAS-EXT-CNTRL-02-WORK-AREA
        //* *  #PEC-NBR-ACTIVE-ACCT
        DbsUtil.callnat(Adsn889.class , getCurrentProcessState(), pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue("*"));                                      //Natural: CALLNAT 'ADSN889' #PEC-ACCT-NAME-1 ( * )
        if (condition(Global.isEscape())) return;
        //*  OS-102013 END
        getReports().write(0, " ADSP1340 ","=",pnd_Pec_Nbr_Active_Acct);                                                                                                  //Natural: WRITE ' ADSP1340 ' '=' #PEC-NBR-ACTIVE-ACCT
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM READ-CONTROL-RECORD
        sub_Read_Control_Record();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-FIELDS
        sub_Initialize_Fields();
        if (condition(Global.isEscape())) {return;}
        PND_PND_L1155:                                                                                                                                                    //Natural: READ WORK FILE 1 ADS-IA-RSLT-VIEW #MORE #NAP-ALT-DEST-RLVR-DEST
        while (condition(getWorkFiles().read(1, ldaAdsl450.getVw_ads_Ia_Rslt_View(), ldaAdsl450.getPnd_More(), pnd_Nap_Alt_Dest_Rlvr_Dest)))
        {
            pnd_Rqst_Id.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Rqst_Id());                                                                                               //Natural: MOVE ADS-IA-RSLT-VIEW.RQST-ID TO #RQST-ID
            //*                                                                                                                                                           //Natural: AT END OF DATA
            //*  GG050608
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Srvvr_Ind().equals("Y")))                                                                                    //Natural: IF ADS-IA-RSLT-VIEW.ADI-SRVVR-IND = 'Y'
            {
                pnd_Surv_Only_Run.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #SURV-ONLY-RUN
            }                                                                                                                                                             //Natural: END-IF
            pnd_Today_Accounting_Date.setValueEdited(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                                 //Natural: MOVE EDITED ADS-IA-RSLT-VIEW.ADI-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #TODAY-ACCOUNTING-DATE
            if (condition(pnd_Today_Accounting_Date.greater(pnd_Today_Business_Date)))                                                                                    //Natural: IF #TODAY-ACCOUNTING-DATE GT #TODAY-BUSINESS-DATE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  *****************************************
            //*     REMOVE NEXT IF STATMENT AFTER TESTING
            //* *******************************************
            if (condition((ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Cde().equals("N") || ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Cde().equals("F"))))                    //Natural: IF ( ADS-IA-RSLT-VIEW.ADI-PYMNT-CDE = 'N' OR ADS-IA-RSLT-VIEW.ADI-PYMNT-CDE = 'F' )
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Lst_Actvty_Dte().greater(ads_Cntl_View_Ads_Daily_Actvty_Dte) && ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde().equals("PP ")  //Natural: ACCEPT IF ADS-IA-RSLT-VIEW.ADI-LST-ACTVTY-DTE GT ADS-DAILY-ACTVTY-DTE AND ADS-IA-RSLT-VIEW.ADI-IA-RCRD-CDE = 'PP ' AND ADS-IA-RSLT-VIEW.ADI-SQNCE-NBR GT 0
                && ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr().greater(getZero()))))
            {
                continue;
            }
            //*  EM - 030512
                                                                                                                                                                          //Natural: PERFORM POPULATE-ADS-IA-RSLT-AREA
            sub_Populate_Ads_Ia_Rslt_Area();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1155"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1155"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, ldaAdsl450a.getAds_Ia_Rslt_Adi_Ia_Rcrd_Cde(),ldaAdsl450a.getAds_Ia_Rslt_Adi_Sqnce_Nbr(),ldaAdsl450a.getAds_Ia_Rslt_Rqst_Id(),           //Natural: WRITE ADS-IA-RSLT.ADI-IA-RCRD-CDE ADS-IA-RSLT.ADI-SQNCE-NBR ADS-IA-RSLT.RQST-ID ADS-IA-RSLT.ADI-OPTN-CDE #NAP-ALT-DEST-RLVR-DEST ADS-IA-RSLT.ADI-INSTLLMNT-DTE ADS-IA-RSLT.ADI-PYMNT-CDE ADS-IA-RSLT.ADI-PYMNT-MODE ADS-IA-RSLT.ADI-TIAA-PRDC-IVC-AMT ADS-IA-RSLT.ADI-SRVVR-IND
                ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde(),pnd_Nap_Alt_Dest_Rlvr_Dest,ldaAdsl450a.getAds_Ia_Rslt_Adi_Instllmnt_Dte(),ldaAdsl450a.getAds_Ia_Rslt_Adi_Pymnt_Cde(),
                ldaAdsl450a.getAds_Ia_Rslt_Adi_Pymnt_Mode(),ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt(),ldaAdsl450a.getAds_Ia_Rslt_Adi_Srvvr_Ind());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1155"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1155"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_First_Time.getBoolean()))                                                                                                                   //Natural: IF #FIRST-TIME
            {
                pnd_Rqst_Id.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Rqst_Id());                                                                                           //Natural: MOVE ADS-IA-RSLT-VIEW.RQST-ID TO #RQST-ID
                pnd_Prev_Payment_Due_Date.setValueEdited(ldaAdsl450a.getAds_Ia_Rslt_Adi_Instllmnt_Dte(),new ReportEditMask("YYYYMMDD"));                                  //Natural: MOVE EDITED ADS-IA-RSLT.ADI-INSTLLMNT-DTE ( EM = YYYYMMDD ) TO #PREV-PAYMENT-DUE-DATE
                pnd_First_Time.setValue(false);                                                                                                                           //Natural: MOVE FALSE TO #FIRST-TIME
            }                                                                                                                                                             //Natural: END-IF
            pnd_Payment_Mode.setValue(ldaAdsl450a.getAds_Ia_Rslt_Adi_Pymnt_Mode());                                                                                       //Natural: MOVE ADS-IA-RSLT.ADI-PYMNT-MODE TO #PAYMENT-MODE
            pnd_Curr_Payment_Due_Date.setValueEdited(ldaAdsl450a.getAds_Ia_Rslt_Adi_Instllmnt_Dte(),new ReportEditMask("YYYYMMDD"));                                      //Natural: MOVE EDITED ADS-IA-RSLT.ADI-INSTLLMNT-DTE ( EM = YYYYMMDD ) TO #CURR-PAYMENT-DUE-DATE
            if (condition(pnd_Curr_Payment_Due_Date.notEquals(pnd_Prev_Payment_Due_Date)))                                                                                //Natural: IF #CURR-PAYMENT-DUE-DATE NE #PREV-PAYMENT-DUE-DATE
            {
                                                                                                                                                                          //Natural: PERFORM BREAK-OF-ANNTY-START-DATE-RTN
                sub_Break_Of_Annty_Start_Date_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("PND_PND_L1155"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1155"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Prev_Payment_Due_Date.setValue(pnd_Curr_Payment_Due_Date);                                                                                            //Natural: MOVE #CURR-PAYMENT-DUE-DATE TO #PREV-PAYMENT-DUE-DATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_All_Indx_Pnd_Ivc_Cnt.reset();                                                                                                                             //Natural: RESET #IVC-CNT
            pnd_All_Indx_Pnd_A.reset();                                                                                                                                   //Natural: RESET #A
            PND_PND_L1455:                                                                                                                                                //Natural: FOR #A 1 TO 1
            for (pnd_All_Indx_Pnd_A.setValue(1); condition(pnd_All_Indx_Pnd_A.lessOrEqual(1)); pnd_All_Indx_Pnd_A.nadd(1))
            {
                if (condition(pnd_All_Indx_Pnd_A.greater(1)))                                                                                                             //Natural: IF #A > 1
                {
                    if (true) break PND_PND_L1455;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L1455. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals(" ")))                                      //Natural: IF #ADI-DTL-TIAA-IA-RATE-CD ( #A ) NE ' '
                {
                    short decideConditionsMet642 = 0;                                                                                                                     //Natural: DECIDE ON FIRST #PAYMENT-MODE-BYTE-1;//Natural: VALUE 1
                    if (condition((pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(1))))
                    {
                        decideConditionsMet642++;
                        pnd_Total_Monthly_Rec.nadd(1);                                                                                                                    //Natural: ADD 1 TO #TOTAL-MONTHLY-REC
                        pnd_M.setValue(1);                                                                                                                                //Natural: ASSIGN #M := 1
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-ACCUMULATIONS
                        sub_Calculate_Tiaa_Accumulations();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("PND_PND_L1455"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1455"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (true) break PND_PND_L1455;                                                                                                                    //Natural: ESCAPE BOTTOM ( ##L1455. )
                    }                                                                                                                                                     //Natural: VALUE 6
                    else if (condition((pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(6))))
                    {
                        decideConditionsMet642++;
                        pnd_Total_Quartly_Rec.nadd(1);                                                                                                                    //Natural: ADD 1 TO #TOTAL-QUARTLY-REC
                        pnd_M.setValue(2);                                                                                                                                //Natural: ASSIGN #M := 2
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-ACCUMULATIONS
                        sub_Calculate_Tiaa_Accumulations();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("PND_PND_L1455"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1455"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (true) break PND_PND_L1455;                                                                                                                    //Natural: ESCAPE BOTTOM ( ##L1455. )
                    }                                                                                                                                                     //Natural: VALUE 7
                    else if (condition((pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(7))))
                    {
                        decideConditionsMet642++;
                        pnd_Total_Semi_Annual_Rec.nadd(1);                                                                                                                //Natural: ADD 1 TO #TOTAL-SEMI-ANNUAL-REC
                        pnd_M.setValue(3);                                                                                                                                //Natural: ASSIGN #M := 3
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-ACCUMULATIONS
                        sub_Calculate_Tiaa_Accumulations();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("PND_PND_L1455"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1455"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (true) break PND_PND_L1455;                                                                                                                    //Natural: ESCAPE BOTTOM ( ##L1455. )
                    }                                                                                                                                                     //Natural: VALUE 8
                    else if (condition((pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(8))))
                    {
                        decideConditionsMet642++;
                        pnd_Total_Annual_Rec.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TOTAL-ANNUAL-REC
                        pnd_M.setValue(4);                                                                                                                                //Natural: ASSIGN #M := 4
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-ACCUMULATIONS
                        sub_Calculate_Tiaa_Accumulations();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("PND_PND_L1455"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1455"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (true) break PND_PND_L1455;                                                                                                                    //Natural: ESCAPE BOTTOM ( ##L1455. )
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1155"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1155"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR01:                                                                                                                                                        //Natural: FOR #A 1 TO #PEC-NBR-ACTIVE-ACCT
            for (pnd_All_Indx_Pnd_A.setValue(1); condition(pnd_All_Indx_Pnd_A.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_A.nadd(1))
            {
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals(" ")))                                             //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE ' '
                {
                    short decideConditionsMet671 = 0;                                                                                                                     //Natural: DECIDE ON FIRST #PAYMENT-MODE-BYTE-1;//Natural: VALUE 1
                    if (condition((pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(1))))
                    {
                        decideConditionsMet671++;
                        pnd_Total_Monthly_Rec.nadd(1);                                                                                                                    //Natural: ADD 1 TO #TOTAL-MONTHLY-REC
                        pnd_C.setValue(1);                                                                                                                                //Natural: ASSIGN #C := 1
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-ACCUMULATIONS
                        sub_Calculate_Cref_Accumulations();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: VALUE 6
                    else if (condition((pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(6))))
                    {
                        decideConditionsMet671++;
                        pnd_Total_Quartly_Rec.nadd(1);                                                                                                                    //Natural: ADD 1 TO #TOTAL-QUARTLY-REC
                        pnd_C.setValue(2);                                                                                                                                //Natural: ASSIGN #C := 2
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-ACCUMULATIONS
                        sub_Calculate_Cref_Accumulations();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: VALUE 7
                    else if (condition((pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(7))))
                    {
                        decideConditionsMet671++;
                        pnd_Total_Semi_Annual_Rec.nadd(1);                                                                                                                //Natural: ADD 1 TO #TOTAL-SEMI-ANNUAL-REC
                        pnd_C.setValue(3);                                                                                                                                //Natural: ASSIGN #C := 3
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-ACCUMULATIONS
                        sub_Calculate_Cref_Accumulations();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: VALUE 8
                    else if (condition((pnd_Payment_Mode_Pnd_Payment_Mode_Byte_1.equals(8))))
                    {
                        decideConditionsMet671++;
                        pnd_Total_Annual_Rec.nadd(1);                                                                                                                     //Natural: ADD 1 TO #TOTAL-ANNUAL-REC
                        pnd_C.setValue(4);                                                                                                                                //Natural: ASSIGN #C := 4
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-ACCUMULATIONS
                        sub_Calculate_Cref_Accumulations();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                //*  DEA
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1155"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1155"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                                                                                                                                           //Natural: AT TOP OF PAGE ( 1 )
        }                                                                                                                                                                 //Natural: AT TOP OF PAGE ( 2 );//Natural: END-WORK
        PND_PND_L1155_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom, "pnd_pnd_L1155");                                                                             //Natural: ESCAPE BOTTOM ( ##L1155. )
            if (true) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM BREAK-OF-ANNTY-START-DATE-RTN
        sub_Break_Of_Annty_Start_Date_Rtn();
        if (condition(Global.isEscape())) {return;}
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-CONTROL-RECORD
        //* *******************************************************************
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-FIELDS
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-ADS-IA-RSLT-AREA
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TIAA-ACCUMULATIONS
        //* **********************************************************************
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TIAA-USING-STANDARD-ACCUMS
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUCKET-TPA-IVC-AMOUNTS
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TIAA-USING-GRADED-ACCUMS
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-CREF-ACCUMULATIONS
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-CREF-MONTHLY-ACCUMULATIONS
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-CREF-ANNUAL-ACCUMULATIONS
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BREAK-OF-ANNTY-START-DATE-RTN
        //* **********************************************************************
    }
    private void sub_Read_Control_Record() throws Exception                                                                                                               //Natural: READ-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        vw_ads_Cntl_View.startDatabaseRead                                                                                                                                //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "PND_PND_L1850",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        PND_PND_L1850:
        while (condition(vw_ads_Cntl_View.readNextRow("PND_PND_L1850")))
        {
            //*  MOVE 20011231 TO ADS-CNTL-BSNSS-DTE
            //*  MOVE 20020102 TO ADS-CNTL-BSNSS-TMRRW-DTE
            if (condition(vw_ads_Cntl_View.getAstCOUNTER().equals(2)))                                                                                                    //Natural: IF *COUNTER ( ##L1850. ) = 2
            {
                pnd_Prev_Bsnss_Dte_N.setValue(ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                                          //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #PREV-BSNSS-DTE-N
                pnd_Prev_Bsnss_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Prev_Bsnss_Dte_N_Pnd_Prev_Bsnss_Dte_A);                                            //Natural: MOVE EDITED #PREV-BSNSS-DTE-A TO #PREV-BSNSS-DTE-D ( EM = YYYYMMDD )
                pnd_Today_Business_Date.setValueEdited(pnd_Prev_Bsnss_Dte_D,new ReportEditMask("YYYYMMDD"));                                                              //Natural: MOVE EDITED #PREV-BSNSS-DTE-D ( EM = YYYYMMDD ) TO #TODAY-BUSINESS-DATE
                if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                             //Natural: IF ADS-RPT-13 NE 0
                {
                    pnd_Date_A.setValueEdited(ads_Cntl_View_Ads_Rpt_13,new ReportEditMask("YYYYMMDD"));                                                                   //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE-A
                    pnd_Prev_Accounting_Dte_Yyyymmdd.setValue(pnd_Date_A_Pnd_Date_N);                                                                                     //Natural: MOVE #DATE-N TO #PREV-ACCOUNTING-DTE-YYYYMMDD
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Prev_Accounting_Dte_Yyyymmdd.setValue(ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                          //Natural: MOVE ADS-CNTL-BSNSS-DTE TO #PREV-ACCOUNTING-DTE-YYYYMMDD
                }                                                                                                                                                         //Natural: END-IF
                //* *    MOVE ADS-CNTL-BSNSS-TMRRW-DTE
                //* *      TO #PREV-ACCOUNTING-DTE-YYYYMMDD
                ldaAdsl1340.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Cc);                  //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-CC TO #ACCOUNTING-DATE-MCDY-CC
                ldaAdsl1340.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Yy);                  //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-YY TO #ACCOUNTING-DATE-MCDY-YY
                ldaAdsl1340.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Mm);                  //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-MM TO #ACCOUNTING-DATE-MCDY-MM
                ldaAdsl1340.getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd().setValue(pnd_Prev_Accounting_Dte_Yyyymmdd_Pnd_Prev_Account_Dte_Dte_Dd);                  //Natural: MOVE #PREV-ACCOUNT-DTE-DTE-DD TO #ACCOUNTING-DATE-MCDY-DD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Initialize_Fields() throws Exception                                                                                                                 //Natural: INITIALIZE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        pnd_Mode_Desc.getValue(1).setValue("MONTHLY MODE        ");                                                                                                       //Natural: MOVE 'MONTHLY MODE        ' TO #MODE-DESC ( 1 )
        pnd_Mode_Desc.getValue(2).setValue("QUARTERLY MODE      ");                                                                                                       //Natural: MOVE 'QUARTERLY MODE      ' TO #MODE-DESC ( 2 )
        pnd_Mode_Desc.getValue(3).setValue("SEMI-ANNUALY MODE   ");                                                                                                       //Natural: MOVE 'SEMI-ANNUALY MODE   ' TO #MODE-DESC ( 3 )
        pnd_Mode_Desc.getValue(4).setValue("ANNUAL MODE         ");                                                                                                       //Natural: MOVE 'ANNUAL MODE         ' TO #MODE-DESC ( 4 )
        pnd_Mode_Desc.getValue(5).setValue("GRAND TOTALS BY     ");                                                                                                       //Natural: MOVE 'GRAND TOTALS BY     ' TO #MODE-DESC ( 5 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode().getValue("*").reset();                                                                                                          //Natural: RESET #TIAA-BY-MODE ( * ) #M-TIAA-BY-DETAILS ( * ) #C-MTH-ACCUM-BY-MODE ( * ) #M-C-MTH-MINOR-ACCUM ( * ) #C-ANN-BY-MODE ( * ) #M-C-ANN-MINOR-ACCUM ( * ) #CREF-MTH-SUB-TOTAL-ACCUM ( * ) #M-CREF-MTH-SUB-TOTAL-ACCUM #CREF-ANN-SUB-TOTAL-ACCUM ( * ) #M-CREF-ANN-SUB-TOTAL-ACCUM #TOTAL-TIAA-CREF-ACCUM ( * ) #M-TOTAL-TIAA-CREF-ACCUM #M-T-CREF-UNITS ( * ) #M-T-CREF-DOLLARS ( * )
        ldaAdsl1340.getPnd_M_Tiaa_By_Details().getValue("*").reset();
        ldaAdsl1340.getPnd_C_Mth_Accum_By_Mode().getValue("*").reset();
        ldaAdsl1340.getPnd_M_C_Mth_Minor_Accum().getValue("*").reset();
        ldaAdsl1340.getPnd_C_Ann_By_Mode().getValue("*").reset();
        ldaAdsl1340.getPnd_M_C_Ann_Minor_Accum().getValue("*").reset();
        ldaAdsl1340.getPnd_Cref_Mth_Sub_Total_Accum().getValue("*").reset();
        ldaAdsl1340.getPnd_M_Cref_Mth_Sub_Total_Accum().reset();
        ldaAdsl1340.getPnd_Cref_Ann_Sub_Total_Accum().getValue("*").reset();
        ldaAdsl1340.getPnd_M_Cref_Ann_Sub_Total_Accum().reset();
        ldaAdsl1340.getPnd_Total_Tiaa_Cref_Accum().getValue("*").reset();
        ldaAdsl1340.getPnd_M_Total_Tiaa_Cref_Accum().reset();
        ldaAdsl1340.getPnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units().getValue("*").reset();
        ldaAdsl1340.getPnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars().getValue("*").reset();
    }
    //*  EM - 030512 START
    private void sub_Populate_Ads_Ia_Rslt_Area() throws Exception                                                                                                         //Natural: POPULATE-ADS-IA-RSLT-AREA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        ldaAdsl450a.getAds_Ia_Rslt().setValuesByName(ldaAdsl450.getVw_ads_Ia_Rslt_View());                                                                                //Natural: MOVE BY NAME ADS-IA-RSLT-VIEW TO ADS-IA-RSLT
        ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Data().reset();                                                                                                           //Natural: RESET ADS-IA-RSLT.ADI-DTL-TIAA-DATA ADS-IA-RSLT.#ADI-DTL-TIA-TPA-GUAR-COMMUT-DAT ADS-IA-RSLT.#ADI-CONTRACT-ID ( * )
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Tpa_Guar_Commut_Dat().reset();
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Contract_Id().getValue("*").reset();
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-RATE-CD ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-RATE-CD ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-STD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-STD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-GRD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-DA-GRD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Acct_Cd().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Acct_Cd().getValue(1,     //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-ACCT-CD ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-ACCT-CD ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Eff_Rte_Intrst().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Eff_Rte_Intrst().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-EFF-RTE-INTRST ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-EFF-RTE-INTRST ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-GRD-GRNTD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-GRNTD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-GRD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GRD-DVDND-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-GRD-PAY-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-FNL-GRD-PAY-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-GRD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-FNL-GRD-DVDND-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-STD-PAY-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-FNL-STD-PAY-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-STD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-FNL-STD-DVDND-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Std_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIA-GUAR-COMMUT-STD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GUAR-COMMUT-STD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Grd_Amt().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt().getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIA-GUAR-COMMUT-GRD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-DTL-TIAA-GUAR-COMMUT-GRD-AMT ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Contract_Id().getValue(1,":",pnd_Max_Rslt).setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Contract_Id().getValue(1,               //Natural: ASSIGN ADS-IA-RSLT.#ADI-CONTRACT-ID ( 1:#MAX-RSLT ) := ADS-IA-RSLT-VIEW.ADI-CONTRACT-ID ( 1:#MAX-RSLT )
            ":",pnd_Max_Rslt));
        if (condition(ldaAdsl450.getPnd_More().equals("Y")))                                                                                                              //Natural: IF #MORE = 'Y'
        {
            pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rqst_Id.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Rqst_Id());                                                                      //Natural: ASSIGN #ADS-IA-RQST-ID := ADS-IA-RSLT-VIEW.RQST-ID
            pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Cde.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde(),              //Natural: COMPRESS ADS-IA-RSLT-VIEW.ADI-IA-RCRD-CDE '1' INTO #ADS-IA-RCRD-CDE LEAVING NO SPACE
                "1"));
            pnd_Ads_Ia_Super1_Pnd_Ads_Ia_Rcrd_Sqnce_Nbr.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr());                                                         //Natural: ASSIGN #ADS-IA-RCRD-SQNCE-NBR := ADS-IA-RSLT-VIEW.ADI-SQNCE-NBR
            vw_ads_Ia_Rslt2.startDatabaseFind                                                                                                                             //Natural: FIND ADS-IA-RSLT2 WITH ADI-SUPER1 = #ADS-IA-SUPER1
            (
            "FIND01",
            new Wc[] { new Wc("ADI_SUPER1", "=", pnd_Ads_Ia_Super1, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_ads_Ia_Rslt2.readNextRow("FIND01")))
            {
                vw_ads_Ia_Rslt2.setIfNotFoundControlFlag(false);
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Rate_Cd.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-RATE-CD ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-DA-RATE-CD ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Std_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-STD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-DA-STD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Da_Grd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-DA-GRD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-DA-GRD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Acct_Cd().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Acct_Cd.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-ACCT-CD ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-ACCT-CD ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-IA-RATE-CD ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Eff_Rte_Intrst().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Eff_Rte_Intrst.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-EFF-RTE-INTRST ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-EFF-RTE-INTRST ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-GRD-GRNTD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-GRD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-GRD-DVDND-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-GRD-PAY-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-FNL-GRD-PAY-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-GRD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-FNL-GRD-DVDND-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-STD-GRNTD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIAA-STD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-STD-PAY-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-FNL-STD-PAY-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-FNL-STD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-FNL-STD-DVDND-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Std_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIA-GUAR-COMMUT-STD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-GUAR-COMMUT-STD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Grd_Amt().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-DTL-TIA-GUAR-COMMUT-GRD-AMT ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-DTL-TIAA-GUAR-COMMUT-GRD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Contract_Id().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rates).setValue(ads_Ia_Rslt2_Adi_Contract_Id.getValue(1, //Natural: ASSIGN ADS-IA-RSLT.#ADI-CONTRACT-ID ( #MAX-RSLT + 1:#MAX-RATES ) := ADS-IA-RSLT2.ADI-CONTRACT-ID ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  EM - 030512 END
    }
    private void sub_Calculate_Tiaa_Accumulations() throws Exception                                                                                                      //Natural: CALCULATE-TIAA-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #MAX-RATES
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Max_Rates)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                        //Natural: IF #ADI-DTL-TIAA-IA-RATE-CD ( #B-INDX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).notEquals(getZero())))                               //Natural: IF #ADI-DTL-TIAA-DA-STD-AMT ( #B-INDX ) NE 0
            {
                //*  IPRO
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                              //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
                {
                    pnd_T.setValue(9);                                                                                                                                    //Natural: ASSIGN #T := 9
                    if (condition(pnd_Nap_Alt_Dest_Rlvr_Dest.equals("    ")))                                                                                             //Natural: IF #NAP-ALT-DEST-RLVR-DEST = '    '
                    {
                        pnd_D.setValue(5);                                                                                                                                //Natural: ASSIGN #D := 5
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*           IF #NAP-ALT-DEST-RLVR-DEST = 'IRAT' OR = 'IRAC' OR = 'IRAX'
                        //*                                   OR = '03BT' OR = '03BC' OR = 'QPLT'
                        //*                                   OR = 'QPLC' OR = 'QPLX' OR = '03BX'
                        //*  EM - 11/03 START
                        if (condition(DbsUtil.maskMatches(pnd_Nap_Alt_Dest_Rlvr_Dest,"...'X'")))                                                                          //Natural: IF #NAP-ALT-DEST-RLVR-DEST = MASK ( ...'X' )
                        {
                            pnd_D.setValue(6);                                                                                                                            //Natural: ASSIGN #D := 6
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_D.setValue(7);                                                                                                                            //Natural: ASSIGN #D := 7
                            //*  EM - 11/03 END
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  TPA (IVC #D=8)
                    if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                          //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
                    {
                        pnd_T.setValue(15);                                                                                                                               //Natural: ASSIGN #T := 15
                        if (condition(pnd_Nap_Alt_Dest_Rlvr_Dest.equals("IRAT") || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("IRAC") || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("QPLC") //Natural: IF #NAP-ALT-DEST-RLVR-DEST = 'IRAT' OR = 'IRAC' OR = 'QPLC' OR = '03BT' OR = '03BC' OR = 'QPLT'
                            || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("03BT") || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("03BC") || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("QPLT")))
                        {
                            pnd_D.setValue(10);                                                                                                                           //Natural: ASSIGN #D := 10
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_Nap_Alt_Dest_Rlvr_Dest.equals("RINV")))                                                                                     //Natural: IF #NAP-ALT-DEST-RLVR-DEST = 'RINV'
                            {
                                pnd_D.setValue(12);                                                                                                                       //Natural: ASSIGN #D := 12
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(pnd_Nap_Alt_Dest_Rlvr_Dest.equals("    ")))                                                                                 //Natural: IF #NAP-ALT-DEST-RLVR-DEST = '    '
                                {
                                    pnd_D.setValue(13);                                                                                                                   //Natural: ASSIGN #D := 13
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    if (condition(pnd_Nap_Alt_Dest_Rlvr_Dest.equals("DTRA") || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("IRAX") || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("QPLX")  //Natural: IF #NAP-ALT-DEST-RLVR-DEST = 'DTRA' OR = 'IRAX' OR = 'QPLX' OR = '03BX'
                                        || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("03BX")))
                                    {
                                        pnd_D.setValue(14);                                                                                                               //Natural: ASSIGN #D := 14
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        //*  OTHER STD CASES
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_T.setValue(9);                                                                                                                                //Natural: ASSIGN #T := 9
                        //*  EM - 12/03
                        if (condition(pnd_Nap_Alt_Dest_Rlvr_Dest.equals(" ") || DbsUtil.maskMatches(pnd_Nap_Alt_Dest_Rlvr_Dest,"...'X'")))                                //Natural: IF #NAP-ALT-DEST-RLVR-DEST = '    ' OR = MASK ( ...'X' )
                        {
                            pnd_D.setValue(1);                                                                                                                            //Natural: ASSIGN #D := 1
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_Nap_Alt_Dest_Rlvr_Dest.equals("IRAC") || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("IRAT") || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("03BT")  //Natural: IF #NAP-ALT-DEST-RLVR-DEST = 'IRAC' OR = 'IRAT' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = 'DTRA'
                                || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("03BC") || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("QPLT") || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("QPLC") 
                                || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("DTRA")))
                            {
                                pnd_D.setValue(2);                                                                                                                        //Natural: ASSIGN #D := 2
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-USING-STANDARD-ACCUMS
                sub_Calculate_Tiaa_Using_Standard_Accums();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx).notEquals(getZero())))                               //Natural: IF #ADI-DTL-TIAA-DA-GRD-AMT ( #B-INDX ) NE 0
            {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TIAA-USING-GRADED-ACCUMS
                sub_Calculate_Tiaa_Using_Graded_Accums();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  TPA
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))                                      //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 28 OR = 30
        {
            if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt().greater(getZero())))                                                                         //Natural: IF ADS-IA-RSLT.ADI-TIAA-PRDC-IVC-AMT > 0
            {
                                                                                                                                                                          //Natural: PERFORM BUCKET-TPA-IVC-AMOUNTS
                sub_Bucket_Tpa_Ivc_Amounts();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Calculate_Tiaa_Using_Standard_Accums() throws Exception                                                                                              //Natural: CALCULATE-TIAA-USING-STANDARD-ACCUMS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  #M = MODE #D = LINE POSITION ON MAP #T = TOTAL LINE ON MAP
        pnd_All_Indx_Pnd_Ivc_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #IVC-CNT
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(pnd_M,pnd_D).nadd(1);                                                                                 //Natural: ADD 1 TO #TIAA-PAY-CNT ( #M,#D )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(pnd_M,pnd_T).nadd(1);                                                                                 //Natural: ADD 1 TO #TIAA-PAY-CNT ( #M,#T )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(pnd_M,16).nadd(1);                                                                                    //Natural: ADD 1 TO #TIAA-PAY-CNT ( #M,16 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(5,pnd_D).nadd(1);                                                                                     //Natural: ADD 1 TO #TIAA-PAY-CNT ( 5,#D )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(5,pnd_T).nadd(1);                                                                                     //Natural: ADD 1 TO #TIAA-PAY-CNT ( 5,#T )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(5,16).nadd(1);                                                                                        //Natural: ADD 1 TO #TIAA-PAY-CNT ( 5,16 )
        //*  TOTAL STANDARD LINE
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(28)  //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27 OR = 28 OR = 30
            || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(30)))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(pnd_M,3).nadd(1);                                                                                 //Natural: ADD 1 TO #TIAA-PAY-CNT ( #M,3 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(pnd_M,3).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-GTD-AMT ( #M,3 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt().getValue(pnd_M,3).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-DIV-AMT ( #M,3 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt().getValue(pnd_M,3).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #TIAA-FIN-GTD-AMT ( #M,3 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt().getValue(pnd_M,3).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-FIN-DIV-AMT ( #M,3 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(5,3).nadd(1);                                                                                     //Natural: ADD 1 TO #TIAA-PAY-CNT ( 5,3 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(5,3).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-GTD-AMT ( 5,3 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt().getValue(5,3).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-DIV-AMT ( 5,3 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt().getValue(5,3).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #TIAA-FIN-GTD-AMT ( 5,3 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt().getValue(5,3).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-FIN-DIV-AMT ( 5,3 )
        }                                                                                                                                                                 //Natural: END-IF
        //*  TOTAL IPRO LINE
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(25) || ldaAdsl450a.getAds_Ia_Rslt_Adi_Optn_Cde().equals(27)))                                      //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 25 OR = 27
        {
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(pnd_M,8).nadd(1);                                                                                 //Natural: ADD 1 TO #TIAA-PAY-CNT ( #M,8 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(pnd_M,8).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-GTD-AMT ( #M,8 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt().getValue(pnd_M,8).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-DIV-AMT ( #M,8 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt().getValue(pnd_M,8).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #TIAA-FIN-GTD-AMT ( #M,8 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt().getValue(pnd_M,8).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-FIN-DIV-AMT ( #M,8 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(5,8).nadd(1);                                                                                     //Natural: ADD 1 TO #TIAA-PAY-CNT ( 5,8 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(5,8).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-GTD-AMT ( 5,8 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt().getValue(5,8).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-DIV-AMT ( 5,8 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt().getValue(5,8).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #TIAA-FIN-GTD-AMT ( 5,8 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt().getValue(5,8).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-FIN-DIV-AMT ( 5,8 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(pnd_M,pnd_D).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-GTD-AMT ( #M,#D )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(pnd_M,pnd_T).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-GTD-AMT ( #M,#T )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(pnd_M,16).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-GTD-AMT ( #M,16 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(5,pnd_D).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-GTD-AMT ( 5,#D )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(5,pnd_T).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-GTD-AMT ( 5,#T )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(5,16).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-GRNTD-AMT ( #B-INDX ) TO #TIAA-GTD-AMT ( 5,16 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt().getValue(pnd_M,pnd_D).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-DIV-AMT ( #M,#D )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt().getValue(pnd_M,pnd_T).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-DIV-AMT ( #M,#T )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt().getValue(pnd_M,16).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-DIV-AMT ( #M,16 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt().getValue(5,pnd_D).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-DIV-AMT ( 5,#D )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt().getValue(5,pnd_T).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-DIV-AMT ( 5,#T )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt().getValue(5,16).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-DIV-AMT ( 5,16 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt().getValue(pnd_M,pnd_D).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #TIAA-FIN-GTD-AMT ( #M,#D )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt().getValue(pnd_M,pnd_T).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #TIAA-FIN-GTD-AMT ( #M,#T )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt().getValue(pnd_M,16).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #TIAA-FIN-GTD-AMT ( #M,16 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt().getValue(5,pnd_D).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #TIAA-FIN-GTD-AMT ( 5,#D )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt().getValue(5,pnd_T).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #TIAA-FIN-GTD-AMT ( 5,#T )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt().getValue(5,16).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-PAY-AMT ( #B-INDX ) TO #TIAA-FIN-GTD-AMT ( 5,16 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt().getValue(pnd_M,pnd_D).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-FIN-DIV-AMT ( #M,#D )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt().getValue(pnd_M,pnd_T).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-FIN-DIV-AMT ( #M,#T )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt().getValue(pnd_M,16).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-FIN-DIV-AMT ( #M,16 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt().getValue(5,pnd_D).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-FIN-DIV-AMT ( 5,#D )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt().getValue(5,pnd_T).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-FIN-DIV-AMT ( 5,#T )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt().getValue(5,16).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-STD-DVDND-AMT ( #B-INDX ) TO #TIAA-FIN-DIV-AMT ( 5,16 )
    }
    private void sub_Bucket_Tpa_Ivc_Amounts() throws Exception                                                                                                            //Natural: BUCKET-TPA-IVC-AMOUNTS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  #M = MODE #D = LINE POSITION ON MAP #T = TOTAL LINE ON MAP
        //*  NO IVC SPLITTING FOR CASH, RINV AND DTRA
        //*     IVC FROM IRAX IS PUT IN CASH
        //*     IVC ONLY FOR IRAT AND IRAC, 03BC, 03BT, QPLT AND QPLC
        //*  SEPARATE IVC FOR INTERNALS ONLY
        if (condition(pnd_Nap_Alt_Dest_Rlvr_Dest.equals("IRAT") || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("IRAC") || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("03BT")                 //Natural: IF #NAP-ALT-DEST-RLVR-DEST = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC'
            || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("03BC") || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("QPLT") || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("QPLC")))
        {
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(pnd_M,11).nadd(pnd_All_Indx_Pnd_Ivc_Cnt);                                                         //Natural: ADD #IVC-CNT TO #TIAA-PAY-CNT ( #M,11 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(pnd_M,11).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt());                               //Natural: ADD ADS-IA-RSLT.ADI-TIAA-PRDC-IVC-AMT TO #TIAA-GTD-AMT ( #M,11 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(5,11).nadd(pnd_All_Indx_Pnd_Ivc_Cnt);                                                             //Natural: ADD #IVC-CNT TO #TIAA-PAY-CNT ( 5 ,11 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(5,11).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt());                                   //Natural: ADD ADS-IA-RSLT.ADI-TIAA-PRDC-IVC-AMT TO #TIAA-GTD-AMT ( 5 ,11 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(pnd_M,15).nadd(pnd_All_Indx_Pnd_Ivc_Cnt);                                                         //Natural: ADD #IVC-CNT TO #TIAA-PAY-CNT ( #M,15 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(pnd_M,16).nadd(pnd_All_Indx_Pnd_Ivc_Cnt);                                                         //Natural: ADD #IVC-CNT TO #TIAA-PAY-CNT ( #M,16 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(5,15).nadd(pnd_All_Indx_Pnd_Ivc_Cnt);                                                             //Natural: ADD #IVC-CNT TO #TIAA-PAY-CNT ( 5 ,15 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(5,16).nadd(pnd_All_Indx_Pnd_Ivc_Cnt);                                                             //Natural: ADD #IVC-CNT TO #TIAA-PAY-CNT ( 5 ,16 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(pnd_M,pnd_D).nsubtract(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt());                       //Natural: COMPUTE #TIAA-GTD-AMT ( #M,#D ) = #TIAA-GTD-AMT ( #M,#D ) - ADS-IA-RSLT.ADI-TIAA-PRDC-IVC-AMT
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(5,pnd_D).nsubtract(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt());                           //Natural: COMPUTE #TIAA-GTD-AMT ( 5,#D ) = #TIAA-GTD-AMT ( 5,#D ) - ADS-IA-RSLT.ADI-TIAA-PRDC-IVC-AMT
        }                                                                                                                                                                 //Natural: END-IF
        //*  REPORT IVC FOR EXTERNALS IN THE CASH BUCKET
        if (condition(pnd_Nap_Alt_Dest_Rlvr_Dest.equals("IRAX") || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("QPLX") || pnd_Nap_Alt_Dest_Rlvr_Dest.equals("0B3X")))               //Natural: IF #NAP-ALT-DEST-RLVR-DEST = 'IRAX' OR = 'QPLX' OR = '0B3X'
        {
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(pnd_M,13).nadd(pnd_All_Indx_Pnd_Ivc_Cnt);                                                         //Natural: ADD #IVC-CNT TO #TIAA-PAY-CNT ( #M,13 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(pnd_M,13).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt());                               //Natural: ADD ADS-IA-RSLT.ADI-TIAA-PRDC-IVC-AMT TO #TIAA-GTD-AMT ( #M,13 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(5,13).nadd(pnd_All_Indx_Pnd_Ivc_Cnt);                                                             //Natural: ADD #IVC-CNT TO #TIAA-PAY-CNT ( 5 ,13 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(5,13).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt());                                   //Natural: ADD ADS-IA-RSLT.ADI-TIAA-PRDC-IVC-AMT TO #TIAA-GTD-AMT ( 5 ,13 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(pnd_M,15).nadd(pnd_All_Indx_Pnd_Ivc_Cnt);                                                         //Natural: ADD #IVC-CNT TO #TIAA-PAY-CNT ( #M,15 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(pnd_M,16).nadd(pnd_All_Indx_Pnd_Ivc_Cnt);                                                         //Natural: ADD #IVC-CNT TO #TIAA-PAY-CNT ( #M,16 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(5,15).nadd(pnd_All_Indx_Pnd_Ivc_Cnt);                                                             //Natural: ADD #IVC-CNT TO #TIAA-PAY-CNT ( 5 ,15 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(5,16).nadd(pnd_All_Indx_Pnd_Ivc_Cnt);                                                             //Natural: ADD #IVC-CNT TO #TIAA-PAY-CNT ( 5 ,16 )
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(pnd_M,14).nsubtract(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt());                          //Natural: COMPUTE #TIAA-GTD-AMT ( #M,14 ) = #TIAA-GTD-AMT ( #M,14 ) - ADS-IA-RSLT.ADI-TIAA-PRDC-IVC-AMT
            ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(5,14).nsubtract(ldaAdsl450a.getAds_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt());                              //Natural: COMPUTE #TIAA-GTD-AMT ( 5,14 ) = #TIAA-GTD-AMT ( 5,14 ) - ADS-IA-RSLT.ADI-TIAA-PRDC-IVC-AMT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Calculate_Tiaa_Using_Graded_Accums() throws Exception                                                                                                //Natural: CALCULATE-TIAA-USING-GRADED-ACCUMS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  #M = MODE #D = LINE POSITION ON MAP #T = TOTAL LINE ON MAP
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(pnd_M,4).nadd(1);                                                                                     //Natural: ADD 1 TO #TIAA-PAY-CNT ( #M,4 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(pnd_M,9).nadd(1);                                                                                     //Natural: ADD 1 TO #TIAA-PAY-CNT ( #M,9 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(pnd_M,16).nadd(1);                                                                                    //Natural: ADD 1 TO #TIAA-PAY-CNT ( #M,16 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(5,4).nadd(1);                                                                                         //Natural: ADD 1 TO #TIAA-PAY-CNT ( 5,4 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(5,9).nadd(1);                                                                                         //Natural: ADD 1 TO #TIAA-PAY-CNT ( 5,9 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(5,16).nadd(1);                                                                                        //Natural: ADD 1 TO #TIAA-PAY-CNT ( 5,16 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(pnd_M,4).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-GTD-AMT ( #M,4 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(pnd_M,9).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-GTD-AMT ( #M,9 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(pnd_M,16).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-GTD-AMT ( #M,16 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(5,4).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-GTD-AMT ( 5,4 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(5,9).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-GTD-AMT ( 5,9 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(5,16).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B-INDX ) TO #TIAA-GTD-AMT ( 5,16 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt().getValue(pnd_M,4).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-DIV-AMT ( #M,4 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt().getValue(pnd_M,9).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-DIV-AMT ( #M,9 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt().getValue(pnd_M,16).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-DIV-AMT ( #M,16 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt().getValue(5,4).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-DIV-AMT ( 5,4 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt().getValue(5,9).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-DIV-AMT ( 5,9 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt().getValue(5,16).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-TIAA-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-DIV-AMT ( 5,16 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt().getValue(pnd_M,4).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #TIAA-FIN-GTD-AMT ( #M,4 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt().getValue(pnd_M,9).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #TIAA-FIN-GTD-AMT ( #M,9 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt().getValue(pnd_M,16).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #TIAA-FIN-GTD-AMT ( #M,16 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt().getValue(5,4).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #TIAA-FIN-GTD-AMT ( 5,4 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt().getValue(5,9).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #TIAA-FIN-GTD-AMT ( 5,9 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt().getValue(5,16).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-PAY-AMT ( #B-INDX ) TO #TIAA-FIN-GTD-AMT ( 5,16 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt().getValue(pnd_M,4).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-FIN-DIV-AMT ( #M,4 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt().getValue(pnd_M,9).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-FIN-DIV-AMT ( #M,9 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt().getValue(pnd_M,16).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-FIN-DIV-AMT ( #M,16 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt().getValue(5,4).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-FIN-DIV-AMT ( 5,4 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt().getValue(5,9).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-FIN-DIV-AMT ( 5,9 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt().getValue(5,16).nadd(ldaAdsl450a.getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_All_Indx_Pnd_B_Indx)); //Natural: ADD #ADI-DTL-FNL-GRD-DVDND-AMT ( #B-INDX ) TO #TIAA-FIN-DIV-AMT ( 5,16 )
    }
    private void sub_Calculate_Cref_Accumulations() throws Exception                                                                                                      //Natural: CALCULATE-CREF-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        pnd_Cref_Prod_Found.setValue(false);                                                                                                                              //Natural: MOVE FALSE TO #CREF-PROD-FOUND
        pnd_All_Indx_Pnd_B_Indx.reset();                                                                                                                                  //Natural: RESET #B-INDX
        FOR03:                                                                                                                                                            //Natural: FOR #B-INDX 1 TO #PEC-NBR-ACTIVE-ACCT
        for (pnd_All_Indx_Pnd_B_Indx.setValue(1); condition(pnd_All_Indx_Pnd_B_Indx.lessOrEqual(pnd_Pec_Nbr_Active_Acct)); pnd_All_Indx_Pnd_B_Indx.nadd(1))
        {
            if (condition(pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue(pnd_All_Indx_Pnd_B_Indx).equals(" ")))                                              //Natural: IF #PEC-ACCT-NAME-1 ( #B-INDX ) = ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Pas_Ext_Cntrl_02_Work_Area_Pnd_Pec_Acct_Name_1.getValue(pnd_All_Indx_Pnd_B_Indx).equals(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A)))) //Natural: IF #PEC-ACCT-NAME-1 ( #B-INDX ) = ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A )
            {
                pnd_Cref_Prod_Found.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO #CREF-PROD-FOUND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cref_Prod_Found.setValue(false);                                                                                                                      //Natural: MOVE FALSE TO #CREF-PROD-FOUND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A).notEquals(getZero())))                                 //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A ) NE 0
                {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-MONTHLY-ACCUMULATIONS
                    sub_Calculate_Cref_Monthly_Accumulations();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Prod_Found.getBoolean()))                                                                                                              //Natural: IF #CREF-PROD-FOUND
            {
                if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A).notEquals(getZero())))                                   //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A ) NE 0
                {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-CREF-ANNUAL-ACCUMULATIONS
                    sub_Calculate_Cref_Annual_Accumulations();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Calculate_Cref_Monthly_Accumulations() throws Exception                                                                                              //Natural: CALCULATE-CREF-MONTHLY-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  #C = MODE #B-INDX = FUND #A = FUND NUMBER
        ldaAdsl1340.getPnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Pay_Cnt().getValue(pnd_C,pnd_All_Indx_Pnd_B_Indx).nadd(1);                                                       //Natural: ADD 1 TO #C-MTH-PAY-CNT ( #C,#B-INDX )
        //*  RS0
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'D'
        {
            ldaAdsl1340.getPnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Pay_Cnt().getValue(pnd_C).nadd(1);                                                               //Natural: ADD 1 TO #CREF-MTH-SUB-PAY-CNT ( #C )
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl1340.getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Pay_Cnt().getValue(pnd_C).nadd(1);                                                                             //Natural: ADD 1 TO #TOTAL-PAY-CNT ( #C )
        ldaAdsl1340.getPnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Pay_Cnt().getValue(5,pnd_All_Indx_Pnd_B_Indx).nadd(1);                                                           //Natural: ADD 1 TO #C-MTH-PAY-CNT ( 5,#B-INDX )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(pnd_C,16).nadd(1);                                                                                    //Natural: ADD 1 TO #TIAA-PAY-CNT ( #C,16 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(5,16).nadd(1);                                                                                        //Natural: ADD 1 TO #TIAA-PAY-CNT ( 5,16 )
        //*  RS0
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'D'
        {
            ldaAdsl1340.getPnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Pay_Cnt().getValue(5).nadd(1);                                                                   //Natural: ADD 1 TO #CREF-MTH-SUB-PAY-CNT ( 5 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl1340.getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Pay_Cnt().getValue(5).nadd(1);                                                                                 //Natural: ADD 1 TO #TOTAL-PAY-CNT ( 5 )
        ldaAdsl1340.getPnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Units().getValue(pnd_C,pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A ) TO #C-MTH-UNITS ( #C,#B-INDX )
        //*  RS0
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'D'
        {
            ldaAdsl1340.getPnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Units().getValue(pnd_C).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A ) TO #CREF-MTH-SUB-UNITS ( #C )
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl1340.getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Units().getValue(pnd_C).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A ) TO #TOTAL-UNITS ( #C )
        ldaAdsl1340.getPnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Units().getValue(5,pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A ) TO #C-MTH-UNITS ( 5,#B-INDX )
        //*  RS0
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'D'
        {
            ldaAdsl1340.getPnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Units().getValue(5).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A ) TO #CREF-MTH-SUB-UNITS ( 5 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl1340.getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Units().getValue(5).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #A ) TO #TOTAL-UNITS ( 5 )
        ldaAdsl1340.getPnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Dollars().getValue(pnd_C,pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A ) TO #C-MTH-DOLLARS ( #C,#B-INDX )
        //*  RS0
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'D'
        {
            ldaAdsl1340.getPnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Dollars().getValue(pnd_C).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A ) TO #CREF-MTH-SUB-DOLLARS ( #C )
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl1340.getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Dollars().getValue(pnd_C).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A ) TO #TOTAL-DOLLARS ( #C )
        ldaAdsl1340.getPnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Dollars().getValue(5,pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A ) TO #C-MTH-DOLLARS ( 5,#B-INDX )
        //*  RS0
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'D'
        {
            ldaAdsl1340.getPnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Dollars().getValue(5).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A ) TO #CREF-MTH-SUB-DOLLARS ( 5 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl1340.getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Dollars().getValue(5).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A ) TO #TOTAL-DOLLARS ( 5 )
    }
    private void sub_Calculate_Cref_Annual_Accumulations() throws Exception                                                                                               //Natural: CALCULATE-CREF-ANNUAL-ACCUMULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  #C = PAYMENT MODE #B-INDX = FUND #A = FUND NUMBER
        ldaAdsl1340.getPnd_C_Ann_By_Mode_Pnd_C_Ann_Pay_Cnt().getValue(pnd_C,pnd_All_Indx_Pnd_B_Indx).nadd(1);                                                             //Natural: ADD 1 TO #C-ANN-PAY-CNT ( #C,#B-INDX )
        //*  RS0
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'D'
        {
            ldaAdsl1340.getPnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Pay_Cnt().getValue(pnd_C).nadd(1);                                                               //Natural: ADD 1 TO #CREF-ANN-SUB-PAY-CNT ( #C )
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl1340.getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Pay_Cnt().getValue(pnd_C).nadd(1);                                                                             //Natural: ADD 1 TO #TOTAL-PAY-CNT ( #C )
        ldaAdsl1340.getPnd_C_Ann_By_Mode_Pnd_C_Ann_Pay_Cnt().getValue(5,pnd_All_Indx_Pnd_B_Indx).nadd(1);                                                                 //Natural: ADD 1 TO #C-ANN-PAY-CNT ( 5,#B-INDX )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(pnd_C,16).nadd(1);                                                                                    //Natural: ADD 1 TO #TIAA-PAY-CNT ( #C,16 )
        ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(5,16).nadd(1);                                                                                        //Natural: ADD 1 TO #TIAA-PAY-CNT ( 5,16 )
        //*  RS0
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'D'
        {
            ldaAdsl1340.getPnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Pay_Cnt().getValue(5).nadd(1);                                                                   //Natural: ADD 1 TO #CREF-ANN-SUB-PAY-CNT ( 5 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl1340.getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Pay_Cnt().getValue(5).nadd(1);                                                                                 //Natural: ADD 1 TO #TOTAL-PAY-CNT ( 5 )
        ldaAdsl1340.getPnd_C_Ann_By_Mode_Pnd_C_Ann_Units().getValue(pnd_C,pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A ) TO #C-ANN-UNITS ( #C,#B-INDX )
        //*  RS0
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'D'
        {
            ldaAdsl1340.getPnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Units().getValue(pnd_C).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A ) TO #CREF-ANN-SUB-UNITS ( #C )
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl1340.getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Units().getValue(pnd_C).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A ) TO #TOTAL-UNITS ( #C )
        ldaAdsl1340.getPnd_C_Ann_By_Mode_Pnd_C_Ann_Units().getValue(5,pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A ) TO #C-ANN-UNITS ( 5,#B-INDX )
        //*  RS0
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'D'
        {
            ldaAdsl1340.getPnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Units().getValue(5).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A ) TO #CREF-ANN-SUB-UNITS ( 5 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl1340.getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Units().getValue(5).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #A ) TO #TOTAL-UNITS ( 5 )
        ldaAdsl1340.getPnd_C_Ann_By_Mode_Pnd_C_Ann_Dollars().getValue(pnd_C,pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A ) TO #C-ANN-DOLLARS ( #C,#B-INDX )
        //*  RS0
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'D'
        {
            ldaAdsl1340.getPnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Dollars().getValue(pnd_C).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A ) TO #CREF-ANN-SUB-DOLLARS ( #C )
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl1340.getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Dollars().getValue(pnd_C).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A ) TO #TOTAL-DOLLARS ( #C )
        ldaAdsl1340.getPnd_C_Ann_By_Mode_Pnd_C_Ann_Dollars().getValue(5,pnd_All_Indx_Pnd_B_Indx).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A ) TO #C-ANN-DOLLARS ( 5,#B-INDX )
        //*  RS0
        if (condition(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("R") && ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_All_Indx_Pnd_A).notEquals("D"))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #A ) NE 'D'
        {
            ldaAdsl1340.getPnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Dollars().getValue(5).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A ) TO #CREF-ANN-SUB-DOLLARS ( 5 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl1340.getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Dollars().getValue(5).nadd(ldaAdsl450a.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_All_Indx_Pnd_A)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #A ) TO #TOTAL-DOLLARS ( 5 )
    }
    private void sub_Break_Of_Annty_Start_Date_Rtn() throws Exception                                                                                                     //Natural: BREAK-OF-ANNTY-START-DATE-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        ldaAdsl1340.getPnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Mm().setValue(pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Mm);                                  //Natural: MOVE #PREV-PAYMENT-DUE-DATE-MM TO #PAYMENT-DATE-MCDY-MM
        ldaAdsl1340.getPnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Dd().setValue(pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Dd);                                  //Natural: MOVE #PREV-PAYMENT-DUE-DATE-DD TO #PAYMENT-DATE-MCDY-DD
        ldaAdsl1340.getPnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Cc().setValue(pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Cc);                                  //Natural: MOVE #PREV-PAYMENT-DUE-DATE-CC TO #PAYMENT-DATE-MCDY-CC
        ldaAdsl1340.getPnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Yy().setValue(pnd_Prev_Payment_Due_Date_Pnd_Prev_Payment_Due_Date_Yy);                                  //Natural: MOVE #PREV-PAYMENT-DUE-DATE-YY TO #PAYMENT-DATE-MCDY-YY
        pnd_X.setValue(1);                                                                                                                                                //Natural: ASSIGN #X := 1
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_X.greater(5))) {break;}                                                                                                                     //Natural: UNTIL #X > 5
            ldaAdsl1340.getPnd_M_Tiaa_By_Details().getValue("*").reset();                                                                                                 //Natural: RESET #M-TIAA-BY-DETAILS ( * )
            ldaAdsl1340.getPnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt().getValue("*").setValue(ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt().getValue(pnd_X,           //Natural: MOVE #TIAA-PAY-CNT ( #X,* ) TO #M-TIAA-PAY-CNT ( * )
                "*"));
            ldaAdsl1340.getPnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt().getValue("*").setValue(ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt().getValue(pnd_X,           //Natural: MOVE #TIAA-GTD-AMT ( #X,* ) TO #M-TIAA-GTD-AMT ( * )
                "*"));
            ldaAdsl1340.getPnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt().getValue("*").setValue(ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt().getValue(pnd_X,           //Natural: MOVE #TIAA-DIV-AMT ( #X,* ) TO #M-TIAA-DIV-AMT ( * )
                "*"));
            ldaAdsl1340.getPnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt().getValue("*").setValue(ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt().getValue(pnd_X,   //Natural: MOVE #TIAA-FIN-GTD-AMT ( #X,* ) TO #M-TIAA-FIN-GTD-AMT ( * )
                "*"));
            ldaAdsl1340.getPnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt().getValue("*").setValue(ldaAdsl1340.getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt().getValue(pnd_X,   //Natural: MOVE #TIAA-FIN-DIV-AMT ( #X,* ) TO #M-TIAA-FIN-DIV-AMT ( * )
                "*"));
            ldaAdsl1340.getPnd_M_C_Mth_Minor_Accum().getValue("*").reset();                                                                                               //Natural: RESET #M-C-MTH-MINOR-ACCUM ( * )
            ldaAdsl1340.getPnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt().getValue("*").setValue(ldaAdsl1340.getPnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Pay_Cnt().getValue(pnd_X, //Natural: MOVE #C-MTH-PAY-CNT ( #X,* ) TO #M-C-MTH-PAY-CNT ( * )
                "*"));
            ldaAdsl1340.getPnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units().getValue("*").setValue(ldaAdsl1340.getPnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Units().getValue(pnd_X,    //Natural: MOVE #C-MTH-UNITS ( #X,* ) TO #M-C-MTH-UNITS ( * )
                "*"));
            ldaAdsl1340.getPnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars().getValue("*").setValue(ldaAdsl1340.getPnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Dollars().getValue(pnd_X, //Natural: MOVE #C-MTH-DOLLARS ( #X,* ) TO #M-C-MTH-DOLLARS ( * )
                "*"));
            ldaAdsl1340.getPnd_M_C_Ann_Minor_Accum().getValue("*").reset();                                                                                               //Natural: RESET #M-C-ANN-MINOR-ACCUM ( * )
            ldaAdsl1340.getPnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt().getValue("*").setValue(ldaAdsl1340.getPnd_C_Ann_By_Mode_Pnd_C_Ann_Pay_Cnt().getValue(pnd_X,      //Natural: MOVE #C-ANN-PAY-CNT ( #X,* ) TO #M-C-ANN-PAY-CNT ( * )
                "*"));
            ldaAdsl1340.getPnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units().getValue("*").setValue(ldaAdsl1340.getPnd_C_Ann_By_Mode_Pnd_C_Ann_Units().getValue(pnd_X,          //Natural: MOVE #C-ANN-UNITS ( #X,* ) TO #M-C-ANN-UNITS ( * )
                "*"));
            ldaAdsl1340.getPnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars().getValue("*").setValue(ldaAdsl1340.getPnd_C_Ann_By_Mode_Pnd_C_Ann_Dollars().getValue(pnd_X,      //Natural: MOVE #C-ANN-DOLLARS ( #X,* ) TO #M-C-ANN-DOLLARS ( * )
                "*"));
            ldaAdsl1340.getPnd_M_Cref_Mth_Sub_Total_Accum().reset();                                                                                                      //Natural: RESET #M-CREF-MTH-SUB-TOTAL-ACCUM
            ldaAdsl1340.getPnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Pay_Cnt().setValue(ldaAdsl1340.getPnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Pay_Cnt().getValue(pnd_X)); //Natural: MOVE #CREF-MTH-SUB-PAY-CNT ( #X ) TO #M-CREF-MTH-SUB-PAY-CNT
            ldaAdsl1340.getPnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Units().setValue(ldaAdsl1340.getPnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Units().getValue(pnd_X)); //Natural: MOVE #CREF-MTH-SUB-UNITS ( #X ) TO #M-CREF-MTH-SUB-UNITS
            ldaAdsl1340.getPnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Dollars().setValue(ldaAdsl1340.getPnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Dollars().getValue(pnd_X)); //Natural: MOVE #CREF-MTH-SUB-DOLLARS ( #X ) TO #M-CREF-MTH-SUB-DOLLARS
            ldaAdsl1340.getPnd_M_Cref_Ann_Sub_Total_Accum().reset();                                                                                                      //Natural: RESET #M-CREF-ANN-SUB-TOTAL-ACCUM
            ldaAdsl1340.getPnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Pay_Cnt().setValue(ldaAdsl1340.getPnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Pay_Cnt().getValue(pnd_X)); //Natural: MOVE #CREF-ANN-SUB-PAY-CNT ( #X ) TO #M-CREF-ANN-SUB-PAY-CNT
            ldaAdsl1340.getPnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Units().setValue(ldaAdsl1340.getPnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Units().getValue(pnd_X)); //Natural: MOVE #CREF-ANN-SUB-UNITS ( #X ) TO #M-CREF-ANN-SUB-UNITS
            ldaAdsl1340.getPnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Dollars().setValue(ldaAdsl1340.getPnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Dollars().getValue(pnd_X)); //Natural: MOVE #CREF-ANN-SUB-DOLLARS ( #X ) TO #M-CREF-ANN-SUB-DOLLARS
            ldaAdsl1340.getPnd_M_Total_Tiaa_Cref_Accum().reset();                                                                                                         //Natural: RESET #M-TOTAL-TIAA-CREF-ACCUM
            ldaAdsl1340.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Pay_Cnt().setValue(ldaAdsl1340.getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Pay_Cnt().getValue(pnd_X));      //Natural: MOVE #TOTAL-PAY-CNT ( #X ) TO #M-TOTAL-PAY-CNT
            ldaAdsl1340.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Units().setValue(ldaAdsl1340.getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Units().getValue(pnd_X));          //Natural: MOVE #TOTAL-UNITS ( #X ) TO #M-TOTAL-UNITS
            ldaAdsl1340.getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Dollars().setValue(ldaAdsl1340.getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Dollars().getValue(pnd_X));      //Natural: MOVE #TOTAL-DOLLARS ( #X ) TO #M-TOTAL-DOLLARS
            ldaAdsl1340.getPnd_Mode_Description().setValue(pnd_Mode_Desc.getValue(pnd_X));                                                                                //Natural: MOVE #MODE-DESC ( #X ) TO #MODE-DESCRIPTION
            //*  GG050608
            if (condition(! (pnd_Surv_Only_Run.getBoolean())))                                                                                                            //Natural: IF NOT #SURV-ONLY-RUN
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm1341.class));                                                                      //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM1341'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 2 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm1341.class));                                                                      //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM1341'
            }                                                                                                                                                             //Natural: END-IF
            pnd_X.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #X
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        ldaAdsl1340.getPnd_Tiaa_By_Mode().getValue("*").reset();                                                                                                          //Natural: RESET #TIAA-BY-MODE ( * ) #M-TIAA-BY-DETAILS ( * ) #C-MTH-ACCUM-BY-MODE ( * ) #M-C-MTH-MINOR-ACCUM ( * ) #C-ANN-BY-MODE ( * ) #M-C-ANN-MINOR-ACCUM ( * ) #CREF-MTH-SUB-TOTAL-ACCUM ( * ) #M-CREF-MTH-SUB-TOTAL-ACCUM #CREF-ANN-SUB-TOTAL-ACCUM ( * ) #M-CREF-ANN-SUB-TOTAL-ACCUM #TOTAL-TIAA-CREF-ACCUM ( * ) #M-TOTAL-TIAA-CREF-ACCUM #M-T-CREF-UNITS ( * ) #M-T-CREF-DOLLARS ( * )
        ldaAdsl1340.getPnd_M_Tiaa_By_Details().getValue("*").reset();
        ldaAdsl1340.getPnd_C_Mth_Accum_By_Mode().getValue("*").reset();
        ldaAdsl1340.getPnd_M_C_Mth_Minor_Accum().getValue("*").reset();
        ldaAdsl1340.getPnd_C_Ann_By_Mode().getValue("*").reset();
        ldaAdsl1340.getPnd_M_C_Ann_Minor_Accum().getValue("*").reset();
        ldaAdsl1340.getPnd_Cref_Mth_Sub_Total_Accum().getValue("*").reset();
        ldaAdsl1340.getPnd_M_Cref_Mth_Sub_Total_Accum().reset();
        ldaAdsl1340.getPnd_Cref_Ann_Sub_Total_Accum().getValue("*").reset();
        ldaAdsl1340.getPnd_M_Cref_Ann_Sub_Total_Accum().reset();
        ldaAdsl1340.getPnd_Total_Tiaa_Cref_Accum().getValue("*").reset();
        ldaAdsl1340.getPnd_M_Total_Tiaa_Cref_Accum().reset();
        ldaAdsl1340.getPnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units().getValue("*").reset();
        ldaAdsl1340.getPnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars().getValue("*").reset();
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Heading.setValue("Annuitization of OmniPlus Survivor Contracts        ");                                                                         //Natural: ASSIGN #HEADING := 'Annuitization of OmniPlus Survivor Contracts        '
                    //*  GG050608
                    if (condition(! (pnd_Surv_Only_Run.getBoolean())))                                                                                                    //Natural: IF NOT #SURV-ONLY-RUN
                    {
                        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Adsm1342.class));                                                              //Natural: WRITE ( 1 ) NOTITLE USING FORM 'ADSM1342'
                    }                                                                                                                                                     //Natural: END-IF
                    //*  GG050608
                    //*  DEA
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Heading.setValue("Annuitization of OmniPlus Survivor 403b/401k Contracts");                                                                       //Natural: ASSIGN #HEADING := 'Annuitization of OmniPlus Survivor 403b/401k Contracts'
                    if (condition(pnd_Surv_Only_Run.getBoolean()))                                                                                                        //Natural: IF #SURV-ONLY-RUN
                    {
                        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Adsm1342.class));                                                              //Natural: WRITE ( 2 ) NOTITLE USING FORM 'ADSM1342'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=79 PS=60");
        Global.format(1, "LS=79 PS=60");
        Global.format(2, "LS=79 PS=60");
    }
}
