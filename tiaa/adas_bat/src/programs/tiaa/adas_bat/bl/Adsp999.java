/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:06:12 PM
**        * FROM NATURAL PROGRAM : Adsp999
************************************************************
**        * FILE NAME            : Adsp999.java
**        * CLASS NAME           : Adsp999
**        * INSTANCE NAME        : Adsp999
************************************************************
************************************************************************
* PROGRAM  : ADSP999
* GENERATED: MAY 10, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : GENERATES RATE BASED SUMMARY REPORT
*
************************  MAINTENANCE LOG ******************************
*  D A T E   PROGRAMMER     D E S C R I P T I O N
* 05/10/04   C. AVE         CREATED
* 07/27/04   T. SHINE       IF MONTH-END PROCESS FALLS ON WEEKEND OR
*                           HOLIDAY, USE ADS-RPT-13 (FACTOR DATE) AS
*                           REPORT DATE.
* 11/28/06   N. CVETKOVIC   MODIFY TO USE SUBPRODUCT CODE FOR TOTALS
*  1/26/07   N. CVETKOVIC   ADD MORE LOBS TO TABLE OF LOBS
* 05/06/08   G. GUERRERO    CECK ROTH IND ON INPUT FILE TO PRINT ROTH
*                           REPORT.
* 12/16/08   R.SACHARNY    SORT CODE "B" BELONGS TO STABLE RETURN FUND
* 03/31/10   D.E.ANDER     ADABAS REPLATFORM, PCPOP STABLE RETURN AND
*                          INDEXED INFO FOR SURVIVOR INFO MARKED DEA
* 10/13/11   O. SOTTO      CHANGED THE HEADING OF RP LOB TO
*                          RETIREMENT CHOICE PLUS ANNUITIES.  SC 101311.
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp999 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Work_File;

    private DbsGroup pnd_Work_File__R_Field_1;
    private DbsField pnd_Work_File_Pnd_Wk_Lst_Actvty_Dte;

    private DbsGroup pnd_Work_File__R_Field_2;
    private DbsField pnd_Work_File_Pnd_Wk_Lst_Actvty_Yy;
    private DbsField pnd_Work_File_Pnd_Wk_Lst_Actvty_Mm;
    private DbsField pnd_Work_File_Pnd_Wk_Lst_Actvty_Dd;
    private DbsField pnd_Work_File_Pnd_Wk_Stts_Acct;

    private DbsGroup pnd_Work_File__R_Field_3;
    private DbsField pnd_Work_File_Pnd_Wk_Stts;
    private DbsField pnd_Work_File_Pnd_Wk_Acct;
    private DbsField pnd_Work_File_Pnd_Wk_Lob;
    private DbsField pnd_Work_File_Pnd_Wk_Sub_Seq;
    private DbsField pnd_Work_File_Pnd_Wk_Sub_Lob;
    private DbsField pnd_Work_File_Pnd_Wk_Rate;
    private DbsField pnd_Work_File_Pnd_Wk_Actl_Amt;
    private DbsField pnd_Work_File_Pnd_Wk_Roth_Ind;
    private DbsField pnd_Work_File_Pnd_Wk_Surv_Ind;
    private DbsField pnd_S;
    private DbsField pnd_I;
    private DbsField pnd_A;
    private DbsField pnd_I1;
    private DbsField pnd_I2;
    private DbsField pnd_Heading_Report_Svsaa;
    private DbsField pnd_Heading_Report_Tiaa;
    private DbsField pnd_Heading_Report_Cref;
    private DbsField pnd_Print_Id;
    private DbsField pnd_First_Time;
    private DbsField pnd_Rate_Break;
    private DbsField pnd_Lob_Break;
    private DbsField pnd_Sub_Lob_Break;
    private DbsField pnd_Stts_Acct_Break;
    private DbsField pnd_Lob_Ref_Desc;
    private DbsField pnd_Lob_Ref_Cde;
    private DbsField pnd_Acctng_Dte;

    private DbsGroup pnd_Acctng_Dte__R_Field_4;
    private DbsField pnd_Acctng_Dte_Pnd_Acctng_Mm;
    private DbsField pnd_Acctng_Dte_Pnd_Acctng_Dd;
    private DbsField pnd_Acctng_Dte_Pnd_Acctng_Yy;
    private DbsField pnd_Effctv_Dte;

    private DbsGroup pnd_Effctv_Dte__R_Field_5;
    private DbsField pnd_Effctv_Dte_Pnd_Effctv_Mm;
    private DbsField pnd_Effctv_Dte_Pnd_Effctv_Dd;
    private DbsField pnd_Effctv_Dte_Pnd_Effctv_Yy;
    private DbsField pnd_Sv_Effctv_Dte;

    private DbsGroup pnd_Sv_Effctv_Dte__R_Field_6;
    private DbsField pnd_Sv_Effctv_Dte_Pnd_Sv_Effctv_Yy;
    private DbsField pnd_Sv_Effctv_Dte_Pnd_Sv_Effctv_Mm;
    private DbsField pnd_Sv_Effctv_Dte_Pnd_Sv_Effctv_Dd;
    private DbsField pnd_Had_Sub_Lob;
    private DbsField pnd_Sv_Lob;
    private DbsField pnd_Sv_Lob_Indx;
    private DbsField pnd_Sv_Lst_Actvty_Dte;
    private DbsField pnd_Sv_Stts_Acct;

    private DbsGroup pnd_Sv_Stts_Acct__R_Field_7;
    private DbsField pnd_Sv_Stts_Acct_Pnd_Sv_Status;
    private DbsField pnd_Sv_Stts_Acct_Pnd_Sv_Fund;
    private DbsField pnd_Sv_Rate;
    private DbsField pnd_Sv_Actl_Amt;
    private DbsField pnd_Sv_Lob_Actl_Amt;
    private DbsField pnd_Sv_Sub_Lob_Actl_Amt;
    private DbsField pnd_Sv_Lob_Desc;
    private DbsField pnd_Lob_Desc;
    private DbsField pnd_Read_Work;
    private DbsField pnd_Lob_Ctr;
    private DbsField pnd_Lob_Desc_Array;
    private DbsField pnd_Lob_Amt_Array;
    private DbsField pnd_Grand_Lob_Total;
    private DbsField pnd_Sv_Sub_Lob;
    private DbsField pnd_Sv_Sub_Lob_Indx;
    private DbsField pnd_Sv_Sub_Lob_Desc;
    private DbsField pnd_Sub_Lob_Desc;
    private DbsField pnd_Sub_Lob_Ctr;
    private DbsField pnd_Sub_Lob_Desc_Array;
    private DbsField pnd_Sub_Lob_Amt_Array;
    private DbsField pnd_Print_Line_Ctr;
    private DbsField pnd_Print_Rec;

    private DbsGroup pnd_Print_Rec__R_Field_8;
    private DbsField pnd_Print_Rec_Pnd_Prt_Fill1;
    private DbsField pnd_Print_Rec_Pnd_Prt_Rate;
    private DbsField pnd_Print_Rec_Pnd_Prt_Fill2;
    private DbsField pnd_Print_Rec_Pnd_Prt_Amt;

    private DbsGroup pnd_Print_Rec__R_Field_9;
    private DbsField pnd_Print_Rec_Pnd_Prt_Line1;
    private DbsField pnd_Print_Rec_Pnd_Prt_Fill3;
    private DbsField pnd_Print_Rec_Pnd_Prt_Line2;

    private DbsGroup pnd_Print_Rec__R_Field_10;
    private DbsField pnd_Print_Rec_Pnd_Prt_Total_Text;
    private DbsField pnd_Print_Rec_Pnd_Prt_Fill4;
    private DbsField pnd_Print_Rec_Pnd_Prt_Total_Amt;

    private DbsGroup pnd_Print_Rec__R_Field_11;
    private DbsField pnd_Print_Rec_Pnd_Prt_Fill5;
    private DbsField pnd_Print_Rec_Pnd_Prt_Lob_Hdr1;
    private DbsField pnd_Print_Rec_Pnd_Prt_Fill6;
    private DbsField pnd_Print_Rec_Pnd_Prt_Lob_Hdr2;

    private DbsGroup pnd_Print_Rec__R_Field_12;
    private DbsField pnd_Print_Rec_Pnd_Prt_Lob_Line1;
    private DbsField pnd_Print_Rec_Pnd_Prt_Fill7;
    private DbsField pnd_Print_Rec_Pnd_Prt_Lob_Line2;

    private DbsGroup pnd_Print_Rec__R_Field_13;
    private DbsField pnd_Print_Rec_Pnd_Prt_Lob_Desc;
    private DbsField pnd_Print_Rec_Pnd_Prt_Fill8;
    private DbsField pnd_Print_Rec_Pnd_Prt_Lob_Amt;
    private DbsField pnd_Stable;
    private DbsField pnd_Sv;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Work_File = localVariables.newFieldInRecord("pnd_Work_File", "#WORK-FILE", FieldType.STRING, 200);

        pnd_Work_File__R_Field_1 = localVariables.newGroupInRecord("pnd_Work_File__R_Field_1", "REDEFINE", pnd_Work_File);
        pnd_Work_File_Pnd_Wk_Lst_Actvty_Dte = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wk_Lst_Actvty_Dte", "#WK-LST-ACTVTY-DTE", FieldType.STRING, 
            8);

        pnd_Work_File__R_Field_2 = pnd_Work_File__R_Field_1.newGroupInGroup("pnd_Work_File__R_Field_2", "REDEFINE", pnd_Work_File_Pnd_Wk_Lst_Actvty_Dte);
        pnd_Work_File_Pnd_Wk_Lst_Actvty_Yy = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Wk_Lst_Actvty_Yy", "#WK-LST-ACTVTY-YY", FieldType.NUMERIC, 
            4);
        pnd_Work_File_Pnd_Wk_Lst_Actvty_Mm = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Wk_Lst_Actvty_Mm", "#WK-LST-ACTVTY-MM", FieldType.NUMERIC, 
            2);
        pnd_Work_File_Pnd_Wk_Lst_Actvty_Dd = pnd_Work_File__R_Field_2.newFieldInGroup("pnd_Work_File_Pnd_Wk_Lst_Actvty_Dd", "#WK-LST-ACTVTY-DD", FieldType.NUMERIC, 
            2);
        pnd_Work_File_Pnd_Wk_Stts_Acct = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wk_Stts_Acct", "#WK-STTS-ACCT", FieldType.STRING, 
            2);

        pnd_Work_File__R_Field_3 = pnd_Work_File__R_Field_1.newGroupInGroup("pnd_Work_File__R_Field_3", "REDEFINE", pnd_Work_File_Pnd_Wk_Stts_Acct);
        pnd_Work_File_Pnd_Wk_Stts = pnd_Work_File__R_Field_3.newFieldInGroup("pnd_Work_File_Pnd_Wk_Stts", "#WK-STTS", FieldType.STRING, 1);
        pnd_Work_File_Pnd_Wk_Acct = pnd_Work_File__R_Field_3.newFieldInGroup("pnd_Work_File_Pnd_Wk_Acct", "#WK-ACCT", FieldType.STRING, 1);
        pnd_Work_File_Pnd_Wk_Lob = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wk_Lob", "#WK-LOB", FieldType.STRING, 10);
        pnd_Work_File_Pnd_Wk_Sub_Seq = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wk_Sub_Seq", "#WK-SUB-SEQ", FieldType.STRING, 2);
        pnd_Work_File_Pnd_Wk_Sub_Lob = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wk_Sub_Lob", "#WK-SUB-LOB", FieldType.STRING, 30);
        pnd_Work_File_Pnd_Wk_Rate = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wk_Rate", "#WK-RATE", FieldType.STRING, 2);
        pnd_Work_File_Pnd_Wk_Actl_Amt = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wk_Actl_Amt", "#WK-ACTL-AMT", FieldType.NUMERIC, 11, 
            2);
        pnd_Work_File_Pnd_Wk_Roth_Ind = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wk_Roth_Ind", "#WK-ROTH-IND", FieldType.STRING, 1);
        pnd_Work_File_Pnd_Wk_Surv_Ind = pnd_Work_File__R_Field_1.newFieldInGroup("pnd_Work_File_Pnd_Wk_Surv_Ind", "#WK-SURV-IND", FieldType.STRING, 1);
        pnd_S = localVariables.newFieldInRecord("pnd_S", "#S", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 2);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 2);
        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.PACKED_DECIMAL, 2);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 2);
        pnd_Heading_Report_Svsaa = localVariables.newFieldInRecord("pnd_Heading_Report_Svsaa", "#HEADING-REPORT-SVSAA", FieldType.STRING, 51);
        pnd_Heading_Report_Tiaa = localVariables.newFieldInRecord("pnd_Heading_Report_Tiaa", "#HEADING-REPORT-TIAA", FieldType.STRING, 51);
        pnd_Heading_Report_Cref = localVariables.newFieldInRecord("pnd_Heading_Report_Cref", "#HEADING-REPORT-CREF", FieldType.STRING, 51);
        pnd_Print_Id = localVariables.newFieldInRecord("pnd_Print_Id", "#PRINT-ID", FieldType.NUMERIC, 2);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Rate_Break = localVariables.newFieldInRecord("pnd_Rate_Break", "#RATE-BREAK", FieldType.BOOLEAN, 1);
        pnd_Lob_Break = localVariables.newFieldInRecord("pnd_Lob_Break", "#LOB-BREAK", FieldType.BOOLEAN, 1);
        pnd_Sub_Lob_Break = localVariables.newFieldInRecord("pnd_Sub_Lob_Break", "#SUB-LOB-BREAK", FieldType.BOOLEAN, 1);
        pnd_Stts_Acct_Break = localVariables.newFieldInRecord("pnd_Stts_Acct_Break", "#STTS-ACCT-BREAK", FieldType.BOOLEAN, 1);
        pnd_Lob_Ref_Desc = localVariables.newFieldArrayInRecord("pnd_Lob_Ref_Desc", "#LOB-REF-DESC", FieldType.STRING, 50, new DbsArrayController(1, 15));
        pnd_Lob_Ref_Cde = localVariables.newFieldArrayInRecord("pnd_Lob_Ref_Cde", "#LOB-REF-CDE", FieldType.STRING, 10, new DbsArrayController(1, 15));
        pnd_Acctng_Dte = localVariables.newFieldInRecord("pnd_Acctng_Dte", "#ACCTNG-DTE", FieldType.NUMERIC, 8);

        pnd_Acctng_Dte__R_Field_4 = localVariables.newGroupInRecord("pnd_Acctng_Dte__R_Field_4", "REDEFINE", pnd_Acctng_Dte);
        pnd_Acctng_Dte_Pnd_Acctng_Mm = pnd_Acctng_Dte__R_Field_4.newFieldInGroup("pnd_Acctng_Dte_Pnd_Acctng_Mm", "#ACCTNG-MM", FieldType.NUMERIC, 2);
        pnd_Acctng_Dte_Pnd_Acctng_Dd = pnd_Acctng_Dte__R_Field_4.newFieldInGroup("pnd_Acctng_Dte_Pnd_Acctng_Dd", "#ACCTNG-DD", FieldType.NUMERIC, 2);
        pnd_Acctng_Dte_Pnd_Acctng_Yy = pnd_Acctng_Dte__R_Field_4.newFieldInGroup("pnd_Acctng_Dte_Pnd_Acctng_Yy", "#ACCTNG-YY", FieldType.NUMERIC, 4);
        pnd_Effctv_Dte = localVariables.newFieldInRecord("pnd_Effctv_Dte", "#EFFCTV-DTE", FieldType.NUMERIC, 8);

        pnd_Effctv_Dte__R_Field_5 = localVariables.newGroupInRecord("pnd_Effctv_Dte__R_Field_5", "REDEFINE", pnd_Effctv_Dte);
        pnd_Effctv_Dte_Pnd_Effctv_Mm = pnd_Effctv_Dte__R_Field_5.newFieldInGroup("pnd_Effctv_Dte_Pnd_Effctv_Mm", "#EFFCTV-MM", FieldType.NUMERIC, 2);
        pnd_Effctv_Dte_Pnd_Effctv_Dd = pnd_Effctv_Dte__R_Field_5.newFieldInGroup("pnd_Effctv_Dte_Pnd_Effctv_Dd", "#EFFCTV-DD", FieldType.NUMERIC, 2);
        pnd_Effctv_Dte_Pnd_Effctv_Yy = pnd_Effctv_Dte__R_Field_5.newFieldInGroup("pnd_Effctv_Dte_Pnd_Effctv_Yy", "#EFFCTV-YY", FieldType.NUMERIC, 4);
        pnd_Sv_Effctv_Dte = localVariables.newFieldInRecord("pnd_Sv_Effctv_Dte", "#SV-EFFCTV-DTE", FieldType.NUMERIC, 8);

        pnd_Sv_Effctv_Dte__R_Field_6 = localVariables.newGroupInRecord("pnd_Sv_Effctv_Dte__R_Field_6", "REDEFINE", pnd_Sv_Effctv_Dte);
        pnd_Sv_Effctv_Dte_Pnd_Sv_Effctv_Yy = pnd_Sv_Effctv_Dte__R_Field_6.newFieldInGroup("pnd_Sv_Effctv_Dte_Pnd_Sv_Effctv_Yy", "#SV-EFFCTV-YY", FieldType.NUMERIC, 
            4);
        pnd_Sv_Effctv_Dte_Pnd_Sv_Effctv_Mm = pnd_Sv_Effctv_Dte__R_Field_6.newFieldInGroup("pnd_Sv_Effctv_Dte_Pnd_Sv_Effctv_Mm", "#SV-EFFCTV-MM", FieldType.NUMERIC, 
            2);
        pnd_Sv_Effctv_Dte_Pnd_Sv_Effctv_Dd = pnd_Sv_Effctv_Dte__R_Field_6.newFieldInGroup("pnd_Sv_Effctv_Dte_Pnd_Sv_Effctv_Dd", "#SV-EFFCTV-DD", FieldType.NUMERIC, 
            2);
        pnd_Had_Sub_Lob = localVariables.newFieldInRecord("pnd_Had_Sub_Lob", "#HAD-SUB-LOB", FieldType.BOOLEAN, 1);
        pnd_Sv_Lob = localVariables.newFieldInRecord("pnd_Sv_Lob", "#SV-LOB", FieldType.STRING, 10);
        pnd_Sv_Lob_Indx = localVariables.newFieldInRecord("pnd_Sv_Lob_Indx", "#SV-LOB-INDX", FieldType.STRING, 2);
        pnd_Sv_Lst_Actvty_Dte = localVariables.newFieldInRecord("pnd_Sv_Lst_Actvty_Dte", "#SV-LST-ACTVTY-DTE", FieldType.STRING, 8);
        pnd_Sv_Stts_Acct = localVariables.newFieldInRecord("pnd_Sv_Stts_Acct", "#SV-STTS-ACCT", FieldType.STRING, 2);

        pnd_Sv_Stts_Acct__R_Field_7 = localVariables.newGroupInRecord("pnd_Sv_Stts_Acct__R_Field_7", "REDEFINE", pnd_Sv_Stts_Acct);
        pnd_Sv_Stts_Acct_Pnd_Sv_Status = pnd_Sv_Stts_Acct__R_Field_7.newFieldInGroup("pnd_Sv_Stts_Acct_Pnd_Sv_Status", "#SV-STATUS", FieldType.STRING, 
            1);
        pnd_Sv_Stts_Acct_Pnd_Sv_Fund = pnd_Sv_Stts_Acct__R_Field_7.newFieldInGroup("pnd_Sv_Stts_Acct_Pnd_Sv_Fund", "#SV-FUND", FieldType.STRING, 1);
        pnd_Sv_Rate = localVariables.newFieldInRecord("pnd_Sv_Rate", "#SV-RATE", FieldType.STRING, 2);
        pnd_Sv_Actl_Amt = localVariables.newFieldInRecord("pnd_Sv_Actl_Amt", "#SV-ACTL-AMT", FieldType.NUMERIC, 14, 2);
        pnd_Sv_Lob_Actl_Amt = localVariables.newFieldInRecord("pnd_Sv_Lob_Actl_Amt", "#SV-LOB-ACTL-AMT", FieldType.NUMERIC, 14, 2);
        pnd_Sv_Sub_Lob_Actl_Amt = localVariables.newFieldInRecord("pnd_Sv_Sub_Lob_Actl_Amt", "#SV-SUB-LOB-ACTL-AMT", FieldType.NUMERIC, 14, 2);
        pnd_Sv_Lob_Desc = localVariables.newFieldInRecord("pnd_Sv_Lob_Desc", "#SV-LOB-DESC", FieldType.STRING, 60);
        pnd_Lob_Desc = localVariables.newFieldInRecord("pnd_Lob_Desc", "#LOB-DESC", FieldType.STRING, 50);
        pnd_Read_Work = localVariables.newFieldInRecord("pnd_Read_Work", "#READ-WORK", FieldType.PACKED_DECIMAL, 7);
        pnd_Lob_Ctr = localVariables.newFieldInRecord("pnd_Lob_Ctr", "#LOB-CTR", FieldType.NUMERIC, 3);
        pnd_Lob_Desc_Array = localVariables.newFieldArrayInRecord("pnd_Lob_Desc_Array", "#LOB-DESC-ARRAY", FieldType.STRING, 60, new DbsArrayController(1, 
            30));
        pnd_Lob_Amt_Array = localVariables.newFieldArrayInRecord("pnd_Lob_Amt_Array", "#LOB-AMT-ARRAY", FieldType.NUMERIC, 14, 2, new DbsArrayController(1, 
            30));
        pnd_Grand_Lob_Total = localVariables.newFieldInRecord("pnd_Grand_Lob_Total", "#GRAND-LOB-TOTAL", FieldType.NUMERIC, 14, 2);
        pnd_Sv_Sub_Lob = localVariables.newFieldInRecord("pnd_Sv_Sub_Lob", "#SV-SUB-LOB", FieldType.STRING, 30);
        pnd_Sv_Sub_Lob_Indx = localVariables.newFieldInRecord("pnd_Sv_Sub_Lob_Indx", "#SV-SUB-LOB-INDX", FieldType.STRING, 2);
        pnd_Sv_Sub_Lob_Desc = localVariables.newFieldInRecord("pnd_Sv_Sub_Lob_Desc", "#SV-SUB-LOB-DESC", FieldType.STRING, 60);
        pnd_Sub_Lob_Desc = localVariables.newFieldInRecord("pnd_Sub_Lob_Desc", "#SUB-LOB-DESC", FieldType.STRING, 50);
        pnd_Sub_Lob_Ctr = localVariables.newFieldInRecord("pnd_Sub_Lob_Ctr", "#SUB-LOB-CTR", FieldType.NUMERIC, 3);
        pnd_Sub_Lob_Desc_Array = localVariables.newFieldArrayInRecord("pnd_Sub_Lob_Desc_Array", "#SUB-LOB-DESC-ARRAY", FieldType.STRING, 20, new DbsArrayController(1, 
            30, 1, 10));
        pnd_Sub_Lob_Amt_Array = localVariables.newFieldArrayInRecord("pnd_Sub_Lob_Amt_Array", "#SUB-LOB-AMT-ARRAY", FieldType.NUMERIC, 14, 2, new DbsArrayController(1, 
            30, 1, 10));
        pnd_Print_Line_Ctr = localVariables.newFieldInRecord("pnd_Print_Line_Ctr", "#PRINT-LINE-CTR", FieldType.NUMERIC, 2);
        pnd_Print_Rec = localVariables.newFieldInRecord("pnd_Print_Rec", "#PRINT-REC", FieldType.STRING, 90);

        pnd_Print_Rec__R_Field_8 = localVariables.newGroupInRecord("pnd_Print_Rec__R_Field_8", "REDEFINE", pnd_Print_Rec);
        pnd_Print_Rec_Pnd_Prt_Fill1 = pnd_Print_Rec__R_Field_8.newFieldInGroup("pnd_Print_Rec_Pnd_Prt_Fill1", "#PRT-FILL1", FieldType.STRING, 4);
        pnd_Print_Rec_Pnd_Prt_Rate = pnd_Print_Rec__R_Field_8.newFieldInGroup("pnd_Print_Rec_Pnd_Prt_Rate", "#PRT-RATE", FieldType.STRING, 2);
        pnd_Print_Rec_Pnd_Prt_Fill2 = pnd_Print_Rec__R_Field_8.newFieldInGroup("pnd_Print_Rec_Pnd_Prt_Fill2", "#PRT-FILL2", FieldType.STRING, 9);
        pnd_Print_Rec_Pnd_Prt_Amt = pnd_Print_Rec__R_Field_8.newFieldInGroup("pnd_Print_Rec_Pnd_Prt_Amt", "#PRT-AMT", FieldType.STRING, 18);

        pnd_Print_Rec__R_Field_9 = localVariables.newGroupInRecord("pnd_Print_Rec__R_Field_9", "REDEFINE", pnd_Print_Rec);
        pnd_Print_Rec_Pnd_Prt_Line1 = pnd_Print_Rec__R_Field_9.newFieldInGroup("pnd_Print_Rec_Pnd_Prt_Line1", "#PRT-LINE1", FieldType.STRING, 10);
        pnd_Print_Rec_Pnd_Prt_Fill3 = pnd_Print_Rec__R_Field_9.newFieldInGroup("pnd_Print_Rec_Pnd_Prt_Fill3", "#PRT-FILL3", FieldType.STRING, 5);
        pnd_Print_Rec_Pnd_Prt_Line2 = pnd_Print_Rec__R_Field_9.newFieldInGroup("pnd_Print_Rec_Pnd_Prt_Line2", "#PRT-LINE2", FieldType.STRING, 18);

        pnd_Print_Rec__R_Field_10 = localVariables.newGroupInRecord("pnd_Print_Rec__R_Field_10", "REDEFINE", pnd_Print_Rec);
        pnd_Print_Rec_Pnd_Prt_Total_Text = pnd_Print_Rec__R_Field_10.newFieldInGroup("pnd_Print_Rec_Pnd_Prt_Total_Text", "#PRT-TOTAL-TEXT", FieldType.STRING, 
            14);
        pnd_Print_Rec_Pnd_Prt_Fill4 = pnd_Print_Rec__R_Field_10.newFieldInGroup("pnd_Print_Rec_Pnd_Prt_Fill4", "#PRT-FILL4", FieldType.STRING, 1);
        pnd_Print_Rec_Pnd_Prt_Total_Amt = pnd_Print_Rec__R_Field_10.newFieldInGroup("pnd_Print_Rec_Pnd_Prt_Total_Amt", "#PRT-TOTAL-AMT", FieldType.STRING, 
            18);

        pnd_Print_Rec__R_Field_11 = localVariables.newGroupInRecord("pnd_Print_Rec__R_Field_11", "REDEFINE", pnd_Print_Rec);
        pnd_Print_Rec_Pnd_Prt_Fill5 = pnd_Print_Rec__R_Field_11.newFieldInGroup("pnd_Print_Rec_Pnd_Prt_Fill5", "#PRT-FILL5", FieldType.STRING, 15);
        pnd_Print_Rec_Pnd_Prt_Lob_Hdr1 = pnd_Print_Rec__R_Field_11.newFieldInGroup("pnd_Print_Rec_Pnd_Prt_Lob_Hdr1", "#PRT-LOB-HDR1", FieldType.STRING, 
            16);
        pnd_Print_Rec_Pnd_Prt_Fill6 = pnd_Print_Rec__R_Field_11.newFieldInGroup("pnd_Print_Rec_Pnd_Prt_Fill6", "#PRT-FILL6", FieldType.STRING, 33);
        pnd_Print_Rec_Pnd_Prt_Lob_Hdr2 = pnd_Print_Rec__R_Field_11.newFieldInGroup("pnd_Print_Rec_Pnd_Prt_Lob_Hdr2", "#PRT-LOB-HDR2", FieldType.STRING, 
            6);

        pnd_Print_Rec__R_Field_12 = localVariables.newGroupInRecord("pnd_Print_Rec__R_Field_12", "REDEFINE", pnd_Print_Rec);
        pnd_Print_Rec_Pnd_Prt_Lob_Line1 = pnd_Print_Rec__R_Field_12.newFieldInGroup("pnd_Print_Rec_Pnd_Prt_Lob_Line1", "#PRT-LOB-LINE1", FieldType.STRING, 
            50);
        pnd_Print_Rec_Pnd_Prt_Fill7 = pnd_Print_Rec__R_Field_12.newFieldInGroup("pnd_Print_Rec_Pnd_Prt_Fill7", "#PRT-FILL7", FieldType.STRING, 8);
        pnd_Print_Rec_Pnd_Prt_Lob_Line2 = pnd_Print_Rec__R_Field_12.newFieldInGroup("pnd_Print_Rec_Pnd_Prt_Lob_Line2", "#PRT-LOB-LINE2", FieldType.STRING, 
            18);

        pnd_Print_Rec__R_Field_13 = localVariables.newGroupInRecord("pnd_Print_Rec__R_Field_13", "REDEFINE", pnd_Print_Rec);
        pnd_Print_Rec_Pnd_Prt_Lob_Desc = pnd_Print_Rec__R_Field_13.newFieldInGroup("pnd_Print_Rec_Pnd_Prt_Lob_Desc", "#PRT-LOB-DESC", FieldType.STRING, 
            50);
        pnd_Print_Rec_Pnd_Prt_Fill8 = pnd_Print_Rec__R_Field_13.newFieldInGroup("pnd_Print_Rec_Pnd_Prt_Fill8", "#PRT-FILL8", FieldType.STRING, 8);
        pnd_Print_Rec_Pnd_Prt_Lob_Amt = pnd_Print_Rec__R_Field_13.newFieldInGroup("pnd_Print_Rec_Pnd_Prt_Lob_Amt", "#PRT-LOB-AMT", FieldType.STRING, 18);
        pnd_Stable = localVariables.newFieldInRecord("pnd_Stable", "#STABLE", FieldType.STRING, 8);
        pnd_Sv = localVariables.newFieldInRecord("pnd_Sv", "#SV", FieldType.STRING, 14);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_First_Time.setInitialValue(true);
        pnd_Stable.setInitialValue("(STABLE)");
        pnd_Sv.setInitialValue("(SVSAA)");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp999() throws Exception
    {
        super("Adsp999");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 100;//Natural: FORMAT ( 1 ) PS = 60 LS = 100;//Natural: FORMAT ( 2 ) PS = 60 LS = 100;//Natural: FORMAT ( 3 ) PS = 60 LS = 100;//Natural: FORMAT ( 4 ) PS = 60 LS = 100
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 3 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 4 )
                                                                                                                                                                          //Natural: PERFORM SETUP-LOB-REF
        sub_Setup_Lob_Ref();
        if (condition(Global.isEscape())) {return;}
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #WORK-FILE
        while (condition(getWorkFiles().read(1, pnd_Work_File)))
        {
            pnd_Read_Work.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #READ-WORK
                                                                                                                                                                          //Natural: PERFORM TEST-PRINT
            sub_Test_Print();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  TESTING LOOP FORCED TERMINATION
            //*  IF #READ-WORK < 100
            //*    WRITE '='
            //*      #WK-LST-ACTVTY-DTE
            //*      '=' #WK-STTS-ACCT
            //*      '=' #WK-LOB
            //*      / '=' #WK-SUB-SEQ
            //*      '=' #WK-SUB-LOB
            //*      '='   #WK-RATE
            //*      '='   #WK-ACTL-AMT
            //*  END-IF
            pnd_Rate_Break.reset();                                                                                                                                       //Natural: RESET #RATE-BREAK #LOB-BREAK #STTS-ACCT-BREAK #SUB-LOB-BREAK
            pnd_Lob_Break.reset();
            pnd_Stts_Acct_Break.reset();
            pnd_Sub_Lob_Break.reset();
            //*  DEA START
            if (condition(pnd_Work_File_Pnd_Wk_Acct.equals("C")))                                                                                                         //Natural: IF #WK-ACCT = 'C'
            {
                if (condition(pnd_Work_File_Pnd_Wk_Roth_Ind.equals("Y")))                                                                                                 //Natural: IF #WK-ROTH-IND = 'Y'
                {
                    pnd_Heading_Report_Svsaa.setValue("SVSAA Roth 403B/401K Rate Base Summary Report by LOB");                                                            //Natural: ASSIGN #HEADING-REPORT-SVSAA := 'SVSAA Roth 403B/401K Rate Base Summary Report by LOB'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Heading_Report_Svsaa.setValue("         SVSAA Rate Base Summary Report by LOB     ");                                                             //Natural: ASSIGN #HEADING-REPORT-SVSAA := '         SVSAA Rate Base Summary Report by LOB     '
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*       GG050608
                if (condition(pnd_Work_File_Pnd_Wk_Roth_Ind.equals("Y")))                                                                                                 //Natural: IF #WK-ROTH-IND = 'Y'
                {
                    pnd_Heading_Report_Tiaa.setValue("TIAA Roth 403b/401k Rate Base Summary Report By LOB");                                                              //Natural: ASSIGN #HEADING-REPORT-TIAA := 'TIAA Roth 403b/401k Rate Base Summary Report By LOB'
                    pnd_Heading_Report_Cref.setValue("CREF Roth 403b/401k Rate Base Summary Report By LOB");                                                              //Natural: ASSIGN #HEADING-REPORT-CREF := 'CREF Roth 403b/401k Rate Base Summary Report By LOB'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Heading_Report_Tiaa.setValue("          TIAA Rate Base Summary Report By LOB     ");                                                              //Natural: ASSIGN #HEADING-REPORT-TIAA := '          TIAA Rate Base Summary Report By LOB     '
                    pnd_Heading_Report_Cref.setValue("          CREF Rate Base Summary Report By LOB     ");                                                              //Natural: ASSIGN #HEADING-REPORT-CREF := '          CREF Rate Base Summary Report By LOB     '
                    //*  DEA END
                }                                                                                                                                                         //Natural: END-IF
                //*  DEA END
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_First_Time.getBoolean()))                                                                                                                   //Natural: IF #FIRST-TIME
            {
                pnd_Sv_Rate.setValue(pnd_Work_File_Pnd_Wk_Rate);                                                                                                          //Natural: ASSIGN #SV-RATE := #WK-RATE
                pnd_Sv_Actl_Amt.setValue(0);                                                                                                                              //Natural: ASSIGN #SV-ACTL-AMT := 0
                pnd_Sv_Sub_Lob.setValue(pnd_Work_File_Pnd_Wk_Sub_Lob);                                                                                                    //Natural: ASSIGN #SV-SUB-LOB := #WK-SUB-LOB
                pnd_Sv_Sub_Lob_Actl_Amt.setValue(0);                                                                                                                      //Natural: ASSIGN #SV-SUB-LOB-ACTL-AMT := 0
                pnd_Sv_Lob.setValue(pnd_Work_File_Pnd_Wk_Lob);                                                                                                            //Natural: ASSIGN #SV-LOB := #WK-LOB
                pnd_Sv_Lob_Actl_Amt.setValue(0);                                                                                                                          //Natural: ASSIGN #SV-LOB-ACTL-AMT := 0
                pnd_Sv_Stts_Acct.setValue(pnd_Work_File_Pnd_Wk_Stts_Acct);                                                                                                //Natural: ASSIGN #SV-STTS-ACCT := #WK-STTS-ACCT
                pnd_Sv_Lst_Actvty_Dte.setValue(pnd_Work_File_Pnd_Wk_Lst_Actvty_Dte);                                                                                      //Natural: ASSIGN #SV-LST-ACTVTY-DTE := #WK-LST-ACTVTY-DTE
                pnd_Acctng_Dte_Pnd_Acctng_Mm.setValue(pnd_Work_File_Pnd_Wk_Lst_Actvty_Mm);                                                                                //Natural: ASSIGN #ACCTNG-MM := #WK-LST-ACTVTY-MM
                pnd_Acctng_Dte_Pnd_Acctng_Dd.setValue(pnd_Work_File_Pnd_Wk_Lst_Actvty_Dd);                                                                                //Natural: ASSIGN #ACCTNG-DD := #WK-LST-ACTVTY-DD
                pnd_Acctng_Dte_Pnd_Acctng_Yy.setValue(pnd_Work_File_Pnd_Wk_Lst_Actvty_Yy);                                                                                //Natural: ASSIGN #ACCTNG-YY := #WK-LST-ACTVTY-YY
                pnd_First_Time.setValue(false);                                                                                                                           //Natural: ASSIGN #FIRST-TIME := FALSE
                if (condition(pnd_Work_File_Pnd_Wk_Sub_Lob.notEquals(" ")))                                                                                               //Natural: IF #WK-SUB-LOB NE ' '
                {
                    pnd_Had_Sub_Lob.setValue(true);                                                                                                                       //Natural: ASSIGN #HAD-SUB-LOB := TRUE
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM IDENTIFY-PRINTER-ID
                sub_Identify_Printer_Id();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    PERFORM PRINT-SUB-LOB-HEADER
                                                                                                                                                                          //Natural: PERFORM PRINT-LOB-HEADER
                sub_Print_Lob_Header();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_File_Pnd_Wk_Stts_Acct.equals(pnd_Sv_Stts_Acct) && pnd_Sv_Lst_Actvty_Dte.equals(pnd_Work_File_Pnd_Wk_Lst_Actvty_Dte)))                  //Natural: IF #WK-STTS-ACCT = #SV-STTS-ACCT AND #SV-LST-ACTVTY-DTE = #WK-LST-ACTVTY-DTE
            {
                if (condition(pnd_Work_File_Pnd_Wk_Lob.equals(pnd_Sv_Lob)))                                                                                               //Natural: IF #WK-LOB = #SV-LOB
                {
                    pnd_Sv_Lob_Actl_Amt.nadd(pnd_Work_File_Pnd_Wk_Actl_Amt);                                                                                              //Natural: ADD #WK-ACTL-AMT TO #SV-LOB-ACTL-AMT
                    //*   SUB LOB TEST GOES HERE
                    if (condition(pnd_Work_File_Pnd_Wk_Sub_Lob.equals(pnd_Sv_Sub_Lob)))                                                                                   //Natural: IF #WK-SUB-LOB = #SV-SUB-LOB
                    {
                        if (condition(pnd_Had_Sub_Lob.getBoolean()))                                                                                                      //Natural: IF #HAD-SUB-LOB
                        {
                            pnd_Sv_Sub_Lob_Actl_Amt.nadd(pnd_Work_File_Pnd_Wk_Actl_Amt);                                                                                  //Natural: ADD #WK-ACTL-AMT TO #SV-SUB-LOB-ACTL-AMT
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Work_File_Pnd_Wk_Rate.equals(pnd_Sv_Rate)))                                                                                     //Natural: IF #WK-RATE = #SV-RATE
                        {
                            pnd_Sv_Actl_Amt.nadd(pnd_Work_File_Pnd_Wk_Actl_Amt);                                                                                          //Natural: ADD #WK-ACTL-AMT TO #SV-ACTL-AMT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Rate_Break.setValue(true);                                                                                                                //Natural: ASSIGN #RATE-BREAK := TRUE
                                                                                                                                                                          //Natural: PERFORM BREAK-ROUTINE
                            sub_Break_Routine();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*  #WK-RATE = #SV-RATE
                        }                                                                                                                                                 //Natural: END-IF
                        //*  SUB LOB BREAK
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Rate_Break.setValue(true);                                                                                                                    //Natural: ASSIGN #RATE-BREAK := TRUE
                        pnd_Sub_Lob_Break.setValue(true);                                                                                                                 //Natural: ASSIGN #SUB-LOB-BREAK := TRUE
                                                                                                                                                                          //Natural: PERFORM BREAK-ROUTINE
                        sub_Break_Routine();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  #WK-SUB-LOB = #SV-SUB-LOB
                    }                                                                                                                                                     //Natural: END-IF
                    //*  LOB BREAK
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Rate_Break.setValue(true);                                                                                                                        //Natural: ASSIGN #RATE-BREAK := TRUE
                    pnd_Sub_Lob_Break.setValue(true);                                                                                                                     //Natural: ASSIGN #SUB-LOB-BREAK := TRUE
                    pnd_Lob_Break.setValue(true);                                                                                                                         //Natural: ASSIGN #LOB-BREAK := TRUE
                                                                                                                                                                          //Natural: PERFORM BREAK-ROUTINE
                    sub_Break_Routine();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  #WK-LOB = #SV-LOB
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rate_Break.setValue(true);                                                                                                                            //Natural: ASSIGN #RATE-BREAK := TRUE
                pnd_Sub_Lob_Break.setValue(true);                                                                                                                         //Natural: ASSIGN #SUB-LOB-BREAK := TRUE
                pnd_Lob_Break.setValue(true);                                                                                                                             //Natural: ASSIGN #LOB-BREAK := TRUE
                pnd_Stts_Acct_Break.setValue(true);                                                                                                                       //Natural: ASSIGN #STTS-ACCT-BREAK := TRUE
                                                                                                                                                                          //Natural: PERFORM BREAK-ROUTINE
                sub_Break_Routine();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  #WK-ACCT = #SV-ACCT
            }                                                                                                                                                             //Natural: END-IF
            pnd_Acctng_Dte_Pnd_Acctng_Mm.setValue(pnd_Work_File_Pnd_Wk_Lst_Actvty_Mm);                                                                                    //Natural: ASSIGN #ACCTNG-MM := #WK-LST-ACTVTY-MM
            pnd_Acctng_Dte_Pnd_Acctng_Dd.setValue(pnd_Work_File_Pnd_Wk_Lst_Actvty_Dd);                                                                                    //Natural: ASSIGN #ACCTNG-DD := #WK-LST-ACTVTY-DD
            pnd_Acctng_Dte_Pnd_Acctng_Yy.setValue(pnd_Work_File_Pnd_Wk_Lst_Actvty_Yy);                                                                                    //Natural: ASSIGN #ACCTNG-YY := #WK-LST-ACTVTY-YY
            if (condition(pnd_Work_File_Pnd_Wk_Sub_Lob.notEquals(" ")))                                                                                                   //Natural: IF #WK-SUB-LOB NE ' '
            {
                pnd_Had_Sub_Lob.setValue(true);                                                                                                                           //Natural: ASSIGN #HAD-SUB-LOB := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM PRINT-RATE
        sub_Print_Rate();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Sv_Sub_Lob.notEquals(" ")))                                                                                                                     //Natural: IF #SV-SUB-LOB NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUB-LOB-TOTAL
            sub_Print_Sub_Lob_Total();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-LOB-TOTAL
        sub_Print_Lob_Total();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-LOB-SUMMARY
        sub_Print_Lob_Summary();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-END-OF-PAGE
        sub_Print_End_Of_Page();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "TOTAL RECORDS READ :",pnd_Read_Work, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                                                  //Natural: WRITE 'TOTAL RECORDS READ :' #READ-WORK ( EM = ZZZ,ZZZ,ZZ9 )
        if (Global.isEscape()) return;
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BREAK-ROUTINE
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-RATE
        //* ***************************
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUB-LOB-TOTAL
        //* ********************************
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-LOB-TOTAL
        //* ********************************
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DTL-REC
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-LOB-HEADER
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUB-LOB-HEADER
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IDENTIFY-PRINTER-ID
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-NEW-PAGE
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-SUB-LOB-TO-GRAND
        //* *****************************************
        //*  FIND WHERE IN THE ARRAY THIS SUB LOB BELONGS
        //*  #I POINTS TO SUBSCRIPT IN LOB ARRAY. IF NO MATCH FOUND, #I WILL BE`
        //*  *#LOB-CTR + 1
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-LOB-SUMMARY
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-END-OF-PAGE
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-LOB-REF
        //* ******************************
        //*  #LOB-REF-DESC (8) := 'Immediate Annuities'
        //* *#LOB-REF-DESC (13) := 'Retirement Select Annuities'
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TEST-PRINT
    }
    private void sub_Break_Routine() throws Exception                                                                                                                     //Natural: BREAK-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        short decideConditionsMet353 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #RATE-BREAK
        if (condition(pnd_Rate_Break.getBoolean()))
        {
            decideConditionsMet353++;
                                                                                                                                                                          //Natural: PERFORM PRINT-RATE
            sub_Print_Rate();
            if (condition(Global.isEscape())) {return;}
            pnd_Sv_Rate.setValue(pnd_Work_File_Pnd_Wk_Rate);                                                                                                              //Natural: ASSIGN #SV-RATE := #WK-RATE
            pnd_Sv_Actl_Amt.setValue(pnd_Work_File_Pnd_Wk_Actl_Amt);                                                                                                      //Natural: ASSIGN #SV-ACTL-AMT := #WK-ACTL-AMT
        }                                                                                                                                                                 //Natural: WHEN #SUB-LOB-BREAK
        if (condition(pnd_Sub_Lob_Break.getBoolean()))
        {
            decideConditionsMet353++;
            if (condition(pnd_Had_Sub_Lob.getBoolean() && pnd_Sv_Sub_Lob.notEquals(" ")))                                                                                 //Natural: IF #HAD-SUB-LOB AND #SV-SUB-LOB NE ' '
            {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUB-LOB-TOTAL
                sub_Print_Sub_Lob_Total();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ADD-SUB-LOB-TO-GRAND
                sub_Add_Sub_Lob_To_Grand();
                if (condition(Global.isEscape())) {return;}
                if (condition(! (pnd_Lob_Break.getBoolean())))                                                                                                            //Natural: IF NOT #LOB-BREAK
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUB-LOB-HEADER
                    sub_Print_Sub_Lob_Header();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Sv_Sub_Lob.setValue(pnd_Work_File_Pnd_Wk_Sub_Lob);                                                                                                        //Natural: ASSIGN #SV-SUB-LOB := #WK-SUB-LOB
            if (condition(pnd_Work_File_Pnd_Wk_Sub_Lob.notEquals(" ")))                                                                                                   //Natural: IF #WK-SUB-LOB NE ' '
            {
                pnd_Sv_Sub_Lob_Actl_Amt.setValue(pnd_Work_File_Pnd_Wk_Actl_Amt);                                                                                          //Natural: ASSIGN #SV-SUB-LOB-ACTL-AMT := #WK-ACTL-AMT
                pnd_Had_Sub_Lob.setValue(true);                                                                                                                           //Natural: ASSIGN #HAD-SUB-LOB := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Had_Sub_Lob.setValue(false);                                                                                                                          //Natural: ASSIGN #HAD-SUB-LOB := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #LOB-BREAK
        if (condition(pnd_Lob_Break.getBoolean()))
        {
            decideConditionsMet353++;
                                                                                                                                                                          //Natural: PERFORM PRINT-LOB-TOTAL
            sub_Print_Lob_Total();
            if (condition(Global.isEscape())) {return;}
            pnd_Sv_Lob.setValue(pnd_Work_File_Pnd_Wk_Lob);                                                                                                                //Natural: ASSIGN #SV-LOB := #WK-LOB
            pnd_Sv_Lob_Actl_Amt.setValue(pnd_Work_File_Pnd_Wk_Actl_Amt);                                                                                                  //Natural: ASSIGN #SV-LOB-ACTL-AMT := #WK-ACTL-AMT
            if (condition(! (pnd_Stts_Acct_Break.getBoolean())))                                                                                                          //Natural: IF NOT #STTS-ACCT-BREAK
            {
                                                                                                                                                                          //Natural: PERFORM PRINT-LOB-HEADER
                sub_Print_Lob_Header();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #STTS-ACCT-BREAK
        if (condition(pnd_Stts_Acct_Break.getBoolean()))
        {
            decideConditionsMet353++;
                                                                                                                                                                          //Natural: PERFORM PRINT-LOB-SUMMARY
            sub_Print_Lob_Summary();
            if (condition(Global.isEscape())) {return;}
            pnd_Sv_Stts_Acct.setValue(pnd_Work_File_Pnd_Wk_Stts_Acct);                                                                                                    //Natural: ASSIGN #SV-STTS-ACCT := #WK-STTS-ACCT
            pnd_Sv_Lst_Actvty_Dte.setValue(pnd_Work_File_Pnd_Wk_Lst_Actvty_Dte);                                                                                          //Natural: ASSIGN #SV-LST-ACTVTY-DTE := #WK-LST-ACTVTY-DTE
            pnd_Acctng_Dte_Pnd_Acctng_Mm.setValue(pnd_Work_File_Pnd_Wk_Lst_Actvty_Mm);                                                                                    //Natural: ASSIGN #ACCTNG-MM := #WK-LST-ACTVTY-MM
            pnd_Acctng_Dte_Pnd_Acctng_Dd.setValue(pnd_Work_File_Pnd_Wk_Lst_Actvty_Dd);                                                                                    //Natural: ASSIGN #ACCTNG-DD := #WK-LST-ACTVTY-DD
            pnd_Acctng_Dte_Pnd_Acctng_Yy.setValue(pnd_Work_File_Pnd_Wk_Lst_Actvty_Yy);                                                                                    //Natural: ASSIGN #ACCTNG-YY := #WK-LST-ACTVTY-YY
                                                                                                                                                                          //Natural: PERFORM IDENTIFY-PRINTER-ID
            sub_Identify_Printer_Id();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-NEW-PAGE
            sub_Print_New_Page();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-LOB-HEADER
            sub_Print_Lob_Header();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet353 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  BREAK-ROUTINE
    }
    private void sub_Print_Rate() throws Exception                                                                                                                        //Natural: PRINT-RATE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Print_Rec_Pnd_Prt_Rate.setValue(pnd_Sv_Rate);                                                                                                                 //Natural: ASSIGN #PRT-RATE := #SV-RATE
        pnd_Print_Rec_Pnd_Prt_Amt.setValueEdited(pnd_Sv_Actl_Amt,new ReportEditMask("ZZZ,ZZZ,ZZZ,ZZ9.99"));                                                               //Natural: MOVE EDITED #SV-ACTL-AMT ( EM = ZZZ,ZZZ,ZZZ,ZZ9.99 ) TO #PRT-AMT
                                                                                                                                                                          //Natural: PERFORM PRINT-DTL-REC
        sub_Print_Dtl_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  PRINT-RATE
    }
    private void sub_Print_Sub_Lob_Total() throws Exception                                                                                                               //Natural: PRINT-SUB-LOB-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Print_Rec_Pnd_Prt_Line1.setValue("----------");                                                                                                               //Natural: ASSIGN #PRT-LINE1 := '----------'
        pnd_Print_Rec_Pnd_Prt_Line2.setValue("------------------");                                                                                                       //Natural: ASSIGN #PRT-LINE2 := '------------------'
                                                                                                                                                                          //Natural: PERFORM PRINT-DTL-REC
        sub_Print_Dtl_Rec();
        if (condition(Global.isEscape())) {return;}
        pnd_Print_Rec_Pnd_Prt_Total_Text.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Sv_Sub_Lob, " SL"));                                                //Natural: COMPRESS #SV-SUB-LOB ' SL' INTO #PRT-TOTAL-TEXT LEAVING NO
        pnd_Print_Rec_Pnd_Prt_Total_Amt.setValueEdited(pnd_Sv_Sub_Lob_Actl_Amt,new ReportEditMask("ZZZ,ZZZ,ZZZ,ZZ9.99"));                                                 //Natural: MOVE EDITED #SV-SUB-LOB-ACTL-AMT ( EM = ZZZ,ZZZ,ZZZ,ZZ9.99 ) TO #PRT-TOTAL-AMT
                                                                                                                                                                          //Natural: PERFORM PRINT-DTL-REC
        sub_Print_Dtl_Rec();
        if (condition(Global.isEscape())) {return;}
        //*  PRINT-LOB-TOTAL
    }
    private void sub_Print_Lob_Total() throws Exception                                                                                                                   //Natural: PRINT-LOB-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Print_Rec_Pnd_Prt_Line1.setValue("----------");                                                                                                               //Natural: ASSIGN #PRT-LINE1 := '----------'
        pnd_Print_Rec_Pnd_Prt_Line2.setValue("------------------");                                                                                                       //Natural: ASSIGN #PRT-LINE2 := '------------------'
                                                                                                                                                                          //Natural: PERFORM PRINT-DTL-REC
        sub_Print_Dtl_Rec();
        if (condition(Global.isEscape())) {return;}
        pnd_Print_Rec_Pnd_Prt_Total_Text.setValue("TOTAL:");                                                                                                              //Natural: ASSIGN #PRT-TOTAL-TEXT := 'TOTAL:'
        pnd_Print_Rec_Pnd_Prt_Total_Amt.setValueEdited(pnd_Sv_Lob_Actl_Amt,new ReportEditMask("ZZZ,ZZZ,ZZZ,ZZ9.99"));                                                     //Natural: MOVE EDITED #SV-LOB-ACTL-AMT ( EM = ZZZ,ZZZ,ZZZ,ZZ9.99 ) TO #PRT-TOTAL-AMT
                                                                                                                                                                          //Natural: PERFORM PRINT-DTL-REC
        sub_Print_Dtl_Rec();
        if (condition(Global.isEscape())) {return;}
        pnd_Lob_Ctr.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #LOB-CTR
        pnd_Lob_Desc_Array.getValue(pnd_Lob_Ctr).setValue(pnd_Lob_Desc);                                                                                                  //Natural: ASSIGN #LOB-DESC-ARRAY ( #LOB-CTR ) := #LOB-DESC
        pnd_Lob_Amt_Array.getValue(pnd_Lob_Ctr).setValue(pnd_Sv_Lob_Actl_Amt);                                                                                            //Natural: ASSIGN #LOB-AMT-ARRAY ( #LOB-CTR ) := #SV-LOB-ACTL-AMT
        //*  PRINT-LOB-TOTAL
    }
    private void sub_Print_Dtl_Rec() throws Exception                                                                                                                     //Natural: PRINT-DTL-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        short decideConditionsMet425 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #PRINT-ID;//Natural: VALUE 1
        if (condition((pnd_Print_Id.equals(1))))
        {
            decideConditionsMet425++;
            getReports().write(1, ReportOption.NOTITLE,pnd_Print_Rec);                                                                                                    //Natural: WRITE ( 1 ) #PRINT-REC
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_Print_Id.equals(2))))
        {
            decideConditionsMet425++;
            getReports().write(2, ReportOption.NOTITLE,pnd_Print_Rec);                                                                                                    //Natural: WRITE ( 2 ) #PRINT-REC
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Print_Id.equals(3))))
        {
            decideConditionsMet425++;
            getReports().write(3, ReportOption.NOTITLE,pnd_Print_Rec);                                                                                                    //Natural: WRITE ( 3 ) #PRINT-REC
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_Print_Id.equals(4))))
        {
            decideConditionsMet425++;
            getReports().write(4, ReportOption.NOTITLE,pnd_Print_Rec);                                                                                                    //Natural: WRITE ( 4 ) #PRINT-REC
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Print_Rec.reset();                                                                                                                                            //Natural: RESET #PRINT-REC
        //*  PRINT-DTL-REC
    }
    private void sub_Print_Lob_Header() throws Exception                                                                                                                  //Natural: PRINT-LOB-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pnd_Lob_Desc.setValue(DbsUtil.compress(pnd_Work_File_Pnd_Wk_Lob, "(unknown)"));                                                                                   //Natural: COMPRESS #WK-LOB '(unknown)' INTO #LOB-DESC
        DbsUtil.examine(new ExamineSource(pnd_Lob_Ref_Cde.getValue("*")), new ExamineSearch(pnd_Work_File_Pnd_Wk_Lob), new ExamineGivingIndex(pnd_I1));                   //Natural: EXAMINE #LOB-REF-CDE ( * ) FOR #WK-LOB GIVING INDEX #I1
        if (condition(pnd_I1.greater(getZero())))                                                                                                                         //Natural: IF #I1 GT 0
        {
            pnd_Lob_Desc.setValue(pnd_Lob_Ref_Desc.getValue(pnd_I1));                                                                                                     //Natural: ASSIGN #LOB-DESC := #LOB-REF-DESC ( #I1 )
            //*  RS0
            short decideConditionsMet447 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE #WK-ACCT;//Natural: VALUE 'B'
            if (condition((pnd_Work_File_Pnd_Wk_Acct.equals("B"))))
            {
                decideConditionsMet447++;
                //*  RS0
                pnd_Lob_Desc.setValue(DbsUtil.compress(pnd_Lob_Desc, " ", pnd_Stable));                                                                                   //Natural: COMPRESS #LOB-DESC ' ' #STABLE INTO #LOB-DESC
            }                                                                                                                                                             //Natural: VALUE 'C'
            else if (condition((pnd_Work_File_Pnd_Wk_Acct.equals("C"))))
            {
                decideConditionsMet447++;
                pnd_Lob_Desc.setValue(DbsUtil.compress(pnd_Lob_Desc, " ", pnd_Sv));                                                                                       //Natural: COMPRESS #LOB-DESC ' ' #SV INTO #LOB-DESC
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, "Unknown LOB=",pnd_Work_File_Pnd_Wk_Lob);                                                                                               //Natural: WRITE 'Unknown LOB=' #WK-LOB
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet459 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #PRINT-ID;//Natural: VALUE 1
        if (condition((pnd_Print_Id.equals(1))))
        {
            decideConditionsMet459++;
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Line of Business :",new TabSetting(20),pnd_Lob_Desc,NEWLINE);                                     //Natural: WRITE ( 1 ) // 'Line of Business :' 20T #LOB-DESC /
            if (Global.isEscape()) return;
            if (condition(pnd_Had_Sub_Lob.getBoolean()))                                                                                                                  //Natural: IF #HAD-SUB-LOB
            {
                getReports().write(1, ReportOption.NOTITLE,"Sub Line of Business :",pnd_Work_File_Pnd_Wk_Sub_Lob);                                                        //Natural: WRITE ( 1 ) 'Sub Line of Business :' #WK-SUB-LOB
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"Rate Basis",new TabSetting(22),"Amount",NEWLINE,new TabSetting(1),"----------",new      //Natural: WRITE ( 1 ) / 1T 'Rate Basis' 22T 'Amount' / 1T '----------' 16T '------------------'
                TabSetting(16),"------------------");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_Print_Id.equals(2))))
        {
            decideConditionsMet459++;
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Line of Business :",new TabSetting(20),pnd_Lob_Desc,NEWLINE);                                     //Natural: WRITE ( 2 ) // 'Line of Business :' 20T #LOB-DESC /
            if (Global.isEscape()) return;
            if (condition(pnd_Had_Sub_Lob.getBoolean()))                                                                                                                  //Natural: IF #HAD-SUB-LOB
            {
                getReports().write(2, ReportOption.NOTITLE,"Sub Line of Business :",pnd_Work_File_Pnd_Wk_Sub_Lob);                                                        //Natural: WRITE ( 2 ) 'Sub Line of Business :' #WK-SUB-LOB
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new TabSetting(2),"Rate Basis",new TabSetting(22),"Amount",NEWLINE,new TabSetting(1),"----------",new      //Natural: WRITE ( 2 ) / 2T 'Rate Basis' 22T 'Amount' / 1T '----------' 16T '------------------'
                TabSetting(16),"------------------");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Print_Id.equals(3))))
        {
            decideConditionsMet459++;
            getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Line of Business :",new TabSetting(20),pnd_Lob_Desc,NEWLINE);                                     //Natural: WRITE ( 3 ) // 'Line of Business :' 20T #LOB-DESC /
            if (Global.isEscape()) return;
            if (condition(pnd_Had_Sub_Lob.getBoolean()))                                                                                                                  //Natural: IF #HAD-SUB-LOB
            {
                getReports().write(3, ReportOption.NOTITLE,"Sub Line of Business :",pnd_Work_File_Pnd_Wk_Sub_Lob);                                                        //Natural: WRITE ( 3 ) 'Sub Line of Business :' #WK-SUB-LOB
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(3, ReportOption.NOTITLE,NEWLINE,new TabSetting(3),"Rate Basis",new TabSetting(22),"Amount",NEWLINE,new TabSetting(1),"----------",new      //Natural: WRITE ( 3 ) / 3T 'Rate Basis' 22T 'Amount' / 1T '----------' 16T '------------------'
                TabSetting(16),"------------------");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_Print_Id.equals(4))))
        {
            decideConditionsMet459++;
            getReports().write(4, ReportOption.NOTITLE,NEWLINE,NEWLINE,"Line of Business :",new TabSetting(20),pnd_Lob_Desc,NEWLINE);                                     //Natural: WRITE ( 4 ) // 'Line of Business :' 20T #LOB-DESC /
            if (Global.isEscape()) return;
            if (condition(pnd_Had_Sub_Lob.getBoolean()))                                                                                                                  //Natural: IF #HAD-SUB-LOB
            {
                getReports().write(4, ReportOption.NOTITLE,"Sub Line of Business :",pnd_Work_File_Pnd_Wk_Sub_Lob);                                                        //Natural: WRITE ( 4 ) 'Sub Line of Business :' #WK-SUB-LOB
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(4, ReportOption.NOTITLE,NEWLINE,new TabSetting(1),"Rate Basis",new TabSetting(22),"Amount",NEWLINE,new TabSetting(1),"----------",new      //Natural: WRITE ( 4 ) / 1T 'Rate Basis' 22T 'Amount' / 1T '----------' 16T '------------------'
                TabSetting(16),"------------------");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Print_Rec.reset();                                                                                                                                            //Natural: RESET #PRINT-REC
        //*  PRINT-DTL-REC
    }
    private void sub_Print_Sub_Lob_Header() throws Exception                                                                                                              //Natural: PRINT-SUB-LOB-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  WRITE 'Sub LOB break for' #SV-SUB-LOB
        //* *********************************
        short decideConditionsMet494 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #PRINT-ID;//Natural: VALUE 1
        if (condition((pnd_Print_Id.equals(1))))
        {
            decideConditionsMet494++;
            getReports().write(1, ReportOption.NOTITLE,"Sub Line of Business :",pnd_Work_File_Pnd_Wk_Sub_Lob);                                                            //Natural: WRITE ( 1 ) 'Sub Line of Business :' #WK-SUB-LOB
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_Print_Id.equals(2))))
        {
            decideConditionsMet494++;
            getReports().write(2, ReportOption.NOTITLE,"Sub Line of Business :",pnd_Work_File_Pnd_Wk_Sub_Lob);                                                            //Natural: WRITE ( 2 ) 'Sub Line of Business :' #WK-SUB-LOB
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Print_Id.equals(3))))
        {
            decideConditionsMet494++;
            getReports().write(3, ReportOption.NOTITLE,"Sub Line of Business :",pnd_Work_File_Pnd_Wk_Sub_Lob);                                                            //Natural: WRITE ( 3 ) 'Sub Line of Business :' #WK-SUB-LOB
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_Print_Id.equals(4))))
        {
            decideConditionsMet494++;
            getReports().write(4, ReportOption.NOTITLE,"Sub Line of Business :",pnd_Work_File_Pnd_Wk_Sub_Lob);                                                            //Natural: WRITE ( 4 ) 'Sub Line of Business :' #WK-SUB-LOB
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Print_Rec.reset();                                                                                                                                            //Natural: RESET #PRINT-REC
        //*  PRINT-DTL-REC
    }
    private void sub_Identify_Printer_Id() throws Exception                                                                                                               //Natural: IDENTIFY-PRINTER-ID
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        //*  COMPLETE (TIAA)
        //*  COMPLETE (STABLE)  RS0
        //*  COMPLETE (CREF)
        //*  DELAY,INCMPLT,REJ (TIAA)
        //*  DELAY,INCMPLT,REJ (STABLE)  RS0
        //*  DELAY,INCMPLT,REJ (CREF)
        //*  COMPLETE (SVSAA)    DEA
        //*  DELAY,INCMPLT,REJ (STABLE VALUE) DEA
        short decideConditionsMet519 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #SV-STTS-ACCT = '1A'
        if (condition(pnd_Sv_Stts_Acct.equals("1A")))
        {
            decideConditionsMet519++;
            pnd_Print_Id.setValue(1);                                                                                                                                     //Natural: ASSIGN #PRINT-ID := 1
        }                                                                                                                                                                 //Natural: WHEN #SV-STTS-ACCT = '1B'
        else if (condition(pnd_Sv_Stts_Acct.equals("1B")))
        {
            decideConditionsMet519++;
            pnd_Print_Id.setValue(1);                                                                                                                                     //Natural: ASSIGN #PRINT-ID := 1
        }                                                                                                                                                                 //Natural: WHEN #SV-STTS-ACCT = '1D'
        else if (condition(pnd_Sv_Stts_Acct.equals("1D")))
        {
            decideConditionsMet519++;
            pnd_Print_Id.setValue(2);                                                                                                                                     //Natural: ASSIGN #PRINT-ID := 2
        }                                                                                                                                                                 //Natural: WHEN #SV-STTS-ACCT = '2A'
        else if (condition(pnd_Sv_Stts_Acct.equals("2A")))
        {
            decideConditionsMet519++;
            pnd_Print_Id.setValue(3);                                                                                                                                     //Natural: ASSIGN #PRINT-ID := 3
        }                                                                                                                                                                 //Natural: WHEN #SV-STTS-ACCT = '2B'
        else if (condition(pnd_Sv_Stts_Acct.equals("2B")))
        {
            decideConditionsMet519++;
            pnd_Print_Id.setValue(3);                                                                                                                                     //Natural: ASSIGN #PRINT-ID := 3
        }                                                                                                                                                                 //Natural: WHEN #SV-STTS-ACCT = '2D'
        else if (condition(pnd_Sv_Stts_Acct.equals("2D")))
        {
            decideConditionsMet519++;
            pnd_Print_Id.setValue(4);                                                                                                                                     //Natural: ASSIGN #PRINT-ID := 4
        }                                                                                                                                                                 //Natural: WHEN #SV-STTS-ACCT = '1C'
        else if (condition(pnd_Sv_Stts_Acct.equals("1C")))
        {
            decideConditionsMet519++;
            pnd_Print_Id.setValue(1);                                                                                                                                     //Natural: ASSIGN #PRINT-ID := 1
        }                                                                                                                                                                 //Natural: WHEN #SV-STTS-ACCT = '2C'
        else if (condition(pnd_Sv_Stts_Acct.equals("2C")))
        {
            decideConditionsMet519++;
            pnd_Print_Id.setValue(3);                                                                                                                                     //Natural: ASSIGN #PRINT-ID := 3
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  IDENTIFY-PRINTER-ID
    }
    private void sub_Print_New_Page() throws Exception                                                                                                                    //Natural: PRINT-NEW-PAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        short decideConditionsMet543 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #PRINT-ID;//Natural: VALUE 1
        if (condition((pnd_Print_Id.equals(1))))
        {
            decideConditionsMet543++;
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_Print_Id.equals(2))))
        {
            decideConditionsMet543++;
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 2 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Print_Id.equals(3))))
        {
            decideConditionsMet543++;
            getReports().newPage(new ReportSpecification(3));                                                                                                             //Natural: NEWPAGE ( 3 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_Print_Id.equals(4))))
        {
            decideConditionsMet543++;
            getReports().newPage(new ReportSpecification(4));                                                                                                             //Natural: NEWPAGE ( 4 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  PRINT-NEW-PAGE
    }
    private void sub_Add_Sub_Lob_To_Grand() throws Exception                                                                                                              //Natural: ADD-SUB-LOB-TO-GRAND
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Lob_Desc.setValue("Unknown");                                                                                                                                 //Natural: ASSIGN #LOB-DESC := 'Unknown'
        DbsUtil.examine(new ExamineSource(pnd_Lob_Ref_Cde.getValue("*")), new ExamineSearch(pnd_Sv_Lob), new ExamineGivingIndex(pnd_I));                                  //Natural: EXAMINE #LOB-REF-CDE ( * ) FOR #SV-LOB GIVING INDEX #I
        if (condition(pnd_I.greater(getZero())))                                                                                                                          //Natural: IF #I GT 0
        {
            pnd_Lob_Desc.setValue(pnd_Lob_Ref_Desc.getValue(pnd_I1));                                                                                                     //Natural: ASSIGN #LOB-DESC := #LOB-REF-DESC ( #I1 )
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #S = 1 TO 10
        for (pnd_S.setValue(1); condition(pnd_S.lessOrEqual(10)); pnd_S.nadd(1))
        {
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Print_Lob_Summary() throws Exception                                                                                                                 //Natural: PRINT-LOB-SUMMARY
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        pnd_Grand_Lob_Total.reset();                                                                                                                                      //Natural: RESET #GRAND-LOB-TOTAL
                                                                                                                                                                          //Natural: PERFORM PRINT-NEW-PAGE
        sub_Print_New_Page();
        if (condition(Global.isEscape())) {return;}
        pnd_Print_Rec_Pnd_Prt_Lob_Hdr1.setValue("Line of Business");                                                                                                      //Natural: ASSIGN #PRT-LOB-HDR1 := 'Line of Business'
        pnd_Print_Rec_Pnd_Prt_Lob_Hdr2.setValue("Amount");                                                                                                                //Natural: ASSIGN #PRT-LOB-HDR2 := 'Amount'
                                                                                                                                                                          //Natural: PERFORM PRINT-DTL-REC
        sub_Print_Dtl_Rec();
        if (condition(Global.isEscape())) {return;}
        pnd_Print_Rec_Pnd_Prt_Lob_Line1.setValue("--------------------------------------------------");                                                                   //Natural: ASSIGN #PRT-LOB-LINE1 := '--------------------------------------------------'
        pnd_Print_Rec_Pnd_Prt_Lob_Line2.setValue("------------------");                                                                                                   //Natural: ASSIGN #PRT-LOB-LINE2 := '------------------'
                                                                                                                                                                          //Natural: PERFORM PRINT-DTL-REC
        sub_Print_Dtl_Rec();
        if (condition(Global.isEscape())) {return;}
        FOR02:                                                                                                                                                            //Natural: FOR #I2 1 TO #LOB-CTR
        for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(pnd_Lob_Ctr)); pnd_I2.nadd(1))
        {
            pnd_Print_Rec_Pnd_Prt_Lob_Desc.setValue(pnd_Lob_Desc_Array.getValue(pnd_I2).getSubstring(1,50));                                                              //Natural: ASSIGN #PRT-LOB-DESC := SUBSTR ( #LOB-DESC-ARRAY ( #I2 ) ,1,50 )
            pnd_Print_Rec_Pnd_Prt_Lob_Amt.setValueEdited(pnd_Lob_Amt_Array.getValue(pnd_I2),new ReportEditMask("ZZZ,ZZZ,ZZZ,ZZ9.99"));                                    //Natural: MOVE EDITED #LOB-AMT-ARRAY ( #I2 ) ( EM = ZZZ,ZZZ,ZZZ,ZZ9.99 ) TO #PRT-LOB-AMT
            //*  RS0
            if (condition(pnd_Sv_Stts_Acct_Pnd_Sv_Fund.equals("B")))                                                                                                      //Natural: IF #SV-FUND = 'B'
            {
                DbsUtil.examine(new ExamineSource(pnd_Print_Rec_Pnd_Prt_Lob_Desc), new ExamineSearch(pnd_Stable), new ExamineGivingNumber(pnd_A));                        //Natural: EXAMINE #PRT-LOB-DESC FOR #STABLE GIVING NUMBER #A
                if (condition(pnd_A.equals(getZero())))                                                                                                                   //Natural: IF #A = 0
                {
                    pnd_Print_Rec_Pnd_Prt_Lob_Desc.setValue(DbsUtil.compress(pnd_Print_Rec_Pnd_Prt_Lob_Desc, " ", pnd_Stable));                                           //Natural: COMPRESS #PRT-LOB-DESC ' ' #STABLE INTO #PRT-LOB-DESC
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Sv_Stts_Acct_Pnd_Sv_Fund.equals("C")))                                                                                                  //Natural: IF #SV-FUND = 'C'
                {
                    DbsUtil.examine(new ExamineSource(pnd_Print_Rec_Pnd_Prt_Lob_Desc), new ExamineSearch(pnd_Sv), new ExamineGivingNumber(pnd_A));                        //Natural: EXAMINE #PRT-LOB-DESC FOR #SV GIVING NUMBER #A
                    if (condition(pnd_A.equals(getZero())))                                                                                                               //Natural: IF #A = 0
                    {
                        pnd_Print_Rec_Pnd_Prt_Lob_Desc.setValue(DbsUtil.compress(pnd_Print_Rec_Pnd_Prt_Lob_Desc, " ", pnd_Sv));                                           //Natural: COMPRESS #PRT-LOB-DESC ' ' #SV INTO #PRT-LOB-DESC
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  REMOVE STABLE IDENTIFIER FROM GRAND TOTAL DESCRIPTION
                    DbsUtil.examine(new ExamineSource(pnd_Print_Rec_Pnd_Prt_Lob_Desc), new ExamineSearch(pnd_Stable), new ExamineReplace(" "));                           //Natural: EXAMINE #PRT-LOB-DESC FOR #STABLE REPLACE ' '
                    DbsUtil.examine(new ExamineSource(pnd_Print_Rec_Pnd_Prt_Lob_Desc), new ExamineSearch(pnd_Sv), new ExamineReplace(" "));                               //Natural: EXAMINE #PRT-LOB-DESC FOR #SV REPLACE ' '
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-DTL-REC
            sub_Print_Dtl_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Grand_Lob_Total.nadd(pnd_Lob_Amt_Array.getValue(pnd_I2));                                                                                                 //Natural: ADD #LOB-AMT-ARRAY ( #I2 ) TO #GRAND-LOB-TOTAL
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Print_Rec_Pnd_Prt_Lob_Line1.setValue("--------------------------------------------------");                                                                   //Natural: ASSIGN #PRT-LOB-LINE1 := '--------------------------------------------------'
        pnd_Print_Rec_Pnd_Prt_Lob_Line2.setValue("------------------");                                                                                                   //Natural: ASSIGN #PRT-LOB-LINE2 := '------------------'
                                                                                                                                                                          //Natural: PERFORM PRINT-DTL-REC
        sub_Print_Dtl_Rec();
        if (condition(Global.isEscape())) {return;}
        pnd_Print_Rec_Pnd_Prt_Lob_Desc.setValue("Grand Total");                                                                                                           //Natural: ASSIGN #PRT-LOB-DESC := 'Grand Total'
        pnd_Print_Rec_Pnd_Prt_Lob_Amt.setValueEdited(pnd_Grand_Lob_Total,new ReportEditMask("ZZZ,ZZZ,ZZZ,ZZ9.99"));                                                       //Natural: MOVE EDITED #GRAND-LOB-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZ9.99 ) TO #PRT-LOB-AMT
                                                                                                                                                                          //Natural: PERFORM PRINT-DTL-REC
        sub_Print_Dtl_Rec();
        if (condition(Global.isEscape())) {return;}
        pnd_Lob_Ctr.reset();                                                                                                                                              //Natural: RESET #LOB-CTR #LOB-DESC-ARRAY ( * ) #LOB-AMT-ARRAY ( * )
        pnd_Lob_Desc_Array.getValue("*").reset();
        pnd_Lob_Amt_Array.getValue("*").reset();
        //*  PRINT-LOB-SUMMARY
    }
    private void sub_Print_End_Of_Page() throws Exception                                                                                                                 //Natural: PRINT-END-OF-PAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(26),"--- End of Report ---");                                                   //Natural: WRITE ( 1 ) /// 26T '--- End of Report ---'
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(26),"--- End of Report ---");                                                   //Natural: WRITE ( 2 ) /// 26T '--- End of Report ---'
        if (Global.isEscape()) return;
        getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(26),"--- End of Report ---");                                                   //Natural: WRITE ( 3 ) /// 26T '--- End of Report ---'
        if (Global.isEscape()) return;
        getReports().write(4, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(26),"--- End of Report ---");                                                   //Natural: WRITE ( 4 ) /// 26T '--- End of Report ---'
        if (Global.isEscape()) return;
        //*  PRINT-END-OF-PAGE
    }
    //*  101311
    private void sub_Setup_Lob_Ref() throws Exception                                                                                                                     //Natural: SETUP-LOB-REF
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Lob_Ref_Cde.getValue(1).setValue("RA");                                                                                                                       //Natural: ASSIGN #LOB-REF-CDE ( 1 ) := 'RA'
        pnd_Lob_Ref_Desc.getValue(1).setValue("Retirement Annuities");                                                                                                    //Natural: ASSIGN #LOB-REF-DESC ( 1 ) := 'Retirement Annuities'
        pnd_Lob_Ref_Cde.getValue(2).setValue("SRA");                                                                                                                      //Natural: ASSIGN #LOB-REF-CDE ( 2 ) := 'SRA'
        pnd_Lob_Ref_Desc.getValue(2).setValue("Supplemental Retirement Annuities");                                                                                       //Natural: ASSIGN #LOB-REF-DESC ( 2 ) := 'Supplemental Retirement Annuities'
        pnd_Lob_Ref_Cde.getValue(3).setValue("GSRA");                                                                                                                     //Natural: ASSIGN #LOB-REF-CDE ( 3 ) := 'GSRA'
        pnd_Lob_Ref_Desc.getValue(3).setValue("Group Supplemental Retirement Annuities");                                                                                 //Natural: ASSIGN #LOB-REF-DESC ( 3 ) := 'Group Supplemental Retirement Annuities'
        pnd_Lob_Ref_Cde.getValue(4).setValue("SSRA");                                                                                                                     //Natural: ASSIGN #LOB-REF-CDE ( 4 ) := 'SSRA'
        pnd_Lob_Ref_Desc.getValue(4).setValue("Special Service Retirement Annuities");                                                                                    //Natural: ASSIGN #LOB-REF-DESC ( 4 ) := 'Special Service Retirement Annuities'
        pnd_Lob_Ref_Cde.getValue(5).setValue("IRA");                                                                                                                      //Natural: ASSIGN #LOB-REF-CDE ( 5 ) := 'IRA'
        pnd_Lob_Ref_Desc.getValue(5).setValue("Individual Retirement Annuities");                                                                                         //Natural: ASSIGN #LOB-REF-DESC ( 5 ) := 'Individual Retirement Annuities'
        pnd_Lob_Ref_Cde.getValue(6).setValue("KEOGH");                                                                                                                    //Natural: ASSIGN #LOB-REF-CDE ( 6 ) := 'KEOGH'
        pnd_Lob_Ref_Desc.getValue(6).setValue("Keogh Annuities");                                                                                                         //Natural: ASSIGN #LOB-REF-DESC ( 6 ) := 'Keogh Annuities'
        pnd_Lob_Ref_Cde.getValue(7).setValue("RL");                                                                                                                       //Natural: ASSIGN #LOB-REF-CDE ( 7 ) := 'RL'
        pnd_Lob_Ref_Desc.getValue(7).setValue("Retirement Loan Certificates");                                                                                            //Natural: ASSIGN #LOB-REF-DESC ( 7 ) := 'Retirement Loan Certificates'
        pnd_Lob_Ref_Cde.getValue(8).setValue("IPRO");                                                                                                                     //Natural: ASSIGN #LOB-REF-CDE ( 8 ) := 'IPRO'
        pnd_Lob_Ref_Desc.getValue(8).setValue("IPRO");                                                                                                                    //Natural: ASSIGN #LOB-REF-DESC ( 8 ) := 'IPRO'
        pnd_Lob_Ref_Cde.getValue(9).setValue("SA");                                                                                                                       //Natural: ASSIGN #LOB-REF-CDE ( 9 ) := 'SA'
        pnd_Lob_Ref_Desc.getValue(9).setValue("Retirement Select Annuities");                                                                                             //Natural: ASSIGN #LOB-REF-DESC ( 9 ) := 'Retirement Select Annuities'
        pnd_Lob_Ref_Cde.getValue(10).setValue("GRA");                                                                                                                     //Natural: ASSIGN #LOB-REF-CDE ( 10 ) := 'GRA'
        pnd_Lob_Ref_Desc.getValue(10).setValue("Group Retirement Annuities");                                                                                             //Natural: ASSIGN #LOB-REF-DESC ( 10 ) := 'Group Retirement Annuities'
        pnd_Lob_Ref_Cde.getValue(11).setValue("TPA");                                                                                                                     //Natural: ASSIGN #LOB-REF-CDE ( 11 ) := 'TPA'
        pnd_Lob_Ref_Desc.getValue(11).setValue("Transfer Payout Annuities");                                                                                              //Natural: ASSIGN #LOB-REF-DESC ( 11 ) := 'Transfer Payout Annuities'
        pnd_Lob_Ref_Cde.getValue(12).setValue("RS");                                                                                                                      //Natural: ASSIGN #LOB-REF-CDE ( 12 ) := 'RS'
        pnd_Lob_Ref_Desc.getValue(12).setValue("Retirement Choice Annuities");                                                                                            //Natural: ASSIGN #LOB-REF-DESC ( 12 ) := 'Retirement Choice Annuities'
        pnd_Lob_Ref_Cde.getValue(13).setValue("RP");                                                                                                                      //Natural: ASSIGN #LOB-REF-CDE ( 13 ) := 'RP'
        pnd_Lob_Ref_Desc.getValue(13).setValue("Retirement Choice Plus Annuities");                                                                                       //Natural: ASSIGN #LOB-REF-DESC ( 13 ) := 'Retirement Choice Plus Annuities'
        pnd_Lob_Ref_Cde.getValue(14).setValue("IRAIDX");                                                                                                                  //Natural: ASSIGN #LOB-REF-CDE ( 14 ) := 'IRAIDX'
        pnd_Lob_Ref_Desc.getValue(14).setValue("Individual Retirement Annuity - Indexed");                                                                                //Natural: ASSIGN #LOB-REF-DESC ( 14 ) := 'Individual Retirement Annuity - Indexed'
        pnd_Lob_Ref_Cde.getValue(15).setValue("GA");                                                                                                                      //Natural: ASSIGN #LOB-REF-CDE ( 15 ) := 'GA'
        pnd_Lob_Ref_Desc.getValue(15).setValue("Group Annuities");                                                                                                        //Natural: ASSIGN #LOB-REF-DESC ( 15 ) := 'Group Annuities'
        //*  SETUP-LOB-REF
    }
    private void sub_Test_Print() throws Exception                                                                                                                        //Natural: TEST-PRINT
    {
        if (BLNatReinput.isReinput()) return;

        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        if (condition(pnd_Read_Work.less(100)))                                                                                                                           //Natural: IF #READ-WORK < 100
        {
            getReports().write(0, "=",pnd_Work_File_Pnd_Wk_Lob,"=",pnd_Work_File_Pnd_Wk_Sub_Lob,"=",pnd_Work_File_Pnd_Wk_Sub_Seq,"=",pnd_Work_File_Pnd_Wk_Actl_Amt);      //Natural: WRITE '=' #WK-LOB '=' #WK-SUB-LOB '=' #WK-SUB-SEQ '=' #WK-ACTL-AMT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(62),"Run Date:",Global.getDATX(),                     //Natural: WRITE ( 1 ) NOTITLE 1T *PROGRAM 62T 'Run Date:' *DATX ( EM = MM/DD/YY ) / 20T 'Annuitization of OmniPlus Contracts' 62T 'Run Time:' *TIMX ( EM = HH:MM:SS ) / 10T #HEADING-REPORT-TIAA 62T 'Page: ' *PAGE-NUMBER ( 1 ) ( EM = ZZZ ) / 26T '(Complete Transactions)' / 25T 'Accounting Date' #ACCTNG-DTE ( EM = 99/99/9999 ) //
                        new ReportEditMask ("MM/DD/YY"),NEWLINE,new TabSetting(20),"Annuitization of OmniPlus Contracts",new TabSetting(62),"Run Time:",Global.getTIMX(), 
                        new ReportEditMask ("HH:MM:SS"),NEWLINE,new TabSetting(10),pnd_Heading_Report_Tiaa,new TabSetting(62),"Page: ",getReports().getPageNumberDbs(1), 
                        new ReportEditMask ("ZZZ"),NEWLINE,new TabSetting(26),"(Complete Transactions)",NEWLINE,new TabSetting(25),"Accounting Date",pnd_Acctng_Dte, 
                        new ReportEditMask ("99/99/9999"),NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(62),"Run Date:",Global.getDATX(),                     //Natural: WRITE ( 2 ) NOTITLE 1T *PROGRAM 62T 'Run Date:' *DATX ( EM = MM/DD/YY ) / 20T 'Annuitization of OmniPlus Contracts' 62T 'Run Time:' *TIMX ( EM = HH:MM:SS ) / 10T #HEADING-REPORT-CREF 62T 'Page: ' *PAGE-NUMBER ( 2 ) ( EM = ZZZ ) / 26T '(Complete Transactions)' / 25T 'Accounting Date' #ACCTNG-DTE ( EM = 99/99/9999 ) //
                        new ReportEditMask ("MM/DD/YY"),NEWLINE,new TabSetting(20),"Annuitization of OmniPlus Contracts",new TabSetting(62),"Run Time:",Global.getTIMX(), 
                        new ReportEditMask ("HH:MM:SS"),NEWLINE,new TabSetting(10),pnd_Heading_Report_Cref,new TabSetting(62),"Page: ",getReports().getPageNumberDbs(2), 
                        new ReportEditMask ("ZZZ"),NEWLINE,new TabSetting(26),"(Complete Transactions)",NEWLINE,new TabSetting(25),"Accounting Date",pnd_Acctng_Dte, 
                        new ReportEditMask ("99/99/9999"),NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(62),"Run Date:",Global.getDATX(),                     //Natural: WRITE ( 3 ) NOTITLE 1T *PROGRAM 62T 'Run Date:' *DATX ( EM = MM/DD/YY ) / 20T 'Annuitization of OmniPlus Contracts' 62T 'Run Time:' *TIMX ( EM = HH:MM:SS ) / 10T #HEADING-REPORT-TIAA 62T 'Page: ' *PAGE-NUMBER ( 3 ) ( EM = ZZZ ) / 15T '(Delayed, Incomplete and Rejected Transactions)' / 25T 'Accounting Date' #ACCTNG-DTE ( EM = 99/99/9999 ) //
                        new ReportEditMask ("MM/DD/YY"),NEWLINE,new TabSetting(20),"Annuitization of OmniPlus Contracts",new TabSetting(62),"Run Time:",Global.getTIMX(), 
                        new ReportEditMask ("HH:MM:SS"),NEWLINE,new TabSetting(10),pnd_Heading_Report_Tiaa,new TabSetting(62),"Page: ",getReports().getPageNumberDbs(3), 
                        new ReportEditMask ("ZZZ"),NEWLINE,new TabSetting(15),"(Delayed, Incomplete and Rejected Transactions)",NEWLINE,new TabSetting(25),"Accounting Date",pnd_Acctng_Dte, 
                        new ReportEditMask ("99/99/9999"),NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(4, ReportOption.NOTITLE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(62),"Run Date:",Global.getDATX(),                     //Natural: WRITE ( 4 ) NOTITLE 1T *PROGRAM 62T 'Run Date:' *DATX ( EM = MM/DD/YY ) / 20T 'Annuitization of OmniPlus Contracts' 62T 'Run Time:' *TIMX ( EM = HH:MM:SS ) / 10T #HEADING-REPORT-CREF 62T 'Page: ' *PAGE-NUMBER ( 4 ) ( EM = ZZZ ) / 15T '(Delayed, Incomplete and Rejected Transactions)' / 25T 'Accounting Date' #ACCTNG-DTE ( EM = 99/99/9999 ) //
                        new ReportEditMask ("MM/DD/YY"),NEWLINE,new TabSetting(20),"Annuitization of OmniPlus Contracts",new TabSetting(62),"Run Time:",Global.getTIMX(), 
                        new ReportEditMask ("HH:MM:SS"),NEWLINE,new TabSetting(10),pnd_Heading_Report_Cref,new TabSetting(62),"Page: ",getReports().getPageNumberDbs(4), 
                        new ReportEditMask ("ZZZ"),NEWLINE,new TabSetting(15),"(Delayed, Incomplete and Rejected Transactions)",NEWLINE,new TabSetting(25),"Accounting Date",pnd_Acctng_Dte, 
                        new ReportEditMask ("99/99/9999"),NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=100");
        Global.format(1, "PS=60 LS=100");
        Global.format(2, "PS=60 LS=100");
        Global.format(3, "PS=60 LS=100");
        Global.format(4, "PS=60 LS=100");
    }
}
