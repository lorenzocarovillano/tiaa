/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:04:30 PM
**        * FROM NATURAL PROGRAM : Adsp730
************************************************************
**        * FILE NAME            : Adsp730.java
**        * CLASS NAME           : Adsp730
**        * INSTANCE NAME        : Adsp730
************************************************************
************************************************************************
* PROGRAM  : ADSP730
* GENERATED: APRIL 13, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : THIS MODULE EXTRACTS ALL THE ANNUITIZATION TRANSACTIONS
*            FROM THE KDO FILE FOR A GIVEN DATE.
*
* REMARKS  : CLONED FROM NAZP730 (ADAM SYSTEM)
************************************************************************
* MOD DATE   MOD BY      DESCRIPTION OF CHANGES
* 01/27/99   ZAFAR KHAN  NAP-BPS-UNIT EXTENDED FROM 6 TO 8 BYTES
* 01/02/01   C SCHNEIDER ADDED OPTION CODE TO WORK FILE TO SUPPORT
*                        CHANGES TO REPORT IN P1060AND-R02 (NAZP1000)
* 02/06/04   O SOTTO     RECOMPILED TO GET UPDATED NAZLDARS.
* 04/12/04   C AVE       MODIFIED FOR ADAS (ANNUITIZATION SUNGUARD)
* 07/27/04   T. SHINE       IF MONTH-END PROCESS FALLS ON WEEKEND OR
*                           HOLIDAY, USE ADS-RPT-13 (FACTOR DATE) AS
*                           REPORT DATE.
* 03/17/2008 E.MELNIK   ROTH 403B/401K CHANGES.  RECOMPILED FOR
*                       NEW ADSL401A/ADSL401.
* 05/07/2010 C. MASON   RE-STOW FOR CHANGES TO  ADSL401A/ADSL401.
* 03/05/12   E. MELNIK RATE BASE EXPANSION PROJECT. RESTOW TO INCLUDE
*                      UPDATED LINKAGE AREAS.
* 01/18/13   E. MELNIK TNG PROJECT.  RESTOW TO INCLUDE UPDATED LINKAGE
*                      AREAS.
* 10/15/13  E. MELNIK RECOMPILED FOR NEW ADSL401A.  CREF/REA REDESIGN
*                     CHANGES.
* 02/27/2017 R.CARREON RESTOWED FOR PIN EXPANSION
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp730 extends BLNatBase
{
    // Data Areas
    private LdaAdsl401a ldaAdsl401a;
    private LdaAdsl401 ldaAdsl401;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ads_Cntl_View;

    private DbsGroup ads_Cntl_View_Ads_Cntl_Grp;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte;

    private DbsGroup ads_Cntl_View__R_Field_1;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A;
    private DbsField ads_Cntl_View_Ads_Rpt_13;
    private DbsField pnd_Cnvrt_Annty_Optn;
    private DbsField pnd_Start_Date;

    private DbsGroup pnd_Start_Date__R_Field_2;
    private DbsField pnd_Start_Date_Pnd_Start_Date_A;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_3;
    private DbsField pnd_Date_Pnd_Date_N;
    private DbsField pnd_Record_Count;
    private DbsField pnd_Last_Activity_Date;

    private DbsGroup pnd_Last_Activity_Date__R_Field_4;
    private DbsField pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N;
    private DbsField pnd_Key_Cntl_Bsnss_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl401a = new LdaAdsl401a();
        registerRecord(ldaAdsl401a);
        registerRecord(ldaAdsl401a.getVw_ads_Cntrct_View());
        ldaAdsl401 = new LdaAdsl401();
        registerRecord(ldaAdsl401);
        registerRecord(ldaAdsl401.getVw_ads_Prtcpnt_View());

        // Local Variables
        localVariables = new DbsRecord();

        vw_ads_Cntl_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntl_View", "ADS-CNTL-VIEW"), "ADS_CNTL", "ADS_CNTL");

        ads_Cntl_View_Ads_Cntl_Grp = vw_ads_Cntl_View.getRecord().newGroupInGroup("ADS_CNTL_VIEW_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_DTE");

        ads_Cntl_View__R_Field_1 = ads_Cntl_View_Ads_Cntl_Grp.newGroupInGroup("ads_Cntl_View__R_Field_1", "REDEFINE", ads_Cntl_View_Ads_Cntl_Bsnss_Dte);
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A = ads_Cntl_View__R_Field_1.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A", "ADS-CNTL-BSNSS-DTE-A", FieldType.STRING, 
            8);
        ads_Cntl_View_Ads_Rpt_13 = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Rpt_13", "ADS-RPT-13", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADS_RPT_13");
        registerRecord(vw_ads_Cntl_View);

        pnd_Cnvrt_Annty_Optn = localVariables.newFieldInRecord("pnd_Cnvrt_Annty_Optn", "#CNVRT-ANNTY-OPTN", FieldType.STRING, 1);
        pnd_Start_Date = localVariables.newFieldInRecord("pnd_Start_Date", "#START-DATE", FieldType.NUMERIC, 8);

        pnd_Start_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Start_Date__R_Field_2", "REDEFINE", pnd_Start_Date);
        pnd_Start_Date_Pnd_Start_Date_A = pnd_Start_Date__R_Field_2.newFieldInGroup("pnd_Start_Date_Pnd_Start_Date_A", "#START-DATE-A", FieldType.STRING, 
            8);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 8);

        pnd_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Date__R_Field_3", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_N = pnd_Date__R_Field_3.newFieldInGroup("pnd_Date_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        pnd_Record_Count = localVariables.newFieldInRecord("pnd_Record_Count", "#RECORD-COUNT", FieldType.PACKED_DECIMAL, 8);
        pnd_Last_Activity_Date = localVariables.newFieldInRecord("pnd_Last_Activity_Date", "#LAST-ACTIVITY-DATE", FieldType.STRING, 8);

        pnd_Last_Activity_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Last_Activity_Date__R_Field_4", "REDEFINE", pnd_Last_Activity_Date);
        pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N = pnd_Last_Activity_Date__R_Field_4.newFieldInGroup("pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N", 
            "#LAST-ACTIVITY-DATE-N", FieldType.NUMERIC, 8);
        pnd_Key_Cntl_Bsnss_Dte = localVariables.newFieldInRecord("pnd_Key_Cntl_Bsnss_Dte", "#KEY-CNTL-BSNSS-DTE", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Cntl_View.reset();

        ldaAdsl401a.initializeValues();
        ldaAdsl401.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp730() throws Exception
    {
        super("Adsp730");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        vw_ads_Cntl_View.startDatabaseRead                                                                                                                                //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM 'F'
        (
        "READ01",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", "F", WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        2
        );
        READ01:
        while (condition(vw_ads_Cntl_View.readNextRow("READ01")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                                     //Natural: IF ADS-RPT-13 NE 0
        {
            pnd_Date.setValueEdited(ads_Cntl_View_Ads_Rpt_13,new ReportEditMask("YYYYMMDD"));                                                                             //Natural: MOVE EDITED ADS-RPT-13 ( EM = YYYYMMDD ) TO #DATE
            pnd_Start_Date.compute(new ComputeParameters(false, pnd_Start_Date), (pnd_Date_Pnd_Date_N.divide(100)).multiply(100));                                        //Natural: COMPUTE #START-DATE = ( #DATE-N / 100 ) * 100
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Start_Date.compute(new ComputeParameters(false, pnd_Start_Date), (ads_Cntl_View_Ads_Cntl_Bsnss_Dte.divide(100)).multiply(100));                           //Natural: COMPUTE #START-DATE = ( ADS-CNTL-BSNSS-DTE / 100 ) * 100
        }                                                                                                                                                                 //Natural: END-IF
        //*  #START-DATE := 20081022            /* RS0
        //*  ADS-CNTL-BSNSS-DTE := 20081026
        getReports().write(0, "START DATE -",pnd_Start_Date);                                                                                                             //Natural: WRITE 'START DATE -' #START-DATE
        if (Global.isEscape()) return;
        getReports().write(0, "END DATE   -",ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                                           //Natural: WRITE 'END DATE   -' ADS-CNTL-BSNSS-DTE
        if (Global.isEscape()) return;
        if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                                     //Natural: IF ADS-RPT-13 NE 0
        {
            pnd_Key_Cntl_Bsnss_Dte.setValue(ads_Cntl_View_Ads_Rpt_13);                                                                                                    //Natural: MOVE ADS-RPT-13 TO #KEY-CNTL-BSNSS-DTE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Key_Cntl_Bsnss_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A);                                                     //Natural: MOVE EDITED ADS-CNTL-BSNSS-DTE-A TO #KEY-CNTL-BSNSS-DTE ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, "=",pnd_Key_Cntl_Bsnss_Dte);                                                                                                                //Natural: WRITE '=' #KEY-CNTL-BSNSS-DTE
        if (Global.isEscape()) return;
        ldaAdsl401.getVw_ads_Prtcpnt_View().startDatabaseRead                                                                                                             //Natural: READ ADS-PRTCPNT-VIEW BY ADP-LST-ACTVTY-DTE STARTING FROM #KEY-CNTL-BSNSS-DTE
        (
        "PND_PND_L0890",
        new Wc[] { new Wc("ADP_LST_ACTVTY_DTE", ">=", pnd_Key_Cntl_Bsnss_Dte, WcType.BY) },
        new Oc[] { new Oc("ADP_LST_ACTVTY_DTE", "ASC") }
        );
        PND_PND_L0890:
        while (condition(ldaAdsl401.getVw_ads_Prtcpnt_View().readNextRow("PND_PND_L0890")))
        {
            pnd_Last_Activity_Date.setValueEdited(ldaAdsl401.getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte(),new ReportEditMask("YYYYMMDD"));                                    //Natural: MOVE EDITED ADP-LST-ACTVTY-DTE ( EM = YYYYMMDD ) TO #LAST-ACTIVITY-DATE
            getReports().write(0, "=",pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N);                                                                                   //Natural: WRITE '=' #LAST-ACTIVITY-DATE-N
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L0890"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0890"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",ads_Cntl_View_Ads_Cntl_Bsnss_Dte);                                                                                                  //Natural: WRITE '=' ADS-CNTL-BSNSS-DTE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L0890"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0890"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",ads_Cntl_View_Ads_Rpt_13);                                                                                                          //Natural: WRITE '=' ADS-RPT-13
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L0890"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0890"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde());                                                                                     //Natural: WRITE '=' ADP-STTS-CDE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L0890"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0890"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",ldaAdsl401.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde());                                                                                 //Natural: WRITE '=' ADP-ANNT-TYP-CDE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L0890"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0890"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ads_Cntl_View_Ads_Rpt_13.notEquals(getZero())))                                                                                                 //Natural: IF ADS-RPT-13 NE 0
            {
                if (condition(pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N.greater(pnd_Date_Pnd_Date_N)))                                                              //Natural: IF #LAST-ACTIVITY-DATE-N > #DATE-N
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Last_Activity_Date_Pnd_Last_Activity_Date_N.greater(ads_Cntl_View_Ads_Cntl_Bsnss_Dte)))                                                 //Natural: IF #LAST-ACTIVITY-DATE-N > ADS-CNTL-BSNSS-DTE
                {
                    if (true) break PND_PND_L0890;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L0890. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  COMPLETE
            if (condition(!(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,1).equals("T") && ldaAdsl401.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde().equals("R")))) //Natural: ACCEPT IF SUBSTR ( ADP-STTS-CDE,1,1 ) = 'T' AND ADP-ANNT-TYP-CDE EQ 'R'
            {
                continue;
            }
            ldaAdsl401a.getVw_ads_Cntrct_View().startDatabaseFind                                                                                                         //Natural: FIND ADS-CNTRCT-VIEW WITH ADS-CNTRCT-VIEW.RQST-ID = ADS-PRTCPNT-VIEW.RQST-ID
            (
            "FIND01",
            new Wc[] { new Wc("RQST_ID", "=", ldaAdsl401.getAds_Prtcpnt_View_Rqst_Id(), WcType.WITH) }
            );
            FIND01:
            while (condition(ldaAdsl401a.getVw_ads_Cntrct_View().readNextRow("FIND01")))
            {
                ldaAdsl401a.getVw_ads_Cntrct_View().setIfNotFoundControlFlag(false);
                pnd_Record_Count.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #RECORD-COUNT
                short decideConditionsMet468 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SUBSTRING ( ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN,1,1 ) = 'T'
                if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Optn().getSubstring(1,1).equals("T")))
                {
                    decideConditionsMet468++;
                    pnd_Cnvrt_Annty_Optn.setValue("1");                                                                                                                   //Natural: MOVE '1' TO #CNVRT-ANNTY-OPTN
                }                                                                                                                                                         //Natural: WHEN SUBSTRING ( ADS-PRTCPNT-VIEW.ADP-ANNTY-OPTN,1,1 ) = 'I'
                else if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annty_Optn().getSubstring(1,1).equals("I")))
                {
                    decideConditionsMet468++;
                    pnd_Cnvrt_Annty_Optn.setValue("2");                                                                                                                   //Natural: MOVE '2' TO #CNVRT-ANNTY-OPTN
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Cnvrt_Annty_Optn.setValue("3");                                                                                                                   //Natural: MOVE '3' TO #CNVRT-ANNTY-OPTN
                }                                                                                                                                                         //Natural: END-DECIDE
                getWorkFiles().write(1, false, ldaAdsl401a.getVw_ads_Cntrct_View(), pnd_Cnvrt_Annty_Optn, ldaAdsl401.getAds_Prtcpnt_View_Adp_Effctv_Dte());               //Natural: WRITE WORK FILE 1 ADS-CNTRCT-VIEW #CNVRT-ANNTY-OPTN ADP-EFFCTV-DTE
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L0890"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0890"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"TOTAL DA RESULT RECORDS WRITTEN TO OUTPUT -",pnd_Record_Count);                                                                    //Natural: WRITE / 'TOTAL DA RESULT RECORDS WRITTEN TO OUTPUT -' #RECORD-COUNT
        if (Global.isEscape()) return;
    }

    //
}
