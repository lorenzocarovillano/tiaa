/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:05:18 PM
**        * FROM NATURAL PROGRAM : Adsp775
************************************************************
**        * FILE NAME            : Adsp775.java
**        * CLASS NAME           : Adsp775
**        * INSTANCE NAME        : Adsp775
************************************************************
************************************************************************
* PROGRAM  : ADSP775
* GENERATED: APRIL 16, 2004
* SYSTEM   : ADAS (ANNUITIZATION OF DEFERRED ANNUITY MONEY ON SUNGUARD)
* PURPOSE  : THIS MODULE UPDATES EFM WITH TRANSACTION HISTORY REPORTS.
*            THIS PROGRAM CALLS EFSN9120 - EFM UPDATE
*
*
* REMARKS  : CLONED FROM NAZP775 (ADAM SYSTEM)
***********************  MAINTENANCE LOG ******************************
*  D A T E   PROGRAMMER     D E S C R I P T I O N
* 01/27/99   ZAFAR KHAN  NAP-BPS-UNIT EXTENDED FROM 6 TO 8 BYTES
* 11/01/01   OSCAR SOTTO IF #I GT 60, BYPASS THE REST OF THE RECORDS
*                        INSTEAD OF TERMINATING THE JOB.
* 04/16/04   C. AVE      MODIFIED FOR ADAS - ANNUITIZATION SUNGUARD
* 7/19/2010  C. MASON    RESTOW DUE TO LDA CHANGES
* 01/18/13   E. MELNIK   TNG PROJECT.  RESTOW TO INCLUDE UPDATED LINKAGE
*                        AREAS.
* 02/28/2017 R.CARREON   PIN EXPANSION 02282017
***********************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsp775 extends BLNatBase
{
    // Data Areas
    private LdaAdsl401 ldaAdsl401;
    private PdaEfsa9120 pdaEfsa9120;
    private PdaEfsa902r pdaEfsa902r;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpdaus pdaCwfpdaus;
    private PdaCwfpdaun pdaCwfpdaun;
    private PdaCdaobj pdaCdaobj;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Nap_Entry_User_Id;
    private DbsField pnd_Mit_Saved;
    private DbsField pnd_Bps_Saved;
    private DbsField pnd_Pin_Saved;
    private DbsField pnd_I;
    private DbsField pnd_First_Time;
    private DbsField pnd_Efm_Table;
    private DbsField pnd_Input_Record;

    private DbsGroup pnd_Input_Record__R_Field_1;
    private DbsField pnd_Input_Record_Pnd_Mit_Heading;
    private DbsField pnd_Input_Record_Pnd_Mit_Number;
    private DbsField pnd_Input_Record_Pnd_Bps_Heading;
    private DbsField pnd_Input_Record_Pnd_Bps_Number;
    private DbsField pnd_Input_Record_Pnd_Other_Data;

    private DbsGroup pnd_Input_Record__R_Field_2;
    private DbsField pnd_Input_Record_Pnd_Pin_Heading;
    private DbsField pnd_Input_Record_Pnd_Pin;

    private DbsGroup pnd_Input_Record__R_Field_3;
    private DbsField pnd_Input_Record_Pnd_Pin_Number;

    private DbsGroup pnd_Input_Record__R_Field_4;
    private DbsField pnd_Input_Record_Pnd_Pin_Number_7;
    private DbsField pnd_Input_Record_Pnd_Pin_Number_5;
    private DbsField pnd_Input_Record_Pnd_Filler_2;

    private DbsGroup pnd_Input_Record__R_Field_5;
    private DbsField pnd_Input_Record_Pnd_Filler_3;
    private DbsField pnd_Input_Record_Pnd_Input_Data;
    private DbsField pnd_Nap_Rqst_Id;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl401 = new LdaAdsl401();
        registerRecord(ldaAdsl401);
        registerRecord(ldaAdsl401.getVw_ads_Prtcpnt_View());
        localVariables = new DbsRecord();
        pdaEfsa9120 = new PdaEfsa9120(localVariables);
        pdaEfsa902r = new PdaEfsa902r(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfpdaus = new PdaCwfpdaus(localVariables);
        pdaCwfpdaun = new PdaCwfpdaun(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);

        // Local Variables
        pnd_Nap_Entry_User_Id = localVariables.newFieldInRecord("pnd_Nap_Entry_User_Id", "#NAP-ENTRY-USER-ID", FieldType.STRING, 8);
        pnd_Mit_Saved = localVariables.newFieldInRecord("pnd_Mit_Saved", "#MIT-SAVED", FieldType.STRING, 15);
        pnd_Bps_Saved = localVariables.newFieldInRecord("pnd_Bps_Saved", "#BPS-SAVED", FieldType.STRING, 8);
        pnd_Pin_Saved = localVariables.newFieldInRecord("pnd_Pin_Saved", "#PIN-SAVED", FieldType.NUMERIC, 12);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 2);
        pnd_First_Time = localVariables.newFieldInRecord("pnd_First_Time", "#FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Efm_Table = localVariables.newFieldArrayInRecord("pnd_Efm_Table", "#EFM-TABLE", FieldType.STRING, 80, new DbsArrayController(1, 60));
        pnd_Input_Record = localVariables.newFieldInRecord("pnd_Input_Record", "#INPUT-RECORD", FieldType.STRING, 85);

        pnd_Input_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Record__R_Field_1", "REDEFINE", pnd_Input_Record);
        pnd_Input_Record_Pnd_Mit_Heading = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Mit_Heading", "#MIT-HEADING", FieldType.STRING, 
            6);
        pnd_Input_Record_Pnd_Mit_Number = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Mit_Number", "#MIT-NUMBER", FieldType.STRING, 
            15);
        pnd_Input_Record_Pnd_Bps_Heading = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Bps_Heading", "#BPS-HEADING", FieldType.STRING, 
            1);
        pnd_Input_Record_Pnd_Bps_Number = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Bps_Number", "#BPS-NUMBER", FieldType.STRING, 
            8);
        pnd_Input_Record_Pnd_Other_Data = pnd_Input_Record__R_Field_1.newFieldInGroup("pnd_Input_Record_Pnd_Other_Data", "#OTHER-DATA", FieldType.STRING, 
            50);

        pnd_Input_Record__R_Field_2 = localVariables.newGroupInRecord("pnd_Input_Record__R_Field_2", "REDEFINE", pnd_Input_Record);
        pnd_Input_Record_Pnd_Pin_Heading = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Pin_Heading", "#PIN-HEADING", FieldType.STRING, 
            13);
        pnd_Input_Record_Pnd_Pin = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Pin", "#PIN", FieldType.STRING, 12);

        pnd_Input_Record__R_Field_3 = pnd_Input_Record__R_Field_2.newGroupInGroup("pnd_Input_Record__R_Field_3", "REDEFINE", pnd_Input_Record_Pnd_Pin);
        pnd_Input_Record_Pnd_Pin_Number = pnd_Input_Record__R_Field_3.newFieldInGroup("pnd_Input_Record_Pnd_Pin_Number", "#PIN-NUMBER", FieldType.NUMERIC, 
            12);

        pnd_Input_Record__R_Field_4 = pnd_Input_Record__R_Field_2.newGroupInGroup("pnd_Input_Record__R_Field_4", "REDEFINE", pnd_Input_Record_Pnd_Pin);
        pnd_Input_Record_Pnd_Pin_Number_7 = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Pin_Number_7", "#PIN-NUMBER-7", FieldType.NUMERIC, 
            7);
        pnd_Input_Record_Pnd_Pin_Number_5 = pnd_Input_Record__R_Field_4.newFieldInGroup("pnd_Input_Record_Pnd_Pin_Number_5", "#PIN-NUMBER-5", FieldType.STRING, 
            5);
        pnd_Input_Record_Pnd_Filler_2 = pnd_Input_Record__R_Field_2.newFieldInGroup("pnd_Input_Record_Pnd_Filler_2", "#FILLER-2", FieldType.STRING, 60);

        pnd_Input_Record__R_Field_5 = localVariables.newGroupInRecord("pnd_Input_Record__R_Field_5", "REDEFINE", pnd_Input_Record);
        pnd_Input_Record_Pnd_Filler_3 = pnd_Input_Record__R_Field_5.newFieldInGroup("pnd_Input_Record_Pnd_Filler_3", "#FILLER-3", FieldType.STRING, 1);
        pnd_Input_Record_Pnd_Input_Data = pnd_Input_Record__R_Field_5.newFieldInGroup("pnd_Input_Record_Pnd_Input_Data", "#INPUT-DATA", FieldType.STRING, 
            84);
        pnd_Nap_Rqst_Id = localVariables.newFieldInRecord("pnd_Nap_Rqst_Id", "#NAP-RQST-ID", FieldType.STRING, 30);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAdsl401.initializeValues();

        localVariables.reset();
        pnd_First_Time.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Adsp775() throws Exception
    {
        super("Adsp775");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  FOR POST & EFM CONSTRUCT
        //*                                                                                                                                                               //Natural: FORMAT PS = 60 LS = 133
        pdaCdaobj.getCdaobj_Pnd_Function().setValue("STORE");                                                                                                             //Natural: ASSIGN CDAOBJ.#FUNCTION := 'STORE'
        pdaEfsa9120.getEfsa9120().reset();                                                                                                                                //Natural: RESET EFSA9120
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #INPUT-RECORD
        while (condition(getWorkFiles().read(1, pnd_Input_Record)))
        {
            if (condition(pnd_First_Time.getBoolean()))                                                                                                                   //Natural: IF #FIRST-TIME
            {
                pnd_First_Time.setValue(false);                                                                                                                           //Natural: MOVE FALSE TO #FIRST-TIME
                pnd_Mit_Saved.setValue(pnd_Input_Record_Pnd_Mit_Number);                                                                                                  //Natural: MOVE #MIT-NUMBER TO #MIT-SAVED
                pnd_Bps_Saved.setValue(pnd_Input_Record_Pnd_Bps_Number);                                                                                                  //Natural: MOVE #BPS-NUMBER TO #BPS-SAVED
                pnd_Input_Record_Pnd_Mit_Heading.reset();                                                                                                                 //Natural: RESET #MIT-HEADING #MIT-NUMBER #BPS-HEADING #BPS-NUMBER
                pnd_Input_Record_Pnd_Mit_Number.reset();
                pnd_Input_Record_Pnd_Bps_Heading.reset();
                pnd_Input_Record_Pnd_Bps_Number.reset();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Input_Record_Pnd_Mit_Heading.equals("1MIT: ")))                                                                                         //Natural: IF #MIT-HEADING = '1MIT: '
                {
                    if (condition(pnd_Input_Record_Pnd_Mit_Number.equals(pnd_Mit_Saved)))                                                                                 //Natural: IF #MIT-NUMBER = #MIT-SAVED
                    {
                        pdaEfsa9120.getEfsa9120_Last_Page_Flag().setValue("N");                                                                                           //Natural: MOVE 'N' TO EFSA9120.LAST-PAGE-FLAG
                                                                                                                                                                          //Natural: PERFORM EFM-UPDATE-ROUTINE
                        sub_Efm_Update_Routine();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaEfsa9120.getEfsa9120_Last_Page_Flag().setValue("Y");                                                                                           //Natural: MOVE 'Y' TO EFSA9120.LAST-PAGE-FLAG
                                                                                                                                                                          //Natural: PERFORM EFM-UPDATE-ROUTINE
                        sub_Efm_Update_Routine();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                        pnd_Mit_Saved.setValue(pnd_Input_Record_Pnd_Mit_Number);                                                                                          //Natural: MOVE #MIT-NUMBER TO #MIT-SAVED
                        pnd_Bps_Saved.setValue(pnd_Input_Record_Pnd_Bps_Number);                                                                                          //Natural: MOVE #BPS-NUMBER TO #BPS-SAVED
                        pdaEfsa9120.getEfsa9120().reset();                                                                                                                //Natural: RESET EFSA9120
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Efm_Table.getValue("*").reset();                                                                                                                  //Natural: RESET #EFM-TABLE ( * ) #I #MIT-HEADING #MIT-NUMBER #BPS-HEADING #BPS-NUMBER
                    pnd_I.reset();
                    pnd_Input_Record_Pnd_Mit_Heading.reset();
                    pnd_Input_Record_Pnd_Mit_Number.reset();
                    pnd_Input_Record_Pnd_Bps_Heading.reset();
                    pnd_Input_Record_Pnd_Bps_Number.reset();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  OS 110101
                    if (condition(pnd_I.greater(60)))                                                                                                                     //Natural: IF #I > 60
                    {
                        //*  OS 110101
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                        //*  OS 110101
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Record_Pnd_Pin_Heading.equals(" Pin Number: ")))                                                                                      //Natural: IF #PIN-HEADING = ' Pin Number: '
            {
                //*    MOVE #PIN-NUMBER TO #PIN-SAVED
                //*    IF #PIN-NUMBER-5 = ' '
                //*      MOVE #PIN-NUMBER-7 TO #PIN-SAVED
                //*    ELSE
                //*      MOVE #PIN-NUMBER TO #PIN-SAVED
                //*    END-IF
                pnd_Pin_Saved.compute(new ComputeParameters(false, pnd_Pin_Saved), pnd_Input_Record_Pnd_Pin.val());                                                       //Natural: ASSIGN #PIN-SAVED := VAL ( #PIN )
            }                                                                                                                                                             //Natural: END-IF
            pnd_I.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #I
            if (condition(pnd_I.greater(60)))                                                                                                                             //Natural: IF #I > 60
            {
                getReports().write(0, "****************** PAGE TOO LONG ***************");                                                                                //Natural: WRITE '****************** PAGE TOO LONG ***************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "=",pnd_Pin_Saved);                                                                                                                 //Natural: WRITE '=' #PIN-SAVED
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "=",pnd_Bps_Saved);                                                                                                                 //Natural: WRITE '=' #BPS-SAVED
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "=",pnd_Mit_Saved);                                                                                                                 //Natural: WRITE '=' #MIT-SAVED
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "**************** EFM NOT COMPLETED *************");                                                                                //Natural: WRITE '**************** EFM NOT COMPLETED *************'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* *  TERMINATE   /* OS  110101
                //*  0S  110101
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Efm_Table.getValue(pnd_I).setValue(pnd_Input_Record_Pnd_Input_Data);                                                                                      //Natural: MOVE #INPUT-DATA TO #EFM-TABLE ( #I )
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        pdaEfsa9120.getEfsa9120_Last_Page_Flag().setValue("Y");                                                                                                           //Natural: MOVE 'Y' TO EFSA9120.LAST-PAGE-FLAG
                                                                                                                                                                          //Natural: PERFORM EFM-UPDATE-ROUTINE
        sub_Efm_Update_Routine();
        if (condition(Global.isEscape())) {return;}
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EFM-UPDATE-ROUTINE
    }
    private void sub_Efm_Update_Routine() throws Exception                                                                                                                //Natural: EFM-UPDATE-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        if (condition(pnd_Pin_Saved.equals(getZero())))                                                                                                                   //Natural: IF #PIN-SAVED = 0
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  'BATCHNAZ'
            //*  'NAZ'
        }                                                                                                                                                                 //Natural: END-IF
        pdaEfsa9120.getEfsa9120_Action().setValue("AD");                                                                                                                  //Natural: ASSIGN EFSA9120.ACTION := 'AD'
        pdaEfsa9120.getEfsa9120_Cabinet_Prefix().setValue("P");                                                                                                           //Natural: ASSIGN EFSA9120.CABINET-PREFIX := 'P'
        pdaEfsa9120.getEfsa9120_Pin_Nbr().setValue(pnd_Pin_Saved);                                                                                                        //Natural: ASSIGN EFSA9120.PIN-NBR := #PIN-SAVED
        pdaEfsa9120.getEfsa9120_Rqst_Id().getValue(1).setValue(pnd_Mit_Saved);                                                                                            //Natural: ASSIGN EFSA9120.RQST-ID ( 1 ) := #MIT-SAVED
        pdaEfsa9120.getEfsa9120_Unit_Cde().getValue(1).setValue(" ");                                                                                                     //Natural: ASSIGN EFSA9120.UNIT-CDE ( 1 ) := ' '
        pdaEfsa9120.getEfsa9120_Rqst_Entry_Op_Cde().setValue("BATCHADS");                                                                                                 //Natural: ASSIGN EFSA9120.RQST-ENTRY-OP-CDE := 'BATCHADS'
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Cde().setValue("G");                                                                                                          //Natural: ASSIGN EFSA9120.RQST-ORIGIN-CDE := 'G'
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Unit_Cde().setValue(pnd_Bps_Saved);                                                                                           //Natural: ASSIGN EFSA9120.RQST-ORIGIN-UNIT-CDE := #BPS-SAVED
        pdaEfsa9120.getEfsa9120_Doc_Category().setValue("I");                                                                                                             //Natural: ASSIGN EFSA9120.DOC-CATEGORY := 'I'
        pdaEfsa9120.getEfsa9120_Doc_Class().setValue("AUD");                                                                                                              //Natural: ASSIGN EFSA9120.DOC-CLASS := 'AUD'
        pdaEfsa9120.getEfsa9120_Doc_Specific().setValue("THST");                                                                                                          //Natural: ASSIGN EFSA9120.DOC-SPECIFIC := 'THST'
        pdaEfsa9120.getEfsa9120_Doc_Direction().setValue("N");                                                                                                            //Natural: ASSIGN EFSA9120.DOC-DIRECTION := 'N'
        pdaEfsa9120.getEfsa9120_Doc_Security_Cde().setValue("0");                                                                                                         //Natural: ASSIGN EFSA9120.DOC-SECURITY-CDE := '0'
        pdaEfsa9120.getEfsa9120_Doc_Format_Cde().setValue("T");                                                                                                           //Natural: ASSIGN EFSA9120.DOC-FORMAT-CDE := 'T'
        pdaEfsa9120.getEfsa9120_Doc_Retention_Cde().setValue("P");                                                                                                        //Natural: ASSIGN EFSA9120.DOC-RETENTION-CDE := 'P'
        pdaEfsa9120.getEfsa9120_System().setValue("ADS");                                                                                                                 //Natural: ASSIGN EFSA9120.SYSTEM := 'ADS'
        pdaEfsa9120.getEfsa9120_Doc_Text().getValue("*").setValue(pnd_Efm_Table.getValue("*"));                                                                           //Natural: MOVE #EFM-TABLE ( * ) TO DOC-TEXT ( * )
        DbsUtil.callnat(Efsn9120.class , getCurrentProcessState(), pdaEfsa9120.getEfsa9120(), pdaEfsa9120.getEfsa9120_Id(), pdaEfsa902r.getEfsa902r(),                    //Natural: CALLNAT 'EFSN9120' EFSA9120 EFSA9120-ID EFSA902R CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().greater(getZero()) || pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E")))                  //Natural: IF ##MSG-NR GT 0 OR ##RETURN-CODE = 'E'
        {
            getReports().write(0, "=",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code());                                                                                 //Natural: WRITE '=' ##RETURN-CODE
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr(),"=",pnd_Pin_Saved);                                                                    //Natural: WRITE '=' ##MSG-NR '=' #PIN-SAVED
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                         //Natural: WRITE '=' ##MSG
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133");
    }
}
