/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:22:30 PM
**        * FROM NATURAL PDA     : NMDPDA_M
************************************************************
**        * FILE NAME            : PdaNmdpda_M.java
**        * CLASS NAME           : PdaNmdpda_M
**        * INSTANCE NAME        : PdaNmdpda_M
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaNmdpda_M extends PdaBase
{
    // Properties
    private DbsGroup msg_Info_Sub;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg;
    private DbsGroup msg_Info_Sub_Pnd_Pnd_MsgRedef1;
    private DbsField msg_Info_Sub_Pnd_Msg_Display;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg_Nr;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg_Data;
    private DbsGroup msg_Info_Sub_Pnd_Pnd_Msg_DataRedef2;
    private DbsGroup msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct;
    private DbsField msg_Info_Sub_Pnd_Pnd_Msg_Data_Char;
    private DbsField msg_Info_Sub_Pnd_Pnd_Return_Code;
    private DbsField msg_Info_Sub_Pnd_Pnd_Error_Field;
    private DbsField msg_Info_Sub_Pnd_Pnd_Error_Field_Index1;
    private DbsField msg_Info_Sub_Pnd_Pnd_Error_Field_Index2;
    private DbsField msg_Info_Sub_Pnd_Pnd_Error_Field_Index3;

    public DbsGroup getMsg_Info_Sub() { return msg_Info_Sub; }

    public DbsField getMsg_Info_Sub_Pnd_Pnd_Msg() { return msg_Info_Sub_Pnd_Pnd_Msg; }

    public DbsGroup getMsg_Info_Sub_Pnd_Pnd_MsgRedef1() { return msg_Info_Sub_Pnd_Pnd_MsgRedef1; }

    public DbsField getMsg_Info_Sub_Pnd_Msg_Display() { return msg_Info_Sub_Pnd_Msg_Display; }

    public DbsField getMsg_Info_Sub_Pnd_Pnd_Msg_Nr() { return msg_Info_Sub_Pnd_Pnd_Msg_Nr; }

    public DbsField getMsg_Info_Sub_Pnd_Pnd_Msg_Data() { return msg_Info_Sub_Pnd_Pnd_Msg_Data; }

    public DbsGroup getMsg_Info_Sub_Pnd_Pnd_Msg_DataRedef2() { return msg_Info_Sub_Pnd_Pnd_Msg_DataRedef2; }

    public DbsGroup getMsg_Info_Sub_Pnd_Pnd_Msg_Data_Struct() { return msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct; }

    public DbsField getMsg_Info_Sub_Pnd_Pnd_Msg_Data_Char() { return msg_Info_Sub_Pnd_Pnd_Msg_Data_Char; }

    public DbsField getMsg_Info_Sub_Pnd_Pnd_Return_Code() { return msg_Info_Sub_Pnd_Pnd_Return_Code; }

    public DbsField getMsg_Info_Sub_Pnd_Pnd_Error_Field() { return msg_Info_Sub_Pnd_Pnd_Error_Field; }

    public DbsField getMsg_Info_Sub_Pnd_Pnd_Error_Field_Index1() { return msg_Info_Sub_Pnd_Pnd_Error_Field_Index1; }

    public DbsField getMsg_Info_Sub_Pnd_Pnd_Error_Field_Index2() { return msg_Info_Sub_Pnd_Pnd_Error_Field_Index2; }

    public DbsField getMsg_Info_Sub_Pnd_Pnd_Error_Field_Index3() { return msg_Info_Sub_Pnd_Pnd_Error_Field_Index3; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        msg_Info_Sub = dbsRecord.newGroupInRecord("msg_Info_Sub", "MSG-INFO-SUB");
        msg_Info_Sub.setParameterOption(ParameterOption.ByReference);
        msg_Info_Sub_Pnd_Pnd_Msg = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Msg", "##MSG", FieldType.STRING, 79);
        msg_Info_Sub_Pnd_Pnd_MsgRedef1 = msg_Info_Sub.newGroupInGroup("msg_Info_Sub_Pnd_Pnd_MsgRedef1", "Redefines", msg_Info_Sub_Pnd_Pnd_Msg);
        msg_Info_Sub_Pnd_Msg_Display = msg_Info_Sub_Pnd_Pnd_MsgRedef1.newFieldInGroup("msg_Info_Sub_Pnd_Msg_Display", "#MSG-DISPLAY", FieldType.STRING, 
            78);
        msg_Info_Sub_Pnd_Pnd_Msg_Nr = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Nr", "##MSG-NR", FieldType.NUMERIC, 4);
        msg_Info_Sub_Pnd_Pnd_Msg_Data = msg_Info_Sub.newFieldArrayInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Data", "##MSG-DATA", FieldType.STRING, 32, new DbsArrayController(1,
            3));
        msg_Info_Sub_Pnd_Pnd_Msg_DataRedef2 = msg_Info_Sub.newGroupInGroup("msg_Info_Sub_Pnd_Pnd_Msg_DataRedef2", "Redefines", msg_Info_Sub_Pnd_Pnd_Msg_Data);
        msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct = msg_Info_Sub_Pnd_Pnd_Msg_DataRedef2.newGroupArrayInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct", "##MSG-DATA-STRUCT", 
            new DbsArrayController(1,3));
        msg_Info_Sub_Pnd_Pnd_Msg_Data_Char = msg_Info_Sub_Pnd_Pnd_Msg_Data_Struct.newFieldArrayInGroup("msg_Info_Sub_Pnd_Pnd_Msg_Data_Char", "##MSG-DATA-CHAR", 
            FieldType.STRING, 1, new DbsArrayController(1,32));
        msg_Info_Sub_Pnd_Pnd_Return_Code = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Return_Code", "##RETURN-CODE", FieldType.STRING, 1);
        msg_Info_Sub_Pnd_Pnd_Error_Field = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Error_Field", "##ERROR-FIELD", FieldType.STRING, 32);
        msg_Info_Sub_Pnd_Pnd_Error_Field_Index1 = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Error_Field_Index1", "##ERROR-FIELD-INDEX1", FieldType.PACKED_DECIMAL, 
            3);
        msg_Info_Sub_Pnd_Pnd_Error_Field_Index2 = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Error_Field_Index2", "##ERROR-FIELD-INDEX2", FieldType.PACKED_DECIMAL, 
            3);
        msg_Info_Sub_Pnd_Pnd_Error_Field_Index3 = msg_Info_Sub.newFieldInGroup("msg_Info_Sub_Pnd_Pnd_Error_Field_Index3", "##ERROR-FIELD-INDEX3", FieldType.PACKED_DECIMAL, 
            3);

        dbsRecord.reset();
    }

    // Constructors
    public PdaNmdpda_M(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

