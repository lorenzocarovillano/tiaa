/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:19 PM
**        * FROM NATURAL LDA     : ADSL761
************************************************************
**        * FILE NAME            : LdaAdsl761.java
**        * CLASS NAME           : LdaAdsl761
**        * INSTANCE NAME        : LdaAdsl761
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdsl761 extends DbsRecord
{
    // Properties
    private DbsField pnd_Rollover_Contract_Nbr;
    private DbsField pnd_Rollover_Amount;
    private DbsField pnd_Tiaa_Contract_Nbr;
    private DbsField pnd_Prev_Tiaa_Contract_Nbr;
    private DbsField pnd_Cref_Certificate_Nbr;
    private DbsField pnd_Nap_Mit_Log_Dte_Tme;
    private DbsField pnd_Nap_Bps_Unit;
    private DbsField pnd_Page_Number;
    private DbsField pnd_Page_Number_Roth;
    private DbsField pnd_Page_Number_Surv;
    private DbsField pnd_Ia_Number;
    private DbsField pnd_Social_Sec_Nbr_1;
    private DbsField pnd_Social_Sec_Nbr_2;
    private DbsField pnd_Sex_1;
    private DbsField pnd_Sex_2;
    private DbsField pnd_Scnd_Spouse;
    private DbsField pnd_Frst_Citizenship;
    private DbsField pnd_Scnd_Citizenship;
    private DbsField pnd_Work_Rqst_Id;
    private DbsField pnd_Nap_Annty_Optn;
    private DbsField pnd_Nap_Pymnt_Mode_All;
    private DbsField pnd_Rtb;
    private DbsField pnd_Slash;
    private DbsField pnd_Nap_Grntee_Period;
    private DbsField pnd_Partcipant_Name_1;
    private DbsField pnd_Partcipant_Name_2;
    private DbsField pnd_Residence;
    private DbsField pnd_Rqst_Id;
    private DbsGroup pnd_Rqst_IdRedef1;
    private DbsField pnd_Rqst_Id_Pnd_Unique_Id;
    private DbsField pnd_Rqst_Id_Pnd_Effctv_Dte;
    private DbsField pnd_Rqst_Id_Pnd_Entry_Dte;
    private DbsField pnd_Rqst_Id_Pnd_Entry_Tme;
    private DbsField pnd_Prev_Da_Rqst_Id;
    private DbsGroup pnd_Prev_Da_Rqst_IdRedef2;
    private DbsField pnd_Prev_Da_Rqst_Id_Pnd_Prev_Da_Unique_Id;
    private DbsField pnd_Prev_Da_Rqst_Id_Pnd_Prev_Da_Effctv_Dte;
    private DbsField pnd_Prev_Da_Rqst_Id_Pnd_Prev_Da_Entry_Dte;
    private DbsField pnd_Prev_Da_Rqst_Id_Pnd_Prev_Da_Entry_Tme;
    private DbsGroup pnd_Rtb_Rollover_Code;
    private DbsField pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde;
    private DbsGroup pnd_Destination_Code;
    private DbsField pnd_Destination_Code_Pnd_Destination_Cde;
    private DbsGroup pnd_Target_All;
    private DbsGroup pnd_Target_All_Pnd_Target_All_No;
    private DbsField pnd_Target_All_Pnd_Target_Number;
    private DbsField pnd_Target_All_Pnd_Target_Disc;
    private DbsField pnd_Cref_Cert_Nbr;
    private DbsField pnd_Cref_Cert_Ind;
    private DbsField pnd_Tiaa_Cert_Nbr;
    private DbsField pnd_Tiaa_Cert_Ind;
    private DbsField pnd_Final_Prem_Ind;
    private DbsField pnd_Da_Accum_Dte_Yyyymmdd;
    private DbsGroup pnd_Da_Accum_Dte_YyyymmddRedef3;
    private DbsField pnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Cc;
    private DbsField pnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Yy;
    private DbsField pnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Mm;
    private DbsField pnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Dd;
    private DbsField pnd_Da_Accum_Date;
    private DbsGroup pnd_Da_Accum_DateRedef4;
    private DbsField pnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Mm;
    private DbsField pnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Dd;
    private DbsField pnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Yy;
    private DbsField pnd_First_Entered_Yyyymmdd;
    private DbsGroup pnd_First_Entered_YyyymmddRedef5;
    private DbsField pnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Cc;
    private DbsField pnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Yy;
    private DbsField pnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Mm;
    private DbsField pnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Dd;
    private DbsField pnd_First_Entered_Date;
    private DbsGroup pnd_First_Entered_DateRedef6;
    private DbsField pnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Mm;
    private DbsField pnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Dd;
    private DbsField pnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Cc;
    private DbsField pnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Yy;
    private DbsField pnd_First_Verified_Yyyymmdd;
    private DbsGroup pnd_First_Verified_YyyymmddRedef7;
    private DbsField pnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Cc;
    private DbsField pnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Yy;
    private DbsField pnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Mm;
    private DbsField pnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Dd;
    private DbsField pnd_First_Verified_Date;
    private DbsGroup pnd_First_Verified_DateRedef8;
    private DbsField pnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Mm;
    private DbsField pnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Dd;
    private DbsField pnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Cc;
    private DbsField pnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Yy;
    private DbsField pnd_Last_Updated_Yyyymmdd;
    private DbsGroup pnd_Last_Updated_YyyymmddRedef9;
    private DbsField pnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Cc;
    private DbsField pnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Yy;
    private DbsField pnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Mm;
    private DbsField pnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Dd;
    private DbsField pnd_Last_Updated_Date;
    private DbsGroup pnd_Last_Updated_DateRedef10;
    private DbsField pnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Mm;
    private DbsField pnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Dd;
    private DbsField pnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Cc;
    private DbsField pnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Yy;
    private DbsField pnd_Last_Verified_Yyyymmdd;
    private DbsGroup pnd_Last_Verified_YyyymmddRedef11;
    private DbsField pnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Cc;
    private DbsField pnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Yy;
    private DbsField pnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Mm;
    private DbsField pnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Dd;
    private DbsField pnd_Last_Verified_Date;
    private DbsGroup pnd_Last_Verified_DateRedef12;
    private DbsField pnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Mm;
    private DbsField pnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Dd;
    private DbsField pnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Cc;
    private DbsField pnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Yy;
    private DbsField pnd_Frst_Birth_Dte_Yyyymmdd;
    private DbsGroup pnd_Frst_Birth_Dte_YyyymmddRedef13;
    private DbsField pnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Yyyy;
    private DbsField pnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Mm;
    private DbsField pnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Dd;
    private DbsField pnd_Frst_Birth_Dte_Mdcy;
    private DbsGroup pnd_Frst_Birth_Dte_MdcyRedef14;
    private DbsField pnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Mm;
    private DbsField pnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Dd;
    private DbsField pnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Yyyy;
    private DbsField pnd_Scnd_Birth_Dte_Yyyymmdd;
    private DbsGroup pnd_Scnd_Birth_Dte_YyyymmddRedef15;
    private DbsField pnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Yyyy;
    private DbsField pnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Mm;
    private DbsField pnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Dd;
    private DbsField pnd_Scnd_Birth_Dte_Mdcy;
    private DbsGroup pnd_Scnd_Birth_Dte_MdcyRedef16;
    private DbsField pnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Mm;
    private DbsField pnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Dd;
    private DbsField pnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Yyyy;
    private DbsField pnd_Rtb_Account_Dte_Yyyymmdd;
    private DbsGroup pnd_Rtb_Account_Dte_YyyymmddRedef17;
    private DbsField pnd_Rtb_Account_Dte_Yyyymmdd_Pnd_Rtb_Account_Dte_Yyyy;
    private DbsField pnd_Rtb_Account_Dte_Yyyymmdd_Pnd_Rtb_Account_Dte_Mm;
    private DbsField pnd_Rtb_Account_Dte_Yyyymmdd_Pnd_Rtb_Account_Dte_Dd;
    private DbsField pnd_Rtb_Account_Dte_Mdcy;
    private DbsGroup pnd_Rtb_Account_Dte_MdcyRedef18;
    private DbsField pnd_Rtb_Account_Dte_Mdcy_Pnd_Rtb_Account_Dte_Mdcy_Mm;
    private DbsField pnd_Rtb_Account_Dte_Mdcy_Pnd_Rtb_Account_Dte_Mdcy_Dd;
    private DbsField pnd_Rtb_Account_Dte_Mdcy_Pnd_Rtb_Account_Dte_Mdcy_Yyyy;
    private DbsField pnd_Rtb_Settle_Dte_Yyyymmdd;
    private DbsGroup pnd_Rtb_Settle_Dte_YyyymmddRedef19;
    private DbsField pnd_Rtb_Settle_Dte_Yyyymmdd_Pnd_Rtb_Settle_Dte_Yyyy;
    private DbsField pnd_Rtb_Settle_Dte_Yyyymmdd_Pnd_Rtb_Settle_Dte_Mm;
    private DbsField pnd_Rtb_Settle_Dte_Yyyymmdd_Pnd_Rtb_Settle_Dte_Dd;
    private DbsField pnd_Rtb_Settle_Dte_Mdcy;
    private DbsGroup pnd_Rtb_Settle_Dte_MdcyRedef20;
    private DbsField pnd_Rtb_Settle_Dte_Mdcy_Pnd_Rtb_Settle_Dte_Mdcy_Mm;
    private DbsField pnd_Rtb_Settle_Dte_Mdcy_Pnd_Rtb_Settle_Dte_Mdcy_Dd;
    private DbsField pnd_Rtb_Settle_Dte_Mdcy_Pnd_Rtb_Settle_Dte_Mdcy_Yyyy;
    private DbsField pnd_Cont_Issue_Dte_Yyyymmdd;
    private DbsGroup pnd_Cont_Issue_Dte_YyyymmddRedef21;
    private DbsField pnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Yyyy;
    private DbsField pnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Mm;
    private DbsField pnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Dd;
    private DbsField pnd_Cont_Issue_Dte_Mdcy;
    private DbsGroup pnd_Cont_Issue_Dte_MdcyRedef22;
    private DbsField pnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Mm;
    private DbsField pnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Dd;
    private DbsField pnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Yyyy;
    private DbsField pnd_Annt_Start_Dte_Yyyymmdd;
    private DbsGroup pnd_Annt_Start_Dte_YyyymmddRedef23;
    private DbsField pnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Yyyy;
    private DbsField pnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Mm;
    private DbsField pnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Dd;
    private DbsField pnd_Annt_Start_Dte_Mdcy;
    private DbsGroup pnd_Annt_Start_Dte_MdcyRedef24;
    private DbsField pnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Mm;
    private DbsField pnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Dd;
    private DbsField pnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Yyyy;
    private DbsField pnd_Frst_Perod_Dte_Yyyymmdd;
    private DbsGroup pnd_Frst_Perod_Dte_YyyymmddRedef25;
    private DbsField pnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Yyyy;
    private DbsField pnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Mm;
    private DbsField pnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Dd;
    private DbsField pnd_Frst_Perod_Dte_Mdcy;
    private DbsGroup pnd_Frst_Perod_Dte_MdcyRedef26;
    private DbsField pnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Mm;
    private DbsField pnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Dd;
    private DbsField pnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Yyyy;
    private DbsField pnd_Last_Prem_Dte_Yyyymmdd;
    private DbsGroup pnd_Last_Prem_Dte_YyyymmddRedef27;
    private DbsField pnd_Last_Prem_Dte_Yyyymmdd_Pnd_Last_Prem_Dte_Yyyy;
    private DbsField pnd_Last_Prem_Dte_Yyyymmdd_Pnd_Last_Prem_Dte_Mm;
    private DbsField pnd_Last_Prem_Dte_Yyyymmdd_Pnd_Last_Prem_Dte_Dd;
    private DbsField pnd_Last_Prem_Dte_Mdcy;
    private DbsGroup pnd_Last_Prem_Dte_MdcyRedef28;
    private DbsField pnd_Last_Prem_Dte_Mdcy_Pnd_Last_Prem_Dte_Mdcy_Mm;
    private DbsField pnd_Last_Prem_Dte_Mdcy_Pnd_Last_Prem_Dte_Mdcy_Dd;
    private DbsField pnd_Last_Prem_Dte_Mdcy_Pnd_Last_Prem_Dte_Mdcy_Yyyy;
    private DbsField pnd_Last_Guar_Dte_Yyyymmdd;
    private DbsGroup pnd_Last_Guar_Dte_YyyymmddRedef29;
    private DbsField pnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Yyyy;
    private DbsField pnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Mm;
    private DbsField pnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Dd;
    private DbsField pnd_Last_Guar_Dte_Mdcy;
    private DbsGroup pnd_Last_Guar_Dte_MdcyRedef30;
    private DbsField pnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Mm;
    private DbsField pnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Dd;
    private DbsField pnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Yyyy;
    private DbsField pnd_Empl_Term_Dte_Yyyymmdd;
    private DbsGroup pnd_Empl_Term_Dte_YyyymmddRedef31;
    private DbsField pnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Yyyy;
    private DbsField pnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Mm;
    private DbsField pnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Dd;
    private DbsField pnd_Empl_Term_Dte_Mdcy;
    private DbsGroup pnd_Empl_Term_Dte_MdcyRedef32;
    private DbsField pnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Mm;
    private DbsField pnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Dd;
    private DbsField pnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Yyyy;
    private DbsField pnd_Form_Comp_Dte_Yyyymmdd;
    private DbsGroup pnd_Form_Comp_Dte_YyyymmddRedef33;
    private DbsField pnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Yyyy;
    private DbsField pnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Mm;
    private DbsField pnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Dd;
    private DbsField pnd_Form_Comp_Dte_Mdcy;
    private DbsGroup pnd_Form_Comp_Dte_MdcyRedef34;
    private DbsField pnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Mm;
    private DbsField pnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Dd;
    private DbsField pnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Yyyy;
    private DbsField pnd_Effective_Dte_Yyyymmdd;
    private DbsGroup pnd_Effective_Dte_YyyymmddRedef35;
    private DbsField pnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Yyyy;
    private DbsField pnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Mm;
    private DbsField pnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Dd;
    private DbsField pnd_Effective_Dte_Mdcy;
    private DbsGroup pnd_Effective_Dte_MdcyRedef36;
    private DbsField pnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Mm;
    private DbsField pnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Dd;
    private DbsField pnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Yyyy;
    private DbsField pnd_Payment_Dte_Yyyymmdd;
    private DbsGroup pnd_Payment_Dte_YyyymmddRedef37;
    private DbsField pnd_Payment_Dte_Yyyymmdd_Pnd_Payment_Dte_Yyyy;
    private DbsField pnd_Payment_Dte_Yyyymmdd_Pnd_Payment_Dte_Mm;
    private DbsField pnd_Payment_Dte_Yyyymmdd_Pnd_Payment_Dte_Dd;
    private DbsField pnd_Payment_Dte_Mdcy;
    private DbsGroup pnd_Payment_Dte_MdcyRedef38;
    private DbsField pnd_Payment_Dte_Mdcy_Pnd_Payment_Dte_Mdcy_Mm;
    private DbsField pnd_Payment_Dte_Mdcy_Pnd_Payment_Dte_Mdcy_Dd;
    private DbsField pnd_Payment_Dte_Mdcy_Pnd_Payment_Dte_Mdcy_Yyyy;
    private DbsField pnd_Account_Dte_Yyyymmdd;
    private DbsGroup pnd_Account_Dte_YyyymmddRedef39;
    private DbsField pnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Yyyy;
    private DbsField pnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Mm;
    private DbsField pnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Dd;
    private DbsField pnd_Account_Dte_Mdcy;
    private DbsGroup pnd_Account_Dte_MdcyRedef40;
    private DbsField pnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Mm;
    private DbsField pnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Dd;
    private DbsField pnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Yyyy;
    private DbsField pnd_Non_Prem_Dte_Yyyymmdd;
    private DbsGroup pnd_Non_Prem_Dte_YyyymmddRedef41;
    private DbsField pnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Yyyy;
    private DbsField pnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Mm;
    private DbsField pnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Dd;
    private DbsField pnd_Non_Prem_Dte_Mdcy;
    private DbsGroup pnd_Non_Prem_Dte_MdcyRedef42;
    private DbsField pnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Mm;
    private DbsField pnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Dd;
    private DbsField pnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Yyyy;
    private DbsField pnd_Nap_Effective_Date;
    private DbsGroup pnd_Nap_Effective_DateRedef43;
    private DbsField pnd_Nap_Effective_Date_Pnd_Nap_Effective_Date_Cc;
    private DbsField pnd_Nap_Effective_Date_Pnd_Nap_Effective_Date_Yy;
    private DbsField pnd_Nap_Effective_Date_Pnd_Nap_Effective_Date_Mm;
    private DbsField pnd_Nap_Effective_Date_Pnd_Nap_Effective_Date_Dd;
    private DbsField pnd_T_Tpa_Io_Lit;
    private DbsField pnd_Dash_666;

    public DbsField getPnd_Rollover_Contract_Nbr() { return pnd_Rollover_Contract_Nbr; }

    public DbsField getPnd_Rollover_Amount() { return pnd_Rollover_Amount; }

    public DbsField getPnd_Tiaa_Contract_Nbr() { return pnd_Tiaa_Contract_Nbr; }

    public DbsField getPnd_Prev_Tiaa_Contract_Nbr() { return pnd_Prev_Tiaa_Contract_Nbr; }

    public DbsField getPnd_Cref_Certificate_Nbr() { return pnd_Cref_Certificate_Nbr; }

    public DbsField getPnd_Nap_Mit_Log_Dte_Tme() { return pnd_Nap_Mit_Log_Dte_Tme; }

    public DbsField getPnd_Nap_Bps_Unit() { return pnd_Nap_Bps_Unit; }

    public DbsField getPnd_Page_Number() { return pnd_Page_Number; }

    public DbsField getPnd_Page_Number_Roth() { return pnd_Page_Number_Roth; }

    public DbsField getPnd_Page_Number_Surv() { return pnd_Page_Number_Surv; }

    public DbsField getPnd_Ia_Number() { return pnd_Ia_Number; }

    public DbsField getPnd_Social_Sec_Nbr_1() { return pnd_Social_Sec_Nbr_1; }

    public DbsField getPnd_Social_Sec_Nbr_2() { return pnd_Social_Sec_Nbr_2; }

    public DbsField getPnd_Sex_1() { return pnd_Sex_1; }

    public DbsField getPnd_Sex_2() { return pnd_Sex_2; }

    public DbsField getPnd_Scnd_Spouse() { return pnd_Scnd_Spouse; }

    public DbsField getPnd_Frst_Citizenship() { return pnd_Frst_Citizenship; }

    public DbsField getPnd_Scnd_Citizenship() { return pnd_Scnd_Citizenship; }

    public DbsField getPnd_Work_Rqst_Id() { return pnd_Work_Rqst_Id; }

    public DbsField getPnd_Nap_Annty_Optn() { return pnd_Nap_Annty_Optn; }

    public DbsField getPnd_Nap_Pymnt_Mode_All() { return pnd_Nap_Pymnt_Mode_All; }

    public DbsField getPnd_Rtb() { return pnd_Rtb; }

    public DbsField getPnd_Slash() { return pnd_Slash; }

    public DbsField getPnd_Nap_Grntee_Period() { return pnd_Nap_Grntee_Period; }

    public DbsField getPnd_Partcipant_Name_1() { return pnd_Partcipant_Name_1; }

    public DbsField getPnd_Partcipant_Name_2() { return pnd_Partcipant_Name_2; }

    public DbsField getPnd_Residence() { return pnd_Residence; }

    public DbsField getPnd_Rqst_Id() { return pnd_Rqst_Id; }

    public DbsGroup getPnd_Rqst_IdRedef1() { return pnd_Rqst_IdRedef1; }

    public DbsField getPnd_Rqst_Id_Pnd_Unique_Id() { return pnd_Rqst_Id_Pnd_Unique_Id; }

    public DbsField getPnd_Rqst_Id_Pnd_Effctv_Dte() { return pnd_Rqst_Id_Pnd_Effctv_Dte; }

    public DbsField getPnd_Rqst_Id_Pnd_Entry_Dte() { return pnd_Rqst_Id_Pnd_Entry_Dte; }

    public DbsField getPnd_Rqst_Id_Pnd_Entry_Tme() { return pnd_Rqst_Id_Pnd_Entry_Tme; }

    public DbsField getPnd_Prev_Da_Rqst_Id() { return pnd_Prev_Da_Rqst_Id; }

    public DbsGroup getPnd_Prev_Da_Rqst_IdRedef2() { return pnd_Prev_Da_Rqst_IdRedef2; }

    public DbsField getPnd_Prev_Da_Rqst_Id_Pnd_Prev_Da_Unique_Id() { return pnd_Prev_Da_Rqst_Id_Pnd_Prev_Da_Unique_Id; }

    public DbsField getPnd_Prev_Da_Rqst_Id_Pnd_Prev_Da_Effctv_Dte() { return pnd_Prev_Da_Rqst_Id_Pnd_Prev_Da_Effctv_Dte; }

    public DbsField getPnd_Prev_Da_Rqst_Id_Pnd_Prev_Da_Entry_Dte() { return pnd_Prev_Da_Rqst_Id_Pnd_Prev_Da_Entry_Dte; }

    public DbsField getPnd_Prev_Da_Rqst_Id_Pnd_Prev_Da_Entry_Tme() { return pnd_Prev_Da_Rqst_Id_Pnd_Prev_Da_Entry_Tme; }

    public DbsGroup getPnd_Rtb_Rollover_Code() { return pnd_Rtb_Rollover_Code; }

    public DbsField getPnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde() { return pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde; }

    public DbsGroup getPnd_Destination_Code() { return pnd_Destination_Code; }

    public DbsField getPnd_Destination_Code_Pnd_Destination_Cde() { return pnd_Destination_Code_Pnd_Destination_Cde; }

    public DbsGroup getPnd_Target_All() { return pnd_Target_All; }

    public DbsGroup getPnd_Target_All_Pnd_Target_All_No() { return pnd_Target_All_Pnd_Target_All_No; }

    public DbsField getPnd_Target_All_Pnd_Target_Number() { return pnd_Target_All_Pnd_Target_Number; }

    public DbsField getPnd_Target_All_Pnd_Target_Disc() { return pnd_Target_All_Pnd_Target_Disc; }

    public DbsField getPnd_Cref_Cert_Nbr() { return pnd_Cref_Cert_Nbr; }

    public DbsField getPnd_Cref_Cert_Ind() { return pnd_Cref_Cert_Ind; }

    public DbsField getPnd_Tiaa_Cert_Nbr() { return pnd_Tiaa_Cert_Nbr; }

    public DbsField getPnd_Tiaa_Cert_Ind() { return pnd_Tiaa_Cert_Ind; }

    public DbsField getPnd_Final_Prem_Ind() { return pnd_Final_Prem_Ind; }

    public DbsField getPnd_Da_Accum_Dte_Yyyymmdd() { return pnd_Da_Accum_Dte_Yyyymmdd; }

    public DbsGroup getPnd_Da_Accum_Dte_YyyymmddRedef3() { return pnd_Da_Accum_Dte_YyyymmddRedef3; }

    public DbsField getPnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Cc() { return pnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Cc; }

    public DbsField getPnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Yy() { return pnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Yy; }

    public DbsField getPnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Mm() { return pnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Mm; }

    public DbsField getPnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Dd() { return pnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Dd; }

    public DbsField getPnd_Da_Accum_Date() { return pnd_Da_Accum_Date; }

    public DbsGroup getPnd_Da_Accum_DateRedef4() { return pnd_Da_Accum_DateRedef4; }

    public DbsField getPnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Mm() { return pnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Mm; }

    public DbsField getPnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Dd() { return pnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Dd; }

    public DbsField getPnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Yy() { return pnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Yy; }

    public DbsField getPnd_First_Entered_Yyyymmdd() { return pnd_First_Entered_Yyyymmdd; }

    public DbsGroup getPnd_First_Entered_YyyymmddRedef5() { return pnd_First_Entered_YyyymmddRedef5; }

    public DbsField getPnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Cc() { return pnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Cc; }

    public DbsField getPnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Yy() { return pnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Yy; }

    public DbsField getPnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Mm() { return pnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Mm; }

    public DbsField getPnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Dd() { return pnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Dd; }

    public DbsField getPnd_First_Entered_Date() { return pnd_First_Entered_Date; }

    public DbsGroup getPnd_First_Entered_DateRedef6() { return pnd_First_Entered_DateRedef6; }

    public DbsField getPnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Mm() { return pnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Mm; }

    public DbsField getPnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Dd() { return pnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Dd; }

    public DbsField getPnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Cc() { return pnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Cc; }

    public DbsField getPnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Yy() { return pnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Yy; }

    public DbsField getPnd_First_Verified_Yyyymmdd() { return pnd_First_Verified_Yyyymmdd; }

    public DbsGroup getPnd_First_Verified_YyyymmddRedef7() { return pnd_First_Verified_YyyymmddRedef7; }

    public DbsField getPnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Cc() { return pnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Cc; }

    public DbsField getPnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Yy() { return pnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Yy; }

    public DbsField getPnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Mm() { return pnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Mm; }

    public DbsField getPnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Dd() { return pnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Dd; }

    public DbsField getPnd_First_Verified_Date() { return pnd_First_Verified_Date; }

    public DbsGroup getPnd_First_Verified_DateRedef8() { return pnd_First_Verified_DateRedef8; }

    public DbsField getPnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Mm() { return pnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Mm; }

    public DbsField getPnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Dd() { return pnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Dd; }

    public DbsField getPnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Cc() { return pnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Cc; }

    public DbsField getPnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Yy() { return pnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Yy; }

    public DbsField getPnd_Last_Updated_Yyyymmdd() { return pnd_Last_Updated_Yyyymmdd; }

    public DbsGroup getPnd_Last_Updated_YyyymmddRedef9() { return pnd_Last_Updated_YyyymmddRedef9; }

    public DbsField getPnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Cc() { return pnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Cc; }

    public DbsField getPnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Yy() { return pnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Yy; }

    public DbsField getPnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Mm() { return pnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Mm; }

    public DbsField getPnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Dd() { return pnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Dd; }

    public DbsField getPnd_Last_Updated_Date() { return pnd_Last_Updated_Date; }

    public DbsGroup getPnd_Last_Updated_DateRedef10() { return pnd_Last_Updated_DateRedef10; }

    public DbsField getPnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Mm() { return pnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Mm; }

    public DbsField getPnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Dd() { return pnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Dd; }

    public DbsField getPnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Cc() { return pnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Cc; }

    public DbsField getPnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Yy() { return pnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Yy; }

    public DbsField getPnd_Last_Verified_Yyyymmdd() { return pnd_Last_Verified_Yyyymmdd; }

    public DbsGroup getPnd_Last_Verified_YyyymmddRedef11() { return pnd_Last_Verified_YyyymmddRedef11; }

    public DbsField getPnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Cc() { return pnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Cc; }

    public DbsField getPnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Yy() { return pnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Yy; }

    public DbsField getPnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Mm() { return pnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Mm; }

    public DbsField getPnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Dd() { return pnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Dd; }

    public DbsField getPnd_Last_Verified_Date() { return pnd_Last_Verified_Date; }

    public DbsGroup getPnd_Last_Verified_DateRedef12() { return pnd_Last_Verified_DateRedef12; }

    public DbsField getPnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Mm() { return pnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Mm; }

    public DbsField getPnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Dd() { return pnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Dd; }

    public DbsField getPnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Cc() { return pnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Cc; }

    public DbsField getPnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Yy() { return pnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Yy; }

    public DbsField getPnd_Frst_Birth_Dte_Yyyymmdd() { return pnd_Frst_Birth_Dte_Yyyymmdd; }

    public DbsGroup getPnd_Frst_Birth_Dte_YyyymmddRedef13() { return pnd_Frst_Birth_Dte_YyyymmddRedef13; }

    public DbsField getPnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Yyyy() { return pnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Yyyy; }

    public DbsField getPnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Mm() { return pnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Mm; }

    public DbsField getPnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Dd() { return pnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Dd; }

    public DbsField getPnd_Frst_Birth_Dte_Mdcy() { return pnd_Frst_Birth_Dte_Mdcy; }

    public DbsGroup getPnd_Frst_Birth_Dte_MdcyRedef14() { return pnd_Frst_Birth_Dte_MdcyRedef14; }

    public DbsField getPnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Mm() { return pnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Mm; }

    public DbsField getPnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Dd() { return pnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Dd; }

    public DbsField getPnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Yyyy() { return pnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Yyyy; }

    public DbsField getPnd_Scnd_Birth_Dte_Yyyymmdd() { return pnd_Scnd_Birth_Dte_Yyyymmdd; }

    public DbsGroup getPnd_Scnd_Birth_Dte_YyyymmddRedef15() { return pnd_Scnd_Birth_Dte_YyyymmddRedef15; }

    public DbsField getPnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Yyyy() { return pnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Yyyy; }

    public DbsField getPnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Mm() { return pnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Mm; }

    public DbsField getPnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Dd() { return pnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Dd; }

    public DbsField getPnd_Scnd_Birth_Dte_Mdcy() { return pnd_Scnd_Birth_Dte_Mdcy; }

    public DbsGroup getPnd_Scnd_Birth_Dte_MdcyRedef16() { return pnd_Scnd_Birth_Dte_MdcyRedef16; }

    public DbsField getPnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Mm() { return pnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Mm; }

    public DbsField getPnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Dd() { return pnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Dd; }

    public DbsField getPnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Yyyy() { return pnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Yyyy; }

    public DbsField getPnd_Rtb_Account_Dte_Yyyymmdd() { return pnd_Rtb_Account_Dte_Yyyymmdd; }

    public DbsGroup getPnd_Rtb_Account_Dte_YyyymmddRedef17() { return pnd_Rtb_Account_Dte_YyyymmddRedef17; }

    public DbsField getPnd_Rtb_Account_Dte_Yyyymmdd_Pnd_Rtb_Account_Dte_Yyyy() { return pnd_Rtb_Account_Dte_Yyyymmdd_Pnd_Rtb_Account_Dte_Yyyy; }

    public DbsField getPnd_Rtb_Account_Dte_Yyyymmdd_Pnd_Rtb_Account_Dte_Mm() { return pnd_Rtb_Account_Dte_Yyyymmdd_Pnd_Rtb_Account_Dte_Mm; }

    public DbsField getPnd_Rtb_Account_Dte_Yyyymmdd_Pnd_Rtb_Account_Dte_Dd() { return pnd_Rtb_Account_Dte_Yyyymmdd_Pnd_Rtb_Account_Dte_Dd; }

    public DbsField getPnd_Rtb_Account_Dte_Mdcy() { return pnd_Rtb_Account_Dte_Mdcy; }

    public DbsGroup getPnd_Rtb_Account_Dte_MdcyRedef18() { return pnd_Rtb_Account_Dte_MdcyRedef18; }

    public DbsField getPnd_Rtb_Account_Dte_Mdcy_Pnd_Rtb_Account_Dte_Mdcy_Mm() { return pnd_Rtb_Account_Dte_Mdcy_Pnd_Rtb_Account_Dte_Mdcy_Mm; }

    public DbsField getPnd_Rtb_Account_Dte_Mdcy_Pnd_Rtb_Account_Dte_Mdcy_Dd() { return pnd_Rtb_Account_Dte_Mdcy_Pnd_Rtb_Account_Dte_Mdcy_Dd; }

    public DbsField getPnd_Rtb_Account_Dte_Mdcy_Pnd_Rtb_Account_Dte_Mdcy_Yyyy() { return pnd_Rtb_Account_Dte_Mdcy_Pnd_Rtb_Account_Dte_Mdcy_Yyyy; }

    public DbsField getPnd_Rtb_Settle_Dte_Yyyymmdd() { return pnd_Rtb_Settle_Dte_Yyyymmdd; }

    public DbsGroup getPnd_Rtb_Settle_Dte_YyyymmddRedef19() { return pnd_Rtb_Settle_Dte_YyyymmddRedef19; }

    public DbsField getPnd_Rtb_Settle_Dte_Yyyymmdd_Pnd_Rtb_Settle_Dte_Yyyy() { return pnd_Rtb_Settle_Dte_Yyyymmdd_Pnd_Rtb_Settle_Dte_Yyyy; }

    public DbsField getPnd_Rtb_Settle_Dte_Yyyymmdd_Pnd_Rtb_Settle_Dte_Mm() { return pnd_Rtb_Settle_Dte_Yyyymmdd_Pnd_Rtb_Settle_Dte_Mm; }

    public DbsField getPnd_Rtb_Settle_Dte_Yyyymmdd_Pnd_Rtb_Settle_Dte_Dd() { return pnd_Rtb_Settle_Dte_Yyyymmdd_Pnd_Rtb_Settle_Dte_Dd; }

    public DbsField getPnd_Rtb_Settle_Dte_Mdcy() { return pnd_Rtb_Settle_Dte_Mdcy; }

    public DbsGroup getPnd_Rtb_Settle_Dte_MdcyRedef20() { return pnd_Rtb_Settle_Dte_MdcyRedef20; }

    public DbsField getPnd_Rtb_Settle_Dte_Mdcy_Pnd_Rtb_Settle_Dte_Mdcy_Mm() { return pnd_Rtb_Settle_Dte_Mdcy_Pnd_Rtb_Settle_Dte_Mdcy_Mm; }

    public DbsField getPnd_Rtb_Settle_Dte_Mdcy_Pnd_Rtb_Settle_Dte_Mdcy_Dd() { return pnd_Rtb_Settle_Dte_Mdcy_Pnd_Rtb_Settle_Dte_Mdcy_Dd; }

    public DbsField getPnd_Rtb_Settle_Dte_Mdcy_Pnd_Rtb_Settle_Dte_Mdcy_Yyyy() { return pnd_Rtb_Settle_Dte_Mdcy_Pnd_Rtb_Settle_Dte_Mdcy_Yyyy; }

    public DbsField getPnd_Cont_Issue_Dte_Yyyymmdd() { return pnd_Cont_Issue_Dte_Yyyymmdd; }

    public DbsGroup getPnd_Cont_Issue_Dte_YyyymmddRedef21() { return pnd_Cont_Issue_Dte_YyyymmddRedef21; }

    public DbsField getPnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Yyyy() { return pnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Yyyy; }

    public DbsField getPnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Mm() { return pnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Mm; }

    public DbsField getPnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Dd() { return pnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Dd; }

    public DbsField getPnd_Cont_Issue_Dte_Mdcy() { return pnd_Cont_Issue_Dte_Mdcy; }

    public DbsGroup getPnd_Cont_Issue_Dte_MdcyRedef22() { return pnd_Cont_Issue_Dte_MdcyRedef22; }

    public DbsField getPnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Mm() { return pnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Mm; }

    public DbsField getPnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Dd() { return pnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Dd; }

    public DbsField getPnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Yyyy() { return pnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Yyyy; }

    public DbsField getPnd_Annt_Start_Dte_Yyyymmdd() { return pnd_Annt_Start_Dte_Yyyymmdd; }

    public DbsGroup getPnd_Annt_Start_Dte_YyyymmddRedef23() { return pnd_Annt_Start_Dte_YyyymmddRedef23; }

    public DbsField getPnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Yyyy() { return pnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Yyyy; }

    public DbsField getPnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Mm() { return pnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Mm; }

    public DbsField getPnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Dd() { return pnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Dd; }

    public DbsField getPnd_Annt_Start_Dte_Mdcy() { return pnd_Annt_Start_Dte_Mdcy; }

    public DbsGroup getPnd_Annt_Start_Dte_MdcyRedef24() { return pnd_Annt_Start_Dte_MdcyRedef24; }

    public DbsField getPnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Mm() { return pnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Mm; }

    public DbsField getPnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Dd() { return pnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Dd; }

    public DbsField getPnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Yyyy() { return pnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Yyyy; }

    public DbsField getPnd_Frst_Perod_Dte_Yyyymmdd() { return pnd_Frst_Perod_Dte_Yyyymmdd; }

    public DbsGroup getPnd_Frst_Perod_Dte_YyyymmddRedef25() { return pnd_Frst_Perod_Dte_YyyymmddRedef25; }

    public DbsField getPnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Yyyy() { return pnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Yyyy; }

    public DbsField getPnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Mm() { return pnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Mm; }

    public DbsField getPnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Dd() { return pnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Dd; }

    public DbsField getPnd_Frst_Perod_Dte_Mdcy() { return pnd_Frst_Perod_Dte_Mdcy; }

    public DbsGroup getPnd_Frst_Perod_Dte_MdcyRedef26() { return pnd_Frst_Perod_Dte_MdcyRedef26; }

    public DbsField getPnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Mm() { return pnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Mm; }

    public DbsField getPnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Dd() { return pnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Dd; }

    public DbsField getPnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Yyyy() { return pnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Yyyy; }

    public DbsField getPnd_Last_Prem_Dte_Yyyymmdd() { return pnd_Last_Prem_Dte_Yyyymmdd; }

    public DbsGroup getPnd_Last_Prem_Dte_YyyymmddRedef27() { return pnd_Last_Prem_Dte_YyyymmddRedef27; }

    public DbsField getPnd_Last_Prem_Dte_Yyyymmdd_Pnd_Last_Prem_Dte_Yyyy() { return pnd_Last_Prem_Dte_Yyyymmdd_Pnd_Last_Prem_Dte_Yyyy; }

    public DbsField getPnd_Last_Prem_Dte_Yyyymmdd_Pnd_Last_Prem_Dte_Mm() { return pnd_Last_Prem_Dte_Yyyymmdd_Pnd_Last_Prem_Dte_Mm; }

    public DbsField getPnd_Last_Prem_Dte_Yyyymmdd_Pnd_Last_Prem_Dte_Dd() { return pnd_Last_Prem_Dte_Yyyymmdd_Pnd_Last_Prem_Dte_Dd; }

    public DbsField getPnd_Last_Prem_Dte_Mdcy() { return pnd_Last_Prem_Dte_Mdcy; }

    public DbsGroup getPnd_Last_Prem_Dte_MdcyRedef28() { return pnd_Last_Prem_Dte_MdcyRedef28; }

    public DbsField getPnd_Last_Prem_Dte_Mdcy_Pnd_Last_Prem_Dte_Mdcy_Mm() { return pnd_Last_Prem_Dte_Mdcy_Pnd_Last_Prem_Dte_Mdcy_Mm; }

    public DbsField getPnd_Last_Prem_Dte_Mdcy_Pnd_Last_Prem_Dte_Mdcy_Dd() { return pnd_Last_Prem_Dte_Mdcy_Pnd_Last_Prem_Dte_Mdcy_Dd; }

    public DbsField getPnd_Last_Prem_Dte_Mdcy_Pnd_Last_Prem_Dte_Mdcy_Yyyy() { return pnd_Last_Prem_Dte_Mdcy_Pnd_Last_Prem_Dte_Mdcy_Yyyy; }

    public DbsField getPnd_Last_Guar_Dte_Yyyymmdd() { return pnd_Last_Guar_Dte_Yyyymmdd; }

    public DbsGroup getPnd_Last_Guar_Dte_YyyymmddRedef29() { return pnd_Last_Guar_Dte_YyyymmddRedef29; }

    public DbsField getPnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Yyyy() { return pnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Yyyy; }

    public DbsField getPnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Mm() { return pnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Mm; }

    public DbsField getPnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Dd() { return pnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Dd; }

    public DbsField getPnd_Last_Guar_Dte_Mdcy() { return pnd_Last_Guar_Dte_Mdcy; }

    public DbsGroup getPnd_Last_Guar_Dte_MdcyRedef30() { return pnd_Last_Guar_Dte_MdcyRedef30; }

    public DbsField getPnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Mm() { return pnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Mm; }

    public DbsField getPnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Dd() { return pnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Dd; }

    public DbsField getPnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Yyyy() { return pnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Yyyy; }

    public DbsField getPnd_Empl_Term_Dte_Yyyymmdd() { return pnd_Empl_Term_Dte_Yyyymmdd; }

    public DbsGroup getPnd_Empl_Term_Dte_YyyymmddRedef31() { return pnd_Empl_Term_Dte_YyyymmddRedef31; }

    public DbsField getPnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Yyyy() { return pnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Yyyy; }

    public DbsField getPnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Mm() { return pnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Mm; }

    public DbsField getPnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Dd() { return pnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Dd; }

    public DbsField getPnd_Empl_Term_Dte_Mdcy() { return pnd_Empl_Term_Dte_Mdcy; }

    public DbsGroup getPnd_Empl_Term_Dte_MdcyRedef32() { return pnd_Empl_Term_Dte_MdcyRedef32; }

    public DbsField getPnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Mm() { return pnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Mm; }

    public DbsField getPnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Dd() { return pnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Dd; }

    public DbsField getPnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Yyyy() { return pnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Yyyy; }

    public DbsField getPnd_Form_Comp_Dte_Yyyymmdd() { return pnd_Form_Comp_Dte_Yyyymmdd; }

    public DbsGroup getPnd_Form_Comp_Dte_YyyymmddRedef33() { return pnd_Form_Comp_Dte_YyyymmddRedef33; }

    public DbsField getPnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Yyyy() { return pnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Yyyy; }

    public DbsField getPnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Mm() { return pnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Mm; }

    public DbsField getPnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Dd() { return pnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Dd; }

    public DbsField getPnd_Form_Comp_Dte_Mdcy() { return pnd_Form_Comp_Dte_Mdcy; }

    public DbsGroup getPnd_Form_Comp_Dte_MdcyRedef34() { return pnd_Form_Comp_Dte_MdcyRedef34; }

    public DbsField getPnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Mm() { return pnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Mm; }

    public DbsField getPnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Dd() { return pnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Dd; }

    public DbsField getPnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Yyyy() { return pnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Yyyy; }

    public DbsField getPnd_Effective_Dte_Yyyymmdd() { return pnd_Effective_Dte_Yyyymmdd; }

    public DbsGroup getPnd_Effective_Dte_YyyymmddRedef35() { return pnd_Effective_Dte_YyyymmddRedef35; }

    public DbsField getPnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Yyyy() { return pnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Yyyy; }

    public DbsField getPnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Mm() { return pnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Mm; }

    public DbsField getPnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Dd() { return pnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Dd; }

    public DbsField getPnd_Effective_Dte_Mdcy() { return pnd_Effective_Dte_Mdcy; }

    public DbsGroup getPnd_Effective_Dte_MdcyRedef36() { return pnd_Effective_Dte_MdcyRedef36; }

    public DbsField getPnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Mm() { return pnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Mm; }

    public DbsField getPnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Dd() { return pnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Dd; }

    public DbsField getPnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Yyyy() { return pnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Yyyy; }

    public DbsField getPnd_Payment_Dte_Yyyymmdd() { return pnd_Payment_Dte_Yyyymmdd; }

    public DbsGroup getPnd_Payment_Dte_YyyymmddRedef37() { return pnd_Payment_Dte_YyyymmddRedef37; }

    public DbsField getPnd_Payment_Dte_Yyyymmdd_Pnd_Payment_Dte_Yyyy() { return pnd_Payment_Dte_Yyyymmdd_Pnd_Payment_Dte_Yyyy; }

    public DbsField getPnd_Payment_Dte_Yyyymmdd_Pnd_Payment_Dte_Mm() { return pnd_Payment_Dte_Yyyymmdd_Pnd_Payment_Dte_Mm; }

    public DbsField getPnd_Payment_Dte_Yyyymmdd_Pnd_Payment_Dte_Dd() { return pnd_Payment_Dte_Yyyymmdd_Pnd_Payment_Dte_Dd; }

    public DbsField getPnd_Payment_Dte_Mdcy() { return pnd_Payment_Dte_Mdcy; }

    public DbsGroup getPnd_Payment_Dte_MdcyRedef38() { return pnd_Payment_Dte_MdcyRedef38; }

    public DbsField getPnd_Payment_Dte_Mdcy_Pnd_Payment_Dte_Mdcy_Mm() { return pnd_Payment_Dte_Mdcy_Pnd_Payment_Dte_Mdcy_Mm; }

    public DbsField getPnd_Payment_Dte_Mdcy_Pnd_Payment_Dte_Mdcy_Dd() { return pnd_Payment_Dte_Mdcy_Pnd_Payment_Dte_Mdcy_Dd; }

    public DbsField getPnd_Payment_Dte_Mdcy_Pnd_Payment_Dte_Mdcy_Yyyy() { return pnd_Payment_Dte_Mdcy_Pnd_Payment_Dte_Mdcy_Yyyy; }

    public DbsField getPnd_Account_Dte_Yyyymmdd() { return pnd_Account_Dte_Yyyymmdd; }

    public DbsGroup getPnd_Account_Dte_YyyymmddRedef39() { return pnd_Account_Dte_YyyymmddRedef39; }

    public DbsField getPnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Yyyy() { return pnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Yyyy; }

    public DbsField getPnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Mm() { return pnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Mm; }

    public DbsField getPnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Dd() { return pnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Dd; }

    public DbsField getPnd_Account_Dte_Mdcy() { return pnd_Account_Dte_Mdcy; }

    public DbsGroup getPnd_Account_Dte_MdcyRedef40() { return pnd_Account_Dte_MdcyRedef40; }

    public DbsField getPnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Mm() { return pnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Mm; }

    public DbsField getPnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Dd() { return pnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Dd; }

    public DbsField getPnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Yyyy() { return pnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Yyyy; }

    public DbsField getPnd_Non_Prem_Dte_Yyyymmdd() { return pnd_Non_Prem_Dte_Yyyymmdd; }

    public DbsGroup getPnd_Non_Prem_Dte_YyyymmddRedef41() { return pnd_Non_Prem_Dte_YyyymmddRedef41; }

    public DbsField getPnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Yyyy() { return pnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Yyyy; }

    public DbsField getPnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Mm() { return pnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Mm; }

    public DbsField getPnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Dd() { return pnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Dd; }

    public DbsField getPnd_Non_Prem_Dte_Mdcy() { return pnd_Non_Prem_Dte_Mdcy; }

    public DbsGroup getPnd_Non_Prem_Dte_MdcyRedef42() { return pnd_Non_Prem_Dte_MdcyRedef42; }

    public DbsField getPnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Mm() { return pnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Mm; }

    public DbsField getPnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Dd() { return pnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Dd; }

    public DbsField getPnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Yyyy() { return pnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Yyyy; }

    public DbsField getPnd_Nap_Effective_Date() { return pnd_Nap_Effective_Date; }

    public DbsGroup getPnd_Nap_Effective_DateRedef43() { return pnd_Nap_Effective_DateRedef43; }

    public DbsField getPnd_Nap_Effective_Date_Pnd_Nap_Effective_Date_Cc() { return pnd_Nap_Effective_Date_Pnd_Nap_Effective_Date_Cc; }

    public DbsField getPnd_Nap_Effective_Date_Pnd_Nap_Effective_Date_Yy() { return pnd_Nap_Effective_Date_Pnd_Nap_Effective_Date_Yy; }

    public DbsField getPnd_Nap_Effective_Date_Pnd_Nap_Effective_Date_Mm() { return pnd_Nap_Effective_Date_Pnd_Nap_Effective_Date_Mm; }

    public DbsField getPnd_Nap_Effective_Date_Pnd_Nap_Effective_Date_Dd() { return pnd_Nap_Effective_Date_Pnd_Nap_Effective_Date_Dd; }

    public DbsField getPnd_T_Tpa_Io_Lit() { return pnd_T_Tpa_Io_Lit; }

    public DbsField getPnd_Dash_666() { return pnd_Dash_666; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Rollover_Contract_Nbr = newFieldArrayInRecord("pnd_Rollover_Contract_Nbr", "#ROLLOVER-CONTRACT-NBR", FieldType.STRING, 10, new DbsArrayController(1,
            2));

        pnd_Rollover_Amount = newFieldInRecord("pnd_Rollover_Amount", "#ROLLOVER-AMOUNT", FieldType.DECIMAL, 11,2);

        pnd_Tiaa_Contract_Nbr = newFieldInRecord("pnd_Tiaa_Contract_Nbr", "#TIAA-CONTRACT-NBR", FieldType.STRING, 10);

        pnd_Prev_Tiaa_Contract_Nbr = newFieldInRecord("pnd_Prev_Tiaa_Contract_Nbr", "#PREV-TIAA-CONTRACT-NBR", FieldType.STRING, 10);

        pnd_Cref_Certificate_Nbr = newFieldInRecord("pnd_Cref_Certificate_Nbr", "#CREF-CERTIFICATE-NBR", FieldType.STRING, 10);

        pnd_Nap_Mit_Log_Dte_Tme = newFieldInRecord("pnd_Nap_Mit_Log_Dte_Tme", "#NAP-MIT-LOG-DTE-TME", FieldType.STRING, 15);

        pnd_Nap_Bps_Unit = newFieldInRecord("pnd_Nap_Bps_Unit", "#NAP-BPS-UNIT", FieldType.STRING, 8);

        pnd_Page_Number = newFieldInRecord("pnd_Page_Number", "#PAGE-NUMBER", FieldType.NUMERIC, 8);

        pnd_Page_Number_Roth = newFieldInRecord("pnd_Page_Number_Roth", "#PAGE-NUMBER-ROTH", FieldType.NUMERIC, 8);

        pnd_Page_Number_Surv = newFieldInRecord("pnd_Page_Number_Surv", "#PAGE-NUMBER-SURV", FieldType.NUMERIC, 8);

        pnd_Ia_Number = newFieldInRecord("pnd_Ia_Number", "#IA-NUMBER", FieldType.STRING, 10);

        pnd_Social_Sec_Nbr_1 = newFieldInRecord("pnd_Social_Sec_Nbr_1", "#SOCIAL-SEC-NBR-1", FieldType.NUMERIC, 9);

        pnd_Social_Sec_Nbr_2 = newFieldInRecord("pnd_Social_Sec_Nbr_2", "#SOCIAL-SEC-NBR-2", FieldType.NUMERIC, 9);

        pnd_Sex_1 = newFieldInRecord("pnd_Sex_1", "#SEX-1", FieldType.STRING, 1);

        pnd_Sex_2 = newFieldInRecord("pnd_Sex_2", "#SEX-2", FieldType.STRING, 1);

        pnd_Scnd_Spouse = newFieldInRecord("pnd_Scnd_Spouse", "#SCND-SPOUSE", FieldType.STRING, 1);

        pnd_Frst_Citizenship = newFieldInRecord("pnd_Frst_Citizenship", "#FRST-CITIZENSHIP", FieldType.STRING, 2);

        pnd_Scnd_Citizenship = newFieldInRecord("pnd_Scnd_Citizenship", "#SCND-CITIZENSHIP", FieldType.STRING, 2);

        pnd_Work_Rqst_Id = newFieldInRecord("pnd_Work_Rqst_Id", "#WORK-RQST-ID", FieldType.STRING, 6);

        pnd_Nap_Annty_Optn = newFieldInRecord("pnd_Nap_Annty_Optn", "#NAP-ANNTY-OPTN", FieldType.STRING, 2);

        pnd_Nap_Pymnt_Mode_All = newFieldInRecord("pnd_Nap_Pymnt_Mode_All", "#NAP-PYMNT-MODE-ALL", FieldType.STRING, 20);

        pnd_Rtb = newFieldInRecord("pnd_Rtb", "#RTB", FieldType.STRING, 1);

        pnd_Slash = newFieldInRecord("pnd_Slash", "#SLASH", FieldType.STRING, 1);

        pnd_Nap_Grntee_Period = newFieldInRecord("pnd_Nap_Grntee_Period", "#NAP-GRNTEE-PERIOD", FieldType.NUMERIC, 2);

        pnd_Partcipant_Name_1 = newFieldInRecord("pnd_Partcipant_Name_1", "#PARTCIPANT-NAME-1", FieldType.STRING, 30);

        pnd_Partcipant_Name_2 = newFieldInRecord("pnd_Partcipant_Name_2", "#PARTCIPANT-NAME-2", FieldType.STRING, 30);

        pnd_Residence = newFieldInRecord("pnd_Residence", "#RESIDENCE", FieldType.STRING, 20);

        pnd_Rqst_Id = newFieldInRecord("pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 35);
        pnd_Rqst_IdRedef1 = newGroupInRecord("pnd_Rqst_IdRedef1", "Redefines", pnd_Rqst_Id);
        pnd_Rqst_Id_Pnd_Unique_Id = pnd_Rqst_IdRedef1.newFieldInGroup("pnd_Rqst_Id_Pnd_Unique_Id", "#UNIQUE-ID", FieldType.NUMERIC, 12);
        pnd_Rqst_Id_Pnd_Effctv_Dte = pnd_Rqst_IdRedef1.newFieldInGroup("pnd_Rqst_Id_Pnd_Effctv_Dte", "#EFFCTV-DTE", FieldType.NUMERIC, 8);
        pnd_Rqst_Id_Pnd_Entry_Dte = pnd_Rqst_IdRedef1.newFieldInGroup("pnd_Rqst_Id_Pnd_Entry_Dte", "#ENTRY-DTE", FieldType.NUMERIC, 8);
        pnd_Rqst_Id_Pnd_Entry_Tme = pnd_Rqst_IdRedef1.newFieldInGroup("pnd_Rqst_Id_Pnd_Entry_Tme", "#ENTRY-TME", FieldType.NUMERIC, 7);

        pnd_Prev_Da_Rqst_Id = newFieldInRecord("pnd_Prev_Da_Rqst_Id", "#PREV-DA-RQST-ID", FieldType.STRING, 35);
        pnd_Prev_Da_Rqst_IdRedef2 = newGroupInRecord("pnd_Prev_Da_Rqst_IdRedef2", "Redefines", pnd_Prev_Da_Rqst_Id);
        pnd_Prev_Da_Rqst_Id_Pnd_Prev_Da_Unique_Id = pnd_Prev_Da_Rqst_IdRedef2.newFieldInGroup("pnd_Prev_Da_Rqst_Id_Pnd_Prev_Da_Unique_Id", "#PREV-DA-UNIQUE-ID", 
            FieldType.NUMERIC, 12);
        pnd_Prev_Da_Rqst_Id_Pnd_Prev_Da_Effctv_Dte = pnd_Prev_Da_Rqst_IdRedef2.newFieldInGroup("pnd_Prev_Da_Rqst_Id_Pnd_Prev_Da_Effctv_Dte", "#PREV-DA-EFFCTV-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Prev_Da_Rqst_Id_Pnd_Prev_Da_Entry_Dte = pnd_Prev_Da_Rqst_IdRedef2.newFieldInGroup("pnd_Prev_Da_Rqst_Id_Pnd_Prev_Da_Entry_Dte", "#PREV-DA-ENTRY-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Prev_Da_Rqst_Id_Pnd_Prev_Da_Entry_Tme = pnd_Prev_Da_Rqst_IdRedef2.newFieldInGroup("pnd_Prev_Da_Rqst_Id_Pnd_Prev_Da_Entry_Tme", "#PREV-DA-ENTRY-TME", 
            FieldType.NUMERIC, 7);

        pnd_Rtb_Rollover_Code = newGroupArrayInRecord("pnd_Rtb_Rollover_Code", "#RTB-ROLLOVER-CODE", new DbsArrayController(1,3));
        pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde = pnd_Rtb_Rollover_Code.newFieldInGroup("pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde", "#RTB-ROLLOVER-CDE", 
            FieldType.STRING, 2);

        pnd_Destination_Code = newGroupArrayInRecord("pnd_Destination_Code", "#DESTINATION-CODE", new DbsArrayController(1,3));
        pnd_Destination_Code_Pnd_Destination_Cde = pnd_Destination_Code.newFieldInGroup("pnd_Destination_Code_Pnd_Destination_Cde", "#DESTINATION-CDE", 
            FieldType.STRING, 5);

        pnd_Target_All = newGroupInRecord("pnd_Target_All", "#TARGET-ALL");
        pnd_Target_All_Pnd_Target_All_No = pnd_Target_All.newGroupArrayInGroup("pnd_Target_All_Pnd_Target_All_No", "#TARGET-ALL-NO", new DbsArrayController(1,
            15));
        pnd_Target_All_Pnd_Target_Number = pnd_Target_All_Pnd_Target_All_No.newFieldInGroup("pnd_Target_All_Pnd_Target_Number", "#TARGET-NUMBER", FieldType.STRING, 
            6);
        pnd_Target_All_Pnd_Target_Disc = pnd_Target_All_Pnd_Target_All_No.newFieldInGroup("pnd_Target_All_Pnd_Target_Disc", "#TARGET-DISC", FieldType.STRING, 
            5);

        pnd_Cref_Cert_Nbr = newFieldArrayInRecord("pnd_Cref_Cert_Nbr", "#CREF-CERT-NBR", FieldType.STRING, 11, new DbsArrayController(1,10));

        pnd_Cref_Cert_Ind = newFieldArrayInRecord("pnd_Cref_Cert_Ind", "#CREF-CERT-IND", FieldType.STRING, 6, new DbsArrayController(1,10));

        pnd_Tiaa_Cert_Nbr = newFieldArrayInRecord("pnd_Tiaa_Cert_Nbr", "#TIAA-CERT-NBR", FieldType.STRING, 11, new DbsArrayController(1,10));

        pnd_Tiaa_Cert_Ind = newFieldArrayInRecord("pnd_Tiaa_Cert_Ind", "#TIAA-CERT-IND", FieldType.STRING, 6, new DbsArrayController(1,10));

        pnd_Final_Prem_Ind = newFieldInRecord("pnd_Final_Prem_Ind", "#FINAL-PREM-IND", FieldType.STRING, 14);

        pnd_Da_Accum_Dte_Yyyymmdd = newFieldInRecord("pnd_Da_Accum_Dte_Yyyymmdd", "#DA-ACCUM-DTE-YYYYMMDD", FieldType.STRING, 8);
        pnd_Da_Accum_Dte_YyyymmddRedef3 = newGroupInRecord("pnd_Da_Accum_Dte_YyyymmddRedef3", "Redefines", pnd_Da_Accum_Dte_Yyyymmdd);
        pnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Cc = pnd_Da_Accum_Dte_YyyymmddRedef3.newFieldInGroup("pnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Cc", 
            "#DA-ACCUM-DTE-CC", FieldType.NUMERIC, 2);
        pnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Yy = pnd_Da_Accum_Dte_YyyymmddRedef3.newFieldInGroup("pnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Yy", 
            "#DA-ACCUM-DTE-YY", FieldType.NUMERIC, 2);
        pnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Mm = pnd_Da_Accum_Dte_YyyymmddRedef3.newFieldInGroup("pnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Mm", 
            "#DA-ACCUM-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Dd = pnd_Da_Accum_Dte_YyyymmddRedef3.newFieldInGroup("pnd_Da_Accum_Dte_Yyyymmdd_Pnd_Da_Accum_Dte_Dd", 
            "#DA-ACCUM-DTE-DD", FieldType.NUMERIC, 2);

        pnd_Da_Accum_Date = newFieldInRecord("pnd_Da_Accum_Date", "#DA-ACCUM-DATE", FieldType.NUMERIC, 6);
        pnd_Da_Accum_DateRedef4 = newGroupInRecord("pnd_Da_Accum_DateRedef4", "Redefines", pnd_Da_Accum_Date);
        pnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Mm = pnd_Da_Accum_DateRedef4.newFieldInGroup("pnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Mm", "#DA-ACCUM-DTE-MDCY-MM", 
            FieldType.NUMERIC, 2);
        pnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Dd = pnd_Da_Accum_DateRedef4.newFieldInGroup("pnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Dd", "#DA-ACCUM-DTE-MDCY-DD", 
            FieldType.NUMERIC, 2);
        pnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Yy = pnd_Da_Accum_DateRedef4.newFieldInGroup("pnd_Da_Accum_Date_Pnd_Da_Accum_Dte_Mdcy_Yy", "#DA-ACCUM-DTE-MDCY-YY", 
            FieldType.NUMERIC, 2);

        pnd_First_Entered_Yyyymmdd = newFieldInRecord("pnd_First_Entered_Yyyymmdd", "#FIRST-ENTERED-YYYYMMDD", FieldType.STRING, 8);
        pnd_First_Entered_YyyymmddRedef5 = newGroupInRecord("pnd_First_Entered_YyyymmddRedef5", "Redefines", pnd_First_Entered_Yyyymmdd);
        pnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Cc = pnd_First_Entered_YyyymmddRedef5.newFieldInGroup("pnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Cc", 
            "#FIRST-ENTERED-CC", FieldType.NUMERIC, 2);
        pnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Yy = pnd_First_Entered_YyyymmddRedef5.newFieldInGroup("pnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Yy", 
            "#FIRST-ENTERED-YY", FieldType.NUMERIC, 2);
        pnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Mm = pnd_First_Entered_YyyymmddRedef5.newFieldInGroup("pnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Mm", 
            "#FIRST-ENTERED-MM", FieldType.NUMERIC, 2);
        pnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Dd = pnd_First_Entered_YyyymmddRedef5.newFieldInGroup("pnd_First_Entered_Yyyymmdd_Pnd_First_Entered_Dd", 
            "#FIRST-ENTERED-DD", FieldType.NUMERIC, 2);

        pnd_First_Entered_Date = newFieldInRecord("pnd_First_Entered_Date", "#FIRST-ENTERED-DATE", FieldType.NUMERIC, 8);
        pnd_First_Entered_DateRedef6 = newGroupInRecord("pnd_First_Entered_DateRedef6", "Redefines", pnd_First_Entered_Date);
        pnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Mm = pnd_First_Entered_DateRedef6.newFieldInGroup("pnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Mm", 
            "#FIRST-ENTERED-DTE-MDCY-MM", FieldType.NUMERIC, 2);
        pnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Dd = pnd_First_Entered_DateRedef6.newFieldInGroup("pnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Dd", 
            "#FIRST-ENTERED-DTE-MDCY-DD", FieldType.NUMERIC, 2);
        pnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Cc = pnd_First_Entered_DateRedef6.newFieldInGroup("pnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Cc", 
            "#FIRST-ENTERED-DTE-MDCY-CC", FieldType.NUMERIC, 2);
        pnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Yy = pnd_First_Entered_DateRedef6.newFieldInGroup("pnd_First_Entered_Date_Pnd_First_Entered_Dte_Mdcy_Yy", 
            "#FIRST-ENTERED-DTE-MDCY-YY", FieldType.NUMERIC, 2);

        pnd_First_Verified_Yyyymmdd = newFieldInRecord("pnd_First_Verified_Yyyymmdd", "#FIRST-VERIFIED-YYYYMMDD", FieldType.STRING, 8);
        pnd_First_Verified_YyyymmddRedef7 = newGroupInRecord("pnd_First_Verified_YyyymmddRedef7", "Redefines", pnd_First_Verified_Yyyymmdd);
        pnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Cc = pnd_First_Verified_YyyymmddRedef7.newFieldInGroup("pnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Cc", 
            "#FIRST-VERIFIED-CC", FieldType.NUMERIC, 2);
        pnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Yy = pnd_First_Verified_YyyymmddRedef7.newFieldInGroup("pnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Yy", 
            "#FIRST-VERIFIED-YY", FieldType.NUMERIC, 2);
        pnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Mm = pnd_First_Verified_YyyymmddRedef7.newFieldInGroup("pnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Mm", 
            "#FIRST-VERIFIED-MM", FieldType.NUMERIC, 2);
        pnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Dd = pnd_First_Verified_YyyymmddRedef7.newFieldInGroup("pnd_First_Verified_Yyyymmdd_Pnd_First_Verified_Dd", 
            "#FIRST-VERIFIED-DD", FieldType.NUMERIC, 2);

        pnd_First_Verified_Date = newFieldInRecord("pnd_First_Verified_Date", "#FIRST-VERIFIED-DATE", FieldType.NUMERIC, 8);
        pnd_First_Verified_DateRedef8 = newGroupInRecord("pnd_First_Verified_DateRedef8", "Redefines", pnd_First_Verified_Date);
        pnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Mm = pnd_First_Verified_DateRedef8.newFieldInGroup("pnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Mm", 
            "#FIRST-VERIFIED-DTE-MDCY-MM", FieldType.NUMERIC, 2);
        pnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Dd = pnd_First_Verified_DateRedef8.newFieldInGroup("pnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Dd", 
            "#FIRST-VERIFIED-DTE-MDCY-DD", FieldType.NUMERIC, 2);
        pnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Cc = pnd_First_Verified_DateRedef8.newFieldInGroup("pnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Cc", 
            "#FIRST-VERIFIED-DTE-MDCY-CC", FieldType.NUMERIC, 2);
        pnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Yy = pnd_First_Verified_DateRedef8.newFieldInGroup("pnd_First_Verified_Date_Pnd_First_Verified_Dte_Mdcy_Yy", 
            "#FIRST-VERIFIED-DTE-MDCY-YY", FieldType.NUMERIC, 2);

        pnd_Last_Updated_Yyyymmdd = newFieldInRecord("pnd_Last_Updated_Yyyymmdd", "#LAST-UPDATED-YYYYMMDD", FieldType.STRING, 8);
        pnd_Last_Updated_YyyymmddRedef9 = newGroupInRecord("pnd_Last_Updated_YyyymmddRedef9", "Redefines", pnd_Last_Updated_Yyyymmdd);
        pnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Cc = pnd_Last_Updated_YyyymmddRedef9.newFieldInGroup("pnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Cc", 
            "#LAST-UPDATED-CC", FieldType.NUMERIC, 2);
        pnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Yy = pnd_Last_Updated_YyyymmddRedef9.newFieldInGroup("pnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Yy", 
            "#LAST-UPDATED-YY", FieldType.NUMERIC, 2);
        pnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Mm = pnd_Last_Updated_YyyymmddRedef9.newFieldInGroup("pnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Mm", 
            "#LAST-UPDATED-MM", FieldType.NUMERIC, 2);
        pnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Dd = pnd_Last_Updated_YyyymmddRedef9.newFieldInGroup("pnd_Last_Updated_Yyyymmdd_Pnd_Last_Updated_Dd", 
            "#LAST-UPDATED-DD", FieldType.NUMERIC, 2);

        pnd_Last_Updated_Date = newFieldInRecord("pnd_Last_Updated_Date", "#LAST-UPDATED-DATE", FieldType.NUMERIC, 8);
        pnd_Last_Updated_DateRedef10 = newGroupInRecord("pnd_Last_Updated_DateRedef10", "Redefines", pnd_Last_Updated_Date);
        pnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Mm = pnd_Last_Updated_DateRedef10.newFieldInGroup("pnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Mm", 
            "#LAST-UPDATED-DTE-MDCY-MM", FieldType.NUMERIC, 2);
        pnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Dd = pnd_Last_Updated_DateRedef10.newFieldInGroup("pnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Dd", 
            "#LAST-UPDATED-DTE-MDCY-DD", FieldType.NUMERIC, 2);
        pnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Cc = pnd_Last_Updated_DateRedef10.newFieldInGroup("pnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Cc", 
            "#LAST-UPDATED-DTE-MDCY-CC", FieldType.NUMERIC, 2);
        pnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Yy = pnd_Last_Updated_DateRedef10.newFieldInGroup("pnd_Last_Updated_Date_Pnd_Last_Updated_Dte_Mdcy_Yy", 
            "#LAST-UPDATED-DTE-MDCY-YY", FieldType.NUMERIC, 2);

        pnd_Last_Verified_Yyyymmdd = newFieldInRecord("pnd_Last_Verified_Yyyymmdd", "#LAST-VERIFIED-YYYYMMDD", FieldType.STRING, 8);
        pnd_Last_Verified_YyyymmddRedef11 = newGroupInRecord("pnd_Last_Verified_YyyymmddRedef11", "Redefines", pnd_Last_Verified_Yyyymmdd);
        pnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Cc = pnd_Last_Verified_YyyymmddRedef11.newFieldInGroup("pnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Cc", 
            "#LAST-VERIFIED-CC", FieldType.NUMERIC, 2);
        pnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Yy = pnd_Last_Verified_YyyymmddRedef11.newFieldInGroup("pnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Yy", 
            "#LAST-VERIFIED-YY", FieldType.NUMERIC, 2);
        pnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Mm = pnd_Last_Verified_YyyymmddRedef11.newFieldInGroup("pnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Mm", 
            "#LAST-VERIFIED-MM", FieldType.NUMERIC, 2);
        pnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Dd = pnd_Last_Verified_YyyymmddRedef11.newFieldInGroup("pnd_Last_Verified_Yyyymmdd_Pnd_Last_Verified_Dd", 
            "#LAST-VERIFIED-DD", FieldType.NUMERIC, 2);

        pnd_Last_Verified_Date = newFieldInRecord("pnd_Last_Verified_Date", "#LAST-VERIFIED-DATE", FieldType.NUMERIC, 8);
        pnd_Last_Verified_DateRedef12 = newGroupInRecord("pnd_Last_Verified_DateRedef12", "Redefines", pnd_Last_Verified_Date);
        pnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Mm = pnd_Last_Verified_DateRedef12.newFieldInGroup("pnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Mm", 
            "#LAST-VERIFIED-DTE-MDCY-MM", FieldType.NUMERIC, 2);
        pnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Dd = pnd_Last_Verified_DateRedef12.newFieldInGroup("pnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Dd", 
            "#LAST-VERIFIED-DTE-MDCY-DD", FieldType.NUMERIC, 2);
        pnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Cc = pnd_Last_Verified_DateRedef12.newFieldInGroup("pnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Cc", 
            "#LAST-VERIFIED-DTE-MDCY-CC", FieldType.NUMERIC, 2);
        pnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Yy = pnd_Last_Verified_DateRedef12.newFieldInGroup("pnd_Last_Verified_Date_Pnd_Last_Verified_Dte_Mdcy_Yy", 
            "#LAST-VERIFIED-DTE-MDCY-YY", FieldType.NUMERIC, 2);

        pnd_Frst_Birth_Dte_Yyyymmdd = newFieldInRecord("pnd_Frst_Birth_Dte_Yyyymmdd", "#FRST-BIRTH-DTE-YYYYMMDD", FieldType.STRING, 8);
        pnd_Frst_Birth_Dte_YyyymmddRedef13 = newGroupInRecord("pnd_Frst_Birth_Dte_YyyymmddRedef13", "Redefines", pnd_Frst_Birth_Dte_Yyyymmdd);
        pnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Yyyy = pnd_Frst_Birth_Dte_YyyymmddRedef13.newFieldInGroup("pnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Yyyy", 
            "#FRST-BIRTH-DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Mm = pnd_Frst_Birth_Dte_YyyymmddRedef13.newFieldInGroup("pnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Mm", 
            "#FRST-BIRTH-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Dd = pnd_Frst_Birth_Dte_YyyymmddRedef13.newFieldInGroup("pnd_Frst_Birth_Dte_Yyyymmdd_Pnd_Frst_Birth_Dte_Dd", 
            "#FRST-BIRTH-DTE-DD", FieldType.NUMERIC, 2);

        pnd_Frst_Birth_Dte_Mdcy = newFieldInRecord("pnd_Frst_Birth_Dte_Mdcy", "#FRST-BIRTH-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Frst_Birth_Dte_MdcyRedef14 = newGroupInRecord("pnd_Frst_Birth_Dte_MdcyRedef14", "Redefines", pnd_Frst_Birth_Dte_Mdcy);
        pnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Mm = pnd_Frst_Birth_Dte_MdcyRedef14.newFieldInGroup("pnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Mm", 
            "#FRST-BIRTH-DTE-MDCY-MM", FieldType.NUMERIC, 2);
        pnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Dd = pnd_Frst_Birth_Dte_MdcyRedef14.newFieldInGroup("pnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Dd", 
            "#FRST-BIRTH-DTE-MDCY-DD", FieldType.NUMERIC, 2);
        pnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Yyyy = pnd_Frst_Birth_Dte_MdcyRedef14.newFieldInGroup("pnd_Frst_Birth_Dte_Mdcy_Pnd_Frst_Birth_Dte_Mdcy_Yyyy", 
            "#FRST-BIRTH-DTE-MDCY-YYYY", FieldType.NUMERIC, 4);

        pnd_Scnd_Birth_Dte_Yyyymmdd = newFieldInRecord("pnd_Scnd_Birth_Dte_Yyyymmdd", "#SCND-BIRTH-DTE-YYYYMMDD", FieldType.STRING, 8);
        pnd_Scnd_Birth_Dte_YyyymmddRedef15 = newGroupInRecord("pnd_Scnd_Birth_Dte_YyyymmddRedef15", "Redefines", pnd_Scnd_Birth_Dte_Yyyymmdd);
        pnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Yyyy = pnd_Scnd_Birth_Dte_YyyymmddRedef15.newFieldInGroup("pnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Yyyy", 
            "#SCND-BIRTH-DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Mm = pnd_Scnd_Birth_Dte_YyyymmddRedef15.newFieldInGroup("pnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Mm", 
            "#SCND-BIRTH-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Dd = pnd_Scnd_Birth_Dte_YyyymmddRedef15.newFieldInGroup("pnd_Scnd_Birth_Dte_Yyyymmdd_Pnd_Scnd_Birth_Dte_Dd", 
            "#SCND-BIRTH-DTE-DD", FieldType.NUMERIC, 2);

        pnd_Scnd_Birth_Dte_Mdcy = newFieldInRecord("pnd_Scnd_Birth_Dte_Mdcy", "#SCND-BIRTH-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Scnd_Birth_Dte_MdcyRedef16 = newGroupInRecord("pnd_Scnd_Birth_Dte_MdcyRedef16", "Redefines", pnd_Scnd_Birth_Dte_Mdcy);
        pnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Mm = pnd_Scnd_Birth_Dte_MdcyRedef16.newFieldInGroup("pnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Mm", 
            "#SCND-BIRTH-DTE-MDCY-MM", FieldType.NUMERIC, 2);
        pnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Dd = pnd_Scnd_Birth_Dte_MdcyRedef16.newFieldInGroup("pnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Dd", 
            "#SCND-BIRTH-DTE-MDCY-DD", FieldType.NUMERIC, 2);
        pnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Yyyy = pnd_Scnd_Birth_Dte_MdcyRedef16.newFieldInGroup("pnd_Scnd_Birth_Dte_Mdcy_Pnd_Scnd_Birth_Dte_Mdcy_Yyyy", 
            "#SCND-BIRTH-DTE-MDCY-YYYY", FieldType.NUMERIC, 4);

        pnd_Rtb_Account_Dte_Yyyymmdd = newFieldInRecord("pnd_Rtb_Account_Dte_Yyyymmdd", "#RTB-ACCOUNT-DTE-YYYYMMDD", FieldType.STRING, 8);
        pnd_Rtb_Account_Dte_YyyymmddRedef17 = newGroupInRecord("pnd_Rtb_Account_Dte_YyyymmddRedef17", "Redefines", pnd_Rtb_Account_Dte_Yyyymmdd);
        pnd_Rtb_Account_Dte_Yyyymmdd_Pnd_Rtb_Account_Dte_Yyyy = pnd_Rtb_Account_Dte_YyyymmddRedef17.newFieldInGroup("pnd_Rtb_Account_Dte_Yyyymmdd_Pnd_Rtb_Account_Dte_Yyyy", 
            "#RTB-ACCOUNT-DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Rtb_Account_Dte_Yyyymmdd_Pnd_Rtb_Account_Dte_Mm = pnd_Rtb_Account_Dte_YyyymmddRedef17.newFieldInGroup("pnd_Rtb_Account_Dte_Yyyymmdd_Pnd_Rtb_Account_Dte_Mm", 
            "#RTB-ACCOUNT-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Rtb_Account_Dte_Yyyymmdd_Pnd_Rtb_Account_Dte_Dd = pnd_Rtb_Account_Dte_YyyymmddRedef17.newFieldInGroup("pnd_Rtb_Account_Dte_Yyyymmdd_Pnd_Rtb_Account_Dte_Dd", 
            "#RTB-ACCOUNT-DTE-DD", FieldType.NUMERIC, 2);

        pnd_Rtb_Account_Dte_Mdcy = newFieldInRecord("pnd_Rtb_Account_Dte_Mdcy", "#RTB-ACCOUNT-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Rtb_Account_Dte_MdcyRedef18 = newGroupInRecord("pnd_Rtb_Account_Dte_MdcyRedef18", "Redefines", pnd_Rtb_Account_Dte_Mdcy);
        pnd_Rtb_Account_Dte_Mdcy_Pnd_Rtb_Account_Dte_Mdcy_Mm = pnd_Rtb_Account_Dte_MdcyRedef18.newFieldInGroup("pnd_Rtb_Account_Dte_Mdcy_Pnd_Rtb_Account_Dte_Mdcy_Mm", 
            "#RTB-ACCOUNT-DTE-MDCY-MM", FieldType.NUMERIC, 2);
        pnd_Rtb_Account_Dte_Mdcy_Pnd_Rtb_Account_Dte_Mdcy_Dd = pnd_Rtb_Account_Dte_MdcyRedef18.newFieldInGroup("pnd_Rtb_Account_Dte_Mdcy_Pnd_Rtb_Account_Dte_Mdcy_Dd", 
            "#RTB-ACCOUNT-DTE-MDCY-DD", FieldType.NUMERIC, 2);
        pnd_Rtb_Account_Dte_Mdcy_Pnd_Rtb_Account_Dte_Mdcy_Yyyy = pnd_Rtb_Account_Dte_MdcyRedef18.newFieldInGroup("pnd_Rtb_Account_Dte_Mdcy_Pnd_Rtb_Account_Dte_Mdcy_Yyyy", 
            "#RTB-ACCOUNT-DTE-MDCY-YYYY", FieldType.NUMERIC, 4);

        pnd_Rtb_Settle_Dte_Yyyymmdd = newFieldInRecord("pnd_Rtb_Settle_Dte_Yyyymmdd", "#RTB-SETTLE-DTE-YYYYMMDD", FieldType.STRING, 8);
        pnd_Rtb_Settle_Dte_YyyymmddRedef19 = newGroupInRecord("pnd_Rtb_Settle_Dte_YyyymmddRedef19", "Redefines", pnd_Rtb_Settle_Dte_Yyyymmdd);
        pnd_Rtb_Settle_Dte_Yyyymmdd_Pnd_Rtb_Settle_Dte_Yyyy = pnd_Rtb_Settle_Dte_YyyymmddRedef19.newFieldInGroup("pnd_Rtb_Settle_Dte_Yyyymmdd_Pnd_Rtb_Settle_Dte_Yyyy", 
            "#RTB-SETTLE-DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Rtb_Settle_Dte_Yyyymmdd_Pnd_Rtb_Settle_Dte_Mm = pnd_Rtb_Settle_Dte_YyyymmddRedef19.newFieldInGroup("pnd_Rtb_Settle_Dte_Yyyymmdd_Pnd_Rtb_Settle_Dte_Mm", 
            "#RTB-SETTLE-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Rtb_Settle_Dte_Yyyymmdd_Pnd_Rtb_Settle_Dte_Dd = pnd_Rtb_Settle_Dte_YyyymmddRedef19.newFieldInGroup("pnd_Rtb_Settle_Dte_Yyyymmdd_Pnd_Rtb_Settle_Dte_Dd", 
            "#RTB-SETTLE-DTE-DD", FieldType.NUMERIC, 2);

        pnd_Rtb_Settle_Dte_Mdcy = newFieldInRecord("pnd_Rtb_Settle_Dte_Mdcy", "#RTB-SETTLE-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Rtb_Settle_Dte_MdcyRedef20 = newGroupInRecord("pnd_Rtb_Settle_Dte_MdcyRedef20", "Redefines", pnd_Rtb_Settle_Dte_Mdcy);
        pnd_Rtb_Settle_Dte_Mdcy_Pnd_Rtb_Settle_Dte_Mdcy_Mm = pnd_Rtb_Settle_Dte_MdcyRedef20.newFieldInGroup("pnd_Rtb_Settle_Dte_Mdcy_Pnd_Rtb_Settle_Dte_Mdcy_Mm", 
            "#RTB-SETTLE-DTE-MDCY-MM", FieldType.NUMERIC, 2);
        pnd_Rtb_Settle_Dte_Mdcy_Pnd_Rtb_Settle_Dte_Mdcy_Dd = pnd_Rtb_Settle_Dte_MdcyRedef20.newFieldInGroup("pnd_Rtb_Settle_Dte_Mdcy_Pnd_Rtb_Settle_Dte_Mdcy_Dd", 
            "#RTB-SETTLE-DTE-MDCY-DD", FieldType.NUMERIC, 2);
        pnd_Rtb_Settle_Dte_Mdcy_Pnd_Rtb_Settle_Dte_Mdcy_Yyyy = pnd_Rtb_Settle_Dte_MdcyRedef20.newFieldInGroup("pnd_Rtb_Settle_Dte_Mdcy_Pnd_Rtb_Settle_Dte_Mdcy_Yyyy", 
            "#RTB-SETTLE-DTE-MDCY-YYYY", FieldType.NUMERIC, 4);

        pnd_Cont_Issue_Dte_Yyyymmdd = newFieldInRecord("pnd_Cont_Issue_Dte_Yyyymmdd", "#CONT-ISSUE-DTE-YYYYMMDD", FieldType.STRING, 8);
        pnd_Cont_Issue_Dte_YyyymmddRedef21 = newGroupInRecord("pnd_Cont_Issue_Dte_YyyymmddRedef21", "Redefines", pnd_Cont_Issue_Dte_Yyyymmdd);
        pnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Yyyy = pnd_Cont_Issue_Dte_YyyymmddRedef21.newFieldInGroup("pnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Yyyy", 
            "#CONT-ISSUE-DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Mm = pnd_Cont_Issue_Dte_YyyymmddRedef21.newFieldInGroup("pnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Mm", 
            "#CONT-ISSUE-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Dd = pnd_Cont_Issue_Dte_YyyymmddRedef21.newFieldInGroup("pnd_Cont_Issue_Dte_Yyyymmdd_Pnd_Cont_Issue_Dte_Dd", 
            "#CONT-ISSUE-DTE-DD", FieldType.NUMERIC, 2);

        pnd_Cont_Issue_Dte_Mdcy = newFieldInRecord("pnd_Cont_Issue_Dte_Mdcy", "#CONT-ISSUE-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Cont_Issue_Dte_MdcyRedef22 = newGroupInRecord("pnd_Cont_Issue_Dte_MdcyRedef22", "Redefines", pnd_Cont_Issue_Dte_Mdcy);
        pnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Mm = pnd_Cont_Issue_Dte_MdcyRedef22.newFieldInGroup("pnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Mm", 
            "#CONT-ISSUE-DTE-MDCY-MM", FieldType.NUMERIC, 2);
        pnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Dd = pnd_Cont_Issue_Dte_MdcyRedef22.newFieldInGroup("pnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Dd", 
            "#CONT-ISSUE-DTE-MDCY-DD", FieldType.NUMERIC, 2);
        pnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Yyyy = pnd_Cont_Issue_Dte_MdcyRedef22.newFieldInGroup("pnd_Cont_Issue_Dte_Mdcy_Pnd_Cont_Issue_Dte_Mdcy_Yyyy", 
            "#CONT-ISSUE-DTE-MDCY-YYYY", FieldType.NUMERIC, 4);

        pnd_Annt_Start_Dte_Yyyymmdd = newFieldInRecord("pnd_Annt_Start_Dte_Yyyymmdd", "#ANNT-START-DTE-YYYYMMDD", FieldType.STRING, 8);
        pnd_Annt_Start_Dte_YyyymmddRedef23 = newGroupInRecord("pnd_Annt_Start_Dte_YyyymmddRedef23", "Redefines", pnd_Annt_Start_Dte_Yyyymmdd);
        pnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Yyyy = pnd_Annt_Start_Dte_YyyymmddRedef23.newFieldInGroup("pnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Yyyy", 
            "#ANNT-START-DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Mm = pnd_Annt_Start_Dte_YyyymmddRedef23.newFieldInGroup("pnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Mm", 
            "#ANNT-START-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Dd = pnd_Annt_Start_Dte_YyyymmddRedef23.newFieldInGroup("pnd_Annt_Start_Dte_Yyyymmdd_Pnd_Annt_Start_Dte_Dd", 
            "#ANNT-START-DTE-DD", FieldType.NUMERIC, 2);

        pnd_Annt_Start_Dte_Mdcy = newFieldInRecord("pnd_Annt_Start_Dte_Mdcy", "#ANNT-START-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Annt_Start_Dte_MdcyRedef24 = newGroupInRecord("pnd_Annt_Start_Dte_MdcyRedef24", "Redefines", pnd_Annt_Start_Dte_Mdcy);
        pnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Mm = pnd_Annt_Start_Dte_MdcyRedef24.newFieldInGroup("pnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Mm", 
            "#ANNT-START-DTE-MDCY-MM", FieldType.NUMERIC, 2);
        pnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Dd = pnd_Annt_Start_Dte_MdcyRedef24.newFieldInGroup("pnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Dd", 
            "#ANNT-START-DTE-MDCY-DD", FieldType.NUMERIC, 2);
        pnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Yyyy = pnd_Annt_Start_Dte_MdcyRedef24.newFieldInGroup("pnd_Annt_Start_Dte_Mdcy_Pnd_Annt_Start_Dte_Mdcy_Yyyy", 
            "#ANNT-START-DTE-MDCY-YYYY", FieldType.NUMERIC, 4);

        pnd_Frst_Perod_Dte_Yyyymmdd = newFieldInRecord("pnd_Frst_Perod_Dte_Yyyymmdd", "#FRST-PEROD-DTE-YYYYMMDD", FieldType.STRING, 8);
        pnd_Frst_Perod_Dte_YyyymmddRedef25 = newGroupInRecord("pnd_Frst_Perod_Dte_YyyymmddRedef25", "Redefines", pnd_Frst_Perod_Dte_Yyyymmdd);
        pnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Yyyy = pnd_Frst_Perod_Dte_YyyymmddRedef25.newFieldInGroup("pnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Yyyy", 
            "#FRST-PEROD-DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Mm = pnd_Frst_Perod_Dte_YyyymmddRedef25.newFieldInGroup("pnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Mm", 
            "#FRST-PEROD-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Dd = pnd_Frst_Perod_Dte_YyyymmddRedef25.newFieldInGroup("pnd_Frst_Perod_Dte_Yyyymmdd_Pnd_Frst_Perod_Dte_Dd", 
            "#FRST-PEROD-DTE-DD", FieldType.NUMERIC, 2);

        pnd_Frst_Perod_Dte_Mdcy = newFieldInRecord("pnd_Frst_Perod_Dte_Mdcy", "#FRST-PEROD-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Frst_Perod_Dte_MdcyRedef26 = newGroupInRecord("pnd_Frst_Perod_Dte_MdcyRedef26", "Redefines", pnd_Frst_Perod_Dte_Mdcy);
        pnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Mm = pnd_Frst_Perod_Dte_MdcyRedef26.newFieldInGroup("pnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Mm", 
            "#FRST-PEROD-DTE-MDCY-MM", FieldType.NUMERIC, 2);
        pnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Dd = pnd_Frst_Perod_Dte_MdcyRedef26.newFieldInGroup("pnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Dd", 
            "#FRST-PEROD-DTE-MDCY-DD", FieldType.NUMERIC, 2);
        pnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Yyyy = pnd_Frst_Perod_Dte_MdcyRedef26.newFieldInGroup("pnd_Frst_Perod_Dte_Mdcy_Pnd_Frst_Perod_Dte_Mdcy_Yyyy", 
            "#FRST-PEROD-DTE-MDCY-YYYY", FieldType.NUMERIC, 4);

        pnd_Last_Prem_Dte_Yyyymmdd = newFieldInRecord("pnd_Last_Prem_Dte_Yyyymmdd", "#LAST-PREM-DTE-YYYYMMDD", FieldType.STRING, 8);
        pnd_Last_Prem_Dte_YyyymmddRedef27 = newGroupInRecord("pnd_Last_Prem_Dte_YyyymmddRedef27", "Redefines", pnd_Last_Prem_Dte_Yyyymmdd);
        pnd_Last_Prem_Dte_Yyyymmdd_Pnd_Last_Prem_Dte_Yyyy = pnd_Last_Prem_Dte_YyyymmddRedef27.newFieldInGroup("pnd_Last_Prem_Dte_Yyyymmdd_Pnd_Last_Prem_Dte_Yyyy", 
            "#LAST-PREM-DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Last_Prem_Dte_Yyyymmdd_Pnd_Last_Prem_Dte_Mm = pnd_Last_Prem_Dte_YyyymmddRedef27.newFieldInGroup("pnd_Last_Prem_Dte_Yyyymmdd_Pnd_Last_Prem_Dte_Mm", 
            "#LAST-PREM-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Last_Prem_Dte_Yyyymmdd_Pnd_Last_Prem_Dte_Dd = pnd_Last_Prem_Dte_YyyymmddRedef27.newFieldInGroup("pnd_Last_Prem_Dte_Yyyymmdd_Pnd_Last_Prem_Dte_Dd", 
            "#LAST-PREM-DTE-DD", FieldType.NUMERIC, 2);

        pnd_Last_Prem_Dte_Mdcy = newFieldInRecord("pnd_Last_Prem_Dte_Mdcy", "#LAST-PREM-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Last_Prem_Dte_MdcyRedef28 = newGroupInRecord("pnd_Last_Prem_Dte_MdcyRedef28", "Redefines", pnd_Last_Prem_Dte_Mdcy);
        pnd_Last_Prem_Dte_Mdcy_Pnd_Last_Prem_Dte_Mdcy_Mm = pnd_Last_Prem_Dte_MdcyRedef28.newFieldInGroup("pnd_Last_Prem_Dte_Mdcy_Pnd_Last_Prem_Dte_Mdcy_Mm", 
            "#LAST-PREM-DTE-MDCY-MM", FieldType.NUMERIC, 2);
        pnd_Last_Prem_Dte_Mdcy_Pnd_Last_Prem_Dte_Mdcy_Dd = pnd_Last_Prem_Dte_MdcyRedef28.newFieldInGroup("pnd_Last_Prem_Dte_Mdcy_Pnd_Last_Prem_Dte_Mdcy_Dd", 
            "#LAST-PREM-DTE-MDCY-DD", FieldType.NUMERIC, 2);
        pnd_Last_Prem_Dte_Mdcy_Pnd_Last_Prem_Dte_Mdcy_Yyyy = pnd_Last_Prem_Dte_MdcyRedef28.newFieldInGroup("pnd_Last_Prem_Dte_Mdcy_Pnd_Last_Prem_Dte_Mdcy_Yyyy", 
            "#LAST-PREM-DTE-MDCY-YYYY", FieldType.NUMERIC, 4);

        pnd_Last_Guar_Dte_Yyyymmdd = newFieldInRecord("pnd_Last_Guar_Dte_Yyyymmdd", "#LAST-GUAR-DTE-YYYYMMDD", FieldType.STRING, 8);
        pnd_Last_Guar_Dte_YyyymmddRedef29 = newGroupInRecord("pnd_Last_Guar_Dte_YyyymmddRedef29", "Redefines", pnd_Last_Guar_Dte_Yyyymmdd);
        pnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Yyyy = pnd_Last_Guar_Dte_YyyymmddRedef29.newFieldInGroup("pnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Yyyy", 
            "#LAST-GUAR-DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Mm = pnd_Last_Guar_Dte_YyyymmddRedef29.newFieldInGroup("pnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Mm", 
            "#LAST-GUAR-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Dd = pnd_Last_Guar_Dte_YyyymmddRedef29.newFieldInGroup("pnd_Last_Guar_Dte_Yyyymmdd_Pnd_Last_Guar_Dte_Dd", 
            "#LAST-GUAR-DTE-DD", FieldType.NUMERIC, 2);

        pnd_Last_Guar_Dte_Mdcy = newFieldInRecord("pnd_Last_Guar_Dte_Mdcy", "#LAST-GUAR-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Last_Guar_Dte_MdcyRedef30 = newGroupInRecord("pnd_Last_Guar_Dte_MdcyRedef30", "Redefines", pnd_Last_Guar_Dte_Mdcy);
        pnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Mm = pnd_Last_Guar_Dte_MdcyRedef30.newFieldInGroup("pnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Mm", 
            "#LAST-GUAR-DTE-MDCY-MM", FieldType.NUMERIC, 2);
        pnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Dd = pnd_Last_Guar_Dte_MdcyRedef30.newFieldInGroup("pnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Dd", 
            "#LAST-GUAR-DTE-MDCY-DD", FieldType.NUMERIC, 2);
        pnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Yyyy = pnd_Last_Guar_Dte_MdcyRedef30.newFieldInGroup("pnd_Last_Guar_Dte_Mdcy_Pnd_Last_Guar_Dte_Mdcy_Yyyy", 
            "#LAST-GUAR-DTE-MDCY-YYYY", FieldType.NUMERIC, 4);

        pnd_Empl_Term_Dte_Yyyymmdd = newFieldInRecord("pnd_Empl_Term_Dte_Yyyymmdd", "#EMPL-TERM-DTE-YYYYMMDD", FieldType.STRING, 8);
        pnd_Empl_Term_Dte_YyyymmddRedef31 = newGroupInRecord("pnd_Empl_Term_Dte_YyyymmddRedef31", "Redefines", pnd_Empl_Term_Dte_Yyyymmdd);
        pnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Yyyy = pnd_Empl_Term_Dte_YyyymmddRedef31.newFieldInGroup("pnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Yyyy", 
            "#EMPL-TERM-DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Mm = pnd_Empl_Term_Dte_YyyymmddRedef31.newFieldInGroup("pnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Mm", 
            "#EMPL-TERM-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Dd = pnd_Empl_Term_Dte_YyyymmddRedef31.newFieldInGroup("pnd_Empl_Term_Dte_Yyyymmdd_Pnd_Empl_Term_Dte_Dd", 
            "#EMPL-TERM-DTE-DD", FieldType.NUMERIC, 2);

        pnd_Empl_Term_Dte_Mdcy = newFieldInRecord("pnd_Empl_Term_Dte_Mdcy", "#EMPL-TERM-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Empl_Term_Dte_MdcyRedef32 = newGroupInRecord("pnd_Empl_Term_Dte_MdcyRedef32", "Redefines", pnd_Empl_Term_Dte_Mdcy);
        pnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Mm = pnd_Empl_Term_Dte_MdcyRedef32.newFieldInGroup("pnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Mm", 
            "#EMPL-TERM-DTE-MDCY-MM", FieldType.NUMERIC, 2);
        pnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Dd = pnd_Empl_Term_Dte_MdcyRedef32.newFieldInGroup("pnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Dd", 
            "#EMPL-TERM-DTE-MDCY-DD", FieldType.NUMERIC, 2);
        pnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Yyyy = pnd_Empl_Term_Dte_MdcyRedef32.newFieldInGroup("pnd_Empl_Term_Dte_Mdcy_Pnd_Empl_Term_Dte_Mdcy_Yyyy", 
            "#EMPL-TERM-DTE-MDCY-YYYY", FieldType.NUMERIC, 4);

        pnd_Form_Comp_Dte_Yyyymmdd = newFieldInRecord("pnd_Form_Comp_Dte_Yyyymmdd", "#FORM-COMP-DTE-YYYYMMDD", FieldType.STRING, 8);
        pnd_Form_Comp_Dte_YyyymmddRedef33 = newGroupInRecord("pnd_Form_Comp_Dte_YyyymmddRedef33", "Redefines", pnd_Form_Comp_Dte_Yyyymmdd);
        pnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Yyyy = pnd_Form_Comp_Dte_YyyymmddRedef33.newFieldInGroup("pnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Yyyy", 
            "#FORM-COMP-DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Mm = pnd_Form_Comp_Dte_YyyymmddRedef33.newFieldInGroup("pnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Mm", 
            "#FORM-COMP-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Dd = pnd_Form_Comp_Dte_YyyymmddRedef33.newFieldInGroup("pnd_Form_Comp_Dte_Yyyymmdd_Pnd_Form_Comp_Dte_Dd", 
            "#FORM-COMP-DTE-DD", FieldType.NUMERIC, 2);

        pnd_Form_Comp_Dte_Mdcy = newFieldInRecord("pnd_Form_Comp_Dte_Mdcy", "#FORM-COMP-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Form_Comp_Dte_MdcyRedef34 = newGroupInRecord("pnd_Form_Comp_Dte_MdcyRedef34", "Redefines", pnd_Form_Comp_Dte_Mdcy);
        pnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Mm = pnd_Form_Comp_Dte_MdcyRedef34.newFieldInGroup("pnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Mm", 
            "#FORM-COMP-DTE-MDCY-MM", FieldType.NUMERIC, 2);
        pnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Dd = pnd_Form_Comp_Dte_MdcyRedef34.newFieldInGroup("pnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Dd", 
            "#FORM-COMP-DTE-MDCY-DD", FieldType.NUMERIC, 2);
        pnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Yyyy = pnd_Form_Comp_Dte_MdcyRedef34.newFieldInGroup("pnd_Form_Comp_Dte_Mdcy_Pnd_Form_Comp_Dte_Mdcy_Yyyy", 
            "#FORM-COMP-DTE-MDCY-YYYY", FieldType.NUMERIC, 4);

        pnd_Effective_Dte_Yyyymmdd = newFieldInRecord("pnd_Effective_Dte_Yyyymmdd", "#EFFECTIVE-DTE-YYYYMMDD", FieldType.STRING, 8);
        pnd_Effective_Dte_YyyymmddRedef35 = newGroupInRecord("pnd_Effective_Dte_YyyymmddRedef35", "Redefines", pnd_Effective_Dte_Yyyymmdd);
        pnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Yyyy = pnd_Effective_Dte_YyyymmddRedef35.newFieldInGroup("pnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Yyyy", 
            "#EFFECTIVE-DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Mm = pnd_Effective_Dte_YyyymmddRedef35.newFieldInGroup("pnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Mm", 
            "#EFFECTIVE-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Dd = pnd_Effective_Dte_YyyymmddRedef35.newFieldInGroup("pnd_Effective_Dte_Yyyymmdd_Pnd_Effective_Dte_Dd", 
            "#EFFECTIVE-DTE-DD", FieldType.NUMERIC, 2);

        pnd_Effective_Dte_Mdcy = newFieldInRecord("pnd_Effective_Dte_Mdcy", "#EFFECTIVE-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Effective_Dte_MdcyRedef36 = newGroupInRecord("pnd_Effective_Dte_MdcyRedef36", "Redefines", pnd_Effective_Dte_Mdcy);
        pnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Mm = pnd_Effective_Dte_MdcyRedef36.newFieldInGroup("pnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Mm", 
            "#EFFECTIVE-DTE-MDCY-MM", FieldType.NUMERIC, 2);
        pnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Dd = pnd_Effective_Dte_MdcyRedef36.newFieldInGroup("pnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Dd", 
            "#EFFECTIVE-DTE-MDCY-DD", FieldType.NUMERIC, 2);
        pnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Yyyy = pnd_Effective_Dte_MdcyRedef36.newFieldInGroup("pnd_Effective_Dte_Mdcy_Pnd_Effective_Dte_Mdcy_Yyyy", 
            "#EFFECTIVE-DTE-MDCY-YYYY", FieldType.NUMERIC, 4);

        pnd_Payment_Dte_Yyyymmdd = newFieldInRecord("pnd_Payment_Dte_Yyyymmdd", "#PAYMENT-DTE-YYYYMMDD", FieldType.STRING, 8);
        pnd_Payment_Dte_YyyymmddRedef37 = newGroupInRecord("pnd_Payment_Dte_YyyymmddRedef37", "Redefines", pnd_Payment_Dte_Yyyymmdd);
        pnd_Payment_Dte_Yyyymmdd_Pnd_Payment_Dte_Yyyy = pnd_Payment_Dte_YyyymmddRedef37.newFieldInGroup("pnd_Payment_Dte_Yyyymmdd_Pnd_Payment_Dte_Yyyy", 
            "#PAYMENT-DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Payment_Dte_Yyyymmdd_Pnd_Payment_Dte_Mm = pnd_Payment_Dte_YyyymmddRedef37.newFieldInGroup("pnd_Payment_Dte_Yyyymmdd_Pnd_Payment_Dte_Mm", "#PAYMENT-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Dte_Yyyymmdd_Pnd_Payment_Dte_Dd = pnd_Payment_Dte_YyyymmddRedef37.newFieldInGroup("pnd_Payment_Dte_Yyyymmdd_Pnd_Payment_Dte_Dd", "#PAYMENT-DTE-DD", 
            FieldType.NUMERIC, 2);

        pnd_Payment_Dte_Mdcy = newFieldInRecord("pnd_Payment_Dte_Mdcy", "#PAYMENT-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Payment_Dte_MdcyRedef38 = newGroupInRecord("pnd_Payment_Dte_MdcyRedef38", "Redefines", pnd_Payment_Dte_Mdcy);
        pnd_Payment_Dte_Mdcy_Pnd_Payment_Dte_Mdcy_Mm = pnd_Payment_Dte_MdcyRedef38.newFieldInGroup("pnd_Payment_Dte_Mdcy_Pnd_Payment_Dte_Mdcy_Mm", "#PAYMENT-DTE-MDCY-MM", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Dte_Mdcy_Pnd_Payment_Dte_Mdcy_Dd = pnd_Payment_Dte_MdcyRedef38.newFieldInGroup("pnd_Payment_Dte_Mdcy_Pnd_Payment_Dte_Mdcy_Dd", "#PAYMENT-DTE-MDCY-DD", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Dte_Mdcy_Pnd_Payment_Dte_Mdcy_Yyyy = pnd_Payment_Dte_MdcyRedef38.newFieldInGroup("pnd_Payment_Dte_Mdcy_Pnd_Payment_Dte_Mdcy_Yyyy", 
            "#PAYMENT-DTE-MDCY-YYYY", FieldType.NUMERIC, 4);

        pnd_Account_Dte_Yyyymmdd = newFieldInRecord("pnd_Account_Dte_Yyyymmdd", "#ACCOUNT-DTE-YYYYMMDD", FieldType.STRING, 8);
        pnd_Account_Dte_YyyymmddRedef39 = newGroupInRecord("pnd_Account_Dte_YyyymmddRedef39", "Redefines", pnd_Account_Dte_Yyyymmdd);
        pnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Yyyy = pnd_Account_Dte_YyyymmddRedef39.newFieldInGroup("pnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Yyyy", 
            "#ACCOUNT-DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Mm = pnd_Account_Dte_YyyymmddRedef39.newFieldInGroup("pnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Mm", "#ACCOUNT-DTE-MM", 
            FieldType.NUMERIC, 2);
        pnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Dd = pnd_Account_Dte_YyyymmddRedef39.newFieldInGroup("pnd_Account_Dte_Yyyymmdd_Pnd_Account_Dte_Dd", "#ACCOUNT-DTE-DD", 
            FieldType.NUMERIC, 2);

        pnd_Account_Dte_Mdcy = newFieldInRecord("pnd_Account_Dte_Mdcy", "#ACCOUNT-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Account_Dte_MdcyRedef40 = newGroupInRecord("pnd_Account_Dte_MdcyRedef40", "Redefines", pnd_Account_Dte_Mdcy);
        pnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Mm = pnd_Account_Dte_MdcyRedef40.newFieldInGroup("pnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Mm", "#ACCOUNT-DTE-MDCY-MM", 
            FieldType.NUMERIC, 2);
        pnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Dd = pnd_Account_Dte_MdcyRedef40.newFieldInGroup("pnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Dd", "#ACCOUNT-DTE-MDCY-DD", 
            FieldType.NUMERIC, 2);
        pnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Yyyy = pnd_Account_Dte_MdcyRedef40.newFieldInGroup("pnd_Account_Dte_Mdcy_Pnd_Account_Dte_Mdcy_Yyyy", 
            "#ACCOUNT-DTE-MDCY-YYYY", FieldType.NUMERIC, 4);

        pnd_Non_Prem_Dte_Yyyymmdd = newFieldInRecord("pnd_Non_Prem_Dte_Yyyymmdd", "#NON-PREM-DTE-YYYYMMDD", FieldType.STRING, 8);
        pnd_Non_Prem_Dte_YyyymmddRedef41 = newGroupInRecord("pnd_Non_Prem_Dte_YyyymmddRedef41", "Redefines", pnd_Non_Prem_Dte_Yyyymmdd);
        pnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Yyyy = pnd_Non_Prem_Dte_YyyymmddRedef41.newFieldInGroup("pnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Yyyy", 
            "#NON-PREM-DTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Mm = pnd_Non_Prem_Dte_YyyymmddRedef41.newFieldInGroup("pnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Mm", 
            "#NON-PREM-DTE-MM", FieldType.NUMERIC, 2);
        pnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Dd = pnd_Non_Prem_Dte_YyyymmddRedef41.newFieldInGroup("pnd_Non_Prem_Dte_Yyyymmdd_Pnd_Non_Prem_Dte_Dd", 
            "#NON-PREM-DTE-DD", FieldType.NUMERIC, 2);

        pnd_Non_Prem_Dte_Mdcy = newFieldInRecord("pnd_Non_Prem_Dte_Mdcy", "#NON-PREM-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Non_Prem_Dte_MdcyRedef42 = newGroupInRecord("pnd_Non_Prem_Dte_MdcyRedef42", "Redefines", pnd_Non_Prem_Dte_Mdcy);
        pnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Mm = pnd_Non_Prem_Dte_MdcyRedef42.newFieldInGroup("pnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Mm", 
            "#NON-PREM-DTE-MDCY-MM", FieldType.NUMERIC, 2);
        pnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Dd = pnd_Non_Prem_Dte_MdcyRedef42.newFieldInGroup("pnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Dd", 
            "#NON-PREM-DTE-MDCY-DD", FieldType.NUMERIC, 2);
        pnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Yyyy = pnd_Non_Prem_Dte_MdcyRedef42.newFieldInGroup("pnd_Non_Prem_Dte_Mdcy_Pnd_Non_Prem_Dte_Mdcy_Yyyy", 
            "#NON-PREM-DTE-MDCY-YYYY", FieldType.NUMERIC, 4);

        pnd_Nap_Effective_Date = newFieldInRecord("pnd_Nap_Effective_Date", "#NAP-EFFECTIVE-DATE", FieldType.STRING, 8);
        pnd_Nap_Effective_DateRedef43 = newGroupInRecord("pnd_Nap_Effective_DateRedef43", "Redefines", pnd_Nap_Effective_Date);
        pnd_Nap_Effective_Date_Pnd_Nap_Effective_Date_Cc = pnd_Nap_Effective_DateRedef43.newFieldInGroup("pnd_Nap_Effective_Date_Pnd_Nap_Effective_Date_Cc", 
            "#NAP-EFFECTIVE-DATE-CC", FieldType.NUMERIC, 2);
        pnd_Nap_Effective_Date_Pnd_Nap_Effective_Date_Yy = pnd_Nap_Effective_DateRedef43.newFieldInGroup("pnd_Nap_Effective_Date_Pnd_Nap_Effective_Date_Yy", 
            "#NAP-EFFECTIVE-DATE-YY", FieldType.NUMERIC, 2);
        pnd_Nap_Effective_Date_Pnd_Nap_Effective_Date_Mm = pnd_Nap_Effective_DateRedef43.newFieldInGroup("pnd_Nap_Effective_Date_Pnd_Nap_Effective_Date_Mm", 
            "#NAP-EFFECTIVE-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Nap_Effective_Date_Pnd_Nap_Effective_Date_Dd = pnd_Nap_Effective_DateRedef43.newFieldInGroup("pnd_Nap_Effective_Date_Pnd_Nap_Effective_Date_Dd", 
            "#NAP-EFFECTIVE-DATE-DD", FieldType.NUMERIC, 2);

        pnd_T_Tpa_Io_Lit = newFieldInRecord("pnd_T_Tpa_Io_Lit", "#T-TPA-IO-LIT", FieldType.STRING, 4);

        pnd_Dash_666 = newFieldInRecord("pnd_Dash_666", "#DASH-666", FieldType.STRING, 1);

        this.setRecordName("LdaAdsl761");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pnd_Slash.setInitialValue("/");
    }

    // Constructor
    public LdaAdsl761() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
