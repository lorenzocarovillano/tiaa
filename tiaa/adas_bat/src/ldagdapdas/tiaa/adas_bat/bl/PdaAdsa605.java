/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:17:37 PM
**        * FROM NATURAL PDA     : ADSA605
************************************************************
**        * FILE NAME            : PdaAdsa605.java
**        * CLASS NAME           : PdaAdsa605
**        * INSTANCE NAME        : PdaAdsa605
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaAdsa605 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Adsa605;
    private DbsField pnd_Adsa605_Pnd_Plan_Nbr;
    private DbsField pnd_Adsa605_Pnd_Sub_Plan_Nbr;
    private DbsField pnd_Adsa605_Pnd_Origin_Code;
    private DbsField pnd_Adsa605_Pnd_Acct_Code;
    private DbsField pnd_Adsa605_Pnd_Contract_Nbrs;
    private DbsField pnd_Adsa605_Pnd_Origin_Lob_Ind;
    private DbsField pnd_Adsa605_Pnd_T395_Fund_Id;
    private DbsField pnd_Adsa605_Pnd_Invstmnt_Grpng_Id;
    private DbsField pnd_Adsa605_Pnd_Roth_Plan_Type;
    private DbsField pnd_Adsa605_Pnd_Adp_Irc_Cde;
    private DbsField pnd_Adsa605_Pnd_Tiaa_Orgn_Cde;
    private DbsField pnd_Adsa605_Pnd_Roth_Tiaa_Orgn_Cde;
    private DbsField pnd_Adsa605_Pnd_Cref_Orgn_Cde;
    private DbsField pnd_Adsa605_Pnd_Roth_Cref_Orgn_Cde;

    public DbsGroup getPnd_Adsa605() { return pnd_Adsa605; }

    public DbsField getPnd_Adsa605_Pnd_Plan_Nbr() { return pnd_Adsa605_Pnd_Plan_Nbr; }

    public DbsField getPnd_Adsa605_Pnd_Sub_Plan_Nbr() { return pnd_Adsa605_Pnd_Sub_Plan_Nbr; }

    public DbsField getPnd_Adsa605_Pnd_Origin_Code() { return pnd_Adsa605_Pnd_Origin_Code; }

    public DbsField getPnd_Adsa605_Pnd_Acct_Code() { return pnd_Adsa605_Pnd_Acct_Code; }

    public DbsField getPnd_Adsa605_Pnd_Contract_Nbrs() { return pnd_Adsa605_Pnd_Contract_Nbrs; }

    public DbsField getPnd_Adsa605_Pnd_Origin_Lob_Ind() { return pnd_Adsa605_Pnd_Origin_Lob_Ind; }

    public DbsField getPnd_Adsa605_Pnd_T395_Fund_Id() { return pnd_Adsa605_Pnd_T395_Fund_Id; }

    public DbsField getPnd_Adsa605_Pnd_Invstmnt_Grpng_Id() { return pnd_Adsa605_Pnd_Invstmnt_Grpng_Id; }

    public DbsField getPnd_Adsa605_Pnd_Roth_Plan_Type() { return pnd_Adsa605_Pnd_Roth_Plan_Type; }

    public DbsField getPnd_Adsa605_Pnd_Adp_Irc_Cde() { return pnd_Adsa605_Pnd_Adp_Irc_Cde; }

    public DbsField getPnd_Adsa605_Pnd_Tiaa_Orgn_Cde() { return pnd_Adsa605_Pnd_Tiaa_Orgn_Cde; }

    public DbsField getPnd_Adsa605_Pnd_Roth_Tiaa_Orgn_Cde() { return pnd_Adsa605_Pnd_Roth_Tiaa_Orgn_Cde; }

    public DbsField getPnd_Adsa605_Pnd_Cref_Orgn_Cde() { return pnd_Adsa605_Pnd_Cref_Orgn_Cde; }

    public DbsField getPnd_Adsa605_Pnd_Roth_Cref_Orgn_Cde() { return pnd_Adsa605_Pnd_Roth_Cref_Orgn_Cde; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Adsa605 = dbsRecord.newGroupInRecord("pnd_Adsa605", "#ADSA605");
        pnd_Adsa605.setParameterOption(ParameterOption.ByReference);
        pnd_Adsa605_Pnd_Plan_Nbr = pnd_Adsa605.newFieldInGroup("pnd_Adsa605_Pnd_Plan_Nbr", "#PLAN-NBR", FieldType.STRING, 6);
        pnd_Adsa605_Pnd_Sub_Plan_Nbr = pnd_Adsa605.newFieldInGroup("pnd_Adsa605_Pnd_Sub_Plan_Nbr", "#SUB-PLAN-NBR", FieldType.STRING, 6);
        pnd_Adsa605_Pnd_Origin_Code = pnd_Adsa605.newFieldInGroup("pnd_Adsa605_Pnd_Origin_Code", "#ORIGIN-CODE", FieldType.NUMERIC, 2);
        pnd_Adsa605_Pnd_Acct_Code = pnd_Adsa605.newFieldArrayInGroup("pnd_Adsa605_Pnd_Acct_Code", "#ACCT-CODE", FieldType.STRING, 1, new DbsArrayController(1,
            20));
        pnd_Adsa605_Pnd_Contract_Nbrs = pnd_Adsa605.newFieldArrayInGroup("pnd_Adsa605_Pnd_Contract_Nbrs", "#CONTRACT-NBRS", FieldType.STRING, 10, new 
            DbsArrayController(1,12));
        pnd_Adsa605_Pnd_Origin_Lob_Ind = pnd_Adsa605.newFieldArrayInGroup("pnd_Adsa605_Pnd_Origin_Lob_Ind", "#ORIGIN-LOB-IND", FieldType.STRING, 1, new 
            DbsArrayController(1,12));
        pnd_Adsa605_Pnd_T395_Fund_Id = pnd_Adsa605.newFieldArrayInGroup("pnd_Adsa605_Pnd_T395_Fund_Id", "#T395-FUND-ID", FieldType.STRING, 2, new DbsArrayController(1,
            14));
        pnd_Adsa605_Pnd_Invstmnt_Grpng_Id = pnd_Adsa605.newFieldArrayInGroup("pnd_Adsa605_Pnd_Invstmnt_Grpng_Id", "#INVSTMNT-GRPNG-ID", FieldType.STRING, 
            4, new DbsArrayController(1,20));
        pnd_Adsa605_Pnd_Roth_Plan_Type = pnd_Adsa605.newFieldInGroup("pnd_Adsa605_Pnd_Roth_Plan_Type", "#ROTH-PLAN-TYPE", FieldType.STRING, 1);
        pnd_Adsa605_Pnd_Adp_Irc_Cde = pnd_Adsa605.newFieldInGroup("pnd_Adsa605_Pnd_Adp_Irc_Cde", "#ADP-IRC-CDE", FieldType.STRING, 2);
        pnd_Adsa605_Pnd_Tiaa_Orgn_Cde = pnd_Adsa605.newFieldInGroup("pnd_Adsa605_Pnd_Tiaa_Orgn_Cde", "#TIAA-ORGN-CDE", FieldType.BOOLEAN);
        pnd_Adsa605_Pnd_Roth_Tiaa_Orgn_Cde = pnd_Adsa605.newFieldInGroup("pnd_Adsa605_Pnd_Roth_Tiaa_Orgn_Cde", "#ROTH-TIAA-ORGN-CDE", FieldType.BOOLEAN);
        pnd_Adsa605_Pnd_Cref_Orgn_Cde = pnd_Adsa605.newFieldInGroup("pnd_Adsa605_Pnd_Cref_Orgn_Cde", "#CREF-ORGN-CDE", FieldType.BOOLEAN);
        pnd_Adsa605_Pnd_Roth_Cref_Orgn_Cde = pnd_Adsa605.newFieldInGroup("pnd_Adsa605_Pnd_Roth_Cref_Orgn_Cde", "#ROTH-CREF-ORGN-CDE", FieldType.BOOLEAN);

        dbsRecord.reset();
    }

    // Constructors
    public PdaAdsa605(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

