/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:17:36 PM
**        * FROM NATURAL PDA     : ADSA470
************************************************************
**        * FILE NAME            : PdaAdsa470.java
**        * CLASS NAME           : PdaAdsa470
**        * INSTANCE NAME        : PdaAdsa470
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaAdsa470 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Adsa470;
    private DbsGroup pnd_Adsa470_Pnd_Adsa470_Prtcpnt_Rcrd;
    private DbsField pnd_Adsa470_Adp_In_Prgrss_Ind;
    private DbsField pnd_Adsa470_Adp_Opn_Clsd_Ind;
    private DbsField pnd_Adsa470_Adp_Stts_Cde;
    private DbsField pnd_Adsa470_Adp_Lst_Actvty_Dte;
    private DbsField pnd_Adsa470_Adp_Alt_Dest_Hold_Cde;
    private DbsField pnd_Adsa470_Adp_State_Of_Issue;
    private DbsField pnd_Adsa470_Adp_Orgnl_Issue_State;
    private DbsField pnd_Adsa470_Adp_Ivc_Pymnt_Rule;
    private DbsGroup pnd_Adsa470_Pnd_Adsa470_Cntrct_Rcrd;
    private DbsField pnd_Adsa470_Adc_Stts_Cde;
    private DbsField pnd_Adsa470_Adc_Gd_Cntrct_Ind;
    private DbsField pnd_Adsa470_Adc_Tiaa_Ivc_Gd_Ind;
    private DbsField pnd_Adsa470_Adc_Cref_Ivc_Gd_Ind;
    private DbsGroup pnd_Adsa470_Adc_Rqst_Info;
    private DbsField pnd_Adsa470_Adc_Acct_Stts_Cde;
    private DbsField pnd_Adsa470_Adc_Grd_Mnthly_Actl_Amt;
    private DbsField pnd_Adsa470_Adc_Stndrd_Annl_Actl_Amt;
    private DbsGroup pnd_Adsa470_Adc_Err_Rpt_Data;
    private DbsField pnd_Adsa470_Adc_Err_Cde;
    private DbsField pnd_Adsa470_Adc_Err_Acct_Cde;
    private DbsField pnd_Adsa470_Adc_Err_Msg;
    private DbsGroup pnd_Adsa470_Adc_Dtl_Tiaa_Data;
    private DbsField pnd_Adsa470_Adc_Dtl_Tiaa_Rate_Cde;
    private DbsField pnd_Adsa470_Adc_Dtl_Tiaa_Ia_Rate_Cde;
    private DbsField pnd_Adsa470_Adc_Dtl_Tiaa_Grd_Actl_Amt;
    private DbsField pnd_Adsa470_Adc_Dtl_Tiaa_Stndrd_Actl_Amt;
    private DbsGroup pnd_Adsa470_Adc_Tpa_Additional_Info;
    private DbsGroup pnd_Adsa470_Adc_Tpa_Guarant_Commut_Info;
    private DbsField pnd_Adsa470_Adc_Tpa_Guarant_Commut_Grd_Amt;
    private DbsField pnd_Adsa470_Adc_Tpa_Guarant_Commut_Std_Amt;
    private DbsField pnd_Adsa470_Adc_Orig_Lob_Ind;
    private DbsField pnd_Adsa470_Adc_Tpa_Paymnt_Cnt;
    private DbsGroup pnd_Adsa470_Pnd_Adsa470_Other_Variables;
    private DbsField pnd_Adsa470_Pnd_Adsa470_Cntrct_Isn;
    private DbsField pnd_Adsa470_Pnd_Adsa470_Prtcpnt_Isn;
    private DbsField pnd_Adsa470_Pnd_Adsa470_Error_Status;
    private DbsField pnd_Adsa470_Pnd_Adsa470_Debug_On;
    private DbsGroup pnd_Adsa470_Pnd_Adsa470_Static_Variables;
    private DbsField pnd_Adsa470_Pnd_Adsa470_Cntl_Bsnss_Dte;
    private DbsField pnd_Adsa470_Pnd_Adsa470_Fund_Cnt;

    public DbsGroup getPnd_Adsa470() { return pnd_Adsa470; }

    public DbsGroup getPnd_Adsa470_Pnd_Adsa470_Prtcpnt_Rcrd() { return pnd_Adsa470_Pnd_Adsa470_Prtcpnt_Rcrd; }

    public DbsField getPnd_Adsa470_Adp_In_Prgrss_Ind() { return pnd_Adsa470_Adp_In_Prgrss_Ind; }

    public DbsField getPnd_Adsa470_Adp_Opn_Clsd_Ind() { return pnd_Adsa470_Adp_Opn_Clsd_Ind; }

    public DbsField getPnd_Adsa470_Adp_Stts_Cde() { return pnd_Adsa470_Adp_Stts_Cde; }

    public DbsField getPnd_Adsa470_Adp_Lst_Actvty_Dte() { return pnd_Adsa470_Adp_Lst_Actvty_Dte; }

    public DbsField getPnd_Adsa470_Adp_Alt_Dest_Hold_Cde() { return pnd_Adsa470_Adp_Alt_Dest_Hold_Cde; }

    public DbsField getPnd_Adsa470_Adp_State_Of_Issue() { return pnd_Adsa470_Adp_State_Of_Issue; }

    public DbsField getPnd_Adsa470_Adp_Orgnl_Issue_State() { return pnd_Adsa470_Adp_Orgnl_Issue_State; }

    public DbsField getPnd_Adsa470_Adp_Ivc_Pymnt_Rule() { return pnd_Adsa470_Adp_Ivc_Pymnt_Rule; }

    public DbsGroup getPnd_Adsa470_Pnd_Adsa470_Cntrct_Rcrd() { return pnd_Adsa470_Pnd_Adsa470_Cntrct_Rcrd; }

    public DbsField getPnd_Adsa470_Adc_Stts_Cde() { return pnd_Adsa470_Adc_Stts_Cde; }

    public DbsField getPnd_Adsa470_Adc_Gd_Cntrct_Ind() { return pnd_Adsa470_Adc_Gd_Cntrct_Ind; }

    public DbsField getPnd_Adsa470_Adc_Tiaa_Ivc_Gd_Ind() { return pnd_Adsa470_Adc_Tiaa_Ivc_Gd_Ind; }

    public DbsField getPnd_Adsa470_Adc_Cref_Ivc_Gd_Ind() { return pnd_Adsa470_Adc_Cref_Ivc_Gd_Ind; }

    public DbsGroup getPnd_Adsa470_Adc_Rqst_Info() { return pnd_Adsa470_Adc_Rqst_Info; }

    public DbsField getPnd_Adsa470_Adc_Acct_Stts_Cde() { return pnd_Adsa470_Adc_Acct_Stts_Cde; }

    public DbsField getPnd_Adsa470_Adc_Grd_Mnthly_Actl_Amt() { return pnd_Adsa470_Adc_Grd_Mnthly_Actl_Amt; }

    public DbsField getPnd_Adsa470_Adc_Stndrd_Annl_Actl_Amt() { return pnd_Adsa470_Adc_Stndrd_Annl_Actl_Amt; }

    public DbsGroup getPnd_Adsa470_Adc_Err_Rpt_Data() { return pnd_Adsa470_Adc_Err_Rpt_Data; }

    public DbsField getPnd_Adsa470_Adc_Err_Cde() { return pnd_Adsa470_Adc_Err_Cde; }

    public DbsField getPnd_Adsa470_Adc_Err_Acct_Cde() { return pnd_Adsa470_Adc_Err_Acct_Cde; }

    public DbsField getPnd_Adsa470_Adc_Err_Msg() { return pnd_Adsa470_Adc_Err_Msg; }

    public DbsGroup getPnd_Adsa470_Adc_Dtl_Tiaa_Data() { return pnd_Adsa470_Adc_Dtl_Tiaa_Data; }

    public DbsField getPnd_Adsa470_Adc_Dtl_Tiaa_Rate_Cde() { return pnd_Adsa470_Adc_Dtl_Tiaa_Rate_Cde; }

    public DbsField getPnd_Adsa470_Adc_Dtl_Tiaa_Ia_Rate_Cde() { return pnd_Adsa470_Adc_Dtl_Tiaa_Ia_Rate_Cde; }

    public DbsField getPnd_Adsa470_Adc_Dtl_Tiaa_Grd_Actl_Amt() { return pnd_Adsa470_Adc_Dtl_Tiaa_Grd_Actl_Amt; }

    public DbsField getPnd_Adsa470_Adc_Dtl_Tiaa_Stndrd_Actl_Amt() { return pnd_Adsa470_Adc_Dtl_Tiaa_Stndrd_Actl_Amt; }

    public DbsGroup getPnd_Adsa470_Adc_Tpa_Additional_Info() { return pnd_Adsa470_Adc_Tpa_Additional_Info; }

    public DbsGroup getPnd_Adsa470_Adc_Tpa_Guarant_Commut_Info() { return pnd_Adsa470_Adc_Tpa_Guarant_Commut_Info; }

    public DbsField getPnd_Adsa470_Adc_Tpa_Guarant_Commut_Grd_Amt() { return pnd_Adsa470_Adc_Tpa_Guarant_Commut_Grd_Amt; }

    public DbsField getPnd_Adsa470_Adc_Tpa_Guarant_Commut_Std_Amt() { return pnd_Adsa470_Adc_Tpa_Guarant_Commut_Std_Amt; }

    public DbsField getPnd_Adsa470_Adc_Orig_Lob_Ind() { return pnd_Adsa470_Adc_Orig_Lob_Ind; }

    public DbsField getPnd_Adsa470_Adc_Tpa_Paymnt_Cnt() { return pnd_Adsa470_Adc_Tpa_Paymnt_Cnt; }

    public DbsGroup getPnd_Adsa470_Pnd_Adsa470_Other_Variables() { return pnd_Adsa470_Pnd_Adsa470_Other_Variables; }

    public DbsField getPnd_Adsa470_Pnd_Adsa470_Cntrct_Isn() { return pnd_Adsa470_Pnd_Adsa470_Cntrct_Isn; }

    public DbsField getPnd_Adsa470_Pnd_Adsa470_Prtcpnt_Isn() { return pnd_Adsa470_Pnd_Adsa470_Prtcpnt_Isn; }

    public DbsField getPnd_Adsa470_Pnd_Adsa470_Error_Status() { return pnd_Adsa470_Pnd_Adsa470_Error_Status; }

    public DbsField getPnd_Adsa470_Pnd_Adsa470_Debug_On() { return pnd_Adsa470_Pnd_Adsa470_Debug_On; }

    public DbsGroup getPnd_Adsa470_Pnd_Adsa470_Static_Variables() { return pnd_Adsa470_Pnd_Adsa470_Static_Variables; }

    public DbsField getPnd_Adsa470_Pnd_Adsa470_Cntl_Bsnss_Dte() { return pnd_Adsa470_Pnd_Adsa470_Cntl_Bsnss_Dte; }

    public DbsField getPnd_Adsa470_Pnd_Adsa470_Fund_Cnt() { return pnd_Adsa470_Pnd_Adsa470_Fund_Cnt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Adsa470 = dbsRecord.newGroupInRecord("pnd_Adsa470", "#ADSA470");
        pnd_Adsa470.setParameterOption(ParameterOption.ByReference);
        pnd_Adsa470_Pnd_Adsa470_Prtcpnt_Rcrd = pnd_Adsa470.newGroupInGroup("pnd_Adsa470_Pnd_Adsa470_Prtcpnt_Rcrd", "#ADSA470-PRTCPNT-RCRD");
        pnd_Adsa470_Adp_In_Prgrss_Ind = pnd_Adsa470_Pnd_Adsa470_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa470_Adp_In_Prgrss_Ind", "ADP-IN-PRGRSS-IND", FieldType.BOOLEAN);
        pnd_Adsa470_Adp_Opn_Clsd_Ind = pnd_Adsa470_Pnd_Adsa470_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa470_Adp_Opn_Clsd_Ind", "ADP-OPN-CLSD-IND", FieldType.STRING, 
            1);
        pnd_Adsa470_Adp_Stts_Cde = pnd_Adsa470_Pnd_Adsa470_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa470_Adp_Stts_Cde", "ADP-STTS-CDE", FieldType.STRING, 
            3);
        pnd_Adsa470_Adp_Lst_Actvty_Dte = pnd_Adsa470_Pnd_Adsa470_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa470_Adp_Lst_Actvty_Dte", "ADP-LST-ACTVTY-DTE", 
            FieldType.DATE);
        pnd_Adsa470_Adp_Alt_Dest_Hold_Cde = pnd_Adsa470_Pnd_Adsa470_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa470_Adp_Alt_Dest_Hold_Cde", "ADP-ALT-DEST-HOLD-CDE", 
            FieldType.STRING, 4);
        pnd_Adsa470_Adp_State_Of_Issue = pnd_Adsa470_Pnd_Adsa470_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa470_Adp_State_Of_Issue", "ADP-STATE-OF-ISSUE", 
            FieldType.STRING, 2);
        pnd_Adsa470_Adp_Orgnl_Issue_State = pnd_Adsa470_Pnd_Adsa470_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa470_Adp_Orgnl_Issue_State", "ADP-ORGNL-ISSUE-STATE", 
            FieldType.STRING, 2);
        pnd_Adsa470_Adp_Ivc_Pymnt_Rule = pnd_Adsa470_Pnd_Adsa470_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa470_Adp_Ivc_Pymnt_Rule", "ADP-IVC-PYMNT-RULE", 
            FieldType.STRING, 1);
        pnd_Adsa470_Pnd_Adsa470_Cntrct_Rcrd = pnd_Adsa470.newGroupArrayInGroup("pnd_Adsa470_Pnd_Adsa470_Cntrct_Rcrd", "#ADSA470-CNTRCT-RCRD", new DbsArrayController(1,
            12));
        pnd_Adsa470_Adc_Stts_Cde = pnd_Adsa470_Pnd_Adsa470_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa470_Adc_Stts_Cde", "ADC-STTS-CDE", FieldType.STRING, 3);
        pnd_Adsa470_Adc_Gd_Cntrct_Ind = pnd_Adsa470_Pnd_Adsa470_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa470_Adc_Gd_Cntrct_Ind", "ADC-GD-CNTRCT-IND", FieldType.BOOLEAN);
        pnd_Adsa470_Adc_Tiaa_Ivc_Gd_Ind = pnd_Adsa470_Pnd_Adsa470_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa470_Adc_Tiaa_Ivc_Gd_Ind", "ADC-TIAA-IVC-GD-IND", 
            FieldType.STRING, 1);
        pnd_Adsa470_Adc_Cref_Ivc_Gd_Ind = pnd_Adsa470_Pnd_Adsa470_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa470_Adc_Cref_Ivc_Gd_Ind", "ADC-CREF-IVC-GD-IND", 
            FieldType.STRING, 1);
        pnd_Adsa470_Adc_Rqst_Info = pnd_Adsa470_Pnd_Adsa470_Cntrct_Rcrd.newGroupArrayInGroup("pnd_Adsa470_Adc_Rqst_Info", "ADC-RQST-INFO", new DbsArrayController(1,
            20));
        pnd_Adsa470_Adc_Acct_Stts_Cde = pnd_Adsa470_Adc_Rqst_Info.newFieldInGroup("pnd_Adsa470_Adc_Acct_Stts_Cde", "ADC-ACCT-STTS-CDE", FieldType.STRING, 
            3);
        pnd_Adsa470_Adc_Grd_Mnthly_Actl_Amt = pnd_Adsa470_Adc_Rqst_Info.newFieldInGroup("pnd_Adsa470_Adc_Grd_Mnthly_Actl_Amt", "ADC-GRD-MNTHLY-ACTL-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Adsa470_Adc_Stndrd_Annl_Actl_Amt = pnd_Adsa470_Adc_Rqst_Info.newFieldInGroup("pnd_Adsa470_Adc_Stndrd_Annl_Actl_Amt", "ADC-STNDRD-ANNL-ACTL-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Adsa470_Adc_Err_Rpt_Data = pnd_Adsa470_Pnd_Adsa470_Cntrct_Rcrd.newGroupArrayInGroup("pnd_Adsa470_Adc_Err_Rpt_Data", "ADC-ERR-RPT-DATA", new 
            DbsArrayController(1,10));
        pnd_Adsa470_Adc_Err_Cde = pnd_Adsa470_Adc_Err_Rpt_Data.newFieldInGroup("pnd_Adsa470_Adc_Err_Cde", "ADC-ERR-CDE", FieldType.STRING, 3);
        pnd_Adsa470_Adc_Err_Acct_Cde = pnd_Adsa470_Adc_Err_Rpt_Data.newFieldInGroup("pnd_Adsa470_Adc_Err_Acct_Cde", "ADC-ERR-ACCT-CDE", FieldType.STRING, 
            1);
        pnd_Adsa470_Adc_Err_Msg = pnd_Adsa470_Adc_Err_Rpt_Data.newFieldInGroup("pnd_Adsa470_Adc_Err_Msg", "ADC-ERR-MSG", FieldType.STRING, 30);
        pnd_Adsa470_Adc_Dtl_Tiaa_Data = pnd_Adsa470_Pnd_Adsa470_Cntrct_Rcrd.newGroupArrayInGroup("pnd_Adsa470_Adc_Dtl_Tiaa_Data", "ADC-DTL-TIAA-DATA", 
            new DbsArrayController(1,250));
        pnd_Adsa470_Adc_Dtl_Tiaa_Rate_Cde = pnd_Adsa470_Adc_Dtl_Tiaa_Data.newFieldInGroup("pnd_Adsa470_Adc_Dtl_Tiaa_Rate_Cde", "ADC-DTL-TIAA-RATE-CDE", 
            FieldType.STRING, 2);
        pnd_Adsa470_Adc_Dtl_Tiaa_Ia_Rate_Cde = pnd_Adsa470_Adc_Dtl_Tiaa_Data.newFieldInGroup("pnd_Adsa470_Adc_Dtl_Tiaa_Ia_Rate_Cde", "ADC-DTL-TIAA-IA-RATE-CDE", 
            FieldType.STRING, 2);
        pnd_Adsa470_Adc_Dtl_Tiaa_Grd_Actl_Amt = pnd_Adsa470_Adc_Dtl_Tiaa_Data.newFieldInGroup("pnd_Adsa470_Adc_Dtl_Tiaa_Grd_Actl_Amt", "ADC-DTL-TIAA-GRD-ACTL-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Adsa470_Adc_Dtl_Tiaa_Stndrd_Actl_Amt = pnd_Adsa470_Adc_Dtl_Tiaa_Data.newFieldInGroup("pnd_Adsa470_Adc_Dtl_Tiaa_Stndrd_Actl_Amt", "ADC-DTL-TIAA-STNDRD-ACTL-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Adsa470_Adc_Tpa_Additional_Info = pnd_Adsa470.newGroupArrayInGroup("pnd_Adsa470_Adc_Tpa_Additional_Info", "ADC-TPA-ADDITIONAL-INFO", new DbsArrayController(1,
            12));
        pnd_Adsa470_Adc_Tpa_Guarant_Commut_Info = pnd_Adsa470_Adc_Tpa_Additional_Info.newGroupArrayInGroup("pnd_Adsa470_Adc_Tpa_Guarant_Commut_Info", 
            "ADC-TPA-GUARANT-COMMUT-INFO", new DbsArrayController(1,250));
        pnd_Adsa470_Adc_Tpa_Guarant_Commut_Grd_Amt = pnd_Adsa470_Adc_Tpa_Guarant_Commut_Info.newFieldInGroup("pnd_Adsa470_Adc_Tpa_Guarant_Commut_Grd_Amt", 
            "ADC-TPA-GUARANT-COMMUT-GRD-AMT", FieldType.DECIMAL, 11,2);
        pnd_Adsa470_Adc_Tpa_Guarant_Commut_Std_Amt = pnd_Adsa470_Adc_Tpa_Guarant_Commut_Info.newFieldInGroup("pnd_Adsa470_Adc_Tpa_Guarant_Commut_Std_Amt", 
            "ADC-TPA-GUARANT-COMMUT-STD-AMT", FieldType.DECIMAL, 11,2);
        pnd_Adsa470_Adc_Orig_Lob_Ind = pnd_Adsa470_Adc_Tpa_Additional_Info.newFieldInGroup("pnd_Adsa470_Adc_Orig_Lob_Ind", "ADC-ORIG-LOB-IND", FieldType.STRING, 
            1);
        pnd_Adsa470_Adc_Tpa_Paymnt_Cnt = pnd_Adsa470_Adc_Tpa_Additional_Info.newFieldInGroup("pnd_Adsa470_Adc_Tpa_Paymnt_Cnt", "ADC-TPA-PAYMNT-CNT", FieldType.NUMERIC, 
            2);
        pnd_Adsa470_Pnd_Adsa470_Other_Variables = pnd_Adsa470.newGroupInGroup("pnd_Adsa470_Pnd_Adsa470_Other_Variables", "#ADSA470-OTHER-VARIABLES");
        pnd_Adsa470_Pnd_Adsa470_Cntrct_Isn = pnd_Adsa470_Pnd_Adsa470_Other_Variables.newFieldArrayInGroup("pnd_Adsa470_Pnd_Adsa470_Cntrct_Isn", "#ADSA470-CNTRCT-ISN", 
            FieldType.PACKED_DECIMAL, 8, new DbsArrayController(1,12));
        pnd_Adsa470_Pnd_Adsa470_Prtcpnt_Isn = pnd_Adsa470_Pnd_Adsa470_Other_Variables.newFieldInGroup("pnd_Adsa470_Pnd_Adsa470_Prtcpnt_Isn", "#ADSA470-PRTCPNT-ISN", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Adsa470_Pnd_Adsa470_Error_Status = pnd_Adsa470_Pnd_Adsa470_Other_Variables.newFieldInGroup("pnd_Adsa470_Pnd_Adsa470_Error_Status", "#ADSA470-ERROR-STATUS", 
            FieldType.STRING, 3);
        pnd_Adsa470_Pnd_Adsa470_Debug_On = pnd_Adsa470_Pnd_Adsa470_Other_Variables.newFieldInGroup("pnd_Adsa470_Pnd_Adsa470_Debug_On", "#ADSA470-DEBUG-ON", 
            FieldType.BOOLEAN);
        pnd_Adsa470_Pnd_Adsa470_Static_Variables = pnd_Adsa470.newGroupInGroup("pnd_Adsa470_Pnd_Adsa470_Static_Variables", "#ADSA470-STATIC-VARIABLES");
        pnd_Adsa470_Pnd_Adsa470_Cntl_Bsnss_Dte = pnd_Adsa470_Pnd_Adsa470_Static_Variables.newFieldInGroup("pnd_Adsa470_Pnd_Adsa470_Cntl_Bsnss_Dte", "#ADSA470-CNTL-BSNSS-DTE", 
            FieldType.DATE);
        pnd_Adsa470_Pnd_Adsa470_Fund_Cnt = pnd_Adsa470_Pnd_Adsa470_Static_Variables.newFieldInGroup("pnd_Adsa470_Pnd_Adsa470_Fund_Cnt", "#ADSA470-FUND-CNT", 
            FieldType.PACKED_DECIMAL, 3);

        dbsRecord.reset();
    }

    // Constructors
    public PdaAdsa470(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

