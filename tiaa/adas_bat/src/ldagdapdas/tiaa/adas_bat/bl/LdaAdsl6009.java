/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:49:58 PM
**        * FROM NATURAL LDA     : ADSL6009
************************************************************
**        * FILE NAME            : LdaAdsl6009.java
**        * CLASS NAME           : LdaAdsl6009
**        * INSTANCE NAME        : LdaAdsl6009
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdsl6009 extends DbsRecord
{
    // Properties
    private DbsGroup adsl6009;
    private DbsField adsl6009_Pnd_Data;
    private DbsGroup adsl6009_Pnd_DataRedef1;
    private DbsField adsl6009_Pnd_Cis_Entry_Date_Time_Key;
    private DbsField adsl6009_Pnd_Cis_Requestor;
    private DbsField adsl6009_Pnd_Cis_Pin_Number;
    private DbsGroup adsl6009_Pnd_Cis_Pin_NumberRedef2;
    private DbsField adsl6009_Pnd_Cis_Pin_Number_Num;
    private DbsGroup adsl6009_Pnd_Cis_Pin_NumberRedef3;
    private DbsField adsl6009_Pnd_Cis_Pin_Number_7;
    private DbsField adsl6009_Pnd_Cis_Pin_Number_5;
    private DbsField adsl6009_Pnd_Cis_Soc_Sec_Number;
    private DbsField adsl6009_Pnd_Cis_Dob;
    private DbsGroup adsl6009_Pnd_Cis_DobRedef4;
    private DbsField adsl6009_Pnd_Cis_Dob_N;
    private DbsField adsl6009_Pnd_Cis_Tiaa_Cntrct_Nbr;
    private DbsField adsl6009_Pnd_Cis_Cref_Cntrct_Nbr;
    private DbsField adsl6009_Pnd_Cis_Rqst_Log_Dte_Tme;
    private DbsField adsl6009_Pnd_Cis_Product_Cde;
    private DbsField adsl6009_Pnd_Cis_Cntrct_Type;
    private DbsField adsl6009_Pnd_Cis_Assign_Issue_Cntrct_Ind;
    private DbsField adsl6009_Pnd_Cis_Mit_Function_Code;
    private DbsField adsl6009_Pnd_Cis_Functions;
    private DbsField adsl6009_Pnd_Cis_Trnsf_Flag;
    private DbsField adsl6009_Pnd_Cis_Sg_Text_Udf_1;
    private DbsField adsl6009_Pnd_Cis_Cntrct_Print_Dte;
    private DbsField adsl6009_Pnd_Cis_Annty_Option;
    private DbsField adsl6009_Pnd_Cis_Appl_Rcvd_Dte;
    private DbsField adsl6009_Pnd_Cis_Appl_Rcvd_User_Id;
    private DbsField adsl6009_Pnd_Cis_Pymnt_Mode;
    private DbsField adsl6009_Pnd_Cis_Annty_Start_Dte;
    private DbsField adsl6009_Pnd_Cis_Grnted_Period_Yrs;
    private DbsField adsl6009_Pnd_Cis_Grnted_Period_Dys;
    private DbsField adsl6009_Pnd_Cis_Scnd_Annt_Frst_Nme;
    private DbsField adsl6009_Pnd_Cis_Scnd_Annt_Mid_Nme;
    private DbsField adsl6009_Pnd_Cis_Scnd_Annt_Lst_Nme;
    private DbsField adsl6009_Pnd_Cis_Scnd_Annt_Sffx;
    private DbsField adsl6009_Pnd_Cis_Scnd_Annt_Ssn;
    private DbsField adsl6009_Pnd_Cis_Scnd_Annt_Dob;
    private DbsField adsl6009_Pnd_Cis_Scnd_Annt_Sex_Cde;
    private DbsField adsl6009_Pnd_Cis_Orig_Issue_State;
    private DbsField adsl6009_Pnd_Cis_Issue_State_Cd;
    private DbsField adsl6009_Pnd_Cis_Cntrct_Apprvl_Ind;
    private DbsGroup adsl6009_Pnd_Cis_Address_Info;
    private DbsGroup adsl6009_Pnd_Cis_Level_3;
    private DbsField adsl6009_Pnd_Cis_Addr_Usage_Code;
    private DbsField adsl6009_Pnd_Cis_Address_Txt;
    private DbsField adsl6009_Pnd_Cis_Zip_Code;
    private DbsField adsl6009_Pnd_Cis_Address_Chg_Ind;
    private DbsField adsl6009_Pnd_Cis_Address_Dest_Name;
    private DbsField adsl6009_Pnd_Cis_Bank_Pymnt_Acct_Nmbr;
    private DbsField adsl6009_Pnd_Cis_Bank_Aba_Acct_Nmbr;
    private DbsField adsl6009_Pnd_Cis_Checking_Saving_Cd;
    private DbsField adsl6009_Pnd_Cis_Mail_Instructions;
    private DbsField adsl6009_Pnd_Cis_Pull_Code;
    private DbsField adsl6009_Pnd_Cis_Four_Fifty_Seven_Ind;
    private DbsField adsl6009_Pnd_Cis_Institution_Name;
    private DbsField adsl6009_Pnd_Cis_Da_Tiaa_Nbr;
    private DbsField adsl6009_Pnd_Cis_Da_Rea_Proceeds_Amt;
    private DbsField adsl6009_Pnd_Cis_Da_Tiaa_Proceeds_Amt;
    private DbsField adsl6009_Pnd_Cis_Da_Cert_Nbr;
    private DbsField adsl6009_Pnd_Cis_Da_Cref_Proceeds_Amt;
    private DbsField adsl6009_Pnd_Cis_Tiaa_Doi;
    private DbsField adsl6009_Pnd_Cis_Cref_Doi;
    private DbsField adsl6009_Pnd_Cis_Cntrct_Type1;
    private DbsField adsl6009_Pnd_Cis_Annty_End_Dte;
    private DbsField adsl6009_Pnd_Cis_Grnted_Grd_Amt;
    private DbsField adsl6009_Pnd_Cis_Grnted_Std_Amt;
    private DbsField adsl6009_Pnd_Cis_Div_Grd_Amt;
    private DbsField adsl6009_Pnd_Cis_Div_Std_Amt;
    private DbsField adsl6009_Pnd_Cis_Mdo_Traditional_Amt;
    private DbsGroup adsl6009_Pnd_Cis_Tiaa_Commuted_Info;
    private DbsField adsl6009_Pnd_Cis_Comut_Grnted_Amt;
    private DbsField adsl6009_Pnd_Cis_Comut_Int_Rate;
    private DbsField adsl6009_Pnd_Cis_Comut_Pymt_Method;
    private DbsField adsl6009_Pnd_Cis_Lob_Type;
    private DbsField adsl6009_Pnd_Cis_Lob;
    private DbsGroup adsl6009_Pnd_Cis_Tiaa_Commuted_Info_2;
    private DbsField adsl6009_Pnd_Cis_Comut_Grnted_Amt_2;
    private DbsField adsl6009_Pnd_Cis_Comut_Pymt_Method_2;
    private DbsField adsl6009_Pnd_Cis_Comut_Int_Rate_2;
    private DbsField adsl6009_Pnd_Cis_Comut_Mortality_Basis;
    private DbsField adsl6009_Pnd_Cis_Rea_Annual_Nbr_Units;
    private DbsField adsl6009_Pnd_Cis_Rea_Mnthly_Nbr_Units;
    private DbsField adsl6009_Pnd_Cis_Tacc_Annual_Nbr_Units;
    private DbsField adsl6009_Pnd_Cis_Tacc_Mnthly_Nbr_Units;
    private DbsField adsl6009_Pnd_Cis_Tacc_Ind;
    private DbsField adsl6009_Pnd_Cis_Tacc_Account;
    private DbsGroup adsl6009_Pnd_Cis_Cref_Annty_Pymnt;
    private DbsField adsl6009_Pnd_Cis_Cref_Mnthly_Nbr_Units;
    private DbsField adsl6009_Pnd_Cis_Cref_Annual_Nbr_Units;
    private DbsField adsl6009_Pnd_Cis_Cref_Acct_Cde;
    private DbsField adsl6009_Pnd_Cis_Sg_Fund_Ticker;
    private DbsField adsl6009_Pnd_Cis_Rea_Annl_Annty_Amt;
    private DbsField adsl6009_Pnd_Cis_Rea_Mnthly_Annty_Amt;
    private DbsField adsl6009_Pnd_Cis_Cref_Annl_Annty_Amt;
    private DbsField adsl6009_Pnd_Cis_Cref_Mnthly_Annty_Amt;
    private DbsField adsl6009_Pnd_Cis_Tacc_Annl_Annty_Amt;
    private DbsField adsl6009_Pnd_Cis_Tacc_Mnthly_Annty_Amt;

    public DbsGroup getAdsl6009() { return adsl6009; }

    public DbsField getAdsl6009_Pnd_Data() { return adsl6009_Pnd_Data; }

    public DbsGroup getAdsl6009_Pnd_DataRedef1() { return adsl6009_Pnd_DataRedef1; }

    public DbsField getAdsl6009_Pnd_Cis_Entry_Date_Time_Key() { return adsl6009_Pnd_Cis_Entry_Date_Time_Key; }

    public DbsField getAdsl6009_Pnd_Cis_Requestor() { return adsl6009_Pnd_Cis_Requestor; }

    public DbsField getAdsl6009_Pnd_Cis_Pin_Number() { return adsl6009_Pnd_Cis_Pin_Number; }

    public DbsGroup getAdsl6009_Pnd_Cis_Pin_NumberRedef2() { return adsl6009_Pnd_Cis_Pin_NumberRedef2; }

    public DbsField getAdsl6009_Pnd_Cis_Pin_Number_Num() { return adsl6009_Pnd_Cis_Pin_Number_Num; }

    public DbsGroup getAdsl6009_Pnd_Cis_Pin_NumberRedef3() { return adsl6009_Pnd_Cis_Pin_NumberRedef3; }

    public DbsField getAdsl6009_Pnd_Cis_Pin_Number_7() { return adsl6009_Pnd_Cis_Pin_Number_7; }

    public DbsField getAdsl6009_Pnd_Cis_Pin_Number_5() { return adsl6009_Pnd_Cis_Pin_Number_5; }

    public DbsField getAdsl6009_Pnd_Cis_Soc_Sec_Number() { return adsl6009_Pnd_Cis_Soc_Sec_Number; }

    public DbsField getAdsl6009_Pnd_Cis_Dob() { return adsl6009_Pnd_Cis_Dob; }

    public DbsGroup getAdsl6009_Pnd_Cis_DobRedef4() { return adsl6009_Pnd_Cis_DobRedef4; }

    public DbsField getAdsl6009_Pnd_Cis_Dob_N() { return adsl6009_Pnd_Cis_Dob_N; }

    public DbsField getAdsl6009_Pnd_Cis_Tiaa_Cntrct_Nbr() { return adsl6009_Pnd_Cis_Tiaa_Cntrct_Nbr; }

    public DbsField getAdsl6009_Pnd_Cis_Cref_Cntrct_Nbr() { return adsl6009_Pnd_Cis_Cref_Cntrct_Nbr; }

    public DbsField getAdsl6009_Pnd_Cis_Rqst_Log_Dte_Tme() { return adsl6009_Pnd_Cis_Rqst_Log_Dte_Tme; }

    public DbsField getAdsl6009_Pnd_Cis_Product_Cde() { return adsl6009_Pnd_Cis_Product_Cde; }

    public DbsField getAdsl6009_Pnd_Cis_Cntrct_Type() { return adsl6009_Pnd_Cis_Cntrct_Type; }

    public DbsField getAdsl6009_Pnd_Cis_Assign_Issue_Cntrct_Ind() { return adsl6009_Pnd_Cis_Assign_Issue_Cntrct_Ind; }

    public DbsField getAdsl6009_Pnd_Cis_Mit_Function_Code() { return adsl6009_Pnd_Cis_Mit_Function_Code; }

    public DbsField getAdsl6009_Pnd_Cis_Functions() { return adsl6009_Pnd_Cis_Functions; }

    public DbsField getAdsl6009_Pnd_Cis_Trnsf_Flag() { return adsl6009_Pnd_Cis_Trnsf_Flag; }

    public DbsField getAdsl6009_Pnd_Cis_Sg_Text_Udf_1() { return adsl6009_Pnd_Cis_Sg_Text_Udf_1; }

    public DbsField getAdsl6009_Pnd_Cis_Cntrct_Print_Dte() { return adsl6009_Pnd_Cis_Cntrct_Print_Dte; }

    public DbsField getAdsl6009_Pnd_Cis_Annty_Option() { return adsl6009_Pnd_Cis_Annty_Option; }

    public DbsField getAdsl6009_Pnd_Cis_Appl_Rcvd_Dte() { return adsl6009_Pnd_Cis_Appl_Rcvd_Dte; }

    public DbsField getAdsl6009_Pnd_Cis_Appl_Rcvd_User_Id() { return adsl6009_Pnd_Cis_Appl_Rcvd_User_Id; }

    public DbsField getAdsl6009_Pnd_Cis_Pymnt_Mode() { return adsl6009_Pnd_Cis_Pymnt_Mode; }

    public DbsField getAdsl6009_Pnd_Cis_Annty_Start_Dte() { return adsl6009_Pnd_Cis_Annty_Start_Dte; }

    public DbsField getAdsl6009_Pnd_Cis_Grnted_Period_Yrs() { return adsl6009_Pnd_Cis_Grnted_Period_Yrs; }

    public DbsField getAdsl6009_Pnd_Cis_Grnted_Period_Dys() { return adsl6009_Pnd_Cis_Grnted_Period_Dys; }

    public DbsField getAdsl6009_Pnd_Cis_Scnd_Annt_Frst_Nme() { return adsl6009_Pnd_Cis_Scnd_Annt_Frst_Nme; }

    public DbsField getAdsl6009_Pnd_Cis_Scnd_Annt_Mid_Nme() { return adsl6009_Pnd_Cis_Scnd_Annt_Mid_Nme; }

    public DbsField getAdsl6009_Pnd_Cis_Scnd_Annt_Lst_Nme() { return adsl6009_Pnd_Cis_Scnd_Annt_Lst_Nme; }

    public DbsField getAdsl6009_Pnd_Cis_Scnd_Annt_Sffx() { return adsl6009_Pnd_Cis_Scnd_Annt_Sffx; }

    public DbsField getAdsl6009_Pnd_Cis_Scnd_Annt_Ssn() { return adsl6009_Pnd_Cis_Scnd_Annt_Ssn; }

    public DbsField getAdsl6009_Pnd_Cis_Scnd_Annt_Dob() { return adsl6009_Pnd_Cis_Scnd_Annt_Dob; }

    public DbsField getAdsl6009_Pnd_Cis_Scnd_Annt_Sex_Cde() { return adsl6009_Pnd_Cis_Scnd_Annt_Sex_Cde; }

    public DbsField getAdsl6009_Pnd_Cis_Orig_Issue_State() { return adsl6009_Pnd_Cis_Orig_Issue_State; }

    public DbsField getAdsl6009_Pnd_Cis_Issue_State_Cd() { return adsl6009_Pnd_Cis_Issue_State_Cd; }

    public DbsField getAdsl6009_Pnd_Cis_Cntrct_Apprvl_Ind() { return adsl6009_Pnd_Cis_Cntrct_Apprvl_Ind; }

    public DbsGroup getAdsl6009_Pnd_Cis_Address_Info() { return adsl6009_Pnd_Cis_Address_Info; }

    public DbsGroup getAdsl6009_Pnd_Cis_Level_3() { return adsl6009_Pnd_Cis_Level_3; }

    public DbsField getAdsl6009_Pnd_Cis_Addr_Usage_Code() { return adsl6009_Pnd_Cis_Addr_Usage_Code; }

    public DbsField getAdsl6009_Pnd_Cis_Address_Txt() { return adsl6009_Pnd_Cis_Address_Txt; }

    public DbsField getAdsl6009_Pnd_Cis_Zip_Code() { return adsl6009_Pnd_Cis_Zip_Code; }

    public DbsField getAdsl6009_Pnd_Cis_Address_Chg_Ind() { return adsl6009_Pnd_Cis_Address_Chg_Ind; }

    public DbsField getAdsl6009_Pnd_Cis_Address_Dest_Name() { return adsl6009_Pnd_Cis_Address_Dest_Name; }

    public DbsField getAdsl6009_Pnd_Cis_Bank_Pymnt_Acct_Nmbr() { return adsl6009_Pnd_Cis_Bank_Pymnt_Acct_Nmbr; }

    public DbsField getAdsl6009_Pnd_Cis_Bank_Aba_Acct_Nmbr() { return adsl6009_Pnd_Cis_Bank_Aba_Acct_Nmbr; }

    public DbsField getAdsl6009_Pnd_Cis_Checking_Saving_Cd() { return adsl6009_Pnd_Cis_Checking_Saving_Cd; }

    public DbsField getAdsl6009_Pnd_Cis_Mail_Instructions() { return adsl6009_Pnd_Cis_Mail_Instructions; }

    public DbsField getAdsl6009_Pnd_Cis_Pull_Code() { return adsl6009_Pnd_Cis_Pull_Code; }

    public DbsField getAdsl6009_Pnd_Cis_Four_Fifty_Seven_Ind() { return adsl6009_Pnd_Cis_Four_Fifty_Seven_Ind; }

    public DbsField getAdsl6009_Pnd_Cis_Institution_Name() { return adsl6009_Pnd_Cis_Institution_Name; }

    public DbsField getAdsl6009_Pnd_Cis_Da_Tiaa_Nbr() { return adsl6009_Pnd_Cis_Da_Tiaa_Nbr; }

    public DbsField getAdsl6009_Pnd_Cis_Da_Rea_Proceeds_Amt() { return adsl6009_Pnd_Cis_Da_Rea_Proceeds_Amt; }

    public DbsField getAdsl6009_Pnd_Cis_Da_Tiaa_Proceeds_Amt() { return adsl6009_Pnd_Cis_Da_Tiaa_Proceeds_Amt; }

    public DbsField getAdsl6009_Pnd_Cis_Da_Cert_Nbr() { return adsl6009_Pnd_Cis_Da_Cert_Nbr; }

    public DbsField getAdsl6009_Pnd_Cis_Da_Cref_Proceeds_Amt() { return adsl6009_Pnd_Cis_Da_Cref_Proceeds_Amt; }

    public DbsField getAdsl6009_Pnd_Cis_Tiaa_Doi() { return adsl6009_Pnd_Cis_Tiaa_Doi; }

    public DbsField getAdsl6009_Pnd_Cis_Cref_Doi() { return adsl6009_Pnd_Cis_Cref_Doi; }

    public DbsField getAdsl6009_Pnd_Cis_Cntrct_Type1() { return adsl6009_Pnd_Cis_Cntrct_Type1; }

    public DbsField getAdsl6009_Pnd_Cis_Annty_End_Dte() { return adsl6009_Pnd_Cis_Annty_End_Dte; }

    public DbsField getAdsl6009_Pnd_Cis_Grnted_Grd_Amt() { return adsl6009_Pnd_Cis_Grnted_Grd_Amt; }

    public DbsField getAdsl6009_Pnd_Cis_Grnted_Std_Amt() { return adsl6009_Pnd_Cis_Grnted_Std_Amt; }

    public DbsField getAdsl6009_Pnd_Cis_Div_Grd_Amt() { return adsl6009_Pnd_Cis_Div_Grd_Amt; }

    public DbsField getAdsl6009_Pnd_Cis_Div_Std_Amt() { return adsl6009_Pnd_Cis_Div_Std_Amt; }

    public DbsField getAdsl6009_Pnd_Cis_Mdo_Traditional_Amt() { return adsl6009_Pnd_Cis_Mdo_Traditional_Amt; }

    public DbsGroup getAdsl6009_Pnd_Cis_Tiaa_Commuted_Info() { return adsl6009_Pnd_Cis_Tiaa_Commuted_Info; }

    public DbsField getAdsl6009_Pnd_Cis_Comut_Grnted_Amt() { return adsl6009_Pnd_Cis_Comut_Grnted_Amt; }

    public DbsField getAdsl6009_Pnd_Cis_Comut_Int_Rate() { return adsl6009_Pnd_Cis_Comut_Int_Rate; }

    public DbsField getAdsl6009_Pnd_Cis_Comut_Pymt_Method() { return adsl6009_Pnd_Cis_Comut_Pymt_Method; }

    public DbsField getAdsl6009_Pnd_Cis_Lob_Type() { return adsl6009_Pnd_Cis_Lob_Type; }

    public DbsField getAdsl6009_Pnd_Cis_Lob() { return adsl6009_Pnd_Cis_Lob; }

    public DbsGroup getAdsl6009_Pnd_Cis_Tiaa_Commuted_Info_2() { return adsl6009_Pnd_Cis_Tiaa_Commuted_Info_2; }

    public DbsField getAdsl6009_Pnd_Cis_Comut_Grnted_Amt_2() { return adsl6009_Pnd_Cis_Comut_Grnted_Amt_2; }

    public DbsField getAdsl6009_Pnd_Cis_Comut_Pymt_Method_2() { return adsl6009_Pnd_Cis_Comut_Pymt_Method_2; }

    public DbsField getAdsl6009_Pnd_Cis_Comut_Int_Rate_2() { return adsl6009_Pnd_Cis_Comut_Int_Rate_2; }

    public DbsField getAdsl6009_Pnd_Cis_Comut_Mortality_Basis() { return adsl6009_Pnd_Cis_Comut_Mortality_Basis; }

    public DbsField getAdsl6009_Pnd_Cis_Rea_Annual_Nbr_Units() { return adsl6009_Pnd_Cis_Rea_Annual_Nbr_Units; }

    public DbsField getAdsl6009_Pnd_Cis_Rea_Mnthly_Nbr_Units() { return adsl6009_Pnd_Cis_Rea_Mnthly_Nbr_Units; }

    public DbsField getAdsl6009_Pnd_Cis_Tacc_Annual_Nbr_Units() { return adsl6009_Pnd_Cis_Tacc_Annual_Nbr_Units; }

    public DbsField getAdsl6009_Pnd_Cis_Tacc_Mnthly_Nbr_Units() { return adsl6009_Pnd_Cis_Tacc_Mnthly_Nbr_Units; }

    public DbsField getAdsl6009_Pnd_Cis_Tacc_Ind() { return adsl6009_Pnd_Cis_Tacc_Ind; }

    public DbsField getAdsl6009_Pnd_Cis_Tacc_Account() { return adsl6009_Pnd_Cis_Tacc_Account; }

    public DbsGroup getAdsl6009_Pnd_Cis_Cref_Annty_Pymnt() { return adsl6009_Pnd_Cis_Cref_Annty_Pymnt; }

    public DbsField getAdsl6009_Pnd_Cis_Cref_Mnthly_Nbr_Units() { return adsl6009_Pnd_Cis_Cref_Mnthly_Nbr_Units; }

    public DbsField getAdsl6009_Pnd_Cis_Cref_Annual_Nbr_Units() { return adsl6009_Pnd_Cis_Cref_Annual_Nbr_Units; }

    public DbsField getAdsl6009_Pnd_Cis_Cref_Acct_Cde() { return adsl6009_Pnd_Cis_Cref_Acct_Cde; }

    public DbsField getAdsl6009_Pnd_Cis_Sg_Fund_Ticker() { return adsl6009_Pnd_Cis_Sg_Fund_Ticker; }

    public DbsField getAdsl6009_Pnd_Cis_Rea_Annl_Annty_Amt() { return adsl6009_Pnd_Cis_Rea_Annl_Annty_Amt; }

    public DbsField getAdsl6009_Pnd_Cis_Rea_Mnthly_Annty_Amt() { return adsl6009_Pnd_Cis_Rea_Mnthly_Annty_Amt; }

    public DbsField getAdsl6009_Pnd_Cis_Cref_Annl_Annty_Amt() { return adsl6009_Pnd_Cis_Cref_Annl_Annty_Amt; }

    public DbsField getAdsl6009_Pnd_Cis_Cref_Mnthly_Annty_Amt() { return adsl6009_Pnd_Cis_Cref_Mnthly_Annty_Amt; }

    public DbsField getAdsl6009_Pnd_Cis_Tacc_Annl_Annty_Amt() { return adsl6009_Pnd_Cis_Tacc_Annl_Annty_Amt; }

    public DbsField getAdsl6009_Pnd_Cis_Tacc_Mnthly_Annty_Amt() { return adsl6009_Pnd_Cis_Tacc_Mnthly_Annty_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        adsl6009 = newGroupInRecord("adsl6009", "ADSL6009");
        adsl6009_Pnd_Data = adsl6009.newFieldArrayInGroup("adsl6009_Pnd_Data", "#DATA", FieldType.STRING, 1, new DbsArrayController(1,2996));
        adsl6009_Pnd_DataRedef1 = adsl6009.newGroupInGroup("adsl6009_Pnd_DataRedef1", "Redefines", adsl6009_Pnd_Data);
        adsl6009_Pnd_Cis_Entry_Date_Time_Key = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Entry_Date_Time_Key", "#CIS-ENTRY-DATE-TIME-KEY", 
            FieldType.STRING, 35);
        adsl6009_Pnd_Cis_Requestor = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Requestor", "#CIS-REQUESTOR", FieldType.STRING, 8);
        adsl6009_Pnd_Cis_Pin_Number = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Pin_Number", "#CIS-PIN-NUMBER", FieldType.STRING, 12);
        adsl6009_Pnd_Cis_Pin_NumberRedef2 = adsl6009_Pnd_DataRedef1.newGroupInGroup("adsl6009_Pnd_Cis_Pin_NumberRedef2", "Redefines", adsl6009_Pnd_Cis_Pin_Number);
        adsl6009_Pnd_Cis_Pin_Number_Num = adsl6009_Pnd_Cis_Pin_NumberRedef2.newFieldInGroup("adsl6009_Pnd_Cis_Pin_Number_Num", "#CIS-PIN-NUMBER-NUM", 
            FieldType.NUMERIC, 12);
        adsl6009_Pnd_Cis_Pin_NumberRedef3 = adsl6009_Pnd_DataRedef1.newGroupInGroup("adsl6009_Pnd_Cis_Pin_NumberRedef3", "Redefines", adsl6009_Pnd_Cis_Pin_Number);
        adsl6009_Pnd_Cis_Pin_Number_7 = adsl6009_Pnd_Cis_Pin_NumberRedef3.newFieldInGroup("adsl6009_Pnd_Cis_Pin_Number_7", "#CIS-PIN-NUMBER-7", FieldType.NUMERIC, 
            7);
        adsl6009_Pnd_Cis_Pin_Number_5 = adsl6009_Pnd_Cis_Pin_NumberRedef3.newFieldInGroup("adsl6009_Pnd_Cis_Pin_Number_5", "#CIS-PIN-NUMBER-5", FieldType.STRING, 
            5);
        adsl6009_Pnd_Cis_Soc_Sec_Number = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Soc_Sec_Number", "#CIS-SOC-SEC-NUMBER", FieldType.NUMERIC, 
            9);
        adsl6009_Pnd_Cis_Dob = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Dob", "#CIS-DOB", FieldType.STRING, 8);
        adsl6009_Pnd_Cis_DobRedef4 = adsl6009_Pnd_DataRedef1.newGroupInGroup("adsl6009_Pnd_Cis_DobRedef4", "Redefines", adsl6009_Pnd_Cis_Dob);
        adsl6009_Pnd_Cis_Dob_N = adsl6009_Pnd_Cis_DobRedef4.newFieldInGroup("adsl6009_Pnd_Cis_Dob_N", "#CIS-DOB-N", FieldType.NUMERIC, 8);
        adsl6009_Pnd_Cis_Tiaa_Cntrct_Nbr = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Tiaa_Cntrct_Nbr", "#CIS-TIAA-CNTRCT-NBR", FieldType.STRING, 
            10);
        adsl6009_Pnd_Cis_Cref_Cntrct_Nbr = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Cref_Cntrct_Nbr", "#CIS-CREF-CNTRCT-NBR", FieldType.STRING, 
            10);
        adsl6009_Pnd_Cis_Rqst_Log_Dte_Tme = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Rqst_Log_Dte_Tme", "#CIS-RQST-LOG-DTE-TME", FieldType.STRING, 
            15);
        adsl6009_Pnd_Cis_Product_Cde = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Product_Cde", "#CIS-PRODUCT-CDE", FieldType.STRING, 10);
        adsl6009_Pnd_Cis_Cntrct_Type = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Cntrct_Type", "#CIS-CNTRCT-TYPE", FieldType.STRING, 1);
        adsl6009_Pnd_Cis_Assign_Issue_Cntrct_Ind = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Assign_Issue_Cntrct_Ind", "#CIS-ASSIGN-ISSUE-CNTRCT-IND", 
            FieldType.STRING, 1);
        adsl6009_Pnd_Cis_Mit_Function_Code = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Mit_Function_Code", "#CIS-MIT-FUNCTION-CODE", FieldType.STRING, 
            2);
        adsl6009_Pnd_Cis_Functions = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Functions", "#CIS-FUNCTIONS", FieldType.STRING, 2);
        adsl6009_Pnd_Cis_Trnsf_Flag = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Trnsf_Flag", "#CIS-TRNSF-FLAG", FieldType.STRING, 1);
        adsl6009_Pnd_Cis_Sg_Text_Udf_1 = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Sg_Text_Udf_1", "#CIS-SG-TEXT-UDF-1", FieldType.STRING, 
            10);
        adsl6009_Pnd_Cis_Cntrct_Print_Dte = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Cntrct_Print_Dte", "#CIS-CNTRCT-PRINT-DTE", FieldType.STRING, 
            6);
        adsl6009_Pnd_Cis_Annty_Option = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Annty_Option", "#CIS-ANNTY-OPTION", FieldType.STRING, 
            3);
        adsl6009_Pnd_Cis_Appl_Rcvd_Dte = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Appl_Rcvd_Dte", "#CIS-APPL-RCVD-DTE", FieldType.STRING, 
            8);
        adsl6009_Pnd_Cis_Appl_Rcvd_User_Id = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Appl_Rcvd_User_Id", "#CIS-APPL-RCVD-USER-ID", FieldType.STRING, 
            8);
        adsl6009_Pnd_Cis_Pymnt_Mode = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Pymnt_Mode", "#CIS-PYMNT-MODE", FieldType.STRING, 1);
        adsl6009_Pnd_Cis_Annty_Start_Dte = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Annty_Start_Dte", "#CIS-ANNTY-START-DTE", FieldType.STRING, 
            8);
        adsl6009_Pnd_Cis_Grnted_Period_Yrs = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Grnted_Period_Yrs", "#CIS-GRNTED-PERIOD-YRS", FieldType.NUMERIC, 
            2);
        adsl6009_Pnd_Cis_Grnted_Period_Dys = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Grnted_Period_Dys", "#CIS-GRNTED-PERIOD-DYS", FieldType.NUMERIC, 
            3);
        adsl6009_Pnd_Cis_Scnd_Annt_Frst_Nme = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Scnd_Annt_Frst_Nme", "#CIS-SCND-ANNT-FRST-NME", 
            FieldType.STRING, 30);
        adsl6009_Pnd_Cis_Scnd_Annt_Mid_Nme = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Scnd_Annt_Mid_Nme", "#CIS-SCND-ANNT-MID-NME", FieldType.STRING, 
            30);
        adsl6009_Pnd_Cis_Scnd_Annt_Lst_Nme = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Scnd_Annt_Lst_Nme", "#CIS-SCND-ANNT-LST-NME", FieldType.STRING, 
            30);
        adsl6009_Pnd_Cis_Scnd_Annt_Sffx = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Scnd_Annt_Sffx", "#CIS-SCND-ANNT-SFFX", FieldType.STRING, 
            8);
        adsl6009_Pnd_Cis_Scnd_Annt_Ssn = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Scnd_Annt_Ssn", "#CIS-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9);
        adsl6009_Pnd_Cis_Scnd_Annt_Dob = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Scnd_Annt_Dob", "#CIS-SCND-ANNT-DOB", FieldType.STRING, 
            8);
        adsl6009_Pnd_Cis_Scnd_Annt_Sex_Cde = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Scnd_Annt_Sex_Cde", "#CIS-SCND-ANNT-SEX-CDE", FieldType.STRING, 
            1);
        adsl6009_Pnd_Cis_Orig_Issue_State = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Orig_Issue_State", "#CIS-ORIG-ISSUE-STATE", FieldType.STRING, 
            2);
        adsl6009_Pnd_Cis_Issue_State_Cd = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Issue_State_Cd", "#CIS-ISSUE-STATE-CD", FieldType.STRING, 
            2);
        adsl6009_Pnd_Cis_Cntrct_Apprvl_Ind = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Cntrct_Apprvl_Ind", "#CIS-CNTRCT-APPRVL-IND", FieldType.STRING, 
            1);
        adsl6009_Pnd_Cis_Address_Info = adsl6009_Pnd_DataRedef1.newGroupArrayInGroup("adsl6009_Pnd_Cis_Address_Info", "#CIS-ADDRESS-INFO", new DbsArrayController(1,
            2));
        adsl6009_Pnd_Cis_Level_3 = adsl6009_Pnd_Cis_Address_Info.newGroupInGroup("adsl6009_Pnd_Cis_Level_3", "#CIS-LEVEL-3");
        adsl6009_Pnd_Cis_Addr_Usage_Code = adsl6009_Pnd_Cis_Level_3.newFieldInGroup("adsl6009_Pnd_Cis_Addr_Usage_Code", "#CIS-ADDR-USAGE-CODE", FieldType.STRING, 
            1);
        adsl6009_Pnd_Cis_Address_Txt = adsl6009_Pnd_Cis_Level_3.newFieldArrayInGroup("adsl6009_Pnd_Cis_Address_Txt", "#CIS-ADDRESS-TXT", FieldType.STRING, 
            35, new DbsArrayController(1,5));
        adsl6009_Pnd_Cis_Zip_Code = adsl6009_Pnd_Cis_Level_3.newFieldInGroup("adsl6009_Pnd_Cis_Zip_Code", "#CIS-ZIP-CODE", FieldType.STRING, 9);
        adsl6009_Pnd_Cis_Address_Chg_Ind = adsl6009_Pnd_Cis_Level_3.newFieldInGroup("adsl6009_Pnd_Cis_Address_Chg_Ind", "#CIS-ADDRESS-CHG-IND", FieldType.STRING, 
            1);
        adsl6009_Pnd_Cis_Address_Dest_Name = adsl6009_Pnd_Cis_Level_3.newFieldInGroup("adsl6009_Pnd_Cis_Address_Dest_Name", "#CIS-ADDRESS-DEST-NAME", 
            FieldType.STRING, 35);
        adsl6009_Pnd_Cis_Bank_Pymnt_Acct_Nmbr = adsl6009_Pnd_Cis_Level_3.newFieldInGroup("adsl6009_Pnd_Cis_Bank_Pymnt_Acct_Nmbr", "#CIS-BANK-PYMNT-ACCT-NMBR", 
            FieldType.STRING, 21);
        adsl6009_Pnd_Cis_Bank_Aba_Acct_Nmbr = adsl6009_Pnd_Cis_Level_3.newFieldInGroup("adsl6009_Pnd_Cis_Bank_Aba_Acct_Nmbr", "#CIS-BANK-ABA-ACCT-NMBR", 
            FieldType.STRING, 9);
        adsl6009_Pnd_Cis_Checking_Saving_Cd = adsl6009_Pnd_Cis_Level_3.newFieldInGroup("adsl6009_Pnd_Cis_Checking_Saving_Cd", "#CIS-CHECKING-SAVING-CD", 
            FieldType.STRING, 1);
        adsl6009_Pnd_Cis_Mail_Instructions = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Mail_Instructions", "#CIS-MAIL-INSTRUCTIONS", FieldType.STRING, 
            1);
        adsl6009_Pnd_Cis_Pull_Code = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Pull_Code", "#CIS-PULL-CODE", FieldType.STRING, 4);
        adsl6009_Pnd_Cis_Four_Fifty_Seven_Ind = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Four_Fifty_Seven_Ind", "#CIS-FOUR-FIFTY-SEVEN-IND", 
            FieldType.STRING, 1);
        adsl6009_Pnd_Cis_Institution_Name = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Institution_Name", "#CIS-INSTITUTION-NAME", FieldType.STRING, 
            32);
        adsl6009_Pnd_Cis_Da_Tiaa_Nbr = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Da_Tiaa_Nbr", "#CIS-DA-TIAA-NBR", FieldType.STRING, 10);
        adsl6009_Pnd_Cis_Da_Rea_Proceeds_Amt = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Da_Rea_Proceeds_Amt", "#CIS-DA-REA-PROCEEDS-AMT", 
            FieldType.DECIMAL, 11,2);
        adsl6009_Pnd_Cis_Da_Tiaa_Proceeds_Amt = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Da_Tiaa_Proceeds_Amt", "#CIS-DA-TIAA-PROCEEDS-AMT", 
            FieldType.DECIMAL, 11,2);
        adsl6009_Pnd_Cis_Da_Cert_Nbr = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Da_Cert_Nbr", "#CIS-DA-CERT-NBR", FieldType.STRING, 10);
        adsl6009_Pnd_Cis_Da_Cref_Proceeds_Amt = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Da_Cref_Proceeds_Amt", "#CIS-DA-CREF-PROCEEDS-AMT", 
            FieldType.DECIMAL, 11,2);
        adsl6009_Pnd_Cis_Tiaa_Doi = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Tiaa_Doi", "#CIS-TIAA-DOI", FieldType.STRING, 8);
        adsl6009_Pnd_Cis_Cref_Doi = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Cref_Doi", "#CIS-CREF-DOI", FieldType.STRING, 8);
        adsl6009_Pnd_Cis_Cntrct_Type1 = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Cntrct_Type1", "#CIS-CNTRCT-TYPE1", FieldType.STRING, 
            1);
        adsl6009_Pnd_Cis_Annty_End_Dte = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Annty_End_Dte", "#CIS-ANNTY-END-DTE", FieldType.STRING, 
            8);
        adsl6009_Pnd_Cis_Grnted_Grd_Amt = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Grnted_Grd_Amt", "#CIS-GRNTED-GRD-AMT", FieldType.DECIMAL, 
            11,2);
        adsl6009_Pnd_Cis_Grnted_Std_Amt = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Grnted_Std_Amt", "#CIS-GRNTED-STD-AMT", FieldType.DECIMAL, 
            11,2);
        adsl6009_Pnd_Cis_Div_Grd_Amt = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Div_Grd_Amt", "#CIS-DIV-GRD-AMT", FieldType.DECIMAL, 
            11,2);
        adsl6009_Pnd_Cis_Div_Std_Amt = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Div_Std_Amt", "#CIS-DIV-STD-AMT", FieldType.DECIMAL, 
            11,2);
        adsl6009_Pnd_Cis_Mdo_Traditional_Amt = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Mdo_Traditional_Amt", "#CIS-MDO-TRADITIONAL-AMT", 
            FieldType.DECIMAL, 11,2);
        adsl6009_Pnd_Cis_Tiaa_Commuted_Info = adsl6009_Pnd_DataRedef1.newGroupArrayInGroup("adsl6009_Pnd_Cis_Tiaa_Commuted_Info", "#CIS-TIAA-COMMUTED-INFO", 
            new DbsArrayController(1,5));
        adsl6009_Pnd_Cis_Comut_Grnted_Amt = adsl6009_Pnd_Cis_Tiaa_Commuted_Info.newFieldInGroup("adsl6009_Pnd_Cis_Comut_Grnted_Amt", "#CIS-COMUT-GRNTED-AMT", 
            FieldType.DECIMAL, 11,2);
        adsl6009_Pnd_Cis_Comut_Int_Rate = adsl6009_Pnd_Cis_Tiaa_Commuted_Info.newFieldInGroup("adsl6009_Pnd_Cis_Comut_Int_Rate", "#CIS-COMUT-INT-RATE", 
            FieldType.DECIMAL, 5,3);
        adsl6009_Pnd_Cis_Comut_Pymt_Method = adsl6009_Pnd_Cis_Tiaa_Commuted_Info.newFieldInGroup("adsl6009_Pnd_Cis_Comut_Pymt_Method", "#CIS-COMUT-PYMT-METHOD", 
            FieldType.STRING, 8);
        adsl6009_Pnd_Cis_Lob_Type = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Lob_Type", "#CIS-LOB-TYPE", FieldType.STRING, 1);
        adsl6009_Pnd_Cis_Lob = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Lob", "#CIS-LOB", FieldType.STRING, 1);
        adsl6009_Pnd_Cis_Tiaa_Commuted_Info_2 = adsl6009_Pnd_DataRedef1.newGroupArrayInGroup("adsl6009_Pnd_Cis_Tiaa_Commuted_Info_2", "#CIS-TIAA-COMMUTED-INFO-2", 
            new DbsArrayController(1,20));
        adsl6009_Pnd_Cis_Comut_Grnted_Amt_2 = adsl6009_Pnd_Cis_Tiaa_Commuted_Info_2.newFieldInGroup("adsl6009_Pnd_Cis_Comut_Grnted_Amt_2", "#CIS-COMUT-GRNTED-AMT-2", 
            FieldType.DECIMAL, 11,2);
        adsl6009_Pnd_Cis_Comut_Pymt_Method_2 = adsl6009_Pnd_Cis_Tiaa_Commuted_Info_2.newFieldInGroup("adsl6009_Pnd_Cis_Comut_Pymt_Method_2", "#CIS-COMUT-PYMT-METHOD-2", 
            FieldType.STRING, 8);
        adsl6009_Pnd_Cis_Comut_Int_Rate_2 = adsl6009_Pnd_Cis_Tiaa_Commuted_Info_2.newFieldInGroup("adsl6009_Pnd_Cis_Comut_Int_Rate_2", "#CIS-COMUT-INT-RATE-2", 
            FieldType.DECIMAL, 5,3);
        adsl6009_Pnd_Cis_Comut_Mortality_Basis = adsl6009_Pnd_Cis_Tiaa_Commuted_Info_2.newFieldInGroup("adsl6009_Pnd_Cis_Comut_Mortality_Basis", "#CIS-COMUT-MORTALITY-BASIS", 
            FieldType.STRING, 30);
        adsl6009_Pnd_Cis_Rea_Annual_Nbr_Units = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Rea_Annual_Nbr_Units", "#CIS-REA-ANNUAL-NBR-UNITS", 
            FieldType.DECIMAL, 12,4);
        adsl6009_Pnd_Cis_Rea_Mnthly_Nbr_Units = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Rea_Mnthly_Nbr_Units", "#CIS-REA-MNTHLY-NBR-UNITS", 
            FieldType.DECIMAL, 12,4);
        adsl6009_Pnd_Cis_Tacc_Annual_Nbr_Units = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Tacc_Annual_Nbr_Units", "#CIS-TACC-ANNUAL-NBR-UNITS", 
            FieldType.DECIMAL, 12,4);
        adsl6009_Pnd_Cis_Tacc_Mnthly_Nbr_Units = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Tacc_Mnthly_Nbr_Units", "#CIS-TACC-MNTHLY-NBR-UNITS", 
            FieldType.DECIMAL, 12,4);
        adsl6009_Pnd_Cis_Tacc_Ind = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Tacc_Ind", "#CIS-TACC-IND", FieldType.STRING, 1);
        adsl6009_Pnd_Cis_Tacc_Account = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Tacc_Account", "#CIS-TACC-ACCOUNT", FieldType.STRING, 
            10);
        adsl6009_Pnd_Cis_Cref_Annty_Pymnt = adsl6009_Pnd_DataRedef1.newGroupArrayInGroup("adsl6009_Pnd_Cis_Cref_Annty_Pymnt", "#CIS-CREF-ANNTY-PYMNT", 
            new DbsArrayController(1,20));
        adsl6009_Pnd_Cis_Cref_Mnthly_Nbr_Units = adsl6009_Pnd_Cis_Cref_Annty_Pymnt.newFieldInGroup("adsl6009_Pnd_Cis_Cref_Mnthly_Nbr_Units", "#CIS-CREF-MNTHLY-NBR-UNITS", 
            FieldType.DECIMAL, 12,4);
        adsl6009_Pnd_Cis_Cref_Annual_Nbr_Units = adsl6009_Pnd_Cis_Cref_Annty_Pymnt.newFieldInGroup("adsl6009_Pnd_Cis_Cref_Annual_Nbr_Units", "#CIS-CREF-ANNUAL-NBR-UNITS", 
            FieldType.DECIMAL, 12,4);
        adsl6009_Pnd_Cis_Cref_Acct_Cde = adsl6009_Pnd_Cis_Cref_Annty_Pymnt.newFieldInGroup("adsl6009_Pnd_Cis_Cref_Acct_Cde", "#CIS-CREF-ACCT-CDE", FieldType.STRING, 
            1);
        adsl6009_Pnd_Cis_Sg_Fund_Ticker = adsl6009_Pnd_Cis_Cref_Annty_Pymnt.newFieldInGroup("adsl6009_Pnd_Cis_Sg_Fund_Ticker", "#CIS-SG-FUND-TICKER", 
            FieldType.STRING, 10);
        adsl6009_Pnd_Cis_Rea_Annl_Annty_Amt = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Rea_Annl_Annty_Amt", "#CIS-REA-ANNL-ANNTY-AMT", 
            FieldType.DECIMAL, 11,2);
        adsl6009_Pnd_Cis_Rea_Mnthly_Annty_Amt = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Rea_Mnthly_Annty_Amt", "#CIS-REA-MNTHLY-ANNTY-AMT", 
            FieldType.DECIMAL, 11,2);
        adsl6009_Pnd_Cis_Cref_Annl_Annty_Amt = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Cref_Annl_Annty_Amt", "#CIS-CREF-ANNL-ANNTY-AMT", 
            FieldType.DECIMAL, 11,2);
        adsl6009_Pnd_Cis_Cref_Mnthly_Annty_Amt = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Cref_Mnthly_Annty_Amt", "#CIS-CREF-MNTHLY-ANNTY-AMT", 
            FieldType.DECIMAL, 11,2);
        adsl6009_Pnd_Cis_Tacc_Annl_Annty_Amt = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Tacc_Annl_Annty_Amt", "#CIS-TACC-ANNL-ANNTY-AMT", 
            FieldType.DECIMAL, 11,2);
        adsl6009_Pnd_Cis_Tacc_Mnthly_Annty_Amt = adsl6009_Pnd_DataRedef1.newFieldInGroup("adsl6009_Pnd_Cis_Tacc_Mnthly_Annty_Amt", "#CIS-TACC-MNTHLY-ANNTY-AMT", 
            FieldType.DECIMAL, 11,2);

        this.setRecordName("LdaAdsl6009");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAdsl6009() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
