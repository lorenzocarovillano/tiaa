/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:22:30 PM
**        * FROM NATURAL PDA     : NMDPDA_E
************************************************************
**        * FILE NAME            : PdaNmdpda_E.java
**        * CLASS NAME           : PdaNmdpda_E
**        * INSTANCE NAME        : PdaNmdpda_E
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaNmdpda_E extends PdaBase
{
    // Properties
    private DbsGroup error_Info_Sub;
    private DbsField error_Info_Sub_Pnd_Pnd_Last_Program;
    private DbsField error_Info_Sub_Pnd_Pnd_Last_Error_Time;

    public DbsGroup getError_Info_Sub() { return error_Info_Sub; }

    public DbsField getError_Info_Sub_Pnd_Pnd_Last_Program() { return error_Info_Sub_Pnd_Pnd_Last_Program; }

    public DbsField getError_Info_Sub_Pnd_Pnd_Last_Error_Time() { return error_Info_Sub_Pnd_Pnd_Last_Error_Time; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        error_Info_Sub = dbsRecord.newGroupInRecord("error_Info_Sub", "ERROR-INFO-SUB");
        error_Info_Sub.setParameterOption(ParameterOption.ByReference);
        error_Info_Sub_Pnd_Pnd_Last_Program = error_Info_Sub.newFieldInGroup("error_Info_Sub_Pnd_Pnd_Last_Program", "##LAST-PROGRAM", FieldType.STRING, 
            8);
        error_Info_Sub_Pnd_Pnd_Last_Error_Time = error_Info_Sub.newFieldInGroup("error_Info_Sub_Pnd_Pnd_Last_Error_Time", "##LAST-ERROR-TIME", FieldType.TIME);

        dbsRecord.reset();
    }

    // Constructors
    public PdaNmdpda_E(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

