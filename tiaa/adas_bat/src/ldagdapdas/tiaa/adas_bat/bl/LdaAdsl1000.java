/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:49:19 PM
**        * FROM NATURAL LDA     : ADSL1000
************************************************************
**        * FILE NAME            : LdaAdsl1000.java
**        * CLASS NAME           : LdaAdsl1000
**        * INSTANCE NAME        : LdaAdsl1000
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdsl1000 extends DbsRecord
{
    // Properties
    private DbsField pnd_Accounting_Date;
    private DbsGroup pnd_Accounting_DateRedef1;
    private DbsField pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm;
    private DbsField pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd;
    private DbsField pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc;
    private DbsField pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy;
    private DbsField pnd_Effective_Date;
    private DbsGroup pnd_Effective_DateRedef2;
    private DbsField pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Mm;
    private DbsField pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Dd;
    private DbsField pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Cc;
    private DbsField pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Yy;
    private DbsField pnd_Check_Cycle;
    private DbsGroup pnd_Check_CycleRedef3;
    private DbsField pnd_Check_Cycle_Pnd_Check_Cycle_Mcdy_Mm;
    private DbsField pnd_Check_Cycle_Pnd_Check_Cycle_Mcdy_Dd;
    private DbsField pnd_Check_Cycle_Pnd_Check_Cycle_Mcdy_Cc;
    private DbsField pnd_Check_Cycle_Pnd_Check_Cycle_Mcdy_Yy;
    private DbsGroup pnd_Tiaa_Minor_Accum;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Maturity_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Maturity_Amt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Grd_Maturity_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Grd_Maturity_Amt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Rate_Std_Maturity_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Std_Maturity_Amt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Rate_Std_Maturity_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Std_Maturity_Amt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Amt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Amt;
    private DbsGroup pnd_Tiaa_Sub_Accum;
    private DbsField pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt;
    private DbsField pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt;
    private DbsGroup pnd_Cref_Monthly_Minor_Accum;
    private DbsField pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt;
    private DbsField pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt;
    private DbsGroup pnd_Cref_Annually_Minor_Accum;
    private DbsField pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt;
    private DbsField pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt;
    private DbsGroup pnd_Cref_Monthly_Sub_Total_Accum;
    private DbsField pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Maturity_Cnt;
    private DbsField pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Maturity_Amt;
    private DbsGroup pnd_Cref_Annually_Sub_Total_Accum;
    private DbsField pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Maturity_Cnt;
    private DbsField pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Maturity_Amt;
    private DbsGroup pnd_Total_Cref_And_Tiaa_Accum;
    private DbsField pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt;
    private DbsField pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt;
    private DbsGroup pnd_Tiaa_Grand_Accum;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Cnt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Amt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Cnt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Amt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Tpa_Grn_Std_Maturity_Cnt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Tpa_Grn_Std_Maturity_Amt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Cnt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Amt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Cnt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Amt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Cnt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Amt;
    private DbsGroup pnd_Tiaa_Sub_Grn_Accum;
    private DbsField pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt;
    private DbsField pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt;
    private DbsGroup pnd_Cref_Monthly_Grand_Accum;
    private DbsField pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt;
    private DbsField pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt;
    private DbsGroup pnd_Cref_Annually_Grand_Accum;
    private DbsField pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt;
    private DbsField pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt;
    private DbsGroup pnd_Cref_Monthly_Sub_Grand_Accum;
    private DbsField pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Cnt;
    private DbsField pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Amt;
    private DbsGroup pnd_Cref_Annually_Sub_Grand_Accum;
    private DbsField pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Cnt;
    private DbsField pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Amt;
    private DbsGroup pnd_Cref_And_Tiaa_Grand_Accum;
    private DbsField pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt;
    private DbsField pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt;

    public DbsField getPnd_Accounting_Date() { return pnd_Accounting_Date; }

    public DbsGroup getPnd_Accounting_DateRedef1() { return pnd_Accounting_DateRedef1; }

    public DbsField getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm() { return pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm; }

    public DbsField getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd() { return pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd; }

    public DbsField getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc() { return pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc; }

    public DbsField getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy() { return pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy; }

    public DbsField getPnd_Effective_Date() { return pnd_Effective_Date; }

    public DbsGroup getPnd_Effective_DateRedef2() { return pnd_Effective_DateRedef2; }

    public DbsField getPnd_Effective_Date_Pnd_Effective_Date_Mcdy_Mm() { return pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Mm; }

    public DbsField getPnd_Effective_Date_Pnd_Effective_Date_Mcdy_Dd() { return pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Dd; }

    public DbsField getPnd_Effective_Date_Pnd_Effective_Date_Mcdy_Cc() { return pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Cc; }

    public DbsField getPnd_Effective_Date_Pnd_Effective_Date_Mcdy_Yy() { return pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Yy; }

    public DbsField getPnd_Check_Cycle() { return pnd_Check_Cycle; }

    public DbsGroup getPnd_Check_CycleRedef3() { return pnd_Check_CycleRedef3; }

    public DbsField getPnd_Check_Cycle_Pnd_Check_Cycle_Mcdy_Mm() { return pnd_Check_Cycle_Pnd_Check_Cycle_Mcdy_Mm; }

    public DbsField getPnd_Check_Cycle_Pnd_Check_Cycle_Mcdy_Dd() { return pnd_Check_Cycle_Pnd_Check_Cycle_Mcdy_Dd; }

    public DbsField getPnd_Check_Cycle_Pnd_Check_Cycle_Mcdy_Cc() { return pnd_Check_Cycle_Pnd_Check_Cycle_Mcdy_Cc; }

    public DbsField getPnd_Check_Cycle_Pnd_Check_Cycle_Mcdy_Yy() { return pnd_Check_Cycle_Pnd_Check_Cycle_Mcdy_Yy; }

    public DbsGroup getPnd_Tiaa_Minor_Accum() { return pnd_Tiaa_Minor_Accum; }

    public DbsField getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Maturity_Cnt() { return pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Maturity_Cnt; }

    public DbsField getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Maturity_Amt() { return pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Maturity_Amt; }

    public DbsField getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Grd_Maturity_Cnt() { return pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Grd_Maturity_Cnt; }

    public DbsField getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Grd_Maturity_Amt() { return pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Grd_Maturity_Amt; }

    public DbsField getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Rate_Std_Maturity_Cnt() { return pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Rate_Std_Maturity_Cnt; }

    public DbsField getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Std_Maturity_Amt() { return pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Std_Maturity_Amt; }

    public DbsField getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Rate_Std_Maturity_Cnt() { return pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Rate_Std_Maturity_Cnt; }

    public DbsField getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Std_Maturity_Amt() { return pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Std_Maturity_Amt; }

    public DbsField getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Cnt() { return pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Cnt; }

    public DbsField getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Amt() { return pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Amt; }

    public DbsField getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Cnt() { return pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Cnt; }

    public DbsField getPnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Amt() { return pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Amt; }

    public DbsGroup getPnd_Tiaa_Sub_Accum() { return pnd_Tiaa_Sub_Accum; }

    public DbsField getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt() { return pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt; }

    public DbsField getPnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt() { return pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt; }

    public DbsGroup getPnd_Cref_Monthly_Minor_Accum() { return pnd_Cref_Monthly_Minor_Accum; }

    public DbsField getPnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt() { return pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt; }

    public DbsField getPnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt() { return pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt; }

    public DbsGroup getPnd_Cref_Annually_Minor_Accum() { return pnd_Cref_Annually_Minor_Accum; }

    public DbsField getPnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt() { return pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt; }

    public DbsField getPnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt() { return pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt; }

    public DbsGroup getPnd_Cref_Monthly_Sub_Total_Accum() { return pnd_Cref_Monthly_Sub_Total_Accum; }

    public DbsField getPnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Maturity_Cnt() { return pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Maturity_Cnt; 
        }

    public DbsField getPnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Maturity_Amt() { return pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Maturity_Amt; 
        }

    public DbsGroup getPnd_Cref_Annually_Sub_Total_Accum() { return pnd_Cref_Annually_Sub_Total_Accum; }

    public DbsField getPnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Maturity_Cnt() { return pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Maturity_Cnt; 
        }

    public DbsField getPnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Maturity_Amt() { return pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Maturity_Amt; 
        }

    public DbsGroup getPnd_Total_Cref_And_Tiaa_Accum() { return pnd_Total_Cref_And_Tiaa_Accum; }

    public DbsField getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt() { return pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt; 
        }

    public DbsField getPnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt() { return pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt; }

    public DbsGroup getPnd_Tiaa_Grand_Accum() { return pnd_Tiaa_Grand_Accum; }

    public DbsField getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Cnt() { return pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Cnt; }

    public DbsField getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Amt() { return pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Amt; }

    public DbsField getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Cnt() { return pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Cnt; }

    public DbsField getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Amt() { return pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Amt; }

    public DbsField getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Tpa_Grn_Std_Maturity_Cnt() { return pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Tpa_Grn_Std_Maturity_Cnt; }

    public DbsField getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Tpa_Grn_Std_Maturity_Amt() { return pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Tpa_Grn_Std_Maturity_Amt; }

    public DbsField getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Cnt() { return pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Cnt; }

    public DbsField getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Amt() { return pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Amt; }

    public DbsField getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Cnt() { return pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Cnt; }

    public DbsField getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Amt() { return pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Amt; }

    public DbsField getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Cnt() { return pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Cnt; }

    public DbsField getPnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Amt() { return pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Amt; }

    public DbsGroup getPnd_Tiaa_Sub_Grn_Accum() { return pnd_Tiaa_Sub_Grn_Accum; }

    public DbsField getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt() { return pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt; }

    public DbsField getPnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt() { return pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt; }

    public DbsGroup getPnd_Cref_Monthly_Grand_Accum() { return pnd_Cref_Monthly_Grand_Accum; }

    public DbsField getPnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt() { return pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt; }

    public DbsField getPnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt() { return pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt; }

    public DbsGroup getPnd_Cref_Annually_Grand_Accum() { return pnd_Cref_Annually_Grand_Accum; }

    public DbsField getPnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt() { return pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt; }

    public DbsField getPnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt() { return pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt; }

    public DbsGroup getPnd_Cref_Monthly_Sub_Grand_Accum() { return pnd_Cref_Monthly_Sub_Grand_Accum; }

    public DbsField getPnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Cnt() { return pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Cnt; 
        }

    public DbsField getPnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Amt() { return pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Amt; 
        }

    public DbsGroup getPnd_Cref_Annually_Sub_Grand_Accum() { return pnd_Cref_Annually_Sub_Grand_Accum; }

    public DbsField getPnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Cnt() { return pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Cnt; 
        }

    public DbsField getPnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Amt() { return pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Amt; 
        }

    public DbsGroup getPnd_Cref_And_Tiaa_Grand_Accum() { return pnd_Cref_And_Tiaa_Grand_Accum; }

    public DbsField getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt() { return pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt; 
        }

    public DbsField getPnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt() { return pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Accounting_Date = newFieldInRecord("pnd_Accounting_Date", "#ACCOUNTING-DATE", FieldType.NUMERIC, 8);
        pnd_Accounting_DateRedef1 = newGroupInRecord("pnd_Accounting_DateRedef1", "Redefines", pnd_Accounting_Date);
        pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm = pnd_Accounting_DateRedef1.newFieldInGroup("pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm", 
            "#ACCOUNTING-DATE-MCDY-MM", FieldType.NUMERIC, 2);
        pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd = pnd_Accounting_DateRedef1.newFieldInGroup("pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd", 
            "#ACCOUNTING-DATE-MCDY-DD", FieldType.NUMERIC, 2);
        pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc = pnd_Accounting_DateRedef1.newFieldInGroup("pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc", 
            "#ACCOUNTING-DATE-MCDY-CC", FieldType.NUMERIC, 2);
        pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy = pnd_Accounting_DateRedef1.newFieldInGroup("pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy", 
            "#ACCOUNTING-DATE-MCDY-YY", FieldType.NUMERIC, 2);

        pnd_Effective_Date = newFieldInRecord("pnd_Effective_Date", "#EFFECTIVE-DATE", FieldType.NUMERIC, 8);
        pnd_Effective_DateRedef2 = newGroupInRecord("pnd_Effective_DateRedef2", "Redefines", pnd_Effective_Date);
        pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Mm = pnd_Effective_DateRedef2.newFieldInGroup("pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Mm", "#EFFECTIVE-DATE-MCDY-MM", 
            FieldType.NUMERIC, 2);
        pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Dd = pnd_Effective_DateRedef2.newFieldInGroup("pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Dd", "#EFFECTIVE-DATE-MCDY-DD", 
            FieldType.NUMERIC, 2);
        pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Cc = pnd_Effective_DateRedef2.newFieldInGroup("pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Cc", "#EFFECTIVE-DATE-MCDY-CC", 
            FieldType.NUMERIC, 2);
        pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Yy = pnd_Effective_DateRedef2.newFieldInGroup("pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Yy", "#EFFECTIVE-DATE-MCDY-YY", 
            FieldType.NUMERIC, 2);

        pnd_Check_Cycle = newFieldInRecord("pnd_Check_Cycle", "#CHECK-CYCLE", FieldType.NUMERIC, 8);
        pnd_Check_CycleRedef3 = newGroupInRecord("pnd_Check_CycleRedef3", "Redefines", pnd_Check_Cycle);
        pnd_Check_Cycle_Pnd_Check_Cycle_Mcdy_Mm = pnd_Check_CycleRedef3.newFieldInGroup("pnd_Check_Cycle_Pnd_Check_Cycle_Mcdy_Mm", "#CHECK-CYCLE-MCDY-MM", 
            FieldType.NUMERIC, 2);
        pnd_Check_Cycle_Pnd_Check_Cycle_Mcdy_Dd = pnd_Check_CycleRedef3.newFieldInGroup("pnd_Check_Cycle_Pnd_Check_Cycle_Mcdy_Dd", "#CHECK-CYCLE-MCDY-DD", 
            FieldType.NUMERIC, 2);
        pnd_Check_Cycle_Pnd_Check_Cycle_Mcdy_Cc = pnd_Check_CycleRedef3.newFieldInGroup("pnd_Check_Cycle_Pnd_Check_Cycle_Mcdy_Cc", "#CHECK-CYCLE-MCDY-CC", 
            FieldType.NUMERIC, 2);
        pnd_Check_Cycle_Pnd_Check_Cycle_Mcdy_Yy = pnd_Check_CycleRedef3.newFieldInGroup("pnd_Check_Cycle_Pnd_Check_Cycle_Mcdy_Yy", "#CHECK-CYCLE-MCDY-YY", 
            FieldType.NUMERIC, 2);

        pnd_Tiaa_Minor_Accum = newGroupInRecord("pnd_Tiaa_Minor_Accum", "#TIAA-MINOR-ACCUM");
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Maturity_Cnt = pnd_Tiaa_Minor_Accum.newFieldInGroup("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Maturity_Cnt", 
            "#TIAA-RATE-STD-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Maturity_Amt = pnd_Tiaa_Minor_Accum.newFieldInGroup("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Maturity_Amt", "#TIAA-STD-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Grd_Maturity_Cnt = pnd_Tiaa_Minor_Accum.newFieldInGroup("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Grd_Maturity_Cnt", 
            "#TIAA-RATE-GRD-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Grd_Maturity_Amt = pnd_Tiaa_Minor_Accum.newFieldInGroup("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Grd_Maturity_Amt", "#TIAA-GRD-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Rate_Std_Maturity_Cnt = pnd_Tiaa_Minor_Accum.newFieldInGroup("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Rate_Std_Maturity_Cnt", 
            "#TIAA-TPA-RATE-STD-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Std_Maturity_Amt = pnd_Tiaa_Minor_Accum.newFieldInGroup("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Std_Maturity_Amt", 
            "#TIAA-TPA-STD-MATURITY-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Rate_Std_Maturity_Cnt = pnd_Tiaa_Minor_Accum.newFieldInGroup("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Rate_Std_Maturity_Cnt", 
            "#TIAA-IPRO-RATE-STD-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Std_Maturity_Amt = pnd_Tiaa_Minor_Accum.newFieldInGroup("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Std_Maturity_Amt", 
            "#TIAA-IPRO-STD-MATURITY-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Cnt = pnd_Tiaa_Minor_Accum.newFieldInGroup("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Cnt", 
            "#TIAA-STBL-STD-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Amt = pnd_Tiaa_Minor_Accum.newFieldInGroup("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Amt", 
            "#TIAA-STBL-STD-MATURITY-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Cnt = pnd_Tiaa_Minor_Accum.newFieldInGroup("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Cnt", 
            "#TIAA-STBL-GRD-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Amt = pnd_Tiaa_Minor_Accum.newFieldInGroup("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Amt", 
            "#TIAA-STBL-GRD-MATURITY-AMT", FieldType.PACKED_DECIMAL, 11,2);

        pnd_Tiaa_Sub_Accum = newGroupInRecord("pnd_Tiaa_Sub_Accum", "#TIAA-SUB-ACCUM");
        pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt = pnd_Tiaa_Sub_Accum.newFieldInGroup("pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt", "#TIAA-SUB-RATE-MATURITY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt = pnd_Tiaa_Sub_Accum.newFieldInGroup("pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt", "#TIAA-SUB-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 14,2);

        pnd_Cref_Monthly_Minor_Accum = newGroupArrayInRecord("pnd_Cref_Monthly_Minor_Accum", "#CREF-MONTHLY-MINOR-ACCUM", new DbsArrayController(1,20));
        pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt = pnd_Cref_Monthly_Minor_Accum.newFieldInGroup("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt", 
            "#CREF-MTH-MAT-CNT", FieldType.NUMERIC, 7);
        pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt = pnd_Cref_Monthly_Minor_Accum.newFieldInGroup("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt", 
            "#CREF-MTH-MAT-AMT", FieldType.PACKED_DECIMAL, 11,2);

        pnd_Cref_Annually_Minor_Accum = newGroupArrayInRecord("pnd_Cref_Annually_Minor_Accum", "#CREF-ANNUALLY-MINOR-ACCUM", new DbsArrayController(1,
            20));
        pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt = pnd_Cref_Annually_Minor_Accum.newFieldInGroup("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt", 
            "#CREF-ANN-MAT-CNT", FieldType.NUMERIC, 7);
        pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt = pnd_Cref_Annually_Minor_Accum.newFieldInGroup("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt", 
            "#CREF-ANN-MAT-AMT", FieldType.PACKED_DECIMAL, 11,2);

        pnd_Cref_Monthly_Sub_Total_Accum = newGroupInRecord("pnd_Cref_Monthly_Sub_Total_Accum", "#CREF-MONTHLY-SUB-TOTAL-ACCUM");
        pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Maturity_Cnt = pnd_Cref_Monthly_Sub_Total_Accum.newFieldInGroup("pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Maturity_Cnt", 
            "#CREF-SUB-RATE-MTH-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Maturity_Amt = pnd_Cref_Monthly_Sub_Total_Accum.newFieldInGroup("pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Maturity_Amt", 
            "#CREF-SUB-MTH-MATURITY-AMT", FieldType.PACKED_DECIMAL, 14,2);

        pnd_Cref_Annually_Sub_Total_Accum = newGroupInRecord("pnd_Cref_Annually_Sub_Total_Accum", "#CREF-ANNUALLY-SUB-TOTAL-ACCUM");
        pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Maturity_Cnt = pnd_Cref_Annually_Sub_Total_Accum.newFieldInGroup("pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Maturity_Cnt", 
            "#CREF-SUB-RATE-ANN-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Maturity_Amt = pnd_Cref_Annually_Sub_Total_Accum.newFieldInGroup("pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Maturity_Amt", 
            "#CREF-SUB-ANN-MATURITY-AMT", FieldType.PACKED_DECIMAL, 14,2);

        pnd_Total_Cref_And_Tiaa_Accum = newGroupInRecord("pnd_Total_Cref_And_Tiaa_Accum", "#TOTAL-CREF-AND-TIAA-ACCUM");
        pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt = pnd_Total_Cref_And_Tiaa_Accum.newFieldInGroup("pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt", 
            "#TOTAL-RATE-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt = pnd_Total_Cref_And_Tiaa_Accum.newFieldInGroup("pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt", 
            "#TOTAL-MATURITY-AMT", FieldType.PACKED_DECIMAL, 14,2);

        pnd_Tiaa_Grand_Accum = newGroupInRecord("pnd_Tiaa_Grand_Accum", "#TIAA-GRAND-ACCUM");
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Cnt = pnd_Tiaa_Grand_Accum.newFieldInGroup("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Cnt", 
            "#TIAA-GRN-STD-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Amt = pnd_Tiaa_Grand_Accum.newFieldInGroup("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Amt", 
            "#TIAA-GRN-STD-MATURITY-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Cnt = pnd_Tiaa_Grand_Accum.newFieldInGroup("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Cnt", 
            "#TIAA-GRN-GRD-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Amt = pnd_Tiaa_Grand_Accum.newFieldInGroup("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Amt", 
            "#TIAA-GRN-GRD-MATURITY-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Tpa_Grn_Std_Maturity_Cnt = pnd_Tiaa_Grand_Accum.newFieldInGroup("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Tpa_Grn_Std_Maturity_Cnt", 
            "#TIAA-TPA-GRN-STD-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Tpa_Grn_Std_Maturity_Amt = pnd_Tiaa_Grand_Accum.newFieldInGroup("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Tpa_Grn_Std_Maturity_Amt", 
            "#TIAA-TPA-GRN-STD-MATURITY-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Cnt = pnd_Tiaa_Grand_Accum.newFieldInGroup("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Cnt", 
            "#TIAA-IPRO-GRN-STD-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Amt = pnd_Tiaa_Grand_Accum.newFieldInGroup("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Amt", 
            "#TIAA-IPRO-GRN-STD-MATURITY-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Cnt = pnd_Tiaa_Grand_Accum.newFieldInGroup("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Cnt", 
            "#TIAA-STBL-GRN-STD-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Amt = pnd_Tiaa_Grand_Accum.newFieldInGroup("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Amt", 
            "#TIAA-STBL-GRN-STD-MATURITY-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Cnt = pnd_Tiaa_Grand_Accum.newFieldInGroup("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Cnt", 
            "#TIAA-STBL-GRN-GRD-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Amt = pnd_Tiaa_Grand_Accum.newFieldInGroup("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Amt", 
            "#TIAA-STBL-GRN-GRD-MATURITY-AMT", FieldType.PACKED_DECIMAL, 14,2);

        pnd_Tiaa_Sub_Grn_Accum = newGroupInRecord("pnd_Tiaa_Sub_Grn_Accum", "#TIAA-SUB-GRN-ACCUM");
        pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt = pnd_Tiaa_Sub_Grn_Accum.newFieldInGroup("pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt", 
            "#TIAA-SUB-GRN-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt = pnd_Tiaa_Sub_Grn_Accum.newFieldInGroup("pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt", 
            "#TIAA-SUB-GRN-MATURITY-AMT", FieldType.PACKED_DECIMAL, 14,2);

        pnd_Cref_Monthly_Grand_Accum = newGroupArrayInRecord("pnd_Cref_Monthly_Grand_Accum", "#CREF-MONTHLY-GRAND-ACCUM", new DbsArrayController(1,20));
        pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt = pnd_Cref_Monthly_Grand_Accum.newFieldInGroup("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt", 
            "#CREF-GRN-MTH-MAT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt = pnd_Cref_Monthly_Grand_Accum.newFieldInGroup("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt", 
            "#CREF-GRN-MTH-MAT-AMT", FieldType.PACKED_DECIMAL, 14,2);

        pnd_Cref_Annually_Grand_Accum = newGroupArrayInRecord("pnd_Cref_Annually_Grand_Accum", "#CREF-ANNUALLY-GRAND-ACCUM", new DbsArrayController(1,
            20));
        pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt = pnd_Cref_Annually_Grand_Accum.newFieldInGroup("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt", 
            "#CREF-GRN-ANN-MAT-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt = pnd_Cref_Annually_Grand_Accum.newFieldInGroup("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt", 
            "#CREF-GRN-ANN-MAT-AMT", FieldType.PACKED_DECIMAL, 14,2);

        pnd_Cref_Monthly_Sub_Grand_Accum = newGroupInRecord("pnd_Cref_Monthly_Sub_Grand_Accum", "#CREF-MONTHLY-SUB-GRAND-ACCUM");
        pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Cnt = pnd_Cref_Monthly_Sub_Grand_Accum.newFieldInGroup("pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Cnt", 
            "#CREF-SUB-GRN-MTH-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Amt = pnd_Cref_Monthly_Sub_Grand_Accum.newFieldInGroup("pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Amt", 
            "#CREF-SUB-GRN-MTH-MATURITY-AMT", FieldType.PACKED_DECIMAL, 14,2);

        pnd_Cref_Annually_Sub_Grand_Accum = newGroupInRecord("pnd_Cref_Annually_Sub_Grand_Accum", "#CREF-ANNUALLY-SUB-GRAND-ACCUM");
        pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Cnt = pnd_Cref_Annually_Sub_Grand_Accum.newFieldInGroup("pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Cnt", 
            "#CREF-SUB-GRN-ANN-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Amt = pnd_Cref_Annually_Sub_Grand_Accum.newFieldInGroup("pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Amt", 
            "#CREF-SUB-GRN-ANN-MATURITY-AMT", FieldType.PACKED_DECIMAL, 14,2);

        pnd_Cref_And_Tiaa_Grand_Accum = newGroupInRecord("pnd_Cref_And_Tiaa_Grand_Accum", "#CREF-AND-TIAA-GRAND-ACCUM");
        pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt = pnd_Cref_And_Tiaa_Grand_Accum.newFieldInGroup("pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt", 
            "#GRAND-RATE-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt = pnd_Cref_And_Tiaa_Grand_Accum.newFieldInGroup("pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt", 
            "#GRAND-MATURITY-AMT", FieldType.PACKED_DECIMAL, 14,2);

        this.setRecordName("LdaAdsl1000");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAdsl1000() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
