/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:23 PM
**        * FROM NATURAL LDA     : ADSLDA01
************************************************************
**        * FILE NAME            : LdaAdslda01.java
**        * CLASS NAME           : LdaAdslda01
**        * INSTANCE NAME        : LdaAdslda01
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdslda01 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_nst_Table_Entry_View;
    private DbsField nst_Table_Entry_View_Entry_Actve_Ind;
    private DbsField nst_Table_Entry_View_Entry_Table_Id_Nbr;
    private DbsField nst_Table_Entry_View_Entry_Table_Sub_Id;
    private DbsField nst_Table_Entry_View_Entry_Cde;
    private DbsField nst_Table_Entry_View_Entry_Dscrptn_Txt;
    private DbsField nst_Table_Entry_View_Entry_User_Id;
    private DbsGroup nst_Table_Entry_View_Entry_Trnsctn_Type_CdeMuGroup;
    private DbsField nst_Table_Entry_View_Entry_Trnsctn_Type_Cde;
    private DbsGroup nst_Table_Entry_View_Entry_Mit_Status_CdeMuGroup;
    private DbsField nst_Table_Entry_View_Entry_Mit_Status_Cde;
    private DbsGroup nst_Table_Entry_View_Addtnl_Dscrptn_TxtMuGroup;
    private DbsField nst_Table_Entry_View_Addtnl_Dscrptn_Txt;
    private DbsGroup nst_Table_Entry_View_Entry_Prgam_CdeMuGroup;
    private DbsField nst_Table_Entry_View_Entry_Prgam_Cde;
    private DbsField nst_Table_Entry_View_Hold_Cde;
    private DbsGroup nst_Table_Entry_View_Entry_Incmplt_Rsn_IndMuGroup;
    private DbsField nst_Table_Entry_View_Entry_Incmplt_Rsn_Ind;

    public DataAccessProgramView getVw_nst_Table_Entry_View() { return vw_nst_Table_Entry_View; }

    public DbsField getNst_Table_Entry_View_Entry_Actve_Ind() { return nst_Table_Entry_View_Entry_Actve_Ind; }

    public DbsField getNst_Table_Entry_View_Entry_Table_Id_Nbr() { return nst_Table_Entry_View_Entry_Table_Id_Nbr; }

    public DbsField getNst_Table_Entry_View_Entry_Table_Sub_Id() { return nst_Table_Entry_View_Entry_Table_Sub_Id; }

    public DbsField getNst_Table_Entry_View_Entry_Cde() { return nst_Table_Entry_View_Entry_Cde; }

    public DbsField getNst_Table_Entry_View_Entry_Dscrptn_Txt() { return nst_Table_Entry_View_Entry_Dscrptn_Txt; }

    public DbsField getNst_Table_Entry_View_Entry_User_Id() { return nst_Table_Entry_View_Entry_User_Id; }

    public DbsGroup getNst_Table_Entry_View_Entry_Trnsctn_Type_CdeMuGroup() { return nst_Table_Entry_View_Entry_Trnsctn_Type_CdeMuGroup; }

    public DbsField getNst_Table_Entry_View_Entry_Trnsctn_Type_Cde() { return nst_Table_Entry_View_Entry_Trnsctn_Type_Cde; }

    public DbsGroup getNst_Table_Entry_View_Entry_Mit_Status_CdeMuGroup() { return nst_Table_Entry_View_Entry_Mit_Status_CdeMuGroup; }

    public DbsField getNst_Table_Entry_View_Entry_Mit_Status_Cde() { return nst_Table_Entry_View_Entry_Mit_Status_Cde; }

    public DbsGroup getNst_Table_Entry_View_Addtnl_Dscrptn_TxtMuGroup() { return nst_Table_Entry_View_Addtnl_Dscrptn_TxtMuGroup; }

    public DbsField getNst_Table_Entry_View_Addtnl_Dscrptn_Txt() { return nst_Table_Entry_View_Addtnl_Dscrptn_Txt; }

    public DbsGroup getNst_Table_Entry_View_Entry_Prgam_CdeMuGroup() { return nst_Table_Entry_View_Entry_Prgam_CdeMuGroup; }

    public DbsField getNst_Table_Entry_View_Entry_Prgam_Cde() { return nst_Table_Entry_View_Entry_Prgam_Cde; }

    public DbsField getNst_Table_Entry_View_Hold_Cde() { return nst_Table_Entry_View_Hold_Cde; }

    public DbsGroup getNst_Table_Entry_View_Entry_Incmplt_Rsn_IndMuGroup() { return nst_Table_Entry_View_Entry_Incmplt_Rsn_IndMuGroup; }

    public DbsField getNst_Table_Entry_View_Entry_Incmplt_Rsn_Ind() { return nst_Table_Entry_View_Entry_Incmplt_Rsn_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_nst_Table_Entry_View = new DataAccessProgramView(new NameInfo("vw_nst_Table_Entry_View", "NST-TABLE-ENTRY-VIEW"), "NST_TABLE_ENTRY", "NST_TABLE_ENTRY");
        nst_Table_Entry_View_Entry_Actve_Ind = vw_nst_Table_Entry_View.getRecord().newFieldInGroup("nst_Table_Entry_View_Entry_Actve_Ind", "ENTRY-ACTVE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        nst_Table_Entry_View_Entry_Table_Id_Nbr = vw_nst_Table_Entry_View.getRecord().newFieldInGroup("nst_Table_Entry_View_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        nst_Table_Entry_View_Entry_Table_Id_Nbr.setDdmHeader("TABLE ID");
        nst_Table_Entry_View_Entry_Table_Sub_Id = vw_nst_Table_Entry_View.getRecord().newFieldInGroup("nst_Table_Entry_View_Entry_Table_Sub_Id", "ENTRY-TABLE-SUB-ID", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ENTRY_TABLE_SUB_ID");
        nst_Table_Entry_View_Entry_Cde = vw_nst_Table_Entry_View.getRecord().newFieldInGroup("nst_Table_Entry_View_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "ENTRY_CDE");
        nst_Table_Entry_View_Entry_Cde.setDdmHeader("ENTRY CODE");
        nst_Table_Entry_View_Entry_Dscrptn_Txt = vw_nst_Table_Entry_View.getRecord().newFieldInGroup("nst_Table_Entry_View_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "ENTRY_DSCRPTN_TXT");
        nst_Table_Entry_View_Entry_User_Id = vw_nst_Table_Entry_View.getRecord().newFieldInGroup("nst_Table_Entry_View_Entry_User_Id", "ENTRY-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_USER_ID");
        nst_Table_Entry_View_Entry_Trnsctn_Type_CdeMuGroup = vw_nst_Table_Entry_View.getRecord().newGroupInGroup("nst_Table_Entry_View_Entry_Trnsctn_Type_CdeMuGroup", 
            "ENTRY_TRNSCTN_TYPE_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NST_TABLE_ENTRY_ENTRY_TRNSCTN_TYPE_CDE");
        nst_Table_Entry_View_Entry_Trnsctn_Type_Cde = nst_Table_Entry_View_Entry_Trnsctn_Type_CdeMuGroup.newFieldArrayInGroup("nst_Table_Entry_View_Entry_Trnsctn_Type_Cde", 
            "ENTRY-TRNSCTN-TYPE-CDE", FieldType.STRING, 1, new DbsArrayController(1,30), RepeatingFieldStrategy.SubTableFieldArray, "ENTRY_TRNSCTN_TYPE_CDE");
        nst_Table_Entry_View_Entry_Mit_Status_CdeMuGroup = vw_nst_Table_Entry_View.getRecord().newGroupInGroup("nst_Table_Entry_View_Entry_Mit_Status_CdeMuGroup", 
            "ENTRY_MIT_STATUS_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NST_TABLE_ENTRY_ENTRY_MIT_STATUS_CDE");
        nst_Table_Entry_View_Entry_Mit_Status_Cde = nst_Table_Entry_View_Entry_Mit_Status_CdeMuGroup.newFieldArrayInGroup("nst_Table_Entry_View_Entry_Mit_Status_Cde", 
            "ENTRY-MIT-STATUS-CDE", FieldType.STRING, 4, new DbsArrayController(1,30), RepeatingFieldStrategy.SubTableFieldArray, "ENTRY_MIT_STATUS_CDE");
        nst_Table_Entry_View_Addtnl_Dscrptn_TxtMuGroup = vw_nst_Table_Entry_View.getRecord().newGroupInGroup("nst_Table_Entry_View_Addtnl_Dscrptn_TxtMuGroup", 
            "ADDTNL_DSCRPTN_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NST_TABLE_ENTRY_ADDTNL_DSCRPTN_TXT");
        nst_Table_Entry_View_Addtnl_Dscrptn_Txt = nst_Table_Entry_View_Addtnl_Dscrptn_TxtMuGroup.newFieldArrayInGroup("nst_Table_Entry_View_Addtnl_Dscrptn_Txt", 
            "ADDTNL-DSCRPTN-TXT", FieldType.STRING, 60, new DbsArrayController(1,10), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADDTNL_DSCRPTN_TXT");
        nst_Table_Entry_View_Entry_Prgam_CdeMuGroup = vw_nst_Table_Entry_View.getRecord().newGroupInGroup("nst_Table_Entry_View_Entry_Prgam_CdeMuGroup", 
            "ENTRY_PRGAM_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NST_TABLE_ENTRY_ENTRY_PRGAM_CDE");
        nst_Table_Entry_View_Entry_Prgam_Cde = nst_Table_Entry_View_Entry_Prgam_CdeMuGroup.newFieldArrayInGroup("nst_Table_Entry_View_Entry_Prgam_Cde", 
            "ENTRY-PRGAM-CDE", FieldType.STRING, 8, new DbsArrayController(1,50), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ENTRY_PRGAM_CDE");
        nst_Table_Entry_View_Hold_Cde = vw_nst_Table_Entry_View.getRecord().newFieldInGroup("nst_Table_Entry_View_Hold_Cde", "HOLD-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "HOLD_CDE");
        nst_Table_Entry_View_Entry_Incmplt_Rsn_IndMuGroup = vw_nst_Table_Entry_View.getRecord().newGroupInGroup("nst_Table_Entry_View_Entry_Incmplt_Rsn_IndMuGroup", 
            "ENTRY_INCMPLT_RSN_INDMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NST_TABLE_ENTRY_ENTRY_INCMPLT_RSN_IND");        nst_Table_Entry_View_Entry_Incmplt_Rsn_Ind 
            = nst_Table_Entry_View_Entry_Incmplt_Rsn_IndMuGroup.newFieldArrayInGroup("nst_Table_Entry_View_Entry_Incmplt_Rsn_Ind", "ENTRY-INCMPLT-RSN-IND", 
            FieldType.STRING, 20, new DbsArrayController(1,30), RepeatingFieldStrategy.SubTableFieldArray, "ENTRY_INCMPLT_RSN_IND");

        this.setRecordName("LdaAdslda01");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_nst_Table_Entry_View.reset();
    }

    // Constructor
    public LdaAdslda01() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
