/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:17:37 PM
**        * FROM NATURAL PDA     : ADSA600
************************************************************
**        * FILE NAME            : PdaAdsa600.java
**        * CLASS NAME           : PdaAdsa600
**        * INSTANCE NAME        : PdaAdsa600
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaAdsa600 extends PdaBase
{
    // Properties
    private DbsGroup adsa600;
    private DbsField adsa600_Adp_Wpid;
    private DbsField adsa600_Adp_Mit_Log_Dte_Tme;
    private DbsField adsa600_Adp_Ia_Tiaa_Nbr;
    private DbsField adsa600_Adp_Ia_Cref_Nbr;
    private DbsField adsa600_Adp_Ia_Tiaa_Rea_Nbr;
    private DbsField adsa600_Adp_Unique_Id;
    private DbsField adsa600_Adp_Frst_Annt_Ctznshp;
    private DbsField adsa600_Adp_Frst_Annt_Rsdnc_Cde;
    private DbsField adsa600_Adp_Orgnl_Issue_State;
    private DbsField adsa600_Adp_Frst_Annt_Sex_Cde;
    private DbsField adsa600_Adp_Frst_Annt_Frst_Nme;
    private DbsField adsa600_Adp_Frst_Annt_Mid_Nme;
    private DbsField adsa600_Adp_Frst_Annt_Lst_Nme;
    private DbsField adsa600_Adp_Frst_Annt_Dte_Of_Brth;
    private DbsField adsa600_Adp_Frst_Annt_Ssn;
    private DbsField adsa600_Adp_Scnd_Annt_Frst_Nme;
    private DbsField adsa600_Adp_Scnd_Annt_Mid_Nme;
    private DbsField adsa600_Adp_Scnd_Annt_Lst_Nme;
    private DbsField adsa600_Adp_Scnd_Annt_Dte_Of_Brth;
    private DbsField adsa600_Adp_Scnd_Annt_Ssn;
    private DbsField adsa600_Adp_Scnd_Annt_Sex_Cde;
    private DbsField adsa600_Adp_Alt_Payee_Ind;
    private DbsField adsa600_Adp_Alt_Dest_Addr_Ind;
    private DbsField adsa600_Adp_Rsttlmnt_Cnt;
    private DbsField adsa600_Adp_State_Of_Issue;
    private DbsField adsa600_Adp_Alt_Dest_Acct_Nbr;
    private DbsField adsa600_Adp_Alt_Dest_Hold_Cde;
    private DbsField adsa600_Adp_Alt_Dest_Rlvr_Dest;
    private DbsField adsa600_Adp_Ivc_Rlvr_Cde;
    private DbsField adsa600_Adp_Ivc_Pymnt_Rule;
    private DbsField adsa600_Adp_Hold_Cde;
    private DbsField adsa600_Adp_Plan_Nbr;
    private DbsField adsa600_Adp_Frm_Irc_Cde;
    private DbsField adsa600_Adp_To_Irc_Cde;
    private DbsField adsa600_Adp_Irc_Cde;
    private DbsField adsa600_Adp_Roth_Plan_Type;
    private DbsField adsa600_Adp_Roth_Frst_Cntrbtn_Dte;
    private DbsField adsa600_Adp_Roth_Dsblty_Dte;
    private DbsField adsa600_Adp_Roth_Rqst_Ind;
    private DbsField adsa600_Adp_Srvvr_Ind;
    private DbsField adsa600_Adp_Srvvr_Rltnshp;
    private DbsField adsa600_Adp_Mndtry_Tax_Ovrd;
    private DbsField adsa600_Adp_Bene_Client_Ind;
    private DbsField adsa600_Adi_Ia_Rcrd_Cde;
    private DbsField adsa600_Adi_Sqnce_Nbr;
    private DbsField adsa600_Adi_Annty_Strt_Dte;
    private DbsField adsa600_Adi_Instllmnt_Dte;
    private DbsField adsa600_Adi_Frst_Pymnt_Due_Dte;
    private DbsField adsa600_Adi_Tiaa_Ivc_Amt;
    private DbsField adsa600_Adi_Tiaa_Ivc_Gd_Ind;
    private DbsField adsa600_Adi_Tiaa_Prdc_Ivc_Amt;
    private DbsField adsa600_Adi_Cref_Ivc_Amt;
    private DbsField adsa600_Adi_Cref_Ivc_Gd_Ind;
    private DbsField adsa600_Adi_Cref_Prdc_Ivc_Amt;
    private DbsField adsa600_Adi_Frst_Ck_Pd_Dte;
    private DbsField adsa600_Adi_Finl_Periodic_Py_Dte;
    private DbsField adsa600_Adi_Finl_Prtl_Py_Dte;
    private DbsField adsa600_Adi_Optn_Cde;
    private DbsField adsa600_Adi_Pymnt_Cde;
    private DbsField adsa600_Adi_Pymnt_Mode;
    private DbsField adsa600_Adi_Tiaa_Nbrs;
    private DbsField adsa600_Adi_Cref_Nbrs;
    private DbsField adsa600_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt;
    private DbsField adsa600_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt;
    private DbsField adsa600_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt;
    private DbsField adsa600_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt;
    private DbsField adsa600_Pnd_Adi_Dtl_Tiaa_Acct_Cd;
    private DbsField adsa600_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd;
    private DbsField adsa600_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt;
    private DbsField adsa600_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt;
    private DbsField adsa600_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt;
    private DbsField adsa600_Pnd_Old_Tiaa_Grd_Grntd_Amt;
    private DbsField adsa600_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt;
    private DbsField adsa600_Pnd_Old_Tiaa_Grd_Dvdnd_Amt;
    private DbsField adsa600_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt;
    private DbsField adsa600_Pnd_Old_Tiaa_Std_Grntd_Amt;
    private DbsField adsa600_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt;
    private DbsField adsa600_Pnd_Old_Tiaa_Std_Dvdnd_Amt;
    private DbsField adsa600_Adi_Dtl_Cref_Da_Rate_Cd;
    private DbsField adsa600_Adi_Dtl_Cref_Acct_Cd;
    private DbsField adsa600_Adi_Dtl_Cref_Ia_Rate_Cd;
    private DbsField adsa600_Adi_Dtl_Cref_Mnthly_Nbr_Units;
    private DbsField adsa600_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val;
    private DbsField adsa600_Adi_Dtl_Cref_Da_Mnthly_Amt;
    private DbsField adsa600_Adi_Dtl_Cref_Mnthly_Amt;
    private DbsField adsa600_Adi_Dtl_Cref_Annl_Nbr_Units;
    private DbsField adsa600_Adi_Dtl_Cref_Ia_Annl_Unit_Val;
    private DbsField adsa600_Adi_Dtl_Cref_Da_Annl_Amt;
    private DbsField adsa600_Adi_Dtl_Cref_Annl_Amt;
    private DbsField adsa600_Adi_Tiaa_Sttlmnt;
    private DbsField adsa600_Adi_Tiaa_Re_Sttlmnt;
    private DbsField adsa600_Adi_Cref_Sttlmnt;
    private DbsField adsa600_Adi_Orig_Lob_Ind;
    private DbsField adsa600_Adi_Roth_Rqst_Ind;
    private DbsField adsa600_Pnd_Adi_Contract_Id;
    private DbsField adsa600_Adc_Ppg_Cde;
    private DbsField adsa600_Adc_Trk_Ro_Ivc;
    private DbsField adsa600_Adc_Acct_Cde;
    private DbsField adsa600_Pnd_Return_Cde;
    private DbsField adsa600_Pnd_Inst_Nbr;
    private DbsField adsa600_Pnd_This_Inst;
    private DbsField adsa600_Pnd_Ia_Check_Dte;
    private DbsGroup adsa600_Pnd_Ia_Check_DteRedef1;
    private DbsField adsa600_Pnd_Filler;
    private DbsField adsa600_Pnd_Ia_Mm;
    private DbsField adsa600_Pnd_Tiaa_Ivc_Used_Amt;
    private DbsField adsa600_Pnd_Cref_Ivc_Used_Amt;
    private DbsField adsa600_Adc_Sub_Plan_Nbr;
    private DbsField adsa600_Adc_T395_Fund_Id;
    private DbsField adsa600_Adc_Original_Cntrct_Nbr;
    private DbsField adsa600_Adc_Original_Sub_Plan_Nbr;
    private DbsField adsa600_Adc_Contract_Id;
    private DbsField adsa600_Adc_Invstmnt_Grpng_Id;

    public DbsGroup getAdsa600() { return adsa600; }

    public DbsField getAdsa600_Adp_Wpid() { return adsa600_Adp_Wpid; }

    public DbsField getAdsa600_Adp_Mit_Log_Dte_Tme() { return adsa600_Adp_Mit_Log_Dte_Tme; }

    public DbsField getAdsa600_Adp_Ia_Tiaa_Nbr() { return adsa600_Adp_Ia_Tiaa_Nbr; }

    public DbsField getAdsa600_Adp_Ia_Cref_Nbr() { return adsa600_Adp_Ia_Cref_Nbr; }

    public DbsField getAdsa600_Adp_Ia_Tiaa_Rea_Nbr() { return adsa600_Adp_Ia_Tiaa_Rea_Nbr; }

    public DbsField getAdsa600_Adp_Unique_Id() { return adsa600_Adp_Unique_Id; }

    public DbsField getAdsa600_Adp_Frst_Annt_Ctznshp() { return adsa600_Adp_Frst_Annt_Ctznshp; }

    public DbsField getAdsa600_Adp_Frst_Annt_Rsdnc_Cde() { return adsa600_Adp_Frst_Annt_Rsdnc_Cde; }

    public DbsField getAdsa600_Adp_Orgnl_Issue_State() { return adsa600_Adp_Orgnl_Issue_State; }

    public DbsField getAdsa600_Adp_Frst_Annt_Sex_Cde() { return adsa600_Adp_Frst_Annt_Sex_Cde; }

    public DbsField getAdsa600_Adp_Frst_Annt_Frst_Nme() { return adsa600_Adp_Frst_Annt_Frst_Nme; }

    public DbsField getAdsa600_Adp_Frst_Annt_Mid_Nme() { return adsa600_Adp_Frst_Annt_Mid_Nme; }

    public DbsField getAdsa600_Adp_Frst_Annt_Lst_Nme() { return adsa600_Adp_Frst_Annt_Lst_Nme; }

    public DbsField getAdsa600_Adp_Frst_Annt_Dte_Of_Brth() { return adsa600_Adp_Frst_Annt_Dte_Of_Brth; }

    public DbsField getAdsa600_Adp_Frst_Annt_Ssn() { return adsa600_Adp_Frst_Annt_Ssn; }

    public DbsField getAdsa600_Adp_Scnd_Annt_Frst_Nme() { return adsa600_Adp_Scnd_Annt_Frst_Nme; }

    public DbsField getAdsa600_Adp_Scnd_Annt_Mid_Nme() { return adsa600_Adp_Scnd_Annt_Mid_Nme; }

    public DbsField getAdsa600_Adp_Scnd_Annt_Lst_Nme() { return adsa600_Adp_Scnd_Annt_Lst_Nme; }

    public DbsField getAdsa600_Adp_Scnd_Annt_Dte_Of_Brth() { return adsa600_Adp_Scnd_Annt_Dte_Of_Brth; }

    public DbsField getAdsa600_Adp_Scnd_Annt_Ssn() { return adsa600_Adp_Scnd_Annt_Ssn; }

    public DbsField getAdsa600_Adp_Scnd_Annt_Sex_Cde() { return adsa600_Adp_Scnd_Annt_Sex_Cde; }

    public DbsField getAdsa600_Adp_Alt_Payee_Ind() { return adsa600_Adp_Alt_Payee_Ind; }

    public DbsField getAdsa600_Adp_Alt_Dest_Addr_Ind() { return adsa600_Adp_Alt_Dest_Addr_Ind; }

    public DbsField getAdsa600_Adp_Rsttlmnt_Cnt() { return adsa600_Adp_Rsttlmnt_Cnt; }

    public DbsField getAdsa600_Adp_State_Of_Issue() { return adsa600_Adp_State_Of_Issue; }

    public DbsField getAdsa600_Adp_Alt_Dest_Acct_Nbr() { return adsa600_Adp_Alt_Dest_Acct_Nbr; }

    public DbsField getAdsa600_Adp_Alt_Dest_Hold_Cde() { return adsa600_Adp_Alt_Dest_Hold_Cde; }

    public DbsField getAdsa600_Adp_Alt_Dest_Rlvr_Dest() { return adsa600_Adp_Alt_Dest_Rlvr_Dest; }

    public DbsField getAdsa600_Adp_Ivc_Rlvr_Cde() { return adsa600_Adp_Ivc_Rlvr_Cde; }

    public DbsField getAdsa600_Adp_Ivc_Pymnt_Rule() { return adsa600_Adp_Ivc_Pymnt_Rule; }

    public DbsField getAdsa600_Adp_Hold_Cde() { return adsa600_Adp_Hold_Cde; }

    public DbsField getAdsa600_Adp_Plan_Nbr() { return adsa600_Adp_Plan_Nbr; }

    public DbsField getAdsa600_Adp_Frm_Irc_Cde() { return adsa600_Adp_Frm_Irc_Cde; }

    public DbsField getAdsa600_Adp_To_Irc_Cde() { return adsa600_Adp_To_Irc_Cde; }

    public DbsField getAdsa600_Adp_Irc_Cde() { return adsa600_Adp_Irc_Cde; }

    public DbsField getAdsa600_Adp_Roth_Plan_Type() { return adsa600_Adp_Roth_Plan_Type; }

    public DbsField getAdsa600_Adp_Roth_Frst_Cntrbtn_Dte() { return adsa600_Adp_Roth_Frst_Cntrbtn_Dte; }

    public DbsField getAdsa600_Adp_Roth_Dsblty_Dte() { return adsa600_Adp_Roth_Dsblty_Dte; }

    public DbsField getAdsa600_Adp_Roth_Rqst_Ind() { return adsa600_Adp_Roth_Rqst_Ind; }

    public DbsField getAdsa600_Adp_Srvvr_Ind() { return adsa600_Adp_Srvvr_Ind; }

    public DbsField getAdsa600_Adp_Srvvr_Rltnshp() { return adsa600_Adp_Srvvr_Rltnshp; }

    public DbsField getAdsa600_Adp_Mndtry_Tax_Ovrd() { return adsa600_Adp_Mndtry_Tax_Ovrd; }

    public DbsField getAdsa600_Adp_Bene_Client_Ind() { return adsa600_Adp_Bene_Client_Ind; }

    public DbsField getAdsa600_Adi_Ia_Rcrd_Cde() { return adsa600_Adi_Ia_Rcrd_Cde; }

    public DbsField getAdsa600_Adi_Sqnce_Nbr() { return adsa600_Adi_Sqnce_Nbr; }

    public DbsField getAdsa600_Adi_Annty_Strt_Dte() { return adsa600_Adi_Annty_Strt_Dte; }

    public DbsField getAdsa600_Adi_Instllmnt_Dte() { return adsa600_Adi_Instllmnt_Dte; }

    public DbsField getAdsa600_Adi_Frst_Pymnt_Due_Dte() { return adsa600_Adi_Frst_Pymnt_Due_Dte; }

    public DbsField getAdsa600_Adi_Tiaa_Ivc_Amt() { return adsa600_Adi_Tiaa_Ivc_Amt; }

    public DbsField getAdsa600_Adi_Tiaa_Ivc_Gd_Ind() { return adsa600_Adi_Tiaa_Ivc_Gd_Ind; }

    public DbsField getAdsa600_Adi_Tiaa_Prdc_Ivc_Amt() { return adsa600_Adi_Tiaa_Prdc_Ivc_Amt; }

    public DbsField getAdsa600_Adi_Cref_Ivc_Amt() { return adsa600_Adi_Cref_Ivc_Amt; }

    public DbsField getAdsa600_Adi_Cref_Ivc_Gd_Ind() { return adsa600_Adi_Cref_Ivc_Gd_Ind; }

    public DbsField getAdsa600_Adi_Cref_Prdc_Ivc_Amt() { return adsa600_Adi_Cref_Prdc_Ivc_Amt; }

    public DbsField getAdsa600_Adi_Frst_Ck_Pd_Dte() { return adsa600_Adi_Frst_Ck_Pd_Dte; }

    public DbsField getAdsa600_Adi_Finl_Periodic_Py_Dte() { return adsa600_Adi_Finl_Periodic_Py_Dte; }

    public DbsField getAdsa600_Adi_Finl_Prtl_Py_Dte() { return adsa600_Adi_Finl_Prtl_Py_Dte; }

    public DbsField getAdsa600_Adi_Optn_Cde() { return adsa600_Adi_Optn_Cde; }

    public DbsField getAdsa600_Adi_Pymnt_Cde() { return adsa600_Adi_Pymnt_Cde; }

    public DbsField getAdsa600_Adi_Pymnt_Mode() { return adsa600_Adi_Pymnt_Mode; }

    public DbsField getAdsa600_Adi_Tiaa_Nbrs() { return adsa600_Adi_Tiaa_Nbrs; }

    public DbsField getAdsa600_Adi_Cref_Nbrs() { return adsa600_Adi_Cref_Nbrs; }

    public DbsField getAdsa600_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt() { return adsa600_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt; }

    public DbsField getAdsa600_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt() { return adsa600_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt; }

    public DbsField getAdsa600_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt() { return adsa600_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt; }

    public DbsField getAdsa600_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt() { return adsa600_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt; }

    public DbsField getAdsa600_Pnd_Adi_Dtl_Tiaa_Acct_Cd() { return adsa600_Pnd_Adi_Dtl_Tiaa_Acct_Cd; }

    public DbsField getAdsa600_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd() { return adsa600_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd; }

    public DbsField getAdsa600_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt() { return adsa600_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt; }

    public DbsField getAdsa600_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt() { return adsa600_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt; }

    public DbsField getAdsa600_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt() { return adsa600_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt; }

    public DbsField getAdsa600_Pnd_Old_Tiaa_Grd_Grntd_Amt() { return adsa600_Pnd_Old_Tiaa_Grd_Grntd_Amt; }

    public DbsField getAdsa600_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt() { return adsa600_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt; }

    public DbsField getAdsa600_Pnd_Old_Tiaa_Grd_Dvdnd_Amt() { return adsa600_Pnd_Old_Tiaa_Grd_Dvdnd_Amt; }

    public DbsField getAdsa600_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt() { return adsa600_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt; }

    public DbsField getAdsa600_Pnd_Old_Tiaa_Std_Grntd_Amt() { return adsa600_Pnd_Old_Tiaa_Std_Grntd_Amt; }

    public DbsField getAdsa600_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt() { return adsa600_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt; }

    public DbsField getAdsa600_Pnd_Old_Tiaa_Std_Dvdnd_Amt() { return adsa600_Pnd_Old_Tiaa_Std_Dvdnd_Amt; }

    public DbsField getAdsa600_Adi_Dtl_Cref_Da_Rate_Cd() { return adsa600_Adi_Dtl_Cref_Da_Rate_Cd; }

    public DbsField getAdsa600_Adi_Dtl_Cref_Acct_Cd() { return adsa600_Adi_Dtl_Cref_Acct_Cd; }

    public DbsField getAdsa600_Adi_Dtl_Cref_Ia_Rate_Cd() { return adsa600_Adi_Dtl_Cref_Ia_Rate_Cd; }

    public DbsField getAdsa600_Adi_Dtl_Cref_Mnthly_Nbr_Units() { return adsa600_Adi_Dtl_Cref_Mnthly_Nbr_Units; }

    public DbsField getAdsa600_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val() { return adsa600_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val; }

    public DbsField getAdsa600_Adi_Dtl_Cref_Da_Mnthly_Amt() { return adsa600_Adi_Dtl_Cref_Da_Mnthly_Amt; }

    public DbsField getAdsa600_Adi_Dtl_Cref_Mnthly_Amt() { return adsa600_Adi_Dtl_Cref_Mnthly_Amt; }

    public DbsField getAdsa600_Adi_Dtl_Cref_Annl_Nbr_Units() { return adsa600_Adi_Dtl_Cref_Annl_Nbr_Units; }

    public DbsField getAdsa600_Adi_Dtl_Cref_Ia_Annl_Unit_Val() { return adsa600_Adi_Dtl_Cref_Ia_Annl_Unit_Val; }

    public DbsField getAdsa600_Adi_Dtl_Cref_Da_Annl_Amt() { return adsa600_Adi_Dtl_Cref_Da_Annl_Amt; }

    public DbsField getAdsa600_Adi_Dtl_Cref_Annl_Amt() { return adsa600_Adi_Dtl_Cref_Annl_Amt; }

    public DbsField getAdsa600_Adi_Tiaa_Sttlmnt() { return adsa600_Adi_Tiaa_Sttlmnt; }

    public DbsField getAdsa600_Adi_Tiaa_Re_Sttlmnt() { return adsa600_Adi_Tiaa_Re_Sttlmnt; }

    public DbsField getAdsa600_Adi_Cref_Sttlmnt() { return adsa600_Adi_Cref_Sttlmnt; }

    public DbsField getAdsa600_Adi_Orig_Lob_Ind() { return adsa600_Adi_Orig_Lob_Ind; }

    public DbsField getAdsa600_Adi_Roth_Rqst_Ind() { return adsa600_Adi_Roth_Rqst_Ind; }

    public DbsField getAdsa600_Pnd_Adi_Contract_Id() { return adsa600_Pnd_Adi_Contract_Id; }

    public DbsField getAdsa600_Adc_Ppg_Cde() { return adsa600_Adc_Ppg_Cde; }

    public DbsField getAdsa600_Adc_Trk_Ro_Ivc() { return adsa600_Adc_Trk_Ro_Ivc; }

    public DbsField getAdsa600_Adc_Acct_Cde() { return adsa600_Adc_Acct_Cde; }

    public DbsField getAdsa600_Pnd_Return_Cde() { return adsa600_Pnd_Return_Cde; }

    public DbsField getAdsa600_Pnd_Inst_Nbr() { return adsa600_Pnd_Inst_Nbr; }

    public DbsField getAdsa600_Pnd_This_Inst() { return adsa600_Pnd_This_Inst; }

    public DbsField getAdsa600_Pnd_Ia_Check_Dte() { return adsa600_Pnd_Ia_Check_Dte; }

    public DbsGroup getAdsa600_Pnd_Ia_Check_DteRedef1() { return adsa600_Pnd_Ia_Check_DteRedef1; }

    public DbsField getAdsa600_Pnd_Filler() { return adsa600_Pnd_Filler; }

    public DbsField getAdsa600_Pnd_Ia_Mm() { return adsa600_Pnd_Ia_Mm; }

    public DbsField getAdsa600_Pnd_Tiaa_Ivc_Used_Amt() { return adsa600_Pnd_Tiaa_Ivc_Used_Amt; }

    public DbsField getAdsa600_Pnd_Cref_Ivc_Used_Amt() { return adsa600_Pnd_Cref_Ivc_Used_Amt; }

    public DbsField getAdsa600_Adc_Sub_Plan_Nbr() { return adsa600_Adc_Sub_Plan_Nbr; }

    public DbsField getAdsa600_Adc_T395_Fund_Id() { return adsa600_Adc_T395_Fund_Id; }

    public DbsField getAdsa600_Adc_Original_Cntrct_Nbr() { return adsa600_Adc_Original_Cntrct_Nbr; }

    public DbsField getAdsa600_Adc_Original_Sub_Plan_Nbr() { return adsa600_Adc_Original_Sub_Plan_Nbr; }

    public DbsField getAdsa600_Adc_Contract_Id() { return adsa600_Adc_Contract_Id; }

    public DbsField getAdsa600_Adc_Invstmnt_Grpng_Id() { return adsa600_Adc_Invstmnt_Grpng_Id; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        adsa600 = dbsRecord.newGroupInRecord("adsa600", "ADSA600");
        adsa600.setParameterOption(ParameterOption.ByReference);
        adsa600_Adp_Wpid = adsa600.newFieldInGroup("adsa600_Adp_Wpid", "ADP-WPID", FieldType.STRING, 6);
        adsa600_Adp_Mit_Log_Dte_Tme = adsa600.newFieldInGroup("adsa600_Adp_Mit_Log_Dte_Tme", "ADP-MIT-LOG-DTE-TME", FieldType.STRING, 15);
        adsa600_Adp_Ia_Tiaa_Nbr = adsa600.newFieldInGroup("adsa600_Adp_Ia_Tiaa_Nbr", "ADP-IA-TIAA-NBR", FieldType.STRING, 10);
        adsa600_Adp_Ia_Cref_Nbr = adsa600.newFieldInGroup("adsa600_Adp_Ia_Cref_Nbr", "ADP-IA-CREF-NBR", FieldType.STRING, 10);
        adsa600_Adp_Ia_Tiaa_Rea_Nbr = adsa600.newFieldInGroup("adsa600_Adp_Ia_Tiaa_Rea_Nbr", "ADP-IA-TIAA-REA-NBR", FieldType.STRING, 10);
        adsa600_Adp_Unique_Id = adsa600.newFieldInGroup("adsa600_Adp_Unique_Id", "ADP-UNIQUE-ID", FieldType.NUMERIC, 12);
        adsa600_Adp_Frst_Annt_Ctznshp = adsa600.newFieldInGroup("adsa600_Adp_Frst_Annt_Ctznshp", "ADP-FRST-ANNT-CTZNSHP", FieldType.STRING, 2);
        adsa600_Adp_Frst_Annt_Rsdnc_Cde = adsa600.newFieldInGroup("adsa600_Adp_Frst_Annt_Rsdnc_Cde", "ADP-FRST-ANNT-RSDNC-CDE", FieldType.STRING, 2);
        adsa600_Adp_Orgnl_Issue_State = adsa600.newFieldInGroup("adsa600_Adp_Orgnl_Issue_State", "ADP-ORGNL-ISSUE-STATE", FieldType.STRING, 2);
        adsa600_Adp_Frst_Annt_Sex_Cde = adsa600.newFieldInGroup("adsa600_Adp_Frst_Annt_Sex_Cde", "ADP-FRST-ANNT-SEX-CDE", FieldType.STRING, 1);
        adsa600_Adp_Frst_Annt_Frst_Nme = adsa600.newFieldInGroup("adsa600_Adp_Frst_Annt_Frst_Nme", "ADP-FRST-ANNT-FRST-NME", FieldType.STRING, 30);
        adsa600_Adp_Frst_Annt_Mid_Nme = adsa600.newFieldInGroup("adsa600_Adp_Frst_Annt_Mid_Nme", "ADP-FRST-ANNT-MID-NME", FieldType.STRING, 30);
        adsa600_Adp_Frst_Annt_Lst_Nme = adsa600.newFieldInGroup("adsa600_Adp_Frst_Annt_Lst_Nme", "ADP-FRST-ANNT-LST-NME", FieldType.STRING, 30);
        adsa600_Adp_Frst_Annt_Dte_Of_Brth = adsa600.newFieldInGroup("adsa600_Adp_Frst_Annt_Dte_Of_Brth", "ADP-FRST-ANNT-DTE-OF-BRTH", FieldType.DATE);
        adsa600_Adp_Frst_Annt_Ssn = adsa600.newFieldInGroup("adsa600_Adp_Frst_Annt_Ssn", "ADP-FRST-ANNT-SSN", FieldType.NUMERIC, 9);
        adsa600_Adp_Scnd_Annt_Frst_Nme = adsa600.newFieldInGroup("adsa600_Adp_Scnd_Annt_Frst_Nme", "ADP-SCND-ANNT-FRST-NME", FieldType.STRING, 30);
        adsa600_Adp_Scnd_Annt_Mid_Nme = adsa600.newFieldInGroup("adsa600_Adp_Scnd_Annt_Mid_Nme", "ADP-SCND-ANNT-MID-NME", FieldType.STRING, 30);
        adsa600_Adp_Scnd_Annt_Lst_Nme = adsa600.newFieldInGroup("adsa600_Adp_Scnd_Annt_Lst_Nme", "ADP-SCND-ANNT-LST-NME", FieldType.STRING, 30);
        adsa600_Adp_Scnd_Annt_Dte_Of_Brth = adsa600.newFieldInGroup("adsa600_Adp_Scnd_Annt_Dte_Of_Brth", "ADP-SCND-ANNT-DTE-OF-BRTH", FieldType.DATE);
        adsa600_Adp_Scnd_Annt_Ssn = adsa600.newFieldInGroup("adsa600_Adp_Scnd_Annt_Ssn", "ADP-SCND-ANNT-SSN", FieldType.NUMERIC, 9);
        adsa600_Adp_Scnd_Annt_Sex_Cde = adsa600.newFieldInGroup("adsa600_Adp_Scnd_Annt_Sex_Cde", "ADP-SCND-ANNT-SEX-CDE", FieldType.STRING, 1);
        adsa600_Adp_Alt_Payee_Ind = adsa600.newFieldInGroup("adsa600_Adp_Alt_Payee_Ind", "ADP-ALT-PAYEE-IND", FieldType.STRING, 1);
        adsa600_Adp_Alt_Dest_Addr_Ind = adsa600.newFieldInGroup("adsa600_Adp_Alt_Dest_Addr_Ind", "ADP-ALT-DEST-ADDR-IND", FieldType.STRING, 1);
        adsa600_Adp_Rsttlmnt_Cnt = adsa600.newFieldInGroup("adsa600_Adp_Rsttlmnt_Cnt", "ADP-RSTTLMNT-CNT", FieldType.STRING, 1);
        adsa600_Adp_State_Of_Issue = adsa600.newFieldInGroup("adsa600_Adp_State_Of_Issue", "ADP-STATE-OF-ISSUE", FieldType.STRING, 2);
        adsa600_Adp_Alt_Dest_Acct_Nbr = adsa600.newFieldInGroup("adsa600_Adp_Alt_Dest_Acct_Nbr", "ADP-ALT-DEST-ACCT-NBR", FieldType.STRING, 8);
        adsa600_Adp_Alt_Dest_Hold_Cde = adsa600.newFieldInGroup("adsa600_Adp_Alt_Dest_Hold_Cde", "ADP-ALT-DEST-HOLD-CDE", FieldType.STRING, 4);
        adsa600_Adp_Alt_Dest_Rlvr_Dest = adsa600.newFieldInGroup("adsa600_Adp_Alt_Dest_Rlvr_Dest", "ADP-ALT-DEST-RLVR-DEST", FieldType.STRING, 4);
        adsa600_Adp_Ivc_Rlvr_Cde = adsa600.newFieldInGroup("adsa600_Adp_Ivc_Rlvr_Cde", "ADP-IVC-RLVR-CDE", FieldType.STRING, 1);
        adsa600_Adp_Ivc_Pymnt_Rule = adsa600.newFieldInGroup("adsa600_Adp_Ivc_Pymnt_Rule", "ADP-IVC-PYMNT-RULE", FieldType.STRING, 1);
        adsa600_Adp_Hold_Cde = adsa600.newFieldInGroup("adsa600_Adp_Hold_Cde", "ADP-HOLD-CDE", FieldType.STRING, 8);
        adsa600_Adp_Plan_Nbr = adsa600.newFieldInGroup("adsa600_Adp_Plan_Nbr", "ADP-PLAN-NBR", FieldType.STRING, 6);
        adsa600_Adp_Frm_Irc_Cde = adsa600.newFieldArrayInGroup("adsa600_Adp_Frm_Irc_Cde", "ADP-FRM-IRC-CDE", FieldType.STRING, 2, new DbsArrayController(1,
            4));
        adsa600_Adp_To_Irc_Cde = adsa600.newFieldInGroup("adsa600_Adp_To_Irc_Cde", "ADP-TO-IRC-CDE", FieldType.STRING, 2);
        adsa600_Adp_Irc_Cde = adsa600.newFieldInGroup("adsa600_Adp_Irc_Cde", "ADP-IRC-CDE", FieldType.STRING, 2);
        adsa600_Adp_Roth_Plan_Type = adsa600.newFieldInGroup("adsa600_Adp_Roth_Plan_Type", "ADP-ROTH-PLAN-TYPE", FieldType.STRING, 1);
        adsa600_Adp_Roth_Frst_Cntrbtn_Dte = adsa600.newFieldInGroup("adsa600_Adp_Roth_Frst_Cntrbtn_Dte", "ADP-ROTH-FRST-CNTRBTN-DTE", FieldType.DATE);
        adsa600_Adp_Roth_Dsblty_Dte = adsa600.newFieldInGroup("adsa600_Adp_Roth_Dsblty_Dte", "ADP-ROTH-DSBLTY-DTE", FieldType.DATE);
        adsa600_Adp_Roth_Rqst_Ind = adsa600.newFieldInGroup("adsa600_Adp_Roth_Rqst_Ind", "ADP-ROTH-RQST-IND", FieldType.STRING, 1);
        adsa600_Adp_Srvvr_Ind = adsa600.newFieldInGroup("adsa600_Adp_Srvvr_Ind", "ADP-SRVVR-IND", FieldType.STRING, 1);
        adsa600_Adp_Srvvr_Rltnshp = adsa600.newFieldInGroup("adsa600_Adp_Srvvr_Rltnshp", "ADP-SRVVR-RLTNSHP", FieldType.STRING, 1);
        adsa600_Adp_Mndtry_Tax_Ovrd = adsa600.newFieldInGroup("adsa600_Adp_Mndtry_Tax_Ovrd", "ADP-MNDTRY-TAX-OVRD", FieldType.STRING, 1);
        adsa600_Adp_Bene_Client_Ind = adsa600.newFieldInGroup("adsa600_Adp_Bene_Client_Ind", "ADP-BENE-CLIENT-IND", FieldType.STRING, 1);
        adsa600_Adi_Ia_Rcrd_Cde = adsa600.newFieldInGroup("adsa600_Adi_Ia_Rcrd_Cde", "ADI-IA-RCRD-CDE", FieldType.STRING, 3);
        adsa600_Adi_Sqnce_Nbr = adsa600.newFieldInGroup("adsa600_Adi_Sqnce_Nbr", "ADI-SQNCE-NBR", FieldType.NUMERIC, 3);
        adsa600_Adi_Annty_Strt_Dte = adsa600.newFieldInGroup("adsa600_Adi_Annty_Strt_Dte", "ADI-ANNTY-STRT-DTE", FieldType.DATE);
        adsa600_Adi_Instllmnt_Dte = adsa600.newFieldInGroup("adsa600_Adi_Instllmnt_Dte", "ADI-INSTLLMNT-DTE", FieldType.DATE);
        adsa600_Adi_Frst_Pymnt_Due_Dte = adsa600.newFieldInGroup("adsa600_Adi_Frst_Pymnt_Due_Dte", "ADI-FRST-PYMNT-DUE-DTE", FieldType.DATE);
        adsa600_Adi_Tiaa_Ivc_Amt = adsa600.newFieldInGroup("adsa600_Adi_Tiaa_Ivc_Amt", "ADI-TIAA-IVC-AMT", FieldType.DECIMAL, 9,2);
        adsa600_Adi_Tiaa_Ivc_Gd_Ind = adsa600.newFieldInGroup("adsa600_Adi_Tiaa_Ivc_Gd_Ind", "ADI-TIAA-IVC-GD-IND", FieldType.STRING, 1);
        adsa600_Adi_Tiaa_Prdc_Ivc_Amt = adsa600.newFieldInGroup("adsa600_Adi_Tiaa_Prdc_Ivc_Amt", "ADI-TIAA-PRDC-IVC-AMT", FieldType.DECIMAL, 9,2);
        adsa600_Adi_Cref_Ivc_Amt = adsa600.newFieldInGroup("adsa600_Adi_Cref_Ivc_Amt", "ADI-CREF-IVC-AMT", FieldType.DECIMAL, 9,2);
        adsa600_Adi_Cref_Ivc_Gd_Ind = adsa600.newFieldInGroup("adsa600_Adi_Cref_Ivc_Gd_Ind", "ADI-CREF-IVC-GD-IND", FieldType.STRING, 1);
        adsa600_Adi_Cref_Prdc_Ivc_Amt = adsa600.newFieldInGroup("adsa600_Adi_Cref_Prdc_Ivc_Amt", "ADI-CREF-PRDC-IVC-AMT", FieldType.DECIMAL, 9,2);
        adsa600_Adi_Frst_Ck_Pd_Dte = adsa600.newFieldInGroup("adsa600_Adi_Frst_Ck_Pd_Dte", "ADI-FRST-CK-PD-DTE", FieldType.DATE);
        adsa600_Adi_Finl_Periodic_Py_Dte = adsa600.newFieldInGroup("adsa600_Adi_Finl_Periodic_Py_Dte", "ADI-FINL-PERIODIC-PY-DTE", FieldType.DATE);
        adsa600_Adi_Finl_Prtl_Py_Dte = adsa600.newFieldInGroup("adsa600_Adi_Finl_Prtl_Py_Dte", "ADI-FINL-PRTL-PY-DTE", FieldType.DATE);
        adsa600_Adi_Optn_Cde = adsa600.newFieldInGroup("adsa600_Adi_Optn_Cde", "ADI-OPTN-CDE", FieldType.NUMERIC, 2);
        adsa600_Adi_Pymnt_Cde = adsa600.newFieldInGroup("adsa600_Adi_Pymnt_Cde", "ADI-PYMNT-CDE", FieldType.STRING, 1);
        adsa600_Adi_Pymnt_Mode = adsa600.newFieldInGroup("adsa600_Adi_Pymnt_Mode", "ADI-PYMNT-MODE", FieldType.NUMERIC, 3);
        adsa600_Adi_Tiaa_Nbrs = adsa600.newFieldArrayInGroup("adsa600_Adi_Tiaa_Nbrs", "ADI-TIAA-NBRS", FieldType.STRING, 10, new DbsArrayController(1,
            12));
        adsa600_Adi_Cref_Nbrs = adsa600.newFieldArrayInGroup("adsa600_Adi_Cref_Nbrs", "ADI-CREF-NBRS", FieldType.STRING, 10, new DbsArrayController(1,
            12));
        adsa600_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt = adsa600.newFieldArrayInGroup("adsa600_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt", "#ADI-DTL-FNL-STD-PAY-AMT", FieldType.DECIMAL, 
            9,2, new DbsArrayController(1,250));
        adsa600_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt = adsa600.newFieldArrayInGroup("adsa600_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt", "#ADI-DTL-FNL-STD-DVDND-AMT", FieldType.DECIMAL, 
            9,2, new DbsArrayController(1,250));
        adsa600_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt = adsa600.newFieldArrayInGroup("adsa600_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt", "#ADI-DTL-FNL-GRD-PAY-AMT", FieldType.DECIMAL, 
            9,2, new DbsArrayController(1,250));
        adsa600_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt = adsa600.newFieldArrayInGroup("adsa600_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt", "#ADI-DTL-FNL-GRD-DVDND-AMT", FieldType.DECIMAL, 
            9,2, new DbsArrayController(1,250));
        adsa600_Pnd_Adi_Dtl_Tiaa_Acct_Cd = adsa600.newFieldArrayInGroup("adsa600_Pnd_Adi_Dtl_Tiaa_Acct_Cd", "#ADI-DTL-TIAA-ACCT-CD", FieldType.STRING, 
            1, new DbsArrayController(1,250));
        adsa600_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd = adsa600.newFieldArrayInGroup("adsa600_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd", "#ADI-DTL-TIAA-IA-RATE-CD", FieldType.STRING, 
            2, new DbsArrayController(1,250));
        adsa600_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt = adsa600.newFieldArrayInGroup("adsa600_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt", "#ADI-DTL-TIAA-DA-STD-AMT", FieldType.DECIMAL, 
            9,2, new DbsArrayController(1,250));
        adsa600_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt = adsa600.newFieldArrayInGroup("adsa600_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt", "#ADI-DTL-TIAA-DA-GRD-AMT", FieldType.DECIMAL, 
            9,2, new DbsArrayController(1,250));
        adsa600_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt = adsa600.newFieldArrayInGroup("adsa600_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt", "#ADI-DTL-TIAA-GRD-GRNTD-AMT", 
            FieldType.DECIMAL, 9,2, new DbsArrayController(1,250));
        adsa600_Pnd_Old_Tiaa_Grd_Grntd_Amt = adsa600.newFieldInGroup("adsa600_Pnd_Old_Tiaa_Grd_Grntd_Amt", "#OLD-TIAA-GRD-GRNTD-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        adsa600_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt = adsa600.newFieldArrayInGroup("adsa600_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt", "#ADI-DTL-TIAA-GRD-DVDND-AMT", 
            FieldType.DECIMAL, 9,2, new DbsArrayController(1,250));
        adsa600_Pnd_Old_Tiaa_Grd_Dvdnd_Amt = adsa600.newFieldInGroup("adsa600_Pnd_Old_Tiaa_Grd_Dvdnd_Amt", "#OLD-TIAA-GRD-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        adsa600_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt = adsa600.newFieldArrayInGroup("adsa600_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt", "#ADI-DTL-TIAA-STD-GRNTD-AMT", 
            FieldType.DECIMAL, 9,2, new DbsArrayController(1,250));
        adsa600_Pnd_Old_Tiaa_Std_Grntd_Amt = adsa600.newFieldInGroup("adsa600_Pnd_Old_Tiaa_Std_Grntd_Amt", "#OLD-TIAA-STD-GRNTD-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        adsa600_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt = adsa600.newFieldArrayInGroup("adsa600_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt", "#ADI-DTL-TIAA-STD-DVDND-AMT", 
            FieldType.DECIMAL, 9,2, new DbsArrayController(1,250));
        adsa600_Pnd_Old_Tiaa_Std_Dvdnd_Amt = adsa600.newFieldInGroup("adsa600_Pnd_Old_Tiaa_Std_Dvdnd_Amt", "#OLD-TIAA-STD-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            9,2);
        adsa600_Adi_Dtl_Cref_Da_Rate_Cd = adsa600.newFieldArrayInGroup("adsa600_Adi_Dtl_Cref_Da_Rate_Cd", "ADI-DTL-CREF-DA-RATE-CD", FieldType.STRING, 
            2, new DbsArrayController(1,20));
        adsa600_Adi_Dtl_Cref_Acct_Cd = adsa600.newFieldArrayInGroup("adsa600_Adi_Dtl_Cref_Acct_Cd", "ADI-DTL-CREF-ACCT-CD", FieldType.STRING, 1, new DbsArrayController(1,
            20));
        adsa600_Adi_Dtl_Cref_Ia_Rate_Cd = adsa600.newFieldArrayInGroup("adsa600_Adi_Dtl_Cref_Ia_Rate_Cd", "ADI-DTL-CREF-IA-RATE-CD", FieldType.STRING, 
            2, new DbsArrayController(1,20));
        adsa600_Adi_Dtl_Cref_Mnthly_Nbr_Units = adsa600.newFieldArrayInGroup("adsa600_Adi_Dtl_Cref_Mnthly_Nbr_Units", "ADI-DTL-CREF-MNTHLY-NBR-UNITS", 
            FieldType.DECIMAL, 10,4, new DbsArrayController(1,20));
        adsa600_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val = adsa600.newFieldArrayInGroup("adsa600_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val", "ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL", 
            FieldType.DECIMAL, 7,4, new DbsArrayController(1,20));
        adsa600_Adi_Dtl_Cref_Da_Mnthly_Amt = adsa600.newFieldArrayInGroup("adsa600_Adi_Dtl_Cref_Da_Mnthly_Amt", "ADI-DTL-CREF-DA-MNTHLY-AMT", FieldType.DECIMAL, 
            9,2, new DbsArrayController(1,20));
        adsa600_Adi_Dtl_Cref_Mnthly_Amt = adsa600.newFieldArrayInGroup("adsa600_Adi_Dtl_Cref_Mnthly_Amt", "ADI-DTL-CREF-MNTHLY-AMT", FieldType.DECIMAL, 
            11,2, new DbsArrayController(1,20));
        adsa600_Adi_Dtl_Cref_Annl_Nbr_Units = adsa600.newFieldArrayInGroup("adsa600_Adi_Dtl_Cref_Annl_Nbr_Units", "ADI-DTL-CREF-ANNL-NBR-UNITS", FieldType.DECIMAL, 
            10,4, new DbsArrayController(1,20));
        adsa600_Adi_Dtl_Cref_Ia_Annl_Unit_Val = adsa600.newFieldArrayInGroup("adsa600_Adi_Dtl_Cref_Ia_Annl_Unit_Val", "ADI-DTL-CREF-IA-ANNL-UNIT-VAL", 
            FieldType.DECIMAL, 7,4, new DbsArrayController(1,20));
        adsa600_Adi_Dtl_Cref_Da_Annl_Amt = adsa600.newFieldArrayInGroup("adsa600_Adi_Dtl_Cref_Da_Annl_Amt", "ADI-DTL-CREF-DA-ANNL-AMT", FieldType.DECIMAL, 
            9,2, new DbsArrayController(1,20));
        adsa600_Adi_Dtl_Cref_Annl_Amt = adsa600.newFieldArrayInGroup("adsa600_Adi_Dtl_Cref_Annl_Amt", "ADI-DTL-CREF-ANNL-AMT", FieldType.DECIMAL, 11,2, 
            new DbsArrayController(1,20));
        adsa600_Adi_Tiaa_Sttlmnt = adsa600.newFieldInGroup("adsa600_Adi_Tiaa_Sttlmnt", "ADI-TIAA-STTLMNT", FieldType.STRING, 1);
        adsa600_Adi_Tiaa_Re_Sttlmnt = adsa600.newFieldInGroup("adsa600_Adi_Tiaa_Re_Sttlmnt", "ADI-TIAA-RE-STTLMNT", FieldType.STRING, 1);
        adsa600_Adi_Cref_Sttlmnt = adsa600.newFieldInGroup("adsa600_Adi_Cref_Sttlmnt", "ADI-CREF-STTLMNT", FieldType.STRING, 1);
        adsa600_Adi_Orig_Lob_Ind = adsa600.newFieldArrayInGroup("adsa600_Adi_Orig_Lob_Ind", "ADI-ORIG-LOB-IND", FieldType.STRING, 1, new DbsArrayController(1,
            12));
        adsa600_Adi_Roth_Rqst_Ind = adsa600.newFieldInGroup("adsa600_Adi_Roth_Rqst_Ind", "ADI-ROTH-RQST-IND", FieldType.STRING, 1);
        adsa600_Pnd_Adi_Contract_Id = adsa600.newFieldArrayInGroup("adsa600_Pnd_Adi_Contract_Id", "#ADI-CONTRACT-ID", FieldType.NUMERIC, 11, new DbsArrayController(1,
            250));
        adsa600_Adc_Ppg_Cde = adsa600.newFieldInGroup("adsa600_Adc_Ppg_Cde", "ADC-PPG-CDE", FieldType.STRING, 6);
        adsa600_Adc_Trk_Ro_Ivc = adsa600.newFieldInGroup("adsa600_Adc_Trk_Ro_Ivc", "ADC-TRK-RO-IVC", FieldType.STRING, 1);
        adsa600_Adc_Acct_Cde = adsa600.newFieldArrayInGroup("adsa600_Adc_Acct_Cde", "ADC-ACCT-CDE", FieldType.STRING, 1, new DbsArrayController(1,20));
        adsa600_Pnd_Return_Cde = adsa600.newFieldInGroup("adsa600_Pnd_Return_Cde", "#RETURN-CDE", FieldType.STRING, 2);
        adsa600_Pnd_Inst_Nbr = adsa600.newFieldInGroup("adsa600_Pnd_Inst_Nbr", "#INST-NBR", FieldType.PACKED_DECIMAL, 3);
        adsa600_Pnd_This_Inst = adsa600.newFieldInGroup("adsa600_Pnd_This_Inst", "#THIS-INST", FieldType.PACKED_DECIMAL, 3);
        adsa600_Pnd_Ia_Check_Dte = adsa600.newFieldInGroup("adsa600_Pnd_Ia_Check_Dte", "#IA-CHECK-DTE", FieldType.STRING, 8);
        adsa600_Pnd_Ia_Check_DteRedef1 = adsa600.newGroupInGroup("adsa600_Pnd_Ia_Check_DteRedef1", "Redefines", adsa600_Pnd_Ia_Check_Dte);
        adsa600_Pnd_Filler = adsa600_Pnd_Ia_Check_DteRedef1.newFieldInGroup("adsa600_Pnd_Filler", "#FILLER", FieldType.STRING, 4);
        adsa600_Pnd_Ia_Mm = adsa600_Pnd_Ia_Check_DteRedef1.newFieldInGroup("adsa600_Pnd_Ia_Mm", "#IA-MM", FieldType.NUMERIC, 2);
        adsa600_Pnd_Tiaa_Ivc_Used_Amt = adsa600.newFieldInGroup("adsa600_Pnd_Tiaa_Ivc_Used_Amt", "#TIAA-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9,2);
        adsa600_Pnd_Cref_Ivc_Used_Amt = adsa600.newFieldInGroup("adsa600_Pnd_Cref_Ivc_Used_Amt", "#CREF-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9,2);
        adsa600_Adc_Sub_Plan_Nbr = adsa600.newFieldInGroup("adsa600_Adc_Sub_Plan_Nbr", "ADC-SUB-PLAN-NBR", FieldType.STRING, 6);
        adsa600_Adc_T395_Fund_Id = adsa600.newFieldArrayInGroup("adsa600_Adc_T395_Fund_Id", "ADC-T395-FUND-ID", FieldType.STRING, 2, new DbsArrayController(1,
            14));
        adsa600_Adc_Original_Cntrct_Nbr = adsa600.newFieldInGroup("adsa600_Adc_Original_Cntrct_Nbr", "ADC-ORIGINAL-CNTRCT-NBR", FieldType.STRING, 10);
        adsa600_Adc_Original_Sub_Plan_Nbr = adsa600.newFieldInGroup("adsa600_Adc_Original_Sub_Plan_Nbr", "ADC-ORIGINAL-SUB-PLAN-NBR", FieldType.STRING, 
            6);
        adsa600_Adc_Contract_Id = adsa600.newFieldArrayInGroup("adsa600_Adc_Contract_Id", "ADC-CONTRACT-ID", FieldType.NUMERIC, 11, new DbsArrayController(1,
            250));
        adsa600_Adc_Invstmnt_Grpng_Id = adsa600.newFieldArrayInGroup("adsa600_Adc_Invstmnt_Grpng_Id", "ADC-INVSTMNT-GRPNG-ID", FieldType.STRING, 4, new 
            DbsArrayController(1,20));

        dbsRecord.reset();
    }

    // Constructors
    public PdaAdsa600(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

