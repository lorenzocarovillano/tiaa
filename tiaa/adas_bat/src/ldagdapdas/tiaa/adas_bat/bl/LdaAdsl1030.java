/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:49:22 PM
**        * FROM NATURAL LDA     : ADSL1030
************************************************************
**        * FILE NAME            : LdaAdsl1030.java
**        * CLASS NAME           : LdaAdsl1030
**        * INSTANCE NAME        : LdaAdsl1030
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdsl1030 extends DbsRecord
{
    // Properties
    private DbsField pnd_Accounting_Date;
    private DbsGroup pnd_Accounting_DateRedef1;
    private DbsField pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm;
    private DbsField pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd;
    private DbsField pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc;
    private DbsField pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy;
    private DbsField pnd_Payment_Due_Date;
    private DbsGroup pnd_Payment_Due_DateRedef2;
    private DbsField pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Mm;
    private DbsField pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Dd;
    private DbsField pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Cc;
    private DbsField pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Yy;
    private DbsGroup pnd_M_Tiaa_Minor_Accum;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Pay_Cnt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Units;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Dollars;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Gtd_Amt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Div_Amt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Fin_Gtd_Amt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Fin_Div_Amt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Fin_Ivc_Amt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Pay_Cnt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Units;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Dollars;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Gtd_Amt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Div_Amt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Fin_Gtd_Amt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Fin_Div_Amt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Pay_Cnt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Units;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Dollars;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Gtd_Amt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Div_Amt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Fin_Gtd_Amt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Fin_Div_Amt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Fin_Ivc_Amt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Pay_Cnt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Units;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Dollars;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Gtd_Amt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Div_Amt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Fin_Gtd_Amt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Fin_Div_Amt;
    private DbsField pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Fin_Ivc_Amt;
    private DbsField pnd_M_Tiaa_Grd_Fin_Ivc_Amt;
    private DbsGroup pnd_M_Tiaa_Sub_Accum;
    private DbsField pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Pay_Cnt;
    private DbsField pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Units;
    private DbsField pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Dollars;
    private DbsField pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Gtd_Amt;
    private DbsField pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Divid_Amt;
    private DbsField pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Fin_Gtd_Amt;
    private DbsField pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Fin_Div_Amt;
    private DbsField pnd_M_Tiaa_Sub_Fin_Ivc_Amt;
    private DbsGroup pnd_M_C_Mth_Minor_Accum;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt;
    private DbsGroup pnd_M_C_Mth_Fin_Ivc_Amount;
    private DbsField pnd_M_C_Mth_Fin_Ivc_Amount_Pnd_M_C_Mth_Fin_Ivc_Amt;
    private DbsGroup pnd_M_C_Ann_Minor_Accum;
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt;
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units;
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars;
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt;
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt;
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt;
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt;
    private DbsGroup pnd_M_C_Ann_Fin_Ivc_Amount;
    private DbsField pnd_M_C_Ann_Fin_Ivc_Amount_Pnd_M_C_Ann_Fin_Ivc_Amt;
    private DbsGroup pnd_M_Cref_Mth_Sub_Total_Accum;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Pay_Cnt;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Units;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Dollars;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Gtd_Amt;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Div_Amt;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Gtd_Amt;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Div_Amt;
    private DbsField pnd_M_Cref_Mth_Sub_Fin_Ivc_Amt;
    private DbsGroup pnd_M_Cref_Ann_Sub_Total_Accum;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Pay_Cnt;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Units;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Dollars;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Gtd_Amt;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Div_Amt;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Gtd_Amt;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Div_Amt;
    private DbsField pnd_M_Cref_Ann_Sub_Fin_Ivc_Amt;
    private DbsGroup pnd_M_Total_Tiaa_Cref_Accum;
    private DbsField pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Pay_Cnt;
    private DbsField pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Units;
    private DbsField pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Dollars;
    private DbsField pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Gtd_Amt;
    private DbsField pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Div_Amt;
    private DbsField pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Gtd_Amt;
    private DbsField pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Div_Amt;
    private DbsField pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Ivc_Amt;

    public DbsField getPnd_Accounting_Date() { return pnd_Accounting_Date; }

    public DbsGroup getPnd_Accounting_DateRedef1() { return pnd_Accounting_DateRedef1; }

    public DbsField getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm() { return pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm; }

    public DbsField getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd() { return pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd; }

    public DbsField getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc() { return pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc; }

    public DbsField getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy() { return pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy; }

    public DbsField getPnd_Payment_Due_Date() { return pnd_Payment_Due_Date; }

    public DbsGroup getPnd_Payment_Due_DateRedef2() { return pnd_Payment_Due_DateRedef2; }

    public DbsField getPnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Mm() { return pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Mm; }

    public DbsField getPnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Dd() { return pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Dd; }

    public DbsField getPnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Cc() { return pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Cc; }

    public DbsField getPnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Yy() { return pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Yy; }

    public DbsGroup getPnd_M_Tiaa_Minor_Accum() { return pnd_M_Tiaa_Minor_Accum; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Pay_Cnt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Pay_Cnt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Units() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Units; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Dollars() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Dollars; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Gtd_Amt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Gtd_Amt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Div_Amt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Div_Amt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Fin_Gtd_Amt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Fin_Gtd_Amt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Fin_Div_Amt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Fin_Div_Amt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Fin_Ivc_Amt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Fin_Ivc_Amt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Pay_Cnt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Pay_Cnt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Units() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Units; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Dollars() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Dollars; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Gtd_Amt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Gtd_Amt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Div_Amt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Div_Amt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Fin_Gtd_Amt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Fin_Gtd_Amt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Fin_Div_Amt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Fin_Div_Amt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Pay_Cnt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Pay_Cnt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Units() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Units; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Dollars() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Dollars; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Gtd_Amt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Gtd_Amt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Div_Amt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Div_Amt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Fin_Gtd_Amt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Fin_Gtd_Amt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Fin_Div_Amt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Fin_Div_Amt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Fin_Ivc_Amt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Fin_Ivc_Amt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Pay_Cnt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Pay_Cnt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Units() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Units; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Dollars() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Dollars; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Gtd_Amt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Gtd_Amt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Div_Amt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Div_Amt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Fin_Gtd_Amt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Fin_Gtd_Amt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Fin_Div_Amt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Fin_Div_Amt; }

    public DbsField getPnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Fin_Ivc_Amt() { return pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Fin_Ivc_Amt; }

    public DbsField getPnd_M_Tiaa_Grd_Fin_Ivc_Amt() { return pnd_M_Tiaa_Grd_Fin_Ivc_Amt; }

    public DbsGroup getPnd_M_Tiaa_Sub_Accum() { return pnd_M_Tiaa_Sub_Accum; }

    public DbsField getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Pay_Cnt() { return pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Pay_Cnt; }

    public DbsField getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Units() { return pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Units; }

    public DbsField getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Dollars() { return pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Dollars; }

    public DbsField getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Gtd_Amt() { return pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Gtd_Amt; }

    public DbsField getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Divid_Amt() { return pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Divid_Amt; }

    public DbsField getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Fin_Gtd_Amt() { return pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Fin_Gtd_Amt; }

    public DbsField getPnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Fin_Div_Amt() { return pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Fin_Div_Amt; }

    public DbsField getPnd_M_Tiaa_Sub_Fin_Ivc_Amt() { return pnd_M_Tiaa_Sub_Fin_Ivc_Amt; }

    public DbsGroup getPnd_M_C_Mth_Minor_Accum() { return pnd_M_C_Mth_Minor_Accum; }

    public DbsField getPnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt() { return pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt; }

    public DbsField getPnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units() { return pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units; }

    public DbsField getPnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars() { return pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars; }

    public DbsField getPnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt() { return pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt; }

    public DbsField getPnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt() { return pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt; }

    public DbsField getPnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt() { return pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt; }

    public DbsField getPnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt() { return pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt; }

    public DbsGroup getPnd_M_C_Mth_Fin_Ivc_Amount() { return pnd_M_C_Mth_Fin_Ivc_Amount; }

    public DbsField getPnd_M_C_Mth_Fin_Ivc_Amount_Pnd_M_C_Mth_Fin_Ivc_Amt() { return pnd_M_C_Mth_Fin_Ivc_Amount_Pnd_M_C_Mth_Fin_Ivc_Amt; }

    public DbsGroup getPnd_M_C_Ann_Minor_Accum() { return pnd_M_C_Ann_Minor_Accum; }

    public DbsField getPnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt() { return pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt; }

    public DbsField getPnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units() { return pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units; }

    public DbsField getPnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars() { return pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars; }

    public DbsField getPnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt() { return pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt; }

    public DbsField getPnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt() { return pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt; }

    public DbsField getPnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt() { return pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt; }

    public DbsField getPnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt() { return pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt; }

    public DbsGroup getPnd_M_C_Ann_Fin_Ivc_Amount() { return pnd_M_C_Ann_Fin_Ivc_Amount; }

    public DbsField getPnd_M_C_Ann_Fin_Ivc_Amount_Pnd_M_C_Ann_Fin_Ivc_Amt() { return pnd_M_C_Ann_Fin_Ivc_Amount_Pnd_M_C_Ann_Fin_Ivc_Amt; }

    public DbsGroup getPnd_M_Cref_Mth_Sub_Total_Accum() { return pnd_M_Cref_Mth_Sub_Total_Accum; }

    public DbsField getPnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Pay_Cnt() { return pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Pay_Cnt; 
        }

    public DbsField getPnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Units() { return pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Units; }

    public DbsField getPnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Dollars() { return pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Dollars; 
        }

    public DbsField getPnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Gtd_Amt() { return pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Gtd_Amt; 
        }

    public DbsField getPnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Div_Amt() { return pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Div_Amt; 
        }

    public DbsField getPnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Gtd_Amt() { return pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Gtd_Amt; 
        }

    public DbsField getPnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Div_Amt() { return pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Div_Amt; 
        }

    public DbsField getPnd_M_Cref_Mth_Sub_Fin_Ivc_Amt() { return pnd_M_Cref_Mth_Sub_Fin_Ivc_Amt; }

    public DbsGroup getPnd_M_Cref_Ann_Sub_Total_Accum() { return pnd_M_Cref_Ann_Sub_Total_Accum; }

    public DbsField getPnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Pay_Cnt() { return pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Pay_Cnt; 
        }

    public DbsField getPnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Units() { return pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Units; }

    public DbsField getPnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Dollars() { return pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Dollars; 
        }

    public DbsField getPnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Gtd_Amt() { return pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Gtd_Amt; 
        }

    public DbsField getPnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Div_Amt() { return pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Div_Amt; 
        }

    public DbsField getPnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Gtd_Amt() { return pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Gtd_Amt; 
        }

    public DbsField getPnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Div_Amt() { return pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Div_Amt; 
        }

    public DbsField getPnd_M_Cref_Ann_Sub_Fin_Ivc_Amt() { return pnd_M_Cref_Ann_Sub_Fin_Ivc_Amt; }

    public DbsGroup getPnd_M_Total_Tiaa_Cref_Accum() { return pnd_M_Total_Tiaa_Cref_Accum; }

    public DbsField getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Pay_Cnt() { return pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Pay_Cnt; }

    public DbsField getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Units() { return pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Units; }

    public DbsField getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Dollars() { return pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Dollars; }

    public DbsField getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Gtd_Amt() { return pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Gtd_Amt; }

    public DbsField getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Div_Amt() { return pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Div_Amt; }

    public DbsField getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Gtd_Amt() { return pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Gtd_Amt; }

    public DbsField getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Div_Amt() { return pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Div_Amt; }

    public DbsField getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Ivc_Amt() { return pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Ivc_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Accounting_Date = newFieldInRecord("pnd_Accounting_Date", "#ACCOUNTING-DATE", FieldType.NUMERIC, 8);
        pnd_Accounting_DateRedef1 = newGroupInRecord("pnd_Accounting_DateRedef1", "Redefines", pnd_Accounting_Date);
        pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm = pnd_Accounting_DateRedef1.newFieldInGroup("pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm", 
            "#ACCOUNTING-DATE-MCDY-MM", FieldType.NUMERIC, 2);
        pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd = pnd_Accounting_DateRedef1.newFieldInGroup("pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd", 
            "#ACCOUNTING-DATE-MCDY-DD", FieldType.NUMERIC, 2);
        pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc = pnd_Accounting_DateRedef1.newFieldInGroup("pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc", 
            "#ACCOUNTING-DATE-MCDY-CC", FieldType.NUMERIC, 2);
        pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy = pnd_Accounting_DateRedef1.newFieldInGroup("pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy", 
            "#ACCOUNTING-DATE-MCDY-YY", FieldType.NUMERIC, 2);

        pnd_Payment_Due_Date = newFieldInRecord("pnd_Payment_Due_Date", "#PAYMENT-DUE-DATE", FieldType.NUMERIC, 8);
        pnd_Payment_Due_DateRedef2 = newGroupInRecord("pnd_Payment_Due_DateRedef2", "Redefines", pnd_Payment_Due_Date);
        pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Mm = pnd_Payment_Due_DateRedef2.newFieldInGroup("pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Mm", "#PAYMENT-DATE-MCDY-MM", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Dd = pnd_Payment_Due_DateRedef2.newFieldInGroup("pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Dd", "#PAYMENT-DATE-MCDY-DD", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Cc = pnd_Payment_Due_DateRedef2.newFieldInGroup("pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Cc", "#PAYMENT-DATE-MCDY-CC", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Yy = pnd_Payment_Due_DateRedef2.newFieldInGroup("pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Yy", "#PAYMENT-DATE-MCDY-YY", 
            FieldType.NUMERIC, 2);

        pnd_M_Tiaa_Minor_Accum = newGroupInRecord("pnd_M_Tiaa_Minor_Accum", "#M-TIAA-MINOR-ACCUM");
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Pay_Cnt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Pay_Cnt", "#M-TIAA-STD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Units = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Units", "#M-TIAA-STD-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Dollars = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Dollars", "#M-TIAA-STD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Gtd_Amt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Gtd_Amt", "#M-TIAA-STD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Div_Amt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Div_Amt", "#M-TIAA-STD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Fin_Gtd_Amt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Fin_Gtd_Amt", 
            "#M-TIAA-STD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Fin_Div_Amt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Fin_Div_Amt", 
            "#M-TIAA-STD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Fin_Ivc_Amt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Std_Fin_Ivc_Amt", 
            "#M-TIAA-STD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Pay_Cnt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Pay_Cnt", "#M-TIAA-GRD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Units = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Units", "#M-TIAA-GRD-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Dollars = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Dollars", "#M-TIAA-GRD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Gtd_Amt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Gtd_Amt", "#M-TIAA-GRD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Div_Amt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Div_Amt", "#M-TIAA-GRD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Fin_Gtd_Amt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Fin_Gtd_Amt", 
            "#M-TIAA-GRD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Fin_Div_Amt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Grd_Fin_Div_Amt", 
            "#M-TIAA-GRD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Pay_Cnt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Pay_Cnt", 
            "#M-TIAA-TPA-STD-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Units = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Units", "#M-TIAA-TPA-STD-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Dollars = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Dollars", 
            "#M-TIAA-TPA-STD-DOLLARS", FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Gtd_Amt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Gtd_Amt", 
            "#M-TIAA-TPA-STD-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Div_Amt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Div_Amt", 
            "#M-TIAA-TPA-STD-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Fin_Gtd_Amt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Fin_Gtd_Amt", 
            "#M-TIAA-TPA-STD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Fin_Div_Amt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Fin_Div_Amt", 
            "#M-TIAA-TPA-STD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Fin_Ivc_Amt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Tpa_Std_Fin_Ivc_Amt", 
            "#M-TIAA-TPA-STD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Pay_Cnt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Pay_Cnt", 
            "#M-TIAA-IPRO-STD-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Units = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Units", 
            "#M-TIAA-IPRO-STD-UNITS", FieldType.PACKED_DECIMAL, 10,4);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Dollars = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Dollars", 
            "#M-TIAA-IPRO-STD-DOLLARS", FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Gtd_Amt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Gtd_Amt", 
            "#M-TIAA-IPRO-STD-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Div_Amt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Div_Amt", 
            "#M-TIAA-IPRO-STD-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Fin_Gtd_Amt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Fin_Gtd_Amt", 
            "#M-TIAA-IPRO-STD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Fin_Div_Amt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Fin_Div_Amt", 
            "#M-TIAA-IPRO-STD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Fin_Ivc_Amt = pnd_M_Tiaa_Minor_Accum.newFieldInGroup("pnd_M_Tiaa_Minor_Accum_Pnd_M_Tiaa_Ipro_Std_Fin_Ivc_Amt", 
            "#M-TIAA-IPRO-STD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_M_Tiaa_Grd_Fin_Ivc_Amt = newFieldInRecord("pnd_M_Tiaa_Grd_Fin_Ivc_Amt", "#M-TIAA-GRD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_M_Tiaa_Sub_Accum = newGroupInRecord("pnd_M_Tiaa_Sub_Accum", "#M-TIAA-SUB-ACCUM");
        pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Pay_Cnt = pnd_M_Tiaa_Sub_Accum.newFieldInGroup("pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Pay_Cnt", "#M-TIAA-SUB-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Units = pnd_M_Tiaa_Sub_Accum.newFieldInGroup("pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Units", "#M-TIAA-SUB-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Dollars = pnd_M_Tiaa_Sub_Accum.newFieldInGroup("pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Dollars", "#M-TIAA-SUB-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Gtd_Amt = pnd_M_Tiaa_Sub_Accum.newFieldInGroup("pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Gtd_Amt", "#M-TIAA-SUB-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Divid_Amt = pnd_M_Tiaa_Sub_Accum.newFieldInGroup("pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Divid_Amt", "#M-TIAA-SUB-DIVID-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Fin_Gtd_Amt = pnd_M_Tiaa_Sub_Accum.newFieldInGroup("pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Fin_Gtd_Amt", "#M-TIAA-SUB-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Fin_Div_Amt = pnd_M_Tiaa_Sub_Accum.newFieldInGroup("pnd_M_Tiaa_Sub_Accum_Pnd_M_Tiaa_Sub_Fin_Div_Amt", "#M-TIAA-SUB-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);

        pnd_M_Tiaa_Sub_Fin_Ivc_Amt = newFieldInRecord("pnd_M_Tiaa_Sub_Fin_Ivc_Amt", "#M-TIAA-SUB-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_M_C_Mth_Minor_Accum = newGroupArrayInRecord("pnd_M_C_Mth_Minor_Accum", "#M-C-MTH-MINOR-ACCUM", new DbsArrayController(1,20));
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt = pnd_M_C_Mth_Minor_Accum.newFieldInGroup("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt", "#M-C-MTH-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units = pnd_M_C_Mth_Minor_Accum.newFieldInGroup("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units", "#M-C-MTH-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars = pnd_M_C_Mth_Minor_Accum.newFieldInGroup("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars", "#M-C-MTH-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt = pnd_M_C_Mth_Minor_Accum.newFieldInGroup("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt", "#M-C-MTH-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt = pnd_M_C_Mth_Minor_Accum.newFieldInGroup("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt", "#M-C-MTH-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt = pnd_M_C_Mth_Minor_Accum.newFieldInGroup("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt", "#M-C-MTH-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt = pnd_M_C_Mth_Minor_Accum.newFieldInGroup("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt", "#M-C-MTH-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);

        pnd_M_C_Mth_Fin_Ivc_Amount = newGroupInRecord("pnd_M_C_Mth_Fin_Ivc_Amount", "#M-C-MTH-FIN-IVC-AMOUNT");
        pnd_M_C_Mth_Fin_Ivc_Amount_Pnd_M_C_Mth_Fin_Ivc_Amt = pnd_M_C_Mth_Fin_Ivc_Amount.newFieldInGroup("pnd_M_C_Mth_Fin_Ivc_Amount_Pnd_M_C_Mth_Fin_Ivc_Amt", 
            "#M-C-MTH-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_M_C_Ann_Minor_Accum = newGroupArrayInRecord("pnd_M_C_Ann_Minor_Accum", "#M-C-ANN-MINOR-ACCUM", new DbsArrayController(1,20));
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt = pnd_M_C_Ann_Minor_Accum.newFieldInGroup("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt", "#M-C-ANN-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units = pnd_M_C_Ann_Minor_Accum.newFieldInGroup("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units", "#M-C-ANN-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars = pnd_M_C_Ann_Minor_Accum.newFieldInGroup("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars", "#M-C-ANN-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt = pnd_M_C_Ann_Minor_Accum.newFieldInGroup("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt", "#M-C-ANN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt = pnd_M_C_Ann_Minor_Accum.newFieldInGroup("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt", "#M-C-ANN-DIVID-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt = pnd_M_C_Ann_Minor_Accum.newFieldInGroup("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt", "#M-C-ANN-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt = pnd_M_C_Ann_Minor_Accum.newFieldInGroup("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt", "#M-C-ANN-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);

        pnd_M_C_Ann_Fin_Ivc_Amount = newGroupInRecord("pnd_M_C_Ann_Fin_Ivc_Amount", "#M-C-ANN-FIN-IVC-AMOUNT");
        pnd_M_C_Ann_Fin_Ivc_Amount_Pnd_M_C_Ann_Fin_Ivc_Amt = pnd_M_C_Ann_Fin_Ivc_Amount.newFieldInGroup("pnd_M_C_Ann_Fin_Ivc_Amount_Pnd_M_C_Ann_Fin_Ivc_Amt", 
            "#M-C-ANN-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_M_Cref_Mth_Sub_Total_Accum = newGroupInRecord("pnd_M_Cref_Mth_Sub_Total_Accum", "#M-CREF-MTH-SUB-TOTAL-ACCUM");
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Pay_Cnt = pnd_M_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Pay_Cnt", 
            "#M-CREF-MTH-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Units = pnd_M_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Units", 
            "#M-CREF-MTH-SUB-UNITS", FieldType.PACKED_DECIMAL, 10,4);
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Dollars = pnd_M_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Dollars", 
            "#M-CREF-MTH-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 12,2);
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Gtd_Amt = pnd_M_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Gtd_Amt", 
            "#M-CREF-MTH-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Div_Amt = pnd_M_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Div_Amt", 
            "#M-CREF-MTH-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Gtd_Amt = pnd_M_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Gtd_Amt", 
            "#M-CREF-MTH-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Div_Amt = pnd_M_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Div_Amt", 
            "#M-CREF-MTH-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);

        pnd_M_Cref_Mth_Sub_Fin_Ivc_Amt = newFieldInRecord("pnd_M_Cref_Mth_Sub_Fin_Ivc_Amt", "#M-CREF-MTH-SUB-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,
            2);

        pnd_M_Cref_Ann_Sub_Total_Accum = newGroupInRecord("pnd_M_Cref_Ann_Sub_Total_Accum", "#M-CREF-ANN-SUB-TOTAL-ACCUM");
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Pay_Cnt = pnd_M_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Pay_Cnt", 
            "#M-CREF-ANN-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Units = pnd_M_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Units", 
            "#M-CREF-ANN-SUB-UNITS", FieldType.PACKED_DECIMAL, 10,4);
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Dollars = pnd_M_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Dollars", 
            "#M-CREF-ANN-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 12,2);
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Gtd_Amt = pnd_M_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Gtd_Amt", 
            "#M-CREF-ANN-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Div_Amt = pnd_M_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Div_Amt", 
            "#M-CREF-ANN-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Gtd_Amt = pnd_M_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Gtd_Amt", 
            "#M-CREF-ANN-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Div_Amt = pnd_M_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Div_Amt", 
            "#M-CREF-ANN-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);

        pnd_M_Cref_Ann_Sub_Fin_Ivc_Amt = newFieldInRecord("pnd_M_Cref_Ann_Sub_Fin_Ivc_Amt", "#M-CREF-ANN-SUB-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,
            2);

        pnd_M_Total_Tiaa_Cref_Accum = newGroupInRecord("pnd_M_Total_Tiaa_Cref_Accum", "#M-TOTAL-TIAA-CREF-ACCUM");
        pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Pay_Cnt = pnd_M_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Pay_Cnt", 
            "#M-TOTAL-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Units = pnd_M_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Units", "#M-TOTAL-UNITS", 
            FieldType.PACKED_DECIMAL, 13,4);
        pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Dollars = pnd_M_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Dollars", 
            "#M-TOTAL-DOLLARS", FieldType.PACKED_DECIMAL, 14,2);
        pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Gtd_Amt = pnd_M_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Gtd_Amt", 
            "#M-TOTAL-GTD-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Div_Amt = pnd_M_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Div_Amt", 
            "#M-TOTAL-DIV-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Gtd_Amt = pnd_M_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Gtd_Amt", 
            "#M-TOTAL-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Div_Amt = pnd_M_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Div_Amt", 
            "#M-TOTAL-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Ivc_Amt = pnd_M_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Ivc_Amt", 
            "#M-TOTAL-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 14,2);

        this.setRecordName("LdaAdsl1030");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAdsl1030() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
