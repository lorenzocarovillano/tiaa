/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:20 PM
**        * FROM NATURAL LDA     : ADSL763
************************************************************
**        * FILE NAME            : LdaAdsl763.java
**        * CLASS NAME           : LdaAdsl763
**        * INSTANCE NAME        : LdaAdsl763
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdsl763 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Cref_Ivc_Amounts;
    private DbsField pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Total;
    private DbsField pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Rtb;
    private DbsField pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Annuity;
    private DbsGroup pnd_Cref_Data_Entry_Information;
    private DbsField pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Entered_By;
    private DbsField pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Verified_By;
    private DbsField pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Updated_By;
    private DbsField pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Verified_By;

    public DbsGroup getPnd_Cref_Ivc_Amounts() { return pnd_Cref_Ivc_Amounts; }

    public DbsField getPnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Total() { return pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Total; }

    public DbsField getPnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Rtb() { return pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Rtb; }

    public DbsField getPnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Annuity() { return pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Annuity; }

    public DbsGroup getPnd_Cref_Data_Entry_Information() { return pnd_Cref_Data_Entry_Information; }

    public DbsField getPnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Entered_By() { return pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Entered_By; 
        }

    public DbsField getPnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Verified_By() { return pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Verified_By; 
        }

    public DbsField getPnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Updated_By() { return pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Updated_By; 
        }

    public DbsField getPnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Verified_By() { return pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Verified_By; 
        }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Cref_Ivc_Amounts = newGroupInRecord("pnd_Cref_Ivc_Amounts", "#CREF-IVC-AMOUNTS");
        pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Total = pnd_Cref_Ivc_Amounts.newFieldInGroup("pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Total", "#CREF-IVC-TOTAL", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Rtb = pnd_Cref_Ivc_Amounts.newFieldInGroup("pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Rtb", "#CREF-IVC-RTB", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Annuity = pnd_Cref_Ivc_Amounts.newFieldInGroup("pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Annuity", "#CREF-IVC-ANNUITY", 
            FieldType.PACKED_DECIMAL, 11,2);

        pnd_Cref_Data_Entry_Information = newGroupInRecord("pnd_Cref_Data_Entry_Information", "#CREF-DATA-ENTRY-INFORMATION");
        pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Entered_By = pnd_Cref_Data_Entry_Information.newFieldInGroup("pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Entered_By", 
            "#CREF-RQST-FIRST-ENTERED-BY", FieldType.STRING, 8);
        pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Verified_By = pnd_Cref_Data_Entry_Information.newFieldInGroup("pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Verified_By", 
            "#CREF-RQST-FIRST-VERIFIED-BY", FieldType.STRING, 8);
        pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Updated_By = pnd_Cref_Data_Entry_Information.newFieldInGroup("pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Updated_By", 
            "#CREF-RQST-LAST-UPDATED-BY", FieldType.STRING, 8);
        pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Verified_By = pnd_Cref_Data_Entry_Information.newFieldInGroup("pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Verified_By", 
            "#CREF-RQST-LAST-VERIFIED-BY", FieldType.STRING, 8);

        this.setRecordName("LdaAdsl763");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAdsl763() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
