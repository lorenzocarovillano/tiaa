/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:17:45 PM
**        * FROM NATURAL PDA     : AIAL0330
************************************************************
**        * FILE NAME            : PdaAial0330.java
**        * CLASS NAME           : PdaAial0330
**        * INSTANCE NAME        : PdaAial0330
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaAial0330 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Aian033_Linkage;
    private DbsGroup pnd_Aian033_Linkage_Pnd_Aian033_Input_Data;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_Option_Code;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_Guar_Years;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_Guar_Months;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_Issue_Date;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_1st_Ann_Birth_Date;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Birth_Date;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_Beneficiary_Birth_Date;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Type;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_Ben_Type;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_Settlement_Type;
    private DbsGroup pnd_Aian033_Linkage_Pnd_Aian033_Output_Data;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_Md_Exp_Life;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_Mdib_Exp_Life;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_Minimum_Life_Exp;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_Minimum_Pct_Allow;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Grp;
    private DbsGroup pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_GrpRedef1;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_Grp;
    private DbsGroup pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_GrpRedef2;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_Pgm;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct;
    private DbsField pnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Pct;

    public DbsGroup getPnd_Aian033_Linkage() { return pnd_Aian033_Linkage; }

    public DbsGroup getPnd_Aian033_Linkage_Pnd_Aian033_Input_Data() { return pnd_Aian033_Linkage_Pnd_Aian033_Input_Data; }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code() { return pnd_Aian033_Linkage_Pnd_Aian033_Option_Code; }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_Guar_Years() { return pnd_Aian033_Linkage_Pnd_Aian033_Guar_Years; }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_Guar_Months() { return pnd_Aian033_Linkage_Pnd_Aian033_Guar_Months; }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_Issue_Date() { return pnd_Aian033_Linkage_Pnd_Aian033_Issue_Date; }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_1st_Ann_Birth_Date() { return pnd_Aian033_Linkage_Pnd_Aian033_1st_Ann_Birth_Date; }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Birth_Date() { return pnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Birth_Date; }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_Beneficiary_Birth_Date() { return pnd_Aian033_Linkage_Pnd_Aian033_Beneficiary_Birth_Date; }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Type() { return pnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Type; }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_Ben_Type() { return pnd_Aian033_Linkage_Pnd_Aian033_Ben_Type; }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_Settlement_Type() { return pnd_Aian033_Linkage_Pnd_Aian033_Settlement_Type; }

    public DbsGroup getPnd_Aian033_Linkage_Pnd_Aian033_Output_Data() { return pnd_Aian033_Linkage_Pnd_Aian033_Output_Data; }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_Md_Exp_Life() { return pnd_Aian033_Linkage_Pnd_Aian033_Md_Exp_Life; }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_Mdib_Exp_Life() { return pnd_Aian033_Linkage_Pnd_Aian033_Mdib_Exp_Life; }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_Minimum_Life_Exp() { return pnd_Aian033_Linkage_Pnd_Aian033_Minimum_Life_Exp; }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_Minimum_Pct_Allow() { return pnd_Aian033_Linkage_Pnd_Aian033_Minimum_Pct_Allow; }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Grp() { return pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Grp; }

    public DbsGroup getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_GrpRedef1() { return pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_GrpRedef1; 
        }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm() { return pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm; }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar() { return pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar; }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar() { return pnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar; }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_Grp() { return pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_Grp; }

    public DbsGroup getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_GrpRedef2() { return pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_GrpRedef2; 
        }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_Pgm() { return pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_Pgm; }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct() { return pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct; }

    public DbsField getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Pct() { return pnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Pct; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Aian033_Linkage = dbsRecord.newGroupInRecord("pnd_Aian033_Linkage", "#AIAN033-LINKAGE");
        pnd_Aian033_Linkage.setParameterOption(ParameterOption.ByReference);
        pnd_Aian033_Linkage_Pnd_Aian033_Input_Data = pnd_Aian033_Linkage.newGroupInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Input_Data", "#AIAN033-INPUT-DATA");
        pnd_Aian033_Linkage_Pnd_Aian033_Option_Code = pnd_Aian033_Linkage_Pnd_Aian033_Input_Data.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Option_Code", 
            "#AIAN033-OPTION-CODE", FieldType.NUMERIC, 2);
        pnd_Aian033_Linkage_Pnd_Aian033_Guar_Years = pnd_Aian033_Linkage_Pnd_Aian033_Input_Data.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Guar_Years", 
            "#AIAN033-GUAR-YEARS", FieldType.NUMERIC, 2);
        pnd_Aian033_Linkage_Pnd_Aian033_Guar_Months = pnd_Aian033_Linkage_Pnd_Aian033_Input_Data.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Guar_Months", 
            "#AIAN033-GUAR-MONTHS", FieldType.NUMERIC, 2);
        pnd_Aian033_Linkage_Pnd_Aian033_Issue_Date = pnd_Aian033_Linkage_Pnd_Aian033_Input_Data.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Issue_Date", 
            "#AIAN033-ISSUE-DATE", FieldType.DATE);
        pnd_Aian033_Linkage_Pnd_Aian033_1st_Ann_Birth_Date = pnd_Aian033_Linkage_Pnd_Aian033_Input_Data.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_1st_Ann_Birth_Date", 
            "#AIAN033-1ST-ANN-BIRTH-DATE", FieldType.DATE);
        pnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Birth_Date = pnd_Aian033_Linkage_Pnd_Aian033_Input_Data.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Birth_Date", 
            "#AIAN033-2ND-ANN-BIRTH-DATE", FieldType.DATE);
        pnd_Aian033_Linkage_Pnd_Aian033_Beneficiary_Birth_Date = pnd_Aian033_Linkage_Pnd_Aian033_Input_Data.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Beneficiary_Birth_Date", 
            "#AIAN033-BENEFICIARY-BIRTH-DATE", FieldType.DATE);
        pnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Type = pnd_Aian033_Linkage_Pnd_Aian033_Input_Data.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Type", 
            "#AIAN033-2ND-ANN-TYPE", FieldType.STRING, 1);
        pnd_Aian033_Linkage_Pnd_Aian033_Ben_Type = pnd_Aian033_Linkage_Pnd_Aian033_Input_Data.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Ben_Type", 
            "#AIAN033-BEN-TYPE", FieldType.STRING, 1);
        pnd_Aian033_Linkage_Pnd_Aian033_Settlement_Type = pnd_Aian033_Linkage_Pnd_Aian033_Input_Data.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Settlement_Type", 
            "#AIAN033-SETTLEMENT-TYPE", FieldType.STRING, 1);
        pnd_Aian033_Linkage_Pnd_Aian033_Output_Data = pnd_Aian033_Linkage.newGroupInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Output_Data", "#AIAN033-OUTPUT-DATA");
        pnd_Aian033_Linkage_Pnd_Aian033_Md_Exp_Life = pnd_Aian033_Linkage_Pnd_Aian033_Output_Data.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Md_Exp_Life", 
            "#AIAN033-MD-EXP-LIFE", FieldType.DECIMAL, 4,1);
        pnd_Aian033_Linkage_Pnd_Aian033_Mdib_Exp_Life = pnd_Aian033_Linkage_Pnd_Aian033_Output_Data.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Mdib_Exp_Life", 
            "#AIAN033-MDIB-EXP-LIFE", FieldType.DECIMAL, 4,1);
        pnd_Aian033_Linkage_Pnd_Aian033_Minimum_Life_Exp = pnd_Aian033_Linkage_Pnd_Aian033_Output_Data.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Minimum_Life_Exp", 
            "#AIAN033-MINIMUM-LIFE-EXP", FieldType.DECIMAL, 4,1);
        pnd_Aian033_Linkage_Pnd_Aian033_Minimum_Pct_Allow = pnd_Aian033_Linkage_Pnd_Aian033_Output_Data.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Minimum_Pct_Allow", 
            "#AIAN033-MINIMUM-PCT-ALLOW", FieldType.DECIMAL, 3,2);
        pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Grp = pnd_Aian033_Linkage_Pnd_Aian033_Output_Data.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Grp", 
            "#AIAN033-RETURN-CODE-GUAR-GRP", FieldType.STRING, 11);
        pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_GrpRedef1 = pnd_Aian033_Linkage_Pnd_Aian033_Output_Data.newGroupInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_GrpRedef1", 
            "Redefines", pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Grp);
        pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm = pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_GrpRedef1.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm", 
            "#AIAN033-RETURN-CODE-GUAR-PGM", FieldType.STRING, 8);
        pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar = pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_GrpRedef1.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar", 
            "#AIAN033-RETURN-CODE-GUAR", FieldType.NUMERIC, 3);
        pnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar = pnd_Aian033_Linkage_Pnd_Aian033_Output_Data.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar", 
            "#AIAN033-RETURN-MSG-GUAR", FieldType.STRING, 30);
        pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_Grp = pnd_Aian033_Linkage_Pnd_Aian033_Output_Data.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_Grp", 
            "#AIAN033-RETURN-CODE-PCT-GRP", FieldType.STRING, 11);
        pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_GrpRedef2 = pnd_Aian033_Linkage_Pnd_Aian033_Output_Data.newGroupInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_GrpRedef2", 
            "Redefines", pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_Grp);
        pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_Pgm = pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_GrpRedef2.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_Pgm", 
            "#AIAN033-RETURN-CODE-PCT-PGM", FieldType.STRING, 8);
        pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct = pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_GrpRedef2.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct", 
            "#AIAN033-RETURN-CODE-PCT", FieldType.NUMERIC, 3);
        pnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Pct = pnd_Aian033_Linkage_Pnd_Aian033_Output_Data.newFieldInGroup("pnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Pct", 
            "#AIAN033-RETURN-MSG-PCT", FieldType.STRING, 30);

        dbsRecord.reset();
    }

    // Constructors
    public PdaAial0330(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

