/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:49:29 PM
**        * FROM NATURAL LDA     : ADSL400A
************************************************************
**        * FILE NAME            : LdaAdsl400a.java
**        * CLASS NAME           : LdaAdsl400a
**        * INSTANCE NAME        : LdaAdsl400a
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdsl400a extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Prtcpnt;
    private DbsField pnd_Prtcpnt_Adp_Stts_Cde;
    private DbsField pnd_Prtcpnt_Adp_Unique_Id;
    private DbsField pnd_Prtcpnt_Adp_Annty_Strt_Dte;
    private DbsField pnd_Prtcpnt_Adp_Annt_Typ_Cde;
    private DbsField pnd_Prtcpnt_Adp_Entry_User_Id;
    private DbsField pnd_Prtcpnt_Adp_Rqst_Last_Updtd_Id;
    private DbsField pnd_Prtcpnt_Adp_Wpid;
    private DbsField pnd_Prtcpnt_Adp_Mit_Log_Dte_Tme;
    private DbsField pnd_Prtcpnt_Adp_Bps_Unit;
    private DbsField pnd_Prtcpnt_Adp_Cntrcts_In_Rqst;
    private DbsField pnd_Prtcpnt_Adp_Isn;
    private DbsField pnd_Prtcpnt_Adp_Cntrct_Isn;
    private DbsField pnd_Prtcpnt_Omni_Cntrcts_In_Rqst;
    private DbsField pnd_Prtcpnt_Adp_Rqst_Complete;
    private DbsGroup pnd_Cntrct;
    private DbsField pnd_Cntrct_Adc_Stts_Cde;
    private DbsField pnd_Cntrct_Adc_Lst_Actvty_Dte;
    private DbsGroup pnd_Cntrct_Adc_Rqst_Info;
    private DbsField pnd_Cntrct_Adc_Acct_Cde;
    private DbsField pnd_Cntrct_Adc_Tkr_Symbl;
    private DbsField pnd_Cntrct_Adc_Acct_Stts_Cde;
    private DbsField pnd_Cntrct_Adc_Acct_Typ;
    private DbsField pnd_Cntrct_Adc_Acct_Qty;
    private DbsField pnd_Cntrct_Adc_Acct_Actl_Amt;
    private DbsField pnd_Cntrct_Adc_Acct_Actl_Nbr_Units;
    private DbsField pnd_Cntrct_Adc_Acct_Cref_Rate_Cde;
    private DbsField pnd_Cntrct_Adc_Acct_Opn_Accum_Amt;
    private DbsField pnd_Cntrct_Adc_Acct_Opn_Nbr_Units;
    private DbsGroup pnd_Cntrct_Adc_Acct_Info;
    private DbsField pnd_Cntrct_Adc_Acct_Aftr_Tax_Amt;
    private DbsGroup pnd_Cntrct_Adc_Invstmt_Info;
    private DbsField pnd_Cntrct_Adc_Invstmt_Id;
    private DbsField pnd_Cntrct_Adc_Inv_Short_Name;
    private DbsGroup pnd_Cntrct_Adc_Dtl_Tiaa_Data;
    private DbsField pnd_Cntrct_Adc_Dtl_Tiaa_Rate_Cde;
    private DbsField pnd_Cntrct_Adc_Dtl_Tiaa_Actl_Amt;
    private DbsField pnd_Cntrct_Adc_Dtl_Tiaa_Opn_Accum_Amt;
    private DbsGroup pnd_Cntrct_Adc_Store_Data;
    private DbsField pnd_Cntrct_Rqst_Id;
    private DbsGroup pnd_Cntrct_Rqst_IdRedef1;
    private DbsField pnd_Cntrct_Rqst_Pin;
    private DbsField pnd_Cntrct_Rqst_Effctv_Dte;
    private DbsField pnd_Cntrct_Rqst_Entry_Dte;
    private DbsField pnd_Cntrct_Rqst_Entry_Tme;
    private DbsField pnd_Cntrct_Adc_Tiaa_Nbr;
    private DbsField pnd_Cntrct_Adc_Cref_Nbr;
    private DbsField pnd_Cntrct_Adc_Cntrct_Issue_State;
    private DbsField pnd_Cntrct_Adc_Cntrct_Issue_Dte;
    private DbsField pnd_Cntrct_Adc_Sttl_Ivc_Amt;
    private DbsField pnd_Cntrct_Adc_Grd_Mnthly_Typ;
    private DbsField pnd_Cntrct_Adc_Grd_Mnthly_Qty;
    private DbsField pnd_Cntrct_Adc_Sub_Plan_Nbr;
    private DbsGroup pnd_Cntrct_Adc_Tpa_Io_Data;
    private DbsGroup pnd_Cntrct_Adc_Tpa_Guarant_Commut_Info;
    private DbsField pnd_Cntrct_Adc_Tpa_Guarant_Commut_Amt;
    private DbsField pnd_Cntrct_Adc_Tpa_Guarant_Commut_Grd_Amt;
    private DbsField pnd_Cntrct_Adc_Tpa_Guarant_Commut_Std_Amt;
    private DbsField pnd_Cntrct_Adc_Orig_Lob_Ind;
    private DbsField pnd_Cntrct_Adc_Tpa_Paymnt_Cnt;
    private DbsField pnd_Cntrct_Adc_Contract_Id;
    private DbsGroup pnd_Cntrct_Adc_Misc_Variables;
    private DbsField pnd_Cntrct_Fnd_Cnt;
    private DbsField pnd_Cntrct_Rte_Cnt;
    private DbsField pnd_Cntrct_New_Cntrct;
    private DbsField pnd_Cntrct_Err_Report_Printed;

    public DbsGroup getPnd_Prtcpnt() { return pnd_Prtcpnt; }

    public DbsField getPnd_Prtcpnt_Adp_Stts_Cde() { return pnd_Prtcpnt_Adp_Stts_Cde; }

    public DbsField getPnd_Prtcpnt_Adp_Unique_Id() { return pnd_Prtcpnt_Adp_Unique_Id; }

    public DbsField getPnd_Prtcpnt_Adp_Annty_Strt_Dte() { return pnd_Prtcpnt_Adp_Annty_Strt_Dte; }

    public DbsField getPnd_Prtcpnt_Adp_Annt_Typ_Cde() { return pnd_Prtcpnt_Adp_Annt_Typ_Cde; }

    public DbsField getPnd_Prtcpnt_Adp_Entry_User_Id() { return pnd_Prtcpnt_Adp_Entry_User_Id; }

    public DbsField getPnd_Prtcpnt_Adp_Rqst_Last_Updtd_Id() { return pnd_Prtcpnt_Adp_Rqst_Last_Updtd_Id; }

    public DbsField getPnd_Prtcpnt_Adp_Wpid() { return pnd_Prtcpnt_Adp_Wpid; }

    public DbsField getPnd_Prtcpnt_Adp_Mit_Log_Dte_Tme() { return pnd_Prtcpnt_Adp_Mit_Log_Dte_Tme; }

    public DbsField getPnd_Prtcpnt_Adp_Bps_Unit() { return pnd_Prtcpnt_Adp_Bps_Unit; }

    public DbsField getPnd_Prtcpnt_Adp_Cntrcts_In_Rqst() { return pnd_Prtcpnt_Adp_Cntrcts_In_Rqst; }

    public DbsField getPnd_Prtcpnt_Adp_Isn() { return pnd_Prtcpnt_Adp_Isn; }

    public DbsField getPnd_Prtcpnt_Adp_Cntrct_Isn() { return pnd_Prtcpnt_Adp_Cntrct_Isn; }

    public DbsField getPnd_Prtcpnt_Omni_Cntrcts_In_Rqst() { return pnd_Prtcpnt_Omni_Cntrcts_In_Rqst; }

    public DbsField getPnd_Prtcpnt_Adp_Rqst_Complete() { return pnd_Prtcpnt_Adp_Rqst_Complete; }

    public DbsGroup getPnd_Cntrct() { return pnd_Cntrct; }

    public DbsField getPnd_Cntrct_Adc_Stts_Cde() { return pnd_Cntrct_Adc_Stts_Cde; }

    public DbsField getPnd_Cntrct_Adc_Lst_Actvty_Dte() { return pnd_Cntrct_Adc_Lst_Actvty_Dte; }

    public DbsGroup getPnd_Cntrct_Adc_Rqst_Info() { return pnd_Cntrct_Adc_Rqst_Info; }

    public DbsField getPnd_Cntrct_Adc_Acct_Cde() { return pnd_Cntrct_Adc_Acct_Cde; }

    public DbsField getPnd_Cntrct_Adc_Tkr_Symbl() { return pnd_Cntrct_Adc_Tkr_Symbl; }

    public DbsField getPnd_Cntrct_Adc_Acct_Stts_Cde() { return pnd_Cntrct_Adc_Acct_Stts_Cde; }

    public DbsField getPnd_Cntrct_Adc_Acct_Typ() { return pnd_Cntrct_Adc_Acct_Typ; }

    public DbsField getPnd_Cntrct_Adc_Acct_Qty() { return pnd_Cntrct_Adc_Acct_Qty; }

    public DbsField getPnd_Cntrct_Adc_Acct_Actl_Amt() { return pnd_Cntrct_Adc_Acct_Actl_Amt; }

    public DbsField getPnd_Cntrct_Adc_Acct_Actl_Nbr_Units() { return pnd_Cntrct_Adc_Acct_Actl_Nbr_Units; }

    public DbsField getPnd_Cntrct_Adc_Acct_Cref_Rate_Cde() { return pnd_Cntrct_Adc_Acct_Cref_Rate_Cde; }

    public DbsField getPnd_Cntrct_Adc_Acct_Opn_Accum_Amt() { return pnd_Cntrct_Adc_Acct_Opn_Accum_Amt; }

    public DbsField getPnd_Cntrct_Adc_Acct_Opn_Nbr_Units() { return pnd_Cntrct_Adc_Acct_Opn_Nbr_Units; }

    public DbsGroup getPnd_Cntrct_Adc_Acct_Info() { return pnd_Cntrct_Adc_Acct_Info; }

    public DbsField getPnd_Cntrct_Adc_Acct_Aftr_Tax_Amt() { return pnd_Cntrct_Adc_Acct_Aftr_Tax_Amt; }

    public DbsGroup getPnd_Cntrct_Adc_Invstmt_Info() { return pnd_Cntrct_Adc_Invstmt_Info; }

    public DbsField getPnd_Cntrct_Adc_Invstmt_Id() { return pnd_Cntrct_Adc_Invstmt_Id; }

    public DbsField getPnd_Cntrct_Adc_Inv_Short_Name() { return pnd_Cntrct_Adc_Inv_Short_Name; }

    public DbsGroup getPnd_Cntrct_Adc_Dtl_Tiaa_Data() { return pnd_Cntrct_Adc_Dtl_Tiaa_Data; }

    public DbsField getPnd_Cntrct_Adc_Dtl_Tiaa_Rate_Cde() { return pnd_Cntrct_Adc_Dtl_Tiaa_Rate_Cde; }

    public DbsField getPnd_Cntrct_Adc_Dtl_Tiaa_Actl_Amt() { return pnd_Cntrct_Adc_Dtl_Tiaa_Actl_Amt; }

    public DbsField getPnd_Cntrct_Adc_Dtl_Tiaa_Opn_Accum_Amt() { return pnd_Cntrct_Adc_Dtl_Tiaa_Opn_Accum_Amt; }

    public DbsGroup getPnd_Cntrct_Adc_Store_Data() { return pnd_Cntrct_Adc_Store_Data; }

    public DbsField getPnd_Cntrct_Rqst_Id() { return pnd_Cntrct_Rqst_Id; }

    public DbsGroup getPnd_Cntrct_Rqst_IdRedef1() { return pnd_Cntrct_Rqst_IdRedef1; }

    public DbsField getPnd_Cntrct_Rqst_Pin() { return pnd_Cntrct_Rqst_Pin; }

    public DbsField getPnd_Cntrct_Rqst_Effctv_Dte() { return pnd_Cntrct_Rqst_Effctv_Dte; }

    public DbsField getPnd_Cntrct_Rqst_Entry_Dte() { return pnd_Cntrct_Rqst_Entry_Dte; }

    public DbsField getPnd_Cntrct_Rqst_Entry_Tme() { return pnd_Cntrct_Rqst_Entry_Tme; }

    public DbsField getPnd_Cntrct_Adc_Tiaa_Nbr() { return pnd_Cntrct_Adc_Tiaa_Nbr; }

    public DbsField getPnd_Cntrct_Adc_Cref_Nbr() { return pnd_Cntrct_Adc_Cref_Nbr; }

    public DbsField getPnd_Cntrct_Adc_Cntrct_Issue_State() { return pnd_Cntrct_Adc_Cntrct_Issue_State; }

    public DbsField getPnd_Cntrct_Adc_Cntrct_Issue_Dte() { return pnd_Cntrct_Adc_Cntrct_Issue_Dte; }

    public DbsField getPnd_Cntrct_Adc_Sttl_Ivc_Amt() { return pnd_Cntrct_Adc_Sttl_Ivc_Amt; }

    public DbsField getPnd_Cntrct_Adc_Grd_Mnthly_Typ() { return pnd_Cntrct_Adc_Grd_Mnthly_Typ; }

    public DbsField getPnd_Cntrct_Adc_Grd_Mnthly_Qty() { return pnd_Cntrct_Adc_Grd_Mnthly_Qty; }

    public DbsField getPnd_Cntrct_Adc_Sub_Plan_Nbr() { return pnd_Cntrct_Adc_Sub_Plan_Nbr; }

    public DbsGroup getPnd_Cntrct_Adc_Tpa_Io_Data() { return pnd_Cntrct_Adc_Tpa_Io_Data; }

    public DbsGroup getPnd_Cntrct_Adc_Tpa_Guarant_Commut_Info() { return pnd_Cntrct_Adc_Tpa_Guarant_Commut_Info; }

    public DbsField getPnd_Cntrct_Adc_Tpa_Guarant_Commut_Amt() { return pnd_Cntrct_Adc_Tpa_Guarant_Commut_Amt; }

    public DbsField getPnd_Cntrct_Adc_Tpa_Guarant_Commut_Grd_Amt() { return pnd_Cntrct_Adc_Tpa_Guarant_Commut_Grd_Amt; }

    public DbsField getPnd_Cntrct_Adc_Tpa_Guarant_Commut_Std_Amt() { return pnd_Cntrct_Adc_Tpa_Guarant_Commut_Std_Amt; }

    public DbsField getPnd_Cntrct_Adc_Orig_Lob_Ind() { return pnd_Cntrct_Adc_Orig_Lob_Ind; }

    public DbsField getPnd_Cntrct_Adc_Tpa_Paymnt_Cnt() { return pnd_Cntrct_Adc_Tpa_Paymnt_Cnt; }

    public DbsField getPnd_Cntrct_Adc_Contract_Id() { return pnd_Cntrct_Adc_Contract_Id; }

    public DbsGroup getPnd_Cntrct_Adc_Misc_Variables() { return pnd_Cntrct_Adc_Misc_Variables; }

    public DbsField getPnd_Cntrct_Fnd_Cnt() { return pnd_Cntrct_Fnd_Cnt; }

    public DbsField getPnd_Cntrct_Rte_Cnt() { return pnd_Cntrct_Rte_Cnt; }

    public DbsField getPnd_Cntrct_New_Cntrct() { return pnd_Cntrct_New_Cntrct; }

    public DbsField getPnd_Cntrct_Err_Report_Printed() { return pnd_Cntrct_Err_Report_Printed; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Prtcpnt = newGroupInRecord("pnd_Prtcpnt", "#PRTCPNT");
        pnd_Prtcpnt_Adp_Stts_Cde = pnd_Prtcpnt.newFieldInGroup("pnd_Prtcpnt_Adp_Stts_Cde", "ADP-STTS-CDE", FieldType.STRING, 3);
        pnd_Prtcpnt_Adp_Unique_Id = pnd_Prtcpnt.newFieldInGroup("pnd_Prtcpnt_Adp_Unique_Id", "ADP-UNIQUE-ID", FieldType.NUMERIC, 12);
        pnd_Prtcpnt_Adp_Annty_Strt_Dte = pnd_Prtcpnt.newFieldInGroup("pnd_Prtcpnt_Adp_Annty_Strt_Dte", "ADP-ANNTY-STRT-DTE", FieldType.DATE);
        pnd_Prtcpnt_Adp_Annt_Typ_Cde = pnd_Prtcpnt.newFieldInGroup("pnd_Prtcpnt_Adp_Annt_Typ_Cde", "ADP-ANNT-TYP-CDE", FieldType.STRING, 1);
        pnd_Prtcpnt_Adp_Entry_User_Id = pnd_Prtcpnt.newFieldInGroup("pnd_Prtcpnt_Adp_Entry_User_Id", "ADP-ENTRY-USER-ID", FieldType.STRING, 8);
        pnd_Prtcpnt_Adp_Rqst_Last_Updtd_Id = pnd_Prtcpnt.newFieldInGroup("pnd_Prtcpnt_Adp_Rqst_Last_Updtd_Id", "ADP-RQST-LAST-UPDTD-ID", FieldType.STRING, 
            7);
        pnd_Prtcpnt_Adp_Wpid = pnd_Prtcpnt.newFieldInGroup("pnd_Prtcpnt_Adp_Wpid", "ADP-WPID", FieldType.STRING, 6);
        pnd_Prtcpnt_Adp_Mit_Log_Dte_Tme = pnd_Prtcpnt.newFieldInGroup("pnd_Prtcpnt_Adp_Mit_Log_Dte_Tme", "ADP-MIT-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Prtcpnt_Adp_Bps_Unit = pnd_Prtcpnt.newFieldInGroup("pnd_Prtcpnt_Adp_Bps_Unit", "ADP-BPS-UNIT", FieldType.STRING, 8);
        pnd_Prtcpnt_Adp_Cntrcts_In_Rqst = pnd_Prtcpnt.newFieldArrayInGroup("pnd_Prtcpnt_Adp_Cntrcts_In_Rqst", "ADP-CNTRCTS-IN-RQST", FieldType.STRING, 
            10, new DbsArrayController(1,12));
        pnd_Prtcpnt_Adp_Isn = pnd_Prtcpnt.newFieldInGroup("pnd_Prtcpnt_Adp_Isn", "ADP-ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Prtcpnt_Adp_Cntrct_Isn = pnd_Prtcpnt.newFieldArrayInGroup("pnd_Prtcpnt_Adp_Cntrct_Isn", "ADP-CNTRCT-ISN", FieldType.PACKED_DECIMAL, 8, new 
            DbsArrayController(1,12));
        pnd_Prtcpnt_Omni_Cntrcts_In_Rqst = pnd_Prtcpnt.newFieldArrayInGroup("pnd_Prtcpnt_Omni_Cntrcts_In_Rqst", "OMNI-CNTRCTS-IN-RQST", FieldType.STRING, 
            10, new DbsArrayController(1,12));
        pnd_Prtcpnt_Adp_Rqst_Complete = pnd_Prtcpnt.newFieldInGroup("pnd_Prtcpnt_Adp_Rqst_Complete", "ADP-RQST-COMPLETE", FieldType.BOOLEAN);

        pnd_Cntrct = newGroupInRecord("pnd_Cntrct", "#CNTRCT");
        pnd_Cntrct_Adc_Stts_Cde = pnd_Cntrct.newFieldInGroup("pnd_Cntrct_Adc_Stts_Cde", "ADC-STTS-CDE", FieldType.STRING, 3);
        pnd_Cntrct_Adc_Lst_Actvty_Dte = pnd_Cntrct.newFieldInGroup("pnd_Cntrct_Adc_Lst_Actvty_Dte", "ADC-LST-ACTVTY-DTE", FieldType.DATE);
        pnd_Cntrct_Adc_Rqst_Info = pnd_Cntrct.newGroupArrayInGroup("pnd_Cntrct_Adc_Rqst_Info", "ADC-RQST-INFO", new DbsArrayController(1,20));
        pnd_Cntrct_Adc_Acct_Cde = pnd_Cntrct_Adc_Rqst_Info.newFieldInGroup("pnd_Cntrct_Adc_Acct_Cde", "ADC-ACCT-CDE", FieldType.STRING, 1);
        pnd_Cntrct_Adc_Tkr_Symbl = pnd_Cntrct_Adc_Rqst_Info.newFieldInGroup("pnd_Cntrct_Adc_Tkr_Symbl", "ADC-TKR-SYMBL", FieldType.STRING, 10);
        pnd_Cntrct_Adc_Acct_Stts_Cde = pnd_Cntrct_Adc_Rqst_Info.newFieldInGroup("pnd_Cntrct_Adc_Acct_Stts_Cde", "ADC-ACCT-STTS-CDE", FieldType.STRING, 
            3);
        pnd_Cntrct_Adc_Acct_Typ = pnd_Cntrct_Adc_Rqst_Info.newFieldInGroup("pnd_Cntrct_Adc_Acct_Typ", "ADC-ACCT-TYP", FieldType.STRING, 1);
        pnd_Cntrct_Adc_Acct_Qty = pnd_Cntrct_Adc_Rqst_Info.newFieldInGroup("pnd_Cntrct_Adc_Acct_Qty", "ADC-ACCT-QTY", FieldType.DECIMAL, 11,2);
        pnd_Cntrct_Adc_Acct_Actl_Amt = pnd_Cntrct_Adc_Rqst_Info.newFieldInGroup("pnd_Cntrct_Adc_Acct_Actl_Amt", "ADC-ACCT-ACTL-AMT", FieldType.DECIMAL, 
            11,2);
        pnd_Cntrct_Adc_Acct_Actl_Nbr_Units = pnd_Cntrct_Adc_Rqst_Info.newFieldInGroup("pnd_Cntrct_Adc_Acct_Actl_Nbr_Units", "ADC-ACCT-ACTL-NBR-UNITS", 
            FieldType.DECIMAL, 10,3);
        pnd_Cntrct_Adc_Acct_Cref_Rate_Cde = pnd_Cntrct_Adc_Rqst_Info.newFieldInGroup("pnd_Cntrct_Adc_Acct_Cref_Rate_Cde", "ADC-ACCT-CREF-RATE-CDE", FieldType.STRING, 
            2);
        pnd_Cntrct_Adc_Acct_Opn_Accum_Amt = pnd_Cntrct_Adc_Rqst_Info.newFieldInGroup("pnd_Cntrct_Adc_Acct_Opn_Accum_Amt", "ADC-ACCT-OPN-ACCUM-AMT", FieldType.DECIMAL, 
            11,2);
        pnd_Cntrct_Adc_Acct_Opn_Nbr_Units = pnd_Cntrct_Adc_Rqst_Info.newFieldInGroup("pnd_Cntrct_Adc_Acct_Opn_Nbr_Units", "ADC-ACCT-OPN-NBR-UNITS", FieldType.DECIMAL, 
            10,3);
        pnd_Cntrct_Adc_Acct_Info = pnd_Cntrct.newGroupArrayInGroup("pnd_Cntrct_Adc_Acct_Info", "ADC-ACCT-INFO", new DbsArrayController(1,20));
        pnd_Cntrct_Adc_Acct_Aftr_Tax_Amt = pnd_Cntrct_Adc_Acct_Info.newFieldInGroup("pnd_Cntrct_Adc_Acct_Aftr_Tax_Amt", "ADC-ACCT-AFTR-TAX-AMT", FieldType.DECIMAL, 
            11,2);
        pnd_Cntrct_Adc_Invstmt_Info = pnd_Cntrct.newGroupArrayInGroup("pnd_Cntrct_Adc_Invstmt_Info", "ADC-INVSTMT-INFO", new DbsArrayController(1,20));
        pnd_Cntrct_Adc_Invstmt_Id = pnd_Cntrct_Adc_Invstmt_Info.newFieldInGroup("pnd_Cntrct_Adc_Invstmt_Id", "ADC-INVSTMT-ID", FieldType.STRING, 2);
        pnd_Cntrct_Adc_Inv_Short_Name = pnd_Cntrct.newFieldArrayInGroup("pnd_Cntrct_Adc_Inv_Short_Name", "ADC-INV-SHORT-NAME", FieldType.STRING, 5, new 
            DbsArrayController(1,20));
        pnd_Cntrct_Adc_Dtl_Tiaa_Data = pnd_Cntrct.newGroupArrayInGroup("pnd_Cntrct_Adc_Dtl_Tiaa_Data", "ADC-DTL-TIAA-DATA", new DbsArrayController(1,250));
        pnd_Cntrct_Adc_Dtl_Tiaa_Rate_Cde = pnd_Cntrct_Adc_Dtl_Tiaa_Data.newFieldInGroup("pnd_Cntrct_Adc_Dtl_Tiaa_Rate_Cde", "ADC-DTL-TIAA-RATE-CDE", FieldType.STRING, 
            2);
        pnd_Cntrct_Adc_Dtl_Tiaa_Actl_Amt = pnd_Cntrct_Adc_Dtl_Tiaa_Data.newFieldInGroup("pnd_Cntrct_Adc_Dtl_Tiaa_Actl_Amt", "ADC-DTL-TIAA-ACTL-AMT", FieldType.DECIMAL, 
            11,2);
        pnd_Cntrct_Adc_Dtl_Tiaa_Opn_Accum_Amt = pnd_Cntrct_Adc_Dtl_Tiaa_Data.newFieldInGroup("pnd_Cntrct_Adc_Dtl_Tiaa_Opn_Accum_Amt", "ADC-DTL-TIAA-OPN-ACCUM-AMT", 
            FieldType.DECIMAL, 11,2);
        pnd_Cntrct_Adc_Store_Data = pnd_Cntrct.newGroupInGroup("pnd_Cntrct_Adc_Store_Data", "ADC-STORE-DATA");
        pnd_Cntrct_Rqst_Id = pnd_Cntrct_Adc_Store_Data.newFieldInGroup("pnd_Cntrct_Rqst_Id", "RQST-ID", FieldType.STRING, 35);
        pnd_Cntrct_Rqst_IdRedef1 = pnd_Cntrct_Adc_Store_Data.newGroupInGroup("pnd_Cntrct_Rqst_IdRedef1", "Redefines", pnd_Cntrct_Rqst_Id);
        pnd_Cntrct_Rqst_Pin = pnd_Cntrct_Rqst_IdRedef1.newFieldInGroup("pnd_Cntrct_Rqst_Pin", "RQST-PIN", FieldType.STRING, 12);
        pnd_Cntrct_Rqst_Effctv_Dte = pnd_Cntrct_Rqst_IdRedef1.newFieldInGroup("pnd_Cntrct_Rqst_Effctv_Dte", "RQST-EFFCTV-DTE", FieldType.STRING, 8);
        pnd_Cntrct_Rqst_Entry_Dte = pnd_Cntrct_Rqst_IdRedef1.newFieldInGroup("pnd_Cntrct_Rqst_Entry_Dte", "RQST-ENTRY-DTE", FieldType.STRING, 8);
        pnd_Cntrct_Rqst_Entry_Tme = pnd_Cntrct_Rqst_IdRedef1.newFieldInGroup("pnd_Cntrct_Rqst_Entry_Tme", "RQST-ENTRY-TME", FieldType.STRING, 7);
        pnd_Cntrct_Adc_Tiaa_Nbr = pnd_Cntrct_Adc_Store_Data.newFieldInGroup("pnd_Cntrct_Adc_Tiaa_Nbr", "ADC-TIAA-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Adc_Cref_Nbr = pnd_Cntrct_Adc_Store_Data.newFieldInGroup("pnd_Cntrct_Adc_Cref_Nbr", "ADC-CREF-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Adc_Cntrct_Issue_State = pnd_Cntrct_Adc_Store_Data.newFieldInGroup("pnd_Cntrct_Adc_Cntrct_Issue_State", "ADC-CNTRCT-ISSUE-STATE", FieldType.STRING, 
            2);
        pnd_Cntrct_Adc_Cntrct_Issue_Dte = pnd_Cntrct_Adc_Store_Data.newFieldInGroup("pnd_Cntrct_Adc_Cntrct_Issue_Dte", "ADC-CNTRCT-ISSUE-DTE", FieldType.DATE);
        pnd_Cntrct_Adc_Sttl_Ivc_Amt = pnd_Cntrct_Adc_Store_Data.newFieldInGroup("pnd_Cntrct_Adc_Sttl_Ivc_Amt", "ADC-STTL-IVC-AMT", FieldType.DECIMAL, 
            11,2);
        pnd_Cntrct_Adc_Grd_Mnthly_Typ = pnd_Cntrct_Adc_Store_Data.newFieldArrayInGroup("pnd_Cntrct_Adc_Grd_Mnthly_Typ", "ADC-GRD-MNTHLY-TYP", FieldType.STRING, 
            1, new DbsArrayController(1,20));
        pnd_Cntrct_Adc_Grd_Mnthly_Qty = pnd_Cntrct_Adc_Store_Data.newFieldArrayInGroup("pnd_Cntrct_Adc_Grd_Mnthly_Qty", "ADC-GRD-MNTHLY-QTY", FieldType.DECIMAL, 
            11,2, new DbsArrayController(1,20));
        pnd_Cntrct_Adc_Sub_Plan_Nbr = pnd_Cntrct_Adc_Store_Data.newFieldInGroup("pnd_Cntrct_Adc_Sub_Plan_Nbr", "ADC-SUB-PLAN-NBR", FieldType.STRING, 6);
        pnd_Cntrct_Adc_Tpa_Io_Data = pnd_Cntrct.newGroupInGroup("pnd_Cntrct_Adc_Tpa_Io_Data", "ADC-TPA-IO-DATA");
        pnd_Cntrct_Adc_Tpa_Guarant_Commut_Info = pnd_Cntrct_Adc_Tpa_Io_Data.newGroupArrayInGroup("pnd_Cntrct_Adc_Tpa_Guarant_Commut_Info", "ADC-TPA-GUARANT-COMMUT-INFO", 
            new DbsArrayController(1,250));
        pnd_Cntrct_Adc_Tpa_Guarant_Commut_Amt = pnd_Cntrct_Adc_Tpa_Guarant_Commut_Info.newFieldInGroup("pnd_Cntrct_Adc_Tpa_Guarant_Commut_Amt", "ADC-TPA-GUARANT-COMMUT-AMT", 
            FieldType.DECIMAL, 11,2);
        pnd_Cntrct_Adc_Tpa_Guarant_Commut_Grd_Amt = pnd_Cntrct_Adc_Tpa_Guarant_Commut_Info.newFieldInGroup("pnd_Cntrct_Adc_Tpa_Guarant_Commut_Grd_Amt", 
            "ADC-TPA-GUARANT-COMMUT-GRD-AMT", FieldType.DECIMAL, 11,2);
        pnd_Cntrct_Adc_Tpa_Guarant_Commut_Std_Amt = pnd_Cntrct_Adc_Tpa_Guarant_Commut_Info.newFieldInGroup("pnd_Cntrct_Adc_Tpa_Guarant_Commut_Std_Amt", 
            "ADC-TPA-GUARANT-COMMUT-STD-AMT", FieldType.DECIMAL, 11,2);
        pnd_Cntrct_Adc_Orig_Lob_Ind = pnd_Cntrct_Adc_Tpa_Io_Data.newFieldInGroup("pnd_Cntrct_Adc_Orig_Lob_Ind", "ADC-ORIG-LOB-IND", FieldType.STRING, 
            1);
        pnd_Cntrct_Adc_Tpa_Paymnt_Cnt = pnd_Cntrct_Adc_Tpa_Io_Data.newFieldInGroup("pnd_Cntrct_Adc_Tpa_Paymnt_Cnt", "ADC-TPA-PAYMNT-CNT", FieldType.NUMERIC, 
            2);
        pnd_Cntrct_Adc_Contract_Id = pnd_Cntrct.newFieldArrayInGroup("pnd_Cntrct_Adc_Contract_Id", "ADC-CONTRACT-ID", FieldType.NUMERIC, 11, new DbsArrayController(1,
            250));
        pnd_Cntrct_Adc_Misc_Variables = pnd_Cntrct.newGroupInGroup("pnd_Cntrct_Adc_Misc_Variables", "ADC-MISC-VARIABLES");
        pnd_Cntrct_Fnd_Cnt = pnd_Cntrct_Adc_Misc_Variables.newFieldInGroup("pnd_Cntrct_Fnd_Cnt", "FND-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Cntrct_Rte_Cnt = pnd_Cntrct_Adc_Misc_Variables.newFieldInGroup("pnd_Cntrct_Rte_Cnt", "RTE-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Cntrct_New_Cntrct = pnd_Cntrct_Adc_Misc_Variables.newFieldInGroup("pnd_Cntrct_New_Cntrct", "NEW-CNTRCT", FieldType.BOOLEAN);
        pnd_Cntrct_Err_Report_Printed = pnd_Cntrct_Adc_Misc_Variables.newFieldInGroup("pnd_Cntrct_Err_Report_Printed", "ERR-REPORT-PRINTED", FieldType.BOOLEAN);

        this.setRecordName("LdaAdsl400a");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAdsl400a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
