/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:49:23 PM
**        * FROM NATURAL LDA     : ADSL1033
************************************************************
**        * FILE NAME            : LdaAdsl1033.java
**        * CLASS NAME           : LdaAdsl1033
**        * INSTANCE NAME        : LdaAdsl1033
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdsl1033 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_A_Tiaa_Minor_Accum;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Pay_Cnt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Units;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Dollars;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Gtd_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Div_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Gtd_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Div_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Ivc_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Pay_Cnt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Units;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Dollars;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Gtd_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Div_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Gtd_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Div_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Ivc_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Pay_Cnt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Units;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Dollars;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Gtd_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Div_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Gtd_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Div_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Ivc_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Pay_Cnt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Units;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Dollars;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Gtd_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Div_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Gtd_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Div_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Ivc_Amt;
    private DbsGroup pnd_A_Tiaa_Sub_Accum;
    private DbsField pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Pay_Cnt;
    private DbsField pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Units;
    private DbsField pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Dollars;
    private DbsField pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Gtd_Amt;
    private DbsField pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Divid_Amt;
    private DbsField pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Gtd_Amt;
    private DbsField pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Div_Amt;
    private DbsField pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Ivc_Amt;
    private DbsGroup pnd_A_C_Mth_Minor_Accum;
    private DbsField pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt;
    private DbsField pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units;
    private DbsField pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars;
    private DbsField pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt;
    private DbsField pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt;
    private DbsField pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt;
    private DbsField pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt;
    private DbsGroup pnd_A_C_Mth_Fin_Ivc_Amount;
    private DbsField pnd_A_C_Mth_Fin_Ivc_Amount_Pnd_A_C_Mth_Fin_Ivc_Amt;
    private DbsGroup pnd_A_C_Ann_Minor_Accum;
    private DbsField pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt;
    private DbsField pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units;
    private DbsField pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars;
    private DbsField pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt;
    private DbsField pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt;
    private DbsField pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt;
    private DbsField pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt;
    private DbsGroup pnd_A_C_Ann_Fin_Ivc_Amount;
    private DbsField pnd_A_C_Ann_Fin_Ivc_Amount_Pnd_A_C_Ann_Fin_Ivc_Amt;
    private DbsGroup pnd_A_Cref_Mth_Sub_Total_Accum;
    private DbsField pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Pay_Cnt;
    private DbsField pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Units;
    private DbsField pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Dollars;
    private DbsField pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Gtd_Amt;
    private DbsField pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Div_Amt;
    private DbsField pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Gtd_Amt;
    private DbsField pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Div_Amt;
    private DbsField pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Ivc_Amt;
    private DbsGroup pnd_A_Cref_Ann_Sub_Total_Accum;
    private DbsField pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Pay_Cnt;
    private DbsField pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Units;
    private DbsField pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Dollars;
    private DbsField pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Gtd_Amt;
    private DbsField pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Div_Amt;
    private DbsField pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Gtd_Amt;
    private DbsField pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Div_Amt;
    private DbsField pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Ivc_Amt;
    private DbsGroup pnd_A_Total_Tiaa_Cref_Accum;
    private DbsField pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Pay_Cnt;
    private DbsField pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Units;
    private DbsField pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Dollars;
    private DbsField pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Gtd_Amt;
    private DbsField pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Div_Amt;
    private DbsField pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Gtd_Amt;
    private DbsField pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Div_Amt;
    private DbsField pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Ivc_Amt;

    public DbsGroup getPnd_A_Tiaa_Minor_Accum() { return pnd_A_Tiaa_Minor_Accum; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Pay_Cnt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Pay_Cnt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Units() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Units; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Dollars() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Dollars; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Gtd_Amt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Gtd_Amt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Div_Amt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Div_Amt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Gtd_Amt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Gtd_Amt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Div_Amt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Div_Amt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Ivc_Amt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Ivc_Amt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Pay_Cnt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Pay_Cnt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Units() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Units; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Dollars() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Dollars; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Gtd_Amt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Gtd_Amt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Div_Amt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Div_Amt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Gtd_Amt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Gtd_Amt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Div_Amt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Div_Amt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Ivc_Amt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Ivc_Amt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Pay_Cnt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Pay_Cnt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Units() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Units; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Dollars() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Dollars; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Gtd_Amt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Gtd_Amt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Div_Amt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Div_Amt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Gtd_Amt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Gtd_Amt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Div_Amt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Div_Amt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Ivc_Amt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Ivc_Amt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Pay_Cnt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Pay_Cnt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Units() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Units; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Dollars() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Dollars; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Gtd_Amt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Gtd_Amt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Div_Amt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Div_Amt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Gtd_Amt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Gtd_Amt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Div_Amt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Div_Amt; }

    public DbsField getPnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Ivc_Amt() { return pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Ivc_Amt; }

    public DbsGroup getPnd_A_Tiaa_Sub_Accum() { return pnd_A_Tiaa_Sub_Accum; }

    public DbsField getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Pay_Cnt() { return pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Pay_Cnt; }

    public DbsField getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Units() { return pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Units; }

    public DbsField getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Dollars() { return pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Dollars; }

    public DbsField getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Gtd_Amt() { return pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Gtd_Amt; }

    public DbsField getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Divid_Amt() { return pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Divid_Amt; }

    public DbsField getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Gtd_Amt() { return pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Gtd_Amt; }

    public DbsField getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Div_Amt() { return pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Div_Amt; }

    public DbsField getPnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Ivc_Amt() { return pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Ivc_Amt; }

    public DbsGroup getPnd_A_C_Mth_Minor_Accum() { return pnd_A_C_Mth_Minor_Accum; }

    public DbsField getPnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt() { return pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt; }

    public DbsField getPnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units() { return pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units; }

    public DbsField getPnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars() { return pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars; }

    public DbsField getPnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt() { return pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt; }

    public DbsField getPnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt() { return pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt; }

    public DbsField getPnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt() { return pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt; }

    public DbsField getPnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt() { return pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt; }

    public DbsGroup getPnd_A_C_Mth_Fin_Ivc_Amount() { return pnd_A_C_Mth_Fin_Ivc_Amount; }

    public DbsField getPnd_A_C_Mth_Fin_Ivc_Amount_Pnd_A_C_Mth_Fin_Ivc_Amt() { return pnd_A_C_Mth_Fin_Ivc_Amount_Pnd_A_C_Mth_Fin_Ivc_Amt; }

    public DbsGroup getPnd_A_C_Ann_Minor_Accum() { return pnd_A_C_Ann_Minor_Accum; }

    public DbsField getPnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt() { return pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt; }

    public DbsField getPnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units() { return pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units; }

    public DbsField getPnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars() { return pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars; }

    public DbsField getPnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt() { return pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt; }

    public DbsField getPnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt() { return pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt; }

    public DbsField getPnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt() { return pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt; }

    public DbsField getPnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt() { return pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt; }

    public DbsGroup getPnd_A_C_Ann_Fin_Ivc_Amount() { return pnd_A_C_Ann_Fin_Ivc_Amount; }

    public DbsField getPnd_A_C_Ann_Fin_Ivc_Amount_Pnd_A_C_Ann_Fin_Ivc_Amt() { return pnd_A_C_Ann_Fin_Ivc_Amount_Pnd_A_C_Ann_Fin_Ivc_Amt; }

    public DbsGroup getPnd_A_Cref_Mth_Sub_Total_Accum() { return pnd_A_Cref_Mth_Sub_Total_Accum; }

    public DbsField getPnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Pay_Cnt() { return pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Pay_Cnt; 
        }

    public DbsField getPnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Units() { return pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Units; }

    public DbsField getPnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Dollars() { return pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Dollars; 
        }

    public DbsField getPnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Gtd_Amt() { return pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Gtd_Amt; 
        }

    public DbsField getPnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Div_Amt() { return pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Div_Amt; 
        }

    public DbsField getPnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Gtd_Amt() { return pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Gtd_Amt; 
        }

    public DbsField getPnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Div_Amt() { return pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Div_Amt; 
        }

    public DbsField getPnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Ivc_Amt() { return pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Ivc_Amt; 
        }

    public DbsGroup getPnd_A_Cref_Ann_Sub_Total_Accum() { return pnd_A_Cref_Ann_Sub_Total_Accum; }

    public DbsField getPnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Pay_Cnt() { return pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Pay_Cnt; 
        }

    public DbsField getPnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Units() { return pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Units; }

    public DbsField getPnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Dollars() { return pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Dollars; 
        }

    public DbsField getPnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Gtd_Amt() { return pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Gtd_Amt; 
        }

    public DbsField getPnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Div_Amt() { return pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Div_Amt; 
        }

    public DbsField getPnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Gtd_Amt() { return pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Gtd_Amt; 
        }

    public DbsField getPnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Div_Amt() { return pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Div_Amt; 
        }

    public DbsField getPnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Ivc_Amt() { return pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Ivc_Amt; 
        }

    public DbsGroup getPnd_A_Total_Tiaa_Cref_Accum() { return pnd_A_Total_Tiaa_Cref_Accum; }

    public DbsField getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Pay_Cnt() { return pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Pay_Cnt; }

    public DbsField getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Units() { return pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Units; }

    public DbsField getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Dollars() { return pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Dollars; }

    public DbsField getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Gtd_Amt() { return pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Gtd_Amt; }

    public DbsField getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Div_Amt() { return pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Div_Amt; }

    public DbsField getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Gtd_Amt() { return pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Gtd_Amt; }

    public DbsField getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Div_Amt() { return pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Div_Amt; }

    public DbsField getPnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Ivc_Amt() { return pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Ivc_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_A_Tiaa_Minor_Accum = newGroupInRecord("pnd_A_Tiaa_Minor_Accum", "#A-TIAA-MINOR-ACCUM");
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Pay_Cnt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Pay_Cnt", "#A-TIAA-STD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Units = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Units", "#A-TIAA-STD-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Dollars = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Dollars", "#A-TIAA-STD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Gtd_Amt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Gtd_Amt", "#A-TIAA-STD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Div_Amt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Div_Amt", "#A-TIAA-STD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Gtd_Amt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Gtd_Amt", 
            "#A-TIAA-STD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Div_Amt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Div_Amt", 
            "#A-TIAA-STD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Ivc_Amt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Ivc_Amt", 
            "#A-TIAA-STD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Pay_Cnt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Pay_Cnt", "#A-TIAA-GRD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Units = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Units", "#A-TIAA-GRD-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Dollars = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Dollars", "#A-TIAA-GRD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Gtd_Amt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Gtd_Amt", "#A-TIAA-GRD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Div_Amt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Div_Amt", "#A-TIAA-GRD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Gtd_Amt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Gtd_Amt", 
            "#A-TIAA-GRD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Div_Amt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Div_Amt", 
            "#A-TIAA-GRD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Ivc_Amt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Ivc_Amt", 
            "#A-TIAA-GRD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Pay_Cnt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Pay_Cnt", 
            "#A-TIAA-TPA-STD-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Units = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Units", "#A-TIAA-TPA-STD-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Dollars = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Dollars", 
            "#A-TIAA-TPA-STD-DOLLARS", FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Gtd_Amt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Gtd_Amt", 
            "#A-TIAA-TPA-STD-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Div_Amt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Div_Amt", 
            "#A-TIAA-TPA-STD-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Gtd_Amt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Gtd_Amt", 
            "#A-TIAA-TPA-STD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Div_Amt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Div_Amt", 
            "#A-TIAA-TPA-STD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Ivc_Amt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Ivc_Amt", 
            "#A-TIAA-TPA-STD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Pay_Cnt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Pay_Cnt", 
            "#A-TIAA-IPRO-STD-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Units = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Units", 
            "#A-TIAA-IPRO-STD-UNITS", FieldType.PACKED_DECIMAL, 10,4);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Dollars = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Dollars", 
            "#A-TIAA-IPRO-STD-DOLLARS", FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Gtd_Amt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Gtd_Amt", 
            "#A-TIAA-IPRO-STD-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Div_Amt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Div_Amt", 
            "#A-TIAA-IPRO-STD-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Gtd_Amt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Gtd_Amt", 
            "#A-TIAA-IPRO-STD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Div_Amt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Div_Amt", 
            "#A-TIAA-IPRO-STD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Ivc_Amt = pnd_A_Tiaa_Minor_Accum.newFieldInGroup("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Ivc_Amt", 
            "#A-TIAA-IPRO-STD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_A_Tiaa_Sub_Accum = newGroupInRecord("pnd_A_Tiaa_Sub_Accum", "#A-TIAA-SUB-ACCUM");
        pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Pay_Cnt = pnd_A_Tiaa_Sub_Accum.newFieldInGroup("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Pay_Cnt", "#A-TIAA-SUB-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Units = pnd_A_Tiaa_Sub_Accum.newFieldInGroup("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Units", "#A-TIAA-SUB-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Dollars = pnd_A_Tiaa_Sub_Accum.newFieldInGroup("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Dollars", "#A-TIAA-SUB-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Gtd_Amt = pnd_A_Tiaa_Sub_Accum.newFieldInGroup("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Gtd_Amt", "#A-TIAA-SUB-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Divid_Amt = pnd_A_Tiaa_Sub_Accum.newFieldInGroup("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Divid_Amt", "#A-TIAA-SUB-DIVID-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Gtd_Amt = pnd_A_Tiaa_Sub_Accum.newFieldInGroup("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Gtd_Amt", "#A-TIAA-SUB-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Div_Amt = pnd_A_Tiaa_Sub_Accum.newFieldInGroup("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Div_Amt", "#A-TIAA-SUB-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Ivc_Amt = pnd_A_Tiaa_Sub_Accum.newFieldInGroup("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Ivc_Amt", "#A-TIAA-SUB-FIN-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);

        pnd_A_C_Mth_Minor_Accum = newGroupArrayInRecord("pnd_A_C_Mth_Minor_Accum", "#A-C-MTH-MINOR-ACCUM", new DbsArrayController(1,20));
        pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt = pnd_A_C_Mth_Minor_Accum.newFieldInGroup("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt", "#A-C-MTH-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units = pnd_A_C_Mth_Minor_Accum.newFieldInGroup("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units", "#A-C-MTH-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars = pnd_A_C_Mth_Minor_Accum.newFieldInGroup("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars", "#A-C-MTH-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt = pnd_A_C_Mth_Minor_Accum.newFieldInGroup("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt", "#A-C-MTH-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt = pnd_A_C_Mth_Minor_Accum.newFieldInGroup("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt", "#A-C-MTH-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt = pnd_A_C_Mth_Minor_Accum.newFieldInGroup("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt", "#A-C-MTH-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt = pnd_A_C_Mth_Minor_Accum.newFieldInGroup("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt", "#A-C-MTH-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);

        pnd_A_C_Mth_Fin_Ivc_Amount = newGroupInRecord("pnd_A_C_Mth_Fin_Ivc_Amount", "#A-C-MTH-FIN-IVC-AMOUNT");
        pnd_A_C_Mth_Fin_Ivc_Amount_Pnd_A_C_Mth_Fin_Ivc_Amt = pnd_A_C_Mth_Fin_Ivc_Amount.newFieldInGroup("pnd_A_C_Mth_Fin_Ivc_Amount_Pnd_A_C_Mth_Fin_Ivc_Amt", 
            "#A-C-MTH-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_A_C_Ann_Minor_Accum = newGroupArrayInRecord("pnd_A_C_Ann_Minor_Accum", "#A-C-ANN-MINOR-ACCUM", new DbsArrayController(1,20));
        pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt = pnd_A_C_Ann_Minor_Accum.newFieldInGroup("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt", "#A-C-ANN-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units = pnd_A_C_Ann_Minor_Accum.newFieldInGroup("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units", "#A-C-ANN-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars = pnd_A_C_Ann_Minor_Accum.newFieldInGroup("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars", "#A-C-ANN-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt = pnd_A_C_Ann_Minor_Accum.newFieldInGroup("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt", "#A-C-ANN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt = pnd_A_C_Ann_Minor_Accum.newFieldInGroup("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt", "#A-C-ANN-DIVID-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt = pnd_A_C_Ann_Minor_Accum.newFieldInGroup("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt", "#A-C-ANN-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt = pnd_A_C_Ann_Minor_Accum.newFieldInGroup("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt", "#A-C-ANN-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);

        pnd_A_C_Ann_Fin_Ivc_Amount = newGroupInRecord("pnd_A_C_Ann_Fin_Ivc_Amount", "#A-C-ANN-FIN-IVC-AMOUNT");
        pnd_A_C_Ann_Fin_Ivc_Amount_Pnd_A_C_Ann_Fin_Ivc_Amt = pnd_A_C_Ann_Fin_Ivc_Amount.newFieldInGroup("pnd_A_C_Ann_Fin_Ivc_Amount_Pnd_A_C_Ann_Fin_Ivc_Amt", 
            "#A-C-ANN-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_A_Cref_Mth_Sub_Total_Accum = newGroupInRecord("pnd_A_Cref_Mth_Sub_Total_Accum", "#A-CREF-MTH-SUB-TOTAL-ACCUM");
        pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Pay_Cnt = pnd_A_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Pay_Cnt", 
            "#A-CREF-MTH-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Units = pnd_A_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Units", 
            "#A-CREF-MTH-SUB-UNITS", FieldType.PACKED_DECIMAL, 10,4);
        pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Dollars = pnd_A_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Dollars", 
            "#A-CREF-MTH-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 12,2);
        pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Gtd_Amt = pnd_A_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Gtd_Amt", 
            "#A-CREF-MTH-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Div_Amt = pnd_A_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Div_Amt", 
            "#A-CREF-MTH-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Gtd_Amt = pnd_A_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Gtd_Amt", 
            "#A-CREF-MTH-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Div_Amt = pnd_A_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Div_Amt", 
            "#A-CREF-MTH-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Ivc_Amt = pnd_A_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Ivc_Amt", 
            "#A-CREF-MTH-SUB-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_A_Cref_Ann_Sub_Total_Accum = newGroupInRecord("pnd_A_Cref_Ann_Sub_Total_Accum", "#A-CREF-ANN-SUB-TOTAL-ACCUM");
        pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Pay_Cnt = pnd_A_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Pay_Cnt", 
            "#A-CREF-ANN-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Units = pnd_A_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Units", 
            "#A-CREF-ANN-SUB-UNITS", FieldType.PACKED_DECIMAL, 10,4);
        pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Dollars = pnd_A_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Dollars", 
            "#A-CREF-ANN-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 12,2);
        pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Gtd_Amt = pnd_A_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Gtd_Amt", 
            "#A-CREF-ANN-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Div_Amt = pnd_A_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Div_Amt", 
            "#A-CREF-ANN-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Gtd_Amt = pnd_A_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Gtd_Amt", 
            "#A-CREF-ANN-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Div_Amt = pnd_A_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Div_Amt", 
            "#A-CREF-ANN-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Ivc_Amt = pnd_A_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Ivc_Amt", 
            "#A-CREF-ANN-SUB-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_A_Total_Tiaa_Cref_Accum = newGroupInRecord("pnd_A_Total_Tiaa_Cref_Accum", "#A-TOTAL-TIAA-CREF-ACCUM");
        pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Pay_Cnt = pnd_A_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Pay_Cnt", 
            "#A-TOTAL-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Units = pnd_A_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Units", "#A-TOTAL-UNITS", 
            FieldType.PACKED_DECIMAL, 13,4);
        pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Dollars = pnd_A_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Dollars", 
            "#A-TOTAL-DOLLARS", FieldType.PACKED_DECIMAL, 14,2);
        pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Gtd_Amt = pnd_A_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Gtd_Amt", 
            "#A-TOTAL-GTD-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Div_Amt = pnd_A_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Div_Amt", 
            "#A-TOTAL-DIV-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Gtd_Amt = pnd_A_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Gtd_Amt", 
            "#A-TOTAL-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Div_Amt = pnd_A_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Div_Amt", 
            "#A-TOTAL-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Ivc_Amt = pnd_A_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Ivc_Amt", 
            "#A-TOTAL-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 14,2);

        this.setRecordName("LdaAdsl1033");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAdsl1033() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
