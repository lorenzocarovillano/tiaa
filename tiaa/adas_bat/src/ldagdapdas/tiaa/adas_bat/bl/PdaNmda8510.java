/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:22:30 PM
**        * FROM NATURAL PDA     : NMDA8510
************************************************************
**        * FILE NAME            : PdaNmda8510.java
**        * CLASS NAME           : PdaNmda8510
**        * INSTANCE NAME        : PdaNmda8510
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaNmda8510 extends PdaBase
{
    // Properties
    private DbsGroup nmda8510;
    private DbsField nmda8510_Pnd_Pin_Nbr;
    private DbsField nmda8510_Pnd_Ssn;
    private DbsField nmda8510_Pnd_Prtcpnt_Date_Of_Birth;
    private DbsField nmda8510_Pnd_Prtcpnt_Date_Of_Death;
    private DbsField nmda8510_Pnd_Prtcpnt_Sex;

    public DbsGroup getNmda8510() { return nmda8510; }

    public DbsField getNmda8510_Pnd_Pin_Nbr() { return nmda8510_Pnd_Pin_Nbr; }

    public DbsField getNmda8510_Pnd_Ssn() { return nmda8510_Pnd_Ssn; }

    public DbsField getNmda8510_Pnd_Prtcpnt_Date_Of_Birth() { return nmda8510_Pnd_Prtcpnt_Date_Of_Birth; }

    public DbsField getNmda8510_Pnd_Prtcpnt_Date_Of_Death() { return nmda8510_Pnd_Prtcpnt_Date_Of_Death; }

    public DbsField getNmda8510_Pnd_Prtcpnt_Sex() { return nmda8510_Pnd_Prtcpnt_Sex; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        nmda8510 = dbsRecord.newGroupInRecord("nmda8510", "NMDA8510");
        nmda8510.setParameterOption(ParameterOption.ByReference);
        nmda8510_Pnd_Pin_Nbr = nmda8510.newFieldInGroup("nmda8510_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 7);
        nmda8510_Pnd_Ssn = nmda8510.newFieldInGroup("nmda8510_Pnd_Ssn", "#SSN", FieldType.NUMERIC, 9);
        nmda8510_Pnd_Prtcpnt_Date_Of_Birth = nmda8510.newFieldInGroup("nmda8510_Pnd_Prtcpnt_Date_Of_Birth", "#PRTCPNT-DATE-OF-BIRTH", FieldType.NUMERIC, 
            8);
        nmda8510_Pnd_Prtcpnt_Date_Of_Death = nmda8510.newFieldInGroup("nmda8510_Pnd_Prtcpnt_Date_Of_Death", "#PRTCPNT-DATE-OF-DEATH", FieldType.NUMERIC, 
            8);
        nmda8510_Pnd_Prtcpnt_Sex = nmda8510.newFieldInGroup("nmda8510_Pnd_Prtcpnt_Sex", "#PRTCPNT-SEX", FieldType.STRING, 1);

        dbsRecord.reset();
    }

    // Constructors
    public PdaNmda8510(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

