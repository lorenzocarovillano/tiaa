/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:17:35 PM
**        * FROM NATURAL PDA     : ADSA401
************************************************************
**        * FILE NAME            : PdaAdsa401.java
**        * CLASS NAME           : PdaAdsa401
**        * INSTANCE NAME        : PdaAdsa401
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaAdsa401 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Adsa401;
    private DbsGroup pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd;
    private DbsField pnd_Adsa401_Rqst_Id;
    private DbsGroup pnd_Adsa401_Adp_Ia_Data;
    private DbsField pnd_Adsa401_Adp_Ia_Tiaa_Nbr;
    private DbsField pnd_Adsa401_Adp_Ia_Tiaa_Rea_Nbr;
    private DbsField pnd_Adsa401_Adp_Ia_Tiaa_Payee_Cd;
    private DbsField pnd_Adsa401_Adp_Ia_Cref_Nbr;
    private DbsField pnd_Adsa401_Adp_Ia_Cref_Payee_Cd;
    private DbsField pnd_Adsa401_Adp_Bps_Unit;
    private DbsField pnd_Adsa401_Adp_Annt_Typ_Cde;
    private DbsField pnd_Adsa401_Adp_Emplymnt_Term_Dte;
    private DbsField pnd_Adsa401_Adp_Cntrcts_In_Rqst;
    private DbsField pnd_Adsa401_Adp_Unique_Id;
    private DbsField pnd_Adsa401_Adp_Effctv_Dte;
    private DbsField pnd_Adsa401_Adp_Cntr_Prt_Issue_Dte;
    private DbsField pnd_Adsa401_Adp_Entry_Dte;
    private DbsField pnd_Adsa401_Adp_Entry_Tme;
    private DbsField pnd_Adsa401_Adp_Entry_User_Id;
    private DbsField pnd_Adsa401_Adp_Rep_Nme;
    private DbsField pnd_Adsa401_Adp_Lst_Actvty_Dte;
    private DbsField pnd_Adsa401_Adp_Wpid;
    private DbsField pnd_Adsa401_Adp_Mit_Log_Dte_Tme;
    private DbsField pnd_Adsa401_Adp_Opn_Clsd_Ind;
    private DbsField pnd_Adsa401_Adp_In_Prgrss_Ind;
    private DbsField pnd_Adsa401_Adp_Rcprcl_Dte;
    private DbsField pnd_Adsa401_Adp_Stts_Cde;
    private DbsGroup pnd_Adsa401_Adp_Crrspndce_Mlng_Dta;
    private DbsField pnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Txt;
    private DbsField pnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Zip;
    private DbsField pnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Typ_Cd;
    private DbsField pnd_Adsa401_Adp_Frst_Annt_Ctznshp;
    private DbsField pnd_Adsa401_Adp_Frst_Annt_Rsdnc_Cde;
    private DbsField pnd_Adsa401_Adp_Frst_Annt_Ssn;
    private DbsField pnd_Adsa401_Adp_Frst_Annt_Title_Cde;
    private DbsField pnd_Adsa401_Adp_Frst_Annt_Frst_Nme;
    private DbsField pnd_Adsa401_Adp_Frst_Annt_Mid_Nme;
    private DbsField pnd_Adsa401_Adp_Frst_Annt_Lst_Nme;
    private DbsField pnd_Adsa401_Adp_Frst_Annt_Sfx;
    private DbsField pnd_Adsa401_Adp_Frst_Annt_Dte_Of_Brth;
    private DbsField pnd_Adsa401_Adp_Frst_Annt_Sex_Cde;
    private DbsField pnd_Adsa401_Adp_Scnd_Annt_Dte_Of_Brth;
    private DbsField pnd_Adsa401_Adp_Scnd_Annt_Ssn;
    private DbsField pnd_Adsa401_Adp_Scnd_Annt_Sex_Cde;
    private DbsField pnd_Adsa401_Adp_Annty_Optn;
    private DbsField pnd_Adsa401_Adp_Grntee_Period;
    private DbsField pnd_Adsa401_Adp_Grntee_Period_Mnths;
    private DbsField pnd_Adsa401_Adp_Annty_Strt_Dte;
    private DbsField pnd_Adsa401_Adp_Pymnt_Mode;
    private DbsField pnd_Adsa401_Adp_Settl_All_Tiaa_Grd_Pct;
    private DbsField pnd_Adsa401_Adp_Settl_All_Cref_Mon_Pct;
    private DbsField pnd_Adsa401_Adp_Alt_Dest_Addr_Ind;
    private DbsField pnd_Adsa401_Adp_Spcfc_Brkdwn_Ind;
    private DbsGroup pnd_Adsa401_Adp_Alt_Dest_Data;
    private DbsField pnd_Adsa401_Adp_Alt_Payee_Ind;
    private DbsField pnd_Adsa401_Adp_Alt_Carrier_Cde;
    private DbsField pnd_Adsa401_Adp_Alt_Dest_Addr_Txt;
    private DbsField pnd_Adsa401_Adp_Alt_Dest_Addr_Zip;
    private DbsField pnd_Adsa401_Adp_Alt_Dest_Addr_Typ_Cde;
    private DbsField pnd_Adsa401_Adp_Alt_Dest_Acct_Nbr;
    private DbsField pnd_Adsa401_Adp_Alt_Dest_Acct_Typ;
    private DbsField pnd_Adsa401_Adp_Alt_Dest_Trnst_Cde;
    private DbsField pnd_Adsa401_Adp_Alt_Dest_Hold_Cde;
    private DbsField pnd_Adsa401_Adp_Alt_Dest_Rlvr_Ind;
    private DbsField pnd_Adsa401_Adp_Alt_Dest_Rlvr_Dest;
    private DbsField pnd_Adsa401_Adp_Sbjct_To_Wvr_Ind;
    private DbsField pnd_Adsa401_Adp_Sbjct_To_Spsl_Cnsnt;
    private DbsGroup pnd_Adsa401_Adp_Forms_Data;
    private DbsField pnd_Adsa401_Adp_Forms_Rcvd_Dte;
    private DbsGroup pnd_Adsa401_Adp_Audit_Trail_Dta;
    private DbsField pnd_Adsa401_Adp_Rqst_Last_Updtd_Id;
    private DbsField pnd_Adsa401_Adp_Rqst_Last_Updtd_Dte;
    private DbsField pnd_Adsa401_Adp_Rsttlmnt_Cnt;
    private DbsField pnd_Adsa401_Adp_State_Of_Issue;
    private DbsField pnd_Adsa401_Adp_Orgnl_Issue_State;
    private DbsField pnd_Adsa401_Adp_Ivc_Rlvr_Cde;
    private DbsField pnd_Adsa401_Adp_Ivc_Pymnt_Rule;
    private DbsField pnd_Adsa401_Adp_Plan_Nbr;
    private DbsGroup pnd_Adsa401_Adp_Frm_Irc_Dta;
    private DbsField pnd_Adsa401_Adp_Frm_Irc_Cde;
    private DbsField pnd_Adsa401_Adp_To_Irc_Cde;
    private DbsField pnd_Adsa401_Adp_Irc_Cde;
    private DbsField pnd_Adsa401_Adp_Roth_Plan_Type;
    private DbsField pnd_Adsa401_Adp_Roth_Frst_Cntrbtn_Dte;
    private DbsField pnd_Adsa401_Adp_Roth_Dsblty_Dte;
    private DbsField pnd_Adsa401_Adp_Roth_Rqst_Ind;
    private DbsField pnd_Adsa401_Adp_Srvvr_Ind;
    private DbsField pnd_Adsa401_Adp_Sole_Prmry_Bene_Rltnshp;
    private DbsField pnd_Adsa401_Adp_Sole_Prmry_Bene_Dte_Of_Brth;
    private DbsField pnd_Adsa401_Adp_Plan_Nme;
    private DbsGroup pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd;
    private DbsField pnd_Adsa401_Adc_Rqst_Id;
    private DbsField pnd_Adsa401_Adc_Sqnce_Nbr;
    private DbsField pnd_Adsa401_Adc_Stts_Cde;
    private DbsField pnd_Adsa401_Adc_Lst_Actvty_Dte;
    private DbsField pnd_Adsa401_Adc_Tiaa_Nbr;
    private DbsField pnd_Adsa401_Adc_Cref_Nbr;
    private DbsField pnd_Adsa401_Adc_Gd_Cntrct_Ind;
    private DbsField pnd_Adsa401_Adc_Tiaa_Ivc_Gd_Ind;
    private DbsField pnd_Adsa401_Adc_Cref_Ivc_Gd_Ind;
    private DbsField pnd_Adsa401_Adc_Cntrct_Issue_State;
    private DbsField pnd_Adsa401_Adc_Cntrct_Issue_Dte;
    private DbsField pnd_Adsa401_Adc_Hold_Cde;
    private DbsGroup pnd_Adsa401_Adc_Ppg_Data;
    private DbsField pnd_Adsa401_Adc_Ppg_Cde;
    private DbsField pnd_Adsa401_Adc_Qlfd_Non_Qlfd_Ind;
    private DbsField pnd_Adsa401_Adc_Ppg_Trgtd;
    private DbsField pnd_Adsa401_Adc_Ppg_Rule_Assoc;
    private DbsGroup pnd_Adsa401_Adc_Rqst_Info;
    private DbsField pnd_Adsa401_Adc_Acct_Cde;
    private DbsField pnd_Adsa401_Adc_Tkr_Symbl;
    private DbsField pnd_Adsa401_Adc_Acct_Stts_Cde;
    private DbsField pnd_Adsa401_Adc_Acct_Typ;
    private DbsField pnd_Adsa401_Adc_Acct_Qty;
    private DbsField pnd_Adsa401_Adc_Acct_Actl_Amt;
    private DbsField pnd_Adsa401_Adc_Acct_Actl_Nbr_Units;
    private DbsField pnd_Adsa401_Adc_Grd_Mnthly_Typ;
    private DbsField pnd_Adsa401_Adc_Grd_Mnthly_Qty;
    private DbsField pnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt;
    private DbsField pnd_Adsa401_Adc_Stndrd_Annl_Actl_Amt;
    private DbsField pnd_Adsa401_Adc_Acct_Cref_Rate_Cde;
    private DbsField pnd_Adsa401_Adc_Ivc_Amt;
    private DbsField pnd_Adsa401_Adc_Acct_Opn_Accum_Amt;
    private DbsField pnd_Adsa401_Adc_Acct_Opn_Nbr_Units;
    private DbsGroup pnd_Adsa401_Adc_Err_Rpt_Data;
    private DbsField pnd_Adsa401_Adc_Err_Cde;
    private DbsField pnd_Adsa401_Adc_Err_Acct_Cde;
    private DbsField pnd_Adsa401_Adc_Err_Msg;
    private DbsField pnd_Adsa401_Adc_Trk_Ro_Ivc;
    private DbsGroup pnd_Adsa401_Adc_Dtl_Tiaa_Data;
    private DbsField pnd_Adsa401_Adc_Dtl_Tiaa_Rate_Cde;
    private DbsField pnd_Adsa401_Adc_Dtl_Tiaa_Actl_Amt;
    private DbsField pnd_Adsa401_Adc_Dtl_Tiaa_Grd_Actl_Amt;
    private DbsField pnd_Adsa401_Adc_Dtl_Tiaa_Stndrd_Actl_Amt;
    private DbsField pnd_Adsa401_Adc_Dtl_Tiaa_Ia_Rate_Cde;
    private DbsField pnd_Adsa401_Adc_Dtl_Tiaa_Opn_Accum_Amt;
    private DbsGroup pnd_Adsa401_Adc_Acct_Info;
    private DbsField pnd_Adsa401_Adc_Acct_Aftr_Tax_Amt;
    private DbsField pnd_Adsa401_Adc_Sttl_Ivc_Amt;
    private DbsField pnd_Adsa401_Adc_Sub_Plan_Nbr;
    private DbsGroup pnd_Adsa401_Adc_Invstmt_Info;
    private DbsField pnd_Adsa401_Adc_Invstmt_Id;
    private DbsGroup pnd_Adsa401_Adc_Tpa_Guarant_Commut_Info;
    private DbsField pnd_Adsa401_Adc_Tpa_Guarant_Commut_Amt;
    private DbsField pnd_Adsa401_Adc_Tpa_Guarant_Commut_Grd_Amt;
    private DbsField pnd_Adsa401_Adc_Tpa_Guarant_Commut_Std_Amt;
    private DbsField pnd_Adsa401_Adc_Orig_Lob_Ind;
    private DbsField pnd_Adsa401_Adc_Tpa_Paymnt_Cnt;
    private DbsField pnd_Adsa401_Adc_T395_Ticker;
    private DbsField pnd_Adsa401_Adc_T395_Fund_Id;
    private DbsField pnd_Adsa401_Adc_Inv_Short_Name;
    private DbsField pnd_Adsa401_Adc_Contract_Id;
    private DbsField pnd_Adsa401_Adc_Invstmnt_Grpng_Id;
    private DbsGroup pnd_Adsa401_Pnd_Adsa401_Static_Variables;
    private DbsField pnd_Adsa401_Pnd_Cics_System_Name;
    private DbsField pnd_Adsa401_Pnd_Fund_Cnt;
    private DbsField pnd_Adsa401_Pnd_Tiaa_Rate_Cnt;
    private DbsField pnd_Adsa401_Pnd_Cntl_Bsnss_Dte;
    private DbsField pnd_Adsa401_Pnd_Cntl_Bsnss_Tmrrw_Dte;
    private DbsGroup pnd_Adsa401_Pnd_Adsa401_Variables;
    private DbsGroup pnd_Adsa401_Pnd_Current_Cntrct_Prtcpnt_Info;
    private DbsField pnd_Adsa401_Pnd_Cur_Cntrct_Isn;
    private DbsField pnd_Adsa401_Pnd_Cur_Prtcpnt_Isn;
    private DbsField pnd_Adsa401_Pnd_Cur_Iarslt_Isn;
    private DbsField pnd_Adsa401_Pnd_Ia_Cntrct_Lob;
    private DbsField pnd_Adsa401_Pnd_Ia_Cntrct_Sub_Lob;
    private DbsField pnd_Adsa401_Pnd_Cntrct_Lob;
    private DbsField pnd_Adsa401_Pnd_Cntrct_Sub_Lob;
    private DbsField pnd_Adsa401_Pnd_Error_Status;
    private DbsGroup pnd_Adsa401_Pnd_Addl_Cntrct_Info;
    private DbsField pnd_Adsa401_Pnd_Check_Rule_Elgblty;
    private DbsField pnd_Adsa401_Pnd_Total_Tiaa_Ivc_Amount;
    private DbsField pnd_Adsa401_Pnd_Total_Cref_Ivc_Amount;
    private DbsField pnd_Adsa401_Pnd_Ia_Origin_Code;

    public DbsGroup getPnd_Adsa401() { return pnd_Adsa401; }

    public DbsGroup getPnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd() { return pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd; }

    public DbsField getPnd_Adsa401_Rqst_Id() { return pnd_Adsa401_Rqst_Id; }

    public DbsGroup getPnd_Adsa401_Adp_Ia_Data() { return pnd_Adsa401_Adp_Ia_Data; }

    public DbsField getPnd_Adsa401_Adp_Ia_Tiaa_Nbr() { return pnd_Adsa401_Adp_Ia_Tiaa_Nbr; }

    public DbsField getPnd_Adsa401_Adp_Ia_Tiaa_Rea_Nbr() { return pnd_Adsa401_Adp_Ia_Tiaa_Rea_Nbr; }

    public DbsField getPnd_Adsa401_Adp_Ia_Tiaa_Payee_Cd() { return pnd_Adsa401_Adp_Ia_Tiaa_Payee_Cd; }

    public DbsField getPnd_Adsa401_Adp_Ia_Cref_Nbr() { return pnd_Adsa401_Adp_Ia_Cref_Nbr; }

    public DbsField getPnd_Adsa401_Adp_Ia_Cref_Payee_Cd() { return pnd_Adsa401_Adp_Ia_Cref_Payee_Cd; }

    public DbsField getPnd_Adsa401_Adp_Bps_Unit() { return pnd_Adsa401_Adp_Bps_Unit; }

    public DbsField getPnd_Adsa401_Adp_Annt_Typ_Cde() { return pnd_Adsa401_Adp_Annt_Typ_Cde; }

    public DbsField getPnd_Adsa401_Adp_Emplymnt_Term_Dte() { return pnd_Adsa401_Adp_Emplymnt_Term_Dte; }

    public DbsField getPnd_Adsa401_Adp_Cntrcts_In_Rqst() { return pnd_Adsa401_Adp_Cntrcts_In_Rqst; }

    public DbsField getPnd_Adsa401_Adp_Unique_Id() { return pnd_Adsa401_Adp_Unique_Id; }

    public DbsField getPnd_Adsa401_Adp_Effctv_Dte() { return pnd_Adsa401_Adp_Effctv_Dte; }

    public DbsField getPnd_Adsa401_Adp_Cntr_Prt_Issue_Dte() { return pnd_Adsa401_Adp_Cntr_Prt_Issue_Dte; }

    public DbsField getPnd_Adsa401_Adp_Entry_Dte() { return pnd_Adsa401_Adp_Entry_Dte; }

    public DbsField getPnd_Adsa401_Adp_Entry_Tme() { return pnd_Adsa401_Adp_Entry_Tme; }

    public DbsField getPnd_Adsa401_Adp_Entry_User_Id() { return pnd_Adsa401_Adp_Entry_User_Id; }

    public DbsField getPnd_Adsa401_Adp_Rep_Nme() { return pnd_Adsa401_Adp_Rep_Nme; }

    public DbsField getPnd_Adsa401_Adp_Lst_Actvty_Dte() { return pnd_Adsa401_Adp_Lst_Actvty_Dte; }

    public DbsField getPnd_Adsa401_Adp_Wpid() { return pnd_Adsa401_Adp_Wpid; }

    public DbsField getPnd_Adsa401_Adp_Mit_Log_Dte_Tme() { return pnd_Adsa401_Adp_Mit_Log_Dte_Tme; }

    public DbsField getPnd_Adsa401_Adp_Opn_Clsd_Ind() { return pnd_Adsa401_Adp_Opn_Clsd_Ind; }

    public DbsField getPnd_Adsa401_Adp_In_Prgrss_Ind() { return pnd_Adsa401_Adp_In_Prgrss_Ind; }

    public DbsField getPnd_Adsa401_Adp_Rcprcl_Dte() { return pnd_Adsa401_Adp_Rcprcl_Dte; }

    public DbsField getPnd_Adsa401_Adp_Stts_Cde() { return pnd_Adsa401_Adp_Stts_Cde; }

    public DbsGroup getPnd_Adsa401_Adp_Crrspndce_Mlng_Dta() { return pnd_Adsa401_Adp_Crrspndce_Mlng_Dta; }

    public DbsField getPnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Txt() { return pnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Txt; }

    public DbsField getPnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Zip() { return pnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Zip; }

    public DbsField getPnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Typ_Cd() { return pnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Typ_Cd; }

    public DbsField getPnd_Adsa401_Adp_Frst_Annt_Ctznshp() { return pnd_Adsa401_Adp_Frst_Annt_Ctznshp; }

    public DbsField getPnd_Adsa401_Adp_Frst_Annt_Rsdnc_Cde() { return pnd_Adsa401_Adp_Frst_Annt_Rsdnc_Cde; }

    public DbsField getPnd_Adsa401_Adp_Frst_Annt_Ssn() { return pnd_Adsa401_Adp_Frst_Annt_Ssn; }

    public DbsField getPnd_Adsa401_Adp_Frst_Annt_Title_Cde() { return pnd_Adsa401_Adp_Frst_Annt_Title_Cde; }

    public DbsField getPnd_Adsa401_Adp_Frst_Annt_Frst_Nme() { return pnd_Adsa401_Adp_Frst_Annt_Frst_Nme; }

    public DbsField getPnd_Adsa401_Adp_Frst_Annt_Mid_Nme() { return pnd_Adsa401_Adp_Frst_Annt_Mid_Nme; }

    public DbsField getPnd_Adsa401_Adp_Frst_Annt_Lst_Nme() { return pnd_Adsa401_Adp_Frst_Annt_Lst_Nme; }

    public DbsField getPnd_Adsa401_Adp_Frst_Annt_Sfx() { return pnd_Adsa401_Adp_Frst_Annt_Sfx; }

    public DbsField getPnd_Adsa401_Adp_Frst_Annt_Dte_Of_Brth() { return pnd_Adsa401_Adp_Frst_Annt_Dte_Of_Brth; }

    public DbsField getPnd_Adsa401_Adp_Frst_Annt_Sex_Cde() { return pnd_Adsa401_Adp_Frst_Annt_Sex_Cde; }

    public DbsField getPnd_Adsa401_Adp_Scnd_Annt_Dte_Of_Brth() { return pnd_Adsa401_Adp_Scnd_Annt_Dte_Of_Brth; }

    public DbsField getPnd_Adsa401_Adp_Scnd_Annt_Ssn() { return pnd_Adsa401_Adp_Scnd_Annt_Ssn; }

    public DbsField getPnd_Adsa401_Adp_Scnd_Annt_Sex_Cde() { return pnd_Adsa401_Adp_Scnd_Annt_Sex_Cde; }

    public DbsField getPnd_Adsa401_Adp_Annty_Optn() { return pnd_Adsa401_Adp_Annty_Optn; }

    public DbsField getPnd_Adsa401_Adp_Grntee_Period() { return pnd_Adsa401_Adp_Grntee_Period; }

    public DbsField getPnd_Adsa401_Adp_Grntee_Period_Mnths() { return pnd_Adsa401_Adp_Grntee_Period_Mnths; }

    public DbsField getPnd_Adsa401_Adp_Annty_Strt_Dte() { return pnd_Adsa401_Adp_Annty_Strt_Dte; }

    public DbsField getPnd_Adsa401_Adp_Pymnt_Mode() { return pnd_Adsa401_Adp_Pymnt_Mode; }

    public DbsField getPnd_Adsa401_Adp_Settl_All_Tiaa_Grd_Pct() { return pnd_Adsa401_Adp_Settl_All_Tiaa_Grd_Pct; }

    public DbsField getPnd_Adsa401_Adp_Settl_All_Cref_Mon_Pct() { return pnd_Adsa401_Adp_Settl_All_Cref_Mon_Pct; }

    public DbsField getPnd_Adsa401_Adp_Alt_Dest_Addr_Ind() { return pnd_Adsa401_Adp_Alt_Dest_Addr_Ind; }

    public DbsField getPnd_Adsa401_Adp_Spcfc_Brkdwn_Ind() { return pnd_Adsa401_Adp_Spcfc_Brkdwn_Ind; }

    public DbsGroup getPnd_Adsa401_Adp_Alt_Dest_Data() { return pnd_Adsa401_Adp_Alt_Dest_Data; }

    public DbsField getPnd_Adsa401_Adp_Alt_Payee_Ind() { return pnd_Adsa401_Adp_Alt_Payee_Ind; }

    public DbsField getPnd_Adsa401_Adp_Alt_Carrier_Cde() { return pnd_Adsa401_Adp_Alt_Carrier_Cde; }

    public DbsField getPnd_Adsa401_Adp_Alt_Dest_Addr_Txt() { return pnd_Adsa401_Adp_Alt_Dest_Addr_Txt; }

    public DbsField getPnd_Adsa401_Adp_Alt_Dest_Addr_Zip() { return pnd_Adsa401_Adp_Alt_Dest_Addr_Zip; }

    public DbsField getPnd_Adsa401_Adp_Alt_Dest_Addr_Typ_Cde() { return pnd_Adsa401_Adp_Alt_Dest_Addr_Typ_Cde; }

    public DbsField getPnd_Adsa401_Adp_Alt_Dest_Acct_Nbr() { return pnd_Adsa401_Adp_Alt_Dest_Acct_Nbr; }

    public DbsField getPnd_Adsa401_Adp_Alt_Dest_Acct_Typ() { return pnd_Adsa401_Adp_Alt_Dest_Acct_Typ; }

    public DbsField getPnd_Adsa401_Adp_Alt_Dest_Trnst_Cde() { return pnd_Adsa401_Adp_Alt_Dest_Trnst_Cde; }

    public DbsField getPnd_Adsa401_Adp_Alt_Dest_Hold_Cde() { return pnd_Adsa401_Adp_Alt_Dest_Hold_Cde; }

    public DbsField getPnd_Adsa401_Adp_Alt_Dest_Rlvr_Ind() { return pnd_Adsa401_Adp_Alt_Dest_Rlvr_Ind; }

    public DbsField getPnd_Adsa401_Adp_Alt_Dest_Rlvr_Dest() { return pnd_Adsa401_Adp_Alt_Dest_Rlvr_Dest; }

    public DbsField getPnd_Adsa401_Adp_Sbjct_To_Wvr_Ind() { return pnd_Adsa401_Adp_Sbjct_To_Wvr_Ind; }

    public DbsField getPnd_Adsa401_Adp_Sbjct_To_Spsl_Cnsnt() { return pnd_Adsa401_Adp_Sbjct_To_Spsl_Cnsnt; }

    public DbsGroup getPnd_Adsa401_Adp_Forms_Data() { return pnd_Adsa401_Adp_Forms_Data; }

    public DbsField getPnd_Adsa401_Adp_Forms_Rcvd_Dte() { return pnd_Adsa401_Adp_Forms_Rcvd_Dte; }

    public DbsGroup getPnd_Adsa401_Adp_Audit_Trail_Dta() { return pnd_Adsa401_Adp_Audit_Trail_Dta; }

    public DbsField getPnd_Adsa401_Adp_Rqst_Last_Updtd_Id() { return pnd_Adsa401_Adp_Rqst_Last_Updtd_Id; }

    public DbsField getPnd_Adsa401_Adp_Rqst_Last_Updtd_Dte() { return pnd_Adsa401_Adp_Rqst_Last_Updtd_Dte; }

    public DbsField getPnd_Adsa401_Adp_Rsttlmnt_Cnt() { return pnd_Adsa401_Adp_Rsttlmnt_Cnt; }

    public DbsField getPnd_Adsa401_Adp_State_Of_Issue() { return pnd_Adsa401_Adp_State_Of_Issue; }

    public DbsField getPnd_Adsa401_Adp_Orgnl_Issue_State() { return pnd_Adsa401_Adp_Orgnl_Issue_State; }

    public DbsField getPnd_Adsa401_Adp_Ivc_Rlvr_Cde() { return pnd_Adsa401_Adp_Ivc_Rlvr_Cde; }

    public DbsField getPnd_Adsa401_Adp_Ivc_Pymnt_Rule() { return pnd_Adsa401_Adp_Ivc_Pymnt_Rule; }

    public DbsField getPnd_Adsa401_Adp_Plan_Nbr() { return pnd_Adsa401_Adp_Plan_Nbr; }

    public DbsGroup getPnd_Adsa401_Adp_Frm_Irc_Dta() { return pnd_Adsa401_Adp_Frm_Irc_Dta; }

    public DbsField getPnd_Adsa401_Adp_Frm_Irc_Cde() { return pnd_Adsa401_Adp_Frm_Irc_Cde; }

    public DbsField getPnd_Adsa401_Adp_To_Irc_Cde() { return pnd_Adsa401_Adp_To_Irc_Cde; }

    public DbsField getPnd_Adsa401_Adp_Irc_Cde() { return pnd_Adsa401_Adp_Irc_Cde; }

    public DbsField getPnd_Adsa401_Adp_Roth_Plan_Type() { return pnd_Adsa401_Adp_Roth_Plan_Type; }

    public DbsField getPnd_Adsa401_Adp_Roth_Frst_Cntrbtn_Dte() { return pnd_Adsa401_Adp_Roth_Frst_Cntrbtn_Dte; }

    public DbsField getPnd_Adsa401_Adp_Roth_Dsblty_Dte() { return pnd_Adsa401_Adp_Roth_Dsblty_Dte; }

    public DbsField getPnd_Adsa401_Adp_Roth_Rqst_Ind() { return pnd_Adsa401_Adp_Roth_Rqst_Ind; }

    public DbsField getPnd_Adsa401_Adp_Srvvr_Ind() { return pnd_Adsa401_Adp_Srvvr_Ind; }

    public DbsField getPnd_Adsa401_Adp_Sole_Prmry_Bene_Rltnshp() { return pnd_Adsa401_Adp_Sole_Prmry_Bene_Rltnshp; }

    public DbsField getPnd_Adsa401_Adp_Sole_Prmry_Bene_Dte_Of_Brth() { return pnd_Adsa401_Adp_Sole_Prmry_Bene_Dte_Of_Brth; }

    public DbsField getPnd_Adsa401_Adp_Plan_Nme() { return pnd_Adsa401_Adp_Plan_Nme; }

    public DbsGroup getPnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd() { return pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd; }

    public DbsField getPnd_Adsa401_Adc_Rqst_Id() { return pnd_Adsa401_Adc_Rqst_Id; }

    public DbsField getPnd_Adsa401_Adc_Sqnce_Nbr() { return pnd_Adsa401_Adc_Sqnce_Nbr; }

    public DbsField getPnd_Adsa401_Adc_Stts_Cde() { return pnd_Adsa401_Adc_Stts_Cde; }

    public DbsField getPnd_Adsa401_Adc_Lst_Actvty_Dte() { return pnd_Adsa401_Adc_Lst_Actvty_Dte; }

    public DbsField getPnd_Adsa401_Adc_Tiaa_Nbr() { return pnd_Adsa401_Adc_Tiaa_Nbr; }

    public DbsField getPnd_Adsa401_Adc_Cref_Nbr() { return pnd_Adsa401_Adc_Cref_Nbr; }

    public DbsField getPnd_Adsa401_Adc_Gd_Cntrct_Ind() { return pnd_Adsa401_Adc_Gd_Cntrct_Ind; }

    public DbsField getPnd_Adsa401_Adc_Tiaa_Ivc_Gd_Ind() { return pnd_Adsa401_Adc_Tiaa_Ivc_Gd_Ind; }

    public DbsField getPnd_Adsa401_Adc_Cref_Ivc_Gd_Ind() { return pnd_Adsa401_Adc_Cref_Ivc_Gd_Ind; }

    public DbsField getPnd_Adsa401_Adc_Cntrct_Issue_State() { return pnd_Adsa401_Adc_Cntrct_Issue_State; }

    public DbsField getPnd_Adsa401_Adc_Cntrct_Issue_Dte() { return pnd_Adsa401_Adc_Cntrct_Issue_Dte; }

    public DbsField getPnd_Adsa401_Adc_Hold_Cde() { return pnd_Adsa401_Adc_Hold_Cde; }

    public DbsGroup getPnd_Adsa401_Adc_Ppg_Data() { return pnd_Adsa401_Adc_Ppg_Data; }

    public DbsField getPnd_Adsa401_Adc_Ppg_Cde() { return pnd_Adsa401_Adc_Ppg_Cde; }

    public DbsField getPnd_Adsa401_Adc_Qlfd_Non_Qlfd_Ind() { return pnd_Adsa401_Adc_Qlfd_Non_Qlfd_Ind; }

    public DbsField getPnd_Adsa401_Adc_Ppg_Trgtd() { return pnd_Adsa401_Adc_Ppg_Trgtd; }

    public DbsField getPnd_Adsa401_Adc_Ppg_Rule_Assoc() { return pnd_Adsa401_Adc_Ppg_Rule_Assoc; }

    public DbsGroup getPnd_Adsa401_Adc_Rqst_Info() { return pnd_Adsa401_Adc_Rqst_Info; }

    public DbsField getPnd_Adsa401_Adc_Acct_Cde() { return pnd_Adsa401_Adc_Acct_Cde; }

    public DbsField getPnd_Adsa401_Adc_Tkr_Symbl() { return pnd_Adsa401_Adc_Tkr_Symbl; }

    public DbsField getPnd_Adsa401_Adc_Acct_Stts_Cde() { return pnd_Adsa401_Adc_Acct_Stts_Cde; }

    public DbsField getPnd_Adsa401_Adc_Acct_Typ() { return pnd_Adsa401_Adc_Acct_Typ; }

    public DbsField getPnd_Adsa401_Adc_Acct_Qty() { return pnd_Adsa401_Adc_Acct_Qty; }

    public DbsField getPnd_Adsa401_Adc_Acct_Actl_Amt() { return pnd_Adsa401_Adc_Acct_Actl_Amt; }

    public DbsField getPnd_Adsa401_Adc_Acct_Actl_Nbr_Units() { return pnd_Adsa401_Adc_Acct_Actl_Nbr_Units; }

    public DbsField getPnd_Adsa401_Adc_Grd_Mnthly_Typ() { return pnd_Adsa401_Adc_Grd_Mnthly_Typ; }

    public DbsField getPnd_Adsa401_Adc_Grd_Mnthly_Qty() { return pnd_Adsa401_Adc_Grd_Mnthly_Qty; }

    public DbsField getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt() { return pnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt; }

    public DbsField getPnd_Adsa401_Adc_Stndrd_Annl_Actl_Amt() { return pnd_Adsa401_Adc_Stndrd_Annl_Actl_Amt; }

    public DbsField getPnd_Adsa401_Adc_Acct_Cref_Rate_Cde() { return pnd_Adsa401_Adc_Acct_Cref_Rate_Cde; }

    public DbsField getPnd_Adsa401_Adc_Ivc_Amt() { return pnd_Adsa401_Adc_Ivc_Amt; }

    public DbsField getPnd_Adsa401_Adc_Acct_Opn_Accum_Amt() { return pnd_Adsa401_Adc_Acct_Opn_Accum_Amt; }

    public DbsField getPnd_Adsa401_Adc_Acct_Opn_Nbr_Units() { return pnd_Adsa401_Adc_Acct_Opn_Nbr_Units; }

    public DbsGroup getPnd_Adsa401_Adc_Err_Rpt_Data() { return pnd_Adsa401_Adc_Err_Rpt_Data; }

    public DbsField getPnd_Adsa401_Adc_Err_Cde() { return pnd_Adsa401_Adc_Err_Cde; }

    public DbsField getPnd_Adsa401_Adc_Err_Acct_Cde() { return pnd_Adsa401_Adc_Err_Acct_Cde; }

    public DbsField getPnd_Adsa401_Adc_Err_Msg() { return pnd_Adsa401_Adc_Err_Msg; }

    public DbsField getPnd_Adsa401_Adc_Trk_Ro_Ivc() { return pnd_Adsa401_Adc_Trk_Ro_Ivc; }

    public DbsGroup getPnd_Adsa401_Adc_Dtl_Tiaa_Data() { return pnd_Adsa401_Adc_Dtl_Tiaa_Data; }

    public DbsField getPnd_Adsa401_Adc_Dtl_Tiaa_Rate_Cde() { return pnd_Adsa401_Adc_Dtl_Tiaa_Rate_Cde; }

    public DbsField getPnd_Adsa401_Adc_Dtl_Tiaa_Actl_Amt() { return pnd_Adsa401_Adc_Dtl_Tiaa_Actl_Amt; }

    public DbsField getPnd_Adsa401_Adc_Dtl_Tiaa_Grd_Actl_Amt() { return pnd_Adsa401_Adc_Dtl_Tiaa_Grd_Actl_Amt; }

    public DbsField getPnd_Adsa401_Adc_Dtl_Tiaa_Stndrd_Actl_Amt() { return pnd_Adsa401_Adc_Dtl_Tiaa_Stndrd_Actl_Amt; }

    public DbsField getPnd_Adsa401_Adc_Dtl_Tiaa_Ia_Rate_Cde() { return pnd_Adsa401_Adc_Dtl_Tiaa_Ia_Rate_Cde; }

    public DbsField getPnd_Adsa401_Adc_Dtl_Tiaa_Opn_Accum_Amt() { return pnd_Adsa401_Adc_Dtl_Tiaa_Opn_Accum_Amt; }

    public DbsGroup getPnd_Adsa401_Adc_Acct_Info() { return pnd_Adsa401_Adc_Acct_Info; }

    public DbsField getPnd_Adsa401_Adc_Acct_Aftr_Tax_Amt() { return pnd_Adsa401_Adc_Acct_Aftr_Tax_Amt; }

    public DbsField getPnd_Adsa401_Adc_Sttl_Ivc_Amt() { return pnd_Adsa401_Adc_Sttl_Ivc_Amt; }

    public DbsField getPnd_Adsa401_Adc_Sub_Plan_Nbr() { return pnd_Adsa401_Adc_Sub_Plan_Nbr; }

    public DbsGroup getPnd_Adsa401_Adc_Invstmt_Info() { return pnd_Adsa401_Adc_Invstmt_Info; }

    public DbsField getPnd_Adsa401_Adc_Invstmt_Id() { return pnd_Adsa401_Adc_Invstmt_Id; }

    public DbsGroup getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Info() { return pnd_Adsa401_Adc_Tpa_Guarant_Commut_Info; }

    public DbsField getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Amt() { return pnd_Adsa401_Adc_Tpa_Guarant_Commut_Amt; }

    public DbsField getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Grd_Amt() { return pnd_Adsa401_Adc_Tpa_Guarant_Commut_Grd_Amt; }

    public DbsField getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Std_Amt() { return pnd_Adsa401_Adc_Tpa_Guarant_Commut_Std_Amt; }

    public DbsField getPnd_Adsa401_Adc_Orig_Lob_Ind() { return pnd_Adsa401_Adc_Orig_Lob_Ind; }

    public DbsField getPnd_Adsa401_Adc_Tpa_Paymnt_Cnt() { return pnd_Adsa401_Adc_Tpa_Paymnt_Cnt; }

    public DbsField getPnd_Adsa401_Adc_T395_Ticker() { return pnd_Adsa401_Adc_T395_Ticker; }

    public DbsField getPnd_Adsa401_Adc_T395_Fund_Id() { return pnd_Adsa401_Adc_T395_Fund_Id; }

    public DbsField getPnd_Adsa401_Adc_Inv_Short_Name() { return pnd_Adsa401_Adc_Inv_Short_Name; }

    public DbsField getPnd_Adsa401_Adc_Contract_Id() { return pnd_Adsa401_Adc_Contract_Id; }

    public DbsField getPnd_Adsa401_Adc_Invstmnt_Grpng_Id() { return pnd_Adsa401_Adc_Invstmnt_Grpng_Id; }

    public DbsGroup getPnd_Adsa401_Pnd_Adsa401_Static_Variables() { return pnd_Adsa401_Pnd_Adsa401_Static_Variables; }

    public DbsField getPnd_Adsa401_Pnd_Cics_System_Name() { return pnd_Adsa401_Pnd_Cics_System_Name; }

    public DbsField getPnd_Adsa401_Pnd_Fund_Cnt() { return pnd_Adsa401_Pnd_Fund_Cnt; }

    public DbsField getPnd_Adsa401_Pnd_Tiaa_Rate_Cnt() { return pnd_Adsa401_Pnd_Tiaa_Rate_Cnt; }

    public DbsField getPnd_Adsa401_Pnd_Cntl_Bsnss_Dte() { return pnd_Adsa401_Pnd_Cntl_Bsnss_Dte; }

    public DbsField getPnd_Adsa401_Pnd_Cntl_Bsnss_Tmrrw_Dte() { return pnd_Adsa401_Pnd_Cntl_Bsnss_Tmrrw_Dte; }

    public DbsGroup getPnd_Adsa401_Pnd_Adsa401_Variables() { return pnd_Adsa401_Pnd_Adsa401_Variables; }

    public DbsGroup getPnd_Adsa401_Pnd_Current_Cntrct_Prtcpnt_Info() { return pnd_Adsa401_Pnd_Current_Cntrct_Prtcpnt_Info; }

    public DbsField getPnd_Adsa401_Pnd_Cur_Cntrct_Isn() { return pnd_Adsa401_Pnd_Cur_Cntrct_Isn; }

    public DbsField getPnd_Adsa401_Pnd_Cur_Prtcpnt_Isn() { return pnd_Adsa401_Pnd_Cur_Prtcpnt_Isn; }

    public DbsField getPnd_Adsa401_Pnd_Cur_Iarslt_Isn() { return pnd_Adsa401_Pnd_Cur_Iarslt_Isn; }

    public DbsField getPnd_Adsa401_Pnd_Ia_Cntrct_Lob() { return pnd_Adsa401_Pnd_Ia_Cntrct_Lob; }

    public DbsField getPnd_Adsa401_Pnd_Ia_Cntrct_Sub_Lob() { return pnd_Adsa401_Pnd_Ia_Cntrct_Sub_Lob; }

    public DbsField getPnd_Adsa401_Pnd_Cntrct_Lob() { return pnd_Adsa401_Pnd_Cntrct_Lob; }

    public DbsField getPnd_Adsa401_Pnd_Cntrct_Sub_Lob() { return pnd_Adsa401_Pnd_Cntrct_Sub_Lob; }

    public DbsField getPnd_Adsa401_Pnd_Error_Status() { return pnd_Adsa401_Pnd_Error_Status; }

    public DbsGroup getPnd_Adsa401_Pnd_Addl_Cntrct_Info() { return pnd_Adsa401_Pnd_Addl_Cntrct_Info; }

    public DbsField getPnd_Adsa401_Pnd_Check_Rule_Elgblty() { return pnd_Adsa401_Pnd_Check_Rule_Elgblty; }

    public DbsField getPnd_Adsa401_Pnd_Total_Tiaa_Ivc_Amount() { return pnd_Adsa401_Pnd_Total_Tiaa_Ivc_Amount; }

    public DbsField getPnd_Adsa401_Pnd_Total_Cref_Ivc_Amount() { return pnd_Adsa401_Pnd_Total_Cref_Ivc_Amount; }

    public DbsField getPnd_Adsa401_Pnd_Ia_Origin_Code() { return pnd_Adsa401_Pnd_Ia_Origin_Code; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Adsa401 = dbsRecord.newGroupInRecord("pnd_Adsa401", "#ADSA401");
        pnd_Adsa401.setParameterOption(ParameterOption.ByReference);
        pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd = pnd_Adsa401.newGroupInGroup("pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd", "#ADSA401-PRTCPNT-RCRD");
        pnd_Adsa401_Rqst_Id = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Rqst_Id", "RQST-ID", FieldType.STRING, 35);
        pnd_Adsa401_Adp_Ia_Data = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newGroupInGroup("pnd_Adsa401_Adp_Ia_Data", "ADP-IA-DATA");
        pnd_Adsa401_Adp_Ia_Tiaa_Nbr = pnd_Adsa401_Adp_Ia_Data.newFieldInGroup("pnd_Adsa401_Adp_Ia_Tiaa_Nbr", "ADP-IA-TIAA-NBR", FieldType.STRING, 10);
        pnd_Adsa401_Adp_Ia_Tiaa_Rea_Nbr = pnd_Adsa401_Adp_Ia_Data.newFieldInGroup("pnd_Adsa401_Adp_Ia_Tiaa_Rea_Nbr", "ADP-IA-TIAA-REA-NBR", FieldType.STRING, 
            10);
        pnd_Adsa401_Adp_Ia_Tiaa_Payee_Cd = pnd_Adsa401_Adp_Ia_Data.newFieldInGroup("pnd_Adsa401_Adp_Ia_Tiaa_Payee_Cd", "ADP-IA-TIAA-PAYEE-CD", FieldType.STRING, 
            2);
        pnd_Adsa401_Adp_Ia_Cref_Nbr = pnd_Adsa401_Adp_Ia_Data.newFieldInGroup("pnd_Adsa401_Adp_Ia_Cref_Nbr", "ADP-IA-CREF-NBR", FieldType.STRING, 10);
        pnd_Adsa401_Adp_Ia_Cref_Payee_Cd = pnd_Adsa401_Adp_Ia_Data.newFieldInGroup("pnd_Adsa401_Adp_Ia_Cref_Payee_Cd", "ADP-IA-CREF-PAYEE-CD", FieldType.STRING, 
            2);
        pnd_Adsa401_Adp_Bps_Unit = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Bps_Unit", "ADP-BPS-UNIT", FieldType.STRING, 
            8);
        pnd_Adsa401_Adp_Annt_Typ_Cde = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Annt_Typ_Cde", "ADP-ANNT-TYP-CDE", FieldType.STRING, 
            1);
        pnd_Adsa401_Adp_Emplymnt_Term_Dte = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Emplymnt_Term_Dte", "ADP-EMPLYMNT-TERM-DTE", 
            FieldType.DATE);
        pnd_Adsa401_Adp_Cntrcts_In_Rqst = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldArrayInGroup("pnd_Adsa401_Adp_Cntrcts_In_Rqst", "ADP-CNTRCTS-IN-RQST", 
            FieldType.STRING, 10, new DbsArrayController(1,12));
        pnd_Adsa401_Adp_Unique_Id = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Unique_Id", "ADP-UNIQUE-ID", FieldType.NUMERIC, 
            12);
        pnd_Adsa401_Adp_Effctv_Dte = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Effctv_Dte", "ADP-EFFCTV-DTE", FieldType.DATE);
        pnd_Adsa401_Adp_Cntr_Prt_Issue_Dte = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Cntr_Prt_Issue_Dte", "ADP-CNTR-PRT-ISSUE-DTE", 
            FieldType.DATE);
        pnd_Adsa401_Adp_Entry_Dte = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Entry_Dte", "ADP-ENTRY-DTE", FieldType.DATE);
        pnd_Adsa401_Adp_Entry_Tme = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Entry_Tme", "ADP-ENTRY-TME", FieldType.TIME);
        pnd_Adsa401_Adp_Entry_User_Id = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Entry_User_Id", "ADP-ENTRY-USER-ID", FieldType.STRING, 
            8);
        pnd_Adsa401_Adp_Rep_Nme = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Rep_Nme", "ADP-REP-NME", FieldType.STRING, 40);
        pnd_Adsa401_Adp_Lst_Actvty_Dte = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Lst_Actvty_Dte", "ADP-LST-ACTVTY-DTE", 
            FieldType.DATE);
        pnd_Adsa401_Adp_Wpid = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Wpid", "ADP-WPID", FieldType.STRING, 6);
        pnd_Adsa401_Adp_Mit_Log_Dte_Tme = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Mit_Log_Dte_Tme", "ADP-MIT-LOG-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Adsa401_Adp_Opn_Clsd_Ind = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Opn_Clsd_Ind", "ADP-OPN-CLSD-IND", FieldType.STRING, 
            1);
        pnd_Adsa401_Adp_In_Prgrss_Ind = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_In_Prgrss_Ind", "ADP-IN-PRGRSS-IND", FieldType.BOOLEAN);
        pnd_Adsa401_Adp_Rcprcl_Dte = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Rcprcl_Dte", "ADP-RCPRCL-DTE", FieldType.NUMERIC, 
            8);
        pnd_Adsa401_Adp_Stts_Cde = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Stts_Cde", "ADP-STTS-CDE", FieldType.STRING, 
            3);
        pnd_Adsa401_Adp_Crrspndce_Mlng_Dta = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newGroupInGroup("pnd_Adsa401_Adp_Crrspndce_Mlng_Dta", "ADP-CRRSPNDCE-MLNG-DTA");
        pnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Txt = pnd_Adsa401_Adp_Crrspndce_Mlng_Dta.newFieldArrayInGroup("pnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Txt", 
            "ADP-CRRSPNDNCE-PERM-ADDR-TXT", FieldType.STRING, 35, new DbsArrayController(1,5));
        pnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Zip = pnd_Adsa401_Adp_Crrspndce_Mlng_Dta.newFieldInGroup("pnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Zip", "ADP-CRRSPNDNCE-PERM-ADDR-ZIP", 
            FieldType.STRING, 9);
        pnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Typ_Cd = pnd_Adsa401_Adp_Crrspndce_Mlng_Dta.newFieldInGroup("pnd_Adsa401_Adp_Crrspndnce_Perm_Addr_Typ_Cd", 
            "ADP-CRRSPNDNCE-PERM-ADDR-TYP-CD", FieldType.STRING, 1);
        pnd_Adsa401_Adp_Frst_Annt_Ctznshp = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Frst_Annt_Ctznshp", "ADP-FRST-ANNT-CTZNSHP", 
            FieldType.STRING, 2);
        pnd_Adsa401_Adp_Frst_Annt_Rsdnc_Cde = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Frst_Annt_Rsdnc_Cde", "ADP-FRST-ANNT-RSDNC-CDE", 
            FieldType.STRING, 2);
        pnd_Adsa401_Adp_Frst_Annt_Ssn = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Frst_Annt_Ssn", "ADP-FRST-ANNT-SSN", FieldType.NUMERIC, 
            9);
        pnd_Adsa401_Adp_Frst_Annt_Title_Cde = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Frst_Annt_Title_Cde", "ADP-FRST-ANNT-TITLE-CDE", 
            FieldType.STRING, 8);
        pnd_Adsa401_Adp_Frst_Annt_Frst_Nme = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Frst_Annt_Frst_Nme", "ADP-FRST-ANNT-FRST-NME", 
            FieldType.STRING, 30);
        pnd_Adsa401_Adp_Frst_Annt_Mid_Nme = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Frst_Annt_Mid_Nme", "ADP-FRST-ANNT-MID-NME", 
            FieldType.STRING, 30);
        pnd_Adsa401_Adp_Frst_Annt_Lst_Nme = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Frst_Annt_Lst_Nme", "ADP-FRST-ANNT-LST-NME", 
            FieldType.STRING, 30);
        pnd_Adsa401_Adp_Frst_Annt_Sfx = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Frst_Annt_Sfx", "ADP-FRST-ANNT-SFX", FieldType.STRING, 
            8);
        pnd_Adsa401_Adp_Frst_Annt_Dte_Of_Brth = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Frst_Annt_Dte_Of_Brth", "ADP-FRST-ANNT-DTE-OF-BRTH", 
            FieldType.DATE);
        pnd_Adsa401_Adp_Frst_Annt_Sex_Cde = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Frst_Annt_Sex_Cde", "ADP-FRST-ANNT-SEX-CDE", 
            FieldType.STRING, 1);
        pnd_Adsa401_Adp_Scnd_Annt_Dte_Of_Brth = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Scnd_Annt_Dte_Of_Brth", "ADP-SCND-ANNT-DTE-OF-BRTH", 
            FieldType.DATE);
        pnd_Adsa401_Adp_Scnd_Annt_Ssn = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Scnd_Annt_Ssn", "ADP-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9);
        pnd_Adsa401_Adp_Scnd_Annt_Sex_Cde = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Scnd_Annt_Sex_Cde", "ADP-SCND-ANNT-SEX-CDE", 
            FieldType.STRING, 1);
        pnd_Adsa401_Adp_Annty_Optn = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Annty_Optn", "ADP-ANNTY-OPTN", FieldType.STRING, 
            2);
        pnd_Adsa401_Adp_Grntee_Period = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Grntee_Period", "ADP-GRNTEE-PERIOD", FieldType.NUMERIC, 
            2);
        pnd_Adsa401_Adp_Grntee_Period_Mnths = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Grntee_Period_Mnths", "ADP-GRNTEE-PERIOD-MNTHS", 
            FieldType.NUMERIC, 2);
        pnd_Adsa401_Adp_Annty_Strt_Dte = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Annty_Strt_Dte", "ADP-ANNTY-STRT-DTE", 
            FieldType.DATE);
        pnd_Adsa401_Adp_Pymnt_Mode = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Pymnt_Mode", "ADP-PYMNT-MODE", FieldType.NUMERIC, 
            3);
        pnd_Adsa401_Adp_Settl_All_Tiaa_Grd_Pct = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Settl_All_Tiaa_Grd_Pct", "ADP-SETTL-ALL-TIAA-GRD-PCT", 
            FieldType.NUMERIC, 3);
        pnd_Adsa401_Adp_Settl_All_Cref_Mon_Pct = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Settl_All_Cref_Mon_Pct", "ADP-SETTL-ALL-CREF-MON-PCT", 
            FieldType.NUMERIC, 3);
        pnd_Adsa401_Adp_Alt_Dest_Addr_Ind = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Alt_Dest_Addr_Ind", "ADP-ALT-DEST-ADDR-IND", 
            FieldType.STRING, 1);
        pnd_Adsa401_Adp_Spcfc_Brkdwn_Ind = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Spcfc_Brkdwn_Ind", "ADP-SPCFC-BRKDWN-IND", 
            FieldType.STRING, 1);
        pnd_Adsa401_Adp_Alt_Dest_Data = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newGroupInGroup("pnd_Adsa401_Adp_Alt_Dest_Data", "ADP-ALT-DEST-DATA");
        pnd_Adsa401_Adp_Alt_Payee_Ind = pnd_Adsa401_Adp_Alt_Dest_Data.newFieldInGroup("pnd_Adsa401_Adp_Alt_Payee_Ind", "ADP-ALT-PAYEE-IND", FieldType.STRING, 
            1);
        pnd_Adsa401_Adp_Alt_Carrier_Cde = pnd_Adsa401_Adp_Alt_Dest_Data.newFieldInGroup("pnd_Adsa401_Adp_Alt_Carrier_Cde", "ADP-ALT-CARRIER-CDE", FieldType.STRING, 
            4);
        pnd_Adsa401_Adp_Alt_Dest_Addr_Txt = pnd_Adsa401_Adp_Alt_Dest_Data.newFieldArrayInGroup("pnd_Adsa401_Adp_Alt_Dest_Addr_Txt", "ADP-ALT-DEST-ADDR-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1,5));
        pnd_Adsa401_Adp_Alt_Dest_Addr_Zip = pnd_Adsa401_Adp_Alt_Dest_Data.newFieldInGroup("pnd_Adsa401_Adp_Alt_Dest_Addr_Zip", "ADP-ALT-DEST-ADDR-ZIP", 
            FieldType.STRING, 9);
        pnd_Adsa401_Adp_Alt_Dest_Addr_Typ_Cde = pnd_Adsa401_Adp_Alt_Dest_Data.newFieldInGroup("pnd_Adsa401_Adp_Alt_Dest_Addr_Typ_Cde", "ADP-ALT-DEST-ADDR-TYP-CDE", 
            FieldType.STRING, 1);
        pnd_Adsa401_Adp_Alt_Dest_Acct_Nbr = pnd_Adsa401_Adp_Alt_Dest_Data.newFieldInGroup("pnd_Adsa401_Adp_Alt_Dest_Acct_Nbr", "ADP-ALT-DEST-ACCT-NBR", 
            FieldType.STRING, 21);
        pnd_Adsa401_Adp_Alt_Dest_Acct_Typ = pnd_Adsa401_Adp_Alt_Dest_Data.newFieldInGroup("pnd_Adsa401_Adp_Alt_Dest_Acct_Typ", "ADP-ALT-DEST-ACCT-TYP", 
            FieldType.STRING, 1);
        pnd_Adsa401_Adp_Alt_Dest_Trnst_Cde = pnd_Adsa401_Adp_Alt_Dest_Data.newFieldInGroup("pnd_Adsa401_Adp_Alt_Dest_Trnst_Cde", "ADP-ALT-DEST-TRNST-CDE", 
            FieldType.STRING, 9);
        pnd_Adsa401_Adp_Alt_Dest_Hold_Cde = pnd_Adsa401_Adp_Alt_Dest_Data.newFieldInGroup("pnd_Adsa401_Adp_Alt_Dest_Hold_Cde", "ADP-ALT-DEST-HOLD-CDE", 
            FieldType.STRING, 4);
        pnd_Adsa401_Adp_Alt_Dest_Rlvr_Ind = pnd_Adsa401_Adp_Alt_Dest_Data.newFieldInGroup("pnd_Adsa401_Adp_Alt_Dest_Rlvr_Ind", "ADP-ALT-DEST-RLVR-IND", 
            FieldType.STRING, 1);
        pnd_Adsa401_Adp_Alt_Dest_Rlvr_Dest = pnd_Adsa401_Adp_Alt_Dest_Data.newFieldInGroup("pnd_Adsa401_Adp_Alt_Dest_Rlvr_Dest", "ADP-ALT-DEST-RLVR-DEST", 
            FieldType.STRING, 4);
        pnd_Adsa401_Adp_Sbjct_To_Wvr_Ind = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Sbjct_To_Wvr_Ind", "ADP-SBJCT-TO-WVR-IND", 
            FieldType.STRING, 1);
        pnd_Adsa401_Adp_Sbjct_To_Spsl_Cnsnt = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Sbjct_To_Spsl_Cnsnt", "ADP-SBJCT-TO-SPSL-CNSNT", 
            FieldType.STRING, 1);
        pnd_Adsa401_Adp_Forms_Data = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newGroupInGroup("pnd_Adsa401_Adp_Forms_Data", "ADP-FORMS-DATA");
        pnd_Adsa401_Adp_Forms_Rcvd_Dte = pnd_Adsa401_Adp_Forms_Data.newFieldInGroup("pnd_Adsa401_Adp_Forms_Rcvd_Dte", "ADP-FORMS-RCVD-DTE", FieldType.DATE);
        pnd_Adsa401_Adp_Audit_Trail_Dta = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newGroupInGroup("pnd_Adsa401_Adp_Audit_Trail_Dta", "ADP-AUDIT-TRAIL-DTA");
        pnd_Adsa401_Adp_Rqst_Last_Updtd_Id = pnd_Adsa401_Adp_Audit_Trail_Dta.newFieldInGroup("pnd_Adsa401_Adp_Rqst_Last_Updtd_Id", "ADP-RQST-LAST-UPDTD-ID", 
            FieldType.STRING, 7);
        pnd_Adsa401_Adp_Rqst_Last_Updtd_Dte = pnd_Adsa401_Adp_Audit_Trail_Dta.newFieldInGroup("pnd_Adsa401_Adp_Rqst_Last_Updtd_Dte", "ADP-RQST-LAST-UPDTD-DTE", 
            FieldType.DATE);
        pnd_Adsa401_Adp_Rsttlmnt_Cnt = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Rsttlmnt_Cnt", "ADP-RSTTLMNT-CNT", FieldType.STRING, 
            1);
        pnd_Adsa401_Adp_State_Of_Issue = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_State_Of_Issue", "ADP-STATE-OF-ISSUE", 
            FieldType.STRING, 2);
        pnd_Adsa401_Adp_Orgnl_Issue_State = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Orgnl_Issue_State", "ADP-ORGNL-ISSUE-STATE", 
            FieldType.STRING, 2);
        pnd_Adsa401_Adp_Ivc_Rlvr_Cde = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Ivc_Rlvr_Cde", "ADP-IVC-RLVR-CDE", FieldType.STRING, 
            1);
        pnd_Adsa401_Adp_Ivc_Pymnt_Rule = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Ivc_Pymnt_Rule", "ADP-IVC-PYMNT-RULE", 
            FieldType.STRING, 1);
        pnd_Adsa401_Adp_Plan_Nbr = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Plan_Nbr", "ADP-PLAN-NBR", FieldType.STRING, 
            6);
        pnd_Adsa401_Adp_Frm_Irc_Dta = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newGroupArrayInGroup("pnd_Adsa401_Adp_Frm_Irc_Dta", "ADP-FRM-IRC-DTA", new 
            DbsArrayController(1,15));
        pnd_Adsa401_Adp_Frm_Irc_Cde = pnd_Adsa401_Adp_Frm_Irc_Dta.newFieldInGroup("pnd_Adsa401_Adp_Frm_Irc_Cde", "ADP-FRM-IRC-CDE", FieldType.STRING, 
            2);
        pnd_Adsa401_Adp_To_Irc_Cde = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_To_Irc_Cde", "ADP-TO-IRC-CDE", FieldType.STRING, 
            2);
        pnd_Adsa401_Adp_Irc_Cde = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Irc_Cde", "ADP-IRC-CDE", FieldType.STRING, 2);
        pnd_Adsa401_Adp_Roth_Plan_Type = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Roth_Plan_Type", "ADP-ROTH-PLAN-TYPE", 
            FieldType.STRING, 1);
        pnd_Adsa401_Adp_Roth_Frst_Cntrbtn_Dte = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Roth_Frst_Cntrbtn_Dte", "ADP-ROTH-FRST-CNTRBTN-DTE", 
            FieldType.DATE);
        pnd_Adsa401_Adp_Roth_Dsblty_Dte = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Roth_Dsblty_Dte", "ADP-ROTH-DSBLTY-DTE", 
            FieldType.DATE);
        pnd_Adsa401_Adp_Roth_Rqst_Ind = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Roth_Rqst_Ind", "ADP-ROTH-RQST-IND", FieldType.STRING, 
            1);
        pnd_Adsa401_Adp_Srvvr_Ind = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Srvvr_Ind", "ADP-SRVVR-IND", FieldType.STRING, 
            1);
        pnd_Adsa401_Adp_Sole_Prmry_Bene_Rltnshp = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Sole_Prmry_Bene_Rltnshp", "ADP-SOLE-PRMRY-BENE-RLTNSHP", 
            FieldType.STRING, 1);
        pnd_Adsa401_Adp_Sole_Prmry_Bene_Dte_Of_Brth = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Sole_Prmry_Bene_Dte_Of_Brth", 
            "ADP-SOLE-PRMRY-BENE-DTE-OF-BRTH", FieldType.DATE);
        pnd_Adsa401_Adp_Plan_Nme = pnd_Adsa401_Pnd_Adsa401_Prtcpnt_Rcrd.newFieldInGroup("pnd_Adsa401_Adp_Plan_Nme", "ADP-PLAN-NME", FieldType.STRING, 
            32);
        pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd = pnd_Adsa401.newGroupInGroup("pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd", "#ADSA401-CNTRCT-RCRD");
        pnd_Adsa401_Adc_Rqst_Id = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa401_Adc_Rqst_Id", "ADC-RQST-ID", FieldType.STRING, 35);
        pnd_Adsa401_Adc_Sqnce_Nbr = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa401_Adc_Sqnce_Nbr", "ADC-SQNCE-NBR", FieldType.NUMERIC, 
            2);
        pnd_Adsa401_Adc_Stts_Cde = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa401_Adc_Stts_Cde", "ADC-STTS-CDE", FieldType.STRING, 3);
        pnd_Adsa401_Adc_Lst_Actvty_Dte = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa401_Adc_Lst_Actvty_Dte", "ADC-LST-ACTVTY-DTE", FieldType.DATE);
        pnd_Adsa401_Adc_Tiaa_Nbr = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa401_Adc_Tiaa_Nbr", "ADC-TIAA-NBR", FieldType.STRING, 10);
        pnd_Adsa401_Adc_Cref_Nbr = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa401_Adc_Cref_Nbr", "ADC-CREF-NBR", FieldType.STRING, 10);
        pnd_Adsa401_Adc_Gd_Cntrct_Ind = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa401_Adc_Gd_Cntrct_Ind", "ADC-GD-CNTRCT-IND", FieldType.BOOLEAN);
        pnd_Adsa401_Adc_Tiaa_Ivc_Gd_Ind = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa401_Adc_Tiaa_Ivc_Gd_Ind", "ADC-TIAA-IVC-GD-IND", 
            FieldType.STRING, 1);
        pnd_Adsa401_Adc_Cref_Ivc_Gd_Ind = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa401_Adc_Cref_Ivc_Gd_Ind", "ADC-CREF-IVC-GD-IND", 
            FieldType.STRING, 1);
        pnd_Adsa401_Adc_Cntrct_Issue_State = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa401_Adc_Cntrct_Issue_State", "ADC-CNTRCT-ISSUE-STATE", 
            FieldType.STRING, 2);
        pnd_Adsa401_Adc_Cntrct_Issue_Dte = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa401_Adc_Cntrct_Issue_Dte", "ADC-CNTRCT-ISSUE-DTE", 
            FieldType.DATE);
        pnd_Adsa401_Adc_Hold_Cde = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa401_Adc_Hold_Cde", "ADC-HOLD-CDE", FieldType.STRING, 1);
        pnd_Adsa401_Adc_Ppg_Data = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newGroupArrayInGroup("pnd_Adsa401_Adc_Ppg_Data", "ADC-PPG-DATA", new DbsArrayController(1,
            15));
        pnd_Adsa401_Adc_Ppg_Cde = pnd_Adsa401_Adc_Ppg_Data.newFieldInGroup("pnd_Adsa401_Adc_Ppg_Cde", "ADC-PPG-CDE", FieldType.STRING, 6);
        pnd_Adsa401_Adc_Qlfd_Non_Qlfd_Ind = pnd_Adsa401_Adc_Ppg_Data.newFieldInGroup("pnd_Adsa401_Adc_Qlfd_Non_Qlfd_Ind", "ADC-QLFD-NON-QLFD-IND", FieldType.NUMERIC, 
            2);
        pnd_Adsa401_Adc_Ppg_Trgtd = pnd_Adsa401_Adc_Ppg_Data.newFieldInGroup("pnd_Adsa401_Adc_Ppg_Trgtd", "ADC-PPG-TRGTD", FieldType.STRING, 1);
        pnd_Adsa401_Adc_Ppg_Rule_Assoc = pnd_Adsa401_Adc_Ppg_Data.newFieldInGroup("pnd_Adsa401_Adc_Ppg_Rule_Assoc", "ADC-PPG-RULE-ASSOC", FieldType.NUMERIC, 
            6);
        pnd_Adsa401_Adc_Rqst_Info = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newGroupArrayInGroup("pnd_Adsa401_Adc_Rqst_Info", "ADC-RQST-INFO", new DbsArrayController(1,
            20));
        pnd_Adsa401_Adc_Acct_Cde = pnd_Adsa401_Adc_Rqst_Info.newFieldInGroup("pnd_Adsa401_Adc_Acct_Cde", "ADC-ACCT-CDE", FieldType.STRING, 1);
        pnd_Adsa401_Adc_Tkr_Symbl = pnd_Adsa401_Adc_Rqst_Info.newFieldInGroup("pnd_Adsa401_Adc_Tkr_Symbl", "ADC-TKR-SYMBL", FieldType.STRING, 10);
        pnd_Adsa401_Adc_Acct_Stts_Cde = pnd_Adsa401_Adc_Rqst_Info.newFieldInGroup("pnd_Adsa401_Adc_Acct_Stts_Cde", "ADC-ACCT-STTS-CDE", FieldType.STRING, 
            3);
        pnd_Adsa401_Adc_Acct_Typ = pnd_Adsa401_Adc_Rqst_Info.newFieldInGroup("pnd_Adsa401_Adc_Acct_Typ", "ADC-ACCT-TYP", FieldType.STRING, 1);
        pnd_Adsa401_Adc_Acct_Qty = pnd_Adsa401_Adc_Rqst_Info.newFieldInGroup("pnd_Adsa401_Adc_Acct_Qty", "ADC-ACCT-QTY", FieldType.DECIMAL, 11,2);
        pnd_Adsa401_Adc_Acct_Actl_Amt = pnd_Adsa401_Adc_Rqst_Info.newFieldInGroup("pnd_Adsa401_Adc_Acct_Actl_Amt", "ADC-ACCT-ACTL-AMT", FieldType.DECIMAL, 
            11,2);
        pnd_Adsa401_Adc_Acct_Actl_Nbr_Units = pnd_Adsa401_Adc_Rqst_Info.newFieldInGroup("pnd_Adsa401_Adc_Acct_Actl_Nbr_Units", "ADC-ACCT-ACTL-NBR-UNITS", 
            FieldType.DECIMAL, 10,3);
        pnd_Adsa401_Adc_Grd_Mnthly_Typ = pnd_Adsa401_Adc_Rqst_Info.newFieldInGroup("pnd_Adsa401_Adc_Grd_Mnthly_Typ", "ADC-GRD-MNTHLY-TYP", FieldType.STRING, 
            1);
        pnd_Adsa401_Adc_Grd_Mnthly_Qty = pnd_Adsa401_Adc_Rqst_Info.newFieldInGroup("pnd_Adsa401_Adc_Grd_Mnthly_Qty", "ADC-GRD-MNTHLY-QTY", FieldType.DECIMAL, 
            11,2);
        pnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt = pnd_Adsa401_Adc_Rqst_Info.newFieldInGroup("pnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt", "ADC-GRD-MNTHLY-ACTL-AMT", 
            FieldType.DECIMAL, 11,2);
        pnd_Adsa401_Adc_Stndrd_Annl_Actl_Amt = pnd_Adsa401_Adc_Rqst_Info.newFieldInGroup("pnd_Adsa401_Adc_Stndrd_Annl_Actl_Amt", "ADC-STNDRD-ANNL-ACTL-AMT", 
            FieldType.DECIMAL, 11,2);
        pnd_Adsa401_Adc_Acct_Cref_Rate_Cde = pnd_Adsa401_Adc_Rqst_Info.newFieldInGroup("pnd_Adsa401_Adc_Acct_Cref_Rate_Cde", "ADC-ACCT-CREF-RATE-CDE", 
            FieldType.STRING, 2);
        pnd_Adsa401_Adc_Ivc_Amt = pnd_Adsa401_Adc_Rqst_Info.newFieldInGroup("pnd_Adsa401_Adc_Ivc_Amt", "ADC-IVC-AMT", FieldType.DECIMAL, 9,2);
        pnd_Adsa401_Adc_Acct_Opn_Accum_Amt = pnd_Adsa401_Adc_Rqst_Info.newFieldInGroup("pnd_Adsa401_Adc_Acct_Opn_Accum_Amt", "ADC-ACCT-OPN-ACCUM-AMT", 
            FieldType.DECIMAL, 11,2);
        pnd_Adsa401_Adc_Acct_Opn_Nbr_Units = pnd_Adsa401_Adc_Rqst_Info.newFieldInGroup("pnd_Adsa401_Adc_Acct_Opn_Nbr_Units", "ADC-ACCT-OPN-NBR-UNITS", 
            FieldType.DECIMAL, 10,3);
        pnd_Adsa401_Adc_Err_Rpt_Data = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newGroupArrayInGroup("pnd_Adsa401_Adc_Err_Rpt_Data", "ADC-ERR-RPT-DATA", new 
            DbsArrayController(1,10));
        pnd_Adsa401_Adc_Err_Cde = pnd_Adsa401_Adc_Err_Rpt_Data.newFieldInGroup("pnd_Adsa401_Adc_Err_Cde", "ADC-ERR-CDE", FieldType.STRING, 3);
        pnd_Adsa401_Adc_Err_Acct_Cde = pnd_Adsa401_Adc_Err_Rpt_Data.newFieldInGroup("pnd_Adsa401_Adc_Err_Acct_Cde", "ADC-ERR-ACCT-CDE", FieldType.STRING, 
            1);
        pnd_Adsa401_Adc_Err_Msg = pnd_Adsa401_Adc_Err_Rpt_Data.newFieldInGroup("pnd_Adsa401_Adc_Err_Msg", "ADC-ERR-MSG", FieldType.STRING, 30);
        pnd_Adsa401_Adc_Trk_Ro_Ivc = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa401_Adc_Trk_Ro_Ivc", "ADC-TRK-RO-IVC", FieldType.STRING, 
            1);
        pnd_Adsa401_Adc_Dtl_Tiaa_Data = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newGroupArrayInGroup("pnd_Adsa401_Adc_Dtl_Tiaa_Data", "ADC-DTL-TIAA-DATA", 
            new DbsArrayController(1,250));
        pnd_Adsa401_Adc_Dtl_Tiaa_Rate_Cde = pnd_Adsa401_Adc_Dtl_Tiaa_Data.newFieldInGroup("pnd_Adsa401_Adc_Dtl_Tiaa_Rate_Cde", "ADC-DTL-TIAA-RATE-CDE", 
            FieldType.STRING, 2);
        pnd_Adsa401_Adc_Dtl_Tiaa_Actl_Amt = pnd_Adsa401_Adc_Dtl_Tiaa_Data.newFieldInGroup("pnd_Adsa401_Adc_Dtl_Tiaa_Actl_Amt", "ADC-DTL-TIAA-ACTL-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Adsa401_Adc_Dtl_Tiaa_Grd_Actl_Amt = pnd_Adsa401_Adc_Dtl_Tiaa_Data.newFieldInGroup("pnd_Adsa401_Adc_Dtl_Tiaa_Grd_Actl_Amt", "ADC-DTL-TIAA-GRD-ACTL-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Adsa401_Adc_Dtl_Tiaa_Stndrd_Actl_Amt = pnd_Adsa401_Adc_Dtl_Tiaa_Data.newFieldInGroup("pnd_Adsa401_Adc_Dtl_Tiaa_Stndrd_Actl_Amt", "ADC-DTL-TIAA-STNDRD-ACTL-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Adsa401_Adc_Dtl_Tiaa_Ia_Rate_Cde = pnd_Adsa401_Adc_Dtl_Tiaa_Data.newFieldInGroup("pnd_Adsa401_Adc_Dtl_Tiaa_Ia_Rate_Cde", "ADC-DTL-TIAA-IA-RATE-CDE", 
            FieldType.STRING, 2);
        pnd_Adsa401_Adc_Dtl_Tiaa_Opn_Accum_Amt = pnd_Adsa401_Adc_Dtl_Tiaa_Data.newFieldInGroup("pnd_Adsa401_Adc_Dtl_Tiaa_Opn_Accum_Amt", "ADC-DTL-TIAA-OPN-ACCUM-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Adsa401_Adc_Acct_Info = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newGroupArrayInGroup("pnd_Adsa401_Adc_Acct_Info", "ADC-ACCT-INFO", new DbsArrayController(1,
            20));
        pnd_Adsa401_Adc_Acct_Aftr_Tax_Amt = pnd_Adsa401_Adc_Acct_Info.newFieldInGroup("pnd_Adsa401_Adc_Acct_Aftr_Tax_Amt", "ADC-ACCT-AFTR-TAX-AMT", FieldType.DECIMAL, 
            11,2);
        pnd_Adsa401_Adc_Sttl_Ivc_Amt = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa401_Adc_Sttl_Ivc_Amt", "ADC-STTL-IVC-AMT", FieldType.DECIMAL, 
            11,2);
        pnd_Adsa401_Adc_Sub_Plan_Nbr = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa401_Adc_Sub_Plan_Nbr", "ADC-SUB-PLAN-NBR", FieldType.STRING, 
            6);
        pnd_Adsa401_Adc_Invstmt_Info = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newGroupArrayInGroup("pnd_Adsa401_Adc_Invstmt_Info", "ADC-INVSTMT-INFO", new 
            DbsArrayController(1,20));
        pnd_Adsa401_Adc_Invstmt_Id = pnd_Adsa401_Adc_Invstmt_Info.newFieldInGroup("pnd_Adsa401_Adc_Invstmt_Id", "ADC-INVSTMT-ID", FieldType.STRING, 2);
        pnd_Adsa401_Adc_Tpa_Guarant_Commut_Info = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newGroupArrayInGroup("pnd_Adsa401_Adc_Tpa_Guarant_Commut_Info", 
            "ADC-TPA-GUARANT-COMMUT-INFO", new DbsArrayController(1,250));
        pnd_Adsa401_Adc_Tpa_Guarant_Commut_Amt = pnd_Adsa401_Adc_Tpa_Guarant_Commut_Info.newFieldInGroup("pnd_Adsa401_Adc_Tpa_Guarant_Commut_Amt", "ADC-TPA-GUARANT-COMMUT-AMT", 
            FieldType.DECIMAL, 11,2);
        pnd_Adsa401_Adc_Tpa_Guarant_Commut_Grd_Amt = pnd_Adsa401_Adc_Tpa_Guarant_Commut_Info.newFieldInGroup("pnd_Adsa401_Adc_Tpa_Guarant_Commut_Grd_Amt", 
            "ADC-TPA-GUARANT-COMMUT-GRD-AMT", FieldType.DECIMAL, 11,2);
        pnd_Adsa401_Adc_Tpa_Guarant_Commut_Std_Amt = pnd_Adsa401_Adc_Tpa_Guarant_Commut_Info.newFieldInGroup("pnd_Adsa401_Adc_Tpa_Guarant_Commut_Std_Amt", 
            "ADC-TPA-GUARANT-COMMUT-STD-AMT", FieldType.DECIMAL, 11,2);
        pnd_Adsa401_Adc_Orig_Lob_Ind = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa401_Adc_Orig_Lob_Ind", "ADC-ORIG-LOB-IND", FieldType.STRING, 
            1);
        pnd_Adsa401_Adc_Tpa_Paymnt_Cnt = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldInGroup("pnd_Adsa401_Adc_Tpa_Paymnt_Cnt", "ADC-TPA-PAYMNT-CNT", FieldType.NUMERIC, 
            2);
        pnd_Adsa401_Adc_T395_Ticker = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldArrayInGroup("pnd_Adsa401_Adc_T395_Ticker", "ADC-T395-TICKER", FieldType.STRING, 
            10, new DbsArrayController(1,14));
        pnd_Adsa401_Adc_T395_Fund_Id = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldArrayInGroup("pnd_Adsa401_Adc_T395_Fund_Id", "ADC-T395-FUND-ID", FieldType.STRING, 
            2, new DbsArrayController(1,14));
        pnd_Adsa401_Adc_Inv_Short_Name = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldArrayInGroup("pnd_Adsa401_Adc_Inv_Short_Name", "ADC-INV-SHORT-NAME", 
            FieldType.STRING, 5, new DbsArrayController(1,20));
        pnd_Adsa401_Adc_Contract_Id = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldArrayInGroup("pnd_Adsa401_Adc_Contract_Id", "ADC-CONTRACT-ID", FieldType.NUMERIC, 
            11, new DbsArrayController(1,250));
        pnd_Adsa401_Adc_Invstmnt_Grpng_Id = pnd_Adsa401_Pnd_Adsa401_Cntrct_Rcrd.newFieldArrayInGroup("pnd_Adsa401_Adc_Invstmnt_Grpng_Id", "ADC-INVSTMNT-GRPNG-ID", 
            FieldType.STRING, 4, new DbsArrayController(1,20));
        pnd_Adsa401_Pnd_Adsa401_Static_Variables = pnd_Adsa401.newGroupInGroup("pnd_Adsa401_Pnd_Adsa401_Static_Variables", "#ADSA401-STATIC-VARIABLES");
        pnd_Adsa401_Pnd_Cics_System_Name = pnd_Adsa401_Pnd_Adsa401_Static_Variables.newFieldInGroup("pnd_Adsa401_Pnd_Cics_System_Name", "#CICS-SYSTEM-NAME", 
            FieldType.STRING, 4);
        pnd_Adsa401_Pnd_Fund_Cnt = pnd_Adsa401_Pnd_Adsa401_Static_Variables.newFieldInGroup("pnd_Adsa401_Pnd_Fund_Cnt", "#FUND-CNT", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Adsa401_Pnd_Tiaa_Rate_Cnt = pnd_Adsa401_Pnd_Adsa401_Static_Variables.newFieldInGroup("pnd_Adsa401_Pnd_Tiaa_Rate_Cnt", "#TIAA-RATE-CNT", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Adsa401_Pnd_Cntl_Bsnss_Dte = pnd_Adsa401_Pnd_Adsa401_Static_Variables.newFieldInGroup("pnd_Adsa401_Pnd_Cntl_Bsnss_Dte", "#CNTL-BSNSS-DTE", 
            FieldType.DATE);
        pnd_Adsa401_Pnd_Cntl_Bsnss_Tmrrw_Dte = pnd_Adsa401_Pnd_Adsa401_Static_Variables.newFieldInGroup("pnd_Adsa401_Pnd_Cntl_Bsnss_Tmrrw_Dte", "#CNTL-BSNSS-TMRRW-DTE", 
            FieldType.DATE);
        pnd_Adsa401_Pnd_Adsa401_Variables = pnd_Adsa401.newGroupInGroup("pnd_Adsa401_Pnd_Adsa401_Variables", "#ADSA401-VARIABLES");
        pnd_Adsa401_Pnd_Current_Cntrct_Prtcpnt_Info = pnd_Adsa401_Pnd_Adsa401_Variables.newGroupInGroup("pnd_Adsa401_Pnd_Current_Cntrct_Prtcpnt_Info", 
            "#CURRENT-CNTRCT-PRTCPNT-INFO");
        pnd_Adsa401_Pnd_Cur_Cntrct_Isn = pnd_Adsa401_Pnd_Current_Cntrct_Prtcpnt_Info.newFieldInGroup("pnd_Adsa401_Pnd_Cur_Cntrct_Isn", "#CUR-CNTRCT-ISN", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Adsa401_Pnd_Cur_Prtcpnt_Isn = pnd_Adsa401_Pnd_Current_Cntrct_Prtcpnt_Info.newFieldInGroup("pnd_Adsa401_Pnd_Cur_Prtcpnt_Isn", "#CUR-PRTCPNT-ISN", 
            FieldType.PACKED_DECIMAL, 8);
        pnd_Adsa401_Pnd_Cur_Iarslt_Isn = pnd_Adsa401_Pnd_Current_Cntrct_Prtcpnt_Info.newFieldArrayInGroup("pnd_Adsa401_Pnd_Cur_Iarslt_Isn", "#CUR-IARSLT-ISN", 
            FieldType.PACKED_DECIMAL, 8, new DbsArrayController(1,2));
        pnd_Adsa401_Pnd_Ia_Cntrct_Lob = pnd_Adsa401_Pnd_Current_Cntrct_Prtcpnt_Info.newFieldInGroup("pnd_Adsa401_Pnd_Ia_Cntrct_Lob", "#IA-CNTRCT-LOB", 
            FieldType.STRING, 10);
        pnd_Adsa401_Pnd_Ia_Cntrct_Sub_Lob = pnd_Adsa401_Pnd_Current_Cntrct_Prtcpnt_Info.newFieldInGroup("pnd_Adsa401_Pnd_Ia_Cntrct_Sub_Lob", "#IA-CNTRCT-SUB-LOB", 
            FieldType.STRING, 12);
        pnd_Adsa401_Pnd_Cntrct_Lob = pnd_Adsa401_Pnd_Current_Cntrct_Prtcpnt_Info.newFieldInGroup("pnd_Adsa401_Pnd_Cntrct_Lob", "#CNTRCT-LOB", FieldType.STRING, 
            10);
        pnd_Adsa401_Pnd_Cntrct_Sub_Lob = pnd_Adsa401_Pnd_Current_Cntrct_Prtcpnt_Info.newFieldInGroup("pnd_Adsa401_Pnd_Cntrct_Sub_Lob", "#CNTRCT-SUB-LOB", 
            FieldType.STRING, 12);
        pnd_Adsa401_Pnd_Error_Status = pnd_Adsa401_Pnd_Current_Cntrct_Prtcpnt_Info.newFieldInGroup("pnd_Adsa401_Pnd_Error_Status", "#ERROR-STATUS", FieldType.STRING, 
            3);
        pnd_Adsa401_Pnd_Addl_Cntrct_Info = pnd_Adsa401_Pnd_Adsa401_Variables.newGroupInGroup("pnd_Adsa401_Pnd_Addl_Cntrct_Info", "#ADDL-CNTRCT-INFO");
        pnd_Adsa401_Pnd_Check_Rule_Elgblty = pnd_Adsa401_Pnd_Addl_Cntrct_Info.newFieldInGroup("pnd_Adsa401_Pnd_Check_Rule_Elgblty", "#CHECK-RULE-ELGBLTY", 
            FieldType.STRING, 1);
        pnd_Adsa401_Pnd_Total_Tiaa_Ivc_Amount = pnd_Adsa401_Pnd_Addl_Cntrct_Info.newFieldInGroup("pnd_Adsa401_Pnd_Total_Tiaa_Ivc_Amount", "#TOTAL-TIAA-IVC-AMOUNT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Adsa401_Pnd_Total_Cref_Ivc_Amount = pnd_Adsa401_Pnd_Addl_Cntrct_Info.newFieldInGroup("pnd_Adsa401_Pnd_Total_Cref_Ivc_Amount", "#TOTAL-CREF-IVC-AMOUNT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Adsa401_Pnd_Ia_Origin_Code = pnd_Adsa401_Pnd_Addl_Cntrct_Info.newFieldInGroup("pnd_Adsa401_Pnd_Ia_Origin_Code", "#IA-ORIGIN-CODE", FieldType.NUMERIC, 
            2);

        dbsRecord.reset();
    }

    // Constructors
    public PdaAdsa401(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

