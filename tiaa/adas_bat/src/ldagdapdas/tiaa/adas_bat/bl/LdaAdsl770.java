/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:20 PM
**        * FROM NATURAL LDA     : ADSL770
************************************************************
**        * FILE NAME            : LdaAdsl770.java
**        * CLASS NAME           : LdaAdsl770
**        * INSTANCE NAME        : LdaAdsl770
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdsl770 extends DbsRecord
{
    // Properties
    private DbsField pnd_Cref_Rec_Cnt;
    private DbsField pnd_Cref_Rec_Cnt_Roth;
    private DbsField pnd_Real_Rec_Cnt;
    private DbsField pnd_Sub_Total_Cref_Cntrcts;
    private DbsField pnd_Final_Total_Cref_Cntrcts;
    private DbsGroup pnd_Cref_Minor_Amt_Totals;
    private DbsField pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Prod;
    private DbsField pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Rate;
    private DbsField pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Method;
    private DbsField pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Accum_Amt;
    private DbsField pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Amt;
    private DbsField pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Rtb_Amt;
    private DbsField pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Rtb_Amt;
    private DbsGroup pnd_Cref_Minor_Unit_Totals;
    private DbsField pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Da_Accum_Units;
    private DbsField pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Units;
    private DbsField pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Rtb_Units;
    private DbsField pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Rtb_Units;
    private DbsGroup pnd_Cref_Sub_Total;
    private DbsField pnd_Cref_Sub_Total_Pnd_Cref_Sub_Da_Accum_Total;
    private DbsField pnd_Cref_Sub_Total_Pnd_Cref_Sub_Settled_Total;
    private DbsField pnd_Cref_Sub_Total_Pnd_Cref_Sub_Rtb_Total;
    private DbsField pnd_Cref_Sub_Total_Pnd_Cref_Sub_Settled_Rtb_Total;
    private DbsGroup pnd_Cref_Tot_Settlement;
    private DbsField pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Da_Accum_Total;
    private DbsField pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Total;
    private DbsField pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Rtb_Total;
    private DbsField pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Rtb_Total;
    private DbsGroup pnd_Cref_Final_Total;
    private DbsField pnd_Cref_Final_Total_Pnd_Cref_Final_Prod;
    private DbsField pnd_Cref_Final_Total_Pnd_Cref_Final_Da_Rate;
    private DbsField pnd_Cref_Final_Total_Pnd_Cref_Final_Da_Method;
    private DbsField pnd_Cref_Final_Total_Pnd_Cref_Da_Accum_Final_Total;
    private DbsField pnd_Cref_Final_Total_Pnd_Cref_Settled_Final_Total;
    private DbsField pnd_Cref_Final_Total_Pnd_Cref_Rtb_Final_Total;
    private DbsField pnd_Cref_Final_Total_Pnd_Cref_Settled_Rtb_Final_Total;
    private DbsGroup pnd_Cref_Final_Units;
    private DbsField pnd_Cref_Final_Units_Pnd_Cref_Da_Accum_Final_Units;
    private DbsField pnd_Cref_Final_Units_Pnd_Cref_Settled_Final_Units;
    private DbsField pnd_Cref_Final_Units_Pnd_Cref_Rtb_Final_Units;
    private DbsField pnd_Cref_Final_Units_Pnd_Cref_Settled_Rtb_Final_Units;
    private DbsGroup pnd_C_Save_Minor_Amt_Totals;
    private DbsGroup pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Area_1_Minor_Amt_Totals;
    private DbsField pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Prod;
    private DbsField pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Type;
    private DbsField pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Qty;
    private DbsField pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Rate;
    private DbsField pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Method;
    private DbsField pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Accum_Amt;
    private DbsField pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Amt;
    private DbsField pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Rtb_Amt;
    private DbsField pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Rtb_Amt;
    private DbsField pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Mnthly_Typ;
    private DbsField pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Mnthly_Qty;
    private DbsGroup pnd_C_Save_Minor_Unit_Totals;
    private DbsGroup pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Area_1_Minor_Unit_Totals;
    private DbsField pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Da_Accum_Units;
    private DbsField pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Settled_Units;
    private DbsField pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Rtb_Units;
    private DbsField pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Settled_Rtb_Units;
    private DbsGroup pnd_C_Save_Final_Total_Amts;
    private DbsGroup pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Area_1_Final_Total_Amt;
    private DbsField pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Cref_Final_Prod;
    private DbsField pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Rate;
    private DbsField pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Method;
    private DbsField pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Accum_Amt;
    private DbsField pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Settled_Amt;
    private DbsField pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Rtb_Amt;
    private DbsField pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Settled_Rtb_Amt;
    private DbsGroup pnd_C_Save_Final_Total_Units;
    private DbsGroup pnd_C_Save_Final_Total_Units_Pnd_C_Save_Area_1_Final_Total_Units;
    private DbsField pnd_C_Save_Final_Total_Units_Pnd_C_Save_Da_Final_Accum_Units;
    private DbsField pnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Settled_Units;
    private DbsField pnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Rtb_Units;
    private DbsField pnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Settled_Rtb_Units;
    private DbsGroup pnd_Cref_Monthly_Totals;
    private DbsField pnd_Cref_Monthly_Totals_Pnd_Cref_Prod_Monthly;
    private DbsField pnd_Cref_Monthly_Totals_Pnd_Cref_Da_Rate_Monthly;
    private DbsField pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Rate_Monthly;
    private DbsField pnd_Cref_Monthly_Totals_Pnd_Cref_Settled_Monthly_Amt;
    private DbsField pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Units;
    private DbsField pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Payments;
    private DbsField pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Unit_Val;
    private DbsGroup pnd_C_Save_Monthly_Method_Totals;
    private DbsGroup pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Monthly_Totals;
    private DbsField pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Prod_Monthly;
    private DbsField pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Da_Rate_Monthly;
    private DbsField pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Rate_Monthly;
    private DbsField pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Settled_Monthly_Amt;
    private DbsField pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Units;
    private DbsField pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Payments;
    private DbsField pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Unit_Val;
    private DbsGroup pnd_Cref_Monthly_Method_Final_Tot;
    private DbsField pnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Settled_Monthly_Final_Amt;
    private DbsField pnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Units;
    private DbsField pnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Payments;
    private DbsField pnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Unit_Val;
    private DbsGroup pnd_Cref_Annually_Method_Totals;
    private DbsField pnd_Cref_Annually_Method_Totals_Pnd_Cref_Prod_Annually;
    private DbsField pnd_Cref_Annually_Method_Totals_Pnd_Cref_Da_Rate_Annually;
    private DbsField pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Rate_Annually;
    private DbsField pnd_Cref_Annually_Method_Totals_Pnd_Cref_Settled_Annually_Amt;
    private DbsField pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Units;
    private DbsField pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Payments;
    private DbsField pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Unit_Val;
    private DbsGroup pnd_C_Save_Annually_Method_Totals;
    private DbsGroup pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Annually_Totals;
    private DbsField pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Prod_Annually;
    private DbsField pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Da_Rate_Annually;
    private DbsField pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Rate_Annually;
    private DbsField pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Settled_Annually_Amt;
    private DbsField pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Units;
    private DbsField pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Payments;
    private DbsField pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Unit_Val;
    private DbsGroup pnd_Cref_Annually_Method_Final_Tot;
    private DbsField pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Settled_Annually_Final_Amt;
    private DbsField pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Units;
    private DbsField pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Payments;
    private DbsField pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Unit_Val;
    private DbsGroup pnd_Cref_Header_Amt;
    private DbsField pnd_Cref_Header_Amt_Pnd_Cref_Header_Account;
    private DbsField pnd_Cref_Header_Amt_Pnd_Cref_Header_Settle_Amt;
    private DbsField pnd_Cref_Header_Amt_Pnd_Cref_Header_Settle_Typ;
    private DbsField pnd_Cref_Header_Amt_Pnd_Cref_Header_Rtb_Amt;
    private DbsField pnd_Cref_Header_Amt_Pnd_Cref_Header_Rtb_Typ;
    private DbsField pnd_Cref_Header_Amt_Pnd_Cref_Header_Grd_Amt;
    private DbsField pnd_Cref_Header_Amt_Pnd_Cref_Header_Grd_Typ;
    private DbsField pnd_Cref_Header_Amt_Pnd_Cref_Header_Cref_Mnthly_Units;
    private DbsGroup pnd_Cref_Header_Settle_Amounts;
    private DbsField pnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Settle_Amount;
    private DbsField pnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Rtb_Settle_Amount;
    private DbsGroup pnd_Cref_Ivc_Amounts;
    private DbsField pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Total;
    private DbsField pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Rtb;
    private DbsField pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Annuity;
    private DbsGroup pnd_Cref_Data_Entry_Info;
    private DbsField pnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_First_Entered_By;
    private DbsField pnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_First_Verified_By;
    private DbsField pnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_Last_Updated_By;
    private DbsField pnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_Last_Verified_By;

    public DbsField getPnd_Cref_Rec_Cnt() { return pnd_Cref_Rec_Cnt; }

    public DbsField getPnd_Cref_Rec_Cnt_Roth() { return pnd_Cref_Rec_Cnt_Roth; }

    public DbsField getPnd_Real_Rec_Cnt() { return pnd_Real_Rec_Cnt; }

    public DbsField getPnd_Sub_Total_Cref_Cntrcts() { return pnd_Sub_Total_Cref_Cntrcts; }

    public DbsField getPnd_Final_Total_Cref_Cntrcts() { return pnd_Final_Total_Cref_Cntrcts; }

    public DbsGroup getPnd_Cref_Minor_Amt_Totals() { return pnd_Cref_Minor_Amt_Totals; }

    public DbsField getPnd_Cref_Minor_Amt_Totals_Pnd_Cref_Prod() { return pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Prod; }

    public DbsField getPnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Rate() { return pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Rate; }

    public DbsField getPnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Method() { return pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Method; }

    public DbsField getPnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Accum_Amt() { return pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Accum_Amt; }

    public DbsField getPnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Amt() { return pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Amt; }

    public DbsField getPnd_Cref_Minor_Amt_Totals_Pnd_Cref_Rtb_Amt() { return pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Rtb_Amt; }

    public DbsField getPnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Rtb_Amt() { return pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Rtb_Amt; }

    public DbsGroup getPnd_Cref_Minor_Unit_Totals() { return pnd_Cref_Minor_Unit_Totals; }

    public DbsField getPnd_Cref_Minor_Unit_Totals_Pnd_Cref_Da_Accum_Units() { return pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Da_Accum_Units; }

    public DbsField getPnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Units() { return pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Units; }

    public DbsField getPnd_Cref_Minor_Unit_Totals_Pnd_Cref_Rtb_Units() { return pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Rtb_Units; }

    public DbsField getPnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Rtb_Units() { return pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Rtb_Units; }

    public DbsGroup getPnd_Cref_Sub_Total() { return pnd_Cref_Sub_Total; }

    public DbsField getPnd_Cref_Sub_Total_Pnd_Cref_Sub_Da_Accum_Total() { return pnd_Cref_Sub_Total_Pnd_Cref_Sub_Da_Accum_Total; }

    public DbsField getPnd_Cref_Sub_Total_Pnd_Cref_Sub_Settled_Total() { return pnd_Cref_Sub_Total_Pnd_Cref_Sub_Settled_Total; }

    public DbsField getPnd_Cref_Sub_Total_Pnd_Cref_Sub_Rtb_Total() { return pnd_Cref_Sub_Total_Pnd_Cref_Sub_Rtb_Total; }

    public DbsField getPnd_Cref_Sub_Total_Pnd_Cref_Sub_Settled_Rtb_Total() { return pnd_Cref_Sub_Total_Pnd_Cref_Sub_Settled_Rtb_Total; }

    public DbsGroup getPnd_Cref_Tot_Settlement() { return pnd_Cref_Tot_Settlement; }

    public DbsField getPnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Da_Accum_Total() { return pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Da_Accum_Total; }

    public DbsField getPnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Total() { return pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Total; }

    public DbsField getPnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Rtb_Total() { return pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Rtb_Total; }

    public DbsField getPnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Rtb_Total() { return pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Rtb_Total; }

    public DbsGroup getPnd_Cref_Final_Total() { return pnd_Cref_Final_Total; }

    public DbsField getPnd_Cref_Final_Total_Pnd_Cref_Final_Prod() { return pnd_Cref_Final_Total_Pnd_Cref_Final_Prod; }

    public DbsField getPnd_Cref_Final_Total_Pnd_Cref_Final_Da_Rate() { return pnd_Cref_Final_Total_Pnd_Cref_Final_Da_Rate; }

    public DbsField getPnd_Cref_Final_Total_Pnd_Cref_Final_Da_Method() { return pnd_Cref_Final_Total_Pnd_Cref_Final_Da_Method; }

    public DbsField getPnd_Cref_Final_Total_Pnd_Cref_Da_Accum_Final_Total() { return pnd_Cref_Final_Total_Pnd_Cref_Da_Accum_Final_Total; }

    public DbsField getPnd_Cref_Final_Total_Pnd_Cref_Settled_Final_Total() { return pnd_Cref_Final_Total_Pnd_Cref_Settled_Final_Total; }

    public DbsField getPnd_Cref_Final_Total_Pnd_Cref_Rtb_Final_Total() { return pnd_Cref_Final_Total_Pnd_Cref_Rtb_Final_Total; }

    public DbsField getPnd_Cref_Final_Total_Pnd_Cref_Settled_Rtb_Final_Total() { return pnd_Cref_Final_Total_Pnd_Cref_Settled_Rtb_Final_Total; }

    public DbsGroup getPnd_Cref_Final_Units() { return pnd_Cref_Final_Units; }

    public DbsField getPnd_Cref_Final_Units_Pnd_Cref_Da_Accum_Final_Units() { return pnd_Cref_Final_Units_Pnd_Cref_Da_Accum_Final_Units; }

    public DbsField getPnd_Cref_Final_Units_Pnd_Cref_Settled_Final_Units() { return pnd_Cref_Final_Units_Pnd_Cref_Settled_Final_Units; }

    public DbsField getPnd_Cref_Final_Units_Pnd_Cref_Rtb_Final_Units() { return pnd_Cref_Final_Units_Pnd_Cref_Rtb_Final_Units; }

    public DbsField getPnd_Cref_Final_Units_Pnd_Cref_Settled_Rtb_Final_Units() { return pnd_Cref_Final_Units_Pnd_Cref_Settled_Rtb_Final_Units; }

    public DbsGroup getPnd_C_Save_Minor_Amt_Totals() { return pnd_C_Save_Minor_Amt_Totals; }

    public DbsGroup getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Area_1_Minor_Amt_Totals() { return pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Area_1_Minor_Amt_Totals; 
        }

    public DbsField getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Prod() { return pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Prod; }

    public DbsField getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Type() { return pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Type; }

    public DbsField getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Qty() { return pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Qty; }

    public DbsField getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Rate() { return pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Rate; }

    public DbsField getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Method() { return pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Method; }

    public DbsField getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Accum_Amt() { return pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Accum_Amt; }

    public DbsField getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Amt() { return pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Amt; }

    public DbsField getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Rtb_Amt() { return pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Rtb_Amt; }

    public DbsField getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Rtb_Amt() { return pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Rtb_Amt; }

    public DbsField getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Mnthly_Typ() { return pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Mnthly_Typ; }

    public DbsField getPnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Mnthly_Qty() { return pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Mnthly_Qty; }

    public DbsGroup getPnd_C_Save_Minor_Unit_Totals() { return pnd_C_Save_Minor_Unit_Totals; }

    public DbsGroup getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Area_1_Minor_Unit_Totals() { return pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Area_1_Minor_Unit_Totals; 
        }

    public DbsField getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Da_Accum_Units() { return pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Da_Accum_Units; }

    public DbsField getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Settled_Units() { return pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Settled_Units; }

    public DbsField getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Rtb_Units() { return pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Rtb_Units; }

    public DbsField getPnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Settled_Rtb_Units() { return pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Settled_Rtb_Units; 
        }

    public DbsGroup getPnd_C_Save_Final_Total_Amts() { return pnd_C_Save_Final_Total_Amts; }

    public DbsGroup getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Area_1_Final_Total_Amt() { return pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Area_1_Final_Total_Amt; 
        }

    public DbsField getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Cref_Final_Prod() { return pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Cref_Final_Prod; }

    public DbsField getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Rate() { return pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Rate; }

    public DbsField getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Method() { return pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Method; }

    public DbsField getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Accum_Amt() { return pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Accum_Amt; 
        }

    public DbsField getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Settled_Amt() { return pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Settled_Amt; }

    public DbsField getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Rtb_Amt() { return pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Rtb_Amt; }

    public DbsField getPnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Settled_Rtb_Amt() { return pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Settled_Rtb_Amt; 
        }

    public DbsGroup getPnd_C_Save_Final_Total_Units() { return pnd_C_Save_Final_Total_Units; }

    public DbsGroup getPnd_C_Save_Final_Total_Units_Pnd_C_Save_Area_1_Final_Total_Units() { return pnd_C_Save_Final_Total_Units_Pnd_C_Save_Area_1_Final_Total_Units; 
        }

    public DbsField getPnd_C_Save_Final_Total_Units_Pnd_C_Save_Da_Final_Accum_Units() { return pnd_C_Save_Final_Total_Units_Pnd_C_Save_Da_Final_Accum_Units; 
        }

    public DbsField getPnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Settled_Units() { return pnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Settled_Units; 
        }

    public DbsField getPnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Rtb_Units() { return pnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Rtb_Units; }

    public DbsField getPnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Settled_Rtb_Units() { return pnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Settled_Rtb_Units; 
        }

    public DbsGroup getPnd_Cref_Monthly_Totals() { return pnd_Cref_Monthly_Totals; }

    public DbsField getPnd_Cref_Monthly_Totals_Pnd_Cref_Prod_Monthly() { return pnd_Cref_Monthly_Totals_Pnd_Cref_Prod_Monthly; }

    public DbsField getPnd_Cref_Monthly_Totals_Pnd_Cref_Da_Rate_Monthly() { return pnd_Cref_Monthly_Totals_Pnd_Cref_Da_Rate_Monthly; }

    public DbsField getPnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Rate_Monthly() { return pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Rate_Monthly; }

    public DbsField getPnd_Cref_Monthly_Totals_Pnd_Cref_Settled_Monthly_Amt() { return pnd_Cref_Monthly_Totals_Pnd_Cref_Settled_Monthly_Amt; }

    public DbsField getPnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Units() { return pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Units; }

    public DbsField getPnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Payments() { return pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Payments; }

    public DbsField getPnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Unit_Val() { return pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Unit_Val; }

    public DbsGroup getPnd_C_Save_Monthly_Method_Totals() { return pnd_C_Save_Monthly_Method_Totals; }

    public DbsGroup getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Monthly_Totals() { return pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Monthly_Totals; 
        }

    public DbsField getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Prod_Monthly() { return pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Prod_Monthly; }

    public DbsField getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Da_Rate_Monthly() { return pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Da_Rate_Monthly; 
        }

    public DbsField getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Rate_Monthly() { return pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Rate_Monthly; 
        }

    public DbsField getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Settled_Monthly_Amt() { return pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Settled_Monthly_Amt; 
        }

    public DbsField getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Units() { return pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Units; 
        }

    public DbsField getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Payments() { return pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Payments; 
        }

    public DbsField getPnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Unit_Val() { return pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Unit_Val; 
        }

    public DbsGroup getPnd_Cref_Monthly_Method_Final_Tot() { return pnd_Cref_Monthly_Method_Final_Tot; }

    public DbsField getPnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Settled_Monthly_Final_Amt() { return pnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Settled_Monthly_Final_Amt; 
        }

    public DbsField getPnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Units() { return pnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Units; 
        }

    public DbsField getPnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Payments() { return pnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Payments; 
        }

    public DbsField getPnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Unit_Val() { return pnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Unit_Val; 
        }

    public DbsGroup getPnd_Cref_Annually_Method_Totals() { return pnd_Cref_Annually_Method_Totals; }

    public DbsField getPnd_Cref_Annually_Method_Totals_Pnd_Cref_Prod_Annually() { return pnd_Cref_Annually_Method_Totals_Pnd_Cref_Prod_Annually; }

    public DbsField getPnd_Cref_Annually_Method_Totals_Pnd_Cref_Da_Rate_Annually() { return pnd_Cref_Annually_Method_Totals_Pnd_Cref_Da_Rate_Annually; 
        }

    public DbsField getPnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Rate_Annually() { return pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Rate_Annually; 
        }

    public DbsField getPnd_Cref_Annually_Method_Totals_Pnd_Cref_Settled_Annually_Amt() { return pnd_Cref_Annually_Method_Totals_Pnd_Cref_Settled_Annually_Amt; 
        }

    public DbsField getPnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Units() { return pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Units; 
        }

    public DbsField getPnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Payments() { return pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Payments; 
        }

    public DbsField getPnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Unit_Val() { return pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Unit_Val; 
        }

    public DbsGroup getPnd_C_Save_Annually_Method_Totals() { return pnd_C_Save_Annually_Method_Totals; }

    public DbsGroup getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Annually_Totals() { return pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Annually_Totals; 
        }

    public DbsField getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Prod_Annually() { return pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Prod_Annually; 
        }

    public DbsField getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Da_Rate_Annually() { return pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Da_Rate_Annually; 
        }

    public DbsField getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Rate_Annually() { return pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Rate_Annually; 
        }

    public DbsField getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Settled_Annually_Amt() { return pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Settled_Annually_Amt; 
        }

    public DbsField getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Units() { return pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Units; 
        }

    public DbsField getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Payments() { return pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Payments; 
        }

    public DbsField getPnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Unit_Val() { return pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Unit_Val; 
        }

    public DbsGroup getPnd_Cref_Annually_Method_Final_Tot() { return pnd_Cref_Annually_Method_Final_Tot; }

    public DbsField getPnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Settled_Annually_Final_Amt() { return pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Settled_Annually_Final_Amt; 
        }

    public DbsField getPnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Units() { return pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Units; 
        }

    public DbsField getPnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Payments() { return pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Payments; 
        }

    public DbsField getPnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Unit_Val() { return pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Unit_Val; 
        }

    public DbsGroup getPnd_Cref_Header_Amt() { return pnd_Cref_Header_Amt; }

    public DbsField getPnd_Cref_Header_Amt_Pnd_Cref_Header_Account() { return pnd_Cref_Header_Amt_Pnd_Cref_Header_Account; }

    public DbsField getPnd_Cref_Header_Amt_Pnd_Cref_Header_Settle_Amt() { return pnd_Cref_Header_Amt_Pnd_Cref_Header_Settle_Amt; }

    public DbsField getPnd_Cref_Header_Amt_Pnd_Cref_Header_Settle_Typ() { return pnd_Cref_Header_Amt_Pnd_Cref_Header_Settle_Typ; }

    public DbsField getPnd_Cref_Header_Amt_Pnd_Cref_Header_Rtb_Amt() { return pnd_Cref_Header_Amt_Pnd_Cref_Header_Rtb_Amt; }

    public DbsField getPnd_Cref_Header_Amt_Pnd_Cref_Header_Rtb_Typ() { return pnd_Cref_Header_Amt_Pnd_Cref_Header_Rtb_Typ; }

    public DbsField getPnd_Cref_Header_Amt_Pnd_Cref_Header_Grd_Amt() { return pnd_Cref_Header_Amt_Pnd_Cref_Header_Grd_Amt; }

    public DbsField getPnd_Cref_Header_Amt_Pnd_Cref_Header_Grd_Typ() { return pnd_Cref_Header_Amt_Pnd_Cref_Header_Grd_Typ; }

    public DbsField getPnd_Cref_Header_Amt_Pnd_Cref_Header_Cref_Mnthly_Units() { return pnd_Cref_Header_Amt_Pnd_Cref_Header_Cref_Mnthly_Units; }

    public DbsGroup getPnd_Cref_Header_Settle_Amounts() { return pnd_Cref_Header_Settle_Amounts; }

    public DbsField getPnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Settle_Amount() { return pnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Settle_Amount; 
        }

    public DbsField getPnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Rtb_Settle_Amount() { return pnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Rtb_Settle_Amount; 
        }

    public DbsGroup getPnd_Cref_Ivc_Amounts() { return pnd_Cref_Ivc_Amounts; }

    public DbsField getPnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Total() { return pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Total; }

    public DbsField getPnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Rtb() { return pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Rtb; }

    public DbsField getPnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Annuity() { return pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Annuity; }

    public DbsGroup getPnd_Cref_Data_Entry_Info() { return pnd_Cref_Data_Entry_Info; }

    public DbsField getPnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_First_Entered_By() { return pnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_First_Entered_By; }

    public DbsField getPnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_First_Verified_By() { return pnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_First_Verified_By; }

    public DbsField getPnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_Last_Updated_By() { return pnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_Last_Updated_By; }

    public DbsField getPnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_Last_Verified_By() { return pnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_Last_Verified_By; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Cref_Rec_Cnt = newFieldInRecord("pnd_Cref_Rec_Cnt", "#CREF-REC-CNT", FieldType.NUMERIC, 7);

        pnd_Cref_Rec_Cnt_Roth = newFieldInRecord("pnd_Cref_Rec_Cnt_Roth", "#CREF-REC-CNT-ROTH", FieldType.NUMERIC, 7);

        pnd_Real_Rec_Cnt = newFieldInRecord("pnd_Real_Rec_Cnt", "#REAL-REC-CNT", FieldType.NUMERIC, 7);

        pnd_Sub_Total_Cref_Cntrcts = newFieldInRecord("pnd_Sub_Total_Cref_Cntrcts", "#SUB-TOTAL-CREF-CNTRCTS", FieldType.NUMERIC, 7);

        pnd_Final_Total_Cref_Cntrcts = newFieldInRecord("pnd_Final_Total_Cref_Cntrcts", "#FINAL-TOTAL-CREF-CNTRCTS", FieldType.NUMERIC, 7);

        pnd_Cref_Minor_Amt_Totals = newGroupInRecord("pnd_Cref_Minor_Amt_Totals", "#CREF-MINOR-AMT-TOTALS");
        pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Prod = pnd_Cref_Minor_Amt_Totals.newFieldInGroup("pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Prod", "#CREF-PROD", FieldType.STRING, 
            1);
        pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Rate = pnd_Cref_Minor_Amt_Totals.newFieldInGroup("pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Rate", "#CREF-DA-RATE", 
            FieldType.STRING, 2);
        pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Method = pnd_Cref_Minor_Amt_Totals.newFieldInGroup("pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Method", "#CREF-DA-METHOD", 
            FieldType.STRING, 10);
        pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Accum_Amt = pnd_Cref_Minor_Amt_Totals.newFieldInGroup("pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Accum_Amt", 
            "#CREF-DA-ACCUM-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Amt = pnd_Cref_Minor_Amt_Totals.newFieldInGroup("pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Amt", "#CREF-SETTLED-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Rtb_Amt = pnd_Cref_Minor_Amt_Totals.newFieldInGroup("pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Rtb_Amt", "#CREF-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Rtb_Amt = pnd_Cref_Minor_Amt_Totals.newFieldInGroup("pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Rtb_Amt", 
            "#CREF-SETTLED-RTB-AMT", FieldType.PACKED_DECIMAL, 11,2);

        pnd_Cref_Minor_Unit_Totals = newGroupInRecord("pnd_Cref_Minor_Unit_Totals", "#CREF-MINOR-UNIT-TOTALS");
        pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Da_Accum_Units = pnd_Cref_Minor_Unit_Totals.newFieldInGroup("pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Da_Accum_Units", 
            "#CREF-DA-ACCUM-UNITS", FieldType.PACKED_DECIMAL, 10,3);
        pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Units = pnd_Cref_Minor_Unit_Totals.newFieldInGroup("pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Units", 
            "#CREF-SETTLED-UNITS", FieldType.PACKED_DECIMAL, 10,3);
        pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Rtb_Units = pnd_Cref_Minor_Unit_Totals.newFieldInGroup("pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Rtb_Units", "#CREF-RTB-UNITS", 
            FieldType.PACKED_DECIMAL, 10,3);
        pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Rtb_Units = pnd_Cref_Minor_Unit_Totals.newFieldInGroup("pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Rtb_Units", 
            "#CREF-SETTLED-RTB-UNITS", FieldType.PACKED_DECIMAL, 10,3);

        pnd_Cref_Sub_Total = newGroupInRecord("pnd_Cref_Sub_Total", "#CREF-SUB-TOTAL");
        pnd_Cref_Sub_Total_Pnd_Cref_Sub_Da_Accum_Total = pnd_Cref_Sub_Total.newFieldInGroup("pnd_Cref_Sub_Total_Pnd_Cref_Sub_Da_Accum_Total", "#CREF-SUB-DA-ACCUM-TOTAL", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cref_Sub_Total_Pnd_Cref_Sub_Settled_Total = pnd_Cref_Sub_Total.newFieldInGroup("pnd_Cref_Sub_Total_Pnd_Cref_Sub_Settled_Total", "#CREF-SUB-SETTLED-TOTAL", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cref_Sub_Total_Pnd_Cref_Sub_Rtb_Total = pnd_Cref_Sub_Total.newFieldInGroup("pnd_Cref_Sub_Total_Pnd_Cref_Sub_Rtb_Total", "#CREF-SUB-RTB-TOTAL", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cref_Sub_Total_Pnd_Cref_Sub_Settled_Rtb_Total = pnd_Cref_Sub_Total.newFieldInGroup("pnd_Cref_Sub_Total_Pnd_Cref_Sub_Settled_Rtb_Total", "#CREF-SUB-SETTLED-RTB-TOTAL", 
            FieldType.PACKED_DECIMAL, 11,2);

        pnd_Cref_Tot_Settlement = newGroupInRecord("pnd_Cref_Tot_Settlement", "#CREF-TOT-SETTLEMENT");
        pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Da_Accum_Total = pnd_Cref_Tot_Settlement.newFieldInGroup("pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Da_Accum_Total", 
            "#CREF-TOT-DA-ACCUM-TOTAL", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Total = pnd_Cref_Tot_Settlement.newFieldInGroup("pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Total", 
            "#CREF-TOT-SETTLED-TOTAL", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Rtb_Total = pnd_Cref_Tot_Settlement.newFieldInGroup("pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Rtb_Total", "#CREF-TOT-RTB-TOTAL", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Rtb_Total = pnd_Cref_Tot_Settlement.newFieldInGroup("pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Rtb_Total", 
            "#CREF-TOT-SETTLED-RTB-TOTAL", FieldType.PACKED_DECIMAL, 11,2);

        pnd_Cref_Final_Total = newGroupInRecord("pnd_Cref_Final_Total", "#CREF-FINAL-TOTAL");
        pnd_Cref_Final_Total_Pnd_Cref_Final_Prod = pnd_Cref_Final_Total.newFieldInGroup("pnd_Cref_Final_Total_Pnd_Cref_Final_Prod", "#CREF-FINAL-PROD", 
            FieldType.STRING, 1);
        pnd_Cref_Final_Total_Pnd_Cref_Final_Da_Rate = pnd_Cref_Final_Total.newFieldInGroup("pnd_Cref_Final_Total_Pnd_Cref_Final_Da_Rate", "#CREF-FINAL-DA-RATE", 
            FieldType.STRING, 2);
        pnd_Cref_Final_Total_Pnd_Cref_Final_Da_Method = pnd_Cref_Final_Total.newFieldInGroup("pnd_Cref_Final_Total_Pnd_Cref_Final_Da_Method", "#CREF-FINAL-DA-METHOD", 
            FieldType.STRING, 10);
        pnd_Cref_Final_Total_Pnd_Cref_Da_Accum_Final_Total = pnd_Cref_Final_Total.newFieldInGroup("pnd_Cref_Final_Total_Pnd_Cref_Da_Accum_Final_Total", 
            "#CREF-DA-ACCUM-FINAL-TOTAL", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cref_Final_Total_Pnd_Cref_Settled_Final_Total = pnd_Cref_Final_Total.newFieldInGroup("pnd_Cref_Final_Total_Pnd_Cref_Settled_Final_Total", 
            "#CREF-SETTLED-FINAL-TOTAL", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cref_Final_Total_Pnd_Cref_Rtb_Final_Total = pnd_Cref_Final_Total.newFieldInGroup("pnd_Cref_Final_Total_Pnd_Cref_Rtb_Final_Total", "#CREF-RTB-FINAL-TOTAL", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cref_Final_Total_Pnd_Cref_Settled_Rtb_Final_Total = pnd_Cref_Final_Total.newFieldInGroup("pnd_Cref_Final_Total_Pnd_Cref_Settled_Rtb_Final_Total", 
            "#CREF-SETTLED-RTB-FINAL-TOTAL", FieldType.PACKED_DECIMAL, 11,2);

        pnd_Cref_Final_Units = newGroupInRecord("pnd_Cref_Final_Units", "#CREF-FINAL-UNITS");
        pnd_Cref_Final_Units_Pnd_Cref_Da_Accum_Final_Units = pnd_Cref_Final_Units.newFieldInGroup("pnd_Cref_Final_Units_Pnd_Cref_Da_Accum_Final_Units", 
            "#CREF-DA-ACCUM-FINAL-UNITS", FieldType.PACKED_DECIMAL, 10,3);
        pnd_Cref_Final_Units_Pnd_Cref_Settled_Final_Units = pnd_Cref_Final_Units.newFieldInGroup("pnd_Cref_Final_Units_Pnd_Cref_Settled_Final_Units", 
            "#CREF-SETTLED-FINAL-UNITS", FieldType.PACKED_DECIMAL, 10,3);
        pnd_Cref_Final_Units_Pnd_Cref_Rtb_Final_Units = pnd_Cref_Final_Units.newFieldInGroup("pnd_Cref_Final_Units_Pnd_Cref_Rtb_Final_Units", "#CREF-RTB-FINAL-UNITS", 
            FieldType.PACKED_DECIMAL, 10,3);
        pnd_Cref_Final_Units_Pnd_Cref_Settled_Rtb_Final_Units = pnd_Cref_Final_Units.newFieldInGroup("pnd_Cref_Final_Units_Pnd_Cref_Settled_Rtb_Final_Units", 
            "#CREF-SETTLED-RTB-FINAL-UNITS", FieldType.PACKED_DECIMAL, 10,3);

        pnd_C_Save_Minor_Amt_Totals = newGroupInRecord("pnd_C_Save_Minor_Amt_Totals", "#C-SAVE-MINOR-AMT-TOTALS");
        pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Area_1_Minor_Amt_Totals = pnd_C_Save_Minor_Amt_Totals.newGroupArrayInGroup("pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Area_1_Minor_Amt_Totals", 
            "#C-SAVE-AREA-1-MINOR-AMT-TOTALS", new DbsArrayController(1,50));
        pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Prod = pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Area_1_Minor_Amt_Totals.newFieldInGroup("pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Prod", 
            "#C-SAVE-CREF-PROD", FieldType.STRING, 1);
        pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Type = pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Area_1_Minor_Amt_Totals.newFieldInGroup("pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Type", 
            "#C-SAVE-CREF-TYPE", FieldType.STRING, 1);
        pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Qty = pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Area_1_Minor_Amt_Totals.newFieldInGroup("pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Cref_Qty", 
            "#C-SAVE-CREF-QTY", FieldType.PACKED_DECIMAL, 11,2);
        pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Rate = pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Area_1_Minor_Amt_Totals.newFieldInGroup("pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Rate", 
            "#C-SAVE-DA-RATE", FieldType.STRING, 2);
        pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Method = pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Area_1_Minor_Amt_Totals.newFieldInGroup("pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Method", 
            "#C-SAVE-DA-METHOD", FieldType.STRING, 10);
        pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Accum_Amt = pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Area_1_Minor_Amt_Totals.newFieldInGroup("pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Da_Accum_Amt", 
            "#C-SAVE-DA-ACCUM-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Amt = pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Area_1_Minor_Amt_Totals.newFieldInGroup("pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Amt", 
            "#C-SAVE-SETTLED-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Rtb_Amt = pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Area_1_Minor_Amt_Totals.newFieldInGroup("pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Rtb_Amt", 
            "#C-SAVE-RTB-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Rtb_Amt = pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Area_1_Minor_Amt_Totals.newFieldInGroup("pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Settled_Rtb_Amt", 
            "#C-SAVE-SETTLED-RTB-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Mnthly_Typ = pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Area_1_Minor_Amt_Totals.newFieldInGroup("pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Mnthly_Typ", 
            "#C-SAVE-MNTHLY-TYP", FieldType.STRING, 1);
        pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Mnthly_Qty = pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Area_1_Minor_Amt_Totals.newFieldInGroup("pnd_C_Save_Minor_Amt_Totals_Pnd_C_Save_Mnthly_Qty", 
            "#C-SAVE-MNTHLY-QTY", FieldType.DECIMAL, 11,2);

        pnd_C_Save_Minor_Unit_Totals = newGroupInRecord("pnd_C_Save_Minor_Unit_Totals", "#C-SAVE-MINOR-UNIT-TOTALS");
        pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Area_1_Minor_Unit_Totals = pnd_C_Save_Minor_Unit_Totals.newGroupArrayInGroup("pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Area_1_Minor_Unit_Totals", 
            "#C-SAVE-AREA-1-MINOR-UNIT-TOTALS", new DbsArrayController(1,50));
        pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Da_Accum_Units = pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Area_1_Minor_Unit_Totals.newFieldInGroup("pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Da_Accum_Units", 
            "#C-SAVE-DA-ACCUM-UNITS", FieldType.PACKED_DECIMAL, 10,3);
        pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Settled_Units = pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Area_1_Minor_Unit_Totals.newFieldInGroup("pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Settled_Units", 
            "#C-SAVE-SETTLED-UNITS", FieldType.PACKED_DECIMAL, 10,3);
        pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Rtb_Units = pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Area_1_Minor_Unit_Totals.newFieldInGroup("pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Rtb_Units", 
            "#C-SAVE-RTB-UNITS", FieldType.PACKED_DECIMAL, 10,3);
        pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Settled_Rtb_Units = pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Area_1_Minor_Unit_Totals.newFieldInGroup("pnd_C_Save_Minor_Unit_Totals_Pnd_C_Save_Settled_Rtb_Units", 
            "#C-SAVE-SETTLED-RTB-UNITS", FieldType.PACKED_DECIMAL, 10,3);

        pnd_C_Save_Final_Total_Amts = newGroupInRecord("pnd_C_Save_Final_Total_Amts", "#C-SAVE-FINAL-TOTAL-AMTS");
        pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Area_1_Final_Total_Amt = pnd_C_Save_Final_Total_Amts.newGroupArrayInGroup("pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Area_1_Final_Total_Amt", 
            "#C-SAVE-AREA-1-FINAL-TOTAL-AMT", new DbsArrayController(1,80));
        pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Cref_Final_Prod = pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Area_1_Final_Total_Amt.newFieldInGroup("pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Cref_Final_Prod", 
            "#C-SAVE-CREF-FINAL-PROD", FieldType.STRING, 1);
        pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Rate = pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Area_1_Final_Total_Amt.newFieldInGroup("pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Rate", 
            "#C-SAVE-DA-FINAL-RATE", FieldType.STRING, 2);
        pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Method = pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Area_1_Final_Total_Amt.newFieldInGroup("pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Method", 
            "#C-SAVE-DA-FINAL-METHOD", FieldType.STRING, 10);
        pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Accum_Amt = pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Area_1_Final_Total_Amt.newFieldInGroup("pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Da_Final_Accum_Amt", 
            "#C-SAVE-DA-FINAL-ACCUM-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Settled_Amt = pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Area_1_Final_Total_Amt.newFieldInGroup("pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Settled_Amt", 
            "#C-SAVE-FINAL-SETTLED-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Rtb_Amt = pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Area_1_Final_Total_Amt.newFieldInGroup("pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Rtb_Amt", 
            "#C-SAVE-FINAL-RTB-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Settled_Rtb_Amt = pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Area_1_Final_Total_Amt.newFieldInGroup("pnd_C_Save_Final_Total_Amts_Pnd_C_Save_Final_Settled_Rtb_Amt", 
            "#C-SAVE-FINAL-SETTLED-RTB-AMT", FieldType.PACKED_DECIMAL, 11,2);

        pnd_C_Save_Final_Total_Units = newGroupInRecord("pnd_C_Save_Final_Total_Units", "#C-SAVE-FINAL-TOTAL-UNITS");
        pnd_C_Save_Final_Total_Units_Pnd_C_Save_Area_1_Final_Total_Units = pnd_C_Save_Final_Total_Units.newGroupArrayInGroup("pnd_C_Save_Final_Total_Units_Pnd_C_Save_Area_1_Final_Total_Units", 
            "#C-SAVE-AREA-1-FINAL-TOTAL-UNITS", new DbsArrayController(1,80));
        pnd_C_Save_Final_Total_Units_Pnd_C_Save_Da_Final_Accum_Units = pnd_C_Save_Final_Total_Units_Pnd_C_Save_Area_1_Final_Total_Units.newFieldInGroup("pnd_C_Save_Final_Total_Units_Pnd_C_Save_Da_Final_Accum_Units", 
            "#C-SAVE-DA-FINAL-ACCUM-UNITS", FieldType.PACKED_DECIMAL, 10,3);
        pnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Settled_Units = pnd_C_Save_Final_Total_Units_Pnd_C_Save_Area_1_Final_Total_Units.newFieldInGroup("pnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Settled_Units", 
            "#C-SAVE-FINAL-SETTLED-UNITS", FieldType.PACKED_DECIMAL, 10,3);
        pnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Rtb_Units = pnd_C_Save_Final_Total_Units_Pnd_C_Save_Area_1_Final_Total_Units.newFieldInGroup("pnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Rtb_Units", 
            "#C-SAVE-FINAL-RTB-UNITS", FieldType.PACKED_DECIMAL, 10,3);
        pnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Settled_Rtb_Units = pnd_C_Save_Final_Total_Units_Pnd_C_Save_Area_1_Final_Total_Units.newFieldInGroup("pnd_C_Save_Final_Total_Units_Pnd_C_Save_Final_Settled_Rtb_Units", 
            "#C-SAVE-FINAL-SETTLED-RTB-UNITS", FieldType.PACKED_DECIMAL, 10,3);

        pnd_Cref_Monthly_Totals = newGroupInRecord("pnd_Cref_Monthly_Totals", "#CREF-MONTHLY-TOTALS");
        pnd_Cref_Monthly_Totals_Pnd_Cref_Prod_Monthly = pnd_Cref_Monthly_Totals.newFieldInGroup("pnd_Cref_Monthly_Totals_Pnd_Cref_Prod_Monthly", "#CREF-PROD-MONTHLY", 
            FieldType.STRING, 1);
        pnd_Cref_Monthly_Totals_Pnd_Cref_Da_Rate_Monthly = pnd_Cref_Monthly_Totals.newFieldInGroup("pnd_Cref_Monthly_Totals_Pnd_Cref_Da_Rate_Monthly", 
            "#CREF-DA-RATE-MONTHLY", FieldType.STRING, 2);
        pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Rate_Monthly = pnd_Cref_Monthly_Totals.newFieldInGroup("pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Rate_Monthly", 
            "#CREF-IA-RATE-MONTHLY", FieldType.STRING, 2);
        pnd_Cref_Monthly_Totals_Pnd_Cref_Settled_Monthly_Amt = pnd_Cref_Monthly_Totals.newFieldInGroup("pnd_Cref_Monthly_Totals_Pnd_Cref_Settled_Monthly_Amt", 
            "#CREF-SETTLED-MONTHLY-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Units = pnd_Cref_Monthly_Totals.newFieldInGroup("pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Units", 
            "#CREF-IA-MONTHLY-UNITS", FieldType.PACKED_DECIMAL, 10,3);
        pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Payments = pnd_Cref_Monthly_Totals.newFieldInGroup("pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Payments", 
            "#CREF-IA-MONTHLY-PAYMENTS", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Unit_Val = pnd_Cref_Monthly_Totals.newFieldInGroup("pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Unit_Val", 
            "#CREF-IA-MONTHLY-UNIT-VAL", FieldType.PACKED_DECIMAL, 11,4);

        pnd_C_Save_Monthly_Method_Totals = newGroupInRecord("pnd_C_Save_Monthly_Method_Totals", "#C-SAVE-MONTHLY-METHOD-TOTALS");
        pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Monthly_Totals = pnd_C_Save_Monthly_Method_Totals.newGroupArrayInGroup("pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Monthly_Totals", 
            "#C-SAVE-MONTHLY-TOTALS", new DbsArrayController(1,80));
        pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Prod_Monthly = pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Monthly_Totals.newFieldInGroup("pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Prod_Monthly", 
            "#C-SAVE-PROD-MONTHLY", FieldType.STRING, 1);
        pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Da_Rate_Monthly = pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Monthly_Totals.newFieldInGroup("pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Da_Rate_Monthly", 
            "#C-SAVE-DA-RATE-MONTHLY", FieldType.STRING, 2);
        pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Rate_Monthly = pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Monthly_Totals.newFieldInGroup("pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Rate_Monthly", 
            "#C-SAVE-IA-RATE-MONTHLY", FieldType.STRING, 2);
        pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Settled_Monthly_Amt = pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Monthly_Totals.newFieldInGroup("pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Settled_Monthly_Amt", 
            "#C-SAVE-SETTLED-MONTHLY-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Units = pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Monthly_Totals.newFieldInGroup("pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Units", 
            "#C-SAVE-IA-MONTHLY-UNITS", FieldType.PACKED_DECIMAL, 10,3);
        pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Payments = pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Monthly_Totals.newFieldInGroup("pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Payments", 
            "#C-SAVE-IA-MONTHLY-PAYMENTS", FieldType.PACKED_DECIMAL, 11,2);
        pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Unit_Val = pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Monthly_Totals.newFieldInGroup("pnd_C_Save_Monthly_Method_Totals_Pnd_C_Save_Ia_Monthly_Unit_Val", 
            "#C-SAVE-IA-MONTHLY-UNIT-VAL", FieldType.PACKED_DECIMAL, 11,4);

        pnd_Cref_Monthly_Method_Final_Tot = newGroupInRecord("pnd_Cref_Monthly_Method_Final_Tot", "#CREF-MONTHLY-METHOD-FINAL-TOT");
        pnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Settled_Monthly_Final_Amt = pnd_Cref_Monthly_Method_Final_Tot.newFieldInGroup("pnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Settled_Monthly_Final_Amt", 
            "#CREF-SETTLED-MONTHLY-FINAL-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Units = pnd_Cref_Monthly_Method_Final_Tot.newFieldInGroup("pnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Units", 
            "#CREF-IA-MONTHLY-FINAL-UNITS", FieldType.PACKED_DECIMAL, 10,3);
        pnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Payments = pnd_Cref_Monthly_Method_Final_Tot.newFieldInGroup("pnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Payments", 
            "#CREF-IA-MONTHLY-FINAL-PAYMENTS", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Unit_Val = pnd_Cref_Monthly_Method_Final_Tot.newFieldInGroup("pnd_Cref_Monthly_Method_Final_Tot_Pnd_Cref_Ia_Monthly_Final_Unit_Val", 
            "#CREF-IA-MONTHLY-FINAL-UNIT-VAL", FieldType.PACKED_DECIMAL, 11,4);

        pnd_Cref_Annually_Method_Totals = newGroupInRecord("pnd_Cref_Annually_Method_Totals", "#CREF-ANNUALLY-METHOD-TOTALS");
        pnd_Cref_Annually_Method_Totals_Pnd_Cref_Prod_Annually = pnd_Cref_Annually_Method_Totals.newFieldInGroup("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Prod_Annually", 
            "#CREF-PROD-ANNUALLY", FieldType.STRING, 1);
        pnd_Cref_Annually_Method_Totals_Pnd_Cref_Da_Rate_Annually = pnd_Cref_Annually_Method_Totals.newFieldInGroup("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Da_Rate_Annually", 
            "#CREF-DA-RATE-ANNUALLY", FieldType.STRING, 2);
        pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Rate_Annually = pnd_Cref_Annually_Method_Totals.newFieldInGroup("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Rate_Annually", 
            "#CREF-IA-RATE-ANNUALLY", FieldType.STRING, 2);
        pnd_Cref_Annually_Method_Totals_Pnd_Cref_Settled_Annually_Amt = pnd_Cref_Annually_Method_Totals.newFieldInGroup("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Settled_Annually_Amt", 
            "#CREF-SETTLED-ANNUALLY-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Units = pnd_Cref_Annually_Method_Totals.newFieldInGroup("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Units", 
            "#CREF-IA-ANNUALLY-UNITS", FieldType.PACKED_DECIMAL, 10,3);
        pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Payments = pnd_Cref_Annually_Method_Totals.newFieldInGroup("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Payments", 
            "#CREF-IA-ANNUALLY-PAYMENTS", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Unit_Val = pnd_Cref_Annually_Method_Totals.newFieldInGroup("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Unit_Val", 
            "#CREF-IA-ANNUALLY-UNIT-VAL", FieldType.PACKED_DECIMAL, 11,4);

        pnd_C_Save_Annually_Method_Totals = newGroupInRecord("pnd_C_Save_Annually_Method_Totals", "#C-SAVE-ANNUALLY-METHOD-TOTALS");
        pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Annually_Totals = pnd_C_Save_Annually_Method_Totals.newGroupArrayInGroup("pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Annually_Totals", 
            "#C-SAVE-ANNUALLY-TOTALS", new DbsArrayController(1,80));
        pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Prod_Annually = pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Annually_Totals.newFieldInGroup("pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Prod_Annually", 
            "#C-SAVE-PROD-ANNUALLY", FieldType.STRING, 1);
        pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Da_Rate_Annually = pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Annually_Totals.newFieldInGroup("pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Da_Rate_Annually", 
            "#C-SAVE-DA-RATE-ANNUALLY", FieldType.STRING, 2);
        pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Rate_Annually = pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Annually_Totals.newFieldInGroup("pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Rate_Annually", 
            "#C-SAVE-IA-RATE-ANNUALLY", FieldType.STRING, 2);
        pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Settled_Annually_Amt = pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Annually_Totals.newFieldInGroup("pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Settled_Annually_Amt", 
            "#C-SAVE-SETTLED-ANNUALLY-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Units = pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Annually_Totals.newFieldInGroup("pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Units", 
            "#C-SAVE-IA-ANNUALLY-UNITS", FieldType.PACKED_DECIMAL, 10,3);
        pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Payments = pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Annually_Totals.newFieldInGroup("pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Payments", 
            "#C-SAVE-IA-ANNUALLY-PAYMENTS", FieldType.PACKED_DECIMAL, 11,2);
        pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Unit_Val = pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Annually_Totals.newFieldInGroup("pnd_C_Save_Annually_Method_Totals_Pnd_C_Save_Ia_Annually_Unit_Val", 
            "#C-SAVE-IA-ANNUALLY-UNIT-VAL", FieldType.PACKED_DECIMAL, 11,4);

        pnd_Cref_Annually_Method_Final_Tot = newGroupInRecord("pnd_Cref_Annually_Method_Final_Tot", "#CREF-ANNUALLY-METHOD-FINAL-TOT");
        pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Settled_Annually_Final_Amt = pnd_Cref_Annually_Method_Final_Tot.newFieldInGroup("pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Settled_Annually_Final_Amt", 
            "#CREF-SETTLED-ANNUALLY-FINAL-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Units = pnd_Cref_Annually_Method_Final_Tot.newFieldInGroup("pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Units", 
            "#CREF-IA-ANNUALLY-FINAL-UNITS", FieldType.PACKED_DECIMAL, 10,3);
        pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Payments = pnd_Cref_Annually_Method_Final_Tot.newFieldInGroup("pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Payments", 
            "#CREF-IA-ANNUALLY-FINAL-PAYMENTS", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Unit_Val = pnd_Cref_Annually_Method_Final_Tot.newFieldInGroup("pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Unit_Val", 
            "#CREF-IA-ANNUALLY-FINAL-UNIT-VAL", FieldType.PACKED_DECIMAL, 11,4);

        pnd_Cref_Header_Amt = newGroupInRecord("pnd_Cref_Header_Amt", "#CREF-HEADER-AMT");
        pnd_Cref_Header_Amt_Pnd_Cref_Header_Account = pnd_Cref_Header_Amt.newFieldInGroup("pnd_Cref_Header_Amt_Pnd_Cref_Header_Account", "#CREF-HEADER-ACCOUNT", 
            FieldType.STRING, 4);
        pnd_Cref_Header_Amt_Pnd_Cref_Header_Settle_Amt = pnd_Cref_Header_Amt.newFieldInGroup("pnd_Cref_Header_Amt_Pnd_Cref_Header_Settle_Amt", "#CREF-HEADER-SETTLE-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Cref_Header_Amt_Pnd_Cref_Header_Settle_Typ = pnd_Cref_Header_Amt.newFieldInGroup("pnd_Cref_Header_Amt_Pnd_Cref_Header_Settle_Typ", "#CREF-HEADER-SETTLE-TYP", 
            FieldType.STRING, 1);
        pnd_Cref_Header_Amt_Pnd_Cref_Header_Rtb_Amt = pnd_Cref_Header_Amt.newFieldInGroup("pnd_Cref_Header_Amt_Pnd_Cref_Header_Rtb_Amt", "#CREF-HEADER-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cref_Header_Amt_Pnd_Cref_Header_Rtb_Typ = pnd_Cref_Header_Amt.newFieldInGroup("pnd_Cref_Header_Amt_Pnd_Cref_Header_Rtb_Typ", "#CREF-HEADER-RTB-TYP", 
            FieldType.STRING, 1);
        pnd_Cref_Header_Amt_Pnd_Cref_Header_Grd_Amt = pnd_Cref_Header_Amt.newFieldInGroup("pnd_Cref_Header_Amt_Pnd_Cref_Header_Grd_Amt", "#CREF-HEADER-GRD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cref_Header_Amt_Pnd_Cref_Header_Grd_Typ = pnd_Cref_Header_Amt.newFieldInGroup("pnd_Cref_Header_Amt_Pnd_Cref_Header_Grd_Typ", "#CREF-HEADER-GRD-TYP", 
            FieldType.STRING, 1);
        pnd_Cref_Header_Amt_Pnd_Cref_Header_Cref_Mnthly_Units = pnd_Cref_Header_Amt.newFieldInGroup("pnd_Cref_Header_Amt_Pnd_Cref_Header_Cref_Mnthly_Units", 
            "#CREF-HEADER-CREF-MNTHLY-UNITS", FieldType.PACKED_DECIMAL, 10,3);

        pnd_Cref_Header_Settle_Amounts = newGroupInRecord("pnd_Cref_Header_Settle_Amounts", "#CREF-HEADER-SETTLE-AMOUNTS");
        pnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Settle_Amount = pnd_Cref_Header_Settle_Amounts.newFieldInGroup("pnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Settle_Amount", 
            "#CREF-HEADER-SETTLE-AMOUNT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Rtb_Settle_Amount = pnd_Cref_Header_Settle_Amounts.newFieldInGroup("pnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Rtb_Settle_Amount", 
            "#CREF-HEADER-RTB-SETTLE-AMOUNT", FieldType.PACKED_DECIMAL, 11,2);

        pnd_Cref_Ivc_Amounts = newGroupInRecord("pnd_Cref_Ivc_Amounts", "#CREF-IVC-AMOUNTS");
        pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Total = pnd_Cref_Ivc_Amounts.newFieldInGroup("pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Total", "#CREF-IVC-TOTAL", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Rtb = pnd_Cref_Ivc_Amounts.newFieldInGroup("pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Rtb", "#CREF-IVC-RTB", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Annuity = pnd_Cref_Ivc_Amounts.newFieldInGroup("pnd_Cref_Ivc_Amounts_Pnd_Cref_Ivc_Annuity", "#CREF-IVC-ANNUITY", 
            FieldType.PACKED_DECIMAL, 11,2);

        pnd_Cref_Data_Entry_Info = newGroupInRecord("pnd_Cref_Data_Entry_Info", "#CREF-DATA-ENTRY-INFO");
        pnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_First_Entered_By = pnd_Cref_Data_Entry_Info.newFieldInGroup("pnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_First_Entered_By", 
            "#CREF-RQST-FIRST-ENTERED-BY", FieldType.STRING, 8);
        pnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_First_Verified_By = pnd_Cref_Data_Entry_Info.newFieldInGroup("pnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_First_Verified_By", 
            "#CREF-RQST-FIRST-VERIFIED-BY", FieldType.STRING, 8);
        pnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_Last_Updated_By = pnd_Cref_Data_Entry_Info.newFieldInGroup("pnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_Last_Updated_By", 
            "#CREF-RQST-LAST-UPDATED-BY", FieldType.STRING, 8);
        pnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_Last_Verified_By = pnd_Cref_Data_Entry_Info.newFieldInGroup("pnd_Cref_Data_Entry_Info_Pnd_Cref_Rqst_Last_Verified_By", 
            "#CREF-RQST-LAST-VERIFIED-BY", FieldType.STRING, 8);

        this.setRecordName("LdaAdsl770");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAdsl770() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
