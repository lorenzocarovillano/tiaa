/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:48:18 PM
**        * FROM NATURAL GDA     : ADSG870
************************************************************
**        * FILE NAME            : GdaAdsg870.java
**        * CLASS NAME           : GdaAdsg870
**        * INSTANCE NAME        : GdaAdsg870
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import java.util.ArrayList;
import java.util.List;

import tiaa.tiaacommon.bl.*;

public final class GdaAdsg870 extends DbsRecord
{
    private static ThreadLocal<List<GdaAdsg870>> _instance = new ThreadLocal<List<GdaAdsg870>>();

    // Properties
    private DbsGroup adsg870;
    private DbsField adsg870_Pnd_Naza870_Rqst_Id;
    private DbsGroup adsg870_Pnd_Naza870_Rqst_IdRedef1;
    private DbsField adsg870_Pnd_Naza870_Unique_Id;
    private DbsField adsg870_Pnd_Naza870_Effective_Date;
    private DbsField adsg870_Pnd_Naza870_Entry_Date;
    private DbsField adsg870_Pnd_Naza870_Entry_Time;
    private DbsField adsg870_Pnd_Naza870_Tiaa_Nbr;
    private DbsField adsg870_Pnd_Naza870_Cref_Cert_Nbr;
    private DbsField adsg870_Pnd_Naza870_Page_Number;
    private DbsField adsg870_Pnd_Naza870_Hold_Settl_Dte;
    private DbsGroup adsg870_Pnd_Naza870_Hold_Settl_DteRedef2;
    private DbsField adsg870_Pnd_Naza870_Hold_Settl_Date;
    private DbsField adsg870_Pnd_Naza870_Non_Prm_Dte;
    private DbsGroup adsg870_Pnd_Naza870_Non_Prm_DteRedef3;
    private DbsField adsg870_Pnd_Naza870_Non_Prm_Date;
    private DbsField adsg870_Pnd_Naza870_Non_Prm_Tme;
    private DbsField adsg870_Pnd_Naza870_Hold_Da_Status_Date;
    private DbsField adsg870_Pnd_Naza870_Trn_Oper_Nme;
    private DbsField adsg870_Pnd_Tiaa_Ia_Std_Gur_Pay_Renew;
    private DbsField adsg870_Pnd_Tiaa_Ia_Std_Div_Pay_Renew;
    private DbsField adsg870_Pnd_Tiaa_Ia_Std_Gur_Fin_Renew;
    private DbsField adsg870_Pnd_Tiaa_Ia_Std_Div_Fin_Renew;
    private DbsField adsg870_Pnd_Tiaa_Ia_Grd_Gur_Pay_Renew;
    private DbsField adsg870_Pnd_Tiaa_Ia_Grd_Div_Pay_Renew;
    private DbsField adsg870_Pnd_Tiaa_Ia_Grd_Gur_Fin_Renew;
    private DbsField adsg870_Pnd_Tiaa_Ia_Grd_Div_Fin_Renew;
    private DbsField adsg870_Pnd_Nai_Rslt_Rqst_Id;
    private DbsField adsg870_Pnd_Nai_Rslt_Rcrd_Cde;
    private DbsField adsg870_Pnd_Nai_Rslt_Sqnce_Nbr;

    public DbsGroup getAdsg870() { return adsg870; }

    public DbsField getAdsg870_Pnd_Naza870_Rqst_Id() { return adsg870_Pnd_Naza870_Rqst_Id; }

    public DbsGroup getAdsg870_Pnd_Naza870_Rqst_IdRedef1() { return adsg870_Pnd_Naza870_Rqst_IdRedef1; }

    public DbsField getAdsg870_Pnd_Naza870_Unique_Id() { return adsg870_Pnd_Naza870_Unique_Id; }

    public DbsField getAdsg870_Pnd_Naza870_Effective_Date() { return adsg870_Pnd_Naza870_Effective_Date; }

    public DbsField getAdsg870_Pnd_Naza870_Entry_Date() { return adsg870_Pnd_Naza870_Entry_Date; }

    public DbsField getAdsg870_Pnd_Naza870_Entry_Time() { return adsg870_Pnd_Naza870_Entry_Time; }

    public DbsField getAdsg870_Pnd_Naza870_Tiaa_Nbr() { return adsg870_Pnd_Naza870_Tiaa_Nbr; }

    public DbsField getAdsg870_Pnd_Naza870_Cref_Cert_Nbr() { return adsg870_Pnd_Naza870_Cref_Cert_Nbr; }

    public DbsField getAdsg870_Pnd_Naza870_Page_Number() { return adsg870_Pnd_Naza870_Page_Number; }

    public DbsField getAdsg870_Pnd_Naza870_Hold_Settl_Dte() { return adsg870_Pnd_Naza870_Hold_Settl_Dte; }

    public DbsGroup getAdsg870_Pnd_Naza870_Hold_Settl_DteRedef2() { return adsg870_Pnd_Naza870_Hold_Settl_DteRedef2; }

    public DbsField getAdsg870_Pnd_Naza870_Hold_Settl_Date() { return adsg870_Pnd_Naza870_Hold_Settl_Date; }

    public DbsField getAdsg870_Pnd_Naza870_Non_Prm_Dte() { return adsg870_Pnd_Naza870_Non_Prm_Dte; }

    public DbsGroup getAdsg870_Pnd_Naza870_Non_Prm_DteRedef3() { return adsg870_Pnd_Naza870_Non_Prm_DteRedef3; }

    public DbsField getAdsg870_Pnd_Naza870_Non_Prm_Date() { return adsg870_Pnd_Naza870_Non_Prm_Date; }

    public DbsField getAdsg870_Pnd_Naza870_Non_Prm_Tme() { return adsg870_Pnd_Naza870_Non_Prm_Tme; }

    public DbsField getAdsg870_Pnd_Naza870_Hold_Da_Status_Date() { return adsg870_Pnd_Naza870_Hold_Da_Status_Date; }

    public DbsField getAdsg870_Pnd_Naza870_Trn_Oper_Nme() { return adsg870_Pnd_Naza870_Trn_Oper_Nme; }

    public DbsField getAdsg870_Pnd_Tiaa_Ia_Std_Gur_Pay_Renew() { return adsg870_Pnd_Tiaa_Ia_Std_Gur_Pay_Renew; }

    public DbsField getAdsg870_Pnd_Tiaa_Ia_Std_Div_Pay_Renew() { return adsg870_Pnd_Tiaa_Ia_Std_Div_Pay_Renew; }

    public DbsField getAdsg870_Pnd_Tiaa_Ia_Std_Gur_Fin_Renew() { return adsg870_Pnd_Tiaa_Ia_Std_Gur_Fin_Renew; }

    public DbsField getAdsg870_Pnd_Tiaa_Ia_Std_Div_Fin_Renew() { return adsg870_Pnd_Tiaa_Ia_Std_Div_Fin_Renew; }

    public DbsField getAdsg870_Pnd_Tiaa_Ia_Grd_Gur_Pay_Renew() { return adsg870_Pnd_Tiaa_Ia_Grd_Gur_Pay_Renew; }

    public DbsField getAdsg870_Pnd_Tiaa_Ia_Grd_Div_Pay_Renew() { return adsg870_Pnd_Tiaa_Ia_Grd_Div_Pay_Renew; }

    public DbsField getAdsg870_Pnd_Tiaa_Ia_Grd_Gur_Fin_Renew() { return adsg870_Pnd_Tiaa_Ia_Grd_Gur_Fin_Renew; }

    public DbsField getAdsg870_Pnd_Tiaa_Ia_Grd_Div_Fin_Renew() { return adsg870_Pnd_Tiaa_Ia_Grd_Div_Fin_Renew; }

    public DbsField getAdsg870_Pnd_Nai_Rslt_Rqst_Id() { return adsg870_Pnd_Nai_Rslt_Rqst_Id; }

    public DbsField getAdsg870_Pnd_Nai_Rslt_Rcrd_Cde() { return adsg870_Pnd_Nai_Rslt_Rcrd_Cde; }

    public DbsField getAdsg870_Pnd_Nai_Rslt_Sqnce_Nbr() { return adsg870_Pnd_Nai_Rslt_Sqnce_Nbr; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        adsg870 = newGroupInRecord("adsg870", "ADSG870");
        adsg870_Pnd_Naza870_Rqst_Id = adsg870.newFieldInGroup("adsg870_Pnd_Naza870_Rqst_Id", "#NAZA870-RQST-ID", FieldType.STRING, 30);
        adsg870_Pnd_Naza870_Rqst_IdRedef1 = adsg870.newGroupInGroup("adsg870_Pnd_Naza870_Rqst_IdRedef1", "Redefines", adsg870_Pnd_Naza870_Rqst_Id);
        adsg870_Pnd_Naza870_Unique_Id = adsg870_Pnd_Naza870_Rqst_IdRedef1.newFieldInGroup("adsg870_Pnd_Naza870_Unique_Id", "#NAZA870-UNIQUE-ID", FieldType.NUMERIC, 
            7);
        adsg870_Pnd_Naza870_Effective_Date = adsg870_Pnd_Naza870_Rqst_IdRedef1.newFieldInGroup("adsg870_Pnd_Naza870_Effective_Date", "#NAZA870-EFFECTIVE-DATE", 
            FieldType.NUMERIC, 8);
        adsg870_Pnd_Naza870_Entry_Date = adsg870_Pnd_Naza870_Rqst_IdRedef1.newFieldInGroup("adsg870_Pnd_Naza870_Entry_Date", "#NAZA870-ENTRY-DATE", FieldType.NUMERIC, 
            8);
        adsg870_Pnd_Naza870_Entry_Time = adsg870_Pnd_Naza870_Rqst_IdRedef1.newFieldInGroup("adsg870_Pnd_Naza870_Entry_Time", "#NAZA870-ENTRY-TIME", FieldType.NUMERIC, 
            7);
        adsg870_Pnd_Naza870_Tiaa_Nbr = adsg870.newFieldInGroup("adsg870_Pnd_Naza870_Tiaa_Nbr", "#NAZA870-TIAA-NBR", FieldType.STRING, 8);
        adsg870_Pnd_Naza870_Cref_Cert_Nbr = adsg870.newFieldInGroup("adsg870_Pnd_Naza870_Cref_Cert_Nbr", "#NAZA870-CREF-CERT-NBR", FieldType.STRING, 8);
        adsg870_Pnd_Naza870_Page_Number = adsg870.newFieldInGroup("adsg870_Pnd_Naza870_Page_Number", "#NAZA870-PAGE-NUMBER", FieldType.NUMERIC, 8);
        adsg870_Pnd_Naza870_Hold_Settl_Dte = adsg870.newFieldInGroup("adsg870_Pnd_Naza870_Hold_Settl_Dte", "#NAZA870-HOLD-SETTL-DTE", FieldType.STRING, 
            8);
        adsg870_Pnd_Naza870_Hold_Settl_DteRedef2 = adsg870.newGroupInGroup("adsg870_Pnd_Naza870_Hold_Settl_DteRedef2", "Redefines", adsg870_Pnd_Naza870_Hold_Settl_Dte);
        adsg870_Pnd_Naza870_Hold_Settl_Date = adsg870_Pnd_Naza870_Hold_Settl_DteRedef2.newFieldInGroup("adsg870_Pnd_Naza870_Hold_Settl_Date", "#NAZA870-HOLD-SETTL-DATE", 
            FieldType.NUMERIC, 8);
        adsg870_Pnd_Naza870_Non_Prm_Dte = adsg870.newFieldInGroup("adsg870_Pnd_Naza870_Non_Prm_Dte", "#NAZA870-NON-PRM-DTE", FieldType.STRING, 8);
        adsg870_Pnd_Naza870_Non_Prm_DteRedef3 = adsg870.newGroupInGroup("adsg870_Pnd_Naza870_Non_Prm_DteRedef3", "Redefines", adsg870_Pnd_Naza870_Non_Prm_Dte);
        adsg870_Pnd_Naza870_Non_Prm_Date = adsg870_Pnd_Naza870_Non_Prm_DteRedef3.newFieldInGroup("adsg870_Pnd_Naza870_Non_Prm_Date", "#NAZA870-NON-PRM-DATE", 
            FieldType.NUMERIC, 8);
        adsg870_Pnd_Naza870_Non_Prm_Tme = adsg870.newFieldInGroup("adsg870_Pnd_Naza870_Non_Prm_Tme", "#NAZA870-NON-PRM-TME", FieldType.NUMERIC, 6);
        adsg870_Pnd_Naza870_Hold_Da_Status_Date = adsg870.newFieldInGroup("adsg870_Pnd_Naza870_Hold_Da_Status_Date", "#NAZA870-HOLD-DA-STATUS-DATE", FieldType.NUMERIC, 
            8);
        adsg870_Pnd_Naza870_Trn_Oper_Nme = adsg870.newFieldInGroup("adsg870_Pnd_Naza870_Trn_Oper_Nme", "#NAZA870-TRN-OPER-NME", FieldType.STRING, 3);
        adsg870_Pnd_Tiaa_Ia_Std_Gur_Pay_Renew = adsg870.newFieldArrayInGroup("adsg870_Pnd_Tiaa_Ia_Std_Gur_Pay_Renew", "#TIAA-IA-STD-GUR-PAY-RENEW", FieldType.PACKED_DECIMAL, 
            12,2, new DbsArrayController(1,250));
        adsg870_Pnd_Tiaa_Ia_Std_Div_Pay_Renew = adsg870.newFieldArrayInGroup("adsg870_Pnd_Tiaa_Ia_Std_Div_Pay_Renew", "#TIAA-IA-STD-DIV-PAY-RENEW", FieldType.PACKED_DECIMAL, 
            12,2, new DbsArrayController(1,250));
        adsg870_Pnd_Tiaa_Ia_Std_Gur_Fin_Renew = adsg870.newFieldInGroup("adsg870_Pnd_Tiaa_Ia_Std_Gur_Fin_Renew", "#TIAA-IA-STD-GUR-FIN-RENEW", FieldType.PACKED_DECIMAL, 
            12,2);
        adsg870_Pnd_Tiaa_Ia_Std_Div_Fin_Renew = adsg870.newFieldInGroup("adsg870_Pnd_Tiaa_Ia_Std_Div_Fin_Renew", "#TIAA-IA-STD-DIV-FIN-RENEW", FieldType.PACKED_DECIMAL, 
            12,2);
        adsg870_Pnd_Tiaa_Ia_Grd_Gur_Pay_Renew = adsg870.newFieldArrayInGroup("adsg870_Pnd_Tiaa_Ia_Grd_Gur_Pay_Renew", "#TIAA-IA-GRD-GUR-PAY-RENEW", FieldType.PACKED_DECIMAL, 
            12,2, new DbsArrayController(1,250));
        adsg870_Pnd_Tiaa_Ia_Grd_Div_Pay_Renew = adsg870.newFieldArrayInGroup("adsg870_Pnd_Tiaa_Ia_Grd_Div_Pay_Renew", "#TIAA-IA-GRD-DIV-PAY-RENEW", FieldType.PACKED_DECIMAL, 
            12,2, new DbsArrayController(1,250));
        adsg870_Pnd_Tiaa_Ia_Grd_Gur_Fin_Renew = adsg870.newFieldInGroup("adsg870_Pnd_Tiaa_Ia_Grd_Gur_Fin_Renew", "#TIAA-IA-GRD-GUR-FIN-RENEW", FieldType.PACKED_DECIMAL, 
            12,2);
        adsg870_Pnd_Tiaa_Ia_Grd_Div_Fin_Renew = adsg870.newFieldInGroup("adsg870_Pnd_Tiaa_Ia_Grd_Div_Fin_Renew", "#TIAA-IA-GRD-DIV-FIN-RENEW", FieldType.PACKED_DECIMAL, 
            12,2);
        adsg870_Pnd_Nai_Rslt_Rqst_Id = adsg870.newFieldInGroup("adsg870_Pnd_Nai_Rslt_Rqst_Id", "#NAI-RSLT-RQST-ID", FieldType.STRING, 30);
        adsg870_Pnd_Nai_Rslt_Rcrd_Cde = adsg870.newFieldInGroup("adsg870_Pnd_Nai_Rslt_Rcrd_Cde", "#NAI-RSLT-RCRD-CDE", FieldType.STRING, 3);
        adsg870_Pnd_Nai_Rslt_Sqnce_Nbr = adsg870.newFieldInGroup("adsg870_Pnd_Nai_Rslt_Sqnce_Nbr", "#NAI-RSLT-SQNCE-NBR", FieldType.NUMERIC, 3);

        this.setRecordName("GdaAdsg870");
    }
    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    private GdaAdsg870() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }

    // Instance Property
    public static GdaAdsg870 getInstance(int callnatLevel) throws Exception
    {
        if (_instance.get() == null)
            _instance.set(new ArrayList<GdaAdsg870>());

        if (_instance.get().size() < callnatLevel)
        {
            while (_instance.get().size() < callnatLevel)
            {
                _instance.get().add(new GdaAdsg870());
            }
        }
        else if (_instance.get().size() > callnatLevel)
        {
            while(_instance.get().size() > callnatLevel)
            _instance.get().remove(_instance.get().size() - 1);
        }

        return _instance.get().get(callnatLevel - 1);
    }
}

