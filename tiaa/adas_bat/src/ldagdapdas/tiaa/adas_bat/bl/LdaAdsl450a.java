/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:49:49 PM
**        * FROM NATURAL LDA     : ADSL450A
************************************************************
**        * FILE NAME            : LdaAdsl450a.java
**        * CLASS NAME           : LdaAdsl450a
**        * INSTANCE NAME        : LdaAdsl450a
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdsl450a extends DbsRecord
{
    // Properties
    private DbsGroup ads_Ia_Rslt;
    private DbsField ads_Ia_Rslt_Rqst_Id;
    private DbsField ads_Ia_Rslt_Adi_Ia_Rcrd_Cde;
    private DbsField ads_Ia_Rslt_Adi_Sqnce_Nbr;
    private DbsField ads_Ia_Rslt_Adi_Stts_Cde;
    private DbsGroup ads_Ia_Rslt_Adi_Stts_CdeRedef1;
    private DbsField ads_Ia_Rslt_Adi_Stts_Cde_1;
    private DbsField ads_Ia_Rslt_Adi_Stts_Cde_2;
    private DbsField ads_Ia_Rslt_Adi_Tiaa_Nbrs;
    private DbsField ads_Ia_Rslt_Adi_Cref_Nbrs;
    private DbsField ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr;
    private DbsGroup ads_Ia_Rslt_Adi_Ia_Tiaa_NbrRedef2;
    private DbsField ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr_8;
    private DbsField ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr_2;
    private DbsField ads_Ia_Rslt_Adi_Ia_Tiaa_Payee_Cde;
    private DbsField ads_Ia_Rslt_Adi_Ia_Cref_Nbr;
    private DbsField ads_Ia_Rslt_Adi_Ia_Cref_Payee_Cde;
    private DbsField ads_Ia_Rslt_Adi_Curncy_Cde;
    private DbsField ads_Ia_Rslt_Adi_Pymnt_Cde;
    private DbsField ads_Ia_Rslt_Adi_Pymnt_Mode;
    private DbsGroup ads_Ia_Rslt_Adi_Pymnt_ModeRedef3;
    private DbsField ads_Ia_Rslt_Adi_Pymnt_Mode_1;
    private DbsField ads_Ia_Rslt_Adi_Pymnt_Mode_2;
    private DbsField ads_Ia_Rslt_Adi_Lst_Actvty_Dte;
    private DbsField ads_Ia_Rslt_Adi_Annty_Strt_Dte;
    private DbsField ads_Ia_Rslt_Adi_Instllmnt_Dte;
    private DbsField ads_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte;
    private DbsField ads_Ia_Rslt_Adi_Frst_Ck_Pd_Dte;
    private DbsField ads_Ia_Rslt_Adi_Finl_Periodic_Py_Dte;
    private DbsField ads_Ia_Rslt_Adi_Finl_Prtl_Py_Dte;
    private DbsGroup ads_Ia_Rslt_Adi_Ivc_Data;
    private DbsField ads_Ia_Rslt_Adi_Tiaa_Ivc_Amt;
    private DbsField ads_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind;
    private DbsField ads_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt;
    private DbsField ads_Ia_Rslt_Adi_Cref_Ivc_Amt;
    private DbsField ads_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind;
    private DbsField ads_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt;
    private DbsGroup ads_Ia_Rslt_Adi_Dtl_Tiaa_Data;
    private DbsField ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd;
    private DbsField ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt;
    private DbsField ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt;
    private DbsField ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Acct_Cd;
    private DbsField ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd;
    private DbsField ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Eff_Rte_Intrst;
    private DbsField ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt;
    private DbsField ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt;
    private DbsField ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt;
    private DbsField ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt;
    private DbsField ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt;
    private DbsGroup ads_Ia_Rslt_Adi_Dtl_Cref_Data;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Da_Rate_Cd;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Amt;
    private DbsField ads_Ia_Rslt_Adi_Optn_Cde;
    private DbsField ads_Ia_Rslt_Adi_Tiaa_Sttlmnt;
    private DbsField ads_Ia_Rslt_Adi_Tiaa_Re_Sttlmnt;
    private DbsField ads_Ia_Rslt_Adi_Cref_Sttlmnt;
    private DbsGroup ads_Ia_Rslt_Pnd_Adi_Dtl_Tia_Tpa_Guar_Commut_Dat;
    private DbsField ads_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Std_Amt;
    private DbsField ads_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Grd_Amt;
    private DbsField ads_Ia_Rslt_Adi_Orig_Lob_Ind;
    private DbsField ads_Ia_Rslt_Adi_Roth_Rqst_Ind;
    private DbsField ads_Ia_Rslt_Adi_Srvvr_Ind;
    private DbsField ads_Ia_Rslt_Pnd_Adi_Contract_Id;

    public DbsGroup getAds_Ia_Rslt() { return ads_Ia_Rslt; }

    public DbsField getAds_Ia_Rslt_Rqst_Id() { return ads_Ia_Rslt_Rqst_Id; }

    public DbsField getAds_Ia_Rslt_Adi_Ia_Rcrd_Cde() { return ads_Ia_Rslt_Adi_Ia_Rcrd_Cde; }

    public DbsField getAds_Ia_Rslt_Adi_Sqnce_Nbr() { return ads_Ia_Rslt_Adi_Sqnce_Nbr; }

    public DbsField getAds_Ia_Rslt_Adi_Stts_Cde() { return ads_Ia_Rslt_Adi_Stts_Cde; }

    public DbsGroup getAds_Ia_Rslt_Adi_Stts_CdeRedef1() { return ads_Ia_Rslt_Adi_Stts_CdeRedef1; }

    public DbsField getAds_Ia_Rslt_Adi_Stts_Cde_1() { return ads_Ia_Rslt_Adi_Stts_Cde_1; }

    public DbsField getAds_Ia_Rslt_Adi_Stts_Cde_2() { return ads_Ia_Rslt_Adi_Stts_Cde_2; }

    public DbsField getAds_Ia_Rslt_Adi_Tiaa_Nbrs() { return ads_Ia_Rslt_Adi_Tiaa_Nbrs; }

    public DbsField getAds_Ia_Rslt_Adi_Cref_Nbrs() { return ads_Ia_Rslt_Adi_Cref_Nbrs; }

    public DbsField getAds_Ia_Rslt_Adi_Ia_Tiaa_Nbr() { return ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr; }

    public DbsGroup getAds_Ia_Rslt_Adi_Ia_Tiaa_NbrRedef2() { return ads_Ia_Rslt_Adi_Ia_Tiaa_NbrRedef2; }

    public DbsField getAds_Ia_Rslt_Adi_Ia_Tiaa_Nbr_8() { return ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr_8; }

    public DbsField getAds_Ia_Rslt_Adi_Ia_Tiaa_Nbr_2() { return ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr_2; }

    public DbsField getAds_Ia_Rslt_Adi_Ia_Tiaa_Payee_Cde() { return ads_Ia_Rslt_Adi_Ia_Tiaa_Payee_Cde; }

    public DbsField getAds_Ia_Rslt_Adi_Ia_Cref_Nbr() { return ads_Ia_Rslt_Adi_Ia_Cref_Nbr; }

    public DbsField getAds_Ia_Rslt_Adi_Ia_Cref_Payee_Cde() { return ads_Ia_Rslt_Adi_Ia_Cref_Payee_Cde; }

    public DbsField getAds_Ia_Rslt_Adi_Curncy_Cde() { return ads_Ia_Rslt_Adi_Curncy_Cde; }

    public DbsField getAds_Ia_Rslt_Adi_Pymnt_Cde() { return ads_Ia_Rslt_Adi_Pymnt_Cde; }

    public DbsField getAds_Ia_Rslt_Adi_Pymnt_Mode() { return ads_Ia_Rslt_Adi_Pymnt_Mode; }

    public DbsGroup getAds_Ia_Rslt_Adi_Pymnt_ModeRedef3() { return ads_Ia_Rslt_Adi_Pymnt_ModeRedef3; }

    public DbsField getAds_Ia_Rslt_Adi_Pymnt_Mode_1() { return ads_Ia_Rslt_Adi_Pymnt_Mode_1; }

    public DbsField getAds_Ia_Rslt_Adi_Pymnt_Mode_2() { return ads_Ia_Rslt_Adi_Pymnt_Mode_2; }

    public DbsField getAds_Ia_Rslt_Adi_Lst_Actvty_Dte() { return ads_Ia_Rslt_Adi_Lst_Actvty_Dte; }

    public DbsField getAds_Ia_Rslt_Adi_Annty_Strt_Dte() { return ads_Ia_Rslt_Adi_Annty_Strt_Dte; }

    public DbsField getAds_Ia_Rslt_Adi_Instllmnt_Dte() { return ads_Ia_Rslt_Adi_Instllmnt_Dte; }

    public DbsField getAds_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte() { return ads_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte; }

    public DbsField getAds_Ia_Rslt_Adi_Frst_Ck_Pd_Dte() { return ads_Ia_Rslt_Adi_Frst_Ck_Pd_Dte; }

    public DbsField getAds_Ia_Rslt_Adi_Finl_Periodic_Py_Dte() { return ads_Ia_Rslt_Adi_Finl_Periodic_Py_Dte; }

    public DbsField getAds_Ia_Rslt_Adi_Finl_Prtl_Py_Dte() { return ads_Ia_Rslt_Adi_Finl_Prtl_Py_Dte; }

    public DbsGroup getAds_Ia_Rslt_Adi_Ivc_Data() { return ads_Ia_Rslt_Adi_Ivc_Data; }

    public DbsField getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt() { return ads_Ia_Rslt_Adi_Tiaa_Ivc_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind() { return ads_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind; }

    public DbsField getAds_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt() { return ads_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Cref_Ivc_Amt() { return ads_Ia_Rslt_Adi_Cref_Ivc_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind() { return ads_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind; }

    public DbsField getAds_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt() { return ads_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt; }

    public DbsGroup getAds_Ia_Rslt_Adi_Dtl_Tiaa_Data() { return ads_Ia_Rslt_Adi_Dtl_Tiaa_Data; }

    public DbsField getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd() { return ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd; }

    public DbsField getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt() { return ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt; }

    public DbsField getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt() { return ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt; }

    public DbsField getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Acct_Cd() { return ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Acct_Cd; }

    public DbsField getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd() { return ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd; }

    public DbsField getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Eff_Rte_Intrst() { return ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Eff_Rte_Intrst; }

    public DbsField getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt() { return ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt; }

    public DbsField getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt() { return ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt; }

    public DbsField getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt() { return ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt; }

    public DbsField getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt() { return ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt; }

    public DbsField getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt() { return ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt; }

    public DbsField getAds_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt() { return ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt; }

    public DbsField getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt() { return ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt; }

    public DbsField getAds_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt() { return ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt; }

    public DbsGroup getAds_Ia_Rslt_Adi_Dtl_Cref_Data() { return ads_Ia_Rslt_Adi_Dtl_Cref_Data; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Rate_Cd() { return ads_Ia_Rslt_Adi_Dtl_Cref_Da_Rate_Cd; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd() { return ads_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd() { return ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt() { return ads_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units() { return ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val() { return ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Amt() { return ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt() { return ads_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units() { return ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val() { return ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Amt() { return ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Optn_Cde() { return ads_Ia_Rslt_Adi_Optn_Cde; }

    public DbsField getAds_Ia_Rslt_Adi_Tiaa_Sttlmnt() { return ads_Ia_Rslt_Adi_Tiaa_Sttlmnt; }

    public DbsField getAds_Ia_Rslt_Adi_Tiaa_Re_Sttlmnt() { return ads_Ia_Rslt_Adi_Tiaa_Re_Sttlmnt; }

    public DbsField getAds_Ia_Rslt_Adi_Cref_Sttlmnt() { return ads_Ia_Rslt_Adi_Cref_Sttlmnt; }

    public DbsGroup getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Tpa_Guar_Commut_Dat() { return ads_Ia_Rslt_Pnd_Adi_Dtl_Tia_Tpa_Guar_Commut_Dat; }

    public DbsField getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Std_Amt() { return ads_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Std_Amt; }

    public DbsField getAds_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Grd_Amt() { return ads_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Grd_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Orig_Lob_Ind() { return ads_Ia_Rslt_Adi_Orig_Lob_Ind; }

    public DbsField getAds_Ia_Rslt_Adi_Roth_Rqst_Ind() { return ads_Ia_Rslt_Adi_Roth_Rqst_Ind; }

    public DbsField getAds_Ia_Rslt_Adi_Srvvr_Ind() { return ads_Ia_Rslt_Adi_Srvvr_Ind; }

    public DbsField getAds_Ia_Rslt_Pnd_Adi_Contract_Id() { return ads_Ia_Rslt_Pnd_Adi_Contract_Id; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        ads_Ia_Rslt = newGroupInRecord("ads_Ia_Rslt", "ADS-IA-RSLT");
        ads_Ia_Rslt_Rqst_Id = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Rqst_Id", "RQST-ID", FieldType.STRING, 35);
        ads_Ia_Rslt_Adi_Ia_Rcrd_Cde = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Ia_Rcrd_Cde", "ADI-IA-RCRD-CDE", FieldType.STRING, 3);
        ads_Ia_Rslt_Adi_Sqnce_Nbr = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Sqnce_Nbr", "ADI-SQNCE-NBR", FieldType.NUMERIC, 3);
        ads_Ia_Rslt_Adi_Stts_Cde = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Stts_Cde", "ADI-STTS-CDE", FieldType.STRING, 3);
        ads_Ia_Rslt_Adi_Stts_CdeRedef1 = ads_Ia_Rslt.newGroupInGroup("ads_Ia_Rslt_Adi_Stts_CdeRedef1", "Redefines", ads_Ia_Rslt_Adi_Stts_Cde);
        ads_Ia_Rslt_Adi_Stts_Cde_1 = ads_Ia_Rslt_Adi_Stts_CdeRedef1.newFieldInGroup("ads_Ia_Rslt_Adi_Stts_Cde_1", "ADI-STTS-CDE-1", FieldType.STRING, 
            1);
        ads_Ia_Rslt_Adi_Stts_Cde_2 = ads_Ia_Rslt_Adi_Stts_CdeRedef1.newFieldInGroup("ads_Ia_Rslt_Adi_Stts_Cde_2", "ADI-STTS-CDE-2", FieldType.STRING, 
            2);
        ads_Ia_Rslt_Adi_Tiaa_Nbrs = ads_Ia_Rslt.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Tiaa_Nbrs", "ADI-TIAA-NBRS", FieldType.STRING, 10, new DbsArrayController(1,
            12));
        ads_Ia_Rslt_Adi_Cref_Nbrs = ads_Ia_Rslt.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Cref_Nbrs", "ADI-CREF-NBRS", FieldType.STRING, 10, new DbsArrayController(1,
            12));
        ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr", "ADI-IA-TIAA-NBR", FieldType.STRING, 10);
        ads_Ia_Rslt_Adi_Ia_Tiaa_NbrRedef2 = ads_Ia_Rslt.newGroupInGroup("ads_Ia_Rslt_Adi_Ia_Tiaa_NbrRedef2", "Redefines", ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr);
        ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr_8 = ads_Ia_Rslt_Adi_Ia_Tiaa_NbrRedef2.newFieldInGroup("ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr_8", "ADI-IA-TIAA-NBR-8", FieldType.STRING, 
            8);
        ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr_2 = ads_Ia_Rslt_Adi_Ia_Tiaa_NbrRedef2.newFieldInGroup("ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr_2", "ADI-IA-TIAA-NBR-2", FieldType.STRING, 
            2);
        ads_Ia_Rslt_Adi_Ia_Tiaa_Payee_Cde = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Ia_Tiaa_Payee_Cde", "ADI-IA-TIAA-PAYEE-CDE", FieldType.STRING, 
            2);
        ads_Ia_Rslt_Adi_Ia_Cref_Nbr = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Ia_Cref_Nbr", "ADI-IA-CREF-NBR", FieldType.STRING, 10);
        ads_Ia_Rslt_Adi_Ia_Cref_Payee_Cde = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Ia_Cref_Payee_Cde", "ADI-IA-CREF-PAYEE-CDE", FieldType.STRING, 
            2);
        ads_Ia_Rslt_Adi_Curncy_Cde = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Curncy_Cde", "ADI-CURNCY-CDE", FieldType.STRING, 1);
        ads_Ia_Rslt_Adi_Pymnt_Cde = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Pymnt_Cde", "ADI-PYMNT-CDE", FieldType.STRING, 1);
        ads_Ia_Rslt_Adi_Pymnt_Mode = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Pymnt_Mode", "ADI-PYMNT-MODE", FieldType.NUMERIC, 3);
        ads_Ia_Rslt_Adi_Pymnt_ModeRedef3 = ads_Ia_Rslt.newGroupInGroup("ads_Ia_Rslt_Adi_Pymnt_ModeRedef3", "Redefines", ads_Ia_Rslt_Adi_Pymnt_Mode);
        ads_Ia_Rslt_Adi_Pymnt_Mode_1 = ads_Ia_Rslt_Adi_Pymnt_ModeRedef3.newFieldInGroup("ads_Ia_Rslt_Adi_Pymnt_Mode_1", "ADI-PYMNT-MODE-1", FieldType.NUMERIC, 
            1);
        ads_Ia_Rslt_Adi_Pymnt_Mode_2 = ads_Ia_Rslt_Adi_Pymnt_ModeRedef3.newFieldInGroup("ads_Ia_Rslt_Adi_Pymnt_Mode_2", "ADI-PYMNT-MODE-2", FieldType.NUMERIC, 
            2);
        ads_Ia_Rslt_Adi_Lst_Actvty_Dte = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Lst_Actvty_Dte", "ADI-LST-ACTVTY-DTE", FieldType.DATE);
        ads_Ia_Rslt_Adi_Annty_Strt_Dte = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Annty_Strt_Dte", "ADI-ANNTY-STRT-DTE", FieldType.DATE);
        ads_Ia_Rslt_Adi_Instllmnt_Dte = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Instllmnt_Dte", "ADI-INSTLLMNT-DTE", FieldType.DATE);
        ads_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte", "ADI-FRST-PYMNT-DUE-DTE", FieldType.DATE);
        ads_Ia_Rslt_Adi_Frst_Ck_Pd_Dte = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Frst_Ck_Pd_Dte", "ADI-FRST-CK-PD-DTE", FieldType.DATE);
        ads_Ia_Rslt_Adi_Finl_Periodic_Py_Dte = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Finl_Periodic_Py_Dte", "ADI-FINL-PERIODIC-PY-DTE", FieldType.DATE);
        ads_Ia_Rslt_Adi_Finl_Prtl_Py_Dte = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Finl_Prtl_Py_Dte", "ADI-FINL-PRTL-PY-DTE", FieldType.DATE);
        ads_Ia_Rslt_Adi_Ivc_Data = ads_Ia_Rslt.newGroupInGroup("ads_Ia_Rslt_Adi_Ivc_Data", "ADI-IVC-DATA");
        ads_Ia_Rslt_Adi_Tiaa_Ivc_Amt = ads_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rslt_Adi_Tiaa_Ivc_Amt", "ADI-TIAA-IVC-AMT", FieldType.DECIMAL, 
            9,2);
        ads_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind = ads_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind", "ADI-TIAA-IVC-GD-IND", FieldType.STRING, 
            1);
        ads_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt = ads_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt", "ADI-TIAA-PRDC-IVC-AMT", FieldType.DECIMAL, 
            9,2);
        ads_Ia_Rslt_Adi_Cref_Ivc_Amt = ads_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rslt_Adi_Cref_Ivc_Amt", "ADI-CREF-IVC-AMT", FieldType.DECIMAL, 
            9,2);
        ads_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind = ads_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind", "ADI-CREF-IVC-GD-IND", FieldType.STRING, 
            1);
        ads_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt = ads_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt", "ADI-CREF-PRDC-IVC-AMT", FieldType.DECIMAL, 
            9,2);
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Data = ads_Ia_Rslt.newGroupInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Data", "ADI-DTL-TIAA-DATA");
        ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Rate_Cd", "#ADI-DTL-TIAA-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1,250));
        ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt", "#ADI-DTL-TIAA-DA-STD-AMT", 
            FieldType.DECIMAL, 9,2, new DbsArrayController(1,250));
        ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt", "#ADI-DTL-TIAA-DA-GRD-AMT", 
            FieldType.DECIMAL, 9,2, new DbsArrayController(1,250));
        ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Acct_Cd = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Acct_Cd", "#ADI-DTL-TIAA-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1,250));
        ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd", "#ADI-DTL-TIAA-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1,250));
        ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Eff_Rte_Intrst = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Eff_Rte_Intrst", 
            "#ADI-DTL-TIAA-EFF-RTE-INTRST", FieldType.DECIMAL, 5,3, new DbsArrayController(1,250));
        ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt", 
            "#ADI-DTL-TIAA-GRD-GRNTD-AMT", FieldType.DECIMAL, 9,2, new DbsArrayController(1,250));
        ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt", 
            "#ADI-DTL-TIAA-GRD-DVDND-AMT", FieldType.DECIMAL, 9,2, new DbsArrayController(1,250));
        ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt", "#ADI-DTL-FNL-GRD-PAY-AMT", 
            FieldType.DECIMAL, 9,2, new DbsArrayController(1,250));
        ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt", "#ADI-DTL-FNL-GRD-DVDND-AMT", 
            FieldType.DECIMAL, 9,2, new DbsArrayController(1,250));
        ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt", 
            "#ADI-DTL-TIAA-STD-GRNTD-AMT", FieldType.DECIMAL, 9,2, new DbsArrayController(1,250));
        ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt", 
            "#ADI-DTL-TIAA-STD-DVDND-AMT", FieldType.DECIMAL, 9,2, new DbsArrayController(1,250));
        ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt", "#ADI-DTL-FNL-STD-PAY-AMT", 
            FieldType.DECIMAL, 9,2, new DbsArrayController(1,250));
        ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt", "#ADI-DTL-FNL-STD-DVDND-AMT", 
            FieldType.DECIMAL, 9,2, new DbsArrayController(1,250));
        ads_Ia_Rslt_Adi_Dtl_Cref_Data = ads_Ia_Rslt.newGroupInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Data", "ADI-DTL-CREF-DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Da_Rate_Cd = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Da_Rate_Cd", "ADI-DTL-CREF-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1,20));
        ads_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd", "ADI-DTL-CREF-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1,20));
        ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd", "ADI-DTL-CREF-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1,20));
        ads_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt", "ADI-DTL-CREF-DA-ANNL-AMT", 
            FieldType.DECIMAL, 9,2, new DbsArrayController(1,20));
        ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units", "ADI-DTL-CREF-ANNL-NBR-UNITS", 
            FieldType.DECIMAL, 10,4, new DbsArrayController(1,20));
        ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val", "ADI-DTL-CREF-IA-ANNL-UNIT-VAL", 
            FieldType.DECIMAL, 7,4, new DbsArrayController(1,20));
        ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Amt = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Amt", "ADI-DTL-CREF-ANNL-AMT", 
            FieldType.DECIMAL, 11,2, new DbsArrayController(1,20));
        ads_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt", "ADI-DTL-CREF-DA-MNTHLY-AMT", 
            FieldType.DECIMAL, 9,2, new DbsArrayController(1,20));
        ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units", "ADI-DTL-CREF-MNTHLY-NBR-UNITS", 
            FieldType.DECIMAL, 10,4, new DbsArrayController(1,20));
        ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val", 
            "ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL", FieldType.DECIMAL, 7,4, new DbsArrayController(1,20));
        ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Amt = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Amt", "ADI-DTL-CREF-MNTHLY-AMT", 
            FieldType.DECIMAL, 11,2, new DbsArrayController(1,20));
        ads_Ia_Rslt_Adi_Optn_Cde = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Optn_Cde", "ADI-OPTN-CDE", FieldType.NUMERIC, 2);
        ads_Ia_Rslt_Adi_Tiaa_Sttlmnt = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Tiaa_Sttlmnt", "ADI-TIAA-STTLMNT", FieldType.STRING, 1);
        ads_Ia_Rslt_Adi_Tiaa_Re_Sttlmnt = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Tiaa_Re_Sttlmnt", "ADI-TIAA-RE-STTLMNT", FieldType.STRING, 1);
        ads_Ia_Rslt_Adi_Cref_Sttlmnt = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Cref_Sttlmnt", "ADI-CREF-STTLMNT", FieldType.STRING, 1);
        ads_Ia_Rslt_Pnd_Adi_Dtl_Tia_Tpa_Guar_Commut_Dat = ads_Ia_Rslt.newGroupInGroup("ads_Ia_Rslt_Pnd_Adi_Dtl_Tia_Tpa_Guar_Commut_Dat", "#ADI-DTL-TIA-TPA-GUAR-COMMUT-DAT");
        ads_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Std_Amt = ads_Ia_Rslt_Pnd_Adi_Dtl_Tia_Tpa_Guar_Commut_Dat.newFieldArrayInGroup("ads_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Std_Amt", 
            "#ADI-DTL-TIA-GUAR-COMMUT-STD-AMT", FieldType.DECIMAL, 9,2, new DbsArrayController(1,250));
        ads_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Grd_Amt = ads_Ia_Rslt_Pnd_Adi_Dtl_Tia_Tpa_Guar_Commut_Dat.newFieldArrayInGroup("ads_Ia_Rslt_Pnd_Adi_Dtl_Tia_Guar_Commut_Grd_Amt", 
            "#ADI-DTL-TIA-GUAR-COMMUT-GRD-AMT", FieldType.DECIMAL, 9,2, new DbsArrayController(1,250));
        ads_Ia_Rslt_Adi_Orig_Lob_Ind = ads_Ia_Rslt.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Orig_Lob_Ind", "ADI-ORIG-LOB-IND", FieldType.STRING, 1, new DbsArrayController(1,
            12));
        ads_Ia_Rslt_Adi_Roth_Rqst_Ind = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Roth_Rqst_Ind", "ADI-ROTH-RQST-IND", FieldType.STRING, 1);
        ads_Ia_Rslt_Adi_Srvvr_Ind = ads_Ia_Rslt.newFieldInGroup("ads_Ia_Rslt_Adi_Srvvr_Ind", "ADI-SRVVR-IND", FieldType.STRING, 1);
        ads_Ia_Rslt_Pnd_Adi_Contract_Id = ads_Ia_Rslt.newFieldArrayInGroup("ads_Ia_Rslt_Pnd_Adi_Contract_Id", "#ADI-CONTRACT-ID", FieldType.NUMERIC, 11, 
            new DbsArrayController(1,250));

        this.setRecordName("LdaAdsl450a");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAdsl450a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
