/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:49:27 PM
**        * FROM NATURAL LDA     : ADSL1350
************************************************************
**        * FILE NAME            : LdaAdsl1350.java
**        * CLASS NAME           : LdaAdsl1350
**        * INSTANCE NAME        : LdaAdsl1350
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdsl1350 extends DbsRecord
{
    // Properties
    private DbsField pnd_Accounting_Date;
    private DbsGroup pnd_Accounting_DateRedef1;
    private DbsField pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm;
    private DbsField pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd;
    private DbsField pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc;
    private DbsField pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy;
    private DbsField pnd_Effective_Date;
    private DbsGroup pnd_Effective_DateRedef2;
    private DbsField pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Mm;
    private DbsField pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Dd;
    private DbsField pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Cc;
    private DbsField pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Yy;
    private DbsGroup pnd_F_Tiaa_Minor_Accum;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Units;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Dollars;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Final_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Final_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Final_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Final_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Final_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Final_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Units;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Dollars;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Final_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Final_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Final_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Final_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Final_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Final_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Final_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Final_Div_Amt;
    private DbsGroup pnd_F_Tiaa_Sub_Accum;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Units;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Dollars;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Divid_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Div_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Div_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Final_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Final_Div_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Div_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Final_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Final_Div_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Div_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Final_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Final_Div_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Div_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Final_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Final_Div_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Div_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Final_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Final_Div_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Div_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Final_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Final_Div_Amt;
    private DbsGroup pnd_F_Cref_Mth_Minor_Accum;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt;
    private DbsGroup pnd_F_Cref_Ann_Minor_Accum;
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt;
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units;
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars;
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt;
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt;
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt;
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt;
    private DbsGroup pnd_F_Cref_Mth_Sub_Total_Accum;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Pay_Cnt;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Units;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Dollars;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Gtd_Amt;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Div_Amt;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Gtd_Amt;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Div_Amt;
    private DbsGroup pnd_F_Cref_Ann_Sub_Total_Accum;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Pay_Cnt;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Units;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Dollars;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Gtd_Amt;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Div_Amt;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Gtd_Amt;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Div_Amt;
    private DbsGroup pnd_F_Total_Tiaa_Cref_Accum;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Units;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Dollars;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Gtd_Amt;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Div_Amt;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Gtd_Amt;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Div_Amt;

    public DbsField getPnd_Accounting_Date() { return pnd_Accounting_Date; }

    public DbsGroup getPnd_Accounting_DateRedef1() { return pnd_Accounting_DateRedef1; }

    public DbsField getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm() { return pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm; }

    public DbsField getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd() { return pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd; }

    public DbsField getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc() { return pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc; }

    public DbsField getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy() { return pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy; }

    public DbsField getPnd_Effective_Date() { return pnd_Effective_Date; }

    public DbsGroup getPnd_Effective_DateRedef2() { return pnd_Effective_DateRedef2; }

    public DbsField getPnd_Effective_Date_Pnd_Effective_Date_Mcdy_Mm() { return pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Mm; }

    public DbsField getPnd_Effective_Date_Pnd_Effective_Date_Mcdy_Dd() { return pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Dd; }

    public DbsField getPnd_Effective_Date_Pnd_Effective_Date_Mcdy_Cc() { return pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Cc; }

    public DbsField getPnd_Effective_Date_Pnd_Effective_Date_Mcdy_Yy() { return pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Yy; }

    public DbsGroup getPnd_F_Tiaa_Minor_Accum() { return pnd_F_Tiaa_Minor_Accum; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Units() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Units; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Dollars() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Dollars; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Pay_Cnt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Pay_Cnt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Gtd_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Div_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Final_Gtd_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Final_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Final_Div_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Final_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Pay_Cnt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Pay_Cnt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Gtd_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Div_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Final_Gtd_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Final_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Final_Div_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Final_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Pay_Cnt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Pay_Cnt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Gtd_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Div_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Final_Gtd_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Final_Gtd_Amt; 
        }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Final_Div_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Final_Div_Amt; 
        }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Pay_Cnt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Pay_Cnt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Units() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Units; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Dollars() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Dollars; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Gtd_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Div_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Gtd_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Div_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Pay_Cnt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Pay_Cnt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Gtd_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Div_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Final_Gtd_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Final_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Final_Div_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Final_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Pay_Cnt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Pay_Cnt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Gtd_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Div_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Final_Gtd_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Final_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Final_Div_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Final_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Pay_Cnt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Pay_Cnt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Gtd_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Div_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Final_Gtd_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Final_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Final_Div_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Final_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Pay_Cnt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Pay_Cnt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Gtd_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Div_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Final_Gtd_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Final_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Final_Div_Amt() { return pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Final_Div_Amt; }

    public DbsGroup getPnd_F_Tiaa_Sub_Accum() { return pnd_F_Tiaa_Sub_Accum; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Pay_Cnt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Pay_Cnt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Units() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Units; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Dollars() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Dollars; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Gtd_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Divid_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Divid_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Gtd_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Div_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Pay_Cnt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Pay_Cnt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Gtd_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Div_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Final_Gtd_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Final_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Final_Div_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Final_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Pay_Cnt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Pay_Cnt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Gtd_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Div_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Final_Gtd_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Final_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Final_Div_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Final_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Pay_Cnt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Pay_Cnt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Gtd_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Div_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Final_Gtd_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Final_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Final_Div_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Final_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Pay_Cnt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Pay_Cnt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Gtd_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Div_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Final_Gtd_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Final_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Final_Div_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Final_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Pay_Cnt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Pay_Cnt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Gtd_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Div_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Final_Gtd_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Final_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Final_Div_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Final_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Pay_Cnt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Pay_Cnt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Gtd_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Div_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Div_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Final_Gtd_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Final_Gtd_Amt; }

    public DbsField getPnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Final_Div_Amt() { return pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Final_Div_Amt; }

    public DbsGroup getPnd_F_Cref_Mth_Minor_Accum() { return pnd_F_Cref_Mth_Minor_Accum; }

    public DbsField getPnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt() { return pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt; }

    public DbsField getPnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units() { return pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units; }

    public DbsField getPnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars() { return pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars; }

    public DbsField getPnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt() { return pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt; }

    public DbsField getPnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt() { return pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt; }

    public DbsField getPnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt() { return pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt; }

    public DbsField getPnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt() { return pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt; }

    public DbsGroup getPnd_F_Cref_Ann_Minor_Accum() { return pnd_F_Cref_Ann_Minor_Accum; }

    public DbsField getPnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt() { return pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt; }

    public DbsField getPnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units() { return pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units; }

    public DbsField getPnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars() { return pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars; }

    public DbsField getPnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt() { return pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt; }

    public DbsField getPnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt() { return pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt; }

    public DbsField getPnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt() { return pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt; }

    public DbsField getPnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt() { return pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt; }

    public DbsGroup getPnd_F_Cref_Mth_Sub_Total_Accum() { return pnd_F_Cref_Mth_Sub_Total_Accum; }

    public DbsField getPnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Pay_Cnt() { return pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Pay_Cnt; 
        }

    public DbsField getPnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Units() { return pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Units; }

    public DbsField getPnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Dollars() { return pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Dollars; 
        }

    public DbsField getPnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Gtd_Amt() { return pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Gtd_Amt; 
        }

    public DbsField getPnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Div_Amt() { return pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Div_Amt; 
        }

    public DbsField getPnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Gtd_Amt() { return pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Gtd_Amt; 
        }

    public DbsField getPnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Div_Amt() { return pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Div_Amt; 
        }

    public DbsGroup getPnd_F_Cref_Ann_Sub_Total_Accum() { return pnd_F_Cref_Ann_Sub_Total_Accum; }

    public DbsField getPnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Pay_Cnt() { return pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Pay_Cnt; 
        }

    public DbsField getPnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Units() { return pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Units; }

    public DbsField getPnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Dollars() { return pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Dollars; 
        }

    public DbsField getPnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Gtd_Amt() { return pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Gtd_Amt; 
        }

    public DbsField getPnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Div_Amt() { return pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Div_Amt; 
        }

    public DbsField getPnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Gtd_Amt() { return pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Gtd_Amt; 
        }

    public DbsField getPnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Div_Amt() { return pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Div_Amt; 
        }

    public DbsGroup getPnd_F_Total_Tiaa_Cref_Accum() { return pnd_F_Total_Tiaa_Cref_Accum; }

    public DbsField getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt() { return pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt; }

    public DbsField getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Units() { return pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Units; }

    public DbsField getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Dollars() { return pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Dollars; }

    public DbsField getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Gtd_Amt() { return pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Gtd_Amt; }

    public DbsField getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Div_Amt() { return pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Div_Amt; }

    public DbsField getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Gtd_Amt() { return pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Gtd_Amt; }

    public DbsField getPnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Div_Amt() { return pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Div_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Accounting_Date = newFieldInRecord("pnd_Accounting_Date", "#ACCOUNTING-DATE", FieldType.NUMERIC, 8);
        pnd_Accounting_DateRedef1 = newGroupInRecord("pnd_Accounting_DateRedef1", "Redefines", pnd_Accounting_Date);
        pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm = pnd_Accounting_DateRedef1.newFieldInGroup("pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm", 
            "#ACCOUNTING-DATE-MCDY-MM", FieldType.NUMERIC, 2);
        pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd = pnd_Accounting_DateRedef1.newFieldInGroup("pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd", 
            "#ACCOUNTING-DATE-MCDY-DD", FieldType.NUMERIC, 2);
        pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc = pnd_Accounting_DateRedef1.newFieldInGroup("pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc", 
            "#ACCOUNTING-DATE-MCDY-CC", FieldType.NUMERIC, 2);
        pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy = pnd_Accounting_DateRedef1.newFieldInGroup("pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy", 
            "#ACCOUNTING-DATE-MCDY-YY", FieldType.NUMERIC, 2);

        pnd_Effective_Date = newFieldInRecord("pnd_Effective_Date", "#EFFECTIVE-DATE", FieldType.NUMERIC, 8);
        pnd_Effective_DateRedef2 = newGroupInRecord("pnd_Effective_DateRedef2", "Redefines", pnd_Effective_Date);
        pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Mm = pnd_Effective_DateRedef2.newFieldInGroup("pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Mm", "#EFFECTIVE-DATE-MCDY-MM", 
            FieldType.NUMERIC, 2);
        pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Dd = pnd_Effective_DateRedef2.newFieldInGroup("pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Dd", "#EFFECTIVE-DATE-MCDY-DD", 
            FieldType.NUMERIC, 2);
        pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Cc = pnd_Effective_DateRedef2.newFieldInGroup("pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Cc", "#EFFECTIVE-DATE-MCDY-CC", 
            FieldType.NUMERIC, 2);
        pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Yy = pnd_Effective_DateRedef2.newFieldInGroup("pnd_Effective_Date_Pnd_Effective_Date_Mcdy_Yy", "#EFFECTIVE-DATE-MCDY-YY", 
            FieldType.NUMERIC, 2);

        pnd_F_Tiaa_Minor_Accum = newGroupInRecord("pnd_F_Tiaa_Minor_Accum", "#F-TIAA-MINOR-ACCUM");
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Units = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Units", "#F-TIAA-STD-UNITS", 
            FieldType.PACKED_DECIMAL, 13,4);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Dollars = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Dollars", "#F-TIAA-STD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Pay_Cnt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Pay_Cnt", 
            "#F-TIAA-STD-CASH-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Gtd_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Gtd_Amt", 
            "#F-TIAA-STD-CASH-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Div_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Div_Amt", 
            "#F-TIAA-STD-CASH-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Final_Gtd_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Final_Gtd_Amt", 
            "#F-TIAA-STD-CASH-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Final_Div_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Final_Div_Amt", 
            "#F-TIAA-STD-CASH-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Pay_Cnt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Pay_Cnt", 
            "#F-TIAA-STD-RLVR-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Gtd_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Gtd_Amt", 
            "#F-TIAA-STD-RLVR-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Div_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Div_Amt", 
            "#F-TIAA-STD-RLVR-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Final_Gtd_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Final_Gtd_Amt", 
            "#F-TIAA-STD-RLVR-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Final_Div_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Final_Div_Amt", 
            "#F-TIAA-STD-RLVR-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Pay_Cnt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Pay_Cnt", 
            "#F-TIAA-STD-TOTAL-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Gtd_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Gtd_Amt", 
            "#F-TIAA-STD-TOTAL-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Div_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Div_Amt", 
            "#F-TIAA-STD-TOTAL-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Final_Gtd_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Final_Gtd_Amt", 
            "#F-TIAA-STD-TOTAL-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Final_Div_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Final_Div_Amt", 
            "#F-TIAA-STD-TOTAL-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Pay_Cnt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Pay_Cnt", "#F-TIAA-GRD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Units = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Units", "#F-TIAA-GRD-UNITS", 
            FieldType.PACKED_DECIMAL, 13,4);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Dollars = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Dollars", "#F-TIAA-GRD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Gtd_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Gtd_Amt", "#F-TIAA-GRD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Div_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Div_Amt", "#F-TIAA-GRD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Gtd_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Gtd_Amt", 
            "#F-TIAA-GRD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Div_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Div_Amt", 
            "#F-TIAA-GRD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Pay_Cnt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Pay_Cnt", "#F-IPRO-CASH-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Gtd_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Gtd_Amt", "#F-IPRO-CASH-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Div_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Div_Amt", "#F-IPRO-CASH-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Final_Gtd_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Final_Gtd_Amt", 
            "#F-IPRO-CASH-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Final_Div_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Cash_Final_Div_Amt", 
            "#F-IPRO-CASH-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Pay_Cnt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Pay_Cnt", 
            "#F-IPRO-EXT-RLVR-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Gtd_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Gtd_Amt", 
            "#F-IPRO-EXT-RLVR-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Div_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Div_Amt", 
            "#F-IPRO-EXT-RLVR-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Final_Gtd_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Final_Gtd_Amt", 
            "#F-IPRO-EXT-RLVR-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Final_Div_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Final_Div_Amt", 
            "#F-IPRO-EXT-RLVR-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Pay_Cnt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Pay_Cnt", 
            "#F-IPRO-INT-RLVR-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Gtd_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Gtd_Amt", 
            "#F-IPRO-INT-RLVR-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Div_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Div_Amt", 
            "#F-IPRO-INT-RLVR-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Final_Gtd_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Final_Gtd_Amt", 
            "#F-IPRO-INT-RLVR-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Final_Div_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Final_Div_Amt", 
            "#F-IPRO-INT-RLVR-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Pay_Cnt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Pay_Cnt", "#F-IPRO-TOTAL-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Gtd_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Gtd_Amt", "#F-IPRO-TOTAL-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Div_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Div_Amt", "#F-IPRO-TOTAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Final_Gtd_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Final_Gtd_Amt", 
            "#F-IPRO-TOTAL-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Final_Div_Amt = pnd_F_Tiaa_Minor_Accum.newFieldInGroup("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Total_Final_Div_Amt", 
            "#F-IPRO-TOTAL-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);

        pnd_F_Tiaa_Sub_Accum = newGroupInRecord("pnd_F_Tiaa_Sub_Accum", "#F-TIAA-SUB-ACCUM");
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Pay_Cnt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Pay_Cnt", "#F-TIAA-SUB-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Units = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Units", "#F-TIAA-SUB-UNITS", 
            FieldType.PACKED_DECIMAL, 13,4);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Dollars = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Dollars", "#F-TIAA-SUB-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Gtd_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Gtd_Amt", "#F-TIAA-SUB-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Divid_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Divid_Amt", "#F-TIAA-SUB-DIVID-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Gtd_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Gtd_Amt", "#F-TIAA-SUB-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Div_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Div_Amt", "#F-TIAA-SUB-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Pay_Cnt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Pay_Cnt", "#F-TPA-INTR-RLVR-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Gtd_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Gtd_Amt", "#F-TPA-INTR-RLVR-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Div_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Div_Amt", "#F-TPA-INTR-RLVR-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Final_Gtd_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Final_Gtd_Amt", 
            "#F-TPA-INTR-RLVR-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Final_Div_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Intr_Rlvr_Final_Div_Amt", 
            "#F-TPA-INTR-RLVR-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Pay_Cnt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Pay_Cnt", "#F-TPA-IVC-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Gtd_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Gtd_Amt", "#F-TPA-IVC-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Div_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Div_Amt", "#F-TPA-IVC-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Final_Gtd_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Final_Gtd_Amt", "#F-TPA-IVC-FINAL-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Final_Div_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Ivc_Final_Div_Amt", "#F-TPA-IVC-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Pay_Cnt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Pay_Cnt", "#F-TPA-RINV-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Gtd_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Gtd_Amt", "#F-TPA-RINV-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Div_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Div_Amt", "#F-TPA-RINV-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Final_Gtd_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Final_Gtd_Amt", 
            "#F-TPA-RINV-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Final_Div_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Rinv_Final_Div_Amt", 
            "#F-TPA-RINV-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Pay_Cnt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Pay_Cnt", "#F-TPA-CASH-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Gtd_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Gtd_Amt", "#F-TPA-CASH-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Div_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Div_Amt", "#F-TPA-CASH-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Final_Gtd_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Final_Gtd_Amt", 
            "#F-TPA-CASH-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Final_Div_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Cash_Final_Div_Amt", 
            "#F-TPA-CASH-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Pay_Cnt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Pay_Cnt", "#F-TPA-EXTR-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Gtd_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Gtd_Amt", "#F-TPA-EXTR-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Div_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Div_Amt", "#F-TPA-EXTR-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Final_Gtd_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Final_Gtd_Amt", 
            "#F-TPA-EXTR-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Final_Div_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Extr_Final_Div_Amt", 
            "#F-TPA-EXTR-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Pay_Cnt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Pay_Cnt", "#F-TPA-TOTAL-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Gtd_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Gtd_Amt", "#F-TPA-TOTAL-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Div_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Div_Amt", "#F-TPA-TOTAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Final_Gtd_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Final_Gtd_Amt", 
            "#F-TPA-TOTAL-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Final_Div_Amt = pnd_F_Tiaa_Sub_Accum.newFieldInGroup("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tpa_Total_Final_Div_Amt", 
            "#F-TPA-TOTAL-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);

        pnd_F_Cref_Mth_Minor_Accum = newGroupArrayInRecord("pnd_F_Cref_Mth_Minor_Accum", "#F-CREF-MTH-MINOR-ACCUM", new DbsArrayController(1,20));
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt = pnd_F_Cref_Mth_Minor_Accum.newFieldInGroup("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt", 
            "#F-CREF-MTH-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units = pnd_F_Cref_Mth_Minor_Accum.newFieldInGroup("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units", 
            "#F-CREF-MTH-UNITS", FieldType.PACKED_DECIMAL, 13,4);
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars = pnd_F_Cref_Mth_Minor_Accum.newFieldInGroup("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars", 
            "#F-CREF-MTH-DOLLARS", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt = pnd_F_Cref_Mth_Minor_Accum.newFieldInGroup("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt", 
            "#F-CREF-MTH-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt = pnd_F_Cref_Mth_Minor_Accum.newFieldInGroup("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt", 
            "#F-CREF-MTH-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt = pnd_F_Cref_Mth_Minor_Accum.newFieldInGroup("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt", 
            "#F-CREF-MTH-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt = pnd_F_Cref_Mth_Minor_Accum.newFieldInGroup("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt", 
            "#F-CREF-MTH-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);

        pnd_F_Cref_Ann_Minor_Accum = newGroupArrayInRecord("pnd_F_Cref_Ann_Minor_Accum", "#F-CREF-ANN-MINOR-ACCUM", new DbsArrayController(1,20));
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt = pnd_F_Cref_Ann_Minor_Accum.newFieldInGroup("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt", 
            "#F-CREF-ANN-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units = pnd_F_Cref_Ann_Minor_Accum.newFieldInGroup("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units", 
            "#F-CREF-ANN-UNITS", FieldType.PACKED_DECIMAL, 13,4);
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars = pnd_F_Cref_Ann_Minor_Accum.newFieldInGroup("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars", 
            "#F-CREF-ANN-DOLLARS", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt = pnd_F_Cref_Ann_Minor_Accum.newFieldInGroup("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt", 
            "#F-CREF-ANN-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt = pnd_F_Cref_Ann_Minor_Accum.newFieldInGroup("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt", 
            "#F-CREF-ANN-DIVID-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt = pnd_F_Cref_Ann_Minor_Accum.newFieldInGroup("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt", 
            "#F-CREF-ANN-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt = pnd_F_Cref_Ann_Minor_Accum.newFieldInGroup("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt", 
            "#F-CREF-ANN-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);

        pnd_F_Cref_Mth_Sub_Total_Accum = newGroupInRecord("pnd_F_Cref_Mth_Sub_Total_Accum", "#F-CREF-MTH-SUB-TOTAL-ACCUM");
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Pay_Cnt = pnd_F_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Pay_Cnt", 
            "#F-CREF-MTH-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Units = pnd_F_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Units", 
            "#F-CREF-MTH-SUB-UNITS", FieldType.PACKED_DECIMAL, 13,4);
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Dollars = pnd_F_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Dollars", 
            "#F-CREF-MTH-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Gtd_Amt = pnd_F_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Gtd_Amt", 
            "#F-CREF-MTH-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Div_Amt = pnd_F_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Div_Amt", 
            "#F-CREF-MTH-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Gtd_Amt = pnd_F_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Gtd_Amt", 
            "#F-CREF-MTH-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Div_Amt = pnd_F_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Div_Amt", 
            "#F-CREF-MTH-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);

        pnd_F_Cref_Ann_Sub_Total_Accum = newGroupInRecord("pnd_F_Cref_Ann_Sub_Total_Accum", "#F-CREF-ANN-SUB-TOTAL-ACCUM");
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Pay_Cnt = pnd_F_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Pay_Cnt", 
            "#F-CREF-ANN-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Units = pnd_F_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Units", 
            "#F-CREF-ANN-SUB-UNITS", FieldType.PACKED_DECIMAL, 13,4);
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Dollars = pnd_F_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Dollars", 
            "#F-CREF-ANN-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Gtd_Amt = pnd_F_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Gtd_Amt", 
            "#F-CREF-ANN-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Div_Amt = pnd_F_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Div_Amt", 
            "#F-CREF-ANN-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Gtd_Amt = pnd_F_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Gtd_Amt", 
            "#F-CREF-ANN-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Div_Amt = pnd_F_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Div_Amt", 
            "#F-CREF-ANN-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);

        pnd_F_Total_Tiaa_Cref_Accum = newGroupInRecord("pnd_F_Total_Tiaa_Cref_Accum", "#F-TOTAL-TIAA-CREF-ACCUM");
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt = pnd_F_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt", 
            "#F-TOTAL-PAY-CNT", FieldType.PACKED_DECIMAL, 10);
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Units = pnd_F_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Units", "#F-TOTAL-UNITS", 
            FieldType.PACKED_DECIMAL, 13,4);
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Dollars = pnd_F_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Dollars", 
            "#F-TOTAL-DOLLARS", FieldType.PACKED_DECIMAL, 14,2);
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Gtd_Amt = pnd_F_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Gtd_Amt", 
            "#F-TOTAL-GTD-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Div_Amt = pnd_F_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Div_Amt", 
            "#F-TOTAL-DIV-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Gtd_Amt = pnd_F_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Gtd_Amt", 
            "#F-TOTAL-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Div_Amt = pnd_F_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Div_Amt", 
            "#F-TOTAL-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 14,2);

        this.setRecordName("LdaAdsl1350");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAdsl1350() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
