/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:21 PM
**        * FROM NATURAL LDA     : ADSLCNTL
************************************************************
**        * FILE NAME            : LdaAdslcntl.java
**        * CLASS NAME           : LdaAdslcntl
**        * INSTANCE NAME        : LdaAdslcntl
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdslcntl extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_ads_Cntl_View;
    private DbsField ads_Cntl_View_Ads_Rcrd_Cde;
    private DbsGroup ads_Cntl_View_Ads_Cntl_Grp;
    private DbsField ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte;
    private DbsField ads_Cntl_View_Ads_Tiaa_Nmbr_Cases_Cnt;
    private DbsGroup ads_Cntl_View_Ads_Cntl_Rprt_Dtes;
    private DbsField ads_Cntl_View_Ads_Inc_Dlay_Trn_Rpt_Dte;
    private DbsField ads_Cntl_View_Ads_Rej_Trn_Rpt_Dte;
    private DbsField ads_Cntl_View_Ads_Trn_Hstry_Extrct_Rpt_Dte;
    private DbsField ads_Cntl_View_Ads_Pndg_Trn_Rpt_Dte;
    private DbsField ads_Cntl_View_Ads_Inst_Lttr_Dte;
    private DbsField ads_Cntl_View_Ads_Cis_Intrfce_Dte;
    private DbsField ads_Cntl_View_Ads_Fnl_Prm_Cntrl_Dte;
    private DbsField ads_Cntl_View_Ads_Daily_Actvty_Dte;
    private DbsField ads_Cntl_View_Ads_Fnl_Prm_Accum_Dte;
    private DbsField ads_Cntl_View_Ads_Rpt_13;
    private DbsField ads_Cntl_View_Ads_Rpt_14;
    private DbsField ads_Cntl_View_Ads_Rpt_15;

    public DataAccessProgramView getVw_ads_Cntl_View() { return vw_ads_Cntl_View; }

    public DbsField getAds_Cntl_View_Ads_Rcrd_Cde() { return ads_Cntl_View_Ads_Rcrd_Cde; }

    public DbsGroup getAds_Cntl_View_Ads_Cntl_Grp() { return ads_Cntl_View_Ads_Cntl_Grp; }

    public DbsField getAds_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde() { return ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde; }

    public DbsField getAds_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte() { return ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte; }

    public DbsField getAds_Cntl_View_Ads_Cntl_Bsnss_Dte() { return ads_Cntl_View_Ads_Cntl_Bsnss_Dte; }

    public DbsField getAds_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte() { return ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte; }

    public DbsField getAds_Cntl_View_Ads_Tiaa_Nmbr_Cases_Cnt() { return ads_Cntl_View_Ads_Tiaa_Nmbr_Cases_Cnt; }

    public DbsGroup getAds_Cntl_View_Ads_Cntl_Rprt_Dtes() { return ads_Cntl_View_Ads_Cntl_Rprt_Dtes; }

    public DbsField getAds_Cntl_View_Ads_Inc_Dlay_Trn_Rpt_Dte() { return ads_Cntl_View_Ads_Inc_Dlay_Trn_Rpt_Dte; }

    public DbsField getAds_Cntl_View_Ads_Rej_Trn_Rpt_Dte() { return ads_Cntl_View_Ads_Rej_Trn_Rpt_Dte; }

    public DbsField getAds_Cntl_View_Ads_Trn_Hstry_Extrct_Rpt_Dte() { return ads_Cntl_View_Ads_Trn_Hstry_Extrct_Rpt_Dte; }

    public DbsField getAds_Cntl_View_Ads_Pndg_Trn_Rpt_Dte() { return ads_Cntl_View_Ads_Pndg_Trn_Rpt_Dte; }

    public DbsField getAds_Cntl_View_Ads_Inst_Lttr_Dte() { return ads_Cntl_View_Ads_Inst_Lttr_Dte; }

    public DbsField getAds_Cntl_View_Ads_Cis_Intrfce_Dte() { return ads_Cntl_View_Ads_Cis_Intrfce_Dte; }

    public DbsField getAds_Cntl_View_Ads_Fnl_Prm_Cntrl_Dte() { return ads_Cntl_View_Ads_Fnl_Prm_Cntrl_Dte; }

    public DbsField getAds_Cntl_View_Ads_Daily_Actvty_Dte() { return ads_Cntl_View_Ads_Daily_Actvty_Dte; }

    public DbsField getAds_Cntl_View_Ads_Fnl_Prm_Accum_Dte() { return ads_Cntl_View_Ads_Fnl_Prm_Accum_Dte; }

    public DbsField getAds_Cntl_View_Ads_Rpt_13() { return ads_Cntl_View_Ads_Rpt_13; }

    public DbsField getAds_Cntl_View_Ads_Rpt_14() { return ads_Cntl_View_Ads_Rpt_14; }

    public DbsField getAds_Cntl_View_Ads_Rpt_15() { return ads_Cntl_View_Ads_Rpt_15; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_ads_Cntl_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntl_View", "ADS-CNTL-VIEW"), "ADS_CNTL", "ADS_CNTL");
        ads_Cntl_View_Ads_Rcrd_Cde = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Rcrd_Cde", "ADS-RCRD-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ADS_RCRD_CDE");
        ads_Cntl_View_Ads_Cntl_Grp = vw_ads_Cntl_View.getRecord().newGroupInGroup("ads_Cntl_View_Ads_Cntl_Grp", "ADS-CNTL-GRP");
        ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Rcrd_Typ_Cde", "ADS-CNTL-RCRD-TYP-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADS_CNTL_RCRD_TYP_CDE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Rcprcl_Dte", "ADS-CNTL-BSNSS-RCPRCL-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_RCPRCL_DTE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_DTE");
        ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte", "ADS-CNTL-BSNSS-TMRRW-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_TMRRW_DTE");
        ads_Cntl_View_Ads_Tiaa_Nmbr_Cases_Cnt = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Tiaa_Nmbr_Cases_Cnt", "ADS-TIAA-NMBR-CASES-CNT", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "ADS_TIAA_NMBR_CASES_CNT");
        ads_Cntl_View_Ads_Cntl_Rprt_Dtes = vw_ads_Cntl_View.getRecord().newGroupInGroup("ads_Cntl_View_Ads_Cntl_Rprt_Dtes", "ADS-CNTL-RPRT-DTES");
        ads_Cntl_View_Ads_Inc_Dlay_Trn_Rpt_Dte = ads_Cntl_View_Ads_Cntl_Rprt_Dtes.newFieldInGroup("ads_Cntl_View_Ads_Inc_Dlay_Trn_Rpt_Dte", "ADS-INC-DLAY-TRN-RPT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADS_INC_DLAY_TRN_RPT_DTE");
        ads_Cntl_View_Ads_Rej_Trn_Rpt_Dte = ads_Cntl_View_Ads_Cntl_Rprt_Dtes.newFieldInGroup("ads_Cntl_View_Ads_Rej_Trn_Rpt_Dte", "ADS-REJ-TRN-RPT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADS_REJ_TRN_RPT_DTE");
        ads_Cntl_View_Ads_Trn_Hstry_Extrct_Rpt_Dte = ads_Cntl_View_Ads_Cntl_Rprt_Dtes.newFieldInGroup("ads_Cntl_View_Ads_Trn_Hstry_Extrct_Rpt_Dte", "ADS-TRN-HSTRY-EXTRCT-RPT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADS_TRN_HSTRY_EXTRCT_RPT_DTE");
        ads_Cntl_View_Ads_Pndg_Trn_Rpt_Dte = ads_Cntl_View_Ads_Cntl_Rprt_Dtes.newFieldInGroup("ads_Cntl_View_Ads_Pndg_Trn_Rpt_Dte", "ADS-PNDG-TRN-RPT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADS_PNDG_TRN_RPT_DTE");
        ads_Cntl_View_Ads_Inst_Lttr_Dte = ads_Cntl_View_Ads_Cntl_Rprt_Dtes.newFieldInGroup("ads_Cntl_View_Ads_Inst_Lttr_Dte", "ADS-INST-LTTR-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADS_INST_LTTR_DTE");
        ads_Cntl_View_Ads_Cis_Intrfce_Dte = ads_Cntl_View_Ads_Cntl_Rprt_Dtes.newFieldInGroup("ads_Cntl_View_Ads_Cis_Intrfce_Dte", "ADS-CIS-INTRFCE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADS_CIS_INTRFCE_DTE");
        ads_Cntl_View_Ads_Fnl_Prm_Cntrl_Dte = ads_Cntl_View_Ads_Cntl_Rprt_Dtes.newFieldInGroup("ads_Cntl_View_Ads_Fnl_Prm_Cntrl_Dte", "ADS-FNL-PRM-CNTRL-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADS_FNL_PRM_CNTRL_DTE");
        ads_Cntl_View_Ads_Daily_Actvty_Dte = ads_Cntl_View_Ads_Cntl_Rprt_Dtes.newFieldInGroup("ads_Cntl_View_Ads_Daily_Actvty_Dte", "ADS-DAILY-ACTVTY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADS_DAILY_ACTVTY_DTE");
        ads_Cntl_View_Ads_Fnl_Prm_Accum_Dte = ads_Cntl_View_Ads_Cntl_Rprt_Dtes.newFieldInGroup("ads_Cntl_View_Ads_Fnl_Prm_Accum_Dte", "ADS-FNL-PRM-ACCUM-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADS_FNL_PRM_ACCUM_DTE");
        ads_Cntl_View_Ads_Rpt_13 = ads_Cntl_View_Ads_Cntl_Rprt_Dtes.newFieldInGroup("ads_Cntl_View_Ads_Rpt_13", "ADS-RPT-13", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADS_RPT_13");
        ads_Cntl_View_Ads_Rpt_14 = ads_Cntl_View_Ads_Cntl_Rprt_Dtes.newFieldInGroup("ads_Cntl_View_Ads_Rpt_14", "ADS-RPT-14", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADS_RPT_14");
        ads_Cntl_View_Ads_Rpt_15 = ads_Cntl_View_Ads_Cntl_Rprt_Dtes.newFieldInGroup("ads_Cntl_View_Ads_Rpt_15", "ADS-RPT-15", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADS_RPT_15");

        this.setRecordName("LdaAdslcntl");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_ads_Cntl_View.reset();
    }

    // Constructor
    public LdaAdslcntl() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
