/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:10:09 PM
**        * FROM NATURAL LDA     : PSTL0175
************************************************************
**        * FILE NAME            : LdaPstl0175.java
**        * CLASS NAME           : LdaPstl0175
**        * INSTANCE NAME        : LdaPstl0175
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaPstl0175 extends DbsRecord
{
    // Properties
    private DbsGroup pstl0175;
    private DbsField pstl0175_Pstl0175_Rec_Id;
    private DbsField pstl0175_Pstl0175_Data_Lgth;
    private DbsField pstl0175_Pstl0175_Data_Occurs;
    private DbsField pstl0175_Pstl0175_First_Record;
    private DbsField pstl0175_Pstl0175_Data_Array;
    private DbsGroup pstl0175_Pstl0175_Data_ArrayRedef1;
    private DbsGroup pstl0175_Participant_Info;
    private DbsField pstl0175_Participant_Name;
    private DbsField pstl0175_Adp_Frst_Annt_Ssn;
    private DbsField pstl0175_Adp_Frst_Annt_Dte_Of_Brth_N;
    private DbsField pstl0175_Adp_Crrspndnce_Perm_Addr_Txt;
    private DbsField pstl0175_Adp_Crrspndnce_Perm_Addr_Zip;
    private DbsField pstl0175_Adp_Frst_Annt_Rsdnc_Cde;
    private DbsField pstl0175_Adp_Frst_Annt_Ctznshp;
    private DbsGroup pstl0175_Settlement_Ia;
    private DbsField pstl0175_Adp_Annty_Optn;
    private DbsField pstl0175_Adp_Grntee_Period;
    private DbsField pstl0175_Adp_Annty_Strt_Dte_N;
    private DbsField pstl0175_Adp_Pymnt_Mode;
    private DbsGroup pstl0175_Adp_Pymnt_ModeRedef2;
    private DbsField pstl0175_Pay_Freq;
    private DbsField pstl0175_Pay_Freq_Fill;
    private DbsField pstl0175_Adp_Settl_Pct_Dol_Swtch;
    private DbsField pstl0175_Adp_Settl_All_Tiaa_Grd_Pct;
    private DbsField pstl0175_Adp_Settl_All_Tiaa_Std_Pct;
    private DbsField pstl0175_Adp_Settl_All_Tiaa_Grd_Amt;
    private DbsField pstl0175_Adp_Settl_All_Tiaa_Grd_Rem;
    private DbsField pstl0175_Adp_Settl_All_Tiaa_Grd_Flg;
    private DbsGroup pstl0175_Beneficiary;
    private DbsField pstl0175_Designation_Text;
    private DbsGroup pstl0175_Designation_TextRedef3;
    private DbsField pstl0175_Designation_Txt;
    private DbsField pstl0175_Designation_Txt_1;
    private DbsField pstl0175_Designation_Text_Cont;
    private DbsGroup pstl0175_Primary;
    private DbsField pstl0175_Bene_Name;
    private DbsField pstl0175_Allocation_Pct;
    private DbsGroup pstl0175_Contingent;
    private DbsField pstl0175_Cont_Name;
    private DbsField pstl0175_Cont_Alloc_Pct;
    private DbsGroup pstl0175_Payment_Destinations;
    private DbsField pstl0175_Adp_Alt_Dest_Nme;
    private DbsField pstl0175_Adp_Alt_Dest_Addr_Txt;
    private DbsField pstl0175_Adp_Alt_Dest_Addr_Zip;
    private DbsField pstl0175_Adp_Alt_Dest_Acct_Nbr;
    private DbsField pstl0175_Adp_Alt_Dest_Trnst_Cde;
    private DbsField pstl0175_Adp_Alt_Dest_Acct_Typ;
    private DbsGroup pstl0175_Rtb_Alternate_Dest;
    private DbsField pstl0175_Adp_Rtb_Alt_Dest_Rlvr_Ind;
    private DbsField pstl0175_Adp_Rtb_Alt_Dest_Rmndr;
    private DbsField pstl0175_Adp_Rtb_Alt_Dest_Carr_Nme;
    private DbsField pstl0175_Adp_Rtb_Alt_Dest_Addr;
    private DbsField pstl0175_Adp_Rtb_Alt_Dest_Zip;
    private DbsField pstl0175_Adp_Rtb_Alt_Dest_Acct_Nbr;
    private DbsField pstl0175_Adp_Rtb_Alt_Dest_Pct;
    private DbsField pstl0175_Adp_Rtb_Alt_Dest_Amt;
    private DbsField pstl0175_Adp_Rtb_Alt_Dest_Acct_Typ;
    private DbsField pstl0175_Adp_Rtb_Alt_Dest_Trnst_Cde;
    private DbsField pstl0175_Adp_Rtb_Alt_Dest_Ira_Typ;
    private DbsField pstl0175_Adp_Rtb_Alt_Dest_Ppcn;
    private DbsField pstl0175_Adp_Rtb_Alt_Dest_Ppcc;
    private DbsGroup pstl0175_Contract;
    private DbsField pstl0175_Adc_Da_Tiaa_Nbr;
    private DbsField pstl0175_Adc_Tiaa_Acct_Nme;
    private DbsField pstl0175_Adc_Tiaa_Acct_Typ;
    private DbsField pstl0175_Adc_Tiaa_Acct_Qty;
    private DbsField pstl0175_Adc_Tiaa_Acct_Payout;
    private DbsField pstl0175_Adc_Tiaa_Rtb_Acct_Typ;
    private DbsField pstl0175_Adc_Tiaa_Rtb_Acct_Qty;
    private DbsField pstl0175_Adc_Tiaa_Rtb_Nbr;
    private DbsField pstl0175_Adc_Tiaa_Rtb_Acct_Nme;
    private DbsField pstl0175_Adc_Da_Cref_Nbr;
    private DbsField pstl0175_Adc_Cref_Acct_Nme;
    private DbsField pstl0175_Adc_Cref_Acct_Typ;
    private DbsField pstl0175_Adc_Cref_Acct_Qty;
    private DbsField pstl0175_Adc_Cref_Grd_Mnthly_Typ;
    private DbsField pstl0175_Adc_Cref_Grd_Mnthly_Qty;
    private DbsField pstl0175_Adc_Cref_Acct_Rem_Qty;
    private DbsField pstl0175_Adc_Cref_Acct_Payout;
    private DbsField pstl0175_Adc_Cref_Rtb_Acct_Typ;
    private DbsField pstl0175_Adc_Cref_Rtb_Acct_Qty;
    private DbsField pstl0175_Adc_Cref_Rtb_Nbr;
    private DbsField pstl0175_Adc_Cref_Rtb_Acct_Nme;
    private DbsField pstl0175_Adp_Rtb_Pymt_Ind;
    private DbsField pstl0175_Adp_Rollvr_Ind;
    private DbsGroup pstl0175_Tax;
    private DbsField pstl0175_Ads_Nra_Flag;
    private DbsField pstl0175_Ads_Nra_Msg_Lns1;
    private DbsField pstl0175_Ads_T_F_Lump_Msg_Lns;
    private DbsField pstl0175_Ads_T_S_Lump_Msg_Lns;
    private DbsField pstl0175_Ads_T_L_Lump_Msg_Lns;
    private DbsField pstl0175_Ads_T_F_Pp_Msg_Lns;
    private DbsField pstl0175_Ads_T_S_Pp_Msg_Lns;
    private DbsField pstl0175_Ads_T_L_Pp_Msg_Lns;
    private DbsField pstl0175_Ads_C_F_Lump_Msg_Lns;
    private DbsField pstl0175_Ads_C_S_Lump_Msg_Lns;
    private DbsField pstl0175_Ads_C_L_Lump_Msg_Lns;
    private DbsField pstl0175_Ads_C_F_Pp_Msg_Lns;
    private DbsField pstl0175_Ads_C_S_Pp_Msg_Lns;
    private DbsField pstl0175_Ads_C_L_Pp_Msg_Lns;
    private DbsGroup pstl0175_Annuity_Info;
    private DbsField pstl0175_Ann_Part_Name;
    private DbsField pstl0175_Ann_Part_Dob;
    private DbsField pstl0175_Ann_Part_Ssn;
    private DbsField pstl0175_Plan_Name;
    private DbsField pstl0175_Stable_Return_Ind;
    private DbsGroup pstl0175_Pi_Data;
    private DbsField pstl0175_Pi_Data_Pstl0175_Pi_Rec_Id;
    private DbsField pstl0175_Pi_Data_Pstl0175_Pi_Data_Lgth;
    private DbsField pstl0175_Pi_Data_Pstl0175_Pi_Data_Occurs;
    private DbsField pstl0175_Pi_Data_Pstl0175_Pi_Data_Array;
    private DbsGroup pstl0175_Pi_Data_Pstl0175_Pi_Data_ArrayRedef4;
    private DbsGroup pstl0175_Pi_Data_Power_Image_Task_Information;
    private DbsField pstl0175_Pi_Data_Pi_Begin_Literal;
    private DbsField pstl0175_Pi_Data_Pi_Export_Ind;
    private DbsField pstl0175_Pi_Data_Pi_Task_Id;
    private DbsField pstl0175_Pi_Data_Pi_Task_Type;
    private DbsField pstl0175_Pi_Data_Pi_Task_Guid;
    private DbsField pstl0175_Pi_Data_Pi_Action_Step;
    private DbsField pstl0175_Pi_Data_Pi_Tiaa_Full_Date;
    private DbsGroup pstl0175_Pi_Data_Pi_Tiaa_Full_DateRedef5;
    private DbsField pstl0175_Pi_Data_Pi_Tiaa_Date_Century;
    private DbsField pstl0175_Pi_Data_Pi_Tiaa_Date_Year;
    private DbsField pstl0175_Pi_Data_Pi_Tiaa_Date_Month;
    private DbsField pstl0175_Pi_Data_Pi_Tiaa_Date_Day;
    private DbsField pstl0175_Pi_Data_Pi_Tiaa_Time;
    private DbsField pstl0175_Pi_Data_Pi_Task_Status;
    private DbsField pstl0175_Pi_Data_Pi_Ssn;
    private DbsField pstl0175_Pi_Data_Pi_Pin_Npin_Ppg;
    private DbsField pstl0175_Pi_Data_Pi_Pin_Type;
    private DbsField pstl0175_Pi_Data_Pi_Contract;
    private DbsGroup pstl0175_Pi_Data_Pi_ContractRedef6;
    private DbsField pstl0175_Pi_Data_Pi_Contract_A;
    private DbsField pstl0175_Pi_Data_Pi_Plan_Id;
    private DbsField pstl0175_Pi_Data_Pi_Doc_Content;
    private DbsField pstl0175_Pi_Data_Pi_Filler_1;
    private DbsField pstl0175_Pi_Data_Pi_Filler_2;
    private DbsField pstl0175_Pi_Data_Pi_Filler_3;
    private DbsField pstl0175_Pi_Data_Pi_Filler_4;
    private DbsField pstl0175_Pi_Data_Pi_Filler_5;
    private DbsField pstl0175_Pi_Data_Pi_End_Literal;

    public DbsGroup getPstl0175() { return pstl0175; }

    public DbsField getPstl0175_Pstl0175_Rec_Id() { return pstl0175_Pstl0175_Rec_Id; }

    public DbsField getPstl0175_Pstl0175_Data_Lgth() { return pstl0175_Pstl0175_Data_Lgth; }

    public DbsField getPstl0175_Pstl0175_Data_Occurs() { return pstl0175_Pstl0175_Data_Occurs; }

    public DbsField getPstl0175_Pstl0175_First_Record() { return pstl0175_Pstl0175_First_Record; }

    public DbsField getPstl0175_Pstl0175_Data_Array() { return pstl0175_Pstl0175_Data_Array; }

    public DbsGroup getPstl0175_Pstl0175_Data_ArrayRedef1() { return pstl0175_Pstl0175_Data_ArrayRedef1; }

    public DbsGroup getPstl0175_Participant_Info() { return pstl0175_Participant_Info; }

    public DbsField getPstl0175_Participant_Name() { return pstl0175_Participant_Name; }

    public DbsField getPstl0175_Adp_Frst_Annt_Ssn() { return pstl0175_Adp_Frst_Annt_Ssn; }

    public DbsField getPstl0175_Adp_Frst_Annt_Dte_Of_Brth_N() { return pstl0175_Adp_Frst_Annt_Dte_Of_Brth_N; }

    public DbsField getPstl0175_Adp_Crrspndnce_Perm_Addr_Txt() { return pstl0175_Adp_Crrspndnce_Perm_Addr_Txt; }

    public DbsField getPstl0175_Adp_Crrspndnce_Perm_Addr_Zip() { return pstl0175_Adp_Crrspndnce_Perm_Addr_Zip; }

    public DbsField getPstl0175_Adp_Frst_Annt_Rsdnc_Cde() { return pstl0175_Adp_Frst_Annt_Rsdnc_Cde; }

    public DbsField getPstl0175_Adp_Frst_Annt_Ctznshp() { return pstl0175_Adp_Frst_Annt_Ctznshp; }

    public DbsGroup getPstl0175_Settlement_Ia() { return pstl0175_Settlement_Ia; }

    public DbsField getPstl0175_Adp_Annty_Optn() { return pstl0175_Adp_Annty_Optn; }

    public DbsField getPstl0175_Adp_Grntee_Period() { return pstl0175_Adp_Grntee_Period; }

    public DbsField getPstl0175_Adp_Annty_Strt_Dte_N() { return pstl0175_Adp_Annty_Strt_Dte_N; }

    public DbsField getPstl0175_Adp_Pymnt_Mode() { return pstl0175_Adp_Pymnt_Mode; }

    public DbsGroup getPstl0175_Adp_Pymnt_ModeRedef2() { return pstl0175_Adp_Pymnt_ModeRedef2; }

    public DbsField getPstl0175_Pay_Freq() { return pstl0175_Pay_Freq; }

    public DbsField getPstl0175_Pay_Freq_Fill() { return pstl0175_Pay_Freq_Fill; }

    public DbsField getPstl0175_Adp_Settl_Pct_Dol_Swtch() { return pstl0175_Adp_Settl_Pct_Dol_Swtch; }

    public DbsField getPstl0175_Adp_Settl_All_Tiaa_Grd_Pct() { return pstl0175_Adp_Settl_All_Tiaa_Grd_Pct; }

    public DbsField getPstl0175_Adp_Settl_All_Tiaa_Std_Pct() { return pstl0175_Adp_Settl_All_Tiaa_Std_Pct; }

    public DbsField getPstl0175_Adp_Settl_All_Tiaa_Grd_Amt() { return pstl0175_Adp_Settl_All_Tiaa_Grd_Amt; }

    public DbsField getPstl0175_Adp_Settl_All_Tiaa_Grd_Rem() { return pstl0175_Adp_Settl_All_Tiaa_Grd_Rem; }

    public DbsField getPstl0175_Adp_Settl_All_Tiaa_Grd_Flg() { return pstl0175_Adp_Settl_All_Tiaa_Grd_Flg; }

    public DbsGroup getPstl0175_Beneficiary() { return pstl0175_Beneficiary; }

    public DbsField getPstl0175_Designation_Text() { return pstl0175_Designation_Text; }

    public DbsGroup getPstl0175_Designation_TextRedef3() { return pstl0175_Designation_TextRedef3; }

    public DbsField getPstl0175_Designation_Txt() { return pstl0175_Designation_Txt; }

    public DbsField getPstl0175_Designation_Txt_1() { return pstl0175_Designation_Txt_1; }

    public DbsField getPstl0175_Designation_Text_Cont() { return pstl0175_Designation_Text_Cont; }

    public DbsGroup getPstl0175_Primary() { return pstl0175_Primary; }

    public DbsField getPstl0175_Bene_Name() { return pstl0175_Bene_Name; }

    public DbsField getPstl0175_Allocation_Pct() { return pstl0175_Allocation_Pct; }

    public DbsGroup getPstl0175_Contingent() { return pstl0175_Contingent; }

    public DbsField getPstl0175_Cont_Name() { return pstl0175_Cont_Name; }

    public DbsField getPstl0175_Cont_Alloc_Pct() { return pstl0175_Cont_Alloc_Pct; }

    public DbsGroup getPstl0175_Payment_Destinations() { return pstl0175_Payment_Destinations; }

    public DbsField getPstl0175_Adp_Alt_Dest_Nme() { return pstl0175_Adp_Alt_Dest_Nme; }

    public DbsField getPstl0175_Adp_Alt_Dest_Addr_Txt() { return pstl0175_Adp_Alt_Dest_Addr_Txt; }

    public DbsField getPstl0175_Adp_Alt_Dest_Addr_Zip() { return pstl0175_Adp_Alt_Dest_Addr_Zip; }

    public DbsField getPstl0175_Adp_Alt_Dest_Acct_Nbr() { return pstl0175_Adp_Alt_Dest_Acct_Nbr; }

    public DbsField getPstl0175_Adp_Alt_Dest_Trnst_Cde() { return pstl0175_Adp_Alt_Dest_Trnst_Cde; }

    public DbsField getPstl0175_Adp_Alt_Dest_Acct_Typ() { return pstl0175_Adp_Alt_Dest_Acct_Typ; }

    public DbsGroup getPstl0175_Rtb_Alternate_Dest() { return pstl0175_Rtb_Alternate_Dest; }

    public DbsField getPstl0175_Adp_Rtb_Alt_Dest_Rlvr_Ind() { return pstl0175_Adp_Rtb_Alt_Dest_Rlvr_Ind; }

    public DbsField getPstl0175_Adp_Rtb_Alt_Dest_Rmndr() { return pstl0175_Adp_Rtb_Alt_Dest_Rmndr; }

    public DbsField getPstl0175_Adp_Rtb_Alt_Dest_Carr_Nme() { return pstl0175_Adp_Rtb_Alt_Dest_Carr_Nme; }

    public DbsField getPstl0175_Adp_Rtb_Alt_Dest_Addr() { return pstl0175_Adp_Rtb_Alt_Dest_Addr; }

    public DbsField getPstl0175_Adp_Rtb_Alt_Dest_Zip() { return pstl0175_Adp_Rtb_Alt_Dest_Zip; }

    public DbsField getPstl0175_Adp_Rtb_Alt_Dest_Acct_Nbr() { return pstl0175_Adp_Rtb_Alt_Dest_Acct_Nbr; }

    public DbsField getPstl0175_Adp_Rtb_Alt_Dest_Pct() { return pstl0175_Adp_Rtb_Alt_Dest_Pct; }

    public DbsField getPstl0175_Adp_Rtb_Alt_Dest_Amt() { return pstl0175_Adp_Rtb_Alt_Dest_Amt; }

    public DbsField getPstl0175_Adp_Rtb_Alt_Dest_Acct_Typ() { return pstl0175_Adp_Rtb_Alt_Dest_Acct_Typ; }

    public DbsField getPstl0175_Adp_Rtb_Alt_Dest_Trnst_Cde() { return pstl0175_Adp_Rtb_Alt_Dest_Trnst_Cde; }

    public DbsField getPstl0175_Adp_Rtb_Alt_Dest_Ira_Typ() { return pstl0175_Adp_Rtb_Alt_Dest_Ira_Typ; }

    public DbsField getPstl0175_Adp_Rtb_Alt_Dest_Ppcn() { return pstl0175_Adp_Rtb_Alt_Dest_Ppcn; }

    public DbsField getPstl0175_Adp_Rtb_Alt_Dest_Ppcc() { return pstl0175_Adp_Rtb_Alt_Dest_Ppcc; }

    public DbsGroup getPstl0175_Contract() { return pstl0175_Contract; }

    public DbsField getPstl0175_Adc_Da_Tiaa_Nbr() { return pstl0175_Adc_Da_Tiaa_Nbr; }

    public DbsField getPstl0175_Adc_Tiaa_Acct_Nme() { return pstl0175_Adc_Tiaa_Acct_Nme; }

    public DbsField getPstl0175_Adc_Tiaa_Acct_Typ() { return pstl0175_Adc_Tiaa_Acct_Typ; }

    public DbsField getPstl0175_Adc_Tiaa_Acct_Qty() { return pstl0175_Adc_Tiaa_Acct_Qty; }

    public DbsField getPstl0175_Adc_Tiaa_Acct_Payout() { return pstl0175_Adc_Tiaa_Acct_Payout; }

    public DbsField getPstl0175_Adc_Tiaa_Rtb_Acct_Typ() { return pstl0175_Adc_Tiaa_Rtb_Acct_Typ; }

    public DbsField getPstl0175_Adc_Tiaa_Rtb_Acct_Qty() { return pstl0175_Adc_Tiaa_Rtb_Acct_Qty; }

    public DbsField getPstl0175_Adc_Tiaa_Rtb_Nbr() { return pstl0175_Adc_Tiaa_Rtb_Nbr; }

    public DbsField getPstl0175_Adc_Tiaa_Rtb_Acct_Nme() { return pstl0175_Adc_Tiaa_Rtb_Acct_Nme; }

    public DbsField getPstl0175_Adc_Da_Cref_Nbr() { return pstl0175_Adc_Da_Cref_Nbr; }

    public DbsField getPstl0175_Adc_Cref_Acct_Nme() { return pstl0175_Adc_Cref_Acct_Nme; }

    public DbsField getPstl0175_Adc_Cref_Acct_Typ() { return pstl0175_Adc_Cref_Acct_Typ; }

    public DbsField getPstl0175_Adc_Cref_Acct_Qty() { return pstl0175_Adc_Cref_Acct_Qty; }

    public DbsField getPstl0175_Adc_Cref_Grd_Mnthly_Typ() { return pstl0175_Adc_Cref_Grd_Mnthly_Typ; }

    public DbsField getPstl0175_Adc_Cref_Grd_Mnthly_Qty() { return pstl0175_Adc_Cref_Grd_Mnthly_Qty; }

    public DbsField getPstl0175_Adc_Cref_Acct_Rem_Qty() { return pstl0175_Adc_Cref_Acct_Rem_Qty; }

    public DbsField getPstl0175_Adc_Cref_Acct_Payout() { return pstl0175_Adc_Cref_Acct_Payout; }

    public DbsField getPstl0175_Adc_Cref_Rtb_Acct_Typ() { return pstl0175_Adc_Cref_Rtb_Acct_Typ; }

    public DbsField getPstl0175_Adc_Cref_Rtb_Acct_Qty() { return pstl0175_Adc_Cref_Rtb_Acct_Qty; }

    public DbsField getPstl0175_Adc_Cref_Rtb_Nbr() { return pstl0175_Adc_Cref_Rtb_Nbr; }

    public DbsField getPstl0175_Adc_Cref_Rtb_Acct_Nme() { return pstl0175_Adc_Cref_Rtb_Acct_Nme; }

    public DbsField getPstl0175_Adp_Rtb_Pymt_Ind() { return pstl0175_Adp_Rtb_Pymt_Ind; }

    public DbsField getPstl0175_Adp_Rollvr_Ind() { return pstl0175_Adp_Rollvr_Ind; }

    public DbsGroup getPstl0175_Tax() { return pstl0175_Tax; }

    public DbsField getPstl0175_Ads_Nra_Flag() { return pstl0175_Ads_Nra_Flag; }

    public DbsField getPstl0175_Ads_Nra_Msg_Lns1() { return pstl0175_Ads_Nra_Msg_Lns1; }

    public DbsField getPstl0175_Ads_T_F_Lump_Msg_Lns() { return pstl0175_Ads_T_F_Lump_Msg_Lns; }

    public DbsField getPstl0175_Ads_T_S_Lump_Msg_Lns() { return pstl0175_Ads_T_S_Lump_Msg_Lns; }

    public DbsField getPstl0175_Ads_T_L_Lump_Msg_Lns() { return pstl0175_Ads_T_L_Lump_Msg_Lns; }

    public DbsField getPstl0175_Ads_T_F_Pp_Msg_Lns() { return pstl0175_Ads_T_F_Pp_Msg_Lns; }

    public DbsField getPstl0175_Ads_T_S_Pp_Msg_Lns() { return pstl0175_Ads_T_S_Pp_Msg_Lns; }

    public DbsField getPstl0175_Ads_T_L_Pp_Msg_Lns() { return pstl0175_Ads_T_L_Pp_Msg_Lns; }

    public DbsField getPstl0175_Ads_C_F_Lump_Msg_Lns() { return pstl0175_Ads_C_F_Lump_Msg_Lns; }

    public DbsField getPstl0175_Ads_C_S_Lump_Msg_Lns() { return pstl0175_Ads_C_S_Lump_Msg_Lns; }

    public DbsField getPstl0175_Ads_C_L_Lump_Msg_Lns() { return pstl0175_Ads_C_L_Lump_Msg_Lns; }

    public DbsField getPstl0175_Ads_C_F_Pp_Msg_Lns() { return pstl0175_Ads_C_F_Pp_Msg_Lns; }

    public DbsField getPstl0175_Ads_C_S_Pp_Msg_Lns() { return pstl0175_Ads_C_S_Pp_Msg_Lns; }

    public DbsField getPstl0175_Ads_C_L_Pp_Msg_Lns() { return pstl0175_Ads_C_L_Pp_Msg_Lns; }

    public DbsGroup getPstl0175_Annuity_Info() { return pstl0175_Annuity_Info; }

    public DbsField getPstl0175_Ann_Part_Name() { return pstl0175_Ann_Part_Name; }

    public DbsField getPstl0175_Ann_Part_Dob() { return pstl0175_Ann_Part_Dob; }

    public DbsField getPstl0175_Ann_Part_Ssn() { return pstl0175_Ann_Part_Ssn; }

    public DbsField getPstl0175_Plan_Name() { return pstl0175_Plan_Name; }

    public DbsField getPstl0175_Stable_Return_Ind() { return pstl0175_Stable_Return_Ind; }

    public DbsGroup getPstl0175_Pi_Data() { return pstl0175_Pi_Data; }

    public DbsField getPstl0175_Pi_Data_Pstl0175_Pi_Rec_Id() { return pstl0175_Pi_Data_Pstl0175_Pi_Rec_Id; }

    public DbsField getPstl0175_Pi_Data_Pstl0175_Pi_Data_Lgth() { return pstl0175_Pi_Data_Pstl0175_Pi_Data_Lgth; }

    public DbsField getPstl0175_Pi_Data_Pstl0175_Pi_Data_Occurs() { return pstl0175_Pi_Data_Pstl0175_Pi_Data_Occurs; }

    public DbsField getPstl0175_Pi_Data_Pstl0175_Pi_Data_Array() { return pstl0175_Pi_Data_Pstl0175_Pi_Data_Array; }

    public DbsGroup getPstl0175_Pi_Data_Pstl0175_Pi_Data_ArrayRedef4() { return pstl0175_Pi_Data_Pstl0175_Pi_Data_ArrayRedef4; }

    public DbsGroup getPstl0175_Pi_Data_Power_Image_Task_Information() { return pstl0175_Pi_Data_Power_Image_Task_Information; }

    public DbsField getPstl0175_Pi_Data_Pi_Begin_Literal() { return pstl0175_Pi_Data_Pi_Begin_Literal; }

    public DbsField getPstl0175_Pi_Data_Pi_Export_Ind() { return pstl0175_Pi_Data_Pi_Export_Ind; }

    public DbsField getPstl0175_Pi_Data_Pi_Task_Id() { return pstl0175_Pi_Data_Pi_Task_Id; }

    public DbsField getPstl0175_Pi_Data_Pi_Task_Type() { return pstl0175_Pi_Data_Pi_Task_Type; }

    public DbsField getPstl0175_Pi_Data_Pi_Task_Guid() { return pstl0175_Pi_Data_Pi_Task_Guid; }

    public DbsField getPstl0175_Pi_Data_Pi_Action_Step() { return pstl0175_Pi_Data_Pi_Action_Step; }

    public DbsField getPstl0175_Pi_Data_Pi_Tiaa_Full_Date() { return pstl0175_Pi_Data_Pi_Tiaa_Full_Date; }

    public DbsGroup getPstl0175_Pi_Data_Pi_Tiaa_Full_DateRedef5() { return pstl0175_Pi_Data_Pi_Tiaa_Full_DateRedef5; }

    public DbsField getPstl0175_Pi_Data_Pi_Tiaa_Date_Century() { return pstl0175_Pi_Data_Pi_Tiaa_Date_Century; }

    public DbsField getPstl0175_Pi_Data_Pi_Tiaa_Date_Year() { return pstl0175_Pi_Data_Pi_Tiaa_Date_Year; }

    public DbsField getPstl0175_Pi_Data_Pi_Tiaa_Date_Month() { return pstl0175_Pi_Data_Pi_Tiaa_Date_Month; }

    public DbsField getPstl0175_Pi_Data_Pi_Tiaa_Date_Day() { return pstl0175_Pi_Data_Pi_Tiaa_Date_Day; }

    public DbsField getPstl0175_Pi_Data_Pi_Tiaa_Time() { return pstl0175_Pi_Data_Pi_Tiaa_Time; }

    public DbsField getPstl0175_Pi_Data_Pi_Task_Status() { return pstl0175_Pi_Data_Pi_Task_Status; }

    public DbsField getPstl0175_Pi_Data_Pi_Ssn() { return pstl0175_Pi_Data_Pi_Ssn; }

    public DbsField getPstl0175_Pi_Data_Pi_Pin_Npin_Ppg() { return pstl0175_Pi_Data_Pi_Pin_Npin_Ppg; }

    public DbsField getPstl0175_Pi_Data_Pi_Pin_Type() { return pstl0175_Pi_Data_Pi_Pin_Type; }

    public DbsField getPstl0175_Pi_Data_Pi_Contract() { return pstl0175_Pi_Data_Pi_Contract; }

    public DbsGroup getPstl0175_Pi_Data_Pi_ContractRedef6() { return pstl0175_Pi_Data_Pi_ContractRedef6; }

    public DbsField getPstl0175_Pi_Data_Pi_Contract_A() { return pstl0175_Pi_Data_Pi_Contract_A; }

    public DbsField getPstl0175_Pi_Data_Pi_Plan_Id() { return pstl0175_Pi_Data_Pi_Plan_Id; }

    public DbsField getPstl0175_Pi_Data_Pi_Doc_Content() { return pstl0175_Pi_Data_Pi_Doc_Content; }

    public DbsField getPstl0175_Pi_Data_Pi_Filler_1() { return pstl0175_Pi_Data_Pi_Filler_1; }

    public DbsField getPstl0175_Pi_Data_Pi_Filler_2() { return pstl0175_Pi_Data_Pi_Filler_2; }

    public DbsField getPstl0175_Pi_Data_Pi_Filler_3() { return pstl0175_Pi_Data_Pi_Filler_3; }

    public DbsField getPstl0175_Pi_Data_Pi_Filler_4() { return pstl0175_Pi_Data_Pi_Filler_4; }

    public DbsField getPstl0175_Pi_Data_Pi_Filler_5() { return pstl0175_Pi_Data_Pi_Filler_5; }

    public DbsField getPstl0175_Pi_Data_Pi_End_Literal() { return pstl0175_Pi_Data_Pi_End_Literal; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pstl0175 = newGroupInRecord("pstl0175", "PSTL0175");
        pstl0175_Pstl0175_Rec_Id = pstl0175.newFieldInGroup("pstl0175_Pstl0175_Rec_Id", "PSTL0175-REC-ID", FieldType.STRING, 2);
        pstl0175_Pstl0175_Data_Lgth = pstl0175.newFieldInGroup("pstl0175_Pstl0175_Data_Lgth", "PSTL0175-DATA-LGTH", FieldType.NUMERIC, 5);
        pstl0175_Pstl0175_Data_Occurs = pstl0175.newFieldInGroup("pstl0175_Pstl0175_Data_Occurs", "PSTL0175-DATA-OCCURS", FieldType.NUMERIC, 5);
        pstl0175_Pstl0175_First_Record = pstl0175.newFieldInGroup("pstl0175_Pstl0175_First_Record", "PSTL0175-FIRST-RECORD", FieldType.BOOLEAN);
        pstl0175_Pstl0175_Data_Array = pstl0175.newFieldArrayInGroup("pstl0175_Pstl0175_Data_Array", "PSTL0175-DATA-ARRAY", FieldType.STRING, 1, new DbsArrayController(1,
            31216));
        pstl0175_Pstl0175_Data_ArrayRedef1 = pstl0175.newGroupInGroup("pstl0175_Pstl0175_Data_ArrayRedef1", "Redefines", pstl0175_Pstl0175_Data_Array);
        pstl0175_Participant_Info = pstl0175_Pstl0175_Data_ArrayRedef1.newGroupInGroup("pstl0175_Participant_Info", "PARTICIPANT-INFO");
        pstl0175_Participant_Name = pstl0175_Participant_Info.newFieldInGroup("pstl0175_Participant_Name", "PARTICIPANT-NAME", FieldType.STRING, 35);
        pstl0175_Adp_Frst_Annt_Ssn = pstl0175_Participant_Info.newFieldInGroup("pstl0175_Adp_Frst_Annt_Ssn", "ADP-FRST-ANNT-SSN", FieldType.NUMERIC, 9);
        pstl0175_Adp_Frst_Annt_Dte_Of_Brth_N = pstl0175_Participant_Info.newFieldInGroup("pstl0175_Adp_Frst_Annt_Dte_Of_Brth_N", "ADP-FRST-ANNT-DTE-OF-BRTH-N", 
            FieldType.NUMERIC, 8);
        pstl0175_Adp_Crrspndnce_Perm_Addr_Txt = pstl0175_Participant_Info.newFieldArrayInGroup("pstl0175_Adp_Crrspndnce_Perm_Addr_Txt", "ADP-CRRSPNDNCE-PERM-ADDR-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1,5));
        pstl0175_Adp_Crrspndnce_Perm_Addr_Zip = pstl0175_Participant_Info.newFieldInGroup("pstl0175_Adp_Crrspndnce_Perm_Addr_Zip", "ADP-CRRSPNDNCE-PERM-ADDR-ZIP", 
            FieldType.STRING, 9);
        pstl0175_Adp_Frst_Annt_Rsdnc_Cde = pstl0175_Participant_Info.newFieldInGroup("pstl0175_Adp_Frst_Annt_Rsdnc_Cde", "ADP-FRST-ANNT-RSDNC-CDE", FieldType.STRING, 
            2);
        pstl0175_Adp_Frst_Annt_Ctznshp = pstl0175_Participant_Info.newFieldInGroup("pstl0175_Adp_Frst_Annt_Ctznshp", "ADP-FRST-ANNT-CTZNSHP", FieldType.STRING, 
            2);
        pstl0175_Settlement_Ia = pstl0175_Pstl0175_Data_ArrayRedef1.newGroupInGroup("pstl0175_Settlement_Ia", "SETTLEMENT-IA");
        pstl0175_Adp_Annty_Optn = pstl0175_Settlement_Ia.newFieldInGroup("pstl0175_Adp_Annty_Optn", "ADP-ANNTY-OPTN", FieldType.NUMERIC, 2);
        pstl0175_Adp_Grntee_Period = pstl0175_Settlement_Ia.newFieldInGroup("pstl0175_Adp_Grntee_Period", "ADP-GRNTEE-PERIOD", FieldType.NUMERIC, 2);
        pstl0175_Adp_Annty_Strt_Dte_N = pstl0175_Settlement_Ia.newFieldInGroup("pstl0175_Adp_Annty_Strt_Dte_N", "ADP-ANNTY-STRT-DTE-N", FieldType.NUMERIC, 
            8);
        pstl0175_Adp_Pymnt_Mode = pstl0175_Settlement_Ia.newFieldInGroup("pstl0175_Adp_Pymnt_Mode", "ADP-PYMNT-MODE", FieldType.NUMERIC, 3);
        pstl0175_Adp_Pymnt_ModeRedef2 = pstl0175_Settlement_Ia.newGroupInGroup("pstl0175_Adp_Pymnt_ModeRedef2", "Redefines", pstl0175_Adp_Pymnt_Mode);
        pstl0175_Pay_Freq = pstl0175_Adp_Pymnt_ModeRedef2.newFieldInGroup("pstl0175_Pay_Freq", "PAY-FREQ", FieldType.NUMERIC, 1);
        pstl0175_Pay_Freq_Fill = pstl0175_Adp_Pymnt_ModeRedef2.newFieldInGroup("pstl0175_Pay_Freq_Fill", "PAY-FREQ-FILL", FieldType.NUMERIC, 2);
        pstl0175_Adp_Settl_Pct_Dol_Swtch = pstl0175_Settlement_Ia.newFieldInGroup("pstl0175_Adp_Settl_Pct_Dol_Swtch", "ADP-SETTL-PCT-DOL-SWTCH", FieldType.STRING, 
            1);
        pstl0175_Adp_Settl_All_Tiaa_Grd_Pct = pstl0175_Settlement_Ia.newFieldInGroup("pstl0175_Adp_Settl_All_Tiaa_Grd_Pct", "ADP-SETTL-ALL-TIAA-GRD-PCT", 
            FieldType.NUMERIC, 3);
        pstl0175_Adp_Settl_All_Tiaa_Std_Pct = pstl0175_Settlement_Ia.newFieldInGroup("pstl0175_Adp_Settl_All_Tiaa_Std_Pct", "ADP-SETTL-ALL-TIAA-STD-PCT", 
            FieldType.NUMERIC, 3);
        pstl0175_Adp_Settl_All_Tiaa_Grd_Amt = pstl0175_Settlement_Ia.newFieldInGroup("pstl0175_Adp_Settl_All_Tiaa_Grd_Amt", "ADP-SETTL-ALL-TIAA-GRD-AMT", 
            FieldType.DECIMAL, 11,2);
        pstl0175_Adp_Settl_All_Tiaa_Grd_Rem = pstl0175_Settlement_Ia.newFieldInGroup("pstl0175_Adp_Settl_All_Tiaa_Grd_Rem", "ADP-SETTL-ALL-TIAA-GRD-REM", 
            FieldType.DECIMAL, 11,2);
        pstl0175_Adp_Settl_All_Tiaa_Grd_Flg = pstl0175_Settlement_Ia.newFieldInGroup("pstl0175_Adp_Settl_All_Tiaa_Grd_Flg", "ADP-SETTL-ALL-TIAA-GRD-FLG", 
            FieldType.STRING, 1);
        pstl0175_Beneficiary = pstl0175_Pstl0175_Data_ArrayRedef1.newGroupInGroup("pstl0175_Beneficiary", "BENEFICIARY");
        pstl0175_Designation_Text = pstl0175_Beneficiary.newFieldArrayInGroup("pstl0175_Designation_Text", "DESIGNATION-TEXT", FieldType.STRING, 72, new 
            DbsArrayController(1,60));
        pstl0175_Designation_TextRedef3 = pstl0175_Beneficiary.newGroupInGroup("pstl0175_Designation_TextRedef3", "Redefines", pstl0175_Designation_Text);
        pstl0175_Designation_Txt = pstl0175_Designation_TextRedef3.newFieldArrayInGroup("pstl0175_Designation_Txt", "DESIGNATION-TXT", FieldType.STRING, 
            72, new DbsArrayController(1,4,1,13));
        pstl0175_Designation_Txt_1 = pstl0175_Designation_TextRedef3.newFieldArrayInGroup("pstl0175_Designation_Txt_1", "DESIGNATION-TXT-1", FieldType.STRING, 
            72, new DbsArrayController(1,8));
        pstl0175_Designation_Text_Cont = pstl0175_Beneficiary.newFieldArrayInGroup("pstl0175_Designation_Text_Cont", "DESIGNATION-TEXT-CONT", FieldType.STRING, 
            72, new DbsArrayController(1,60));
        pstl0175_Primary = pstl0175_Pstl0175_Data_ArrayRedef1.newGroupInGroup("pstl0175_Primary", "PRIMARY");
        pstl0175_Bene_Name = pstl0175_Primary.newFieldArrayInGroup("pstl0175_Bene_Name", "BENE-NAME", FieldType.STRING, 35, new DbsArrayController(1,20));
        pstl0175_Allocation_Pct = pstl0175_Primary.newFieldArrayInGroup("pstl0175_Allocation_Pct", "ALLOCATION-PCT", FieldType.STRING, 30, new DbsArrayController(1,
            20));
        pstl0175_Contingent = pstl0175_Pstl0175_Data_ArrayRedef1.newGroupInGroup("pstl0175_Contingent", "CONTINGENT");
        pstl0175_Cont_Name = pstl0175_Contingent.newFieldArrayInGroup("pstl0175_Cont_Name", "CONT-NAME", FieldType.STRING, 35, new DbsArrayController(1,
            20));
        pstl0175_Cont_Alloc_Pct = pstl0175_Contingent.newFieldArrayInGroup("pstl0175_Cont_Alloc_Pct", "CONT-ALLOC-PCT", FieldType.STRING, 30, new DbsArrayController(1,
            20));
        pstl0175_Payment_Destinations = pstl0175_Pstl0175_Data_ArrayRedef1.newGroupInGroup("pstl0175_Payment_Destinations", "PAYMENT-DESTINATIONS");
        pstl0175_Adp_Alt_Dest_Nme = pstl0175_Payment_Destinations.newFieldInGroup("pstl0175_Adp_Alt_Dest_Nme", "ADP-ALT-DEST-NME", FieldType.STRING, 35);
        pstl0175_Adp_Alt_Dest_Addr_Txt = pstl0175_Payment_Destinations.newFieldArrayInGroup("pstl0175_Adp_Alt_Dest_Addr_Txt", "ADP-ALT-DEST-ADDR-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1,5));
        pstl0175_Adp_Alt_Dest_Addr_Zip = pstl0175_Payment_Destinations.newFieldInGroup("pstl0175_Adp_Alt_Dest_Addr_Zip", "ADP-ALT-DEST-ADDR-ZIP", FieldType.STRING, 
            9);
        pstl0175_Adp_Alt_Dest_Acct_Nbr = pstl0175_Payment_Destinations.newFieldInGroup("pstl0175_Adp_Alt_Dest_Acct_Nbr", "ADP-ALT-DEST-ACCT-NBR", FieldType.STRING, 
            21);
        pstl0175_Adp_Alt_Dest_Trnst_Cde = pstl0175_Payment_Destinations.newFieldInGroup("pstl0175_Adp_Alt_Dest_Trnst_Cde", "ADP-ALT-DEST-TRNST-CDE", FieldType.STRING, 
            9);
        pstl0175_Adp_Alt_Dest_Acct_Typ = pstl0175_Payment_Destinations.newFieldInGroup("pstl0175_Adp_Alt_Dest_Acct_Typ", "ADP-ALT-DEST-ACCT-TYP", FieldType.STRING, 
            1);
        pstl0175_Rtb_Alternate_Dest = pstl0175_Pstl0175_Data_ArrayRedef1.newGroupInGroup("pstl0175_Rtb_Alternate_Dest", "RTB-ALTERNATE-DEST");
        pstl0175_Adp_Rtb_Alt_Dest_Rlvr_Ind = pstl0175_Rtb_Alternate_Dest.newFieldArrayInGroup("pstl0175_Adp_Rtb_Alt_Dest_Rlvr_Ind", "ADP-RTB-ALT-DEST-RLVR-IND", 
            FieldType.STRING, 1, new DbsArrayController(1,3));
        pstl0175_Adp_Rtb_Alt_Dest_Rmndr = pstl0175_Rtb_Alternate_Dest.newFieldArrayInGroup("pstl0175_Adp_Rtb_Alt_Dest_Rmndr", "ADP-RTB-ALT-DEST-RMNDR", 
            FieldType.STRING, 1, new DbsArrayController(1,3));
        pstl0175_Adp_Rtb_Alt_Dest_Carr_Nme = pstl0175_Rtb_Alternate_Dest.newFieldArrayInGroup("pstl0175_Adp_Rtb_Alt_Dest_Carr_Nme", "ADP-RTB-ALT-DEST-CARR-NME", 
            FieldType.STRING, 35, new DbsArrayController(1,3));
        pstl0175_Adp_Rtb_Alt_Dest_Addr = pstl0175_Rtb_Alternate_Dest.newFieldArrayInGroup("pstl0175_Adp_Rtb_Alt_Dest_Addr", "ADP-RTB-ALT-DEST-ADDR", FieldType.STRING, 
            35, new DbsArrayController(1,3,1,5));
        pstl0175_Adp_Rtb_Alt_Dest_Zip = pstl0175_Rtb_Alternate_Dest.newFieldArrayInGroup("pstl0175_Adp_Rtb_Alt_Dest_Zip", "ADP-RTB-ALT-DEST-ZIP", FieldType.STRING, 
            9, new DbsArrayController(1,3));
        pstl0175_Adp_Rtb_Alt_Dest_Acct_Nbr = pstl0175_Rtb_Alternate_Dest.newFieldArrayInGroup("pstl0175_Adp_Rtb_Alt_Dest_Acct_Nbr", "ADP-RTB-ALT-DEST-ACCT-NBR", 
            FieldType.STRING, 21, new DbsArrayController(1,3));
        pstl0175_Adp_Rtb_Alt_Dest_Pct = pstl0175_Rtb_Alternate_Dest.newFieldArrayInGroup("pstl0175_Adp_Rtb_Alt_Dest_Pct", "ADP-RTB-ALT-DEST-PCT", FieldType.NUMERIC, 
            3, new DbsArrayController(1,3));
        pstl0175_Adp_Rtb_Alt_Dest_Amt = pstl0175_Rtb_Alternate_Dest.newFieldArrayInGroup("pstl0175_Adp_Rtb_Alt_Dest_Amt", "ADP-RTB-ALT-DEST-AMT", FieldType.DECIMAL, 
            11,2, new DbsArrayController(1,3));
        pstl0175_Adp_Rtb_Alt_Dest_Acct_Typ = pstl0175_Rtb_Alternate_Dest.newFieldArrayInGroup("pstl0175_Adp_Rtb_Alt_Dest_Acct_Typ", "ADP-RTB-ALT-DEST-ACCT-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1,3));
        pstl0175_Adp_Rtb_Alt_Dest_Trnst_Cde = pstl0175_Rtb_Alternate_Dest.newFieldArrayInGroup("pstl0175_Adp_Rtb_Alt_Dest_Trnst_Cde", "ADP-RTB-ALT-DEST-TRNST-CDE", 
            FieldType.STRING, 9, new DbsArrayController(1,3));
        pstl0175_Adp_Rtb_Alt_Dest_Ira_Typ = pstl0175_Rtb_Alternate_Dest.newFieldArrayInGroup("pstl0175_Adp_Rtb_Alt_Dest_Ira_Typ", "ADP-RTB-ALT-DEST-IRA-TYP", 
            FieldType.STRING, 1, new DbsArrayController(1,3));
        pstl0175_Adp_Rtb_Alt_Dest_Ppcn = pstl0175_Rtb_Alternate_Dest.newFieldArrayInGroup("pstl0175_Adp_Rtb_Alt_Dest_Ppcn", "ADP-RTB-ALT-DEST-PPCN", FieldType.STRING, 
            10, new DbsArrayController(1,3));
        pstl0175_Adp_Rtb_Alt_Dest_Ppcc = pstl0175_Rtb_Alternate_Dest.newFieldArrayInGroup("pstl0175_Adp_Rtb_Alt_Dest_Ppcc", "ADP-RTB-ALT-DEST-PPCC", FieldType.STRING, 
            10, new DbsArrayController(1,3));
        pstl0175_Contract = pstl0175_Pstl0175_Data_ArrayRedef1.newGroupInGroup("pstl0175_Contract", "CONTRACT");
        pstl0175_Adc_Da_Tiaa_Nbr = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Da_Tiaa_Nbr", "ADC-DA-TIAA-NBR", FieldType.STRING, 10, new DbsArrayController(1,
            6));
        pstl0175_Adc_Tiaa_Acct_Nme = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Tiaa_Acct_Nme", "ADC-TIAA-ACCT-NME", FieldType.STRING, 30, new 
            DbsArrayController(1,6,1,3));
        pstl0175_Adc_Tiaa_Acct_Typ = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Tiaa_Acct_Typ", "ADC-TIAA-ACCT-TYP", FieldType.STRING, 1, new 
            DbsArrayController(1,6,1,3));
        pstl0175_Adc_Tiaa_Acct_Qty = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Tiaa_Acct_Qty", "ADC-TIAA-ACCT-QTY", FieldType.DECIMAL, 11,2, 
            new DbsArrayController(1,6,1,3));
        pstl0175_Adc_Tiaa_Acct_Payout = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Tiaa_Acct_Payout", "ADC-TIAA-ACCT-PAYOUT", FieldType.STRING, 
            12, new DbsArrayController(1,6,1,3));
        pstl0175_Adc_Tiaa_Rtb_Acct_Typ = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Tiaa_Rtb_Acct_Typ", "ADC-TIAA-RTB-ACCT-TYP", FieldType.STRING, 
            1, new DbsArrayController(1,6,1,3));
        pstl0175_Adc_Tiaa_Rtb_Acct_Qty = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Tiaa_Rtb_Acct_Qty", "ADC-TIAA-RTB-ACCT-QTY", FieldType.DECIMAL, 
            11,2, new DbsArrayController(1,6,1,3));
        pstl0175_Adc_Tiaa_Rtb_Nbr = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Tiaa_Rtb_Nbr", "ADC-TIAA-RTB-NBR", FieldType.STRING, 10, new 
            DbsArrayController(1,6));
        pstl0175_Adc_Tiaa_Rtb_Acct_Nme = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Tiaa_Rtb_Acct_Nme", "ADC-TIAA-RTB-ACCT-NME", FieldType.STRING, 
            30, new DbsArrayController(1,6,1,3));
        pstl0175_Adc_Da_Cref_Nbr = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Da_Cref_Nbr", "ADC-DA-CREF-NBR", FieldType.STRING, 10, new DbsArrayController(1,
            6));
        pstl0175_Adc_Cref_Acct_Nme = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Cref_Acct_Nme", "ADC-CREF-ACCT-NME", FieldType.STRING, 30, new 
            DbsArrayController(1,6,1,20));
        pstl0175_Adc_Cref_Acct_Typ = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Cref_Acct_Typ", "ADC-CREF-ACCT-TYP", FieldType.STRING, 1, new 
            DbsArrayController(1,6,1,20));
        pstl0175_Adc_Cref_Acct_Qty = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Cref_Acct_Qty", "ADC-CREF-ACCT-QTY", FieldType.DECIMAL, 11,2, 
            new DbsArrayController(1,6,1,20));
        pstl0175_Adc_Cref_Grd_Mnthly_Typ = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Cref_Grd_Mnthly_Typ", "ADC-CREF-GRD-MNTHLY-TYP", FieldType.STRING, 
            1, new DbsArrayController(1,6,1,20));
        pstl0175_Adc_Cref_Grd_Mnthly_Qty = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Cref_Grd_Mnthly_Qty", "ADC-CREF-GRD-MNTHLY-QTY", FieldType.DECIMAL, 
            11,2, new DbsArrayController(1,6,1,20));
        pstl0175_Adc_Cref_Acct_Rem_Qty = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Cref_Acct_Rem_Qty", "ADC-CREF-ACCT-REM-QTY", FieldType.DECIMAL, 
            11,2, new DbsArrayController(1,6,1,20));
        pstl0175_Adc_Cref_Acct_Payout = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Cref_Acct_Payout", "ADC-CREF-ACCT-PAYOUT", FieldType.STRING, 
            10, new DbsArrayController(1,6,1,20));
        pstl0175_Adc_Cref_Rtb_Acct_Typ = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Cref_Rtb_Acct_Typ", "ADC-CREF-RTB-ACCT-TYP", FieldType.STRING, 
            1, new DbsArrayController(1,6,1,20));
        pstl0175_Adc_Cref_Rtb_Acct_Qty = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Cref_Rtb_Acct_Qty", "ADC-CREF-RTB-ACCT-QTY", FieldType.DECIMAL, 
            11,2, new DbsArrayController(1,6,1,20));
        pstl0175_Adc_Cref_Rtb_Nbr = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Cref_Rtb_Nbr", "ADC-CREF-RTB-NBR", FieldType.STRING, 10, new 
            DbsArrayController(1,6));
        pstl0175_Adc_Cref_Rtb_Acct_Nme = pstl0175_Contract.newFieldArrayInGroup("pstl0175_Adc_Cref_Rtb_Acct_Nme", "ADC-CREF-RTB-ACCT-NME", FieldType.STRING, 
            30, new DbsArrayController(1,6,1,20));
        pstl0175_Adp_Rtb_Pymt_Ind = pstl0175_Pstl0175_Data_ArrayRedef1.newFieldInGroup("pstl0175_Adp_Rtb_Pymt_Ind", "ADP-RTB-PYMT-IND", FieldType.STRING, 
            1);
        pstl0175_Adp_Rollvr_Ind = pstl0175_Pstl0175_Data_ArrayRedef1.newFieldInGroup("pstl0175_Adp_Rollvr_Ind", "ADP-ROLLVR-IND", FieldType.STRING, 1);
        pstl0175_Tax = pstl0175_Pstl0175_Data_ArrayRedef1.newGroupInGroup("pstl0175_Tax", "TAX");
        pstl0175_Ads_Nra_Flag = pstl0175_Tax.newFieldInGroup("pstl0175_Ads_Nra_Flag", "ADS-NRA-FLAG", FieldType.STRING, 1);
        pstl0175_Ads_Nra_Msg_Lns1 = pstl0175_Tax.newFieldInGroup("pstl0175_Ads_Nra_Msg_Lns1", "ADS-NRA-MSG-LNS1", FieldType.STRING, 50);
        pstl0175_Ads_T_F_Lump_Msg_Lns = pstl0175_Tax.newFieldInGroup("pstl0175_Ads_T_F_Lump_Msg_Lns", "ADS-T-F-LUMP-MSG-LNS", FieldType.STRING, 200);
        pstl0175_Ads_T_S_Lump_Msg_Lns = pstl0175_Tax.newFieldInGroup("pstl0175_Ads_T_S_Lump_Msg_Lns", "ADS-T-S-LUMP-MSG-LNS", FieldType.STRING, 250);
        pstl0175_Ads_T_L_Lump_Msg_Lns = pstl0175_Tax.newFieldInGroup("pstl0175_Ads_T_L_Lump_Msg_Lns", "ADS-T-L-LUMP-MSG-LNS", FieldType.STRING, 150);
        pstl0175_Ads_T_F_Pp_Msg_Lns = pstl0175_Tax.newFieldInGroup("pstl0175_Ads_T_F_Pp_Msg_Lns", "ADS-T-F-PP-MSG-LNS", FieldType.STRING, 200);
        pstl0175_Ads_T_S_Pp_Msg_Lns = pstl0175_Tax.newFieldInGroup("pstl0175_Ads_T_S_Pp_Msg_Lns", "ADS-T-S-PP-MSG-LNS", FieldType.STRING, 250);
        pstl0175_Ads_T_L_Pp_Msg_Lns = pstl0175_Tax.newFieldInGroup("pstl0175_Ads_T_L_Pp_Msg_Lns", "ADS-T-L-PP-MSG-LNS", FieldType.STRING, 150);
        pstl0175_Ads_C_F_Lump_Msg_Lns = pstl0175_Tax.newFieldInGroup("pstl0175_Ads_C_F_Lump_Msg_Lns", "ADS-C-F-LUMP-MSG-LNS", FieldType.STRING, 200);
        pstl0175_Ads_C_S_Lump_Msg_Lns = pstl0175_Tax.newFieldInGroup("pstl0175_Ads_C_S_Lump_Msg_Lns", "ADS-C-S-LUMP-MSG-LNS", FieldType.STRING, 250);
        pstl0175_Ads_C_L_Lump_Msg_Lns = pstl0175_Tax.newFieldInGroup("pstl0175_Ads_C_L_Lump_Msg_Lns", "ADS-C-L-LUMP-MSG-LNS", FieldType.STRING, 150);
        pstl0175_Ads_C_F_Pp_Msg_Lns = pstl0175_Tax.newFieldInGroup("pstl0175_Ads_C_F_Pp_Msg_Lns", "ADS-C-F-PP-MSG-LNS", FieldType.STRING, 200);
        pstl0175_Ads_C_S_Pp_Msg_Lns = pstl0175_Tax.newFieldInGroup("pstl0175_Ads_C_S_Pp_Msg_Lns", "ADS-C-S-PP-MSG-LNS", FieldType.STRING, 250);
        pstl0175_Ads_C_L_Pp_Msg_Lns = pstl0175_Tax.newFieldInGroup("pstl0175_Ads_C_L_Pp_Msg_Lns", "ADS-C-L-PP-MSG-LNS", FieldType.STRING, 150);
        pstl0175_Annuity_Info = pstl0175_Pstl0175_Data_ArrayRedef1.newGroupInGroup("pstl0175_Annuity_Info", "ANNUITY-INFO");
        pstl0175_Ann_Part_Name = pstl0175_Annuity_Info.newFieldInGroup("pstl0175_Ann_Part_Name", "ANN-PART-NAME", FieldType.STRING, 35);
        pstl0175_Ann_Part_Dob = pstl0175_Annuity_Info.newFieldInGroup("pstl0175_Ann_Part_Dob", "ANN-PART-DOB", FieldType.NUMERIC, 8);
        pstl0175_Ann_Part_Ssn = pstl0175_Annuity_Info.newFieldInGroup("pstl0175_Ann_Part_Ssn", "ANN-PART-SSN", FieldType.NUMERIC, 9);
        pstl0175_Plan_Name = pstl0175_Annuity_Info.newFieldInGroup("pstl0175_Plan_Name", "PLAN-NAME", FieldType.STRING, 50);
        pstl0175_Stable_Return_Ind = pstl0175_Annuity_Info.newFieldInGroup("pstl0175_Stable_Return_Ind", "STABLE-RETURN-IND", FieldType.STRING, 1);

        pstl0175_Pi_Data = newGroupInRecord("pstl0175_Pi_Data", "PSTL0175-PI-DATA");
        pstl0175_Pi_Data_Pstl0175_Pi_Rec_Id = pstl0175_Pi_Data.newFieldInGroup("pstl0175_Pi_Data_Pstl0175_Pi_Rec_Id", "PSTL0175-PI-REC-ID", FieldType.STRING, 
            2);
        pstl0175_Pi_Data_Pstl0175_Pi_Data_Lgth = pstl0175_Pi_Data.newFieldInGroup("pstl0175_Pi_Data_Pstl0175_Pi_Data_Lgth", "PSTL0175-PI-DATA-LGTH", FieldType.NUMERIC, 
            5);
        pstl0175_Pi_Data_Pstl0175_Pi_Data_Occurs = pstl0175_Pi_Data.newFieldInGroup("pstl0175_Pi_Data_Pstl0175_Pi_Data_Occurs", "PSTL0175-PI-DATA-OCCURS", 
            FieldType.NUMERIC, 5);
        pstl0175_Pi_Data_Pstl0175_Pi_Data_Array = pstl0175_Pi_Data.newFieldArrayInGroup("pstl0175_Pi_Data_Pstl0175_Pi_Data_Array", "PSTL0175-PI-DATA-ARRAY", 
            FieldType.STRING, 1, new DbsArrayController(1,400));
        pstl0175_Pi_Data_Pstl0175_Pi_Data_ArrayRedef4 = pstl0175_Pi_Data.newGroupInGroup("pstl0175_Pi_Data_Pstl0175_Pi_Data_ArrayRedef4", "Redefines", 
            pstl0175_Pi_Data_Pstl0175_Pi_Data_Array);
        pstl0175_Pi_Data_Power_Image_Task_Information = pstl0175_Pi_Data_Pstl0175_Pi_Data_ArrayRedef4.newGroupInGroup("pstl0175_Pi_Data_Power_Image_Task_Information", 
            "POWER-IMAGE-TASK-INFORMATION");
        pstl0175_Pi_Data_Pi_Begin_Literal = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_Begin_Literal", "PI-BEGIN-LITERAL", 
            FieldType.STRING, 4);
        pstl0175_Pi_Data_Pi_Export_Ind = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_Export_Ind", "PI-EXPORT-IND", 
            FieldType.STRING, 1);
        pstl0175_Pi_Data_Pi_Task_Id = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_Task_Id", "PI-TASK-ID", FieldType.STRING, 
            11);
        pstl0175_Pi_Data_Pi_Task_Type = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_Task_Type", "PI-TASK-TYPE", 
            FieldType.STRING, 10);
        pstl0175_Pi_Data_Pi_Task_Guid = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_Task_Guid", "PI-TASK-GUID", 
            FieldType.STRING, 47);
        pstl0175_Pi_Data_Pi_Action_Step = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_Action_Step", "PI-ACTION-STEP", 
            FieldType.STRING, 8);
        pstl0175_Pi_Data_Pi_Tiaa_Full_Date = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_Tiaa_Full_Date", "PI-TIAA-FULL-DATE", 
            FieldType.STRING, 8);
        pstl0175_Pi_Data_Pi_Tiaa_Full_DateRedef5 = pstl0175_Pi_Data_Power_Image_Task_Information.newGroupInGroup("pstl0175_Pi_Data_Pi_Tiaa_Full_DateRedef5", 
            "Redefines", pstl0175_Pi_Data_Pi_Tiaa_Full_Date);
        pstl0175_Pi_Data_Pi_Tiaa_Date_Century = pstl0175_Pi_Data_Pi_Tiaa_Full_DateRedef5.newFieldInGroup("pstl0175_Pi_Data_Pi_Tiaa_Date_Century", "PI-TIAA-DATE-CENTURY", 
            FieldType.NUMERIC, 2);
        pstl0175_Pi_Data_Pi_Tiaa_Date_Year = pstl0175_Pi_Data_Pi_Tiaa_Full_DateRedef5.newFieldInGroup("pstl0175_Pi_Data_Pi_Tiaa_Date_Year", "PI-TIAA-DATE-YEAR", 
            FieldType.NUMERIC, 2);
        pstl0175_Pi_Data_Pi_Tiaa_Date_Month = pstl0175_Pi_Data_Pi_Tiaa_Full_DateRedef5.newFieldInGroup("pstl0175_Pi_Data_Pi_Tiaa_Date_Month", "PI-TIAA-DATE-MONTH", 
            FieldType.NUMERIC, 2);
        pstl0175_Pi_Data_Pi_Tiaa_Date_Day = pstl0175_Pi_Data_Pi_Tiaa_Full_DateRedef5.newFieldInGroup("pstl0175_Pi_Data_Pi_Tiaa_Date_Day", "PI-TIAA-DATE-DAY", 
            FieldType.NUMERIC, 2);
        pstl0175_Pi_Data_Pi_Tiaa_Time = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_Tiaa_Time", "PI-TIAA-TIME", 
            FieldType.STRING, 8);
        pstl0175_Pi_Data_Pi_Task_Status = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_Task_Status", "PI-TASK-STATUS", 
            FieldType.STRING, 1);
        pstl0175_Pi_Data_Pi_Ssn = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_Ssn", "PI-SSN", FieldType.STRING, 
            9);
        pstl0175_Pi_Data_Pi_Pin_Npin_Ppg = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_Pin_Npin_Ppg", "PI-PIN-NPIN-PPG", 
            FieldType.STRING, 7);
        pstl0175_Pi_Data_Pi_Pin_Type = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_Pin_Type", "PI-PIN-TYPE", FieldType.STRING, 
            1);
        pstl0175_Pi_Data_Pi_Contract = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_Contract", "PI-CONTRACT", FieldType.STRING, 
            100);
        pstl0175_Pi_Data_Pi_ContractRedef6 = pstl0175_Pi_Data_Power_Image_Task_Information.newGroupInGroup("pstl0175_Pi_Data_Pi_ContractRedef6", "Redefines", 
            pstl0175_Pi_Data_Pi_Contract);
        pstl0175_Pi_Data_Pi_Contract_A = pstl0175_Pi_Data_Pi_ContractRedef6.newFieldArrayInGroup("pstl0175_Pi_Data_Pi_Contract_A", "PI-CONTRACT-A", FieldType.STRING, 
            10, new DbsArrayController(1,10));
        pstl0175_Pi_Data_Pi_Plan_Id = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_Plan_Id", "PI-PLAN-ID", FieldType.STRING, 
            6);
        pstl0175_Pi_Data_Pi_Doc_Content = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_Doc_Content", "PI-DOC-CONTENT", 
            FieldType.STRING, 30);
        pstl0175_Pi_Data_Pi_Filler_1 = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_Filler_1", "PI-FILLER-1", FieldType.STRING, 
            20);
        pstl0175_Pi_Data_Pi_Filler_2 = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_Filler_2", "PI-FILLER-2", FieldType.STRING, 
            20);
        pstl0175_Pi_Data_Pi_Filler_3 = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_Filler_3", "PI-FILLER-3", FieldType.STRING, 
            20);
        pstl0175_Pi_Data_Pi_Filler_4 = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_Filler_4", "PI-FILLER-4", FieldType.STRING, 
            20);
        pstl0175_Pi_Data_Pi_Filler_5 = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_Filler_5", "PI-FILLER-5", FieldType.STRING, 
            20);
        pstl0175_Pi_Data_Pi_End_Literal = pstl0175_Pi_Data_Power_Image_Task_Information.newFieldInGroup("pstl0175_Pi_Data_Pi_End_Literal", "PI-END-LITERAL", 
            FieldType.STRING, 4);

        this.setRecordName("LdaPstl0175");
    }

    public void initializeValues() throws Exception
    {
        reset();
        pstl0175_Pstl0175_Rec_Id.setInitialValue("AA");
        pstl0175_Pstl0175_Data_Lgth.setInitialValue(31216);
        pstl0175_Pi_Data_Pstl0175_Pi_Rec_Id.setInitialValue("PI");
        pstl0175_Pi_Data_Pstl0175_Pi_Data_Lgth.setInitialValue(400);
        pstl0175_Pi_Data_Pstl0175_Pi_Data_Occurs.setInitialValue(1);
    }

    // Constructor
    public LdaPstl0175() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
