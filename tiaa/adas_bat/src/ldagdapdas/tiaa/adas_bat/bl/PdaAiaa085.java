/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:17:41 PM
**        * FROM NATURAL PDA     : AIAA085
************************************************************
**        * FILE NAME            : PdaAiaa085.java
**        * CLASS NAME           : PdaAiaa085
**        * INSTANCE NAME        : PdaAiaa085
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaAiaa085 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Aiaa085_Linkage;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Contract_Payee;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Teachers_Or_Cref;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Value_Date;
    private DbsGroup pnd_Aiaa085_Linkage_Ivc_Value_DateRedef1;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Value_Yr;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Value_Mo;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Value_Day;
    private DbsGroup pnd_Aiaa085_Linkage_Ivc_Value_DateRedef2;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Value_Yr_Mo;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Value_Dy;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Issue_Date;
    private DbsGroup pnd_Aiaa085_Linkage_Ivc_Issue_DateRedef3;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Issue_Yr;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Issue_Mo;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Final_Date;
    private DbsGroup pnd_Aiaa085_Linkage_Ivc_Final_DateRedef4;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Final_Yr;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Final_Mo;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Mode;
    private DbsGroup pnd_Aiaa085_Linkage_Ivc_ModeRedef5;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Mode_1;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Mode_2;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Number_Of_Rates;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Rate;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Rate_Guar_Payment;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Rate_Div_Payment;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Cref_Fund_Code;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Cref_Reval;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Cref_Payment;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Cref_Units;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Remaining_Amount;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Calc_Method;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Begin_Date;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Total_Payment_Amount;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Amount;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Pre_Tax_Amount;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Return_Code;
    private DbsGroup pnd_Aiaa085_Linkage_Ivc_Return_CodeRedef6;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Return_Code_Pgm;
    private DbsField pnd_Aiaa085_Linkage_Ivc_Return_Code_Nbr;

    public DbsGroup getPnd_Aiaa085_Linkage() { return pnd_Aiaa085_Linkage; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Contract_Payee() { return pnd_Aiaa085_Linkage_Ivc_Contract_Payee; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Teachers_Or_Cref() { return pnd_Aiaa085_Linkage_Ivc_Teachers_Or_Cref; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Value_Date() { return pnd_Aiaa085_Linkage_Ivc_Value_Date; }

    public DbsGroup getPnd_Aiaa085_Linkage_Ivc_Value_DateRedef1() { return pnd_Aiaa085_Linkage_Ivc_Value_DateRedef1; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Value_Yr() { return pnd_Aiaa085_Linkage_Ivc_Value_Yr; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Value_Mo() { return pnd_Aiaa085_Linkage_Ivc_Value_Mo; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Value_Day() { return pnd_Aiaa085_Linkage_Ivc_Value_Day; }

    public DbsGroup getPnd_Aiaa085_Linkage_Ivc_Value_DateRedef2() { return pnd_Aiaa085_Linkage_Ivc_Value_DateRedef2; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Value_Yr_Mo() { return pnd_Aiaa085_Linkage_Ivc_Value_Yr_Mo; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Value_Dy() { return pnd_Aiaa085_Linkage_Ivc_Value_Dy; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Issue_Date() { return pnd_Aiaa085_Linkage_Ivc_Issue_Date; }

    public DbsGroup getPnd_Aiaa085_Linkage_Ivc_Issue_DateRedef3() { return pnd_Aiaa085_Linkage_Ivc_Issue_DateRedef3; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Issue_Yr() { return pnd_Aiaa085_Linkage_Ivc_Issue_Yr; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Issue_Mo() { return pnd_Aiaa085_Linkage_Ivc_Issue_Mo; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Final_Date() { return pnd_Aiaa085_Linkage_Ivc_Final_Date; }

    public DbsGroup getPnd_Aiaa085_Linkage_Ivc_Final_DateRedef4() { return pnd_Aiaa085_Linkage_Ivc_Final_DateRedef4; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Final_Yr() { return pnd_Aiaa085_Linkage_Ivc_Final_Yr; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Final_Mo() { return pnd_Aiaa085_Linkage_Ivc_Final_Mo; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Mode() { return pnd_Aiaa085_Linkage_Ivc_Mode; }

    public DbsGroup getPnd_Aiaa085_Linkage_Ivc_ModeRedef5() { return pnd_Aiaa085_Linkage_Ivc_ModeRedef5; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Mode_1() { return pnd_Aiaa085_Linkage_Ivc_Mode_1; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Mode_2() { return pnd_Aiaa085_Linkage_Ivc_Mode_2; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Number_Of_Rates() { return pnd_Aiaa085_Linkage_Ivc_Number_Of_Rates; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Rate() { return pnd_Aiaa085_Linkage_Ivc_Rate; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Rate_Guar_Payment() { return pnd_Aiaa085_Linkage_Ivc_Rate_Guar_Payment; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Rate_Div_Payment() { return pnd_Aiaa085_Linkage_Ivc_Rate_Div_Payment; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Cref_Fund_Code() { return pnd_Aiaa085_Linkage_Ivc_Cref_Fund_Code; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Cref_Reval() { return pnd_Aiaa085_Linkage_Ivc_Cref_Reval; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Cref_Payment() { return pnd_Aiaa085_Linkage_Ivc_Cref_Payment; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Cref_Units() { return pnd_Aiaa085_Linkage_Ivc_Cref_Units; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Remaining_Amount() { return pnd_Aiaa085_Linkage_Ivc_Remaining_Amount; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Calc_Method() { return pnd_Aiaa085_Linkage_Ivc_Calc_Method; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Begin_Date() { return pnd_Aiaa085_Linkage_Ivc_Begin_Date; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Total_Payment_Amount() { return pnd_Aiaa085_Linkage_Ivc_Total_Payment_Amount; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Amount() { return pnd_Aiaa085_Linkage_Ivc_Amount; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Pre_Tax_Amount() { return pnd_Aiaa085_Linkage_Ivc_Pre_Tax_Amount; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Return_Code() { return pnd_Aiaa085_Linkage_Ivc_Return_Code; }

    public DbsGroup getPnd_Aiaa085_Linkage_Ivc_Return_CodeRedef6() { return pnd_Aiaa085_Linkage_Ivc_Return_CodeRedef6; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Return_Code_Pgm() { return pnd_Aiaa085_Linkage_Ivc_Return_Code_Pgm; }

    public DbsField getPnd_Aiaa085_Linkage_Ivc_Return_Code_Nbr() { return pnd_Aiaa085_Linkage_Ivc_Return_Code_Nbr; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Aiaa085_Linkage = dbsRecord.newGroupInRecord("pnd_Aiaa085_Linkage", "#AIAA085-LINKAGE");
        pnd_Aiaa085_Linkage.setParameterOption(ParameterOption.ByReference);
        pnd_Aiaa085_Linkage_Ivc_Contract_Payee = pnd_Aiaa085_Linkage.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Contract_Payee", "IVC-CONTRACT-PAYEE", FieldType.STRING, 
            10);
        pnd_Aiaa085_Linkage_Ivc_Teachers_Or_Cref = pnd_Aiaa085_Linkage.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Teachers_Or_Cref", "IVC-TEACHERS-OR-CREF", 
            FieldType.STRING, 1);
        pnd_Aiaa085_Linkage_Ivc_Value_Date = pnd_Aiaa085_Linkage.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Value_Date", "IVC-VALUE-DATE", FieldType.NUMERIC, 
            8);
        pnd_Aiaa085_Linkage_Ivc_Value_DateRedef1 = pnd_Aiaa085_Linkage.newGroupInGroup("pnd_Aiaa085_Linkage_Ivc_Value_DateRedef1", "Redefines", pnd_Aiaa085_Linkage_Ivc_Value_Date);
        pnd_Aiaa085_Linkage_Ivc_Value_Yr = pnd_Aiaa085_Linkage_Ivc_Value_DateRedef1.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Value_Yr", "IVC-VALUE-YR", 
            FieldType.NUMERIC, 4);
        pnd_Aiaa085_Linkage_Ivc_Value_Mo = pnd_Aiaa085_Linkage_Ivc_Value_DateRedef1.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Value_Mo", "IVC-VALUE-MO", 
            FieldType.NUMERIC, 2);
        pnd_Aiaa085_Linkage_Ivc_Value_Day = pnd_Aiaa085_Linkage_Ivc_Value_DateRedef1.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Value_Day", "IVC-VALUE-DAY", 
            FieldType.NUMERIC, 2);
        pnd_Aiaa085_Linkage_Ivc_Value_DateRedef2 = pnd_Aiaa085_Linkage.newGroupInGroup("pnd_Aiaa085_Linkage_Ivc_Value_DateRedef2", "Redefines", pnd_Aiaa085_Linkage_Ivc_Value_Date);
        pnd_Aiaa085_Linkage_Ivc_Value_Yr_Mo = pnd_Aiaa085_Linkage_Ivc_Value_DateRedef2.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Value_Yr_Mo", "IVC-VALUE-YR-MO", 
            FieldType.NUMERIC, 6);
        pnd_Aiaa085_Linkage_Ivc_Value_Dy = pnd_Aiaa085_Linkage_Ivc_Value_DateRedef2.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Value_Dy", "IVC-VALUE-DY", 
            FieldType.NUMERIC, 2);
        pnd_Aiaa085_Linkage_Ivc_Issue_Date = pnd_Aiaa085_Linkage.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Issue_Date", "IVC-ISSUE-DATE", FieldType.NUMERIC, 
            6);
        pnd_Aiaa085_Linkage_Ivc_Issue_DateRedef3 = pnd_Aiaa085_Linkage.newGroupInGroup("pnd_Aiaa085_Linkage_Ivc_Issue_DateRedef3", "Redefines", pnd_Aiaa085_Linkage_Ivc_Issue_Date);
        pnd_Aiaa085_Linkage_Ivc_Issue_Yr = pnd_Aiaa085_Linkage_Ivc_Issue_DateRedef3.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Issue_Yr", "IVC-ISSUE-YR", 
            FieldType.NUMERIC, 4);
        pnd_Aiaa085_Linkage_Ivc_Issue_Mo = pnd_Aiaa085_Linkage_Ivc_Issue_DateRedef3.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Issue_Mo", "IVC-ISSUE-MO", 
            FieldType.NUMERIC, 2);
        pnd_Aiaa085_Linkage_Ivc_Final_Date = pnd_Aiaa085_Linkage.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Final_Date", "IVC-FINAL-DATE", FieldType.NUMERIC, 
            6);
        pnd_Aiaa085_Linkage_Ivc_Final_DateRedef4 = pnd_Aiaa085_Linkage.newGroupInGroup("pnd_Aiaa085_Linkage_Ivc_Final_DateRedef4", "Redefines", pnd_Aiaa085_Linkage_Ivc_Final_Date);
        pnd_Aiaa085_Linkage_Ivc_Final_Yr = pnd_Aiaa085_Linkage_Ivc_Final_DateRedef4.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Final_Yr", "IVC-FINAL-YR", 
            FieldType.NUMERIC, 4);
        pnd_Aiaa085_Linkage_Ivc_Final_Mo = pnd_Aiaa085_Linkage_Ivc_Final_DateRedef4.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Final_Mo", "IVC-FINAL-MO", 
            FieldType.NUMERIC, 2);
        pnd_Aiaa085_Linkage_Ivc_Mode = pnd_Aiaa085_Linkage.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Mode", "IVC-MODE", FieldType.NUMERIC, 3);
        pnd_Aiaa085_Linkage_Ivc_ModeRedef5 = pnd_Aiaa085_Linkage.newGroupInGroup("pnd_Aiaa085_Linkage_Ivc_ModeRedef5", "Redefines", pnd_Aiaa085_Linkage_Ivc_Mode);
        pnd_Aiaa085_Linkage_Ivc_Mode_1 = pnd_Aiaa085_Linkage_Ivc_ModeRedef5.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Mode_1", "IVC-MODE-1", FieldType.NUMERIC, 
            1);
        pnd_Aiaa085_Linkage_Ivc_Mode_2 = pnd_Aiaa085_Linkage_Ivc_ModeRedef5.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Mode_2", "IVC-MODE-2", FieldType.NUMERIC, 
            2);
        pnd_Aiaa085_Linkage_Ivc_Number_Of_Rates = pnd_Aiaa085_Linkage.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Number_Of_Rates", "IVC-NUMBER-OF-RATES", 
            FieldType.NUMERIC, 3);
        pnd_Aiaa085_Linkage_Ivc_Rate = pnd_Aiaa085_Linkage.newFieldArrayInGroup("pnd_Aiaa085_Linkage_Ivc_Rate", "IVC-RATE", FieldType.STRING, 2, new DbsArrayController(1,
            250));
        pnd_Aiaa085_Linkage_Ivc_Rate_Guar_Payment = pnd_Aiaa085_Linkage.newFieldArrayInGroup("pnd_Aiaa085_Linkage_Ivc_Rate_Guar_Payment", "IVC-RATE-GUAR-PAYMENT", 
            FieldType.DECIMAL, 11,2, new DbsArrayController(1,250));
        pnd_Aiaa085_Linkage_Ivc_Rate_Div_Payment = pnd_Aiaa085_Linkage.newFieldArrayInGroup("pnd_Aiaa085_Linkage_Ivc_Rate_Div_Payment", "IVC-RATE-DIV-PAYMENT", 
            FieldType.DECIMAL, 11,2, new DbsArrayController(1,250));
        pnd_Aiaa085_Linkage_Ivc_Cref_Fund_Code = pnd_Aiaa085_Linkage.newFieldArrayInGroup("pnd_Aiaa085_Linkage_Ivc_Cref_Fund_Code", "IVC-CREF-FUND-CODE", 
            FieldType.STRING, 1, new DbsArrayController(1,40));
        pnd_Aiaa085_Linkage_Ivc_Cref_Reval = pnd_Aiaa085_Linkage.newFieldArrayInGroup("pnd_Aiaa085_Linkage_Ivc_Cref_Reval", "IVC-CREF-REVAL", FieldType.STRING, 
            1, new DbsArrayController(1,40));
        pnd_Aiaa085_Linkage_Ivc_Cref_Payment = pnd_Aiaa085_Linkage.newFieldArrayInGroup("pnd_Aiaa085_Linkage_Ivc_Cref_Payment", "IVC-CREF-PAYMENT", FieldType.DECIMAL, 
            11,2, new DbsArrayController(1,40));
        pnd_Aiaa085_Linkage_Ivc_Cref_Units = pnd_Aiaa085_Linkage.newFieldArrayInGroup("pnd_Aiaa085_Linkage_Ivc_Cref_Units", "IVC-CREF-UNITS", FieldType.DECIMAL, 
            11,3, new DbsArrayController(1,40));
        pnd_Aiaa085_Linkage_Ivc_Remaining_Amount = pnd_Aiaa085_Linkage.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Remaining_Amount", "IVC-REMAINING-AMOUNT", 
            FieldType.DECIMAL, 13,2);
        pnd_Aiaa085_Linkage_Ivc_Calc_Method = pnd_Aiaa085_Linkage.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Calc_Method", "IVC-CALC-METHOD", FieldType.STRING, 
            1);
        pnd_Aiaa085_Linkage_Ivc_Begin_Date = pnd_Aiaa085_Linkage.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Begin_Date", "IVC-BEGIN-DATE", FieldType.NUMERIC, 
            8);
        pnd_Aiaa085_Linkage_Ivc_Total_Payment_Amount = pnd_Aiaa085_Linkage.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Total_Payment_Amount", "IVC-TOTAL-PAYMENT-AMOUNT", 
            FieldType.DECIMAL, 13,2);
        pnd_Aiaa085_Linkage_Ivc_Amount = pnd_Aiaa085_Linkage.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Amount", "IVC-AMOUNT", FieldType.DECIMAL, 13,2);
        pnd_Aiaa085_Linkage_Ivc_Pre_Tax_Amount = pnd_Aiaa085_Linkage.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Pre_Tax_Amount", "IVC-PRE-TAX-AMOUNT", FieldType.DECIMAL, 
            13,2);
        pnd_Aiaa085_Linkage_Ivc_Return_Code = pnd_Aiaa085_Linkage.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Return_Code", "IVC-RETURN-CODE", FieldType.STRING, 
            11);
        pnd_Aiaa085_Linkage_Ivc_Return_CodeRedef6 = pnd_Aiaa085_Linkage.newGroupInGroup("pnd_Aiaa085_Linkage_Ivc_Return_CodeRedef6", "Redefines", pnd_Aiaa085_Linkage_Ivc_Return_Code);
        pnd_Aiaa085_Linkage_Ivc_Return_Code_Pgm = pnd_Aiaa085_Linkage_Ivc_Return_CodeRedef6.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Return_Code_Pgm", 
            "IVC-RETURN-CODE-PGM", FieldType.STRING, 8);
        pnd_Aiaa085_Linkage_Ivc_Return_Code_Nbr = pnd_Aiaa085_Linkage_Ivc_Return_CodeRedef6.newFieldInGroup("pnd_Aiaa085_Linkage_Ivc_Return_Code_Nbr", 
            "IVC-RETURN-CODE-NBR", FieldType.NUMERIC, 3);

        dbsRecord.reset();
    }

    // Constructors
    public PdaAiaa085(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

