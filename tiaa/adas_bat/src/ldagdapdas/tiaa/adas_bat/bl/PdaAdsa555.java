/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:17:36 PM
**        * FROM NATURAL PDA     : ADSA555
************************************************************
**        * FILE NAME            : PdaAdsa555.java
**        * CLASS NAME           : PdaAdsa555
**        * INSTANCE NAME        : PdaAdsa555
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaAdsa555 extends PdaBase
{
    // Properties
    private DbsGroup tax_Linkage;
    private DbsField tax_Linkage_Ads_Tx_Tiaa_No;
    private DbsField tax_Linkage_Ads_Tx_Tiaa_Payee_No;
    private DbsGroup tax_Linkage_Ads_Tx_Tiaa_Payee_NoRedef1;
    private DbsField tax_Linkage_Ads_Tx_Tiaa_Payee_Cd;
    private DbsField tax_Linkage_Ads_Tx_Cref_No;
    private DbsField tax_Linkage_Ads_Tx_Cref_Payee_No;
    private DbsGroup tax_Linkage_Ads_Tx_Cref_Payee_NoRedef2;
    private DbsField tax_Linkage_Ads_Tx_Cref_Payee_Cd;
    private DbsField tax_Linkage_Ads_Rsdncy_Cde;
    private DbsField tax_Linkage_Ads_Ctzn_Cde;
    private DbsField tax_Linkage_Ads_Tx_Ss_Num;
    private DbsGroup tax_Linkage_Ads_Type_Of_Payment_Ind;
    private DbsField tax_Linkage_Ads_Pp_Pymt_Ind;
    private DbsField tax_Linkage_Ads_Mdo_Pymt_Ind;
    private DbsField tax_Linkage_Ads_Rtb_T_Pymt_Ind;
    private DbsField tax_Linkage_Ads_Rtb_C_Pymt_Ind;
    private DbsField tax_Linkage_Ads_Ls_Pymt_Ind;
    private DbsField tax_Linkage_Ads_Rollover_Ind;
    private DbsField tax_Linkage_Ads_Pp_Rollover_Ind;
    private DbsField tax_Linkage_Ads_Filler;
    private DbsField tax_Linkage_Ads_Settl_Option;
    private DbsField tax_Linkage_Ads_Term_Years;
    private DbsField tax_Linkage_Ads_Spouse_Ind;
    private DbsField tax_Linkage_Ads_Money_Source;
    private DbsField tax_Linkage_Ads_Lob;
    private DbsField tax_Linkage_Ads_Pymnt_Typ_Ind;
    private DbsField tax_Linkage_Ads_Hardship_Ind;
    private DbsField tax_Linkage_Ads_Dob;
    private DbsField tax_Linkage_Ads_Dod;
    private DbsField tax_Linkage_Ads_Roth_Frst_Cntrbtn_Dte;

    public DbsGroup getTax_Linkage() { return tax_Linkage; }

    public DbsField getTax_Linkage_Ads_Tx_Tiaa_No() { return tax_Linkage_Ads_Tx_Tiaa_No; }

    public DbsField getTax_Linkage_Ads_Tx_Tiaa_Payee_No() { return tax_Linkage_Ads_Tx_Tiaa_Payee_No; }

    public DbsGroup getTax_Linkage_Ads_Tx_Tiaa_Payee_NoRedef1() { return tax_Linkage_Ads_Tx_Tiaa_Payee_NoRedef1; }

    public DbsField getTax_Linkage_Ads_Tx_Tiaa_Payee_Cd() { return tax_Linkage_Ads_Tx_Tiaa_Payee_Cd; }

    public DbsField getTax_Linkage_Ads_Tx_Cref_No() { return tax_Linkage_Ads_Tx_Cref_No; }

    public DbsField getTax_Linkage_Ads_Tx_Cref_Payee_No() { return tax_Linkage_Ads_Tx_Cref_Payee_No; }

    public DbsGroup getTax_Linkage_Ads_Tx_Cref_Payee_NoRedef2() { return tax_Linkage_Ads_Tx_Cref_Payee_NoRedef2; }

    public DbsField getTax_Linkage_Ads_Tx_Cref_Payee_Cd() { return tax_Linkage_Ads_Tx_Cref_Payee_Cd; }

    public DbsField getTax_Linkage_Ads_Rsdncy_Cde() { return tax_Linkage_Ads_Rsdncy_Cde; }

    public DbsField getTax_Linkage_Ads_Ctzn_Cde() { return tax_Linkage_Ads_Ctzn_Cde; }

    public DbsField getTax_Linkage_Ads_Tx_Ss_Num() { return tax_Linkage_Ads_Tx_Ss_Num; }

    public DbsGroup getTax_Linkage_Ads_Type_Of_Payment_Ind() { return tax_Linkage_Ads_Type_Of_Payment_Ind; }

    public DbsField getTax_Linkage_Ads_Pp_Pymt_Ind() { return tax_Linkage_Ads_Pp_Pymt_Ind; }

    public DbsField getTax_Linkage_Ads_Mdo_Pymt_Ind() { return tax_Linkage_Ads_Mdo_Pymt_Ind; }

    public DbsField getTax_Linkage_Ads_Rtb_T_Pymt_Ind() { return tax_Linkage_Ads_Rtb_T_Pymt_Ind; }

    public DbsField getTax_Linkage_Ads_Rtb_C_Pymt_Ind() { return tax_Linkage_Ads_Rtb_C_Pymt_Ind; }

    public DbsField getTax_Linkage_Ads_Ls_Pymt_Ind() { return tax_Linkage_Ads_Ls_Pymt_Ind; }

    public DbsField getTax_Linkage_Ads_Rollover_Ind() { return tax_Linkage_Ads_Rollover_Ind; }

    public DbsField getTax_Linkage_Ads_Pp_Rollover_Ind() { return tax_Linkage_Ads_Pp_Rollover_Ind; }

    public DbsField getTax_Linkage_Ads_Filler() { return tax_Linkage_Ads_Filler; }

    public DbsField getTax_Linkage_Ads_Settl_Option() { return tax_Linkage_Ads_Settl_Option; }

    public DbsField getTax_Linkage_Ads_Term_Years() { return tax_Linkage_Ads_Term_Years; }

    public DbsField getTax_Linkage_Ads_Spouse_Ind() { return tax_Linkage_Ads_Spouse_Ind; }

    public DbsField getTax_Linkage_Ads_Money_Source() { return tax_Linkage_Ads_Money_Source; }

    public DbsField getTax_Linkage_Ads_Lob() { return tax_Linkage_Ads_Lob; }

    public DbsField getTax_Linkage_Ads_Pymnt_Typ_Ind() { return tax_Linkage_Ads_Pymnt_Typ_Ind; }

    public DbsField getTax_Linkage_Ads_Hardship_Ind() { return tax_Linkage_Ads_Hardship_Ind; }

    public DbsField getTax_Linkage_Ads_Dob() { return tax_Linkage_Ads_Dob; }

    public DbsField getTax_Linkage_Ads_Dod() { return tax_Linkage_Ads_Dod; }

    public DbsField getTax_Linkage_Ads_Roth_Frst_Cntrbtn_Dte() { return tax_Linkage_Ads_Roth_Frst_Cntrbtn_Dte; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        tax_Linkage = dbsRecord.newGroupInRecord("tax_Linkage", "TAX-LINKAGE");
        tax_Linkage.setParameterOption(ParameterOption.ByReference);
        tax_Linkage_Ads_Tx_Tiaa_No = tax_Linkage.newFieldInGroup("tax_Linkage_Ads_Tx_Tiaa_No", "ADS-TX-TIAA-NO", FieldType.STRING, 8);
        tax_Linkage_Ads_Tx_Tiaa_Payee_No = tax_Linkage.newFieldInGroup("tax_Linkage_Ads_Tx_Tiaa_Payee_No", "ADS-TX-TIAA-PAYEE-NO", FieldType.NUMERIC, 
            2);
        tax_Linkage_Ads_Tx_Tiaa_Payee_NoRedef1 = tax_Linkage.newGroupInGroup("tax_Linkage_Ads_Tx_Tiaa_Payee_NoRedef1", "Redefines", tax_Linkage_Ads_Tx_Tiaa_Payee_No);
        tax_Linkage_Ads_Tx_Tiaa_Payee_Cd = tax_Linkage_Ads_Tx_Tiaa_Payee_NoRedef1.newFieldInGroup("tax_Linkage_Ads_Tx_Tiaa_Payee_Cd", "ADS-TX-TIAA-PAYEE-CD", 
            FieldType.STRING, 2);
        tax_Linkage_Ads_Tx_Cref_No = tax_Linkage.newFieldInGroup("tax_Linkage_Ads_Tx_Cref_No", "ADS-TX-CREF-NO", FieldType.STRING, 8);
        tax_Linkage_Ads_Tx_Cref_Payee_No = tax_Linkage.newFieldInGroup("tax_Linkage_Ads_Tx_Cref_Payee_No", "ADS-TX-CREF-PAYEE-NO", FieldType.NUMERIC, 
            2);
        tax_Linkage_Ads_Tx_Cref_Payee_NoRedef2 = tax_Linkage.newGroupInGroup("tax_Linkage_Ads_Tx_Cref_Payee_NoRedef2", "Redefines", tax_Linkage_Ads_Tx_Cref_Payee_No);
        tax_Linkage_Ads_Tx_Cref_Payee_Cd = tax_Linkage_Ads_Tx_Cref_Payee_NoRedef2.newFieldInGroup("tax_Linkage_Ads_Tx_Cref_Payee_Cd", "ADS-TX-CREF-PAYEE-CD", 
            FieldType.STRING, 2);
        tax_Linkage_Ads_Rsdncy_Cde = tax_Linkage.newFieldInGroup("tax_Linkage_Ads_Rsdncy_Cde", "ADS-RSDNCY-CDE", FieldType.STRING, 2);
        tax_Linkage_Ads_Ctzn_Cde = tax_Linkage.newFieldInGroup("tax_Linkage_Ads_Ctzn_Cde", "ADS-CTZN-CDE", FieldType.STRING, 2);
        tax_Linkage_Ads_Tx_Ss_Num = tax_Linkage.newFieldInGroup("tax_Linkage_Ads_Tx_Ss_Num", "ADS-TX-SS-NUM", FieldType.NUMERIC, 9);
        tax_Linkage_Ads_Type_Of_Payment_Ind = tax_Linkage.newGroupInGroup("tax_Linkage_Ads_Type_Of_Payment_Ind", "ADS-TYPE-OF-PAYMENT-IND");
        tax_Linkage_Ads_Pp_Pymt_Ind = tax_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("tax_Linkage_Ads_Pp_Pymt_Ind", "ADS-PP-PYMT-IND", FieldType.STRING, 
            1);
        tax_Linkage_Ads_Mdo_Pymt_Ind = tax_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("tax_Linkage_Ads_Mdo_Pymt_Ind", "ADS-MDO-PYMT-IND", FieldType.STRING, 
            1);
        tax_Linkage_Ads_Rtb_T_Pymt_Ind = tax_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("tax_Linkage_Ads_Rtb_T_Pymt_Ind", "ADS-RTB-T-PYMT-IND", FieldType.STRING, 
            1);
        tax_Linkage_Ads_Rtb_C_Pymt_Ind = tax_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("tax_Linkage_Ads_Rtb_C_Pymt_Ind", "ADS-RTB-C-PYMT-IND", FieldType.STRING, 
            1);
        tax_Linkage_Ads_Ls_Pymt_Ind = tax_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("tax_Linkage_Ads_Ls_Pymt_Ind", "ADS-LS-PYMT-IND", FieldType.STRING, 
            1);
        tax_Linkage_Ads_Rollover_Ind = tax_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("tax_Linkage_Ads_Rollover_Ind", "ADS-ROLLOVER-IND", FieldType.STRING, 
            1);
        tax_Linkage_Ads_Pp_Rollover_Ind = tax_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("tax_Linkage_Ads_Pp_Rollover_Ind", "ADS-PP-ROLLOVER-IND", 
            FieldType.STRING, 1);
        tax_Linkage_Ads_Filler = tax_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("tax_Linkage_Ads_Filler", "ADS-FILLER", FieldType.STRING, 5);
        tax_Linkage_Ads_Settl_Option = tax_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("tax_Linkage_Ads_Settl_Option", "ADS-SETTL-OPTION", FieldType.STRING, 
            5);
        tax_Linkage_Ads_Term_Years = tax_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("tax_Linkage_Ads_Term_Years", "ADS-TERM-YEARS", FieldType.STRING, 
            1);
        tax_Linkage_Ads_Spouse_Ind = tax_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("tax_Linkage_Ads_Spouse_Ind", "ADS-SPOUSE-IND", FieldType.STRING, 
            1);
        tax_Linkage_Ads_Money_Source = tax_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("tax_Linkage_Ads_Money_Source", "ADS-MONEY-SOURCE", FieldType.STRING, 
            5);
        tax_Linkage_Ads_Lob = tax_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("tax_Linkage_Ads_Lob", "ADS-LOB", FieldType.STRING, 5);
        tax_Linkage_Ads_Pymnt_Typ_Ind = tax_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("tax_Linkage_Ads_Pymnt_Typ_Ind", "ADS-PYMNT-TYP-IND", FieldType.STRING, 
            2);
        tax_Linkage_Ads_Hardship_Ind = tax_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("tax_Linkage_Ads_Hardship_Ind", "ADS-HARDSHIP-IND", FieldType.STRING, 
            1);
        tax_Linkage_Ads_Dob = tax_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("tax_Linkage_Ads_Dob", "ADS-DOB", FieldType.NUMERIC, 8);
        tax_Linkage_Ads_Dod = tax_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("tax_Linkage_Ads_Dod", "ADS-DOD", FieldType.NUMERIC, 8);
        tax_Linkage_Ads_Roth_Frst_Cntrbtn_Dte = tax_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("tax_Linkage_Ads_Roth_Frst_Cntrbtn_Dte", "ADS-ROTH-FRST-CNTRBTN-DTE", 
            FieldType.NUMERIC, 8);

        dbsRecord.reset();
    }

    // Constructors
    public PdaAdsa555(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

