/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:49:59 PM
**        * FROM NATURAL LDA     : ADSL601
************************************************************
**        * FILE NAME            : LdaAdsl601.java
**        * CLASS NAME           : LdaAdsl601
**        * INSTANCE NAME        : LdaAdsl601
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdsl601 extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_ads_Ia_Rslt;
    private DbsField ads_Ia_Rslt_Rqst_Id;
    private DbsField ads_Ia_Rslt_Adi_Ia_Rcrd_Cde;
    private DbsField ads_Ia_Rslt_Adi_Sqnce_Nbr;
    private DbsField ads_Ia_Rslt_Adi_Stts_Cde;
    private DbsGroup ads_Ia_Rslt_Adi_Tiaa_NbrsMuGroup;
    private DbsField ads_Ia_Rslt_Adi_Tiaa_Nbrs;
    private DbsGroup ads_Ia_Rslt_Adi_Cref_NbrsMuGroup;
    private DbsField ads_Ia_Rslt_Adi_Cref_Nbrs;
    private DbsField ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr;
    private DbsField ads_Ia_Rslt_Adi_Ia_Tiaa_Payee_Cde;
    private DbsField ads_Ia_Rslt_Adi_Ia_Cref_Nbr;
    private DbsField ads_Ia_Rslt_Adi_Ia_Cref_Payee_Cde;
    private DbsField ads_Ia_Rslt_Adi_Curncy_Cde;
    private DbsField ads_Ia_Rslt_Adi_Pymnt_Cde;
    private DbsField ads_Ia_Rslt_Adi_Pymnt_Mode;
    private DbsField ads_Ia_Rslt_Adi_Lst_Actvty_Dte;
    private DbsField ads_Ia_Rslt_Adi_Annty_Strt_Dte;
    private DbsField ads_Ia_Rslt_Adi_Instllmnt_Dte;
    private DbsField ads_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte;
    private DbsField ads_Ia_Rslt_Adi_Frst_Ck_Pd_Dte;
    private DbsField ads_Ia_Rslt_Adi_Finl_Periodic_Py_Dte;
    private DbsField ads_Ia_Rslt_Adi_Finl_Prtl_Py_Dte;
    private DbsGroup ads_Ia_Rslt_Adi_Ivc_Data;
    private DbsField ads_Ia_Rslt_Adi_Tiaa_Ivc_Amt;
    private DbsField ads_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind;
    private DbsField ads_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt;
    private DbsField ads_Ia_Rslt_Adi_Cref_Ivc_Amt;
    private DbsField ads_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind;
    private DbsField ads_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt;
    private DbsGroup ads_Ia_Rslt_Adi_Dtl_Tiaa_Data;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Rate_Cd;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Std_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Grd_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Acct_Cd;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Eff_Rte_Intrst;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt;
    private DbsGroup ads_Ia_Rslt_Adi_Dtl_Cref_Data;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Da_Rate_Cd;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Amt;
    private DbsField ads_Ia_Rslt_Adi_Optn_Cde;
    private DbsField ads_Ia_Rslt_Adi_Tiaa_Sttlmnt;
    private DbsField ads_Ia_Rslt_Adi_Tiaa_Re_Sttlmnt;
    private DbsField ads_Ia_Rslt_Adi_Cref_Sttlmnt;
    private DbsGroup ads_Ia_Rslt_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt;
    private DbsGroup ads_Ia_Rslt_Adi_Orig_Lob_IndMuGroup;
    private DbsField ads_Ia_Rslt_Adi_Orig_Lob_Ind;
    private DbsField ads_Ia_Rslt_Adi_Roth_Rqst_Ind;
    private DbsField ads_Ia_Rslt_Adi_Srvvr_Ind;
    private DbsGroup ads_Ia_Rslt_Adi_Contract_IdMuGroup;
    private DbsField ads_Ia_Rslt_Adi_Contract_Id;
    private DataAccessProgramView vw_iaa_Cntrct;
    private DbsField iaa_Cntrct_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Cntrct_Type;
    private DbsField iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Cntrct_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Cntrct_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField iaa_Cntrct_Cntrct_Ssnng_Dte;
    private DbsField iaa_Cntrct_Roth_Frst_Cntrb_Dte;
    private DbsField iaa_Cntrct_Roth_Ssnng_Dte;
    private DbsField iaa_Cntrct_Plan_Nmbr;
    private DbsField iaa_Cntrct_Tax_Exmpt_Ind;
    private DbsField iaa_Cntrct_Orig_Ownr_Dob;
    private DbsField iaa_Cntrct_Orig_Ownr_Dod;
    private DbsField iaa_Cntrct_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_Orgntng_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_Orgntng_Cntrct_Nmbr;
    private DataAccessProgramView vw_iaa_Cntrct_Prtcpnt_Role;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde;
    private DbsGroup iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind;
    private DbsGroup iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind;
    private DbsField iaa_Cntrct_Prtcpnt_Role_Roth_Dsblty_Dte;
    private DataAccessProgramView vw_iaa_Tiaa_Fund_Rcrd;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tckr_Symbl;
    private DbsGroup iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind;
    private DbsField iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund;
    private DataAccessProgramView vw_iaa_Cref_Fund_Rcrd_1;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Payee_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_Cde;
    private DbsGroup iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_CdeRedef2;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Unit_Val;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Old_Per_Amt;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Old_Unit_Val;
    private DbsField iaa_Cref_Fund_Rcrd_1_Count_Castcref_Rate_Data_Grp;
    private DbsGroup iaa_Cref_Fund_Rcrd_1_Cref_Rate_Data_Grp;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Rate_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt;
    private DbsField iaa_Cref_Fund_Rcrd_1_Lst_Trans_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Xfr_Iss_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Lst_Xfr_In_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_1_Cref_Lst_Xfr_Out_Dte;
    private DbsField iaa_Cref_Fund_Rcrd_1_Tckr_Symbl;
    private DataAccessProgramView vw_iaa_Trans_Rcrd;
    private DbsField iaa_Trans_Rcrd_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Invrse_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Lst_Trans_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Ppcn_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Payee_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Sub_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Actvty_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Check_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_Todays_Dte;
    private DbsField iaa_Trans_Rcrd_Trans_User_Area;
    private DbsField iaa_Trans_Rcrd_Trans_User_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Verify_Id;
    private DbsField iaa_Trans_Rcrd_Trans_Cmbne_Cde;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Wpid;
    private DbsField iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr;
    private DbsField iaa_Trans_Rcrd_Trans_Effective_Dte;
    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts;
    private DbsGroup iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde;
    private DbsGroup iaa_Cntrl_Rcrd_1_Cntrl_Fund_CdeRedef3;
    private DbsGroup iaa_Cntrl_Rcrd_1_Cf_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cmpny_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Fund_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Units;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees;
    private DataAccessProgramView vw_iaa_Cntrct_Trans;
    private DbsField iaa_Cntrct_Trans_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Trans_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Orgn_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Acctng_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Issue_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Crrncy_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Type_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr;
    private DbsField iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt;
    private DbsField iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt;
    private DbsField iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn;
    private DbsField iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde;
    private DbsField iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde;
    private DbsField iaa_Cntrct_Trans_Trans_Check_Dte;
    private DbsField iaa_Cntrct_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cntrct_Trans_Aftr_Imge_Id;
    private DbsField iaa_Cntrct_Trans_Cntrct_Type;
    private DbsField iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re;
    private DbsGroup iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup;
    private DbsField iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn;
    private DbsField iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte;
    private DbsField iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd;
    private DbsField iaa_Cntrct_Trans_Cntrct_Ssnng_Dte;
    private DbsField iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte;
    private DbsField iaa_Cntrct_Trans_Roth_Ssnng_Dte;
    private DbsField iaa_Cntrct_Trans_Plan_Nmbr;
    private DbsField iaa_Cntrct_Trans_Tax_Exmpt_Ind;
    private DbsField iaa_Cntrct_Trans_Orig_Ownr_Dob;
    private DbsField iaa_Cntrct_Trans_Orig_Ownr_Dod;
    private DbsField iaa_Cntrct_Trans_Sub_Plan_Nmbr;
    private DbsField iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr;
    private DbsField iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr;
    private DataAccessProgramView vw_iaa_Cpr_Trans;
    private DbsField iaa_Cpr_Trans_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Lst_Trans_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr;
    private DbsField iaa_Cpr_Trans_Cntrct_Part_Payee_Cde;
    private DbsField iaa_Cpr_Trans_Cpr_Id_Nbr;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr;
    private DbsField iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ;
    private DbsField iaa_Cpr_Trans_Cntrct_Actvty_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Trmnte_Rsn;
    private DbsField iaa_Cpr_Trans_Cntrct_Rwrttn_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Cash_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde;
    private DbsGroup iaa_Cpr_Trans_Cntrct_Company_Data;
    private DbsField iaa_Cpr_Trans_Cntrct_Company_Cd;
    private DbsField iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Ivc_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Rtb_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Rtb_Percent;
    private DbsField iaa_Cpr_Trans_Cntrct_Mode_Ind;
    private DbsField iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Final_Pay_Dte;
    private DbsField iaa_Cpr_Trans_Bnfcry_Xref_Ind;
    private DbsField iaa_Cpr_Trans_Bnfcry_Dod_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Hold_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Pend_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Cmbne_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Srce;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte;
    private DbsField iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_State_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_State_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Local_Cde;
    private DbsField iaa_Cpr_Trans_Cntrct_Local_Tax_Amt;
    private DbsField iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte;
    private DbsField iaa_Cpr_Trans_Trans_Check_Dte;
    private DbsField iaa_Cpr_Trans_Bfre_Imge_Id;
    private DbsField iaa_Cpr_Trans_Aftr_Imge_Id;
    private DbsField iaa_Cpr_Trans_Cpr_Xfr_Term_Cde;
    private DbsField iaa_Cpr_Trans_Cpr_Lgl_Res_Cde;
    private DbsField iaa_Cpr_Trans_Cpr_Xfr_Iss_Dte;
    private DbsField iaa_Cpr_Trans_Rllvr_Cntrct_Nbr;
    private DbsField iaa_Cpr_Trans_Rllvr_Ivc_Ind;
    private DbsField iaa_Cpr_Trans_Rllvr_Elgble_Ind;
    private DbsGroup iaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_CdeMuGroup;
    private DbsField iaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_Cde;
    private DbsField iaa_Cpr_Trans_Rllvr_Accptng_Irc_Cde;
    private DbsField iaa_Cpr_Trans_Rllvr_Pln_Admn_Ind;
    private DbsField iaa_Cpr_Trans_Roth_Dsblty_Dte;
    private DataAccessProgramView vw_iaa_Tiaa_Fund_Trans;
    private DbsField iaa_Tiaa_Fund_Trans_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Lst_Trans_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde;
    private DbsGroup iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef4;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp;
    private DbsGroup iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt;
    private DbsField iaa_Tiaa_Fund_Trans_Trans_Check_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Bfre_Imge_Id;
    private DbsField iaa_Tiaa_Fund_Trans_Aftr_Imge_Id;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Xfr_Iss_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_In_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_Out_Dte;
    private DbsField iaa_Tiaa_Fund_Trans_Tckr_Symbl;
    private DbsGroup iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind;
    private DbsField iaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund;
    private DataAccessProgramView vw_iaa_Cref_Fund_Trans_1;
    private DbsField iaa_Cref_Fund_Trans_1_Trans_Dte;
    private DbsField iaa_Cref_Fund_Trans_1_Invrse_Trans_Dte;
    private DbsField iaa_Cref_Fund_Trans_1_Lst_Trans_Dte;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Cntrct_Payee_Cde;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_Cde;
    private DbsGroup iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_CdeRedef5;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Cmpny_Cde;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Fund_Cde;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Tot_Per_Amt;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Unit_Val;
    private DbsField iaa_Cref_Fund_Trans_1_Count_Castcref_Rate_Data_Grp;
    private DbsGroup iaa_Cref_Fund_Trans_1_Cref_Rate_Data_Grp;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Rate_Cde;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Rate_Dte;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Units_Cnt;
    private DbsField iaa_Cref_Fund_Trans_1_Trans_Check_Dte;
    private DbsField iaa_Cref_Fund_Trans_1_Bfre_Imge_Id;
    private DbsField iaa_Cref_Fund_Trans_1_Aftr_Imge_Id;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Xfr_Iss_Dte;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Lst_Xfr_In_Dte;
    private DbsField iaa_Cref_Fund_Trans_1_Cref_Lst_Xfr_Out_Dte;
    private DbsField iaa_Cref_Fund_Trans_1_Tckr_Symbl;

    public DataAccessProgramView getVw_ads_Ia_Rslt() { return vw_ads_Ia_Rslt; }

    public DbsField getAds_Ia_Rslt_Rqst_Id() { return ads_Ia_Rslt_Rqst_Id; }

    public DbsField getAds_Ia_Rslt_Adi_Ia_Rcrd_Cde() { return ads_Ia_Rslt_Adi_Ia_Rcrd_Cde; }

    public DbsField getAds_Ia_Rslt_Adi_Sqnce_Nbr() { return ads_Ia_Rslt_Adi_Sqnce_Nbr; }

    public DbsField getAds_Ia_Rslt_Adi_Stts_Cde() { return ads_Ia_Rslt_Adi_Stts_Cde; }

    public DbsGroup getAds_Ia_Rslt_Adi_Tiaa_NbrsMuGroup() { return ads_Ia_Rslt_Adi_Tiaa_NbrsMuGroup; }

    public DbsField getAds_Ia_Rslt_Adi_Tiaa_Nbrs() { return ads_Ia_Rslt_Adi_Tiaa_Nbrs; }

    public DbsGroup getAds_Ia_Rslt_Adi_Cref_NbrsMuGroup() { return ads_Ia_Rslt_Adi_Cref_NbrsMuGroup; }

    public DbsField getAds_Ia_Rslt_Adi_Cref_Nbrs() { return ads_Ia_Rslt_Adi_Cref_Nbrs; }

    public DbsField getAds_Ia_Rslt_Adi_Ia_Tiaa_Nbr() { return ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr; }

    public DbsField getAds_Ia_Rslt_Adi_Ia_Tiaa_Payee_Cde() { return ads_Ia_Rslt_Adi_Ia_Tiaa_Payee_Cde; }

    public DbsField getAds_Ia_Rslt_Adi_Ia_Cref_Nbr() { return ads_Ia_Rslt_Adi_Ia_Cref_Nbr; }

    public DbsField getAds_Ia_Rslt_Adi_Ia_Cref_Payee_Cde() { return ads_Ia_Rslt_Adi_Ia_Cref_Payee_Cde; }

    public DbsField getAds_Ia_Rslt_Adi_Curncy_Cde() { return ads_Ia_Rslt_Adi_Curncy_Cde; }

    public DbsField getAds_Ia_Rslt_Adi_Pymnt_Cde() { return ads_Ia_Rslt_Adi_Pymnt_Cde; }

    public DbsField getAds_Ia_Rslt_Adi_Pymnt_Mode() { return ads_Ia_Rslt_Adi_Pymnt_Mode; }

    public DbsField getAds_Ia_Rslt_Adi_Lst_Actvty_Dte() { return ads_Ia_Rslt_Adi_Lst_Actvty_Dte; }

    public DbsField getAds_Ia_Rslt_Adi_Annty_Strt_Dte() { return ads_Ia_Rslt_Adi_Annty_Strt_Dte; }

    public DbsField getAds_Ia_Rslt_Adi_Instllmnt_Dte() { return ads_Ia_Rslt_Adi_Instllmnt_Dte; }

    public DbsField getAds_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte() { return ads_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte; }

    public DbsField getAds_Ia_Rslt_Adi_Frst_Ck_Pd_Dte() { return ads_Ia_Rslt_Adi_Frst_Ck_Pd_Dte; }

    public DbsField getAds_Ia_Rslt_Adi_Finl_Periodic_Py_Dte() { return ads_Ia_Rslt_Adi_Finl_Periodic_Py_Dte; }

    public DbsField getAds_Ia_Rslt_Adi_Finl_Prtl_Py_Dte() { return ads_Ia_Rslt_Adi_Finl_Prtl_Py_Dte; }

    public DbsGroup getAds_Ia_Rslt_Adi_Ivc_Data() { return ads_Ia_Rslt_Adi_Ivc_Data; }

    public DbsField getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt() { return ads_Ia_Rslt_Adi_Tiaa_Ivc_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind() { return ads_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind; }

    public DbsField getAds_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt() { return ads_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Cref_Ivc_Amt() { return ads_Ia_Rslt_Adi_Cref_Ivc_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind() { return ads_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind; }

    public DbsField getAds_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt() { return ads_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt; }

    public DbsGroup getAds_Ia_Rslt_Adi_Dtl_Tiaa_Data() { return ads_Ia_Rslt_Adi_Dtl_Tiaa_Data; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Tiaa_Da_Rate_Cd() { return ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Rate_Cd; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Tiaa_Da_Std_Amt() { return ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Std_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Tiaa_Da_Grd_Amt() { return ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Grd_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Tiaa_Acct_Cd() { return ads_Ia_Rslt_Adi_Dtl_Tiaa_Acct_Cd; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd() { return ads_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Tiaa_Eff_Rte_Intrst() { return ads_Ia_Rslt_Adi_Dtl_Tiaa_Eff_Rte_Intrst; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt() { return ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt() { return ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt() { return ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt() { return ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt() { return ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt() { return ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt() { return ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt() { return ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt; }

    public DbsGroup getAds_Ia_Rslt_Adi_Dtl_Cref_Data() { return ads_Ia_Rslt_Adi_Dtl_Cref_Data; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Rate_Cd() { return ads_Ia_Rslt_Adi_Dtl_Cref_Da_Rate_Cd; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd() { return ads_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd() { return ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt() { return ads_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units() { return ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val() { return ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Amt() { return ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt() { return ads_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units() { return ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val() { return ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Amt() { return ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Optn_Cde() { return ads_Ia_Rslt_Adi_Optn_Cde; }

    public DbsField getAds_Ia_Rslt_Adi_Tiaa_Sttlmnt() { return ads_Ia_Rslt_Adi_Tiaa_Sttlmnt; }

    public DbsField getAds_Ia_Rslt_Adi_Tiaa_Re_Sttlmnt() { return ads_Ia_Rslt_Adi_Tiaa_Re_Sttlmnt; }

    public DbsField getAds_Ia_Rslt_Adi_Cref_Sttlmnt() { return ads_Ia_Rslt_Adi_Cref_Sttlmnt; }

    public DbsGroup getAds_Ia_Rslt_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat() { return ads_Ia_Rslt_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt() { return ads_Ia_Rslt_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt; }

    public DbsField getAds_Ia_Rslt_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt() { return ads_Ia_Rslt_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt; }

    public DbsGroup getAds_Ia_Rslt_Adi_Orig_Lob_IndMuGroup() { return ads_Ia_Rslt_Adi_Orig_Lob_IndMuGroup; }

    public DbsField getAds_Ia_Rslt_Adi_Orig_Lob_Ind() { return ads_Ia_Rslt_Adi_Orig_Lob_Ind; }

    public DbsField getAds_Ia_Rslt_Adi_Roth_Rqst_Ind() { return ads_Ia_Rslt_Adi_Roth_Rqst_Ind; }

    public DbsField getAds_Ia_Rslt_Adi_Srvvr_Ind() { return ads_Ia_Rslt_Adi_Srvvr_Ind; }

    public DbsGroup getAds_Ia_Rslt_Adi_Contract_IdMuGroup() { return ads_Ia_Rslt_Adi_Contract_IdMuGroup; }

    public DbsField getAds_Ia_Rslt_Adi_Contract_Id() { return ads_Ia_Rslt_Adi_Contract_Id; }

    public DataAccessProgramView getVw_iaa_Cntrct() { return vw_iaa_Cntrct; }

    public DbsField getIaa_Cntrct_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Cntrct_Optn_Cde() { return iaa_Cntrct_Cntrct_Optn_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Orgn_Cde() { return iaa_Cntrct_Cntrct_Orgn_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Acctng_Cde() { return iaa_Cntrct_Cntrct_Acctng_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Issue_Dte() { return iaa_Cntrct_Cntrct_Issue_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte() { return iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte() { return iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Crrncy_Cde() { return iaa_Cntrct_Cntrct_Crrncy_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Type_Cde() { return iaa_Cntrct_Cntrct_Type_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Pymnt_Mthd() { return iaa_Cntrct_Cntrct_Pymnt_Mthd; }

    public DbsField getIaa_Cntrct_Cntrct_Pnsn_Pln_Cde() { return iaa_Cntrct_Cntrct_Pnsn_Pln_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind() { return iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind; }

    public DbsField getIaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr() { return iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr; }

    public DbsField getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde() { return iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind() { return iaa_Cntrct_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte() { return iaa_Cntrct_Cntrct_First_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde() { return iaa_Cntrct_Cntrct_First_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt() { return iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt; }

    public DbsField getIaa_Cntrct_Cntrct_First_Annt_Dod_Dte() { return iaa_Cntrct_Cntrct_First_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde() { return iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt() { return iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt; }

    public DbsField getIaa_Cntrct_Cntrct_Scnd_Annt_Ssn() { return iaa_Cntrct_Cntrct_Scnd_Annt_Ssn; }

    public DbsField getIaa_Cntrct_Cntrct_Div_Payee_Cde() { return iaa_Cntrct_Cntrct_Div_Payee_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Div_Coll_Cde() { return iaa_Cntrct_Cntrct_Div_Coll_Cde; }

    public DbsField getIaa_Cntrct_Cntrct_Inst_Iss_Cde() { return iaa_Cntrct_Cntrct_Inst_Iss_Cde; }

    public DbsField getIaa_Cntrct_Lst_Trans_Dte() { return iaa_Cntrct_Lst_Trans_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Type() { return iaa_Cntrct_Cntrct_Type; }

    public DbsField getIaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re() { return iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re; }

    public DbsGroup getIaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup() { return iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup; }

    public DbsField getIaa_Cntrct_Cntrct_Fnl_Prm_Dte() { return iaa_Cntrct_Cntrct_Fnl_Prm_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Mtch_Ppcn() { return iaa_Cntrct_Cntrct_Mtch_Ppcn; }

    public DbsField getIaa_Cntrct_Cntrct_Annty_Strt_Dte() { return iaa_Cntrct_Cntrct_Annty_Strt_Dte; }

    public DbsField getIaa_Cntrct_Cntrct_Issue_Dte_Dd() { return iaa_Cntrct_Cntrct_Issue_Dte_Dd; }

    public DbsField getIaa_Cntrct_Cntrct_Fp_Due_Dte_Dd() { return iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd; }

    public DbsField getIaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd() { return iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd; }

    public DbsField getIaa_Cntrct_Cntrct_Ssnng_Dte() { return iaa_Cntrct_Cntrct_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_Roth_Frst_Cntrb_Dte() { return iaa_Cntrct_Roth_Frst_Cntrb_Dte; }

    public DbsField getIaa_Cntrct_Roth_Ssnng_Dte() { return iaa_Cntrct_Roth_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_Plan_Nmbr() { return iaa_Cntrct_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Tax_Exmpt_Ind() { return iaa_Cntrct_Tax_Exmpt_Ind; }

    public DbsField getIaa_Cntrct_Orig_Ownr_Dob() { return iaa_Cntrct_Orig_Ownr_Dob; }

    public DbsField getIaa_Cntrct_Orig_Ownr_Dod() { return iaa_Cntrct_Orig_Ownr_Dod; }

    public DbsField getIaa_Cntrct_Sub_Plan_Nmbr() { return iaa_Cntrct_Sub_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Orgntng_Sub_Plan_Nmbr() { return iaa_Cntrct_Orgntng_Sub_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Orgntng_Cntrct_Nmbr() { return iaa_Cntrct_Orgntng_Cntrct_Nmbr; }

    public DataAccessProgramView getVw_iaa_Cntrct_Prtcpnt_Role() { return vw_iaa_Cntrct_Prtcpnt_Role; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr() { return iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte() { return iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde() { return iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde() { return iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw() { return iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr() { return iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ() { return iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde; }

    public DbsGroup getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind() { return iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte() { return iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde() { return iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte() { return iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr() { return iaa_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind() { return iaa_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind() { return iaa_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind; }

    public DbsGroup getIaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup() { return iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde() { return iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde() { return iaa_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind() { return iaa_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind; }

    public DbsField getIaa_Cntrct_Prtcpnt_Role_Roth_Dsblty_Dte() { return iaa_Cntrct_Prtcpnt_Role_Roth_Dsblty_Dte; }

    public DataAccessProgramView getVw_iaa_Tiaa_Fund_Rcrd() { return vw_iaa_Tiaa_Fund_Rcrd; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte() { return iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tckr_Symbl() { return iaa_Tiaa_Fund_Rcrd_Tckr_Symbl; }

    public DbsGroup getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind; }

    public DbsField getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund() { return iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund; }

    public DataAccessProgramView getVw_iaa_Cref_Fund_Rcrd_1() { return vw_iaa_Cref_Fund_Rcrd_1; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr() { return iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Payee_Cde() { return iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Payee_Cde; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_Cde() { return iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_CdeRedef2() { return iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_CdeRedef2; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde() { return iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde() { return iaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt() { return iaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Unit_Val() { return iaa_Cref_Fund_Rcrd_1_Cref_Unit_Val; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Old_Per_Amt() { return iaa_Cref_Fund_Rcrd_1_Cref_Old_Per_Amt; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Old_Unit_Val() { return iaa_Cref_Fund_Rcrd_1_Cref_Old_Unit_Val; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Count_Castcref_Rate_Data_Grp() { return iaa_Cref_Fund_Rcrd_1_Count_Castcref_Rate_Data_Grp; }

    public DbsGroup getIaa_Cref_Fund_Rcrd_1_Cref_Rate_Data_Grp() { return iaa_Cref_Fund_Rcrd_1_Cref_Rate_Data_Grp; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde() { return iaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Rate_Dte() { return iaa_Cref_Fund_Rcrd_1_Cref_Rate_Dte; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt() { return iaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Lst_Trans_Dte() { return iaa_Cref_Fund_Rcrd_1_Lst_Trans_Dte; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Xfr_Iss_Dte() { return iaa_Cref_Fund_Rcrd_1_Cref_Xfr_Iss_Dte; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Lst_Xfr_In_Dte() { return iaa_Cref_Fund_Rcrd_1_Cref_Lst_Xfr_In_Dte; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Cref_Lst_Xfr_Out_Dte() { return iaa_Cref_Fund_Rcrd_1_Cref_Lst_Xfr_Out_Dte; }

    public DbsField getIaa_Cref_Fund_Rcrd_1_Tckr_Symbl() { return iaa_Cref_Fund_Rcrd_1_Tckr_Symbl; }

    public DataAccessProgramView getVw_iaa_Trans_Rcrd() { return vw_iaa_Trans_Rcrd; }

    public DbsField getIaa_Trans_Rcrd_Trans_Dte() { return iaa_Trans_Rcrd_Trans_Dte; }

    public DbsField getIaa_Trans_Rcrd_Invrse_Trans_Dte() { return iaa_Trans_Rcrd_Invrse_Trans_Dte; }

    public DbsField getIaa_Trans_Rcrd_Lst_Trans_Dte() { return iaa_Trans_Rcrd_Lst_Trans_Dte; }

    public DbsField getIaa_Trans_Rcrd_Trans_Ppcn_Nbr() { return iaa_Trans_Rcrd_Trans_Ppcn_Nbr; }

    public DbsField getIaa_Trans_Rcrd_Trans_Payee_Cde() { return iaa_Trans_Rcrd_Trans_Payee_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Sub_Cde() { return iaa_Trans_Rcrd_Trans_Sub_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Cde() { return iaa_Trans_Rcrd_Trans_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Actvty_Cde() { return iaa_Trans_Rcrd_Trans_Actvty_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Check_Dte() { return iaa_Trans_Rcrd_Trans_Check_Dte; }

    public DbsField getIaa_Trans_Rcrd_Trans_Todays_Dte() { return iaa_Trans_Rcrd_Trans_Todays_Dte; }

    public DbsField getIaa_Trans_Rcrd_Trans_User_Area() { return iaa_Trans_Rcrd_Trans_User_Area; }

    public DbsField getIaa_Trans_Rcrd_Trans_User_Id() { return iaa_Trans_Rcrd_Trans_User_Id; }

    public DbsField getIaa_Trans_Rcrd_Trans_Verify_Cde() { return iaa_Trans_Rcrd_Trans_Verify_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Verify_Id() { return iaa_Trans_Rcrd_Trans_Verify_Id; }

    public DbsField getIaa_Trans_Rcrd_Trans_Cmbne_Cde() { return iaa_Trans_Rcrd_Trans_Cmbne_Cde; }

    public DbsField getIaa_Trans_Rcrd_Trans_Cwf_Wpid() { return iaa_Trans_Rcrd_Trans_Cwf_Wpid; }

    public DbsField getIaa_Trans_Rcrd_Trans_Cwf_Id_Nbr() { return iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr; }

    public DbsField getIaa_Trans_Rcrd_Trans_Effective_Dte() { return iaa_Trans_Rcrd_Trans_Effective_Dte; }

    public DataAccessProgramView getVw_iaa_Cntrl_Rcrd_1() { return vw_iaa_Cntrl_Rcrd_1; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Check_Dte() { return iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte() { return iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Cde() { return iaa_Cntrl_Rcrd_1_Cntrl_Cde; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte() { return iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde() { return iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees() { return iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt() { return iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt() { return iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt() { return iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt() { return iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt() { return iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte() { return iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte; }

    public DbsField getIaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts() { return iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts; }

    public DbsGroup getIaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts() { return iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde() { return iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde; }

    public DbsGroup getIaa_Cntrl_Rcrd_1_Cntrl_Fund_CdeRedef3() { return iaa_Cntrl_Rcrd_1_Cntrl_Fund_CdeRedef3; }

    public DbsGroup getIaa_Cntrl_Rcrd_1_Cf_Cde() { return iaa_Cntrl_Rcrd_1_Cf_Cde; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cmpny_Cde() { return iaa_Cntrl_Rcrd_1_Cmpny_Cde; }

    public DbsField getIaa_Cntrl_Rcrd_1_Fund_Cde() { return iaa_Cntrl_Rcrd_1_Fund_Cde; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees() { return iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Units() { return iaa_Cntrl_Rcrd_1_Cntrl_Units; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Amt() { return iaa_Cntrl_Rcrd_1_Cntrl_Amt; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt() { return iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt() { return iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys() { return iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys() { return iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys; }

    public DbsField getIaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees() { return iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees; }

    public DataAccessProgramView getVw_iaa_Cntrct_Trans() { return vw_iaa_Cntrct_Trans; }

    public DbsField getIaa_Cntrct_Trans_Trans_Dte() { return iaa_Cntrct_Trans_Trans_Dte; }

    public DbsField getIaa_Cntrct_Trans_Invrse_Trans_Dte() { return iaa_Cntrct_Trans_Invrse_Trans_Dte; }

    public DbsField getIaa_Cntrct_Trans_Lst_Trans_Dte() { return iaa_Cntrct_Trans_Lst_Trans_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Ppcn_Nbr() { return iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Optn_Cde() { return iaa_Cntrct_Trans_Cntrct_Optn_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Orgn_Cde() { return iaa_Cntrct_Trans_Cntrct_Orgn_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Acctng_Cde() { return iaa_Cntrct_Trans_Cntrct_Acctng_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Issue_Dte() { return iaa_Cntrct_Trans_Cntrct_Issue_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Crrncy_Cde() { return iaa_Cntrct_Trans_Cntrct_Crrncy_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Type_Cde() { return iaa_Cntrct_Trans_Cntrct_Type_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Pymnt_Mthd() { return iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde() { return iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind() { return iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr() { return iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde() { return iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte() { return iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn() { return iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Div_Payee_Cde() { return iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Div_Coll_Cde() { return iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde() { return iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde; }

    public DbsField getIaa_Cntrct_Trans_Trans_Check_Dte() { return iaa_Cntrct_Trans_Trans_Check_Dte; }

    public DbsField getIaa_Cntrct_Trans_Bfre_Imge_Id() { return iaa_Cntrct_Trans_Bfre_Imge_Id; }

    public DbsField getIaa_Cntrct_Trans_Aftr_Imge_Id() { return iaa_Cntrct_Trans_Aftr_Imge_Id; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Type() { return iaa_Cntrct_Trans_Cntrct_Type; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re() { return iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re; }

    public DbsGroup getIaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup() { return iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte() { return iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Mtch_Ppcn() { return iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte() { return iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd() { return iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd() { return iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd() { return iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd; }

    public DbsField getIaa_Cntrct_Trans_Cntrct_Ssnng_Dte() { return iaa_Cntrct_Trans_Cntrct_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte() { return iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte; }

    public DbsField getIaa_Cntrct_Trans_Roth_Ssnng_Dte() { return iaa_Cntrct_Trans_Roth_Ssnng_Dte; }

    public DbsField getIaa_Cntrct_Trans_Plan_Nmbr() { return iaa_Cntrct_Trans_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Trans_Tax_Exmpt_Ind() { return iaa_Cntrct_Trans_Tax_Exmpt_Ind; }

    public DbsField getIaa_Cntrct_Trans_Orig_Ownr_Dob() { return iaa_Cntrct_Trans_Orig_Ownr_Dob; }

    public DbsField getIaa_Cntrct_Trans_Orig_Ownr_Dod() { return iaa_Cntrct_Trans_Orig_Ownr_Dod; }

    public DbsField getIaa_Cntrct_Trans_Sub_Plan_Nmbr() { return iaa_Cntrct_Trans_Sub_Plan_Nmbr; }

    public DbsField getIaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr() { return iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr; }

    public DbsField getIaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr() { return iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr; }

    public DataAccessProgramView getVw_iaa_Cpr_Trans() { return vw_iaa_Cpr_Trans; }

    public DbsField getIaa_Cpr_Trans_Trans_Dte() { return iaa_Cpr_Trans_Trans_Dte; }

    public DbsField getIaa_Cpr_Trans_Invrse_Trans_Dte() { return iaa_Cpr_Trans_Invrse_Trans_Dte; }

    public DbsField getIaa_Cpr_Trans_Lst_Trans_Dte() { return iaa_Cpr_Trans_Lst_Trans_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr() { return iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Part_Payee_Cde() { return iaa_Cpr_Trans_Cntrct_Part_Payee_Cde; }

    public DbsField getIaa_Cpr_Trans_Cpr_Id_Nbr() { return iaa_Cpr_Trans_Cpr_Id_Nbr; }

    public DbsField getIaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde() { return iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde; }

    public DbsField getIaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde() { return iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde; }

    public DbsField getIaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw() { return iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw; }

    public DbsField getIaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr() { return iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr; }

    public DbsField getIaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ() { return iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Actvty_Cde() { return iaa_Cpr_Trans_Cntrct_Actvty_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Trmnte_Rsn() { return iaa_Cpr_Trans_Cntrct_Trmnte_Rsn; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Rwrttn_Ind() { return iaa_Cpr_Trans_Cntrct_Rwrttn_Ind; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Cash_Cde() { return iaa_Cpr_Trans_Cntrct_Cash_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde() { return iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde; }

    public DbsGroup getIaa_Cpr_Trans_Cntrct_Company_Data() { return iaa_Cpr_Trans_Cntrct_Company_Data; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Company_Cd() { return iaa_Cpr_Trans_Cntrct_Company_Cd; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind() { return iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Per_Ivc_Amt() { return iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt() { return iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Ivc_Amt() { return iaa_Cpr_Trans_Cntrct_Ivc_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Ivc_Used_Amt() { return iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Rtb_Amt() { return iaa_Cpr_Trans_Cntrct_Rtb_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Rtb_Percent() { return iaa_Cpr_Trans_Cntrct_Rtb_Percent; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Mode_Ind() { return iaa_Cpr_Trans_Cntrct_Mode_Ind; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Wthdrwl_Dte() { return iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte() { return iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Final_Pay_Dte() { return iaa_Cpr_Trans_Cntrct_Final_Pay_Dte; }

    public DbsField getIaa_Cpr_Trans_Bnfcry_Xref_Ind() { return iaa_Cpr_Trans_Bnfcry_Xref_Ind; }

    public DbsField getIaa_Cpr_Trans_Bnfcry_Dod_Dte() { return iaa_Cpr_Trans_Bnfcry_Dod_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Pend_Cde() { return iaa_Cpr_Trans_Cntrct_Pend_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Hold_Cde() { return iaa_Cpr_Trans_Cntrct_Hold_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Pend_Dte() { return iaa_Cpr_Trans_Cntrct_Pend_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Prev_Dist_Cde() { return iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Curr_Dist_Cde() { return iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Cmbne_Cde() { return iaa_Cpr_Trans_Cntrct_Cmbne_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Spirt_Cde() { return iaa_Cpr_Trans_Cntrct_Spirt_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Spirt_Amt() { return iaa_Cpr_Trans_Cntrct_Spirt_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Spirt_Srce() { return iaa_Cpr_Trans_Cntrct_Spirt_Srce; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte() { return iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte() { return iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Fed_Tax_Amt() { return iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_State_Cde() { return iaa_Cpr_Trans_Cntrct_State_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_State_Tax_Amt() { return iaa_Cpr_Trans_Cntrct_State_Tax_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Local_Cde() { return iaa_Cpr_Trans_Cntrct_Local_Cde; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Local_Tax_Amt() { return iaa_Cpr_Trans_Cntrct_Local_Tax_Amt; }

    public DbsField getIaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte() { return iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte; }

    public DbsField getIaa_Cpr_Trans_Trans_Check_Dte() { return iaa_Cpr_Trans_Trans_Check_Dte; }

    public DbsField getIaa_Cpr_Trans_Bfre_Imge_Id() { return iaa_Cpr_Trans_Bfre_Imge_Id; }

    public DbsField getIaa_Cpr_Trans_Aftr_Imge_Id() { return iaa_Cpr_Trans_Aftr_Imge_Id; }

    public DbsField getIaa_Cpr_Trans_Cpr_Xfr_Term_Cde() { return iaa_Cpr_Trans_Cpr_Xfr_Term_Cde; }

    public DbsField getIaa_Cpr_Trans_Cpr_Lgl_Res_Cde() { return iaa_Cpr_Trans_Cpr_Lgl_Res_Cde; }

    public DbsField getIaa_Cpr_Trans_Cpr_Xfr_Iss_Dte() { return iaa_Cpr_Trans_Cpr_Xfr_Iss_Dte; }

    public DbsField getIaa_Cpr_Trans_Rllvr_Cntrct_Nbr() { return iaa_Cpr_Trans_Rllvr_Cntrct_Nbr; }

    public DbsField getIaa_Cpr_Trans_Rllvr_Ivc_Ind() { return iaa_Cpr_Trans_Rllvr_Ivc_Ind; }

    public DbsField getIaa_Cpr_Trans_Rllvr_Elgble_Ind() { return iaa_Cpr_Trans_Rllvr_Elgble_Ind; }

    public DbsGroup getIaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_CdeMuGroup() { return iaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_CdeMuGroup; }

    public DbsField getIaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_Cde() { return iaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_Cde; }

    public DbsField getIaa_Cpr_Trans_Rllvr_Accptng_Irc_Cde() { return iaa_Cpr_Trans_Rllvr_Accptng_Irc_Cde; }

    public DbsField getIaa_Cpr_Trans_Rllvr_Pln_Admn_Ind() { return iaa_Cpr_Trans_Rllvr_Pln_Admn_Ind; }

    public DbsField getIaa_Cpr_Trans_Roth_Dsblty_Dte() { return iaa_Cpr_Trans_Roth_Dsblty_Dte; }

    public DataAccessProgramView getVw_iaa_Tiaa_Fund_Trans() { return vw_iaa_Tiaa_Fund_Trans; }

    public DbsField getIaa_Tiaa_Fund_Trans_Trans_Dte() { return iaa_Tiaa_Fund_Trans_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Invrse_Trans_Dte() { return iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Lst_Trans_Dte() { return iaa_Tiaa_Fund_Trans_Lst_Trans_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr() { return iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde() { return iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde() { return iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef4() { return iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef4; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde() { return iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde() { return iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp; }

    public DbsGroup getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt() { return iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt; }

    public DbsField getIaa_Tiaa_Fund_Trans_Trans_Check_Dte() { return iaa_Tiaa_Fund_Trans_Trans_Check_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Bfre_Imge_Id() { return iaa_Tiaa_Fund_Trans_Bfre_Imge_Id; }

    public DbsField getIaa_Tiaa_Fund_Trans_Aftr_Imge_Id() { return iaa_Tiaa_Fund_Trans_Aftr_Imge_Id; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Xfr_Iss_Dte() { return iaa_Tiaa_Fund_Trans_Tiaa_Xfr_Iss_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_In_Dte() { return iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_In_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_Out_Dte() { return iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_Out_Dte; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tckr_Symbl() { return iaa_Tiaa_Fund_Trans_Tckr_Symbl; }

    public DbsGroup getIaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic() { return iaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind() { return iaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind; }

    public DbsField getIaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund() { return iaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund; }

    public DataAccessProgramView getVw_iaa_Cref_Fund_Trans_1() { return vw_iaa_Cref_Fund_Trans_1; }

    public DbsField getIaa_Cref_Fund_Trans_1_Trans_Dte() { return iaa_Cref_Fund_Trans_1_Trans_Dte; }

    public DbsField getIaa_Cref_Fund_Trans_1_Invrse_Trans_Dte() { return iaa_Cref_Fund_Trans_1_Invrse_Trans_Dte; }

    public DbsField getIaa_Cref_Fund_Trans_1_Lst_Trans_Dte() { return iaa_Cref_Fund_Trans_1_Lst_Trans_Dte; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Cntrct_Ppcn_Nbr() { return iaa_Cref_Fund_Trans_1_Cref_Cntrct_Ppcn_Nbr; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Cntrct_Payee_Cde() { return iaa_Cref_Fund_Trans_1_Cref_Cntrct_Payee_Cde; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_Cde() { return iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_Cde; }

    public DbsGroup getIaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_CdeRedef5() { return iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_CdeRedef5; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Cmpny_Cde() { return iaa_Cref_Fund_Trans_1_Cref_Cmpny_Cde; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Fund_Cde() { return iaa_Cref_Fund_Trans_1_Cref_Fund_Cde; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Tot_Per_Amt() { return iaa_Cref_Fund_Trans_1_Cref_Tot_Per_Amt; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Unit_Val() { return iaa_Cref_Fund_Trans_1_Cref_Unit_Val; }

    public DbsField getIaa_Cref_Fund_Trans_1_Count_Castcref_Rate_Data_Grp() { return iaa_Cref_Fund_Trans_1_Count_Castcref_Rate_Data_Grp; }

    public DbsGroup getIaa_Cref_Fund_Trans_1_Cref_Rate_Data_Grp() { return iaa_Cref_Fund_Trans_1_Cref_Rate_Data_Grp; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Rate_Cde() { return iaa_Cref_Fund_Trans_1_Cref_Rate_Cde; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Rate_Dte() { return iaa_Cref_Fund_Trans_1_Cref_Rate_Dte; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Units_Cnt() { return iaa_Cref_Fund_Trans_1_Cref_Units_Cnt; }

    public DbsField getIaa_Cref_Fund_Trans_1_Trans_Check_Dte() { return iaa_Cref_Fund_Trans_1_Trans_Check_Dte; }

    public DbsField getIaa_Cref_Fund_Trans_1_Bfre_Imge_Id() { return iaa_Cref_Fund_Trans_1_Bfre_Imge_Id; }

    public DbsField getIaa_Cref_Fund_Trans_1_Aftr_Imge_Id() { return iaa_Cref_Fund_Trans_1_Aftr_Imge_Id; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Xfr_Iss_Dte() { return iaa_Cref_Fund_Trans_1_Cref_Xfr_Iss_Dte; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Lst_Xfr_In_Dte() { return iaa_Cref_Fund_Trans_1_Cref_Lst_Xfr_In_Dte; }

    public DbsField getIaa_Cref_Fund_Trans_1_Cref_Lst_Xfr_Out_Dte() { return iaa_Cref_Fund_Trans_1_Cref_Lst_Xfr_Out_Dte; }

    public DbsField getIaa_Cref_Fund_Trans_1_Tckr_Symbl() { return iaa_Cref_Fund_Trans_1_Tckr_Symbl; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_ads_Ia_Rslt = new DataAccessProgramView(new NameInfo("vw_ads_Ia_Rslt", "ADS-IA-RSLT"), "ADS_IA_RSLT", "ADS_IA_RSLT", DdmPeriodicGroups.getInstance().getGroups("ADS_IA_RSLT"));
        ads_Ia_Rslt_Rqst_Id = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "RQST_ID");
        ads_Ia_Rslt_Adi_Ia_Rcrd_Cde = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Ia_Rcrd_Cde", "ADI-IA-RCRD-CDE", FieldType.STRING, 3, 
            RepeatingFieldStrategy.None, "ADI_IA_RCRD_CDE");
        ads_Ia_Rslt_Adi_Sqnce_Nbr = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Sqnce_Nbr", "ADI-SQNCE-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "ADI_SQNCE_NBR");
        ads_Ia_Rslt_Adi_Stts_Cde = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Stts_Cde", "ADI-STTS-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "ADI_STTS_CDE");
        ads_Ia_Rslt_Adi_Tiaa_NbrsMuGroup = vw_ads_Ia_Rslt.getRecord().newGroupInGroup("ads_Ia_Rslt_Adi_Tiaa_NbrsMuGroup", "ADI_TIAA_NBRSMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "ADS_IA_RSLT_ADI_TIAA_NBRS");
        ads_Ia_Rslt_Adi_Tiaa_Nbrs = ads_Ia_Rslt_Adi_Tiaa_NbrsMuGroup.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Tiaa_Nbrs", "ADI-TIAA-NBRS", FieldType.STRING, 
            10, new DbsArrayController(1,12), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_TIAA_NBRS");
        ads_Ia_Rslt_Adi_Cref_NbrsMuGroup = vw_ads_Ia_Rslt.getRecord().newGroupInGroup("ads_Ia_Rslt_Adi_Cref_NbrsMuGroup", "ADI_CREF_NBRSMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "ADS_IA_RSLT_ADI_CREF_NBRS");
        ads_Ia_Rslt_Adi_Cref_Nbrs = ads_Ia_Rslt_Adi_Cref_NbrsMuGroup.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Cref_Nbrs", "ADI-CREF-NBRS", FieldType.STRING, 
            10, new DbsArrayController(1,12), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_CREF_NBRS");
        ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr", "ADI-IA-TIAA-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "ADI_IA_TIAA_NBR");
        ads_Ia_Rslt_Adi_Ia_Tiaa_Payee_Cde = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Ia_Tiaa_Payee_Cde", "ADI-IA-TIAA-PAYEE-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADI_IA_TIAA_PAYEE_CDE");
        ads_Ia_Rslt_Adi_Ia_Cref_Nbr = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Ia_Cref_Nbr", "ADI-IA-CREF-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "ADI_IA_CREF_NBR");
        ads_Ia_Rslt_Adi_Ia_Cref_Payee_Cde = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Ia_Cref_Payee_Cde", "ADI-IA-CREF-PAYEE-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADI_IA_CREF_PAYEE_CDE");
        ads_Ia_Rslt_Adi_Curncy_Cde = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Curncy_Cde", "ADI-CURNCY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ADI_CURNCY_CDE");
        ads_Ia_Rslt_Adi_Pymnt_Cde = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Pymnt_Cde", "ADI-PYMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ADI_PYMNT_CDE");
        ads_Ia_Rslt_Adi_Pymnt_Mode = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Pymnt_Mode", "ADI-PYMNT-MODE", FieldType.NUMERIC, 3, 
            RepeatingFieldStrategy.None, "ADI_PYMNT_MODE");
        ads_Ia_Rslt_Adi_Lst_Actvty_Dte = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Lst_Actvty_Dte", "ADI-LST-ACTVTY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_LST_ACTVTY_DTE");
        ads_Ia_Rslt_Adi_Annty_Strt_Dte = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Annty_Strt_Dte", "ADI-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_ANNTY_STRT_DTE");
        ads_Ia_Rslt_Adi_Instllmnt_Dte = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Instllmnt_Dte", "ADI-INSTLLMNT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_INSTLLMNT_DTE");
        ads_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte", "ADI-FRST-PYMNT-DUE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADI_FRST_PYMNT_DUE_DTE");
        ads_Ia_Rslt_Adi_Frst_Ck_Pd_Dte = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Frst_Ck_Pd_Dte", "ADI-FRST-CK-PD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_FRST_CK_PD_DTE");
        ads_Ia_Rslt_Adi_Finl_Periodic_Py_Dte = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Finl_Periodic_Py_Dte", "ADI-FINL-PERIODIC-PY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADI_FINL_PERIODIC_PY_DTE");
        ads_Ia_Rslt_Adi_Finl_Prtl_Py_Dte = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Finl_Prtl_Py_Dte", "ADI-FINL-PRTL-PY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_FINL_PRTL_PY_DTE");
        ads_Ia_Rslt_Adi_Ivc_Data = vw_ads_Ia_Rslt.getRecord().newGroupInGroup("ads_Ia_Rslt_Adi_Ivc_Data", "ADI-IVC-DATA");
        ads_Ia_Rslt_Adi_Tiaa_Ivc_Amt = ads_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rslt_Adi_Tiaa_Ivc_Amt", "ADI-TIAA-IVC-AMT", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "ADI_TIAA_IVC_AMT");
        ads_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind = ads_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind", "ADI-TIAA-IVC-GD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_TIAA_IVC_GD_IND");
        ads_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt = ads_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt", "ADI-TIAA-PRDC-IVC-AMT", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "ADI_TIAA_PRDC_IVC_AMT");
        ads_Ia_Rslt_Adi_Cref_Ivc_Amt = ads_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rslt_Adi_Cref_Ivc_Amt", "ADI-CREF-IVC-AMT", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "ADI_CREF_IVC_AMT");
        ads_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind = ads_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind", "ADI-CREF-IVC-GD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_CREF_IVC_GD_IND");
        ads_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt = ads_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt", "ADI-CREF-PRDC-IVC-AMT", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "ADI_CREF_PRDC_IVC_AMT");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Data = vw_ads_Ia_Rslt.getRecord().newGroupInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Data", "ADI-DTL-TIAA-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Rate_Cd = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Rate_Cd", "ADI-DTL-TIAA-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1,125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Std_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Std_Amt", "ADI-DTL-TIAA-DA-STD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1,125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Grd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Grd_Amt", "ADI-DTL-TIAA-DA-GRD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1,125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Acct_Cd = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Acct_Cd", "ADI-DTL-TIAA-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1,125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_ACCT_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd", "ADI-DTL-TIAA-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1,125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_IA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Eff_Rte_Intrst = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Eff_Rte_Intrst", "ADI-DTL-TIAA-EFF-RTE-INTRST", 
            FieldType.NUMERIC, 5, 3, new DbsArrayController(1,125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_EFF_RTE_INTRST", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt", "ADI-DTL-TIAA-GRD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1,125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt", "ADI-DTL-TIAA-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1,125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt", "ADI-DTL-FNL-GRD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1,125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt", "ADI-DTL-FNL-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1,125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt", "ADI-DTL-TIAA-STD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1,125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt", "ADI-DTL-TIAA-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1,125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt", "ADI-DTL-FNL-STD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1,125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt", "ADI-DTL-FNL-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1,125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Data = vw_ads_Ia_Rslt.getRecord().newGroupInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Data", "ADI-DTL-CREF-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Da_Rate_Cd = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Da_Rate_Cd", "ADI-DTL-CREF-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_DA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd", "ADI-DTL-CREF-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_ACCT_CD", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd", "ADI-DTL-CREF-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_IA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt", "ADI-DTL-CREF-DA-ANNL-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_DA_ANNL_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units", "ADI-DTL-CREF-ANNL-NBR-UNITS", 
            FieldType.NUMERIC, 10, 4, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_ANNL_NBR_UNITS", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val", "ADI-DTL-CREF-IA-ANNL-UNIT-VAL", 
            FieldType.NUMERIC, 7, 4, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_IA_ANNL_UNIT_VAL", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Amt = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Amt", "ADI-DTL-CREF-ANNL-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_ANNL_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt", "ADI-DTL-CREF-DA-MNTHLY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_DA_MNTHLY_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units", "ADI-DTL-CREF-MNTHLY-NBR-UNITS", 
            FieldType.NUMERIC, 10, 4, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_MNTHLY_NBR_UNITS", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val", 
            "ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL", FieldType.NUMERIC, 7, 4, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_IA_MNTHLY_UNIT_VAL", 
            "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Amt = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Amt", "ADI-DTL-CREF-MNTHLY-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_MNTHLY_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Optn_Cde = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Optn_Cde", "ADI-OPTN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "ADI_OPTN_CDE");
        ads_Ia_Rslt_Adi_Tiaa_Sttlmnt = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Tiaa_Sttlmnt", "ADI-TIAA-STTLMNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_TIAA_STTLMNT");
        ads_Ia_Rslt_Adi_Tiaa_Re_Sttlmnt = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Tiaa_Re_Sttlmnt", "ADI-TIAA-RE-STTLMNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_TIAA_RE_STTLMNT");
        ads_Ia_Rslt_Adi_Cref_Sttlmnt = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Cref_Sttlmnt", "ADI-CREF-STTLMNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_CREF_STTLMNT");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat = vw_ads_Ia_Rslt.getRecord().newGroupInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat", "ADI-DTL-TIAA-TPA-GUAR-COMMUT-DAT", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt", 
            "ADI-DTL-TIAA-GUAR-COMMUT-STD-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1,125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_TIAA_GUAR_COMMUT_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt", 
            "ADI-DTL-TIAA-GUAR-COMMUT-GRD-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1,125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_TIAA_GUAR_COMMUT_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        ads_Ia_Rslt_Adi_Orig_Lob_IndMuGroup = vw_ads_Ia_Rslt.getRecord().newGroupInGroup("ads_Ia_Rslt_Adi_Orig_Lob_IndMuGroup", "ADI_ORIG_LOB_INDMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_IA_RSLT_ADI_ORIG_LOB_IND");
        ads_Ia_Rslt_Adi_Orig_Lob_Ind = ads_Ia_Rslt_Adi_Orig_Lob_IndMuGroup.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Orig_Lob_Ind", "ADI-ORIG-LOB-IND", FieldType.STRING, 
            1, new DbsArrayController(1,12), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_ORIG_LOB_IND");
        ads_Ia_Rslt_Adi_Roth_Rqst_Ind = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Roth_Rqst_Ind", "ADI-ROTH-RQST-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_ROTH_RQST_IND");
        ads_Ia_Rslt_Adi_Srvvr_Ind = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Srvvr_Ind", "ADI-SRVVR-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ADI_SRVVR_IND");
        ads_Ia_Rslt_Adi_Contract_IdMuGroup = vw_ads_Ia_Rslt.getRecord().newGroupInGroup("ads_Ia_Rslt_Adi_Contract_IdMuGroup", "ADI_CONTRACT_IDMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_IA_RSLT_ADI_CONTRACT_ID");
        ads_Ia_Rslt_Adi_Contract_Id = ads_Ia_Rslt_Adi_Contract_IdMuGroup.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Contract_Id", "ADI-CONTRACT-ID", FieldType.NUMERIC, 
            11, new DbsArrayController(1,125), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_CONTRACT_ID");

        vw_iaa_Cntrct = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct", "IAA-CNTRCT"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Cntrct_Optn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Cntrct_Orgn_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Cntrct_Acctng_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Cntrct_Issue_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Cntrct_Crrncy_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Cntrct_Type_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind", "CNTRCT-JOINT-CNVRT-RCRD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte", "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Lst_Trans_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cntrct_Cntrct_Type = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE");
        iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re", "CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct.getRecord().newGroupInGroup("iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Cntrct_Mtch_Ppcn = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Cntrct_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Cntrct_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        iaa_Cntrct_Cntrct_Ssnng_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Cntrct_Ssnng_Dte", "CNTRCT-SSNNG-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNTRCT_SSNNG_DTE");
        iaa_Cntrct_Roth_Frst_Cntrb_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Roth_Frst_Cntrb_Dte", "ROTH-FRST-CNTRB-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ROTH_FRST_CNTRB_DTE");
        iaa_Cntrct_Roth_Ssnng_Dte = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Roth_Ssnng_Dte", "ROTH-SSNNG-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ROTH_SSNNG_DTE");
        iaa_Cntrct_Plan_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Plan_Nmbr", "PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PLAN_NMBR");
        iaa_Cntrct_Tax_Exmpt_Ind = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Tax_Exmpt_Ind", "TAX-EXMPT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "TAX_EXMPT_IND");
        iaa_Cntrct_Orig_Ownr_Dob = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Orig_Ownr_Dob", "ORIG-OWNR-DOB", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ORIG_OWNR_DOB");
        iaa_Cntrct_Orig_Ownr_Dod = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Orig_Ownr_Dod", "ORIG-OWNR-DOD", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ORIG_OWNR_DOD");
        iaa_Cntrct_Sub_Plan_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Sub_Plan_Nmbr", "SUB-PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SUB_PLAN_NMBR");
        iaa_Cntrct_Orgntng_Sub_Plan_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Orgntng_Sub_Plan_Nmbr", "ORGNTNG-SUB-PLAN-NMBR", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "ORGNTNG_SUB_PLAN_NMBR");
        iaa_Cntrct_Orgntng_Cntrct_Nmbr = vw_iaa_Cntrct.getRecord().newFieldInGroup("iaa_Cntrct_Orgntng_Cntrct_Nmbr", "ORGNTNG-CNTRCT-NMBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "ORGNTNG_CNTRCT_NMBR");

        vw_iaa_Cntrct_Prtcpnt_Role = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Prtcpnt_Role", "IAA-CNTRCT-PRTCPNT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", 
            "IA_CONTRACT_PART", DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRCT_PRTCPNT_ROLE"));
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr", 
            "CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde", 
            "CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr", "CPR-ID-NBR", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "CPR_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde", 
            "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde", 
            "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Sw", 
            "PRTCPNT-RSDNCY-SW", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr", 
            "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Typ", 
            "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde", 
            "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn", 
            "CNTRCT-TRMNTE-RSN", FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rwrttn_Ind", 
            "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde", 
            "CNTRCT-EMPLYMNT-TRMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data", 
            "CNTRCT-COMPANY-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd", 
            "CNTRCT-COMPANY-CD", FieldType.STRING, 1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind", 
            "CNTRCT-RCVRY-TYPE-IND", FieldType.NUMERIC, 1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt", 
            "CNTRCT-PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt", 
            "CNTRCT-RESDL-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt", 
            "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt", 
            "CNTRCT-IVC-USED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt", 
            "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent = iaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent", 
            "CNTRCT-RTB-PERCENT", FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", 
            "IA_CONTRACT_PART_CNTRCT_COMPANY_DATA");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Wthdrwl_Dte", 
            "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte", 
            "CNTRCT-FINAL-PER-PAY-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte", 
            "CNTRCT-FINAL-PAY-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde", 
            "CNTRCT-PREV-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde", 
            "CNTRCT-CURR-DIST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Cmbne_Cde", 
            "CNTRCT-CMBNE-CDE", FieldType.STRING, 12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Cde", 
            "CNTRCT-SPIRT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Amt", 
            "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Srce", 
            "CNTRCT-SPIRT-SRCE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Arr_Dte", 
            "CNTRCT-SPIRT-ARR-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Spirt_Prcss_Dte", 
            "CNTRCT-SPIRT-PRCSS-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Fed_Tax_Amt", 
            "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Cde", 
            "CNTRCT-STATE-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_State_Tax_Amt", 
            "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde", 
            "CNTRCT-LOCAL-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Tax_Amt", 
            "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cntrct_Lst_Chnge_Dte", 
            "CNTRCT-LST-CHNGE-DTE", FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Term_Cde", 
            "CPR-XFR-TERM-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CPR_XFR_TERM_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CPR_LGL_RES_CDE");
        iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CPR_XFR_ISS_DTE");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Cntrct_Nbr", 
            "RLLVR-CNTRCT-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "RLLVR_CNTRCT_NBR");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind", "RLLVR-IVC-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_IVC_IND");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind", 
            "RLLVR-ELGBLE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_ELGBLE_IND");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newGroupInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup", 
            "RLLVR_DSTRBTNG_IRC_CDEMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "IA_CONTRACT_PART_RLLVR_DSTRBTNG_IRC_CDE");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde = iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_CdeMuGroup.newFieldArrayInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde", 
            "RLLVR-DSTRBTNG-IRC-CDE", FieldType.STRING, 2, new DbsArrayController(1,4), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RLLVR_DSTRBTNG_IRC_CDE");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde", 
            "RLLVR-ACCPTNG-IRC-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "RLLVR_ACCPTNG_IRC_CDE");
        iaa_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind", 
            "RLLVR-PLN-ADMN-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RLLVR_PLN_ADMN_IND");
        iaa_Cntrct_Prtcpnt_Role_Roth_Dsblty_Dte = vw_iaa_Cntrct_Prtcpnt_Role.getRecord().newFieldInGroup("iaa_Cntrct_Prtcpnt_Role_Roth_Dsblty_Dte", "ROTH-DSBLTY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ROTH_DSBLTY_DTE");

        vw_iaa_Tiaa_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Rcrd", "IAA-TIAA-FUND-RCRD"), "IAA_TIAA_FUND_RCRD", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_RCRD"));
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1 = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1", 
            "Redefines", iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", 
            FieldType.STRING, 1);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_CdeRedef1.newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde", "TIAA-FUND-CDE", 
            FieldType.STRING, 2);
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Ivc_Amt", "TIAA-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_PER_IVC_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rtb_Amt", "TIAA-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "TIAA_RTB_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt", "TIAA-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt", "TIAA-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Units_Cnt", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Xfr_Iss_Dte", "TIAA-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_In_Dte", "TIAA-LST-XFR-IN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_IN_DTE");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Lst_Xfr_Out_Dte", "TIAA-LST-XFR-OUT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        iaa_Tiaa_Fund_Rcrd_Tckr_Symbl = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tckr_Symbl", "TCKR-SYMBL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TCKR_SYMBL");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup", "TIAA_RATE_GICMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic = iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic", "TIAA-RATE-GIC", 
            FieldType.NUMERIC, 11, new DbsArrayController(1,250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind", "TIAA-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund = vw_iaa_Tiaa_Fund_Rcrd.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Rcrd_Tiaa_Old_Cmpny_Fund", "TIAA-OLD-CMPNY-FUND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CREF_OLD_CMPNY_FUND");

        vw_iaa_Cref_Fund_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cref_Fund_Rcrd_1", "IAA-CREF-FUND-RCRD-1"), "IAA_CREF_FUND_RCRD_1", "IA_MULTI_FUNDS", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CREF_FUND_RCRD_1"));
        iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr", "CREF-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Payee_Cde = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Payee_Cde", 
            "CREF-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_Cde = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_Cde", "CREF-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_CdeRedef2 = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newGroupInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_CdeRedef2", 
            "Redefines", iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_Cde);
        iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde = iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_CdeRedef2.newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde", "CREF-CMPNY-CDE", 
            FieldType.STRING, 1);
        iaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde = iaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_CdeRedef2.newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde", "CREF-FUND-CDE", 
            FieldType.STRING, 2);
        iaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt", "CREF-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Cref_Fund_Rcrd_1_Cref_Unit_Val = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Unit_Val", "CREF-UNIT-VAL", 
            FieldType.PACKED_DECIMAL, 9, 4, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        iaa_Cref_Fund_Rcrd_1_Cref_Old_Per_Amt = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Old_Per_Amt", "CREF-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "AJ");
        iaa_Cref_Fund_Rcrd_1_Cref_Old_Unit_Val = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Old_Unit_Val", "CREF-OLD-UNIT-VAL", 
            FieldType.PACKED_DECIMAL, 9, 4, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        iaa_Cref_Fund_Rcrd_1_Count_Castcref_Rate_Data_Grp = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Count_Castcref_Rate_Data_Grp", 
            "C*CREF-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_CREF_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_Cref_Rate_Data_Grp = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newGroupInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Rate_Data_Grp", "CREF-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde = iaa_Cref_Fund_Rcrd_1_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde", "CREF-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_Cref_Rate_Dte = iaa_Cref_Fund_Rcrd_1_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Rate_Dte", "CREF-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt = iaa_Cref_Fund_Rcrd_1_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt", "CREF-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Rcrd_1_Lst_Trans_Dte = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cref_Fund_Rcrd_1_Cref_Xfr_Iss_Dte = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Xfr_Iss_Dte", "CREF-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Cref_Fund_Rcrd_1_Cref_Lst_Xfr_In_Dte = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Lst_Xfr_In_Dte", "CREF-LST-XFR-IN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_IN_DTE");
        iaa_Cref_Fund_Rcrd_1_Cref_Lst_Xfr_Out_Dte = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Cref_Lst_Xfr_Out_Dte", "CREF-LST-XFR-OUT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        iaa_Cref_Fund_Rcrd_1_Tckr_Symbl = vw_iaa_Cref_Fund_Rcrd_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Rcrd_1_Tckr_Symbl", "TCKR-SYMBL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TCKR_SYMBL");

        vw_iaa_Trans_Rcrd = new DataAccessProgramView(new NameInfo("vw_iaa_Trans_Rcrd", "IAA-TRANS-RCRD"), "IAA_TRANS_RCRD", "IA_TRANS_FILE");
        iaa_Trans_Rcrd_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Trans_Rcrd_Invrse_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Trans_Rcrd_Lst_Trans_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Trans_Rcrd_Trans_Ppcn_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Ppcn_Nbr", "TRANS-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TRANS_PPCN_NBR");
        iaa_Trans_Rcrd_Trans_Payee_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Payee_Cde", "TRANS-PAYEE-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "TRANS_PAYEE_CDE");
        iaa_Trans_Rcrd_Trans_Sub_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Sub_Cde", "TRANS-SUB-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "TRANS_SUB_CDE");
        iaa_Trans_Rcrd_Trans_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cde", "TRANS-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "TRANS_CDE");
        iaa_Trans_Rcrd_Trans_Actvty_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Actvty_Cde", "TRANS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_ACTVTY_CDE");
        iaa_Trans_Rcrd_Trans_Check_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Trans_Rcrd_Trans_Todays_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Todays_Dte", "TRANS-TODAYS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_TODAYS_DTE");
        iaa_Trans_Rcrd_Trans_User_Area = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Area", "TRANS-USER-AREA", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Trans_Rcrd_Trans_User_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Trans_Rcrd_Trans_Verify_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Cde", "TRANS-VERIFY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TRANS_VERIFY_CDE");
        iaa_Trans_Rcrd_Trans_Verify_Id = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Verify_Id", "TRANS-VERIFY-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Trans_Rcrd_Trans_Cmbne_Cde = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cmbne_Cde", "TRANS-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "TRANS_CMBNE_CDE");
        iaa_Trans_Rcrd_Trans_Cwf_Wpid = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Wpid", "TRANS-CWF-WPID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "TRANS_CWF_WPID");
        iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Cwf_Id_Nbr", "TRANS-CWF-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "TRANS_CWF_ID_NBR");
        iaa_Trans_Rcrd_Trans_Effective_Dte = vw_iaa_Trans_Rcrd.getRecord().newFieldInGroup("iaa_Trans_Rcrd_Trans_Effective_Dte", "TRANS-EFFECTIVE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TRANS_EFFECTIVE_DTE");

        vw_iaa_Cntrl_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1", "IAA-CNTRL-RCRD-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_CNTRL_RCRD_1"));
        iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Invrse_Dte", "CNTRL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRL_INVRSE_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Frst_Trans_Dte", "CNTRL-FRST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "CNTRL_FRST_TRANS_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRL_ACTVTY_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Tiaa_Payees", "CNTRL-TIAA-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_TIAA_FUND_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Per_Pay_Amt", "CNTRL-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_PAY_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Per_Div_Amt", "CNTRL-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_PER_DIV_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Units_Cnt", "CNTRL-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            13, 3, RepeatingFieldStrategy.None, "CNTRL_UNITS_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Final_Pay_Amt", "CNTRL-FINAL-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_PAY_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Final_Div_Amt", "CNTRL-FINAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, "CNTRL_FINAL_DIV_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Todays_Dte", "CNTRL-TODAYS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_TODAYS_DTE");
        iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Count_Castcntrl_Fund_Cnts", "C*CNTRL-FUND-CNTS", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts = vw_iaa_Cntrl_Rcrd_1.getRecord().newGroupInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts", "CNTRL-FUND-CNTS", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde", "CNTRL-FUND-CDE", FieldType.STRING, 
            3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_CDE", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Fund_CdeRedef3 = vw_iaa_Cntrl_Rcrd_1.getRecord().newGroupInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_CdeRedef3", "Redefines", 
            iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde);
        iaa_Cntrl_Rcrd_1_Cf_Cde = iaa_Cntrl_Rcrd_1_Cntrl_Fund_CdeRedef3.newGroupArrayInGroup("iaa_Cntrl_Rcrd_1_Cf_Cde", "CF-CDE", new DbsArrayController(1,
            20));
        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde", "CNTRL-FUND-CDE", FieldType.STRING, 
            3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_CDE", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cde", "CNTRL-FUND-CDE", FieldType.STRING, 
            3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_CDE", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Fund_Payees", "CNTRL-FUND-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_FUND_PAYEES", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Units = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Units", "CNTRL-UNITS", FieldType.PACKED_DECIMAL, 
            13, 3, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_UNITS", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Amt = iaa_Cntrl_Rcrd_1_Cntrl_Fund_Cnts.newFieldArrayInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Amt", "CNTRL-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, new DbsArrayController(1,20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRL_AMT", "IA_TRANS_FILE_CNTRL_FUND_CNTS");
        iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Cnt", "CNTRL-DDCTN-CNT", FieldType.PACKED_DECIMAL, 
            9, RepeatingFieldStrategy.None, "CNTRL_DDCTN_CNT");
        iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Ddctn_Amt", "CNTRL-DDCTN-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRL_DDCTN_AMT");
        iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actve_Tiaa_Pys", "CNTRL-ACTVE-TIAA-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_TIAA_PYS");
        iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actve_Cref_Pys", "CNTRL-ACTVE-CREF-PYS", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_ACTVE_CREF_PYS");
        iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Inactve_Payees", "CNTRL-INACTVE-PAYEES", 
            FieldType.PACKED_DECIMAL, 9, RepeatingFieldStrategy.None, "CNTRL_INACTVE_PAYEES");

        vw_iaa_Cntrct_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrct_Trans", "IAA-CNTRCT-TRANS"), "IAA_CNTRCT_TRANS", "IA_TRANS_FILE");
        iaa_Cntrct_Trans_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cntrct_Trans_Invrse_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cntrct_Trans_Lst_Trans_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Trans_Cntrct_Optn_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Trans_Cntrct_Orgn_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        iaa_Cntrct_Trans_Cntrct_Acctng_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Acctng_Cde", "CNTRCT-ACCTNG-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_ACCTNG_CDE");
        iaa_Cntrct_Trans_Cntrct_Issue_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Issue_Dte", "CNTRCT-ISSUE-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Pymnt_Due_Dte", "CNTRCT-FIRST-PYMNT-DUE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_DUE_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Pymnt_Pd_Dte", "CNTRCT-FIRST-PYMNT-PD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_PYMNT_PD_DTE");
        iaa_Cntrct_Trans_Cntrct_Crrncy_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        iaa_Cntrct_Trans_Cntrct_Type_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_TYPE_CDE");
        iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Pymnt_Mthd", "CNTRCT-PYMNT-MTHD", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_MTHD");
        iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Pnsn_Pln_Cde", "CNTRCT-PNSN-PLN-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PNSN_PLN_CDE");
        iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Joint_Cnvrt_Rcrd_Ind", 
            "CNTRCT-JOINT-CNVRT-RCRD-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_JOINT_CNVRT_RCRD_IND");
        iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Orig_Da_Cntrct_Nbr", "CNTRCT-ORIG-DA-CNTRCT-NBR", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNTRCT_ORIG_DA_CNTRCT_NBR");
        iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Issue_Cde", "CNTRCT-RSDNCY-AT-ISSUE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISSUE_CDE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Xref_Ind", "CNTRCT-FIRST-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_XREF_IND");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dob_Dte", "CNTRCT-FIRST-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Sex_Cde", "CNTRCT-FIRST-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_SEX_CDE");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Lfe_Cnt", "CNTRCT-FIRST-ANNT-LFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_LFE_CNT");
        iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_First_Annt_Dod_Dte", "CNTRCT-FIRST-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FIRST_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Xref_Ind", "CNTRCT-SCND-ANNT-XREF-IND", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_XREF_IND");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dob_Dte", "CNTRCT-SCND-ANNT-DOB-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOB_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte", 
            "CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE", FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_MRTLTY_YOB_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Sex_Cde", "CNTRCT-SCND-ANNT-SEX-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SEX_CDE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Dod_Dte", "CNTRCT-SCND-ANNT-DOD-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_DOD_DTE");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Life_Cnt", "CNTRCT-SCND-ANNT-LIFE-CNT", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_LIFE_CNT");
        iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Scnd_Annt_Ssn", "CNTRCT-SCND-ANNT-SSN", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CNTRCT_SCND_ANNT_SSN");
        iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Div_Payee_Cde", "CNTRCT-DIV-PAYEE-CDE", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "CNTRCT_DIV_PAYEE_CDE");
        iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Div_Coll_Cde", "CNTRCT-DIV-COLL-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_DIV_COLL_CDE");
        iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Inst_Iss_Cde", "CNTRCT-INST-ISS-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "CNTRCT_INST_ISS_CDE");
        iaa_Cntrct_Trans_Trans_Check_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cntrct_Trans_Bfre_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Cntrct_Trans_Aftr_Imge_Id = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Cntrct_Trans_Cntrct_Type = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Type", "CNTRCT-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_TYPE");
        iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Rsdncy_At_Iss_Re", "CNTRCT-RSDNCY-AT-ISS-RE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "CNTRCT_RSDNCY_AT_ISS_RE");
        iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup = vw_iaa_Cntrct_Trans.getRecord().newGroupInGroup("iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup", "CNTRCT_FNL_PRM_DTEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_TRANS_FILE_CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte = iaa_Cntrct_Trans_Cntrct_Fnl_Prm_DteMuGroup.newFieldArrayInGroup("iaa_Cntrct_Trans_Cntrct_Fnl_Prm_Dte", "CNTRCT-FNL-PRM-DTE", 
            FieldType.DATE, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "CNTRCT_FNL_PRM_DTE");
        iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Mtch_Ppcn", "CNTRCT-MTCH-PPCN", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_MTCH_PPCN");
        iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Annty_Strt_Dte", "CNTRCT-ANNTY-STRT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNTRCT_ANNTY_STRT_DTE");
        iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Issue_Dte_Dd", "CNTRCT-ISSUE-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_ISSUE_DTE_DD");
        iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Fp_Due_Dte_Dd", "CNTRCT-FP-DUE-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_FP_DUE_DTE_DD");
        iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Fp_Pd_Dte_Dd", "CNTRCT-FP-PD-DTE-DD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_FP_PD_DTE_DD");
        iaa_Cntrct_Trans_Cntrct_Ssnng_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Cntrct_Ssnng_Dte", "CNTRCT-SSNNG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_SSNNG_DTE");
        iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Roth_Frst_Cntrb_Dte", "ROTH-FRST-CNTRB-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ROTH_FRST_CNTRB_DTE");
        iaa_Cntrct_Trans_Roth_Ssnng_Dte = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Roth_Ssnng_Dte", "ROTH-SSNNG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ROTH_SSNNG_DTE");
        iaa_Cntrct_Trans_Plan_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Plan_Nmbr", "PLAN-NMBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PLAN_NMBR");
        iaa_Cntrct_Trans_Tax_Exmpt_Ind = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Tax_Exmpt_Ind", "TAX-EXMPT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "TAX_EXMPT_IND");
        iaa_Cntrct_Trans_Orig_Ownr_Dob = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orig_Ownr_Dob", "ORIG-OWNR-DOB", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ORIG_OWNR_DOB");
        iaa_Cntrct_Trans_Orig_Ownr_Dod = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orig_Ownr_Dod", "ORIG-OWNR-DOD", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ORIG_OWNR_DOD");
        iaa_Cntrct_Trans_Sub_Plan_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Sub_Plan_Nmbr", "SUB-PLAN-NMBR", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "SUB_PLAN_NMBR");
        iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr", "ORGNTNG-CNTRCT-NMBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "ORGNTNG_CNTRCT_NMBR");
        iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr = vw_iaa_Cntrct_Trans.getRecord().newFieldInGroup("iaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr", "ORGNTNG-SUB-PLAN-NMBR", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "ORGNTNG_SUB_PLAN_NMBR");

        vw_iaa_Cpr_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Cpr_Trans", "IAA-CPR-TRANS"), "IAA_CPR_TRANS", "IA_TRANS_FILE", DdmPeriodicGroups.getInstance().getGroups("IAA_CPR_TRANS"));
        iaa_Cpr_Trans_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "TRANS_DTE");
        iaa_Cpr_Trans_Invrse_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cpr_Trans_Lst_Trans_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, RepeatingFieldStrategy.None, 
            "LST_TRANS_DTE");
        iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        iaa_Cpr_Trans_Cntrct_Part_Payee_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        iaa_Cpr_Trans_Cpr_Id_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Id_Nbr", "CPR-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "CPR_ID_NBR");
        iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Ctznshp_Cde", "PRTCPNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_CTZNSHP_CDE");
        iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Rsdncy_Cde", "PRTCPNT-RSDNCY-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_CDE");
        iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Rsdncy_Sw", "PRTCPNT-RSDNCY-SW", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRTCPNT_RSDNCY_SW");
        iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Tax_Id_Nbr", "PRTCPNT-TAX-ID-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_NBR");
        iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Prtcpnt_Tax_Id_Typ", "PRTCPNT-TAX-ID-TYP", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PRTCPNT_TAX_ID_TYP");
        iaa_Cpr_Trans_Cntrct_Actvty_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Actvty_Cde", "CNTRCT-ACTVTY-CDE", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_ACTVTY_CDE");
        iaa_Cpr_Trans_Cntrct_Trmnte_Rsn = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Trmnte_Rsn", "CNTRCT-TRMNTE-RSN", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_TRMNTE_RSN");
        iaa_Cpr_Trans_Cntrct_Rwrttn_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Rwrttn_Ind", "CNTRCT-RWRTTN-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "CNTRCT_RWRTTN_IND");
        iaa_Cpr_Trans_Cntrct_Cash_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Cash_Cde", "CNTRCT-CASH-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CASH_CDE");
        iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Emplymnt_Trmnt_Cde", "CNTRCT-EMPLYMNT-TRMNT-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_EMPLYMNT_TRMNT_CDE");
        iaa_Cpr_Trans_Cntrct_Company_Data = vw_iaa_Cpr_Trans.getRecord().newGroupInGroup("iaa_Cpr_Trans_Cntrct_Company_Data", "CNTRCT-COMPANY-DATA", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Company_Cd = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Company_Cd", "CNTRCT-COMPANY-CD", 
            FieldType.STRING, 1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_COMPANY_CD", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rcvry_Type_Ind", "CNTRCT-RCVRY-TYPE-IND", 
            FieldType.NUMERIC, 1, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RCVRY_TYPE_IND", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Per_Ivc_Amt", "CNTRCT-PER-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_PER_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Resdl_Ivc_Amt", "CNTRCT-RESDL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RESDL_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Ivc_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Ivc_Used_Amt", "CNTRCT-IVC-USED-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_IVC_USED_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rtb_Amt = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rtb_Amt", "CNTRCT-RTB-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_AMT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Rtb_Percent = iaa_Cpr_Trans_Cntrct_Company_Data.newFieldArrayInGroup("iaa_Cpr_Trans_Cntrct_Rtb_Percent", "CNTRCT-RTB-PERCENT", 
            FieldType.PACKED_DECIMAL, 7, 4, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CNTRCT_RTB_PERCENT", "IA_TRANS_FILE_CNTRCT_COMPANY_DATA");
        iaa_Cpr_Trans_Cntrct_Mode_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Wthdrwl_Dte", "CNTRCT-WTHDRWL-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_WTHDRWL_DTE");
        iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        iaa_Cpr_Trans_Cntrct_Final_Pay_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        iaa_Cpr_Trans_Bnfcry_Xref_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Xref_Ind", "BNFCRY-XREF-IND", FieldType.STRING, 
            9, RepeatingFieldStrategy.None, "BNFCRY_XREF_IND");
        iaa_Cpr_Trans_Bnfcry_Dod_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bnfcry_Dod_Dte", "BNFCRY-DOD-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "BNFCRY_DOD_DTE");
        iaa_Cpr_Trans_Cntrct_Pend_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Cde", "CNTRCT-PEND-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PEND_CDE");
        iaa_Cpr_Trans_Cntrct_Hold_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        iaa_Cpr_Trans_Cntrct_Pend_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Pend_Dte", "CNTRCT-PEND-DTE", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "CNTRCT_PEND_DTE");
        iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Prev_Dist_Cde", "CNTRCT-PREV-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PREV_DIST_CDE");
        iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Curr_Dist_Cde", "CNTRCT-CURR-DIST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_CURR_DIST_CDE");
        iaa_Cpr_Trans_Cntrct_Cmbne_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Cmbne_Cde", "CNTRCT-CMBNE-CDE", FieldType.STRING, 
            12, RepeatingFieldStrategy.None, "CNTRCT_CMBNE_CDE");
        iaa_Cpr_Trans_Cntrct_Spirt_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Cde", "CNTRCT-SPIRT-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_CDE");
        iaa_Cpr_Trans_Cntrct_Spirt_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Amt", "CNTRCT-SPIRT-AMT", FieldType.PACKED_DECIMAL, 
            7, 2, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_AMT");
        iaa_Cpr_Trans_Cntrct_Spirt_Srce = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Srce", "CNTRCT-SPIRT-SRCE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_SRCE");
        iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Arr_Dte", "CNTRCT-SPIRT-ARR-DTE", 
            FieldType.NUMERIC, 4, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_ARR_DTE");
        iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Spirt_Prcss_Dte", "CNTRCT-SPIRT-PRCSS-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_SPIRT_PRCSS_DTE");
        iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Fed_Tax_Amt", "CNTRCT-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CNTRCT_FED_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_State_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_State_Cde", "CNTRCT-STATE-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_STATE_CDE");
        iaa_Cpr_Trans_Cntrct_State_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_State_Tax_Amt", "CNTRCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_Local_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Local_Cde", "CNTRCT-LOCAL-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_CDE");
        iaa_Cpr_Trans_Cntrct_Local_Tax_Amt = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Local_Tax_Amt", "CNTRCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cntrct_Lst_Chnge_Dte", "CNTRCT-LST-CHNGE-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_LST_CHNGE_DTE");
        iaa_Cpr_Trans_Trans_Check_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cpr_Trans_Bfre_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "BFRE_IMGE_ID");
        iaa_Cpr_Trans_Aftr_Imge_Id = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "AFTR_IMGE_ID");
        iaa_Cpr_Trans_Cpr_Xfr_Term_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Xfr_Term_Cde", "CPR-XFR-TERM-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CPR_XFR_TERM_CDE");
        iaa_Cpr_Trans_Cpr_Lgl_Res_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Lgl_Res_Cde", "CPR-LGL-RES-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CPR_LGL_RES_CDE");
        iaa_Cpr_Trans_Cpr_Xfr_Iss_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Cpr_Xfr_Iss_Dte", "CPR-XFR-ISS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CPR_XFR_ISS_DTE");
        iaa_Cpr_Trans_Rllvr_Cntrct_Nbr = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Rllvr_Cntrct_Nbr", "RLLVR-CNTRCT-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "RLLVR_CNTRCT_NBR");
        iaa_Cpr_Trans_Rllvr_Ivc_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Rllvr_Ivc_Ind", "RLLVR-IVC-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "RLLVR_IVC_IND");
        iaa_Cpr_Trans_Rllvr_Elgble_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Rllvr_Elgble_Ind", "RLLVR-ELGBLE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RLLVR_ELGBLE_IND");
        iaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_CdeMuGroup = vw_iaa_Cpr_Trans.getRecord().newGroupInGroup("iaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_CdeMuGroup", "RLLVR_DSTRBTNG_IRC_CDEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_TRANS_FILE_RLLVR_DSTRBTNG_IRC_CDE");
        iaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_Cde = iaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_CdeMuGroup.newFieldArrayInGroup("iaa_Cpr_Trans_Rllvr_Dstrbtng_Irc_Cde", 
            "RLLVR-DSTRBTNG-IRC-CDE", FieldType.STRING, 2, new DbsArrayController(1,4), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "RLLVR_DSTRBTNG_IRC_CDE");
        iaa_Cpr_Trans_Rllvr_Accptng_Irc_Cde = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Rllvr_Accptng_Irc_Cde", "RLLVR-ACCPTNG-IRC-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "RLLVR_ACCPTNG_IRC_CDE");
        iaa_Cpr_Trans_Rllvr_Pln_Admn_Ind = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Rllvr_Pln_Admn_Ind", "RLLVR-PLN-ADMN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RLLVR_PLN_ADMN_IND");
        iaa_Cpr_Trans_Roth_Dsblty_Dte = vw_iaa_Cpr_Trans.getRecord().newFieldInGroup("iaa_Cpr_Trans_Roth_Dsblty_Dte", "ROTH-DSBLTY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ROTH_DSBLTY_DTE");

        vw_iaa_Tiaa_Fund_Trans = new DataAccessProgramView(new NameInfo("vw_iaa_Tiaa_Fund_Trans", "IAA-TIAA-FUND-TRANS"), "IAA_TIAA_FUND_TRANS", "IA_TRANS_FILE", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_TIAA_FUND_TRANS"));
        iaa_Tiaa_Fund_Trans_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Lst_Trans_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr", "TIAA-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde", "TIAA-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde", "TIAA-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef4 = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef4", 
            "Redefines", iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde);
        iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde = iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef4.newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Cde", "TIAA-CMPNY-CDE", 
            FieldType.STRING, 1);
        iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde = iaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_CdeRedef4.newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Fund_Cde", "TIAA-FUND-CDE", 
            FieldType.STRING, 2);
        iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt", "TIAA-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt", "TIAA-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_UNIT_VAL");
        iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Old_Per_Amt", "TIAA-OLD-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_PER_AMT");
        iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Old_Div_Amt", "TIAA-OLD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "TIAA_OLD_DIV_AMT");
        iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Cde", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Dte", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CREF_RATE_DTE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Units_Cnt", "TIAA-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_UNITS_CNT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt = iaa_Tiaa_Fund_Trans_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1,250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Tiaa_Fund_Trans_Trans_Check_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Trans_Check_Dte", "TRANS-CHECK-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Tiaa_Fund_Trans_Bfre_Imge_Id = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Bfre_Imge_Id", "BFRE-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Tiaa_Fund_Trans_Aftr_Imge_Id = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Aftr_Imge_Id", "AFTR-IMGE-ID", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Tiaa_Fund_Trans_Tiaa_Xfr_Iss_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Xfr_Iss_Dte", "TIAA-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_In_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_In_Dte", "TIAA-LST-XFR-IN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_LST_XFR_IN_DTE");
        iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_Out_Dte = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Lst_Xfr_Out_Dte", "TIAA-LST-XFR-OUT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        iaa_Tiaa_Fund_Trans_Tckr_Symbl = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tckr_Symbl", "TCKR-SYMBL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TCKR_SYMBL");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup = vw_iaa_Tiaa_Fund_Trans.getRecord().newGroupInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup", "TIAA_RATE_GICMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_TRANS_FILE_TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic = iaa_Tiaa_Fund_Trans_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Rate_Gic", "TIAA-RATE-GIC", 
            FieldType.NUMERIC, 11, new DbsArrayController(1,250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        iaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Mode_Ind", "TIAA-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund = vw_iaa_Tiaa_Fund_Trans.getRecord().newFieldInGroup("iaa_Tiaa_Fund_Trans_Tiaa_Old_Cmpny_Fund", "TIAA-OLD-CMPNY-FUND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_OLD_CMPNY_FUND");

        vw_iaa_Cref_Fund_Trans_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cref_Fund_Trans_1", "IAA-CREF-FUND-TRANS-1"), "IAA_CREF_FUND_TRANS_1", 
            "IA_TRANS_FILE", DdmPeriodicGroups.getInstance().getGroups("IAA_CREF_FUND_TRANS_1"));
        iaa_Cref_Fund_Trans_1_Trans_Dte = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Cref_Fund_Trans_1_Invrse_Trans_Dte = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Invrse_Trans_Dte", "INVRSE-TRANS-DTE", 
            FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, "INVRSE_TRANS_DTE");
        iaa_Cref_Fund_Trans_1_Lst_Trans_Dte = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Lst_Trans_Dte", "LST-TRANS-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Cref_Fund_Trans_1_Cref_Cntrct_Ppcn_Nbr = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Cref_Cntrct_Ppcn_Nbr", 
            "CREF-CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        iaa_Cref_Fund_Trans_1_Cref_Cntrct_Payee_Cde = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Cref_Cntrct_Payee_Cde", 
            "CREF-CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CREF_CNTRCT_PAYEE_CDE");
        iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_Cde = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_Cde", 
            "CREF-CMPNY-FUND-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");
        iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_CdeRedef5 = vw_iaa_Cref_Fund_Trans_1.getRecord().newGroupInGroup("iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_CdeRedef5", 
            "Redefines", iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_Cde);
        iaa_Cref_Fund_Trans_1_Cref_Cmpny_Cde = iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_CdeRedef5.newFieldInGroup("iaa_Cref_Fund_Trans_1_Cref_Cmpny_Cde", 
            "CREF-CMPNY-CDE", FieldType.STRING, 1);
        iaa_Cref_Fund_Trans_1_Cref_Fund_Cde = iaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_CdeRedef5.newFieldInGroup("iaa_Cref_Fund_Trans_1_Cref_Fund_Cde", "CREF-FUND-CDE", 
            FieldType.STRING, 2);
        iaa_Cref_Fund_Trans_1_Cref_Tot_Per_Amt = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Cref_Tot_Per_Amt", "CREF-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        iaa_Cref_Fund_Trans_1_Cref_Unit_Val = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Cref_Unit_Val", "CREF-UNIT-VAL", 
            FieldType.PACKED_DECIMAL, 9, 4, RepeatingFieldStrategy.None, "CREF_UNIT_VAL");
        iaa_Cref_Fund_Trans_1_Count_Castcref_Rate_Data_Grp = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Count_Castcref_Rate_Data_Grp", 
            "C*CREF-RATE-DATA-GRP", FieldType.NUMERIC, 3, RepeatingFieldStrategy.CAsteriskVariable, "IA_TRANS_FILE_CREF_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_1_Cref_Rate_Data_Grp = vw_iaa_Cref_Fund_Trans_1.getRecord().newGroupInGroup("iaa_Cref_Fund_Trans_1_Cref_Rate_Data_Grp", "CREF-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_1_Cref_Rate_Cde = iaa_Cref_Fund_Trans_1_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Trans_1_Cref_Rate_Cde", "CREF-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_1_Cref_Rate_Dte = iaa_Cref_Fund_Trans_1_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Trans_1_Cref_Rate_Dte", "CREF-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "CREF_RATE_DTE", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_1_Cref_Units_Cnt = iaa_Cref_Fund_Trans_1_Cref_Rate_Data_Grp.newFieldArrayInGroup("iaa_Cref_Fund_Trans_1_Cref_Units_Cnt", "CREF-UNITS-CNT", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1,5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_UNITS_CNT", "IA_TRANS_FILE_TIAA_RATE_DATA_GRP");
        iaa_Cref_Fund_Trans_1_Trans_Check_Dte = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Trans_Check_Dte", "TRANS-CHECK-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "TRANS_CHECK_DTE");
        iaa_Cref_Fund_Trans_1_Bfre_Imge_Id = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Bfre_Imge_Id", "BFRE-IMGE-ID", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "BFRE_IMGE_ID");
        iaa_Cref_Fund_Trans_1_Aftr_Imge_Id = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Aftr_Imge_Id", "AFTR-IMGE-ID", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "AFTR_IMGE_ID");
        iaa_Cref_Fund_Trans_1_Cref_Xfr_Iss_Dte = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Cref_Xfr_Iss_Dte", "CREF-XFR-ISS-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        iaa_Cref_Fund_Trans_1_Cref_Lst_Xfr_In_Dte = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Cref_Lst_Xfr_In_Dte", 
            "CREF-LST-XFR-IN-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "TIAA_LST_XFR_IN_DTE");
        iaa_Cref_Fund_Trans_1_Cref_Lst_Xfr_Out_Dte = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Cref_Lst_Xfr_Out_Dte", 
            "CREF-LST-XFR-OUT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        iaa_Cref_Fund_Trans_1_Tckr_Symbl = vw_iaa_Cref_Fund_Trans_1.getRecord().newFieldInGroup("iaa_Cref_Fund_Trans_1_Tckr_Symbl", "TCKR-SYMBL", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "TCKR_SYMBL");
        vw_ads_Ia_Rslt.setUniquePeList();
        vw_iaa_Cntrct_Prtcpnt_Role.setUniquePeList();
        vw_iaa_Tiaa_Fund_Rcrd.setUniquePeList();
        vw_iaa_Cref_Fund_Rcrd_1.setUniquePeList();
        vw_iaa_Cntrl_Rcrd_1.setUniquePeList();
        vw_iaa_Cpr_Trans.setUniquePeList();
        vw_iaa_Tiaa_Fund_Trans.setUniquePeList();
        vw_iaa_Cref_Fund_Trans_1.setUniquePeList();

        this.setRecordName("LdaAdsl601");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_ads_Ia_Rslt.reset();
        vw_iaa_Cntrct.reset();
        vw_iaa_Cntrct_Prtcpnt_Role.reset();
        vw_iaa_Tiaa_Fund_Rcrd.reset();
        vw_iaa_Cref_Fund_Rcrd_1.reset();
        vw_iaa_Trans_Rcrd.reset();
        vw_iaa_Cntrl_Rcrd_1.reset();
        vw_iaa_Cntrct_Trans.reset();
        vw_iaa_Cpr_Trans.reset();
        vw_iaa_Tiaa_Fund_Trans.reset();
        vw_iaa_Cref_Fund_Trans_1.reset();
    }

    // Constructor
    public LdaAdsl601() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
