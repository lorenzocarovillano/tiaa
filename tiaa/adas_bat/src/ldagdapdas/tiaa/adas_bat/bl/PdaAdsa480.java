/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:17:36 PM
**        * FROM NATURAL PDA     : ADSA480
************************************************************
**        * FILE NAME            : PdaAdsa480.java
**        * CLASS NAME           : PdaAdsa480
**        * INSTANCE NAME        : PdaAdsa480
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaAdsa480 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Adsa480_Mit_Update;
    private DbsField pnd_Adsa480_Mit_Update_Adp_Unique_Id;
    private DbsField pnd_Adsa480_Mit_Update_Adp_Plan_Nbr;
    private DbsField pnd_Adsa480_Mit_Update_Adp_Annty_Strt_Dte;
    private DbsField pnd_Adsa480_Mit_Update_Adp_Annt_Typ_Cde;
    private DbsField pnd_Adsa480_Mit_Update_Adp_Entry_User_Id;
    private DbsField pnd_Adsa480_Mit_Update_Adp_Wpid;
    private DbsField pnd_Adsa480_Mit_Update_Adp_Mit_Log_Dte_Tme;
    private DbsField pnd_Adsa480_Mit_Update_Adp_Stts_Cde;
    private DbsField pnd_Adsa480_Mit_Update_Adp_Bps_Unit;
    private DbsField pnd_Adsa480_Mit_Update_Adp_Cntrcts_In_Rqst;
    private DbsField pnd_Adsa480_Mit_Update_Pnd_User_Id;
    private DbsField pnd_Adsa480_Mit_Update_Pnd_Type_Of_Process;
    private DbsField pnd_Adsa480_Mit_Update_Pnd_Action_Cde;
    private DbsField pnd_Adsa480_Mit_Update_Pnd_Accntng_Dte;
    private DbsField pnd_Adsa480_Mit_Update_Pnd_Mit_Msg;
    private DbsField pnd_Adsa480_Mit_Update_Pnd_Mit_Msg_Nbr;
    private DbsField pnd_Adsa480_Mit_Update_Pnd_Mit_Debug_On;

    public DbsGroup getPnd_Adsa480_Mit_Update() { return pnd_Adsa480_Mit_Update; }

    public DbsField getPnd_Adsa480_Mit_Update_Adp_Unique_Id() { return pnd_Adsa480_Mit_Update_Adp_Unique_Id; }

    public DbsField getPnd_Adsa480_Mit_Update_Adp_Plan_Nbr() { return pnd_Adsa480_Mit_Update_Adp_Plan_Nbr; }

    public DbsField getPnd_Adsa480_Mit_Update_Adp_Annty_Strt_Dte() { return pnd_Adsa480_Mit_Update_Adp_Annty_Strt_Dte; }

    public DbsField getPnd_Adsa480_Mit_Update_Adp_Annt_Typ_Cde() { return pnd_Adsa480_Mit_Update_Adp_Annt_Typ_Cde; }

    public DbsField getPnd_Adsa480_Mit_Update_Adp_Entry_User_Id() { return pnd_Adsa480_Mit_Update_Adp_Entry_User_Id; }

    public DbsField getPnd_Adsa480_Mit_Update_Adp_Wpid() { return pnd_Adsa480_Mit_Update_Adp_Wpid; }

    public DbsField getPnd_Adsa480_Mit_Update_Adp_Mit_Log_Dte_Tme() { return pnd_Adsa480_Mit_Update_Adp_Mit_Log_Dte_Tme; }

    public DbsField getPnd_Adsa480_Mit_Update_Adp_Stts_Cde() { return pnd_Adsa480_Mit_Update_Adp_Stts_Cde; }

    public DbsField getPnd_Adsa480_Mit_Update_Adp_Bps_Unit() { return pnd_Adsa480_Mit_Update_Adp_Bps_Unit; }

    public DbsField getPnd_Adsa480_Mit_Update_Adp_Cntrcts_In_Rqst() { return pnd_Adsa480_Mit_Update_Adp_Cntrcts_In_Rqst; }

    public DbsField getPnd_Adsa480_Mit_Update_Pnd_User_Id() { return pnd_Adsa480_Mit_Update_Pnd_User_Id; }

    public DbsField getPnd_Adsa480_Mit_Update_Pnd_Type_Of_Process() { return pnd_Adsa480_Mit_Update_Pnd_Type_Of_Process; }

    public DbsField getPnd_Adsa480_Mit_Update_Pnd_Action_Cde() { return pnd_Adsa480_Mit_Update_Pnd_Action_Cde; }

    public DbsField getPnd_Adsa480_Mit_Update_Pnd_Accntng_Dte() { return pnd_Adsa480_Mit_Update_Pnd_Accntng_Dte; }

    public DbsField getPnd_Adsa480_Mit_Update_Pnd_Mit_Msg() { return pnd_Adsa480_Mit_Update_Pnd_Mit_Msg; }

    public DbsField getPnd_Adsa480_Mit_Update_Pnd_Mit_Msg_Nbr() { return pnd_Adsa480_Mit_Update_Pnd_Mit_Msg_Nbr; }

    public DbsField getPnd_Adsa480_Mit_Update_Pnd_Mit_Debug_On() { return pnd_Adsa480_Mit_Update_Pnd_Mit_Debug_On; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Adsa480_Mit_Update = dbsRecord.newGroupInRecord("pnd_Adsa480_Mit_Update", "#ADSA480-MIT-UPDATE");
        pnd_Adsa480_Mit_Update.setParameterOption(ParameterOption.ByReference);
        pnd_Adsa480_Mit_Update_Adp_Unique_Id = pnd_Adsa480_Mit_Update.newFieldInGroup("pnd_Adsa480_Mit_Update_Adp_Unique_Id", "ADP-UNIQUE-ID", FieldType.NUMERIC, 
            12);
        pnd_Adsa480_Mit_Update_Adp_Plan_Nbr = pnd_Adsa480_Mit_Update.newFieldInGroup("pnd_Adsa480_Mit_Update_Adp_Plan_Nbr", "ADP-PLAN-NBR", FieldType.STRING, 
            6);
        pnd_Adsa480_Mit_Update_Adp_Annty_Strt_Dte = pnd_Adsa480_Mit_Update.newFieldInGroup("pnd_Adsa480_Mit_Update_Adp_Annty_Strt_Dte", "ADP-ANNTY-STRT-DTE", 
            FieldType.DATE);
        pnd_Adsa480_Mit_Update_Adp_Annt_Typ_Cde = pnd_Adsa480_Mit_Update.newFieldInGroup("pnd_Adsa480_Mit_Update_Adp_Annt_Typ_Cde", "ADP-ANNT-TYP-CDE", 
            FieldType.STRING, 1);
        pnd_Adsa480_Mit_Update_Adp_Entry_User_Id = pnd_Adsa480_Mit_Update.newFieldInGroup("pnd_Adsa480_Mit_Update_Adp_Entry_User_Id", "ADP-ENTRY-USER-ID", 
            FieldType.STRING, 8);
        pnd_Adsa480_Mit_Update_Adp_Wpid = pnd_Adsa480_Mit_Update.newFieldInGroup("pnd_Adsa480_Mit_Update_Adp_Wpid", "ADP-WPID", FieldType.STRING, 6);
        pnd_Adsa480_Mit_Update_Adp_Mit_Log_Dte_Tme = pnd_Adsa480_Mit_Update.newFieldInGroup("pnd_Adsa480_Mit_Update_Adp_Mit_Log_Dte_Tme", "ADP-MIT-LOG-DTE-TME", 
            FieldType.STRING, 15);
        pnd_Adsa480_Mit_Update_Adp_Stts_Cde = pnd_Adsa480_Mit_Update.newFieldInGroup("pnd_Adsa480_Mit_Update_Adp_Stts_Cde", "ADP-STTS-CDE", FieldType.STRING, 
            3);
        pnd_Adsa480_Mit_Update_Adp_Bps_Unit = pnd_Adsa480_Mit_Update.newFieldInGroup("pnd_Adsa480_Mit_Update_Adp_Bps_Unit", "ADP-BPS-UNIT", FieldType.STRING, 
            8);
        pnd_Adsa480_Mit_Update_Adp_Cntrcts_In_Rqst = pnd_Adsa480_Mit_Update.newFieldArrayInGroup("pnd_Adsa480_Mit_Update_Adp_Cntrcts_In_Rqst", "ADP-CNTRCTS-IN-RQST", 
            FieldType.STRING, 10, new DbsArrayController(1,12));
        pnd_Adsa480_Mit_Update_Pnd_User_Id = pnd_Adsa480_Mit_Update.newFieldInGroup("pnd_Adsa480_Mit_Update_Pnd_User_Id", "#USER-ID", FieldType.STRING, 
            8);
        pnd_Adsa480_Mit_Update_Pnd_Type_Of_Process = pnd_Adsa480_Mit_Update.newFieldInGroup("pnd_Adsa480_Mit_Update_Pnd_Type_Of_Process", "#TYPE-OF-PROCESS", 
            FieldType.STRING, 8);
        pnd_Adsa480_Mit_Update_Pnd_Action_Cde = pnd_Adsa480_Mit_Update.newFieldInGroup("pnd_Adsa480_Mit_Update_Pnd_Action_Cde", "#ACTION-CDE", FieldType.STRING, 
            2);
        pnd_Adsa480_Mit_Update_Pnd_Accntng_Dte = pnd_Adsa480_Mit_Update.newFieldInGroup("pnd_Adsa480_Mit_Update_Pnd_Accntng_Dte", "#ACCNTNG-DTE", FieldType.DATE);
        pnd_Adsa480_Mit_Update_Pnd_Mit_Msg = pnd_Adsa480_Mit_Update.newFieldInGroup("pnd_Adsa480_Mit_Update_Pnd_Mit_Msg", "#MIT-MSG", FieldType.STRING, 
            79);
        pnd_Adsa480_Mit_Update_Pnd_Mit_Msg_Nbr = pnd_Adsa480_Mit_Update.newFieldInGroup("pnd_Adsa480_Mit_Update_Pnd_Mit_Msg_Nbr", "#MIT-MSG-NBR", FieldType.NUMERIC, 
            4);
        pnd_Adsa480_Mit_Update_Pnd_Mit_Debug_On = pnd_Adsa480_Mit_Update.newFieldInGroup("pnd_Adsa480_Mit_Update_Pnd_Mit_Debug_On", "#MIT-DEBUG-ON", FieldType.BOOLEAN);

        dbsRecord.reset();
    }

    // Constructors
    public PdaAdsa480(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

