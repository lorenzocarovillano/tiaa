/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:49:28 PM
**        * FROM NATURAL LDA     : ADSL400
************************************************************
**        * FILE NAME            : LdaAdsl400.java
**        * CLASS NAME           : LdaAdsl400
**        * INSTANCE NAME        : LdaAdsl400
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdsl400 extends DbsRecord
{
    // Properties
    private DbsField pnd_Omni_Input;
    private DbsGroup pnd_Omni_InputRedef1;
    private DbsField pnd_Omni_Input_Pnd_Omni_Pin_A;
    private DbsGroup pnd_Omni_Input_Pnd_Omni_Pin_ARedef2;
    private DbsField pnd_Omni_Input_Pnd_Omni_Pin;
    private DbsField pnd_Omni_Input_Pnd_Omni_Ssn_A;
    private DbsGroup pnd_Omni_Input_Pnd_Omni_Ssn_ARedef3;
    private DbsField pnd_Omni_Input_Pnd_Omni_Ssn;
    private DbsField pnd_Omni_Input_Pnd_Omni_Eff_Dte_A;
    private DbsGroup pnd_Omni_Input_Pnd_Omni_Eff_Dte_ARedef4;
    private DbsField pnd_Omni_Input_Pnd_Omni_Eff_Dte;
    private DbsField pnd_Omni_Input_Pnd_Omni_Annty_Optn;
    private DbsField pnd_Omni_Input_Pnd_Omni_Plan_Nbr;
    private DbsField pnd_Omni_Input_Pnd_Omni_Emp_Term_Dte_A;
    private DbsGroup pnd_Omni_Input_Pnd_Omni_Emp_Term_Dte_ARedef5;
    private DbsField pnd_Omni_Input_Pnd_Omni_Emp_Term_Dte;
    private DbsField pnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct;
    private DbsField pnd_Omni_Input_Pnd_Omni_Cref_Cntrct;
    private DbsField pnd_Omni_Input_Pnd_Omni_Issue_State;
    private DbsField pnd_Omni_Input_Pnd_Omni_Issue_Dte_A;
    private DbsGroup pnd_Omni_Input_Pnd_Omni_Issue_Dte_ARedef6;
    private DbsField pnd_Omni_Input_Pnd_Omni_Issue_Dte;
    private DbsField pnd_Omni_Input_Pnd_Omni_Fund_Ticker;
    private DbsField pnd_Omni_Input_Pnd_Omni_Fund_Typ;
    private DbsField pnd_Omni_Input_Pnd_Omni_Contract_Id_A;
    private DbsGroup pnd_Omni_Input_Pnd_Omni_Contract_Id_ARedef7;
    private DbsField pnd_Omni_Input_Pnd_Omni_Contract_Id;
    private DbsField pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt_A;
    private DbsGroup pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt_ARedef8;
    private DbsField pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt;
    private DbsField pnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt_A;
    private DbsGroup pnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt_ARedef9;
    private DbsField pnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt;
    private DbsField pnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt_A;
    private DbsGroup pnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt_ARedef10;
    private DbsField pnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt;
    private DbsField pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts_A;
    private DbsGroup pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts_ARedef11;
    private DbsField pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts;
    private DbsField pnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts_A;
    private DbsGroup pnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts_ARedef12;
    private DbsField pnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts;
    private DbsField pnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt_A;
    private DbsGroup pnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt_ARedef13;
    private DbsField pnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt;
    private DbsField pnd_Omni_Input_Pnd_Omni_Usage_Cde1;
    private DbsField pnd_Omni_Input_Pnd_Omni_Usage_Cde2;
    private DbsField pnd_Omni_Input_Pnd_Omni_Invstmt_Id;
    private DbsField pnd_Omni_Input_Pnd_Omni_Fund_Source;
    private DbsField pnd_Omni_Input_Pnd_Omni_Sub_Plan_Nbr;
    private DbsField pnd_Omni_Input_Pnd_Omni_File_Dte_A;
    private DbsGroup pnd_Omni_Input_Pnd_Omni_File_Dte_ARedef14;
    private DbsField pnd_Omni_Input_Pnd_Omni_File_Dte;
    private DbsField pnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt_A;
    private DbsGroup pnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt_ARedef15;
    private DbsField pnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt;
    private DbsField pnd_Omni_Input_Pnd_Omni_Orig_Cntrct_Lob_Ind;
    private DbsField pnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt_A;
    private DbsGroup pnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt_ARedef16;
    private DbsField pnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt;
    private DbsField pnd_Omni_Input_Pnd_Omni_Inv_Short_Name;
    private DbsField pnd_Omni_Input_Pnd_Omni_Roth_Rqst_Ind;
    private DbsField pnd_Omni_Input_Pnd_Omni_Roth_Plan_Type;
    private DbsField pnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte_A;
    private DbsGroup pnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte_ARedef17;
    private DbsField pnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte;
    private DbsField pnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte_A;
    private DbsGroup pnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte_ARedef18;
    private DbsField pnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte;
    private DbsField pnd_Omni_Input_Pnd_Omni_Roth_Tot_Contrib_Amt_A;
    private DbsGroup pnd_Omni_Input_Pnd_Omni_Roth_Tot_Contrib_Amt_ARedef19;
    private DbsField pnd_Omni_Input_Pnd_Omni_Roth_Tot_Contrib_Amt;
    private DbsField pnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt_A;
    private DbsGroup pnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt_ARedef20;
    private DbsField pnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt;

    public DbsField getPnd_Omni_Input() { return pnd_Omni_Input; }

    public DbsGroup getPnd_Omni_InputRedef1() { return pnd_Omni_InputRedef1; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Pin_A() { return pnd_Omni_Input_Pnd_Omni_Pin_A; }

    public DbsGroup getPnd_Omni_Input_Pnd_Omni_Pin_ARedef2() { return pnd_Omni_Input_Pnd_Omni_Pin_ARedef2; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Pin() { return pnd_Omni_Input_Pnd_Omni_Pin; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Ssn_A() { return pnd_Omni_Input_Pnd_Omni_Ssn_A; }

    public DbsGroup getPnd_Omni_Input_Pnd_Omni_Ssn_ARedef3() { return pnd_Omni_Input_Pnd_Omni_Ssn_ARedef3; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Ssn() { return pnd_Omni_Input_Pnd_Omni_Ssn; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Eff_Dte_A() { return pnd_Omni_Input_Pnd_Omni_Eff_Dte_A; }

    public DbsGroup getPnd_Omni_Input_Pnd_Omni_Eff_Dte_ARedef4() { return pnd_Omni_Input_Pnd_Omni_Eff_Dte_ARedef4; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Eff_Dte() { return pnd_Omni_Input_Pnd_Omni_Eff_Dte; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Annty_Optn() { return pnd_Omni_Input_Pnd_Omni_Annty_Optn; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Plan_Nbr() { return pnd_Omni_Input_Pnd_Omni_Plan_Nbr; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Emp_Term_Dte_A() { return pnd_Omni_Input_Pnd_Omni_Emp_Term_Dte_A; }

    public DbsGroup getPnd_Omni_Input_Pnd_Omni_Emp_Term_Dte_ARedef5() { return pnd_Omni_Input_Pnd_Omni_Emp_Term_Dte_ARedef5; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Emp_Term_Dte() { return pnd_Omni_Input_Pnd_Omni_Emp_Term_Dte; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct() { return pnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Cref_Cntrct() { return pnd_Omni_Input_Pnd_Omni_Cref_Cntrct; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Issue_State() { return pnd_Omni_Input_Pnd_Omni_Issue_State; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Issue_Dte_A() { return pnd_Omni_Input_Pnd_Omni_Issue_Dte_A; }

    public DbsGroup getPnd_Omni_Input_Pnd_Omni_Issue_Dte_ARedef6() { return pnd_Omni_Input_Pnd_Omni_Issue_Dte_ARedef6; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Issue_Dte() { return pnd_Omni_Input_Pnd_Omni_Issue_Dte; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Fund_Ticker() { return pnd_Omni_Input_Pnd_Omni_Fund_Ticker; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Fund_Typ() { return pnd_Omni_Input_Pnd_Omni_Fund_Typ; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Contract_Id_A() { return pnd_Omni_Input_Pnd_Omni_Contract_Id_A; }

    public DbsGroup getPnd_Omni_Input_Pnd_Omni_Contract_Id_ARedef7() { return pnd_Omni_Input_Pnd_Omni_Contract_Id_ARedef7; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Contract_Id() { return pnd_Omni_Input_Pnd_Omni_Contract_Id; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt_A() { return pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt_A; }

    public DbsGroup getPnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt_ARedef8() { return pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt_ARedef8; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt() { return pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt_A() { return pnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt_A; }

    public DbsGroup getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt_ARedef9() { return pnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt_ARedef9; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt() { return pnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt_A() { return pnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt_A; }

    public DbsGroup getPnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt_ARedef10() { return pnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt_ARedef10; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt() { return pnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts_A() { return pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts_A; }

    public DbsGroup getPnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts_ARedef11() { return pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts_ARedef11; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts() { return pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts_A() { return pnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts_A; }

    public DbsGroup getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts_ARedef12() { return pnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts_ARedef12; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts() { return pnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt_A() { return pnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt_A; }

    public DbsGroup getPnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt_ARedef13() { return pnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt_ARedef13; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt() { return pnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Usage_Cde1() { return pnd_Omni_Input_Pnd_Omni_Usage_Cde1; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Usage_Cde2() { return pnd_Omni_Input_Pnd_Omni_Usage_Cde2; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Invstmt_Id() { return pnd_Omni_Input_Pnd_Omni_Invstmt_Id; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Fund_Source() { return pnd_Omni_Input_Pnd_Omni_Fund_Source; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Sub_Plan_Nbr() { return pnd_Omni_Input_Pnd_Omni_Sub_Plan_Nbr; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_File_Dte_A() { return pnd_Omni_Input_Pnd_Omni_File_Dte_A; }

    public DbsGroup getPnd_Omni_Input_Pnd_Omni_File_Dte_ARedef14() { return pnd_Omni_Input_Pnd_Omni_File_Dte_ARedef14; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_File_Dte() { return pnd_Omni_Input_Pnd_Omni_File_Dte; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt_A() { return pnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt_A; }

    public DbsGroup getPnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt_ARedef15() { return pnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt_ARedef15; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt() { return pnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Orig_Cntrct_Lob_Ind() { return pnd_Omni_Input_Pnd_Omni_Orig_Cntrct_Lob_Ind; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt_A() { return pnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt_A; }

    public DbsGroup getPnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt_ARedef16() { return pnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt_ARedef16; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt() { return pnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Inv_Short_Name() { return pnd_Omni_Input_Pnd_Omni_Inv_Short_Name; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Roth_Rqst_Ind() { return pnd_Omni_Input_Pnd_Omni_Roth_Rqst_Ind; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Roth_Plan_Type() { return pnd_Omni_Input_Pnd_Omni_Roth_Plan_Type; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte_A() { return pnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte_A; }

    public DbsGroup getPnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte_ARedef17() { return pnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte_ARedef17; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte() { return pnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte_A() { return pnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte_A; }

    public DbsGroup getPnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte_ARedef18() { return pnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte_ARedef18; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte() { return pnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Roth_Tot_Contrib_Amt_A() { return pnd_Omni_Input_Pnd_Omni_Roth_Tot_Contrib_Amt_A; }

    public DbsGroup getPnd_Omni_Input_Pnd_Omni_Roth_Tot_Contrib_Amt_ARedef19() { return pnd_Omni_Input_Pnd_Omni_Roth_Tot_Contrib_Amt_ARedef19; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Roth_Tot_Contrib_Amt() { return pnd_Omni_Input_Pnd_Omni_Roth_Tot_Contrib_Amt; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt_A() { return pnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt_A; }

    public DbsGroup getPnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt_ARedef20() { return pnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt_ARedef20; }

    public DbsField getPnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt() { return pnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Omni_Input = newFieldArrayInRecord("pnd_Omni_Input", "#OMNI-INPUT", FieldType.STRING, 1, new DbsArrayController(1,299));
        pnd_Omni_InputRedef1 = newGroupInRecord("pnd_Omni_InputRedef1", "Redefines", pnd_Omni_Input);
        pnd_Omni_Input_Pnd_Omni_Pin_A = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Pin_A", "#OMNI-PIN-A", FieldType.STRING, 12);
        pnd_Omni_Input_Pnd_Omni_Pin_ARedef2 = pnd_Omni_InputRedef1.newGroupInGroup("pnd_Omni_Input_Pnd_Omni_Pin_ARedef2", "Redefines", pnd_Omni_Input_Pnd_Omni_Pin_A);
        pnd_Omni_Input_Pnd_Omni_Pin = pnd_Omni_Input_Pnd_Omni_Pin_ARedef2.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Pin", "#OMNI-PIN", FieldType.NUMERIC, 
            12);
        pnd_Omni_Input_Pnd_Omni_Ssn_A = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Ssn_A", "#OMNI-SSN-A", FieldType.STRING, 9);
        pnd_Omni_Input_Pnd_Omni_Ssn_ARedef3 = pnd_Omni_InputRedef1.newGroupInGroup("pnd_Omni_Input_Pnd_Omni_Ssn_ARedef3", "Redefines", pnd_Omni_Input_Pnd_Omni_Ssn_A);
        pnd_Omni_Input_Pnd_Omni_Ssn = pnd_Omni_Input_Pnd_Omni_Ssn_ARedef3.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Ssn", "#OMNI-SSN", FieldType.NUMERIC, 
            9);
        pnd_Omni_Input_Pnd_Omni_Eff_Dte_A = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Eff_Dte_A", "#OMNI-EFF-DTE-A", FieldType.STRING, 
            8);
        pnd_Omni_Input_Pnd_Omni_Eff_Dte_ARedef4 = pnd_Omni_InputRedef1.newGroupInGroup("pnd_Omni_Input_Pnd_Omni_Eff_Dte_ARedef4", "Redefines", pnd_Omni_Input_Pnd_Omni_Eff_Dte_A);
        pnd_Omni_Input_Pnd_Omni_Eff_Dte = pnd_Omni_Input_Pnd_Omni_Eff_Dte_ARedef4.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Eff_Dte", "#OMNI-EFF-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Omni_Input_Pnd_Omni_Annty_Optn = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Annty_Optn", "#OMNI-ANNTY-OPTN", FieldType.STRING, 
            2);
        pnd_Omni_Input_Pnd_Omni_Plan_Nbr = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Plan_Nbr", "#OMNI-PLAN-NBR", FieldType.STRING, 
            6);
        pnd_Omni_Input_Pnd_Omni_Emp_Term_Dte_A = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Emp_Term_Dte_A", "#OMNI-EMP-TERM-DTE-A", 
            FieldType.STRING, 8);
        pnd_Omni_Input_Pnd_Omni_Emp_Term_Dte_ARedef5 = pnd_Omni_InputRedef1.newGroupInGroup("pnd_Omni_Input_Pnd_Omni_Emp_Term_Dte_ARedef5", "Redefines", 
            pnd_Omni_Input_Pnd_Omni_Emp_Term_Dte_A);
        pnd_Omni_Input_Pnd_Omni_Emp_Term_Dte = pnd_Omni_Input_Pnd_Omni_Emp_Term_Dte_ARedef5.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Emp_Term_Dte", "#OMNI-EMP-TERM-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Tiaa_Cntrct", "#OMNI-TIAA-CNTRCT", FieldType.STRING, 
            10);
        pnd_Omni_Input_Pnd_Omni_Cref_Cntrct = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Cref_Cntrct", "#OMNI-CREF-CNTRCT", FieldType.STRING, 
            10);
        pnd_Omni_Input_Pnd_Omni_Issue_State = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Issue_State", "#OMNI-ISSUE-STATE", FieldType.STRING, 
            2);
        pnd_Omni_Input_Pnd_Omni_Issue_Dte_A = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Issue_Dte_A", "#OMNI-ISSUE-DTE-A", FieldType.STRING, 
            8);
        pnd_Omni_Input_Pnd_Omni_Issue_Dte_ARedef6 = pnd_Omni_InputRedef1.newGroupInGroup("pnd_Omni_Input_Pnd_Omni_Issue_Dte_ARedef6", "Redefines", pnd_Omni_Input_Pnd_Omni_Issue_Dte_A);
        pnd_Omni_Input_Pnd_Omni_Issue_Dte = pnd_Omni_Input_Pnd_Omni_Issue_Dte_ARedef6.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Issue_Dte", "#OMNI-ISSUE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Omni_Input_Pnd_Omni_Fund_Ticker = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Ticker", "#OMNI-FUND-TICKER", FieldType.STRING, 
            10);
        pnd_Omni_Input_Pnd_Omni_Fund_Typ = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Typ", "#OMNI-FUND-TYP", FieldType.STRING, 
            1);
        pnd_Omni_Input_Pnd_Omni_Contract_Id_A = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Contract_Id_A", "#OMNI-CONTRACT-ID-A", FieldType.STRING, 
            11);
        pnd_Omni_Input_Pnd_Omni_Contract_Id_ARedef7 = pnd_Omni_InputRedef1.newGroupInGroup("pnd_Omni_Input_Pnd_Omni_Contract_Id_ARedef7", "Redefines", 
            pnd_Omni_Input_Pnd_Omni_Contract_Id_A);
        pnd_Omni_Input_Pnd_Omni_Contract_Id = pnd_Omni_Input_Pnd_Omni_Contract_Id_ARedef7.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Contract_Id", "#OMNI-CONTRACT-ID", 
            FieldType.NUMERIC, 11);
        pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt_A = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt_A", "#OMNI-FUND-OPN-ACCUM-AMT-A", 
            FieldType.STRING, 11);
        pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt_ARedef8 = pnd_Omni_InputRedef1.newGroupInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt_ARedef8", 
            "Redefines", pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt_A);
        pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt = pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt_ARedef8.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Amt", 
            "#OMNI-FUND-OPN-ACCUM-AMT", FieldType.DECIMAL, 11,2);
        pnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt_A = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt_A", "#OMNI-FUND-SETTL-AMT-A", 
            FieldType.STRING, 11);
        pnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt_ARedef9 = pnd_Omni_InputRedef1.newGroupInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt_ARedef9", "Redefines", 
            pnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt_A);
        pnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt = pnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt_ARedef9.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Settl_Amt", 
            "#OMNI-FUND-SETTL-AMT", FieldType.DECIMAL, 11,2);
        pnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt_A = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt_A", "#OMNI-FUND-IVC-AMT-A", 
            FieldType.STRING, 9);
        pnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt_ARedef10 = pnd_Omni_InputRedef1.newGroupInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt_ARedef10", "Redefines", 
            pnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt_A);
        pnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt = pnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt_ARedef10.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Ivc_Amt", "#OMNI-FUND-IVC-AMT", 
            FieldType.DECIMAL, 9,2);
        pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts_A = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts_A", "#OMNI-FUND-OPN-ACCUM-UNTS-A", 
            FieldType.STRING, 10);
        pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts_ARedef11 = pnd_Omni_InputRedef1.newGroupInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts_ARedef11", 
            "Redefines", pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts_A);
        pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts = pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts_ARedef11.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Opn_Accum_Unts", 
            "#OMNI-FUND-OPN-ACCUM-UNTS", FieldType.DECIMAL, 10,3);
        pnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts_A = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts_A", "#OMNI-FUND-SETTL-UNTS-A", 
            FieldType.STRING, 10);
        pnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts_ARedef12 = pnd_Omni_InputRedef1.newGroupInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts_ARedef12", "Redefines", 
            pnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts_A);
        pnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts = pnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts_ARedef12.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Settl_Unts", 
            "#OMNI-FUND-SETTL-UNTS", FieldType.DECIMAL, 10,3);
        pnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt_A = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt_A", "#OMNI-FUND-RATE-SETTL-AMT-A", 
            FieldType.STRING, 11);
        pnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt_ARedef13 = pnd_Omni_InputRedef1.newGroupInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt_ARedef13", 
            "Redefines", pnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt_A);
        pnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt = pnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt_ARedef13.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Rate_Settl_Amt", 
            "#OMNI-FUND-RATE-SETTL-AMT", FieldType.DECIMAL, 11,2);
        pnd_Omni_Input_Pnd_Omni_Usage_Cde1 = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Usage_Cde1", "#OMNI-USAGE-CDE1", FieldType.STRING, 
            1);
        pnd_Omni_Input_Pnd_Omni_Usage_Cde2 = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Usage_Cde2", "#OMNI-USAGE-CDE2", FieldType.STRING, 
            1);
        pnd_Omni_Input_Pnd_Omni_Invstmt_Id = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Invstmt_Id", "#OMNI-INVSTMT-ID", FieldType.STRING, 
            2);
        pnd_Omni_Input_Pnd_Omni_Fund_Source = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Fund_Source", "#OMNI-FUND-SOURCE", FieldType.STRING, 
            1);
        pnd_Omni_Input_Pnd_Omni_Sub_Plan_Nbr = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Sub_Plan_Nbr", "#OMNI-SUB-PLAN-NBR", FieldType.STRING, 
            6);
        pnd_Omni_Input_Pnd_Omni_File_Dte_A = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_File_Dte_A", "#OMNI-FILE-DTE-A", FieldType.STRING, 
            8);
        pnd_Omni_Input_Pnd_Omni_File_Dte_ARedef14 = pnd_Omni_InputRedef1.newGroupInGroup("pnd_Omni_Input_Pnd_Omni_File_Dte_ARedef14", "Redefines", pnd_Omni_Input_Pnd_Omni_File_Dte_A);
        pnd_Omni_Input_Pnd_Omni_File_Dte = pnd_Omni_Input_Pnd_Omni_File_Dte_ARedef14.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_File_Dte", "#OMNI-FILE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt_A = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt_A", "#OMNI-TPA-GUARANT-COMMUT-AMT-A", 
            FieldType.STRING, 11);
        pnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt_ARedef15 = pnd_Omni_InputRedef1.newGroupInGroup("pnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt_ARedef15", 
            "Redefines", pnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt_A);
        pnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt = pnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt_ARedef15.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Tpa_Guarant_Commut_Amt", 
            "#OMNI-TPA-GUARANT-COMMUT-AMT", FieldType.DECIMAL, 11,2);
        pnd_Omni_Input_Pnd_Omni_Orig_Cntrct_Lob_Ind = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Orig_Cntrct_Lob_Ind", "#OMNI-ORIG-CNTRCT-LOB-IND", 
            FieldType.STRING, 1);
        pnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt_A = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt_A", "#OMNI-TPA-PAYMENT-CNT-A", 
            FieldType.STRING, 2);
        pnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt_ARedef16 = pnd_Omni_InputRedef1.newGroupInGroup("pnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt_ARedef16", "Redefines", 
            pnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt_A);
        pnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt = pnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt_ARedef16.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Tpa_Payment_Cnt", 
            "#OMNI-TPA-PAYMENT-CNT", FieldType.NUMERIC, 2);
        pnd_Omni_Input_Pnd_Omni_Inv_Short_Name = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Inv_Short_Name", "#OMNI-INV-SHORT-NAME", 
            FieldType.STRING, 5);
        pnd_Omni_Input_Pnd_Omni_Roth_Rqst_Ind = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Roth_Rqst_Ind", "#OMNI-ROTH-RQST-IND", FieldType.STRING, 
            1);
        pnd_Omni_Input_Pnd_Omni_Roth_Plan_Type = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Roth_Plan_Type", "#OMNI-ROTH-PLAN-TYPE", 
            FieldType.STRING, 1);
        pnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte_A = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte_A", "#OMNI-ROTH-FRST-CONTRIB-DTE-A", 
            FieldType.STRING, 8);
        pnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte_ARedef17 = pnd_Omni_InputRedef1.newGroupInGroup("pnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte_ARedef17", 
            "Redefines", pnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte_A);
        pnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte = pnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte_ARedef17.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Roth_Frst_Contrib_Dte", 
            "#OMNI-ROTH-FRST-CONTRIB-DTE", FieldType.NUMERIC, 8);
        pnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte_A = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte_A", "#OMNI-ROTH-DSBLTY-DTE-A", 
            FieldType.STRING, 8);
        pnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte_ARedef18 = pnd_Omni_InputRedef1.newGroupInGroup("pnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte_ARedef18", "Redefines", 
            pnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte_A);
        pnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte = pnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte_ARedef18.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Roth_Dsblty_Dte", 
            "#OMNI-ROTH-DSBLTY-DTE", FieldType.NUMERIC, 8);
        pnd_Omni_Input_Pnd_Omni_Roth_Tot_Contrib_Amt_A = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Roth_Tot_Contrib_Amt_A", "#OMNI-ROTH-TOT-CONTRIB-AMT-A", 
            FieldType.STRING, 11);
        pnd_Omni_Input_Pnd_Omni_Roth_Tot_Contrib_Amt_ARedef19 = pnd_Omni_InputRedef1.newGroupInGroup("pnd_Omni_Input_Pnd_Omni_Roth_Tot_Contrib_Amt_ARedef19", 
            "Redefines", pnd_Omni_Input_Pnd_Omni_Roth_Tot_Contrib_Amt_A);
        pnd_Omni_Input_Pnd_Omni_Roth_Tot_Contrib_Amt = pnd_Omni_Input_Pnd_Omni_Roth_Tot_Contrib_Amt_ARedef19.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Roth_Tot_Contrib_Amt", 
            "#OMNI-ROTH-TOT-CONTRIB-AMT", FieldType.DECIMAL, 11,2);
        pnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt_A = pnd_Omni_InputRedef1.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt_A", "#OMNI-ROTH-CONTRIB-SETTL-AMT-A", 
            FieldType.STRING, 11);
        pnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt_ARedef20 = pnd_Omni_InputRedef1.newGroupInGroup("pnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt_ARedef20", 
            "Redefines", pnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt_A);
        pnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt = pnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt_ARedef20.newFieldInGroup("pnd_Omni_Input_Pnd_Omni_Roth_Contrib_Settl_Amt", 
            "#OMNI-ROTH-CONTRIB-SETTL-AMT", FieldType.DECIMAL, 11,2);

        this.setRecordName("LdaAdsl400");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAdsl400() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
