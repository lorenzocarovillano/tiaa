/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:17:36 PM
**        * FROM NATURAL PDA     : ADSA450
************************************************************
**        * FILE NAME            : PdaAdsa450.java
**        * CLASS NAME           : PdaAdsa450
**        * INSTANCE NAME        : PdaAdsa450
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaAdsa450 extends PdaBase
{
    // Properties
    private DbsGroup pnd_Adsa450_Actuarial_Data;
    private DbsField pnd_Adsa450_Actuarial_Data_Pnd_Ia_Next_Cycle_Check_Dte;
    private DbsField pnd_Adsa450_Actuarial_Data_Pnd_Account_C;
    private DbsField pnd_Adsa450_Actuarial_Data_Pnd_Account_R;
    private DbsField pnd_Adsa450_Actuarial_Data_Pnd_Account_T;
    private DbsField pnd_Adsa450_Actuarial_Data_Pnd_All_Tiaa_Contracts;
    private DbsField pnd_Adsa450_Actuarial_Data_Pnd_All_Cref_Contracts;
    private DbsGroup pnd_Adsa450_Actuarial_Data_Pnd_Dtl_Tiaa_Data;
    private DbsField pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde;
    private DbsField pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Actl_Amt;
    private DbsField pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Actl_Amt;
    private DbsField pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Guar_Comm_Amt;
    private DbsField pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Guar_Comm_Amt;
    private DbsGroup pnd_Adsa450_Actuarial_Data_Pnd_Dtl_Cref_Data;
    private DbsField pnd_Adsa450_Actuarial_Data_Pnd_Cref_Acct_Cde;
    private DbsField pnd_Adsa450_Actuarial_Data_Pnd_Cref_Rate_Cde;
    private DbsField pnd_Adsa450_Actuarial_Data_Pnd_Cref_Graded_Mnthly_Actl_Amt;
    private DbsField pnd_Adsa450_Actuarial_Data_Pnd_Cref_Stndrd_Annual_Actl_Amt;
    private DbsField pnd_Adsa450_Actuarial_Data_Pnd_Orig_Lob_Ind;
    private DbsField pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Gic;

    public DbsGroup getPnd_Adsa450_Actuarial_Data() { return pnd_Adsa450_Actuarial_Data; }

    public DbsField getPnd_Adsa450_Actuarial_Data_Pnd_Ia_Next_Cycle_Check_Dte() { return pnd_Adsa450_Actuarial_Data_Pnd_Ia_Next_Cycle_Check_Dte; }

    public DbsField getPnd_Adsa450_Actuarial_Data_Pnd_Account_C() { return pnd_Adsa450_Actuarial_Data_Pnd_Account_C; }

    public DbsField getPnd_Adsa450_Actuarial_Data_Pnd_Account_R() { return pnd_Adsa450_Actuarial_Data_Pnd_Account_R; }

    public DbsField getPnd_Adsa450_Actuarial_Data_Pnd_Account_T() { return pnd_Adsa450_Actuarial_Data_Pnd_Account_T; }

    public DbsField getPnd_Adsa450_Actuarial_Data_Pnd_All_Tiaa_Contracts() { return pnd_Adsa450_Actuarial_Data_Pnd_All_Tiaa_Contracts; }

    public DbsField getPnd_Adsa450_Actuarial_Data_Pnd_All_Cref_Contracts() { return pnd_Adsa450_Actuarial_Data_Pnd_All_Cref_Contracts; }

    public DbsGroup getPnd_Adsa450_Actuarial_Data_Pnd_Dtl_Tiaa_Data() { return pnd_Adsa450_Actuarial_Data_Pnd_Dtl_Tiaa_Data; }

    public DbsField getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde() { return pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde; }

    public DbsField getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Actl_Amt() { return pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Actl_Amt; }

    public DbsField getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Actl_Amt() { return pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Actl_Amt; }

    public DbsField getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Guar_Comm_Amt() { return pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Guar_Comm_Amt; }

    public DbsField getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Guar_Comm_Amt() { return pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Guar_Comm_Amt; }

    public DbsGroup getPnd_Adsa450_Actuarial_Data_Pnd_Dtl_Cref_Data() { return pnd_Adsa450_Actuarial_Data_Pnd_Dtl_Cref_Data; }

    public DbsField getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Acct_Cde() { return pnd_Adsa450_Actuarial_Data_Pnd_Cref_Acct_Cde; }

    public DbsField getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Rate_Cde() { return pnd_Adsa450_Actuarial_Data_Pnd_Cref_Rate_Cde; }

    public DbsField getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Graded_Mnthly_Actl_Amt() { return pnd_Adsa450_Actuarial_Data_Pnd_Cref_Graded_Mnthly_Actl_Amt; 
        }

    public DbsField getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Stndrd_Annual_Actl_Amt() { return pnd_Adsa450_Actuarial_Data_Pnd_Cref_Stndrd_Annual_Actl_Amt; 
        }

    public DbsField getPnd_Adsa450_Actuarial_Data_Pnd_Orig_Lob_Ind() { return pnd_Adsa450_Actuarial_Data_Pnd_Orig_Lob_Ind; }

    public DbsField getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Gic() { return pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Gic; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Adsa450_Actuarial_Data = dbsRecord.newGroupInRecord("pnd_Adsa450_Actuarial_Data", "#ADSA450-ACTUARIAL-DATA");
        pnd_Adsa450_Actuarial_Data.setParameterOption(ParameterOption.ByReference);
        pnd_Adsa450_Actuarial_Data_Pnd_Ia_Next_Cycle_Check_Dte = pnd_Adsa450_Actuarial_Data.newFieldInGroup("pnd_Adsa450_Actuarial_Data_Pnd_Ia_Next_Cycle_Check_Dte", 
            "#IA-NEXT-CYCLE-CHECK-DTE", FieldType.DATE);
        pnd_Adsa450_Actuarial_Data_Pnd_Account_C = pnd_Adsa450_Actuarial_Data.newFieldInGroup("pnd_Adsa450_Actuarial_Data_Pnd_Account_C", "#ACCOUNT-C", 
            FieldType.STRING, 1);
        pnd_Adsa450_Actuarial_Data_Pnd_Account_R = pnd_Adsa450_Actuarial_Data.newFieldInGroup("pnd_Adsa450_Actuarial_Data_Pnd_Account_R", "#ACCOUNT-R", 
            FieldType.STRING, 1);
        pnd_Adsa450_Actuarial_Data_Pnd_Account_T = pnd_Adsa450_Actuarial_Data.newFieldInGroup("pnd_Adsa450_Actuarial_Data_Pnd_Account_T", "#ACCOUNT-T", 
            FieldType.STRING, 1);
        pnd_Adsa450_Actuarial_Data_Pnd_All_Tiaa_Contracts = pnd_Adsa450_Actuarial_Data.newFieldArrayInGroup("pnd_Adsa450_Actuarial_Data_Pnd_All_Tiaa_Contracts", 
            "#ALL-TIAA-CONTRACTS", FieldType.STRING, 10, new DbsArrayController(1,12));
        pnd_Adsa450_Actuarial_Data_Pnd_All_Cref_Contracts = pnd_Adsa450_Actuarial_Data.newFieldArrayInGroup("pnd_Adsa450_Actuarial_Data_Pnd_All_Cref_Contracts", 
            "#ALL-CREF-CONTRACTS", FieldType.STRING, 10, new DbsArrayController(1,12));
        pnd_Adsa450_Actuarial_Data_Pnd_Dtl_Tiaa_Data = pnd_Adsa450_Actuarial_Data.newGroupArrayInGroup("pnd_Adsa450_Actuarial_Data_Pnd_Dtl_Tiaa_Data", 
            "#DTL-TIAA-DATA", new DbsArrayController(1,250));
        pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde = pnd_Adsa450_Actuarial_Data_Pnd_Dtl_Tiaa_Data.newFieldInGroup("pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde", 
            "#TIAA-RATE-CDE", FieldType.STRING, 2);
        pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Actl_Amt = pnd_Adsa450_Actuarial_Data_Pnd_Dtl_Tiaa_Data.newFieldInGroup("pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Actl_Amt", 
            "#TIAA-STNDRD-ACTL-AMT", FieldType.DECIMAL, 11,2);
        pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Actl_Amt = pnd_Adsa450_Actuarial_Data_Pnd_Dtl_Tiaa_Data.newFieldInGroup("pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Actl_Amt", 
            "#TIAA-GRADED-ACTL-AMT", FieldType.DECIMAL, 11,2);
        pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Guar_Comm_Amt = pnd_Adsa450_Actuarial_Data_Pnd_Dtl_Tiaa_Data.newFieldInGroup("pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Guar_Comm_Amt", 
            "#TIAA-STNDRD-GUAR-COMM-AMT", FieldType.DECIMAL, 11,2);
        pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Guar_Comm_Amt = pnd_Adsa450_Actuarial_Data_Pnd_Dtl_Tiaa_Data.newFieldInGroup("pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Guar_Comm_Amt", 
            "#TIAA-GRADED-GUAR-COMM-AMT", FieldType.DECIMAL, 11,2);
        pnd_Adsa450_Actuarial_Data_Pnd_Dtl_Cref_Data = pnd_Adsa450_Actuarial_Data.newGroupArrayInGroup("pnd_Adsa450_Actuarial_Data_Pnd_Dtl_Cref_Data", 
            "#DTL-CREF-DATA", new DbsArrayController(1,20));
        pnd_Adsa450_Actuarial_Data_Pnd_Cref_Acct_Cde = pnd_Adsa450_Actuarial_Data_Pnd_Dtl_Cref_Data.newFieldInGroup("pnd_Adsa450_Actuarial_Data_Pnd_Cref_Acct_Cde", 
            "#CREF-ACCT-CDE", FieldType.STRING, 1);
        pnd_Adsa450_Actuarial_Data_Pnd_Cref_Rate_Cde = pnd_Adsa450_Actuarial_Data_Pnd_Dtl_Cref_Data.newFieldInGroup("pnd_Adsa450_Actuarial_Data_Pnd_Cref_Rate_Cde", 
            "#CREF-RATE-CDE", FieldType.STRING, 2);
        pnd_Adsa450_Actuarial_Data_Pnd_Cref_Graded_Mnthly_Actl_Amt = pnd_Adsa450_Actuarial_Data_Pnd_Dtl_Cref_Data.newFieldInGroup("pnd_Adsa450_Actuarial_Data_Pnd_Cref_Graded_Mnthly_Actl_Amt", 
            "#CREF-GRADED-MNTHLY-ACTL-AMT", FieldType.DECIMAL, 11,2);
        pnd_Adsa450_Actuarial_Data_Pnd_Cref_Stndrd_Annual_Actl_Amt = pnd_Adsa450_Actuarial_Data_Pnd_Dtl_Cref_Data.newFieldInGroup("pnd_Adsa450_Actuarial_Data_Pnd_Cref_Stndrd_Annual_Actl_Amt", 
            "#CREF-STNDRD-ANNUAL-ACTL-AMT", FieldType.DECIMAL, 11,2);
        pnd_Adsa450_Actuarial_Data_Pnd_Orig_Lob_Ind = pnd_Adsa450_Actuarial_Data.newFieldArrayInGroup("pnd_Adsa450_Actuarial_Data_Pnd_Orig_Lob_Ind", "#ORIG-LOB-IND", 
            FieldType.STRING, 1, new DbsArrayController(1,12));
        pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Gic = pnd_Adsa450_Actuarial_Data.newFieldArrayInGroup("pnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Gic", "#TIAA-GIC", 
            FieldType.NUMERIC, 11, new DbsArrayController(1,250));

        dbsRecord.reset();
    }

    // Constructors
    public PdaAdsa450(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

