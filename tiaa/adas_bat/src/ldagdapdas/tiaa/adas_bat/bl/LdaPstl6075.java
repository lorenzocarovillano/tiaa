/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:10:29 PM
**        * FROM NATURAL LDA     : PSTL6075
************************************************************
**        * FILE NAME            : LdaPstl6075.java
**        * CLASS NAME           : LdaPstl6075
**        * INSTANCE NAME        : LdaPstl6075
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaPstl6075 extends DbsRecord
{
    // Properties
    private DbsGroup sort_Key_Struct;
    private DbsField sort_Key_Struct_Sort_Key_Lgth;
    private DbsField sort_Key_Struct_Sort_Key_Data;
    private DbsGroup sort_Key_Struct_Sort_Key_DataRedef1;
    private DbsGroup sort_Key_Struct_Sort_Key;
    private DbsGroup sort_Key_Struct_Level_1_Sort_Flds;
    private DbsField sort_Key_Struct_Rsk_L1_Value;
    private DbsField sort_Key_Struct_Id_L1_Record;
    private DbsGroup sort_Key_Struct_Level_2_Sort_Flds;
    private DbsField sort_Key_Struct_Id_L2_Record;
    private DbsField sort_Key_Struct_Key_L2_Post_Req_Id;
    private DbsGroup sort_Key_Struct_Level_3_Sort_Flds;
    private DbsField sort_Key_Struct_Id_L3_Record;
    private DbsField sort_Key_Struct_Key_L3_Lob;
    private DbsField sort_Key_Struct_Key_L3_Tiebreak;
    private DbsGroup sort_Key_Struct_Level_3_Sort_FldsRedef2;
    private DbsField sort_Key_Struct_Level_3_Sort_Flds_Reset;
    private DbsGroup sort_Key_Struct_Level_4_Sort_Flds;
    private DbsField sort_Key_Struct_Id_L4_Record;
    private DbsField sort_Key_Struct_Key_L4_Tiebreak;
    private DbsGroup sort_Key_Struct_Level_4_Sort_FldsRedef3;
    private DbsField sort_Key_Struct_Level_4_Sort_Flds_Reset;
    private DbsGroup ct_Contract_Struct;
    private DbsField ct_Contract_Struct_Ct_Record_Id;
    private DbsField ct_Contract_Struct_Ct_Data_Lgth;
    private DbsField ct_Contract_Struct_Ct_Data_Occurs;
    private DbsField ct_Contract_Struct_Ct_Data;
    private DbsGroup ct_Contract_Struct_Ct_DataRedef4;
    private DbsGroup ct_Contract_Struct_Ct_Contract;
    private DbsField ct_Contract_Struct_Ct_Issue_Date;
    private DbsGroup ct_Contract_Struct_Ct_Issue_DateRedef5;
    private DbsField ct_Contract_Struct_Ct_Issue_Month;
    private DbsField ct_Contract_Struct_Ct_Issue_Day;
    private DbsField ct_Contract_Struct_Ct_Issue_Century;
    private DbsField ct_Contract_Struct_Ct_Issue_Year;
    private DbsField ct_Contract_Struct_Ct_Effective_Date;
    private DbsGroup ct_Contract_Struct_Ct_Effective_DateRedef6;
    private DbsField ct_Contract_Struct_Ct_Effective_Month;
    private DbsField ct_Contract_Struct_Ct_Effective_Day;
    private DbsField ct_Contract_Struct_Ct_Effective_Century;
    private DbsField ct_Contract_Struct_Ct_Effective_Year;
    private DbsField ct_Contract_Struct_Ct_Contract_Number;
    private DbsField ct_Contract_Struct_Ct_Line_Of_Business;
    private DbsField ct_Contract_Struct_Ct_Ind_1;
    private DbsField ct_Contract_Struct_Ct_Ind_2;
    private DbsField ct_Contract_Struct_Ct_Ind_3;
    private DbsField ct_Contract_Struct_Ct_Ind_4;
    private DbsField ct_Contract_Struct_Ct_Ind_5;
    private DbsGroup ac_Accumulation_Info_Struct;
    private DbsField ac_Accumulation_Info_Struct_Ac_Record_Id;
    private DbsField ac_Accumulation_Info_Struct_Ac_Data_Lgth;
    private DbsField ac_Accumulation_Info_Struct_Ac_Data_Occurs;
    private DbsField ac_Accumulation_Info_Struct_Ac_Data;
    private DbsGroup ac_Accumulation_Info_Struct_Ac_DataRedef7;
    private DbsField ac_Accumulation_Info_Struct_Ac_Data_R;
    private DbsGroup ac_Accumulation_Info_Struct_Ac_DataRedef8;
    private DbsGroup ac_Accumulation_Info_Struct_Ac_Accumulation_Info;
    private DbsField ac_Accumulation_Info_Struct_Ac_Contract_Number;
    private DbsField ac_Accumulation_Info_Struct_Ac_Accumulation;
    private DbsGroup in_Income_Struct;
    private DbsField in_Income_Struct_In_Record_Id;
    private DbsField in_Income_Struct_In_Data_Lgth;
    private DbsField in_Income_Struct_In_Data_Occurs;
    private DbsField in_Income_Struct_In_Data;
    private DbsGroup in_Income_Struct_In_DataRedef9;
    private DbsField in_Income_Struct_In_Data_R;
    private DbsGroup in_Income_Struct_In_DataRedef10;
    private DbsGroup in_Income_Struct_In_Income;
    private DbsField in_Income_Struct_In_Estimated_Income;
    private DbsField in_Income_Struct_In_Guaranteed_Income;
    private DbsField in_Income_Struct_In_Tiaa_Payment_Method;
    private DbsField in_Income_Struct_In_Fund_Name;
    private DbsField in_Income_Struct_In_Units;
    private DbsField in_Income_Struct_In_Unit_Value;
    private DbsField in_Income_Struct_In_Change_Method;
    private DbsGroup ms_Messages_Struct;
    private DbsField ms_Messages_Struct_Ms_Record_Id;
    private DbsField ms_Messages_Struct_Ms_Data_Lgth;
    private DbsField ms_Messages_Struct_Ms_Data_Occurs;
    private DbsField ms_Messages_Struct_Ms_Data;
    private DbsGroup ms_Messages_Struct_Ms_DataRedef11;
    private DbsField ms_Messages_Struct_Ms_Data_R;
    private DbsGroup ms_Messages_Struct_Ms_DataRedef12;
    private DbsGroup ms_Messages_Struct_Ms_Messages;
    private DbsField ms_Messages_Struct_Ms_Message_Date;
    private DbsField ms_Messages_Struct_Ms_Message_Id;
    private DbsField ms_Messages_Struct_Ms_Payment_Frequency;

    public DbsGroup getSort_Key_Struct() { return sort_Key_Struct; }

    public DbsField getSort_Key_Struct_Sort_Key_Lgth() { return sort_Key_Struct_Sort_Key_Lgth; }

    public DbsField getSort_Key_Struct_Sort_Key_Data() { return sort_Key_Struct_Sort_Key_Data; }

    public DbsGroup getSort_Key_Struct_Sort_Key_DataRedef1() { return sort_Key_Struct_Sort_Key_DataRedef1; }

    public DbsGroup getSort_Key_Struct_Sort_Key() { return sort_Key_Struct_Sort_Key; }

    public DbsGroup getSort_Key_Struct_Level_1_Sort_Flds() { return sort_Key_Struct_Level_1_Sort_Flds; }

    public DbsField getSort_Key_Struct_Rsk_L1_Value() { return sort_Key_Struct_Rsk_L1_Value; }

    public DbsField getSort_Key_Struct_Id_L1_Record() { return sort_Key_Struct_Id_L1_Record; }

    public DbsGroup getSort_Key_Struct_Level_2_Sort_Flds() { return sort_Key_Struct_Level_2_Sort_Flds; }

    public DbsField getSort_Key_Struct_Id_L2_Record() { return sort_Key_Struct_Id_L2_Record; }

    public DbsField getSort_Key_Struct_Key_L2_Post_Req_Id() { return sort_Key_Struct_Key_L2_Post_Req_Id; }

    public DbsGroup getSort_Key_Struct_Level_3_Sort_Flds() { return sort_Key_Struct_Level_3_Sort_Flds; }

    public DbsField getSort_Key_Struct_Id_L3_Record() { return sort_Key_Struct_Id_L3_Record; }

    public DbsField getSort_Key_Struct_Key_L3_Lob() { return sort_Key_Struct_Key_L3_Lob; }

    public DbsField getSort_Key_Struct_Key_L3_Tiebreak() { return sort_Key_Struct_Key_L3_Tiebreak; }

    public DbsGroup getSort_Key_Struct_Level_3_Sort_FldsRedef2() { return sort_Key_Struct_Level_3_Sort_FldsRedef2; }

    public DbsField getSort_Key_Struct_Level_3_Sort_Flds_Reset() { return sort_Key_Struct_Level_3_Sort_Flds_Reset; }

    public DbsGroup getSort_Key_Struct_Level_4_Sort_Flds() { return sort_Key_Struct_Level_4_Sort_Flds; }

    public DbsField getSort_Key_Struct_Id_L4_Record() { return sort_Key_Struct_Id_L4_Record; }

    public DbsField getSort_Key_Struct_Key_L4_Tiebreak() { return sort_Key_Struct_Key_L4_Tiebreak; }

    public DbsGroup getSort_Key_Struct_Level_4_Sort_FldsRedef3() { return sort_Key_Struct_Level_4_Sort_FldsRedef3; }

    public DbsField getSort_Key_Struct_Level_4_Sort_Flds_Reset() { return sort_Key_Struct_Level_4_Sort_Flds_Reset; }

    public DbsGroup getCt_Contract_Struct() { return ct_Contract_Struct; }

    public DbsField getCt_Contract_Struct_Ct_Record_Id() { return ct_Contract_Struct_Ct_Record_Id; }

    public DbsField getCt_Contract_Struct_Ct_Data_Lgth() { return ct_Contract_Struct_Ct_Data_Lgth; }

    public DbsField getCt_Contract_Struct_Ct_Data_Occurs() { return ct_Contract_Struct_Ct_Data_Occurs; }

    public DbsField getCt_Contract_Struct_Ct_Data() { return ct_Contract_Struct_Ct_Data; }

    public DbsGroup getCt_Contract_Struct_Ct_DataRedef4() { return ct_Contract_Struct_Ct_DataRedef4; }

    public DbsGroup getCt_Contract_Struct_Ct_Contract() { return ct_Contract_Struct_Ct_Contract; }

    public DbsField getCt_Contract_Struct_Ct_Issue_Date() { return ct_Contract_Struct_Ct_Issue_Date; }

    public DbsGroup getCt_Contract_Struct_Ct_Issue_DateRedef5() { return ct_Contract_Struct_Ct_Issue_DateRedef5; }

    public DbsField getCt_Contract_Struct_Ct_Issue_Month() { return ct_Contract_Struct_Ct_Issue_Month; }

    public DbsField getCt_Contract_Struct_Ct_Issue_Day() { return ct_Contract_Struct_Ct_Issue_Day; }

    public DbsField getCt_Contract_Struct_Ct_Issue_Century() { return ct_Contract_Struct_Ct_Issue_Century; }

    public DbsField getCt_Contract_Struct_Ct_Issue_Year() { return ct_Contract_Struct_Ct_Issue_Year; }

    public DbsField getCt_Contract_Struct_Ct_Effective_Date() { return ct_Contract_Struct_Ct_Effective_Date; }

    public DbsGroup getCt_Contract_Struct_Ct_Effective_DateRedef6() { return ct_Contract_Struct_Ct_Effective_DateRedef6; }

    public DbsField getCt_Contract_Struct_Ct_Effective_Month() { return ct_Contract_Struct_Ct_Effective_Month; }

    public DbsField getCt_Contract_Struct_Ct_Effective_Day() { return ct_Contract_Struct_Ct_Effective_Day; }

    public DbsField getCt_Contract_Struct_Ct_Effective_Century() { return ct_Contract_Struct_Ct_Effective_Century; }

    public DbsField getCt_Contract_Struct_Ct_Effective_Year() { return ct_Contract_Struct_Ct_Effective_Year; }

    public DbsField getCt_Contract_Struct_Ct_Contract_Number() { return ct_Contract_Struct_Ct_Contract_Number; }

    public DbsField getCt_Contract_Struct_Ct_Line_Of_Business() { return ct_Contract_Struct_Ct_Line_Of_Business; }

    public DbsField getCt_Contract_Struct_Ct_Ind_1() { return ct_Contract_Struct_Ct_Ind_1; }

    public DbsField getCt_Contract_Struct_Ct_Ind_2() { return ct_Contract_Struct_Ct_Ind_2; }

    public DbsField getCt_Contract_Struct_Ct_Ind_3() { return ct_Contract_Struct_Ct_Ind_3; }

    public DbsField getCt_Contract_Struct_Ct_Ind_4() { return ct_Contract_Struct_Ct_Ind_4; }

    public DbsField getCt_Contract_Struct_Ct_Ind_5() { return ct_Contract_Struct_Ct_Ind_5; }

    public DbsGroup getAc_Accumulation_Info_Struct() { return ac_Accumulation_Info_Struct; }

    public DbsField getAc_Accumulation_Info_Struct_Ac_Record_Id() { return ac_Accumulation_Info_Struct_Ac_Record_Id; }

    public DbsField getAc_Accumulation_Info_Struct_Ac_Data_Lgth() { return ac_Accumulation_Info_Struct_Ac_Data_Lgth; }

    public DbsField getAc_Accumulation_Info_Struct_Ac_Data_Occurs() { return ac_Accumulation_Info_Struct_Ac_Data_Occurs; }

    public DbsField getAc_Accumulation_Info_Struct_Ac_Data() { return ac_Accumulation_Info_Struct_Ac_Data; }

    public DbsGroup getAc_Accumulation_Info_Struct_Ac_DataRedef7() { return ac_Accumulation_Info_Struct_Ac_DataRedef7; }

    public DbsField getAc_Accumulation_Info_Struct_Ac_Data_R() { return ac_Accumulation_Info_Struct_Ac_Data_R; }

    public DbsGroup getAc_Accumulation_Info_Struct_Ac_DataRedef8() { return ac_Accumulation_Info_Struct_Ac_DataRedef8; }

    public DbsGroup getAc_Accumulation_Info_Struct_Ac_Accumulation_Info() { return ac_Accumulation_Info_Struct_Ac_Accumulation_Info; }

    public DbsField getAc_Accumulation_Info_Struct_Ac_Contract_Number() { return ac_Accumulation_Info_Struct_Ac_Contract_Number; }

    public DbsField getAc_Accumulation_Info_Struct_Ac_Accumulation() { return ac_Accumulation_Info_Struct_Ac_Accumulation; }

    public DbsGroup getIn_Income_Struct() { return in_Income_Struct; }

    public DbsField getIn_Income_Struct_In_Record_Id() { return in_Income_Struct_In_Record_Id; }

    public DbsField getIn_Income_Struct_In_Data_Lgth() { return in_Income_Struct_In_Data_Lgth; }

    public DbsField getIn_Income_Struct_In_Data_Occurs() { return in_Income_Struct_In_Data_Occurs; }

    public DbsField getIn_Income_Struct_In_Data() { return in_Income_Struct_In_Data; }

    public DbsGroup getIn_Income_Struct_In_DataRedef9() { return in_Income_Struct_In_DataRedef9; }

    public DbsField getIn_Income_Struct_In_Data_R() { return in_Income_Struct_In_Data_R; }

    public DbsGroup getIn_Income_Struct_In_DataRedef10() { return in_Income_Struct_In_DataRedef10; }

    public DbsGroup getIn_Income_Struct_In_Income() { return in_Income_Struct_In_Income; }

    public DbsField getIn_Income_Struct_In_Estimated_Income() { return in_Income_Struct_In_Estimated_Income; }

    public DbsField getIn_Income_Struct_In_Guaranteed_Income() { return in_Income_Struct_In_Guaranteed_Income; }

    public DbsField getIn_Income_Struct_In_Tiaa_Payment_Method() { return in_Income_Struct_In_Tiaa_Payment_Method; }

    public DbsField getIn_Income_Struct_In_Fund_Name() { return in_Income_Struct_In_Fund_Name; }

    public DbsField getIn_Income_Struct_In_Units() { return in_Income_Struct_In_Units; }

    public DbsField getIn_Income_Struct_In_Unit_Value() { return in_Income_Struct_In_Unit_Value; }

    public DbsField getIn_Income_Struct_In_Change_Method() { return in_Income_Struct_In_Change_Method; }

    public DbsGroup getMs_Messages_Struct() { return ms_Messages_Struct; }

    public DbsField getMs_Messages_Struct_Ms_Record_Id() { return ms_Messages_Struct_Ms_Record_Id; }

    public DbsField getMs_Messages_Struct_Ms_Data_Lgth() { return ms_Messages_Struct_Ms_Data_Lgth; }

    public DbsField getMs_Messages_Struct_Ms_Data_Occurs() { return ms_Messages_Struct_Ms_Data_Occurs; }

    public DbsField getMs_Messages_Struct_Ms_Data() { return ms_Messages_Struct_Ms_Data; }

    public DbsGroup getMs_Messages_Struct_Ms_DataRedef11() { return ms_Messages_Struct_Ms_DataRedef11; }

    public DbsField getMs_Messages_Struct_Ms_Data_R() { return ms_Messages_Struct_Ms_Data_R; }

    public DbsGroup getMs_Messages_Struct_Ms_DataRedef12() { return ms_Messages_Struct_Ms_DataRedef12; }

    public DbsGroup getMs_Messages_Struct_Ms_Messages() { return ms_Messages_Struct_Ms_Messages; }

    public DbsField getMs_Messages_Struct_Ms_Message_Date() { return ms_Messages_Struct_Ms_Message_Date; }

    public DbsField getMs_Messages_Struct_Ms_Message_Id() { return ms_Messages_Struct_Ms_Message_Id; }

    public DbsField getMs_Messages_Struct_Ms_Payment_Frequency() { return ms_Messages_Struct_Ms_Payment_Frequency; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        sort_Key_Struct = newGroupInRecord("sort_Key_Struct", "SORT-KEY-STRUCT");
        sort_Key_Struct_Sort_Key_Lgth = sort_Key_Struct.newFieldInGroup("sort_Key_Struct_Sort_Key_Lgth", "SORT-KEY-LGTH", FieldType.NUMERIC, 3);
        sort_Key_Struct_Sort_Key_Data = sort_Key_Struct.newFieldArrayInGroup("sort_Key_Struct_Sort_Key_Data", "SORT-KEY-DATA", FieldType.STRING, 1, new 
            DbsArrayController(1,32));
        sort_Key_Struct_Sort_Key_DataRedef1 = sort_Key_Struct.newGroupInGroup("sort_Key_Struct_Sort_Key_DataRedef1", "Redefines", sort_Key_Struct_Sort_Key_Data);
        sort_Key_Struct_Sort_Key = sort_Key_Struct_Sort_Key_DataRedef1.newGroupInGroup("sort_Key_Struct_Sort_Key", "SORT-KEY");
        sort_Key_Struct_Level_1_Sort_Flds = sort_Key_Struct_Sort_Key.newGroupInGroup("sort_Key_Struct_Level_1_Sort_Flds", "LEVEL-1-SORT-FLDS");
        sort_Key_Struct_Rsk_L1_Value = sort_Key_Struct_Level_1_Sort_Flds.newFieldInGroup("sort_Key_Struct_Rsk_L1_Value", "RSK-L1-VALUE", FieldType.NUMERIC, 
            2);
        sort_Key_Struct_Id_L1_Record = sort_Key_Struct_Level_1_Sort_Flds.newFieldInGroup("sort_Key_Struct_Id_L1_Record", "ID-L1-RECORD", FieldType.STRING, 
            1);
        sort_Key_Struct_Level_2_Sort_Flds = sort_Key_Struct_Sort_Key.newGroupInGroup("sort_Key_Struct_Level_2_Sort_Flds", "LEVEL-2-SORT-FLDS");
        sort_Key_Struct_Id_L2_Record = sort_Key_Struct_Level_2_Sort_Flds.newFieldInGroup("sort_Key_Struct_Id_L2_Record", "ID-L2-RECORD", FieldType.STRING, 
            1);
        sort_Key_Struct_Key_L2_Post_Req_Id = sort_Key_Struct_Level_2_Sort_Flds.newFieldInGroup("sort_Key_Struct_Key_L2_Post_Req_Id", "KEY-L2-POST-REQ-ID", 
            FieldType.STRING, 11);
        sort_Key_Struct_Level_3_Sort_Flds = sort_Key_Struct_Sort_Key.newGroupInGroup("sort_Key_Struct_Level_3_Sort_Flds", "LEVEL-3-SORT-FLDS");
        sort_Key_Struct_Id_L3_Record = sort_Key_Struct_Level_3_Sort_Flds.newFieldInGroup("sort_Key_Struct_Id_L3_Record", "ID-L3-RECORD", FieldType.STRING, 
            1);
        sort_Key_Struct_Key_L3_Lob = sort_Key_Struct_Level_3_Sort_Flds.newFieldInGroup("sort_Key_Struct_Key_L3_Lob", "KEY-L3-LOB", FieldType.STRING, 1);
        sort_Key_Struct_Key_L3_Tiebreak = sort_Key_Struct_Level_3_Sort_Flds.newFieldInGroup("sort_Key_Struct_Key_L3_Tiebreak", "KEY-L3-TIEBREAK", FieldType.NUMERIC, 
            7);
        sort_Key_Struct_Level_3_Sort_FldsRedef2 = sort_Key_Struct_Sort_Key.newGroupInGroup("sort_Key_Struct_Level_3_Sort_FldsRedef2", "Redefines", sort_Key_Struct_Level_3_Sort_Flds);
        sort_Key_Struct_Level_3_Sort_Flds_Reset = sort_Key_Struct_Level_3_Sort_FldsRedef2.newFieldInGroup("sort_Key_Struct_Level_3_Sort_Flds_Reset", "LEVEL-3-SORT-FLDS-RESET", 
            FieldType.STRING, 9);
        sort_Key_Struct_Level_4_Sort_Flds = sort_Key_Struct_Sort_Key.newGroupInGroup("sort_Key_Struct_Level_4_Sort_Flds", "LEVEL-4-SORT-FLDS");
        sort_Key_Struct_Id_L4_Record = sort_Key_Struct_Level_4_Sort_Flds.newFieldInGroup("sort_Key_Struct_Id_L4_Record", "ID-L4-RECORD", FieldType.STRING, 
            1);
        sort_Key_Struct_Key_L4_Tiebreak = sort_Key_Struct_Level_4_Sort_Flds.newFieldInGroup("sort_Key_Struct_Key_L4_Tiebreak", "KEY-L4-TIEBREAK", FieldType.NUMERIC, 
            7);
        sort_Key_Struct_Level_4_Sort_FldsRedef3 = sort_Key_Struct_Sort_Key.newGroupInGroup("sort_Key_Struct_Level_4_Sort_FldsRedef3", "Redefines", sort_Key_Struct_Level_4_Sort_Flds);
        sort_Key_Struct_Level_4_Sort_Flds_Reset = sort_Key_Struct_Level_4_Sort_FldsRedef3.newFieldInGroup("sort_Key_Struct_Level_4_Sort_Flds_Reset", "LEVEL-4-SORT-FLDS-RESET", 
            FieldType.STRING, 8);

        ct_Contract_Struct = newGroupInRecord("ct_Contract_Struct", "CT-CONTRACT-STRUCT");
        ct_Contract_Struct_Ct_Record_Id = ct_Contract_Struct.newFieldInGroup("ct_Contract_Struct_Ct_Record_Id", "CT-RECORD-ID", FieldType.STRING, 2);
        ct_Contract_Struct_Ct_Data_Lgth = ct_Contract_Struct.newFieldInGroup("ct_Contract_Struct_Ct_Data_Lgth", "CT-DATA-LGTH", FieldType.NUMERIC, 5);
        ct_Contract_Struct_Ct_Data_Occurs = ct_Contract_Struct.newFieldInGroup("ct_Contract_Struct_Ct_Data_Occurs", "CT-DATA-OCCURS", FieldType.NUMERIC, 
            5);
        ct_Contract_Struct_Ct_Data = ct_Contract_Struct.newFieldArrayInGroup("ct_Contract_Struct_Ct_Data", "CT-DATA", FieldType.STRING, 1, new DbsArrayController(1,
            35));
        ct_Contract_Struct_Ct_DataRedef4 = ct_Contract_Struct.newGroupInGroup("ct_Contract_Struct_Ct_DataRedef4", "Redefines", ct_Contract_Struct_Ct_Data);
        ct_Contract_Struct_Ct_Contract = ct_Contract_Struct_Ct_DataRedef4.newGroupInGroup("ct_Contract_Struct_Ct_Contract", "CT-CONTRACT");
        ct_Contract_Struct_Ct_Issue_Date = ct_Contract_Struct_Ct_Contract.newFieldInGroup("ct_Contract_Struct_Ct_Issue_Date", "CT-ISSUE-DATE", FieldType.NUMERIC, 
            8);
        ct_Contract_Struct_Ct_Issue_DateRedef5 = ct_Contract_Struct_Ct_Contract.newGroupInGroup("ct_Contract_Struct_Ct_Issue_DateRedef5", "Redefines", 
            ct_Contract_Struct_Ct_Issue_Date);
        ct_Contract_Struct_Ct_Issue_Month = ct_Contract_Struct_Ct_Issue_DateRedef5.newFieldInGroup("ct_Contract_Struct_Ct_Issue_Month", "CT-ISSUE-MONTH", 
            FieldType.NUMERIC, 2);
        ct_Contract_Struct_Ct_Issue_Day = ct_Contract_Struct_Ct_Issue_DateRedef5.newFieldInGroup("ct_Contract_Struct_Ct_Issue_Day", "CT-ISSUE-DAY", FieldType.NUMERIC, 
            2);
        ct_Contract_Struct_Ct_Issue_Century = ct_Contract_Struct_Ct_Issue_DateRedef5.newFieldInGroup("ct_Contract_Struct_Ct_Issue_Century", "CT-ISSUE-CENTURY", 
            FieldType.NUMERIC, 2);
        ct_Contract_Struct_Ct_Issue_Year = ct_Contract_Struct_Ct_Issue_DateRedef5.newFieldInGroup("ct_Contract_Struct_Ct_Issue_Year", "CT-ISSUE-YEAR", 
            FieldType.NUMERIC, 2);
        ct_Contract_Struct_Ct_Effective_Date = ct_Contract_Struct_Ct_Contract.newFieldInGroup("ct_Contract_Struct_Ct_Effective_Date", "CT-EFFECTIVE-DATE", 
            FieldType.NUMERIC, 8);
        ct_Contract_Struct_Ct_Effective_DateRedef6 = ct_Contract_Struct_Ct_Contract.newGroupInGroup("ct_Contract_Struct_Ct_Effective_DateRedef6", "Redefines", 
            ct_Contract_Struct_Ct_Effective_Date);
        ct_Contract_Struct_Ct_Effective_Month = ct_Contract_Struct_Ct_Effective_DateRedef6.newFieldInGroup("ct_Contract_Struct_Ct_Effective_Month", "CT-EFFECTIVE-MONTH", 
            FieldType.NUMERIC, 2);
        ct_Contract_Struct_Ct_Effective_Day = ct_Contract_Struct_Ct_Effective_DateRedef6.newFieldInGroup("ct_Contract_Struct_Ct_Effective_Day", "CT-EFFECTIVE-DAY", 
            FieldType.NUMERIC, 2);
        ct_Contract_Struct_Ct_Effective_Century = ct_Contract_Struct_Ct_Effective_DateRedef6.newFieldInGroup("ct_Contract_Struct_Ct_Effective_Century", 
            "CT-EFFECTIVE-CENTURY", FieldType.NUMERIC, 2);
        ct_Contract_Struct_Ct_Effective_Year = ct_Contract_Struct_Ct_Effective_DateRedef6.newFieldInGroup("ct_Contract_Struct_Ct_Effective_Year", "CT-EFFECTIVE-YEAR", 
            FieldType.NUMERIC, 2);
        ct_Contract_Struct_Ct_Contract_Number = ct_Contract_Struct_Ct_Contract.newFieldInGroup("ct_Contract_Struct_Ct_Contract_Number", "CT-CONTRACT-NUMBER", 
            FieldType.STRING, 13);
        ct_Contract_Struct_Ct_Line_Of_Business = ct_Contract_Struct_Ct_Contract.newFieldInGroup("ct_Contract_Struct_Ct_Line_Of_Business", "CT-LINE-OF-BUSINESS", 
            FieldType.STRING, 1);
        ct_Contract_Struct_Ct_Ind_1 = ct_Contract_Struct_Ct_Contract.newFieldInGroup("ct_Contract_Struct_Ct_Ind_1", "CT-IND-1", FieldType.STRING, 1);
        ct_Contract_Struct_Ct_Ind_2 = ct_Contract_Struct_Ct_Contract.newFieldInGroup("ct_Contract_Struct_Ct_Ind_2", "CT-IND-2", FieldType.STRING, 1);
        ct_Contract_Struct_Ct_Ind_3 = ct_Contract_Struct_Ct_Contract.newFieldInGroup("ct_Contract_Struct_Ct_Ind_3", "CT-IND-3", FieldType.STRING, 1);
        ct_Contract_Struct_Ct_Ind_4 = ct_Contract_Struct_Ct_Contract.newFieldInGroup("ct_Contract_Struct_Ct_Ind_4", "CT-IND-4", FieldType.STRING, 1);
        ct_Contract_Struct_Ct_Ind_5 = ct_Contract_Struct_Ct_Contract.newFieldInGroup("ct_Contract_Struct_Ct_Ind_5", "CT-IND-5", FieldType.STRING, 1);

        ac_Accumulation_Info_Struct = newGroupInRecord("ac_Accumulation_Info_Struct", "AC-ACCUMULATION-INFO-STRUCT");
        ac_Accumulation_Info_Struct_Ac_Record_Id = ac_Accumulation_Info_Struct.newFieldInGroup("ac_Accumulation_Info_Struct_Ac_Record_Id", "AC-RECORD-ID", 
            FieldType.STRING, 2);
        ac_Accumulation_Info_Struct_Ac_Data_Lgth = ac_Accumulation_Info_Struct.newFieldInGroup("ac_Accumulation_Info_Struct_Ac_Data_Lgth", "AC-DATA-LGTH", 
            FieldType.NUMERIC, 5);
        ac_Accumulation_Info_Struct_Ac_Data_Occurs = ac_Accumulation_Info_Struct.newFieldInGroup("ac_Accumulation_Info_Struct_Ac_Data_Occurs", "AC-DATA-OCCURS", 
            FieldType.NUMERIC, 5);
        ac_Accumulation_Info_Struct_Ac_Data = ac_Accumulation_Info_Struct.newFieldArrayInGroup("ac_Accumulation_Info_Struct_Ac_Data", "AC-DATA", FieldType.STRING, 
            1, new DbsArrayController(1,126));
        ac_Accumulation_Info_Struct_Ac_DataRedef7 = ac_Accumulation_Info_Struct.newGroupInGroup("ac_Accumulation_Info_Struct_Ac_DataRedef7", "Redefines", 
            ac_Accumulation_Info_Struct_Ac_Data);
        ac_Accumulation_Info_Struct_Ac_Data_R = ac_Accumulation_Info_Struct_Ac_DataRedef7.newFieldArrayInGroup("ac_Accumulation_Info_Struct_Ac_Data_R", 
            "AC-DATA-R", FieldType.STRING, 18, new DbsArrayController(1,7));
        ac_Accumulation_Info_Struct_Ac_DataRedef8 = ac_Accumulation_Info_Struct.newGroupInGroup("ac_Accumulation_Info_Struct_Ac_DataRedef8", "Redefines", 
            ac_Accumulation_Info_Struct_Ac_Data);
        ac_Accumulation_Info_Struct_Ac_Accumulation_Info = ac_Accumulation_Info_Struct_Ac_DataRedef8.newGroupArrayInGroup("ac_Accumulation_Info_Struct_Ac_Accumulation_Info", 
            "AC-ACCUMULATION-INFO", new DbsArrayController(1,7));
        ac_Accumulation_Info_Struct_Ac_Contract_Number = ac_Accumulation_Info_Struct_Ac_Accumulation_Info.newFieldInGroup("ac_Accumulation_Info_Struct_Ac_Contract_Number", 
            "AC-CONTRACT-NUMBER", FieldType.STRING, 9);
        ac_Accumulation_Info_Struct_Ac_Accumulation = ac_Accumulation_Info_Struct_Ac_Accumulation_Info.newFieldInGroup("ac_Accumulation_Info_Struct_Ac_Accumulation", 
            "AC-ACCUMULATION", FieldType.DECIMAL, 9,2);

        in_Income_Struct = newGroupInRecord("in_Income_Struct", "IN-INCOME-STRUCT");
        in_Income_Struct_In_Record_Id = in_Income_Struct.newFieldInGroup("in_Income_Struct_In_Record_Id", "IN-RECORD-ID", FieldType.STRING, 2);
        in_Income_Struct_In_Data_Lgth = in_Income_Struct.newFieldInGroup("in_Income_Struct_In_Data_Lgth", "IN-DATA-LGTH", FieldType.NUMERIC, 5);
        in_Income_Struct_In_Data_Occurs = in_Income_Struct.newFieldInGroup("in_Income_Struct_In_Data_Occurs", "IN-DATA-OCCURS", FieldType.NUMERIC, 5);
        in_Income_Struct_In_Data = in_Income_Struct.newFieldArrayInGroup("in_Income_Struct_In_Data", "IN-DATA", FieldType.STRING, 1, new DbsArrayController(1,
            1332));
        in_Income_Struct_In_DataRedef9 = in_Income_Struct.newGroupInGroup("in_Income_Struct_In_DataRedef9", "Redefines", in_Income_Struct_In_Data);
        in_Income_Struct_In_Data_R = in_Income_Struct_In_DataRedef9.newFieldArrayInGroup("in_Income_Struct_In_Data_R", "IN-DATA-R", FieldType.STRING, 
            74, new DbsArrayController(1,18));
        in_Income_Struct_In_DataRedef10 = in_Income_Struct.newGroupInGroup("in_Income_Struct_In_DataRedef10", "Redefines", in_Income_Struct_In_Data);
        in_Income_Struct_In_Income = in_Income_Struct_In_DataRedef10.newGroupArrayInGroup("in_Income_Struct_In_Income", "IN-INCOME", new DbsArrayController(1,
            18));
        in_Income_Struct_In_Estimated_Income = in_Income_Struct_In_Income.newFieldInGroup("in_Income_Struct_In_Estimated_Income", "IN-ESTIMATED-INCOME", 
            FieldType.DECIMAL, 9,2);
        in_Income_Struct_In_Guaranteed_Income = in_Income_Struct_In_Income.newFieldInGroup("in_Income_Struct_In_Guaranteed_Income", "IN-GUARANTEED-INCOME", 
            FieldType.DECIMAL, 9,2);
        in_Income_Struct_In_Tiaa_Payment_Method = in_Income_Struct_In_Income.newFieldInGroup("in_Income_Struct_In_Tiaa_Payment_Method", "IN-TIAA-PAYMENT-METHOD", 
            FieldType.STRING, 8);
        in_Income_Struct_In_Fund_Name = in_Income_Struct_In_Income.newFieldInGroup("in_Income_Struct_In_Fund_Name", "IN-FUND-NAME", FieldType.STRING, 
            30);
        in_Income_Struct_In_Units = in_Income_Struct_In_Income.newFieldInGroup("in_Income_Struct_In_Units", "IN-UNITS", FieldType.DECIMAL, 8,3);
        in_Income_Struct_In_Unit_Value = in_Income_Struct_In_Income.newFieldInGroup("in_Income_Struct_In_Unit_Value", "IN-UNIT-VALUE", FieldType.DECIMAL, 
            9,4);
        in_Income_Struct_In_Change_Method = in_Income_Struct_In_Income.newFieldInGroup("in_Income_Struct_In_Change_Method", "IN-CHANGE-METHOD", FieldType.STRING, 
            1);

        ms_Messages_Struct = newGroupInRecord("ms_Messages_Struct", "MS-MESSAGES-STRUCT");
        ms_Messages_Struct_Ms_Record_Id = ms_Messages_Struct.newFieldInGroup("ms_Messages_Struct_Ms_Record_Id", "MS-RECORD-ID", FieldType.STRING, 2);
        ms_Messages_Struct_Ms_Data_Lgth = ms_Messages_Struct.newFieldInGroup("ms_Messages_Struct_Ms_Data_Lgth", "MS-DATA-LGTH", FieldType.NUMERIC, 5);
        ms_Messages_Struct_Ms_Data_Occurs = ms_Messages_Struct.newFieldInGroup("ms_Messages_Struct_Ms_Data_Occurs", "MS-DATA-OCCURS", FieldType.NUMERIC, 
            5);
        ms_Messages_Struct_Ms_Data = ms_Messages_Struct.newFieldArrayInGroup("ms_Messages_Struct_Ms_Data", "MS-DATA", FieldType.STRING, 1, new DbsArrayController(1,
            240));
        ms_Messages_Struct_Ms_DataRedef11 = ms_Messages_Struct.newGroupInGroup("ms_Messages_Struct_Ms_DataRedef11", "Redefines", ms_Messages_Struct_Ms_Data);
        ms_Messages_Struct_Ms_Data_R = ms_Messages_Struct_Ms_DataRedef11.newFieldArrayInGroup("ms_Messages_Struct_Ms_Data_R", "MS-DATA-R", FieldType.STRING, 
            24, new DbsArrayController(1,10));
        ms_Messages_Struct_Ms_DataRedef12 = ms_Messages_Struct.newGroupInGroup("ms_Messages_Struct_Ms_DataRedef12", "Redefines", ms_Messages_Struct_Ms_Data);
        ms_Messages_Struct_Ms_Messages = ms_Messages_Struct_Ms_DataRedef12.newGroupArrayInGroup("ms_Messages_Struct_Ms_Messages", "MS-MESSAGES", new DbsArrayController(1,
            10));
        ms_Messages_Struct_Ms_Message_Date = ms_Messages_Struct_Ms_Messages.newFieldInGroup("ms_Messages_Struct_Ms_Message_Date", "MS-MESSAGE-DATE", FieldType.NUMERIC, 
            8);
        ms_Messages_Struct_Ms_Message_Id = ms_Messages_Struct_Ms_Messages.newFieldInGroup("ms_Messages_Struct_Ms_Message_Id", "MS-MESSAGE-ID", FieldType.STRING, 
            1);
        ms_Messages_Struct_Ms_Payment_Frequency = ms_Messages_Struct_Ms_Messages.newFieldInGroup("ms_Messages_Struct_Ms_Payment_Frequency", "MS-PAYMENT-FREQUENCY", 
            FieldType.STRING, 15);

        this.setRecordName("LdaPstl6075");
    }

    public void initializeValues() throws Exception
    {
        reset();
        sort_Key_Struct_Sort_Key_Lgth.setInitialValue(32);
        ct_Contract_Struct_Ct_Record_Id.setInitialValue("03");
        ct_Contract_Struct_Ct_Data_Lgth.setInitialValue(35);
        ct_Contract_Struct_Ct_Data_Occurs.setInitialValue(1);
        ac_Accumulation_Info_Struct_Ac_Record_Id.setInitialValue("04");
        ac_Accumulation_Info_Struct_Ac_Data_Lgth.setInitialValue(18);
        ac_Accumulation_Info_Struct_Ac_Data_Occurs.setInitialValue(1);
        in_Income_Struct_In_Record_Id.setInitialValue("05");
        in_Income_Struct_In_Data_Lgth.setInitialValue(74);
        in_Income_Struct_In_Data_Occurs.setInitialValue(1);
        ms_Messages_Struct_Ms_Record_Id.setInitialValue("06");
        ms_Messages_Struct_Ms_Data_Lgth.setInitialValue(24);
        ms_Messages_Struct_Ms_Data_Occurs.setInitialValue(1);
    }

    // Constructor
    public LdaPstl6075() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
