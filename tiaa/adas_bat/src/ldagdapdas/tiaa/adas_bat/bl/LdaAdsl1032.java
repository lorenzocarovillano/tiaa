/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:49:22 PM
**        * FROM NATURAL LDA     : ADSL1032
************************************************************
**        * FILE NAME            : LdaAdsl1032.java
**        * CLASS NAME           : LdaAdsl1032
**        * INSTANCE NAME        : LdaAdsl1032
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdsl1032 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_S_Tiaa_Minor_Accum;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Pay_Cnt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Units;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Dollars;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Gtd_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Div_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Gtd_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Div_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Ivc_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Pay_Cnt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Units;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Dollars;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Gtd_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Div_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Gtd_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Div_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Ivc_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Pay_Cnt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Units;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Dollars;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Gtd_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Div_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Gtd_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Div_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Ivc_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Pay_Cnt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Units;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Dollars;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Gtd_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Div_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Gtd_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Div_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Ivc_Amt;
    private DbsGroup pnd_S_Tiaa_Sub_Accum;
    private DbsField pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Pay_Cnt;
    private DbsField pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Units;
    private DbsField pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Dollars;
    private DbsField pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Gtd_Amt;
    private DbsField pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Divid_Amt;
    private DbsField pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Gtd_Amt;
    private DbsField pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Div_Amt;
    private DbsField pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Ivc_Amt;
    private DbsGroup pnd_S_C_Mth_Minor_Accum;
    private DbsField pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt;
    private DbsField pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units;
    private DbsField pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars;
    private DbsField pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt;
    private DbsField pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt;
    private DbsField pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt;
    private DbsField pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt;
    private DbsGroup pnd_S_C_Mth_Fin_Ivc_Amount;
    private DbsField pnd_S_C_Mth_Fin_Ivc_Amount_Pnd_S_C_Mth_Fin_Ivc_Amt;
    private DbsGroup pnd_S_C_Ann_Minor_Accum;
    private DbsField pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt;
    private DbsField pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units;
    private DbsField pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars;
    private DbsField pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt;
    private DbsField pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt;
    private DbsField pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt;
    private DbsField pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt;
    private DbsGroup pnd_S_C_Ann_Fin_Ivc_Amount;
    private DbsField pnd_S_C_Ann_Fin_Ivc_Amount_Pnd_S_C_Ann_Fin_Ivc_Amt;
    private DbsGroup pnd_S_Cref_Mth_Sub_Total_Accum;
    private DbsField pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Pay_Cnt;
    private DbsField pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Units;
    private DbsField pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Dollars;
    private DbsField pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Gtd_Amt;
    private DbsField pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Div_Amt;
    private DbsField pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Gtd_Amt;
    private DbsField pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Div_Amt;
    private DbsField pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Ivc_Amt;
    private DbsGroup pnd_S_Cref_Ann_Sub_Total_Accum;
    private DbsField pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Pay_Cnt;
    private DbsField pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Units;
    private DbsField pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Dollars;
    private DbsField pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Gtd_Amt;
    private DbsField pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Div_Amt;
    private DbsField pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Gtd_Amt;
    private DbsField pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Div_Amt;
    private DbsField pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Ivc_Amt;
    private DbsGroup pnd_S_Total_Tiaa_Cref_Accum;
    private DbsField pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Pay_Cnt;
    private DbsField pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Units;
    private DbsField pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Dollars;
    private DbsField pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Gtd_Amt;
    private DbsField pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Div_Amt;
    private DbsField pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Gtd_Amt;
    private DbsField pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Div_Amt;
    private DbsField pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Ivc_Amt;

    public DbsGroup getPnd_S_Tiaa_Minor_Accum() { return pnd_S_Tiaa_Minor_Accum; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Pay_Cnt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Pay_Cnt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Units() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Units; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Dollars() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Dollars; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Gtd_Amt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Gtd_Amt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Div_Amt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Div_Amt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Gtd_Amt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Gtd_Amt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Div_Amt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Div_Amt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Ivc_Amt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Ivc_Amt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Pay_Cnt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Pay_Cnt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Units() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Units; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Dollars() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Dollars; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Gtd_Amt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Gtd_Amt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Div_Amt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Div_Amt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Gtd_Amt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Gtd_Amt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Div_Amt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Div_Amt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Ivc_Amt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Ivc_Amt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Pay_Cnt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Pay_Cnt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Units() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Units; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Dollars() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Dollars; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Gtd_Amt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Gtd_Amt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Div_Amt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Div_Amt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Gtd_Amt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Gtd_Amt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Div_Amt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Div_Amt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Ivc_Amt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Ivc_Amt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Pay_Cnt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Pay_Cnt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Units() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Units; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Dollars() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Dollars; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Gtd_Amt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Gtd_Amt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Div_Amt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Div_Amt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Gtd_Amt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Gtd_Amt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Div_Amt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Div_Amt; }

    public DbsField getPnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Ivc_Amt() { return pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Ivc_Amt; }

    public DbsGroup getPnd_S_Tiaa_Sub_Accum() { return pnd_S_Tiaa_Sub_Accum; }

    public DbsField getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Pay_Cnt() { return pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Pay_Cnt; }

    public DbsField getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Units() { return pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Units; }

    public DbsField getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Dollars() { return pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Dollars; }

    public DbsField getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Gtd_Amt() { return pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Gtd_Amt; }

    public DbsField getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Divid_Amt() { return pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Divid_Amt; }

    public DbsField getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Gtd_Amt() { return pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Gtd_Amt; }

    public DbsField getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Div_Amt() { return pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Div_Amt; }

    public DbsField getPnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Ivc_Amt() { return pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Ivc_Amt; }

    public DbsGroup getPnd_S_C_Mth_Minor_Accum() { return pnd_S_C_Mth_Minor_Accum; }

    public DbsField getPnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt() { return pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt; }

    public DbsField getPnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units() { return pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units; }

    public DbsField getPnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars() { return pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars; }

    public DbsField getPnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt() { return pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt; }

    public DbsField getPnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt() { return pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt; }

    public DbsField getPnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt() { return pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt; }

    public DbsField getPnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt() { return pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt; }

    public DbsGroup getPnd_S_C_Mth_Fin_Ivc_Amount() { return pnd_S_C_Mth_Fin_Ivc_Amount; }

    public DbsField getPnd_S_C_Mth_Fin_Ivc_Amount_Pnd_S_C_Mth_Fin_Ivc_Amt() { return pnd_S_C_Mth_Fin_Ivc_Amount_Pnd_S_C_Mth_Fin_Ivc_Amt; }

    public DbsGroup getPnd_S_C_Ann_Minor_Accum() { return pnd_S_C_Ann_Minor_Accum; }

    public DbsField getPnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt() { return pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt; }

    public DbsField getPnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units() { return pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units; }

    public DbsField getPnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars() { return pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars; }

    public DbsField getPnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt() { return pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt; }

    public DbsField getPnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt() { return pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt; }

    public DbsField getPnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt() { return pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt; }

    public DbsField getPnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt() { return pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt; }

    public DbsGroup getPnd_S_C_Ann_Fin_Ivc_Amount() { return pnd_S_C_Ann_Fin_Ivc_Amount; }

    public DbsField getPnd_S_C_Ann_Fin_Ivc_Amount_Pnd_S_C_Ann_Fin_Ivc_Amt() { return pnd_S_C_Ann_Fin_Ivc_Amount_Pnd_S_C_Ann_Fin_Ivc_Amt; }

    public DbsGroup getPnd_S_Cref_Mth_Sub_Total_Accum() { return pnd_S_Cref_Mth_Sub_Total_Accum; }

    public DbsField getPnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Pay_Cnt() { return pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Pay_Cnt; 
        }

    public DbsField getPnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Units() { return pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Units; }

    public DbsField getPnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Dollars() { return pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Dollars; 
        }

    public DbsField getPnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Gtd_Amt() { return pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Gtd_Amt; 
        }

    public DbsField getPnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Div_Amt() { return pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Div_Amt; 
        }

    public DbsField getPnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Gtd_Amt() { return pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Gtd_Amt; 
        }

    public DbsField getPnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Div_Amt() { return pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Div_Amt; 
        }

    public DbsField getPnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Ivc_Amt() { return pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Ivc_Amt; 
        }

    public DbsGroup getPnd_S_Cref_Ann_Sub_Total_Accum() { return pnd_S_Cref_Ann_Sub_Total_Accum; }

    public DbsField getPnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Pay_Cnt() { return pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Pay_Cnt; 
        }

    public DbsField getPnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Units() { return pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Units; }

    public DbsField getPnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Dollars() { return pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Dollars; 
        }

    public DbsField getPnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Gtd_Amt() { return pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Gtd_Amt; 
        }

    public DbsField getPnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Div_Amt() { return pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Div_Amt; 
        }

    public DbsField getPnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Gtd_Amt() { return pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Gtd_Amt; 
        }

    public DbsField getPnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Div_Amt() { return pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Div_Amt; 
        }

    public DbsField getPnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Ivc_Amt() { return pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Ivc_Amt; 
        }

    public DbsGroup getPnd_S_Total_Tiaa_Cref_Accum() { return pnd_S_Total_Tiaa_Cref_Accum; }

    public DbsField getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Pay_Cnt() { return pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Pay_Cnt; }

    public DbsField getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Units() { return pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Units; }

    public DbsField getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Dollars() { return pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Dollars; }

    public DbsField getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Gtd_Amt() { return pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Gtd_Amt; }

    public DbsField getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Div_Amt() { return pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Div_Amt; }

    public DbsField getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Gtd_Amt() { return pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Gtd_Amt; }

    public DbsField getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Div_Amt() { return pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Div_Amt; }

    public DbsField getPnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Ivc_Amt() { return pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Ivc_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_S_Tiaa_Minor_Accum = newGroupInRecord("pnd_S_Tiaa_Minor_Accum", "#S-TIAA-MINOR-ACCUM");
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Pay_Cnt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Pay_Cnt", "#S-TIAA-STD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Units = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Units", "#S-TIAA-STD-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Dollars = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Dollars", "#S-TIAA-STD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Gtd_Amt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Gtd_Amt", "#S-TIAA-STD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Div_Amt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Div_Amt", "#S-TIAA-STD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Gtd_Amt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Gtd_Amt", 
            "#S-TIAA-STD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Div_Amt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Div_Amt", 
            "#S-TIAA-STD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Ivc_Amt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Ivc_Amt", 
            "#S-TIAA-STD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Pay_Cnt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Pay_Cnt", "#S-TIAA-GRD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Units = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Units", "#S-TIAA-GRD-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Dollars = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Dollars", "#S-TIAA-GRD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Gtd_Amt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Gtd_Amt", "#S-TIAA-GRD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Div_Amt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Div_Amt", "#S-TIAA-GRD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Gtd_Amt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Gtd_Amt", 
            "#S-TIAA-GRD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Div_Amt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Div_Amt", 
            "#S-TIAA-GRD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Ivc_Amt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Ivc_Amt", 
            "#S-TIAA-GRD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Pay_Cnt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Pay_Cnt", 
            "#S-TIAA-TPA-STD-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Units = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Units", "#S-TIAA-TPA-STD-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Dollars = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Dollars", 
            "#S-TIAA-TPA-STD-DOLLARS", FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Gtd_Amt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Gtd_Amt", 
            "#S-TIAA-TPA-STD-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Div_Amt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Div_Amt", 
            "#S-TIAA-TPA-STD-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Gtd_Amt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Gtd_Amt", 
            "#S-TIAA-TPA-STD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Div_Amt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Div_Amt", 
            "#S-TIAA-TPA-STD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Ivc_Amt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Ivc_Amt", 
            "#S-TIAA-TPA-STD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Pay_Cnt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Pay_Cnt", 
            "#S-TIAA-IPRO-STD-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Units = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Units", 
            "#S-TIAA-IPRO-STD-UNITS", FieldType.PACKED_DECIMAL, 10,4);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Dollars = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Dollars", 
            "#S-TIAA-IPRO-STD-DOLLARS", FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Gtd_Amt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Gtd_Amt", 
            "#S-TIAA-IPRO-STD-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Div_Amt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Div_Amt", 
            "#S-TIAA-IPRO-STD-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Gtd_Amt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Gtd_Amt", 
            "#S-TIAA-IPRO-STD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Div_Amt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Div_Amt", 
            "#S-TIAA-IPRO-STD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Ivc_Amt = pnd_S_Tiaa_Minor_Accum.newFieldInGroup("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Ivc_Amt", 
            "#S-TIAA-IPRO-STD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_S_Tiaa_Sub_Accum = newGroupInRecord("pnd_S_Tiaa_Sub_Accum", "#S-TIAA-SUB-ACCUM");
        pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Pay_Cnt = pnd_S_Tiaa_Sub_Accum.newFieldInGroup("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Pay_Cnt", "#S-TIAA-SUB-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Units = pnd_S_Tiaa_Sub_Accum.newFieldInGroup("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Units", "#S-TIAA-SUB-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Dollars = pnd_S_Tiaa_Sub_Accum.newFieldInGroup("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Dollars", "#S-TIAA-SUB-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Gtd_Amt = pnd_S_Tiaa_Sub_Accum.newFieldInGroup("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Gtd_Amt", "#S-TIAA-SUB-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Divid_Amt = pnd_S_Tiaa_Sub_Accum.newFieldInGroup("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Divid_Amt", "#S-TIAA-SUB-DIVID-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Gtd_Amt = pnd_S_Tiaa_Sub_Accum.newFieldInGroup("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Gtd_Amt", "#S-TIAA-SUB-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Div_Amt = pnd_S_Tiaa_Sub_Accum.newFieldInGroup("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Div_Amt", "#S-TIAA-SUB-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Ivc_Amt = pnd_S_Tiaa_Sub_Accum.newFieldInGroup("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Ivc_Amt", "#S-TIAA-SUB-FIN-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);

        pnd_S_C_Mth_Minor_Accum = newGroupArrayInRecord("pnd_S_C_Mth_Minor_Accum", "#S-C-MTH-MINOR-ACCUM", new DbsArrayController(1,20));
        pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt = pnd_S_C_Mth_Minor_Accum.newFieldInGroup("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt", "#S-C-MTH-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units = pnd_S_C_Mth_Minor_Accum.newFieldInGroup("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units", "#S-C-MTH-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars = pnd_S_C_Mth_Minor_Accum.newFieldInGroup("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars", "#S-C-MTH-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt = pnd_S_C_Mth_Minor_Accum.newFieldInGroup("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt", "#S-C-MTH-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt = pnd_S_C_Mth_Minor_Accum.newFieldInGroup("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt", "#S-C-MTH-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt = pnd_S_C_Mth_Minor_Accum.newFieldInGroup("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt", "#S-C-MTH-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt = pnd_S_C_Mth_Minor_Accum.newFieldInGroup("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt", "#S-C-MTH-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);

        pnd_S_C_Mth_Fin_Ivc_Amount = newGroupInRecord("pnd_S_C_Mth_Fin_Ivc_Amount", "#S-C-MTH-FIN-IVC-AMOUNT");
        pnd_S_C_Mth_Fin_Ivc_Amount_Pnd_S_C_Mth_Fin_Ivc_Amt = pnd_S_C_Mth_Fin_Ivc_Amount.newFieldInGroup("pnd_S_C_Mth_Fin_Ivc_Amount_Pnd_S_C_Mth_Fin_Ivc_Amt", 
            "#S-C-MTH-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_S_C_Ann_Minor_Accum = newGroupArrayInRecord("pnd_S_C_Ann_Minor_Accum", "#S-C-ANN-MINOR-ACCUM", new DbsArrayController(1,20));
        pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt = pnd_S_C_Ann_Minor_Accum.newFieldInGroup("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt", "#S-C-ANN-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units = pnd_S_C_Ann_Minor_Accum.newFieldInGroup("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units", "#S-C-ANN-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars = pnd_S_C_Ann_Minor_Accum.newFieldInGroup("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars", "#S-C-ANN-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt = pnd_S_C_Ann_Minor_Accum.newFieldInGroup("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt", "#S-C-ANN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt = pnd_S_C_Ann_Minor_Accum.newFieldInGroup("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt", "#S-C-ANN-DIVID-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt = pnd_S_C_Ann_Minor_Accum.newFieldInGroup("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt", "#S-C-ANN-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt = pnd_S_C_Ann_Minor_Accum.newFieldInGroup("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt", "#S-C-ANN-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);

        pnd_S_C_Ann_Fin_Ivc_Amount = newGroupInRecord("pnd_S_C_Ann_Fin_Ivc_Amount", "#S-C-ANN-FIN-IVC-AMOUNT");
        pnd_S_C_Ann_Fin_Ivc_Amount_Pnd_S_C_Ann_Fin_Ivc_Amt = pnd_S_C_Ann_Fin_Ivc_Amount.newFieldInGroup("pnd_S_C_Ann_Fin_Ivc_Amount_Pnd_S_C_Ann_Fin_Ivc_Amt", 
            "#S-C-ANN-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_S_Cref_Mth_Sub_Total_Accum = newGroupInRecord("pnd_S_Cref_Mth_Sub_Total_Accum", "#S-CREF-MTH-SUB-TOTAL-ACCUM");
        pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Pay_Cnt = pnd_S_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Pay_Cnt", 
            "#S-CREF-MTH-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Units = pnd_S_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Units", 
            "#S-CREF-MTH-SUB-UNITS", FieldType.PACKED_DECIMAL, 10,4);
        pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Dollars = pnd_S_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Dollars", 
            "#S-CREF-MTH-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 12,2);
        pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Gtd_Amt = pnd_S_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Gtd_Amt", 
            "#S-CREF-MTH-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Div_Amt = pnd_S_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Div_Amt", 
            "#S-CREF-MTH-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Gtd_Amt = pnd_S_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Gtd_Amt", 
            "#S-CREF-MTH-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Div_Amt = pnd_S_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Div_Amt", 
            "#S-CREF-MTH-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Ivc_Amt = pnd_S_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Ivc_Amt", 
            "#S-CREF-MTH-SUB-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_S_Cref_Ann_Sub_Total_Accum = newGroupInRecord("pnd_S_Cref_Ann_Sub_Total_Accum", "#S-CREF-ANN-SUB-TOTAL-ACCUM");
        pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Pay_Cnt = pnd_S_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Pay_Cnt", 
            "#S-CREF-ANN-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Units = pnd_S_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Units", 
            "#S-CREF-ANN-SUB-UNITS", FieldType.PACKED_DECIMAL, 10,4);
        pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Dollars = pnd_S_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Dollars", 
            "#S-CREF-ANN-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 12,2);
        pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Gtd_Amt = pnd_S_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Gtd_Amt", 
            "#S-CREF-ANN-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Div_Amt = pnd_S_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Div_Amt", 
            "#S-CREF-ANN-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Gtd_Amt = pnd_S_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Gtd_Amt", 
            "#S-CREF-ANN-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Div_Amt = pnd_S_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Div_Amt", 
            "#S-CREF-ANN-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Ivc_Amt = pnd_S_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Ivc_Amt", 
            "#S-CREF-ANN-SUB-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_S_Total_Tiaa_Cref_Accum = newGroupInRecord("pnd_S_Total_Tiaa_Cref_Accum", "#S-TOTAL-TIAA-CREF-ACCUM");
        pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Pay_Cnt = pnd_S_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Pay_Cnt", 
            "#S-TOTAL-PAY-CNT", FieldType.PACKED_DECIMAL, 10);
        pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Units = pnd_S_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Units", "#S-TOTAL-UNITS", 
            FieldType.PACKED_DECIMAL, 13,4);
        pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Dollars = pnd_S_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Dollars", 
            "#S-TOTAL-DOLLARS", FieldType.PACKED_DECIMAL, 14,2);
        pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Gtd_Amt = pnd_S_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Gtd_Amt", 
            "#S-TOTAL-GTD-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Div_Amt = pnd_S_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Div_Amt", 
            "#S-TOTAL-DIV-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Gtd_Amt = pnd_S_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Gtd_Amt", 
            "#S-TOTAL-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Div_Amt = pnd_S_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Div_Amt", 
            "#S-TOTAL-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Ivc_Amt = pnd_S_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Ivc_Amt", 
            "#S-TOTAL-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 14,2);

        this.setRecordName("LdaAdsl1032");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAdsl1032() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
