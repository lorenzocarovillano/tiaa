/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:49:58 PM
**        * FROM NATURAL LDA     : ADSL6005
************************************************************
**        * FILE NAME            : LdaAdsl6005.java
**        * CLASS NAME           : LdaAdsl6005
**        * INSTANCE NAME        : LdaAdsl6005
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdsl6005 extends DbsRecord
{
    // Properties
    private DbsField pnd_Total_T_Std_Guar_M;
    private DbsField pnd_Total_T_Std_Guar_Q;
    private DbsField pnd_Total_T_Std_Guar_S;
    private DbsField pnd_Total_T_Std_Guar_A;
    private DbsField pnd_Total_T_Std_Guar_T;
    private DbsField pnd_Total_T_Std_Div_M;
    private DbsField pnd_Total_T_Std_Div_Q;
    private DbsField pnd_Total_T_Std_Div_S;
    private DbsField pnd_Total_T_Std_Div_A;
    private DbsField pnd_Total_T_Std_Div_T;
    private DbsField pnd_Total_T_Std_M;
    private DbsField pnd_Total_T_Std_Q;
    private DbsField pnd_Total_T_Std_S;
    private DbsField pnd_Total_T_Std_A;
    private DbsField pnd_Total_T_Std_T;
    private DbsField pnd_Total_T_Grd_Guar_M;
    private DbsField pnd_Total_T_Grd_Guar_Q;
    private DbsField pnd_Total_T_Grd_Guar_S;
    private DbsField pnd_Total_T_Grd_Guar_A;
    private DbsField pnd_Total_T_Grd_Guar_T;
    private DbsField pnd_Total_T_Grd_Div_M;
    private DbsField pnd_Total_T_Grd_Div_Q;
    private DbsField pnd_Total_T_Grd_Div_S;
    private DbsField pnd_Total_T_Grd_Div_A;
    private DbsField pnd_Total_T_Grd_Div_T;
    private DbsField pnd_Total_T_Grd_M;
    private DbsField pnd_Total_T_Grd_Q;
    private DbsField pnd_Total_T_Grd_S;
    private DbsField pnd_Total_T_Grd_A;
    private DbsField pnd_Total_T_Grd_T;
    private DbsField pnd_Total_C_Ann_M;
    private DbsField pnd_Total_C_Ann_Q;
    private DbsField pnd_Total_C_Ann_S;
    private DbsField pnd_Total_C_Ann_A;
    private DbsField pnd_Total_C_Ann_T;
    private DbsField pnd_Total_M_Ann_M;
    private DbsField pnd_Total_M_Ann_Q;
    private DbsField pnd_Total_M_Ann_S;
    private DbsField pnd_Total_M_Ann_A;
    private DbsField pnd_Total_M_Ann_T;
    private DbsField pnd_Total_S_Ann_M;
    private DbsField pnd_Total_S_Ann_Q;
    private DbsField pnd_Total_S_Ann_S;
    private DbsField pnd_Total_S_Ann_A;
    private DbsField pnd_Total_S_Ann_T;
    private DbsField pnd_Total_B_Ann_M;
    private DbsField pnd_Total_B_Ann_Q;
    private DbsField pnd_Total_B_Ann_S;
    private DbsField pnd_Total_B_Ann_A;
    private DbsField pnd_Total_B_Ann_T;
    private DbsField pnd_Total_W_Ann_M;
    private DbsField pnd_Total_W_Ann_Q;
    private DbsField pnd_Total_W_Ann_S;
    private DbsField pnd_Total_W_Ann_A;
    private DbsField pnd_Total_W_Ann_T;
    private DbsField pnd_Total_L_Ann_M;
    private DbsField pnd_Total_L_Ann_Q;
    private DbsField pnd_Total_L_Ann_S;
    private DbsField pnd_Total_L_Ann_A;
    private DbsField pnd_Total_L_Ann_T;
    private DbsField pnd_Total_E_Ann_M;
    private DbsField pnd_Total_E_Ann_Q;
    private DbsField pnd_Total_E_Ann_S;
    private DbsField pnd_Total_E_Ann_A;
    private DbsField pnd_Total_E_Ann_T;
    private DbsField pnd_Total_R_Ann_M;
    private DbsField pnd_Total_R_Ann_Q;
    private DbsField pnd_Total_R_Ann_S;
    private DbsField pnd_Total_R_Ann_A;
    private DbsField pnd_Total_R_Ann_T;
    private DbsField pnd_Total_A_Ann_M;
    private DbsField pnd_Total_A_Ann_Q;
    private DbsField pnd_Total_A_Ann_S;
    private DbsField pnd_Total_A_Ann_A;
    private DbsField pnd_Total_A_Ann_T;
    private DbsField pnd_Total_I_Ann_M;
    private DbsField pnd_Total_I_Ann_Q;
    private DbsField pnd_Total_I_Ann_S;
    private DbsField pnd_Total_I_Ann_A;
    private DbsField pnd_Total_I_Ann_T;
    private DbsField pnd_Total_C_Mth_M;
    private DbsField pnd_Total_C_Mth_Q;
    private DbsField pnd_Total_C_Mth_S;
    private DbsField pnd_Total_C_Mth_A;
    private DbsField pnd_Total_C_Mth_T;
    private DbsField pnd_Total_M_Mth_M;
    private DbsField pnd_Total_M_Mth_Q;
    private DbsField pnd_Total_M_Mth_S;
    private DbsField pnd_Total_M_Mth_A;
    private DbsField pnd_Total_M_Mth_T;
    private DbsField pnd_Total_S_Mth_M;
    private DbsField pnd_Total_S_Mth_Q;
    private DbsField pnd_Total_S_Mth_S;
    private DbsField pnd_Total_S_Mth_A;
    private DbsField pnd_Total_S_Mth_T;
    private DbsField pnd_Total_B_Mth_M;
    private DbsField pnd_Total_B_Mth_Q;
    private DbsField pnd_Total_B_Mth_S;
    private DbsField pnd_Total_B_Mth_A;
    private DbsField pnd_Total_B_Mth_T;
    private DbsField pnd_Total_W_Mth_M;
    private DbsField pnd_Total_W_Mth_Q;
    private DbsField pnd_Total_W_Mth_S;
    private DbsField pnd_Total_W_Mth_A;
    private DbsField pnd_Total_W_Mth_T;
    private DbsField pnd_Total_L_Mth_M;
    private DbsField pnd_Total_L_Mth_Q;
    private DbsField pnd_Total_L_Mth_S;
    private DbsField pnd_Total_L_Mth_A;
    private DbsField pnd_Total_L_Mth_T;
    private DbsField pnd_Total_E_Mth_M;
    private DbsField pnd_Total_E_Mth_Q;
    private DbsField pnd_Total_E_Mth_S;
    private DbsField pnd_Total_E_Mth_A;
    private DbsField pnd_Total_E_Mth_T;
    private DbsField pnd_Total_R_Mth_M;
    private DbsField pnd_Total_R_Mth_Q;
    private DbsField pnd_Total_R_Mth_S;
    private DbsField pnd_Total_R_Mth_A;
    private DbsField pnd_Total_R_Mth_T;
    private DbsField pnd_Total_A_Mth_M;
    private DbsField pnd_Total_A_Mth_Q;
    private DbsField pnd_Total_A_Mth_S;
    private DbsField pnd_Total_A_Mth_A;
    private DbsField pnd_Total_A_Mth_T;
    private DbsField pnd_Total_I_Mth_M;
    private DbsField pnd_Total_I_Mth_Q;
    private DbsField pnd_Total_I_Mth_S;
    private DbsField pnd_Total_I_Mth_A;
    private DbsField pnd_Total_I_Mth_T;
    private DbsField pnd_Total_T_Issued_M;
    private DbsField pnd_Total_T_Issued_S;
    private DbsField pnd_Total_T_Issued_Q;
    private DbsField pnd_Total_T_Issued_A;
    private DbsField pnd_Total_T_Issued_T;
    private DbsField pnd_Total_T_Applied_M;
    private DbsField pnd_Total_T_Applied_S;
    private DbsField pnd_Total_T_Applied_Q;
    private DbsField pnd_Total_T_Applied_A;
    private DbsField pnd_Total_T_Applied_T;
    private DbsField pnd_Total_T_Income_M;
    private DbsField pnd_Total_T_Income_S;
    private DbsField pnd_Total_T_Income_Q;
    private DbsField pnd_Total_T_Income_A;
    private DbsField pnd_Total_T_Income_T;
    private DbsField pnd_Total_Tpa_Std_Guar_M;
    private DbsField pnd_Total_Tpa_Std_Guar_Q;
    private DbsField pnd_Total_Tpa_Std_Guar_S;
    private DbsField pnd_Total_Tpa_Std_Guar_A;
    private DbsField pnd_Total_Tpa_Std_Guar_T;
    private DbsField pnd_Total_Tpa_Std_Div_M;
    private DbsField pnd_Total_Tpa_Std_Div_Q;
    private DbsField pnd_Total_Tpa_Std_Div_S;
    private DbsField pnd_Total_Tpa_Std_Div_A;
    private DbsField pnd_Total_Tpa_Std_Div_T;
    private DbsField pnd_Total_Tpa_Std_M;
    private DbsField pnd_Total_Tpa_Std_Q;
    private DbsField pnd_Total_Tpa_Std_S;
    private DbsField pnd_Total_Tpa_Std_A;
    private DbsField pnd_Total_Tpa_Std_T;
    private DbsField pnd_Total_Io_Std_Guar_M;
    private DbsField pnd_Total_Io_Std_Guar_Q;
    private DbsField pnd_Total_Io_Std_Guar_S;
    private DbsField pnd_Total_Io_Std_Guar_A;
    private DbsField pnd_Total_Io_Std_Guar_T;
    private DbsField pnd_Total_Io_Std_Div_M;
    private DbsField pnd_Total_Io_Std_Div_Q;
    private DbsField pnd_Total_Io_Std_Div_S;
    private DbsField pnd_Total_Io_Std_Div_A;
    private DbsField pnd_Total_Io_Std_Div_T;
    private DbsField pnd_Total_Io_Std_M;
    private DbsField pnd_Total_Io_Std_Q;
    private DbsField pnd_Total_Io_Std_S;
    private DbsField pnd_Total_Io_Std_A;
    private DbsField pnd_Total_Io_Std_T;
    private DbsField pnd_Total_Tpa_Issued_M;
    private DbsField pnd_Total_Tpa_Issued_S;
    private DbsField pnd_Total_Tpa_Issued_Q;
    private DbsField pnd_Total_Tpa_Issued_A;
    private DbsField pnd_Total_Tpa_Issued_T;
    private DbsField pnd_Total_Tpa_Applied_M;
    private DbsField pnd_Total_Tpa_Applied_S;
    private DbsField pnd_Total_Tpa_Applied_Q;
    private DbsField pnd_Total_Tpa_Applied_A;
    private DbsField pnd_Total_Tpa_Applied_T;
    private DbsField pnd_Total_Tpa_Income_M;
    private DbsField pnd_Total_Tpa_Income_S;
    private DbsField pnd_Total_Tpa_Income_Q;
    private DbsField pnd_Total_Tpa_Income_A;
    private DbsField pnd_Total_Tpa_Income_T;
    private DbsField pnd_Total_Io_Issued_M;
    private DbsField pnd_Total_Io_Issued_S;
    private DbsField pnd_Total_Io_Issued_Q;
    private DbsField pnd_Total_Io_Issued_A;
    private DbsField pnd_Total_Io_Issued_T;
    private DbsField pnd_Total_Io_Applied_M;
    private DbsField pnd_Total_Io_Applied_S;
    private DbsField pnd_Total_Io_Applied_Q;
    private DbsField pnd_Total_Io_Applied_A;
    private DbsField pnd_Total_Io_Applied_T;
    private DbsField pnd_Total_Io_Income_M;
    private DbsField pnd_Total_Io_Income_S;
    private DbsField pnd_Total_Io_Income_Q;
    private DbsField pnd_Total_Io_Income_A;
    private DbsField pnd_Total_Io_Income_T;
    private DbsField pnd_Total_R_Issued_M;
    private DbsField pnd_Total_R_Issued_S;
    private DbsField pnd_Total_R_Issued_Q;
    private DbsField pnd_Total_R_Issued_A;
    private DbsField pnd_Total_R_Issued_T;
    private DbsField pnd_Total_R_Applied_M;
    private DbsField pnd_Total_R_Applied_S;
    private DbsField pnd_Total_R_Applied_Q;
    private DbsField pnd_Total_R_Applied_A;
    private DbsField pnd_Total_R_Applied_T;
    private DbsField pnd_Total_R_Income_M;
    private DbsField pnd_Total_R_Income_S;
    private DbsField pnd_Total_R_Income_Q;
    private DbsField pnd_Total_R_Income_A;
    private DbsField pnd_Total_R_Income_T;
    private DbsField pnd_Total_A_Issued_M;
    private DbsField pnd_Total_A_Issued_S;
    private DbsField pnd_Total_A_Issued_Q;
    private DbsField pnd_Total_A_Issued_A;
    private DbsField pnd_Total_A_Issued_T;
    private DbsField pnd_Total_A_Applied_M;
    private DbsField pnd_Total_A_Applied_S;
    private DbsField pnd_Total_A_Applied_Q;
    private DbsField pnd_Total_A_Applied_A;
    private DbsField pnd_Total_A_Applied_T;
    private DbsField pnd_Total_A_Income_M;
    private DbsField pnd_Total_A_Income_S;
    private DbsField pnd_Total_A_Income_Q;
    private DbsField pnd_Total_A_Income_A;
    private DbsField pnd_Total_A_Income_T;
    private DbsField pnd_Total_C_Issued_M;
    private DbsField pnd_Total_C_Issued_S;
    private DbsField pnd_Total_C_Issued_Q;
    private DbsField pnd_Total_C_Issued_A;
    private DbsField pnd_Total_C_Issued_T;
    private DbsField pnd_Total_C_Applied_M;
    private DbsField pnd_Total_C_Applied_S;
    private DbsField pnd_Total_C_Applied_Q;
    private DbsField pnd_Total_C_Applied_A;
    private DbsField pnd_Total_C_Applied_T;
    private DbsField pnd_Total_C_Income_M;
    private DbsField pnd_Total_C_Income_S;
    private DbsField pnd_Total_C_Income_Q;
    private DbsField pnd_Total_C_Income_A;
    private DbsField pnd_Total_C_Income_T;

    public DbsField getPnd_Total_T_Std_Guar_M() { return pnd_Total_T_Std_Guar_M; }

    public DbsField getPnd_Total_T_Std_Guar_Q() { return pnd_Total_T_Std_Guar_Q; }

    public DbsField getPnd_Total_T_Std_Guar_S() { return pnd_Total_T_Std_Guar_S; }

    public DbsField getPnd_Total_T_Std_Guar_A() { return pnd_Total_T_Std_Guar_A; }

    public DbsField getPnd_Total_T_Std_Guar_T() { return pnd_Total_T_Std_Guar_T; }

    public DbsField getPnd_Total_T_Std_Div_M() { return pnd_Total_T_Std_Div_M; }

    public DbsField getPnd_Total_T_Std_Div_Q() { return pnd_Total_T_Std_Div_Q; }

    public DbsField getPnd_Total_T_Std_Div_S() { return pnd_Total_T_Std_Div_S; }

    public DbsField getPnd_Total_T_Std_Div_A() { return pnd_Total_T_Std_Div_A; }

    public DbsField getPnd_Total_T_Std_Div_T() { return pnd_Total_T_Std_Div_T; }

    public DbsField getPnd_Total_T_Std_M() { return pnd_Total_T_Std_M; }

    public DbsField getPnd_Total_T_Std_Q() { return pnd_Total_T_Std_Q; }

    public DbsField getPnd_Total_T_Std_S() { return pnd_Total_T_Std_S; }

    public DbsField getPnd_Total_T_Std_A() { return pnd_Total_T_Std_A; }

    public DbsField getPnd_Total_T_Std_T() { return pnd_Total_T_Std_T; }

    public DbsField getPnd_Total_T_Grd_Guar_M() { return pnd_Total_T_Grd_Guar_M; }

    public DbsField getPnd_Total_T_Grd_Guar_Q() { return pnd_Total_T_Grd_Guar_Q; }

    public DbsField getPnd_Total_T_Grd_Guar_S() { return pnd_Total_T_Grd_Guar_S; }

    public DbsField getPnd_Total_T_Grd_Guar_A() { return pnd_Total_T_Grd_Guar_A; }

    public DbsField getPnd_Total_T_Grd_Guar_T() { return pnd_Total_T_Grd_Guar_T; }

    public DbsField getPnd_Total_T_Grd_Div_M() { return pnd_Total_T_Grd_Div_M; }

    public DbsField getPnd_Total_T_Grd_Div_Q() { return pnd_Total_T_Grd_Div_Q; }

    public DbsField getPnd_Total_T_Grd_Div_S() { return pnd_Total_T_Grd_Div_S; }

    public DbsField getPnd_Total_T_Grd_Div_A() { return pnd_Total_T_Grd_Div_A; }

    public DbsField getPnd_Total_T_Grd_Div_T() { return pnd_Total_T_Grd_Div_T; }

    public DbsField getPnd_Total_T_Grd_M() { return pnd_Total_T_Grd_M; }

    public DbsField getPnd_Total_T_Grd_Q() { return pnd_Total_T_Grd_Q; }

    public DbsField getPnd_Total_T_Grd_S() { return pnd_Total_T_Grd_S; }

    public DbsField getPnd_Total_T_Grd_A() { return pnd_Total_T_Grd_A; }

    public DbsField getPnd_Total_T_Grd_T() { return pnd_Total_T_Grd_T; }

    public DbsField getPnd_Total_C_Ann_M() { return pnd_Total_C_Ann_M; }

    public DbsField getPnd_Total_C_Ann_Q() { return pnd_Total_C_Ann_Q; }

    public DbsField getPnd_Total_C_Ann_S() { return pnd_Total_C_Ann_S; }

    public DbsField getPnd_Total_C_Ann_A() { return pnd_Total_C_Ann_A; }

    public DbsField getPnd_Total_C_Ann_T() { return pnd_Total_C_Ann_T; }

    public DbsField getPnd_Total_M_Ann_M() { return pnd_Total_M_Ann_M; }

    public DbsField getPnd_Total_M_Ann_Q() { return pnd_Total_M_Ann_Q; }

    public DbsField getPnd_Total_M_Ann_S() { return pnd_Total_M_Ann_S; }

    public DbsField getPnd_Total_M_Ann_A() { return pnd_Total_M_Ann_A; }

    public DbsField getPnd_Total_M_Ann_T() { return pnd_Total_M_Ann_T; }

    public DbsField getPnd_Total_S_Ann_M() { return pnd_Total_S_Ann_M; }

    public DbsField getPnd_Total_S_Ann_Q() { return pnd_Total_S_Ann_Q; }

    public DbsField getPnd_Total_S_Ann_S() { return pnd_Total_S_Ann_S; }

    public DbsField getPnd_Total_S_Ann_A() { return pnd_Total_S_Ann_A; }

    public DbsField getPnd_Total_S_Ann_T() { return pnd_Total_S_Ann_T; }

    public DbsField getPnd_Total_B_Ann_M() { return pnd_Total_B_Ann_M; }

    public DbsField getPnd_Total_B_Ann_Q() { return pnd_Total_B_Ann_Q; }

    public DbsField getPnd_Total_B_Ann_S() { return pnd_Total_B_Ann_S; }

    public DbsField getPnd_Total_B_Ann_A() { return pnd_Total_B_Ann_A; }

    public DbsField getPnd_Total_B_Ann_T() { return pnd_Total_B_Ann_T; }

    public DbsField getPnd_Total_W_Ann_M() { return pnd_Total_W_Ann_M; }

    public DbsField getPnd_Total_W_Ann_Q() { return pnd_Total_W_Ann_Q; }

    public DbsField getPnd_Total_W_Ann_S() { return pnd_Total_W_Ann_S; }

    public DbsField getPnd_Total_W_Ann_A() { return pnd_Total_W_Ann_A; }

    public DbsField getPnd_Total_W_Ann_T() { return pnd_Total_W_Ann_T; }

    public DbsField getPnd_Total_L_Ann_M() { return pnd_Total_L_Ann_M; }

    public DbsField getPnd_Total_L_Ann_Q() { return pnd_Total_L_Ann_Q; }

    public DbsField getPnd_Total_L_Ann_S() { return pnd_Total_L_Ann_S; }

    public DbsField getPnd_Total_L_Ann_A() { return pnd_Total_L_Ann_A; }

    public DbsField getPnd_Total_L_Ann_T() { return pnd_Total_L_Ann_T; }

    public DbsField getPnd_Total_E_Ann_M() { return pnd_Total_E_Ann_M; }

    public DbsField getPnd_Total_E_Ann_Q() { return pnd_Total_E_Ann_Q; }

    public DbsField getPnd_Total_E_Ann_S() { return pnd_Total_E_Ann_S; }

    public DbsField getPnd_Total_E_Ann_A() { return pnd_Total_E_Ann_A; }

    public DbsField getPnd_Total_E_Ann_T() { return pnd_Total_E_Ann_T; }

    public DbsField getPnd_Total_R_Ann_M() { return pnd_Total_R_Ann_M; }

    public DbsField getPnd_Total_R_Ann_Q() { return pnd_Total_R_Ann_Q; }

    public DbsField getPnd_Total_R_Ann_S() { return pnd_Total_R_Ann_S; }

    public DbsField getPnd_Total_R_Ann_A() { return pnd_Total_R_Ann_A; }

    public DbsField getPnd_Total_R_Ann_T() { return pnd_Total_R_Ann_T; }

    public DbsField getPnd_Total_A_Ann_M() { return pnd_Total_A_Ann_M; }

    public DbsField getPnd_Total_A_Ann_Q() { return pnd_Total_A_Ann_Q; }

    public DbsField getPnd_Total_A_Ann_S() { return pnd_Total_A_Ann_S; }

    public DbsField getPnd_Total_A_Ann_A() { return pnd_Total_A_Ann_A; }

    public DbsField getPnd_Total_A_Ann_T() { return pnd_Total_A_Ann_T; }

    public DbsField getPnd_Total_I_Ann_M() { return pnd_Total_I_Ann_M; }

    public DbsField getPnd_Total_I_Ann_Q() { return pnd_Total_I_Ann_Q; }

    public DbsField getPnd_Total_I_Ann_S() { return pnd_Total_I_Ann_S; }

    public DbsField getPnd_Total_I_Ann_A() { return pnd_Total_I_Ann_A; }

    public DbsField getPnd_Total_I_Ann_T() { return pnd_Total_I_Ann_T; }

    public DbsField getPnd_Total_C_Mth_M() { return pnd_Total_C_Mth_M; }

    public DbsField getPnd_Total_C_Mth_Q() { return pnd_Total_C_Mth_Q; }

    public DbsField getPnd_Total_C_Mth_S() { return pnd_Total_C_Mth_S; }

    public DbsField getPnd_Total_C_Mth_A() { return pnd_Total_C_Mth_A; }

    public DbsField getPnd_Total_C_Mth_T() { return pnd_Total_C_Mth_T; }

    public DbsField getPnd_Total_M_Mth_M() { return pnd_Total_M_Mth_M; }

    public DbsField getPnd_Total_M_Mth_Q() { return pnd_Total_M_Mth_Q; }

    public DbsField getPnd_Total_M_Mth_S() { return pnd_Total_M_Mth_S; }

    public DbsField getPnd_Total_M_Mth_A() { return pnd_Total_M_Mth_A; }

    public DbsField getPnd_Total_M_Mth_T() { return pnd_Total_M_Mth_T; }

    public DbsField getPnd_Total_S_Mth_M() { return pnd_Total_S_Mth_M; }

    public DbsField getPnd_Total_S_Mth_Q() { return pnd_Total_S_Mth_Q; }

    public DbsField getPnd_Total_S_Mth_S() { return pnd_Total_S_Mth_S; }

    public DbsField getPnd_Total_S_Mth_A() { return pnd_Total_S_Mth_A; }

    public DbsField getPnd_Total_S_Mth_T() { return pnd_Total_S_Mth_T; }

    public DbsField getPnd_Total_B_Mth_M() { return pnd_Total_B_Mth_M; }

    public DbsField getPnd_Total_B_Mth_Q() { return pnd_Total_B_Mth_Q; }

    public DbsField getPnd_Total_B_Mth_S() { return pnd_Total_B_Mth_S; }

    public DbsField getPnd_Total_B_Mth_A() { return pnd_Total_B_Mth_A; }

    public DbsField getPnd_Total_B_Mth_T() { return pnd_Total_B_Mth_T; }

    public DbsField getPnd_Total_W_Mth_M() { return pnd_Total_W_Mth_M; }

    public DbsField getPnd_Total_W_Mth_Q() { return pnd_Total_W_Mth_Q; }

    public DbsField getPnd_Total_W_Mth_S() { return pnd_Total_W_Mth_S; }

    public DbsField getPnd_Total_W_Mth_A() { return pnd_Total_W_Mth_A; }

    public DbsField getPnd_Total_W_Mth_T() { return pnd_Total_W_Mth_T; }

    public DbsField getPnd_Total_L_Mth_M() { return pnd_Total_L_Mth_M; }

    public DbsField getPnd_Total_L_Mth_Q() { return pnd_Total_L_Mth_Q; }

    public DbsField getPnd_Total_L_Mth_S() { return pnd_Total_L_Mth_S; }

    public DbsField getPnd_Total_L_Mth_A() { return pnd_Total_L_Mth_A; }

    public DbsField getPnd_Total_L_Mth_T() { return pnd_Total_L_Mth_T; }

    public DbsField getPnd_Total_E_Mth_M() { return pnd_Total_E_Mth_M; }

    public DbsField getPnd_Total_E_Mth_Q() { return pnd_Total_E_Mth_Q; }

    public DbsField getPnd_Total_E_Mth_S() { return pnd_Total_E_Mth_S; }

    public DbsField getPnd_Total_E_Mth_A() { return pnd_Total_E_Mth_A; }

    public DbsField getPnd_Total_E_Mth_T() { return pnd_Total_E_Mth_T; }

    public DbsField getPnd_Total_R_Mth_M() { return pnd_Total_R_Mth_M; }

    public DbsField getPnd_Total_R_Mth_Q() { return pnd_Total_R_Mth_Q; }

    public DbsField getPnd_Total_R_Mth_S() { return pnd_Total_R_Mth_S; }

    public DbsField getPnd_Total_R_Mth_A() { return pnd_Total_R_Mth_A; }

    public DbsField getPnd_Total_R_Mth_T() { return pnd_Total_R_Mth_T; }

    public DbsField getPnd_Total_A_Mth_M() { return pnd_Total_A_Mth_M; }

    public DbsField getPnd_Total_A_Mth_Q() { return pnd_Total_A_Mth_Q; }

    public DbsField getPnd_Total_A_Mth_S() { return pnd_Total_A_Mth_S; }

    public DbsField getPnd_Total_A_Mth_A() { return pnd_Total_A_Mth_A; }

    public DbsField getPnd_Total_A_Mth_T() { return pnd_Total_A_Mth_T; }

    public DbsField getPnd_Total_I_Mth_M() { return pnd_Total_I_Mth_M; }

    public DbsField getPnd_Total_I_Mth_Q() { return pnd_Total_I_Mth_Q; }

    public DbsField getPnd_Total_I_Mth_S() { return pnd_Total_I_Mth_S; }

    public DbsField getPnd_Total_I_Mth_A() { return pnd_Total_I_Mth_A; }

    public DbsField getPnd_Total_I_Mth_T() { return pnd_Total_I_Mth_T; }

    public DbsField getPnd_Total_T_Issued_M() { return pnd_Total_T_Issued_M; }

    public DbsField getPnd_Total_T_Issued_S() { return pnd_Total_T_Issued_S; }

    public DbsField getPnd_Total_T_Issued_Q() { return pnd_Total_T_Issued_Q; }

    public DbsField getPnd_Total_T_Issued_A() { return pnd_Total_T_Issued_A; }

    public DbsField getPnd_Total_T_Issued_T() { return pnd_Total_T_Issued_T; }

    public DbsField getPnd_Total_T_Applied_M() { return pnd_Total_T_Applied_M; }

    public DbsField getPnd_Total_T_Applied_S() { return pnd_Total_T_Applied_S; }

    public DbsField getPnd_Total_T_Applied_Q() { return pnd_Total_T_Applied_Q; }

    public DbsField getPnd_Total_T_Applied_A() { return pnd_Total_T_Applied_A; }

    public DbsField getPnd_Total_T_Applied_T() { return pnd_Total_T_Applied_T; }

    public DbsField getPnd_Total_T_Income_M() { return pnd_Total_T_Income_M; }

    public DbsField getPnd_Total_T_Income_S() { return pnd_Total_T_Income_S; }

    public DbsField getPnd_Total_T_Income_Q() { return pnd_Total_T_Income_Q; }

    public DbsField getPnd_Total_T_Income_A() { return pnd_Total_T_Income_A; }

    public DbsField getPnd_Total_T_Income_T() { return pnd_Total_T_Income_T; }

    public DbsField getPnd_Total_Tpa_Std_Guar_M() { return pnd_Total_Tpa_Std_Guar_M; }

    public DbsField getPnd_Total_Tpa_Std_Guar_Q() { return pnd_Total_Tpa_Std_Guar_Q; }

    public DbsField getPnd_Total_Tpa_Std_Guar_S() { return pnd_Total_Tpa_Std_Guar_S; }

    public DbsField getPnd_Total_Tpa_Std_Guar_A() { return pnd_Total_Tpa_Std_Guar_A; }

    public DbsField getPnd_Total_Tpa_Std_Guar_T() { return pnd_Total_Tpa_Std_Guar_T; }

    public DbsField getPnd_Total_Tpa_Std_Div_M() { return pnd_Total_Tpa_Std_Div_M; }

    public DbsField getPnd_Total_Tpa_Std_Div_Q() { return pnd_Total_Tpa_Std_Div_Q; }

    public DbsField getPnd_Total_Tpa_Std_Div_S() { return pnd_Total_Tpa_Std_Div_S; }

    public DbsField getPnd_Total_Tpa_Std_Div_A() { return pnd_Total_Tpa_Std_Div_A; }

    public DbsField getPnd_Total_Tpa_Std_Div_T() { return pnd_Total_Tpa_Std_Div_T; }

    public DbsField getPnd_Total_Tpa_Std_M() { return pnd_Total_Tpa_Std_M; }

    public DbsField getPnd_Total_Tpa_Std_Q() { return pnd_Total_Tpa_Std_Q; }

    public DbsField getPnd_Total_Tpa_Std_S() { return pnd_Total_Tpa_Std_S; }

    public DbsField getPnd_Total_Tpa_Std_A() { return pnd_Total_Tpa_Std_A; }

    public DbsField getPnd_Total_Tpa_Std_T() { return pnd_Total_Tpa_Std_T; }

    public DbsField getPnd_Total_Io_Std_Guar_M() { return pnd_Total_Io_Std_Guar_M; }

    public DbsField getPnd_Total_Io_Std_Guar_Q() { return pnd_Total_Io_Std_Guar_Q; }

    public DbsField getPnd_Total_Io_Std_Guar_S() { return pnd_Total_Io_Std_Guar_S; }

    public DbsField getPnd_Total_Io_Std_Guar_A() { return pnd_Total_Io_Std_Guar_A; }

    public DbsField getPnd_Total_Io_Std_Guar_T() { return pnd_Total_Io_Std_Guar_T; }

    public DbsField getPnd_Total_Io_Std_Div_M() { return pnd_Total_Io_Std_Div_M; }

    public DbsField getPnd_Total_Io_Std_Div_Q() { return pnd_Total_Io_Std_Div_Q; }

    public DbsField getPnd_Total_Io_Std_Div_S() { return pnd_Total_Io_Std_Div_S; }

    public DbsField getPnd_Total_Io_Std_Div_A() { return pnd_Total_Io_Std_Div_A; }

    public DbsField getPnd_Total_Io_Std_Div_T() { return pnd_Total_Io_Std_Div_T; }

    public DbsField getPnd_Total_Io_Std_M() { return pnd_Total_Io_Std_M; }

    public DbsField getPnd_Total_Io_Std_Q() { return pnd_Total_Io_Std_Q; }

    public DbsField getPnd_Total_Io_Std_S() { return pnd_Total_Io_Std_S; }

    public DbsField getPnd_Total_Io_Std_A() { return pnd_Total_Io_Std_A; }

    public DbsField getPnd_Total_Io_Std_T() { return pnd_Total_Io_Std_T; }

    public DbsField getPnd_Total_Tpa_Issued_M() { return pnd_Total_Tpa_Issued_M; }

    public DbsField getPnd_Total_Tpa_Issued_S() { return pnd_Total_Tpa_Issued_S; }

    public DbsField getPnd_Total_Tpa_Issued_Q() { return pnd_Total_Tpa_Issued_Q; }

    public DbsField getPnd_Total_Tpa_Issued_A() { return pnd_Total_Tpa_Issued_A; }

    public DbsField getPnd_Total_Tpa_Issued_T() { return pnd_Total_Tpa_Issued_T; }

    public DbsField getPnd_Total_Tpa_Applied_M() { return pnd_Total_Tpa_Applied_M; }

    public DbsField getPnd_Total_Tpa_Applied_S() { return pnd_Total_Tpa_Applied_S; }

    public DbsField getPnd_Total_Tpa_Applied_Q() { return pnd_Total_Tpa_Applied_Q; }

    public DbsField getPnd_Total_Tpa_Applied_A() { return pnd_Total_Tpa_Applied_A; }

    public DbsField getPnd_Total_Tpa_Applied_T() { return pnd_Total_Tpa_Applied_T; }

    public DbsField getPnd_Total_Tpa_Income_M() { return pnd_Total_Tpa_Income_M; }

    public DbsField getPnd_Total_Tpa_Income_S() { return pnd_Total_Tpa_Income_S; }

    public DbsField getPnd_Total_Tpa_Income_Q() { return pnd_Total_Tpa_Income_Q; }

    public DbsField getPnd_Total_Tpa_Income_A() { return pnd_Total_Tpa_Income_A; }

    public DbsField getPnd_Total_Tpa_Income_T() { return pnd_Total_Tpa_Income_T; }

    public DbsField getPnd_Total_Io_Issued_M() { return pnd_Total_Io_Issued_M; }

    public DbsField getPnd_Total_Io_Issued_S() { return pnd_Total_Io_Issued_S; }

    public DbsField getPnd_Total_Io_Issued_Q() { return pnd_Total_Io_Issued_Q; }

    public DbsField getPnd_Total_Io_Issued_A() { return pnd_Total_Io_Issued_A; }

    public DbsField getPnd_Total_Io_Issued_T() { return pnd_Total_Io_Issued_T; }

    public DbsField getPnd_Total_Io_Applied_M() { return pnd_Total_Io_Applied_M; }

    public DbsField getPnd_Total_Io_Applied_S() { return pnd_Total_Io_Applied_S; }

    public DbsField getPnd_Total_Io_Applied_Q() { return pnd_Total_Io_Applied_Q; }

    public DbsField getPnd_Total_Io_Applied_A() { return pnd_Total_Io_Applied_A; }

    public DbsField getPnd_Total_Io_Applied_T() { return pnd_Total_Io_Applied_T; }

    public DbsField getPnd_Total_Io_Income_M() { return pnd_Total_Io_Income_M; }

    public DbsField getPnd_Total_Io_Income_S() { return pnd_Total_Io_Income_S; }

    public DbsField getPnd_Total_Io_Income_Q() { return pnd_Total_Io_Income_Q; }

    public DbsField getPnd_Total_Io_Income_A() { return pnd_Total_Io_Income_A; }

    public DbsField getPnd_Total_Io_Income_T() { return pnd_Total_Io_Income_T; }

    public DbsField getPnd_Total_R_Issued_M() { return pnd_Total_R_Issued_M; }

    public DbsField getPnd_Total_R_Issued_S() { return pnd_Total_R_Issued_S; }

    public DbsField getPnd_Total_R_Issued_Q() { return pnd_Total_R_Issued_Q; }

    public DbsField getPnd_Total_R_Issued_A() { return pnd_Total_R_Issued_A; }

    public DbsField getPnd_Total_R_Issued_T() { return pnd_Total_R_Issued_T; }

    public DbsField getPnd_Total_R_Applied_M() { return pnd_Total_R_Applied_M; }

    public DbsField getPnd_Total_R_Applied_S() { return pnd_Total_R_Applied_S; }

    public DbsField getPnd_Total_R_Applied_Q() { return pnd_Total_R_Applied_Q; }

    public DbsField getPnd_Total_R_Applied_A() { return pnd_Total_R_Applied_A; }

    public DbsField getPnd_Total_R_Applied_T() { return pnd_Total_R_Applied_T; }

    public DbsField getPnd_Total_R_Income_M() { return pnd_Total_R_Income_M; }

    public DbsField getPnd_Total_R_Income_S() { return pnd_Total_R_Income_S; }

    public DbsField getPnd_Total_R_Income_Q() { return pnd_Total_R_Income_Q; }

    public DbsField getPnd_Total_R_Income_A() { return pnd_Total_R_Income_A; }

    public DbsField getPnd_Total_R_Income_T() { return pnd_Total_R_Income_T; }

    public DbsField getPnd_Total_A_Issued_M() { return pnd_Total_A_Issued_M; }

    public DbsField getPnd_Total_A_Issued_S() { return pnd_Total_A_Issued_S; }

    public DbsField getPnd_Total_A_Issued_Q() { return pnd_Total_A_Issued_Q; }

    public DbsField getPnd_Total_A_Issued_A() { return pnd_Total_A_Issued_A; }

    public DbsField getPnd_Total_A_Issued_T() { return pnd_Total_A_Issued_T; }

    public DbsField getPnd_Total_A_Applied_M() { return pnd_Total_A_Applied_M; }

    public DbsField getPnd_Total_A_Applied_S() { return pnd_Total_A_Applied_S; }

    public DbsField getPnd_Total_A_Applied_Q() { return pnd_Total_A_Applied_Q; }

    public DbsField getPnd_Total_A_Applied_A() { return pnd_Total_A_Applied_A; }

    public DbsField getPnd_Total_A_Applied_T() { return pnd_Total_A_Applied_T; }

    public DbsField getPnd_Total_A_Income_M() { return pnd_Total_A_Income_M; }

    public DbsField getPnd_Total_A_Income_S() { return pnd_Total_A_Income_S; }

    public DbsField getPnd_Total_A_Income_Q() { return pnd_Total_A_Income_Q; }

    public DbsField getPnd_Total_A_Income_A() { return pnd_Total_A_Income_A; }

    public DbsField getPnd_Total_A_Income_T() { return pnd_Total_A_Income_T; }

    public DbsField getPnd_Total_C_Issued_M() { return pnd_Total_C_Issued_M; }

    public DbsField getPnd_Total_C_Issued_S() { return pnd_Total_C_Issued_S; }

    public DbsField getPnd_Total_C_Issued_Q() { return pnd_Total_C_Issued_Q; }

    public DbsField getPnd_Total_C_Issued_A() { return pnd_Total_C_Issued_A; }

    public DbsField getPnd_Total_C_Issued_T() { return pnd_Total_C_Issued_T; }

    public DbsField getPnd_Total_C_Applied_M() { return pnd_Total_C_Applied_M; }

    public DbsField getPnd_Total_C_Applied_S() { return pnd_Total_C_Applied_S; }

    public DbsField getPnd_Total_C_Applied_Q() { return pnd_Total_C_Applied_Q; }

    public DbsField getPnd_Total_C_Applied_A() { return pnd_Total_C_Applied_A; }

    public DbsField getPnd_Total_C_Applied_T() { return pnd_Total_C_Applied_T; }

    public DbsField getPnd_Total_C_Income_M() { return pnd_Total_C_Income_M; }

    public DbsField getPnd_Total_C_Income_S() { return pnd_Total_C_Income_S; }

    public DbsField getPnd_Total_C_Income_Q() { return pnd_Total_C_Income_Q; }

    public DbsField getPnd_Total_C_Income_A() { return pnd_Total_C_Income_A; }

    public DbsField getPnd_Total_C_Income_T() { return pnd_Total_C_Income_T; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Total_T_Std_Guar_M = newFieldInRecord("pnd_Total_T_Std_Guar_M", "#TOTAL-T-STD-GUAR-M", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Std_Guar_Q = newFieldInRecord("pnd_Total_T_Std_Guar_Q", "#TOTAL-T-STD-GUAR-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Std_Guar_S = newFieldInRecord("pnd_Total_T_Std_Guar_S", "#TOTAL-T-STD-GUAR-S", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Std_Guar_A = newFieldInRecord("pnd_Total_T_Std_Guar_A", "#TOTAL-T-STD-GUAR-A", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Std_Guar_T = newFieldInRecord("pnd_Total_T_Std_Guar_T", "#TOTAL-T-STD-GUAR-T", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Std_Div_M = newFieldInRecord("pnd_Total_T_Std_Div_M", "#TOTAL-T-STD-DIV-M", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Std_Div_Q = newFieldInRecord("pnd_Total_T_Std_Div_Q", "#TOTAL-T-STD-DIV-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Std_Div_S = newFieldInRecord("pnd_Total_T_Std_Div_S", "#TOTAL-T-STD-DIV-S", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Std_Div_A = newFieldInRecord("pnd_Total_T_Std_Div_A", "#TOTAL-T-STD-DIV-A", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Std_Div_T = newFieldInRecord("pnd_Total_T_Std_Div_T", "#TOTAL-T-STD-DIV-T", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Std_M = newFieldInRecord("pnd_Total_T_Std_M", "#TOTAL-T-STD-M", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Std_Q = newFieldInRecord("pnd_Total_T_Std_Q", "#TOTAL-T-STD-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Std_S = newFieldInRecord("pnd_Total_T_Std_S", "#TOTAL-T-STD-S", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Std_A = newFieldInRecord("pnd_Total_T_Std_A", "#TOTAL-T-STD-A", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Std_T = newFieldInRecord("pnd_Total_T_Std_T", "#TOTAL-T-STD-T", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Grd_Guar_M = newFieldInRecord("pnd_Total_T_Grd_Guar_M", "#TOTAL-T-GRD-GUAR-M", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Grd_Guar_Q = newFieldInRecord("pnd_Total_T_Grd_Guar_Q", "#TOTAL-T-GRD-GUAR-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Grd_Guar_S = newFieldInRecord("pnd_Total_T_Grd_Guar_S", "#TOTAL-T-GRD-GUAR-S", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Grd_Guar_A = newFieldInRecord("pnd_Total_T_Grd_Guar_A", "#TOTAL-T-GRD-GUAR-A", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Grd_Guar_T = newFieldInRecord("pnd_Total_T_Grd_Guar_T", "#TOTAL-T-GRD-GUAR-T", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Grd_Div_M = newFieldInRecord("pnd_Total_T_Grd_Div_M", "#TOTAL-T-GRD-DIV-M", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Grd_Div_Q = newFieldInRecord("pnd_Total_T_Grd_Div_Q", "#TOTAL-T-GRD-DIV-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Grd_Div_S = newFieldInRecord("pnd_Total_T_Grd_Div_S", "#TOTAL-T-GRD-DIV-S", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Grd_Div_A = newFieldInRecord("pnd_Total_T_Grd_Div_A", "#TOTAL-T-GRD-DIV-A", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Grd_Div_T = newFieldInRecord("pnd_Total_T_Grd_Div_T", "#TOTAL-T-GRD-DIV-T", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Grd_M = newFieldInRecord("pnd_Total_T_Grd_M", "#TOTAL-T-GRD-M", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Grd_Q = newFieldInRecord("pnd_Total_T_Grd_Q", "#TOTAL-T-GRD-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Grd_S = newFieldInRecord("pnd_Total_T_Grd_S", "#TOTAL-T-GRD-S", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Grd_A = newFieldInRecord("pnd_Total_T_Grd_A", "#TOTAL-T-GRD-A", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Grd_T = newFieldInRecord("pnd_Total_T_Grd_T", "#TOTAL-T-GRD-T", FieldType.DECIMAL, 11,2);

        pnd_Total_C_Ann_M = newFieldInRecord("pnd_Total_C_Ann_M", "#TOTAL-C-ANN-M", FieldType.DECIMAL, 11,3);

        pnd_Total_C_Ann_Q = newFieldInRecord("pnd_Total_C_Ann_Q", "#TOTAL-C-ANN-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_C_Ann_S = newFieldInRecord("pnd_Total_C_Ann_S", "#TOTAL-C-ANN-S", FieldType.DECIMAL, 11,3);

        pnd_Total_C_Ann_A = newFieldInRecord("pnd_Total_C_Ann_A", "#TOTAL-C-ANN-A", FieldType.DECIMAL, 11,3);

        pnd_Total_C_Ann_T = newFieldInRecord("pnd_Total_C_Ann_T", "#TOTAL-C-ANN-T", FieldType.DECIMAL, 11,3);

        pnd_Total_M_Ann_M = newFieldInRecord("pnd_Total_M_Ann_M", "#TOTAL-M-ANN-M", FieldType.DECIMAL, 11,3);

        pnd_Total_M_Ann_Q = newFieldInRecord("pnd_Total_M_Ann_Q", "#TOTAL-M-ANN-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_M_Ann_S = newFieldInRecord("pnd_Total_M_Ann_S", "#TOTAL-M-ANN-S", FieldType.DECIMAL, 11,3);

        pnd_Total_M_Ann_A = newFieldInRecord("pnd_Total_M_Ann_A", "#TOTAL-M-ANN-A", FieldType.DECIMAL, 11,3);

        pnd_Total_M_Ann_T = newFieldInRecord("pnd_Total_M_Ann_T", "#TOTAL-M-ANN-T", FieldType.DECIMAL, 11,3);

        pnd_Total_S_Ann_M = newFieldInRecord("pnd_Total_S_Ann_M", "#TOTAL-S-ANN-M", FieldType.DECIMAL, 11,3);

        pnd_Total_S_Ann_Q = newFieldInRecord("pnd_Total_S_Ann_Q", "#TOTAL-S-ANN-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_S_Ann_S = newFieldInRecord("pnd_Total_S_Ann_S", "#TOTAL-S-ANN-S", FieldType.DECIMAL, 11,3);

        pnd_Total_S_Ann_A = newFieldInRecord("pnd_Total_S_Ann_A", "#TOTAL-S-ANN-A", FieldType.DECIMAL, 11,3);

        pnd_Total_S_Ann_T = newFieldInRecord("pnd_Total_S_Ann_T", "#TOTAL-S-ANN-T", FieldType.DECIMAL, 11,3);

        pnd_Total_B_Ann_M = newFieldInRecord("pnd_Total_B_Ann_M", "#TOTAL-B-ANN-M", FieldType.DECIMAL, 11,3);

        pnd_Total_B_Ann_Q = newFieldInRecord("pnd_Total_B_Ann_Q", "#TOTAL-B-ANN-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_B_Ann_S = newFieldInRecord("pnd_Total_B_Ann_S", "#TOTAL-B-ANN-S", FieldType.DECIMAL, 11,3);

        pnd_Total_B_Ann_A = newFieldInRecord("pnd_Total_B_Ann_A", "#TOTAL-B-ANN-A", FieldType.DECIMAL, 11,3);

        pnd_Total_B_Ann_T = newFieldInRecord("pnd_Total_B_Ann_T", "#TOTAL-B-ANN-T", FieldType.DECIMAL, 11,3);

        pnd_Total_W_Ann_M = newFieldInRecord("pnd_Total_W_Ann_M", "#TOTAL-W-ANN-M", FieldType.DECIMAL, 11,3);

        pnd_Total_W_Ann_Q = newFieldInRecord("pnd_Total_W_Ann_Q", "#TOTAL-W-ANN-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_W_Ann_S = newFieldInRecord("pnd_Total_W_Ann_S", "#TOTAL-W-ANN-S", FieldType.DECIMAL, 11,3);

        pnd_Total_W_Ann_A = newFieldInRecord("pnd_Total_W_Ann_A", "#TOTAL-W-ANN-A", FieldType.DECIMAL, 11,3);

        pnd_Total_W_Ann_T = newFieldInRecord("pnd_Total_W_Ann_T", "#TOTAL-W-ANN-T", FieldType.DECIMAL, 11,3);

        pnd_Total_L_Ann_M = newFieldInRecord("pnd_Total_L_Ann_M", "#TOTAL-L-ANN-M", FieldType.DECIMAL, 11,3);

        pnd_Total_L_Ann_Q = newFieldInRecord("pnd_Total_L_Ann_Q", "#TOTAL-L-ANN-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_L_Ann_S = newFieldInRecord("pnd_Total_L_Ann_S", "#TOTAL-L-ANN-S", FieldType.DECIMAL, 11,3);

        pnd_Total_L_Ann_A = newFieldInRecord("pnd_Total_L_Ann_A", "#TOTAL-L-ANN-A", FieldType.DECIMAL, 11,3);

        pnd_Total_L_Ann_T = newFieldInRecord("pnd_Total_L_Ann_T", "#TOTAL-L-ANN-T", FieldType.DECIMAL, 11,3);

        pnd_Total_E_Ann_M = newFieldInRecord("pnd_Total_E_Ann_M", "#TOTAL-E-ANN-M", FieldType.DECIMAL, 11,3);

        pnd_Total_E_Ann_Q = newFieldInRecord("pnd_Total_E_Ann_Q", "#TOTAL-E-ANN-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_E_Ann_S = newFieldInRecord("pnd_Total_E_Ann_S", "#TOTAL-E-ANN-S", FieldType.DECIMAL, 11,3);

        pnd_Total_E_Ann_A = newFieldInRecord("pnd_Total_E_Ann_A", "#TOTAL-E-ANN-A", FieldType.DECIMAL, 11,3);

        pnd_Total_E_Ann_T = newFieldInRecord("pnd_Total_E_Ann_T", "#TOTAL-E-ANN-T", FieldType.DECIMAL, 11,3);

        pnd_Total_R_Ann_M = newFieldInRecord("pnd_Total_R_Ann_M", "#TOTAL-R-ANN-M", FieldType.DECIMAL, 11,3);

        pnd_Total_R_Ann_Q = newFieldInRecord("pnd_Total_R_Ann_Q", "#TOTAL-R-ANN-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_R_Ann_S = newFieldInRecord("pnd_Total_R_Ann_S", "#TOTAL-R-ANN-S", FieldType.DECIMAL, 11,3);

        pnd_Total_R_Ann_A = newFieldInRecord("pnd_Total_R_Ann_A", "#TOTAL-R-ANN-A", FieldType.DECIMAL, 11,3);

        pnd_Total_R_Ann_T = newFieldInRecord("pnd_Total_R_Ann_T", "#TOTAL-R-ANN-T", FieldType.DECIMAL, 11,3);

        pnd_Total_A_Ann_M = newFieldInRecord("pnd_Total_A_Ann_M", "#TOTAL-A-ANN-M", FieldType.DECIMAL, 11,3);

        pnd_Total_A_Ann_Q = newFieldInRecord("pnd_Total_A_Ann_Q", "#TOTAL-A-ANN-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_A_Ann_S = newFieldInRecord("pnd_Total_A_Ann_S", "#TOTAL-A-ANN-S", FieldType.DECIMAL, 11,3);

        pnd_Total_A_Ann_A = newFieldInRecord("pnd_Total_A_Ann_A", "#TOTAL-A-ANN-A", FieldType.DECIMAL, 11,3);

        pnd_Total_A_Ann_T = newFieldInRecord("pnd_Total_A_Ann_T", "#TOTAL-A-ANN-T", FieldType.DECIMAL, 11,3);

        pnd_Total_I_Ann_M = newFieldInRecord("pnd_Total_I_Ann_M", "#TOTAL-I-ANN-M", FieldType.DECIMAL, 11,3);

        pnd_Total_I_Ann_Q = newFieldInRecord("pnd_Total_I_Ann_Q", "#TOTAL-I-ANN-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_I_Ann_S = newFieldInRecord("pnd_Total_I_Ann_S", "#TOTAL-I-ANN-S", FieldType.DECIMAL, 11,3);

        pnd_Total_I_Ann_A = newFieldInRecord("pnd_Total_I_Ann_A", "#TOTAL-I-ANN-A", FieldType.DECIMAL, 11,3);

        pnd_Total_I_Ann_T = newFieldInRecord("pnd_Total_I_Ann_T", "#TOTAL-I-ANN-T", FieldType.DECIMAL, 11,3);

        pnd_Total_C_Mth_M = newFieldInRecord("pnd_Total_C_Mth_M", "#TOTAL-C-MTH-M", FieldType.DECIMAL, 11,3);

        pnd_Total_C_Mth_Q = newFieldInRecord("pnd_Total_C_Mth_Q", "#TOTAL-C-MTH-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_C_Mth_S = newFieldInRecord("pnd_Total_C_Mth_S", "#TOTAL-C-MTH-S", FieldType.DECIMAL, 11,3);

        pnd_Total_C_Mth_A = newFieldInRecord("pnd_Total_C_Mth_A", "#TOTAL-C-MTH-A", FieldType.DECIMAL, 11,3);

        pnd_Total_C_Mth_T = newFieldInRecord("pnd_Total_C_Mth_T", "#TOTAL-C-MTH-T", FieldType.DECIMAL, 11,3);

        pnd_Total_M_Mth_M = newFieldInRecord("pnd_Total_M_Mth_M", "#TOTAL-M-MTH-M", FieldType.DECIMAL, 11,3);

        pnd_Total_M_Mth_Q = newFieldInRecord("pnd_Total_M_Mth_Q", "#TOTAL-M-MTH-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_M_Mth_S = newFieldInRecord("pnd_Total_M_Mth_S", "#TOTAL-M-MTH-S", FieldType.DECIMAL, 11,3);

        pnd_Total_M_Mth_A = newFieldInRecord("pnd_Total_M_Mth_A", "#TOTAL-M-MTH-A", FieldType.DECIMAL, 11,3);

        pnd_Total_M_Mth_T = newFieldInRecord("pnd_Total_M_Mth_T", "#TOTAL-M-MTH-T", FieldType.DECIMAL, 11,3);

        pnd_Total_S_Mth_M = newFieldInRecord("pnd_Total_S_Mth_M", "#TOTAL-S-MTH-M", FieldType.DECIMAL, 11,3);

        pnd_Total_S_Mth_Q = newFieldInRecord("pnd_Total_S_Mth_Q", "#TOTAL-S-MTH-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_S_Mth_S = newFieldInRecord("pnd_Total_S_Mth_S", "#TOTAL-S-MTH-S", FieldType.DECIMAL, 11,3);

        pnd_Total_S_Mth_A = newFieldInRecord("pnd_Total_S_Mth_A", "#TOTAL-S-MTH-A", FieldType.DECIMAL, 11,3);

        pnd_Total_S_Mth_T = newFieldInRecord("pnd_Total_S_Mth_T", "#TOTAL-S-MTH-T", FieldType.DECIMAL, 11,3);

        pnd_Total_B_Mth_M = newFieldInRecord("pnd_Total_B_Mth_M", "#TOTAL-B-MTH-M", FieldType.DECIMAL, 11,3);

        pnd_Total_B_Mth_Q = newFieldInRecord("pnd_Total_B_Mth_Q", "#TOTAL-B-MTH-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_B_Mth_S = newFieldInRecord("pnd_Total_B_Mth_S", "#TOTAL-B-MTH-S", FieldType.DECIMAL, 11,3);

        pnd_Total_B_Mth_A = newFieldInRecord("pnd_Total_B_Mth_A", "#TOTAL-B-MTH-A", FieldType.DECIMAL, 11,3);

        pnd_Total_B_Mth_T = newFieldInRecord("pnd_Total_B_Mth_T", "#TOTAL-B-MTH-T", FieldType.DECIMAL, 11,3);

        pnd_Total_W_Mth_M = newFieldInRecord("pnd_Total_W_Mth_M", "#TOTAL-W-MTH-M", FieldType.DECIMAL, 11,3);

        pnd_Total_W_Mth_Q = newFieldInRecord("pnd_Total_W_Mth_Q", "#TOTAL-W-MTH-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_W_Mth_S = newFieldInRecord("pnd_Total_W_Mth_S", "#TOTAL-W-MTH-S", FieldType.DECIMAL, 11,3);

        pnd_Total_W_Mth_A = newFieldInRecord("pnd_Total_W_Mth_A", "#TOTAL-W-MTH-A", FieldType.DECIMAL, 11,3);

        pnd_Total_W_Mth_T = newFieldInRecord("pnd_Total_W_Mth_T", "#TOTAL-W-MTH-T", FieldType.DECIMAL, 11,3);

        pnd_Total_L_Mth_M = newFieldInRecord("pnd_Total_L_Mth_M", "#TOTAL-L-MTH-M", FieldType.DECIMAL, 11,3);

        pnd_Total_L_Mth_Q = newFieldInRecord("pnd_Total_L_Mth_Q", "#TOTAL-L-MTH-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_L_Mth_S = newFieldInRecord("pnd_Total_L_Mth_S", "#TOTAL-L-MTH-S", FieldType.DECIMAL, 11,3);

        pnd_Total_L_Mth_A = newFieldInRecord("pnd_Total_L_Mth_A", "#TOTAL-L-MTH-A", FieldType.DECIMAL, 11,3);

        pnd_Total_L_Mth_T = newFieldInRecord("pnd_Total_L_Mth_T", "#TOTAL-L-MTH-T", FieldType.DECIMAL, 11,3);

        pnd_Total_E_Mth_M = newFieldInRecord("pnd_Total_E_Mth_M", "#TOTAL-E-MTH-M", FieldType.DECIMAL, 11,3);

        pnd_Total_E_Mth_Q = newFieldInRecord("pnd_Total_E_Mth_Q", "#TOTAL-E-MTH-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_E_Mth_S = newFieldInRecord("pnd_Total_E_Mth_S", "#TOTAL-E-MTH-S", FieldType.DECIMAL, 11,3);

        pnd_Total_E_Mth_A = newFieldInRecord("pnd_Total_E_Mth_A", "#TOTAL-E-MTH-A", FieldType.DECIMAL, 11,3);

        pnd_Total_E_Mth_T = newFieldInRecord("pnd_Total_E_Mth_T", "#TOTAL-E-MTH-T", FieldType.DECIMAL, 11,3);

        pnd_Total_R_Mth_M = newFieldInRecord("pnd_Total_R_Mth_M", "#TOTAL-R-MTH-M", FieldType.DECIMAL, 11,3);

        pnd_Total_R_Mth_Q = newFieldInRecord("pnd_Total_R_Mth_Q", "#TOTAL-R-MTH-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_R_Mth_S = newFieldInRecord("pnd_Total_R_Mth_S", "#TOTAL-R-MTH-S", FieldType.DECIMAL, 11,3);

        pnd_Total_R_Mth_A = newFieldInRecord("pnd_Total_R_Mth_A", "#TOTAL-R-MTH-A", FieldType.DECIMAL, 11,3);

        pnd_Total_R_Mth_T = newFieldInRecord("pnd_Total_R_Mth_T", "#TOTAL-R-MTH-T", FieldType.DECIMAL, 11,3);

        pnd_Total_A_Mth_M = newFieldInRecord("pnd_Total_A_Mth_M", "#TOTAL-A-MTH-M", FieldType.DECIMAL, 11,3);

        pnd_Total_A_Mth_Q = newFieldInRecord("pnd_Total_A_Mth_Q", "#TOTAL-A-MTH-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_A_Mth_S = newFieldInRecord("pnd_Total_A_Mth_S", "#TOTAL-A-MTH-S", FieldType.DECIMAL, 11,3);

        pnd_Total_A_Mth_A = newFieldInRecord("pnd_Total_A_Mth_A", "#TOTAL-A-MTH-A", FieldType.DECIMAL, 11,3);

        pnd_Total_A_Mth_T = newFieldInRecord("pnd_Total_A_Mth_T", "#TOTAL-A-MTH-T", FieldType.DECIMAL, 11,3);

        pnd_Total_I_Mth_M = newFieldInRecord("pnd_Total_I_Mth_M", "#TOTAL-I-MTH-M", FieldType.DECIMAL, 11,3);

        pnd_Total_I_Mth_Q = newFieldInRecord("pnd_Total_I_Mth_Q", "#TOTAL-I-MTH-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_I_Mth_S = newFieldInRecord("pnd_Total_I_Mth_S", "#TOTAL-I-MTH-S", FieldType.DECIMAL, 11,3);

        pnd_Total_I_Mth_A = newFieldInRecord("pnd_Total_I_Mth_A", "#TOTAL-I-MTH-A", FieldType.DECIMAL, 11,3);

        pnd_Total_I_Mth_T = newFieldInRecord("pnd_Total_I_Mth_T", "#TOTAL-I-MTH-T", FieldType.DECIMAL, 11,3);

        pnd_Total_T_Issued_M = newFieldInRecord("pnd_Total_T_Issued_M", "#TOTAL-T-ISSUED-M", FieldType.NUMERIC, 9);

        pnd_Total_T_Issued_S = newFieldInRecord("pnd_Total_T_Issued_S", "#TOTAL-T-ISSUED-S", FieldType.NUMERIC, 9);

        pnd_Total_T_Issued_Q = newFieldInRecord("pnd_Total_T_Issued_Q", "#TOTAL-T-ISSUED-Q", FieldType.NUMERIC, 9);

        pnd_Total_T_Issued_A = newFieldInRecord("pnd_Total_T_Issued_A", "#TOTAL-T-ISSUED-A", FieldType.NUMERIC, 9);

        pnd_Total_T_Issued_T = newFieldInRecord("pnd_Total_T_Issued_T", "#TOTAL-T-ISSUED-T", FieldType.NUMERIC, 9);

        pnd_Total_T_Applied_M = newFieldInRecord("pnd_Total_T_Applied_M", "#TOTAL-T-APPLIED-M", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Applied_S = newFieldInRecord("pnd_Total_T_Applied_S", "#TOTAL-T-APPLIED-S", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Applied_Q = newFieldInRecord("pnd_Total_T_Applied_Q", "#TOTAL-T-APPLIED-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Applied_A = newFieldInRecord("pnd_Total_T_Applied_A", "#TOTAL-T-APPLIED-A", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Applied_T = newFieldInRecord("pnd_Total_T_Applied_T", "#TOTAL-T-APPLIED-T", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Income_M = newFieldInRecord("pnd_Total_T_Income_M", "#TOTAL-T-INCOME-M", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Income_S = newFieldInRecord("pnd_Total_T_Income_S", "#TOTAL-T-INCOME-S", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Income_Q = newFieldInRecord("pnd_Total_T_Income_Q", "#TOTAL-T-INCOME-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Income_A = newFieldInRecord("pnd_Total_T_Income_A", "#TOTAL-T-INCOME-A", FieldType.DECIMAL, 11,2);

        pnd_Total_T_Income_T = newFieldInRecord("pnd_Total_T_Income_T", "#TOTAL-T-INCOME-T", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Std_Guar_M = newFieldInRecord("pnd_Total_Tpa_Std_Guar_M", "#TOTAL-TPA-STD-GUAR-M", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Std_Guar_Q = newFieldInRecord("pnd_Total_Tpa_Std_Guar_Q", "#TOTAL-TPA-STD-GUAR-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Std_Guar_S = newFieldInRecord("pnd_Total_Tpa_Std_Guar_S", "#TOTAL-TPA-STD-GUAR-S", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Std_Guar_A = newFieldInRecord("pnd_Total_Tpa_Std_Guar_A", "#TOTAL-TPA-STD-GUAR-A", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Std_Guar_T = newFieldInRecord("pnd_Total_Tpa_Std_Guar_T", "#TOTAL-TPA-STD-GUAR-T", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Std_Div_M = newFieldInRecord("pnd_Total_Tpa_Std_Div_M", "#TOTAL-TPA-STD-DIV-M", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Std_Div_Q = newFieldInRecord("pnd_Total_Tpa_Std_Div_Q", "#TOTAL-TPA-STD-DIV-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Std_Div_S = newFieldInRecord("pnd_Total_Tpa_Std_Div_S", "#TOTAL-TPA-STD-DIV-S", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Std_Div_A = newFieldInRecord("pnd_Total_Tpa_Std_Div_A", "#TOTAL-TPA-STD-DIV-A", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Std_Div_T = newFieldInRecord("pnd_Total_Tpa_Std_Div_T", "#TOTAL-TPA-STD-DIV-T", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Std_M = newFieldInRecord("pnd_Total_Tpa_Std_M", "#TOTAL-TPA-STD-M", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Std_Q = newFieldInRecord("pnd_Total_Tpa_Std_Q", "#TOTAL-TPA-STD-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Std_S = newFieldInRecord("pnd_Total_Tpa_Std_S", "#TOTAL-TPA-STD-S", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Std_A = newFieldInRecord("pnd_Total_Tpa_Std_A", "#TOTAL-TPA-STD-A", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Std_T = newFieldInRecord("pnd_Total_Tpa_Std_T", "#TOTAL-TPA-STD-T", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Std_Guar_M = newFieldInRecord("pnd_Total_Io_Std_Guar_M", "#TOTAL-IO-STD-GUAR-M", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Std_Guar_Q = newFieldInRecord("pnd_Total_Io_Std_Guar_Q", "#TOTAL-IO-STD-GUAR-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Std_Guar_S = newFieldInRecord("pnd_Total_Io_Std_Guar_S", "#TOTAL-IO-STD-GUAR-S", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Std_Guar_A = newFieldInRecord("pnd_Total_Io_Std_Guar_A", "#TOTAL-IO-STD-GUAR-A", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Std_Guar_T = newFieldInRecord("pnd_Total_Io_Std_Guar_T", "#TOTAL-IO-STD-GUAR-T", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Std_Div_M = newFieldInRecord("pnd_Total_Io_Std_Div_M", "#TOTAL-IO-STD-DIV-M", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Std_Div_Q = newFieldInRecord("pnd_Total_Io_Std_Div_Q", "#TOTAL-IO-STD-DIV-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Std_Div_S = newFieldInRecord("pnd_Total_Io_Std_Div_S", "#TOTAL-IO-STD-DIV-S", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Std_Div_A = newFieldInRecord("pnd_Total_Io_Std_Div_A", "#TOTAL-IO-STD-DIV-A", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Std_Div_T = newFieldInRecord("pnd_Total_Io_Std_Div_T", "#TOTAL-IO-STD-DIV-T", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Std_M = newFieldInRecord("pnd_Total_Io_Std_M", "#TOTAL-IO-STD-M", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Std_Q = newFieldInRecord("pnd_Total_Io_Std_Q", "#TOTAL-IO-STD-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Std_S = newFieldInRecord("pnd_Total_Io_Std_S", "#TOTAL-IO-STD-S", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Std_A = newFieldInRecord("pnd_Total_Io_Std_A", "#TOTAL-IO-STD-A", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Std_T = newFieldInRecord("pnd_Total_Io_Std_T", "#TOTAL-IO-STD-T", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Issued_M = newFieldInRecord("pnd_Total_Tpa_Issued_M", "#TOTAL-TPA-ISSUED-M", FieldType.NUMERIC, 9);

        pnd_Total_Tpa_Issued_S = newFieldInRecord("pnd_Total_Tpa_Issued_S", "#TOTAL-TPA-ISSUED-S", FieldType.NUMERIC, 9);

        pnd_Total_Tpa_Issued_Q = newFieldInRecord("pnd_Total_Tpa_Issued_Q", "#TOTAL-TPA-ISSUED-Q", FieldType.NUMERIC, 9);

        pnd_Total_Tpa_Issued_A = newFieldInRecord("pnd_Total_Tpa_Issued_A", "#TOTAL-TPA-ISSUED-A", FieldType.NUMERIC, 9);

        pnd_Total_Tpa_Issued_T = newFieldInRecord("pnd_Total_Tpa_Issued_T", "#TOTAL-TPA-ISSUED-T", FieldType.NUMERIC, 9);

        pnd_Total_Tpa_Applied_M = newFieldInRecord("pnd_Total_Tpa_Applied_M", "#TOTAL-TPA-APPLIED-M", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Applied_S = newFieldInRecord("pnd_Total_Tpa_Applied_S", "#TOTAL-TPA-APPLIED-S", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Applied_Q = newFieldInRecord("pnd_Total_Tpa_Applied_Q", "#TOTAL-TPA-APPLIED-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Applied_A = newFieldInRecord("pnd_Total_Tpa_Applied_A", "#TOTAL-TPA-APPLIED-A", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Applied_T = newFieldInRecord("pnd_Total_Tpa_Applied_T", "#TOTAL-TPA-APPLIED-T", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Income_M = newFieldInRecord("pnd_Total_Tpa_Income_M", "#TOTAL-TPA-INCOME-M", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Income_S = newFieldInRecord("pnd_Total_Tpa_Income_S", "#TOTAL-TPA-INCOME-S", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Income_Q = newFieldInRecord("pnd_Total_Tpa_Income_Q", "#TOTAL-TPA-INCOME-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Income_A = newFieldInRecord("pnd_Total_Tpa_Income_A", "#TOTAL-TPA-INCOME-A", FieldType.DECIMAL, 11,2);

        pnd_Total_Tpa_Income_T = newFieldInRecord("pnd_Total_Tpa_Income_T", "#TOTAL-TPA-INCOME-T", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Issued_M = newFieldInRecord("pnd_Total_Io_Issued_M", "#TOTAL-IO-ISSUED-M", FieldType.NUMERIC, 9);

        pnd_Total_Io_Issued_S = newFieldInRecord("pnd_Total_Io_Issued_S", "#TOTAL-IO-ISSUED-S", FieldType.NUMERIC, 9);

        pnd_Total_Io_Issued_Q = newFieldInRecord("pnd_Total_Io_Issued_Q", "#TOTAL-IO-ISSUED-Q", FieldType.NUMERIC, 9);

        pnd_Total_Io_Issued_A = newFieldInRecord("pnd_Total_Io_Issued_A", "#TOTAL-IO-ISSUED-A", FieldType.NUMERIC, 9);

        pnd_Total_Io_Issued_T = newFieldInRecord("pnd_Total_Io_Issued_T", "#TOTAL-IO-ISSUED-T", FieldType.NUMERIC, 9);

        pnd_Total_Io_Applied_M = newFieldInRecord("pnd_Total_Io_Applied_M", "#TOTAL-IO-APPLIED-M", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Applied_S = newFieldInRecord("pnd_Total_Io_Applied_S", "#TOTAL-IO-APPLIED-S", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Applied_Q = newFieldInRecord("pnd_Total_Io_Applied_Q", "#TOTAL-IO-APPLIED-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Applied_A = newFieldInRecord("pnd_Total_Io_Applied_A", "#TOTAL-IO-APPLIED-A", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Applied_T = newFieldInRecord("pnd_Total_Io_Applied_T", "#TOTAL-IO-APPLIED-T", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Income_M = newFieldInRecord("pnd_Total_Io_Income_M", "#TOTAL-IO-INCOME-M", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Income_S = newFieldInRecord("pnd_Total_Io_Income_S", "#TOTAL-IO-INCOME-S", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Income_Q = newFieldInRecord("pnd_Total_Io_Income_Q", "#TOTAL-IO-INCOME-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Income_A = newFieldInRecord("pnd_Total_Io_Income_A", "#TOTAL-IO-INCOME-A", FieldType.DECIMAL, 11,2);

        pnd_Total_Io_Income_T = newFieldInRecord("pnd_Total_Io_Income_T", "#TOTAL-IO-INCOME-T", FieldType.DECIMAL, 11,2);

        pnd_Total_R_Issued_M = newFieldInRecord("pnd_Total_R_Issued_M", "#TOTAL-R-ISSUED-M", FieldType.NUMERIC, 9);

        pnd_Total_R_Issued_S = newFieldInRecord("pnd_Total_R_Issued_S", "#TOTAL-R-ISSUED-S", FieldType.NUMERIC, 9);

        pnd_Total_R_Issued_Q = newFieldInRecord("pnd_Total_R_Issued_Q", "#TOTAL-R-ISSUED-Q", FieldType.NUMERIC, 9);

        pnd_Total_R_Issued_A = newFieldInRecord("pnd_Total_R_Issued_A", "#TOTAL-R-ISSUED-A", FieldType.NUMERIC, 9);

        pnd_Total_R_Issued_T = newFieldInRecord("pnd_Total_R_Issued_T", "#TOTAL-R-ISSUED-T", FieldType.NUMERIC, 9);

        pnd_Total_R_Applied_M = newFieldInRecord("pnd_Total_R_Applied_M", "#TOTAL-R-APPLIED-M", FieldType.DECIMAL, 11,2);

        pnd_Total_R_Applied_S = newFieldInRecord("pnd_Total_R_Applied_S", "#TOTAL-R-APPLIED-S", FieldType.DECIMAL, 11,2);

        pnd_Total_R_Applied_Q = newFieldInRecord("pnd_Total_R_Applied_Q", "#TOTAL-R-APPLIED-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_R_Applied_A = newFieldInRecord("pnd_Total_R_Applied_A", "#TOTAL-R-APPLIED-A", FieldType.DECIMAL, 11,2);

        pnd_Total_R_Applied_T = newFieldInRecord("pnd_Total_R_Applied_T", "#TOTAL-R-APPLIED-T", FieldType.DECIMAL, 11,2);

        pnd_Total_R_Income_M = newFieldInRecord("pnd_Total_R_Income_M", "#TOTAL-R-INCOME-M", FieldType.DECIMAL, 11,3);

        pnd_Total_R_Income_S = newFieldInRecord("pnd_Total_R_Income_S", "#TOTAL-R-INCOME-S", FieldType.DECIMAL, 11,3);

        pnd_Total_R_Income_Q = newFieldInRecord("pnd_Total_R_Income_Q", "#TOTAL-R-INCOME-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_R_Income_A = newFieldInRecord("pnd_Total_R_Income_A", "#TOTAL-R-INCOME-A", FieldType.DECIMAL, 11,3);

        pnd_Total_R_Income_T = newFieldInRecord("pnd_Total_R_Income_T", "#TOTAL-R-INCOME-T", FieldType.DECIMAL, 11,3);

        pnd_Total_A_Issued_M = newFieldInRecord("pnd_Total_A_Issued_M", "#TOTAL-A-ISSUED-M", FieldType.NUMERIC, 9);

        pnd_Total_A_Issued_S = newFieldInRecord("pnd_Total_A_Issued_S", "#TOTAL-A-ISSUED-S", FieldType.NUMERIC, 9);

        pnd_Total_A_Issued_Q = newFieldInRecord("pnd_Total_A_Issued_Q", "#TOTAL-A-ISSUED-Q", FieldType.NUMERIC, 9);

        pnd_Total_A_Issued_A = newFieldInRecord("pnd_Total_A_Issued_A", "#TOTAL-A-ISSUED-A", FieldType.NUMERIC, 9);

        pnd_Total_A_Issued_T = newFieldInRecord("pnd_Total_A_Issued_T", "#TOTAL-A-ISSUED-T", FieldType.NUMERIC, 9);

        pnd_Total_A_Applied_M = newFieldInRecord("pnd_Total_A_Applied_M", "#TOTAL-A-APPLIED-M", FieldType.DECIMAL, 11,2);

        pnd_Total_A_Applied_S = newFieldInRecord("pnd_Total_A_Applied_S", "#TOTAL-A-APPLIED-S", FieldType.DECIMAL, 11,2);

        pnd_Total_A_Applied_Q = newFieldInRecord("pnd_Total_A_Applied_Q", "#TOTAL-A-APPLIED-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_A_Applied_A = newFieldInRecord("pnd_Total_A_Applied_A", "#TOTAL-A-APPLIED-A", FieldType.DECIMAL, 11,2);

        pnd_Total_A_Applied_T = newFieldInRecord("pnd_Total_A_Applied_T", "#TOTAL-A-APPLIED-T", FieldType.DECIMAL, 11,2);

        pnd_Total_A_Income_M = newFieldInRecord("pnd_Total_A_Income_M", "#TOTAL-A-INCOME-M", FieldType.DECIMAL, 11,3);

        pnd_Total_A_Income_S = newFieldInRecord("pnd_Total_A_Income_S", "#TOTAL-A-INCOME-S", FieldType.DECIMAL, 11,3);

        pnd_Total_A_Income_Q = newFieldInRecord("pnd_Total_A_Income_Q", "#TOTAL-A-INCOME-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_A_Income_A = newFieldInRecord("pnd_Total_A_Income_A", "#TOTAL-A-INCOME-A", FieldType.DECIMAL, 11,3);

        pnd_Total_A_Income_T = newFieldInRecord("pnd_Total_A_Income_T", "#TOTAL-A-INCOME-T", FieldType.DECIMAL, 11,3);

        pnd_Total_C_Issued_M = newFieldInRecord("pnd_Total_C_Issued_M", "#TOTAL-C-ISSUED-M", FieldType.NUMERIC, 9);

        pnd_Total_C_Issued_S = newFieldInRecord("pnd_Total_C_Issued_S", "#TOTAL-C-ISSUED-S", FieldType.NUMERIC, 9);

        pnd_Total_C_Issued_Q = newFieldInRecord("pnd_Total_C_Issued_Q", "#TOTAL-C-ISSUED-Q", FieldType.NUMERIC, 9);

        pnd_Total_C_Issued_A = newFieldInRecord("pnd_Total_C_Issued_A", "#TOTAL-C-ISSUED-A", FieldType.NUMERIC, 9);

        pnd_Total_C_Issued_T = newFieldInRecord("pnd_Total_C_Issued_T", "#TOTAL-C-ISSUED-T", FieldType.NUMERIC, 9);

        pnd_Total_C_Applied_M = newFieldInRecord("pnd_Total_C_Applied_M", "#TOTAL-C-APPLIED-M", FieldType.DECIMAL, 11,2);

        pnd_Total_C_Applied_S = newFieldInRecord("pnd_Total_C_Applied_S", "#TOTAL-C-APPLIED-S", FieldType.DECIMAL, 11,2);

        pnd_Total_C_Applied_Q = newFieldInRecord("pnd_Total_C_Applied_Q", "#TOTAL-C-APPLIED-Q", FieldType.DECIMAL, 11,2);

        pnd_Total_C_Applied_A = newFieldInRecord("pnd_Total_C_Applied_A", "#TOTAL-C-APPLIED-A", FieldType.DECIMAL, 11,2);

        pnd_Total_C_Applied_T = newFieldInRecord("pnd_Total_C_Applied_T", "#TOTAL-C-APPLIED-T", FieldType.DECIMAL, 11,2);

        pnd_Total_C_Income_M = newFieldInRecord("pnd_Total_C_Income_M", "#TOTAL-C-INCOME-M", FieldType.DECIMAL, 11,3);

        pnd_Total_C_Income_S = newFieldInRecord("pnd_Total_C_Income_S", "#TOTAL-C-INCOME-S", FieldType.DECIMAL, 11,3);

        pnd_Total_C_Income_Q = newFieldInRecord("pnd_Total_C_Income_Q", "#TOTAL-C-INCOME-Q", FieldType.DECIMAL, 11,3);

        pnd_Total_C_Income_A = newFieldInRecord("pnd_Total_C_Income_A", "#TOTAL-C-INCOME-A", FieldType.DECIMAL, 11,3);

        pnd_Total_C_Income_T = newFieldInRecord("pnd_Total_C_Income_T", "#TOTAL-C-INCOME-T", FieldType.DECIMAL, 11,3);

        this.setRecordName("LdaAdsl6005");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAdsl6005() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
