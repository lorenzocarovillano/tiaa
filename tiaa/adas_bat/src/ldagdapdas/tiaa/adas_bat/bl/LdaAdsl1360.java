/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:49:27 PM
**        * FROM NATURAL LDA     : ADSL1360
************************************************************
**        * FILE NAME            : LdaAdsl1360.java
**        * CLASS NAME           : LdaAdsl1360
**        * INSTANCE NAME        : LdaAdsl1360
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdsl1360 extends DbsRecord
{
    // Properties
    private DbsField pnd_Accounting_Date;
    private DbsGroup pnd_Accounting_DateRedef1;
    private DbsField pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm;
    private DbsField pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd;
    private DbsField pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc;
    private DbsField pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy;
    private DbsField pnd_Check_Cycle_Date;
    private DbsGroup pnd_Check_Cycle_DateRedef2;
    private DbsField pnd_Check_Cycle_Date_Pnd_Ckeck_Cycle_Mcdy_Mm;
    private DbsField pnd_Check_Cycle_Date_Pnd_Check_Cycle_Mcdy_Dd;
    private DbsField pnd_Check_Cycle_Date_Pnd_Check_Cycle_Mcdy_Cc;
    private DbsField pnd_Check_Cycle_Date_Pnd_Check_Cycle_Mcdy_Yy;
    private DbsField pnd_Payment_Due_Date;
    private DbsGroup pnd_Payment_Due_DateRedef3;
    private DbsField pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Mm;
    private DbsField pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Dd;
    private DbsField pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Cc;
    private DbsField pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Yy;
    private DbsField pnd_Mode_Description;
    private DbsGroup pnd_Tiaa_By_Mode;
    private DbsGroup pnd_Tiaa_By_Mode_Pnd_Tiaa_By_Details;
    private DbsField pnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt;
    private DbsField pnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt;
    private DbsField pnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt;
    private DbsField pnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt;
    private DbsField pnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt;
    private DbsGroup pnd_M_Tiaa_By_Details;
    private DbsField pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt;
    private DbsField pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt;
    private DbsField pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt;
    private DbsField pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt;
    private DbsField pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt;
    private DbsGroup pnd_C_Mth_Accum_By_Mode;
    private DbsGroup pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Minor_Accum;
    private DbsField pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Pay_Cnt;
    private DbsField pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Units;
    private DbsField pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Dollars;
    private DbsField pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Gtd_Amt;
    private DbsField pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Div_Amt;
    private DbsField pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Fin_Gtd_Amt;
    private DbsField pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Fin_Div_Amt;
    private DbsGroup pnd_M_C_Mth_Minor_Accum;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt;
    private DbsGroup pnd_C_Ann_By_Mode;
    private DbsGroup pnd_C_Ann_By_Mode_Pnd_C_Ann_Minor_Accum;
    private DbsField pnd_C_Ann_By_Mode_Pnd_C_Ann_Pay_Cnt;
    private DbsField pnd_C_Ann_By_Mode_Pnd_C_Ann_Units;
    private DbsField pnd_C_Ann_By_Mode_Pnd_C_Ann_Dollars;
    private DbsField pnd_C_Ann_By_Mode_Pnd_C_Ann_Gtd_Amt;
    private DbsField pnd_C_Ann_By_Mode_Pnd_C_Ann_Divid_Amt;
    private DbsField pnd_C_Ann_By_Mode_Pnd_C_Ann_Fin_Gtd_Amt;
    private DbsField pnd_C_Ann_By_Mode_Pnd_C_Ann_Fin_Div_Amt;
    private DbsGroup pnd_M_C_Ann_Minor_Accum;
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt;
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units;
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars;
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt;
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt;
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt;
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt;
    private DbsGroup pnd_Cref_Mth_Sub_Total_Accum;
    private DbsField pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Pay_Cnt;
    private DbsField pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Units;
    private DbsField pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Dollars;
    private DbsField pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Gtd_Amt;
    private DbsField pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Div_Amt;
    private DbsField pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Fin_Gtd_Amt;
    private DbsField pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Fin_Div_Amt;
    private DbsGroup pnd_M_Cref_Mth_Sub_Total_Accum;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Pay_Cnt;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Units;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Dollars;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Gtd_Amt;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Div_Amt;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Gtd_Amt;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Div_Amt;
    private DbsGroup pnd_Cref_Ann_Sub_Total_Accum;
    private DbsField pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Pay_Cnt;
    private DbsField pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Units;
    private DbsField pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Dollars;
    private DbsField pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Gtd_Amt;
    private DbsField pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Div_Amt;
    private DbsField pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Fin_Gtd_Amt;
    private DbsField pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Fin_Div_Amt;
    private DbsGroup pnd_M_Cref_Ann_Sub_Total_Accum;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Pay_Cnt;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Units;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Dollars;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Gtd_Amt;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Div_Amt;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Gtd_Amt;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Div_Amt;
    private DbsGroup pnd_Total_Tiaa_Cref_Accum;
    private DbsField pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Pay_Cnt;
    private DbsField pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Units;
    private DbsField pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Dollars;
    private DbsField pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Gtd_Amt;
    private DbsField pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Div_Amt;
    private DbsField pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Fin_Gtd_Amt;
    private DbsField pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Fin_Div_Amt;
    private DbsGroup pnd_M_Total_Tiaa_Cref_Accum;
    private DbsField pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Pay_Cnt;
    private DbsField pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Units;
    private DbsField pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Dollars;
    private DbsField pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Gtd_Amt;
    private DbsField pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Div_Amt;
    private DbsField pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Gtd_Amt;
    private DbsField pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Div_Amt;
    private DbsGroup pnd_M_T_Cref_Units_Filler;
    private DbsField pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units;
    private DbsGroup pnd_M_T_Cref_Dollars_Filler;
    private DbsField pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars;

    public DbsField getPnd_Accounting_Date() { return pnd_Accounting_Date; }

    public DbsGroup getPnd_Accounting_DateRedef1() { return pnd_Accounting_DateRedef1; }

    public DbsField getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm() { return pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm; }

    public DbsField getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd() { return pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd; }

    public DbsField getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc() { return pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc; }

    public DbsField getPnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy() { return pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy; }

    public DbsField getPnd_Check_Cycle_Date() { return pnd_Check_Cycle_Date; }

    public DbsGroup getPnd_Check_Cycle_DateRedef2() { return pnd_Check_Cycle_DateRedef2; }

    public DbsField getPnd_Check_Cycle_Date_Pnd_Ckeck_Cycle_Mcdy_Mm() { return pnd_Check_Cycle_Date_Pnd_Ckeck_Cycle_Mcdy_Mm; }

    public DbsField getPnd_Check_Cycle_Date_Pnd_Check_Cycle_Mcdy_Dd() { return pnd_Check_Cycle_Date_Pnd_Check_Cycle_Mcdy_Dd; }

    public DbsField getPnd_Check_Cycle_Date_Pnd_Check_Cycle_Mcdy_Cc() { return pnd_Check_Cycle_Date_Pnd_Check_Cycle_Mcdy_Cc; }

    public DbsField getPnd_Check_Cycle_Date_Pnd_Check_Cycle_Mcdy_Yy() { return pnd_Check_Cycle_Date_Pnd_Check_Cycle_Mcdy_Yy; }

    public DbsField getPnd_Payment_Due_Date() { return pnd_Payment_Due_Date; }

    public DbsGroup getPnd_Payment_Due_DateRedef3() { return pnd_Payment_Due_DateRedef3; }

    public DbsField getPnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Mm() { return pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Mm; }

    public DbsField getPnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Dd() { return pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Dd; }

    public DbsField getPnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Cc() { return pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Cc; }

    public DbsField getPnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Yy() { return pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Yy; }

    public DbsField getPnd_Mode_Description() { return pnd_Mode_Description; }

    public DbsGroup getPnd_Tiaa_By_Mode() { return pnd_Tiaa_By_Mode; }

    public DbsGroup getPnd_Tiaa_By_Mode_Pnd_Tiaa_By_Details() { return pnd_Tiaa_By_Mode_Pnd_Tiaa_By_Details; }

    public DbsField getPnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt() { return pnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt; }

    public DbsField getPnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt() { return pnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt; }

    public DbsField getPnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt() { return pnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt; }

    public DbsField getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt() { return pnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt; }

    public DbsField getPnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt() { return pnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt; }

    public DbsGroup getPnd_M_Tiaa_By_Details() { return pnd_M_Tiaa_By_Details; }

    public DbsField getPnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt() { return pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt; }

    public DbsField getPnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt() { return pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt; }

    public DbsField getPnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt() { return pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt; }

    public DbsField getPnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt() { return pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt; }

    public DbsField getPnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt() { return pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt; }

    public DbsGroup getPnd_C_Mth_Accum_By_Mode() { return pnd_C_Mth_Accum_By_Mode; }

    public DbsGroup getPnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Minor_Accum() { return pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Minor_Accum; }

    public DbsField getPnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Pay_Cnt() { return pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Pay_Cnt; }

    public DbsField getPnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Units() { return pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Units; }

    public DbsField getPnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Dollars() { return pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Dollars; }

    public DbsField getPnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Gtd_Amt() { return pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Gtd_Amt; }

    public DbsField getPnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Div_Amt() { return pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Div_Amt; }

    public DbsField getPnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Fin_Gtd_Amt() { return pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Fin_Gtd_Amt; }

    public DbsField getPnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Fin_Div_Amt() { return pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Fin_Div_Amt; }

    public DbsGroup getPnd_M_C_Mth_Minor_Accum() { return pnd_M_C_Mth_Minor_Accum; }

    public DbsField getPnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt() { return pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt; }

    public DbsField getPnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units() { return pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units; }

    public DbsField getPnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars() { return pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars; }

    public DbsField getPnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt() { return pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt; }

    public DbsField getPnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt() { return pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt; }

    public DbsField getPnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt() { return pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt; }

    public DbsField getPnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt() { return pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt; }

    public DbsGroup getPnd_C_Ann_By_Mode() { return pnd_C_Ann_By_Mode; }

    public DbsGroup getPnd_C_Ann_By_Mode_Pnd_C_Ann_Minor_Accum() { return pnd_C_Ann_By_Mode_Pnd_C_Ann_Minor_Accum; }

    public DbsField getPnd_C_Ann_By_Mode_Pnd_C_Ann_Pay_Cnt() { return pnd_C_Ann_By_Mode_Pnd_C_Ann_Pay_Cnt; }

    public DbsField getPnd_C_Ann_By_Mode_Pnd_C_Ann_Units() { return pnd_C_Ann_By_Mode_Pnd_C_Ann_Units; }

    public DbsField getPnd_C_Ann_By_Mode_Pnd_C_Ann_Dollars() { return pnd_C_Ann_By_Mode_Pnd_C_Ann_Dollars; }

    public DbsField getPnd_C_Ann_By_Mode_Pnd_C_Ann_Gtd_Amt() { return pnd_C_Ann_By_Mode_Pnd_C_Ann_Gtd_Amt; }

    public DbsField getPnd_C_Ann_By_Mode_Pnd_C_Ann_Divid_Amt() { return pnd_C_Ann_By_Mode_Pnd_C_Ann_Divid_Amt; }

    public DbsField getPnd_C_Ann_By_Mode_Pnd_C_Ann_Fin_Gtd_Amt() { return pnd_C_Ann_By_Mode_Pnd_C_Ann_Fin_Gtd_Amt; }

    public DbsField getPnd_C_Ann_By_Mode_Pnd_C_Ann_Fin_Div_Amt() { return pnd_C_Ann_By_Mode_Pnd_C_Ann_Fin_Div_Amt; }

    public DbsGroup getPnd_M_C_Ann_Minor_Accum() { return pnd_M_C_Ann_Minor_Accum; }

    public DbsField getPnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt() { return pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt; }

    public DbsField getPnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units() { return pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units; }

    public DbsField getPnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars() { return pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars; }

    public DbsField getPnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt() { return pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt; }

    public DbsField getPnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt() { return pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt; }

    public DbsField getPnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt() { return pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt; }

    public DbsField getPnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt() { return pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt; }

    public DbsGroup getPnd_Cref_Mth_Sub_Total_Accum() { return pnd_Cref_Mth_Sub_Total_Accum; }

    public DbsField getPnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Pay_Cnt() { return pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Pay_Cnt; }

    public DbsField getPnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Units() { return pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Units; }

    public DbsField getPnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Dollars() { return pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Dollars; }

    public DbsField getPnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Gtd_Amt() { return pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Gtd_Amt; }

    public DbsField getPnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Div_Amt() { return pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Div_Amt; }

    public DbsField getPnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Fin_Gtd_Amt() { return pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Fin_Gtd_Amt; 
        }

    public DbsField getPnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Fin_Div_Amt() { return pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Fin_Div_Amt; 
        }

    public DbsGroup getPnd_M_Cref_Mth_Sub_Total_Accum() { return pnd_M_Cref_Mth_Sub_Total_Accum; }

    public DbsField getPnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Pay_Cnt() { return pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Pay_Cnt; 
        }

    public DbsField getPnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Units() { return pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Units; }

    public DbsField getPnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Dollars() { return pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Dollars; 
        }

    public DbsField getPnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Gtd_Amt() { return pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Gtd_Amt; 
        }

    public DbsField getPnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Div_Amt() { return pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Div_Amt; 
        }

    public DbsField getPnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Gtd_Amt() { return pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Gtd_Amt; 
        }

    public DbsField getPnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Div_Amt() { return pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Div_Amt; 
        }

    public DbsGroup getPnd_Cref_Ann_Sub_Total_Accum() { return pnd_Cref_Ann_Sub_Total_Accum; }

    public DbsField getPnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Pay_Cnt() { return pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Pay_Cnt; }

    public DbsField getPnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Units() { return pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Units; }

    public DbsField getPnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Dollars() { return pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Dollars; }

    public DbsField getPnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Gtd_Amt() { return pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Gtd_Amt; }

    public DbsField getPnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Div_Amt() { return pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Div_Amt; }

    public DbsField getPnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Fin_Gtd_Amt() { return pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Fin_Gtd_Amt; 
        }

    public DbsField getPnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Fin_Div_Amt() { return pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Fin_Div_Amt; 
        }

    public DbsGroup getPnd_M_Cref_Ann_Sub_Total_Accum() { return pnd_M_Cref_Ann_Sub_Total_Accum; }

    public DbsField getPnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Pay_Cnt() { return pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Pay_Cnt; 
        }

    public DbsField getPnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Units() { return pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Units; }

    public DbsField getPnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Dollars() { return pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Dollars; 
        }

    public DbsField getPnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Gtd_Amt() { return pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Gtd_Amt; 
        }

    public DbsField getPnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Div_Amt() { return pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Div_Amt; 
        }

    public DbsField getPnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Gtd_Amt() { return pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Gtd_Amt; 
        }

    public DbsField getPnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Div_Amt() { return pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Div_Amt; 
        }

    public DbsGroup getPnd_Total_Tiaa_Cref_Accum() { return pnd_Total_Tiaa_Cref_Accum; }

    public DbsField getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Pay_Cnt() { return pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Pay_Cnt; }

    public DbsField getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Units() { return pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Units; }

    public DbsField getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Dollars() { return pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Dollars; }

    public DbsField getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Gtd_Amt() { return pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Gtd_Amt; }

    public DbsField getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Div_Amt() { return pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Div_Amt; }

    public DbsField getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Fin_Gtd_Amt() { return pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Fin_Gtd_Amt; }

    public DbsField getPnd_Total_Tiaa_Cref_Accum_Pnd_Total_Fin_Div_Amt() { return pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Fin_Div_Amt; }

    public DbsGroup getPnd_M_Total_Tiaa_Cref_Accum() { return pnd_M_Total_Tiaa_Cref_Accum; }

    public DbsField getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Pay_Cnt() { return pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Pay_Cnt; }

    public DbsField getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Units() { return pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Units; }

    public DbsField getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Dollars() { return pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Dollars; }

    public DbsField getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Gtd_Amt() { return pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Gtd_Amt; }

    public DbsField getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Div_Amt() { return pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Div_Amt; }

    public DbsField getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Gtd_Amt() { return pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Gtd_Amt; }

    public DbsField getPnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Div_Amt() { return pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Div_Amt; }

    public DbsGroup getPnd_M_T_Cref_Units_Filler() { return pnd_M_T_Cref_Units_Filler; }

    public DbsField getPnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units() { return pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units; }

    public DbsGroup getPnd_M_T_Cref_Dollars_Filler() { return pnd_M_T_Cref_Dollars_Filler; }

    public DbsField getPnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars() { return pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Accounting_Date = newFieldInRecord("pnd_Accounting_Date", "#ACCOUNTING-DATE", FieldType.NUMERIC, 8);
        pnd_Accounting_DateRedef1 = newGroupInRecord("pnd_Accounting_DateRedef1", "Redefines", pnd_Accounting_Date);
        pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm = pnd_Accounting_DateRedef1.newFieldInGroup("pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Mm", 
            "#ACCOUNTING-DATE-MCDY-MM", FieldType.NUMERIC, 2);
        pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd = pnd_Accounting_DateRedef1.newFieldInGroup("pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Dd", 
            "#ACCOUNTING-DATE-MCDY-DD", FieldType.NUMERIC, 2);
        pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc = pnd_Accounting_DateRedef1.newFieldInGroup("pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Cc", 
            "#ACCOUNTING-DATE-MCDY-CC", FieldType.NUMERIC, 2);
        pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy = pnd_Accounting_DateRedef1.newFieldInGroup("pnd_Accounting_Date_Pnd_Accounting_Date_Mcdy_Yy", 
            "#ACCOUNTING-DATE-MCDY-YY", FieldType.NUMERIC, 2);

        pnd_Check_Cycle_Date = newFieldInRecord("pnd_Check_Cycle_Date", "#CHECK-CYCLE-DATE", FieldType.NUMERIC, 8);
        pnd_Check_Cycle_DateRedef2 = newGroupInRecord("pnd_Check_Cycle_DateRedef2", "Redefines", pnd_Check_Cycle_Date);
        pnd_Check_Cycle_Date_Pnd_Ckeck_Cycle_Mcdy_Mm = pnd_Check_Cycle_DateRedef2.newFieldInGroup("pnd_Check_Cycle_Date_Pnd_Ckeck_Cycle_Mcdy_Mm", "#CKECK-CYCLE-MCDY-MM", 
            FieldType.NUMERIC, 2);
        pnd_Check_Cycle_Date_Pnd_Check_Cycle_Mcdy_Dd = pnd_Check_Cycle_DateRedef2.newFieldInGroup("pnd_Check_Cycle_Date_Pnd_Check_Cycle_Mcdy_Dd", "#CHECK-CYCLE-MCDY-DD", 
            FieldType.NUMERIC, 2);
        pnd_Check_Cycle_Date_Pnd_Check_Cycle_Mcdy_Cc = pnd_Check_Cycle_DateRedef2.newFieldInGroup("pnd_Check_Cycle_Date_Pnd_Check_Cycle_Mcdy_Cc", "#CHECK-CYCLE-MCDY-CC", 
            FieldType.NUMERIC, 2);
        pnd_Check_Cycle_Date_Pnd_Check_Cycle_Mcdy_Yy = pnd_Check_Cycle_DateRedef2.newFieldInGroup("pnd_Check_Cycle_Date_Pnd_Check_Cycle_Mcdy_Yy", "#CHECK-CYCLE-MCDY-YY", 
            FieldType.NUMERIC, 2);

        pnd_Payment_Due_Date = newFieldInRecord("pnd_Payment_Due_Date", "#PAYMENT-DUE-DATE", FieldType.NUMERIC, 8);
        pnd_Payment_Due_DateRedef3 = newGroupInRecord("pnd_Payment_Due_DateRedef3", "Redefines", pnd_Payment_Due_Date);
        pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Mm = pnd_Payment_Due_DateRedef3.newFieldInGroup("pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Mm", "#PAYMENT-DATE-MCDY-MM", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Dd = pnd_Payment_Due_DateRedef3.newFieldInGroup("pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Dd", "#PAYMENT-DATE-MCDY-DD", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Cc = pnd_Payment_Due_DateRedef3.newFieldInGroup("pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Cc", "#PAYMENT-DATE-MCDY-CC", 
            FieldType.NUMERIC, 2);
        pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Yy = pnd_Payment_Due_DateRedef3.newFieldInGroup("pnd_Payment_Due_Date_Pnd_Payment_Date_Mcdy_Yy", "#PAYMENT-DATE-MCDY-YY", 
            FieldType.NUMERIC, 2);

        pnd_Mode_Description = newFieldInRecord("pnd_Mode_Description", "#MODE-DESCRIPTION", FieldType.STRING, 20);

        pnd_Tiaa_By_Mode = newGroupArrayInRecord("pnd_Tiaa_By_Mode", "#TIAA-BY-MODE", new DbsArrayController(1,5));
        pnd_Tiaa_By_Mode_Pnd_Tiaa_By_Details = pnd_Tiaa_By_Mode.newGroupArrayInGroup("pnd_Tiaa_By_Mode_Pnd_Tiaa_By_Details", "#TIAA-BY-DETAILS", new DbsArrayController(1,
            16));
        pnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt = pnd_Tiaa_By_Mode_Pnd_Tiaa_By_Details.newFieldInGroup("pnd_Tiaa_By_Mode_Pnd_Tiaa_Pay_Cnt", "#TIAA-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt = pnd_Tiaa_By_Mode_Pnd_Tiaa_By_Details.newFieldInGroup("pnd_Tiaa_By_Mode_Pnd_Tiaa_Gtd_Amt", "#TIAA-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt = pnd_Tiaa_By_Mode_Pnd_Tiaa_By_Details.newFieldInGroup("pnd_Tiaa_By_Mode_Pnd_Tiaa_Div_Amt", "#TIAA-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt = pnd_Tiaa_By_Mode_Pnd_Tiaa_By_Details.newFieldInGroup("pnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Gtd_Amt", "#TIAA-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt = pnd_Tiaa_By_Mode_Pnd_Tiaa_By_Details.newFieldInGroup("pnd_Tiaa_By_Mode_Pnd_Tiaa_Fin_Div_Amt", "#TIAA-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);

        pnd_M_Tiaa_By_Details = newGroupArrayInRecord("pnd_M_Tiaa_By_Details", "#M-TIAA-BY-DETAILS", new DbsArrayController(1,16));
        pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt = pnd_M_Tiaa_By_Details.newFieldInGroup("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt", "#M-TIAA-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt = pnd_M_Tiaa_By_Details.newFieldInGroup("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt", "#M-TIAA-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt = pnd_M_Tiaa_By_Details.newFieldInGroup("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt", "#M-TIAA-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt = pnd_M_Tiaa_By_Details.newFieldInGroup("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt", "#M-TIAA-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt = pnd_M_Tiaa_By_Details.newFieldInGroup("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt", "#M-TIAA-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);

        pnd_C_Mth_Accum_By_Mode = newGroupArrayInRecord("pnd_C_Mth_Accum_By_Mode", "#C-MTH-ACCUM-BY-MODE", new DbsArrayController(1,5));
        pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Minor_Accum = pnd_C_Mth_Accum_By_Mode.newGroupArrayInGroup("pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Minor_Accum", 
            "#C-MTH-MINOR-ACCUM", new DbsArrayController(1,20));
        pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Pay_Cnt = pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Minor_Accum.newFieldInGroup("pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Pay_Cnt", 
            "#C-MTH-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Units = pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Minor_Accum.newFieldInGroup("pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Units", 
            "#C-MTH-UNITS", FieldType.PACKED_DECIMAL, 10,4);
        pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Dollars = pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Minor_Accum.newFieldInGroup("pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Dollars", 
            "#C-MTH-DOLLARS", FieldType.PACKED_DECIMAL, 11,2);
        pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Gtd_Amt = pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Minor_Accum.newFieldInGroup("pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Gtd_Amt", 
            "#C-MTH-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Div_Amt = pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Minor_Accum.newFieldInGroup("pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Div_Amt", 
            "#C-MTH-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Fin_Gtd_Amt = pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Minor_Accum.newFieldInGroup("pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Fin_Gtd_Amt", 
            "#C-MTH-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Fin_Div_Amt = pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Minor_Accum.newFieldInGroup("pnd_C_Mth_Accum_By_Mode_Pnd_C_Mth_Fin_Div_Amt", 
            "#C-MTH-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);

        pnd_M_C_Mth_Minor_Accum = newGroupArrayInRecord("pnd_M_C_Mth_Minor_Accum", "#M-C-MTH-MINOR-ACCUM", new DbsArrayController(1,20));
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt = pnd_M_C_Mth_Minor_Accum.newFieldInGroup("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt", "#M-C-MTH-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units = pnd_M_C_Mth_Minor_Accum.newFieldInGroup("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units", "#M-C-MTH-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars = pnd_M_C_Mth_Minor_Accum.newFieldInGroup("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars", "#M-C-MTH-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt = pnd_M_C_Mth_Minor_Accum.newFieldInGroup("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt", "#M-C-MTH-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt = pnd_M_C_Mth_Minor_Accum.newFieldInGroup("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt", "#M-C-MTH-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt = pnd_M_C_Mth_Minor_Accum.newFieldInGroup("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt", "#M-C-MTH-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt = pnd_M_C_Mth_Minor_Accum.newFieldInGroup("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt", "#M-C-MTH-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);

        pnd_C_Ann_By_Mode = newGroupArrayInRecord("pnd_C_Ann_By_Mode", "#C-ANN-BY-MODE", new DbsArrayController(1,5));
        pnd_C_Ann_By_Mode_Pnd_C_Ann_Minor_Accum = pnd_C_Ann_By_Mode.newGroupArrayInGroup("pnd_C_Ann_By_Mode_Pnd_C_Ann_Minor_Accum", "#C-ANN-MINOR-ACCUM", 
            new DbsArrayController(1,20));
        pnd_C_Ann_By_Mode_Pnd_C_Ann_Pay_Cnt = pnd_C_Ann_By_Mode_Pnd_C_Ann_Minor_Accum.newFieldInGroup("pnd_C_Ann_By_Mode_Pnd_C_Ann_Pay_Cnt", "#C-ANN-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_C_Ann_By_Mode_Pnd_C_Ann_Units = pnd_C_Ann_By_Mode_Pnd_C_Ann_Minor_Accum.newFieldInGroup("pnd_C_Ann_By_Mode_Pnd_C_Ann_Units", "#C-ANN-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_C_Ann_By_Mode_Pnd_C_Ann_Dollars = pnd_C_Ann_By_Mode_Pnd_C_Ann_Minor_Accum.newFieldInGroup("pnd_C_Ann_By_Mode_Pnd_C_Ann_Dollars", "#C-ANN-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_C_Ann_By_Mode_Pnd_C_Ann_Gtd_Amt = pnd_C_Ann_By_Mode_Pnd_C_Ann_Minor_Accum.newFieldInGroup("pnd_C_Ann_By_Mode_Pnd_C_Ann_Gtd_Amt", "#C-ANN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_C_Ann_By_Mode_Pnd_C_Ann_Divid_Amt = pnd_C_Ann_By_Mode_Pnd_C_Ann_Minor_Accum.newFieldInGroup("pnd_C_Ann_By_Mode_Pnd_C_Ann_Divid_Amt", "#C-ANN-DIVID-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_C_Ann_By_Mode_Pnd_C_Ann_Fin_Gtd_Amt = pnd_C_Ann_By_Mode_Pnd_C_Ann_Minor_Accum.newFieldInGroup("pnd_C_Ann_By_Mode_Pnd_C_Ann_Fin_Gtd_Amt", "#C-ANN-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_C_Ann_By_Mode_Pnd_C_Ann_Fin_Div_Amt = pnd_C_Ann_By_Mode_Pnd_C_Ann_Minor_Accum.newFieldInGroup("pnd_C_Ann_By_Mode_Pnd_C_Ann_Fin_Div_Amt", "#C-ANN-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);

        pnd_M_C_Ann_Minor_Accum = newGroupArrayInRecord("pnd_M_C_Ann_Minor_Accum", "#M-C-ANN-MINOR-ACCUM", new DbsArrayController(1,20));
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt = pnd_M_C_Ann_Minor_Accum.newFieldInGroup("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt", "#M-C-ANN-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units = pnd_M_C_Ann_Minor_Accum.newFieldInGroup("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units", "#M-C-ANN-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars = pnd_M_C_Ann_Minor_Accum.newFieldInGroup("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars", "#M-C-ANN-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt = pnd_M_C_Ann_Minor_Accum.newFieldInGroup("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt", "#M-C-ANN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt = pnd_M_C_Ann_Minor_Accum.newFieldInGroup("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt", "#M-C-ANN-DIVID-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt = pnd_M_C_Ann_Minor_Accum.newFieldInGroup("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt", "#M-C-ANN-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt = pnd_M_C_Ann_Minor_Accum.newFieldInGroup("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt", "#M-C-ANN-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);

        pnd_Cref_Mth_Sub_Total_Accum = newGroupArrayInRecord("pnd_Cref_Mth_Sub_Total_Accum", "#CREF-MTH-SUB-TOTAL-ACCUM", new DbsArrayController(1,5));
        pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Pay_Cnt = pnd_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Pay_Cnt", 
            "#CREF-MTH-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Units = pnd_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Units", 
            "#CREF-MTH-SUB-UNITS", FieldType.PACKED_DECIMAL, 13,4);
        pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Dollars = pnd_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Dollars", 
            "#CREF-MTH-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Gtd_Amt = pnd_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Gtd_Amt", 
            "#CREF-MTH-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Div_Amt = pnd_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Div_Amt", 
            "#CREF-MTH-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Fin_Gtd_Amt = pnd_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Fin_Gtd_Amt", 
            "#CREF-MTH-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Fin_Div_Amt = pnd_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_Cref_Mth_Sub_Total_Accum_Pnd_Cref_Mth_Sub_Fin_Div_Amt", 
            "#CREF-MTH-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);

        pnd_M_Cref_Mth_Sub_Total_Accum = newGroupInRecord("pnd_M_Cref_Mth_Sub_Total_Accum", "#M-CREF-MTH-SUB-TOTAL-ACCUM");
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Pay_Cnt = pnd_M_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Pay_Cnt", 
            "#M-CREF-MTH-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Units = pnd_M_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Units", 
            "#M-CREF-MTH-SUB-UNITS", FieldType.PACKED_DECIMAL, 13,4);
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Dollars = pnd_M_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Dollars", 
            "#M-CREF-MTH-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 12,2);
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Gtd_Amt = pnd_M_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Gtd_Amt", 
            "#M-CREF-MTH-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Div_Amt = pnd_M_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Div_Amt", 
            "#M-CREF-MTH-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Gtd_Amt = pnd_M_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Gtd_Amt", 
            "#M-CREF-MTH-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Div_Amt = pnd_M_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Div_Amt", 
            "#M-CREF-MTH-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);

        pnd_Cref_Ann_Sub_Total_Accum = newGroupArrayInRecord("pnd_Cref_Ann_Sub_Total_Accum", "#CREF-ANN-SUB-TOTAL-ACCUM", new DbsArrayController(1,5));
        pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Pay_Cnt = pnd_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Pay_Cnt", 
            "#CREF-ANN-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Units = pnd_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Units", 
            "#CREF-ANN-SUB-UNITS", FieldType.PACKED_DECIMAL, 13,4);
        pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Dollars = pnd_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Dollars", 
            "#CREF-ANN-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Gtd_Amt = pnd_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Gtd_Amt", 
            "#CREF-ANN-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Div_Amt = pnd_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Div_Amt", 
            "#CREF-ANN-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Fin_Gtd_Amt = pnd_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Fin_Gtd_Amt", 
            "#CREF-ANN-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Fin_Div_Amt = pnd_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_Cref_Ann_Sub_Total_Accum_Pnd_Cref_Ann_Sub_Fin_Div_Amt", 
            "#CREF-ANN-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);

        pnd_M_Cref_Ann_Sub_Total_Accum = newGroupInRecord("pnd_M_Cref_Ann_Sub_Total_Accum", "#M-CREF-ANN-SUB-TOTAL-ACCUM");
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Pay_Cnt = pnd_M_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Pay_Cnt", 
            "#M-CREF-ANN-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Units = pnd_M_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Units", 
            "#M-CREF-ANN-SUB-UNITS", FieldType.PACKED_DECIMAL, 13,4);
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Dollars = pnd_M_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Dollars", 
            "#M-CREF-ANN-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 12,2);
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Gtd_Amt = pnd_M_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Gtd_Amt", 
            "#M-CREF-ANN-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Div_Amt = pnd_M_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Div_Amt", 
            "#M-CREF-ANN-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Gtd_Amt = pnd_M_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Gtd_Amt", 
            "#M-CREF-ANN-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Div_Amt = pnd_M_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Div_Amt", 
            "#M-CREF-ANN-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);

        pnd_Total_Tiaa_Cref_Accum = newGroupArrayInRecord("pnd_Total_Tiaa_Cref_Accum", "#TOTAL-TIAA-CREF-ACCUM", new DbsArrayController(1,5));
        pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Pay_Cnt = pnd_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Pay_Cnt", "#TOTAL-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Units = pnd_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Units", "#TOTAL-UNITS", 
            FieldType.PACKED_DECIMAL, 13,4);
        pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Dollars = pnd_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Dollars", "#TOTAL-DOLLARS", 
            FieldType.PACKED_DECIMAL, 14,2);
        pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Gtd_Amt = pnd_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Gtd_Amt", "#TOTAL-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 14,2);
        pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Div_Amt = pnd_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Div_Amt", "#TOTAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 14,2);
        pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Fin_Gtd_Amt = pnd_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Fin_Gtd_Amt", 
            "#TOTAL-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Fin_Div_Amt = pnd_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_Total_Tiaa_Cref_Accum_Pnd_Total_Fin_Div_Amt", 
            "#TOTAL-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 14,2);

        pnd_M_Total_Tiaa_Cref_Accum = newGroupInRecord("pnd_M_Total_Tiaa_Cref_Accum", "#M-TOTAL-TIAA-CREF-ACCUM");
        pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Pay_Cnt = pnd_M_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Pay_Cnt", 
            "#M-TOTAL-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Units = pnd_M_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Units", "#M-TOTAL-UNITS", 
            FieldType.PACKED_DECIMAL, 13,4);
        pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Dollars = pnd_M_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Dollars", 
            "#M-TOTAL-DOLLARS", FieldType.PACKED_DECIMAL, 14,2);
        pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Gtd_Amt = pnd_M_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Gtd_Amt", 
            "#M-TOTAL-GTD-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Div_Amt = pnd_M_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Div_Amt", 
            "#M-TOTAL-DIV-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Gtd_Amt = pnd_M_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Gtd_Amt", 
            "#M-TOTAL-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Div_Amt = pnd_M_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Fin_Div_Amt", 
            "#M-TOTAL-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 14,2);

        pnd_M_T_Cref_Units_Filler = newGroupArrayInRecord("pnd_M_T_Cref_Units_Filler", "#M-T-CREF-UNITS-FILLER", new DbsArrayController(1,15));
        pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units = pnd_M_T_Cref_Units_Filler.newFieldInGroup("pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units", "#M-T-CREF-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);

        pnd_M_T_Cref_Dollars_Filler = newGroupArrayInRecord("pnd_M_T_Cref_Dollars_Filler", "#M-T-CREF-DOLLARS-FILLER", new DbsArrayController(1,15));
        pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars = pnd_M_T_Cref_Dollars_Filler.newFieldInGroup("pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars", 
            "#M-T-CREF-DOLLARS", FieldType.PACKED_DECIMAL, 11,2);

        this.setRecordName("LdaAdsl1360");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAdsl1360() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
