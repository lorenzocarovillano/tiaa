/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:50:20 PM
**        * FROM NATURAL LDA     : ADSL762
************************************************************
**        * FILE NAME            : LdaAdsl762.java
**        * CLASS NAME           : LdaAdsl762
**        * INSTANCE NAME        : LdaAdsl762
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdsl762 extends DbsRecord
{
    // Properties
    private DbsField pnd_Tiaa_Rec_Cnt;
    private DbsGroup pnd_Tiaa_Minor_Totals;
    private DbsField pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Rate;
    private DbsField pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Accum_Amtfslash_Units;
    private DbsField pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Amtfslash_Units;
    private DbsField pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Rtb_Amtfslash_Units;
    private DbsField pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Rtb_Amtfslash_Unit;
    private DbsGroup pnd_T_Save_Sub_Totals;
    private DbsGroup pnd_T_Save_Sub_Totals_Pnd_T_Save_Area_1_Sub_Totals;
    private DbsField pnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Rate;
    private DbsField pnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Prod;
    private DbsField pnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Accum_Amtfslash_Units;
    private DbsField pnd_T_Save_Sub_Totals_Pnd_T_Sub_Settled_Amtfslash_Units;
    private DbsField pnd_T_Save_Sub_Totals_Pnd_T_Sub_Rtb_Amtfslash_Units;
    private DbsField pnd_T_Save_Sub_Totals_Pnd_T_Sub_Settled_Rtb_Amtfslash_Units;
    private DbsField pnd_T_Sub_Settled_Rtb_Amounts;
    private DbsGroup pnd_Tiaa_Sub_Totals;
    private DbsGroup pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Area_1_Sub_Totals;
    private DbsField pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Da_Accum_Sub_Total;
    private DbsField pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Sub_Total;
    private DbsField pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Rtb_Sub_Total;
    private DbsField pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Rtb_Sub_Total;
    private DbsGroup pnd_Tiaa_Total_Settlement_Line;
    private DbsGroup pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Area_1_Settlement_Line;
    private DbsField pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Rate_Total_Settle;
    private DbsField pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Prod_Total_Settle;
    private DbsField pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Accum_Total_Settle;
    private DbsField pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Total_Settle;
    private DbsField pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Rtb_Total_Settle;
    private DbsField pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Rtb_Total_Settle;
    private DbsGroup pnd_Tiaa_Final_Totals;
    private DbsGroup pnd_Tiaa_Final_Totals_Pnd_Tiaa_Area_1_Final_Totals;
    private DbsField pnd_Tiaa_Final_Totals_Pnd_Tiaa_Da_Accum_Final_Total;
    private DbsField pnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Final_Total;
    private DbsField pnd_Tiaa_Final_Totals_Pnd_Tiaa_Rtb_Final_Total;
    private DbsField pnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Rtb_Final_Total;
    private DbsGroup pnd_Tiaa_Standatd_Totals;
    private DbsField pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Prod_Std;
    private DbsField pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Da_Rate_Std;
    private DbsField pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Rate_Std;
    private DbsField pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Settled_Std_Amt;
    private DbsField pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Amt;
    private DbsField pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Dividend_Amt;
    private DbsGroup pnd_Tiaa_Standard_Sub_Totals;
    private DbsField pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Settled_Std_Sub_Total;
    private DbsField pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Sub_Total;
    private DbsField pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Dividend_Sub_Total;
    private DbsGroup pnd_Tiaa_Graded_Totals;
    private DbsGroup pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Area_1_Graded_Totals;
    private DbsField pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Prod_Grd;
    private DbsField pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Da_Rate_Grd;
    private DbsField pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Rate_Grd;
    private DbsField pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Settled_Grd_Amt;
    private DbsField pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Amt;
    private DbsField pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Amt;
    private DbsGroup pnd_Tiaa_Graded_Sub_Totals;
    private DbsGroup pnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Area_1_Graded_Sub_Totals;
    private DbsField pnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Settled_Grd_Sub_Total;
    private DbsField pnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Sub_Total;
    private DbsField pnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Sub_Total;
    private DbsGroup pnd_Tiaa_Std_Method_Save_Amounts;
    private DbsGroup pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Std_Method_Save_Amt;
    private DbsField pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Da_Rate_Std_Payment_Amt;
    private DbsField pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Std_Payment_Amt;
    private DbsField pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Da_Payment_Amt;
    private DbsField pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Gur_Payment_Amt;
    private DbsField pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Div_Payment_Amt;
    private DbsGroup pnd_Tiaa_Std_Method_Save_Payments;
    private DbsGroup pnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Std_Method_Save_Payment;
    private DbsField pnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Std_Payment_Save;
    private DbsField pnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Gur_Payment_Save;
    private DbsField pnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Div_Payment_Save;
    private DbsGroup pnd_Tiaa_Std_Method_Payment_Totals;
    private DbsField pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Std_Payment;
    private DbsField pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Payment;
    private DbsField pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Dividend_Payment;
    private DbsField pnd_Tiaa_Ia_Std_Accum_Final;
    private DbsField pnd_Tiaa_Ia_Std_Guranteed_Final;
    private DbsField pnd_Tiaa_Ia_Std_Dividend_Final;
    private DbsGroup pnd_Tiaa_Grd_Method_Save_Amounts;
    private DbsGroup pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Grd_Method_Save_Amt;
    private DbsField pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Da_Rate_Grd_Payment_Amt;
    private DbsField pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Grd_Payment_Amt;
    private DbsField pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Accum_Payment_Amt;
    private DbsField pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Gur_Payment_Amt;
    private DbsField pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Div_Payment_Amt;
    private DbsGroup pnd_Tiaa_Grd_Method_Save_Payments;
    private DbsGroup pnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Grd_Method_Save_Payment;
    private DbsField pnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Grd_Payment_Save;
    private DbsField pnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Gur_Payment_Save;
    private DbsField pnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Div_Payment_Save;
    private DbsGroup pnd_Tiaa_Grd_Method_Payment_Totals;
    private DbsField pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Grd_Payment;
    private DbsField pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Payment;
    private DbsField pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Payment;
    private DbsField pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Pay_Renew;
    private DbsField pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Pay_Renew;
    private DbsField pnd_Tiaa_Ia_Grd_Guranteed_Final;
    private DbsField pnd_Tiaa_Ia_Grd_Dividend_Final;
    private DbsField pnd_Tiaa_Ia_Grd_Guranteed_Fin_Renew;
    private DbsField pnd_Tiaa_Ia_Grd_Dividend_Fin_Renew;
    private DbsGroup pnd_Tiaa_Header_Amt;
    private DbsField pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Account;
    private DbsField pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Amt;
    private DbsField pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Typ;
    private DbsField pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Rtb_Amt;
    private DbsField pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Rtb_Typ;
    private DbsField pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Amt;
    private DbsField pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Typ;
    private DbsField pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Cref_Mnthly_Units;
    private DbsGroup pnd_Tiaa_Header_Settle_Amounts;
    private DbsField pnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Settle_Amount;
    private DbsField pnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Rtb_Settle_Amount;
    private DbsGroup pnd_Tiaa_Ivc_Amounts;
    private DbsField pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Total;
    private DbsField pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Rtb;
    private DbsField pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Annuity;

    public DbsField getPnd_Tiaa_Rec_Cnt() { return pnd_Tiaa_Rec_Cnt; }

    public DbsGroup getPnd_Tiaa_Minor_Totals() { return pnd_Tiaa_Minor_Totals; }

    public DbsField getPnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Rate() { return pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Rate; }

    public DbsField getPnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Accum_Amtfslash_Units() { return pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Accum_Amtfslash_Units; }

    public DbsField getPnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Amtfslash_Units() { return pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Amtfslash_Units; }

    public DbsField getPnd_Tiaa_Minor_Totals_Pnd_Tiaa_Rtb_Amtfslash_Units() { return pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Rtb_Amtfslash_Units; }

    public DbsField getPnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Rtb_Amtfslash_Unit() { return pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Rtb_Amtfslash_Unit; 
        }

    public DbsGroup getPnd_T_Save_Sub_Totals() { return pnd_T_Save_Sub_Totals; }

    public DbsGroup getPnd_T_Save_Sub_Totals_Pnd_T_Save_Area_1_Sub_Totals() { return pnd_T_Save_Sub_Totals_Pnd_T_Save_Area_1_Sub_Totals; }

    public DbsField getPnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Rate() { return pnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Rate; }

    public DbsField getPnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Prod() { return pnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Prod; }

    public DbsField getPnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Accum_Amtfslash_Units() { return pnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Accum_Amtfslash_Units; }

    public DbsField getPnd_T_Save_Sub_Totals_Pnd_T_Sub_Settled_Amtfslash_Units() { return pnd_T_Save_Sub_Totals_Pnd_T_Sub_Settled_Amtfslash_Units; }

    public DbsField getPnd_T_Save_Sub_Totals_Pnd_T_Sub_Rtb_Amtfslash_Units() { return pnd_T_Save_Sub_Totals_Pnd_T_Sub_Rtb_Amtfslash_Units; }

    public DbsField getPnd_T_Save_Sub_Totals_Pnd_T_Sub_Settled_Rtb_Amtfslash_Units() { return pnd_T_Save_Sub_Totals_Pnd_T_Sub_Settled_Rtb_Amtfslash_Units; 
        }

    public DbsField getPnd_T_Sub_Settled_Rtb_Amounts() { return pnd_T_Sub_Settled_Rtb_Amounts; }

    public DbsGroup getPnd_Tiaa_Sub_Totals() { return pnd_Tiaa_Sub_Totals; }

    public DbsGroup getPnd_Tiaa_Sub_Totals_Pnd_Tiaa_Area_1_Sub_Totals() { return pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Area_1_Sub_Totals; }

    public DbsField getPnd_Tiaa_Sub_Totals_Pnd_Tiaa_Da_Accum_Sub_Total() { return pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Da_Accum_Sub_Total; }

    public DbsField getPnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Sub_Total() { return pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Sub_Total; }

    public DbsField getPnd_Tiaa_Sub_Totals_Pnd_Tiaa_Rtb_Sub_Total() { return pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Rtb_Sub_Total; }

    public DbsField getPnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Rtb_Sub_Total() { return pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Rtb_Sub_Total; }

    public DbsGroup getPnd_Tiaa_Total_Settlement_Line() { return pnd_Tiaa_Total_Settlement_Line; }

    public DbsGroup getPnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Area_1_Settlement_Line() { return pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Area_1_Settlement_Line; 
        }

    public DbsField getPnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Rate_Total_Settle() { return pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Rate_Total_Settle; 
        }

    public DbsField getPnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Prod_Total_Settle() { return pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Prod_Total_Settle; 
        }

    public DbsField getPnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Accum_Total_Settle() { return pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Accum_Total_Settle; 
        }

    public DbsField getPnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Total_Settle() { return pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Total_Settle; 
        }

    public DbsField getPnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Rtb_Total_Settle() { return pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Rtb_Total_Settle; }

    public DbsField getPnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Rtb_Total_Settle() { return pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Rtb_Total_Settle; 
        }

    public DbsGroup getPnd_Tiaa_Final_Totals() { return pnd_Tiaa_Final_Totals; }

    public DbsGroup getPnd_Tiaa_Final_Totals_Pnd_Tiaa_Area_1_Final_Totals() { return pnd_Tiaa_Final_Totals_Pnd_Tiaa_Area_1_Final_Totals; }

    public DbsField getPnd_Tiaa_Final_Totals_Pnd_Tiaa_Da_Accum_Final_Total() { return pnd_Tiaa_Final_Totals_Pnd_Tiaa_Da_Accum_Final_Total; }

    public DbsField getPnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Final_Total() { return pnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Final_Total; }

    public DbsField getPnd_Tiaa_Final_Totals_Pnd_Tiaa_Rtb_Final_Total() { return pnd_Tiaa_Final_Totals_Pnd_Tiaa_Rtb_Final_Total; }

    public DbsField getPnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Rtb_Final_Total() { return pnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Rtb_Final_Total; }

    public DbsGroup getPnd_Tiaa_Standatd_Totals() { return pnd_Tiaa_Standatd_Totals; }

    public DbsField getPnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Prod_Std() { return pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Prod_Std; }

    public DbsField getPnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Da_Rate_Std() { return pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Da_Rate_Std; }

    public DbsField getPnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Rate_Std() { return pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Rate_Std; }

    public DbsField getPnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Settled_Std_Amt() { return pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Settled_Std_Amt; }

    public DbsField getPnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Amt() { return pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Amt; }

    public DbsField getPnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Dividend_Amt() { return pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Dividend_Amt; }

    public DbsGroup getPnd_Tiaa_Standard_Sub_Totals() { return pnd_Tiaa_Standard_Sub_Totals; }

    public DbsField getPnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Settled_Std_Sub_Total() { return pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Settled_Std_Sub_Total; 
        }

    public DbsField getPnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Sub_Total() { return pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Sub_Total; 
        }

    public DbsField getPnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Dividend_Sub_Total() { return pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Dividend_Sub_Total; 
        }

    public DbsGroup getPnd_Tiaa_Graded_Totals() { return pnd_Tiaa_Graded_Totals; }

    public DbsGroup getPnd_Tiaa_Graded_Totals_Pnd_Tiaa_Area_1_Graded_Totals() { return pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Area_1_Graded_Totals; }

    public DbsField getPnd_Tiaa_Graded_Totals_Pnd_Tiaa_Prod_Grd() { return pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Prod_Grd; }

    public DbsField getPnd_Tiaa_Graded_Totals_Pnd_Tiaa_Da_Rate_Grd() { return pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Da_Rate_Grd; }

    public DbsField getPnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Rate_Grd() { return pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Rate_Grd; }

    public DbsField getPnd_Tiaa_Graded_Totals_Pnd_Tiaa_Settled_Grd_Amt() { return pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Settled_Grd_Amt; }

    public DbsField getPnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Amt() { return pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Amt; }

    public DbsField getPnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Amt() { return pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Amt; }

    public DbsGroup getPnd_Tiaa_Graded_Sub_Totals() { return pnd_Tiaa_Graded_Sub_Totals; }

    public DbsGroup getPnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Area_1_Graded_Sub_Totals() { return pnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Area_1_Graded_Sub_Totals; 
        }

    public DbsField getPnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Settled_Grd_Sub_Total() { return pnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Settled_Grd_Sub_Total; 
        }

    public DbsField getPnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Sub_Total() { return pnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Sub_Total; 
        }

    public DbsField getPnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Sub_Total() { return pnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Sub_Total; 
        }

    public DbsGroup getPnd_Tiaa_Std_Method_Save_Amounts() { return pnd_Tiaa_Std_Method_Save_Amounts; }

    public DbsGroup getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Std_Method_Save_Amt() { return pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Std_Method_Save_Amt; 
        }

    public DbsField getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Da_Rate_Std_Payment_Amt() { return pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Da_Rate_Std_Payment_Amt; 
        }

    public DbsField getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Std_Payment_Amt() { return pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Std_Payment_Amt; 
        }

    public DbsField getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Da_Payment_Amt() { return pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Da_Payment_Amt; 
        }

    public DbsField getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Gur_Payment_Amt() { return pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Gur_Payment_Amt; 
        }

    public DbsField getPnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Div_Payment_Amt() { return pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Div_Payment_Amt; 
        }

    public DbsGroup getPnd_Tiaa_Std_Method_Save_Payments() { return pnd_Tiaa_Std_Method_Save_Payments; }

    public DbsGroup getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Std_Method_Save_Payment() { return pnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Std_Method_Save_Payment; 
        }

    public DbsField getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Std_Payment_Save() { return pnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Std_Payment_Save; 
        }

    public DbsField getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Gur_Payment_Save() { return pnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Gur_Payment_Save; 
        }

    public DbsField getPnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Div_Payment_Save() { return pnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Div_Payment_Save; 
        }

    public DbsGroup getPnd_Tiaa_Std_Method_Payment_Totals() { return pnd_Tiaa_Std_Method_Payment_Totals; }

    public DbsField getPnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Std_Payment() { return pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Std_Payment; 
        }

    public DbsField getPnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Payment() { return pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Payment; 
        }

    public DbsField getPnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Dividend_Payment() { return pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Dividend_Payment; 
        }

    public DbsField getPnd_Tiaa_Ia_Std_Accum_Final() { return pnd_Tiaa_Ia_Std_Accum_Final; }

    public DbsField getPnd_Tiaa_Ia_Std_Guranteed_Final() { return pnd_Tiaa_Ia_Std_Guranteed_Final; }

    public DbsField getPnd_Tiaa_Ia_Std_Dividend_Final() { return pnd_Tiaa_Ia_Std_Dividend_Final; }

    public DbsGroup getPnd_Tiaa_Grd_Method_Save_Amounts() { return pnd_Tiaa_Grd_Method_Save_Amounts; }

    public DbsGroup getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Grd_Method_Save_Amt() { return pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Grd_Method_Save_Amt; 
        }

    public DbsField getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Da_Rate_Grd_Payment_Amt() { return pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Da_Rate_Grd_Payment_Amt; 
        }

    public DbsField getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Grd_Payment_Amt() { return pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Grd_Payment_Amt; 
        }

    public DbsField getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Accum_Payment_Amt() { return pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Accum_Payment_Amt; 
        }

    public DbsField getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Gur_Payment_Amt() { return pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Gur_Payment_Amt; 
        }

    public DbsField getPnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Div_Payment_Amt() { return pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Div_Payment_Amt; 
        }

    public DbsGroup getPnd_Tiaa_Grd_Method_Save_Payments() { return pnd_Tiaa_Grd_Method_Save_Payments; }

    public DbsGroup getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Grd_Method_Save_Payment() { return pnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Grd_Method_Save_Payment; 
        }

    public DbsField getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Grd_Payment_Save() { return pnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Grd_Payment_Save; 
        }

    public DbsField getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Gur_Payment_Save() { return pnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Gur_Payment_Save; 
        }

    public DbsField getPnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Div_Payment_Save() { return pnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Div_Payment_Save; 
        }

    public DbsGroup getPnd_Tiaa_Grd_Method_Payment_Totals() { return pnd_Tiaa_Grd_Method_Payment_Totals; }

    public DbsField getPnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Grd_Payment() { return pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Grd_Payment; 
        }

    public DbsField getPnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Payment() { return pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Payment; 
        }

    public DbsField getPnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Payment() { return pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Payment; 
        }

    public DbsField getPnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Pay_Renew() { return pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Pay_Renew; 
        }

    public DbsField getPnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Pay_Renew() { return pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Pay_Renew; 
        }

    public DbsField getPnd_Tiaa_Ia_Grd_Guranteed_Final() { return pnd_Tiaa_Ia_Grd_Guranteed_Final; }

    public DbsField getPnd_Tiaa_Ia_Grd_Dividend_Final() { return pnd_Tiaa_Ia_Grd_Dividend_Final; }

    public DbsField getPnd_Tiaa_Ia_Grd_Guranteed_Fin_Renew() { return pnd_Tiaa_Ia_Grd_Guranteed_Fin_Renew; }

    public DbsField getPnd_Tiaa_Ia_Grd_Dividend_Fin_Renew() { return pnd_Tiaa_Ia_Grd_Dividend_Fin_Renew; }

    public DbsGroup getPnd_Tiaa_Header_Amt() { return pnd_Tiaa_Header_Amt; }

    public DbsField getPnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Account() { return pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Account; }

    public DbsField getPnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Amt() { return pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Amt; }

    public DbsField getPnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Typ() { return pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Typ; }

    public DbsField getPnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Rtb_Amt() { return pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Rtb_Amt; }

    public DbsField getPnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Rtb_Typ() { return pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Rtb_Typ; }

    public DbsField getPnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Amt() { return pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Amt; }

    public DbsField getPnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Typ() { return pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Typ; }

    public DbsField getPnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Cref_Mnthly_Units() { return pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Cref_Mnthly_Units; }

    public DbsGroup getPnd_Tiaa_Header_Settle_Amounts() { return pnd_Tiaa_Header_Settle_Amounts; }

    public DbsField getPnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Settle_Amount() { return pnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Settle_Amount; 
        }

    public DbsField getPnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Rtb_Settle_Amount() { return pnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Rtb_Settle_Amount; 
        }

    public DbsGroup getPnd_Tiaa_Ivc_Amounts() { return pnd_Tiaa_Ivc_Amounts; }

    public DbsField getPnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Total() { return pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Total; }

    public DbsField getPnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Rtb() { return pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Rtb; }

    public DbsField getPnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Annuity() { return pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Annuity; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Tiaa_Rec_Cnt = newFieldInRecord("pnd_Tiaa_Rec_Cnt", "#TIAA-REC-CNT", FieldType.NUMERIC, 7);

        pnd_Tiaa_Minor_Totals = newGroupInRecord("pnd_Tiaa_Minor_Totals", "#TIAA-MINOR-TOTALS");
        pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Rate = pnd_Tiaa_Minor_Totals.newFieldInGroup("pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Rate", "#TIAA-DA-RATE", FieldType.STRING, 
            2);
        pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Accum_Amtfslash_Units = pnd_Tiaa_Minor_Totals.newFieldInGroup("pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Accum_Amtfslash_Units", 
            "#TIAA-DA-ACCUM-AMT/UNITS", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Amtfslash_Units = pnd_Tiaa_Minor_Totals.newFieldInGroup("pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Amtfslash_Units", 
            "#TIAA-SETTLED-AMT/UNITS", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Rtb_Amtfslash_Units = pnd_Tiaa_Minor_Totals.newFieldInGroup("pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Rtb_Amtfslash_Units", 
            "#TIAA-RTB-AMT/UNITS", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Rtb_Amtfslash_Unit = pnd_Tiaa_Minor_Totals.newFieldInGroup("pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Rtb_Amtfslash_Unit", 
            "#TIAA-SETTLED-RTB-AMT/UNIT", FieldType.PACKED_DECIMAL, 11,2);

        pnd_T_Save_Sub_Totals = newGroupInRecord("pnd_T_Save_Sub_Totals", "#T-SAVE-SUB-TOTALS");
        pnd_T_Save_Sub_Totals_Pnd_T_Save_Area_1_Sub_Totals = pnd_T_Save_Sub_Totals.newGroupArrayInGroup("pnd_T_Save_Sub_Totals_Pnd_T_Save_Area_1_Sub_Totals", 
            "#T-SAVE-AREA-1-SUB-TOTALS", new DbsArrayController(1,120));
        pnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Rate = pnd_T_Save_Sub_Totals_Pnd_T_Save_Area_1_Sub_Totals.newFieldInGroup("pnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Rate", 
            "#T-SUB-DA-RATE", FieldType.STRING, 2);
        pnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Prod = pnd_T_Save_Sub_Totals_Pnd_T_Save_Area_1_Sub_Totals.newFieldInGroup("pnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Prod", 
            "#T-SUB-DA-PROD", FieldType.STRING, 4);
        pnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Accum_Amtfslash_Units = pnd_T_Save_Sub_Totals_Pnd_T_Save_Area_1_Sub_Totals.newFieldInGroup("pnd_T_Save_Sub_Totals_Pnd_T_Sub_Da_Accum_Amtfslash_Units", 
            "#T-SUB-DA-ACCUM-AMT/UNITS", FieldType.PACKED_DECIMAL, 14,2);
        pnd_T_Save_Sub_Totals_Pnd_T_Sub_Settled_Amtfslash_Units = pnd_T_Save_Sub_Totals_Pnd_T_Save_Area_1_Sub_Totals.newFieldInGroup("pnd_T_Save_Sub_Totals_Pnd_T_Sub_Settled_Amtfslash_Units", 
            "#T-SUB-SETTLED-AMT/UNITS", FieldType.PACKED_DECIMAL, 14,2);
        pnd_T_Save_Sub_Totals_Pnd_T_Sub_Rtb_Amtfslash_Units = pnd_T_Save_Sub_Totals_Pnd_T_Save_Area_1_Sub_Totals.newFieldInGroup("pnd_T_Save_Sub_Totals_Pnd_T_Sub_Rtb_Amtfslash_Units", 
            "#T-SUB-RTB-AMT/UNITS", FieldType.PACKED_DECIMAL, 14,2);
        pnd_T_Save_Sub_Totals_Pnd_T_Sub_Settled_Rtb_Amtfslash_Units = pnd_T_Save_Sub_Totals_Pnd_T_Save_Area_1_Sub_Totals.newFieldInGroup("pnd_T_Save_Sub_Totals_Pnd_T_Sub_Settled_Rtb_Amtfslash_Units", 
            "#T-SUB-SETTLED-RTB-AMT/UNITS", FieldType.PACKED_DECIMAL, 14,2);

        pnd_T_Sub_Settled_Rtb_Amounts = newFieldInRecord("pnd_T_Sub_Settled_Rtb_Amounts", "#T-SUB-SETTLED-RTB-AMOUNTS", FieldType.PACKED_DECIMAL, 14,2);

        pnd_Tiaa_Sub_Totals = newGroupInRecord("pnd_Tiaa_Sub_Totals", "#TIAA-SUB-TOTALS");
        pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Area_1_Sub_Totals = pnd_Tiaa_Sub_Totals.newGroupInGroup("pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Area_1_Sub_Totals", "#TIAA-AREA-1-SUB-TOTALS");
        pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Da_Accum_Sub_Total = pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Area_1_Sub_Totals.newFieldInGroup("pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Da_Accum_Sub_Total", 
            "#TIAA-DA-ACCUM-SUB-TOTAL", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Sub_Total = pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Area_1_Sub_Totals.newFieldInGroup("pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Sub_Total", 
            "#TIAA-SETTLED-SUB-TOTAL", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Rtb_Sub_Total = pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Area_1_Sub_Totals.newFieldInGroup("pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Rtb_Sub_Total", 
            "#TIAA-RTB-SUB-TOTAL", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Rtb_Sub_Total = pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Area_1_Sub_Totals.newFieldInGroup("pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Rtb_Sub_Total", 
            "#TIAA-SETTLED-RTB-SUB-TOTAL", FieldType.PACKED_DECIMAL, 14,2);

        pnd_Tiaa_Total_Settlement_Line = newGroupInRecord("pnd_Tiaa_Total_Settlement_Line", "#TIAA-TOTAL-SETTLEMENT-LINE");
        pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Area_1_Settlement_Line = pnd_Tiaa_Total_Settlement_Line.newGroupInGroup("pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Area_1_Settlement_Line", 
            "#TIAA-AREA-1-SETTLEMENT-LINE");
        pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Rate_Total_Settle = pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Area_1_Settlement_Line.newFieldInGroup("pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Rate_Total_Settle", 
            "#TIAA-DA-RATE-TOTAL-SETTLE", FieldType.STRING, 2);
        pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Prod_Total_Settle = pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Area_1_Settlement_Line.newFieldInGroup("pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Prod_Total_Settle", 
            "#TIAA-DA-PROD-TOTAL-SETTLE", FieldType.STRING, 4);
        pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Accum_Total_Settle = pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Area_1_Settlement_Line.newFieldInGroup("pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Accum_Total_Settle", 
            "#TIAA-DA-ACCUM-TOTAL-SETTLE", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Total_Settle = pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Area_1_Settlement_Line.newFieldInGroup("pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Total_Settle", 
            "#TIAA-SETTLED-TOTAL-SETTLE", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Rtb_Total_Settle = pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Area_1_Settlement_Line.newFieldInGroup("pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Rtb_Total_Settle", 
            "#TIAA-RTB-TOTAL-SETTLE", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Rtb_Total_Settle = pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Area_1_Settlement_Line.newFieldInGroup("pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Rtb_Total_Settle", 
            "#TIAA-SETTLED-RTB-TOTAL-SETTLE", FieldType.PACKED_DECIMAL, 14,2);

        pnd_Tiaa_Final_Totals = newGroupInRecord("pnd_Tiaa_Final_Totals", "#TIAA-FINAL-TOTALS");
        pnd_Tiaa_Final_Totals_Pnd_Tiaa_Area_1_Final_Totals = pnd_Tiaa_Final_Totals.newGroupInGroup("pnd_Tiaa_Final_Totals_Pnd_Tiaa_Area_1_Final_Totals", 
            "#TIAA-AREA-1-FINAL-TOTALS");
        pnd_Tiaa_Final_Totals_Pnd_Tiaa_Da_Accum_Final_Total = pnd_Tiaa_Final_Totals_Pnd_Tiaa_Area_1_Final_Totals.newFieldInGroup("pnd_Tiaa_Final_Totals_Pnd_Tiaa_Da_Accum_Final_Total", 
            "#TIAA-DA-ACCUM-FINAL-TOTAL", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Final_Total = pnd_Tiaa_Final_Totals_Pnd_Tiaa_Area_1_Final_Totals.newFieldInGroup("pnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Final_Total", 
            "#TIAA-SETTLED-FINAL-TOTAL", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Final_Totals_Pnd_Tiaa_Rtb_Final_Total = pnd_Tiaa_Final_Totals_Pnd_Tiaa_Area_1_Final_Totals.newFieldInGroup("pnd_Tiaa_Final_Totals_Pnd_Tiaa_Rtb_Final_Total", 
            "#TIAA-RTB-FINAL-TOTAL", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Rtb_Final_Total = pnd_Tiaa_Final_Totals_Pnd_Tiaa_Area_1_Final_Totals.newFieldInGroup("pnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Rtb_Final_Total", 
            "#TIAA-SETTLED-RTB-FINAL-TOTAL", FieldType.PACKED_DECIMAL, 14,2);

        pnd_Tiaa_Standatd_Totals = newGroupInRecord("pnd_Tiaa_Standatd_Totals", "#TIAA-STANDATD-TOTALS");
        pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Prod_Std = pnd_Tiaa_Standatd_Totals.newFieldInGroup("pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Prod_Std", "#TIAA-PROD-STD", 
            FieldType.STRING, 1);
        pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Da_Rate_Std = pnd_Tiaa_Standatd_Totals.newFieldInGroup("pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Da_Rate_Std", "#TIAA-DA-RATE-STD", 
            FieldType.STRING, 2);
        pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Rate_Std = pnd_Tiaa_Standatd_Totals.newFieldInGroup("pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Rate_Std", "#TIAA-IA-RATE-STD", 
            FieldType.STRING, 2);
        pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Settled_Std_Amt = pnd_Tiaa_Standatd_Totals.newFieldInGroup("pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Settled_Std_Amt", 
            "#TIAA-SETTLED-STD-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Amt = pnd_Tiaa_Standatd_Totals.newFieldInGroup("pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Amt", 
            "#TIAA-IA-STD-GURANTEED-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Dividend_Amt = pnd_Tiaa_Standatd_Totals.newFieldInGroup("pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Dividend_Amt", 
            "#TIAA-IA-STD-DIVIDEND-AMT", FieldType.PACKED_DECIMAL, 11,2);

        pnd_Tiaa_Standard_Sub_Totals = newGroupInRecord("pnd_Tiaa_Standard_Sub_Totals", "#TIAA-STANDARD-SUB-TOTALS");
        pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Settled_Std_Sub_Total = pnd_Tiaa_Standard_Sub_Totals.newFieldInGroup("pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Settled_Std_Sub_Total", 
            "#TIAA-SETTLED-STD-SUB-TOTAL", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Sub_Total = pnd_Tiaa_Standard_Sub_Totals.newFieldInGroup("pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Sub_Total", 
            "#TIAA-IA-STD-GURANTEED-SUB-TOTAL", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Dividend_Sub_Total = pnd_Tiaa_Standard_Sub_Totals.newFieldInGroup("pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Dividend_Sub_Total", 
            "#TIAA-IA-STD-DIVIDEND-SUB-TOTAL", FieldType.PACKED_DECIMAL, 14,2);

        pnd_Tiaa_Graded_Totals = newGroupInRecord("pnd_Tiaa_Graded_Totals", "#TIAA-GRADED-TOTALS");
        pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Area_1_Graded_Totals = pnd_Tiaa_Graded_Totals.newGroupInGroup("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Area_1_Graded_Totals", 
            "#TIAA-AREA-1-GRADED-TOTALS");
        pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Prod_Grd = pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Area_1_Graded_Totals.newFieldInGroup("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Prod_Grd", 
            "#TIAA-PROD-GRD", FieldType.STRING, 1);
        pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Da_Rate_Grd = pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Area_1_Graded_Totals.newFieldInGroup("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Da_Rate_Grd", 
            "#TIAA-DA-RATE-GRD", FieldType.STRING, 2);
        pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Rate_Grd = pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Area_1_Graded_Totals.newFieldInGroup("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Rate_Grd", 
            "#TIAA-IA-RATE-GRD", FieldType.STRING, 2);
        pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Settled_Grd_Amt = pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Area_1_Graded_Totals.newFieldInGroup("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Settled_Grd_Amt", 
            "#TIAA-SETTLED-GRD-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Amt = pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Area_1_Graded_Totals.newFieldInGroup("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Amt", 
            "#TIAA-IA-GRD-GURANTEED-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Amt = pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Area_1_Graded_Totals.newFieldInGroup("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Amt", 
            "#TIAA-IA-GRD-DIVIDEND-AMT", FieldType.PACKED_DECIMAL, 11,2);

        pnd_Tiaa_Graded_Sub_Totals = newGroupInRecord("pnd_Tiaa_Graded_Sub_Totals", "#TIAA-GRADED-SUB-TOTALS");
        pnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Area_1_Graded_Sub_Totals = pnd_Tiaa_Graded_Sub_Totals.newGroupInGroup("pnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Area_1_Graded_Sub_Totals", 
            "#TIAA-AREA-1-GRADED-SUB-TOTALS");
        pnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Settled_Grd_Sub_Total = pnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Area_1_Graded_Sub_Totals.newFieldInGroup("pnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Settled_Grd_Sub_Total", 
            "#TIAA-SETTLED-GRD-SUB-TOTAL", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Sub_Total = pnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Area_1_Graded_Sub_Totals.newFieldInGroup("pnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Sub_Total", 
            "#TIAA-IA-GRD-GURANTEED-SUB-TOTAL", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Sub_Total = pnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Area_1_Graded_Sub_Totals.newFieldInGroup("pnd_Tiaa_Graded_Sub_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Sub_Total", 
            "#TIAA-IA-GRD-DIVIDEND-SUB-TOTAL", FieldType.PACKED_DECIMAL, 14,2);

        pnd_Tiaa_Std_Method_Save_Amounts = newGroupInRecord("pnd_Tiaa_Std_Method_Save_Amounts", "#TIAA-STD-METHOD-SAVE-AMOUNTS");
        pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Std_Method_Save_Amt = pnd_Tiaa_Std_Method_Save_Amounts.newGroupArrayInGroup("pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Std_Method_Save_Amt", 
            "#TIAA-STD-METHOD-SAVE-AMT", new DbsArrayController(1,250));
        pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Da_Rate_Std_Payment_Amt = pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Std_Method_Save_Amt.newFieldInGroup("pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Da_Rate_Std_Payment_Amt", 
            "#TIAA-DA-RATE-STD-PAYMENT-AMT", FieldType.STRING, 2);
        pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Std_Payment_Amt = pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Std_Method_Save_Amt.newFieldInGroup("pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Std_Payment_Amt", 
            "#TIAA-IA-RATE-STD-PAYMENT-AMT", FieldType.STRING, 2);
        pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Da_Payment_Amt = pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Std_Method_Save_Amt.newFieldInGroup("pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Da_Payment_Amt", 
            "#TIAA-IA-STD-DA-PAYMENT-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Gur_Payment_Amt = pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Std_Method_Save_Amt.newFieldInGroup("pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Gur_Payment_Amt", 
            "#TIAA-IA-STD-GUR-PAYMENT-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Div_Payment_Amt = pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Std_Method_Save_Amt.newFieldInGroup("pnd_Tiaa_Std_Method_Save_Amounts_Pnd_Tiaa_Ia_Std_Div_Payment_Amt", 
            "#TIAA-IA-STD-DIV-PAYMENT-AMT", FieldType.PACKED_DECIMAL, 14,2);

        pnd_Tiaa_Std_Method_Save_Payments = newGroupInRecord("pnd_Tiaa_Std_Method_Save_Payments", "#TIAA-STD-METHOD-SAVE-PAYMENTS");
        pnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Std_Method_Save_Payment = pnd_Tiaa_Std_Method_Save_Payments.newGroupArrayInGroup("pnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Std_Method_Save_Payment", 
            "#TIAA-STD-METHOD-SAVE-PAYMENT", new DbsArrayController(1,250));
        pnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Std_Payment_Save = pnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Std_Method_Save_Payment.newFieldInGroup("pnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Std_Payment_Save", 
            "#TIAA-IA-RATE-STD-PAYMENT-SAVE", FieldType.STRING, 2);
        pnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Gur_Payment_Save = pnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Std_Method_Save_Payment.newFieldInGroup("pnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Gur_Payment_Save", 
            "#TIAA-IA-STD-GUR-PAYMENT-SAVE", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Div_Payment_Save = pnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Std_Method_Save_Payment.newFieldInGroup("pnd_Tiaa_Std_Method_Save_Payments_Pnd_Tiaa_Ia_Std_Div_Payment_Save", 
            "#TIAA-IA-STD-DIV-PAYMENT-SAVE", FieldType.PACKED_DECIMAL, 12,2);

        pnd_Tiaa_Std_Method_Payment_Totals = newGroupInRecord("pnd_Tiaa_Std_Method_Payment_Totals", "#TIAA-STD-METHOD-PAYMENT-TOTALS");
        pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Std_Payment = pnd_Tiaa_Std_Method_Payment_Totals.newFieldInGroup("pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Std_Payment", 
            "#TIAA-IA-RATE-STD-PAYMENT", FieldType.STRING, 2);
        pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Payment = pnd_Tiaa_Std_Method_Payment_Totals.newFieldInGroup("pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Payment", 
            "#TIAA-IA-STD-GURANTEED-PAYMENT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Dividend_Payment = pnd_Tiaa_Std_Method_Payment_Totals.newFieldInGroup("pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Dividend_Payment", 
            "#TIAA-IA-STD-DIVIDEND-PAYMENT", FieldType.PACKED_DECIMAL, 12,2);

        pnd_Tiaa_Ia_Std_Accum_Final = newFieldInRecord("pnd_Tiaa_Ia_Std_Accum_Final", "#TIAA-IA-STD-ACCUM-FINAL", FieldType.PACKED_DECIMAL, 14,2);

        pnd_Tiaa_Ia_Std_Guranteed_Final = newFieldInRecord("pnd_Tiaa_Ia_Std_Guranteed_Final", "#TIAA-IA-STD-GURANTEED-FINAL", FieldType.PACKED_DECIMAL, 
            12,2);

        pnd_Tiaa_Ia_Std_Dividend_Final = newFieldInRecord("pnd_Tiaa_Ia_Std_Dividend_Final", "#TIAA-IA-STD-DIVIDEND-FINAL", FieldType.PACKED_DECIMAL, 12,
            2);

        pnd_Tiaa_Grd_Method_Save_Amounts = newGroupInRecord("pnd_Tiaa_Grd_Method_Save_Amounts", "#TIAA-GRD-METHOD-SAVE-AMOUNTS");
        pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Grd_Method_Save_Amt = pnd_Tiaa_Grd_Method_Save_Amounts.newGroupArrayInGroup("pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Grd_Method_Save_Amt", 
            "#TIAA-GRD-METHOD-SAVE-AMT", new DbsArrayController(1,250));
        pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Da_Rate_Grd_Payment_Amt = pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Grd_Method_Save_Amt.newFieldInGroup("pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Da_Rate_Grd_Payment_Amt", 
            "#TIAA-DA-RATE-GRD-PAYMENT-AMT", FieldType.STRING, 2);
        pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Grd_Payment_Amt = pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Grd_Method_Save_Amt.newFieldInGroup("pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Rate_Grd_Payment_Amt", 
            "#TIAA-IA-RATE-GRD-PAYMENT-AMT", FieldType.STRING, 2);
        pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Accum_Payment_Amt = pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Grd_Method_Save_Amt.newFieldInGroup("pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Accum_Payment_Amt", 
            "#TIAA-IA-GRD-ACCUM-PAYMENT-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Gur_Payment_Amt = pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Grd_Method_Save_Amt.newFieldInGroup("pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Gur_Payment_Amt", 
            "#TIAA-IA-GRD-GUR-PAYMENT-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Div_Payment_Amt = pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Grd_Method_Save_Amt.newFieldInGroup("pnd_Tiaa_Grd_Method_Save_Amounts_Pnd_Tiaa_Ia_Grd_Div_Payment_Amt", 
            "#TIAA-IA-GRD-DIV-PAYMENT-AMT", FieldType.PACKED_DECIMAL, 14,2);

        pnd_Tiaa_Grd_Method_Save_Payments = newGroupInRecord("pnd_Tiaa_Grd_Method_Save_Payments", "#TIAA-GRD-METHOD-SAVE-PAYMENTS");
        pnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Grd_Method_Save_Payment = pnd_Tiaa_Grd_Method_Save_Payments.newGroupArrayInGroup("pnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Grd_Method_Save_Payment", 
            "#TIAA-GRD-METHOD-SAVE-PAYMENT", new DbsArrayController(1,250));
        pnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Grd_Payment_Save = pnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Grd_Method_Save_Payment.newFieldInGroup("pnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Rate_Grd_Payment_Save", 
            "#TIAA-IA-RATE-GRD-PAYMENT-SAVE", FieldType.STRING, 2);
        pnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Gur_Payment_Save = pnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Grd_Method_Save_Payment.newFieldInGroup("pnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Gur_Payment_Save", 
            "#TIAA-IA-GRD-GUR-PAYMENT-SAVE", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Div_Payment_Save = pnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Grd_Method_Save_Payment.newFieldInGroup("pnd_Tiaa_Grd_Method_Save_Payments_Pnd_Tiaa_Ia_Grd_Div_Payment_Save", 
            "#TIAA-IA-GRD-DIV-PAYMENT-SAVE", FieldType.PACKED_DECIMAL, 14,2);

        pnd_Tiaa_Grd_Method_Payment_Totals = newGroupInRecord("pnd_Tiaa_Grd_Method_Payment_Totals", "#TIAA-GRD-METHOD-PAYMENT-TOTALS");
        pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Grd_Payment = pnd_Tiaa_Grd_Method_Payment_Totals.newFieldInGroup("pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Grd_Payment", 
            "#TIAA-IA-RATE-GRD-PAYMENT", FieldType.STRING, 2);
        pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Payment = pnd_Tiaa_Grd_Method_Payment_Totals.newFieldInGroup("pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Payment", 
            "#TIAA-IA-GRD-GURANTEED-PAYMENT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Payment = pnd_Tiaa_Grd_Method_Payment_Totals.newFieldInGroup("pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Payment", 
            "#TIAA-IA-GRD-DIVIDEND-PAYMENT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Pay_Renew = pnd_Tiaa_Grd_Method_Payment_Totals.newFieldInGroup("pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Pay_Renew", 
            "#TIAA-IA-GRD-GURANTEED-PAY-RENEW", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Pay_Renew = pnd_Tiaa_Grd_Method_Payment_Totals.newFieldInGroup("pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Pay_Renew", 
            "#TIAA-IA-GRD-DIVIDEND-PAY-RENEW", FieldType.PACKED_DECIMAL, 12,2);

        pnd_Tiaa_Ia_Grd_Guranteed_Final = newFieldInRecord("pnd_Tiaa_Ia_Grd_Guranteed_Final", "#TIAA-IA-GRD-GURANTEED-FINAL", FieldType.PACKED_DECIMAL, 
            12,2);

        pnd_Tiaa_Ia_Grd_Dividend_Final = newFieldInRecord("pnd_Tiaa_Ia_Grd_Dividend_Final", "#TIAA-IA-GRD-DIVIDEND-FINAL", FieldType.PACKED_DECIMAL, 12,
            2);

        pnd_Tiaa_Ia_Grd_Guranteed_Fin_Renew = newFieldInRecord("pnd_Tiaa_Ia_Grd_Guranteed_Fin_Renew", "#TIAA-IA-GRD-GURANTEED-FIN-RENEW", FieldType.PACKED_DECIMAL, 
            12,2);

        pnd_Tiaa_Ia_Grd_Dividend_Fin_Renew = newFieldInRecord("pnd_Tiaa_Ia_Grd_Dividend_Fin_Renew", "#TIAA-IA-GRD-DIVIDEND-FIN-RENEW", FieldType.PACKED_DECIMAL, 
            12,2);

        pnd_Tiaa_Header_Amt = newGroupInRecord("pnd_Tiaa_Header_Amt", "#TIAA-HEADER-AMT");
        pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Account = pnd_Tiaa_Header_Amt.newFieldInGroup("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Account", "#TIAA-HEADER-ACCOUNT", 
            FieldType.STRING, 4);
        pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Amt = pnd_Tiaa_Header_Amt.newFieldInGroup("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Amt", "#TIAA-HEADER-SETTLE-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);
        pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Typ = pnd_Tiaa_Header_Amt.newFieldInGroup("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Typ", "#TIAA-HEADER-SETTLE-TYP", 
            FieldType.STRING, 1);
        pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Rtb_Amt = pnd_Tiaa_Header_Amt.newFieldInGroup("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Rtb_Amt", "#TIAA-HEADER-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Rtb_Typ = pnd_Tiaa_Header_Amt.newFieldInGroup("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Rtb_Typ", "#TIAA-HEADER-RTB-TYP", 
            FieldType.STRING, 1);
        pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Amt = pnd_Tiaa_Header_Amt.newFieldInGroup("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Amt", "#TIAA-HEADER-GRD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Typ = pnd_Tiaa_Header_Amt.newFieldInGroup("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Typ", "#TIAA-HEADER-GRD-TYP", 
            FieldType.STRING, 1);
        pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Cref_Mnthly_Units = pnd_Tiaa_Header_Amt.newFieldInGroup("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Cref_Mnthly_Units", 
            "#TIAA-HEADER-CREF-MNTHLY-UNITS", FieldType.PACKED_DECIMAL, 10,3);

        pnd_Tiaa_Header_Settle_Amounts = newGroupInRecord("pnd_Tiaa_Header_Settle_Amounts", "#TIAA-HEADER-SETTLE-AMOUNTS");
        pnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Settle_Amount = pnd_Tiaa_Header_Settle_Amounts.newFieldInGroup("pnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Settle_Amount", 
            "#TIAA-HEADER-SETTLE-AMOUNT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Rtb_Settle_Amount = pnd_Tiaa_Header_Settle_Amounts.newFieldInGroup("pnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Rtb_Settle_Amount", 
            "#TIAA-HEADER-RTB-SETTLE-AMOUNT", FieldType.PACKED_DECIMAL, 11,2);

        pnd_Tiaa_Ivc_Amounts = newGroupInRecord("pnd_Tiaa_Ivc_Amounts", "#TIAA-IVC-AMOUNTS");
        pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Total = pnd_Tiaa_Ivc_Amounts.newFieldInGroup("pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Total", "#TIAA-IVC-TOTAL", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Rtb = pnd_Tiaa_Ivc_Amounts.newFieldInGroup("pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Rtb", "#TIAA-IVC-RTB", FieldType.PACKED_DECIMAL, 
            11,2);
        pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Annuity = pnd_Tiaa_Ivc_Amounts.newFieldInGroup("pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Annuity", "#TIAA-IVC-ANNUITY", 
            FieldType.PACKED_DECIMAL, 11,2);

        this.setRecordName("LdaAdsl762");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAdsl762() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
