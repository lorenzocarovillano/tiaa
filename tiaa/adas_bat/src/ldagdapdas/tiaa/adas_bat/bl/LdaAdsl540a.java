/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:49:49 PM
**        * FROM NATURAL LDA     : ADSL540A
************************************************************
**        * FILE NAME            : LdaAdsl540a.java
**        * CLASS NAME           : LdaAdsl540a
**        * INSTANCE NAME        : LdaAdsl540a
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdsl540a extends DbsRecord
{
    // Properties
    private DataAccessProgramView vw_ads_Prtcpnt_View;
    private DbsField ads_Prtcpnt_View_Rqst_Id;
    private DbsGroup ads_Prtcpnt_View_Adp_Ia_Dta;
    private DbsField ads_Prtcpnt_View_Adp_Ia_Tiaa_Nbr;
    private DbsField ads_Prtcpnt_View_Adp_Ia_Tiaa_Rea_Nbr;
    private DbsField ads_Prtcpnt_View_Adp_Ia_Tiaa_Payee_Cd;
    private DbsField ads_Prtcpnt_View_Adp_Ia_Cref_Nbr;
    private DbsField ads_Prtcpnt_View_Adp_Ia_Cref_Payee_Cd;
    private DbsField ads_Prtcpnt_View_Adp_Bps_Unit;
    private DbsField ads_Prtcpnt_View_Adp_Emplymnt_Term_Dte;
    private DbsField ads_Prtcpnt_View_Adp_Cntr_Prt_Issue_Dte;
    private DbsField ads_Prtcpnt_View_Adp_Entry_Dte;
    private DbsField ads_Prtcpnt_View_Adp_Lst_Actvty_Dte;
    private DbsField ads_Prtcpnt_View_Adp_Annty_Strt_Dte;
    private DbsField ads_Prtcpnt_View_Adp_Annt_Typ_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Apprvl_Wo_Term_Dte;
    private DbsGroup ads_Prtcpnt_View_Adp_Cntrcts_In_RqstMuGroup;
    private DbsField ads_Prtcpnt_View_Adp_Cntrcts_In_Rqst;
    private DbsField ads_Prtcpnt_View_Adp_Unique_Id;
    private DbsField ads_Prtcpnt_View_Adp_Effctv_Dte;
    private DbsField ads_Prtcpnt_View_Adp_Entry_Tme;
    private DbsField ads_Prtcpnt_View_Adp_Entry_User_Id;
    private DbsField ads_Prtcpnt_View_Adp_Rep_Nme;
    private DbsField ads_Prtcpnt_View_Adp_Wpid;
    private DbsField ads_Prtcpnt_View_Adp_Mit_Log_Dte_Tme;
    private DbsField ads_Prtcpnt_View_Adp_Opn_Clsd_Ind;
    private DbsField ads_Prtcpnt_View_Adp_In_Prgrss_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Rcprcl_Dte;
    private DbsField ads_Prtcpnt_View_Adp_Stts_Cde;
    private DbsGroup ads_Prtcpnt_View_Adp_Crrspndce_Mlng_Dta;
    private DbsGroup ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_TxtMuGroup;
    private DbsField ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt;
    private DbsField ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Zip;
    private DbsField ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Typ_Cd;
    private DbsField ads_Prtcpnt_View_Adp_Crrspndnce_Addr_Lst_Chg_Dte;
    private DbsField ads_Prtcpnt_View_Adp_Forms_Cmplte_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Frst_Annt_Ctznshp;
    private DbsField ads_Prtcpnt_View_Adp_Frst_Annt_Rsdnc_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Frst_Annt_Ssn;
    private DbsField ads_Prtcpnt_View_Adp_Frst_Annt_Title_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Frst_Annt_Frst_Nme;
    private DbsField ads_Prtcpnt_View_Adp_Frst_Annt_Mid_Nme;
    private DbsField ads_Prtcpnt_View_Adp_Frst_Annt_Lst_Nme;
    private DbsField ads_Prtcpnt_View_Adp_Frst_Annt_Sfx;
    private DbsField ads_Prtcpnt_View_Adp_Frst_Annt_Dte_Of_Brth;
    private DbsField ads_Prtcpnt_View_Adp_Frst_Annt_Sex_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Scnd_Annt_Frst_Nme;
    private DbsField ads_Prtcpnt_View_Adp_Scnd_Annt_Mid_Nme;
    private DbsField ads_Prtcpnt_View_Adp_Scnd_Annt_Lst_Nme;
    private DbsField ads_Prtcpnt_View_Adp_Scnd_Annt_Rltnshp;
    private DbsField ads_Prtcpnt_View_Adp_Scnd_Annt_Dte_Of_Brth;
    private DbsField ads_Prtcpnt_View_Adp_Scnd_Annt_Ssn;
    private DbsField ads_Prtcpnt_View_Adp_Scnd_Annt_Sex_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Annty_Optn;
    private DbsField ads_Prtcpnt_View_Adp_Grntee_Period;
    private DbsField ads_Prtcpnt_View_Adp_Grntee_Period_Mnths;
    private DbsField ads_Prtcpnt_View_Adp_Pymnt_Mode;
    private DbsField ads_Prtcpnt_View_Adp_Settl_All_Tiaa_Grd_Pct;
    private DbsField ads_Prtcpnt_View_Adp_Settl_All_Cref_Mon_Pct;
    private DbsField ads_Prtcpnt_View_Adp_Alt_Dest_Addr_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Spcfc_Brkdwn_Ind;
    private DbsGroup ads_Prtcpnt_View_Adp_Alt_Dest_Data;
    private DbsField ads_Prtcpnt_View_Adp_Alt_Payee_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Alt_Carrier_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Alt_Dest_Nme;
    private DbsGroup ads_Prtcpnt_View_Adp_Alt_Dest_Addr_TxtMuGroup;
    private DbsField ads_Prtcpnt_View_Adp_Alt_Dest_Addr_Txt;
    private DbsField ads_Prtcpnt_View_Adp_Alt_Dest_Addr_Zip;
    private DbsField ads_Prtcpnt_View_Adp_Alt_Dest_Addr_Typ_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Alt_Dest_Acct_Nbr;
    private DbsField ads_Prtcpnt_View_Adp_Alt_Dest_Acct_Typ;
    private DbsField ads_Prtcpnt_View_Adp_Alt_Dest_Trnst_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Alt_Dest_Hold_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Alt_Dest_Rlvr_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Alt_Dest_Rlvr_Dest;
    private DbsField ads_Prtcpnt_View_Adp_Sbjct_To_Wvr_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Sbjct_To_Spsl_Cnsnt;
    private DbsGroup ads_Prtcpnt_View_Adp_Forms_Data;
    private DbsField ads_Prtcpnt_View_Adp_Forms_Rcvd_Dte;
    private DbsField ads_Prtcpnt_View_Adp_Frst_Annt_Prf_Of_Age_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Scnd_Annt_Prf_Of_Age_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Glbl_Pay_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Pull_Ia_Cntrct_Pckg_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Pull_Ia_Cntrct_Pckg_Rsn;
    private DbsField ads_Prtcpnt_View_Adp_Pull_Crrspndce_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Pull_Crrspndce_Prntr_Id;
    private DbsGroup ads_Prtcpnt_View_Adp_Dltn_Rsn_CdeMuGroup;
    private DbsField ads_Prtcpnt_View_Adp_Dltn_Rsn_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Dltn_Txt;
    private DbsGroup ads_Prtcpnt_View_Adp_Audit_Trail_Dta;
    private DbsField ads_Prtcpnt_View_Adp_Rqst_1st_Vrfd_Id;
    private DbsField ads_Prtcpnt_View_Adp_Rqst_1st_Vrfd_Dte;
    private DbsField ads_Prtcpnt_View_Adp_Rqst_Last_Vrfd_Id;
    private DbsField ads_Prtcpnt_View_Adp_Rqst_Last_Vrfd_Dte;
    private DbsField ads_Prtcpnt_View_Adp_Rqst_Last_Updtd_Id;
    private DbsField ads_Prtcpnt_View_Adp_Rqst_Last_Updtd_Dte;
    private DbsField ads_Prtcpnt_View_Adp_Scnd_Sight_Vrfd_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Rsttlmnt_Cnt;
    private DbsField ads_Prtcpnt_View_Adp_State_Of_Issue;
    private DbsField ads_Prtcpnt_View_Adp_Orgnl_Issue_State;
    private DbsField ads_Prtcpnt_View_Adp_Ss_Field_Chng_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Orig_Rqst_Id;
    private DbsField ads_Prtcpnt_View_Adp_Ivc_Rlvr_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Ivc_Pymnt_Rule;
    private DbsField ads_Prtcpnt_View_Adp_Hold_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Plan_Nbr;
    private DbsGroup ads_Prtcpnt_View_Adp_Frm_Irc_Dta;
    private DbsField ads_Prtcpnt_View_Adp_Frm_Irc_Cde;
    private DbsField ads_Prtcpnt_View_Adp_To_Irc_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Irc_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Ackl_Lttr_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Ntfctn_Lttr_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Pi_Task_Id;
    private DbsField ads_Prtcpnt_View_Adp_Plan_Nme;
    private DbsField ads_Prtcpnt_View_Adp_Lttr_Dlvry_Typ;
    private DbsField ads_Prtcpnt_View_Adp_Eft_Check_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Plan_Admin;
    private DbsField ads_Prtcpnt_View_Adp_Rtb_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Rtb_Alt_Addr_Ind;
    private DbsGroup ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Data;
    private DbsField ads_Prtcpnt_View_Adp_Rtb_Alt_Name;
    private DbsGroup ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_TxtMuGroup;
    private DbsField ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_Txt;
    private DbsField ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_Zip;
    private DbsField ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_Typ_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Acct_Nbr;
    private DbsField ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Acct_Typ;
    private DbsField ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Trnst_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Hold_Cde;
    private DbsField ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Rlvr_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Rlvr_Dest;
    private DbsField ads_Prtcpnt_View_Adp_Rtb_Alt_Eft_Check_Ind;
    private DbsField ads_Prtcpnt_View_Adp_T395_Ind;
    private DbsField ads_Prtcpnt_View_Adp_Roth_Plan_Type;
    private DbsField ads_Prtcpnt_View_Adp_Roth_Frst_Cntrbtn_Dte;
    private DbsField ads_Prtcpnt_View_Adp_Roth_Dsblty_Dte;
    private DbsField ads_Prtcpnt_View_Adp_Roth_Rqst_Ind;

    public DataAccessProgramView getVw_ads_Prtcpnt_View() { return vw_ads_Prtcpnt_View; }

    public DbsField getAds_Prtcpnt_View_Rqst_Id() { return ads_Prtcpnt_View_Rqst_Id; }

    public DbsGroup getAds_Prtcpnt_View_Adp_Ia_Dta() { return ads_Prtcpnt_View_Adp_Ia_Dta; }

    public DbsField getAds_Prtcpnt_View_Adp_Ia_Tiaa_Nbr() { return ads_Prtcpnt_View_Adp_Ia_Tiaa_Nbr; }

    public DbsField getAds_Prtcpnt_View_Adp_Ia_Tiaa_Rea_Nbr() { return ads_Prtcpnt_View_Adp_Ia_Tiaa_Rea_Nbr; }

    public DbsField getAds_Prtcpnt_View_Adp_Ia_Tiaa_Payee_Cd() { return ads_Prtcpnt_View_Adp_Ia_Tiaa_Payee_Cd; }

    public DbsField getAds_Prtcpnt_View_Adp_Ia_Cref_Nbr() { return ads_Prtcpnt_View_Adp_Ia_Cref_Nbr; }

    public DbsField getAds_Prtcpnt_View_Adp_Ia_Cref_Payee_Cd() { return ads_Prtcpnt_View_Adp_Ia_Cref_Payee_Cd; }

    public DbsField getAds_Prtcpnt_View_Adp_Bps_Unit() { return ads_Prtcpnt_View_Adp_Bps_Unit; }

    public DbsField getAds_Prtcpnt_View_Adp_Emplymnt_Term_Dte() { return ads_Prtcpnt_View_Adp_Emplymnt_Term_Dte; }

    public DbsField getAds_Prtcpnt_View_Adp_Cntr_Prt_Issue_Dte() { return ads_Prtcpnt_View_Adp_Cntr_Prt_Issue_Dte; }

    public DbsField getAds_Prtcpnt_View_Adp_Entry_Dte() { return ads_Prtcpnt_View_Adp_Entry_Dte; }

    public DbsField getAds_Prtcpnt_View_Adp_Lst_Actvty_Dte() { return ads_Prtcpnt_View_Adp_Lst_Actvty_Dte; }

    public DbsField getAds_Prtcpnt_View_Adp_Annty_Strt_Dte() { return ads_Prtcpnt_View_Adp_Annty_Strt_Dte; }

    public DbsField getAds_Prtcpnt_View_Adp_Annt_Typ_Cde() { return ads_Prtcpnt_View_Adp_Annt_Typ_Cde; }

    public DbsField getAds_Prtcpnt_View_Adp_Apprvl_Wo_Term_Dte() { return ads_Prtcpnt_View_Adp_Apprvl_Wo_Term_Dte; }

    public DbsGroup getAds_Prtcpnt_View_Adp_Cntrcts_In_RqstMuGroup() { return ads_Prtcpnt_View_Adp_Cntrcts_In_RqstMuGroup; }

    public DbsField getAds_Prtcpnt_View_Adp_Cntrcts_In_Rqst() { return ads_Prtcpnt_View_Adp_Cntrcts_In_Rqst; }

    public DbsField getAds_Prtcpnt_View_Adp_Unique_Id() { return ads_Prtcpnt_View_Adp_Unique_Id; }

    public DbsField getAds_Prtcpnt_View_Adp_Effctv_Dte() { return ads_Prtcpnt_View_Adp_Effctv_Dte; }

    public DbsField getAds_Prtcpnt_View_Adp_Entry_Tme() { return ads_Prtcpnt_View_Adp_Entry_Tme; }

    public DbsField getAds_Prtcpnt_View_Adp_Entry_User_Id() { return ads_Prtcpnt_View_Adp_Entry_User_Id; }

    public DbsField getAds_Prtcpnt_View_Adp_Rep_Nme() { return ads_Prtcpnt_View_Adp_Rep_Nme; }

    public DbsField getAds_Prtcpnt_View_Adp_Wpid() { return ads_Prtcpnt_View_Adp_Wpid; }

    public DbsField getAds_Prtcpnt_View_Adp_Mit_Log_Dte_Tme() { return ads_Prtcpnt_View_Adp_Mit_Log_Dte_Tme; }

    public DbsField getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind() { return ads_Prtcpnt_View_Adp_Opn_Clsd_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_In_Prgrss_Ind() { return ads_Prtcpnt_View_Adp_In_Prgrss_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_Rcprcl_Dte() { return ads_Prtcpnt_View_Adp_Rcprcl_Dte; }

    public DbsField getAds_Prtcpnt_View_Adp_Stts_Cde() { return ads_Prtcpnt_View_Adp_Stts_Cde; }

    public DbsGroup getAds_Prtcpnt_View_Adp_Crrspndce_Mlng_Dta() { return ads_Prtcpnt_View_Adp_Crrspndce_Mlng_Dta; }

    public DbsGroup getAds_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_TxtMuGroup() { return ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_TxtMuGroup; }

    public DbsField getAds_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt() { return ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt; }

    public DbsField getAds_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Zip() { return ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Zip; }

    public DbsField getAds_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Typ_Cd() { return ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Typ_Cd; }

    public DbsField getAds_Prtcpnt_View_Adp_Crrspndnce_Addr_Lst_Chg_Dte() { return ads_Prtcpnt_View_Adp_Crrspndnce_Addr_Lst_Chg_Dte; }

    public DbsField getAds_Prtcpnt_View_Adp_Forms_Cmplte_Ind() { return ads_Prtcpnt_View_Adp_Forms_Cmplte_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_Frst_Annt_Ctznshp() { return ads_Prtcpnt_View_Adp_Frst_Annt_Ctznshp; }

    public DbsField getAds_Prtcpnt_View_Adp_Frst_Annt_Rsdnc_Cde() { return ads_Prtcpnt_View_Adp_Frst_Annt_Rsdnc_Cde; }

    public DbsField getAds_Prtcpnt_View_Adp_Frst_Annt_Ssn() { return ads_Prtcpnt_View_Adp_Frst_Annt_Ssn; }

    public DbsField getAds_Prtcpnt_View_Adp_Frst_Annt_Title_Cde() { return ads_Prtcpnt_View_Adp_Frst_Annt_Title_Cde; }

    public DbsField getAds_Prtcpnt_View_Adp_Frst_Annt_Frst_Nme() { return ads_Prtcpnt_View_Adp_Frst_Annt_Frst_Nme; }

    public DbsField getAds_Prtcpnt_View_Adp_Frst_Annt_Mid_Nme() { return ads_Prtcpnt_View_Adp_Frst_Annt_Mid_Nme; }

    public DbsField getAds_Prtcpnt_View_Adp_Frst_Annt_Lst_Nme() { return ads_Prtcpnt_View_Adp_Frst_Annt_Lst_Nme; }

    public DbsField getAds_Prtcpnt_View_Adp_Frst_Annt_Sfx() { return ads_Prtcpnt_View_Adp_Frst_Annt_Sfx; }

    public DbsField getAds_Prtcpnt_View_Adp_Frst_Annt_Dte_Of_Brth() { return ads_Prtcpnt_View_Adp_Frst_Annt_Dte_Of_Brth; }

    public DbsField getAds_Prtcpnt_View_Adp_Frst_Annt_Sex_Cde() { return ads_Prtcpnt_View_Adp_Frst_Annt_Sex_Cde; }

    public DbsField getAds_Prtcpnt_View_Adp_Scnd_Annt_Frst_Nme() { return ads_Prtcpnt_View_Adp_Scnd_Annt_Frst_Nme; }

    public DbsField getAds_Prtcpnt_View_Adp_Scnd_Annt_Mid_Nme() { return ads_Prtcpnt_View_Adp_Scnd_Annt_Mid_Nme; }

    public DbsField getAds_Prtcpnt_View_Adp_Scnd_Annt_Lst_Nme() { return ads_Prtcpnt_View_Adp_Scnd_Annt_Lst_Nme; }

    public DbsField getAds_Prtcpnt_View_Adp_Scnd_Annt_Rltnshp() { return ads_Prtcpnt_View_Adp_Scnd_Annt_Rltnshp; }

    public DbsField getAds_Prtcpnt_View_Adp_Scnd_Annt_Dte_Of_Brth() { return ads_Prtcpnt_View_Adp_Scnd_Annt_Dte_Of_Brth; }

    public DbsField getAds_Prtcpnt_View_Adp_Scnd_Annt_Ssn() { return ads_Prtcpnt_View_Adp_Scnd_Annt_Ssn; }

    public DbsField getAds_Prtcpnt_View_Adp_Scnd_Annt_Sex_Cde() { return ads_Prtcpnt_View_Adp_Scnd_Annt_Sex_Cde; }

    public DbsField getAds_Prtcpnt_View_Adp_Annty_Optn() { return ads_Prtcpnt_View_Adp_Annty_Optn; }

    public DbsField getAds_Prtcpnt_View_Adp_Grntee_Period() { return ads_Prtcpnt_View_Adp_Grntee_Period; }

    public DbsField getAds_Prtcpnt_View_Adp_Grntee_Period_Mnths() { return ads_Prtcpnt_View_Adp_Grntee_Period_Mnths; }

    public DbsField getAds_Prtcpnt_View_Adp_Pymnt_Mode() { return ads_Prtcpnt_View_Adp_Pymnt_Mode; }

    public DbsField getAds_Prtcpnt_View_Adp_Settl_All_Tiaa_Grd_Pct() { return ads_Prtcpnt_View_Adp_Settl_All_Tiaa_Grd_Pct; }

    public DbsField getAds_Prtcpnt_View_Adp_Settl_All_Cref_Mon_Pct() { return ads_Prtcpnt_View_Adp_Settl_All_Cref_Mon_Pct; }

    public DbsField getAds_Prtcpnt_View_Adp_Alt_Dest_Addr_Ind() { return ads_Prtcpnt_View_Adp_Alt_Dest_Addr_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_Spcfc_Brkdwn_Ind() { return ads_Prtcpnt_View_Adp_Spcfc_Brkdwn_Ind; }

    public DbsGroup getAds_Prtcpnt_View_Adp_Alt_Dest_Data() { return ads_Prtcpnt_View_Adp_Alt_Dest_Data; }

    public DbsField getAds_Prtcpnt_View_Adp_Alt_Payee_Ind() { return ads_Prtcpnt_View_Adp_Alt_Payee_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_Alt_Carrier_Cde() { return ads_Prtcpnt_View_Adp_Alt_Carrier_Cde; }

    public DbsField getAds_Prtcpnt_View_Adp_Alt_Dest_Nme() { return ads_Prtcpnt_View_Adp_Alt_Dest_Nme; }

    public DbsGroup getAds_Prtcpnt_View_Adp_Alt_Dest_Addr_TxtMuGroup() { return ads_Prtcpnt_View_Adp_Alt_Dest_Addr_TxtMuGroup; }

    public DbsField getAds_Prtcpnt_View_Adp_Alt_Dest_Addr_Txt() { return ads_Prtcpnt_View_Adp_Alt_Dest_Addr_Txt; }

    public DbsField getAds_Prtcpnt_View_Adp_Alt_Dest_Addr_Zip() { return ads_Prtcpnt_View_Adp_Alt_Dest_Addr_Zip; }

    public DbsField getAds_Prtcpnt_View_Adp_Alt_Dest_Addr_Typ_Cde() { return ads_Prtcpnt_View_Adp_Alt_Dest_Addr_Typ_Cde; }

    public DbsField getAds_Prtcpnt_View_Adp_Alt_Dest_Acct_Nbr() { return ads_Prtcpnt_View_Adp_Alt_Dest_Acct_Nbr; }

    public DbsField getAds_Prtcpnt_View_Adp_Alt_Dest_Acct_Typ() { return ads_Prtcpnt_View_Adp_Alt_Dest_Acct_Typ; }

    public DbsField getAds_Prtcpnt_View_Adp_Alt_Dest_Trnst_Cde() { return ads_Prtcpnt_View_Adp_Alt_Dest_Trnst_Cde; }

    public DbsField getAds_Prtcpnt_View_Adp_Alt_Dest_Hold_Cde() { return ads_Prtcpnt_View_Adp_Alt_Dest_Hold_Cde; }

    public DbsField getAds_Prtcpnt_View_Adp_Alt_Dest_Rlvr_Ind() { return ads_Prtcpnt_View_Adp_Alt_Dest_Rlvr_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_Alt_Dest_Rlvr_Dest() { return ads_Prtcpnt_View_Adp_Alt_Dest_Rlvr_Dest; }

    public DbsField getAds_Prtcpnt_View_Adp_Sbjct_To_Wvr_Ind() { return ads_Prtcpnt_View_Adp_Sbjct_To_Wvr_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_Sbjct_To_Spsl_Cnsnt() { return ads_Prtcpnt_View_Adp_Sbjct_To_Spsl_Cnsnt; }

    public DbsGroup getAds_Prtcpnt_View_Adp_Forms_Data() { return ads_Prtcpnt_View_Adp_Forms_Data; }

    public DbsField getAds_Prtcpnt_View_Adp_Forms_Rcvd_Dte() { return ads_Prtcpnt_View_Adp_Forms_Rcvd_Dte; }

    public DbsField getAds_Prtcpnt_View_Adp_Frst_Annt_Prf_Of_Age_Ind() { return ads_Prtcpnt_View_Adp_Frst_Annt_Prf_Of_Age_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_Scnd_Annt_Prf_Of_Age_Ind() { return ads_Prtcpnt_View_Adp_Scnd_Annt_Prf_Of_Age_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_Glbl_Pay_Ind() { return ads_Prtcpnt_View_Adp_Glbl_Pay_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_Pull_Ia_Cntrct_Pckg_Ind() { return ads_Prtcpnt_View_Adp_Pull_Ia_Cntrct_Pckg_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_Pull_Ia_Cntrct_Pckg_Rsn() { return ads_Prtcpnt_View_Adp_Pull_Ia_Cntrct_Pckg_Rsn; }

    public DbsField getAds_Prtcpnt_View_Adp_Pull_Crrspndce_Ind() { return ads_Prtcpnt_View_Adp_Pull_Crrspndce_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_Pull_Crrspndce_Prntr_Id() { return ads_Prtcpnt_View_Adp_Pull_Crrspndce_Prntr_Id; }

    public DbsGroup getAds_Prtcpnt_View_Adp_Dltn_Rsn_CdeMuGroup() { return ads_Prtcpnt_View_Adp_Dltn_Rsn_CdeMuGroup; }

    public DbsField getAds_Prtcpnt_View_Adp_Dltn_Rsn_Cde() { return ads_Prtcpnt_View_Adp_Dltn_Rsn_Cde; }

    public DbsField getAds_Prtcpnt_View_Adp_Dltn_Txt() { return ads_Prtcpnt_View_Adp_Dltn_Txt; }

    public DbsGroup getAds_Prtcpnt_View_Adp_Audit_Trail_Dta() { return ads_Prtcpnt_View_Adp_Audit_Trail_Dta; }

    public DbsField getAds_Prtcpnt_View_Adp_Rqst_1st_Vrfd_Id() { return ads_Prtcpnt_View_Adp_Rqst_1st_Vrfd_Id; }

    public DbsField getAds_Prtcpnt_View_Adp_Rqst_1st_Vrfd_Dte() { return ads_Prtcpnt_View_Adp_Rqst_1st_Vrfd_Dte; }

    public DbsField getAds_Prtcpnt_View_Adp_Rqst_Last_Vrfd_Id() { return ads_Prtcpnt_View_Adp_Rqst_Last_Vrfd_Id; }

    public DbsField getAds_Prtcpnt_View_Adp_Rqst_Last_Vrfd_Dte() { return ads_Prtcpnt_View_Adp_Rqst_Last_Vrfd_Dte; }

    public DbsField getAds_Prtcpnt_View_Adp_Rqst_Last_Updtd_Id() { return ads_Prtcpnt_View_Adp_Rqst_Last_Updtd_Id; }

    public DbsField getAds_Prtcpnt_View_Adp_Rqst_Last_Updtd_Dte() { return ads_Prtcpnt_View_Adp_Rqst_Last_Updtd_Dte; }

    public DbsField getAds_Prtcpnt_View_Adp_Scnd_Sight_Vrfd_Ind() { return ads_Prtcpnt_View_Adp_Scnd_Sight_Vrfd_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_Rsttlmnt_Cnt() { return ads_Prtcpnt_View_Adp_Rsttlmnt_Cnt; }

    public DbsField getAds_Prtcpnt_View_Adp_State_Of_Issue() { return ads_Prtcpnt_View_Adp_State_Of_Issue; }

    public DbsField getAds_Prtcpnt_View_Adp_Orgnl_Issue_State() { return ads_Prtcpnt_View_Adp_Orgnl_Issue_State; }

    public DbsField getAds_Prtcpnt_View_Adp_Ss_Field_Chng_Ind() { return ads_Prtcpnt_View_Adp_Ss_Field_Chng_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_Orig_Rqst_Id() { return ads_Prtcpnt_View_Adp_Orig_Rqst_Id; }

    public DbsField getAds_Prtcpnt_View_Adp_Ivc_Rlvr_Cde() { return ads_Prtcpnt_View_Adp_Ivc_Rlvr_Cde; }

    public DbsField getAds_Prtcpnt_View_Adp_Ivc_Pymnt_Rule() { return ads_Prtcpnt_View_Adp_Ivc_Pymnt_Rule; }

    public DbsField getAds_Prtcpnt_View_Adp_Hold_Cde() { return ads_Prtcpnt_View_Adp_Hold_Cde; }

    public DbsField getAds_Prtcpnt_View_Adp_Plan_Nbr() { return ads_Prtcpnt_View_Adp_Plan_Nbr; }

    public DbsGroup getAds_Prtcpnt_View_Adp_Frm_Irc_Dta() { return ads_Prtcpnt_View_Adp_Frm_Irc_Dta; }

    public DbsField getAds_Prtcpnt_View_Adp_Frm_Irc_Cde() { return ads_Prtcpnt_View_Adp_Frm_Irc_Cde; }

    public DbsField getAds_Prtcpnt_View_Adp_To_Irc_Cde() { return ads_Prtcpnt_View_Adp_To_Irc_Cde; }

    public DbsField getAds_Prtcpnt_View_Adp_Irc_Cde() { return ads_Prtcpnt_View_Adp_Irc_Cde; }

    public DbsField getAds_Prtcpnt_View_Adp_Ackl_Lttr_Ind() { return ads_Prtcpnt_View_Adp_Ackl_Lttr_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_Ntfctn_Lttr_Ind() { return ads_Prtcpnt_View_Adp_Ntfctn_Lttr_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_Pi_Task_Id() { return ads_Prtcpnt_View_Adp_Pi_Task_Id; }

    public DbsField getAds_Prtcpnt_View_Adp_Plan_Nme() { return ads_Prtcpnt_View_Adp_Plan_Nme; }

    public DbsField getAds_Prtcpnt_View_Adp_Lttr_Dlvry_Typ() { return ads_Prtcpnt_View_Adp_Lttr_Dlvry_Typ; }

    public DbsField getAds_Prtcpnt_View_Adp_Eft_Check_Ind() { return ads_Prtcpnt_View_Adp_Eft_Check_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_Plan_Admin() { return ads_Prtcpnt_View_Adp_Plan_Admin; }

    public DbsField getAds_Prtcpnt_View_Adp_Rtb_Ind() { return ads_Prtcpnt_View_Adp_Rtb_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_Rtb_Alt_Addr_Ind() { return ads_Prtcpnt_View_Adp_Rtb_Alt_Addr_Ind; }

    public DbsGroup getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Data() { return ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Data; }

    public DbsField getAds_Prtcpnt_View_Adp_Rtb_Alt_Name() { return ads_Prtcpnt_View_Adp_Rtb_Alt_Name; }

    public DbsGroup getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_TxtMuGroup() { return ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_TxtMuGroup; }

    public DbsField getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_Txt() { return ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_Txt; }

    public DbsField getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_Zip() { return ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_Zip; }

    public DbsField getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_Typ_Cde() { return ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_Typ_Cde; }

    public DbsField getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Acct_Nbr() { return ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Acct_Nbr; }

    public DbsField getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Acct_Typ() { return ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Acct_Typ; }

    public DbsField getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Trnst_Cde() { return ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Trnst_Cde; }

    public DbsField getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Hold_Cde() { return ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Hold_Cde; }

    public DbsField getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Rlvr_Ind() { return ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Rlvr_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_Rtb_Alt_Dest_Rlvr_Dest() { return ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Rlvr_Dest; }

    public DbsField getAds_Prtcpnt_View_Adp_Rtb_Alt_Eft_Check_Ind() { return ads_Prtcpnt_View_Adp_Rtb_Alt_Eft_Check_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_T395_Ind() { return ads_Prtcpnt_View_Adp_T395_Ind; }

    public DbsField getAds_Prtcpnt_View_Adp_Roth_Plan_Type() { return ads_Prtcpnt_View_Adp_Roth_Plan_Type; }

    public DbsField getAds_Prtcpnt_View_Adp_Roth_Frst_Cntrbtn_Dte() { return ads_Prtcpnt_View_Adp_Roth_Frst_Cntrbtn_Dte; }

    public DbsField getAds_Prtcpnt_View_Adp_Roth_Dsblty_Dte() { return ads_Prtcpnt_View_Adp_Roth_Dsblty_Dte; }

    public DbsField getAds_Prtcpnt_View_Adp_Roth_Rqst_Ind() { return ads_Prtcpnt_View_Adp_Roth_Rqst_Ind; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        vw_ads_Prtcpnt_View = new DataAccessProgramView(new NameInfo("vw_ads_Prtcpnt_View", "ADS-PRTCPNT-VIEW"), "ADS_PRTCPNT", "ADS_PRTCPNT", DdmPeriodicGroups.getInstance().getGroups("ADS_PRTCPNT"));
        ads_Prtcpnt_View_Rqst_Id = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "RQST_ID");
        ads_Prtcpnt_View_Adp_Ia_Dta = vw_ads_Prtcpnt_View.getRecord().newGroupInGroup("ads_Prtcpnt_View_Adp_Ia_Dta", "ADP-IA-DTA");
        ads_Prtcpnt_View_Adp_Ia_Tiaa_Nbr = ads_Prtcpnt_View_Adp_Ia_Dta.newFieldInGroup("ads_Prtcpnt_View_Adp_Ia_Tiaa_Nbr", "ADP-IA-TIAA-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "ADP_IA_TIAA_NBR");
        ads_Prtcpnt_View_Adp_Ia_Tiaa_Rea_Nbr = ads_Prtcpnt_View_Adp_Ia_Dta.newFieldInGroup("ads_Prtcpnt_View_Adp_Ia_Tiaa_Rea_Nbr", "ADP-IA-TIAA-REA-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "ADP_IA_TIAA_REA_NBR");
        ads_Prtcpnt_View_Adp_Ia_Tiaa_Payee_Cd = ads_Prtcpnt_View_Adp_Ia_Dta.newFieldInGroup("ads_Prtcpnt_View_Adp_Ia_Tiaa_Payee_Cd", "ADP-IA-TIAA-PAYEE-CD", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ADP_IA_TIAA_PAYEE_CD");
        ads_Prtcpnt_View_Adp_Ia_Cref_Nbr = ads_Prtcpnt_View_Adp_Ia_Dta.newFieldInGroup("ads_Prtcpnt_View_Adp_Ia_Cref_Nbr", "ADP-IA-CREF-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "ADP_IA_CREF_NBR");
        ads_Prtcpnt_View_Adp_Ia_Cref_Payee_Cd = ads_Prtcpnt_View_Adp_Ia_Dta.newFieldInGroup("ads_Prtcpnt_View_Adp_Ia_Cref_Payee_Cd", "ADP-IA-CREF-PAYEE-CD", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ADP_IA_CREF_PAYEE_CD");
        ads_Prtcpnt_View_Adp_Bps_Unit = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Bps_Unit", "ADP-BPS-UNIT", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADP_BPS_UNIT");
        ads_Prtcpnt_View_Adp_Emplymnt_Term_Dte = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Emplymnt_Term_Dte", "ADP-EMPLYMNT-TERM-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_EMPLYMNT_TERM_DTE");
        ads_Prtcpnt_View_Adp_Cntr_Prt_Issue_Dte = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Cntr_Prt_Issue_Dte", "ADP-CNTR-PRT-ISSUE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_CNTR_PRT_ISSUE_DTE");
        ads_Prtcpnt_View_Adp_Entry_Dte = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Entry_Dte", "ADP-ENTRY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADP_ENTRY_DTE");
        ads_Prtcpnt_View_Adp_Lst_Actvty_Dte = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Lst_Actvty_Dte", "ADP-LST-ACTVTY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_LST_ACTVTY_DTE");
        ads_Prtcpnt_View_Adp_Annty_Strt_Dte = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Annty_Strt_Dte", "ADP-ANNTY-STRT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_ANNTY_STRT_DTE");
        ads_Prtcpnt_View_Adp_Annt_Typ_Cde = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Annt_Typ_Cde", "ADP-ANNT-TYP-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_ANNT_TYP_CDE");
        ads_Prtcpnt_View_Adp_Apprvl_Wo_Term_Dte = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Apprvl_Wo_Term_Dte", "ADP-APPRVL-WO-TERM-DTE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_APPRVL_WO_TERM_DTE");
        ads_Prtcpnt_View_Adp_Cntrcts_In_RqstMuGroup = vw_ads_Prtcpnt_View.getRecord().newGroupInGroup("ads_Prtcpnt_View_Adp_Cntrcts_In_RqstMuGroup", "ADP_CNTRCTS_IN_RQSTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_PRTCPNT_ADP_CNTRCTS_IN_RQST");
        ads_Prtcpnt_View_Adp_Cntrcts_In_Rqst = ads_Prtcpnt_View_Adp_Cntrcts_In_RqstMuGroup.newFieldArrayInGroup("ads_Prtcpnt_View_Adp_Cntrcts_In_Rqst", 
            "ADP-CNTRCTS-IN-RQST", FieldType.STRING, 10, new DbsArrayController(1,12), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADP_CNTRCTS_IN_RQST");
        ads_Prtcpnt_View_Adp_Unique_Id = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Unique_Id", "ADP-UNIQUE-ID", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "ADP_UNIQUE_ID");
        ads_Prtcpnt_View_Adp_Effctv_Dte = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Effctv_Dte", "ADP-EFFCTV-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADP_EFFCTV_DTE");
        ads_Prtcpnt_View_Adp_Entry_Tme = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Entry_Tme", "ADP-ENTRY-TME", FieldType.TIME, 
            RepeatingFieldStrategy.None, "ADP_ENTRY_TME");
        ads_Prtcpnt_View_Adp_Entry_User_Id = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Entry_User_Id", "ADP-ENTRY-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADP_ENTRY_USER_ID");
        ads_Prtcpnt_View_Adp_Rep_Nme = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Rep_Nme", "ADP-REP-NME", FieldType.STRING, 
            40, RepeatingFieldStrategy.None, "ADP_REP_NME");
        ads_Prtcpnt_View_Adp_Wpid = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Wpid", "ADP-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "ADP_WPID");
        ads_Prtcpnt_View_Adp_Mit_Log_Dte_Tme = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Mit_Log_Dte_Tme", "ADP-MIT-LOG-DTE-TME", 
            FieldType.STRING, 15, RepeatingFieldStrategy.None, "ADP_MIT_LOG_DTE_TME");
        ads_Prtcpnt_View_Adp_Opn_Clsd_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Opn_Clsd_Ind", "ADP-OPN-CLSD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_OPN_CLSD_IND");
        ads_Prtcpnt_View_Adp_In_Prgrss_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_In_Prgrss_Ind", "ADP-IN-PRGRSS-IND", 
            FieldType.BOOLEAN, 1, RepeatingFieldStrategy.None, "ADP_IN_PRGRSS_IND");
        ads_Prtcpnt_View_Adp_Rcprcl_Dte = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Rcprcl_Dte", "ADP-RCPRCL-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADP_RCPRCL_DTE");
        ads_Prtcpnt_View_Adp_Stts_Cde = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Stts_Cde", "ADP-STTS-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "ADP_STTS_CDE");
        ads_Prtcpnt_View_Adp_Crrspndce_Mlng_Dta = vw_ads_Prtcpnt_View.getRecord().newGroupInGroup("ads_Prtcpnt_View_Adp_Crrspndce_Mlng_Dta", "ADP-CRRSPNDCE-MLNG-DTA");
        ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_TxtMuGroup = ads_Prtcpnt_View_Adp_Crrspndce_Mlng_Dta.newGroupInGroup("ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_TxtMuGroup", 
            "ADP_CRRSPNDNCE_PERM_ADDR_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "ADS_PRTCPNT_ADP_CRRSPNDNCE_PERM_ADDR_TXT");
        ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt = ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_TxtMuGroup.newFieldArrayInGroup("ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Txt", 
            "ADP-CRRSPNDNCE-PERM-ADDR-TXT", FieldType.STRING, 35, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArray, "ADP_CRRSPNDNCE_PERM_ADDR_TXT");
        ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Zip = ads_Prtcpnt_View_Adp_Crrspndce_Mlng_Dta.newFieldInGroup("ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Zip", 
            "ADP-CRRSPNDNCE-PERM-ADDR-ZIP", FieldType.STRING, 9, RepeatingFieldStrategy.None, "ADP_CRRSPNDNCE_PERM_ADDR_ZIP");
        ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Typ_Cd = ads_Prtcpnt_View_Adp_Crrspndce_Mlng_Dta.newFieldInGroup("ads_Prtcpnt_View_Adp_Crrspndnce_Perm_Addr_Typ_Cd", 
            "ADP-CRRSPNDNCE-PERM-ADDR-TYP-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_CRRSPNDNCE_PERM_ADDR_TYP_CD");
        ads_Prtcpnt_View_Adp_Crrspndnce_Addr_Lst_Chg_Dte = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Crrspndnce_Addr_Lst_Chg_Dte", 
            "ADP-CRRSPNDNCE-ADDR-LST-CHG-DTE", FieldType.DATE, RepeatingFieldStrategy.None, "ADP_CRRSPNDNCE_ADDR_LST_CHG_DTE");
        ads_Prtcpnt_View_Adp_Forms_Cmplte_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Forms_Cmplte_Ind", "ADP-FORMS-CMPLTE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_FORMS_CMPLTE_IND");
        ads_Prtcpnt_View_Adp_Frst_Annt_Ctznshp = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Frst_Annt_Ctznshp", "ADP-FRST-ANNT-CTZNSHP", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_CTZNSHP");
        ads_Prtcpnt_View_Adp_Frst_Annt_Rsdnc_Cde = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Frst_Annt_Rsdnc_Cde", "ADP-FRST-ANNT-RSDNC-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_RSDNC_CDE");
        ads_Prtcpnt_View_Adp_Frst_Annt_Ssn = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Frst_Annt_Ssn", "ADP-FRST-ANNT-SSN", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_SSN");
        ads_Prtcpnt_View_Adp_Frst_Annt_Title_Cde = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Frst_Annt_Title_Cde", "ADP-FRST-ANNT-TITLE-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_TITLE_CDE");
        ads_Prtcpnt_View_Adp_Frst_Annt_Frst_Nme = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Frst_Annt_Frst_Nme", "ADP-FRST-ANNT-FRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_FRST_NME");
        ads_Prtcpnt_View_Adp_Frst_Annt_Mid_Nme = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Frst_Annt_Mid_Nme", "ADP-FRST-ANNT-MID-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_MID_NME");
        ads_Prtcpnt_View_Adp_Frst_Annt_Lst_Nme = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Frst_Annt_Lst_Nme", "ADP-FRST-ANNT-LST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_LST_NME");
        ads_Prtcpnt_View_Adp_Frst_Annt_Sfx = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Frst_Annt_Sfx", "ADP-FRST-ANNT-SFX", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_SFX");
        ads_Prtcpnt_View_Adp_Frst_Annt_Dte_Of_Brth = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Frst_Annt_Dte_Of_Brth", "ADP-FRST-ANNT-DTE-OF-BRTH", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_DTE_OF_BRTH");
        ads_Prtcpnt_View_Adp_Frst_Annt_Sex_Cde = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Frst_Annt_Sex_Cde", "ADP-FRST-ANNT-SEX-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_SEX_CDE");
        ads_Prtcpnt_View_Adp_Scnd_Annt_Frst_Nme = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Scnd_Annt_Frst_Nme", "ADP-SCND-ANNT-FRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_FRST_NME");
        ads_Prtcpnt_View_Adp_Scnd_Annt_Mid_Nme = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Scnd_Annt_Mid_Nme", "ADP-SCND-ANNT-MID-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_MID_NME");
        ads_Prtcpnt_View_Adp_Scnd_Annt_Lst_Nme = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Scnd_Annt_Lst_Nme", "ADP-SCND-ANNT-LST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_LST_NME");
        ads_Prtcpnt_View_Adp_Scnd_Annt_Rltnshp = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Scnd_Annt_Rltnshp", "ADP-SCND-ANNT-RLTNSHP", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_RLTNSHP");
        ads_Prtcpnt_View_Adp_Scnd_Annt_Dte_Of_Brth = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Scnd_Annt_Dte_Of_Brth", "ADP-SCND-ANNT-DTE-OF-BRTH", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_DTE_OF_BRTH");
        ads_Prtcpnt_View_Adp_Scnd_Annt_Ssn = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Scnd_Annt_Ssn", "ADP-SCND-ANNT-SSN", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_SSN");
        ads_Prtcpnt_View_Adp_Scnd_Annt_Sex_Cde = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Scnd_Annt_Sex_Cde", "ADP-SCND-ANNT-SEX-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_SEX_CDE");
        ads_Prtcpnt_View_Adp_Annty_Optn = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Annty_Optn", "ADP-ANNTY-OPTN", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADP_ANNTY_OPTN");
        ads_Prtcpnt_View_Adp_Grntee_Period = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Grntee_Period", "ADP-GRNTEE-PERIOD", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "ADP_GRNTEE_PERIOD");
        ads_Prtcpnt_View_Adp_Grntee_Period_Mnths = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Grntee_Period_Mnths", "ADP-GRNTEE-PERIOD-MNTHS", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "ADP_GRNTEE_PERIOD_MNTHS");
        ads_Prtcpnt_View_Adp_Pymnt_Mode = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Pymnt_Mode", "ADP-PYMNT-MODE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "ADP_PYMNT_MODE");
        ads_Prtcpnt_View_Adp_Settl_All_Tiaa_Grd_Pct = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Settl_All_Tiaa_Grd_Pct", "ADP-SETTL-ALL-TIAA-GRD-PCT", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "ADP_SETTL_ALL_TIAA_GRD_PCT");
        ads_Prtcpnt_View_Adp_Settl_All_Cref_Mon_Pct = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Settl_All_Cref_Mon_Pct", "ADP-SETTL-ALL-CREF-MON-PCT", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "ADP_SETTL_ALL_CREF_MON_PCT");
        ads_Prtcpnt_View_Adp_Alt_Dest_Addr_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Alt_Dest_Addr_Ind", "ADP-ALT-DEST-ADDR-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_ALT_DEST_ADDR_IND");
        ads_Prtcpnt_View_Adp_Spcfc_Brkdwn_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Spcfc_Brkdwn_Ind", "ADP-SPCFC-BRKDWN-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_SPCFC_BRKDWN_IND");
        ads_Prtcpnt_View_Adp_Alt_Dest_Data = vw_ads_Prtcpnt_View.getRecord().newGroupInGroup("ads_Prtcpnt_View_Adp_Alt_Dest_Data", "ADP-ALT-DEST-DATA");
        ads_Prtcpnt_View_Adp_Alt_Payee_Ind = ads_Prtcpnt_View_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Alt_Payee_Ind", "ADP-ALT-PAYEE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_ALT_PAYEE_IND");
        ads_Prtcpnt_View_Adp_Alt_Carrier_Cde = ads_Prtcpnt_View_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Alt_Carrier_Cde", "ADP-ALT-CARRIER-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADP_ALT_CARRIER_CDE");
        ads_Prtcpnt_View_Adp_Alt_Dest_Nme = ads_Prtcpnt_View_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Alt_Dest_Nme", "ADP-ALT-DEST-NME", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "ADP_ALT_DEST_NME");
        ads_Prtcpnt_View_Adp_Alt_Dest_Addr_TxtMuGroup = ads_Prtcpnt_View_Adp_Alt_Dest_Data.newGroupInGroup("ads_Prtcpnt_View_Adp_Alt_Dest_Addr_TxtMuGroup", 
            "ADP_ALT_DEST_ADDR_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "ADS_PRTCPNT_ADP_ALT_DEST_ADDR_TXT");
        ads_Prtcpnt_View_Adp_Alt_Dest_Addr_Txt = ads_Prtcpnt_View_Adp_Alt_Dest_Addr_TxtMuGroup.newFieldArrayInGroup("ads_Prtcpnt_View_Adp_Alt_Dest_Addr_Txt", 
            "ADP-ALT-DEST-ADDR-TXT", FieldType.STRING, 35, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADP_ALT_DEST_ADDR_TXT");
        ads_Prtcpnt_View_Adp_Alt_Dest_Addr_Zip = ads_Prtcpnt_View_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Alt_Dest_Addr_Zip", "ADP-ALT-DEST-ADDR-ZIP", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "ADP_ALT_DEST_ADDR_ZIP");
        ads_Prtcpnt_View_Adp_Alt_Dest_Addr_Typ_Cde = ads_Prtcpnt_View_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Alt_Dest_Addr_Typ_Cde", 
            "ADP-ALT-DEST-ADDR-TYP-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_ALT_DEST_ADDR_TYP_CDE");
        ads_Prtcpnt_View_Adp_Alt_Dest_Acct_Nbr = ads_Prtcpnt_View_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Alt_Dest_Acct_Nbr", "ADP-ALT-DEST-ACCT-NBR", 
            FieldType.STRING, 21, RepeatingFieldStrategy.None, "ADP_ALT_DEST_ACCT_NBR");
        ads_Prtcpnt_View_Adp_Alt_Dest_Acct_Typ = ads_Prtcpnt_View_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Alt_Dest_Acct_Typ", "ADP-ALT-DEST-ACCT-TYP", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_ALT_DEST_ACCT_TYP");
        ads_Prtcpnt_View_Adp_Alt_Dest_Trnst_Cde = ads_Prtcpnt_View_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Alt_Dest_Trnst_Cde", "ADP-ALT-DEST-TRNST-CDE", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "ADP_ALT_DEST_TRNST_CDE");
        ads_Prtcpnt_View_Adp_Alt_Dest_Hold_Cde = ads_Prtcpnt_View_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Alt_Dest_Hold_Cde", "ADP-ALT-DEST-HOLD-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADP_ALT_DEST_HOLD_CDE");
        ads_Prtcpnt_View_Adp_Alt_Dest_Rlvr_Ind = ads_Prtcpnt_View_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Alt_Dest_Rlvr_Ind", "ADP-ALT-DEST-RLVR-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_ALT_DEST_RLVR_IND");
        ads_Prtcpnt_View_Adp_Alt_Dest_Rlvr_Dest = ads_Prtcpnt_View_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Alt_Dest_Rlvr_Dest", "ADP-ALT-DEST-RLVR-DEST", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADP_ALT_DEST_RLVR_DEST");
        ads_Prtcpnt_View_Adp_Sbjct_To_Wvr_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Sbjct_To_Wvr_Ind", "ADP-SBJCT-TO-WVR-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_SBJCT_TO_WVR_IND");
        ads_Prtcpnt_View_Adp_Sbjct_To_Spsl_Cnsnt = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Sbjct_To_Spsl_Cnsnt", "ADP-SBJCT-TO-SPSL-CNSNT", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_SBJCT_TO_SPSL_CNSNT");
        ads_Prtcpnt_View_Adp_Forms_Data = vw_ads_Prtcpnt_View.getRecord().newGroupInGroup("ads_Prtcpnt_View_Adp_Forms_Data", "ADP-FORMS-DATA");
        ads_Prtcpnt_View_Adp_Forms_Rcvd_Dte = ads_Prtcpnt_View_Adp_Forms_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Forms_Rcvd_Dte", "ADP-FORMS-RCVD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_FORMS_RCVD_DTE");
        ads_Prtcpnt_View_Adp_Frst_Annt_Prf_Of_Age_Ind = ads_Prtcpnt_View_Adp_Forms_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Frst_Annt_Prf_Of_Age_Ind", 
            "ADP-FRST-ANNT-PRF-OF-AGE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_PRF_OF_AGE_IND");
        ads_Prtcpnt_View_Adp_Scnd_Annt_Prf_Of_Age_Ind = ads_Prtcpnt_View_Adp_Forms_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Scnd_Annt_Prf_Of_Age_Ind", 
            "ADP-SCND-ANNT-PRF-OF-AGE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_PRF_OF_AGE_IND");
        ads_Prtcpnt_View_Adp_Glbl_Pay_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Glbl_Pay_Ind", "ADP-GLBL-PAY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_GLBL_PAY_IND");
        ads_Prtcpnt_View_Adp_Pull_Ia_Cntrct_Pckg_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Pull_Ia_Cntrct_Pckg_Ind", 
            "ADP-PULL-IA-CNTRCT-PCKG-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_PULL_IA_CNTRCT_PCKG_IND");
        ads_Prtcpnt_View_Adp_Pull_Ia_Cntrct_Pckg_Rsn = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Pull_Ia_Cntrct_Pckg_Rsn", 
            "ADP-PULL-IA-CNTRCT-PCKG-RSN", FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADP_PULL_IA_CNTRCT_PCKG_RSN");
        ads_Prtcpnt_View_Adp_Pull_Crrspndce_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Pull_Crrspndce_Ind", "ADP-PULL-CRRSPNDCE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_PULL_CRRSPNDCE_IND");
        ads_Prtcpnt_View_Adp_Pull_Crrspndce_Prntr_Id = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Pull_Crrspndce_Prntr_Id", 
            "ADP-PULL-CRRSPNDCE-PRNTR-ID", FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADP_PULL_CRRSPNDCE_PRNTR_ID");
        ads_Prtcpnt_View_Adp_Dltn_Rsn_CdeMuGroup = vw_ads_Prtcpnt_View.getRecord().newGroupInGroup("ads_Prtcpnt_View_Adp_Dltn_Rsn_CdeMuGroup", "ADP_DLTN_RSN_CDEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_PRTCPNT_ADP_DLTN_RSN_CDE");
        ads_Prtcpnt_View_Adp_Dltn_Rsn_Cde = ads_Prtcpnt_View_Adp_Dltn_Rsn_CdeMuGroup.newFieldArrayInGroup("ads_Prtcpnt_View_Adp_Dltn_Rsn_Cde", "ADP-DLTN-RSN-CDE", 
            FieldType.STRING, 4, new DbsArrayController(1,1), RepeatingFieldStrategy.SubTableFieldArray, "ADP_DLTN_RSN_CDE");
        ads_Prtcpnt_View_Adp_Dltn_Txt = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Dltn_Txt", "ADP-DLTN-TXT", FieldType.STRING, 
            72, RepeatingFieldStrategy.None, "ADP_DLTN_TXT");
        ads_Prtcpnt_View_Adp_Audit_Trail_Dta = vw_ads_Prtcpnt_View.getRecord().newGroupInGroup("ads_Prtcpnt_View_Adp_Audit_Trail_Dta", "ADP-AUDIT-TRAIL-DTA");
        ads_Prtcpnt_View_Adp_Rqst_1st_Vrfd_Id = ads_Prtcpnt_View_Adp_Audit_Trail_Dta.newFieldInGroup("ads_Prtcpnt_View_Adp_Rqst_1st_Vrfd_Id", "ADP-RQST-1ST-VRFD-ID", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "ADP_RQST_1ST_VRFD_ID");
        ads_Prtcpnt_View_Adp_Rqst_1st_Vrfd_Dte = ads_Prtcpnt_View_Adp_Audit_Trail_Dta.newFieldInGroup("ads_Prtcpnt_View_Adp_Rqst_1st_Vrfd_Dte", "ADP-RQST-1ST-VRFD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_RQST_1ST_VRFD_DTE");
        ads_Prtcpnt_View_Adp_Rqst_Last_Vrfd_Id = ads_Prtcpnt_View_Adp_Audit_Trail_Dta.newFieldInGroup("ads_Prtcpnt_View_Adp_Rqst_Last_Vrfd_Id", "ADP-RQST-LAST-VRFD-ID", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "ADP_RQST_LAST_VRFD_ID");
        ads_Prtcpnt_View_Adp_Rqst_Last_Vrfd_Dte = ads_Prtcpnt_View_Adp_Audit_Trail_Dta.newFieldInGroup("ads_Prtcpnt_View_Adp_Rqst_Last_Vrfd_Dte", "ADP-RQST-LAST-VRFD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_RQST_LAST_VRFD_DTE");
        ads_Prtcpnt_View_Adp_Rqst_Last_Updtd_Id = ads_Prtcpnt_View_Adp_Audit_Trail_Dta.newFieldInGroup("ads_Prtcpnt_View_Adp_Rqst_Last_Updtd_Id", "ADP-RQST-LAST-UPDTD-ID", 
            FieldType.STRING, 7, RepeatingFieldStrategy.None, "ADP_RQST_LAST_UPDTD_ID");
        ads_Prtcpnt_View_Adp_Rqst_Last_Updtd_Dte = ads_Prtcpnt_View_Adp_Audit_Trail_Dta.newFieldInGroup("ads_Prtcpnt_View_Adp_Rqst_Last_Updtd_Dte", "ADP-RQST-LAST-UPDTD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_RQST_LAST_UPDTD_DTE");
        ads_Prtcpnt_View_Adp_Scnd_Sight_Vrfd_Ind = ads_Prtcpnt_View_Adp_Audit_Trail_Dta.newFieldInGroup("ads_Prtcpnt_View_Adp_Scnd_Sight_Vrfd_Ind", "ADP-SCND-SIGHT-VRFD-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_SCND_SIGHT_VRFD_IND");
        ads_Prtcpnt_View_Adp_Rsttlmnt_Cnt = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Rsttlmnt_Cnt", "ADP-RSTTLMNT-CNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_RSTTLMNT_CNT");
        ads_Prtcpnt_View_Adp_State_Of_Issue = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_State_Of_Issue", "ADP-STATE-OF-ISSUE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ADP_STATE_OF_ISSUE");
        ads_Prtcpnt_View_Adp_Orgnl_Issue_State = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Orgnl_Issue_State", "ADP-ORGNL-ISSUE-STATE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ADP_ORGNL_ISSUE_STATE");
        ads_Prtcpnt_View_Adp_Ss_Field_Chng_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Ss_Field_Chng_Ind", "ADP-SS-FIELD-CHNG-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_SS_FIELD_CHNG_IND");
        ads_Prtcpnt_View_Adp_Orig_Rqst_Id = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Orig_Rqst_Id", "ADP-ORIG-RQST-ID", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "ADP_ORIG_RQST_ID");
        ads_Prtcpnt_View_Adp_Ivc_Rlvr_Cde = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Ivc_Rlvr_Cde", "ADP-IVC-RLVR-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_IVC_RLVR_CDE");
        ads_Prtcpnt_View_Adp_Ivc_Pymnt_Rule = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Ivc_Pymnt_Rule", "ADP-IVC-PYMNT-RULE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_IVC_PYMNT_RULE");
        ads_Prtcpnt_View_Adp_Hold_Cde = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Hold_Cde", "ADP-HOLD-CDE", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADP_HOLD_CDE");
        ads_Prtcpnt_View_Adp_Plan_Nbr = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Plan_Nbr", "ADP-PLAN-NBR", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "ADP_PLAN_NBR");
        ads_Prtcpnt_View_Adp_Frm_Irc_Dta = vw_ads_Prtcpnt_View.getRecord().newGroupInGroup("ads_Prtcpnt_View_Adp_Frm_Irc_Dta", "ADP-FRM-IRC-DTA", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADS_PRTCPNT_ADP_FRM_IRC_DTA");
        ads_Prtcpnt_View_Adp_Frm_Irc_Cde = ads_Prtcpnt_View_Adp_Frm_Irc_Dta.newFieldArrayInGroup("ads_Prtcpnt_View_Adp_Frm_Irc_Cde", "ADP-FRM-IRC-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1,15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADP_FRM_IRC_CDE", "ADS_PRTCPNT_ADP_FRM_IRC_DTA");
        ads_Prtcpnt_View_Adp_To_Irc_Cde = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_To_Irc_Cde", "ADP-TO-IRC-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADP_TO_IRC_CDE");
        ads_Prtcpnt_View_Adp_Irc_Cde = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Irc_Cde", "ADP-IRC-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADP_IRC_CDE");
        ads_Prtcpnt_View_Adp_Ackl_Lttr_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Ackl_Lttr_Ind", "ADP-ACKL-LTTR-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_ACKL_LTTR_IND");
        ads_Prtcpnt_View_Adp_Ntfctn_Lttr_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Ntfctn_Lttr_Ind", "ADP-NTFCTN-LTTR-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_NTFCTN_LTTR_IND");
        ads_Prtcpnt_View_Adp_Pi_Task_Id = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Pi_Task_Id", "ADP-PI-TASK-ID", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "ADP_PI_TASK_ID");
        ads_Prtcpnt_View_Adp_Plan_Nme = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Plan_Nme", "ADP-PLAN-NME", FieldType.STRING, 
            32, RepeatingFieldStrategy.None, "ADP_PLAN_NME");
        ads_Prtcpnt_View_Adp_Lttr_Dlvry_Typ = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Lttr_Dlvry_Typ", "ADP-LTTR-DLVRY-TYP", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_LTTR_DLVRY_TYP");
        ads_Prtcpnt_View_Adp_Eft_Check_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Eft_Check_Ind", "ADP-EFT-CHECK-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_EFT_CHECK_IND");
        ads_Prtcpnt_View_Adp_Plan_Admin = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Plan_Admin", "ADP-PLAN-ADMIN", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_PLAN_ADMIN");
        ads_Prtcpnt_View_Adp_Rtb_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Rtb_Ind", "ADP-RTB-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_RTB_IND");
        ads_Prtcpnt_View_Adp_Rtb_Alt_Addr_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Rtb_Alt_Addr_Ind", "ADP-RTB-ALT-ADDR-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_RTB_ALT_ADDR_IND");
        ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Data = vw_ads_Prtcpnt_View.getRecord().newGroupInGroup("ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Data", "ADP-RTB-ALT-DEST-DATA");
        ads_Prtcpnt_View_Adp_Rtb_Alt_Name = ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Rtb_Alt_Name", "ADP-RTB-ALT-NAME", 
            FieldType.STRING, 35, RepeatingFieldStrategy.None, "ADP_RTB_ALT_NAME");
        ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_TxtMuGroup = ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Data.newGroupInGroup("ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_TxtMuGroup", 
            "ADP_RTB_ALT_DEST_ADDR_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "ADS_PRTCPNT_ADP_RTB_ALT_DEST_ADDR_TXT");
        ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_Txt = ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_TxtMuGroup.newFieldArrayInGroup("ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_Txt", 
            "ADP-RTB-ALT-DEST-ADDR-TXT", FieldType.STRING, 35, new DbsArrayController(1,5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADP_RTB_ALT_DEST_ADDR_TXT");
        ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_Zip = ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_Zip", 
            "ADP-RTB-ALT-DEST-ADDR-ZIP", FieldType.STRING, 9, RepeatingFieldStrategy.None, "ADP_RTB_ALT_DEST_ADDR_ZIP");
        ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_Typ_Cde = ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Addr_Typ_Cde", 
            "ADP-RTB-ALT-DEST-ADDR-TYP-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_RTB_ALT_DEST_ADDR_TYP_CDE");
        ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Acct_Nbr = ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Acct_Nbr", 
            "ADP-RTB-ALT-DEST-ACCT-NBR", FieldType.STRING, 21, RepeatingFieldStrategy.None, "ADP_RTB_ALT_DEST_ACCT_NBR");
        ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Acct_Typ = ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Acct_Typ", 
            "ADP-RTB-ALT-DEST-ACCT-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_RTB_ALT_DEST_ACCT_TYP");
        ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Trnst_Cde = ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Trnst_Cde", 
            "ADP-RTB-ALT-DEST-TRNST-CDE", FieldType.STRING, 9, RepeatingFieldStrategy.None, "ADP_RTB_ALT_DEST_TRNST_CDE");
        ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Hold_Cde = ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Hold_Cde", 
            "ADP-RTB-ALT-DEST-HOLD-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADP_RTB_ALT_DEST_HOLD_CDE");
        ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Rlvr_Ind = ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Rlvr_Ind", 
            "ADP-RTB-ALT-DEST-RLVR-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_RTB_ALT_DEST_RLVR_IND");
        ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Rlvr_Dest = ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Rlvr_Dest", 
            "ADP-RTB-ALT-DEST-RLVR-DEST", FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADP_RTB_ALT_DEST_RLVR_DEST");
        ads_Prtcpnt_View_Adp_Rtb_Alt_Eft_Check_Ind = ads_Prtcpnt_View_Adp_Rtb_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_View_Adp_Rtb_Alt_Eft_Check_Ind", 
            "ADP-RTB-ALT-EFT-CHECK-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_RTB_ALT_EFT_CHECK_IND");
        ads_Prtcpnt_View_Adp_T395_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_T395_Ind", "ADP-T395-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_T395_IND");
        ads_Prtcpnt_View_Adp_Roth_Plan_Type = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Roth_Plan_Type", "ADP-ROTH-PLAN-TYPE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_ROTH_PLAN_TYPE");
        ads_Prtcpnt_View_Adp_Roth_Frst_Cntrbtn_Dte = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Roth_Frst_Cntrbtn_Dte", "ADP-ROTH-FRST-CNTRBTN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_ROTH_FRST_CNTRBTN_DTE");
        ads_Prtcpnt_View_Adp_Roth_Dsblty_Dte = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Roth_Dsblty_Dte", "ADP-ROTH-DSBLTY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_ROTH_DSBLTY_DTE");
        ads_Prtcpnt_View_Adp_Roth_Rqst_Ind = vw_ads_Prtcpnt_View.getRecord().newFieldInGroup("ads_Prtcpnt_View_Adp_Roth_Rqst_Ind", "ADP-ROTH-RQST-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_ROTH_RQST_IND");
        vw_ads_Prtcpnt_View.setUniquePeList();

        this.setRecordName("LdaAdsl540a");
    }

    public void initializeValues() throws Exception
    {
        reset();
        vw_ads_Prtcpnt_View.reset();
    }

    // Constructor
    public LdaAdsl540a() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
