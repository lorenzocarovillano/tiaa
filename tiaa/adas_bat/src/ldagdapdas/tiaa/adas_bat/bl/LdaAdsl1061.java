/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:49:24 PM
**        * FROM NATURAL LDA     : ADSL1061
************************************************************
**        * FILE NAME            : LdaAdsl1061.java
**        * CLASS NAME           : LdaAdsl1061
**        * INSTANCE NAME        : LdaAdsl1061
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdsl1061 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_Q_Tiaa_Minor_Accum;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Pay_Cnt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Units;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Dollars;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Gtd_Amt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Div_Amt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Fin_Gtd_Amt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Fin_Div_Amt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Fin_Ivc_Amt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Pay_Cnt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Units;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Dollars;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Gtd_Amt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Div_Amt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Fin_Gtd_Amt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Fin_Div_Amt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Fin_Ivc_Amt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Pay_Cnt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Units;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Dollars;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Gtd_Amt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Div_Amt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Fin_Gtd_Amt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Fin_Div_Amt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Fin_Ivc_Amt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Pay_Cnt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Units;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Dollars;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Gtd_Amt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Div_Amt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Fin_Gtd_Amt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Fin_Div_Amt;
    private DbsField pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Fin_Ivc_Amt;
    private DbsGroup pnd_Q_Tiaa_Sub_Accum;
    private DbsField pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Pay_Cnt;
    private DbsField pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Units;
    private DbsField pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Dollars;
    private DbsField pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Gtd_Amt;
    private DbsField pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Divid_Amt;
    private DbsField pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Gtd_Amt;
    private DbsField pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Div_Amt;
    private DbsField pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Ivc_Amt;
    private DbsGroup pnd_Q_C_Mth_Minor_Accum;
    private DbsField pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Pay_Cnt;
    private DbsField pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Units;
    private DbsField pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Dollars;
    private DbsField pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Gtd_Amt;
    private DbsField pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Div_Amt;
    private DbsField pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Fin_Gtd_Amt;
    private DbsField pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Fin_Div_Amt;
    private DbsGroup pnd_Q_C_Mth_Fin_Ivc_Amount;
    private DbsField pnd_Q_C_Mth_Fin_Ivc_Amount_Pnd_Q_C_Mth_Fin_Ivc_Amt;
    private DbsGroup pnd_Q_C_Ann_Minor_Accum;
    private DbsField pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Pay_Cnt;
    private DbsField pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Units;
    private DbsField pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Dollars;
    private DbsField pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Gtd_Amt;
    private DbsField pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Divid_Amt;
    private DbsField pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Fin_Gtd_Amt;
    private DbsField pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Fin_Div_Amt;
    private DbsGroup pnd_Q_C_Ann_Fin_Ivc_Amount;
    private DbsField pnd_Q_C_Ann_Fin_Ivc_Amount_Pnd_Q_C_Ann_Fin_Ivc_Amt;
    private DbsGroup pnd_Q_Cref_Mth_Sub_Total_Accum;
    private DbsField pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Pay_Cnt;
    private DbsField pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Units;
    private DbsField pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Dollars;
    private DbsField pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Gtd_Amt;
    private DbsField pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Div_Amt;
    private DbsField pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Fin_Gtd_Amt;
    private DbsField pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Fin_Div_Amt;
    private DbsGroup pnd_Q_Cref_Mth_Sub_Fin_Ivc_Amount;
    private DbsField pnd_Q_Cref_Mth_Sub_Fin_Ivc_Amount_Pnd_Q_Cref_Mth_Sub_Fin_Ivc_Amt;
    private DbsGroup pnd_Q_Cref_Ann_Sub_Total_Accum;
    private DbsField pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Pay_Cnt;
    private DbsField pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Units;
    private DbsField pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Dollars;
    private DbsField pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Gtd_Amt;
    private DbsField pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Div_Amt;
    private DbsField pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Fin_Gtd_Amt;
    private DbsField pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Fin_Div_Amt;
    private DbsGroup pnd_Q_Cref_Ann_Sub_Fin_Ivc_Amount;
    private DbsField pnd_Q_Cref_Ann_Sub_Fin_Ivc_Amount_Pnd_Q_Cref_Ann_Sub_Fin_Ivc_Amt;
    private DbsGroup pnd_Q_Total_Tiaa_Cref_Accum;
    private DbsField pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Pay_Cnt;
    private DbsField pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Units;
    private DbsField pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Dollars;
    private DbsField pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Gtd_Amt;
    private DbsField pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Div_Amt;
    private DbsField pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Gtd_Amt;
    private DbsField pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Div_Amt;
    private DbsField pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Ivc_Amt;

    public DbsGroup getPnd_Q_Tiaa_Minor_Accum() { return pnd_Q_Tiaa_Minor_Accum; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Pay_Cnt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Pay_Cnt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Units() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Units; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Dollars() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Dollars; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Gtd_Amt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Gtd_Amt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Div_Amt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Div_Amt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Fin_Gtd_Amt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Fin_Gtd_Amt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Fin_Div_Amt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Fin_Div_Amt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Fin_Ivc_Amt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Fin_Ivc_Amt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Pay_Cnt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Pay_Cnt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Units() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Units; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Dollars() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Dollars; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Gtd_Amt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Gtd_Amt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Div_Amt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Div_Amt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Fin_Gtd_Amt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Fin_Gtd_Amt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Fin_Div_Amt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Fin_Div_Amt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Fin_Ivc_Amt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Fin_Ivc_Amt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Pay_Cnt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Pay_Cnt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Units() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Units; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Dollars() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Dollars; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Gtd_Amt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Gtd_Amt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Div_Amt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Div_Amt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Fin_Gtd_Amt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Fin_Gtd_Amt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Fin_Div_Amt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Fin_Div_Amt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Fin_Ivc_Amt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Fin_Ivc_Amt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Pay_Cnt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Pay_Cnt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Units() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Units; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Dollars() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Dollars; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Gtd_Amt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Gtd_Amt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Div_Amt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Div_Amt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Fin_Gtd_Amt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Fin_Gtd_Amt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Fin_Div_Amt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Fin_Div_Amt; }

    public DbsField getPnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Fin_Ivc_Amt() { return pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Fin_Ivc_Amt; }

    public DbsGroup getPnd_Q_Tiaa_Sub_Accum() { return pnd_Q_Tiaa_Sub_Accum; }

    public DbsField getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Pay_Cnt() { return pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Pay_Cnt; }

    public DbsField getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Units() { return pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Units; }

    public DbsField getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Dollars() { return pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Dollars; }

    public DbsField getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Gtd_Amt() { return pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Gtd_Amt; }

    public DbsField getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Divid_Amt() { return pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Divid_Amt; }

    public DbsField getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Gtd_Amt() { return pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Gtd_Amt; }

    public DbsField getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Div_Amt() { return pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Div_Amt; }

    public DbsField getPnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Ivc_Amt() { return pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Ivc_Amt; }

    public DbsGroup getPnd_Q_C_Mth_Minor_Accum() { return pnd_Q_C_Mth_Minor_Accum; }

    public DbsField getPnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Pay_Cnt() { return pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Pay_Cnt; }

    public DbsField getPnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Units() { return pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Units; }

    public DbsField getPnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Dollars() { return pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Dollars; }

    public DbsField getPnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Gtd_Amt() { return pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Gtd_Amt; }

    public DbsField getPnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Div_Amt() { return pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Div_Amt; }

    public DbsField getPnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Fin_Gtd_Amt() { return pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Fin_Gtd_Amt; }

    public DbsField getPnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Fin_Div_Amt() { return pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Fin_Div_Amt; }

    public DbsGroup getPnd_Q_C_Mth_Fin_Ivc_Amount() { return pnd_Q_C_Mth_Fin_Ivc_Amount; }

    public DbsField getPnd_Q_C_Mth_Fin_Ivc_Amount_Pnd_Q_C_Mth_Fin_Ivc_Amt() { return pnd_Q_C_Mth_Fin_Ivc_Amount_Pnd_Q_C_Mth_Fin_Ivc_Amt; }

    public DbsGroup getPnd_Q_C_Ann_Minor_Accum() { return pnd_Q_C_Ann_Minor_Accum; }

    public DbsField getPnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Pay_Cnt() { return pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Pay_Cnt; }

    public DbsField getPnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Units() { return pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Units; }

    public DbsField getPnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Dollars() { return pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Dollars; }

    public DbsField getPnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Gtd_Amt() { return pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Gtd_Amt; }

    public DbsField getPnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Divid_Amt() { return pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Divid_Amt; }

    public DbsField getPnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Fin_Gtd_Amt() { return pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Fin_Gtd_Amt; }

    public DbsField getPnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Fin_Div_Amt() { return pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Fin_Div_Amt; }

    public DbsGroup getPnd_Q_C_Ann_Fin_Ivc_Amount() { return pnd_Q_C_Ann_Fin_Ivc_Amount; }

    public DbsField getPnd_Q_C_Ann_Fin_Ivc_Amount_Pnd_Q_C_Ann_Fin_Ivc_Amt() { return pnd_Q_C_Ann_Fin_Ivc_Amount_Pnd_Q_C_Ann_Fin_Ivc_Amt; }

    public DbsGroup getPnd_Q_Cref_Mth_Sub_Total_Accum() { return pnd_Q_Cref_Mth_Sub_Total_Accum; }

    public DbsField getPnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Pay_Cnt() { return pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Pay_Cnt; 
        }

    public DbsField getPnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Units() { return pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Units; }

    public DbsField getPnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Dollars() { return pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Dollars; 
        }

    public DbsField getPnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Gtd_Amt() { return pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Gtd_Amt; 
        }

    public DbsField getPnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Div_Amt() { return pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Div_Amt; 
        }

    public DbsField getPnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Fin_Gtd_Amt() { return pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Fin_Gtd_Amt; 
        }

    public DbsField getPnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Fin_Div_Amt() { return pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Fin_Div_Amt; 
        }

    public DbsGroup getPnd_Q_Cref_Mth_Sub_Fin_Ivc_Amount() { return pnd_Q_Cref_Mth_Sub_Fin_Ivc_Amount; }

    public DbsField getPnd_Q_Cref_Mth_Sub_Fin_Ivc_Amount_Pnd_Q_Cref_Mth_Sub_Fin_Ivc_Amt() { return pnd_Q_Cref_Mth_Sub_Fin_Ivc_Amount_Pnd_Q_Cref_Mth_Sub_Fin_Ivc_Amt; 
        }

    public DbsGroup getPnd_Q_Cref_Ann_Sub_Total_Accum() { return pnd_Q_Cref_Ann_Sub_Total_Accum; }

    public DbsField getPnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Pay_Cnt() { return pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Pay_Cnt; 
        }

    public DbsField getPnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Units() { return pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Units; }

    public DbsField getPnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Dollars() { return pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Dollars; 
        }

    public DbsField getPnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Gtd_Amt() { return pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Gtd_Amt; 
        }

    public DbsField getPnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Div_Amt() { return pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Div_Amt; 
        }

    public DbsField getPnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Fin_Gtd_Amt() { return pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Fin_Gtd_Amt; 
        }

    public DbsField getPnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Fin_Div_Amt() { return pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Fin_Div_Amt; 
        }

    public DbsGroup getPnd_Q_Cref_Ann_Sub_Fin_Ivc_Amount() { return pnd_Q_Cref_Ann_Sub_Fin_Ivc_Amount; }

    public DbsField getPnd_Q_Cref_Ann_Sub_Fin_Ivc_Amount_Pnd_Q_Cref_Ann_Sub_Fin_Ivc_Amt() { return pnd_Q_Cref_Ann_Sub_Fin_Ivc_Amount_Pnd_Q_Cref_Ann_Sub_Fin_Ivc_Amt; 
        }

    public DbsGroup getPnd_Q_Total_Tiaa_Cref_Accum() { return pnd_Q_Total_Tiaa_Cref_Accum; }

    public DbsField getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Pay_Cnt() { return pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Pay_Cnt; }

    public DbsField getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Units() { return pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Units; }

    public DbsField getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Dollars() { return pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Dollars; }

    public DbsField getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Gtd_Amt() { return pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Gtd_Amt; }

    public DbsField getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Div_Amt() { return pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Div_Amt; }

    public DbsField getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Gtd_Amt() { return pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Gtd_Amt; }

    public DbsField getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Div_Amt() { return pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Div_Amt; }

    public DbsField getPnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Ivc_Amt() { return pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Ivc_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_Q_Tiaa_Minor_Accum = newGroupInRecord("pnd_Q_Tiaa_Minor_Accum", "#Q-TIAA-MINOR-ACCUM");
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Pay_Cnt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Pay_Cnt", "#Q-TIAA-STD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Units = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Units", "#Q-TIAA-STD-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Dollars = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Dollars", "#Q-TIAA-STD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Gtd_Amt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Gtd_Amt", "#Q-TIAA-STD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Div_Amt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Div_Amt", "#Q-TIAA-STD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Fin_Gtd_Amt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Fin_Gtd_Amt", 
            "#Q-TIAA-STD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Fin_Div_Amt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Fin_Div_Amt", 
            "#Q-TIAA-STD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Fin_Ivc_Amt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Std_Fin_Ivc_Amt", 
            "#Q-TIAA-STD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Pay_Cnt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Pay_Cnt", "#Q-TIAA-GRD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Units = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Units", "#Q-TIAA-GRD-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Dollars = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Dollars", "#Q-TIAA-GRD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Gtd_Amt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Gtd_Amt", "#Q-TIAA-GRD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Div_Amt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Div_Amt", "#Q-TIAA-GRD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Fin_Gtd_Amt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Fin_Gtd_Amt", 
            "#Q-TIAA-GRD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Fin_Div_Amt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Fin_Div_Amt", 
            "#Q-TIAA-GRD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Fin_Ivc_Amt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Grd_Fin_Ivc_Amt", 
            "#Q-TIAA-GRD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Pay_Cnt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Pay_Cnt", 
            "#Q-TIAA-TPA-STD-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Units = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Units", "#Q-TIAA-TPA-STD-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Dollars = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Dollars", 
            "#Q-TIAA-TPA-STD-DOLLARS", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Gtd_Amt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Gtd_Amt", 
            "#Q-TIAA-TPA-STD-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Div_Amt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Div_Amt", 
            "#Q-TIAA-TPA-STD-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Fin_Gtd_Amt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Fin_Gtd_Amt", 
            "#Q-TIAA-TPA-STD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Fin_Div_Amt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Fin_Div_Amt", 
            "#Q-TIAA-TPA-STD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Fin_Ivc_Amt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Tpa_Std_Fin_Ivc_Amt", 
            "#Q-TIAA-TPA-STD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Pay_Cnt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Pay_Cnt", 
            "#Q-TIAA-IPRO-STD-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Units = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Units", 
            "#Q-TIAA-IPRO-STD-UNITS", FieldType.PACKED_DECIMAL, 10,4);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Dollars = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Dollars", 
            "#Q-TIAA-IPRO-STD-DOLLARS", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Gtd_Amt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Gtd_Amt", 
            "#Q-TIAA-IPRO-STD-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Div_Amt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Div_Amt", 
            "#Q-TIAA-IPRO-STD-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Fin_Gtd_Amt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Fin_Gtd_Amt", 
            "#Q-TIAA-IPRO-STD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Fin_Div_Amt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Fin_Div_Amt", 
            "#Q-TIAA-IPRO-STD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Fin_Ivc_Amt = pnd_Q_Tiaa_Minor_Accum.newFieldInGroup("pnd_Q_Tiaa_Minor_Accum_Pnd_Q_Tiaa_Ipro_Std_Fin_Ivc_Amt", 
            "#Q-TIAA-IPRO-STD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_Q_Tiaa_Sub_Accum = newGroupInRecord("pnd_Q_Tiaa_Sub_Accum", "#Q-TIAA-SUB-ACCUM");
        pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Pay_Cnt = pnd_Q_Tiaa_Sub_Accum.newFieldInGroup("pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Pay_Cnt", "#Q-TIAA-SUB-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Units = pnd_Q_Tiaa_Sub_Accum.newFieldInGroup("pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Units", "#Q-TIAA-SUB-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Dollars = pnd_Q_Tiaa_Sub_Accum.newFieldInGroup("pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Dollars", "#Q-TIAA-SUB-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Gtd_Amt = pnd_Q_Tiaa_Sub_Accum.newFieldInGroup("pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Gtd_Amt", "#Q-TIAA-SUB-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Divid_Amt = pnd_Q_Tiaa_Sub_Accum.newFieldInGroup("pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Divid_Amt", "#Q-TIAA-SUB-DIVID-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Gtd_Amt = pnd_Q_Tiaa_Sub_Accum.newFieldInGroup("pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Gtd_Amt", "#Q-TIAA-SUB-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Div_Amt = pnd_Q_Tiaa_Sub_Accum.newFieldInGroup("pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Div_Amt", "#Q-TIAA-SUB-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Ivc_Amt = pnd_Q_Tiaa_Sub_Accum.newFieldInGroup("pnd_Q_Tiaa_Sub_Accum_Pnd_Q_Tiaa_Sub_Fin_Ivc_Amt", "#Q-TIAA-SUB-FIN-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);

        pnd_Q_C_Mth_Minor_Accum = newGroupArrayInRecord("pnd_Q_C_Mth_Minor_Accum", "#Q-C-MTH-MINOR-ACCUM", new DbsArrayController(1,20));
        pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Pay_Cnt = pnd_Q_C_Mth_Minor_Accum.newFieldInGroup("pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Pay_Cnt", "#Q-C-MTH-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Units = pnd_Q_C_Mth_Minor_Accum.newFieldInGroup("pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Units", "#Q-C-MTH-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Dollars = pnd_Q_C_Mth_Minor_Accum.newFieldInGroup("pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Dollars", "#Q-C-MTH-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Gtd_Amt = pnd_Q_C_Mth_Minor_Accum.newFieldInGroup("pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Gtd_Amt", "#Q-C-MTH-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Div_Amt = pnd_Q_C_Mth_Minor_Accum.newFieldInGroup("pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Div_Amt", "#Q-C-MTH-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Fin_Gtd_Amt = pnd_Q_C_Mth_Minor_Accum.newFieldInGroup("pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Fin_Gtd_Amt", "#Q-C-MTH-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Fin_Div_Amt = pnd_Q_C_Mth_Minor_Accum.newFieldInGroup("pnd_Q_C_Mth_Minor_Accum_Pnd_Q_C_Mth_Fin_Div_Amt", "#Q-C-MTH-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);

        pnd_Q_C_Mth_Fin_Ivc_Amount = newGroupInRecord("pnd_Q_C_Mth_Fin_Ivc_Amount", "#Q-C-MTH-FIN-IVC-AMOUNT");
        pnd_Q_C_Mth_Fin_Ivc_Amount_Pnd_Q_C_Mth_Fin_Ivc_Amt = pnd_Q_C_Mth_Fin_Ivc_Amount.newFieldInGroup("pnd_Q_C_Mth_Fin_Ivc_Amount_Pnd_Q_C_Mth_Fin_Ivc_Amt", 
            "#Q-C-MTH-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_Q_C_Ann_Minor_Accum = newGroupArrayInRecord("pnd_Q_C_Ann_Minor_Accum", "#Q-C-ANN-MINOR-ACCUM", new DbsArrayController(1,20));
        pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Pay_Cnt = pnd_Q_C_Ann_Minor_Accum.newFieldInGroup("pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Pay_Cnt", "#Q-C-ANN-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Units = pnd_Q_C_Ann_Minor_Accum.newFieldInGroup("pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Units", "#Q-C-ANN-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Dollars = pnd_Q_C_Ann_Minor_Accum.newFieldInGroup("pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Dollars", "#Q-C-ANN-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Gtd_Amt = pnd_Q_C_Ann_Minor_Accum.newFieldInGroup("pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Gtd_Amt", "#Q-C-ANN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Divid_Amt = pnd_Q_C_Ann_Minor_Accum.newFieldInGroup("pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Divid_Amt", "#Q-C-ANN-DIVID-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Fin_Gtd_Amt = pnd_Q_C_Ann_Minor_Accum.newFieldInGroup("pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Fin_Gtd_Amt", "#Q-C-ANN-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Fin_Div_Amt = pnd_Q_C_Ann_Minor_Accum.newFieldInGroup("pnd_Q_C_Ann_Minor_Accum_Pnd_Q_C_Ann_Fin_Div_Amt", "#Q-C-ANN-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);

        pnd_Q_C_Ann_Fin_Ivc_Amount = newGroupInRecord("pnd_Q_C_Ann_Fin_Ivc_Amount", "#Q-C-ANN-FIN-IVC-AMOUNT");
        pnd_Q_C_Ann_Fin_Ivc_Amount_Pnd_Q_C_Ann_Fin_Ivc_Amt = pnd_Q_C_Ann_Fin_Ivc_Amount.newFieldInGroup("pnd_Q_C_Ann_Fin_Ivc_Amount_Pnd_Q_C_Ann_Fin_Ivc_Amt", 
            "#Q-C-ANN-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_Q_Cref_Mth_Sub_Total_Accum = newGroupInRecord("pnd_Q_Cref_Mth_Sub_Total_Accum", "#Q-CREF-MTH-SUB-TOTAL-ACCUM");
        pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Pay_Cnt = pnd_Q_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Pay_Cnt", 
            "#Q-CREF-MTH-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Units = pnd_Q_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Units", 
            "#Q-CREF-MTH-SUB-UNITS", FieldType.PACKED_DECIMAL, 10,4);
        pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Dollars = pnd_Q_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Dollars", 
            "#Q-CREF-MTH-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Gtd_Amt = pnd_Q_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Gtd_Amt", 
            "#Q-CREF-MTH-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Div_Amt = pnd_Q_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Div_Amt", 
            "#Q-CREF-MTH-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Fin_Gtd_Amt = pnd_Q_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Fin_Gtd_Amt", 
            "#Q-CREF-MTH-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Fin_Div_Amt = pnd_Q_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_Q_Cref_Mth_Sub_Total_Accum_Pnd_Q_Cref_Mth_Sub_Fin_Div_Amt", 
            "#Q-CREF-MTH-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);

        pnd_Q_Cref_Mth_Sub_Fin_Ivc_Amount = newGroupInRecord("pnd_Q_Cref_Mth_Sub_Fin_Ivc_Amount", "#Q-CREF-MTH-SUB-FIN-IVC-AMOUNT");
        pnd_Q_Cref_Mth_Sub_Fin_Ivc_Amount_Pnd_Q_Cref_Mth_Sub_Fin_Ivc_Amt = pnd_Q_Cref_Mth_Sub_Fin_Ivc_Amount.newFieldInGroup("pnd_Q_Cref_Mth_Sub_Fin_Ivc_Amount_Pnd_Q_Cref_Mth_Sub_Fin_Ivc_Amt", 
            "#Q-CREF-MTH-SUB-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_Q_Cref_Ann_Sub_Total_Accum = newGroupInRecord("pnd_Q_Cref_Ann_Sub_Total_Accum", "#Q-CREF-ANN-SUB-TOTAL-ACCUM");
        pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Pay_Cnt = pnd_Q_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Pay_Cnt", 
            "#Q-CREF-ANN-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Units = pnd_Q_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Units", 
            "#Q-CREF-ANN-SUB-UNITS", FieldType.PACKED_DECIMAL, 10,4);
        pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Dollars = pnd_Q_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Dollars", 
            "#Q-CREF-ANN-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Gtd_Amt = pnd_Q_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Gtd_Amt", 
            "#Q-CREF-ANN-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Div_Amt = pnd_Q_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Div_Amt", 
            "#Q-CREF-ANN-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Fin_Gtd_Amt = pnd_Q_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Fin_Gtd_Amt", 
            "#Q-CREF-ANN-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Fin_Div_Amt = pnd_Q_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_Q_Cref_Ann_Sub_Total_Accum_Pnd_Q_Cref_Ann_Sub_Fin_Div_Amt", 
            "#Q-CREF-ANN-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);

        pnd_Q_Cref_Ann_Sub_Fin_Ivc_Amount = newGroupInRecord("pnd_Q_Cref_Ann_Sub_Fin_Ivc_Amount", "#Q-CREF-ANN-SUB-FIN-IVC-AMOUNT");
        pnd_Q_Cref_Ann_Sub_Fin_Ivc_Amount_Pnd_Q_Cref_Ann_Sub_Fin_Ivc_Amt = pnd_Q_Cref_Ann_Sub_Fin_Ivc_Amount.newFieldInGroup("pnd_Q_Cref_Ann_Sub_Fin_Ivc_Amount_Pnd_Q_Cref_Ann_Sub_Fin_Ivc_Amt", 
            "#Q-CREF-ANN-SUB-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_Q_Total_Tiaa_Cref_Accum = newGroupInRecord("pnd_Q_Total_Tiaa_Cref_Accum", "#Q-TOTAL-TIAA-CREF-ACCUM");
        pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Pay_Cnt = pnd_Q_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Pay_Cnt", 
            "#Q-TOTAL-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Units = pnd_Q_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Units", "#Q-TOTAL-UNITS", 
            FieldType.PACKED_DECIMAL, 13,4);
        pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Dollars = pnd_Q_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Dollars", 
            "#Q-TOTAL-DOLLARS", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Gtd_Amt = pnd_Q_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Gtd_Amt", 
            "#Q-TOTAL-GTD-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Div_Amt = pnd_Q_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Div_Amt", 
            "#Q-TOTAL-DIV-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Gtd_Amt = pnd_Q_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Gtd_Amt", 
            "#Q-TOTAL-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Div_Amt = pnd_Q_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Div_Amt", 
            "#Q-TOTAL-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Ivc_Amt = pnd_Q_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_Q_Total_Tiaa_Cref_Accum_Pnd_Q_Total_Fin_Ivc_Amt", 
            "#Q-TOTAL-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 14,2);

        this.setRecordName("LdaAdsl1061");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAdsl1061() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
