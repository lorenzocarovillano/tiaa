/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 06:49:25 PM
**        * FROM NATURAL LDA     : ADSL1064
************************************************************
**        * FILE NAME            : LdaAdsl1064.java
**        * CLASS NAME           : LdaAdsl1064
**        * INSTANCE NAME        : LdaAdsl1064
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public final class LdaAdsl1064 extends DbsRecord
{
    // Properties
    private DbsGroup pnd_G_Tiaa_Minor_Accum;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Pay_Cnt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Units;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Dollars;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Gtd_Amt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Div_Amt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Gtd_Amt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Div_Amt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Ivc_Amt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Pay_Cnt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Units;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Dollars;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Gtd_Amt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Div_Amt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Gtd_Amt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Div_Amt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Ivc_Amt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Pay_Cnt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Units;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Dollars;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Gtd_Amt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Div_Amt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Gtd_Amt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Div_Amt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Ivc_Amt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Pay_Cnt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Units;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Dollars;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Gtd_Amt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Div_Amt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Gtd_Amt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Div_Amt;
    private DbsField pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Ivc_Amt;
    private DbsGroup pnd_G_Tiaa_Sub_Accum;
    private DbsField pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Pay_Cnt;
    private DbsField pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Units;
    private DbsField pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Dollars;
    private DbsField pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Gtd_Amt;
    private DbsField pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Divid_Amt;
    private DbsField pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Gtd_Amt;
    private DbsField pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Div_Amt;
    private DbsField pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Ivc_Amt;
    private DbsGroup pnd_G_C_Mth_Minor_Accum;
    private DbsField pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Pay_Cnt;
    private DbsField pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Units;
    private DbsField pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Dollars;
    private DbsField pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Gtd_Amt;
    private DbsField pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Div_Amt;
    private DbsField pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Fin_Gtd_Amt;
    private DbsField pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Fin_Div_Amt;
    private DbsGroup pnd_G_C_Mth_Fin_Ivc_Amount;
    private DbsField pnd_G_C_Mth_Fin_Ivc_Amount_Pnd_G_C_Mth_Fin_Ivc_Amt;
    private DbsGroup pnd_G_C_Ann_Minor_Accum;
    private DbsField pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Pay_Cnt;
    private DbsField pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Units;
    private DbsField pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Dollars;
    private DbsField pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Gtd_Amt;
    private DbsField pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Divid_Amt;
    private DbsField pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Fin_Gtd_Amt;
    private DbsField pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Fin_Div_Amt;
    private DbsGroup pnd_G_C_Ann_Fin_Ivc_Amount;
    private DbsField pnd_G_C_Ann_Fin_Ivc_Amount_Pnd_G_C_Ann_Fin_Ivc_Amt;
    private DbsGroup pnd_G_Cref_Mth_Sub_Total_Accum;
    private DbsField pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Pay_Cnt;
    private DbsField pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Units;
    private DbsField pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Dollars;
    private DbsField pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Gtd_Amt;
    private DbsField pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Div_Amt;
    private DbsField pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Fin_Gtd_Amt;
    private DbsField pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Fin_Div_Amt;
    private DbsField pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Fin_Ivc_Amt;
    private DbsGroup pnd_G_Cref_Ann_Sub_Total_Accum;
    private DbsField pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Pay_Cnt;
    private DbsField pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Units;
    private DbsField pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Dollars;
    private DbsField pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Gtd_Amt;
    private DbsField pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Div_Amt;
    private DbsField pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Fin_Gtd_Amt;
    private DbsField pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Fin_Div_Amt;
    private DbsField pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Fin_Ivc_Amt;
    private DbsGroup pnd_G_Total_Tiaa_Cref_Accum;
    private DbsField pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt;
    private DbsField pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Units;
    private DbsField pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Dollars;
    private DbsField pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Gtd_Amt;
    private DbsField pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Div_Amt;
    private DbsField pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Gtd_Amt;
    private DbsField pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Div_Amt;
    private DbsField pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Ivc_Amt;

    public DbsGroup getPnd_G_Tiaa_Minor_Accum() { return pnd_G_Tiaa_Minor_Accum; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Pay_Cnt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Pay_Cnt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Units() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Units; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Dollars() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Dollars; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Gtd_Amt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Gtd_Amt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Div_Amt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Div_Amt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Gtd_Amt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Gtd_Amt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Div_Amt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Div_Amt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Ivc_Amt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Ivc_Amt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Pay_Cnt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Pay_Cnt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Units() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Units; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Dollars() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Dollars; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Gtd_Amt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Gtd_Amt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Div_Amt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Div_Amt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Gtd_Amt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Gtd_Amt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Div_Amt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Div_Amt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Ivc_Amt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Ivc_Amt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Pay_Cnt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Pay_Cnt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Units() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Units; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Dollars() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Dollars; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Gtd_Amt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Gtd_Amt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Div_Amt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Div_Amt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Gtd_Amt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Gtd_Amt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Div_Amt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Div_Amt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Ivc_Amt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Ivc_Amt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Pay_Cnt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Pay_Cnt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Units() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Units; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Dollars() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Dollars; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Gtd_Amt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Gtd_Amt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Div_Amt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Div_Amt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Gtd_Amt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Gtd_Amt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Div_Amt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Div_Amt; }

    public DbsField getPnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Ivc_Amt() { return pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Ivc_Amt; }

    public DbsGroup getPnd_G_Tiaa_Sub_Accum() { return pnd_G_Tiaa_Sub_Accum; }

    public DbsField getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Pay_Cnt() { return pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Pay_Cnt; }

    public DbsField getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Units() { return pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Units; }

    public DbsField getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Dollars() { return pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Dollars; }

    public DbsField getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Gtd_Amt() { return pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Gtd_Amt; }

    public DbsField getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Divid_Amt() { return pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Divid_Amt; }

    public DbsField getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Gtd_Amt() { return pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Gtd_Amt; }

    public DbsField getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Div_Amt() { return pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Div_Amt; }

    public DbsField getPnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Ivc_Amt() { return pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Ivc_Amt; }

    public DbsGroup getPnd_G_C_Mth_Minor_Accum() { return pnd_G_C_Mth_Minor_Accum; }

    public DbsField getPnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Pay_Cnt() { return pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Pay_Cnt; }

    public DbsField getPnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Units() { return pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Units; }

    public DbsField getPnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Dollars() { return pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Dollars; }

    public DbsField getPnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Gtd_Amt() { return pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Gtd_Amt; }

    public DbsField getPnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Div_Amt() { return pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Div_Amt; }

    public DbsField getPnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Fin_Gtd_Amt() { return pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Fin_Gtd_Amt; }

    public DbsField getPnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Fin_Div_Amt() { return pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Fin_Div_Amt; }

    public DbsGroup getPnd_G_C_Mth_Fin_Ivc_Amount() { return pnd_G_C_Mth_Fin_Ivc_Amount; }

    public DbsField getPnd_G_C_Mth_Fin_Ivc_Amount_Pnd_G_C_Mth_Fin_Ivc_Amt() { return pnd_G_C_Mth_Fin_Ivc_Amount_Pnd_G_C_Mth_Fin_Ivc_Amt; }

    public DbsGroup getPnd_G_C_Ann_Minor_Accum() { return pnd_G_C_Ann_Minor_Accum; }

    public DbsField getPnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Pay_Cnt() { return pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Pay_Cnt; }

    public DbsField getPnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Units() { return pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Units; }

    public DbsField getPnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Dollars() { return pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Dollars; }

    public DbsField getPnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Gtd_Amt() { return pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Gtd_Amt; }

    public DbsField getPnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Divid_Amt() { return pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Divid_Amt; }

    public DbsField getPnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Fin_Gtd_Amt() { return pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Fin_Gtd_Amt; }

    public DbsField getPnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Fin_Div_Amt() { return pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Fin_Div_Amt; }

    public DbsGroup getPnd_G_C_Ann_Fin_Ivc_Amount() { return pnd_G_C_Ann_Fin_Ivc_Amount; }

    public DbsField getPnd_G_C_Ann_Fin_Ivc_Amount_Pnd_G_C_Ann_Fin_Ivc_Amt() { return pnd_G_C_Ann_Fin_Ivc_Amount_Pnd_G_C_Ann_Fin_Ivc_Amt; }

    public DbsGroup getPnd_G_Cref_Mth_Sub_Total_Accum() { return pnd_G_Cref_Mth_Sub_Total_Accum; }

    public DbsField getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Pay_Cnt() { return pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Pay_Cnt; 
        }

    public DbsField getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Units() { return pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Units; }

    public DbsField getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Dollars() { return pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Dollars; 
        }

    public DbsField getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Gtd_Amt() { return pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Gtd_Amt; 
        }

    public DbsField getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Div_Amt() { return pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Div_Amt; 
        }

    public DbsField getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Fin_Gtd_Amt() { return pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Fin_Gtd_Amt; 
        }

    public DbsField getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Fin_Div_Amt() { return pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Fin_Div_Amt; 
        }

    public DbsField getPnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Fin_Ivc_Amt() { return pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Fin_Ivc_Amt; 
        }

    public DbsGroup getPnd_G_Cref_Ann_Sub_Total_Accum() { return pnd_G_Cref_Ann_Sub_Total_Accum; }

    public DbsField getPnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Pay_Cnt() { return pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Pay_Cnt; 
        }

    public DbsField getPnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Units() { return pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Units; }

    public DbsField getPnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Dollars() { return pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Dollars; 
        }

    public DbsField getPnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Gtd_Amt() { return pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Gtd_Amt; 
        }

    public DbsField getPnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Div_Amt() { return pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Div_Amt; 
        }

    public DbsField getPnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Fin_Gtd_Amt() { return pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Fin_Gtd_Amt; 
        }

    public DbsField getPnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Fin_Div_Amt() { return pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Fin_Div_Amt; 
        }

    public DbsField getPnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Fin_Ivc_Amt() { return pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Fin_Ivc_Amt; 
        }

    public DbsGroup getPnd_G_Total_Tiaa_Cref_Accum() { return pnd_G_Total_Tiaa_Cref_Accum; }

    public DbsField getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt() { return pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt; }

    public DbsField getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Units() { return pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Units; }

    public DbsField getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Dollars() { return pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Dollars; }

    public DbsField getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Gtd_Amt() { return pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Gtd_Amt; }

    public DbsField getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Div_Amt() { return pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Div_Amt; }

    public DbsField getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Gtd_Amt() { return pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Gtd_Amt; }

    public DbsField getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Div_Amt() { return pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Div_Amt; }

    public DbsField getPnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Ivc_Amt() { return pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Ivc_Amt; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        pnd_G_Tiaa_Minor_Accum = newGroupInRecord("pnd_G_Tiaa_Minor_Accum", "#G-TIAA-MINOR-ACCUM");
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Pay_Cnt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Pay_Cnt", "#G-TIAA-STD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Units = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Units", "#G-TIAA-STD-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Dollars = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Dollars", "#G-TIAA-STD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Gtd_Amt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Gtd_Amt", "#G-TIAA-STD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Div_Amt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Div_Amt", "#G-TIAA-STD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Gtd_Amt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Gtd_Amt", 
            "#G-TIAA-STD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Div_Amt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Div_Amt", 
            "#G-TIAA-STD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Ivc_Amt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Std_Fin_Ivc_Amt", 
            "#G-TIAA-STD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Pay_Cnt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Pay_Cnt", "#G-TIAA-GRD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Units = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Units", "#G-TIAA-GRD-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Dollars = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Dollars", "#G-TIAA-GRD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Gtd_Amt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Gtd_Amt", "#G-TIAA-GRD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Div_Amt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Div_Amt", "#G-TIAA-GRD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Gtd_Amt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Gtd_Amt", 
            "#G-TIAA-GRD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Div_Amt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Div_Amt", 
            "#G-TIAA-GRD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Ivc_Amt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Grd_Fin_Ivc_Amt", 
            "#G-TIAA-GRD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Pay_Cnt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Pay_Cnt", 
            "#G-TIAA-TPA-STD-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Units = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Units", "#G-TIAA-TPA-STD-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Dollars = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Dollars", 
            "#G-TIAA-TPA-STD-DOLLARS", FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Gtd_Amt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Gtd_Amt", 
            "#G-TIAA-TPA-STD-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Div_Amt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Div_Amt", 
            "#G-TIAA-TPA-STD-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Gtd_Amt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Gtd_Amt", 
            "#G-TIAA-TPA-STD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Div_Amt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Div_Amt", 
            "#G-TIAA-TPA-STD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Ivc_Amt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Tpa_Std_Fin_Ivc_Amt", 
            "#G-TIAA-TPA-STD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Pay_Cnt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Pay_Cnt", 
            "#G-TIAA-IPRO-STD-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Units = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Units", 
            "#G-TIAA-IPRO-STD-UNITS", FieldType.PACKED_DECIMAL, 10,4);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Dollars = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Dollars", 
            "#G-TIAA-IPRO-STD-DOLLARS", FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Gtd_Amt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Gtd_Amt", 
            "#G-TIAA-IPRO-STD-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Div_Amt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Div_Amt", 
            "#G-TIAA-IPRO-STD-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Gtd_Amt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Gtd_Amt", 
            "#G-TIAA-IPRO-STD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Div_Amt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Div_Amt", 
            "#G-TIAA-IPRO-STD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Ivc_Amt = pnd_G_Tiaa_Minor_Accum.newFieldInGroup("pnd_G_Tiaa_Minor_Accum_Pnd_G_Tiaa_Ipro_Std_Fin_Ivc_Amt", 
            "#G-TIAA-IPRO-STD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_G_Tiaa_Sub_Accum = newGroupInRecord("pnd_G_Tiaa_Sub_Accum", "#G-TIAA-SUB-ACCUM");
        pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Pay_Cnt = pnd_G_Tiaa_Sub_Accum.newFieldInGroup("pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Pay_Cnt", "#G-TIAA-SUB-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Units = pnd_G_Tiaa_Sub_Accum.newFieldInGroup("pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Units", "#G-TIAA-SUB-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Dollars = pnd_G_Tiaa_Sub_Accum.newFieldInGroup("pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Dollars", "#G-TIAA-SUB-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Gtd_Amt = pnd_G_Tiaa_Sub_Accum.newFieldInGroup("pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Gtd_Amt", "#G-TIAA-SUB-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Divid_Amt = pnd_G_Tiaa_Sub_Accum.newFieldInGroup("pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Divid_Amt", "#G-TIAA-SUB-DIVID-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Gtd_Amt = pnd_G_Tiaa_Sub_Accum.newFieldInGroup("pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Gtd_Amt", "#G-TIAA-SUB-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Div_Amt = pnd_G_Tiaa_Sub_Accum.newFieldInGroup("pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Div_Amt", "#G-TIAA-SUB-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12,2);
        pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Ivc_Amt = pnd_G_Tiaa_Sub_Accum.newFieldInGroup("pnd_G_Tiaa_Sub_Accum_Pnd_G_Tiaa_Sub_Fin_Ivc_Amt", "#G-TIAA-SUB-FIN-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9,2);

        pnd_G_C_Mth_Minor_Accum = newGroupArrayInRecord("pnd_G_C_Mth_Minor_Accum", "#G-C-MTH-MINOR-ACCUM", new DbsArrayController(1,20));
        pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Pay_Cnt = pnd_G_C_Mth_Minor_Accum.newFieldInGroup("pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Pay_Cnt", "#G-C-MTH-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Units = pnd_G_C_Mth_Minor_Accum.newFieldInGroup("pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Units", "#G-C-MTH-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Dollars = pnd_G_C_Mth_Minor_Accum.newFieldInGroup("pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Dollars", "#G-C-MTH-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Gtd_Amt = pnd_G_C_Mth_Minor_Accum.newFieldInGroup("pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Gtd_Amt", "#G-C-MTH-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Div_Amt = pnd_G_C_Mth_Minor_Accum.newFieldInGroup("pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Div_Amt", "#G-C-MTH-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Fin_Gtd_Amt = pnd_G_C_Mth_Minor_Accum.newFieldInGroup("pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Fin_Gtd_Amt", "#G-C-MTH-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Fin_Div_Amt = pnd_G_C_Mth_Minor_Accum.newFieldInGroup("pnd_G_C_Mth_Minor_Accum_Pnd_G_C_Mth_Fin_Div_Amt", "#G-C-MTH-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);

        pnd_G_C_Mth_Fin_Ivc_Amount = newGroupInRecord("pnd_G_C_Mth_Fin_Ivc_Amount", "#G-C-MTH-FIN-IVC-AMOUNT");
        pnd_G_C_Mth_Fin_Ivc_Amount_Pnd_G_C_Mth_Fin_Ivc_Amt = pnd_G_C_Mth_Fin_Ivc_Amount.newFieldInGroup("pnd_G_C_Mth_Fin_Ivc_Amount_Pnd_G_C_Mth_Fin_Ivc_Amt", 
            "#G-C-MTH-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_G_C_Ann_Minor_Accum = newGroupArrayInRecord("pnd_G_C_Ann_Minor_Accum", "#G-C-ANN-MINOR-ACCUM", new DbsArrayController(1,20));
        pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Pay_Cnt = pnd_G_C_Ann_Minor_Accum.newFieldInGroup("pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Pay_Cnt", "#G-C-ANN-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Units = pnd_G_C_Ann_Minor_Accum.newFieldInGroup("pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Units", "#G-C-ANN-UNITS", 
            FieldType.PACKED_DECIMAL, 10,4);
        pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Dollars = pnd_G_C_Ann_Minor_Accum.newFieldInGroup("pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Dollars", "#G-C-ANN-DOLLARS", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Gtd_Amt = pnd_G_C_Ann_Minor_Accum.newFieldInGroup("pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Gtd_Amt", "#G-C-ANN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Divid_Amt = pnd_G_C_Ann_Minor_Accum.newFieldInGroup("pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Divid_Amt", "#G-C-ANN-DIVID-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Fin_Gtd_Amt = pnd_G_C_Ann_Minor_Accum.newFieldInGroup("pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Fin_Gtd_Amt", "#G-C-ANN-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);
        pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Fin_Div_Amt = pnd_G_C_Ann_Minor_Accum.newFieldInGroup("pnd_G_C_Ann_Minor_Accum_Pnd_G_C_Ann_Fin_Div_Amt", "#G-C-ANN-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 11,2);

        pnd_G_C_Ann_Fin_Ivc_Amount = newGroupInRecord("pnd_G_C_Ann_Fin_Ivc_Amount", "#G-C-ANN-FIN-IVC-AMOUNT");
        pnd_G_C_Ann_Fin_Ivc_Amount_Pnd_G_C_Ann_Fin_Ivc_Amt = pnd_G_C_Ann_Fin_Ivc_Amount.newFieldInGroup("pnd_G_C_Ann_Fin_Ivc_Amount_Pnd_G_C_Ann_Fin_Ivc_Amt", 
            "#G-C-ANN-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_G_Cref_Mth_Sub_Total_Accum = newGroupInRecord("pnd_G_Cref_Mth_Sub_Total_Accum", "#G-CREF-MTH-SUB-TOTAL-ACCUM");
        pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Pay_Cnt = pnd_G_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Pay_Cnt", 
            "#G-CREF-MTH-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Units = pnd_G_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Units", 
            "#G-CREF-MTH-SUB-UNITS", FieldType.PACKED_DECIMAL, 10,4);
        pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Dollars = pnd_G_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Dollars", 
            "#G-CREF-MTH-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 12,2);
        pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Gtd_Amt = pnd_G_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Gtd_Amt", 
            "#G-CREF-MTH-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Div_Amt = pnd_G_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Div_Amt", 
            "#G-CREF-MTH-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Fin_Gtd_Amt = pnd_G_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Fin_Gtd_Amt", 
            "#G-CREF-MTH-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Fin_Div_Amt = pnd_G_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Fin_Div_Amt", 
            "#G-CREF-MTH-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Fin_Ivc_Amt = pnd_G_Cref_Mth_Sub_Total_Accum.newFieldInGroup("pnd_G_Cref_Mth_Sub_Total_Accum_Pnd_G_Cref_Mth_Sub_Fin_Ivc_Amt", 
            "#G-CREF-MTH-SUB-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_G_Cref_Ann_Sub_Total_Accum = newGroupInRecord("pnd_G_Cref_Ann_Sub_Total_Accum", "#G-CREF-ANN-SUB-TOTAL-ACCUM");
        pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Pay_Cnt = pnd_G_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Pay_Cnt", 
            "#G-CREF-ANN-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Units = pnd_G_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Units", 
            "#G-CREF-ANN-SUB-UNITS", FieldType.PACKED_DECIMAL, 10,4);
        pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Dollars = pnd_G_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Dollars", 
            "#G-CREF-ANN-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 12,2);
        pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Gtd_Amt = pnd_G_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Gtd_Amt", 
            "#G-CREF-ANN-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Div_Amt = pnd_G_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Div_Amt", 
            "#G-CREF-ANN-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Fin_Gtd_Amt = pnd_G_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Fin_Gtd_Amt", 
            "#G-CREF-ANN-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Fin_Div_Amt = pnd_G_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Fin_Div_Amt", 
            "#G-CREF-ANN-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12,2);
        pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Fin_Ivc_Amt = pnd_G_Cref_Ann_Sub_Total_Accum.newFieldInGroup("pnd_G_Cref_Ann_Sub_Total_Accum_Pnd_G_Cref_Ann_Sub_Fin_Ivc_Amt", 
            "#G-CREF-ANN-SUB-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 9,2);

        pnd_G_Total_Tiaa_Cref_Accum = newGroupInRecord("pnd_G_Total_Tiaa_Cref_Accum", "#G-TOTAL-TIAA-CREF-ACCUM");
        pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt = pnd_G_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Pay_Cnt", 
            "#G-TOTAL-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Units = pnd_G_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Units", "#G-TOTAL-UNITS", 
            FieldType.PACKED_DECIMAL, 13,4);
        pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Dollars = pnd_G_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Dollars", 
            "#G-TOTAL-DOLLARS", FieldType.PACKED_DECIMAL, 14,2);
        pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Gtd_Amt = pnd_G_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Gtd_Amt", 
            "#G-TOTAL-GTD-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Div_Amt = pnd_G_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Div_Amt", 
            "#G-TOTAL-DIV-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Gtd_Amt = pnd_G_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Gtd_Amt", 
            "#G-TOTAL-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Div_Amt = pnd_G_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Div_Amt", 
            "#G-TOTAL-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 14,2);
        pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Ivc_Amt = pnd_G_Total_Tiaa_Cref_Accum.newFieldInGroup("pnd_G_Total_Tiaa_Cref_Accum_Pnd_G_Total_Fin_Ivc_Amt", 
            "#G-TOTAL-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 14,2);

        this.setRecordName("LdaAdsl1064");
    }

    public void initializeValues() throws Exception
    {
        reset();
    }

    // Constructor
    public LdaAdsl1064() throws Exception
    {
        super();
        initializeFields();
        initializeValues();
    }
}
