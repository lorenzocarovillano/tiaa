/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 07:17:36 PM
**        * FROM NATURAL PDA     : ADSA540
************************************************************
**        * FILE NAME            : PdaAdsa540.java
**        * CLASS NAME           : PdaAdsa540
**        * INSTANCE NAME        : PdaAdsa540
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.domain.*;
import tiaa.tiaacommon.bl.*;

public class PdaAdsa540 extends PdaBase
{
    // Properties
    private DbsGroup ads_Tx_Msg_Linkage;
    private DbsField ads_Tx_Msg_Linkage_Ads_Tx_Tiaa_No;
    private DbsField ads_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_No;
    private DbsGroup ads_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_NoRedef1;
    private DbsField ads_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_Cd;
    private DbsField ads_Tx_Msg_Linkage_Ads_Tx_Cref_No;
    private DbsField ads_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_No;
    private DbsGroup ads_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_NoRedef2;
    private DbsField ads_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_Cd;
    private DbsField ads_Tx_Msg_Linkage_Ads_Rsdncy_Cde;
    private DbsField ads_Tx_Msg_Linkage_Ads_Ctzn_Cde;
    private DbsField ads_Tx_Msg_Linkage_Ads_Tx_Ss_Num;
    private DbsGroup ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind;
    private DbsField ads_Tx_Msg_Linkage_Ads_Pp_Pymt_Ind;
    private DbsField ads_Tx_Msg_Linkage_Ads_Mdo_Pymt_Ind;
    private DbsField ads_Tx_Msg_Linkage_Ads_Rtb_T_Pymt_Ind;
    private DbsField ads_Tx_Msg_Linkage_Ads_Rtb_C_Pymt_Ind;
    private DbsField ads_Tx_Msg_Linkage_Ads_Ls_Pymt_Ind;
    private DbsField ads_Tx_Msg_Linkage_Ads_Rollvr_Ind;
    private DbsField ads_Tx_Msg_Linkage_Ads_Pp_Rollvr_Ind;
    private DbsField ads_Tx_Msg_Linkage_Ads_Filler;
    private DbsField ads_Tx_Msg_Linkage_Ads_Settl_Option;
    private DbsField ads_Tx_Msg_Linkage_Ads_Term_Years;
    private DbsField ads_Tx_Msg_Linkage_Ads_Spouse_Ind;
    private DbsField ads_Tx_Msg_Linkage_Ads_Money_Source;
    private DbsField ads_Tx_Msg_Linkage_Ads_Lob;
    private DbsField ads_Tx_Msg_Linkage_Ads_Pymnt_Typ_Ind;
    private DbsField ads_Tx_Msg_Linkage_Ads_Hardship_Ind;
    private DbsField ads_Tx_Msg_Linkage_Ads_Dob;
    private DbsField ads_Tx_Msg_Linkage_Ads_Dod;
    private DbsField ads_Tx_Msg_Linkage_Ads_Roth_Frst_Cntrbtn_Dte;
    private DbsField ads_Tx_Msg_Linkage_Ads_Nra_Flag;
    private DbsField ads_Tx_Msg_Linkage_Ads_Nra_Msg_Lns_1;
    private DbsGroup ads_Tx_Msg_Linkage_Ads_Tiaa_Msg_Area;
    private DbsGroup ads_Tx_Msg_Linkage_Ads_Tiaa_Lump_Sum_Msg_Area;
    private DbsField ads_Tx_Msg_Linkage_Ads_T_F_Lump_Msg_Lns;
    private DbsField ads_Tx_Msg_Linkage_Ads_T_S_Lump_Msg_Lns;
    private DbsField ads_Tx_Msg_Linkage_Ads_T_L_Lump_Msg_Lns;
    private DbsGroup ads_Tx_Msg_Linkage_Ads_Tiaa_Pp_Sum_Msg_Area;
    private DbsField ads_Tx_Msg_Linkage_Ads_T_F_Pp_Msg_Lns;
    private DbsField ads_Tx_Msg_Linkage_Ads_T_S_Pp_Msg_Lns;
    private DbsField ads_Tx_Msg_Linkage_Ads_T_L_Pp_Msg_Lns;
    private DbsGroup ads_Tx_Msg_Linkage_Ads_Cref_Msg_Area;
    private DbsGroup ads_Tx_Msg_Linkage_Ads_Cref_Lump_Sum_Msg_Area;
    private DbsField ads_Tx_Msg_Linkage_Ads_C_F_Lump_Msg_Lns;
    private DbsField ads_Tx_Msg_Linkage_Ads_C_S_Lump_Msg_Lns;
    private DbsField ads_Tx_Msg_Linkage_Ads_C_L_Lump_Msg_Lns;
    private DbsGroup ads_Tx_Msg_Linkage_Ads_Cref_Pp_Sum_Msg_Area;
    private DbsField ads_Tx_Msg_Linkage_Ads_C_F_Pp_Msg_Lns;
    private DbsField ads_Tx_Msg_Linkage_Ads_C_S_Pp_Msg_Lns;
    private DbsField ads_Tx_Msg_Linkage_Ads_C_L_Pp_Msg_Lns;

    public DbsGroup getAds_Tx_Msg_Linkage() { return ads_Tx_Msg_Linkage; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Tx_Tiaa_No() { return ads_Tx_Msg_Linkage_Ads_Tx_Tiaa_No; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_No() { return ads_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_No; }

    public DbsGroup getAds_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_NoRedef1() { return ads_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_NoRedef1; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_Cd() { return ads_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_Cd; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Tx_Cref_No() { return ads_Tx_Msg_Linkage_Ads_Tx_Cref_No; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_No() { return ads_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_No; }

    public DbsGroup getAds_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_NoRedef2() { return ads_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_NoRedef2; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_Cd() { return ads_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_Cd; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Rsdncy_Cde() { return ads_Tx_Msg_Linkage_Ads_Rsdncy_Cde; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Ctzn_Cde() { return ads_Tx_Msg_Linkage_Ads_Ctzn_Cde; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Tx_Ss_Num() { return ads_Tx_Msg_Linkage_Ads_Tx_Ss_Num; }

    public DbsGroup getAds_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind() { return ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Pp_Pymt_Ind() { return ads_Tx_Msg_Linkage_Ads_Pp_Pymt_Ind; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Mdo_Pymt_Ind() { return ads_Tx_Msg_Linkage_Ads_Mdo_Pymt_Ind; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Rtb_T_Pymt_Ind() { return ads_Tx_Msg_Linkage_Ads_Rtb_T_Pymt_Ind; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Rtb_C_Pymt_Ind() { return ads_Tx_Msg_Linkage_Ads_Rtb_C_Pymt_Ind; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Ls_Pymt_Ind() { return ads_Tx_Msg_Linkage_Ads_Ls_Pymt_Ind; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Rollvr_Ind() { return ads_Tx_Msg_Linkage_Ads_Rollvr_Ind; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Pp_Rollvr_Ind() { return ads_Tx_Msg_Linkage_Ads_Pp_Rollvr_Ind; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Filler() { return ads_Tx_Msg_Linkage_Ads_Filler; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Settl_Option() { return ads_Tx_Msg_Linkage_Ads_Settl_Option; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Term_Years() { return ads_Tx_Msg_Linkage_Ads_Term_Years; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Spouse_Ind() { return ads_Tx_Msg_Linkage_Ads_Spouse_Ind; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Money_Source() { return ads_Tx_Msg_Linkage_Ads_Money_Source; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Lob() { return ads_Tx_Msg_Linkage_Ads_Lob; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Pymnt_Typ_Ind() { return ads_Tx_Msg_Linkage_Ads_Pymnt_Typ_Ind; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Hardship_Ind() { return ads_Tx_Msg_Linkage_Ads_Hardship_Ind; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Dob() { return ads_Tx_Msg_Linkage_Ads_Dob; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Dod() { return ads_Tx_Msg_Linkage_Ads_Dod; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Roth_Frst_Cntrbtn_Dte() { return ads_Tx_Msg_Linkage_Ads_Roth_Frst_Cntrbtn_Dte; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Nra_Flag() { return ads_Tx_Msg_Linkage_Ads_Nra_Flag; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_Nra_Msg_Lns_1() { return ads_Tx_Msg_Linkage_Ads_Nra_Msg_Lns_1; }

    public DbsGroup getAds_Tx_Msg_Linkage_Ads_Tiaa_Msg_Area() { return ads_Tx_Msg_Linkage_Ads_Tiaa_Msg_Area; }

    public DbsGroup getAds_Tx_Msg_Linkage_Ads_Tiaa_Lump_Sum_Msg_Area() { return ads_Tx_Msg_Linkage_Ads_Tiaa_Lump_Sum_Msg_Area; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_T_F_Lump_Msg_Lns() { return ads_Tx_Msg_Linkage_Ads_T_F_Lump_Msg_Lns; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_T_S_Lump_Msg_Lns() { return ads_Tx_Msg_Linkage_Ads_T_S_Lump_Msg_Lns; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_T_L_Lump_Msg_Lns() { return ads_Tx_Msg_Linkage_Ads_T_L_Lump_Msg_Lns; }

    public DbsGroup getAds_Tx_Msg_Linkage_Ads_Tiaa_Pp_Sum_Msg_Area() { return ads_Tx_Msg_Linkage_Ads_Tiaa_Pp_Sum_Msg_Area; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_T_F_Pp_Msg_Lns() { return ads_Tx_Msg_Linkage_Ads_T_F_Pp_Msg_Lns; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_T_S_Pp_Msg_Lns() { return ads_Tx_Msg_Linkage_Ads_T_S_Pp_Msg_Lns; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_T_L_Pp_Msg_Lns() { return ads_Tx_Msg_Linkage_Ads_T_L_Pp_Msg_Lns; }

    public DbsGroup getAds_Tx_Msg_Linkage_Ads_Cref_Msg_Area() { return ads_Tx_Msg_Linkage_Ads_Cref_Msg_Area; }

    public DbsGroup getAds_Tx_Msg_Linkage_Ads_Cref_Lump_Sum_Msg_Area() { return ads_Tx_Msg_Linkage_Ads_Cref_Lump_Sum_Msg_Area; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_C_F_Lump_Msg_Lns() { return ads_Tx_Msg_Linkage_Ads_C_F_Lump_Msg_Lns; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_C_S_Lump_Msg_Lns() { return ads_Tx_Msg_Linkage_Ads_C_S_Lump_Msg_Lns; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_C_L_Lump_Msg_Lns() { return ads_Tx_Msg_Linkage_Ads_C_L_Lump_Msg_Lns; }

    public DbsGroup getAds_Tx_Msg_Linkage_Ads_Cref_Pp_Sum_Msg_Area() { return ads_Tx_Msg_Linkage_Ads_Cref_Pp_Sum_Msg_Area; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_C_F_Pp_Msg_Lns() { return ads_Tx_Msg_Linkage_Ads_C_F_Pp_Msg_Lns; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_C_S_Pp_Msg_Lns() { return ads_Tx_Msg_Linkage_Ads_C_S_Pp_Msg_Lns; }

    public DbsField getAds_Tx_Msg_Linkage_Ads_C_L_Pp_Msg_Lns() { return ads_Tx_Msg_Linkage_Ads_C_L_Pp_Msg_Lns; }

    // Data Initialization Methods
    private void initializeFields() throws Exception
    {
        ads_Tx_Msg_Linkage = dbsRecord.newGroupInRecord("ads_Tx_Msg_Linkage", "ADS-TX-MSG-LINKAGE");
        ads_Tx_Msg_Linkage.setParameterOption(ParameterOption.ByReference);
        ads_Tx_Msg_Linkage_Ads_Tx_Tiaa_No = ads_Tx_Msg_Linkage.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Tx_Tiaa_No", "ADS-TX-TIAA-NO", FieldType.STRING, 
            8);
        ads_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_No = ads_Tx_Msg_Linkage.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_No", "ADS-TX-TIAA-PAYEE-NO", 
            FieldType.NUMERIC, 2);
        ads_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_NoRedef1 = ads_Tx_Msg_Linkage.newGroupInGroup("ads_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_NoRedef1", "Redefines", 
            ads_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_No);
        ads_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_Cd = ads_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_NoRedef1.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_Cd", 
            "ADS-TX-TIAA-PAYEE-CD", FieldType.STRING, 2);
        ads_Tx_Msg_Linkage_Ads_Tx_Cref_No = ads_Tx_Msg_Linkage.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Tx_Cref_No", "ADS-TX-CREF-NO", FieldType.STRING, 
            8);
        ads_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_No = ads_Tx_Msg_Linkage.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_No", "ADS-TX-CREF-PAYEE-NO", 
            FieldType.NUMERIC, 2);
        ads_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_NoRedef2 = ads_Tx_Msg_Linkage.newGroupInGroup("ads_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_NoRedef2", "Redefines", 
            ads_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_No);
        ads_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_Cd = ads_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_NoRedef2.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_Cd", 
            "ADS-TX-CREF-PAYEE-CD", FieldType.STRING, 2);
        ads_Tx_Msg_Linkage_Ads_Rsdncy_Cde = ads_Tx_Msg_Linkage.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Rsdncy_Cde", "ADS-RSDNCY-CDE", FieldType.STRING, 
            2);
        ads_Tx_Msg_Linkage_Ads_Ctzn_Cde = ads_Tx_Msg_Linkage.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Ctzn_Cde", "ADS-CTZN-CDE", FieldType.STRING, 2);
        ads_Tx_Msg_Linkage_Ads_Tx_Ss_Num = ads_Tx_Msg_Linkage.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Tx_Ss_Num", "ADS-TX-SS-NUM", FieldType.NUMERIC, 
            9);
        ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind = ads_Tx_Msg_Linkage.newGroupInGroup("ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind", "ADS-TYPE-OF-PAYMENT-IND");
        ads_Tx_Msg_Linkage_Ads_Pp_Pymt_Ind = ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Pp_Pymt_Ind", "ADS-PP-PYMT-IND", 
            FieldType.STRING, 1);
        ads_Tx_Msg_Linkage_Ads_Mdo_Pymt_Ind = ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Mdo_Pymt_Ind", "ADS-MDO-PYMT-IND", 
            FieldType.STRING, 1);
        ads_Tx_Msg_Linkage_Ads_Rtb_T_Pymt_Ind = ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Rtb_T_Pymt_Ind", "ADS-RTB-T-PYMT-IND", 
            FieldType.STRING, 1);
        ads_Tx_Msg_Linkage_Ads_Rtb_C_Pymt_Ind = ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Rtb_C_Pymt_Ind", "ADS-RTB-C-PYMT-IND", 
            FieldType.STRING, 1);
        ads_Tx_Msg_Linkage_Ads_Ls_Pymt_Ind = ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Ls_Pymt_Ind", "ADS-LS-PYMT-IND", 
            FieldType.STRING, 1);
        ads_Tx_Msg_Linkage_Ads_Rollvr_Ind = ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Rollvr_Ind", "ADS-ROLLVR-IND", 
            FieldType.STRING, 1);
        ads_Tx_Msg_Linkage_Ads_Pp_Rollvr_Ind = ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Pp_Rollvr_Ind", "ADS-PP-ROLLVR-IND", 
            FieldType.STRING, 1);
        ads_Tx_Msg_Linkage_Ads_Filler = ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Filler", "ADS-FILLER", FieldType.STRING, 
            5);
        ads_Tx_Msg_Linkage_Ads_Settl_Option = ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Settl_Option", "ADS-SETTL-OPTION", 
            FieldType.STRING, 5);
        ads_Tx_Msg_Linkage_Ads_Term_Years = ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Term_Years", "ADS-TERM-YEARS", 
            FieldType.STRING, 1);
        ads_Tx_Msg_Linkage_Ads_Spouse_Ind = ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Spouse_Ind", "ADS-SPOUSE-IND", 
            FieldType.STRING, 1);
        ads_Tx_Msg_Linkage_Ads_Money_Source = ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Money_Source", "ADS-MONEY-SOURCE", 
            FieldType.STRING, 5);
        ads_Tx_Msg_Linkage_Ads_Lob = ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Lob", "ADS-LOB", FieldType.STRING, 
            5);
        ads_Tx_Msg_Linkage_Ads_Pymnt_Typ_Ind = ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Pymnt_Typ_Ind", "ADS-PYMNT-TYP-IND", 
            FieldType.STRING, 2);
        ads_Tx_Msg_Linkage_Ads_Hardship_Ind = ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Hardship_Ind", "ADS-HARDSHIP-IND", 
            FieldType.STRING, 1);
        ads_Tx_Msg_Linkage_Ads_Dob = ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Dob", "ADS-DOB", FieldType.NUMERIC, 
            8);
        ads_Tx_Msg_Linkage_Ads_Dod = ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Dod", "ADS-DOD", FieldType.NUMERIC, 
            8);
        ads_Tx_Msg_Linkage_Ads_Roth_Frst_Cntrbtn_Dte = ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Roth_Frst_Cntrbtn_Dte", 
            "ADS-ROTH-FRST-CNTRBTN-DTE", FieldType.NUMERIC, 8);
        ads_Tx_Msg_Linkage_Ads_Nra_Flag = ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Nra_Flag", "ADS-NRA-FLAG", 
            FieldType.STRING, 1);
        ads_Tx_Msg_Linkage_Ads_Nra_Msg_Lns_1 = ads_Tx_Msg_Linkage_Ads_Type_Of_Payment_Ind.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_Nra_Msg_Lns_1", "ADS-NRA-MSG-LNS-1", 
            FieldType.STRING, 50);
        ads_Tx_Msg_Linkage_Ads_Tiaa_Msg_Area = ads_Tx_Msg_Linkage.newGroupInGroup("ads_Tx_Msg_Linkage_Ads_Tiaa_Msg_Area", "ADS-TIAA-MSG-AREA");
        ads_Tx_Msg_Linkage_Ads_Tiaa_Lump_Sum_Msg_Area = ads_Tx_Msg_Linkage_Ads_Tiaa_Msg_Area.newGroupInGroup("ads_Tx_Msg_Linkage_Ads_Tiaa_Lump_Sum_Msg_Area", 
            "ADS-TIAA-LUMP-SUM-MSG-AREA");
        ads_Tx_Msg_Linkage_Ads_T_F_Lump_Msg_Lns = ads_Tx_Msg_Linkage_Ads_Tiaa_Lump_Sum_Msg_Area.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_T_F_Lump_Msg_Lns", 
            "ADS-T-F-LUMP-MSG-LNS", FieldType.STRING, 200);
        ads_Tx_Msg_Linkage_Ads_T_S_Lump_Msg_Lns = ads_Tx_Msg_Linkage_Ads_Tiaa_Lump_Sum_Msg_Area.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_T_S_Lump_Msg_Lns", 
            "ADS-T-S-LUMP-MSG-LNS", FieldType.STRING, 250);
        ads_Tx_Msg_Linkage_Ads_T_L_Lump_Msg_Lns = ads_Tx_Msg_Linkage_Ads_Tiaa_Lump_Sum_Msg_Area.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_T_L_Lump_Msg_Lns", 
            "ADS-T-L-LUMP-MSG-LNS", FieldType.STRING, 150);
        ads_Tx_Msg_Linkage_Ads_Tiaa_Pp_Sum_Msg_Area = ads_Tx_Msg_Linkage_Ads_Tiaa_Msg_Area.newGroupInGroup("ads_Tx_Msg_Linkage_Ads_Tiaa_Pp_Sum_Msg_Area", 
            "ADS-TIAA-PP-SUM-MSG-AREA");
        ads_Tx_Msg_Linkage_Ads_T_F_Pp_Msg_Lns = ads_Tx_Msg_Linkage_Ads_Tiaa_Pp_Sum_Msg_Area.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_T_F_Pp_Msg_Lns", "ADS-T-F-PP-MSG-LNS", 
            FieldType.STRING, 200);
        ads_Tx_Msg_Linkage_Ads_T_S_Pp_Msg_Lns = ads_Tx_Msg_Linkage_Ads_Tiaa_Pp_Sum_Msg_Area.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_T_S_Pp_Msg_Lns", "ADS-T-S-PP-MSG-LNS", 
            FieldType.STRING, 250);
        ads_Tx_Msg_Linkage_Ads_T_L_Pp_Msg_Lns = ads_Tx_Msg_Linkage_Ads_Tiaa_Pp_Sum_Msg_Area.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_T_L_Pp_Msg_Lns", "ADS-T-L-PP-MSG-LNS", 
            FieldType.STRING, 150);
        ads_Tx_Msg_Linkage_Ads_Cref_Msg_Area = ads_Tx_Msg_Linkage.newGroupInGroup("ads_Tx_Msg_Linkage_Ads_Cref_Msg_Area", "ADS-CREF-MSG-AREA");
        ads_Tx_Msg_Linkage_Ads_Cref_Lump_Sum_Msg_Area = ads_Tx_Msg_Linkage_Ads_Cref_Msg_Area.newGroupInGroup("ads_Tx_Msg_Linkage_Ads_Cref_Lump_Sum_Msg_Area", 
            "ADS-CREF-LUMP-SUM-MSG-AREA");
        ads_Tx_Msg_Linkage_Ads_C_F_Lump_Msg_Lns = ads_Tx_Msg_Linkage_Ads_Cref_Lump_Sum_Msg_Area.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_C_F_Lump_Msg_Lns", 
            "ADS-C-F-LUMP-MSG-LNS", FieldType.STRING, 200);
        ads_Tx_Msg_Linkage_Ads_C_S_Lump_Msg_Lns = ads_Tx_Msg_Linkage_Ads_Cref_Lump_Sum_Msg_Area.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_C_S_Lump_Msg_Lns", 
            "ADS-C-S-LUMP-MSG-LNS", FieldType.STRING, 250);
        ads_Tx_Msg_Linkage_Ads_C_L_Lump_Msg_Lns = ads_Tx_Msg_Linkage_Ads_Cref_Lump_Sum_Msg_Area.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_C_L_Lump_Msg_Lns", 
            "ADS-C-L-LUMP-MSG-LNS", FieldType.STRING, 150);
        ads_Tx_Msg_Linkage_Ads_Cref_Pp_Sum_Msg_Area = ads_Tx_Msg_Linkage_Ads_Cref_Msg_Area.newGroupInGroup("ads_Tx_Msg_Linkage_Ads_Cref_Pp_Sum_Msg_Area", 
            "ADS-CREF-PP-SUM-MSG-AREA");
        ads_Tx_Msg_Linkage_Ads_C_F_Pp_Msg_Lns = ads_Tx_Msg_Linkage_Ads_Cref_Pp_Sum_Msg_Area.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_C_F_Pp_Msg_Lns", "ADS-C-F-PP-MSG-LNS", 
            FieldType.STRING, 200);
        ads_Tx_Msg_Linkage_Ads_C_S_Pp_Msg_Lns = ads_Tx_Msg_Linkage_Ads_Cref_Pp_Sum_Msg_Area.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_C_S_Pp_Msg_Lns", "ADS-C-S-PP-MSG-LNS", 
            FieldType.STRING, 250);
        ads_Tx_Msg_Linkage_Ads_C_L_Pp_Msg_Lns = ads_Tx_Msg_Linkage_Ads_Cref_Pp_Sum_Msg_Area.newFieldInGroup("ads_Tx_Msg_Linkage_Ads_C_L_Pp_Msg_Lns", "ADS-C-L-PP-MSG-LNS", 
            FieldType.STRING, 150);

        dbsRecord.reset();
    }

    // Constructors
    public PdaAdsa540(DbsRecord dbsRecord) throws Exception
    {
        super(dbsRecord);
        initializeFields();
    }
}

