/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:58 PM
**        *   FROM NATURAL MAP   :  Adsm770
************************************************************
**        * FILE NAME               : Adsm770.java
**        * CLASS NAME              : Adsm770
**        * INSTANCE NAME           : Adsm770
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #TIAA-STD-METHOD-PAYMENT-TOTALS.#TIAA-IA-RATE-STD-PAYMENT                                                                *     #TIAA-STD-METHOD-PAYMENT-TOTALS.#TIAA-IA-STD-DIVIDEND-PAY-RENEW 
    *     #TIAA-STD-METHOD-PAYMENT-TOTALS.#TIAA-IA-STD-DIVIDEND-PAYMENT                                                            *     #TIAA-STD-METHOD-PAYMENT-TOTALS.#TIAA-IA-STD-GURANTEED-PAY-RENEW 
    *     #TIAA-STD-METHOD-PAYMENT-TOTALS.#TIAA-IA-STD-GURANTEED-PAYMENT
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm770 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Std_Payment;
    private DbsField pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Dividend_Pay_Renew;
    private DbsField pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Dividend_Payment;
    private DbsField pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Pay_Renew;
    private DbsField pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Payment;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Std_Payment = parameters.newFieldInRecord("pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Std_Payment", 
            "#TIAA-STD-METHOD-PAYMENT-TOTALS.#TIAA-IA-RATE-STD-PAYMENT", FieldType.STRING, 2);
        pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Dividend_Pay_Renew = parameters.newFieldInRecord("pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Dividend_Pay_Renew", 
            "#TIAA-STD-METHOD-PAYMENT-TOTALS.#TIAA-IA-STD-DIVIDEND-PAY-RENEW", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Dividend_Payment = parameters.newFieldInRecord("pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Dividend_Payment", 
            "#TIAA-STD-METHOD-PAYMENT-TOTALS.#TIAA-IA-STD-DIVIDEND-PAYMENT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Pay_Renew = parameters.newFieldInRecord("pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Pay_Renew", 
            "#TIAA-STD-METHOD-PAYMENT-TOTALS.#TIAA-IA-STD-GURANTEED-PAY-RENEW", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Payment = parameters.newFieldInRecord("pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Payment", 
            "#TIAA-STD-METHOD-PAYMENT-TOTALS.#TIAA-IA-STD-GURANTEED-PAYMENT", FieldType.PACKED_DECIMAL, 13, 2);
        parameters.reset();
    }

    public Adsm770() throws Exception
    {
        super("Adsm770");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm770", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm770"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Std_Payment", pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Std_Payment, 
                true, 1, 2, 2, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Payment", pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Payment, 
                true, 1, 10, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Dividend_Payment", pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Dividend_Payment, 
                true, 1, 27, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Pay_Renew", pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Pay_Renew, 
                true, 1, 44, 16, "WHITE", "Z,ZZZ,ZZZ,ZZZ.99", true, false, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Dividend_Pay_Renew", pnd_Tiaa_Std_Method_Payment_Totals_Pnd_Tiaa_Ia_Std_Dividend_Pay_Renew, 
                true, 1, 61, 16, "WHITE", "Z,ZZZ,ZZZ,ZZZ.99", true, false, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
