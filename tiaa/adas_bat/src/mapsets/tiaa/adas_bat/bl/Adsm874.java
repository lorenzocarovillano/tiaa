/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:39 PM
**        *   FROM NATURAL MAP   :  Adsm874
************************************************************
**        * FILE NAME               : Adsm874.java
**        * CLASS NAME              : Adsm874
**        * INSTANCE NAME           : Adsm874
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    * MAP2: MAP PROFILES *****************************        200***********                                                       * .TTO      OOO1II RE 
    NE6IBL ?_      +()                    *                                                       * 060080NAZM874 N1NYUCN____        X         01 SYSDBA 
    YL             *                                                       ************************************************************************     
    *   * MAP2: VALIDATION *****************************************************                                                       * MAP2: END OF MAP 
    *****************************************************
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm874 extends BLNatBase
{
    // from LocalVariables/Parameters

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				
    }

    public Adsm874() throws Exception
    {
        super("Adsm874");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=080 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm874", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm874"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "ACCOUNT", "BLUE", 2, 1, 7);
            uiForm.setUiLabel("label_2", "SETTLEMENT", "BLUE", 2, 19, 10);
            uiForm.setUiLabel("label_3", "RTB", "BLUE", 2, 39, 3);
            uiForm.setUiLabel("label_4", "CREF MNTHLY UNITS", "BLUE", 2, 50, 17);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
