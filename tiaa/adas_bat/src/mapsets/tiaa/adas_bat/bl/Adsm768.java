/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:57 PM
**        *   FROM NATURAL MAP   :  Adsm768
************************************************************
**        * FILE NAME               : Adsm768.java
**        * CLASS NAME              : Adsm768
**        * INSTANCE NAME           : Adsm768
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #TIAA-GRADED-TOTALS.#TIAA-DA-RATE-GRD                                                                                    *     #TIAA-GRADED-TOTALS.#TIAA-IA-GRD-DIVIDEND-AMT 
    *     #TIAA-GRADED-TOTALS.#TIAA-IA-GRD-GUAR-COMMUT-REP-AMT                                                                     *     #TIAA-GRADED-TOTALS.#TIAA-IA-GRD-GURANTEED-AMT 
    *     #TIAA-GRADED-TOTALS.#TIAA-IA-RATE-GRD                                                                                    *     #TIAA-GRADED-TOTALS.#TIAA-PROD-GRD 
    *     #TIAA-GRADED-TOTALS.#TIAA-SETTLED-GRD-AMT
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm768 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Da_Rate_Grd;
    private DbsField pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Amt;
    private DbsField pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Guar_Commut_Rep_Amt;
    private DbsField pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Amt;
    private DbsField pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Rate_Grd;
    private DbsField pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Prod_Grd;
    private DbsField pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Settled_Grd_Amt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Da_Rate_Grd = parameters.newFieldInRecord("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Da_Rate_Grd", "#TIAA-GRADED-TOTALS.#TIAA-DA-RATE-GRD", 
            FieldType.STRING, 2);
        pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Amt = parameters.newFieldInRecord("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Amt", "#TIAA-GRADED-TOTALS.#TIAA-IA-GRD-DIVIDEND-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Guar_Commut_Rep_Amt = parameters.newFieldInRecord("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Guar_Commut_Rep_Amt", 
            "#TIAA-GRADED-TOTALS.#TIAA-IA-GRD-GUAR-COMMUT-REP-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Amt = parameters.newFieldInRecord("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Amt", "#TIAA-GRADED-TOTALS.#TIAA-IA-GRD-GURANTEED-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Rate_Grd = parameters.newFieldInRecord("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Rate_Grd", "#TIAA-GRADED-TOTALS.#TIAA-IA-RATE-GRD", 
            FieldType.STRING, 2);
        pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Prod_Grd = parameters.newFieldInRecord("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Prod_Grd", "#TIAA-GRADED-TOTALS.#TIAA-PROD-GRD", 
            FieldType.STRING, 1);
        pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Settled_Grd_Amt = parameters.newFieldInRecord("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Settled_Grd_Amt", "#TIAA-GRADED-TOTALS.#TIAA-SETTLED-GRD-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        parameters.reset();
    }

    public Adsm768() throws Exception
    {
        super("Adsm768");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm768", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm768"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Prod_Grd", pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Prod_Grd, true, 1, 2, 1, "WHITE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Da_Rate_Grd", pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Da_Rate_Grd, true, 1, 10, 2, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Rate_Grd", pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Rate_Grd, true, 1, 15, 2, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Settled_Grd_Amt", pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Settled_Grd_Amt, true, 1, 20, 18, "WHITE", 
                "-ZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Amt", pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Amt, true, 1, 
                41, 14, "WHITE", "-ZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Amt", pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Amt, true, 1, 61, 
                14, "WHITE", "-ZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Guar_Commut_Rep_Amt", pnd_Tiaa_Graded_Totals_Pnd_Tiaa_Ia_Grd_Guar_Commut_Rep_Amt, 
                true, 1, 79, 14, "WHITE", "-ZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
