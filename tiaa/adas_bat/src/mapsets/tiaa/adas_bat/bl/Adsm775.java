/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:59 PM
**        *   FROM NATURAL MAP   :  Adsm775
************************************************************
**        * FILE NAME               : Adsm775.java
**        * CLASS NAME              : Adsm775
**        * INSTANCE NAME           : Adsm775
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #TIAA-MINOR-TOTALS.#TIAA-DA-ACCUM-AMT/UNITS                                                                              *     #TIAA-MINOR-TOTALS.#TIAA-DA-RATE 
    *     #TIAA-MINOR-TOTALS.#TIAA-RTB-AMT/UNITS                                                                                   *     #TIAA-MINOR-TOTALS.#TIAA-SETTLED-AMT/UNITS 
    *     #TIAA-MINOR-TOTALS.#TIAA-SETTLED-RTB-AMT/UNIT
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm775 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Accum_Amtfslash_Units;
    private DbsField pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Rate;
    private DbsField pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Rtb_Amtfslash_Units;
    private DbsField pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Amtfslash_Units;
    private DbsField pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Rtb_Amtfslash_Unit;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Accum_Amtfslash_Units = parameters.newFieldInRecord("pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Accum_Amtfslash_Units", 
            "#TIAA-MINOR-TOTALS.#TIAA-DA-ACCUM-AMT/UNITS", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Rate = parameters.newFieldInRecord("pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Rate", "#TIAA-MINOR-TOTALS.#TIAA-DA-RATE", 
            FieldType.STRING, 2);
        pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Rtb_Amtfslash_Units = parameters.newFieldInRecord("pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Rtb_Amtfslash_Units", "#TIAA-MINOR-TOTALS.#TIAA-RTB-AMT/UNITS", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Amtfslash_Units = parameters.newFieldInRecord("pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Amtfslash_Units", 
            "#TIAA-MINOR-TOTALS.#TIAA-SETTLED-AMT/UNITS", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Rtb_Amtfslash_Unit = parameters.newFieldInRecord("pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Rtb_Amtfslash_Unit", 
            "#TIAA-MINOR-TOTALS.#TIAA-SETTLED-RTB-AMT/UNIT", FieldType.PACKED_DECIMAL, 12, 2);
        parameters.reset();
    }

    public Adsm775() throws Exception
    {
        super("Adsm775");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm775", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm775"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Rate", pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Rate, true, 1, 2, 2, "WHITE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Accum_Amtfslash_Units", pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Da_Accum_Amtfslash_Units, true, 
                1, 9, 14, "WHITE", "-ZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Amtfslash_Units", pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Amtfslash_Units, true, 
                1, 28, 14, "WHITE", "-ZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Rtb_Amtfslash_Units", pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Rtb_Amtfslash_Units, true, 1, 47, 
                14, "WHITE", "-ZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Rtb_Amtfslash_Unit", pnd_Tiaa_Minor_Totals_Pnd_Tiaa_Settled_Rtb_Amtfslash_Unit, 
                true, 1, 66, 14, "WHITE", "-ZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
