/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:39 PM
**        *   FROM NATURAL MAP   :  Adsm878
************************************************************
**        * FILE NAME               : Adsm878.java
**        * CLASS NAME              : Adsm878
**        * INSTANCE NAME           : Adsm878
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #CREF-MINOR-AMT-TOTALS.#CREF-DA-ACCUM-AMT                                                                                *     #CREF-MINOR-AMT-TOTALS.#CREF-DA-RATE 
    *     #CREF-MINOR-AMT-TOTALS.#CREF-PROD                                                                                        *     #CREF-MINOR-AMT-TOTALS.#CREF-RTB-AMT 
    *     #CREF-MINOR-AMT-TOTALS.#CREF-SETTLED-AMT                                                                                 *     #CREF-MINOR-AMT-TOTALS.#CREF-SETTLED-RTB-AMT 
    *     #CREF-MINOR-UNIT-TOTALS.#CREF-DA-ACCUM-UNITS                                                                             *     #CREF-MINOR-UNIT-TOTALS.#CREF-RTB-UNITS 
    *     #CREF-MINOR-UNIT-TOTALS.#CREF-SETTLED-RTB-UNITS                                                                          *     #CREF-MINOR-UNIT-TOTALS.#CREF-SETTLED-UNITS
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm878 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Accum_Amt;
    private DbsField pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Rate;
    private DbsField pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Prod;
    private DbsField pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Rtb_Amt;
    private DbsField pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Amt;
    private DbsField pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Rtb_Amt;
    private DbsField pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Da_Accum_Units;
    private DbsField pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Rtb_Units;
    private DbsField pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Rtb_Units;
    private DbsField pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Units;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Accum_Amt = parameters.newFieldInRecord("pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Accum_Amt", "#CREF-MINOR-AMT-TOTALS.#CREF-DA-ACCUM-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Rate = parameters.newFieldInRecord("pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Rate", "#CREF-MINOR-AMT-TOTALS.#CREF-DA-RATE", 
            FieldType.STRING, 2);
        pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Prod = parameters.newFieldInRecord("pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Prod", "#CREF-MINOR-AMT-TOTALS.#CREF-PROD", 
            FieldType.STRING, 1);
        pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Rtb_Amt = parameters.newFieldInRecord("pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Rtb_Amt", "#CREF-MINOR-AMT-TOTALS.#CREF-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Amt = parameters.newFieldInRecord("pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Amt", "#CREF-MINOR-AMT-TOTALS.#CREF-SETTLED-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Rtb_Amt = parameters.newFieldInRecord("pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Rtb_Amt", "#CREF-MINOR-AMT-TOTALS.#CREF-SETTLED-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Da_Accum_Units = parameters.newFieldInRecord("pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Da_Accum_Units", "#CREF-MINOR-UNIT-TOTALS.#CREF-DA-ACCUM-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 3);
        pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Rtb_Units = parameters.newFieldInRecord("pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Rtb_Units", "#CREF-MINOR-UNIT-TOTALS.#CREF-RTB-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 3);
        pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Rtb_Units = parameters.newFieldInRecord("pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Rtb_Units", "#CREF-MINOR-UNIT-TOTALS.#CREF-SETTLED-RTB-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 3);
        pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Units = parameters.newFieldInRecord("pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Units", "#CREF-MINOR-UNIT-TOTALS.#CREF-SETTLED-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 3);
        parameters.reset();
    }

    public Adsm878() throws Exception
    {
        super("Adsm878");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=080 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm878", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm878"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Prod", pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Prod, true, 1, 7, 1, "WHITE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Rate", pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Rate, true, 1, 17, 2, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Accum_Amt", pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Da_Accum_Amt, true, 1, 20, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Amt", pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Amt, true, 1, 35, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Rtb_Amt", pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Rtb_Amt, true, 1, 50, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Rtb_Amt", pnd_Cref_Minor_Amt_Totals_Pnd_Cref_Settled_Rtb_Amt, true, 1, 65, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Da_Accum_Units", pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Da_Accum_Units, true, 2, 22, 
                13, "WHITE", "Z,ZZZ,ZZ9.999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Units", pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Units, true, 2, 37, 13, 
                "WHITE", "Z,ZZZ,ZZ9.999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Rtb_Units", pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Rtb_Units, true, 2, 52, 13, "WHITE", 
                "Z,ZZZ,ZZ9.999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Rtb_Units", pnd_Cref_Minor_Unit_Totals_Pnd_Cref_Settled_Rtb_Units, true, 
                2, 67, 13, "WHITE", "Z,ZZZ,ZZ9.999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
