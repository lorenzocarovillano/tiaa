/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:59 PM
**        *   FROM NATURAL MAP   :  Adsm772
************************************************************
**        * FILE NAME               : Adsm772.java
**        * CLASS NAME              : Adsm772
**        * INSTANCE NAME           : Adsm772
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #TIAA-GRD-METHOD-PAYMENT-TOTALS.#TIAA-IA-GRD-DIVIDEND-PAY-RENEW                                                          *     #TIAA-GRD-METHOD-PAYMENT-TOTALS.#TIAA-IA-GRD-DIVIDEND-PAYMENT 
    *     #TIAA-GRD-METHOD-PAYMENT-TOTALS.#TIAA-IA-GRD-GURANTEED-PAY-RENEW                                                         *     #TIAA-GRD-METHOD-PAYMENT-TOTALS.#TIAA-IA-GRD-GURANTEED-PAYMENT 
    *     #TIAA-GRD-METHOD-PAYMENT-TOTALS.#TIAA-IA-RATE-GRD-PAYMENT
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm772 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Pay_Renew;
    private DbsField pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Payment;
    private DbsField pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Pay_Renew;
    private DbsField pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Payment;
    private DbsField pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Grd_Payment;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Pay_Renew = parameters.newFieldInRecord("pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Pay_Renew", 
            "#TIAA-GRD-METHOD-PAYMENT-TOTALS.#TIAA-IA-GRD-DIVIDEND-PAY-RENEW", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Payment = parameters.newFieldInRecord("pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Payment", 
            "#TIAA-GRD-METHOD-PAYMENT-TOTALS.#TIAA-IA-GRD-DIVIDEND-PAYMENT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Pay_Renew = parameters.newFieldInRecord("pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Pay_Renew", 
            "#TIAA-GRD-METHOD-PAYMENT-TOTALS.#TIAA-IA-GRD-GURANTEED-PAY-RENEW", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Payment = parameters.newFieldInRecord("pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Payment", 
            "#TIAA-GRD-METHOD-PAYMENT-TOTALS.#TIAA-IA-GRD-GURANTEED-PAYMENT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Grd_Payment = parameters.newFieldInRecord("pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Grd_Payment", 
            "#TIAA-GRD-METHOD-PAYMENT-TOTALS.#TIAA-IA-RATE-GRD-PAYMENT", FieldType.STRING, 2);
        parameters.reset();
    }

    public Adsm772() throws Exception
    {
        super("Adsm772");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=080 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm772", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm772"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Grd_Payment", pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Rate_Grd_Payment, 
                true, 1, 2, 2, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Payment", pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Payment, 
                true, 1, 11, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Payment", pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Payment, 
                true, 1, 28, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Pay_Renew", pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Guranteed_Pay_Renew, 
                true, 1, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZZ.99", true, false, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Pay_Renew", pnd_Tiaa_Grd_Method_Payment_Totals_Pnd_Tiaa_Ia_Grd_Dividend_Pay_Renew, 
                true, 1, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZZ.99", true, false, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
