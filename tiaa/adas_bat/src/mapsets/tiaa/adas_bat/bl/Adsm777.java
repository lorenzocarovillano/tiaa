/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:00 PM
**        *   FROM NATURAL MAP   :  Adsm777
************************************************************
**        * FILE NAME               : Adsm777.java
**        * CLASS NAME              : Adsm777
**        * INSTANCE NAME           : Adsm777
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #TIAA-TOTAL-SETTLEMENT-LINE.#TIAA-DA-ACCUM-TOTAL-SETTLE                                                                  *     #TIAA-TOTAL-SETTLEMENT-LINE.#TIAA-DA-RATE-TOTAL-SETTLE 
    *     #TIAA-TOTAL-SETTLEMENT-LINE.#TIAA-RTB-TOTAL-SETTLE                                                                       *     #TIAA-TOTAL-SETTLEMENT-LINE.#TIAA-SETTLED-RTB-TOTAL-SETTLE 
    *     #TIAA-TOTAL-SETTLEMENT-LINE.#TIAA-SETTLED-TOTAL-SETTLE
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm777 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Accum_Total_Settle;
    private DbsField pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Rate_Total_Settle;
    private DbsField pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Rtb_Total_Settle;
    private DbsField pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Rtb_Total_Settle;
    private DbsField pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Total_Settle;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Accum_Total_Settle = parameters.newFieldInRecord("pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Accum_Total_Settle", 
            "#TIAA-TOTAL-SETTLEMENT-LINE.#TIAA-DA-ACCUM-TOTAL-SETTLE", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Rate_Total_Settle = parameters.newFieldInRecord("pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Rate_Total_Settle", 
            "#TIAA-TOTAL-SETTLEMENT-LINE.#TIAA-DA-RATE-TOTAL-SETTLE", FieldType.STRING, 2);
        pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Rtb_Total_Settle = parameters.newFieldInRecord("pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Rtb_Total_Settle", 
            "#TIAA-TOTAL-SETTLEMENT-LINE.#TIAA-RTB-TOTAL-SETTLE", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Rtb_Total_Settle = parameters.newFieldInRecord("pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Rtb_Total_Settle", 
            "#TIAA-TOTAL-SETTLEMENT-LINE.#TIAA-SETTLED-RTB-TOTAL-SETTLE", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Total_Settle = parameters.newFieldInRecord("pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Total_Settle", 
            "#TIAA-TOTAL-SETTLEMENT-LINE.#TIAA-SETTLED-TOTAL-SETTLE", FieldType.PACKED_DECIMAL, 15, 2);
        parameters.reset();
    }

    public Adsm777() throws Exception
    {
        super("Adsm777");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm777", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm777"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Rate_Total_Settle", pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Rate_Total_Settle, 
                true, 1, 1, 2, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Accum_Total_Settle", pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Da_Accum_Total_Settle, 
                true, 1, 5, 18, "WHITE", "-ZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Total_Settle", pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Total_Settle, 
                true, 1, 24, 18, "WHITE", "-ZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Rtb_Total_Settle", pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Rtb_Total_Settle, 
                true, 1, 43, 18, "WHITE", "-ZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Rtb_Total_Settle", pnd_Tiaa_Total_Settlement_Line_Pnd_Tiaa_Settled_Rtb_Total_Settle, 
                true, 1, 62, 18, "WHITE", "-ZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
