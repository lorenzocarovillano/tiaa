/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:53 PM
**        *   FROM NATURAL MAP   :  Adsm666
************************************************************
**        * FILE NAME               : Adsm666.java
**        * CLASS NAME              : Adsm666
**        * INSTANCE NAME           : Adsm666
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #DASH-666 #PARTCIPANT-NAME-1 #RQST-ID.#UNIQUE-ID #T-TPA-IO-LIT
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm666 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Dash_666;
    private DbsField pnd_Partcipant_Name_1;
    private DbsField pnd_Rqst_Id_Pnd_Unique_Id;
    private DbsField pnd_T_Tpa_Io_Lit;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Dash_666 = parameters.newFieldInRecord("pnd_Dash_666", "#DASH-666", FieldType.STRING, 1);
        pnd_Partcipant_Name_1 = parameters.newFieldInRecord("pnd_Partcipant_Name_1", "#PARTCIPANT-NAME-1", FieldType.STRING, 30);
        pnd_Rqst_Id_Pnd_Unique_Id = parameters.newFieldInRecord("pnd_Rqst_Id_Pnd_Unique_Id", "#RQST-ID.#UNIQUE-ID", FieldType.NUMERIC, 12);
        pnd_T_Tpa_Io_Lit = parameters.newFieldInRecord("pnd_T_Tpa_Io_Lit", "#T-TPA-IO-LIT", FieldType.STRING, 4);
        parameters.reset();
    }

    public Adsm666() throws Exception
    {
        super("Adsm666");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm666", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm666"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Rqst_Id_Pnd_Unique_Id", pnd_Rqst_Id_Pnd_Unique_Id, true, 1, 2, 12, "WHITE", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Partcipant_Name_1", pnd_Partcipant_Name_1, true, 2, 2, 30, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_1", "SETTLEMENT CALCULATION INFORMATION", "BLUE", 5, 1, 34);
            uiForm.setUiLabel("label_2", "STANDARD METHOD", "BLUE", 7, 1, 15);
            uiForm.setUiControl("pnd_Dash_666", pnd_Dash_666, true, 7, 17, 1, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_T_Tpa_Io_Lit", pnd_T_Tpa_Io_Lit, true, 7, 19, 4, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "_______________", "BLUE", 8, 1, 15);
            uiForm.setUiLabel("label_4", "SETTLED", "BLUE", 9, 26, 7);
            uiForm.setUiLabel("label_5", "IA PAYMENTS", "BLUE", 9, 53, 11);
            uiForm.setUiLabel("label_6", "GUARANTEED", "BLUE", 9, 86, 10);
            uiForm.setUiLabel("label_7", "DA", "BLUE", 10, 10, 2);
            uiForm.setUiLabel("label_8", "IA", "BLUE", 10, 15, 2);
            uiForm.setUiLabel("label_9", "ACCUM", "BLUE", 10, 27, 5);
            uiForm.setUiLabel("label_10", "STANDARD", "BLUE", 10, 45, 8);
            uiForm.setUiLabel("label_11", "STANDARD", "BLUE", 10, 64, 8);
            uiForm.setUiLabel("label_12", "COMMUTED", "BLUE", 10, 87, 8);
            uiForm.setUiLabel("label_13", "PROD", "BLUE", 11, 1, 4);
            uiForm.setUiLabel("label_14", "RATE RATE", "BLUE", 11, 9, 9);
            uiForm.setUiLabel("label_15", "STANDARD", "BLUE", 11, 26, 8);
            uiForm.setUiLabel("label_16", "GUARANTEED", "BLUE", 11, 44, 10);
            uiForm.setUiLabel("label_17", "DIVIDEND", "BLUE", 11, 64, 8);
            uiForm.setUiLabel("label_18", "VALUE", "BLUE", 11, 88, 5);
            uiForm.setUiLabel("label_19", "___", "BLUE", 12, 1, 3);
            uiForm.setUiLabel("label_20", "_____ _____", "BLUE", 12, 8, 11);
            uiForm.setUiLabel("label_21", "_______________", "BLUE", 12, 23, 15);
            uiForm.setUiLabel("label_22", "______________", "BLUE", 12, 43, 14);
            uiForm.setUiLabel("label_23", "______________", "BLUE", 12, 61, 14);
            uiForm.setUiLabel("label_24", "_____________", "BLUE", 12, 84, 13);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
