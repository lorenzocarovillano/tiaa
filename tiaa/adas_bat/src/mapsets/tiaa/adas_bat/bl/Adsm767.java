/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:57 PM
**        *   FROM NATURAL MAP   :  Adsm767
************************************************************
**        * FILE NAME               : Adsm767.java
**        * CLASS NAME              : Adsm767
**        * INSTANCE NAME           : Adsm767
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #TIAA-STANDARD-SUB-TOTALS.#TIAA-IA-STD-DIVIDEND-SUB-TOTAL                                                                *     #TIAA-STANDARD-SUB-TOTALS.#TIAA-IA-STD-GUAR-COMMUT-SUB-TOT 
    *     #TIAA-STANDARD-SUB-TOTALS.#TIAA-IA-STD-GURANTEED-SUB-TOTAL                                                               *     #TIAA-STANDARD-SUB-TOTALS.#TIAA-SETTLED-STD-SUB-TOTAL
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm767 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Dividend_Sub_Total;
    private DbsField pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Guar_Commut_Sub_Tot;
    private DbsField pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Sub_Total;
    private DbsField pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Settled_Std_Sub_Total;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Dividend_Sub_Total = parameters.newFieldInRecord("pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Dividend_Sub_Total", 
            "#TIAA-STANDARD-SUB-TOTALS.#TIAA-IA-STD-DIVIDEND-SUB-TOTAL", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Guar_Commut_Sub_Tot = parameters.newFieldInRecord("pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Guar_Commut_Sub_Tot", 
            "#TIAA-STANDARD-SUB-TOTALS.#TIAA-IA-STD-GUAR-COMMUT-SUB-TOT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Sub_Total = parameters.newFieldInRecord("pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Sub_Total", 
            "#TIAA-STANDARD-SUB-TOTALS.#TIAA-IA-STD-GURANTEED-SUB-TOTAL", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Settled_Std_Sub_Total = parameters.newFieldInRecord("pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Settled_Std_Sub_Total", 
            "#TIAA-STANDARD-SUB-TOTALS.#TIAA-SETTLED-STD-SUB-TOTAL", FieldType.PACKED_DECIMAL, 15, 2);
        parameters.reset();
    }

    public Adsm767() throws Exception
    {
        super("Adsm767");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm767", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm767"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "----------------------------------------------------------------------------------------------", "", 1, 1, 94);
            uiForm.setUiLabel("label_2", "TOTALS", "BLUE", 2, 1, 6);
            uiForm.setUiControl("pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Settled_Std_Sub_Total", pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Settled_Std_Sub_Total, 
                true, 2, 20, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Sub_Total", pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Sub_Total, 
                true, 2, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Dividend_Sub_Total", pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Dividend_Sub_Total, 
                true, 2, 58, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Guar_Commut_Sub_Tot", pnd_Tiaa_Standard_Sub_Totals_Pnd_Tiaa_Ia_Std_Guar_Commut_Sub_Tot, 
                true, 2, 77, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "-------", "BLUE", 3, 1, 7);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
