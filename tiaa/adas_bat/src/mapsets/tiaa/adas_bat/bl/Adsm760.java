/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:54 PM
**        *   FROM NATURAL MAP   :  Adsm760
************************************************************
**        * FILE NAME               : Adsm760.java
**        * CLASS NAME              : Adsm760
**        * INSTANCE NAME           : Adsm760
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #TIAA-HEADER-AMT.#TIAA-HEADER-ACCOUNT                                                                                    *     #TIAA-HEADER-AMT.#TIAA-HEADER-GRD-AMT 
    *     #TIAA-HEADER-AMT.#TIAA-HEADER-GRD-TYP                                                                                    *     #TIAA-HEADER-AMT.#TIAA-HEADER-RTB-AMT 
    *     #TIAA-HEADER-AMT.#TIAA-HEADER-RTB-TYP                                                                                    *     #TIAA-HEADER-AMT.#TIAA-HEADER-SETTLE-AMT 
    *     #TIAA-HEADER-AMT.#TIAA-HEADER-SETTLE-TYP
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm760 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Account;
    private DbsField pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Amt;
    private DbsField pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Typ;
    private DbsField pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Rtb_Amt;
    private DbsField pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Rtb_Typ;
    private DbsField pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Amt;
    private DbsField pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Typ;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Account = parameters.newFieldInRecord("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Account", "#TIAA-HEADER-AMT.#TIAA-HEADER-ACCOUNT", 
            FieldType.STRING, 4);
        pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Amt = parameters.newFieldInRecord("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Amt", "#TIAA-HEADER-AMT.#TIAA-HEADER-GRD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Typ = parameters.newFieldInRecord("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Typ", "#TIAA-HEADER-AMT.#TIAA-HEADER-GRD-TYP", 
            FieldType.STRING, 1);
        pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Rtb_Amt = parameters.newFieldInRecord("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Rtb_Amt", "#TIAA-HEADER-AMT.#TIAA-HEADER-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Rtb_Typ = parameters.newFieldInRecord("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Rtb_Typ", "#TIAA-HEADER-AMT.#TIAA-HEADER-RTB-TYP", 
            FieldType.STRING, 1);
        pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Amt = parameters.newFieldInRecord("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Amt", "#TIAA-HEADER-AMT.#TIAA-HEADER-SETTLE-AMT", 
            FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Typ = parameters.newFieldInRecord("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Typ", "#TIAA-HEADER-AMT.#TIAA-HEADER-SETTLE-TYP", 
            FieldType.STRING, 1);
        parameters.reset();
    }

    public Adsm760() throws Exception
    {
        super("Adsm760");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm760", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm760"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Account", pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Account, true, 2, 1, 4, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Amt", pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Amt, true, 2, 14, 12, "WHITE", 
                "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Typ", pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Settle_Typ, true, 2, 27, 1, "WHITE", 
                true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Rtb_Amt", pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Rtb_Amt, true, 2, 29, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Rtb_Typ", pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Rtb_Typ, true, 2, 44, 1, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Amt", pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Amt, true, 2, 46, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Typ", pnd_Tiaa_Header_Amt_Pnd_Tiaa_Header_Grd_Typ, true, 2, 61, 1, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
