/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:59 PM
**        *   FROM NATURAL MAP   :  Adsm774
************************************************************
**        * FILE NAME               : Adsm774.java
**        * CLASS NAME              : Adsm774
**        * INSTANCE NAME           : Adsm774
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #TPA-IO-LIT-774
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm774 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Tpa_Io_Lit_774;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Tpa_Io_Lit_774 = parameters.newFieldInRecord("pnd_Tpa_Io_Lit_774", "#TPA-IO-LIT-774", FieldType.STRING, 4);
        parameters.reset();
    }

    public Adsm774() throws Exception
    {
        super("Adsm774");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm774", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm774"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "ACCOUNT", "BLUE", 2, 1, 7);
            uiForm.setUiLabel("label_2", "SETTLEMENT", "BLUE", 2, 19, 10);
            uiForm.setUiLabel("label_3", "RTB", "BLUE", 2, 39, 3);
            uiForm.setUiLabel("label_4", "TIAA", "BLUE", 2, 53, 4);
            uiForm.setUiControl("pnd_Tpa_Io_Lit_774", pnd_Tpa_Io_Lit_774, true, 2, 59, 4, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
