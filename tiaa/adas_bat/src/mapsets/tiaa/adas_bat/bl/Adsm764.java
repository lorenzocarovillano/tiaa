/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:56 PM
**        *   FROM NATURAL MAP   :  Adsm764
************************************************************
**        * FILE NAME               : Adsm764.java
**        * CLASS NAME              : Adsm764
**        * INSTANCE NAME           : Adsm764
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #ASTERISK #COMMENT1 #COMMENT2                                                                                            *     #TIAA-SUB-TOTALS.#TIAA-DA-ACCUM-SUB-TOTAL 
    *     #TIAA-SUB-TOTALS.#TIAA-RTB-SUB-TOTAL                                                                                     *     #TIAA-SUB-TOTALS.#TIAA-SETTLED-RTB-SUB-TOTAL 
    *     #TIAA-SUB-TOTALS.#TIAA-SETTLED-SUB-TOTAL
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm764 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Asterisk;
    private DbsField pnd_Comment1;
    private DbsField pnd_Comment2;
    private DbsField pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Da_Accum_Sub_Total;
    private DbsField pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Rtb_Sub_Total;
    private DbsField pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Rtb_Sub_Total;
    private DbsField pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Sub_Total;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Asterisk = parameters.newFieldInRecord("pnd_Asterisk", "#ASTERISK", FieldType.STRING, 1);
        pnd_Comment1 = parameters.newFieldInRecord("pnd_Comment1", "#COMMENT1", FieldType.STRING, 79);
        pnd_Comment2 = parameters.newFieldInRecord("pnd_Comment2", "#COMMENT2", FieldType.STRING, 79);
        pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Da_Accum_Sub_Total = parameters.newFieldInRecord("pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Da_Accum_Sub_Total", "#TIAA-SUB-TOTALS.#TIAA-DA-ACCUM-SUB-TOTAL", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Rtb_Sub_Total = parameters.newFieldInRecord("pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Rtb_Sub_Total", "#TIAA-SUB-TOTALS.#TIAA-RTB-SUB-TOTAL", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Rtb_Sub_Total = parameters.newFieldInRecord("pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Rtb_Sub_Total", "#TIAA-SUB-TOTALS.#TIAA-SETTLED-RTB-SUB-TOTAL", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Sub_Total = parameters.newFieldInRecord("pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Sub_Total", "#TIAA-SUB-TOTALS.#TIAA-SETTLED-SUB-TOTAL", 
            FieldType.PACKED_DECIMAL, 15, 2);
        parameters.reset();
    }

    public Adsm764() throws Exception
    {
        super("Adsm764");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm764", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm764"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "-------------------------------------------------------------------------------", "", 2, 1, 79);
            uiForm.setUiLabel("label_2", "TOT", "BLUE", 3, 1, 3);
            uiForm.setUiControl("pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Da_Accum_Sub_Total", pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Da_Accum_Sub_Total, true, 3, 5, 18, "WHITE", 
                "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Asterisk", pnd_Asterisk, true, 3, 24, 1, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Sub_Total", pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Sub_Total, true, 3, 26, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Rtb_Sub_Total", pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Rtb_Sub_Total, true, 3, 43, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Rtb_Sub_Total", pnd_Tiaa_Sub_Totals_Pnd_Tiaa_Settled_Rtb_Sub_Total, true, 3, 62, 
                18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "----", "BLUE", 4, 1, 4);
            uiForm.setUiControl("pnd_Comment1", pnd_Comment1, true, 6, 1, 79, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Comment2", pnd_Comment2, true, 7, 1, 79, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
