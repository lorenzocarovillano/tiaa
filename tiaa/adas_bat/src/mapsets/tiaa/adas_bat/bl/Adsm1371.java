/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:45 PM
**        *   FROM NATURAL MAP   :  Adsm1371
************************************************************
**        * FILE NAME               : Adsm1371.java
**        * CLASS NAME              : Adsm1371
**        * INSTANCE NAME           : Adsm1371
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-DIVID-AMT(*)                                                                         *     #F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-DOLLARS(*) 
    *     #F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-FIN-DIV-AMT(*)                                                                       *     #F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-FIN-GTD-AMT(*) 
    *     #F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-GTD-AMT(*)                                                                           *     #F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-PAY-CNT(*) 
    *     #F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-UNITS(*)                                                                             *     #F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-DIV-AMT 
    *     #F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-DOLLARS                                                                      *     #F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-FIN-DIV-AMT 
    *     #F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-FIN-GTD-AMT                                                                  *     #F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-GTD-AMT 
    *     #F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-PAY-CNT                                                                      *     #F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-UNITS 
    *     #F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-DIV-AMT(*)                                                                           *     #F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-DOLLARS(*) 
    *     #F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-FIN-DIV-AMT(*)                                                                       *     #F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-FIN-GTD-AMT(*) 
    *     #F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-GTD-AMT(*)                                                                           *     #F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-PAY-CNT(*) 
    *     #F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-UNITS(*)                                                                             *     #F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-DIV-AMT 
    *     #F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-DOLLARS                                                                      *     #F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-FIN-DIV-AMT 
    *     #F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-FIN-GTD-AMT                                                                  *     #F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-GTD-AMT 
    *     #F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-PAY-CNT                                                                      *     #F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-UNITS 
    *     #F-IPRO-CASH-DIV-AMT #F-IPRO-CASH-FINAL-DIV-AMT                                                                          *     #F-IPRO-CASH-FINAL-GTD-AMT 
    #F-IPRO-CASH-GTD-AMT                                                                          *     #F-IPRO-CASH-PAY-CNT #F-IPRO-TOTAL-DIV-AMT      
    *     #F-IPRO-TOTAL-FINAL-DIV-AMT #F-IPRO-TOTAL-FINAL-GTD-AMT                                                                  *     #F-IPRO-TOTAL-GTD-AMT 
    #F-IPRO-TOTAL-PAY-CNT                                                                              *     #F-TIAA-MINOR-ACCUM.#F-IPRO-EXT-RLVR-DIV-AMT 
    *     #F-TIAA-MINOR-ACCUM.#F-IPRO-EXT-RLVR-FINAL-DIV-AMT                                                                       *     #F-TIAA-MINOR-ACCUM.#F-IPRO-EXT-RLVR-FINAL-GTD-AMT 
    *     #F-TIAA-MINOR-ACCUM.#F-IPRO-EXT-RLVR-GTD-AMT                                                                             *     #F-TIAA-MINOR-ACCUM.#F-IPRO-EXT-RLVR-PAY-CNT 
    *     #F-TIAA-MINOR-ACCUM.#F-IPRO-INT-RLVR-DIV-AMT                                                                             *     #F-TIAA-MINOR-ACCUM.#F-IPRO-INT-RLVR-FINAL-DIV-AMT 
    *     #F-TIAA-MINOR-ACCUM.#F-IPRO-INT-RLVR-FINAL-GTD-AMT                                                                       *     #F-TIAA-MINOR-ACCUM.#F-IPRO-INT-RLVR-GTD-AMT 
    *     #F-TIAA-MINOR-ACCUM.#F-IPRO-INT-RLVR-PAY-CNT                                                                             *     #F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-DIV-AMT 
    *     #F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-DOLLARS                                                                                  *     #F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-FIN-DIV-AMT 
    *     #F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-FIN-GTD-AMT                                                                              *     #F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-GTD-AMT 
    *     #F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-PAY-CNT                                                                                  *     #F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-UNITS 
    *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-CASH-DIV-AMT                                                                             *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-CASH-FINAL-DIV-AMT 
    *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-CASH-FINAL-GTD-AMT                                                                       *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-CASH-GTD-AMT 
    *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-CASH-PAY-CNT                                                                             *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-DOLLARS 
    *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-RLVR-DIV-AMT                                                                             *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-RLVR-FINAL-DIV-AMT 
    *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-RLVR-FINAL-GTD-AMT                                                                       *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-RLVR-GTD-AMT 
    *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-RLVR-PAY-CNT                                                                             *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-TOTAL-DIV-AMT 
    *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-TOTAL-FINAL-DIV-AMT                                                                      *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-TOTAL-FINAL-GTD-AMT 
    *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-TOTAL-GTD-AMT                                                                            *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-TOTAL-PAY-CNT 
    *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-UNITS                                                                                    *     #F-TIAA-SUB-ACCUM.#F-TIAA-SUB-DIVID-AMT 
    *     #F-TIAA-SUB-ACCUM.#F-TIAA-SUB-DOLLARS                                                                                    *     #F-TIAA-SUB-ACCUM.#F-TIAA-SUB-FIN-DIV-AMT 
    *     #F-TIAA-SUB-ACCUM.#F-TIAA-SUB-FIN-GTD-AMT                                                                                *     #F-TIAA-SUB-ACCUM.#F-TIAA-SUB-GTD-AMT 
    *     #F-TIAA-SUB-ACCUM.#F-TIAA-SUB-PAY-CNT                                                                                    *     #F-TIAA-SUB-ACCUM.#F-TIAA-SUB-UNITS 
    *     #F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-DIV-AMT                                                                                *     #F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-DOLLARS 
    *     #F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-FIN-DIV-AMT                                                                            *     #F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-FIN-GTD-AMT 
    *     #F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-GTD-AMT                                                                                *     #F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-PAY-CNT 
    *     #F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-UNITS #F-TPA-CASH-DIV-AMT                                                              *     #F-TPA-CASH-FINAL-DIV-AMT 
    #F-TPA-CASH-FINAL-GTD-AMT                                                                      *     #F-TPA-CASH-GTD-AMT #F-TPA-CASH-PAY-CNT #F-TPA-EXTR-DIV-AMT 
    *     #F-TPA-EXTR-FINAL-DIV-AMT #F-TPA-EXTR-FINAL-GTD-AMT                                                                      *     #F-TPA-EXTR-GTD-AMT 
    #F-TPA-EXTR-PAY-CNT #F-TPA-INTR-RLVR-DIV-AMT                                                         *     #F-TPA-INTR-RLVR-FINAL-DIV-AMT #F-TPA-INTR-RLVR-FINAL-GTD-AMT 
    *     #F-TPA-INTR-RLVR-GTD-AMT #F-TPA-INTR-RLVR-PAY-CNT                                                                        *     #F-TPA-IVC-DIV-AMT 
    #F-TPA-IVC-FINAL-DIV-AMT                                                                              *     #F-TPA-IVC-FINAL-GTD-AMT #F-TPA-IVC-GTD-AMT 
    #F-TPA-IVC-PAY-CNT                                                           *     #F-TPA-RINV-DIV-AMT #F-TPA-RINV-FINAL-DIV-AMT                    
    *     #F-TPA-RINV-FINAL-GTD-AMT #F-TPA-RINV-GTD-AMT #F-TPA-RINV-PAY-CNT                                                        *     #F-TPA-TOTAL-DIV-AMT 
    #F-TPA-TOTAL-FINAL-DIV-AMT                                                                          *     #F-TPA-TOTAL-FINAL-GTD-AMT #F-TPA-TOTAL-GTD-AMT 
    *     #F-TPA-TOTAL-PAY-CNT
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm1371 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt;
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars;
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt;
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt;
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt;
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt;
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Div_Amt;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Dollars;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Div_Amt;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Gtd_Amt;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Gtd_Amt;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Pay_Cnt;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Units;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Div_Amt;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Dollars;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Div_Amt;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Gtd_Amt;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Gtd_Amt;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Pay_Cnt;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Units;
    private DbsField pnd_F_Ipro_Cash_Div_Amt;
    private DbsField pnd_F_Ipro_Cash_Final_Div_Amt;
    private DbsField pnd_F_Ipro_Cash_Final_Gtd_Amt;
    private DbsField pnd_F_Ipro_Cash_Gtd_Amt;
    private DbsField pnd_F_Ipro_Cash_Pay_Cnt;
    private DbsField pnd_F_Ipro_Total_Div_Amt;
    private DbsField pnd_F_Ipro_Total_Final_Div_Amt;
    private DbsField pnd_F_Ipro_Total_Final_Gtd_Amt;
    private DbsField pnd_F_Ipro_Total_Gtd_Amt;
    private DbsField pnd_F_Ipro_Total_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Final_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Final_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Final_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Final_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Dollars;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Units;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Final_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Final_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Dollars;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Final_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Final_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Final_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Final_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Units;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Divid_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Dollars;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Div_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Units;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Div_Amt;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Dollars;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Div_Amt;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Gtd_Amt;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Gtd_Amt;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Units;
    private DbsField pnd_F_Tpa_Cash_Div_Amt;
    private DbsField pnd_F_Tpa_Cash_Final_Div_Amt;
    private DbsField pnd_F_Tpa_Cash_Final_Gtd_Amt;
    private DbsField pnd_F_Tpa_Cash_Gtd_Amt;
    private DbsField pnd_F_Tpa_Cash_Pay_Cnt;
    private DbsField pnd_F_Tpa_Extr_Div_Amt;
    private DbsField pnd_F_Tpa_Extr_Final_Div_Amt;
    private DbsField pnd_F_Tpa_Extr_Final_Gtd_Amt;
    private DbsField pnd_F_Tpa_Extr_Gtd_Amt;
    private DbsField pnd_F_Tpa_Extr_Pay_Cnt;
    private DbsField pnd_F_Tpa_Intr_Rlvr_Div_Amt;
    private DbsField pnd_F_Tpa_Intr_Rlvr_Final_Div_Amt;
    private DbsField pnd_F_Tpa_Intr_Rlvr_Final_Gtd_Amt;
    private DbsField pnd_F_Tpa_Intr_Rlvr_Gtd_Amt;
    private DbsField pnd_F_Tpa_Intr_Rlvr_Pay_Cnt;
    private DbsField pnd_F_Tpa_Ivc_Div_Amt;
    private DbsField pnd_F_Tpa_Ivc_Final_Div_Amt;
    private DbsField pnd_F_Tpa_Ivc_Final_Gtd_Amt;
    private DbsField pnd_F_Tpa_Ivc_Gtd_Amt;
    private DbsField pnd_F_Tpa_Ivc_Pay_Cnt;
    private DbsField pnd_F_Tpa_Rinv_Div_Amt;
    private DbsField pnd_F_Tpa_Rinv_Final_Div_Amt;
    private DbsField pnd_F_Tpa_Rinv_Final_Gtd_Amt;
    private DbsField pnd_F_Tpa_Rinv_Gtd_Amt;
    private DbsField pnd_F_Tpa_Rinv_Pay_Cnt;
    private DbsField pnd_F_Tpa_Total_Div_Amt;
    private DbsField pnd_F_Tpa_Total_Final_Div_Amt;
    private DbsField pnd_F_Tpa_Total_Final_Gtd_Amt;
    private DbsField pnd_F_Tpa_Total_Gtd_Amt;
    private DbsField pnd_F_Tpa_Total_Pay_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt = parameters.newFieldArrayInRecord("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt", 
            "#F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-DIVID-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 20));
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars = parameters.newFieldArrayInRecord("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars", "#F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-DOLLARS", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 20));
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt = parameters.newFieldArrayInRecord("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt", 
            "#F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 20));
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt", 
            "#F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 20));
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt", "#F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 20));
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt = parameters.newFieldArrayInRecord("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt", "#F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units = parameters.newFieldArrayInRecord("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units", "#F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-UNITS", 
            FieldType.PACKED_DECIMAL, 14, 4, new DbsArrayController(1, 20));
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Div_Amt = parameters.newFieldInRecord("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Div_Amt", 
            "#F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Dollars = parameters.newFieldInRecord("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Dollars", 
            "#F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Div_Amt = parameters.newFieldInRecord("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Div_Amt", 
            "#F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Gtd_Amt", 
            "#F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Gtd_Amt", 
            "#F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Pay_Cnt", 
            "#F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Units = parameters.newFieldInRecord("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Units", 
            "#F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-UNITS", FieldType.PACKED_DECIMAL, 14, 4);
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt = parameters.newFieldArrayInRecord("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt", "#F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 20));
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars = parameters.newFieldArrayInRecord("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars", "#F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-DOLLARS", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 20));
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt = parameters.newFieldArrayInRecord("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt", 
            "#F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 20));
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt", 
            "#F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 20));
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt", "#F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 20));
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt = parameters.newFieldArrayInRecord("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt", "#F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units = parameters.newFieldArrayInRecord("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units", "#F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-UNITS", 
            FieldType.PACKED_DECIMAL, 14, 4, new DbsArrayController(1, 20));
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Div_Amt = parameters.newFieldInRecord("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Div_Amt", 
            "#F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Dollars = parameters.newFieldInRecord("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Dollars", 
            "#F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Div_Amt = parameters.newFieldInRecord("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Div_Amt", 
            "#F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Gtd_Amt", 
            "#F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Gtd_Amt", 
            "#F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Pay_Cnt", 
            "#F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Units = parameters.newFieldInRecord("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Units", 
            "#F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-UNITS", FieldType.PACKED_DECIMAL, 14, 4);
        pnd_F_Ipro_Cash_Div_Amt = parameters.newFieldInRecord("pnd_F_Ipro_Cash_Div_Amt", "#F-IPRO-CASH-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Ipro_Cash_Final_Div_Amt = parameters.newFieldInRecord("pnd_F_Ipro_Cash_Final_Div_Amt", "#F-IPRO-CASH-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_F_Ipro_Cash_Final_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Ipro_Cash_Final_Gtd_Amt", "#F-IPRO-CASH-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_F_Ipro_Cash_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Ipro_Cash_Gtd_Amt", "#F-IPRO-CASH-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Ipro_Cash_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Ipro_Cash_Pay_Cnt", "#F-IPRO-CASH-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Ipro_Total_Div_Amt = parameters.newFieldInRecord("pnd_F_Ipro_Total_Div_Amt", "#F-IPRO-TOTAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Ipro_Total_Final_Div_Amt = parameters.newFieldInRecord("pnd_F_Ipro_Total_Final_Div_Amt", "#F-IPRO-TOTAL-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_F_Ipro_Total_Final_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Ipro_Total_Final_Gtd_Amt", "#F-IPRO-TOTAL-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_F_Ipro_Total_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Ipro_Total_Gtd_Amt", "#F-IPRO-TOTAL-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Ipro_Total_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Ipro_Total_Pay_Cnt", "#F-IPRO-TOTAL-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Div_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Div_Amt", "#F-TIAA-MINOR-ACCUM.#F-IPRO-EXT-RLVR-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Final_Div_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Final_Div_Amt", 
            "#F-TIAA-MINOR-ACCUM.#F-IPRO-EXT-RLVR-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Final_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Final_Gtd_Amt", 
            "#F-TIAA-MINOR-ACCUM.#F-IPRO-EXT-RLVR-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Gtd_Amt", "#F-TIAA-MINOR-ACCUM.#F-IPRO-EXT-RLVR-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Pay_Cnt", "#F-TIAA-MINOR-ACCUM.#F-IPRO-EXT-RLVR-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Div_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Div_Amt", "#F-TIAA-MINOR-ACCUM.#F-IPRO-INT-RLVR-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Final_Div_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Final_Div_Amt", 
            "#F-TIAA-MINOR-ACCUM.#F-IPRO-INT-RLVR-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Final_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Final_Gtd_Amt", 
            "#F-TIAA-MINOR-ACCUM.#F-IPRO-INT-RLVR-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Gtd_Amt", "#F-TIAA-MINOR-ACCUM.#F-IPRO-INT-RLVR-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Pay_Cnt", "#F-TIAA-MINOR-ACCUM.#F-IPRO-INT-RLVR-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Div_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Div_Amt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Dollars = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Dollars", "#F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Div_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Div_Amt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Gtd_Amt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Gtd_Amt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Pay_Cnt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Units = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Units", "#F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-UNITS", 
            FieldType.PACKED_DECIMAL, 14, 4);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Div_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Div_Amt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-CASH-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Final_Div_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Final_Div_Amt", 
            "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-CASH-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Final_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Final_Gtd_Amt", 
            "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-CASH-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Gtd_Amt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-CASH-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Pay_Cnt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-CASH-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Dollars = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Dollars", "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Div_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Div_Amt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-RLVR-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Final_Div_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Final_Div_Amt", 
            "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-RLVR-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Final_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Final_Gtd_Amt", 
            "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-RLVR-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Gtd_Amt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-RLVR-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Pay_Cnt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-RLVR-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Div_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Div_Amt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-TOTAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Final_Div_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Final_Div_Amt", 
            "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-TOTAL-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Final_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Final_Gtd_Amt", 
            "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-TOTAL-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Gtd_Amt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-TOTAL-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Pay_Cnt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-TOTAL-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Units = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Units", "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-UNITS", 
            FieldType.PACKED_DECIMAL, 14, 4);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Divid_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Divid_Amt", "#F-TIAA-SUB-ACCUM.#F-TIAA-SUB-DIVID-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Dollars = parameters.newFieldInRecord("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Dollars", "#F-TIAA-SUB-ACCUM.#F-TIAA-SUB-DOLLARS", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Div_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Div_Amt", "#F-TIAA-SUB-ACCUM.#F-TIAA-SUB-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Gtd_Amt", "#F-TIAA-SUB-ACCUM.#F-TIAA-SUB-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Gtd_Amt", "#F-TIAA-SUB-ACCUM.#F-TIAA-SUB-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Pay_Cnt", "#F-TIAA-SUB-ACCUM.#F-TIAA-SUB-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Units = parameters.newFieldInRecord("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Units", "#F-TIAA-SUB-ACCUM.#F-TIAA-SUB-UNITS", 
            FieldType.PACKED_DECIMAL, 14, 4);
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Div_Amt = parameters.newFieldInRecord("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Div_Amt", "#F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Dollars = parameters.newFieldInRecord("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Dollars", "#F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-DOLLARS", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Div_Amt = parameters.newFieldInRecord("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Div_Amt", "#F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Gtd_Amt", "#F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Gtd_Amt", "#F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt", "#F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 10);
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Units = parameters.newFieldInRecord("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Units", "#F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-UNITS", 
            FieldType.PACKED_DECIMAL, 14, 4);
        pnd_F_Tpa_Cash_Div_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Cash_Div_Amt", "#F-TPA-CASH-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tpa_Cash_Final_Div_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Cash_Final_Div_Amt", "#F-TPA-CASH-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_F_Tpa_Cash_Final_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Cash_Final_Gtd_Amt", "#F-TPA-CASH-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_F_Tpa_Cash_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Cash_Gtd_Amt", "#F-TPA-CASH-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tpa_Cash_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Tpa_Cash_Pay_Cnt", "#F-TPA-CASH-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tpa_Extr_Div_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Extr_Div_Amt", "#F-TPA-EXTR-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tpa_Extr_Final_Div_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Extr_Final_Div_Amt", "#F-TPA-EXTR-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_F_Tpa_Extr_Final_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Extr_Final_Gtd_Amt", "#F-TPA-EXTR-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_F_Tpa_Extr_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Extr_Gtd_Amt", "#F-TPA-EXTR-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tpa_Extr_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Tpa_Extr_Pay_Cnt", "#F-TPA-EXTR-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tpa_Intr_Rlvr_Div_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Intr_Rlvr_Div_Amt", "#F-TPA-INTR-RLVR-DIV-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_F_Tpa_Intr_Rlvr_Final_Div_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Intr_Rlvr_Final_Div_Amt", "#F-TPA-INTR-RLVR-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_F_Tpa_Intr_Rlvr_Final_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Intr_Rlvr_Final_Gtd_Amt", "#F-TPA-INTR-RLVR-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_F_Tpa_Intr_Rlvr_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Intr_Rlvr_Gtd_Amt", "#F-TPA-INTR-RLVR-GTD-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_F_Tpa_Intr_Rlvr_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Tpa_Intr_Rlvr_Pay_Cnt", "#F-TPA-INTR-RLVR-PAY-CNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_F_Tpa_Ivc_Div_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Ivc_Div_Amt", "#F-TPA-IVC-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tpa_Ivc_Final_Div_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Ivc_Final_Div_Amt", "#F-TPA-IVC-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_F_Tpa_Ivc_Final_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Ivc_Final_Gtd_Amt", "#F-TPA-IVC-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_F_Tpa_Ivc_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Ivc_Gtd_Amt", "#F-TPA-IVC-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tpa_Ivc_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Tpa_Ivc_Pay_Cnt", "#F-TPA-IVC-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tpa_Rinv_Div_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Rinv_Div_Amt", "#F-TPA-RINV-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tpa_Rinv_Final_Div_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Rinv_Final_Div_Amt", "#F-TPA-RINV-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_F_Tpa_Rinv_Final_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Rinv_Final_Gtd_Amt", "#F-TPA-RINV-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_F_Tpa_Rinv_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Rinv_Gtd_Amt", "#F-TPA-RINV-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tpa_Rinv_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Tpa_Rinv_Pay_Cnt", "#F-TPA-RINV-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tpa_Total_Div_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Total_Div_Amt", "#F-TPA-TOTAL-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tpa_Total_Final_Div_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Total_Final_Div_Amt", "#F-TPA-TOTAL-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_F_Tpa_Total_Final_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Total_Final_Gtd_Amt", "#F-TPA-TOTAL-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_F_Tpa_Total_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Total_Gtd_Amt", "#F-TPA-TOTAL-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tpa_Total_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Tpa_Total_Pay_Cnt", "#F-TPA-TOTAL-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        parameters.reset();
    }

    public Adsm1371() throws Exception
    {
        super("Adsm1371");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm1371", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm1371"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "ADSP1370", "BLUE", 1, 1, 8);
            uiForm.setUiLabel("label_2", "GRAND TOTALS", "BLUE", 2, 1, 12);
            uiForm.setUiLabel("label_3", "ALL MODES WITHIN INSTALLMENT-DATE", "BLUE", 3, 1, 33);
            uiForm.setUiLabel("label_4", "PAYMENT", "BLUE", 5, 22, 7);
            uiForm.setUiLabel("label_5", "CREF", "BLUE", 5, 39, 4);
            uiForm.setUiLabel("label_6", "CREF", "BLUE", 5, 55, 4);
            uiForm.setUiLabel("label_7", "TIAA", "BLUE", 5, 72, 4);
            uiForm.setUiLabel("label_8", "TIAA", "BLUE", 5, 89, 4);
            uiForm.setUiLabel("label_9", "FINAL", "BLUE", 5, 107, 5);
            uiForm.setUiLabel("label_10", "FINAL", "BLUE", 5, 124, 5);
            uiForm.setUiLabel("label_11", "Product", "BLUE", 6, 1, 7);
            uiForm.setUiLabel("label_12", "COUNTS", "BLUE", 6, 23, 6);
            uiForm.setUiLabel("label_13", "UNITS", "BLUE", 6, 39, 5);
            uiForm.setUiLabel("label_14", "DOLLARS", "BLUE", 6, 54, 7);
            uiForm.setUiLabel("label_15", "GTD/AMT", "BLUE", 6, 71, 7);
            uiForm.setUiLabel("label_16", "DIVID/AMT", "BLUE", 6, 87, 9);
            uiForm.setUiLabel("label_17", "GTD/AMT", "BLUE", 6, 106, 7);
            uiForm.setUiLabel("label_18", "DIVID/AMT", "BLUE", 6, 123, 9);
            uiForm.setUiLabel("label_19", "TIAA STD CASH", "BLUE", 7, 1, 13);
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Pay_Cnt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Pay_Cnt, true, 7, 18, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Units", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Units, true, 7, 28, 16, "WHITE", 
                "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Dollars", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Dollars, true, 7, 45, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Gtd_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Gtd_Amt, true, 7, 62, 
                16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Div_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Div_Amt, true, 7, 80, 
                16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Final_Gtd_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Final_Gtd_Amt, 
                true, 7, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Final_Div_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Cash_Final_Div_Amt, 
                true, 7, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_20", "TIAA STD INT ROL", "BLUE", 8, 1, 16);
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Pay_Cnt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Pay_Cnt, true, 8, 18, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Gtd_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Gtd_Amt, true, 8, 62, 
                16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Div_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Div_Amt, true, 8, 80, 
                16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Final_Gtd_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Final_Gtd_Amt, 
                true, 8, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Final_Div_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Rlvr_Final_Div_Amt, 
                true, 8, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_21", "TOTAL STD", "BLUE", 9, 1, 9);
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Pay_Cnt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Pay_Cnt, true, 9, 18, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Gtd_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Gtd_Amt, true, 9, 62, 
                16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Div_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Div_Amt, true, 9, 80, 
                16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Final_Gtd_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Final_Gtd_Amt, 
                true, 9, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Final_Div_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Total_Final_Div_Amt, 
                true, 9, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_22", "TIAA GRADED", "BLUE", 11, 1, 11);
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Pay_Cnt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Pay_Cnt, true, 11, 18, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Units", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Units, true, 11, 28, 16, "WHITE", 
                "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Dollars", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Dollars, true, 11, 45, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Gtd_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Gtd_Amt, true, 11, 62, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Div_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Div_Amt, true, 11, 80, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Gtd_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Gtd_Amt, true, 11, 97, 
                16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Div_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Div_Amt, true, 11, 115, 
                16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_23", "TIAA IPRO", "BLUE", 13, 1, 9);
            uiForm.setUiLabel("label_24", "CASH", "BLUE", 14, 6, 4);
            uiForm.setUiControl("pnd_F_Ipro_Cash_Pay_Cnt", pnd_F_Ipro_Cash_Pay_Cnt, true, 14, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Ipro_Cash_Gtd_Amt", pnd_F_Ipro_Cash_Gtd_Amt, true, 14, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Ipro_Cash_Div_Amt", pnd_F_Ipro_Cash_Div_Amt, true, 14, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Ipro_Cash_Final_Gtd_Amt", pnd_F_Ipro_Cash_Final_Gtd_Amt, true, 14, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Ipro_Cash_Final_Div_Amt", pnd_F_Ipro_Cash_Final_Div_Amt, true, 14, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, 
                true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_25", "EXT ROLLOVER", "BLUE", 15, 2, 12);
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Pay_Cnt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Pay_Cnt, true, 15, 18, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Gtd_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Gtd_Amt, true, 15, 62, 
                16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Div_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Div_Amt, true, 15, 80, 
                16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Final_Gtd_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Final_Gtd_Amt, 
                true, 15, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Final_Div_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Ext_Rlvr_Final_Div_Amt, 
                true, 15, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_26", "INT ROLLOVER", "BLUE", 16, 2, 12);
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Pay_Cnt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Pay_Cnt, true, 16, 18, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Gtd_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Gtd_Amt, true, 16, 62, 
                16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Div_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Div_Amt, true, 16, 80, 
                16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Final_Gtd_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Final_Gtd_Amt, 
                true, 16, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Final_Div_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Ipro_Int_Rlvr_Final_Div_Amt, 
                true, 16, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_27", "TOTAL IPRO", "BLUE", 17, 1, 10);
            uiForm.setUiControl("pnd_F_Ipro_Total_Pay_Cnt", pnd_F_Ipro_Total_Pay_Cnt, true, 17, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Ipro_Total_Gtd_Amt", pnd_F_Ipro_Total_Gtd_Amt, true, 17, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, 
                "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Ipro_Total_Div_Amt", pnd_F_Ipro_Total_Div_Amt, true, 17, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, 
                "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Ipro_Total_Final_Gtd_Amt", pnd_F_Ipro_Total_Final_Gtd_Amt, true, 17, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, 
                true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Ipro_Total_Final_Div_Amt", pnd_F_Ipro_Total_Final_Div_Amt, true, 17, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, 
                true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_28", "TOTAL", "BLUE", 19, 1, 5);
            uiForm.setUiControl("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Pay_Cnt", pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Pay_Cnt, true, 19, 18, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Units", pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Units, true, 19, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Dollars", pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Dollars, true, 19, 45, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Gtd_Amt", pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Gtd_Amt, true, 19, 62, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Divid_Amt", pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Divid_Amt, true, 19, 80, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Gtd_Amt", pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Gtd_Amt, true, 19, 97, 16, 
                "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Div_Amt", pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Div_Amt, true, 19, 115, 16, 
                "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_29", "T.P.A", "BLUE", 21, 1, 5);
            uiForm.setUiLabel("label_30", "INT-ROLLOVER", "BLUE", 22, 3, 12);
            uiForm.setUiControl("pnd_F_Tpa_Intr_Rlvr_Pay_Cnt", pnd_F_Tpa_Intr_Rlvr_Pay_Cnt, true, 22, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Intr_Rlvr_Gtd_Amt", pnd_F_Tpa_Intr_Rlvr_Gtd_Amt, true, 22, 62, 17, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99-", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Intr_Rlvr_Div_Amt", pnd_F_Tpa_Intr_Rlvr_Div_Amt, true, 22, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Intr_Rlvr_Final_Gtd_Amt", pnd_F_Tpa_Intr_Rlvr_Final_Gtd_Amt, true, 22, 97, 17, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99-", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Intr_Rlvr_Final_Div_Amt", pnd_F_Tpa_Intr_Rlvr_Final_Div_Amt, true, 22, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_31", "IVC", "BLUE", 23, 3, 3);
            uiForm.setUiControl("pnd_F_Tpa_Ivc_Pay_Cnt", pnd_F_Tpa_Ivc_Pay_Cnt, true, 23, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Ivc_Gtd_Amt", pnd_F_Tpa_Ivc_Gtd_Amt, true, 23, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Ivc_Div_Amt", pnd_F_Tpa_Ivc_Div_Amt, true, 23, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Ivc_Final_Gtd_Amt", pnd_F_Tpa_Ivc_Final_Gtd_Amt, true, 23, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Ivc_Final_Div_Amt", pnd_F_Tpa_Ivc_Final_Div_Amt, true, 23, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_32", "REINVESTMENT", "BLUE", 24, 3, 12);
            uiForm.setUiControl("pnd_F_Tpa_Rinv_Pay_Cnt", pnd_F_Tpa_Rinv_Pay_Cnt, true, 24, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Rinv_Gtd_Amt", pnd_F_Tpa_Rinv_Gtd_Amt, true, 24, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Rinv_Div_Amt", pnd_F_Tpa_Rinv_Div_Amt, true, 24, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Rinv_Final_Gtd_Amt", pnd_F_Tpa_Rinv_Final_Gtd_Amt, true, 24, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Rinv_Final_Div_Amt", pnd_F_Tpa_Rinv_Final_Div_Amt, true, 24, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_33", "CASH", "BLUE", 25, 3, 4);
            uiForm.setUiControl("pnd_F_Tpa_Cash_Pay_Cnt", pnd_F_Tpa_Cash_Pay_Cnt, true, 25, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Cash_Gtd_Amt", pnd_F_Tpa_Cash_Gtd_Amt, true, 25, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Cash_Div_Amt", pnd_F_Tpa_Cash_Div_Amt, true, 25, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Cash_Final_Gtd_Amt", pnd_F_Tpa_Cash_Final_Gtd_Amt, true, 25, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Cash_Final_Div_Amt", pnd_F_Tpa_Cash_Final_Div_Amt, true, 25, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_34", "EXT-TRF-RLVR", "BLUE", 26, 3, 12);
            uiForm.setUiControl("pnd_F_Tpa_Extr_Pay_Cnt", pnd_F_Tpa_Extr_Pay_Cnt, true, 26, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Extr_Gtd_Amt", pnd_F_Tpa_Extr_Gtd_Amt, true, 26, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Extr_Div_Amt", pnd_F_Tpa_Extr_Div_Amt, true, 26, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Extr_Final_Gtd_Amt", pnd_F_Tpa_Extr_Final_Gtd_Amt, true, 26, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Extr_Final_Div_Amt", pnd_F_Tpa_Extr_Final_Div_Amt, true, 26, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_35", "TOTAL TPA", "BLUE", 27, 1, 9);
            uiForm.setUiControl("pnd_F_Tpa_Total_Pay_Cnt", pnd_F_Tpa_Total_Pay_Cnt, true, 27, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Total_Gtd_Amt", pnd_F_Tpa_Total_Gtd_Amt, true, 27, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Total_Div_Amt", pnd_F_Tpa_Total_Div_Amt, true, 27, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Total_Final_Gtd_Amt", pnd_F_Tpa_Total_Final_Gtd_Amt, true, 27, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Total_Final_Div_Amt", pnd_F_Tpa_Total_Final_Div_Amt, true, 27, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, 
                true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_36", "TIAA ACCESS MTH", "BLUE", 29, 1, 15);
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt_12pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt.getValue(12+0), 
                true, 29, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units_12pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units.getValue(12+0), 
                true, 29, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars_12pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars.getValue(12+0), 
                true, 29, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt_12pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt.getValue(12+0), 
                true, 29, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt_12pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt.getValue(12+0), 
                true, 29, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt_12pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt.getValue(12+0), 
                true, 29, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt_12pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt.getValue(12+0), 
                true, 29, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_37", "TIAA ACCESS ANN", "BLUE", 30, 1, 15);
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt_12pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt.getValue(12+0), 
                true, 30, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units_12pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units.getValue(12+0), 
                true, 30, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars_12pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars.getValue(12+0), 
                true, 30, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt_12pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt.getValue(12+0), 
                true, 30, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt_12pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt.getValue(12+0), 
                true, 30, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt_12pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt.getValue(12+0), 
                true, 30, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt_12pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt.getValue(12+0), 
                true, 30, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_38", "TIAA REA MONTHLY", "BLUE", 31, 1, 16);
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt_2pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt.getValue(2+0), 
                true, 31, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units_2pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units.getValue(2+0), 
                true, 31, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars_2pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars.getValue(2+0), 
                true, 31, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt_2pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt.getValue(2+0), 
                true, 31, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt_2pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt.getValue(2+0), 
                true, 31, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt_2pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt.getValue(2+0), 
                true, 31, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt_2pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt.getValue(2+0), 
                true, 31, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_39", "TIAA REA ANNUAL", "BLUE", 32, 1, 15);
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt_2pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt.getValue(2+0), 
                true, 32, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units_2pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units.getValue(2+0), 
                true, 32, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars_2pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars.getValue(2+0), 
                true, 32, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt_2pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt.getValue(2+0), 
                true, 32, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt_2pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt.getValue(2+0), 
                true, 32, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt_2pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt.getValue(2+0), 
                true, 32, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt_2pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt.getValue(2+0), 
                true, 32, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_40", "STOCK", "BLUE", 34, 1, 5);
            uiForm.setUiLabel("label_41", "MONTHLY", "BLUE", 34, 8, 7);
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt_3pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt.getValue(3+0), 
                true, 34, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units_3pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units.getValue(3+0), 
                true, 34, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars_3pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars.getValue(3+0), 
                true, 34, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt_3pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt.getValue(3+0), 
                true, 34, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt_3pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt.getValue(3+0), 
                true, 34, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt_3pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt.getValue(3+0), 
                true, 34, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt_3pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt.getValue(3+0), 
                true, 34, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_42", "MMA", "BLUE", 35, 1, 3);
            uiForm.setUiLabel("label_43", "MONTHLY", "BLUE", 35, 8, 7);
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt_3pls1", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt.getValue(3+1), 
                true, 35, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units_3pls1", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units.getValue(3+1), 
                true, 35, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars_3pls1", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars.getValue(3+1), 
                true, 35, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt_3pls1", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt.getValue(3+1), 
                true, 35, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt_3pls1", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt.getValue(3+1), 
                true, 35, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt_3pls1", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt.getValue(3+1), 
                true, 35, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt_3pls1", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt.getValue(3+1), 
                true, 35, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_44", "SOCIAL MONTHLY", "BLUE", 36, 1, 14);
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt_3pls2", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt.getValue(3+2), 
                true, 36, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units_3pls2", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units.getValue(3+2), 
                true, 36, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars_3pls2", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars.getValue(3+2), 
                true, 36, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt_3pls2", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt.getValue(3+2), 
                true, 36, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt_3pls2", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt.getValue(3+2), 
                true, 36, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt_3pls2", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt.getValue(3+2), 
                true, 36, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt_3pls2", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt.getValue(3+2), 
                true, 36, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_45", "BOND", "BLUE", 37, 1, 4);
            uiForm.setUiLabel("label_46", "MONTHLY", "BLUE", 37, 8, 7);
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt_3pls3", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt.getValue(3+3), 
                true, 37, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units_3pls3", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units.getValue(3+3), 
                true, 37, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars_3pls3", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars.getValue(3+3), 
                true, 37, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt_3pls3", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt.getValue(3+3), 
                true, 37, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt_3pls3", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt.getValue(3+3), 
                true, 37, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt_3pls3", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt.getValue(3+3), 
                true, 37, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt_3pls3", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt.getValue(3+3), 
                true, 37, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_47", "GLOBAL MONTHLY", "BLUE", 38, 1, 14);
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt_3pls4", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt.getValue(3+4), 
                true, 38, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units_3pls4", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units.getValue(3+4), 
                true, 38, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars_3pls4", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars.getValue(3+4), 
                true, 38, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt_3pls4", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt.getValue(3+4), 
                true, 38, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt_3pls4", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt.getValue(3+4), 
                true, 38, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt_3pls4", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt.getValue(3+4), 
                true, 38, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt_3pls4", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt.getValue(3+4), 
                true, 38, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_48", "GROWTH MONTHLY", "BLUE", 39, 1, 14);
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt_3pls5", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt.getValue(3+5), 
                true, 39, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units_3pls5", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units.getValue(3+5), 
                true, 39, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars_3pls5", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars.getValue(3+5), 
                true, 39, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt_3pls5", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt.getValue(3+5), 
                true, 39, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt_3pls5", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt.getValue(3+5), 
                true, 39, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt_3pls5", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt.getValue(3+5), 
                true, 39, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt_3pls5", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt.getValue(3+5), 
                true, 39, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_49", "EQUITY MONTHLY", "BLUE", 40, 1, 14);
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt_3pls6", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt.getValue(3+6), 
                true, 40, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units_3pls6", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units.getValue(3+6), 
                true, 40, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars_3pls6", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars.getValue(3+6), 
                true, 40, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt_3pls6", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt.getValue(3+6), 
                true, 40, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt_3pls6", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt.getValue(3+6), 
                true, 40, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt_3pls6", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt.getValue(3+6), 
                true, 40, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt_3pls6", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt.getValue(3+6), 
                true, 40, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_50", "I L B", "BLUE", 41, 1, 5);
            uiForm.setUiLabel("label_51", "MONTHLY", "BLUE", 41, 8, 7);
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt_3pls7", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt.getValue(3+7), 
                true, 41, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units_3pls7", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units.getValue(3+7), 
                true, 41, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars_3pls7", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars.getValue(3+7), 
                true, 41, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt_3pls7", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt.getValue(3+7), 
                true, 41, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt_3pls7", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt.getValue(3+7), 
                true, 41, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt_3pls7", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt.getValue(3+7), 
                true, 41, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt_3pls7", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt.getValue(3+7), 
                true, 41, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_52", "TOTAL", "BLUE", 42, 1, 5);
            uiForm.setUiLabel("label_53", "MONTHLY", "BLUE", 42, 8, 7);
            uiForm.setUiControl("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Pay_Cnt", pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Pay_Cnt, 
                true, 42, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Units", pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Units, true, 
                42, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Dollars", pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Dollars, 
                true, 42, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Gtd_Amt", pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Gtd_Amt, 
                true, 42, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Div_Amt", pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Div_Amt, 
                true, 42, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Gtd_Amt", pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Gtd_Amt, 
                true, 42, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Div_Amt", pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Div_Amt, 
                true, 42, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_54", "STOCK", "BLUE", 44, 1, 5);
            uiForm.setUiLabel("label_55", "ANNUAL", "BLUE", 44, 8, 6);
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt_3pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt.getValue(3+0), 
                true, 44, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units_3pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units.getValue(3+0), 
                true, 44, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars_3pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars.getValue(3+0), 
                true, 44, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt_3pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt.getValue(3+0), 
                true, 44, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt_3pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt.getValue(3+0), 
                true, 44, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt_3pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt.getValue(3+0), 
                true, 44, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt_3pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt.getValue(3+0), 
                true, 44, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_56", "MMA", "BLUE", 45, 1, 3);
            uiForm.setUiLabel("label_57", "ANNUAL", "BLUE", 45, 8, 6);
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt_3pls1", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt.getValue(3+1), 
                true, 45, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units_3pls1", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units.getValue(3+1), 
                true, 45, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars_3pls1", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars.getValue(3+1), 
                true, 45, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt_3pls1", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt.getValue(3+1), 
                true, 45, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt_3pls1", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt.getValue(3+1), 
                true, 45, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt_3pls1", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt.getValue(3+1), 
                true, 45, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt_3pls1", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt.getValue(3+1), 
                true, 45, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_58", "SOCIAL ANNUAL", "BLUE", 46, 1, 13);
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt_3pls2", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt.getValue(3+2), 
                true, 46, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units_3pls2", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units.getValue(3+2), 
                true, 46, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars_3pls2", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars.getValue(3+2), 
                true, 46, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt_3pls2", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt.getValue(3+2), 
                true, 46, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt_3pls2", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt.getValue(3+2), 
                true, 46, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt_3pls2", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt.getValue(3+2), 
                true, 46, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt_3pls2", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt.getValue(3+2), 
                true, 46, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_59", "BOND", "BLUE", 47, 1, 4);
            uiForm.setUiLabel("label_60", "ANNUAL", "BLUE", 47, 8, 6);
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt_3pls3", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt.getValue(3+3), 
                true, 47, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units_3pls3", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units.getValue(3+3), 
                true, 47, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars_3pls3", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars.getValue(3+3), 
                true, 47, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt_3pls3", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt.getValue(3+3), 
                true, 47, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt_3pls3", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt.getValue(3+3), 
                true, 47, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt_3pls3", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt.getValue(3+3), 
                true, 47, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt_3pls3", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt.getValue(3+3), 
                true, 47, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_61", "GLOBAL ANNUAL", "BLUE", 48, 1, 13);
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt_3pls4", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt.getValue(3+4), 
                true, 48, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units_3pls4", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units.getValue(3+4), 
                true, 48, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars_3pls4", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars.getValue(3+4), 
                true, 48, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt_3pls4", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt.getValue(3+4), 
                true, 48, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt_3pls4", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt.getValue(3+4), 
                true, 48, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt_3pls4", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt.getValue(3+4), 
                true, 48, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt_3pls4", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt.getValue(3+4), 
                true, 48, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_62", "GROWTH ANNUAL", "BLUE", 49, 1, 13);
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt_3pls5", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt.getValue(3+5), 
                true, 49, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units_3pls5", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units.getValue(3+5), 
                true, 49, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars_3pls5", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars.getValue(3+5), 
                true, 49, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt_3pls5", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt.getValue(3+5), 
                true, 49, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt_3pls5", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt.getValue(3+5), 
                true, 49, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt_3pls5", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt.getValue(3+5), 
                true, 49, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt_3pls5", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt.getValue(3+5), 
                true, 49, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_63", "EQUITY ANNUAL", "BLUE", 50, 1, 13);
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt_3pls6", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt.getValue(3+6), 
                true, 50, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units_3pls6", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units.getValue(3+6), 
                true, 50, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars_3pls6", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars.getValue(3+6), 
                true, 50, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt_3pls6", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt.getValue(3+6), 
                true, 50, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt_3pls6", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt.getValue(3+6), 
                true, 50, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt_3pls6", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt.getValue(3+6), 
                true, 50, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt_3pls6", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt.getValue(3+6), 
                true, 50, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_64", "I L B", "BLUE", 51, 1, 5);
            uiForm.setUiLabel("label_65", "ANNUAL", "BLUE", 51, 8, 6);
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt_3pls7", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt.getValue(3+7), 
                true, 51, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units_3pls7", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units.getValue(3+7), 
                true, 51, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars_3pls7", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars.getValue(3+7), 
                true, 51, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt_3pls7", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt.getValue(3+7), 
                true, 51, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt_3pls7", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt.getValue(3+7), 
                true, 51, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt_3pls7", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt.getValue(3+7), 
                true, 51, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt_3pls7", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt.getValue(3+7), 
                true, 51, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_66", "TOTAL", "BLUE", 52, 1, 5);
            uiForm.setUiLabel("label_67", "ANNUAL", "BLUE", 52, 8, 6);
            uiForm.setUiControl("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Pay_Cnt", pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Pay_Cnt, 
                true, 52, 18, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Units", pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Units, true, 
                52, 28, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Dollars", pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Dollars, 
                true, 52, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Gtd_Amt", pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Gtd_Amt, 
                true, 52, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Div_Amt", pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Div_Amt, 
                true, 52, 80, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Gtd_Amt", pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Gtd_Amt, 
                true, 52, 97, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Div_Amt", pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Div_Amt, 
                true, 52, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_68", "TOTAL ALL", "BLUE", 55, 1, 9);
            uiForm.setUiControl("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt", pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt, true, 55, 14, 13, 
                "WHITE", "Z,ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Dollars", pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Dollars, true, 55, 43, 18, 
                "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Div_Amt", pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Div_Amt, true, 55, 78, 18, 
                "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Div_Amt", pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Div_Amt, true, 55, 
                113, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Units", pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Units, true, 56, 28, 16, "WHITE", 
                "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Gtd_Amt", pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Gtd_Amt, true, 56, 60, 18, 
                "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Gtd_Amt", pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Gtd_Amt, true, 56, 
                95, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
