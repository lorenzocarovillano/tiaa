/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:31 PM
**        *   FROM NATURAL MAP   :  Adsm1312
************************************************************
**        * FILE NAME               : Adsm1312.java
**        * CLASS NAME              : Adsm1312
**        * INSTANCE NAME           : Adsm1312
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #ACCOUNTING-DATE #HEADING
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm1312 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Accounting_Date;
    private DbsField pnd_Heading;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Accounting_Date = parameters.newFieldInRecord("pnd_Accounting_Date", "#ACCOUNTING-DATE", FieldType.NUMERIC, 8);
        pnd_Heading = parameters.newFieldInRecord("pnd_Heading", "#HEADING", FieldType.STRING, 50);
        parameters.reset();
    }

    public Adsm1312() throws Exception
    {
        super("Adsm1312");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm1312", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm1312"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "ADSP1311", "BLUE", 1, 1, 8);
            uiForm.setUiControl("pnd_Heading", pnd_Heading, true, 1, 43, 50, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "Run Date:", "BLUE", 1, 110, 9);
            uiForm.setUiControl("astDATX", Global.getDATX(), true, 1, 120, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "Daily Activity Report - Payment Summary", "BLUE", 2, 48, 39);
            uiForm.setUiLabel("label_4", "Page No :", "BLUE", 2, 110, 9);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 2, 120, 5, "WHITE", true, true, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_5", "MATURITIES", "BLUE", 3, 48, 10);
            uiForm.setUiLabel("label_6", "TO", "BLUE", 3, 60, 2);
            uiForm.setUiLabel("label_7", "CONTRACT ISSUE 'INTERFACE'", "BLUE", 3, 64, 26);
            uiForm.setUiLabel("label_8", "Accounting Date", "BLUE", 4, 53, 15);
            uiForm.setUiControl("pnd_Accounting_Date", pnd_Accounting_Date, true, 4, 70, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
