/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:39 PM
**        *   FROM NATURAL MAP   :  Adsm879
************************************************************
**        * FILE NAME               : Adsm879.java
**        * CLASS NAME              : Adsm879
**        * INSTANCE NAME           : Adsm879
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #CREF-FINAL-TOTAL.#CREF-DA-ACCUM-FINAL-TOTAL                                                                             *     #CREF-FINAL-TOTAL.#CREF-FINAL-DA-RATE 
    *     #CREF-FINAL-TOTAL.#CREF-FINAL-PROD                                                                                       *     #CREF-FINAL-TOTAL.#CREF-RTB-FINAL-TOTAL 
    *     #CREF-FINAL-TOTAL.#CREF-SETTLED-FINAL-TOTAL                                                                              *     #CREF-FINAL-TOTAL.#CREF-SETTLED-RTB-FINAL-TOTAL 
    *     #CREF-FINAL-UNITS.#CREF-DA-ACCUM-FINAL-UNITS                                                                             *     #CREF-FINAL-UNITS.#CREF-RTB-FINAL-UNITS 
    *     #CREF-FINAL-UNITS.#CREF-SETTLED-FINAL-UNITS                                                                              *     #CREF-FINAL-UNITS.#CREF-SETTLED-RTB-FINAL-UNITS
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm879 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Cref_Final_Total_Pnd_Cref_Da_Accum_Final_Total;
    private DbsField pnd_Cref_Final_Total_Pnd_Cref_Final_Da_Rate;
    private DbsField pnd_Cref_Final_Total_Pnd_Cref_Final_Prod;
    private DbsField pnd_Cref_Final_Total_Pnd_Cref_Rtb_Final_Total;
    private DbsField pnd_Cref_Final_Total_Pnd_Cref_Settled_Final_Total;
    private DbsField pnd_Cref_Final_Total_Pnd_Cref_Settled_Rtb_Final_Total;
    private DbsField pnd_Cref_Final_Units_Pnd_Cref_Da_Accum_Final_Units;
    private DbsField pnd_Cref_Final_Units_Pnd_Cref_Rtb_Final_Units;
    private DbsField pnd_Cref_Final_Units_Pnd_Cref_Settled_Final_Units;
    private DbsField pnd_Cref_Final_Units_Pnd_Cref_Settled_Rtb_Final_Units;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Cref_Final_Total_Pnd_Cref_Da_Accum_Final_Total = parameters.newFieldInRecord("pnd_Cref_Final_Total_Pnd_Cref_Da_Accum_Final_Total", "#CREF-FINAL-TOTAL.#CREF-DA-ACCUM-FINAL-TOTAL", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Cref_Final_Total_Pnd_Cref_Final_Da_Rate = parameters.newFieldInRecord("pnd_Cref_Final_Total_Pnd_Cref_Final_Da_Rate", "#CREF-FINAL-TOTAL.#CREF-FINAL-DA-RATE", 
            FieldType.STRING, 2);
        pnd_Cref_Final_Total_Pnd_Cref_Final_Prod = parameters.newFieldInRecord("pnd_Cref_Final_Total_Pnd_Cref_Final_Prod", "#CREF-FINAL-TOTAL.#CREF-FINAL-PROD", 
            FieldType.STRING, 1);
        pnd_Cref_Final_Total_Pnd_Cref_Rtb_Final_Total = parameters.newFieldInRecord("pnd_Cref_Final_Total_Pnd_Cref_Rtb_Final_Total", "#CREF-FINAL-TOTAL.#CREF-RTB-FINAL-TOTAL", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Cref_Final_Total_Pnd_Cref_Settled_Final_Total = parameters.newFieldInRecord("pnd_Cref_Final_Total_Pnd_Cref_Settled_Final_Total", "#CREF-FINAL-TOTAL.#CREF-SETTLED-FINAL-TOTAL", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Cref_Final_Total_Pnd_Cref_Settled_Rtb_Final_Total = parameters.newFieldInRecord("pnd_Cref_Final_Total_Pnd_Cref_Settled_Rtb_Final_Total", "#CREF-FINAL-TOTAL.#CREF-SETTLED-RTB-FINAL-TOTAL", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Cref_Final_Units_Pnd_Cref_Da_Accum_Final_Units = parameters.newFieldInRecord("pnd_Cref_Final_Units_Pnd_Cref_Da_Accum_Final_Units", "#CREF-FINAL-UNITS.#CREF-DA-ACCUM-FINAL-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 3);
        pnd_Cref_Final_Units_Pnd_Cref_Rtb_Final_Units = parameters.newFieldInRecord("pnd_Cref_Final_Units_Pnd_Cref_Rtb_Final_Units", "#CREF-FINAL-UNITS.#CREF-RTB-FINAL-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 3);
        pnd_Cref_Final_Units_Pnd_Cref_Settled_Final_Units = parameters.newFieldInRecord("pnd_Cref_Final_Units_Pnd_Cref_Settled_Final_Units", "#CREF-FINAL-UNITS.#CREF-SETTLED-FINAL-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 3);
        pnd_Cref_Final_Units_Pnd_Cref_Settled_Rtb_Final_Units = parameters.newFieldInRecord("pnd_Cref_Final_Units_Pnd_Cref_Settled_Rtb_Final_Units", "#CREF-FINAL-UNITS.#CREF-SETTLED-RTB-FINAL-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 3);
        parameters.reset();
    }

    public Adsm879() throws Exception
    {
        super("Adsm879");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=080 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm879", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm879"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Cref_Final_Total_Pnd_Cref_Final_Prod", pnd_Cref_Final_Total_Pnd_Cref_Final_Prod, true, 1, 6, 1, "WHITE", true, false, 
                null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Final_Total_Pnd_Cref_Final_Da_Rate", pnd_Cref_Final_Total_Pnd_Cref_Final_Da_Rate, true, 1, 17, 2, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Final_Total_Pnd_Cref_Da_Accum_Final_Total", pnd_Cref_Final_Total_Pnd_Cref_Da_Accum_Final_Total, true, 1, 21, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Final_Total_Pnd_Cref_Settled_Final_Total", pnd_Cref_Final_Total_Pnd_Cref_Settled_Final_Total, true, 1, 36, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Final_Total_Pnd_Cref_Rtb_Final_Total", pnd_Cref_Final_Total_Pnd_Cref_Rtb_Final_Total, true, 1, 51, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Final_Total_Pnd_Cref_Settled_Rtb_Final_Total", pnd_Cref_Final_Total_Pnd_Cref_Settled_Rtb_Final_Total, true, 
                1, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Final_Units_Pnd_Cref_Da_Accum_Final_Units", pnd_Cref_Final_Units_Pnd_Cref_Da_Accum_Final_Units, true, 2, 23, 
                13, "WHITE", "Z,ZZZ,ZZ9.999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Final_Units_Pnd_Cref_Settled_Final_Units", pnd_Cref_Final_Units_Pnd_Cref_Settled_Final_Units, true, 2, 38, 13, 
                "WHITE", "Z,ZZZ,ZZ9.999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Final_Units_Pnd_Cref_Rtb_Final_Units", pnd_Cref_Final_Units_Pnd_Cref_Rtb_Final_Units, true, 2, 53, 13, "WHITE", 
                "Z,ZZZ,ZZ9.999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Final_Units_Pnd_Cref_Settled_Rtb_Final_Units", pnd_Cref_Final_Units_Pnd_Cref_Settled_Rtb_Final_Units, true, 
                2, 67, 13, "WHITE", "Z,ZZZ,ZZ9.999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
