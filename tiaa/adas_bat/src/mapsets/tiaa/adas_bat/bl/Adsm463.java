/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:48 PM
**        *   FROM NATURAL MAP   :  Adsm463
************************************************************
**        * FILE NAME               : Adsm463.java
**        * CLASS NAME              : Adsm463
**        * INSTANCE NAME           : Adsm463
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #CREF-ANNUALLY-MINOR-ACCUM.#CREF-ANN-MAT-AMT(*)                                                                          *     #CREF-ANNUALLY-MINOR-ACCUM.#CREF-ANN-MAT-CNT(*) 
    *     #CREF-ANNUALLY-MINOR-ACCUM.#CREF-ANN-RTB-AMT(*)                                                                          *     #CREF-ANNUALLY-MINOR-ACCUM.#CREF-ANN-RTB-CNT(*) 
    *     #CREF-ANNUALLY-MINOR-ACCUM.#CREF-ANN-RTB-RLVR-AMT(*)                                                                     *     #CREF-ANNUALLY-MINOR-ACCUM.#CREF-ANN-RTB-RLVR-CNT(*) 
    *     #CREF-ANNUALLY-SUB-TOTAL-ACCUM.#CREF-SUB-ANN-MATURITY-AMT                                                                *     #CREF-ANNUALLY-SUB-TOTAL-ACCUM.#CREF-SUB-ANN-RTB-AMT 
    *     #CREF-ANNUALLY-SUB-TOTAL-ACCUM.#CREF-SUB-ANN-RTB-RLVR-AMT                                                                *     #CREF-ANNUALLY-SUB-TOTAL-ACCUM.#CREF-SUB-RATE-ANN-MATURITY-CNT 
    *     #CREF-ANNUALLY-SUB-TOTAL-ACCUM.#CREF-SUB-RATE-ANN-RTB-CNT                                                                *     #CREF-ANNUALLY-SUB-TOTAL-ACCUM.#CREF-SUB-RATE-ANN-RTB-RLVR-CNT 
    *     #CREF-MONTHLY-MINOR-ACCUM.#CREF-MTH-MAT-AMT(*)                                                                           *     #CREF-MONTHLY-MINOR-ACCUM.#CREF-MTH-MAT-CNT(*) 
    *     #CREF-MONTHLY-MINOR-ACCUM.#CREF-MTH-RTB-AMT(*)                                                                           *     #CREF-MONTHLY-MINOR-ACCUM.#CREF-MTH-RTB-CNT(*) 
    *     #CREF-MONTHLY-MINOR-ACCUM.#CREF-MTH-RTB-RLVR-AMT(*)                                                                      *     #CREF-MONTHLY-MINOR-ACCUM.#CREF-MTH-RTB-RLVR-CNT(*) 
    *     #CREF-MONTHLY-SUB-TOTAL-ACCUM.#CREF-SUB-MTH-MATURITY-AMT                                                                 *     #CREF-MONTHLY-SUB-TOTAL-ACCUM.#CREF-SUB-MTH-RTB-AMT 
    *     #CREF-MONTHLY-SUB-TOTAL-ACCUM.#CREF-SUB-MTH-RTB-RLVR-AMT                                                                 *     #CREF-MONTHLY-SUB-TOTAL-ACCUM.#CREF-SUB-RATE-MTH-MATURITY-CNT 
    *     #CREF-MONTHLY-SUB-TOTAL-ACCUM.#CREF-SUB-RATE-MTH-RTB-CNT                                                                 *     #CREF-MONTHLY-SUB-TOTAL-ACCUM.#CREF-SUB-RATE-MTH-RTB-RLVR-CNT 
    *     #EFFECTIVE-DATE #TIAA-IO-ACCM-AMT #TIAA-IO-ACCM-CNT                                                                      *     #TIAA-IO-RLVR-AMT 
    #TIAA-IO-RLVR-CNT                                                                                      *     #TIAA-MINOR-ACCUM.#TIAA-GRD-MATURITY-AMT 
    *     #TIAA-MINOR-ACCUM.#TIAA-RATE-GRD-MATURITY-CNT                                                                            *     #TIAA-MINOR-ACCUM.#TIAA-RATE-STD-MATURITY-CNT 
    *     #TIAA-MINOR-ACCUM.#TIAA-RATE-STD-RTB-CNT                                                                                 *     #TIAA-MINOR-ACCUM.#TIAA-RATE-STD-RTB-RLVR-CNT 
    *     #TIAA-MINOR-ACCUM.#TIAA-STBL-GRD-MATURITY-AMT                                                                            *     #TIAA-MINOR-ACCUM.#TIAA-STBL-GRD-MATURITY-CNT 
    *     #TIAA-MINOR-ACCUM.#TIAA-STBL-STD-MATURITY-AMT                                                                            *     #TIAA-MINOR-ACCUM.#TIAA-STBL-STD-MATURITY-CNT 
    *     #TIAA-MINOR-ACCUM.#TIAA-STBL-STD-RTB-AMT                                                                                 *     #TIAA-MINOR-ACCUM.#TIAA-STBL-STD-RTB-CNT 
    *     #TIAA-MINOR-ACCUM.#TIAA-STBL-STD-RTB-RLVR-AMT                                                                            *     #TIAA-MINOR-ACCUM.#TIAA-STBL-STD-RTB-RLVR-CNT 
    *     #TIAA-MINOR-ACCUM.#TIAA-STD-MATURITY-AMT                                                                                 *     #TIAA-MINOR-ACCUM.#TIAA-STD-RTB-AMT 
    *     #TIAA-MINOR-ACCUM.#TIAA-STD-RTB-RLVR-AMT                                                                                 *     #TIAA-SUB-ACCUM.#TIAA-SUB-MATURITY-AMT 
    *     #TIAA-SUB-ACCUM.#TIAA-SUB-RATE-MATURITY-CNT                                                                              *     #TIAA-SUB-ACCUM.#TIAA-SUB-RATE-RTB-CNT 
    *     #TIAA-SUB-ACCUM.#TIAA-SUB-RATE-RTB-RLVR-CNT                                                                              *     #TIAA-SUB-ACCUM.#TIAA-SUB-RTB-AMT 
    *     #TIAA-SUB-ACCUM.#TIAA-SUB-RTB-RLVR-AMT #TIAA-TPA-ACCM-AMT                                                                *     #TIAA-TPA-ACCM-CNT 
    #TIAA-TPA-RLVR-AMT #TIAA-TPA-RLVR-CNT                                                                 *     #TOTAL-CREF-AND-TIAA-ACCUM.#TOTAL-MATURITY-AMT 
    *     #TOTAL-CREF-AND-TIAA-ACCUM.#TOTAL-RATE-MATURITY-CNT                                                                      *     #TOTAL-CREF-AND-TIAA-ACCUM.#TOTAL-RATE-RTB-CNT 
    *     #TOTAL-CREF-AND-TIAA-ACCUM.#TOTAL-RATE-RTB-RLVR-CNT                                                                      *     #TOTAL-CREF-AND-TIAA-ACCUM.#TOTAL-RTB-AMT 
    *     #TOTAL-CREF-AND-TIAA-ACCUM.#TOTAL-RTB-RLVR-AMT
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm463 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt;
    private DbsField pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt;
    private DbsField pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt;
    private DbsField pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt;
    private DbsField pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt;
    private DbsField pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt;
    private DbsField pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Maturity_Amt;
    private DbsField pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Rtb_Amt;
    private DbsField pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Rtb_Rlvr_Amt;
    private DbsField pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Maturity_Cnt;
    private DbsField pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Rtb_Cnt;
    private DbsField pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Rtb_Rlvr_Cnt;
    private DbsField pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt;
    private DbsField pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt;
    private DbsField pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt;
    private DbsField pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt;
    private DbsField pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt;
    private DbsField pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt;
    private DbsField pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Maturity_Amt;
    private DbsField pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Rtb_Amt;
    private DbsField pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Rtb_Rlvr_Amt;
    private DbsField pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Maturity_Cnt;
    private DbsField pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Rtb_Cnt;
    private DbsField pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Rtb_Rlvr_Cnt;
    private DbsField pnd_Effective_Date;
    private DbsField pnd_Tiaa_Io_Accm_Amt;
    private DbsField pnd_Tiaa_Io_Accm_Cnt;
    private DbsField pnd_Tiaa_Io_Rlvr_Amt;
    private DbsField pnd_Tiaa_Io_Rlvr_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Grd_Maturity_Amt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Grd_Maturity_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Maturity_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Rtb_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Rtb_Rlvr_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Amt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Amt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Rtb_Amt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Rtb_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Rtb_Rlvr_Amt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Rtb_Rlvr_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Maturity_Amt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Rtb_Amt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Rtb_Rlvr_Amt;
    private DbsField pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt;
    private DbsField pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt;
    private DbsField pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Rtb_Cnt;
    private DbsField pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Rtb_Rlvr_Cnt;
    private DbsField pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rtb_Amt;
    private DbsField pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rtb_Rlvr_Amt;
    private DbsField pnd_Tiaa_Tpa_Accm_Amt;
    private DbsField pnd_Tiaa_Tpa_Accm_Cnt;
    private DbsField pnd_Tiaa_Tpa_Rlvr_Amt;
    private DbsField pnd_Tiaa_Tpa_Rlvr_Cnt;
    private DbsField pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt;
    private DbsField pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt;
    private DbsField pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Rtb_Cnt;
    private DbsField pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Rtb_Rlvr_Cnt;
    private DbsField pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rtb_Amt;
    private DbsField pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rtb_Rlvr_Amt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt = parameters.newFieldArrayInRecord("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt", "#CREF-ANNUALLY-MINOR-ACCUM.#CREF-ANN-MAT-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt = parameters.newFieldArrayInRecord("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt", "#CREF-ANNUALLY-MINOR-ACCUM.#CREF-ANN-MAT-CNT", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt = parameters.newFieldArrayInRecord("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt", "#CREF-ANNUALLY-MINOR-ACCUM.#CREF-ANN-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt = parameters.newFieldArrayInRecord("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt", "#CREF-ANNUALLY-MINOR-ACCUM.#CREF-ANN-RTB-CNT", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt = parameters.newFieldArrayInRecord("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt", 
            "#CREF-ANNUALLY-MINOR-ACCUM.#CREF-ANN-RTB-RLVR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt = parameters.newFieldArrayInRecord("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt", 
            "#CREF-ANNUALLY-MINOR-ACCUM.#CREF-ANN-RTB-RLVR-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Maturity_Amt = parameters.newFieldInRecord("pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Maturity_Amt", 
            "#CREF-ANNUALLY-SUB-TOTAL-ACCUM.#CREF-SUB-ANN-MATURITY-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Rtb_Amt = parameters.newFieldInRecord("pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Rtb_Amt", 
            "#CREF-ANNUALLY-SUB-TOTAL-ACCUM.#CREF-SUB-ANN-RTB-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Rtb_Rlvr_Amt = parameters.newFieldInRecord("pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Rtb_Rlvr_Amt", 
            "#CREF-ANNUALLY-SUB-TOTAL-ACCUM.#CREF-SUB-ANN-RTB-RLVR-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Maturity_Cnt = parameters.newFieldInRecord("pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Maturity_Cnt", 
            "#CREF-ANNUALLY-SUB-TOTAL-ACCUM.#CREF-SUB-RATE-ANN-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Rtb_Cnt = parameters.newFieldInRecord("pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Rtb_Cnt", 
            "#CREF-ANNUALLY-SUB-TOTAL-ACCUM.#CREF-SUB-RATE-ANN-RTB-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Rtb_Rlvr_Cnt = parameters.newFieldInRecord("pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Rtb_Rlvr_Cnt", 
            "#CREF-ANNUALLY-SUB-TOTAL-ACCUM.#CREF-SUB-RATE-ANN-RTB-RLVR-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt = parameters.newFieldArrayInRecord("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt", "#CREF-MONTHLY-MINOR-ACCUM.#CREF-MTH-MAT-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt = parameters.newFieldArrayInRecord("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt", "#CREF-MONTHLY-MINOR-ACCUM.#CREF-MTH-MAT-CNT", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt = parameters.newFieldArrayInRecord("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt", "#CREF-MONTHLY-MINOR-ACCUM.#CREF-MTH-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt = parameters.newFieldArrayInRecord("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt", "#CREF-MONTHLY-MINOR-ACCUM.#CREF-MTH-RTB-CNT", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt = parameters.newFieldArrayInRecord("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt", 
            "#CREF-MONTHLY-MINOR-ACCUM.#CREF-MTH-RTB-RLVR-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt = parameters.newFieldArrayInRecord("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt", 
            "#CREF-MONTHLY-MINOR-ACCUM.#CREF-MTH-RTB-RLVR-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Maturity_Amt = parameters.newFieldInRecord("pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Maturity_Amt", 
            "#CREF-MONTHLY-SUB-TOTAL-ACCUM.#CREF-SUB-MTH-MATURITY-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Rtb_Amt = parameters.newFieldInRecord("pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Rtb_Amt", 
            "#CREF-MONTHLY-SUB-TOTAL-ACCUM.#CREF-SUB-MTH-RTB-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Rtb_Rlvr_Amt = parameters.newFieldInRecord("pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Rtb_Rlvr_Amt", 
            "#CREF-MONTHLY-SUB-TOTAL-ACCUM.#CREF-SUB-MTH-RTB-RLVR-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Maturity_Cnt = parameters.newFieldInRecord("pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Maturity_Cnt", 
            "#CREF-MONTHLY-SUB-TOTAL-ACCUM.#CREF-SUB-RATE-MTH-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Rtb_Cnt = parameters.newFieldInRecord("pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Rtb_Cnt", 
            "#CREF-MONTHLY-SUB-TOTAL-ACCUM.#CREF-SUB-RATE-MTH-RTB-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Rtb_Rlvr_Cnt = parameters.newFieldInRecord("pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Rtb_Rlvr_Cnt", 
            "#CREF-MONTHLY-SUB-TOTAL-ACCUM.#CREF-SUB-RATE-MTH-RTB-RLVR-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Effective_Date = parameters.newFieldInRecord("pnd_Effective_Date", "#EFFECTIVE-DATE", FieldType.NUMERIC, 8);
        pnd_Tiaa_Io_Accm_Amt = parameters.newFieldInRecord("pnd_Tiaa_Io_Accm_Amt", "#TIAA-IO-ACCM-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Io_Accm_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Io_Accm_Cnt", "#TIAA-IO-ACCM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Io_Rlvr_Amt = parameters.newFieldInRecord("pnd_Tiaa_Io_Rlvr_Amt", "#TIAA-IO-RLVR-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Io_Rlvr_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Io_Rlvr_Cnt", "#TIAA-IO-RLVR-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Grd_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Grd_Maturity_Amt", "#TIAA-MINOR-ACCUM.#TIAA-GRD-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Grd_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Grd_Maturity_Cnt", "#TIAA-MINOR-ACCUM.#TIAA-RATE-GRD-MATURITY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Maturity_Cnt", "#TIAA-MINOR-ACCUM.#TIAA-RATE-STD-MATURITY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Rtb_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Rtb_Cnt", "#TIAA-MINOR-ACCUM.#TIAA-RATE-STD-RTB-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Rtb_Rlvr_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Rtb_Rlvr_Cnt", "#TIAA-MINOR-ACCUM.#TIAA-RATE-STD-RTB-RLVR-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Amt", "#TIAA-MINOR-ACCUM.#TIAA-STBL-GRD-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Cnt", "#TIAA-MINOR-ACCUM.#TIAA-STBL-GRD-MATURITY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Amt", "#TIAA-MINOR-ACCUM.#TIAA-STBL-STD-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Cnt", "#TIAA-MINOR-ACCUM.#TIAA-STBL-STD-MATURITY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Rtb_Amt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Rtb_Amt", "#TIAA-MINOR-ACCUM.#TIAA-STBL-STD-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Rtb_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Rtb_Cnt", "#TIAA-MINOR-ACCUM.#TIAA-STBL-STD-RTB-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Rtb_Rlvr_Amt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Rtb_Rlvr_Amt", "#TIAA-MINOR-ACCUM.#TIAA-STBL-STD-RTB-RLVR-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Rtb_Rlvr_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Rtb_Rlvr_Cnt", "#TIAA-MINOR-ACCUM.#TIAA-STBL-STD-RTB-RLVR-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Maturity_Amt", "#TIAA-MINOR-ACCUM.#TIAA-STD-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Rtb_Amt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Rtb_Amt", "#TIAA-MINOR-ACCUM.#TIAA-STD-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Rtb_Rlvr_Amt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Rtb_Rlvr_Amt", "#TIAA-MINOR-ACCUM.#TIAA-STD-RTB-RLVR-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt", "#TIAA-SUB-ACCUM.#TIAA-SUB-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt", "#TIAA-SUB-ACCUM.#TIAA-SUB-RATE-MATURITY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Rtb_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Rtb_Cnt", "#TIAA-SUB-ACCUM.#TIAA-SUB-RATE-RTB-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Rtb_Rlvr_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Rtb_Rlvr_Cnt", "#TIAA-SUB-ACCUM.#TIAA-SUB-RATE-RTB-RLVR-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rtb_Amt = parameters.newFieldInRecord("pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rtb_Amt", "#TIAA-SUB-ACCUM.#TIAA-SUB-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rtb_Rlvr_Amt = parameters.newFieldInRecord("pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rtb_Rlvr_Amt", "#TIAA-SUB-ACCUM.#TIAA-SUB-RTB-RLVR-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Tpa_Accm_Amt = parameters.newFieldInRecord("pnd_Tiaa_Tpa_Accm_Amt", "#TIAA-TPA-ACCM-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Tpa_Accm_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Tpa_Accm_Cnt", "#TIAA-TPA-ACCM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Tpa_Rlvr_Amt = parameters.newFieldInRecord("pnd_Tiaa_Tpa_Rlvr_Amt", "#TIAA-TPA-RLVR-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Tpa_Rlvr_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Tpa_Rlvr_Cnt", "#TIAA-TPA-RLVR-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt = parameters.newFieldInRecord("pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt", "#TOTAL-CREF-AND-TIAA-ACCUM.#TOTAL-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt = parameters.newFieldInRecord("pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt", 
            "#TOTAL-CREF-AND-TIAA-ACCUM.#TOTAL-RATE-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Rtb_Cnt = parameters.newFieldInRecord("pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Rtb_Cnt", "#TOTAL-CREF-AND-TIAA-ACCUM.#TOTAL-RATE-RTB-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Rtb_Rlvr_Cnt = parameters.newFieldInRecord("pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Rtb_Rlvr_Cnt", 
            "#TOTAL-CREF-AND-TIAA-ACCUM.#TOTAL-RATE-RTB-RLVR-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rtb_Amt = parameters.newFieldInRecord("pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rtb_Amt", "#TOTAL-CREF-AND-TIAA-ACCUM.#TOTAL-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rtb_Rlvr_Amt = parameters.newFieldInRecord("pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rtb_Rlvr_Amt", "#TOTAL-CREF-AND-TIAA-ACCUM.#TOTAL-RTB-RLVR-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        parameters.reset();
    }

    public Adsm463() throws Exception
    {
        super("Adsm463");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm463", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm463"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "Effective Date", "BLUE", 1, 1, 14);
            uiForm.setUiControl("pnd_Effective_Date", pnd_Effective_Date, true, 2, 2, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_2", "DA RATE", "BLUE", 3, 26, 7);
            uiForm.setUiLabel("label_3", "RTB", "BLUE", 3, 47, 3);
            uiForm.setUiLabel("label_4", "DA RATE", "BLUE", 3, 63, 7);
            uiForm.setUiLabel("label_5", "INTERNAL", "BLUE", 3, 79, 8);
            uiForm.setUiLabel("label_6", "DA RATE", "BLUE", 3, 101, 7);
            uiForm.setUiLabel("label_7", "MATURITY", "BLUE", 3, 122, 8);
            uiForm.setUiLabel("label_8", "Product", "BLUE", 4, 1, 7);
            uiForm.setUiLabel("label_9", "COUNTS", "BLUE", 4, 26, 6);
            uiForm.setUiLabel("label_10", "ACCUMULATION", "BLUE", 4, 43, 12);
            uiForm.setUiLabel("label_11", "COUNTS", "BLUE", 4, 64, 6);
            uiForm.setUiLabel("label_12", "ROLLOVER AMOUNT", "BLUE", 4, 76, 15);
            uiForm.setUiLabel("label_13", "COUNTS", "BLUE", 4, 102, 6);
            uiForm.setUiLabel("label_14", "ACCUMULATION", "BLUE", 4, 120, 12);
            uiForm.setUiLabel("label_15", "TIAA STANDARD", "BLUE", 5, 1, 13);
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Rtb_Cnt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Rtb_Cnt, true, 5, 24, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Rtb_Amt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Rtb_Amt, true, 5, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Rtb_Rlvr_Cnt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Rtb_Rlvr_Cnt, true, 5, 61, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Rtb_Rlvr_Amt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Rtb_Rlvr_Amt, true, 5, 77, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Maturity_Cnt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Maturity_Cnt, true, 5, 99, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Maturity_Amt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Maturity_Amt, true, 5, 118, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_16", "TIAA GRADED", "BLUE", 6, 1, 11);
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Grd_Maturity_Cnt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Grd_Maturity_Cnt, true, 6, 99, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Grd_Maturity_Amt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Grd_Maturity_Amt, true, 6, 118, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "TIAA IPRO", "BLUE", 7, 1, 9);
            uiForm.setUiControl("pnd_Tiaa_Io_Rlvr_Cnt", pnd_Tiaa_Io_Rlvr_Cnt, true, 7, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Io_Rlvr_Amt", pnd_Tiaa_Io_Rlvr_Amt, true, 7, 77, 14, "WHITE", "ZZZ,ZZZ,ZZZ.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Io_Accm_Cnt", pnd_Tiaa_Io_Accm_Cnt, true, 7, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Io_Accm_Amt", pnd_Tiaa_Io_Accm_Amt, true, 7, 118, 14, "WHITE", "ZZZ,ZZZ,ZZZ.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_18", "TIAA STABLE STANDARD", "BLUE", 8, 1, 20);
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Rtb_Cnt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Rtb_Cnt, true, 8, 24, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Rtb_Amt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Rtb_Amt, true, 8, 41, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Rtb_Rlvr_Cnt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Rtb_Rlvr_Cnt, true, 8, 61, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Rtb_Rlvr_Amt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Rtb_Rlvr_Amt, true, 8, 77, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Cnt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Cnt, true, 8, 99, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Amt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Amt, true, 8, 118, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", "TIAA STABLE GRADED", "BLUE", 9, 1, 18);
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Cnt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Cnt, true, 9, 99, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Amt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Amt, true, 9, 118, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_20", "TOTAL", "BLUE", 10, 1, 5);
            uiForm.setUiControl("pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Rtb_Cnt", pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Rtb_Cnt, true, 10, 24, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rtb_Amt", pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rtb_Amt, true, 10, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Rtb_Rlvr_Cnt", pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Rtb_Rlvr_Cnt, true, 10, 61, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rtb_Rlvr_Amt", pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rtb_Rlvr_Amt, true, 10, 73, 18, "WHITE", 
                "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt", pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt, true, 10, 99, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt", pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt, true, 10, 114, 18, "WHITE", 
                "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_21", "T.P.A", "BLUE", 12, 1, 5);
            uiForm.setUiControl("pnd_Tiaa_Tpa_Rlvr_Cnt", pnd_Tiaa_Tpa_Rlvr_Cnt, true, 12, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Tpa_Rlvr_Amt", pnd_Tiaa_Tpa_Rlvr_Amt, true, 12, 77, 14, "WHITE", "ZZZ,ZZZ,ZZZ.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Tpa_Accm_Cnt", pnd_Tiaa_Tpa_Accm_Cnt, true, 12, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Tpa_Accm_Amt", pnd_Tiaa_Tpa_Accm_Amt, true, 12, 118, 14, "WHITE", "ZZZ,ZZZ,ZZZ.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_22", "TIAA ACCESS MONTHLY", "BLUE", 14, 1, 19);
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt_12pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt.getValue(12+0), 
                true, 14, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt_12pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt.getValue(12+0), 
                true, 14, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt_12pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt.getValue(12+0), 
                true, 14, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt_12pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt.getValue(12+0), 
                true, 14, 77, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt_12pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt.getValue(12+0), 
                true, 14, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt_12pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt.getValue(12+0), 
                true, 14, 118, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_23", "TIAA ACCESS ANNUAL", "BLUE", 15, 1, 18);
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt_12pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt.getValue(12+0), 
                true, 15, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt_12pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt.getValue(12+0), 
                true, 15, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt_12pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt.getValue(12+0), 
                true, 15, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt_12pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt.getValue(12+0), 
                true, 15, 77, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt_12pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt.getValue(12+0), 
                true, 15, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt_12pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt.getValue(12+0), 
                true, 15, 118, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_24", "TIAA REA MONTHLY", "BLUE", 16, 1, 16);
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt_2pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt.getValue(2+0), 
                true, 16, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt_2pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt.getValue(2+0), 
                true, 16, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt_2pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt.getValue(2+0), 
                true, 16, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt_2pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt.getValue(2+0), 
                true, 16, 77, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt_2pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt.getValue(2+0), 
                true, 16, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt_2pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt.getValue(2+0), 
                true, 16, 118, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_25", "TIAA REA ANNUAL", "BLUE", 17, 1, 15);
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt_2pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt.getValue(2+0), 
                true, 17, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt_2pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt.getValue(2+0), 
                true, 17, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt_2pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt.getValue(2+0), 
                true, 17, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt_2pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt.getValue(2+0), 
                true, 17, 77, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt_2pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt.getValue(2+0), 
                true, 17, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt_2pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt.getValue(2+0), 
                true, 17, 118, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_26", "STOCK", "BLUE", 19, 1, 5);
            uiForm.setUiLabel("label_27", "MONTHLY", "BLUE", 19, 8, 7);
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt_3pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt.getValue(3+0), 
                true, 19, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt_3pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt.getValue(3+0), 
                true, 19, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt_3pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt.getValue(3+0), 
                true, 19, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt_3pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt.getValue(3+0), 
                true, 19, 77, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt_3pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt.getValue(3+0), 
                true, 19, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt_3pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt.getValue(3+0), 
                true, 19, 118, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_28", "MMA", "BLUE", 20, 1, 3);
            uiForm.setUiLabel("label_29", "MONTHLY", "BLUE", 20, 8, 7);
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt_3pls1", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt.getValue(3+1), 
                true, 20, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt_3pls1", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt.getValue(3+1), 
                true, 20, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt_3pls1", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt.getValue(3+1), 
                true, 20, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt_3pls1", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt.getValue(3+1), 
                true, 20, 77, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt_3pls1", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt.getValue(3+1), 
                true, 20, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt_3pls1", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt.getValue(3+1), 
                true, 20, 118, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_30", "SOCIAL MONTHLY", "BLUE", 21, 1, 14);
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt_3pls2", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt.getValue(3+2), 
                true, 21, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt_3pls2", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt.getValue(3+2), 
                true, 21, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt_3pls2", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt.getValue(3+2), 
                true, 21, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt_3pls2", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt.getValue(3+2), 
                true, 21, 77, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt_3pls2", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt.getValue(3+2), 
                true, 21, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt_3pls2", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt.getValue(3+2), 
                true, 21, 118, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_31", "BOND", "BLUE", 22, 1, 4);
            uiForm.setUiLabel("label_32", "MONTHLY", "BLUE", 22, 8, 7);
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt_3pls3", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt.getValue(3+3), 
                true, 22, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt_3pls3", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt.getValue(3+3), 
                true, 22, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt_3pls3", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt.getValue(3+3), 
                true, 22, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt_3pls3", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt.getValue(3+3), 
                true, 22, 77, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt_3pls3", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt.getValue(3+3), 
                true, 22, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt_3pls3", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt.getValue(3+3), 
                true, 22, 118, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_33", "GLOBAL MONTHLY", "BLUE", 23, 1, 14);
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt_3pls4", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt.getValue(3+4), 
                true, 23, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt_3pls4", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt.getValue(3+4), 
                true, 23, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt_3pls4", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt.getValue(3+4), 
                true, 23, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt_3pls4", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt.getValue(3+4), 
                true, 23, 77, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt_3pls4", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt.getValue(3+4), 
                true, 23, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt_3pls4", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt.getValue(3+4), 
                true, 23, 118, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_34", "GROWTH MONTHLY", "BLUE", 24, 1, 14);
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt_3pls5", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt.getValue(3+5), 
                true, 24, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt_3pls5", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt.getValue(3+5), 
                true, 24, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt_3pls5", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt.getValue(3+5), 
                true, 24, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt_3pls5", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt.getValue(3+5), 
                true, 24, 77, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt_3pls5", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt.getValue(3+5), 
                true, 24, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt_3pls5", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt.getValue(3+5), 
                true, 24, 118, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_35", "EQUITY MONTHLY", "BLUE", 25, 1, 14);
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt_3pls6", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt.getValue(3+6), 
                true, 25, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt_3pls6", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt.getValue(3+6), 
                true, 25, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt_3pls6", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt.getValue(3+6), 
                true, 25, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt_3pls6", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt.getValue(3+6), 
                true, 25, 77, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt_3pls6", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt.getValue(3+6), 
                true, 25, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt_3pls6", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt.getValue(3+6), 
                true, 25, 118, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_36", "I L B", "BLUE", 26, 1, 5);
            uiForm.setUiLabel("label_37", "MONTHLY", "BLUE", 26, 8, 7);
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt_3pls7", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Cnt.getValue(3+7), 
                true, 26, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt_3pls7", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Amt.getValue(3+7), 
                true, 26, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt_3pls7", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Cnt.getValue(3+7), 
                true, 26, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt_3pls7", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Rtb_Rlvr_Amt.getValue(3+7), 
                true, 26, 77, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt_3pls7", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt.getValue(3+7), 
                true, 26, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt_3pls7", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt.getValue(3+7), 
                true, 26, 118, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_38", "TOTAL CREF MONTHLY", "BLUE", 27, 1, 18);
            uiForm.setUiControl("pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Rtb_Cnt", pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Rtb_Cnt, 
                true, 27, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Rtb_Amt", pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Rtb_Amt, 
                true, 27, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Rtb_Rlvr_Cnt", pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Rtb_Rlvr_Cnt, 
                true, 27, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Rtb_Rlvr_Amt", pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Rtb_Rlvr_Amt, 
                true, 27, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Maturity_Cnt", pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Maturity_Cnt, 
                true, 27, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Maturity_Amt", pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Maturity_Amt, 
                true, 27, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_39", "STOCK", "BLUE", 29, 1, 5);
            uiForm.setUiLabel("label_40", "ANNUAL", "BLUE", 29, 8, 6);
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt_3pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt.getValue(3+0), 
                true, 29, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt_3pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt.getValue(3+0), 
                true, 29, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt_3pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt.getValue(3+0), 
                true, 29, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt_3pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt.getValue(3+0), 
                true, 29, 77, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt_3pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt.getValue(3+0), 
                true, 29, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt_3pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt.getValue(3+0), 
                true, 29, 118, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_41", "MMA", "BLUE", 30, 1, 3);
            uiForm.setUiLabel("label_42", "ANNUAL", "BLUE", 30, 8, 6);
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt_3pls1", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt.getValue(3+1), 
                true, 30, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt_3pls1", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt.getValue(3+1), 
                true, 30, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt_3pls1", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt.getValue(3+1), 
                true, 30, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt_3pls1", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt.getValue(3+1), 
                true, 30, 77, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt_3pls1", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt.getValue(3+1), 
                true, 30, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt_3pls1", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt.getValue(3+1), 
                true, 30, 118, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_43", "SOCIAL ANNUAL", "BLUE", 31, 1, 13);
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt_3pls2", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt.getValue(3+2), 
                true, 31, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt_3pls2", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt.getValue(3+2), 
                true, 31, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt_3pls2", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt.getValue(3+2), 
                true, 31, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt_3pls2", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt.getValue(3+2), 
                true, 31, 77, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt_3pls2", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt.getValue(3+2), 
                true, 31, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt_3pls2", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt.getValue(3+2), 
                true, 31, 118, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_44", "BOND", "BLUE", 32, 1, 4);
            uiForm.setUiLabel("label_45", "ANNUAL", "BLUE", 32, 8, 6);
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt_3pls3", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt.getValue(3+3), 
                true, 32, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt_3pls3", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt.getValue(3+3), 
                true, 32, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt_3pls3", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt.getValue(3+3), 
                true, 32, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt_3pls3", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt.getValue(3+3), 
                true, 32, 77, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt_3pls3", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt.getValue(3+3), 
                true, 32, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt_3pls3", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt.getValue(3+3), 
                true, 32, 118, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_46", "GLOBAL ANNUAL", "BLUE", 33, 1, 13);
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt_3pls4", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt.getValue(3+4), 
                true, 33, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt_3pls4", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt.getValue(3+4), 
                true, 33, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt_3pls4", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt.getValue(3+4), 
                true, 33, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt_3pls4", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt.getValue(3+4), 
                true, 33, 77, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt_3pls4", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt.getValue(3+4), 
                true, 33, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt_3pls4", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt.getValue(3+4), 
                true, 33, 118, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_47", "GROWTH ANNUAL", "BLUE", 34, 1, 13);
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt_3pls5", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt.getValue(3+5), 
                true, 34, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt_3pls5", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt.getValue(3+5), 
                true, 34, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt_3pls5", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt.getValue(3+5), 
                true, 34, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt_3pls5", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt.getValue(3+5), 
                true, 34, 77, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt_3pls5", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt.getValue(3+5), 
                true, 34, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt_3pls5", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt.getValue(3+5), 
                true, 34, 118, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_48", "EQUITY ANNUAL", "BLUE", 35, 1, 13);
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt_3pls6", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt.getValue(3+6), 
                true, 35, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt_3pls6", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt.getValue(3+6), 
                true, 35, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt_3pls6", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt.getValue(3+6), 
                true, 35, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt_3pls6", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt.getValue(3+6), 
                true, 35, 77, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt_3pls6", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt.getValue(3+6), 
                true, 35, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt_3pls6", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt.getValue(3+6), 
                true, 35, 118, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_49", "I L B", "BLUE", 36, 1, 5);
            uiForm.setUiLabel("label_50", "ANNUAL", "BLUE", 36, 8, 6);
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt_3pls7", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Cnt.getValue(3+7), 
                true, 36, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt_3pls7", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Amt.getValue(3+7), 
                true, 36, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt_3pls7", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Cnt.getValue(3+7), 
                true, 36, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt_3pls7", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Rtb_Rlvr_Amt.getValue(3+7), 
                true, 36, 77, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt_3pls7", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt.getValue(3+7), 
                true, 36, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt_3pls7", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt.getValue(3+7), 
                true, 36, 118, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_51", "TOTAL CREF ANNUAL", "BLUE", 37, 1, 17);
            uiForm.setUiControl("pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Rtb_Cnt", pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Rtb_Cnt, 
                true, 37, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Rtb_Amt", pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Rtb_Amt, 
                true, 37, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Rtb_Rlvr_Cnt", pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Rtb_Rlvr_Cnt, 
                true, 37, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Rtb_Rlvr_Amt", pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Rtb_Rlvr_Amt, 
                true, 37, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Maturity_Cnt", pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Maturity_Cnt, 
                true, 37, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Maturity_Amt", pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Maturity_Amt, 
                true, 37, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_52", "TOTAL ALL", "BLUE", 40, 1, 9);
            uiForm.setUiControl("pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Rtb_Cnt", pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Rtb_Cnt, true, 40, 
                24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rtb_Amt", pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rtb_Amt, true, 40, 37, 18, 
                "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Rtb_Rlvr_Cnt", pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Rtb_Rlvr_Cnt, 
                true, 40, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rtb_Rlvr_Amt", pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rtb_Rlvr_Amt, true, 40, 
                73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt", pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt, 
                true, 40, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt", pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt, true, 40, 
                114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
