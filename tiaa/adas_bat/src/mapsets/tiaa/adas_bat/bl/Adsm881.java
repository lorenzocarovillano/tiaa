/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:40 PM
**        *   FROM NATURAL MAP   :  Adsm881
************************************************************
**        * FILE NAME               : Adsm881.java
**        * CLASS NAME              : Adsm881
**        * INSTANCE NAME           : Adsm881
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #CREF-ANNUALLY-METHOD-TOTALS.#CREF-DA-RATE-ANNUALLY                                                                      *     #CREF-ANNUALLY-METHOD-TOTALS.#CREF-IA-ANNUALLY-PAYMENTS 
    *     #CREF-ANNUALLY-METHOD-TOTALS.#CREF-IA-ANNUALLY-UNIT-VAL                                                                  *     #CREF-ANNUALLY-METHOD-TOTALS.#CREF-IA-ANNUALLY-UNITS 
    *     #CREF-ANNUALLY-METHOD-TOTALS.#CREF-IA-RATE-ANNUALLY                                                                      *     #CREF-ANNUALLY-METHOD-TOTALS.#CREF-PROD-ANNUALLY 
    *     #CREF-ANNUALLY-METHOD-TOTALS.#CREF-SETTLED-ANNUALLY-AMT
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm881 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Cref_Annually_Method_Totals_Pnd_Cref_Da_Rate_Annually;
    private DbsField pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Payments;
    private DbsField pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Unit_Val;
    private DbsField pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Units;
    private DbsField pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Rate_Annually;
    private DbsField pnd_Cref_Annually_Method_Totals_Pnd_Cref_Prod_Annually;
    private DbsField pnd_Cref_Annually_Method_Totals_Pnd_Cref_Settled_Annually_Amt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Cref_Annually_Method_Totals_Pnd_Cref_Da_Rate_Annually = parameters.newFieldInRecord("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Da_Rate_Annually", 
            "#CREF-ANNUALLY-METHOD-TOTALS.#CREF-DA-RATE-ANNUALLY", FieldType.STRING, 2);
        pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Payments = parameters.newFieldInRecord("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Payments", 
            "#CREF-ANNUALLY-METHOD-TOTALS.#CREF-IA-ANNUALLY-PAYMENTS", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Unit_Val = parameters.newFieldInRecord("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Unit_Val", 
            "#CREF-ANNUALLY-METHOD-TOTALS.#CREF-IA-ANNUALLY-UNIT-VAL", FieldType.PACKED_DECIMAL, 12, 4);
        pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Units = parameters.newFieldInRecord("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Units", 
            "#CREF-ANNUALLY-METHOD-TOTALS.#CREF-IA-ANNUALLY-UNITS", FieldType.PACKED_DECIMAL, 11, 3);
        pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Rate_Annually = parameters.newFieldInRecord("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Rate_Annually", 
            "#CREF-ANNUALLY-METHOD-TOTALS.#CREF-IA-RATE-ANNUALLY", FieldType.STRING, 2);
        pnd_Cref_Annually_Method_Totals_Pnd_Cref_Prod_Annually = parameters.newFieldInRecord("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Prod_Annually", 
            "#CREF-ANNUALLY-METHOD-TOTALS.#CREF-PROD-ANNUALLY", FieldType.STRING, 1);
        pnd_Cref_Annually_Method_Totals_Pnd_Cref_Settled_Annually_Amt = parameters.newFieldInRecord("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Settled_Annually_Amt", 
            "#CREF-ANNUALLY-METHOD-TOTALS.#CREF-SETTLED-ANNUALLY-AMT", FieldType.PACKED_DECIMAL, 10, 2);
        parameters.reset();
    }

    public Adsm881() throws Exception
    {
        super("Adsm881");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm881", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm881"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Prod_Annually", pnd_Cref_Annually_Method_Totals_Pnd_Cref_Prod_Annually, true, 
                2, 2, 1, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Da_Rate_Annually", pnd_Cref_Annually_Method_Totals_Pnd_Cref_Da_Rate_Annually, 
                true, 2, 9, 2, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Rate_Annually", pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Rate_Annually, 
                true, 2, 18, 2, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Settled_Annually_Amt", pnd_Cref_Annually_Method_Totals_Pnd_Cref_Settled_Annually_Amt, 
                true, 2, 23, 12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Units", pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Units, 
                true, 2, 37, 13, "WHITE", "Z,ZZZ,ZZ9.999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Payments", pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Payments, 
                true, 2, 51, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Unit_Val", pnd_Cref_Annually_Method_Totals_Pnd_Cref_Ia_Annually_Unit_Val, 
                true, 2, 66, 14, "WHITE", "Z,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
