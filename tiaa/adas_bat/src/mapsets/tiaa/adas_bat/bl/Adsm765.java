/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:56 PM
**        *   FROM NATURAL MAP   :  Adsm765
************************************************************
**        * FILE NAME               : Adsm765.java
**        * CLASS NAME              : Adsm765
**        * INSTANCE NAME           : Adsm765
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #DA-ACCUM-DATE #PARTCIPANT-NAME-1 #RQST-ID.#UNIQUE-ID
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm765 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Da_Accum_Date;
    private DbsField pnd_Partcipant_Name_1;
    private DbsField pnd_Rqst_Id_Pnd_Unique_Id;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Da_Accum_Date = parameters.newFieldInRecord("pnd_Da_Accum_Date", "#DA-ACCUM-DATE", FieldType.NUMERIC, 6);
        pnd_Partcipant_Name_1 = parameters.newFieldInRecord("pnd_Partcipant_Name_1", "#PARTCIPANT-NAME-1", FieldType.STRING, 30);
        pnd_Rqst_Id_Pnd_Unique_Id = parameters.newFieldInRecord("pnd_Rqst_Id_Pnd_Unique_Id", "#RQST-ID.#UNIQUE-ID", FieldType.NUMERIC, 12);
        parameters.reset();
    }

    public Adsm765() throws Exception
    {
        super("Adsm765");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm765", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm765"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Rqst_Id_Pnd_Unique_Id", pnd_Rqst_Id_Pnd_Unique_Id, true, 1, 2, 12, "WHITE", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Partcipant_Name_1", pnd_Partcipant_Name_1, true, 2, 2, 30, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_1", "* TOTAL SETTLEMENT FOR ALL TIAA CONTRACTS", "BLUE", 4, 1, 41);
            uiForm.setUiLabel("label_2", "__________________________________________________________________________________________________________________________________", 
                "", 5, 1, 130);
            uiForm.setUiLabel("label_3", "Rate", "BLUE", 6, 1, 4);
            uiForm.setUiLabel("label_4", "DA Accum Date", "BLUE", 6, 10, 13);
            uiForm.setUiLabel("label_5", "Settlement", "BLUE", 6, 33, 10);
            uiForm.setUiLabel("label_6", "RTB", "BLUE", 6, 52, 3);
            uiForm.setUiLabel("label_7", "Settlement", "BLUE", 6, 68, 10);
            uiForm.setUiLabel("label_8", "Basis", "BLUE", 7, 1, 5);
            uiForm.setUiControl("pnd_Da_Accum_Date", pnd_Da_Accum_Date, true, 7, 11, 8, "WHITE", "99/99/99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_9", "Amount", "BLUE", 7, 33, 6);
            uiForm.setUiLabel("label_10", "Amount", "BLUE", 7, 51, 6);
            uiForm.setUiLabel("label_11", "Minus RTB", "BLUE", 7, 67, 9);
            uiForm.setUiLabel("label_12", "Prod Amt/Units", "BLUE", 8, 9, 14);
            uiForm.setUiLabel("label_13", "Prod Amt/Units", "BLUE", 8, 29, 14);
            uiForm.setUiLabel("label_14", "Prod Amt/Units", "BLUE", 8, 47, 14);
            uiForm.setUiLabel("label_15", "Prod Amt/Units", "BLUE", 8, 65, 14);
            uiForm.setUiLabel("label_16", "__________________________________________________________________________________________________________________________________", 
                "", 9, 1, 130);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
