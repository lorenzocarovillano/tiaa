/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:03 PM
**        *   FROM NATURAL MAP   :  Adsm783
************************************************************
**        * FILE NAME               : Adsm783.java
**        * CLASS NAME              : Adsm783
**        * INSTANCE NAME           : Adsm783
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #CREF-AND-TIAA-GRAND-ACCUM.#GRAND-MATURITY-AMT                                                                           *     #CREF-AND-TIAA-GRAND-ACCUM.#GRAND-RATE-MATURITY-CNT 
    *     #CREF-ANNUALLY-GRAND-ACCUM.#CREF-GRN-ANN-MAT-AMT(*)                                                                      *     #CREF-ANNUALLY-GRAND-ACCUM.#CREF-GRN-ANN-MAT-CNT(*) 
    *     #CREF-ANNUALLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-ANN-MATURITY-AMT                                                            *     #CREF-ANNUALLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-ANN-MATURITY-CNT 
    *     #CREF-MONTHLY-GRAND-ACCUM.#CREF-GRN-MTH-MAT-AMT(*)                                                                       *     #CREF-MONTHLY-GRAND-ACCUM.#CREF-GRN-MTH-MAT-CNT(*) 
    *     #CREF-MONTHLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-MTH-MATURITY-AMT                                                             *     #CREF-MONTHLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-MTH-MATURITY-CNT 
    *     #TIAA-GRAND-ACCUM.#TIAA-GRN-GRD-MATURITY-AMT                                                                             *     #TIAA-GRAND-ACCUM.#TIAA-GRN-GRD-MATURITY-CNT 
    *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STD-MATURITY-AMT                                                                             *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STD-MATURITY-CNT 
    *     #TIAA-GRAND-ACCUM.#TIAA-IPRO-GRN-STD-MATURITY-AMT                                                                        *     #TIAA-GRAND-ACCUM.#TIAA-IPRO-GRN-STD-MATURITY-CNT 
    *     #TIAA-GRAND-ACCUM.#TIAA-STBL-GRN-GRD-MATURITY-AMT                                                                        *     #TIAA-GRAND-ACCUM.#TIAA-STBL-GRN-GRD-MATURITY-CNT 
    *     #TIAA-GRAND-ACCUM.#TIAA-STBL-GRN-STD-MATURITY-AMT                                                                        *     #TIAA-GRAND-ACCUM.#TIAA-STBL-GRN-STD-MATURITY-CNT 
    *     #TIAA-GRAND-ACCUM.#TIAA-TPA-GRN-STD-MATURITY-AMT                                                                         *     #TIAA-SUB-GRN-ACCUM.#TIAA-SUB-GRN-MATURITY-AMT 
    *     #TIAA-SUB-GRN-ACCUM.#TIAA-SUB-GRN-MATURITY-CNT
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm783 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt;
    private DbsField pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt;
    private DbsField pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt;
    private DbsField pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt;
    private DbsField pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Amt;
    private DbsField pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Cnt;
    private DbsField pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt;
    private DbsField pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt;
    private DbsField pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Amt;
    private DbsField pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Cnt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Amt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Cnt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Amt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Cnt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Amt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Cnt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Amt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Cnt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Amt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Cnt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Tpa_Grn_Std_Maturity_Amt;
    private DbsField pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt;
    private DbsField pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt = parameters.newFieldInRecord("pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt", "#CREF-AND-TIAA-GRAND-ACCUM.#GRAND-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt = parameters.newFieldInRecord("pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt", 
            "#CREF-AND-TIAA-GRAND-ACCUM.#GRAND-RATE-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt = parameters.newFieldArrayInRecord("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt", 
            "#CREF-ANNUALLY-GRAND-ACCUM.#CREF-GRN-ANN-MAT-AMT", FieldType.PACKED_DECIMAL, 15, 2, new DbsArrayController(1, 20));
        pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt = parameters.newFieldArrayInRecord("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt", 
            "#CREF-ANNUALLY-GRAND-ACCUM.#CREF-GRN-ANN-MAT-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Amt = parameters.newFieldInRecord("pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Amt", 
            "#CREF-ANNUALLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-ANN-MATURITY-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Cnt = parameters.newFieldInRecord("pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Cnt", 
            "#CREF-ANNUALLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-ANN-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt = parameters.newFieldArrayInRecord("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt", 
            "#CREF-MONTHLY-GRAND-ACCUM.#CREF-GRN-MTH-MAT-AMT", FieldType.PACKED_DECIMAL, 15, 2, new DbsArrayController(1, 20));
        pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt = parameters.newFieldArrayInRecord("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt", 
            "#CREF-MONTHLY-GRAND-ACCUM.#CREF-GRN-MTH-MAT-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Amt = parameters.newFieldInRecord("pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Amt", 
            "#CREF-MONTHLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-MTH-MATURITY-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Cnt = parameters.newFieldInRecord("pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Cnt", 
            "#CREF-MONTHLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-MTH-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Amt", "#TIAA-GRAND-ACCUM.#TIAA-GRN-GRD-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Cnt", "#TIAA-GRAND-ACCUM.#TIAA-GRN-GRD-MATURITY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Amt", "#TIAA-GRAND-ACCUM.#TIAA-GRN-STD-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Cnt", "#TIAA-GRAND-ACCUM.#TIAA-GRN-STD-MATURITY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Amt", 
            "#TIAA-GRAND-ACCUM.#TIAA-IPRO-GRN-STD-MATURITY-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Cnt", 
            "#TIAA-GRAND-ACCUM.#TIAA-IPRO-GRN-STD-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Amt", 
            "#TIAA-GRAND-ACCUM.#TIAA-STBL-GRN-GRD-MATURITY-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Cnt", 
            "#TIAA-GRAND-ACCUM.#TIAA-STBL-GRN-GRD-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Amt", 
            "#TIAA-GRAND-ACCUM.#TIAA-STBL-GRN-STD-MATURITY-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Cnt", 
            "#TIAA-GRAND-ACCUM.#TIAA-STBL-GRN-STD-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Tpa_Grn_Std_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Tpa_Grn_Std_Maturity_Amt", 
            "#TIAA-GRAND-ACCUM.#TIAA-TPA-GRN-STD-MATURITY-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt", "#TIAA-SUB-GRN-ACCUM.#TIAA-SUB-GRN-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt", "#TIAA-SUB-GRN-ACCUM.#TIAA-SUB-GRN-MATURITY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        parameters.reset();
    }

    public Adsm783() throws Exception
    {
        super("Adsm783");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm783", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm783"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "GRAND TOTAL", "BLUE", 1, 1, 11);
            uiForm.setUiLabel("label_2", "ALL EFFECTIVE DATES", "BLUE", 2, 1, 19);
            uiForm.setUiLabel("label_3", "DA RATE", "BLUE", 3, 29, 7);
            uiForm.setUiLabel("label_4", "FINAL PREMIUM", "BLUE", 3, 44, 13);
            uiForm.setUiLabel("label_5", "Product", "BLUE", 4, 1, 7);
            uiForm.setUiLabel("label_6", "COUNTS", "BLUE", 4, 29, 6);
            uiForm.setUiLabel("label_7", "ACCUMULATIONS", "BLUE", 4, 44, 13);
            uiForm.setUiLabel("label_8", "TIAA STANDARD", "BLUE", 5, 1, 13);
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Cnt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Cnt, true, 5, 26, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Amt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Amt, true, 5, 39, 
                18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_9", "TIAA GRADED", "BLUE", 6, 1, 11);
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Cnt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Cnt, true, 6, 26, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Amt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Amt, true, 6, 39, 
                18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_10", "TIAA IPRO", "BLUE", 7, 1, 9);
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Cnt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Cnt, true, 
                7, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Amt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Amt, true, 
                7, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_11", "TIAA STABLE STANDARD", "BLUE", 8, 1, 20);
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Cnt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Cnt, true, 
                8, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Amt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Std_Maturity_Amt, true, 
                8, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_12", "TIAA STABLE GRADED", "BLUE", 9, 1, 18);
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Cnt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Cnt, true, 
                9, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Amt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Stbl_Grn_Grd_Maturity_Amt, true, 
                9, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_13", "TOTAL", "BLUE", 10, 1, 5);
            uiForm.setUiControl("pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt", pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt, true, 10, 
                26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt", pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt, true, 10, 
                39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_14", "TIAA TPA", "BLUE", 12, 1, 8);
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Cnt2", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Ipro_Grn_Std_Maturity_Cnt, true, 
                12, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Tpa_Grn_Std_Maturity_Amt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Tpa_Grn_Std_Maturity_Amt, true, 
                12, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_15", "TIAA ACCESS MONTHLY", "BLUE", 14, 1, 19);
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt_12pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt.getValue(12+0), 
                true, 14, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt_12pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt.getValue(12+0), 
                true, 14, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_16", "TIAA ACCESS ANNUAL", "BLUE", 15, 1, 18);
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt_12pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt.getValue(12+0), 
                true, 15, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt_12pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt.getValue(12+0), 
                true, 15, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "TIAA REA MONTHLY", "BLUE", 16, 1, 16);
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt_2pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt.getValue(2+0), 
                true, 16, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt_2pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt.getValue(2+0), 
                true, 16, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_18", "TIAA REA ANNUAL", "BLUE", 17, 1, 15);
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt_2pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt.getValue(2+0), 
                true, 17, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt_2pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt.getValue(2+0), 
                true, 17, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", "STOCK", "BLUE", 19, 1, 5);
            uiForm.setUiLabel("label_20", "MONTHLY", "BLUE", 19, 8, 7);
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt_3pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt.getValue(3+0), 
                true, 19, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt_3pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt.getValue(3+0), 
                true, 19, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_21", "MMA", "BLUE", 20, 1, 3);
            uiForm.setUiLabel("label_22", "MONTHLY", "BLUE", 20, 8, 7);
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt_3pls1", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt.getValue(3+1), 
                true, 20, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt_3pls1", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt.getValue(3+1), 
                true, 20, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_23", "SOCIAL MONTHLY", "BLUE", 21, 1, 14);
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt_3pls2", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt.getValue(3+2), 
                true, 21, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt_3pls2", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt.getValue(3+2), 
                true, 21, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_24", "BOND", "BLUE", 22, 1, 4);
            uiForm.setUiLabel("label_25", "MONTHLY", "BLUE", 22, 8, 7);
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt_3pls3", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt.getValue(3+3), 
                true, 22, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt_3pls3", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt.getValue(3+3), 
                true, 22, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_26", "GLOBAL MONTHLY", "BLUE", 23, 1, 14);
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt_3pls4", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt.getValue(3+4), 
                true, 23, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt_3pls4", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt.getValue(3+4), 
                true, 23, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_27", "GROWTH MONTHLY", "BLUE", 24, 1, 14);
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt_3pls5", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt.getValue(3+5), 
                true, 24, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt_3pls5", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt.getValue(3+5), 
                true, 24, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_28", "EQUITY MONTHLY", "BLUE", 25, 1, 14);
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt_3pls6", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt.getValue(3+6), 
                true, 25, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt_3pls6", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt.getValue(3+6), 
                true, 25, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_29", "I L B", "BLUE", 26, 1, 5);
            uiForm.setUiLabel("label_30", "MONTHLY", "BLUE", 26, 8, 7);
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt_3pls7", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt.getValue(3+7), 
                true, 26, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt_3pls7", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt.getValue(3+7), 
                true, 26, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_31", "TOTAL CREF MONTHLY", "BLUE", 27, 1, 18);
            uiForm.setUiControl("pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Cnt", pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Cnt, 
                true, 27, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Amt", pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Amt, 
                true, 27, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_32", "STOCK", "BLUE", 29, 1, 5);
            uiForm.setUiLabel("label_33", "ANNUAL", "BLUE", 29, 8, 6);
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt_3pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt.getValue(3+0), 
                true, 29, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt_3pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt.getValue(3+0), 
                true, 29, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_34", "MMA", "BLUE", 30, 1, 3);
            uiForm.setUiLabel("label_35", "ANNUAL", "BLUE", 30, 8, 6);
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt_3pls1", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt.getValue(3+1), 
                true, 30, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt_3pls1", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt.getValue(3+1), 
                true, 30, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_36", "SOCIAL ANNUAL", "BLUE", 31, 1, 13);
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt_3pls2", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt.getValue(3+2), 
                true, 31, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt_3pls2", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt.getValue(3+2), 
                true, 31, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_37", "BOND", "BLUE", 32, 1, 4);
            uiForm.setUiLabel("label_38", "ANNUAL", "BLUE", 32, 8, 6);
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt_3pls3", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt.getValue(3+3), 
                true, 32, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt_3pls3", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt.getValue(3+3), 
                true, 32, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_39", "GLOBAL ANNUAL", "BLUE", 33, 1, 13);
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt_3pls4", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt.getValue(3+4), 
                true, 33, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt_3pls4", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt.getValue(3+4), 
                true, 33, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_40", "GROWTH ANNUAL", "BLUE", 34, 1, 13);
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt_3pls5", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt.getValue(3+5), 
                true, 34, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt_3pls5", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt.getValue(3+5), 
                true, 34, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_41", "EQUITY ANNUAL", "BLUE", 35, 1, 13);
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt_3pls6", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt.getValue(3+6), 
                true, 35, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt_3pls6", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt.getValue(3+6), 
                true, 35, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_42", "I L B", "BLUE", 36, 1, 5);
            uiForm.setUiLabel("label_43", "ANNUAL", "BLUE", 36, 8, 6);
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt_3pls7", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt.getValue(3+7), 
                true, 36, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt_3pls7", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt.getValue(3+7), 
                true, 36, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_44", "TOTAL CREF ANNUAL", "BLUE", 37, 1, 17);
            uiForm.setUiControl("pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Cnt", pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Cnt, 
                true, 37, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Amt", pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Amt, 
                true, 37, 39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_45", "TOTAL ALL", "BLUE", 40, 1, 9);
            uiForm.setUiControl("pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt", pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt, 
                true, 40, 26, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt", pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt, true, 40, 
                39, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
