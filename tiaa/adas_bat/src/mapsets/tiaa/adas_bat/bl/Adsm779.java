/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:01 PM
**        *   FROM NATURAL MAP   :  Adsm779
************************************************************
**        * FILE NAME               : Adsm779.java
**        * CLASS NAME              : Adsm779
**        * INSTANCE NAME           : Adsm779
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #ACCOUNTING-DATE
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm779 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Accounting_Date;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Accounting_Date = parameters.newFieldInRecord("pnd_Accounting_Date", "#ACCOUNTING-DATE", FieldType.NUMERIC, 8);
        parameters.reset();
    }

    public Adsm779() throws Exception
    {
        super("Adsm779");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm779", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm779"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "ADSP1000", "BLUE", 1, 1, 8);
            uiForm.setUiLabel("label_2", "Annuitization of OmniPlus Contracts", "BLUE", 1, 50, 35);
            uiForm.setUiLabel("label_3", "Run Date:", "BLUE", 1, 109, 9);
            uiForm.setUiControl("astDATX", Global.getDATX(), true, 1, 119, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "Daily Activity Report - Payment Summary", "BLUE", 2, 48, 39);
            uiForm.setUiLabel("label_5", "Page No :", "BLUE", 2, 109, 9);
            uiForm.setUiControl("astPAGE_NUMBER", getReports().getPageNumberDbs(0), true, 2, 119, 5, "WHITE", true, true, null, null, "AD=D?OFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_6", "FINAL PREMIUM ACCUMULATION", "BLUE", 3, 53, 26);
            uiForm.setUiLabel("label_7", "Accounting Date", "BLUE", 4, 52, 15);
            uiForm.setUiControl("pnd_Accounting_Date", pnd_Accounting_Date, true, 4, 70, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
