/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:37 PM
**        *   FROM NATURAL MAP   :  Adsm869
************************************************************
**        * FILE NAME               : Adsm869.java
**        * CLASS NAME              : Adsm869
**        * INSTANCE NAME           : Adsm869
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #PARTCIPANT-NAME-1 #RQST-ID.#UNIQUE-ID
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm869 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Partcipant_Name_1;
    private DbsField pnd_Rqst_Id_Pnd_Unique_Id;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Partcipant_Name_1 = parameters.newFieldInRecord("pnd_Partcipant_Name_1", "#PARTCIPANT-NAME-1", FieldType.STRING, 30);
        pnd_Rqst_Id_Pnd_Unique_Id = parameters.newFieldInRecord("pnd_Rqst_Id_Pnd_Unique_Id", "#RQST-ID.#UNIQUE-ID", FieldType.NUMERIC, 12);
        parameters.reset();
    }

    public Adsm869() throws Exception
    {
        super("Adsm869");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm869", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm869"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Rqst_Id_Pnd_Unique_Id", pnd_Rqst_Id_Pnd_Unique_Id, true, 1, 2, 12, "WHITE", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Partcipant_Name_1", pnd_Partcipant_Name_1, true, 2, 2, 30, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_1", "SETTLEMENT CALCULATION INFORMATION", "BLUE", 4, 1, 34);
            uiForm.setUiLabel("label_2", "ANNUAL REVALUATION METHOD", "BLUE", 6, 1, 25);
            uiForm.setUiLabel("label_3", "__________________________", "", 7, 1, 26);
            uiForm.setUiLabel("label_4", "IA PAYMENTS", "BLUE", 9, 60, 11);
            uiForm.setUiLabel("label_5", "DA", "BLUE", 10, 9, 2);
            uiForm.setUiLabel("label_6", "IA", "BLUE", 10, 18, 2);
            uiForm.setUiLabel("label_7", "SETTLED", "BLUE", 10, 27, 7);
            uiForm.setUiLabel("label_8", "PROD", "BLUE", 11, 1, 4);
            uiForm.setUiLabel("label_9", "RATE", "BLUE", 11, 8, 4);
            uiForm.setUiLabel("label_10", "RATE", "BLUE", 11, 17, 4);
            uiForm.setUiLabel("label_11", "ACCUM", "BLUE", 11, 27, 5);
            uiForm.setUiLabel("label_12", "UNITS", "BLUE", 11, 44, 5);
            uiForm.setUiLabel("label_13", "PAYMENTS", "BLUE", 11, 56, 8);
            uiForm.setUiLabel("label_14", "UNIT VALUE", "BLUE", 11, 69, 10);
            uiForm.setUiLabel("label_15", "____", "BLUE", 12, 1, 4);
            uiForm.setUiLabel("label_16", "____", "BLUE", 12, 8, 4);
            uiForm.setUiLabel("label_17", "____", "BLUE", 12, 17, 4);
            uiForm.setUiLabel("label_18", "________", "BLUE", 12, 26, 8);
            uiForm.setUiLabel("label_19", "_________", "BLUE", 12, 42, 9);
            uiForm.setUiLabel("label_20", "__________", "BLUE", 12, 55, 10);
            uiForm.setUiLabel("label_21", "__________", "BLUE", 12, 69, 10);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
