/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:34 PM
**        *   FROM NATURAL MAP   :  Adsm1331
************************************************************
**        * FILE NAME               : Adsm1331.java
**        * CLASS NAME              : Adsm1331
**        * INSTANCE NAME           : Adsm1331
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-DIVID-AMT(*)                                                                         *     #F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-DOLLARS(*) 
    *     #F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-FIN-DIV-AMT(*)                                                                       *     #F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-FIN-GTD-AMT(*) 
    *     #F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-GTD-AMT(*)                                                                           *     #F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-PAY-CNT(*) 
    *     #F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-UNITS(*)                                                                             *     #F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-DIV-AMT 
    *     #F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-DOLLARS                                                                      *     #F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-FIN-DIV-AMT 
    *     #F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-FIN-GTD-AMT                                                                  *     #F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-GTD-AMT 
    *     #F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-PAY-CNT                                                                      *     #F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-UNITS 
    *     #F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-DIV-AMT(*)                                                                           *     #F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-DOLLARS(*) 
    *     #F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-FIN-DIV-AMT(*)                                                                       *     #F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-FIN-GTD-AMT(*) 
    *     #F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-GTD-AMT(*)                                                                           *     #F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-PAY-CNT(*) 
    *     #F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-UNITS(*)                                                                             *     #F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-DIV-AMT 
    *     #F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-DOLLARS                                                                      *     #F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-FIN-DIV-AMT 
    *     #F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-FIN-GTD-AMT                                                                  *     #F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-GTD-AMT 
    *     #F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-PAY-CNT                                                                      *     #F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-UNITS 
    #F-IPRO-DIV-AMT                                                        *     #F-IPRO-FINAL-GTD-AMT #F-IPRO-GTD-AMT #F-IPRO-PAY-CNT                  
    *     #F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-DIV-AMT                                                                                  *     #F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-DOLLARS 
    *     #F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-FIN-DIV-AMT                                                                              *     #F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-FIN-GTD-AMT 
    *     #F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-GTD-AMT                                                                                  *     #F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-PAY-CNT 
    *     #F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-UNITS                                                                                    *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-DIV-AMT 
    *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-DOLLARS                                                                                  *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-FIN-DIV-AMT 
    *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-FIN-GTD-AMT                                                                              *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-GTD-AMT 
    *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-PAY-CNT                                                                                  *     #F-TIAA-MINOR-ACCUM.#F-TIAA-STD-UNITS 
    *     #F-TIAA-SUB-ACCUM.#F-TIAA-SUB-DIVID-AMT                                                                                  *     #F-TIAA-SUB-ACCUM.#F-TIAA-SUB-DOLLARS 
    *     #F-TIAA-SUB-ACCUM.#F-TIAA-SUB-FIN-DIV-AMT                                                                                *     #F-TIAA-SUB-ACCUM.#F-TIAA-SUB-FIN-GTD-AMT 
    *     #F-TIAA-SUB-ACCUM.#F-TIAA-SUB-GTD-AMT                                                                                    *     #F-TIAA-SUB-ACCUM.#F-TIAA-SUB-PAY-CNT 
    *     #F-TIAA-SUB-ACCUM.#F-TIAA-SUB-UNITS                                                                                      *     #F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-DIV-AMT 
    *     #F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-DOLLARS                                                                                *     #F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-FIN-DIV-AMT 
    *     #F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-FIN-GTD-AMT                                                                            *     #F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-GTD-AMT 
    *     #F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-PAY-CNT                                                                                *     #F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-UNITS 
    #F-TPA-DIV-AMT                                                                   *     #F-TPA-GTD-AMT #F-TPA-PAY-CNT
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm1331 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt;
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars;
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt;
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt;
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt;
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt;
    private DbsField pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Div_Amt;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Dollars;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Div_Amt;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Gtd_Amt;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Gtd_Amt;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Pay_Cnt;
    private DbsField pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Units;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt;
    private DbsField pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Div_Amt;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Dollars;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Div_Amt;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Gtd_Amt;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Gtd_Amt;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Pay_Cnt;
    private DbsField pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Units;
    private DbsField pnd_F_Ipro_Div_Amt;
    private DbsField pnd_F_Ipro_Final_Gtd_Amt;
    private DbsField pnd_F_Ipro_Gtd_Amt;
    private DbsField pnd_F_Ipro_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Dollars;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Units;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Dollars;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Fin_Div_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Fin_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Units;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Divid_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Dollars;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Div_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Gtd_Amt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Pay_Cnt;
    private DbsField pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Units;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Div_Amt;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Dollars;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Div_Amt;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Gtd_Amt;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Gtd_Amt;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt;
    private DbsField pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Units;
    private DbsField pnd_F_Tpa_Div_Amt;
    private DbsField pnd_F_Tpa_Gtd_Amt;
    private DbsField pnd_F_Tpa_Pay_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt = parameters.newFieldArrayInRecord("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt", 
            "#F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-DIVID-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 20));
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars = parameters.newFieldArrayInRecord("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars", "#F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-DOLLARS", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 20));
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt = parameters.newFieldArrayInRecord("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt", 
            "#F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 20));
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt", 
            "#F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 20));
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt", "#F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 20));
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt = parameters.newFieldArrayInRecord("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt", "#F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units = parameters.newFieldArrayInRecord("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units", "#F-CREF-ANN-MINOR-ACCUM.#F-CREF-ANN-UNITS", 
            FieldType.PACKED_DECIMAL, 14, 4, new DbsArrayController(1, 20));
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Div_Amt = parameters.newFieldInRecord("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Div_Amt", 
            "#F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Dollars = parameters.newFieldInRecord("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Dollars", 
            "#F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Div_Amt = parameters.newFieldInRecord("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Div_Amt", 
            "#F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Gtd_Amt", 
            "#F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Gtd_Amt", 
            "#F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Pay_Cnt", 
            "#F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Units = parameters.newFieldInRecord("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Units", 
            "#F-CREF-ANN-SUB-TOTAL-ACCUM.#F-CREF-ANN-SUB-UNITS", FieldType.PACKED_DECIMAL, 14, 4);
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt = parameters.newFieldArrayInRecord("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt", "#F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 20));
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars = parameters.newFieldArrayInRecord("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars", "#F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-DOLLARS", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 20));
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt = parameters.newFieldArrayInRecord("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt", 
            "#F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 20));
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt", 
            "#F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 20));
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt", "#F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 20));
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt = parameters.newFieldArrayInRecord("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt", "#F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units = parameters.newFieldArrayInRecord("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units", "#F-CREF-MTH-MINOR-ACCUM.#F-CREF-MTH-UNITS", 
            FieldType.PACKED_DECIMAL, 14, 4, new DbsArrayController(1, 20));
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Div_Amt = parameters.newFieldInRecord("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Div_Amt", 
            "#F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Dollars = parameters.newFieldInRecord("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Dollars", 
            "#F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Div_Amt = parameters.newFieldInRecord("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Div_Amt", 
            "#F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Gtd_Amt", 
            "#F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Gtd_Amt", 
            "#F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Pay_Cnt", 
            "#F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Units = parameters.newFieldInRecord("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Units", 
            "#F-CREF-MTH-SUB-TOTAL-ACCUM.#F-CREF-MTH-SUB-UNITS", FieldType.PACKED_DECIMAL, 14, 4);
        pnd_F_Ipro_Div_Amt = parameters.newFieldInRecord("pnd_F_Ipro_Div_Amt", "#F-IPRO-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Ipro_Final_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Ipro_Final_Gtd_Amt", "#F-IPRO-FINAL-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Ipro_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Ipro_Gtd_Amt", "#F-IPRO-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Ipro_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Ipro_Pay_Cnt", "#F-IPRO-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Div_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Div_Amt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Dollars = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Dollars", "#F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Div_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Div_Amt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Gtd_Amt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Gtd_Amt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Pay_Cnt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Units = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Units", "#F-TIAA-MINOR-ACCUM.#F-TIAA-GRD-UNITS", 
            FieldType.PACKED_DECIMAL, 14, 4);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Div_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Div_Amt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Dollars = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Dollars", "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Fin_Div_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Fin_Div_Amt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Fin_Gtd_Amt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Gtd_Amt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Pay_Cnt", "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Units = parameters.newFieldInRecord("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Units", "#F-TIAA-MINOR-ACCUM.#F-TIAA-STD-UNITS", 
            FieldType.PACKED_DECIMAL, 14, 4);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Divid_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Divid_Amt", "#F-TIAA-SUB-ACCUM.#F-TIAA-SUB-DIVID-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Dollars = parameters.newFieldInRecord("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Dollars", "#F-TIAA-SUB-ACCUM.#F-TIAA-SUB-DOLLARS", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Div_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Div_Amt", "#F-TIAA-SUB-ACCUM.#F-TIAA-SUB-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Gtd_Amt", "#F-TIAA-SUB-ACCUM.#F-TIAA-SUB-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Gtd_Amt", "#F-TIAA-SUB-ACCUM.#F-TIAA-SUB-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Pay_Cnt", "#F-TIAA-SUB-ACCUM.#F-TIAA-SUB-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Units = parameters.newFieldInRecord("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Units", "#F-TIAA-SUB-ACCUM.#F-TIAA-SUB-UNITS", 
            FieldType.PACKED_DECIMAL, 14, 4);
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Div_Amt = parameters.newFieldInRecord("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Div_Amt", "#F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Dollars = parameters.newFieldInRecord("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Dollars", "#F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-DOLLARS", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Div_Amt = parameters.newFieldInRecord("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Div_Amt", "#F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Gtd_Amt", "#F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Gtd_Amt", "#F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt", "#F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 10);
        pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Units = parameters.newFieldInRecord("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Units", "#F-TOTAL-TIAA-CREF-ACCUM.#F-TOTAL-UNITS", 
            FieldType.PACKED_DECIMAL, 14, 4);
        pnd_F_Tpa_Div_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Div_Amt", "#F-TPA-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tpa_Gtd_Amt = parameters.newFieldInRecord("pnd_F_Tpa_Gtd_Amt", "#F-TPA-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_F_Tpa_Pay_Cnt = parameters.newFieldInRecord("pnd_F_Tpa_Pay_Cnt", "#F-TPA-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        parameters.reset();
    }

    public Adsm1331() throws Exception
    {
        super("Adsm1331");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm1331", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm1331"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "ADSP1330", "BLUE", 1, 1, 8);
            uiForm.setUiLabel("label_2", "GRAND TOTALS", "BLUE", 2, 1, 12);
            uiForm.setUiLabel("label_3", "ALL MODES WITHIN ANNTY-START-DATE", "BLUE", 3, 1, 33);
            uiForm.setUiLabel("label_4", "PAYMENT", "BLUE", 5, 24, 7);
            uiForm.setUiLabel("label_5", "CREF", "BLUE", 5, 40, 4);
            uiForm.setUiLabel("label_6", "CREF", "BLUE", 5, 57, 4);
            uiForm.setUiLabel("label_7", "TIAA", "BLUE", 5, 75, 4);
            uiForm.setUiLabel("label_8", "TIAA", "BLUE", 5, 91, 4);
            uiForm.setUiLabel("label_9", "FINAL", "BLUE", 5, 109, 5);
            uiForm.setUiLabel("label_10", "FINAL", "BLUE", 5, 124, 5);
            uiForm.setUiLabel("label_11", "Product", "BLUE", 6, 1, 7);
            uiForm.setUiLabel("label_12", "COUNTS", "BLUE", 6, 25, 6);
            uiForm.setUiLabel("label_13", "UNITS", "BLUE", 6, 40, 5);
            uiForm.setUiLabel("label_14", "DOLLARS", "BLUE", 6, 56, 7);
            uiForm.setUiLabel("label_15", "GTD/AMT", "BLUE", 6, 74, 7);
            uiForm.setUiLabel("label_16", "DIVID/AMT", "BLUE", 6, 89, 9);
            uiForm.setUiLabel("label_17", "GTD/AMT", "BLUE", 6, 108, 7);
            uiForm.setUiLabel("label_18", "DIVID/AMT", "BLUE", 6, 123, 9);
            uiForm.setUiLabel("label_19", "TIAA STANDARD", "BLUE", 7, 1, 13);
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Pay_Cnt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Pay_Cnt, true, 7, 19, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Units", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Units, true, 7, 29, 16, "WHITE", 
                "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Dollars", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Dollars, true, 7, 47, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Gtd_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Gtd_Amt, true, 7, 64, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Div_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Div_Amt, true, 7, 81, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Fin_Gtd_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Fin_Gtd_Amt, true, 7, 98, 16, 
                "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Fin_Div_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Std_Fin_Div_Amt, true, 7, 115, 
                16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_20", "TIAA GRADED", "BLUE", 8, 1, 11);
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Pay_Cnt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Pay_Cnt, true, 8, 19, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Units", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Units, true, 8, 29, 16, "WHITE", 
                "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Dollars", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Dollars, true, 8, 47, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Gtd_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Gtd_Amt, true, 8, 64, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Div_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Div_Amt, true, 8, 81, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Gtd_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Gtd_Amt, true, 8, 98, 16, 
                "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Div_Amt", pnd_F_Tiaa_Minor_Accum_Pnd_F_Tiaa_Grd_Fin_Div_Amt, true, 8, 115, 
                16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_21", "TIAA IPRO", "BLUE", 9, 1, 9);
            uiForm.setUiControl("pnd_F_Ipro_Pay_Cnt", pnd_F_Ipro_Pay_Cnt, true, 9, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_F_Ipro_Gtd_Amt", pnd_F_Ipro_Gtd_Amt, true, 9, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Ipro_Div_Amt", pnd_F_Ipro_Div_Amt, true, 9, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Ipro_Final_Gtd_Amt", pnd_F_Ipro_Final_Gtd_Amt, true, 9, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, 
                "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_22", "TOTAL", "BLUE", 10, 1, 5);
            uiForm.setUiControl("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Pay_Cnt", pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Pay_Cnt, true, 10, 19, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Units", pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Units, true, 10, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Dollars", pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Dollars, true, 10, 47, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Gtd_Amt", pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Gtd_Amt, true, 10, 64, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Divid_Amt", pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Divid_Amt, true, 10, 81, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Gtd_Amt", pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Gtd_Amt, true, 10, 98, 16, 
                "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Div_Amt", pnd_F_Tiaa_Sub_Accum_Pnd_F_Tiaa_Sub_Fin_Div_Amt, true, 10, 115, 16, 
                "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_23", "T.P.A", "BLUE", 12, 1, 5);
            uiForm.setUiControl("pnd_F_Tpa_Pay_Cnt", pnd_F_Tpa_Pay_Cnt, true, 12, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_F_Tpa_Gtd_Amt", pnd_F_Tpa_Gtd_Amt, true, 12, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Tpa_Div_Amt", pnd_F_Tpa_Div_Amt, true, 12, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_24", "TIAA ACCESS MTH", "BLUE", 14, 1, 15);
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt_12pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt.getValue(12+0), 
                true, 14, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units_12pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units.getValue(12+0), 
                true, 14, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars_12pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars.getValue(12+0), 
                true, 14, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt_12pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt.getValue(12+0), 
                true, 14, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt_12pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt.getValue(12+0), 
                true, 14, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt_12pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt.getValue(12+0), 
                true, 14, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt_12pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt.getValue(12+0), 
                true, 14, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_25", "TIAA ACCESS ANN", "BLUE", 15, 1, 15);
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt_12pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt.getValue(12+0), 
                true, 15, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units_12pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units.getValue(12+0), 
                true, 15, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars_12pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars.getValue(12+0), 
                true, 15, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt_12pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt.getValue(12+0), 
                true, 15, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt_12pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt.getValue(12+0), 
                true, 15, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt_12pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt.getValue(12+0), 
                true, 15, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt_12pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt.getValue(12+0), 
                true, 15, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_26", "TIAA REA MONTHLY", "BLUE", 16, 1, 16);
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt_2pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt.getValue(2+0), 
                true, 16, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units_2pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units.getValue(2+0), 
                true, 16, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars_2pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars.getValue(2+0), 
                true, 16, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt_2pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt.getValue(2+0), 
                true, 16, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt_2pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt.getValue(2+0), 
                true, 16, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt_2pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt.getValue(2+0), 
                true, 16, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt_2pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt.getValue(2+0), 
                true, 16, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_27", "TIAA REA ANNUAL", "BLUE", 17, 1, 15);
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt_2pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt.getValue(2+0), 
                true, 17, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units_2pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units.getValue(2+0), 
                true, 17, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars_2pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars.getValue(2+0), 
                true, 17, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt_2pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt.getValue(2+0), 
                true, 17, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt_2pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt.getValue(2+0), 
                true, 17, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt_2pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt.getValue(2+0), 
                true, 17, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt_2pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt.getValue(2+0), 
                true, 17, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_28", "STOCK", "BLUE", 19, 1, 5);
            uiForm.setUiLabel("label_29", "MONTHLY", "BLUE", 19, 8, 7);
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt_3pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt.getValue(3+0), 
                true, 19, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units_3pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units.getValue(3+0), 
                true, 19, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars_3pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars.getValue(3+0), 
                true, 19, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt_3pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt.getValue(3+0), 
                true, 19, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt_3pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt.getValue(3+0), 
                true, 19, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt_3pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt.getValue(3+0), 
                true, 19, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt_3pls0", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt.getValue(3+0), 
                true, 19, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_30", "MMA", "BLUE", 20, 1, 3);
            uiForm.setUiLabel("label_31", "MONTHLY", "BLUE", 20, 8, 7);
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt_3pls1", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt.getValue(3+1), 
                true, 20, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units_3pls1", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units.getValue(3+1), 
                true, 20, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars_3pls1", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars.getValue(3+1), 
                true, 20, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt_3pls1", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt.getValue(3+1), 
                true, 20, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt_3pls1", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt.getValue(3+1), 
                true, 20, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt_3pls1", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt.getValue(3+1), 
                true, 20, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt_3pls1", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt.getValue(3+1), 
                true, 20, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_32", "SOCIAL MONTHLY", "BLUE", 21, 1, 14);
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt_3pls2", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt.getValue(3+2), 
                true, 21, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units_3pls2", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units.getValue(3+2), 
                true, 21, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars_3pls2", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars.getValue(3+2), 
                true, 21, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt_3pls2", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt.getValue(3+2), 
                true, 21, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt_3pls2", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt.getValue(3+2), 
                true, 21, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt_3pls2", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt.getValue(3+2), 
                true, 21, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt_3pls2", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt.getValue(3+2), 
                true, 21, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_33", "BOND", "BLUE", 22, 1, 4);
            uiForm.setUiLabel("label_34", "MONTHLY", "BLUE", 22, 8, 7);
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt_3pls3", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt.getValue(3+3), 
                true, 22, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units_3pls3", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units.getValue(3+3), 
                true, 22, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars_3pls3", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars.getValue(3+3), 
                true, 22, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt_3pls3", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt.getValue(3+3), 
                true, 22, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt_3pls3", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt.getValue(3+3), 
                true, 22, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt_3pls3", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt.getValue(3+3), 
                true, 22, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt_3pls3", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt.getValue(3+3), 
                true, 22, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_35", "GLOBAL MONTHLY", "BLUE", 23, 1, 14);
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt_3pls4", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt.getValue(3+4), 
                true, 23, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units_3pls4", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units.getValue(3+4), 
                true, 23, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars_3pls4", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars.getValue(3+4), 
                true, 23, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt_3pls4", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt.getValue(3+4), 
                true, 23, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt_3pls4", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt.getValue(3+4), 
                true, 23, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt_3pls4", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt.getValue(3+4), 
                true, 23, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt_3pls4", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt.getValue(3+4), 
                true, 23, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_36", "GROWTH MONTHLY", "BLUE", 24, 1, 14);
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt_3pls5", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt.getValue(3+5), 
                true, 24, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units_3pls5", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units.getValue(3+5), 
                true, 24, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars_3pls5", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars.getValue(3+5), 
                true, 24, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt_3pls5", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt.getValue(3+5), 
                true, 24, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt_3pls5", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt.getValue(3+5), 
                true, 24, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt_3pls5", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt.getValue(3+5), 
                true, 24, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt_3pls5", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt.getValue(3+5), 
                true, 24, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_37", "EQUITY MONTHLY", "BLUE", 25, 1, 14);
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt_3pls6", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt.getValue(3+6), 
                true, 25, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units_3pls6", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units.getValue(3+6), 
                true, 25, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars_3pls6", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars.getValue(3+6), 
                true, 25, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt_3pls6", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt.getValue(3+6), 
                true, 25, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt_3pls6", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt.getValue(3+6), 
                true, 25, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt_3pls6", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt.getValue(3+6), 
                true, 25, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt_3pls6", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt.getValue(3+6), 
                true, 25, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_38", "I L B", "BLUE", 26, 1, 5);
            uiForm.setUiLabel("label_39", "MONTHLY", "BLUE", 26, 8, 7);
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt_3pls7", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Pay_Cnt.getValue(3+7), 
                true, 26, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units_3pls7", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Units.getValue(3+7), 
                true, 26, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars_3pls7", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Dollars.getValue(3+7), 
                true, 26, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt_3pls7", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Gtd_Amt.getValue(3+7), 
                true, 26, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt_3pls7", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Div_Amt.getValue(3+7), 
                true, 26, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt_3pls7", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Gtd_Amt.getValue(3+7), 
                true, 26, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt_3pls7", pnd_F_Cref_Mth_Minor_Accum_Pnd_F_Cref_Mth_Fin_Div_Amt.getValue(3+7), 
                true, 26, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_40", "TOTAL", "BLUE", 27, 1, 5);
            uiForm.setUiLabel("label_41", "MONTHLY", "BLUE", 27, 8, 7);
            uiForm.setUiControl("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Pay_Cnt", pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Pay_Cnt, 
                true, 27, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Units", pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Units, true, 
                27, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Dollars", pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Dollars, 
                true, 27, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Gtd_Amt", pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Gtd_Amt, 
                true, 27, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Div_Amt", pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Div_Amt, 
                true, 27, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Gtd_Amt", pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Gtd_Amt, 
                true, 27, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Div_Amt", pnd_F_Cref_Mth_Sub_Total_Accum_Pnd_F_Cref_Mth_Sub_Fin_Div_Amt, 
                true, 27, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_42", "STOCK", "BLUE", 29, 1, 5);
            uiForm.setUiLabel("label_43", "ANNUAL", "BLUE", 29, 8, 6);
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt_3pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt.getValue(3+0), 
                true, 29, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units_3pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units.getValue(3+0), 
                true, 29, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars_3pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars.getValue(3+0), 
                true, 29, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt_3pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt.getValue(3+0), 
                true, 29, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt_3pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt.getValue(3+0), 
                true, 29, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt_3pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt.getValue(3+0), 
                true, 29, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt_3pls0", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt.getValue(3+0), 
                true, 29, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_44", "MMA", "BLUE", 30, 1, 3);
            uiForm.setUiLabel("label_45", "ANNUAL", "BLUE", 30, 8, 6);
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt_3pls1", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt.getValue(3+1), 
                true, 30, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units_3pls1", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units.getValue(3+1), 
                true, 30, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars_3pls1", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars.getValue(3+1), 
                true, 30, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt_3pls1", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt.getValue(3+1), 
                true, 30, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt_3pls1", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt.getValue(3+1), 
                true, 30, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt_3pls1", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt.getValue(3+1), 
                true, 30, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt_3pls1", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt.getValue(3+1), 
                true, 30, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_46", "SOCIAL ANNUAL", "BLUE", 31, 1, 13);
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt_3pls2", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt.getValue(3+2), 
                true, 31, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units_3pls2", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units.getValue(3+2), 
                true, 31, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars_3pls2", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars.getValue(3+2), 
                true, 31, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt_3pls2", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt.getValue(3+2), 
                true, 31, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt_3pls2", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt.getValue(3+2), 
                true, 31, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt_3pls2", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt.getValue(3+2), 
                true, 31, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt_3pls2", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt.getValue(3+2), 
                true, 31, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_47", "BOND", "BLUE", 32, 1, 4);
            uiForm.setUiLabel("label_48", "ANNUAL", "BLUE", 32, 8, 6);
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt_3pls3", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt.getValue(3+3), 
                true, 32, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units_3pls3", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units.getValue(3+3), 
                true, 32, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars_3pls3", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars.getValue(3+3), 
                true, 32, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt_3pls3", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt.getValue(3+3), 
                true, 32, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt_3pls3", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt.getValue(3+3), 
                true, 32, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt_3pls3", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt.getValue(3+3), 
                true, 32, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt_3pls3", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt.getValue(3+3), 
                true, 32, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_49", "GLOBAL ANNUAL", "BLUE", 33, 1, 13);
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt_3pls4", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt.getValue(3+4), 
                true, 33, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units_3pls4", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units.getValue(3+4), 
                true, 33, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars_3pls4", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars.getValue(3+4), 
                true, 33, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt_3pls4", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt.getValue(3+4), 
                true, 33, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt_3pls4", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt.getValue(3+4), 
                true, 33, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt_3pls4", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt.getValue(3+4), 
                true, 33, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt_3pls4", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt.getValue(3+4), 
                true, 33, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_50", "GROWTH ANNUAL", "BLUE", 34, 1, 13);
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt_3pls5", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt.getValue(3+5), 
                true, 34, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units_3pls5", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units.getValue(3+5), 
                true, 34, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars_3pls5", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars.getValue(3+5), 
                true, 34, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt_3pls5", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt.getValue(3+5), 
                true, 34, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt_3pls5", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt.getValue(3+5), 
                true, 34, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt_3pls5", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt.getValue(3+5), 
                true, 34, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt_3pls5", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt.getValue(3+5), 
                true, 34, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_51", "EQUITY ANNUAL", "BLUE", 35, 1, 13);
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt_3pls6", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt.getValue(3+6), 
                true, 35, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units_3pls6", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units.getValue(3+6), 
                true, 35, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars_3pls6", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars.getValue(3+6), 
                true, 35, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt_3pls6", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt.getValue(3+6), 
                true, 35, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt_3pls6", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt.getValue(3+6), 
                true, 35, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt_3pls6", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt.getValue(3+6), 
                true, 35, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt_3pls6", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt.getValue(3+6), 
                true, 35, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_52", "I L B", "BLUE", 36, 1, 5);
            uiForm.setUiLabel("label_53", "ANNUAL", "BLUE", 36, 8, 6);
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt_3pls7", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Pay_Cnt.getValue(3+7), 
                true, 36, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units_3pls7", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Units.getValue(3+7), 
                true, 36, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars_3pls7", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Dollars.getValue(3+7), 
                true, 36, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt_3pls7", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Gtd_Amt.getValue(3+7), 
                true, 36, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt_3pls7", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Divid_Amt.getValue(3+7), 
                true, 36, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt_3pls7", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Gtd_Amt.getValue(3+7), 
                true, 36, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt_3pls7", pnd_F_Cref_Ann_Minor_Accum_Pnd_F_Cref_Ann_Fin_Div_Amt.getValue(3+7), 
                true, 36, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_54", "TOTAL", "BLUE", 37, 1, 5);
            uiForm.setUiLabel("label_55", "ANNUAL", "BLUE", 37, 8, 6);
            uiForm.setUiControl("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Pay_Cnt", pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Pay_Cnt, 
                true, 37, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Units", pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Units, true, 
                37, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Dollars", pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Dollars, 
                true, 37, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Gtd_Amt", pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Gtd_Amt, 
                true, 37, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Div_Amt", pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Div_Amt, 
                true, 37, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Gtd_Amt", pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Gtd_Amt, 
                true, 37, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Div_Amt", pnd_F_Cref_Ann_Sub_Total_Accum_Pnd_F_Cref_Ann_Sub_Fin_Div_Amt, 
                true, 37, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_56", "TOTAL ALL", "BLUE", 40, 1, 9);
            uiForm.setUiControl("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt", pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Pay_Cnt, true, 40, 15, 13, 
                "WHITE", "Z,ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Dollars", pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Dollars, true, 40, 45, 18, 
                "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Div_Amt", pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Div_Amt, true, 40, 79, 18, 
                "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Div_Amt", pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Div_Amt, true, 40, 
                113, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Units", pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Units, true, 41, 29, 16, "WHITE", 
                "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Gtd_Amt", pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Gtd_Amt, true, 41, 62, 18, 
                "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Gtd_Amt", pnd_F_Total_Tiaa_Cref_Accum_Pnd_F_Total_Fin_Gtd_Amt, true, 41, 
                96, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
