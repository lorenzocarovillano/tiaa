/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:33 PM
**        *   FROM NATURAL MAP   :  Adsm860
************************************************************
**        * FILE NAME               : Adsm860.java
**        * CLASS NAME              : Adsm860
**        * INSTANCE NAME           : Adsm860
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #CREF-HEADER-AMT.#CREF-HEADER-ACCOUNT                                                                                    *     #CREF-HEADER-AMT.#CREF-HEADER-GRD-AMT 
    *     #CREF-HEADER-AMT.#CREF-HEADER-GRD-TYP                                                                                    *     #CREF-HEADER-AMT.#CREF-HEADER-RTB-AMT 
    *     #CREF-HEADER-AMT.#CREF-HEADER-RTB-TYP                                                                                    *     #CREF-HEADER-AMT.#CREF-HEADER-SETTLE-AMT 
    *     #CREF-HEADER-AMT.#CREF-HEADER-SETTLE-TYP
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm860 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Cref_Header_Amt_Pnd_Cref_Header_Account;
    private DbsField pnd_Cref_Header_Amt_Pnd_Cref_Header_Grd_Amt;
    private DbsField pnd_Cref_Header_Amt_Pnd_Cref_Header_Grd_Typ;
    private DbsField pnd_Cref_Header_Amt_Pnd_Cref_Header_Rtb_Amt;
    private DbsField pnd_Cref_Header_Amt_Pnd_Cref_Header_Rtb_Typ;
    private DbsField pnd_Cref_Header_Amt_Pnd_Cref_Header_Settle_Amt;
    private DbsField pnd_Cref_Header_Amt_Pnd_Cref_Header_Settle_Typ;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Cref_Header_Amt_Pnd_Cref_Header_Account = parameters.newFieldInRecord("pnd_Cref_Header_Amt_Pnd_Cref_Header_Account", "#CREF-HEADER-AMT.#CREF-HEADER-ACCOUNT", 
            FieldType.STRING, 4);
        pnd_Cref_Header_Amt_Pnd_Cref_Header_Grd_Amt = parameters.newFieldInRecord("pnd_Cref_Header_Amt_Pnd_Cref_Header_Grd_Amt", "#CREF-HEADER-AMT.#CREF-HEADER-GRD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Cref_Header_Amt_Pnd_Cref_Header_Grd_Typ = parameters.newFieldInRecord("pnd_Cref_Header_Amt_Pnd_Cref_Header_Grd_Typ", "#CREF-HEADER-AMT.#CREF-HEADER-GRD-TYP", 
            FieldType.STRING, 1);
        pnd_Cref_Header_Amt_Pnd_Cref_Header_Rtb_Amt = parameters.newFieldInRecord("pnd_Cref_Header_Amt_Pnd_Cref_Header_Rtb_Amt", "#CREF-HEADER-AMT.#CREF-HEADER-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Cref_Header_Amt_Pnd_Cref_Header_Rtb_Typ = parameters.newFieldInRecord("pnd_Cref_Header_Amt_Pnd_Cref_Header_Rtb_Typ", "#CREF-HEADER-AMT.#CREF-HEADER-RTB-TYP", 
            FieldType.STRING, 1);
        pnd_Cref_Header_Amt_Pnd_Cref_Header_Settle_Amt = parameters.newFieldInRecord("pnd_Cref_Header_Amt_Pnd_Cref_Header_Settle_Amt", "#CREF-HEADER-AMT.#CREF-HEADER-SETTLE-AMT", 
            FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Cref_Header_Amt_Pnd_Cref_Header_Settle_Typ = parameters.newFieldInRecord("pnd_Cref_Header_Amt_Pnd_Cref_Header_Settle_Typ", "#CREF-HEADER-AMT.#CREF-HEADER-SETTLE-TYP", 
            FieldType.STRING, 1);
        parameters.reset();
    }

    public Adsm860() throws Exception
    {
        super("Adsm860");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=080 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm860", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm860"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Cref_Header_Amt_Pnd_Cref_Header_Account", pnd_Cref_Header_Amt_Pnd_Cref_Header_Account, true, 1, 2, 4, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Header_Amt_Pnd_Cref_Header_Settle_Amt", pnd_Cref_Header_Amt_Pnd_Cref_Header_Settle_Amt, true, 1, 14, 12, "WHITE", 
                "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Header_Amt_Pnd_Cref_Header_Settle_Typ", pnd_Cref_Header_Amt_Pnd_Cref_Header_Settle_Typ, true, 1, 27, 1, "WHITE", 
                true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Header_Amt_Pnd_Cref_Header_Rtb_Amt", pnd_Cref_Header_Amt_Pnd_Cref_Header_Rtb_Amt, true, 1, 29, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Header_Amt_Pnd_Cref_Header_Rtb_Typ", pnd_Cref_Header_Amt_Pnd_Cref_Header_Rtb_Typ, true, 1, 44, 1, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Header_Amt_Pnd_Cref_Header_Grd_Amt", pnd_Cref_Header_Amt_Pnd_Cref_Header_Grd_Amt, true, 1, 46, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Header_Amt_Pnd_Cref_Header_Grd_Typ", pnd_Cref_Header_Amt_Pnd_Cref_Header_Grd_Typ, true, 1, 61, 1, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
