/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:00 PM
**        *   FROM NATURAL MAP   :  Adsm776
************************************************************
**        * FILE NAME               : Adsm776.java
**        * CLASS NAME              : Adsm776
**        * INSTANCE NAME           : Adsm776
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #ASTERISK #COMMENT1 #COMMENT2                                                                                            *     #TIAA-FINAL-TOTALS.#TIAA-DA-ACCUM-FINAL-TOTAL 
    *     #TIAA-FINAL-TOTALS.#TIAA-RTB-FINAL-TOTAL                                                                                 *     #TIAA-FINAL-TOTALS.#TIAA-SETTLED-FINAL-TOTAL 
    *     #TIAA-FINAL-TOTALS.#TIAA-SETTLED-RTB-FINAL-TOTAL
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm776 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Asterisk;
    private DbsField pnd_Comment1;
    private DbsField pnd_Comment2;
    private DbsField pnd_Tiaa_Final_Totals_Pnd_Tiaa_Da_Accum_Final_Total;
    private DbsField pnd_Tiaa_Final_Totals_Pnd_Tiaa_Rtb_Final_Total;
    private DbsField pnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Final_Total;
    private DbsField pnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Rtb_Final_Total;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Asterisk = parameters.newFieldInRecord("pnd_Asterisk", "#ASTERISK", FieldType.STRING, 1);
        pnd_Comment1 = parameters.newFieldInRecord("pnd_Comment1", "#COMMENT1", FieldType.STRING, 79);
        pnd_Comment2 = parameters.newFieldInRecord("pnd_Comment2", "#COMMENT2", FieldType.STRING, 79);
        pnd_Tiaa_Final_Totals_Pnd_Tiaa_Da_Accum_Final_Total = parameters.newFieldInRecord("pnd_Tiaa_Final_Totals_Pnd_Tiaa_Da_Accum_Final_Total", "#TIAA-FINAL-TOTALS.#TIAA-DA-ACCUM-FINAL-TOTAL", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Final_Totals_Pnd_Tiaa_Rtb_Final_Total = parameters.newFieldInRecord("pnd_Tiaa_Final_Totals_Pnd_Tiaa_Rtb_Final_Total", "#TIAA-FINAL-TOTALS.#TIAA-RTB-FINAL-TOTAL", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Final_Total = parameters.newFieldInRecord("pnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Final_Total", "#TIAA-FINAL-TOTALS.#TIAA-SETTLED-FINAL-TOTAL", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Rtb_Final_Total = parameters.newFieldInRecord("pnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Rtb_Final_Total", 
            "#TIAA-FINAL-TOTALS.#TIAA-SETTLED-RTB-FINAL-TOTAL", FieldType.PACKED_DECIMAL, 15, 2);
        parameters.reset();
    }

    public Adsm776() throws Exception
    {
        super("Adsm776");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm776", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm776"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "------------------------------------------------------------------------------", "", 1, 1, 78);
            uiForm.setUiLabel("label_2", "TOT", "BLUE", 2, 1, 3);
            uiForm.setUiControl("pnd_Tiaa_Final_Totals_Pnd_Tiaa_Da_Accum_Final_Total", pnd_Tiaa_Final_Totals_Pnd_Tiaa_Da_Accum_Final_Total, true, 2, 5, 
                18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Asterisk", pnd_Asterisk, true, 2, 24, 1, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Final_Total", pnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Final_Total, true, 2, 26, 
                16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Final_Totals_Pnd_Tiaa_Rtb_Final_Total", pnd_Tiaa_Final_Totals_Pnd_Tiaa_Rtb_Final_Total, true, 2, 43, 18, "WHITE", 
                "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Rtb_Final_Total", pnd_Tiaa_Final_Totals_Pnd_Tiaa_Settled_Rtb_Final_Total, true, 
                2, 62, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "---", "BLUE", 3, 1, 3);
            uiForm.setUiControl("pnd_Comment1", pnd_Comment1, true, 5, 1, 79, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Comment2", pnd_Comment2, true, 6, 1, 79, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
