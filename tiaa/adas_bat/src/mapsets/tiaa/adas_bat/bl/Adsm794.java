/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:19 PM
**        *   FROM NATURAL MAP   :  Adsm794
************************************************************
**        * FILE NAME               : Adsm794.java
**        * CLASS NAME              : Adsm794
**        * INSTANCE NAME           : Adsm794
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #A-C-ANN-FIN-IVC-AMOUNT.#A-C-ANN-FIN-IVC-AMT                                                                             *     #A-C-ANN-MINOR-ACCUM.#A-C-ANN-DIVID-AMT(*) 
    *     #A-C-ANN-MINOR-ACCUM.#A-C-ANN-DOLLARS(*)                                                                                 *     #A-C-ANN-MINOR-ACCUM.#A-C-ANN-FIN-DIV-AMT(*) 
    *     #A-C-ANN-MINOR-ACCUM.#A-C-ANN-FIN-GTD-AMT(*)                                                                             *     #A-C-ANN-MINOR-ACCUM.#A-C-ANN-GTD-AMT(*) 
    *     #A-C-ANN-MINOR-ACCUM.#A-C-ANN-PAY-CNT(*)                                                                                 *     #A-C-ANN-MINOR-ACCUM.#A-C-ANN-UNITS(*) 
    *     #A-C-MTH-FIN-IVC-AMOUNT.#A-C-MTH-FIN-IVC-AMT                                                                             *     #A-C-MTH-MINOR-ACCUM.#A-C-MTH-DIV-AMT(*) 
    *     #A-C-MTH-MINOR-ACCUM.#A-C-MTH-DOLLARS(*)                                                                                 *     #A-C-MTH-MINOR-ACCUM.#A-C-MTH-FIN-DIV-AMT(*) 
    *     #A-C-MTH-MINOR-ACCUM.#A-C-MTH-FIN-GTD-AMT(*)                                                                             *     #A-C-MTH-MINOR-ACCUM.#A-C-MTH-GTD-AMT(*) 
    *     #A-C-MTH-MINOR-ACCUM.#A-C-MTH-PAY-CNT(*)                                                                                 *     #A-C-MTH-MINOR-ACCUM.#A-C-MTH-UNITS(*) 
    *     #A-CREF-ANN-SUB-TOTAL-ACCUM.#A-CREF-ANN-SUB-DIV-AMT                                                                      *     #A-CREF-ANN-SUB-TOTAL-ACCUM.#A-CREF-ANN-SUB-DOLLARS 
    *     #A-CREF-ANN-SUB-TOTAL-ACCUM.#A-CREF-ANN-SUB-FIN-DIV-AMT                                                                  *     #A-CREF-ANN-SUB-TOTAL-ACCUM.#A-CREF-ANN-SUB-FIN-GTD-AMT 
    *     #A-CREF-ANN-SUB-TOTAL-ACCUM.#A-CREF-ANN-SUB-FIN-IVC-AMT                                                                  *     #A-CREF-ANN-SUB-TOTAL-ACCUM.#A-CREF-ANN-SUB-GTD-AMT 
    *     #A-CREF-ANN-SUB-TOTAL-ACCUM.#A-CREF-ANN-SUB-PAY-CNT                                                                      *     #A-CREF-ANN-SUB-TOTAL-ACCUM.#A-CREF-ANN-SUB-UNITS 
    *     #A-CREF-MTH-SUB-TOTAL-ACCUM.#A-CREF-MTH-SUB-DIV-AMT                                                                      *     #A-CREF-MTH-SUB-TOTAL-ACCUM.#A-CREF-MTH-SUB-DOLLARS 
    *     #A-CREF-MTH-SUB-TOTAL-ACCUM.#A-CREF-MTH-SUB-FIN-DIV-AMT                                                                  *     #A-CREF-MTH-SUB-TOTAL-ACCUM.#A-CREF-MTH-SUB-FIN-GTD-AMT 
    *     #A-CREF-MTH-SUB-TOTAL-ACCUM.#A-CREF-MTH-SUB-FIN-IVC-AMT                                                                  *     #A-CREF-MTH-SUB-TOTAL-ACCUM.#A-CREF-MTH-SUB-GTD-AMT 
    *     #A-CREF-MTH-SUB-TOTAL-ACCUM.#A-CREF-MTH-SUB-PAY-CNT                                                                      *     #A-CREF-MTH-SUB-TOTAL-ACCUM.#A-CREF-MTH-SUB-UNITS 
    *     #A-TIAA-MINOR-ACCUM.#A-TIAA-GRD-DIV-AMT                                                                                  *     #A-TIAA-MINOR-ACCUM.#A-TIAA-GRD-DOLLARS 
    *     #A-TIAA-MINOR-ACCUM.#A-TIAA-GRD-FIN-DIV-AMT                                                                              *     #A-TIAA-MINOR-ACCUM.#A-TIAA-GRD-FIN-GTD-AMT 
    *     #A-TIAA-MINOR-ACCUM.#A-TIAA-GRD-FIN-IVC-AMT                                                                              *     #A-TIAA-MINOR-ACCUM.#A-TIAA-GRD-GTD-AMT 
    *     #A-TIAA-MINOR-ACCUM.#A-TIAA-GRD-PAY-CNT                                                                                  *     #A-TIAA-MINOR-ACCUM.#A-TIAA-GRD-UNITS 
    *     #A-TIAA-MINOR-ACCUM.#A-TIAA-IPRO-STD-DIV-AMT                                                                             *     #A-TIAA-MINOR-ACCUM.#A-TIAA-IPRO-STD-DOLLARS 
    *     #A-TIAA-MINOR-ACCUM.#A-TIAA-IPRO-STD-FIN-DIV-AMT                                                                         *     #A-TIAA-MINOR-ACCUM.#A-TIAA-IPRO-STD-FIN-GTD-AMT 
    *     #A-TIAA-MINOR-ACCUM.#A-TIAA-IPRO-STD-FIN-IVC-AMT                                                                         *     #A-TIAA-MINOR-ACCUM.#A-TIAA-IPRO-STD-GTD-AMT 
    *     #A-TIAA-MINOR-ACCUM.#A-TIAA-IPRO-STD-PAY-CNT                                                                             *     #A-TIAA-MINOR-ACCUM.#A-TIAA-IPRO-STD-UNITS 
    *     #A-TIAA-MINOR-ACCUM.#A-TIAA-STD-DIV-AMT                                                                                  *     #A-TIAA-MINOR-ACCUM.#A-TIAA-STD-DOLLARS 
    *     #A-TIAA-MINOR-ACCUM.#A-TIAA-STD-FIN-DIV-AMT                                                                              *     #A-TIAA-MINOR-ACCUM.#A-TIAA-STD-FIN-GTD-AMT 
    *     #A-TIAA-MINOR-ACCUM.#A-TIAA-STD-FIN-IVC-AMT                                                                              *     #A-TIAA-MINOR-ACCUM.#A-TIAA-STD-GTD-AMT 
    *     #A-TIAA-MINOR-ACCUM.#A-TIAA-STD-PAY-CNT                                                                                  *     #A-TIAA-MINOR-ACCUM.#A-TIAA-STD-UNITS 
    *     #A-TIAA-MINOR-ACCUM.#A-TIAA-TPA-STD-DIV-AMT                                                                              *     #A-TIAA-MINOR-ACCUM.#A-TIAA-TPA-STD-DOLLARS 
    *     #A-TIAA-MINOR-ACCUM.#A-TIAA-TPA-STD-FIN-DIV-AMT                                                                          *     #A-TIAA-MINOR-ACCUM.#A-TIAA-TPA-STD-FIN-GTD-AMT 
    *     #A-TIAA-MINOR-ACCUM.#A-TIAA-TPA-STD-FIN-IVC-AMT                                                                          *     #A-TIAA-MINOR-ACCUM.#A-TIAA-TPA-STD-GTD-AMT 
    *     #A-TIAA-MINOR-ACCUM.#A-TIAA-TPA-STD-PAY-CNT                                                                              *     #A-TIAA-MINOR-ACCUM.#A-TIAA-TPA-STD-UNITS 
    *     #A-TIAA-SUB-ACCUM.#A-TIAA-SUB-DIVID-AMT                                                                                  *     #A-TIAA-SUB-ACCUM.#A-TIAA-SUB-DOLLARS 
    *     #A-TIAA-SUB-ACCUM.#A-TIAA-SUB-FIN-DIV-AMT                                                                                *     #A-TIAA-SUB-ACCUM.#A-TIAA-SUB-FIN-GTD-AMT 
    *     #A-TIAA-SUB-ACCUM.#A-TIAA-SUB-FIN-IVC-AMT                                                                                *     #A-TIAA-SUB-ACCUM.#A-TIAA-SUB-GTD-AMT 
    *     #A-TIAA-SUB-ACCUM.#A-TIAA-SUB-PAY-CNT                                                                                    *     #A-TIAA-SUB-ACCUM.#A-TIAA-SUB-UNITS 
    *     #A-TOTAL-TIAA-CREF-ACCUM.#A-TOTAL-DIV-AMT                                                                                *     #A-TOTAL-TIAA-CREF-ACCUM.#A-TOTAL-DOLLARS 
    *     #A-TOTAL-TIAA-CREF-ACCUM.#A-TOTAL-FIN-DIV-AMT                                                                            *     #A-TOTAL-TIAA-CREF-ACCUM.#A-TOTAL-FIN-GTD-AMT 
    *     #A-TOTAL-TIAA-CREF-ACCUM.#A-TOTAL-FIN-IVC-AMT                                                                            *     #A-TOTAL-TIAA-CREF-ACCUM.#A-TOTAL-GTD-AMT 
    *     #A-TOTAL-TIAA-CREF-ACCUM.#A-TOTAL-PAY-CNT                                                                                *     #A-TOTAL-TIAA-CREF-ACCUM.#A-TOTAL-UNITS 
    #PAYMENT-DUE-DATE
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm794 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_A_C_Ann_Fin_Ivc_Amount_Pnd_A_C_Ann_Fin_Ivc_Amt;
    private DbsField pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt;
    private DbsField pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars;
    private DbsField pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt;
    private DbsField pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt;
    private DbsField pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt;
    private DbsField pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt;
    private DbsField pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units;
    private DbsField pnd_A_C_Mth_Fin_Ivc_Amount_Pnd_A_C_Mth_Fin_Ivc_Amt;
    private DbsField pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt;
    private DbsField pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars;
    private DbsField pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt;
    private DbsField pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt;
    private DbsField pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt;
    private DbsField pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt;
    private DbsField pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units;
    private DbsField pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Div_Amt;
    private DbsField pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Dollars;
    private DbsField pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Div_Amt;
    private DbsField pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Gtd_Amt;
    private DbsField pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Ivc_Amt;
    private DbsField pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Gtd_Amt;
    private DbsField pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Pay_Cnt;
    private DbsField pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Units;
    private DbsField pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Div_Amt;
    private DbsField pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Dollars;
    private DbsField pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Div_Amt;
    private DbsField pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Gtd_Amt;
    private DbsField pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Ivc_Amt;
    private DbsField pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Gtd_Amt;
    private DbsField pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Pay_Cnt;
    private DbsField pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Units;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Div_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Dollars;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Div_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Gtd_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Ivc_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Gtd_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Pay_Cnt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Units;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Div_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Dollars;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Div_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Gtd_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Ivc_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Gtd_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Pay_Cnt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Units;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Div_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Dollars;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Div_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Gtd_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Ivc_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Gtd_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Pay_Cnt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Units;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Div_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Dollars;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Div_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Gtd_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Ivc_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Gtd_Amt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Pay_Cnt;
    private DbsField pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Units;
    private DbsField pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Divid_Amt;
    private DbsField pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Dollars;
    private DbsField pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Div_Amt;
    private DbsField pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Gtd_Amt;
    private DbsField pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Ivc_Amt;
    private DbsField pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Gtd_Amt;
    private DbsField pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Pay_Cnt;
    private DbsField pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Units;
    private DbsField pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Div_Amt;
    private DbsField pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Dollars;
    private DbsField pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Div_Amt;
    private DbsField pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Gtd_Amt;
    private DbsField pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Ivc_Amt;
    private DbsField pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Gtd_Amt;
    private DbsField pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Pay_Cnt;
    private DbsField pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Units;
    private DbsField pnd_Payment_Due_Date;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_A_C_Ann_Fin_Ivc_Amount_Pnd_A_C_Ann_Fin_Ivc_Amt = parameters.newFieldInRecord("pnd_A_C_Ann_Fin_Ivc_Amount_Pnd_A_C_Ann_Fin_Ivc_Amt", "#A-C-ANN-FIN-IVC-AMOUNT.#A-C-ANN-FIN-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 10, 2);
        pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt = parameters.newFieldArrayInRecord("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt", "#A-C-ANN-MINOR-ACCUM.#A-C-ANN-DIVID-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars = parameters.newFieldArrayInRecord("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars", "#A-C-ANN-MINOR-ACCUM.#A-C-ANN-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt = parameters.newFieldArrayInRecord("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt", "#A-C-ANN-MINOR-ACCUM.#A-C-ANN-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt", "#A-C-ANN-MINOR-ACCUM.#A-C-ANN-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt", "#A-C-ANN-MINOR-ACCUM.#A-C-ANN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt = parameters.newFieldArrayInRecord("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt", "#A-C-ANN-MINOR-ACCUM.#A-C-ANN-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units = parameters.newFieldArrayInRecord("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units", "#A-C-ANN-MINOR-ACCUM.#A-C-ANN-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4, new DbsArrayController(1, 20));
        pnd_A_C_Mth_Fin_Ivc_Amount_Pnd_A_C_Mth_Fin_Ivc_Amt = parameters.newFieldInRecord("pnd_A_C_Mth_Fin_Ivc_Amount_Pnd_A_C_Mth_Fin_Ivc_Amt", "#A-C-MTH-FIN-IVC-AMOUNT.#A-C-MTH-FIN-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 10, 2);
        pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt = parameters.newFieldArrayInRecord("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt", "#A-C-MTH-MINOR-ACCUM.#A-C-MTH-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars = parameters.newFieldArrayInRecord("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars", "#A-C-MTH-MINOR-ACCUM.#A-C-MTH-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt = parameters.newFieldArrayInRecord("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt", "#A-C-MTH-MINOR-ACCUM.#A-C-MTH-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt", "#A-C-MTH-MINOR-ACCUM.#A-C-MTH-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt", "#A-C-MTH-MINOR-ACCUM.#A-C-MTH-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt = parameters.newFieldArrayInRecord("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt", "#A-C-MTH-MINOR-ACCUM.#A-C-MTH-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units = parameters.newFieldArrayInRecord("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units", "#A-C-MTH-MINOR-ACCUM.#A-C-MTH-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4, new DbsArrayController(1, 20));
        pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Div_Amt = parameters.newFieldInRecord("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Div_Amt", 
            "#A-CREF-ANN-SUB-TOTAL-ACCUM.#A-CREF-ANN-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Dollars = parameters.newFieldInRecord("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Dollars", 
            "#A-CREF-ANN-SUB-TOTAL-ACCUM.#A-CREF-ANN-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Div_Amt = parameters.newFieldInRecord("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Div_Amt", 
            "#A-CREF-ANN-SUB-TOTAL-ACCUM.#A-CREF-ANN-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Gtd_Amt", 
            "#A-CREF-ANN-SUB-TOTAL-ACCUM.#A-CREF-ANN-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Ivc_Amt = parameters.newFieldInRecord("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Ivc_Amt", 
            "#A-CREF-ANN-SUB-TOTAL-ACCUM.#A-CREF-ANN-SUB-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Gtd_Amt = parameters.newFieldInRecord("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Gtd_Amt", 
            "#A-CREF-ANN-SUB-TOTAL-ACCUM.#A-CREF-ANN-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Pay_Cnt = parameters.newFieldInRecord("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Pay_Cnt", 
            "#A-CREF-ANN-SUB-TOTAL-ACCUM.#A-CREF-ANN-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Units = parameters.newFieldInRecord("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Units", 
            "#A-CREF-ANN-SUB-TOTAL-ACCUM.#A-CREF-ANN-SUB-UNITS", FieldType.PACKED_DECIMAL, 11, 4);
        pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Div_Amt = parameters.newFieldInRecord("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Div_Amt", 
            "#A-CREF-MTH-SUB-TOTAL-ACCUM.#A-CREF-MTH-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Dollars = parameters.newFieldInRecord("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Dollars", 
            "#A-CREF-MTH-SUB-TOTAL-ACCUM.#A-CREF-MTH-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Div_Amt = parameters.newFieldInRecord("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Div_Amt", 
            "#A-CREF-MTH-SUB-TOTAL-ACCUM.#A-CREF-MTH-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Gtd_Amt", 
            "#A-CREF-MTH-SUB-TOTAL-ACCUM.#A-CREF-MTH-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Ivc_Amt = parameters.newFieldInRecord("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Ivc_Amt", 
            "#A-CREF-MTH-SUB-TOTAL-ACCUM.#A-CREF-MTH-SUB-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Gtd_Amt = parameters.newFieldInRecord("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Gtd_Amt", 
            "#A-CREF-MTH-SUB-TOTAL-ACCUM.#A-CREF-MTH-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Pay_Cnt = parameters.newFieldInRecord("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Pay_Cnt", 
            "#A-CREF-MTH-SUB-TOTAL-ACCUM.#A-CREF-MTH-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Units = parameters.newFieldInRecord("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Units", 
            "#A-CREF-MTH-SUB-TOTAL-ACCUM.#A-CREF-MTH-SUB-UNITS", FieldType.PACKED_DECIMAL, 11, 4);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Div_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Div_Amt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-GRD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Dollars = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Dollars", "#A-TIAA-MINOR-ACCUM.#A-TIAA-GRD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Div_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Div_Amt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-GRD-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Gtd_Amt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-GRD-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Ivc_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Ivc_Amt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-GRD-FIN-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 10, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Gtd_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Gtd_Amt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-GRD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Pay_Cnt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Pay_Cnt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-GRD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Units = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Units", "#A-TIAA-MINOR-ACCUM.#A-TIAA-GRD-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Div_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Div_Amt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-IPRO-STD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Dollars = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Dollars", "#A-TIAA-MINOR-ACCUM.#A-TIAA-IPRO-STD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Div_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Div_Amt", 
            "#A-TIAA-MINOR-ACCUM.#A-TIAA-IPRO-STD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Gtd_Amt", 
            "#A-TIAA-MINOR-ACCUM.#A-TIAA-IPRO-STD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Ivc_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Ivc_Amt", 
            "#A-TIAA-MINOR-ACCUM.#A-TIAA-IPRO-STD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Gtd_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Gtd_Amt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-IPRO-STD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Pay_Cnt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Pay_Cnt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-IPRO-STD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Units = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Units", "#A-TIAA-MINOR-ACCUM.#A-TIAA-IPRO-STD-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Div_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Div_Amt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-STD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Dollars = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Dollars", "#A-TIAA-MINOR-ACCUM.#A-TIAA-STD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Div_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Div_Amt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-STD-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Gtd_Amt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-STD-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Ivc_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Ivc_Amt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-STD-FIN-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 10, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Gtd_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Gtd_Amt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-STD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Pay_Cnt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Pay_Cnt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-STD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Units = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Units", "#A-TIAA-MINOR-ACCUM.#A-TIAA-STD-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Div_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Div_Amt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-TPA-STD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Dollars = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Dollars", "#A-TIAA-MINOR-ACCUM.#A-TIAA-TPA-STD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Div_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Div_Amt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-TPA-STD-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Gtd_Amt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-TPA-STD-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Ivc_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Ivc_Amt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-TPA-STD-FIN-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 10, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Gtd_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Gtd_Amt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-TPA-STD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Pay_Cnt = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Pay_Cnt", "#A-TIAA-MINOR-ACCUM.#A-TIAA-TPA-STD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Units = parameters.newFieldInRecord("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Units", "#A-TIAA-MINOR-ACCUM.#A-TIAA-TPA-STD-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4);
        pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Divid_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Divid_Amt", "#A-TIAA-SUB-ACCUM.#A-TIAA-SUB-DIVID-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Dollars = parameters.newFieldInRecord("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Dollars", "#A-TIAA-SUB-ACCUM.#A-TIAA-SUB-DOLLARS", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Div_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Div_Amt", "#A-TIAA-SUB-ACCUM.#A-TIAA-SUB-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Gtd_Amt", "#A-TIAA-SUB-ACCUM.#A-TIAA-SUB-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Ivc_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Ivc_Amt", "#A-TIAA-SUB-ACCUM.#A-TIAA-SUB-FIN-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 10, 2);
        pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Gtd_Amt = parameters.newFieldInRecord("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Gtd_Amt", "#A-TIAA-SUB-ACCUM.#A-TIAA-SUB-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Pay_Cnt = parameters.newFieldInRecord("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Pay_Cnt", "#A-TIAA-SUB-ACCUM.#A-TIAA-SUB-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Units = parameters.newFieldInRecord("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Units", "#A-TIAA-SUB-ACCUM.#A-TIAA-SUB-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4);
        pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Div_Amt = parameters.newFieldInRecord("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Div_Amt", "#A-TOTAL-TIAA-CREF-ACCUM.#A-TOTAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Dollars = parameters.newFieldInRecord("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Dollars", "#A-TOTAL-TIAA-CREF-ACCUM.#A-TOTAL-DOLLARS", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Div_Amt = parameters.newFieldInRecord("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Div_Amt", "#A-TOTAL-TIAA-CREF-ACCUM.#A-TOTAL-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Gtd_Amt", "#A-TOTAL-TIAA-CREF-ACCUM.#A-TOTAL-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Ivc_Amt = parameters.newFieldInRecord("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Ivc_Amt", "#A-TOTAL-TIAA-CREF-ACCUM.#A-TOTAL-FIN-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Gtd_Amt = parameters.newFieldInRecord("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Gtd_Amt", "#A-TOTAL-TIAA-CREF-ACCUM.#A-TOTAL-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Pay_Cnt = parameters.newFieldInRecord("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Pay_Cnt", "#A-TOTAL-TIAA-CREF-ACCUM.#A-TOTAL-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Units = parameters.newFieldInRecord("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Units", "#A-TOTAL-TIAA-CREF-ACCUM.#A-TOTAL-UNITS", 
            FieldType.PACKED_DECIMAL, 14, 4);
        pnd_Payment_Due_Date = parameters.newFieldInRecord("pnd_Payment_Due_Date", "#PAYMENT-DUE-DATE", FieldType.NUMERIC, 8);
        parameters.reset();
    }

    public Adsm794() throws Exception
    {
        super("Adsm794");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm794", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm794"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "ANNUAL MODE", "BLUE", 1, 1, 11);
            uiForm.setUiLabel("label_2", "PAYMENT DUE DATE", "BLUE", 2, 1, 16);
            uiForm.setUiControl("pnd_Payment_Due_Date", pnd_Payment_Due_Date, true, 3, 3, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "ADD'L", "BLUE", 3, 28, 5);
            uiForm.setUiLabel("label_4", "ADD'L", "BLUE", 3, 45, 5);
            uiForm.setUiLabel("label_5", "ADD'L", "BLUE", 3, 61, 5);
            uiForm.setUiLabel("label_6", "ADD'L", "BLUE", 3, 77, 5);
            uiForm.setUiLabel("label_7", "ADD'L", "BLUE", 3, 95, 5);
            uiForm.setUiLabel("label_8", "ADD'L", "BLUE", 3, 110, 5);
            uiForm.setUiLabel("label_9", "ADD'L", "BLUE", 3, 126, 5);
            uiForm.setUiLabel("label_10", "PAYMENT", "BLUE", 4, 14, 7);
            uiForm.setUiLabel("label_11", "CREF", "BLUE", 4, 28, 4);
            uiForm.setUiLabel("label_12", "CREF", "BLUE", 4, 45, 4);
            uiForm.setUiLabel("label_13", "TIAA", "BLUE", 4, 61, 4);
            uiForm.setUiLabel("label_14", "TIAA", "BLUE", 4, 77, 4);
            uiForm.setUiLabel("label_15", "FINAL", "BLUE", 4, 95, 5);
            uiForm.setUiLabel("label_16", "FINAL", "BLUE", 4, 110, 5);
            uiForm.setUiLabel("label_17", "IVC", "BLUE", 4, 126, 3);
            uiForm.setUiLabel("label_18", "PRODUCT", "BLUE", 5, 1, 7);
            uiForm.setUiLabel("label_19", "COUNTS", "BLUE", 5, 14, 6);
            uiForm.setUiLabel("label_20", "UNITS", "BLUE", 5, 27, 5);
            uiForm.setUiLabel("label_21", "DOLLARS", "BLUE", 5, 44, 7);
            uiForm.setUiLabel("label_22", "GTD/AMT", "BLUE", 5, 60, 7);
            uiForm.setUiLabel("label_23", "DIVID/AMT", "BLUE", 5, 75, 9);
            uiForm.setUiLabel("label_24", "GTD/AMT", "BLUE", 5, 94, 7);
            uiForm.setUiLabel("label_25", "DIVID/AMT", "BLUE", 5, 108, 9);
            uiForm.setUiLabel("label_26", "AMOUNT", "BLUE", 5, 125, 6);
            uiForm.setUiLabel("label_27", "TIAA STD", "BLUE", 7, 1, 8);
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Pay_Cnt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Pay_Cnt, true, 7, 10, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Units", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Units, true, 7, 20, 12, "WHITE", 
                "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Dollars", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Dollars, true, 7, 36, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Gtd_Amt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Gtd_Amt, true, 7, 53, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Div_Amt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Div_Amt, true, 7, 70, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Gtd_Amt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Gtd_Amt, true, 7, 87, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Div_Amt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Div_Amt, true, 7, 104, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Ivc_Amt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Std_Fin_Ivc_Amt, true, 7, 119, 
                12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_28", "TIAA GRD", "BLUE", 8, 1, 8);
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Pay_Cnt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Pay_Cnt, true, 8, 10, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Units", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Units, true, 8, 20, 12, "WHITE", 
                "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Dollars", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Dollars, true, 8, 36, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Gtd_Amt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Gtd_Amt, true, 8, 53, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Div_Amt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Div_Amt, true, 8, 70, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Gtd_Amt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Gtd_Amt, true, 8, 87, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Div_Amt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Div_Amt, true, 8, 104, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Ivc_Amt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Grd_Fin_Ivc_Amt, true, 8, 119, 
                12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_29", "TIAA IPR", "BLUE", 9, 1, 8);
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Pay_Cnt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Pay_Cnt, true, 9, 10, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Units", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Units, true, 9, 20, 12, 
                "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Dollars", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Dollars, true, 9, 36, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Gtd_Amt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Gtd_Amt, true, 9, 53, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Div_Amt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Div_Amt, true, 9, 70, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Gtd_Amt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Gtd_Amt, true, 
                9, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Div_Amt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Div_Amt, true, 
                9, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Ivc_Amt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Ipro_Std_Fin_Ivc_Amt, true, 
                9, 119, 12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_30", "TOTAL", "BLUE", 10, 1, 5);
            uiForm.setUiControl("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Pay_Cnt", pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Pay_Cnt, true, 10, 10, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Units", pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Units, true, 10, 20, 12, "WHITE", "ZZZ,ZZ9.9999", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Dollars", pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Dollars, true, 10, 34, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Gtd_Amt", pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Gtd_Amt, true, 10, 51, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Divid_Amt", pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Divid_Amt, true, 10, 68, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Gtd_Amt", pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Gtd_Amt, true, 10, 85, 16, 
                "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Div_Amt", pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Div_Amt, true, 10, 102, 16, 
                "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Ivc_Amt", pnd_A_Tiaa_Sub_Accum_Pnd_A_Tiaa_Sub_Fin_Ivc_Amt, true, 10, 119, 12, 
                "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_31", "TIAA TPA", "BLUE", 12, 1, 8);
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Pay_Cnt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Pay_Cnt, true, 12, 10, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Units", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Units, true, 12, 20, 12, 
                "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Dollars", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Dollars, true, 12, 36, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Gtd_Amt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Gtd_Amt, true, 12, 53, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Div_Amt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Div_Amt, true, 12, 70, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Gtd_Amt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Gtd_Amt, true, 
                12, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Div_Amt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Div_Amt, true, 
                12, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Ivc_Amt", pnd_A_Tiaa_Minor_Accum_Pnd_A_Tiaa_Tpa_Std_Fin_Ivc_Amt, true, 
                12, 119, 12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_32", "T", "BLUE", 14, 1, 1);
            uiForm.setUiLabel("label_33", "A MTH", "BLUE", 14, 4, 5);
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt_12pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt.getValue(12+0), true, 
                14, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units_12pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units.getValue(12+0), true, 14, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars_12pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars.getValue(12+0), true, 
                14, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt_12pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt.getValue(12+0), true, 
                14, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt_12pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt.getValue(12+0), true, 
                14, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt_12pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt.getValue(12+0), 
                true, 14, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt_12pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt.getValue(12+0), 
                true, 14, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Fin_Ivc_Amount_Pnd_A_C_Mth_Fin_Ivc_Amt", pnd_A_C_Mth_Fin_Ivc_Amount_Pnd_A_C_Mth_Fin_Ivc_Amt, true, 14, 119, 
                12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_34", "T", "BLUE", 15, 1, 1);
            uiForm.setUiLabel("label_35", "A ANN", "BLUE", 15, 4, 5);
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt_12pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt.getValue(12+0), true, 
                15, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units_12pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units.getValue(12+0), true, 15, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars_12pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars.getValue(12+0), true, 
                15, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt_12pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt.getValue(12+0), true, 
                15, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt_12pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt.getValue(12+0), 
                true, 15, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt_12pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt.getValue(12+0), 
                true, 15, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt_12pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt.getValue(12+0), 
                true, 15, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Fin_Ivc_Amount_Pnd_A_C_Ann_Fin_Ivc_Amt", pnd_A_C_Ann_Fin_Ivc_Amount_Pnd_A_C_Ann_Fin_Ivc_Amt, true, 15, 119, 
                12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_36", "T", "BLUE", 16, 1, 1);
            uiForm.setUiLabel("label_37", "R MTH", "BLUE", 16, 4, 5);
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt_2pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt.getValue(2+0), true, 
                16, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units_2pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units.getValue(2+0), true, 16, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars_2pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars.getValue(2+0), true, 
                16, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt_2pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt.getValue(2+0), true, 
                16, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt_2pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt.getValue(2+0), true, 
                16, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt_2pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt.getValue(2+0), 
                true, 16, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt_2pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt.getValue(2+0), 
                true, 16, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Fin_Ivc_Amount_Pnd_A_C_Mth_Fin_Ivc_Amt2", pnd_A_C_Mth_Fin_Ivc_Amount_Pnd_A_C_Mth_Fin_Ivc_Amt, true, 16, 119, 
                12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_38", "T", "BLUE", 17, 1, 1);
            uiForm.setUiLabel("label_39", "R ANN", "BLUE", 17, 4, 5);
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt_2pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt.getValue(2+0), true, 
                17, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units_2pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units.getValue(2+0), true, 17, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars_2pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars.getValue(2+0), true, 
                17, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt_2pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt.getValue(2+0), true, 
                17, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt_2pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt.getValue(2+0), true, 
                17, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt_2pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt.getValue(2+0), 
                true, 17, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt_2pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt.getValue(2+0), 
                true, 17, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Fin_Ivc_Amount_Pnd_A_C_Ann_Fin_Ivc_Amt2", pnd_A_C_Ann_Fin_Ivc_Amount_Pnd_A_C_Ann_Fin_Ivc_Amt, true, 17, 119, 
                12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_40", "STK", "BLUE", 19, 1, 3);
            uiForm.setUiLabel("label_41", "MTH", "BLUE", 19, 6, 3);
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt_3pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt.getValue(3+0), true, 
                19, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units_3pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units.getValue(3+0), true, 19, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars_3pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars.getValue(3+0), true, 
                19, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt_3pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt.getValue(3+0), true, 
                19, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt_3pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt.getValue(3+0), true, 
                19, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt_3pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt.getValue(3+0), 
                true, 19, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt_3pls0", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt.getValue(3+0), 
                true, 19, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_42", "MMA", "BLUE", 20, 1, 3);
            uiForm.setUiLabel("label_43", "MTH", "BLUE", 20, 6, 3);
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt_3pls1", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt.getValue(3+1), true, 
                20, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units_3pls1", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units.getValue(3+1), true, 20, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars_3pls1", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars.getValue(3+1), true, 
                20, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt_3pls1", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt.getValue(3+1), true, 
                20, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt_3pls1", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt.getValue(3+1), true, 
                20, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt_3pls1", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt.getValue(3+1), 
                true, 20, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt_3pls1", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt.getValue(3+1), 
                true, 20, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_44", "SOCL MTH", "BLUE", 21, 1, 8);
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt_3pls2", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt.getValue(3+2), true, 
                21, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units_3pls2", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units.getValue(3+2), true, 21, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars_3pls2", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars.getValue(3+2), true, 
                21, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt_3pls2", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt.getValue(3+2), true, 
                21, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt_3pls2", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt.getValue(3+2), true, 
                21, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt_3pls2", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt.getValue(3+2), 
                true, 21, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt_3pls2", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt.getValue(3+2), 
                true, 21, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_45", "BOND MTH", "BLUE", 22, 1, 8);
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt_3pls3", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt.getValue(3+3), true, 
                22, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units_3pls3", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units.getValue(3+3), true, 22, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars_3pls3", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars.getValue(3+3), true, 
                22, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt_3pls3", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt.getValue(3+3), true, 
                22, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt_3pls3", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt.getValue(3+3), true, 
                22, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt_3pls3", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt.getValue(3+3), 
                true, 22, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt_3pls3", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt.getValue(3+3), 
                true, 22, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_46", "GLOB MTH", "BLUE", 23, 1, 8);
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt_3pls4", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt.getValue(3+4), true, 
                23, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units_3pls4", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units.getValue(3+4), true, 23, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars_3pls4", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars.getValue(3+4), true, 
                23, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt_3pls4", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt.getValue(3+4), true, 
                23, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt_3pls4", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt.getValue(3+4), true, 
                23, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt_3pls4", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt.getValue(3+4), 
                true, 23, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt_3pls4", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt.getValue(3+4), 
                true, 23, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_47", "GROW MTH", "BLUE", 24, 1, 8);
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt_3pls5", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt.getValue(3+5), true, 
                24, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units_3pls5", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units.getValue(3+5), true, 24, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars_3pls5", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars.getValue(3+5), true, 
                24, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt_3pls5", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt.getValue(3+5), true, 
                24, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt_3pls5", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt.getValue(3+5), true, 
                24, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt_3pls5", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt.getValue(3+5), 
                true, 24, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt_3pls5", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt.getValue(3+5), 
                true, 24, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_48", "EQTY MTH", "BLUE", 25, 1, 8);
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt_3pls6", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt.getValue(3+6), true, 
                25, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units_3pls6", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units.getValue(3+6), true, 25, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars_3pls6", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars.getValue(3+6), true, 
                25, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt_3pls6", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt.getValue(3+6), true, 
                25, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt_3pls6", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt.getValue(3+6), true, 
                25, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt_3pls6", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt.getValue(3+6), 
                true, 25, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt_3pls6", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt.getValue(3+6), 
                true, 25, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_49", "ILB", "BLUE", 26, 1, 3);
            uiForm.setUiLabel("label_50", "MTH", "BLUE", 26, 6, 3);
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt_3pls7", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Pay_Cnt.getValue(3+7), true, 
                26, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units_3pls7", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Units.getValue(3+7), true, 26, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars_3pls7", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Dollars.getValue(3+7), true, 
                26, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt_3pls7", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Gtd_Amt.getValue(3+7), true, 
                26, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt_3pls7", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Div_Amt.getValue(3+7), true, 
                26, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt_3pls7", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Gtd_Amt.getValue(3+7), 
                true, 26, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt_3pls7", pnd_A_C_Mth_Minor_Accum_Pnd_A_C_Mth_Fin_Div_Amt.getValue(3+7), 
                true, 26, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_51", "TOT", "BLUE", 27, 1, 3);
            uiForm.setUiLabel("label_52", "MTH", "BLUE", 27, 6, 3);
            uiForm.setUiControl("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Pay_Cnt", pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Pay_Cnt, 
                true, 27, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Units", pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Units, true, 
                27, 20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Dollars", pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Dollars, 
                true, 27, 34, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Gtd_Amt", pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Gtd_Amt, 
                true, 27, 51, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Div_Amt", pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Div_Amt, 
                true, 27, 68, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Gtd_Amt", pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Gtd_Amt, 
                true, 27, 85, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Div_Amt", pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Div_Amt, 
                true, 27, 102, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Ivc_Amt", pnd_A_Cref_Mth_Sub_Total_Accum_Pnd_A_Cref_Mth_Sub_Fin_Ivc_Amt, 
                true, 27, 119, 12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_53", "STK", "BLUE", 29, 1, 3);
            uiForm.setUiLabel("label_54", "ANN", "BLUE", 29, 6, 3);
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt_3pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt.getValue(3+0), true, 
                29, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units_3pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units.getValue(3+0), true, 29, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars_3pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars.getValue(3+0), true, 
                29, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt_3pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt.getValue(3+0), true, 
                29, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt_3pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt.getValue(3+0), true, 
                29, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt_3pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt.getValue(3+0), 
                true, 29, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt_3pls0", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt.getValue(3+0), 
                true, 29, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_55", "MMA", "BLUE", 30, 1, 3);
            uiForm.setUiLabel("label_56", "ANN", "BLUE", 30, 6, 3);
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt_3pls1", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt.getValue(3+1), true, 
                30, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units_3pls1", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units.getValue(3+1), true, 30, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars_3pls1", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars.getValue(3+1), true, 
                30, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt_3pls1", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt.getValue(3+1), true, 
                30, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt_3pls1", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt.getValue(3+1), true, 
                30, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt_3pls1", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt.getValue(3+1), 
                true, 30, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt_3pls1", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt.getValue(3+1), 
                true, 30, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_57", "SOC", "BLUE", 31, 1, 3);
            uiForm.setUiLabel("label_58", "ANN", "BLUE", 31, 6, 3);
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt_3pls2", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt.getValue(3+2), true, 
                31, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units_3pls2", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units.getValue(3+2), true, 31, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars_3pls2", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars.getValue(3+2), true, 
                31, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt_3pls2", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt.getValue(3+2), true, 
                31, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt_3pls2", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt.getValue(3+2), true, 
                31, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt_3pls2", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt.getValue(3+2), 
                true, 31, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt_3pls2", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt.getValue(3+2), 
                true, 31, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_59", "BOND ANN", "BLUE", 32, 1, 8);
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt_3pls3", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt.getValue(3+3), true, 
                32, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units_3pls3", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units.getValue(3+3), true, 32, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars_3pls3", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars.getValue(3+3), true, 
                32, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt_3pls3", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt.getValue(3+3), true, 
                32, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt_3pls3", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt.getValue(3+3), true, 
                32, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt_3pls3", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt.getValue(3+3), 
                true, 32, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt_3pls3", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt.getValue(3+3), 
                true, 32, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_60", "GLOB ANN", "BLUE", 33, 1, 8);
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt_3pls4", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt.getValue(3+4), true, 
                33, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units_3pls4", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units.getValue(3+4), true, 33, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars_3pls4", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars.getValue(3+4), true, 
                33, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt_3pls4", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt.getValue(3+4), true, 
                33, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt_3pls4", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt.getValue(3+4), true, 
                33, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt_3pls4", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt.getValue(3+4), 
                true, 33, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt_3pls4", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt.getValue(3+4), 
                true, 33, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_61", "GROW ANN", "BLUE", 34, 1, 8);
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt_3pls5", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt.getValue(3+5), true, 
                34, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units_3pls5", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units.getValue(3+5), true, 34, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars_3pls5", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars.getValue(3+5), true, 
                34, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt_3pls5", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt.getValue(3+5), true, 
                34, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt_3pls5", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt.getValue(3+5), true, 
                34, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt_3pls5", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt.getValue(3+5), 
                true, 34, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt_3pls5", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt.getValue(3+5), 
                true, 34, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_62", "EQTY ANN", "BLUE", 35, 1, 8);
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt_3pls6", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt.getValue(3+6), true, 
                35, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units_3pls6", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units.getValue(3+6), true, 35, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars_3pls6", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars.getValue(3+6), true, 
                35, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt_3pls6", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt.getValue(3+6), true, 
                35, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt_3pls6", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt.getValue(3+6), true, 
                35, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt_3pls6", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt.getValue(3+6), 
                true, 35, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt_3pls6", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt.getValue(3+6), 
                true, 35, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_63", "ILB", "BLUE", 36, 1, 3);
            uiForm.setUiLabel("label_64", "ANN", "BLUE", 36, 6, 3);
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt_3pls7", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Pay_Cnt.getValue(3+7), true, 
                36, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units_3pls7", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Units.getValue(3+7), true, 36, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars_3pls7", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Dollars.getValue(3+7), true, 
                36, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt_3pls7", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Gtd_Amt.getValue(3+7), true, 
                36, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt_3pls7", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Divid_Amt.getValue(3+7), true, 
                36, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt_3pls7", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Gtd_Amt.getValue(3+7), 
                true, 36, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt_3pls7", pnd_A_C_Ann_Minor_Accum_Pnd_A_C_Ann_Fin_Div_Amt.getValue(3+7), 
                true, 36, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_65", "TOT", "BLUE", 37, 1, 3);
            uiForm.setUiLabel("label_66", "ANN", "BLUE", 37, 6, 3);
            uiForm.setUiControl("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Pay_Cnt", pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Pay_Cnt, 
                true, 37, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Units", pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Units, true, 
                37, 20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Dollars", pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Dollars, 
                true, 37, 34, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Gtd_Amt", pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Gtd_Amt, 
                true, 37, 51, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Div_Amt", pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Div_Amt, 
                true, 37, 68, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Gtd_Amt", pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Gtd_Amt, 
                true, 37, 85, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Div_Amt", pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Div_Amt, 
                true, 37, 102, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Ivc_Amt", pnd_A_Cref_Ann_Sub_Total_Accum_Pnd_A_Cref_Ann_Sub_Fin_Ivc_Amt, 
                true, 37, 119, 12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Units", pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Units, true, 39, 16, 16, "WHITE", 
                "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Gtd_Amt", pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Gtd_Amt, true, 39, 49, 18, 
                "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Gtd_Amt", pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Gtd_Amt, true, 39, 
                83, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Ivc_Amt", pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Ivc_Amt, true, 39, 
                113, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_67", "TOTL ALL", "BLUE", 40, 1, 8);
            uiForm.setUiControl("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Pay_Cnt", pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Pay_Cnt, true, 40, 10, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Dollars", pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Dollars, true, 40, 32, 18, 
                "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Div_Amt", pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Div_Amt, true, 40, 66, 18, 
                "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Div_Amt", pnd_A_Total_Tiaa_Cref_Accum_Pnd_A_Total_Fin_Div_Amt, true, 40, 
                100, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
