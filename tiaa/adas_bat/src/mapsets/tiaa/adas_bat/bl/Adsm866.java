/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:36 PM
**        *   FROM NATURAL MAP   :  Adsm866
************************************************************
**        * FILE NAME               : Adsm866.java
**        * CLASS NAME              : Adsm866
**        * INSTANCE NAME           : Adsm866
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #ASTERISK #COMMENT1 #COMMENT2                                                                                            *     #CREF-TOT-SETTLEMENT.#CREF-TOT-DA-ACCUM-TOTAL 
    *     #CREF-TOT-SETTLEMENT.#CREF-TOT-RTB-TOTAL                                                                                 *     #CREF-TOT-SETTLEMENT.#CREF-TOT-SETTLED-RTB-TOTAL 
    *     #CREF-TOT-SETTLEMENT.#CREF-TOT-SETTLED-TOTAL
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm866 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Asterisk;
    private DbsField pnd_Comment1;
    private DbsField pnd_Comment2;
    private DbsField pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Da_Accum_Total;
    private DbsField pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Rtb_Total;
    private DbsField pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Rtb_Total;
    private DbsField pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Total;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Asterisk = parameters.newFieldInRecord("pnd_Asterisk", "#ASTERISK", FieldType.STRING, 1);
        pnd_Comment1 = parameters.newFieldInRecord("pnd_Comment1", "#COMMENT1", FieldType.STRING, 79);
        pnd_Comment2 = parameters.newFieldInRecord("pnd_Comment2", "#COMMENT2", FieldType.STRING, 79);
        pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Da_Accum_Total = parameters.newFieldInRecord("pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Da_Accum_Total", "#CREF-TOT-SETTLEMENT.#CREF-TOT-DA-ACCUM-TOTAL", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Rtb_Total = parameters.newFieldInRecord("pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Rtb_Total", "#CREF-TOT-SETTLEMENT.#CREF-TOT-RTB-TOTAL", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Rtb_Total = parameters.newFieldInRecord("pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Rtb_Total", 
            "#CREF-TOT-SETTLEMENT.#CREF-TOT-SETTLED-RTB-TOTAL", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Total = parameters.newFieldInRecord("pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Total", "#CREF-TOT-SETTLEMENT.#CREF-TOT-SETTLED-TOTAL", 
            FieldType.PACKED_DECIMAL, 12, 2);
        parameters.reset();
    }

    public Adsm866() throws Exception
    {
        super("Adsm866");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm866", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm866"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "TOTALS", "BLUE", 2, 1, 6);
            uiForm.setUiControl("pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Da_Accum_Total", pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Da_Accum_Total, true, 2, 21, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Asterisk", pnd_Asterisk, true, 2, 36, 1, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Total", pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Total, true, 2, 38, 
                12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Rtb_Total", pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Rtb_Total, true, 2, 51, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Rtb_Total", pnd_Cref_Tot_Settlement_Pnd_Cref_Tot_Settled_Rtb_Total, true, 
                2, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "------", "BLUE", 3, 1, 6);
            uiForm.setUiControl("pnd_Comment1", pnd_Comment1, true, 5, 1, 79, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Comment2", pnd_Comment2, true, 6, 1, 79, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
