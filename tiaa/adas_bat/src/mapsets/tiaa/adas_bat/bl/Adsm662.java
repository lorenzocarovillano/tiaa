/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:52 PM
**        *   FROM NATURAL MAP   :  Adsm662
************************************************************
**        * FILE NAME               : Adsm662.java
**        * CLASS NAME              : Adsm662
**        * INSTANCE NAME           : Adsm662
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #NAP-PYMNT-MODE-ALL #PARTCIPANT-NAME-1 #RQST-ID.#UNIQUE-ID
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm662 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Nap_Pymnt_Mode_All;
    private DbsField pnd_Partcipant_Name_1;
    private DbsField pnd_Rqst_Id_Pnd_Unique_Id;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Nap_Pymnt_Mode_All = parameters.newFieldInRecord("pnd_Nap_Pymnt_Mode_All", "#NAP-PYMNT-MODE-ALL", FieldType.STRING, 20);
        pnd_Partcipant_Name_1 = parameters.newFieldInRecord("pnd_Partcipant_Name_1", "#PARTCIPANT-NAME-1", FieldType.STRING, 30);
        pnd_Rqst_Id_Pnd_Unique_Id = parameters.newFieldInRecord("pnd_Rqst_Id_Pnd_Unique_Id", "#RQST-ID.#UNIQUE-ID", FieldType.NUMERIC, 12);
        parameters.reset();
    }

    public Adsm662() throws Exception
    {
        super("Adsm662");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=080 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm662", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm662"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Rqst_Id_Pnd_Unique_Id", pnd_Rqst_Id_Pnd_Unique_Id, true, 1, 2, 12, "WHITE", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Partcipant_Name_1", pnd_Partcipant_Name_1, true, 2, 2, 30, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_1", "IA PAYMENT/RENEWAL CALCULATION INFORMATION", "BLUE", 5, 1, 42);
            uiForm.setUiLabel("label_2", "MODE:", "BLUE", 7, 36, 5);
            uiForm.setUiControl("pnd_Nap_Pymnt_Mode_All", pnd_Nap_Pymnt_Mode_All, true, 7, 42, 20, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_3", "GRADED METHOD", "BLUE", 9, 1, 13);
            uiForm.setUiLabel("label_4", "_______________", "BLUE", 10, 1, 15);
            uiForm.setUiLabel("label_5", "IA PAYMENTS", "BLUE", 12, 27, 11);
            uiForm.setUiLabel("label_6", "RENEWAL PAYMENTS", "BLUE", 12, 57, 16);
            uiForm.setUiLabel("label_7", "IA", "BLUE", 13, 2, 2);
            uiForm.setUiLabel("label_8", "GRADED", "BLUE", 13, 21, 6);
            uiForm.setUiLabel("label_9", "GRADED", "BLUE", 13, 38, 6);
            uiForm.setUiLabel("label_10", "GRADED", "BLUE", 13, 55, 6);
            uiForm.setUiLabel("label_11", "GRADED", "BLUE", 13, 71, 6);
            uiForm.setUiLabel("label_12", "RATE", "BLUE", 14, 1, 4);
            uiForm.setUiLabel("label_13", "GUARANTEED", "BLUE", 14, 19, 10);
            uiForm.setUiLabel("label_14", "DIVIDEND", "BLUE", 14, 37, 8);
            uiForm.setUiLabel("label_15", "GUARANTEED", "BLUE", 14, 53, 10);
            uiForm.setUiLabel("label_16", "DIVIDEND", "BLUE", 14, 71, 8);
            uiForm.setUiLabel("label_17", "___", "BLUE", 15, 1, 3);
            uiForm.setUiLabel("label_18", "__________", "BLUE", 15, 19, 10);
            uiForm.setUiLabel("label_19", "__________", "BLUE", 15, 36, 10);
            uiForm.setUiLabel("label_20", "__________", "BLUE", 15, 53, 10);
            uiForm.setUiLabel("label_21", "_________", "BLUE", 15, 70, 9);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
