/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:53 PM
**        *   FROM NATURAL MAP   :  Adsm668
************************************************************
**        * FILE NAME               : Adsm668.java
**        * CLASS NAME              : Adsm668
**        * INSTANCE NAME           : Adsm668
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #PARTCIPANT-NAME-1 #RQST-ID.#UNIQUE-ID
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm668 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Partcipant_Name_1;
    private DbsField pnd_Rqst_Id_Pnd_Unique_Id;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Partcipant_Name_1 = parameters.newFieldInRecord("pnd_Partcipant_Name_1", "#PARTCIPANT-NAME-1", FieldType.STRING, 30);
        pnd_Rqst_Id_Pnd_Unique_Id = parameters.newFieldInRecord("pnd_Rqst_Id_Pnd_Unique_Id", "#RQST-ID.#UNIQUE-ID", FieldType.NUMERIC, 12);
        parameters.reset();
    }

    public Adsm668() throws Exception
    {
        super("Adsm668");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm668", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm668"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Rqst_Id_Pnd_Unique_Id", pnd_Rqst_Id_Pnd_Unique_Id, true, 1, 2, 12, "WHITE", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Partcipant_Name_1", pnd_Partcipant_Name_1, true, 2, 2, 30, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_1", "SETTLEMENT CALCULATION INFORMATION", "BLUE", 4, 1, 34);
            uiForm.setUiLabel("label_2", "GRADED METHOD", "BLUE", 6, 1, 13);
            uiForm.setUiLabel("label_3", "_______________", "BLUE", 7, 1, 15);
            uiForm.setUiLabel("label_4", "SETTLED", "BLUE", 8, 27, 7);
            uiForm.setUiLabel("label_5", "IA PAYMENTS", "BLUE", 8, 53, 11);
            uiForm.setUiLabel("label_6", "GUARANTEED", "BLUE", 8, 85, 10);
            uiForm.setUiLabel("label_7", "DA", "BLUE", 9, 10, 2);
            uiForm.setUiLabel("label_8", "IA", "BLUE", 9, 15, 2);
            uiForm.setUiLabel("label_9", "ACCUM", "BLUE", 9, 28, 5);
            uiForm.setUiLabel("label_10", "GRADED", "BLUE", 9, 46, 6);
            uiForm.setUiLabel("label_11", "GRADED", "BLUE", 9, 65, 6);
            uiForm.setUiLabel("label_12", "COMMUTED", "BLUE", 9, 86, 8);
            uiForm.setUiLabel("label_13", "PROD", "BLUE", 10, 1, 4);
            uiForm.setUiLabel("label_14", "RATE RATE", "BLUE", 10, 9, 9);
            uiForm.setUiLabel("label_15", "GRADED", "BLUE", 10, 28, 6);
            uiForm.setUiLabel("label_16", "GUARANTEED", "BLUE", 10, 44, 10);
            uiForm.setUiLabel("label_17", "DIVIDEND", "BLUE", 10, 64, 8);
            uiForm.setUiLabel("label_18", "VALUE", "BLUE", 10, 87, 5);
            uiForm.setUiLabel("label_19", "___", "BLUE", 11, 1, 3);
            uiForm.setUiLabel("label_20", "_____ _____", "BLUE", 11, 8, 11);
            uiForm.setUiLabel("label_21", "______________", "BLUE", 11, 24, 14);
            uiForm.setUiLabel("label_22", "________________", "", 11, 41, 16);
            uiForm.setUiLabel("label_23", "______________", "BLUE", 11, 61, 14);
            uiForm.setUiLabel("label_24", "_______________", "BLUE", 11, 82, 15);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
