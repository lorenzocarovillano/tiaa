/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:36 PM
**        *   FROM NATURAL MAP   :  Adsm863
************************************************************
**        * FILE NAME               : Adsm863.java
**        * CLASS NAME              : Adsm863
**        * INSTANCE NAME           : Adsm863
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #DA-ACCUM-DATE
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm863 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Da_Accum_Date;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Da_Accum_Date = parameters.newFieldInRecord("pnd_Da_Accum_Date", "#DA-ACCUM-DATE", FieldType.NUMERIC, 6);
        parameters.reset();
    }

    public Adsm863() throws Exception
    {
        super("Adsm863");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=080 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm863", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm863"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "------------------------------------------------------------------------------", "", 2, 1, 78);
            uiForm.setUiLabel("label_2", "Rate", "BLUE", 3, 16, 4);
            uiForm.setUiLabel("label_3", "DA Accum Date", "BLUE", 3, 24, 13);
            uiForm.setUiLabel("label_4", "Settlement", "BLUE", 3, 40, 10);
            uiForm.setUiLabel("label_5", "RTB", "BLUE", 3, 58, 3);
            uiForm.setUiLabel("label_6", "Settlement", "BLUE", 3, 69, 10);
            uiForm.setUiLabel("label_7", "Product", "BLUE", 4, 3, 7);
            uiForm.setUiLabel("label_8", "Basis", "BLUE", 4, 16, 5);
            uiForm.setUiControl("pnd_Da_Accum_Date", pnd_Da_Accum_Date, true, 4, 28, 8, "WHITE", "99/99/99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_9", "Amount", "BLUE", 4, 41, 6);
            uiForm.setUiLabel("label_10", "Amount", "BLUE", 4, 57, 6);
            uiForm.setUiLabel("label_11", "minus RTB", "BLUE", 4, 70, 9);
            uiForm.setUiLabel("label_12", "Amt/Units", "BLUE", 5, 27, 9);
            uiForm.setUiLabel("label_13", "Amt/units", "BLUE", 5, 40, 9);
            uiForm.setUiLabel("label_14", "Amt/Units", "BLUE", 5, 55, 9);
            uiForm.setUiLabel("label_15", "Amt/Units", "BLUE", 5, 70, 9);
            uiForm.setUiLabel("label_16", "------------------------------------------------------------------------------", "", 6, 1, 78);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
