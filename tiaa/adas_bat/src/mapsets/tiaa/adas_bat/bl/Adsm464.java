/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:50 PM
**        *   FROM NATURAL MAP   :  Adsm464
************************************************************
**        * FILE NAME               : Adsm464.java
**        * CLASS NAME              : Adsm464
**        * INSTANCE NAME           : Adsm464
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #CREF-AND-TIAA-GRAND-ACCUM.#GRAND-MATURITY-AMT                                                                           *     #CREF-AND-TIAA-GRAND-ACCUM.#GRAND-RATE-MATURITY-CNT 
    *     #CREF-AND-TIAA-GRAND-ACCUM.#GRAND-RATE-RTB-CNT                                                                           *     #CREF-AND-TIAA-GRAND-ACCUM.#GRAND-RATE-RTB-RLVR-CNT 
    *     #CREF-AND-TIAA-GRAND-ACCUM.#GRAND-RTB-AMT                                                                                *     #CREF-AND-TIAA-GRAND-ACCUM.#GRAND-RTB-RLVR-AMT 
    *     #CREF-ANNUALLY-GRAND-ACCUM.#CREF-GRN-ANN-MAT-AMT(*)                                                                      *     #CREF-ANNUALLY-GRAND-ACCUM.#CREF-GRN-ANN-MAT-CNT(*) 
    *     #CREF-ANNUALLY-GRAND-ACCUM.#CREF-GRN-ANN-RTB-AMT(*)                                                                      *     #CREF-ANNUALLY-GRAND-ACCUM.#CREF-GRN-ANN-RTB-CNT(*) 
    *     #CREF-ANNUALLY-GRAND-ACCUM.#CREF-GRN-ANN-RTB-RLA(*)                                                                      *     #CREF-ANNUALLY-GRAND-ACCUM.#CREF-GRN-ANN-RTB-RLC(*) 
    *     #CREF-ANNUALLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-ANN-MATURITY-AMT                                                            *     #CREF-ANNUALLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-ANN-MATURITY-CNT 
    *     #CREF-ANNUALLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-ANN-RTB-AMT                                                                 *     #CREF-ANNUALLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-ANN-RTB-CNT 
    *     #CREF-ANNUALLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-ANN-RTB-RLVR-AMT                                                            *     #CREF-ANNUALLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-ANN-RTB-RLVR-CNT 
    *     #CREF-MONTHLY-GRAND-ACCUM.#CREF-GRN-MTH-MAT-AMT(*)                                                                       *     #CREF-MONTHLY-GRAND-ACCUM.#CREF-GRN-MTH-MAT-CNT(*) 
    *     #CREF-MONTHLY-GRAND-ACCUM.#CREF-GRN-MTH-RTB-AMT(*)                                                                       *     #CREF-MONTHLY-GRAND-ACCUM.#CREF-GRN-MTH-RTB-CNT(*) 
    *     #CREF-MONTHLY-GRAND-ACCUM.#CREF-GRN-MTH-RTB-RLA(*)                                                                       *     #CREF-MONTHLY-GRAND-ACCUM.#CREF-GRN-MTH-RTB-RLC(*) 
    *     #CREF-MONTHLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-MTH-MATURITY-AMT                                                             *     #CREF-MONTHLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-MTH-MATURITY-CNT 
    *     #CREF-MONTHLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-MTH-RTB-AMT                                                                  *     #CREF-MONTHLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-MTH-RTB-CNT 
    *     #CREF-MONTHLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-MTH-RTB-RLVR-AMT                                                             *     #CREF-MONTHLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-MTH-RTB-RLVR-CNT 
    *     #TIAA-GRAND-ACCUM.#TIAA-GRN-GRD-MATURITY-AMT                                                                             *     #TIAA-GRAND-ACCUM.#TIAA-GRN-GRD-MATURITY-CNT 
    *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-GRD-MATURITY-AMT                                                                        *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-GRD-MATURITY-ASV 
    *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-GRD-MATURITY-CNT                                                                        *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-GRD-MATURITY-CSV 
    *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-STD-MATURITY-AMT                                                                        *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-STD-MATURITY-ASV 
    *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-STD-MATURITY-CNT                                                                        *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-STD-MATURITY-CSV 
    *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-STD-RTB-AMT                                                                             *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-STD-RTB-CNT 
    *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-STD-RTB-RLVR-AMT                                                                        *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-STD-RTB-RLVR-ASV 
    *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-STD-RTB-RLVR-CNT                                                                        *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-STD-RTB-RLVR-CSV 
    *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STD-MATURITY-AMT                                                                             *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STD-MATURITY-CNT 
    *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STD-RTB-AMT                                                                                  *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STD-RTB-CNT 
    *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STD-RTB-RLVR-AMT                                                                             *     #TIAA-GRAND-ACCUM.#TIAA-GRN-STD-RTB-RLVR-CNT 
    *     #TIAA-GRN-IO-ACCM-AMT #TIAA-GRN-IO-ACCM-CNT #TIAA-GRN-IO-RLVR-AMT                                                        *     #TIAA-GRN-IO-RLVR-CNT 
    #TIAA-GRN-TPA-ACCM-AMT                                                                             *     #TIAA-GRN-TPA-ACCM-CNT #TIAA-GRN-TPA-RLVR-AMT 
    *     #TIAA-GRN-TPA-RLVR-CNT                                                                                                   *     #TIAA-SUB-GRN-ACCUM.#TIAA-SUB-GRN-MATURITY-AMT 
    *     #TIAA-SUB-GRN-ACCUM.#TIAA-SUB-GRN-MATURITY-CNT                                                                           *     #TIAA-SUB-GRN-ACCUM.#TIAA-SUB-GRN-RTB-AMT 
    *     #TIAA-SUB-GRN-ACCUM.#TIAA-SUB-GRN-RTB-CNT                                                                                *     #TIAA-SUB-GRN-ACCUM.#TIAA-SUB-GRN-RTB-RLVR-AMT 
    *     #TIAA-SUB-GRN-ACCUM.#TIAA-SUB-GRN-RTB-RLVR-CNT
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm464 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt;
    private DbsField pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt;
    private DbsField pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Rtb_Cnt;
    private DbsField pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Rtb_Rlvr_Cnt;
    private DbsField pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rtb_Amt;
    private DbsField pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rtb_Rlvr_Amt;
    private DbsField pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt;
    private DbsField pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt;
    private DbsField pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt;
    private DbsField pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt;
    private DbsField pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla;
    private DbsField pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc;
    private DbsField pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Amt;
    private DbsField pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Cnt;
    private DbsField pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Amt;
    private DbsField pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Cnt;
    private DbsField pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Rlvr_Amt;
    private DbsField pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Rlvr_Cnt;
    private DbsField pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt;
    private DbsField pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt;
    private DbsField pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt;
    private DbsField pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt;
    private DbsField pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla;
    private DbsField pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc;
    private DbsField pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Amt;
    private DbsField pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Cnt;
    private DbsField pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Rtb_Amt;
    private DbsField pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Rtb_Cnt;
    private DbsField pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Rtb_Rlvr_Amt;
    private DbsField pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Rtb_Rlvr_Cnt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Amt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Cnt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Amt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Asv;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Cnt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Csv;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Amt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Asv;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Cnt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Csv;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Amt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Cnt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Rlvr_Amt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Rlvr_Asv;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Rlvr_Cnt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Rlvr_Csv;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Amt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Cnt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Amt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Cnt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Rlvr_Amt;
    private DbsField pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Rlvr_Cnt;
    private DbsField pnd_Tiaa_Grn_Io_Accm_Amt;
    private DbsField pnd_Tiaa_Grn_Io_Accm_Cnt;
    private DbsField pnd_Tiaa_Grn_Io_Rlvr_Amt;
    private DbsField pnd_Tiaa_Grn_Io_Rlvr_Cnt;
    private DbsField pnd_Tiaa_Grn_Tpa_Accm_Amt;
    private DbsField pnd_Tiaa_Grn_Tpa_Accm_Cnt;
    private DbsField pnd_Tiaa_Grn_Tpa_Rlvr_Amt;
    private DbsField pnd_Tiaa_Grn_Tpa_Rlvr_Cnt;
    private DbsField pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt;
    private DbsField pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt;
    private DbsField pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Amt;
    private DbsField pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Cnt;
    private DbsField pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Rlvr_Amt;
    private DbsField pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Rlvr_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt = parameters.newFieldInRecord("pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt", "#CREF-AND-TIAA-GRAND-ACCUM.#GRAND-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt = parameters.newFieldInRecord("pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt", 
            "#CREF-AND-TIAA-GRAND-ACCUM.#GRAND-RATE-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Rtb_Cnt = parameters.newFieldInRecord("pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Rtb_Cnt", "#CREF-AND-TIAA-GRAND-ACCUM.#GRAND-RATE-RTB-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Rtb_Rlvr_Cnt = parameters.newFieldInRecord("pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Rtb_Rlvr_Cnt", 
            "#CREF-AND-TIAA-GRAND-ACCUM.#GRAND-RATE-RTB-RLVR-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rtb_Amt = parameters.newFieldInRecord("pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rtb_Amt", "#CREF-AND-TIAA-GRAND-ACCUM.#GRAND-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rtb_Rlvr_Amt = parameters.newFieldInRecord("pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rtb_Rlvr_Amt", "#CREF-AND-TIAA-GRAND-ACCUM.#GRAND-RTB-RLVR-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt = parameters.newFieldArrayInRecord("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt", 
            "#CREF-ANNUALLY-GRAND-ACCUM.#CREF-GRN-ANN-MAT-AMT", FieldType.PACKED_DECIMAL, 15, 2, new DbsArrayController(1, 20));
        pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt = parameters.newFieldArrayInRecord("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt", 
            "#CREF-ANNUALLY-GRAND-ACCUM.#CREF-GRN-ANN-MAT-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt = parameters.newFieldArrayInRecord("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt", 
            "#CREF-ANNUALLY-GRAND-ACCUM.#CREF-GRN-ANN-RTB-AMT", FieldType.PACKED_DECIMAL, 15, 2, new DbsArrayController(1, 20));
        pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt = parameters.newFieldArrayInRecord("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt", 
            "#CREF-ANNUALLY-GRAND-ACCUM.#CREF-GRN-ANN-RTB-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla = parameters.newFieldArrayInRecord("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla", 
            "#CREF-ANNUALLY-GRAND-ACCUM.#CREF-GRN-ANN-RTB-RLA", FieldType.PACKED_DECIMAL, 15, 2, new DbsArrayController(1, 20));
        pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc = parameters.newFieldArrayInRecord("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc", 
            "#CREF-ANNUALLY-GRAND-ACCUM.#CREF-GRN-ANN-RTB-RLC", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Amt = parameters.newFieldInRecord("pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Amt", 
            "#CREF-ANNUALLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-ANN-MATURITY-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Cnt = parameters.newFieldInRecord("pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Cnt", 
            "#CREF-ANNUALLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-ANN-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Amt = parameters.newFieldInRecord("pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Amt", 
            "#CREF-ANNUALLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-ANN-RTB-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Cnt = parameters.newFieldInRecord("pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Cnt", 
            "#CREF-ANNUALLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-ANN-RTB-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Rlvr_Amt = parameters.newFieldInRecord("pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Rlvr_Amt", 
            "#CREF-ANNUALLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-ANN-RTB-RLVR-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Rlvr_Cnt = parameters.newFieldInRecord("pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Rlvr_Cnt", 
            "#CREF-ANNUALLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-ANN-RTB-RLVR-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt = parameters.newFieldArrayInRecord("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt", 
            "#CREF-MONTHLY-GRAND-ACCUM.#CREF-GRN-MTH-MAT-AMT", FieldType.PACKED_DECIMAL, 15, 2, new DbsArrayController(1, 20));
        pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt = parameters.newFieldArrayInRecord("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt", 
            "#CREF-MONTHLY-GRAND-ACCUM.#CREF-GRN-MTH-MAT-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt = parameters.newFieldArrayInRecord("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt", 
            "#CREF-MONTHLY-GRAND-ACCUM.#CREF-GRN-MTH-RTB-AMT", FieldType.PACKED_DECIMAL, 15, 2, new DbsArrayController(1, 20));
        pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt = parameters.newFieldArrayInRecord("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt", 
            "#CREF-MONTHLY-GRAND-ACCUM.#CREF-GRN-MTH-RTB-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla = parameters.newFieldArrayInRecord("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla", 
            "#CREF-MONTHLY-GRAND-ACCUM.#CREF-GRN-MTH-RTB-RLA", FieldType.PACKED_DECIMAL, 15, 2, new DbsArrayController(1, 20));
        pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc = parameters.newFieldArrayInRecord("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc", 
            "#CREF-MONTHLY-GRAND-ACCUM.#CREF-GRN-MTH-RTB-RLC", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Amt = parameters.newFieldInRecord("pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Amt", 
            "#CREF-MONTHLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-MTH-MATURITY-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Cnt = parameters.newFieldInRecord("pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Cnt", 
            "#CREF-MONTHLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-MTH-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Rtb_Amt = parameters.newFieldInRecord("pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Rtb_Amt", 
            "#CREF-MONTHLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-MTH-RTB-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Rtb_Cnt = parameters.newFieldInRecord("pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Rtb_Cnt", 
            "#CREF-MONTHLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-MTH-RTB-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Rtb_Rlvr_Amt = parameters.newFieldInRecord("pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Rtb_Rlvr_Amt", 
            "#CREF-MONTHLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-MTH-RTB-RLVR-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Rtb_Rlvr_Cnt = parameters.newFieldInRecord("pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Rtb_Rlvr_Cnt", 
            "#CREF-MONTHLY-SUB-GRAND-ACCUM.#CREF-SUB-GRN-MTH-RTB-RLVR-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Amt", "#TIAA-GRAND-ACCUM.#TIAA-GRN-GRD-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Cnt", "#TIAA-GRAND-ACCUM.#TIAA-GRN-GRD-MATURITY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Amt", 
            "#TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-GRD-MATURITY-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Asv = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Asv", 
            "#TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-GRD-MATURITY-ASV", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Cnt", 
            "#TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-GRD-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Csv = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Csv", 
            "#TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-GRD-MATURITY-CSV", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Amt", 
            "#TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-STD-MATURITY-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Asv = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Asv", 
            "#TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-STD-MATURITY-ASV", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Cnt", 
            "#TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-STD-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Csv = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Csv", 
            "#TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-STD-MATURITY-CSV", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Amt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Amt", "#TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-STD-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Cnt", "#TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-STD-RTB-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Rlvr_Amt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Rlvr_Amt", 
            "#TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-STD-RTB-RLVR-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Rlvr_Asv = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Rlvr_Asv", 
            "#TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-STD-RTB-RLVR-ASV", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Rlvr_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Rlvr_Cnt", 
            "#TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-STD-RTB-RLVR-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Rlvr_Csv = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Rlvr_Csv", 
            "#TIAA-GRAND-ACCUM.#TIAA-GRN-STBL-STD-RTB-RLVR-CSV", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Amt", "#TIAA-GRAND-ACCUM.#TIAA-GRN-STD-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Cnt", "#TIAA-GRAND-ACCUM.#TIAA-GRN-STD-MATURITY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Amt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Amt", "#TIAA-GRAND-ACCUM.#TIAA-GRN-STD-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Cnt", "#TIAA-GRAND-ACCUM.#TIAA-GRN-STD-RTB-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Rlvr_Amt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Rlvr_Amt", "#TIAA-GRAND-ACCUM.#TIAA-GRN-STD-RTB-RLVR-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Rlvr_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Rlvr_Cnt", "#TIAA-GRAND-ACCUM.#TIAA-GRN-STD-RTB-RLVR-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grn_Io_Accm_Amt = parameters.newFieldInRecord("pnd_Tiaa_Grn_Io_Accm_Amt", "#TIAA-GRN-IO-ACCM-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Grn_Io_Accm_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Grn_Io_Accm_Cnt", "#TIAA-GRN-IO-ACCM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grn_Io_Rlvr_Amt = parameters.newFieldInRecord("pnd_Tiaa_Grn_Io_Rlvr_Amt", "#TIAA-GRN-IO-RLVR-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Grn_Io_Rlvr_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Grn_Io_Rlvr_Cnt", "#TIAA-GRN-IO-RLVR-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grn_Tpa_Accm_Amt = parameters.newFieldInRecord("pnd_Tiaa_Grn_Tpa_Accm_Amt", "#TIAA-GRN-TPA-ACCM-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Grn_Tpa_Accm_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Grn_Tpa_Accm_Cnt", "#TIAA-GRN-TPA-ACCM-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Grn_Tpa_Rlvr_Amt = parameters.newFieldInRecord("pnd_Tiaa_Grn_Tpa_Rlvr_Amt", "#TIAA-GRN-TPA-RLVR-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Grn_Tpa_Rlvr_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Grn_Tpa_Rlvr_Cnt", "#TIAA-GRN-TPA-RLVR-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt", "#TIAA-SUB-GRN-ACCUM.#TIAA-SUB-GRN-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt", "#TIAA-SUB-GRN-ACCUM.#TIAA-SUB-GRN-MATURITY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Amt = parameters.newFieldInRecord("pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Amt", "#TIAA-SUB-GRN-ACCUM.#TIAA-SUB-GRN-RTB-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Cnt", "#TIAA-SUB-GRN-ACCUM.#TIAA-SUB-GRN-RTB-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Rlvr_Amt = parameters.newFieldInRecord("pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Rlvr_Amt", "#TIAA-SUB-GRN-ACCUM.#TIAA-SUB-GRN-RTB-RLVR-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Rlvr_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Rlvr_Cnt", "#TIAA-SUB-GRN-ACCUM.#TIAA-SUB-GRN-RTB-RLVR-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        parameters.reset();
    }

    public Adsm464() throws Exception
    {
        super("Adsm464");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm464", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm464"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "GRAND TOTAL", "BLUE", 1, 1, 11);
            uiForm.setUiLabel("label_2", "ALL EFFECTIVE DATES", "BLUE", 2, 1, 19);
            uiForm.setUiLabel("label_3", "DA RATE", "BLUE", 3, 26, 7);
            uiForm.setUiLabel("label_4", "RTB", "BLUE", 3, 47, 3);
            uiForm.setUiLabel("label_5", "DA RATE", "BLUE", 3, 63, 7);
            uiForm.setUiLabel("label_6", "INTERNAL", "BLUE", 3, 79, 8);
            uiForm.setUiLabel("label_7", "DA RATE", "BLUE", 3, 101, 7);
            uiForm.setUiLabel("label_8", "MATURITY", "BLUE", 3, 122, 8);
            uiForm.setUiLabel("label_9", "Product", "BLUE", 4, 1, 7);
            uiForm.setUiLabel("label_10", "COUNTS", "BLUE", 4, 26, 6);
            uiForm.setUiLabel("label_11", "ACCUMULATION", "BLUE", 4, 43, 12);
            uiForm.setUiLabel("label_12", "COUNTS", "BLUE", 4, 64, 6);
            uiForm.setUiLabel("label_13", "ROLLOVER AMOUNT", "BLUE", 4, 76, 15);
            uiForm.setUiLabel("label_14", "COUNTS", "BLUE", 4, 102, 6);
            uiForm.setUiLabel("label_15", "ACCUMULATION", "BLUE", 4, 120, 12);
            uiForm.setUiLabel("label_16", "TIAA STANDARD", "BLUE", 5, 1, 13);
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Cnt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Cnt, true, 5, 24, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Amt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Amt, true, 5, 37, 18, "WHITE", 
                "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Rlvr_Cnt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Rlvr_Cnt, true, 5, 61, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Rlvr_Amt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Rtb_Rlvr_Amt, true, 5, 73, 
                18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Cnt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Cnt, true, 5, 99, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Amt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Std_Maturity_Amt, true, 5, 114, 
                18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "TIAA GRADED", "BLUE", 6, 1, 11);
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Cnt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Cnt, true, 6, 99, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Amt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Grd_Maturity_Amt, true, 6, 114, 
                18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_18", "TIAA IPRO", "BLUE", 7, 1, 9);
            uiForm.setUiControl("pnd_Tiaa_Grn_Io_Rlvr_Cnt", pnd_Tiaa_Grn_Io_Rlvr_Cnt, true, 7, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grn_Io_Rlvr_Amt", pnd_Tiaa_Grn_Io_Rlvr_Amt, true, 7, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ0.99", true, true, null, 
                "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grn_Io_Accm_Cnt", pnd_Tiaa_Grn_Io_Accm_Cnt, true, 7, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grn_Io_Accm_Amt", pnd_Tiaa_Grn_Io_Accm_Amt, true, 7, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, 
                "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", "TIAA STABLE STANDARD", "BLUE", 8, 1, 20);
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Cnt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Cnt, true, 8, 24, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Amt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Amt, true, 8, 37, 
                18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Rlvr_Cnt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Rlvr_Cnt, true, 
                8, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Rlvr_Amt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Rlvr_Amt, true, 
                8, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Cnt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Cnt, true, 
                8, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Amt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Amt, true, 
                8, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_20", "TIAA STABLE GRADED", "BLUE", 9, 1, 18);
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Cnt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Cnt, true, 
                9, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Amt", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Amt, true, 
                9, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_21", "TIAA STANDARD SVSAA", "BLUE", 10, 1, 19);
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Cnt2", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Cnt, true, 10, 24, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Amt2", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Amt, true, 10, 37, 
                18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Rlvr_Csv", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Rlvr_Csv, true, 
                10, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Rlvr_Asv", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Rtb_Rlvr_Asv, true, 
                10, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Csv", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Csv, true, 
                10, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Asv", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Std_Maturity_Asv, true, 
                10, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_22", "TIAA GRADED", "BLUE", 11, 1, 11);
            uiForm.setUiLabel("label_23", "SVSAA", "BLUE", 11, 15, 5);
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Csv", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Csv, true, 
                11, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Asv", pnd_Tiaa_Grand_Accum_Pnd_Tiaa_Grn_Stbl_Grd_Maturity_Asv, true, 
                11, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_24", "TOTAL", "BLUE", 12, 1, 5);
            uiForm.setUiControl("pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Cnt", pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Cnt, true, 12, 24, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Amt", pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Amt, true, 12, 37, 18, 
                "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Rlvr_Cnt", pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Rlvr_Cnt, true, 12, 
                61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Rlvr_Amt", pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Rtb_Rlvr_Amt, true, 12, 
                73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt", pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Cnt, true, 12, 
                99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt", pnd_Tiaa_Sub_Grn_Accum_Pnd_Tiaa_Sub_Grn_Maturity_Amt, true, 12, 
                114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_25", "T.P.A", "BLUE", 14, 1, 5);
            uiForm.setUiControl("pnd_Tiaa_Grn_Tpa_Rlvr_Cnt", pnd_Tiaa_Grn_Tpa_Rlvr_Cnt, true, 14, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grn_Tpa_Rlvr_Amt", pnd_Tiaa_Grn_Tpa_Rlvr_Amt, true, 14, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, 
                "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grn_Tpa_Accm_Cnt", pnd_Tiaa_Grn_Tpa_Accm_Cnt, true, 14, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Grn_Tpa_Accm_Amt", pnd_Tiaa_Grn_Tpa_Accm_Amt, true, 14, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,Z99.99", true, true, 
                null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_26", "TIAA ACCESS MONTHLY", "BLUE", 16, 1, 19);
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt_12pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt.getValue(12+0), 
                true, 16, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt_12pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt.getValue(12+0), 
                true, 16, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc_12pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc.getValue(12+0), 
                true, 16, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla_12pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla.getValue(12+0), 
                true, 16, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt_12pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt.getValue(12+0), 
                true, 16, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt_12pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt.getValue(12+0), 
                true, 16, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_27", "TIAA ACCESS ANNUAL", "BLUE", 17, 1, 18);
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt_12pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt.getValue(12+0), 
                true, 17, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt_12pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt.getValue(12+0), 
                true, 17, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc_12pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc.getValue(12+0), 
                true, 17, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla_12pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla.getValue(12+0), 
                true, 17, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt_12pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt.getValue(12+0), 
                true, 17, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt_12pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt.getValue(12+0), 
                true, 17, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_28", "TIAA REA MONTHLY", "BLUE", 18, 1, 16);
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt_2pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt.getValue(2+0), 
                true, 18, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt_2pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt.getValue(2+0), 
                true, 18, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc_2pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc.getValue(2+0), 
                true, 18, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla_2pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla.getValue(2+0), 
                true, 18, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt_2pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt.getValue(2+0), 
                true, 18, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt_2pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt.getValue(2+0), 
                true, 18, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_29", "TIAA REA ANNUAL", "BLUE", 19, 1, 15);
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt_2pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt.getValue(2+0), 
                true, 19, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt_2pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt.getValue(2+0), 
                true, 19, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc_2pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc.getValue(2+0), 
                true, 19, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla_2pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla.getValue(2+0), 
                true, 19, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt_2pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt.getValue(2+0), 
                true, 19, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt_2pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt.getValue(2+0), 
                true, 19, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_30", "STOCK", "BLUE", 21, 1, 5);
            uiForm.setUiLabel("label_31", "MONTHLY", "BLUE", 21, 8, 7);
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt_3pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt.getValue(3+0), 
                true, 21, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt_3pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt.getValue(3+0), 
                true, 21, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc_3pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc.getValue(3+0), 
                true, 21, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla_3pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla.getValue(3+0), 
                true, 21, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt_3pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt.getValue(3+0), 
                true, 21, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt_3pls0", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt.getValue(3+0), 
                true, 21, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_32", "MMA", "BLUE", 22, 1, 3);
            uiForm.setUiLabel("label_33", "MONTHLY", "BLUE", 22, 8, 7);
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt_3pls1", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt.getValue(3+1), 
                true, 22, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt_3pls1", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt.getValue(3+1), 
                true, 22, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc_3pls1", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc.getValue(3+1), 
                true, 22, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla_3pls1", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla.getValue(3+1), 
                true, 22, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt_3pls1", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt.getValue(3+1), 
                true, 22, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt_3pls1", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt.getValue(3+1), 
                true, 22, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_34", "SOCIAL MONTHLY", "BLUE", 23, 1, 14);
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt_3pls2", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt.getValue(3+2), 
                true, 23, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt_3pls2", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt.getValue(3+2), 
                true, 23, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc_3pls2", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc.getValue(3+2), 
                true, 23, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla_3pls2", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla.getValue(3+2), 
                true, 23, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt_3pls2", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt.getValue(3+2), 
                true, 23, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt_3pls2", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt.getValue(3+2), 
                true, 23, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_35", "BOND", "BLUE", 24, 1, 4);
            uiForm.setUiLabel("label_36", "MONTHLY", "BLUE", 24, 8, 7);
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt_3pls3", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt.getValue(3+3), 
                true, 24, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt_3pls3", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt.getValue(3+3), 
                true, 24, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc_3pls3", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc.getValue(3+3), 
                true, 24, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla_3pls3", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla.getValue(3+3), 
                true, 24, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt_3pls3", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt.getValue(3+3), 
                true, 24, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt_3pls3", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt.getValue(3+3), 
                true, 24, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_37", "GLOBAL MONTHLY", "BLUE", 25, 1, 14);
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt_3pls4", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt.getValue(3+4), 
                true, 25, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt_3pls4", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt.getValue(3+4), 
                true, 25, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc_3pls4", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc.getValue(3+4), 
                true, 25, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla_3pls4", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla.getValue(3+4), 
                true, 25, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt_3pls4", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt.getValue(3+4), 
                true, 25, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt_3pls4", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt.getValue(3+4), 
                true, 25, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_38", "GROWTH MONTHLY", "BLUE", 26, 1, 14);
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt_3pls5", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt.getValue(3+5), 
                true, 26, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt_3pls5", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt.getValue(3+5), 
                true, 26, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc_3pls5", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc.getValue(3+5), 
                true, 26, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla_3pls5", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla.getValue(3+5), 
                true, 26, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt_3pls5", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt.getValue(3+5), 
                true, 26, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt_3pls5", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt.getValue(3+5), 
                true, 26, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_39", "EQUITY MONTHLY", "BLUE", 27, 1, 14);
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt_3pls6", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt.getValue(3+6), 
                true, 27, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt_3pls6", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt.getValue(3+6), 
                true, 27, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc_3pls6", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc.getValue(3+6), 
                true, 27, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla_3pls6", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla.getValue(3+6), 
                true, 27, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt_3pls6", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt.getValue(3+6), 
                true, 27, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt_3pls6", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt.getValue(3+6), 
                true, 27, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_40", "I L B", "BLUE", 28, 1, 5);
            uiForm.setUiLabel("label_41", "MONTHLY", "BLUE", 28, 8, 7);
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt_3pls7", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Cnt.getValue(3+7), 
                true, 28, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt_3pls7", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Amt.getValue(3+7), 
                true, 28, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc_3pls7", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rlc.getValue(3+7), 
                true, 28, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla_3pls7", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Rtb_Rla.getValue(3+7), 
                true, 28, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt_3pls7", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Cnt.getValue(3+7), 
                true, 28, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt_3pls7", pnd_Cref_Monthly_Grand_Accum_Pnd_Cref_Grn_Mth_Mat_Amt.getValue(3+7), 
                true, 28, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_42", "TOTAL CREF MONTHLY", "BLUE", 29, 1, 18);
            uiForm.setUiControl("pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Rtb_Cnt", pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Rtb_Cnt, 
                true, 29, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Rtb_Amt", pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Rtb_Amt, 
                true, 29, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Rtb_Rlvr_Cnt", pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Rtb_Rlvr_Cnt, 
                true, 29, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Rtb_Rlvr_Amt", pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Rtb_Rlvr_Amt, 
                true, 29, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Cnt", pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Cnt, 
                true, 29, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Amt", pnd_Cref_Monthly_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Mth_Maturity_Amt, 
                true, 29, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_43", "STOCK", "BLUE", 31, 1, 5);
            uiForm.setUiLabel("label_44", "ANNUAL", "BLUE", 31, 8, 6);
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt_3pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt.getValue(3+0), 
                true, 31, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt_3pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt.getValue(3+0), 
                true, 31, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc_3pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc.getValue(3+0), 
                true, 31, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla_3pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla.getValue(3+0), 
                true, 31, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt_3pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt.getValue(3+0), 
                true, 31, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt_3pls0", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt.getValue(3+0), 
                true, 31, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_45", "MMA", "BLUE", 32, 1, 3);
            uiForm.setUiLabel("label_46", "ANNUAL", "BLUE", 32, 8, 6);
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt_3pls1", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt.getValue(3+1), 
                true, 32, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt_3pls1", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt.getValue(3+1), 
                true, 32, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc_3pls1", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc.getValue(3+1), 
                true, 32, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla_3pls1", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla.getValue(3+1), 
                true, 32, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt_3pls1", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt.getValue(3+1), 
                true, 32, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt_3pls1", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt.getValue(3+1), 
                true, 32, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_47", "SOCIAL ANNUAL", "BLUE", 33, 1, 13);
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt_3pls2", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt.getValue(3+2), 
                true, 33, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt_3pls2", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt.getValue(3+2), 
                true, 33, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc_3pls2", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc.getValue(3+2), 
                true, 33, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla_3pls2", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla.getValue(3+2), 
                true, 33, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt_3pls2", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt.getValue(3+2), 
                true, 33, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt_3pls2", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt.getValue(3+2), 
                true, 33, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_48", "BOND", "BLUE", 34, 1, 4);
            uiForm.setUiLabel("label_49", "ANNUAL", "BLUE", 34, 8, 6);
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt_3pls3", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt.getValue(3+3), 
                true, 34, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt_3pls3", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt.getValue(3+3), 
                true, 34, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc_3pls3", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc.getValue(3+3), 
                true, 34, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla_3pls3", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla.getValue(3+3), 
                true, 34, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt_3pls3", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt.getValue(3+3), 
                true, 34, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt_3pls3", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt.getValue(3+3), 
                true, 34, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_50", "GLOBAL ANNUAL", "BLUE", 35, 1, 13);
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt_3pls4", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt.getValue(3+4), 
                true, 35, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt_3pls4", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt.getValue(3+4), 
                true, 35, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc_3pls4", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc.getValue(3+4), 
                true, 35, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla_3pls4", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla.getValue(3+4), 
                true, 35, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt_3pls4", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt.getValue(3+4), 
                true, 35, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt_3pls4", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt.getValue(3+4), 
                true, 35, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_51", "GROWTH ANNUAL", "BLUE", 36, 1, 13);
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt_3pls5", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt.getValue(3+5), 
                true, 36, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt_3pls5", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt.getValue(3+5), 
                true, 36, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc_3pls5", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc.getValue(3+5), 
                true, 36, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla_3pls5", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla.getValue(3+5), 
                true, 36, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt_3pls5", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt.getValue(3+5), 
                true, 36, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt_3pls5", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt.getValue(3+5), 
                true, 36, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_52", "EQUITY ANNUAL", "BLUE", 37, 1, 13);
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt_3pls6", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt.getValue(3+6), 
                true, 37, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt_3pls6", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt.getValue(3+6), 
                true, 37, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc_3pls6", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc.getValue(3+6), 
                true, 37, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla_3pls6", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla.getValue(3+6), 
                true, 37, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt_3pls6", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt.getValue(3+6), 
                true, 37, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt_3pls6", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt.getValue(3+6), 
                true, 37, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_53", "I L B", "BLUE", 38, 1, 5);
            uiForm.setUiLabel("label_54", "ANNUAL", "BLUE", 38, 8, 6);
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt_3pls7", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Cnt.getValue(3+7), 
                true, 38, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt_3pls7", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Amt.getValue(3+7), 
                true, 38, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc_3pls7", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rlc.getValue(3+7), 
                true, 38, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla_3pls7", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Rtb_Rla.getValue(3+7), 
                true, 38, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt_3pls7", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Cnt.getValue(3+7), 
                true, 38, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt_3pls7", pnd_Cref_Annually_Grand_Accum_Pnd_Cref_Grn_Ann_Mat_Amt.getValue(3+7), 
                true, 38, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_55", "TOTAL CREF ANNUAL", "BLUE", 39, 1, 17);
            uiForm.setUiControl("pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Cnt", pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Cnt, 
                true, 39, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Amt", pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Amt, 
                true, 39, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Rlvr_Cnt", pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Rlvr_Cnt, 
                true, 39, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Rlvr_Amt", pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Rtb_Rlvr_Amt, 
                true, 39, 73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Cnt", pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Cnt, 
                true, 39, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Amt", pnd_Cref_Annually_Sub_Grand_Accum_Pnd_Cref_Sub_Grn_Ann_Maturity_Amt, 
                true, 39, 114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_56", "TOTAL ALL", "BLUE", 42, 1, 9);
            uiForm.setUiControl("pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Rtb_Cnt", pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Rtb_Cnt, true, 42, 
                24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rtb_Amt", pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rtb_Amt, true, 42, 37, 18, 
                "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Rtb_Rlvr_Cnt", pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Rtb_Rlvr_Cnt, 
                true, 42, 61, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rtb_Rlvr_Amt", pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rtb_Rlvr_Amt, true, 42, 
                73, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt", pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Rate_Maturity_Cnt, 
                true, 42, 99, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt", pnd_Cref_And_Tiaa_Grand_Accum_Pnd_Grand_Maturity_Amt, true, 42, 
                114, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
