/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:41 PM
**        *   FROM NATURAL MAP   :  Adsm883
************************************************************
**        * FILE NAME               : Adsm883.java
**        * CLASS NAME              : Adsm883
**        * INSTANCE NAME           : Adsm883
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #CREF-DATA-ENTRY-INFORMATION.#CREF-RQST-FIRST-ENTERED-BY                                                                 *     #CREF-DATA-ENTRY-INFORMATION.#CREF-RQST-FIRST-VERIFIED-BY 
    *     #CREF-DATA-ENTRY-INFORMATION.#CREF-RQST-LAST-UPDATED-BY                                                                  *     #CREF-DATA-ENTRY-INFORMATION.#CREF-RQST-LAST-VERIFIED-BY 
    *     #FIRST-ENTERED-DATE #FIRST-VERIFIED-DATE #LAST-UPDATED-DATE                                                              *     #LAST-VERIFIED-DATE 
    #TIAA-IVC-AMOUNTS.#TIAA-IVC-ANNUITY                                                                  *     #TIAA-IVC-AMOUNTS.#TIAA-IVC-RTB #TIAA-IVC-AMOUNTS.#TIAA-IVC-TOTAL
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm883 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Entered_By;
    private DbsField pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Verified_By;
    private DbsField pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Updated_By;
    private DbsField pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Verified_By;
    private DbsField pnd_First_Entered_Date;
    private DbsField pnd_First_Verified_Date;
    private DbsField pnd_Last_Updated_Date;
    private DbsField pnd_Last_Verified_Date;
    private DbsField pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Annuity;
    private DbsField pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Rtb;
    private DbsField pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Total;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Entered_By = parameters.newFieldInRecord("pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Entered_By", 
            "#CREF-DATA-ENTRY-INFORMATION.#CREF-RQST-FIRST-ENTERED-BY", FieldType.STRING, 8);
        pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Verified_By = parameters.newFieldInRecord("pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Verified_By", 
            "#CREF-DATA-ENTRY-INFORMATION.#CREF-RQST-FIRST-VERIFIED-BY", FieldType.STRING, 8);
        pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Updated_By = parameters.newFieldInRecord("pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Updated_By", 
            "#CREF-DATA-ENTRY-INFORMATION.#CREF-RQST-LAST-UPDATED-BY", FieldType.STRING, 8);
        pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Verified_By = parameters.newFieldInRecord("pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Verified_By", 
            "#CREF-DATA-ENTRY-INFORMATION.#CREF-RQST-LAST-VERIFIED-BY", FieldType.STRING, 8);
        pnd_First_Entered_Date = parameters.newFieldInRecord("pnd_First_Entered_Date", "#FIRST-ENTERED-DATE", FieldType.NUMERIC, 8);
        pnd_First_Verified_Date = parameters.newFieldInRecord("pnd_First_Verified_Date", "#FIRST-VERIFIED-DATE", FieldType.NUMERIC, 8);
        pnd_Last_Updated_Date = parameters.newFieldInRecord("pnd_Last_Updated_Date", "#LAST-UPDATED-DATE", FieldType.NUMERIC, 8);
        pnd_Last_Verified_Date = parameters.newFieldInRecord("pnd_Last_Verified_Date", "#LAST-VERIFIED-DATE", FieldType.NUMERIC, 8);
        pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Annuity = parameters.newFieldInRecord("pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Annuity", "#TIAA-IVC-AMOUNTS.#TIAA-IVC-ANNUITY", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Rtb = parameters.newFieldInRecord("pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Rtb", "#TIAA-IVC-AMOUNTS.#TIAA-IVC-RTB", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Total = parameters.newFieldInRecord("pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Total", "#TIAA-IVC-AMOUNTS.#TIAA-IVC-TOTAL", 
            FieldType.PACKED_DECIMAL, 12, 2);
        parameters.reset();
    }

    public Adsm883() throws Exception
    {
        super("Adsm883");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm883", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm883"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "TIAA IVC TOTAL AMOUNTS", "BLUE", 2, 32, 22);
            uiForm.setUiLabel("label_2", "______________________", "", 3, 32, 22);
            uiForm.setUiLabel("label_3", "TIAA-IVC-TOTAL", "BLUE", 5, 3, 14);
            uiForm.setUiLabel("label_4", "TIAA-IVC-RTB", "BLUE", 5, 26, 12);
            uiForm.setUiLabel("label_5", "TIAA-IVC-ANNUITY", "BLUE", 5, 43, 16);
            uiForm.setUiLabel("label_6", "--------------", "BLUE", 6, 3, 14);
            uiForm.setUiLabel("label_7", "--------------", "BLUE", 6, 24, 14);
            uiForm.setUiLabel("label_8", "----------------", "", 6, 44, 16);
            uiForm.setUiControl("pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Total", pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Total, true, 7, 3, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Rtb", pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Rtb, true, 7, 24, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Annuity", pnd_Tiaa_Ivc_Amounts_Pnd_Tiaa_Ivc_Annuity, true, 7, 45, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_9", "-----------------------------------------------------------------------------", "", 9, 1, 77);
            uiForm.setUiLabel("label_10", "DATA ENTRY INFORMATION:", "BLUE", 10, 1, 23);
            uiForm.setUiLabel("label_11", "-----------------------------------------------------------------------------", "", 11, 1, 77);
            uiForm.setUiLabel("label_12", "Request First Entered ...........:", "BLUE", 12, 1, 34);
            uiForm.setUiControl("pnd_First_Entered_Date", pnd_First_Entered_Date, true, 12, 36, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_13", "Request First Entered By.........:", "BLUE", 13, 1, 34);
            uiForm.setUiControl("pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Entered_By", pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Entered_By, 
                true, 13, 36, 8, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_14", "Request First Verified...........:", "BLUE", 14, 1, 34);
            uiForm.setUiControl("pnd_First_Verified_Date", pnd_First_Verified_Date, true, 14, 36, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_15", "Request First Verified By........:", "BLUE", 15, 1, 34);
            uiForm.setUiControl("pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Verified_By", pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_First_Verified_By, 
                true, 15, 36, 8, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_16", "Request Last Updated.............:", "BLUE", 16, 1, 34);
            uiForm.setUiControl("pnd_Last_Updated_Date", pnd_Last_Updated_Date, true, 16, 36, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "Request Last Updated By..........:", "BLUE", 17, 1, 34);
            uiForm.setUiControl("pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Updated_By", pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Updated_By, 
                true, 17, 36, 8, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_18", "Request Last Verified............:", "BLUE", 18, 1, 34);
            uiForm.setUiControl("pnd_Last_Verified_Date", pnd_Last_Verified_Date, true, 18, 36, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", "Request Last Verified By.........:", "BLUE", 19, 1, 34);
            uiForm.setUiControl("pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Verified_By", pnd_Cref_Data_Entry_Information_Pnd_Cref_Rqst_Last_Verified_By, 
                true, 19, 36, 8, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
