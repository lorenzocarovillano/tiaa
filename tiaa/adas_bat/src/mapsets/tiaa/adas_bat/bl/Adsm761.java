/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:54 PM
**        *   FROM NATURAL MAP   :  Adsm761
************************************************************
**        * FILE NAME               : Adsm761.java
**        * CLASS NAME              : Adsm761
**        * INSTANCE NAME           : Adsm761
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #ACCOUNT-DTE-MDCY #ANNT-START-DTE-MDCY #CONT-ISSUE-DTE-MDCY                                                              *     #DESTINATION-CODE.#DESTINATION-CDE(*) 
    #EFFECTIVE-DTE-MDCY                                                                *     #EMPL-TERM-DTE-MDCY #FORM-COMP-DTE-MDCY #FRST-BIRTH-DTE-MDCY 
    *     #FRST-CITIZENSHIP #FRST-PEROD-DTE-MDCY #IA-NUMBER                                                                        *     #LAST-GUAR-DTE-MDCY 
    #LAST-PREM-DTE-MDCY #NAP-ANNTY-OPTN                                                                  *     #NAP-GRNTEE-PERIOD #NAP-PYMNT-MODE-ALL #NON-PREM-DTE-MDCY 
    *     #PARTCIPANT-NAME-1 #PARTCIPANT-NAME-2 #RESIDENCE #ROLLOVER-AMOUNT                                                        *     #ROLLOVER-CONTRACT-NBR(*) 
    #RQST-ID.#UNIQUE-ID #RTB                                                                       *     #RTB-ACCOUNT-DTE-MDCY #RTB-ROLLOVER-CODE.#RTB-ROLLOVER-CDE(*) 
    *     #RTB-SETTLE-DTE-MDCY #SCND-BIRTH-DTE-MDCY #SCND-SPOUSE #SEX-1                                                            *     #SEX-2 #SLASH #SOCIAL-SEC-NBR-1 
    #SOCIAL-SEC-NBR-2                                                                        *     #TARGET-ALL.#TARGET-DISC(*) #TARGET-ALL.#TARGET-NUMBER(*) 
    *     #TIAA-CERT-IND(*) #TIAA-CERT-NBR(*)                                                                                      *     #TIAA-HEADER-SETTLE-AMOUNTS.#TIAA-HEADER-RTB-SETTLE-AMOUNT 
    *     #TIAA-HEADER-SETTLE-AMOUNTS.#TIAA-HEADER-SETTLE-AMOUNT                                                                   *     #WORK-RQST-ID
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm761 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Account_Dte_Mdcy;
    private DbsField pnd_Annt_Start_Dte_Mdcy;
    private DbsField pnd_Cont_Issue_Dte_Mdcy;
    private DbsField pnd_Destination_Code_Pnd_Destination_Cde;
    private DbsField pnd_Effective_Dte_Mdcy;
    private DbsField pnd_Empl_Term_Dte_Mdcy;
    private DbsField pnd_Form_Comp_Dte_Mdcy;
    private DbsField pnd_Frst_Birth_Dte_Mdcy;
    private DbsField pnd_Frst_Citizenship;
    private DbsField pnd_Frst_Perod_Dte_Mdcy;
    private DbsField pnd_Ia_Number;
    private DbsField pnd_Last_Guar_Dte_Mdcy;
    private DbsField pnd_Last_Prem_Dte_Mdcy;
    private DbsField pnd_Nap_Annty_Optn;
    private DbsField pnd_Nap_Grntee_Period;
    private DbsField pnd_Nap_Pymnt_Mode_All;
    private DbsField pnd_Non_Prem_Dte_Mdcy;
    private DbsField pnd_Partcipant_Name_1;
    private DbsField pnd_Partcipant_Name_2;
    private DbsField pnd_Residence;
    private DbsField pnd_Rollover_Amount;
    private DbsField pnd_Rollover_Contract_Nbr;
    private DbsField pnd_Rqst_Id_Pnd_Unique_Id;
    private DbsField pnd_Rtb;
    private DbsField pnd_Rtb_Account_Dte_Mdcy;
    private DbsField pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde;
    private DbsField pnd_Rtb_Settle_Dte_Mdcy;
    private DbsField pnd_Scnd_Birth_Dte_Mdcy;
    private DbsField pnd_Scnd_Spouse;
    private DbsField pnd_Sex_1;
    private DbsField pnd_Sex_2;
    private DbsField pnd_Slash;
    private DbsField pnd_Social_Sec_Nbr_1;
    private DbsField pnd_Social_Sec_Nbr_2;
    private DbsField pnd_Target_All_Pnd_Target_Disc;
    private DbsField pnd_Target_All_Pnd_Target_Number;
    private DbsField pnd_Tiaa_Cert_Ind;
    private DbsField pnd_Tiaa_Cert_Nbr;
    private DbsField pnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Rtb_Settle_Amount;
    private DbsField pnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Settle_Amount;
    private DbsField pnd_Work_Rqst_Id;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Account_Dte_Mdcy = parameters.newFieldInRecord("pnd_Account_Dte_Mdcy", "#ACCOUNT-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Annt_Start_Dte_Mdcy = parameters.newFieldInRecord("pnd_Annt_Start_Dte_Mdcy", "#ANNT-START-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Cont_Issue_Dte_Mdcy = parameters.newFieldInRecord("pnd_Cont_Issue_Dte_Mdcy", "#CONT-ISSUE-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Destination_Code_Pnd_Destination_Cde = parameters.newFieldArrayInRecord("pnd_Destination_Code_Pnd_Destination_Cde", "#DESTINATION-CODE.#DESTINATION-CDE", 
            FieldType.STRING, 5, new DbsArrayController(1, 3));
        pnd_Effective_Dte_Mdcy = parameters.newFieldInRecord("pnd_Effective_Dte_Mdcy", "#EFFECTIVE-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Empl_Term_Dte_Mdcy = parameters.newFieldInRecord("pnd_Empl_Term_Dte_Mdcy", "#EMPL-TERM-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Form_Comp_Dte_Mdcy = parameters.newFieldInRecord("pnd_Form_Comp_Dte_Mdcy", "#FORM-COMP-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Frst_Birth_Dte_Mdcy = parameters.newFieldInRecord("pnd_Frst_Birth_Dte_Mdcy", "#FRST-BIRTH-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Frst_Citizenship = parameters.newFieldInRecord("pnd_Frst_Citizenship", "#FRST-CITIZENSHIP", FieldType.STRING, 2);
        pnd_Frst_Perod_Dte_Mdcy = parameters.newFieldInRecord("pnd_Frst_Perod_Dte_Mdcy", "#FRST-PEROD-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Ia_Number = parameters.newFieldInRecord("pnd_Ia_Number", "#IA-NUMBER", FieldType.STRING, 10);
        pnd_Last_Guar_Dte_Mdcy = parameters.newFieldInRecord("pnd_Last_Guar_Dte_Mdcy", "#LAST-GUAR-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Last_Prem_Dte_Mdcy = parameters.newFieldInRecord("pnd_Last_Prem_Dte_Mdcy", "#LAST-PREM-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Nap_Annty_Optn = parameters.newFieldInRecord("pnd_Nap_Annty_Optn", "#NAP-ANNTY-OPTN", FieldType.STRING, 2);
        pnd_Nap_Grntee_Period = parameters.newFieldInRecord("pnd_Nap_Grntee_Period", "#NAP-GRNTEE-PERIOD", FieldType.NUMERIC, 2);
        pnd_Nap_Pymnt_Mode_All = parameters.newFieldInRecord("pnd_Nap_Pymnt_Mode_All", "#NAP-PYMNT-MODE-ALL", FieldType.STRING, 20);
        pnd_Non_Prem_Dte_Mdcy = parameters.newFieldInRecord("pnd_Non_Prem_Dte_Mdcy", "#NON-PREM-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Partcipant_Name_1 = parameters.newFieldInRecord("pnd_Partcipant_Name_1", "#PARTCIPANT-NAME-1", FieldType.STRING, 30);
        pnd_Partcipant_Name_2 = parameters.newFieldInRecord("pnd_Partcipant_Name_2", "#PARTCIPANT-NAME-2", FieldType.STRING, 30);
        pnd_Residence = parameters.newFieldInRecord("pnd_Residence", "#RESIDENCE", FieldType.STRING, 20);
        pnd_Rollover_Amount = parameters.newFieldInRecord("pnd_Rollover_Amount", "#ROLLOVER-AMOUNT", FieldType.NUMERIC, 12, 2);
        pnd_Rollover_Contract_Nbr = parameters.newFieldArrayInRecord("pnd_Rollover_Contract_Nbr", "#ROLLOVER-CONTRACT-NBR", FieldType.STRING, 10, new 
            DbsArrayController(1, 2));
        pnd_Rqst_Id_Pnd_Unique_Id = parameters.newFieldInRecord("pnd_Rqst_Id_Pnd_Unique_Id", "#RQST-ID.#UNIQUE-ID", FieldType.NUMERIC, 12);
        pnd_Rtb = parameters.newFieldInRecord("pnd_Rtb", "#RTB", FieldType.STRING, 1);
        pnd_Rtb_Account_Dte_Mdcy = parameters.newFieldInRecord("pnd_Rtb_Account_Dte_Mdcy", "#RTB-ACCOUNT-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde = parameters.newFieldArrayInRecord("pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde", "#RTB-ROLLOVER-CODE.#RTB-ROLLOVER-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 3));
        pnd_Rtb_Settle_Dte_Mdcy = parameters.newFieldInRecord("pnd_Rtb_Settle_Dte_Mdcy", "#RTB-SETTLE-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Scnd_Birth_Dte_Mdcy = parameters.newFieldInRecord("pnd_Scnd_Birth_Dte_Mdcy", "#SCND-BIRTH-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Scnd_Spouse = parameters.newFieldInRecord("pnd_Scnd_Spouse", "#SCND-SPOUSE", FieldType.STRING, 1);
        pnd_Sex_1 = parameters.newFieldInRecord("pnd_Sex_1", "#SEX-1", FieldType.STRING, 1);
        pnd_Sex_2 = parameters.newFieldInRecord("pnd_Sex_2", "#SEX-2", FieldType.STRING, 1);
        pnd_Slash = parameters.newFieldInRecord("pnd_Slash", "#SLASH", FieldType.STRING, 1);
        pnd_Social_Sec_Nbr_1 = parameters.newFieldInRecord("pnd_Social_Sec_Nbr_1", "#SOCIAL-SEC-NBR-1", FieldType.NUMERIC, 9);
        pnd_Social_Sec_Nbr_2 = parameters.newFieldInRecord("pnd_Social_Sec_Nbr_2", "#SOCIAL-SEC-NBR-2", FieldType.NUMERIC, 9);
        pnd_Target_All_Pnd_Target_Disc = parameters.newFieldArrayInRecord("pnd_Target_All_Pnd_Target_Disc", "#TARGET-ALL.#TARGET-DISC", FieldType.STRING, 
            5, new DbsArrayController(1, 15));
        pnd_Target_All_Pnd_Target_Number = parameters.newFieldArrayInRecord("pnd_Target_All_Pnd_Target_Number", "#TARGET-ALL.#TARGET-NUMBER", FieldType.STRING, 
            6, new DbsArrayController(1, 15));
        pnd_Tiaa_Cert_Ind = parameters.newFieldArrayInRecord("pnd_Tiaa_Cert_Ind", "#TIAA-CERT-IND", FieldType.STRING, 6, new DbsArrayController(1, 10));
        pnd_Tiaa_Cert_Nbr = parameters.newFieldArrayInRecord("pnd_Tiaa_Cert_Nbr", "#TIAA-CERT-NBR", FieldType.STRING, 11, new DbsArrayController(1, 10));
        pnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Rtb_Settle_Amount = parameters.newFieldInRecord("pnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Rtb_Settle_Amount", 
            "#TIAA-HEADER-SETTLE-AMOUNTS.#TIAA-HEADER-RTB-SETTLE-AMOUNT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Settle_Amount = parameters.newFieldInRecord("pnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Settle_Amount", 
            "#TIAA-HEADER-SETTLE-AMOUNTS.#TIAA-HEADER-SETTLE-AMOUNT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Work_Rqst_Id = parameters.newFieldInRecord("pnd_Work_Rqst_Id", "#WORK-RQST-ID", FieldType.STRING, 6);
        parameters.reset();
    }

    public Adsm761() throws Exception
    {
        super("Adsm761");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=080 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm761", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm761"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "Work Request ID.:", "BLUE", 2, 1, 17);
            uiForm.setUiControl("pnd_Work_Rqst_Id", pnd_Work_Rqst_Id, true, 2, 19, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "Annuitant:", "BLUE", 2, 41, 10);
            uiForm.setUiControl("pnd_Partcipant_Name_1", pnd_Partcipant_Name_1, true, 2, 52, 28, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_3", "Pin Number:", "BLUE", 3, 1, 11);
            uiForm.setUiControl("pnd_Rqst_Id_Pnd_Unique_Id", pnd_Rqst_Id_Pnd_Unique_Id, true, 3, 13, 12, "WHITE", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "IA Number:", "BLUE", 3, 41, 10);
            uiForm.setUiControl("pnd_Ia_Number", pnd_Ia_Number, true, 3, 52, 11, "WHITE", "XXXXXXX-XXX", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_5", "SSN:", "BLUE", 4, 1, 4);
            uiForm.setUiControl("pnd_Social_Sec_Nbr_1", pnd_Social_Sec_Nbr_1, true, 4, 6, 11, "WHITE", "999-99-9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "Date of Birth:", "BLUE", 4, 41, 14);
            uiForm.setUiControl("pnd_Frst_Birth_Dte_Mdcy", pnd_Frst_Birth_Dte_Mdcy, true, 4, 56, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_7", "Sex:", "BLUE", 4, 72, 4);
            uiForm.setUiControl("pnd_Sex_1", pnd_Sex_1, true, 4, 77, 1, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_8", "Citizenship:", "BLUE", 5, 1, 12);
            uiForm.setUiControl("pnd_Frst_Citizenship", pnd_Frst_Citizenship, true, 5, 14, 2, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_9", "Residence:", "BLUE", 5, 41, 10);
            uiForm.setUiControl("pnd_Residence", pnd_Residence, true, 5, 52, 20, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_10", "Annuitant Partner:", "BLUE", 8, 1, 18);
            uiForm.setUiControl("pnd_Partcipant_Name_2", pnd_Partcipant_Name_2, true, 8, 20, 20, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_11", "Relationship:", "BLUE", 8, 41, 13);
            uiForm.setUiControl("pnd_Scnd_Spouse", pnd_Scnd_Spouse, true, 8, 55, 1, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_12", "Sex:", "BLUE", 8, 72, 4);
            uiForm.setUiControl("pnd_Sex_2", pnd_Sex_2, true, 8, 77, 1, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_13", "SSN:", "BLUE", 9, 1, 4);
            uiForm.setUiControl("pnd_Social_Sec_Nbr_2", pnd_Social_Sec_Nbr_2, true, 9, 6, 11, "WHITE", "999-99-9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_14", "Date of Birth:", "BLUE", 9, 41, 14);
            uiForm.setUiControl("pnd_Scnd_Birth_Dte_Mdcy", pnd_Scnd_Birth_Dte_Mdcy, true, 9, 56, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_15", "Contract Issue Date:", "BLUE", 11, 1, 20);
            uiForm.setUiControl("pnd_Cont_Issue_Dte_Mdcy", pnd_Cont_Issue_Dte_Mdcy, true, 11, 22, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_16", "Total Settlement Amount:", "BLUE", 11, 41, 24);
            uiForm.setUiControl("pnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Settle_Amount", pnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Settle_Amount, 
                true, 11, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "Annuity Start Date :", "BLUE", 12, 1, 20);
            uiForm.setUiControl("pnd_Annt_Start_Dte_Mdcy", pnd_Annt_Start_Dte_Mdcy, true, 12, 22, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_18", "First Periodic Payment Date:", "BLUE", 12, 41, 28);
            uiForm.setUiControl("pnd_Frst_Perod_Dte_Mdcy", pnd_Frst_Perod_Dte_Mdcy, true, 12, 70, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", "Last Premium Received Date:", "BLUE", 13, 1, 27);
            uiForm.setUiControl("pnd_Last_Prem_Dte_Mdcy", pnd_Last_Prem_Dte_Mdcy, true, 13, 29, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_20", "Forms Completion Date:", "BLUE", 13, 41, 22);
            uiForm.setUiControl("pnd_Form_Comp_Dte_Mdcy", pnd_Form_Comp_Dte_Mdcy, true, 13, 64, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_21", "Accounting Date:", "BLUE", 14, 1, 16);
            uiForm.setUiControl("pnd_Account_Dte_Mdcy", pnd_Account_Dte_Mdcy, true, 14, 18, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_22", "Non Premium Effective Date:", "BLUE", 14, 41, 27);
            uiForm.setUiControl("pnd_Non_Prem_Dte_Mdcy", pnd_Non_Prem_Dte_Mdcy, true, 14, 69, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_23", "RTB:", "BLUE", 16, 1, 4);
            uiForm.setUiControl("pnd_Rtb", pnd_Rtb, true, 16, 6, 1, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_24", "Effective Date:", "BLUE", 16, 10, 15);
            uiForm.setUiControl("pnd_Effective_Dte_Mdcy", pnd_Effective_Dte_Mdcy, true, 16, 26, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_25", "RTB Accounting Date:", "BLUE", 16, 41, 20);
            uiForm.setUiControl("pnd_Rtb_Account_Dte_Mdcy", pnd_Rtb_Account_Dte_Mdcy, true, 16, 62, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_26", "RTB Settlement Date:", "BLUE", 17, 1, 20);
            uiForm.setUiControl("pnd_Rtb_Settle_Dte_Mdcy", pnd_Rtb_Settle_Dte_Mdcy, true, 17, 22, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_27", "RTB Settlement Amount:", "BLUE", 17, 41, 22);
            uiForm.setUiControl("pnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Rtb_Settle_Amount", pnd_Tiaa_Header_Settle_Amounts_Pnd_Tiaa_Header_Rtb_Settle_Amount, 
                true, 17, 64, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_28", "RTB Rollover:", "BLUE", 18, 1, 13);
            uiForm.setUiControl("pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde_1pls0", pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde.getValue(1+0), true, 18, 
                15, 2, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde_1pls1", pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde.getValue(1+1), true, 18, 
                18, 2, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde_1pls2", pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde.getValue(1+2), true, 18, 
                21, 2, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_29", "Destination Code:", "BLUE", 18, 41, 17);
            uiForm.setUiControl("pnd_Destination_Code_Pnd_Destination_Cde_1pls0", pnd_Destination_Code_Pnd_Destination_Cde.getValue(1+0), true, 18, 59, 
                5, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Destination_Code_Pnd_Destination_Cde_1pls1", pnd_Destination_Code_Pnd_Destination_Cde.getValue(1+1), true, 18, 65, 
                5, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Destination_Code_Pnd_Destination_Cde_1pls2", pnd_Destination_Code_Pnd_Destination_Cde.getValue(1+2), true, 18, 71, 
                5, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_30", "Rollover Contract #:", "BLUE", 19, 1, 20);
            uiForm.setUiControl("pnd_Rollover_Contract_Nbr_1", pnd_Rollover_Contract_Nbr.getValue(1), true, 19, 22, 10, "WHITE", true, false, null, null, 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_31", "Rollover Dollar Amount:", "BLUE", 19, 41, 23);
            uiForm.setUiControl("pnd_Rollover_Amount", pnd_Rollover_Amount, true, 19, 65, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rollover_Contract_Nbr_2", pnd_Rollover_Contract_Nbr.getValue(2), true, 20, 22, 10, "WHITE", true, false, null, null, 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_32", "Option:", "BLUE", 21, 1, 7);
            uiForm.setUiControl("pnd_Nap_Annty_Optn", pnd_Nap_Annty_Optn, true, 21, 9, 2, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Slash", pnd_Slash, true, 21, 12, 1, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Nap_Grntee_Period", pnd_Nap_Grntee_Period, true, 21, 14, 2, "WHITE", "99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_33", "Employment Termination Date:", "BLUE", 21, 41, 28);
            uiForm.setUiControl("pnd_Empl_Term_Dte_Mdcy", pnd_Empl_Term_Dte_Mdcy, true, 21, 70, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_34", "Mode:", "BLUE", 22, 1, 5);
            uiForm.setUiControl("pnd_Nap_Pymnt_Mode_All", pnd_Nap_Pymnt_Mode_All, true, 22, 7, 20, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_35", "Last Guaranteed Pay Date", "BLUE", 22, 41, 24);
            uiForm.setUiLabel("label_36", ":", "BLUE", 22, 68, 1);
            uiForm.setUiControl("pnd_Last_Guar_Dte_Mdcy", pnd_Last_Guar_Dte_Mdcy, true, 22, 70, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_37", "TIAA Contract NO:", "BLUE", 24, 1, 17);
            uiForm.setUiControl("pnd_Tiaa_Cert_Nbr_1pls0", pnd_Tiaa_Cert_Nbr.getValue(1+0), true, 24, 19, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Tiaa_Cert_Ind_1pls0", pnd_Tiaa_Cert_Ind.getValue(1+0), true, 24, 31, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_38", "Target:", "BLUE", 24, 41, 7);
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls0", pnd_Target_All_Pnd_Target_Number.getValue(1+0), true, 24, 49, 6, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls0", pnd_Target_All_Pnd_Target_Disc.getValue(1+0), true, 24, 56, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Cert_Nbr_1pls1", pnd_Tiaa_Cert_Nbr.getValue(1+1), true, 25, 19, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Tiaa_Cert_Ind_1pls1", pnd_Tiaa_Cert_Ind.getValue(1+1), true, 25, 31, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls1", pnd_Target_All_Pnd_Target_Number.getValue(1+1), true, 25, 49, 6, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls1", pnd_Target_All_Pnd_Target_Disc.getValue(1+1), true, 25, 56, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Cert_Nbr_1pls2", pnd_Tiaa_Cert_Nbr.getValue(1+2), true, 26, 19, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Tiaa_Cert_Ind_1pls2", pnd_Tiaa_Cert_Ind.getValue(1+2), true, 26, 31, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls2", pnd_Target_All_Pnd_Target_Number.getValue(1+2), true, 26, 49, 6, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls2", pnd_Target_All_Pnd_Target_Disc.getValue(1+2), true, 26, 56, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Cert_Nbr_1pls3", pnd_Tiaa_Cert_Nbr.getValue(1+3), true, 27, 19, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Tiaa_Cert_Ind_1pls3", pnd_Tiaa_Cert_Ind.getValue(1+3), true, 27, 31, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls3", pnd_Target_All_Pnd_Target_Number.getValue(1+3), true, 27, 49, 6, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls3", pnd_Target_All_Pnd_Target_Disc.getValue(1+3), true, 27, 56, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Cert_Nbr_1pls4", pnd_Tiaa_Cert_Nbr.getValue(1+4), true, 28, 19, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Tiaa_Cert_Ind_1pls4", pnd_Tiaa_Cert_Ind.getValue(1+4), true, 28, 31, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls4", pnd_Target_All_Pnd_Target_Number.getValue(1+4), true, 28, 49, 6, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls4", pnd_Target_All_Pnd_Target_Disc.getValue(1+4), true, 28, 56, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Cert_Nbr_1pls5", pnd_Tiaa_Cert_Nbr.getValue(1+5), true, 29, 19, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Tiaa_Cert_Ind_1pls5", pnd_Tiaa_Cert_Ind.getValue(1+5), true, 29, 31, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls5", pnd_Target_All_Pnd_Target_Number.getValue(1+5), true, 29, 49, 6, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls5", pnd_Target_All_Pnd_Target_Disc.getValue(1+5), true, 29, 56, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Cert_Nbr_1pls6", pnd_Tiaa_Cert_Nbr.getValue(1+6), true, 30, 19, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Tiaa_Cert_Ind_1pls6", pnd_Tiaa_Cert_Ind.getValue(1+6), true, 30, 31, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls6", pnd_Target_All_Pnd_Target_Number.getValue(1+6), true, 30, 49, 6, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls6", pnd_Target_All_Pnd_Target_Disc.getValue(1+6), true, 30, 56, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Cert_Nbr_1pls7", pnd_Tiaa_Cert_Nbr.getValue(1+7), true, 31, 19, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Tiaa_Cert_Ind_1pls7", pnd_Tiaa_Cert_Ind.getValue(1+7), true, 31, 31, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls7", pnd_Target_All_Pnd_Target_Number.getValue(1+7), true, 31, 49, 6, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls7", pnd_Target_All_Pnd_Target_Disc.getValue(1+7), true, 31, 56, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Cert_Nbr_1pls8", pnd_Tiaa_Cert_Nbr.getValue(1+8), true, 32, 19, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Tiaa_Cert_Ind_1pls8", pnd_Tiaa_Cert_Ind.getValue(1+8), true, 32, 31, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls8", pnd_Target_All_Pnd_Target_Number.getValue(1+8), true, 32, 49, 6, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls8", pnd_Target_All_Pnd_Target_Disc.getValue(1+8), true, 32, 56, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Cert_Nbr_1pls9", pnd_Tiaa_Cert_Nbr.getValue(1+9), true, 33, 19, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Tiaa_Cert_Ind_1pls9", pnd_Tiaa_Cert_Ind.getValue(1+9), true, 33, 31, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls9", pnd_Target_All_Pnd_Target_Number.getValue(1+9), true, 33, 49, 6, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls9", pnd_Target_All_Pnd_Target_Disc.getValue(1+9), true, 33, 56, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls10", pnd_Target_All_Pnd_Target_Number.getValue(1+10), true, 34, 49, 6, "WHITE", 
                true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls10", pnd_Target_All_Pnd_Target_Disc.getValue(1+10), true, 34, 56, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls11", pnd_Target_All_Pnd_Target_Number.getValue(1+11), true, 35, 49, 6, "WHITE", 
                true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls11", pnd_Target_All_Pnd_Target_Disc.getValue(1+11), true, 35, 56, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls12", pnd_Target_All_Pnd_Target_Number.getValue(1+12), true, 36, 49, 6, "WHITE", 
                true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls12", pnd_Target_All_Pnd_Target_Disc.getValue(1+12), true, 36, 56, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls13", pnd_Target_All_Pnd_Target_Number.getValue(1+13), true, 37, 49, 6, "WHITE", 
                true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls13", pnd_Target_All_Pnd_Target_Disc.getValue(1+13), true, 37, 56, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls14", pnd_Target_All_Pnd_Target_Number.getValue(1+14), true, 38, 49, 6, "WHITE", 
                true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls14", pnd_Target_All_Pnd_Target_Disc.getValue(1+14), true, 38, 56, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
