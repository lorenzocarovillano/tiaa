/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:27 PM
**        *   FROM NATURAL MAP   :  Adsm808
************************************************************
**        * FILE NAME               : Adsm808.java
**        * CLASS NAME              : Adsm808
**        * INSTANCE NAME           : Adsm808
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #PAYMENT-DUE-DATE #S-C-ANN-FIN-IVC-AMOUNT.#S-C-ANN-FIN-IVC-AMT                                                           *     #S-C-ANN-MINOR-ACCUM.#S-C-ANN-DIVID-AMT(*) 
    *     #S-C-ANN-MINOR-ACCUM.#S-C-ANN-DOLLARS(*)                                                                                 *     #S-C-ANN-MINOR-ACCUM.#S-C-ANN-FIN-DIV-AMT(*) 
    *     #S-C-ANN-MINOR-ACCUM.#S-C-ANN-FIN-GTD-AMT(*)                                                                             *     #S-C-ANN-MINOR-ACCUM.#S-C-ANN-GTD-AMT(*) 
    *     #S-C-ANN-MINOR-ACCUM.#S-C-ANN-PAY-CNT(*)                                                                                 *     #S-C-ANN-MINOR-ACCUM.#S-C-ANN-UNITS(*) 
    *     #S-C-MTH-FIN-IVC-AMOUNT.#S-C-MTH-FIN-IVC-AMT                                                                             *     #S-C-MTH-MINOR-ACCUM.#S-C-MTH-DIV-AMT(*) 
    *     #S-C-MTH-MINOR-ACCUM.#S-C-MTH-DOLLARS(*)                                                                                 *     #S-C-MTH-MINOR-ACCUM.#S-C-MTH-FIN-DIV-AMT(*) 
    *     #S-C-MTH-MINOR-ACCUM.#S-C-MTH-FIN-GTD-AMT(*)                                                                             *     #S-C-MTH-MINOR-ACCUM.#S-C-MTH-GTD-AMT(*) 
    *     #S-C-MTH-MINOR-ACCUM.#S-C-MTH-PAY-CNT(*)                                                                                 *     #S-C-MTH-MINOR-ACCUM.#S-C-MTH-UNITS(*) 
    *     #S-CREF-ANN-SUB-TOTAL-ACCUM.#S-CREF-ANN-SUB-DIV-AMT                                                                      *     #S-CREF-ANN-SUB-TOTAL-ACCUM.#S-CREF-ANN-SUB-DOLLARS 
    *     #S-CREF-ANN-SUB-TOTAL-ACCUM.#S-CREF-ANN-SUB-FIN-DIV-AMT                                                                  *     #S-CREF-ANN-SUB-TOTAL-ACCUM.#S-CREF-ANN-SUB-FIN-GTD-AMT 
    *     #S-CREF-ANN-SUB-TOTAL-ACCUM.#S-CREF-ANN-SUB-FIN-IVC-AMT                                                                  *     #S-CREF-ANN-SUB-TOTAL-ACCUM.#S-CREF-ANN-SUB-GTD-AMT 
    *     #S-CREF-ANN-SUB-TOTAL-ACCUM.#S-CREF-ANN-SUB-PAY-CNT                                                                      *     #S-CREF-ANN-SUB-TOTAL-ACCUM.#S-CREF-ANN-SUB-UNITS 
    *     #S-CREF-MTH-SUB-TOTAL-ACCUM.#S-CREF-MTH-SUB-DIV-AMT                                                                      *     #S-CREF-MTH-SUB-TOTAL-ACCUM.#S-CREF-MTH-SUB-DOLLARS 
    *     #S-CREF-MTH-SUB-TOTAL-ACCUM.#S-CREF-MTH-SUB-FIN-DIV-AMT                                                                  *     #S-CREF-MTH-SUB-TOTAL-ACCUM.#S-CREF-MTH-SUB-FIN-GTD-AMT 
    *     #S-CREF-MTH-SUB-TOTAL-ACCUM.#S-CREF-MTH-SUB-FIN-IVC-AMT                                                                  *     #S-CREF-MTH-SUB-TOTAL-ACCUM.#S-CREF-MTH-SUB-GTD-AMT 
    *     #S-CREF-MTH-SUB-TOTAL-ACCUM.#S-CREF-MTH-SUB-PAY-CNT                                                                      *     #S-CREF-MTH-SUB-TOTAL-ACCUM.#S-CREF-MTH-SUB-UNITS 
    *     #S-TIAA-MINOR-ACCUM.#S-TIAA-GRD-DIV-AMT                                                                                  *     #S-TIAA-MINOR-ACCUM.#S-TIAA-GRD-DOLLARS 
    *     #S-TIAA-MINOR-ACCUM.#S-TIAA-GRD-FIN-DIV-AMT                                                                              *     #S-TIAA-MINOR-ACCUM.#S-TIAA-GRD-FIN-GTD-AMT 
    *     #S-TIAA-MINOR-ACCUM.#S-TIAA-GRD-FIN-IVC-AMT                                                                              *     #S-TIAA-MINOR-ACCUM.#S-TIAA-GRD-GTD-AMT 
    *     #S-TIAA-MINOR-ACCUM.#S-TIAA-GRD-PAY-CNT                                                                                  *     #S-TIAA-MINOR-ACCUM.#S-TIAA-GRD-UNITS 
    *     #S-TIAA-MINOR-ACCUM.#S-TIAA-IPRO-STD-DIV-AMT                                                                             *     #S-TIAA-MINOR-ACCUM.#S-TIAA-IPRO-STD-DOLLARS 
    *     #S-TIAA-MINOR-ACCUM.#S-TIAA-IPRO-STD-FIN-DIV-AMT                                                                         *     #S-TIAA-MINOR-ACCUM.#S-TIAA-IPRO-STD-FIN-GTD-AMT 
    *     #S-TIAA-MINOR-ACCUM.#S-TIAA-IPRO-STD-FIN-IVC-AMT                                                                         *     #S-TIAA-MINOR-ACCUM.#S-TIAA-IPRO-STD-GTD-AMT 
    *     #S-TIAA-MINOR-ACCUM.#S-TIAA-IPRO-STD-PAY-CNT                                                                             *     #S-TIAA-MINOR-ACCUM.#S-TIAA-IPRO-STD-UNITS 
    *     #S-TIAA-MINOR-ACCUM.#S-TIAA-STD-DIV-AMT                                                                                  *     #S-TIAA-MINOR-ACCUM.#S-TIAA-STD-DOLLARS 
    *     #S-TIAA-MINOR-ACCUM.#S-TIAA-STD-FIN-DIV-AMT                                                                              *     #S-TIAA-MINOR-ACCUM.#S-TIAA-STD-FIN-GTD-AMT 
    *     #S-TIAA-MINOR-ACCUM.#S-TIAA-STD-FIN-IVC-AMT                                                                              *     #S-TIAA-MINOR-ACCUM.#S-TIAA-STD-GTD-AMT 
    *     #S-TIAA-MINOR-ACCUM.#S-TIAA-STD-PAY-CNT                                                                                  *     #S-TIAA-MINOR-ACCUM.#S-TIAA-STD-UNITS 
    *     #S-TIAA-MINOR-ACCUM.#S-TIAA-TPA-STD-DIV-AMT                                                                              *     #S-TIAA-MINOR-ACCUM.#S-TIAA-TPA-STD-DOLLARS 
    *     #S-TIAA-MINOR-ACCUM.#S-TIAA-TPA-STD-FIN-DIV-AMT                                                                          *     #S-TIAA-MINOR-ACCUM.#S-TIAA-TPA-STD-FIN-GTD-AMT 
    *     #S-TIAA-MINOR-ACCUM.#S-TIAA-TPA-STD-FIN-IVC-AMT                                                                          *     #S-TIAA-MINOR-ACCUM.#S-TIAA-TPA-STD-GTD-AMT 
    *     #S-TIAA-MINOR-ACCUM.#S-TIAA-TPA-STD-PAY-CNT                                                                              *     #S-TIAA-MINOR-ACCUM.#S-TIAA-TPA-STD-UNITS 
    *     #S-TIAA-SUB-ACCUM.#S-TIAA-SUB-DIVID-AMT                                                                                  *     #S-TIAA-SUB-ACCUM.#S-TIAA-SUB-DOLLARS 
    *     #S-TIAA-SUB-ACCUM.#S-TIAA-SUB-FIN-DIV-AMT                                                                                *     #S-TIAA-SUB-ACCUM.#S-TIAA-SUB-FIN-GTD-AMT 
    *     #S-TIAA-SUB-ACCUM.#S-TIAA-SUB-FIN-IVC-AMT                                                                                *     #S-TIAA-SUB-ACCUM.#S-TIAA-SUB-GTD-AMT 
    *     #S-TIAA-SUB-ACCUM.#S-TIAA-SUB-PAY-CNT                                                                                    *     #S-TIAA-SUB-ACCUM.#S-TIAA-SUB-UNITS 
    *     #S-TOTAL-TIAA-CREF-ACCUM.#S-TOTAL-DIV-AMT                                                                                *     #S-TOTAL-TIAA-CREF-ACCUM.#S-TOTAL-DOLLARS 
    *     #S-TOTAL-TIAA-CREF-ACCUM.#S-TOTAL-FIN-DIV-AMT                                                                            *     #S-TOTAL-TIAA-CREF-ACCUM.#S-TOTAL-FIN-GTD-AMT 
    *     #S-TOTAL-TIAA-CREF-ACCUM.#S-TOTAL-FIN-IVC-AMT                                                                            *     #S-TOTAL-TIAA-CREF-ACCUM.#S-TOTAL-GTD-AMT 
    *     #S-TOTAL-TIAA-CREF-ACCUM.#S-TOTAL-PAY-CNT                                                                                *     #S-TOTAL-TIAA-CREF-ACCUM.#S-TOTAL-UNITS
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm808 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Payment_Due_Date;
    private DbsField pnd_S_C_Ann_Fin_Ivc_Amount_Pnd_S_C_Ann_Fin_Ivc_Amt;
    private DbsField pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt;
    private DbsField pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars;
    private DbsField pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt;
    private DbsField pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt;
    private DbsField pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt;
    private DbsField pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt;
    private DbsField pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units;
    private DbsField pnd_S_C_Mth_Fin_Ivc_Amount_Pnd_S_C_Mth_Fin_Ivc_Amt;
    private DbsField pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt;
    private DbsField pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars;
    private DbsField pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt;
    private DbsField pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt;
    private DbsField pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt;
    private DbsField pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt;
    private DbsField pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units;
    private DbsField pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Div_Amt;
    private DbsField pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Dollars;
    private DbsField pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Div_Amt;
    private DbsField pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Gtd_Amt;
    private DbsField pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Ivc_Amt;
    private DbsField pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Gtd_Amt;
    private DbsField pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Pay_Cnt;
    private DbsField pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Units;
    private DbsField pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Div_Amt;
    private DbsField pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Dollars;
    private DbsField pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Div_Amt;
    private DbsField pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Gtd_Amt;
    private DbsField pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Ivc_Amt;
    private DbsField pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Gtd_Amt;
    private DbsField pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Pay_Cnt;
    private DbsField pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Units;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Div_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Dollars;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Div_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Gtd_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Ivc_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Gtd_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Pay_Cnt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Units;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Div_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Dollars;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Div_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Gtd_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Ivc_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Gtd_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Pay_Cnt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Units;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Div_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Dollars;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Div_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Gtd_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Ivc_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Gtd_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Pay_Cnt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Units;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Div_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Dollars;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Div_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Gtd_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Ivc_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Gtd_Amt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Pay_Cnt;
    private DbsField pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Units;
    private DbsField pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Divid_Amt;
    private DbsField pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Dollars;
    private DbsField pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Div_Amt;
    private DbsField pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Gtd_Amt;
    private DbsField pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Ivc_Amt;
    private DbsField pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Gtd_Amt;
    private DbsField pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Pay_Cnt;
    private DbsField pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Units;
    private DbsField pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Div_Amt;
    private DbsField pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Dollars;
    private DbsField pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Div_Amt;
    private DbsField pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Gtd_Amt;
    private DbsField pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Ivc_Amt;
    private DbsField pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Gtd_Amt;
    private DbsField pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Pay_Cnt;
    private DbsField pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Units;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Payment_Due_Date = parameters.newFieldInRecord("pnd_Payment_Due_Date", "#PAYMENT-DUE-DATE", FieldType.NUMERIC, 8);
        pnd_S_C_Ann_Fin_Ivc_Amount_Pnd_S_C_Ann_Fin_Ivc_Amt = parameters.newFieldInRecord("pnd_S_C_Ann_Fin_Ivc_Amount_Pnd_S_C_Ann_Fin_Ivc_Amt", "#S-C-ANN-FIN-IVC-AMOUNT.#S-C-ANN-FIN-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 10, 2);
        pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt = parameters.newFieldArrayInRecord("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt", "#S-C-ANN-MINOR-ACCUM.#S-C-ANN-DIVID-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars = parameters.newFieldArrayInRecord("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars", "#S-C-ANN-MINOR-ACCUM.#S-C-ANN-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt = parameters.newFieldArrayInRecord("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt", "#S-C-ANN-MINOR-ACCUM.#S-C-ANN-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt", "#S-C-ANN-MINOR-ACCUM.#S-C-ANN-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt", "#S-C-ANN-MINOR-ACCUM.#S-C-ANN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt = parameters.newFieldArrayInRecord("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt", "#S-C-ANN-MINOR-ACCUM.#S-C-ANN-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units = parameters.newFieldArrayInRecord("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units", "#S-C-ANN-MINOR-ACCUM.#S-C-ANN-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4, new DbsArrayController(1, 20));
        pnd_S_C_Mth_Fin_Ivc_Amount_Pnd_S_C_Mth_Fin_Ivc_Amt = parameters.newFieldInRecord("pnd_S_C_Mth_Fin_Ivc_Amount_Pnd_S_C_Mth_Fin_Ivc_Amt", "#S-C-MTH-FIN-IVC-AMOUNT.#S-C-MTH-FIN-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 10, 2);
        pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt = parameters.newFieldArrayInRecord("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt", "#S-C-MTH-MINOR-ACCUM.#S-C-MTH-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars = parameters.newFieldArrayInRecord("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars", "#S-C-MTH-MINOR-ACCUM.#S-C-MTH-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt = parameters.newFieldArrayInRecord("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt", "#S-C-MTH-MINOR-ACCUM.#S-C-MTH-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt", "#S-C-MTH-MINOR-ACCUM.#S-C-MTH-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt", "#S-C-MTH-MINOR-ACCUM.#S-C-MTH-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt = parameters.newFieldArrayInRecord("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt", "#S-C-MTH-MINOR-ACCUM.#S-C-MTH-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units = parameters.newFieldArrayInRecord("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units", "#S-C-MTH-MINOR-ACCUM.#S-C-MTH-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4, new DbsArrayController(1, 20));
        pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Div_Amt = parameters.newFieldInRecord("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Div_Amt", 
            "#S-CREF-ANN-SUB-TOTAL-ACCUM.#S-CREF-ANN-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Dollars = parameters.newFieldInRecord("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Dollars", 
            "#S-CREF-ANN-SUB-TOTAL-ACCUM.#S-CREF-ANN-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Div_Amt = parameters.newFieldInRecord("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Div_Amt", 
            "#S-CREF-ANN-SUB-TOTAL-ACCUM.#S-CREF-ANN-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Gtd_Amt", 
            "#S-CREF-ANN-SUB-TOTAL-ACCUM.#S-CREF-ANN-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Ivc_Amt = parameters.newFieldInRecord("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Ivc_Amt", 
            "#S-CREF-ANN-SUB-TOTAL-ACCUM.#S-CREF-ANN-SUB-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Gtd_Amt = parameters.newFieldInRecord("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Gtd_Amt", 
            "#S-CREF-ANN-SUB-TOTAL-ACCUM.#S-CREF-ANN-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Pay_Cnt = parameters.newFieldInRecord("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Pay_Cnt", 
            "#S-CREF-ANN-SUB-TOTAL-ACCUM.#S-CREF-ANN-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Units = parameters.newFieldInRecord("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Units", 
            "#S-CREF-ANN-SUB-TOTAL-ACCUM.#S-CREF-ANN-SUB-UNITS", FieldType.PACKED_DECIMAL, 11, 4);
        pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Div_Amt = parameters.newFieldInRecord("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Div_Amt", 
            "#S-CREF-MTH-SUB-TOTAL-ACCUM.#S-CREF-MTH-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Dollars = parameters.newFieldInRecord("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Dollars", 
            "#S-CREF-MTH-SUB-TOTAL-ACCUM.#S-CREF-MTH-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Div_Amt = parameters.newFieldInRecord("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Div_Amt", 
            "#S-CREF-MTH-SUB-TOTAL-ACCUM.#S-CREF-MTH-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Gtd_Amt", 
            "#S-CREF-MTH-SUB-TOTAL-ACCUM.#S-CREF-MTH-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Ivc_Amt = parameters.newFieldInRecord("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Ivc_Amt", 
            "#S-CREF-MTH-SUB-TOTAL-ACCUM.#S-CREF-MTH-SUB-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Gtd_Amt = parameters.newFieldInRecord("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Gtd_Amt", 
            "#S-CREF-MTH-SUB-TOTAL-ACCUM.#S-CREF-MTH-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Pay_Cnt = parameters.newFieldInRecord("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Pay_Cnt", 
            "#S-CREF-MTH-SUB-TOTAL-ACCUM.#S-CREF-MTH-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Units = parameters.newFieldInRecord("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Units", 
            "#S-CREF-MTH-SUB-TOTAL-ACCUM.#S-CREF-MTH-SUB-UNITS", FieldType.PACKED_DECIMAL, 11, 4);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Div_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Div_Amt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-GRD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Dollars = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Dollars", "#S-TIAA-MINOR-ACCUM.#S-TIAA-GRD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Div_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Div_Amt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-GRD-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Gtd_Amt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-GRD-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Ivc_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Ivc_Amt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-GRD-FIN-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 10, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Gtd_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Gtd_Amt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-GRD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Pay_Cnt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Pay_Cnt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-GRD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Units = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Units", "#S-TIAA-MINOR-ACCUM.#S-TIAA-GRD-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Div_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Div_Amt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-IPRO-STD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Dollars = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Dollars", "#S-TIAA-MINOR-ACCUM.#S-TIAA-IPRO-STD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Div_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Div_Amt", 
            "#S-TIAA-MINOR-ACCUM.#S-TIAA-IPRO-STD-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Gtd_Amt", 
            "#S-TIAA-MINOR-ACCUM.#S-TIAA-IPRO-STD-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Ivc_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Ivc_Amt", 
            "#S-TIAA-MINOR-ACCUM.#S-TIAA-IPRO-STD-FIN-IVC-AMT", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Gtd_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Gtd_Amt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-IPRO-STD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Pay_Cnt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Pay_Cnt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-IPRO-STD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Units = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Units", "#S-TIAA-MINOR-ACCUM.#S-TIAA-IPRO-STD-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Div_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Div_Amt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-STD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Dollars = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Dollars", "#S-TIAA-MINOR-ACCUM.#S-TIAA-STD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Div_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Div_Amt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-STD-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Gtd_Amt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-STD-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Ivc_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Ivc_Amt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-STD-FIN-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 10, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Gtd_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Gtd_Amt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-STD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Pay_Cnt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Pay_Cnt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-STD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Units = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Units", "#S-TIAA-MINOR-ACCUM.#S-TIAA-STD-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Div_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Div_Amt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-TPA-STD-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Dollars = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Dollars", "#S-TIAA-MINOR-ACCUM.#S-TIAA-TPA-STD-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Div_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Div_Amt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-TPA-STD-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Gtd_Amt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-TPA-STD-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Ivc_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Ivc_Amt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-TPA-STD-FIN-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 10, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Gtd_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Gtd_Amt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-TPA-STD-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Pay_Cnt = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Pay_Cnt", "#S-TIAA-MINOR-ACCUM.#S-TIAA-TPA-STD-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Units = parameters.newFieldInRecord("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Units", "#S-TIAA-MINOR-ACCUM.#S-TIAA-TPA-STD-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4);
        pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Divid_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Divid_Amt", "#S-TIAA-SUB-ACCUM.#S-TIAA-SUB-DIVID-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Dollars = parameters.newFieldInRecord("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Dollars", "#S-TIAA-SUB-ACCUM.#S-TIAA-SUB-DOLLARS", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Div_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Div_Amt", "#S-TIAA-SUB-ACCUM.#S-TIAA-SUB-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Gtd_Amt", "#S-TIAA-SUB-ACCUM.#S-TIAA-SUB-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Ivc_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Ivc_Amt", "#S-TIAA-SUB-ACCUM.#S-TIAA-SUB-FIN-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 10, 2);
        pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Gtd_Amt = parameters.newFieldInRecord("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Gtd_Amt", "#S-TIAA-SUB-ACCUM.#S-TIAA-SUB-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 13, 2);
        pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Pay_Cnt = parameters.newFieldInRecord("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Pay_Cnt", "#S-TIAA-SUB-ACCUM.#S-TIAA-SUB-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Units = parameters.newFieldInRecord("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Units", "#S-TIAA-SUB-ACCUM.#S-TIAA-SUB-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4);
        pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Div_Amt = parameters.newFieldInRecord("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Div_Amt", "#S-TOTAL-TIAA-CREF-ACCUM.#S-TOTAL-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Dollars = parameters.newFieldInRecord("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Dollars", "#S-TOTAL-TIAA-CREF-ACCUM.#S-TOTAL-DOLLARS", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Div_Amt = parameters.newFieldInRecord("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Div_Amt", "#S-TOTAL-TIAA-CREF-ACCUM.#S-TOTAL-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Gtd_Amt", "#S-TOTAL-TIAA-CREF-ACCUM.#S-TOTAL-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Ivc_Amt = parameters.newFieldInRecord("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Ivc_Amt", "#S-TOTAL-TIAA-CREF-ACCUM.#S-TOTAL-FIN-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Gtd_Amt = parameters.newFieldInRecord("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Gtd_Amt", "#S-TOTAL-TIAA-CREF-ACCUM.#S-TOTAL-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Pay_Cnt = parameters.newFieldInRecord("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Pay_Cnt", "#S-TOTAL-TIAA-CREF-ACCUM.#S-TOTAL-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 10);
        pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Units = parameters.newFieldInRecord("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Units", "#S-TOTAL-TIAA-CREF-ACCUM.#S-TOTAL-UNITS", 
            FieldType.PACKED_DECIMAL, 14, 4);
        parameters.reset();
    }

    public Adsm808() throws Exception
    {
        super("Adsm808");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm808", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm808"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "SEMI-ANNUAL MODE", "BLUE", 1, 1, 16);
            uiForm.setUiLabel("label_2", "PAYMENT DUE DATE", "BLUE", 2, 1, 16);
            uiForm.setUiControl("pnd_Payment_Due_Date", pnd_Payment_Due_Date, true, 3, 3, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "NEW", "BLUE", 3, 28, 3);
            uiForm.setUiLabel("label_4", "NEW", "BLUE", 3, 45, 3);
            uiForm.setUiLabel("label_5", "NEW", "BLUE", 3, 61, 3);
            uiForm.setUiLabel("label_6", "NEW", "BLUE", 3, 76, 3);
            uiForm.setUiLabel("label_7", "NEW", "BLUE", 3, 95, 3);
            uiForm.setUiLabel("label_8", "NEW", "BLUE", 3, 111, 3);
            uiForm.setUiLabel("label_9", "NEW", "BLUE", 3, 126, 3);
            uiForm.setUiLabel("label_10", "PAYMENT", "BLUE", 4, 14, 7);
            uiForm.setUiLabel("label_11", "CREF", "BLUE", 4, 28, 4);
            uiForm.setUiLabel("label_12", "CREF", "BLUE", 4, 45, 4);
            uiForm.setUiLabel("label_13", "TIAA", "BLUE", 4, 61, 4);
            uiForm.setUiLabel("label_14", "TIAA", "BLUE", 4, 76, 4);
            uiForm.setUiLabel("label_15", "FINAL", "BLUE", 4, 94, 5);
            uiForm.setUiLabel("label_16", "FINAL", "BLUE", 4, 110, 5);
            uiForm.setUiLabel("label_17", "IVC", "BLUE", 4, 126, 3);
            uiForm.setUiLabel("label_18", "Prod", "BLUE", 5, 1, 4);
            uiForm.setUiLabel("label_19", "COUNTS", "BLUE", 5, 14, 6);
            uiForm.setUiLabel("label_20", "UNITS", "BLUE", 5, 27, 5);
            uiForm.setUiLabel("label_21", "DOLLARS", "BLUE", 5, 44, 7);
            uiForm.setUiLabel("label_22", "GTD/AMT", "BLUE", 5, 61, 7);
            uiForm.setUiLabel("label_23", "DIVID/AMT", "BLUE", 5, 75, 9);
            uiForm.setUiLabel("label_24", "GTD/AMT", "BLUE", 5, 94, 7);
            uiForm.setUiLabel("label_25", "DIVID/AMT", "BLUE", 5, 109, 9);
            uiForm.setUiLabel("label_26", "AMOUNT", "BLUE", 5, 125, 6);
            uiForm.setUiLabel("label_27", "TIAA STD", "BLUE", 6, 1, 8);
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Pay_Cnt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Pay_Cnt, true, 6, 10, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Units", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Units, true, 6, 20, 12, "WHITE", 
                "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Dollars", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Dollars, true, 6, 36, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Gtd_Amt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Gtd_Amt, true, 6, 53, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Div_Amt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Div_Amt, true, 6, 70, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Gtd_Amt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Gtd_Amt, true, 6, 87, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Div_Amt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Div_Amt, true, 6, 104, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Ivc_Amt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Std_Fin_Ivc_Amt, true, 6, 119, 
                12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_28", "TIAA GRD", "BLUE", 7, 1, 8);
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Pay_Cnt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Pay_Cnt, true, 7, 10, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Units", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Units, true, 7, 20, 12, "WHITE", 
                "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Dollars", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Dollars, true, 7, 36, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Gtd_Amt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Gtd_Amt, true, 7, 53, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Div_Amt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Div_Amt, true, 7, 70, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Gtd_Amt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Gtd_Amt, true, 7, 87, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Div_Amt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Div_Amt, true, 7, 104, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Ivc_Amt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Grd_Fin_Ivc_Amt, true, 7, 119, 
                12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_29", "TIAA IPR", "BLUE", 8, 1, 8);
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Pay_Cnt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Pay_Cnt, true, 8, 10, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Units", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Units, true, 8, 20, 12, 
                "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Dollars", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Dollars, true, 8, 36, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Gtd_Amt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Gtd_Amt, true, 8, 53, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Div_Amt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Div_Amt, true, 8, 70, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Gtd_Amt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Gtd_Amt, true, 
                8, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Div_Amt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Div_Amt, true, 
                8, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Ivc_Amt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Ipro_Std_Fin_Ivc_Amt, true, 
                8, 119, 12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_30", "TOTAL", "BLUE", 9, 1, 5);
            uiForm.setUiControl("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Pay_Cnt", pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Pay_Cnt, true, 9, 10, 9, "WHITE", "Z,ZZZ,ZZ9", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Units", pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Units, true, 9, 20, 12, "WHITE", "ZZZ,ZZ9.9999", 
                true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Dollars", pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Dollars, true, 9, 34, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Gtd_Amt", pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Gtd_Amt, true, 9, 51, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Divid_Amt", pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Divid_Amt, true, 9, 68, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Gtd_Amt", pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Gtd_Amt, true, 9, 85, 16, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Div_Amt", pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Div_Amt, true, 9, 102, 16, 
                "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Ivc_Amt", pnd_S_Tiaa_Sub_Accum_Pnd_S_Tiaa_Sub_Fin_Ivc_Amt, true, 9, 119, 12, 
                "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_31", "TIAA TPA", "BLUE", 11, 1, 8);
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Pay_Cnt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Pay_Cnt, true, 11, 10, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Units", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Units, true, 11, 20, 12, 
                "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Dollars", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Dollars, true, 11, 36, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Gtd_Amt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Gtd_Amt, true, 11, 53, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Div_Amt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Div_Amt, true, 11, 70, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Gtd_Amt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Gtd_Amt, true, 
                11, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Div_Amt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Div_Amt, true, 
                11, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Ivc_Amt", pnd_S_Tiaa_Minor_Accum_Pnd_S_Tiaa_Tpa_Std_Fin_Ivc_Amt, true, 
                11, 119, 12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_32", "T", "BLUE", 13, 1, 1);
            uiForm.setUiLabel("label_33", "A MTH", "BLUE", 13, 4, 5);
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt_12pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt.getValue(12+0), true, 
                13, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units_12pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units.getValue(12+0), true, 13, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars_12pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars.getValue(12+0), true, 
                13, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt_12pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt.getValue(12+0), true, 
                13, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt_12pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt.getValue(12+0), true, 
                13, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt_12pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt.getValue(12+0), 
                true, 13, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt_12pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt.getValue(12+0), 
                true, 13, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Fin_Ivc_Amount_Pnd_S_C_Mth_Fin_Ivc_Amt", pnd_S_C_Mth_Fin_Ivc_Amount_Pnd_S_C_Mth_Fin_Ivc_Amt, true, 13, 119, 
                12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_34", "T", "BLUE", 14, 1, 1);
            uiForm.setUiLabel("label_35", "A ANN", "BLUE", 14, 4, 5);
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt_12pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt.getValue(12+0), true, 
                14, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units_12pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units.getValue(12+0), true, 14, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars_12pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars.getValue(12+0), true, 
                14, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt_12pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt.getValue(12+0), true, 
                14, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt_12pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt.getValue(12+0), 
                true, 14, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt_12pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt.getValue(12+0), 
                true, 14, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt_12pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt.getValue(12+0), 
                true, 14, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Fin_Ivc_Amount_Pnd_S_C_Ann_Fin_Ivc_Amt", pnd_S_C_Ann_Fin_Ivc_Amount_Pnd_S_C_Ann_Fin_Ivc_Amt, true, 14, 119, 
                12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_36", "T", "BLUE", 15, 1, 1);
            uiForm.setUiLabel("label_37", "R MTH", "BLUE", 15, 4, 5);
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt_2pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt.getValue(2+0), true, 
                15, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units_2pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units.getValue(2+0), true, 15, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars_2pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars.getValue(2+0), true, 
                15, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt_2pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt.getValue(2+0), true, 
                15, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt_2pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt.getValue(2+0), true, 
                15, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt_2pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt.getValue(2+0), 
                true, 15, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt_2pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt.getValue(2+0), 
                true, 15, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Fin_Ivc_Amount_Pnd_S_C_Mth_Fin_Ivc_Amt2", pnd_S_C_Mth_Fin_Ivc_Amount_Pnd_S_C_Mth_Fin_Ivc_Amt, true, 15, 119, 
                12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_38", "T", "BLUE", 16, 1, 1);
            uiForm.setUiLabel("label_39", "R ANN", "BLUE", 16, 4, 5);
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt_2pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt.getValue(2+0), true, 
                16, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units_2pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units.getValue(2+0), true, 16, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars_2pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars.getValue(2+0), true, 
                16, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt_2pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt.getValue(2+0), true, 
                16, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt_2pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt.getValue(2+0), true, 
                16, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt_2pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt.getValue(2+0), 
                true, 16, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt_2pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt.getValue(2+0), 
                true, 16, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Fin_Ivc_Amount_Pnd_S_C_Ann_Fin_Ivc_Amt2", pnd_S_C_Ann_Fin_Ivc_Amount_Pnd_S_C_Ann_Fin_Ivc_Amt, true, 16, 119, 
                12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_40", "STK", "BLUE", 18, 1, 3);
            uiForm.setUiLabel("label_41", "MTH", "BLUE", 18, 6, 3);
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt_3pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt.getValue(3+0), true, 
                18, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units_3pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units.getValue(3+0), true, 18, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars_3pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars.getValue(3+0), true, 
                18, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt_3pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt.getValue(3+0), true, 
                18, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt_3pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt.getValue(3+0), true, 
                18, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt_3pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt.getValue(3+0), 
                true, 18, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt_3pls0", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt.getValue(3+0), 
                true, 18, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_42", "MMA", "BLUE", 19, 1, 3);
            uiForm.setUiLabel("label_43", "MTH", "BLUE", 19, 6, 3);
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt_3pls1", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt.getValue(3+1), true, 
                19, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units_3pls1", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units.getValue(3+1), true, 19, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars_3pls1", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars.getValue(3+1), true, 
                19, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt_3pls1", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt.getValue(3+1), true, 
                19, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt_3pls1", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt.getValue(3+1), true, 
                19, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt_3pls1", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt.getValue(3+1), 
                true, 19, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt_3pls1", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt.getValue(3+1), 
                true, 19, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_44", "SOCL MTH", "BLUE", 20, 1, 8);
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt_3pls2", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt.getValue(3+2), true, 
                20, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units_3pls2", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units.getValue(3+2), true, 20, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars_3pls2", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars.getValue(3+2), true, 
                20, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt_3pls2", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt.getValue(3+2), true, 
                20, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt_3pls2", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt.getValue(3+2), true, 
                20, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt_3pls2", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt.getValue(3+2), 
                true, 20, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt_3pls2", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt.getValue(3+2), 
                true, 20, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_45", "BOND MTH", "BLUE", 21, 1, 8);
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt_3pls3", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt.getValue(3+3), true, 
                21, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units_3pls3", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units.getValue(3+3), true, 21, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars_3pls3", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars.getValue(3+3), true, 
                21, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt_3pls3", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt.getValue(3+3), true, 
                21, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt_3pls3", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt.getValue(3+3), true, 
                21, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt_3pls3", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt.getValue(3+3), 
                true, 21, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt_3pls3", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt.getValue(3+3), 
                true, 21, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_46", "GLOB MTH", "BLUE", 22, 1, 8);
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt_3pls4", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt.getValue(3+4), true, 
                22, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units_3pls4", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units.getValue(3+4), true, 22, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars_3pls4", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars.getValue(3+4), true, 
                22, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt_3pls4", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt.getValue(3+4), true, 
                22, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt_3pls4", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt.getValue(3+4), true, 
                22, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt_3pls4", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt.getValue(3+4), 
                true, 22, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt_3pls4", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt.getValue(3+4), 
                true, 22, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_47", "GROW MTH", "BLUE", 23, 1, 8);
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt_3pls5", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt.getValue(3+5), true, 
                23, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units_3pls5", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units.getValue(3+5), true, 23, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars_3pls5", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars.getValue(3+5), true, 
                23, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt_3pls5", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt.getValue(3+5), true, 
                23, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt_3pls5", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt.getValue(3+5), true, 
                23, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt_3pls5", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt.getValue(3+5), 
                true, 23, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt_3pls5", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt.getValue(3+5), 
                true, 23, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_48", "EQTY MTH", "BLUE", 24, 1, 8);
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt_3pls6", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt.getValue(3+6), true, 
                24, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units_3pls6", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units.getValue(3+6), true, 24, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars_3pls6", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars.getValue(3+6), true, 
                24, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt_3pls6", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt.getValue(3+6), true, 
                24, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt_3pls6", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt.getValue(3+6), true, 
                24, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt_3pls6", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt.getValue(3+6), 
                true, 24, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt_3pls6", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt.getValue(3+6), 
                true, 24, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_49", "ILB", "BLUE", 25, 1, 3);
            uiForm.setUiLabel("label_50", "MTH", "BLUE", 25, 6, 3);
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt_3pls7", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Pay_Cnt.getValue(3+7), true, 
                25, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units_3pls7", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Units.getValue(3+7), true, 25, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars_3pls7", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Dollars.getValue(3+7), true, 
                25, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt_3pls7", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Gtd_Amt.getValue(3+7), true, 
                25, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt_3pls7", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Div_Amt.getValue(3+7), true, 
                25, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt_3pls7", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Gtd_Amt.getValue(3+7), 
                true, 25, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt_3pls7", pnd_S_C_Mth_Minor_Accum_Pnd_S_C_Mth_Fin_Div_Amt.getValue(3+7), 
                true, 25, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_51", "TOT", "BLUE", 26, 1, 3);
            uiForm.setUiLabel("label_52", "MTH", "BLUE", 26, 6, 3);
            uiForm.setUiControl("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Pay_Cnt", pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Pay_Cnt, 
                true, 26, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Units", pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Units, true, 
                26, 20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Dollars", pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Dollars, 
                true, 26, 34, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Gtd_Amt", pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Gtd_Amt, 
                true, 26, 51, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Div_Amt", pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Div_Amt, 
                true, 26, 68, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Gtd_Amt", pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Gtd_Amt, 
                true, 26, 85, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Div_Amt", pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Div_Amt, 
                true, 26, 102, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Ivc_Amt", pnd_S_Cref_Mth_Sub_Total_Accum_Pnd_S_Cref_Mth_Sub_Fin_Ivc_Amt, 
                true, 26, 119, 12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_53", "STK", "BLUE", 28, 1, 3);
            uiForm.setUiLabel("label_54", "ANN", "BLUE", 28, 6, 3);
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt_3pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt.getValue(3+0), true, 
                28, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units_3pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units.getValue(3+0), true, 28, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars_3pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars.getValue(3+0), true, 
                28, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt_3pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt.getValue(3+0), true, 
                28, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt_3pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt.getValue(3+0), true, 
                28, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt_3pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt.getValue(3+0), 
                true, 28, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt_3pls0", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt.getValue(3+0), 
                true, 28, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_55", "MMA", "BLUE", 29, 1, 3);
            uiForm.setUiLabel("label_56", "ANN", "BLUE", 29, 6, 3);
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt_3pls1", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt.getValue(3+1), true, 
                29, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units_3pls1", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units.getValue(3+1), true, 29, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars_3pls1", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars.getValue(3+1), true, 
                29, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt_3pls1", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt.getValue(3+1), true, 
                29, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt_3pls1", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt.getValue(3+1), true, 
                29, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt_3pls1", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt.getValue(3+1), 
                true, 29, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt_3pls1", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt.getValue(3+1), 
                true, 29, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_57", "SOC", "BLUE", 30, 1, 3);
            uiForm.setUiLabel("label_58", "ANN", "BLUE", 30, 6, 3);
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt_3pls2", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt.getValue(3+2), true, 
                30, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units_3pls2", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units.getValue(3+2), true, 30, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars_3pls2", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars.getValue(3+2), true, 
                30, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt_3pls2", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt.getValue(3+2), true, 
                30, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt_3pls2", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt.getValue(3+2), true, 
                30, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt_3pls2", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt.getValue(3+2), 
                true, 30, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt_3pls2", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt.getValue(3+2), 
                true, 30, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_59", "BOND ANN", "BLUE", 31, 1, 8);
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt_3pls3", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt.getValue(3+3), true, 
                31, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units_3pls3", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units.getValue(3+3), true, 31, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars_3pls3", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars.getValue(3+3), true, 
                31, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt_3pls3", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt.getValue(3+3), true, 
                31, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt_3pls3", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt.getValue(3+3), true, 
                31, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt_3pls3", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt.getValue(3+3), 
                true, 31, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt_3pls3", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt.getValue(3+3), 
                true, 31, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_60", "GLOB ANN", "BLUE", 32, 1, 8);
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt_3pls4", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt.getValue(3+4), true, 
                32, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units_3pls4", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units.getValue(3+4), true, 32, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars_3pls4", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars.getValue(3+4), true, 
                32, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt_3pls4", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt.getValue(3+4), true, 
                32, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt_3pls4", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt.getValue(3+4), true, 
                32, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt_3pls4", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt.getValue(3+4), 
                true, 32, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt_3pls4", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt.getValue(3+4), 
                true, 32, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_61", "GROW ANN", "BLUE", 33, 1, 8);
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt_3pls5", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt.getValue(3+5), true, 
                33, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units_3pls5", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units.getValue(3+5), true, 33, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars_3pls5", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars.getValue(3+5), true, 
                33, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt_3pls5", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt.getValue(3+5), true, 
                33, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt_3pls5", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt.getValue(3+5), true, 
                33, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt_3pls5", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt.getValue(3+5), 
                true, 33, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt_3pls5", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt.getValue(3+5), 
                true, 33, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_62", "EQTY ANN", "BLUE", 34, 1, 8);
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt_3pls6", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt.getValue(3+6), true, 
                34, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units_3pls6", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units.getValue(3+6), true, 34, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars_3pls6", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars.getValue(3+6), true, 
                34, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt_3pls6", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt.getValue(3+6), true, 
                34, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt_3pls6", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt.getValue(3+6), true, 
                34, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt_3pls6", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt.getValue(3+6), 
                true, 34, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt_3pls6", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt.getValue(3+6), 
                true, 34, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_63", "ILB", "BLUE", 35, 1, 3);
            uiForm.setUiLabel("label_64", "ANN", "BLUE", 35, 6, 3);
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt_3pls7", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Pay_Cnt.getValue(3+7), true, 
                35, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units_3pls7", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Units.getValue(3+7), true, 35, 
                20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars_3pls7", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Dollars.getValue(3+7), true, 
                35, 36, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt_3pls7", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Gtd_Amt.getValue(3+7), true, 
                35, 53, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt_3pls7", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Divid_Amt.getValue(3+7), true, 
                35, 70, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt_3pls7", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Gtd_Amt.getValue(3+7), 
                true, 35, 87, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt_3pls7", pnd_S_C_Ann_Minor_Accum_Pnd_S_C_Ann_Fin_Div_Amt.getValue(3+7), 
                true, 35, 104, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_65", "TOT", "BLUE", 36, 1, 3);
            uiForm.setUiLabel("label_66", "ANN", "BLUE", 36, 6, 3);
            uiForm.setUiControl("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Pay_Cnt", pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Pay_Cnt, 
                true, 36, 10, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Units", pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Units, true, 
                36, 20, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Dollars", pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Dollars, 
                true, 36, 34, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Gtd_Amt", pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Gtd_Amt, 
                true, 36, 51, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Div_Amt", pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Div_Amt, 
                true, 36, 68, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Gtd_Amt", pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Gtd_Amt, 
                true, 36, 85, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Div_Amt", pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Div_Amt, 
                true, 36, 102, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Ivc_Amt", pnd_S_Cref_Ann_Sub_Total_Accum_Pnd_S_Cref_Ann_Sub_Fin_Ivc_Amt, 
                true, 36, 119, 12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Dollars", pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Dollars, true, 38, 32, 18, 
                "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Div_Amt", pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Div_Amt, true, 38, 66, 18, 
                "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Div_Amt", pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Div_Amt, true, 38, 
                100, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_67", "TOTL ALL", "BLUE", 39, 1, 8);
            uiForm.setUiControl("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Units", pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Units, true, 39, 16, 16, "WHITE", 
                "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Gtd_Amt", pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Gtd_Amt, true, 39, 49, 18, 
                "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Gtd_Amt", pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Gtd_Amt, true, 39, 
                83, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Ivc_Amt", pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Fin_Ivc_Amt, true, 39, 
                113, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Pay_Cnt", pnd_S_Total_Tiaa_Cref_Accum_Pnd_S_Total_Pay_Cnt, true, 40, 6, 13, "WHITE", 
                "Z,ZZZ,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
