/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:53 PM
**        *   FROM NATURAL MAP   :  Adsm665
************************************************************
**        * FILE NAME               : Adsm665.java
**        * CLASS NAME              : Adsm665
**        * INSTANCE NAME           : Adsm665
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #NAP-PYMNT-MODE-ALL #PARTCIPANT-NAME-1 #RQST-ID.#UNIQUE-ID                                                               *     #TIAA-TPA-IO-665
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm665 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Nap_Pymnt_Mode_All;
    private DbsField pnd_Partcipant_Name_1;
    private DbsField pnd_Rqst_Id_Pnd_Unique_Id;
    private DbsField pnd_Tiaa_Tpa_Io_665;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Nap_Pymnt_Mode_All = parameters.newFieldInRecord("pnd_Nap_Pymnt_Mode_All", "#NAP-PYMNT-MODE-ALL", FieldType.STRING, 20);
        pnd_Partcipant_Name_1 = parameters.newFieldInRecord("pnd_Partcipant_Name_1", "#PARTCIPANT-NAME-1", FieldType.STRING, 30);
        pnd_Rqst_Id_Pnd_Unique_Id = parameters.newFieldInRecord("pnd_Rqst_Id_Pnd_Unique_Id", "#RQST-ID.#UNIQUE-ID", FieldType.NUMERIC, 12);
        pnd_Tiaa_Tpa_Io_665 = parameters.newFieldInRecord("pnd_Tiaa_Tpa_Io_665", "#TIAA-TPA-IO-665", FieldType.STRING, 4);
        parameters.reset();
    }

    public Adsm665() throws Exception
    {
        super("Adsm665");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=080 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm665", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm665"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Rqst_Id_Pnd_Unique_Id", pnd_Rqst_Id_Pnd_Unique_Id, true, 1, 2, 12, "WHITE", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Partcipant_Name_1", pnd_Partcipant_Name_1, true, 2, 2, 30, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_1", "IA PAYMENT/RENEWAL CALCULATION INFORMATION", "BLUE", 5, 1, 42);
            uiForm.setUiLabel("label_2", "MODE:", "BLUE", 7, 36, 5);
            uiForm.setUiControl("pnd_Nap_Pymnt_Mode_All", pnd_Nap_Pymnt_Mode_All, true, 7, 42, 20, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_3", "STANDARD METHOD-", "BLUE", 9, 1, 16);
            uiForm.setUiControl("pnd_Tiaa_Tpa_Io_665", pnd_Tiaa_Tpa_Io_665, true, 9, 19, 4, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "_______________", "BLUE", 10, 1, 15);
            uiForm.setUiLabel("label_5", "IA PAYMENTS", "BLUE", 12, 24, 11);
            uiForm.setUiLabel("label_6", "RENEWAL PAYMENTS", "BLUE", 12, 58, 16);
            uiForm.setUiLabel("label_7", "IA", "BLUE", 13, 2, 2);
            uiForm.setUiLabel("label_8", "STANDARD", "BLUE", 13, 16, 8);
            uiForm.setUiLabel("label_9", "STANDARD", "BLUE", 13, 34, 8);
            uiForm.setUiLabel("label_10", "STANDARD", "BLUE", 13, 52, 8);
            uiForm.setUiLabel("label_11", "STANDARD", "BLUE", 13, 70, 8);
            uiForm.setUiLabel("label_12", "RATE", "BLUE", 14, 1, 4);
            uiForm.setUiLabel("label_13", "GUARANTEED", "BLUE", 14, 16, 10);
            uiForm.setUiLabel("label_14", "DIVIDEND", "BLUE", 14, 34, 8);
            uiForm.setUiLabel("label_15", "GUARANTEED", "BLUE", 14, 52, 10);
            uiForm.setUiLabel("label_16", "DIVIDEND", "BLUE", 14, 70, 8);
            uiForm.setUiLabel("label_17", "____", "BLUE", 15, 1, 4);
            uiForm.setUiLabel("label_18", "__________", "BLUE", 15, 16, 10);
            uiForm.setUiLabel("label_19", "__________", "BLUE", 15, 34, 10);
            uiForm.setUiLabel("label_20", "___________", "BLUE", 15, 52, 11);
            uiForm.setUiLabel("label_21", "__________", "BLUE", 15, 69, 10);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
