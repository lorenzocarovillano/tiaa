/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:59 PM
**        *   FROM NATURAL MAP   :  Adsm773
************************************************************
**        * FILE NAME               : Adsm773.java
**        * CLASS NAME              : Adsm773
**        * INSTANCE NAME           : Adsm773
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #TIAA-IA-GRD-DIVIDEND-FIN-RENEW #TIAA-IA-GRD-DIVIDEND-FINAL                                                              *     #TIAA-IA-GRD-GURANTEED-FIN-RENEW 
    #TIAA-IA-GRD-GURANTEED-FINAL
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm773 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Tiaa_Ia_Grd_Dividend_Fin_Renew;
    private DbsField pnd_Tiaa_Ia_Grd_Dividend_Final;
    private DbsField pnd_Tiaa_Ia_Grd_Guranteed_Fin_Renew;
    private DbsField pnd_Tiaa_Ia_Grd_Guranteed_Final;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Tiaa_Ia_Grd_Dividend_Fin_Renew = parameters.newFieldInRecord("pnd_Tiaa_Ia_Grd_Dividend_Fin_Renew", "#TIAA-IA-GRD-DIVIDEND-FIN-RENEW", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Ia_Grd_Dividend_Final = parameters.newFieldInRecord("pnd_Tiaa_Ia_Grd_Dividend_Final", "#TIAA-IA-GRD-DIVIDEND-FINAL", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Ia_Grd_Guranteed_Fin_Renew = parameters.newFieldInRecord("pnd_Tiaa_Ia_Grd_Guranteed_Fin_Renew", "#TIAA-IA-GRD-GURANTEED-FIN-RENEW", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Tiaa_Ia_Grd_Guranteed_Final = parameters.newFieldInRecord("pnd_Tiaa_Ia_Grd_Guranteed_Final", "#TIAA-IA-GRD-GURANTEED-FINAL", FieldType.PACKED_DECIMAL, 
            13, 2);
        parameters.reset();
    }

    public Adsm773() throws Exception
    {
        super("Adsm773");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=080 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm773", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm773"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "-----------------------------------------------------------------------------", "", 1, 1, 77);
            uiForm.setUiLabel("label_2", "TOTALS", "BLUE", 2, 1, 6);
            uiForm.setUiControl("pnd_Tiaa_Ia_Grd_Guranteed_Final", pnd_Tiaa_Ia_Grd_Guranteed_Final, true, 2, 11, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, 
                true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Ia_Grd_Dividend_Final", pnd_Tiaa_Ia_Grd_Dividend_Final, true, 2, 28, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, 
                true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Ia_Grd_Guranteed_Fin_Renew", pnd_Tiaa_Ia_Grd_Guranteed_Fin_Renew, true, 2, 45, 16, "WHITE", "Z,ZZZ,ZZZ,ZZZ.99", 
                true, false, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Ia_Grd_Dividend_Fin_Renew", pnd_Tiaa_Ia_Grd_Dividend_Fin_Renew, true, 2, 62, 16, "WHITE", "Z,ZZZ,ZZZ,ZZZ.99", 
                true, false, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_3", "------", "BLUE", 3, 1, 6);
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
