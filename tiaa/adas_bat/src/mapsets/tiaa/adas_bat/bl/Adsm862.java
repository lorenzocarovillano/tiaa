/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:35 PM
**        *   FROM NATURAL MAP   :  Adsm862
************************************************************
**        * FILE NAME               : Adsm862.java
**        * CLASS NAME              : Adsm862
**        * INSTANCE NAME           : Adsm862
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #FINAL-PREM-IND #HEADING #NAP-BPS-UNIT #NAP-MIT-LOG-DTE-TME                                                              *     #PAGE-NUMBER-MAP
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm862 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Final_Prem_Ind;
    private DbsField pnd_Heading;
    private DbsField pnd_Nap_Bps_Unit;
    private DbsField pnd_Nap_Mit_Log_Dte_Tme;
    private DbsField pnd_Page_Number_Map;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Final_Prem_Ind = parameters.newFieldInRecord("pnd_Final_Prem_Ind", "#FINAL-PREM-IND", FieldType.STRING, 14);
        pnd_Heading = parameters.newFieldInRecord("pnd_Heading", "#HEADING", FieldType.STRING, 44);
        pnd_Nap_Bps_Unit = parameters.newFieldInRecord("pnd_Nap_Bps_Unit", "#NAP-BPS-UNIT", FieldType.STRING, 8);
        pnd_Nap_Mit_Log_Dte_Tme = parameters.newFieldInRecord("pnd_Nap_Mit_Log_Dte_Tme", "#NAP-MIT-LOG-DTE-TME", FieldType.STRING, 15);
        pnd_Page_Number_Map = parameters.newFieldInRecord("pnd_Page_Number_Map", "#PAGE-NUMBER-MAP", FieldType.NUMERIC, 8);
        parameters.reset();
    }

    public Adsm862() throws Exception
    {
        super("Adsm862");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=091 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm862", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm862"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "MIT:", "BLUE", 1, 1, 4);
            uiForm.setUiControl("pnd_Nap_Mit_Log_Dte_Tme", pnd_Nap_Mit_Log_Dte_Tme, true, 1, 6, 15, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Nap_Bps_Unit", pnd_Nap_Bps_Unit, true, 1, 22, 8, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "ANNUITIZATION OF OMNIPLUS CONTRACTS", "BLUE", 1, 31, 35);
            uiForm.setUiLabel("label_3", "Run Date:", "BLUE", 1, 68, 9);
            uiForm.setUiControl("astDATX", Global.getDATX(), true, 1, 78, 8, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "ADSP765", "BLUE", 2, 1, 7);
            uiForm.setUiLabel("label_5", "TRANSACTION HISTORY REPORT", "BLUE", 2, 35, 26);
            uiForm.setUiLabel("label_6", "Page No :", "BLUE", 2, 68, 9);
            uiForm.setUiControl("pnd_Page_Number_Map", pnd_Page_Number_Map, true, 2, 78, 8, "WHITE", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Heading", pnd_Heading, true, 3, 27, 44, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Final_Prem_Ind", pnd_Final_Prem_Ind, true, 4, 41, 14, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
