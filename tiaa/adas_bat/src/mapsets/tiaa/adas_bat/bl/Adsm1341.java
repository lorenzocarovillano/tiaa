/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:36 PM
**        *   FROM NATURAL MAP   :  Adsm1341
************************************************************
**        * FILE NAME               : Adsm1341.java
**        * CLASS NAME              : Adsm1341
**        * INSTANCE NAME           : Adsm1341
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #M-C-ANN-MINOR-ACCUM.#M-C-ANN-DIVID-AMT(*)                                                                               *     #M-C-ANN-MINOR-ACCUM.#M-C-ANN-DOLLARS(*) 
    *     #M-C-ANN-MINOR-ACCUM.#M-C-ANN-FIN-DIV-AMT(*)                                                                             *     #M-C-ANN-MINOR-ACCUM.#M-C-ANN-FIN-GTD-AMT(*) 
    *     #M-C-ANN-MINOR-ACCUM.#M-C-ANN-GTD-AMT(*)                                                                                 *     #M-C-ANN-MINOR-ACCUM.#M-C-ANN-PAY-CNT(*) 
    *     #M-C-ANN-MINOR-ACCUM.#M-C-ANN-UNITS(*)                                                                                   *     #M-C-MTH-MINOR-ACCUM.#M-C-MTH-DIV-AMT(*) 
    *     #M-C-MTH-MINOR-ACCUM.#M-C-MTH-DOLLARS(*)                                                                                 *     #M-C-MTH-MINOR-ACCUM.#M-C-MTH-FIN-DIV-AMT(*) 
    *     #M-C-MTH-MINOR-ACCUM.#M-C-MTH-FIN-GTD-AMT(*)                                                                             *     #M-C-MTH-MINOR-ACCUM.#M-C-MTH-GTD-AMT(*) 
    *     #M-C-MTH-MINOR-ACCUM.#M-C-MTH-PAY-CNT(*)                                                                                 *     #M-C-MTH-MINOR-ACCUM.#M-C-MTH-UNITS(*) 
    *     #M-CREF-ANN-SUB-TOTAL-ACCUM.#M-CREF-ANN-SUB-DIV-AMT                                                                      *     #M-CREF-ANN-SUB-TOTAL-ACCUM.#M-CREF-ANN-SUB-DOLLARS 
    *     #M-CREF-ANN-SUB-TOTAL-ACCUM.#M-CREF-ANN-SUB-FIN-DIV-AMT                                                                  *     #M-CREF-ANN-SUB-TOTAL-ACCUM.#M-CREF-ANN-SUB-FIN-GTD-AMT 
    *     #M-CREF-ANN-SUB-TOTAL-ACCUM.#M-CREF-ANN-SUB-GTD-AMT                                                                      *     #M-CREF-ANN-SUB-TOTAL-ACCUM.#M-CREF-ANN-SUB-PAY-CNT 
    *     #M-CREF-ANN-SUB-TOTAL-ACCUM.#M-CREF-ANN-SUB-UNITS                                                                        *     #M-CREF-MTH-SUB-TOTAL-ACCUM.#M-CREF-MTH-SUB-DIV-AMT 
    *     #M-CREF-MTH-SUB-TOTAL-ACCUM.#M-CREF-MTH-SUB-DOLLARS                                                                      *     #M-CREF-MTH-SUB-TOTAL-ACCUM.#M-CREF-MTH-SUB-FIN-DIV-AMT 
    *     #M-CREF-MTH-SUB-TOTAL-ACCUM.#M-CREF-MTH-SUB-FIN-GTD-AMT                                                                  *     #M-CREF-MTH-SUB-TOTAL-ACCUM.#M-CREF-MTH-SUB-GTD-AMT 
    *     #M-CREF-MTH-SUB-TOTAL-ACCUM.#M-CREF-MTH-SUB-PAY-CNT                                                                      *     #M-CREF-MTH-SUB-TOTAL-ACCUM.#M-CREF-MTH-SUB-UNITS 
    *     #M-T-CREF-DOLLARS-FILLER.#M-T-CREF-DOLLARS(*)                                                                            *     #M-T-CREF-UNITS-FILLER.#M-T-CREF-UNITS(*) 
    *     #M-TIAA-BY-DETAILS.#M-TIAA-DIV-AMT(*)                                                                                    *     #M-TIAA-BY-DETAILS.#M-TIAA-FIN-DIV-AMT(*) 
    *     #M-TIAA-BY-DETAILS.#M-TIAA-FIN-GTD-AMT(*)                                                                                *     #M-TIAA-BY-DETAILS.#M-TIAA-GTD-AMT(*) 
    *     #M-TIAA-BY-DETAILS.#M-TIAA-PAY-CNT(*)                                                                                    *     #M-TOTAL-TIAA-CREF-ACCUM.#M-TOTAL-DOLLARS 
    *     #M-TOTAL-TIAA-CREF-ACCUM.#M-TOTAL-UNITS #MODE-DESCRIPTION                                                                *     #PAYMENT-DUE-DATE
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm1341 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt;
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars;
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt;
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt;
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt;
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt;
    private DbsField pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt;
    private DbsField pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Div_Amt;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Dollars;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Div_Amt;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Gtd_Amt;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Gtd_Amt;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Pay_Cnt;
    private DbsField pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Units;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Div_Amt;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Dollars;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Div_Amt;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Gtd_Amt;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Gtd_Amt;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Pay_Cnt;
    private DbsField pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Units;
    private DbsField pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars;
    private DbsField pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units;
    private DbsField pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt;
    private DbsField pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt;
    private DbsField pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt;
    private DbsField pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt;
    private DbsField pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt;
    private DbsField pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Dollars;
    private DbsField pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Units;
    private DbsField pnd_Mode_Description;
    private DbsField pnd_Payment_Due_Date;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt = parameters.newFieldArrayInRecord("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt", "#M-C-ANN-MINOR-ACCUM.#M-C-ANN-DIVID-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars = parameters.newFieldArrayInRecord("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars", "#M-C-ANN-MINOR-ACCUM.#M-C-ANN-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt = parameters.newFieldArrayInRecord("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt", "#M-C-ANN-MINOR-ACCUM.#M-C-ANN-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt", "#M-C-ANN-MINOR-ACCUM.#M-C-ANN-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt", "#M-C-ANN-MINOR-ACCUM.#M-C-ANN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt = parameters.newFieldArrayInRecord("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt", "#M-C-ANN-MINOR-ACCUM.#M-C-ANN-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units = parameters.newFieldArrayInRecord("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units", "#M-C-ANN-MINOR-ACCUM.#M-C-ANN-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4, new DbsArrayController(1, 20));
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt = parameters.newFieldArrayInRecord("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt", "#M-C-MTH-MINOR-ACCUM.#M-C-MTH-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars = parameters.newFieldArrayInRecord("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars", "#M-C-MTH-MINOR-ACCUM.#M-C-MTH-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt = parameters.newFieldArrayInRecord("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt", "#M-C-MTH-MINOR-ACCUM.#M-C-MTH-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt", "#M-C-MTH-MINOR-ACCUM.#M-C-MTH-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt", "#M-C-MTH-MINOR-ACCUM.#M-C-MTH-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt = parameters.newFieldArrayInRecord("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt", "#M-C-MTH-MINOR-ACCUM.#M-C-MTH-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units = parameters.newFieldArrayInRecord("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units", "#M-C-MTH-MINOR-ACCUM.#M-C-MTH-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4, new DbsArrayController(1, 20));
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Div_Amt = parameters.newFieldInRecord("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Div_Amt", 
            "#M-CREF-ANN-SUB-TOTAL-ACCUM.#M-CREF-ANN-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Dollars = parameters.newFieldInRecord("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Dollars", 
            "#M-CREF-ANN-SUB-TOTAL-ACCUM.#M-CREF-ANN-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Div_Amt = parameters.newFieldInRecord("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Div_Amt", 
            "#M-CREF-ANN-SUB-TOTAL-ACCUM.#M-CREF-ANN-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Gtd_Amt", 
            "#M-CREF-ANN-SUB-TOTAL-ACCUM.#M-CREF-ANN-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Gtd_Amt = parameters.newFieldInRecord("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Gtd_Amt", 
            "#M-CREF-ANN-SUB-TOTAL-ACCUM.#M-CREF-ANN-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Pay_Cnt = parameters.newFieldInRecord("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Pay_Cnt", 
            "#M-CREF-ANN-SUB-TOTAL-ACCUM.#M-CREF-ANN-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Units = parameters.newFieldInRecord("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Units", 
            "#M-CREF-ANN-SUB-TOTAL-ACCUM.#M-CREF-ANN-SUB-UNITS", FieldType.PACKED_DECIMAL, 14, 4);
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Div_Amt = parameters.newFieldInRecord("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Div_Amt", 
            "#M-CREF-MTH-SUB-TOTAL-ACCUM.#M-CREF-MTH-SUB-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Dollars = parameters.newFieldInRecord("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Dollars", 
            "#M-CREF-MTH-SUB-TOTAL-ACCUM.#M-CREF-MTH-SUB-DOLLARS", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Div_Amt = parameters.newFieldInRecord("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Div_Amt", 
            "#M-CREF-MTH-SUB-TOTAL-ACCUM.#M-CREF-MTH-SUB-FIN-DIV-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Gtd_Amt = parameters.newFieldInRecord("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Gtd_Amt", 
            "#M-CREF-MTH-SUB-TOTAL-ACCUM.#M-CREF-MTH-SUB-FIN-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Gtd_Amt = parameters.newFieldInRecord("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Gtd_Amt", 
            "#M-CREF-MTH-SUB-TOTAL-ACCUM.#M-CREF-MTH-SUB-GTD-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Pay_Cnt = parameters.newFieldInRecord("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Pay_Cnt", 
            "#M-CREF-MTH-SUB-TOTAL-ACCUM.#M-CREF-MTH-SUB-PAY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Units = parameters.newFieldInRecord("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Units", 
            "#M-CREF-MTH-SUB-TOTAL-ACCUM.#M-CREF-MTH-SUB-UNITS", FieldType.PACKED_DECIMAL, 14, 4);
        pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars = parameters.newFieldArrayInRecord("pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars", "#M-T-CREF-DOLLARS-FILLER.#M-T-CREF-DOLLARS", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 15));
        pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units = parameters.newFieldArrayInRecord("pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units", "#M-T-CREF-UNITS-FILLER.#M-T-CREF-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 4, new DbsArrayController(1, 15));
        pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt = parameters.newFieldArrayInRecord("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt", "#M-TIAA-BY-DETAILS.#M-TIAA-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 16));
        pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt = parameters.newFieldArrayInRecord("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt", "#M-TIAA-BY-DETAILS.#M-TIAA-FIN-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 16));
        pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt", "#M-TIAA-BY-DETAILS.#M-TIAA-FIN-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 16));
        pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt = parameters.newFieldArrayInRecord("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt", "#M-TIAA-BY-DETAILS.#M-TIAA-GTD-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 16));
        pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt = parameters.newFieldArrayInRecord("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt", "#M-TIAA-BY-DETAILS.#M-TIAA-PAY-CNT", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 16));
        pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Dollars = parameters.newFieldInRecord("pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Dollars", "#M-TOTAL-TIAA-CREF-ACCUM.#M-TOTAL-DOLLARS", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Units = parameters.newFieldInRecord("pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Units", "#M-TOTAL-TIAA-CREF-ACCUM.#M-TOTAL-UNITS", 
            FieldType.PACKED_DECIMAL, 14, 4);
        pnd_Mode_Description = parameters.newFieldInRecord("pnd_Mode_Description", "#MODE-DESCRIPTION", FieldType.STRING, 20);
        pnd_Payment_Due_Date = parameters.newFieldInRecord("pnd_Payment_Due_Date", "#PAYMENT-DUE-DATE", FieldType.NUMERIC, 8);
        parameters.reset();
    }

    public Adsm1341() throws Exception
    {
        super("Adsm1341");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm1341", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm1341"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Mode_Description", pnd_Mode_Description, true, 1, 2, 20, "WHITE", true, false, null, null, "AD=D?OFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_1", "INSTALLMENT DATE", "BLUE", 2, 1, 16);
            uiForm.setUiControl("pnd_Payment_Due_Date", pnd_Payment_Due_Date, true, 3, 3, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "PAYMENT", "BLUE", 4, 25, 7);
            uiForm.setUiLabel("label_3", "CREF", "BLUE", 4, 43, 4);
            uiForm.setUiLabel("label_4", "CREF", "BLUE", 4, 59, 4);
            uiForm.setUiLabel("label_5", "TIAA", "BLUE", 4, 77, 4);
            uiForm.setUiLabel("label_6", "TIAA", "BLUE", 4, 93, 4);
            uiForm.setUiLabel("label_7", "FINAL", "BLUE", 4, 110, 5);
            uiForm.setUiLabel("label_8", "FINAL", "BLUE", 4, 125, 5);
            uiForm.setUiLabel("label_9", "Product", "BLUE", 5, 1, 7);
            uiForm.setUiLabel("label_10", "COUNTS", "BLUE", 5, 25, 6);
            uiForm.setUiLabel("label_11", "UNITS", "BLUE", 5, 43, 5);
            uiForm.setUiLabel("label_12", "DOLLARS", "BLUE", 5, 58, 7);
            uiForm.setUiLabel("label_13", "GTD/AMT", "BLUE", 5, 76, 7);
            uiForm.setUiLabel("label_14", "DIVID/AMT", "BLUE", 5, 91, 9);
            uiForm.setUiLabel("label_15", "GTD/AMT", "BLUE", 5, 109, 7);
            uiForm.setUiLabel("label_16", "DIVID/AMT", "BLUE", 5, 123, 9);
            uiForm.setUiLabel("label_17", "TIAA STD CASH", "BLUE", 6, 1, 13);
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt_1", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt.getValue(1), true, 6, 19, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units_1", pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units.getValue(1), true, 6, 33, 
                12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars_1", pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars.getValue(1), true, 
                6, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt_1", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt.getValue(1), true, 6, 66, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt_1", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt.getValue(1), true, 6, 83, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt_1", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt.getValue(1), true, 6, 100, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt_1", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt.getValue(1), true, 6, 117, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_18", "TIAA STD INT ROL", "BLUE", 7, 1, 16);
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt_2", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt.getValue(2), true, 7, 19, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units_2", pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units.getValue(2), true, 7, 33, 
                12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars_2", pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars.getValue(2), true, 
                7, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt_2", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt.getValue(2), true, 7, 66, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt_2", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt.getValue(2), true, 7, 83, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt_2", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt.getValue(2), true, 7, 100, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt_2", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt.getValue(2), true, 7, 117, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", "TOTAL STD", "BLUE", 8, 1, 9);
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt_3", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt.getValue(3), true, 8, 19, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units_3", pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units.getValue(3), true, 8, 33, 
                12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars_3", pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars.getValue(3), true, 
                8, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt_3", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt.getValue(3), true, 8, 66, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt_3", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt.getValue(3), true, 8, 83, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt_3", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt.getValue(3), true, 8, 100, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt_3", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt.getValue(3), true, 8, 117, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_20", "TIAA GRADED", "BLUE", 10, 1, 11);
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt_4", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt.getValue(4), true, 10, 19, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units_4", pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units.getValue(4), true, 10, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars_4", pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars.getValue(4), true, 
                10, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt_4", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt.getValue(4), true, 10, 66, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt_4", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt.getValue(4), true, 10, 83, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt_4", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt.getValue(4), true, 10, 
                100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt_4", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt.getValue(4), true, 10, 
                117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_21", "TIAA IPRO", "BLUE", 12, 1, 9);
            uiForm.setUiLabel("label_22", "CASH", "BLUE", 13, 6, 4);
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt_5", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt.getValue(5), true, 13, 19, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units_5", pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units.getValue(5), true, 13, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars_5", pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars.getValue(5), true, 
                13, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt_5", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt.getValue(5), true, 13, 66, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt_5", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt.getValue(5), true, 13, 83, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt_5", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt.getValue(5), true, 13, 
                100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt_5", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt.getValue(5), true, 13, 
                117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_23", "EXT ROLLOVER", "BLUE", 14, 2, 12);
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt_6", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt.getValue(6), true, 14, 19, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units_6", pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units.getValue(6), true, 14, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars_6", pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars.getValue(6), true, 
                14, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt_6", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt.getValue(6), true, 14, 66, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt_6", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt.getValue(6), true, 14, 83, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt_6", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt.getValue(6), true, 14, 
                100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt_6", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt.getValue(6), true, 14, 
                117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_24", "INT ROLLOVER", "BLUE", 15, 2, 12);
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt_7", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt.getValue(7), true, 15, 19, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units_7", pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units.getValue(7), true, 15, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars_7", pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars.getValue(7), true, 
                15, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt_7", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt.getValue(7), true, 15, 66, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt_7", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt.getValue(7), true, 15, 83, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt_7", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt.getValue(7), true, 15, 
                100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt_7", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt.getValue(7), true, 15, 
                117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_25", "TOTAL IPRO", "BLUE", 16, 1, 10);
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt_8", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt.getValue(8), true, 16, 19, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units_8", pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units.getValue(8), true, 16, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars_8", pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars.getValue(8), true, 
                16, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt_8", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt.getValue(8), true, 16, 66, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt_8", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt.getValue(8), true, 16, 83, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt_8", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt.getValue(8), true, 16, 
                100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt_8", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt.getValue(8), true, 16, 
                117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_26", "TOTAL", "BLUE", 18, 1, 5);
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt_9", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt.getValue(9), true, 18, 19, 9, "WHITE", 
                "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units_9", pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units.getValue(9), true, 18, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars_9", pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars.getValue(9), true, 
                18, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt_9", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt.getValue(9), true, 18, 66, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt_9", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt.getValue(9), true, 18, 83, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt_9", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt.getValue(9), true, 18, 
                100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt_9", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt.getValue(9), true, 18, 
                117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_27", "T.P.A", "BLUE", 20, 1, 5);
            uiForm.setUiLabel("label_28", "INTR-ROLLOVER", "BLUE", 21, 3, 13);
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt_10", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt.getValue(10), true, 21, 19, 9, 
                "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units_10", pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units.getValue(10), true, 21, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars_10", pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars.getValue(10), 
                true, 21, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt_10", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt.getValue(10), true, 21, 66, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt_10", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt.getValue(10), true, 21, 83, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt_10", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt.getValue(10), true, 21, 
                100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt_10", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt.getValue(10), true, 21, 
                117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_29", "IVC", "BLUE", 22, 3, 3);
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt_11", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt.getValue(11), true, 22, 19, 9, 
                "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units_11", pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units.getValue(11), true, 22, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars_11", pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars.getValue(11), 
                true, 22, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt_11", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt.getValue(11), true, 22, 66, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt_11", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt.getValue(11), true, 22, 83, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt_11", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt.getValue(11), true, 22, 
                100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt_11", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt.getValue(11), true, 22, 
                117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_30", "REINVESTMENT", "BLUE", 23, 3, 12);
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt_12", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt.getValue(12), true, 23, 19, 9, 
                "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units_12", pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units.getValue(12), true, 23, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars_12", pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars.getValue(12), 
                true, 23, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt_12", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt.getValue(12), true, 23, 66, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt_12", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt.getValue(12), true, 23, 83, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt_12", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt.getValue(12), true, 23, 
                100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt_12", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt.getValue(12), true, 23, 
                117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_31", "CASH", "BLUE", 24, 3, 4);
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt_13", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt.getValue(13), true, 24, 19, 9, 
                "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units_13", pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units.getValue(13), true, 24, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars_13", pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars.getValue(13), 
                true, 24, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt_13", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt.getValue(13), true, 24, 66, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt_13", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt.getValue(13), true, 24, 83, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt_13", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt.getValue(13), true, 24, 
                100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt_13", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt.getValue(13), true, 24, 
                117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_32", "EXT-TRF/RLVR", "BLUE", 25, 3, 12);
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt_14", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt.getValue(14), true, 25, 19, 9, 
                "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units_14", pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units.getValue(14), true, 25, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars_14", pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars.getValue(14), 
                true, 25, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt_14", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt.getValue(14), true, 25, 66, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt_14", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt.getValue(14), true, 25, 83, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt_14", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt.getValue(14), true, 25, 
                100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt_14", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt.getValue(14), true, 25, 
                117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_33", "TOTAL TPA", "BLUE", 26, 1, 9);
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt_15", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt.getValue(15), true, 26, 19, 9, 
                "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units_15", pnd_M_T_Cref_Units_Filler_Pnd_M_T_Cref_Units.getValue(15), true, 26, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars_15", pnd_M_T_Cref_Dollars_Filler_Pnd_M_T_Cref_Dollars.getValue(15), 
                true, 26, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt_15", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt.getValue(15), true, 26, 66, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt_15", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt.getValue(15), true, 26, 83, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt_15", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt.getValue(15), true, 26, 
                100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt_15", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt.getValue(15), true, 26, 
                117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_34", "TIAA ACCESS MTH", "BLUE", 28, 1, 15);
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt_12pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt.getValue(12+0), true, 
                28, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units_12pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units.getValue(12+0), true, 28, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars_12pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars.getValue(12+0), true, 
                28, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt_12pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt.getValue(12+0), true, 
                28, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt_12pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt.getValue(12+0), true, 
                28, 83, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt_12pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt.getValue(12+0), 
                true, 28, 100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt_12pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt.getValue(12+0), 
                true, 28, 117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_35", "TIAA ACCESS ANN", "BLUE", 29, 1, 15);
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt_12pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt.getValue(12+0), true, 
                29, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units_12pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units.getValue(12+0), true, 29, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars_12pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars.getValue(12+0), true, 
                29, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt_12pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt.getValue(12+0), true, 
                29, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt_12pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt.getValue(12+0), 
                true, 29, 83, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt_12pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt.getValue(12+0), 
                true, 29, 100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt_12pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt.getValue(12+0), 
                true, 29, 117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_36", "TIAA REA MONTHLY", "BLUE", 30, 1, 16);
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt_2pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt.getValue(2+0), true, 
                30, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units_2pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units.getValue(2+0), true, 30, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars_2pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars.getValue(2+0), true, 
                30, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt_2pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt.getValue(2+0), true, 
                30, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt_2pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt.getValue(2+0), true, 
                30, 83, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt_2pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt.getValue(2+0), 
                true, 30, 100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt_2pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt.getValue(2+0), 
                true, 30, 117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_37", "TIAA REA ANNUAL", "BLUE", 31, 1, 15);
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt_2pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt.getValue(2+0), true, 
                31, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units_2pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units.getValue(2+0), true, 31, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars_2pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars.getValue(2+0), true, 
                31, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt_2pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt.getValue(2+0), true, 
                31, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt_2pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt.getValue(2+0), true, 
                31, 83, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt_2pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt.getValue(2+0), 
                true, 31, 100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt_2pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt.getValue(2+0), 
                true, 31, 117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_38", "STOCK", "BLUE", 33, 1, 5);
            uiForm.setUiLabel("label_39", "MONTHLY", "BLUE", 33, 8, 7);
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt_3pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt.getValue(3+0), true, 
                33, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units_3pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units.getValue(3+0), true, 33, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars_3pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars.getValue(3+0), true, 
                33, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt_3pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt.getValue(3+0), true, 
                33, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt_3pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt.getValue(3+0), true, 
                33, 83, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt_3pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt.getValue(3+0), 
                true, 33, 100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt_3pls0", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt.getValue(3+0), 
                true, 33, 117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_40", "MMA", "BLUE", 34, 1, 3);
            uiForm.setUiLabel("label_41", "MONTHLY", "BLUE", 34, 8, 7);
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt_3pls1", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt.getValue(3+1), true, 
                34, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units_3pls1", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units.getValue(3+1), true, 34, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars_3pls1", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars.getValue(3+1), true, 
                34, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt_3pls1", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt.getValue(3+1), true, 
                34, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt_3pls1", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt.getValue(3+1), true, 
                34, 83, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt_3pls1", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt.getValue(3+1), 
                true, 34, 100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt_3pls1", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt.getValue(3+1), 
                true, 34, 117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_42", "SOCIAL MONTHLY", "BLUE", 35, 1, 14);
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt_3pls2", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt.getValue(3+2), true, 
                35, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units_3pls2", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units.getValue(3+2), true, 35, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars_3pls2", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars.getValue(3+2), true, 
                35, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt_3pls2", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt.getValue(3+2), true, 
                35, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt_3pls2", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt.getValue(3+2), true, 
                35, 83, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt_3pls2", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt.getValue(3+2), 
                true, 35, 100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt_3pls2", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt.getValue(3+2), 
                true, 35, 117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_43", "BOND", "BLUE", 36, 1, 4);
            uiForm.setUiLabel("label_44", "MONTHLY", "BLUE", 36, 8, 7);
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt_3pls3", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt.getValue(3+3), true, 
                36, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units_3pls3", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units.getValue(3+3), true, 36, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars_3pls3", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars.getValue(3+3), true, 
                36, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt_3pls3", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt.getValue(3+3), true, 
                36, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt_3pls3", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt.getValue(3+3), true, 
                36, 83, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt_3pls3", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt.getValue(3+3), 
                true, 36, 100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt_3pls3", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt.getValue(3+3), 
                true, 36, 117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_45", "GLOBAL MONTHLY", "BLUE", 37, 1, 14);
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt_3pls4", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt.getValue(3+4), true, 
                37, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units_3pls4", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units.getValue(3+4), true, 37, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars_3pls4", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars.getValue(3+4), true, 
                37, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt_3pls4", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt.getValue(3+4), true, 
                37, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt_3pls4", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt.getValue(3+4), true, 
                37, 83, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt_3pls4", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt.getValue(3+4), 
                true, 37, 100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt_3pls4", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt.getValue(3+4), 
                true, 37, 117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_46", "GROWTH MONTHLY", "BLUE", 38, 1, 14);
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt_3pls5", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt.getValue(3+5), true, 
                38, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units_3pls5", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units.getValue(3+5), true, 38, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars_3pls5", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars.getValue(3+5), true, 
                38, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt_3pls5", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt.getValue(3+5), true, 
                38, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt_3pls5", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt.getValue(3+5), true, 
                38, 83, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt_3pls5", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt.getValue(3+5), 
                true, 38, 100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt_3pls5", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt.getValue(3+5), 
                true, 38, 117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_47", "EQUITY MONTHLY", "BLUE", 39, 1, 14);
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt_3pls6", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt.getValue(3+6), true, 
                39, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units_3pls6", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units.getValue(3+6), true, 39, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars_3pls6", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars.getValue(3+6), true, 
                39, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt_3pls6", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt.getValue(3+6), true, 
                39, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt_3pls6", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt.getValue(3+6), true, 
                39, 83, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt_3pls6", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt.getValue(3+6), 
                true, 39, 100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt_3pls6", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt.getValue(3+6), 
                true, 39, 117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_48", "I L B", "BLUE", 40, 1, 5);
            uiForm.setUiLabel("label_49", "MONTHLY", "BLUE", 40, 8, 7);
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt_3pls7", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Pay_Cnt.getValue(3+7), true, 
                40, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units_3pls7", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Units.getValue(3+7), true, 40, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars_3pls7", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Dollars.getValue(3+7), true, 
                40, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt_3pls7", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Gtd_Amt.getValue(3+7), true, 
                40, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt_3pls7", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Div_Amt.getValue(3+7), true, 
                40, 83, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt_3pls7", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Gtd_Amt.getValue(3+7), 
                true, 40, 100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt_3pls7", pnd_M_C_Mth_Minor_Accum_Pnd_M_C_Mth_Fin_Div_Amt.getValue(3+7), 
                true, 40, 117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_50", "TOTAL", "BLUE", 41, 1, 5);
            uiForm.setUiLabel("label_51", "MONTHLY", "BLUE", 41, 8, 7);
            uiForm.setUiControl("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Pay_Cnt", pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Pay_Cnt, 
                true, 41, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Units", pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Units, true, 
                41, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Dollars", pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Dollars, 
                true, 41, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Gtd_Amt", pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Gtd_Amt, 
                true, 41, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Div_Amt", pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Div_Amt, 
                true, 41, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Gtd_Amt", pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Gtd_Amt, 
                true, 41, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Div_Amt", pnd_M_Cref_Mth_Sub_Total_Accum_Pnd_M_Cref_Mth_Sub_Fin_Div_Amt, 
                true, 41, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_52", "STOCK", "BLUE", 43, 1, 5);
            uiForm.setUiLabel("label_53", "ANNUAL", "BLUE", 43, 8, 6);
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt_3pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt.getValue(3+0), true, 
                43, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units_3pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units.getValue(3+0), true, 43, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars_3pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars.getValue(3+0), true, 
                43, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt_3pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt.getValue(3+0), true, 
                43, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt_3pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt.getValue(3+0), true, 
                43, 83, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt_3pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt.getValue(3+0), 
                true, 43, 100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt_3pls0", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt.getValue(3+0), 
                true, 43, 117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_54", "MMA", "BLUE", 44, 1, 3);
            uiForm.setUiLabel("label_55", "ANNUAL", "BLUE", 44, 8, 6);
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt_3pls1", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt.getValue(3+1), true, 
                44, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units_3pls1", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units.getValue(3+1), true, 44, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars_3pls1", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars.getValue(3+1), true, 
                44, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt_3pls1", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt.getValue(3+1), true, 
                44, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt_3pls1", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt.getValue(3+1), true, 
                44, 83, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt_3pls1", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt.getValue(3+1), 
                true, 44, 100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt_3pls1", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt.getValue(3+1), 
                true, 44, 117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_56", "SOCIAL ANNUAL", "BLUE", 45, 1, 13);
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt_3pls2", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt.getValue(3+2), true, 
                45, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units_3pls2", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units.getValue(3+2), true, 45, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars_3pls2", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars.getValue(3+2), true, 
                45, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt_3pls2", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt.getValue(3+2), true, 
                45, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt_3pls2", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt.getValue(3+2), true, 
                45, 83, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt_3pls2", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt.getValue(3+2), 
                true, 45, 100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt_3pls2", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt.getValue(3+2), 
                true, 45, 117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_57", "BOND", "BLUE", 46, 1, 4);
            uiForm.setUiLabel("label_58", "ANNUAL", "BLUE", 46, 8, 6);
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt_3pls3", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt.getValue(3+3), true, 
                46, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units_3pls3", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units.getValue(3+3), true, 46, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars_3pls3", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars.getValue(3+3), true, 
                46, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt_3pls3", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt.getValue(3+3), true, 
                46, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt_3pls3", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt.getValue(3+3), true, 
                46, 83, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt_3pls3", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt.getValue(3+3), 
                true, 46, 100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt_3pls3", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt.getValue(3+3), 
                true, 46, 117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_59", "GLOBAL ANNUAL", "BLUE", 47, 1, 13);
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt_3pls4", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt.getValue(3+4), true, 
                47, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units_3pls4", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units.getValue(3+4), true, 47, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars_3pls4", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars.getValue(3+4), true, 
                47, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt_3pls4", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt.getValue(3+4), true, 
                47, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt_3pls4", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt.getValue(3+4), true, 
                47, 83, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt_3pls4", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt.getValue(3+4), 
                true, 47, 100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt_3pls4", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt.getValue(3+4), 
                true, 47, 117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_60", "GROWTH ANNUAL", "BLUE", 48, 1, 13);
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt_3pls5", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt.getValue(3+5), true, 
                48, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units_3pls5", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units.getValue(3+5), true, 48, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars_3pls5", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars.getValue(3+5), true, 
                48, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt_3pls5", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt.getValue(3+5), true, 
                48, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt_3pls5", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt.getValue(3+5), true, 
                48, 83, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt_3pls5", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt.getValue(3+5), 
                true, 48, 100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt_3pls5", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt.getValue(3+5), 
                true, 48, 117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_61", "EQUITY ANNUAL", "BLUE", 49, 1, 13);
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt_3pls6", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt.getValue(3+6), true, 
                49, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units_3pls6", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units.getValue(3+6), true, 49, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars_3pls6", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars.getValue(3+6), true, 
                49, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt_3pls6", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt.getValue(3+6), true, 
                49, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt_3pls6", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt.getValue(3+6), true, 
                49, 83, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt_3pls6", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt.getValue(3+6), 
                true, 49, 100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt_3pls6", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt.getValue(3+6), 
                true, 49, 117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_62", "I L B", "BLUE", 50, 1, 5);
            uiForm.setUiLabel("label_63", "ANNUAL", "BLUE", 50, 8, 6);
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt_3pls7", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Pay_Cnt.getValue(3+7), true, 
                50, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units_3pls7", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Units.getValue(3+7), true, 50, 
                33, 12, "WHITE", "ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars_3pls7", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Dollars.getValue(3+7), true, 
                50, 49, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt_3pls7", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Gtd_Amt.getValue(3+7), true, 
                50, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt_3pls7", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Divid_Amt.getValue(3+7), true, 
                50, 83, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt_3pls7", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Gtd_Amt.getValue(3+7), 
                true, 50, 100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt_3pls7", pnd_M_C_Ann_Minor_Accum_Pnd_M_C_Ann_Fin_Div_Amt.getValue(3+7), 
                true, 50, 117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_64", "TOTAL", "BLUE", 51, 1, 5);
            uiForm.setUiLabel("label_65", "ANNUAL", "BLUE", 51, 8, 6);
            uiForm.setUiControl("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Pay_Cnt", pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Pay_Cnt, 
                true, 51, 19, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Units", pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Units, true, 
                51, 29, 16, "WHITE", "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Dollars", pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Dollars, 
                true, 51, 47, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Gtd_Amt", pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Gtd_Amt, 
                true, 51, 64, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Div_Amt", pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Div_Amt, 
                true, 51, 81, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Gtd_Amt", pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Gtd_Amt, 
                true, 51, 98, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Div_Amt", pnd_M_Cref_Ann_Sub_Total_Accum_Pnd_M_Cref_Ann_Sub_Fin_Div_Amt, 
                true, 51, 115, 16, "WHITE", "Z,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Dollars", pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Dollars, true, 53, 45, 18, 
                "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_66", "TOTAL ALL", "BLUE", 54, 1, 9);
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt_16", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Pay_Cnt.getValue(16), true, 54, 19, 9, 
                "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Units", pnd_M_Total_Tiaa_Cref_Accum_Pnd_M_Total_Units, true, 54, 29, 16, "WHITE", 
                "ZZZ,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt_16", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Gtd_Amt.getValue(16), true, 54, 66, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt_16", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Div_Amt.getValue(16), true, 54, 83, 14, 
                "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt_16", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Gtd_Amt.getValue(16), true, 54, 
                100, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt_16", pnd_M_Tiaa_By_Details_Pnd_M_Tiaa_Fin_Div_Amt.getValue(16), true, 54, 
                117, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
