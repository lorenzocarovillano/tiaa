/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:38 PM
**        *   FROM NATURAL MAP   :  Adsm870
************************************************************
**        * FILE NAME               : Adsm870.java
**        * CLASS NAME              : Adsm870
**        * INSTANCE NAME           : Adsm870
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #CREF-ANNUALLY-METHOD-FINAL-TOT.#CREF-IA-ANNUALLY-FINAL-PAYMENTS                                                         *     #CREF-ANNUALLY-METHOD-FINAL-TOT.#CREF-IA-ANNUALLY-FINAL-UNIT-VAL 
    *     #CREF-ANNUALLY-METHOD-FINAL-TOT.#CREF-IA-ANNUALLY-FINAL-UNITS                                                            *     #CREF-ANNUALLY-METHOD-FINAL-TOT.#CREF-SETTLED-ANNUALLY-FINAL-AMT
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm870 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Payments;
    private DbsField pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Unit_Val;
    private DbsField pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Units;
    private DbsField pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Settled_Annually_Final_Amt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Payments = parameters.newFieldInRecord("pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Payments", 
            "#CREF-ANNUALLY-METHOD-FINAL-TOT.#CREF-IA-ANNUALLY-FINAL-PAYMENTS", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Unit_Val = parameters.newFieldInRecord("pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Unit_Val", 
            "#CREF-ANNUALLY-METHOD-FINAL-TOT.#CREF-IA-ANNUALLY-FINAL-UNIT-VAL", FieldType.PACKED_DECIMAL, 12, 4);
        pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Units = parameters.newFieldInRecord("pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Units", 
            "#CREF-ANNUALLY-METHOD-FINAL-TOT.#CREF-IA-ANNUALLY-FINAL-UNITS", FieldType.PACKED_DECIMAL, 11, 3);
        pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Settled_Annually_Final_Amt = parameters.newFieldInRecord("pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Settled_Annually_Final_Amt", 
            "#CREF-ANNUALLY-METHOD-FINAL-TOT.#CREF-SETTLED-ANNUALLY-FINAL-AMT", FieldType.PACKED_DECIMAL, 10, 2);
        parameters.reset();
    }

    public Adsm870() throws Exception
    {
        super("Adsm870");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm870", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm870"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "-------------------------------------------------------------------------------", "", 2, 1, 79);
            uiForm.setUiLabel("label_2", "TOTALS", "BLUE", 3, 1, 6);
            uiForm.setUiControl("pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Settled_Annually_Final_Amt", pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Settled_Annually_Final_Amt, 
                true, 3, 23, 12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Units", pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Units, 
                true, 3, 37, 13, "WHITE", "Z,ZZZ,ZZ9.999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Payments", pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Payments, 
                true, 3, 51, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Unit_Val", pnd_Cref_Annually_Method_Final_Tot_Pnd_Cref_Ia_Annually_Final_Unit_Val, 
                true, 3, 66, 14, "WHITE", "Z,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
