/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:38:57 PM
**        *   FROM NATURAL MAP   :  Adsm766
************************************************************
**        * FILE NAME               : Adsm766.java
**        * CLASS NAME              : Adsm766
**        * INSTANCE NAME           : Adsm766
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #TIAA-STANDATD-TOTALS.#TIAA-DA-RATE-STD                                                                                  *     #TIAA-STANDATD-TOTALS.#TIAA-IA-RATE-STD 
    *     #TIAA-STANDATD-TOTALS.#TIAA-IA-STD-DIVIDEND-AMT                                                                          *     #TIAA-STANDATD-TOTALS.#TIAA-IA-STD-GUAR-COMMUT-REP-AMT 
    *     #TIAA-STANDATD-TOTALS.#TIAA-IA-STD-GURANTEED-AMT                                                                         *     #TIAA-STANDATD-TOTALS.#TIAA-PROD-STD 
    *     #TIAA-STANDATD-TOTALS.#TIAA-SETTLED-STD-AMT
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm766 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Da_Rate_Std;
    private DbsField pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Rate_Std;
    private DbsField pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Dividend_Amt;
    private DbsField pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Guar_Commut_Rep_Amt;
    private DbsField pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Amt;
    private DbsField pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Prod_Std;
    private DbsField pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Settled_Std_Amt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Da_Rate_Std = parameters.newFieldInRecord("pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Da_Rate_Std", "#TIAA-STANDATD-TOTALS.#TIAA-DA-RATE-STD", 
            FieldType.STRING, 2);
        pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Rate_Std = parameters.newFieldInRecord("pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Rate_Std", "#TIAA-STANDATD-TOTALS.#TIAA-IA-RATE-STD", 
            FieldType.STRING, 2);
        pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Dividend_Amt = parameters.newFieldInRecord("pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Dividend_Amt", "#TIAA-STANDATD-TOTALS.#TIAA-IA-STD-DIVIDEND-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Guar_Commut_Rep_Amt = parameters.newFieldInRecord("pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Guar_Commut_Rep_Amt", 
            "#TIAA-STANDATD-TOTALS.#TIAA-IA-STD-GUAR-COMMUT-REP-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Amt = parameters.newFieldInRecord("pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Amt", 
            "#TIAA-STANDATD-TOTALS.#TIAA-IA-STD-GURANTEED-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Prod_Std = parameters.newFieldInRecord("pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Prod_Std", "#TIAA-STANDATD-TOTALS.#TIAA-PROD-STD", 
            FieldType.STRING, 1);
        pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Settled_Std_Amt = parameters.newFieldInRecord("pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Settled_Std_Amt", "#TIAA-STANDATD-TOTALS.#TIAA-SETTLED-STD-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        parameters.reset();
    }

    public Adsm766() throws Exception
    {
        super("Adsm766");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm766", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm766"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Prod_Std", pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Prod_Std, true, 1, 2, 1, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Da_Rate_Std", pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Da_Rate_Std, true, 1, 10, 2, "WHITE", 
                true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Rate_Std", pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Rate_Std, true, 1, 15, 2, "WHITE", 
                true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Settled_Std_Amt", pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Settled_Std_Amt, true, 1, 20, 18, 
                "WHITE", "-ZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Amt", pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Guranteed_Amt, true, 
                1, 41, 16, "WHITE", "-ZZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Dividend_Amt", pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Dividend_Amt, true, 
                1, 61, 14, "WHITE", "-ZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Guar_Commut_Rep_Amt", pnd_Tiaa_Standatd_Totals_Pnd_Tiaa_Ia_Std_Guar_Commut_Rep_Amt, 
                true, 1, 79, 14, "WHITE", "-ZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
