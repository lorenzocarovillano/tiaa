/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:02 PM
**        *   FROM NATURAL MAP   :  Adsm781
************************************************************
**        * FILE NAME               : Adsm781.java
**        * CLASS NAME              : Adsm781
**        * INSTANCE NAME           : Adsm781
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #CREF-ANNUALLY-MINOR-ACCUM.#CREF-ANN-MAT-AMT(*)                                                                          *     #CREF-ANNUALLY-MINOR-ACCUM.#CREF-ANN-MAT-CNT(*) 
    *     #CREF-ANNUALLY-SUB-TOTAL-ACCUM.#CREF-SUB-ANN-MATURITY-AMT                                                                *     #CREF-ANNUALLY-SUB-TOTAL-ACCUM.#CREF-SUB-RATE-ANN-MATURITY-CNT 
    *     #CREF-MONTHLY-MINOR-ACCUM.#CREF-MTH-MAT-AMT(*)                                                                           *     #CREF-MONTHLY-MINOR-ACCUM.#CREF-MTH-MAT-CNT(*) 
    *     #CREF-MONTHLY-SUB-TOTAL-ACCUM.#CREF-SUB-MTH-MATURITY-AMT                                                                 *     #CREF-MONTHLY-SUB-TOTAL-ACCUM.#CREF-SUB-RATE-MTH-MATURITY-CNT 
    *     #EFFECTIVE-DATE #TIAA-MINOR-ACCUM.#TIAA-GRD-MATURITY-AMT                                                                 *     #TIAA-MINOR-ACCUM.#TIAA-IPRO-RATE-STD-MATURITY-CNT 
    *     #TIAA-MINOR-ACCUM.#TIAA-IPRO-STD-MATURITY-AMT                                                                            *     #TIAA-MINOR-ACCUM.#TIAA-RATE-GRD-MATURITY-CNT 
    *     #TIAA-MINOR-ACCUM.#TIAA-RATE-STD-MATURITY-CNT                                                                            *     #TIAA-MINOR-ACCUM.#TIAA-STBL-GRD-MATURITY-AMT 
    *     #TIAA-MINOR-ACCUM.#TIAA-STBL-GRD-MATURITY-CNT                                                                            *     #TIAA-MINOR-ACCUM.#TIAA-STBL-STD-MATURITY-AMT 
    *     #TIAA-MINOR-ACCUM.#TIAA-STBL-STD-MATURITY-CNT                                                                            *     #TIAA-MINOR-ACCUM.#TIAA-STD-MATURITY-AMT 
    *     #TIAA-MINOR-ACCUM.#TIAA-TPA-RATE-STD-MATURITY-CNT                                                                        *     #TIAA-MINOR-ACCUM.#TIAA-TPA-STD-MATURITY-AMT 
    *     #TIAA-SUB-ACCUM.#TIAA-SUB-MATURITY-AMT                                                                                   *     #TIAA-SUB-ACCUM.#TIAA-SUB-RATE-MATURITY-CNT 
    *     #TOTAL-CREF-AND-TIAA-ACCUM.#TOTAL-MATURITY-AMT                                                                           *     #TOTAL-CREF-AND-TIAA-ACCUM.#TOTAL-RATE-MATURITY-CNT
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm781 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt;
    private DbsField pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt;
    private DbsField pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Maturity_Amt;
    private DbsField pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Maturity_Cnt;
    private DbsField pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt;
    private DbsField pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt;
    private DbsField pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Maturity_Amt;
    private DbsField pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Maturity_Cnt;
    private DbsField pnd_Effective_Date;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Grd_Maturity_Amt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Rate_Std_Maturity_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Std_Maturity_Amt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Grd_Maturity_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Maturity_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Amt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Amt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Maturity_Amt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Rate_Std_Maturity_Cnt;
    private DbsField pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Std_Maturity_Amt;
    private DbsField pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt;
    private DbsField pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt;
    private DbsField pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt;
    private DbsField pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt = parameters.newFieldArrayInRecord("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt", "#CREF-ANNUALLY-MINOR-ACCUM.#CREF-ANN-MAT-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt = parameters.newFieldArrayInRecord("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt", "#CREF-ANNUALLY-MINOR-ACCUM.#CREF-ANN-MAT-CNT", 
            FieldType.NUMERIC, 7, new DbsArrayController(1, 20));
        pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Maturity_Amt = parameters.newFieldInRecord("pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Maturity_Amt", 
            "#CREF-ANNUALLY-SUB-TOTAL-ACCUM.#CREF-SUB-ANN-MATURITY-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Maturity_Cnt = parameters.newFieldInRecord("pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Maturity_Cnt", 
            "#CREF-ANNUALLY-SUB-TOTAL-ACCUM.#CREF-SUB-RATE-ANN-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt = parameters.newFieldArrayInRecord("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt", "#CREF-MONTHLY-MINOR-ACCUM.#CREF-MTH-MAT-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt = parameters.newFieldArrayInRecord("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt", "#CREF-MONTHLY-MINOR-ACCUM.#CREF-MTH-MAT-CNT", 
            FieldType.NUMERIC, 7, new DbsArrayController(1, 20));
        pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Maturity_Amt = parameters.newFieldInRecord("pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Maturity_Amt", 
            "#CREF-MONTHLY-SUB-TOTAL-ACCUM.#CREF-SUB-MTH-MATURITY-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Maturity_Cnt = parameters.newFieldInRecord("pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Maturity_Cnt", 
            "#CREF-MONTHLY-SUB-TOTAL-ACCUM.#CREF-SUB-RATE-MTH-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Effective_Date = parameters.newFieldInRecord("pnd_Effective_Date", "#EFFECTIVE-DATE", FieldType.NUMERIC, 8);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Grd_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Grd_Maturity_Amt", "#TIAA-MINOR-ACCUM.#TIAA-GRD-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Rate_Std_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Rate_Std_Maturity_Cnt", 
            "#TIAA-MINOR-ACCUM.#TIAA-IPRO-RATE-STD-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Std_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Std_Maturity_Amt", "#TIAA-MINOR-ACCUM.#TIAA-IPRO-STD-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Grd_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Grd_Maturity_Cnt", "#TIAA-MINOR-ACCUM.#TIAA-RATE-GRD-MATURITY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Maturity_Cnt", "#TIAA-MINOR-ACCUM.#TIAA-RATE-STD-MATURITY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Amt", "#TIAA-MINOR-ACCUM.#TIAA-STBL-GRD-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Cnt", "#TIAA-MINOR-ACCUM.#TIAA-STBL-GRD-MATURITY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Amt", "#TIAA-MINOR-ACCUM.#TIAA-STBL-STD-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Cnt", "#TIAA-MINOR-ACCUM.#TIAA-STBL-STD-MATURITY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Maturity_Amt", "#TIAA-MINOR-ACCUM.#TIAA-STD-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Rate_Std_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Rate_Std_Maturity_Cnt", 
            "#TIAA-MINOR-ACCUM.#TIAA-TPA-RATE-STD-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Std_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Std_Maturity_Amt", "#TIAA-MINOR-ACCUM.#TIAA-TPA-STD-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt = parameters.newFieldInRecord("pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt", "#TIAA-SUB-ACCUM.#TIAA-SUB-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt = parameters.newFieldInRecord("pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt", "#TIAA-SUB-ACCUM.#TIAA-SUB-RATE-MATURITY-CNT", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt = parameters.newFieldInRecord("pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt", "#TOTAL-CREF-AND-TIAA-ACCUM.#TOTAL-MATURITY-AMT", 
            FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt = parameters.newFieldInRecord("pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt", 
            "#TOTAL-CREF-AND-TIAA-ACCUM.#TOTAL-RATE-MATURITY-CNT", FieldType.PACKED_DECIMAL, 7);
        parameters.reset();
    }

    public Adsm781() throws Exception
    {
        super("Adsm781");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm781", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm781"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "Effective Date", "BLUE", 1, 1, 14);
            uiForm.setUiControl("pnd_Effective_Date", pnd_Effective_Date, true, 2, 2, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_2", "DA RATE", "BLUE", 3, 26, 7);
            uiForm.setUiLabel("label_3", "FINAL PREMIUM", "BLUE", 3, 42, 13);
            uiForm.setUiLabel("label_4", "Product", "BLUE", 4, 1, 7);
            uiForm.setUiLabel("label_5", "COUNTS", "BLUE", 4, 26, 6);
            uiForm.setUiLabel("label_6", "ACCUMULATIONS", "BLUE", 4, 42, 13);
            uiForm.setUiLabel("label_7", "TIAA STANDARD", "BLUE", 5, 1, 13);
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Maturity_Cnt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Std_Maturity_Cnt, true, 5, 24, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Maturity_Amt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Std_Maturity_Amt, true, 5, 41, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_8", "TIAA GRADED", "BLUE", 6, 1, 11);
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Grd_Maturity_Cnt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Rate_Grd_Maturity_Cnt, true, 6, 24, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Grd_Maturity_Amt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Grd_Maturity_Amt, true, 6, 41, 14, "WHITE", 
                "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_9", "TIAA IPRO", "BLUE", 7, 1, 9);
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Rate_Std_Maturity_Cnt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Rate_Std_Maturity_Cnt, 
                true, 7, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Std_Maturity_Amt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Ipro_Std_Maturity_Amt, true, 7, 41, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_10", "TIAA STABLE STANDARD", "BLUE", 8, 1, 20);
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Cnt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Cnt, true, 8, 24, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Amt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Std_Maturity_Amt, true, 8, 41, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_11", "TIAA STABLE GRADED", "BLUE", 9, 1, 18);
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Cnt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Cnt, true, 9, 24, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Amt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Stbl_Grd_Maturity_Amt, true, 9, 41, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_12", "TOTAL", "BLUE", 10, 1, 5);
            uiForm.setUiControl("pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt", pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Rate_Maturity_Cnt, true, 10, 24, 
                9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt", pnd_Tiaa_Sub_Accum_Pnd_Tiaa_Sub_Maturity_Amt, true, 10, 37, 18, "WHITE", 
                "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_13", "TIAA TPA", "BLUE", 12, 1, 8);
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Rate_Std_Maturity_Cnt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Rate_Std_Maturity_Cnt, true, 
                12, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Std_Maturity_Amt", pnd_Tiaa_Minor_Accum_Pnd_Tiaa_Tpa_Std_Maturity_Amt, true, 12, 41, 
                14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_14", "TIAA ACCESS MONTHLY", "BLUE", 14, 1, 19);
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt_12pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt.getValue(12+0), 
                true, 14, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt_12pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt.getValue(12+0), 
                true, 14, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_15", "TIAA ACCESS ANNUAL", "BLUE", 15, 1, 18);
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt_12pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt.getValue(12+0), 
                true, 15, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt_12pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt.getValue(12+0), 
                true, 15, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_16", "TIAA REA MONTHLY", "BLUE", 16, 1, 16);
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt_2pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt.getValue(2+0), 
                true, 16, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt_2pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt.getValue(2+0), 
                true, 16, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "TIAA REA ANNUAL", "BLUE", 17, 1, 15);
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt_2pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt.getValue(2+0), 
                true, 17, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt_2pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt.getValue(2+0), 
                true, 17, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_18", "STOCK", "BLUE", 19, 1, 5);
            uiForm.setUiLabel("label_19", "MONTHLY", "BLUE", 19, 8, 7);
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt_3pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt.getValue(3+0), 
                true, 19, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt_3pls0", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt.getValue(3+0), 
                true, 19, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_20", "MMA", "BLUE", 20, 1, 3);
            uiForm.setUiLabel("label_21", "MONTHLY", "BLUE", 20, 8, 7);
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt_3pls1", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt.getValue(3+1), 
                true, 20, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt_3pls1", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt.getValue(3+1), 
                true, 20, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_22", "SOCIAL MONTHLY", "BLUE", 21, 1, 14);
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt_3pls2", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt.getValue(3+2), 
                true, 21, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt_3pls2", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt.getValue(3+2), 
                true, 21, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_23", "BOND", "BLUE", 22, 1, 4);
            uiForm.setUiLabel("label_24", "MONTHLY", "BLUE", 22, 8, 7);
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt_3pls3", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt.getValue(3+3), 
                true, 22, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt_3pls3", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt.getValue(3+3), 
                true, 22, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_25", "GLOBAL MONTHLY", "BLUE", 23, 1, 14);
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt_3pls4", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt.getValue(3+4), 
                true, 23, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt_3pls4", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt.getValue(3+4), 
                true, 23, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_26", "GROWTH MONTHLY", "BLUE", 24, 1, 14);
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt_3pls5", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt.getValue(3+5), 
                true, 24, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt_3pls5", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt.getValue(3+5), 
                true, 24, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_27", "EQUITY MONTHLY", "BLUE", 25, 1, 14);
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt_3pls6", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt.getValue(3+6), 
                true, 25, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt_3pls6", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt.getValue(3+6), 
                true, 25, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_28", "I L B", "BLUE", 26, 1, 5);
            uiForm.setUiLabel("label_29", "MONTHLY", "BLUE", 26, 8, 7);
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt_3pls7", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Cnt.getValue(3+7), 
                true, 26, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt_3pls7", pnd_Cref_Monthly_Minor_Accum_Pnd_Cref_Mth_Mat_Amt.getValue(3+7), 
                true, 26, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_30", "TOTAL CREF MONTHLY", "BLUE", 27, 1, 18);
            uiForm.setUiControl("pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Maturity_Cnt", pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Mth_Maturity_Cnt, 
                true, 27, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Maturity_Amt", pnd_Cref_Monthly_Sub_Total_Accum_Pnd_Cref_Sub_Mth_Maturity_Amt, 
                true, 27, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_31", "STOCK", "BLUE", 29, 1, 5);
            uiForm.setUiLabel("label_32", "ANNUAL", "BLUE", 29, 8, 6);
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt_3pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt.getValue(3+0), 
                true, 29, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt_3pls0", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt.getValue(3+0), 
                true, 29, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_33", "MMA", "BLUE", 30, 1, 3);
            uiForm.setUiLabel("label_34", "ANNUAL", "BLUE", 30, 8, 6);
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt_3pls1", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt.getValue(3+1), 
                true, 30, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt_3pls1", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt.getValue(3+1), 
                true, 30, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_35", "SOCIAL ANNUAL", "BLUE", 31, 1, 13);
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt_3pls2", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt.getValue(3+2), 
                true, 31, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt_3pls2", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt.getValue(3+2), 
                true, 31, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_36", "BOND", "BLUE", 32, 1, 4);
            uiForm.setUiLabel("label_37", "ANNUAL", "BLUE", 32, 8, 6);
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt_3pls3", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt.getValue(3+3), 
                true, 32, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt_3pls3", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt.getValue(3+3), 
                true, 32, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_38", "GLOBAL ANNUAL", "BLUE", 33, 1, 13);
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt_3pls4", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt.getValue(3+4), 
                true, 33, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt_3pls4", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt.getValue(3+4), 
                true, 33, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_39", "GROWTH ANNUAL", "BLUE", 34, 1, 13);
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt_3pls5", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt.getValue(3+5), 
                true, 34, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt_3pls5", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt.getValue(3+5), 
                true, 34, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_40", "EQUITY ANNUAL", "BLUE", 35, 1, 13);
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt_3pls6", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt.getValue(3+6), 
                true, 35, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt_3pls6", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt.getValue(3+6), 
                true, 35, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_41", "I L B", "BLUE", 36, 1, 5);
            uiForm.setUiLabel("label_42", "ANNUAL", "BLUE", 36, 8, 6);
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt_3pls7", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Cnt.getValue(3+7), 
                true, 36, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt_3pls7", pnd_Cref_Annually_Minor_Accum_Pnd_Cref_Ann_Mat_Amt.getValue(3+7), 
                true, 36, 41, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_43", "TOTAL CREF ANNUAL", "BLUE", 37, 1, 17);
            uiForm.setUiControl("pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Maturity_Cnt", pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Rate_Ann_Maturity_Cnt, 
                true, 37, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Maturity_Amt", pnd_Cref_Annually_Sub_Total_Accum_Pnd_Cref_Sub_Ann_Maturity_Amt, 
                true, 37, 37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_44", "TOTAL ALL", "BLUE", 40, 1, 9);
            uiForm.setUiControl("pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt", pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Rate_Maturity_Cnt, 
                true, 40, 24, 9, "WHITE", "Z,ZZZ,ZZ9", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt", pnd_Total_Cref_And_Tiaa_Accum_Pnd_Total_Maturity_Amt, true, 40, 
                37, 18, "WHITE", "ZZZ,ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
