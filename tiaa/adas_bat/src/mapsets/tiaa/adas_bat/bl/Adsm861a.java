/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:34 PM
**        *   FROM NATURAL MAP   :  Adsm861a
************************************************************
**        * FILE NAME               : Adsm861a.java
**        * CLASS NAME              : Adsm861a
**        * INSTANCE NAME           : Adsm861a
************************************************************
* MAP2: PROTOTYPE VERSION 820 --- CREATED BY NAT 0802050006 ---                                                                * WRITE USING MAP 'XXXXXXXX' 
    *     #ACCOUNT-DTE-MDCY #ANNT-START-DTE-MDCY #CONT-ISSUE-DTE-MDCY                                                              *     #CREF-CERT-IND(*) 
    #CREF-CERT-NBR(*)                                                                                      *     #CREF-HEADER-SETTLE-AMOUNTS.#CREF-HEADER-RTB-SETTLE-AMOUNT 
    *     #CREF-HEADER-SETTLE-AMOUNTS.#CREF-HEADER-SETTLE-AMOUNT                                                                   *     #DESTINATION-CODE.#DESTINATION-CDE(*) 
    #EFFECTIVE-DTE-MDCY                                                                *     #EMPL-TERM-DTE-MDCY #FORM-COMP-DTE-MDCY #FRST-BIRTH-DTE-MDCY 
    *     #FRST-CITIZENSHIP #FRST-PEROD-DTE-MDCY #IA-NUMBER                                                                        *     #LAST-GUAR-DTE-MDCY 
    #LAST-PREM-DTE-MDCY #NAP-ANNTY-OPTN                                                                  *     #NAP-GRNTEE-PERIOD #NAP-PYMNT-MODE-ALL #NON-PREM-DTE-MDCY 
    *     #PARTCIPANT-NAME-1 #PARTCIPANT-NAME-2 #RESIDENCE #ROLLOVER-AMOUNT                                                        *     #ROLLOVER-CONTRACT-NBR(*) 
    #RQST-ID.#UNIQUE-ID #RTB                                                                       *     #RTB-ACCOUNT-DTE-MDCY #RTB-ROLLOVER-CODE.#RTB-ROLLOVER-CDE(*) 
    *     #RTB-SETTLE-DTE-MDCY #SCND-BIRTH-DTE-MDCY #SCND-SPOUSE #SEX-1                                                            *     #SEX-2 #SLASH #SOCIAL-SEC-NBR-1 
    #SOCIAL-SEC-NBR-2                                                                        *     #TARGET-ALL.#TARGET-DISC(*) #TARGET-ALL.#TARGET-NUMBER(*) 
    *     #WORK-RQST-ID
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm861a extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Account_Dte_Mdcy;
    private DbsField pnd_Annt_Start_Dte_Mdcy;
    private DbsField pnd_Cont_Issue_Dte_Mdcy;
    private DbsField pnd_Cref_Cert_Ind;
    private DbsField pnd_Cref_Cert_Nbr;
    private DbsField pnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Rtb_Settle_Amount;
    private DbsField pnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Settle_Amount;
    private DbsField pnd_Destination_Code_Pnd_Destination_Cde;
    private DbsField pnd_Effective_Dte_Mdcy;
    private DbsField pnd_Empl_Term_Dte_Mdcy;
    private DbsField pnd_Form_Comp_Dte_Mdcy;
    private DbsField pnd_Frst_Birth_Dte_Mdcy;
    private DbsField pnd_Frst_Citizenship;
    private DbsField pnd_Frst_Perod_Dte_Mdcy;
    private DbsField pnd_Ia_Number;
    private DbsField pnd_Last_Guar_Dte_Mdcy;
    private DbsField pnd_Last_Prem_Dte_Mdcy;
    private DbsField pnd_Nap_Annty_Optn;
    private DbsField pnd_Nap_Grntee_Period;
    private DbsField pnd_Nap_Pymnt_Mode_All;
    private DbsField pnd_Non_Prem_Dte_Mdcy;
    private DbsField pnd_Partcipant_Name_1;
    private DbsField pnd_Partcipant_Name_2;
    private DbsField pnd_Residence;
    private DbsField pnd_Rollover_Amount;
    private DbsField pnd_Rollover_Contract_Nbr;
    private DbsField pnd_Rqst_Id_Pnd_Unique_Id;
    private DbsField pnd_Rtb;
    private DbsField pnd_Rtb_Account_Dte_Mdcy;
    private DbsField pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde;
    private DbsField pnd_Rtb_Settle_Dte_Mdcy;
    private DbsField pnd_Scnd_Birth_Dte_Mdcy;
    private DbsField pnd_Scnd_Spouse;
    private DbsField pnd_Sex_1;
    private DbsField pnd_Sex_2;
    private DbsField pnd_Slash;
    private DbsField pnd_Social_Sec_Nbr_1;
    private DbsField pnd_Social_Sec_Nbr_2;
    private DbsField pnd_Target_All_Pnd_Target_Disc;
    private DbsField pnd_Target_All_Pnd_Target_Number;
    private DbsField pnd_Work_Rqst_Id;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Account_Dte_Mdcy = parameters.newFieldInRecord("pnd_Account_Dte_Mdcy", "#ACCOUNT-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Annt_Start_Dte_Mdcy = parameters.newFieldInRecord("pnd_Annt_Start_Dte_Mdcy", "#ANNT-START-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Cont_Issue_Dte_Mdcy = parameters.newFieldInRecord("pnd_Cont_Issue_Dte_Mdcy", "#CONT-ISSUE-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Cref_Cert_Ind = parameters.newFieldArrayInRecord("pnd_Cref_Cert_Ind", "#CREF-CERT-IND", FieldType.STRING, 6, new DbsArrayController(1, 10));
        pnd_Cref_Cert_Nbr = parameters.newFieldArrayInRecord("pnd_Cref_Cert_Nbr", "#CREF-CERT-NBR", FieldType.STRING, 11, new DbsArrayController(1, 10));
        pnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Rtb_Settle_Amount = parameters.newFieldInRecord("pnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Rtb_Settle_Amount", 
            "#CREF-HEADER-SETTLE-AMOUNTS.#CREF-HEADER-RTB-SETTLE-AMOUNT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Settle_Amount = parameters.newFieldInRecord("pnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Settle_Amount", 
            "#CREF-HEADER-SETTLE-AMOUNTS.#CREF-HEADER-SETTLE-AMOUNT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Destination_Code_Pnd_Destination_Cde = parameters.newFieldArrayInRecord("pnd_Destination_Code_Pnd_Destination_Cde", "#DESTINATION-CODE.#DESTINATION-CDE", 
            FieldType.STRING, 5, new DbsArrayController(1, 3));
        pnd_Effective_Dte_Mdcy = parameters.newFieldInRecord("pnd_Effective_Dte_Mdcy", "#EFFECTIVE-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Empl_Term_Dte_Mdcy = parameters.newFieldInRecord("pnd_Empl_Term_Dte_Mdcy", "#EMPL-TERM-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Form_Comp_Dte_Mdcy = parameters.newFieldInRecord("pnd_Form_Comp_Dte_Mdcy", "#FORM-COMP-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Frst_Birth_Dte_Mdcy = parameters.newFieldInRecord("pnd_Frst_Birth_Dte_Mdcy", "#FRST-BIRTH-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Frst_Citizenship = parameters.newFieldInRecord("pnd_Frst_Citizenship", "#FRST-CITIZENSHIP", FieldType.STRING, 2);
        pnd_Frst_Perod_Dte_Mdcy = parameters.newFieldInRecord("pnd_Frst_Perod_Dte_Mdcy", "#FRST-PEROD-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Ia_Number = parameters.newFieldInRecord("pnd_Ia_Number", "#IA-NUMBER", FieldType.STRING, 10);
        pnd_Last_Guar_Dte_Mdcy = parameters.newFieldInRecord("pnd_Last_Guar_Dte_Mdcy", "#LAST-GUAR-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Last_Prem_Dte_Mdcy = parameters.newFieldInRecord("pnd_Last_Prem_Dte_Mdcy", "#LAST-PREM-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Nap_Annty_Optn = parameters.newFieldInRecord("pnd_Nap_Annty_Optn", "#NAP-ANNTY-OPTN", FieldType.STRING, 2);
        pnd_Nap_Grntee_Period = parameters.newFieldInRecord("pnd_Nap_Grntee_Period", "#NAP-GRNTEE-PERIOD", FieldType.NUMERIC, 2);
        pnd_Nap_Pymnt_Mode_All = parameters.newFieldInRecord("pnd_Nap_Pymnt_Mode_All", "#NAP-PYMNT-MODE-ALL", FieldType.STRING, 20);
        pnd_Non_Prem_Dte_Mdcy = parameters.newFieldInRecord("pnd_Non_Prem_Dte_Mdcy", "#NON-PREM-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Partcipant_Name_1 = parameters.newFieldInRecord("pnd_Partcipant_Name_1", "#PARTCIPANT-NAME-1", FieldType.STRING, 30);
        pnd_Partcipant_Name_2 = parameters.newFieldInRecord("pnd_Partcipant_Name_2", "#PARTCIPANT-NAME-2", FieldType.STRING, 30);
        pnd_Residence = parameters.newFieldInRecord("pnd_Residence", "#RESIDENCE", FieldType.STRING, 20);
        pnd_Rollover_Amount = parameters.newFieldInRecord("pnd_Rollover_Amount", "#ROLLOVER-AMOUNT", FieldType.NUMERIC, 12, 2);
        pnd_Rollover_Contract_Nbr = parameters.newFieldArrayInRecord("pnd_Rollover_Contract_Nbr", "#ROLLOVER-CONTRACT-NBR", FieldType.STRING, 10, new 
            DbsArrayController(1, 2));
        pnd_Rqst_Id_Pnd_Unique_Id = parameters.newFieldInRecord("pnd_Rqst_Id_Pnd_Unique_Id", "#RQST-ID.#UNIQUE-ID", FieldType.NUMERIC, 12);
        pnd_Rtb = parameters.newFieldInRecord("pnd_Rtb", "#RTB", FieldType.STRING, 1);
        pnd_Rtb_Account_Dte_Mdcy = parameters.newFieldInRecord("pnd_Rtb_Account_Dte_Mdcy", "#RTB-ACCOUNT-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde = parameters.newFieldArrayInRecord("pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde", "#RTB-ROLLOVER-CODE.#RTB-ROLLOVER-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 3));
        pnd_Rtb_Settle_Dte_Mdcy = parameters.newFieldInRecord("pnd_Rtb_Settle_Dte_Mdcy", "#RTB-SETTLE-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Scnd_Birth_Dte_Mdcy = parameters.newFieldInRecord("pnd_Scnd_Birth_Dte_Mdcy", "#SCND-BIRTH-DTE-MDCY", FieldType.NUMERIC, 8);
        pnd_Scnd_Spouse = parameters.newFieldInRecord("pnd_Scnd_Spouse", "#SCND-SPOUSE", FieldType.STRING, 1);
        pnd_Sex_1 = parameters.newFieldInRecord("pnd_Sex_1", "#SEX-1", FieldType.STRING, 1);
        pnd_Sex_2 = parameters.newFieldInRecord("pnd_Sex_2", "#SEX-2", FieldType.STRING, 1);
        pnd_Slash = parameters.newFieldInRecord("pnd_Slash", "#SLASH", FieldType.STRING, 1);
        pnd_Social_Sec_Nbr_1 = parameters.newFieldInRecord("pnd_Social_Sec_Nbr_1", "#SOCIAL-SEC-NBR-1", FieldType.NUMERIC, 9);
        pnd_Social_Sec_Nbr_2 = parameters.newFieldInRecord("pnd_Social_Sec_Nbr_2", "#SOCIAL-SEC-NBR-2", FieldType.NUMERIC, 9);
        pnd_Target_All_Pnd_Target_Disc = parameters.newFieldArrayInRecord("pnd_Target_All_Pnd_Target_Disc", "#TARGET-ALL.#TARGET-DISC", FieldType.STRING, 
            5, new DbsArrayController(1, 15));
        pnd_Target_All_Pnd_Target_Number = parameters.newFieldArrayInRecord("pnd_Target_All_Pnd_Target_Number", "#TARGET-ALL.#TARGET-NUMBER", FieldType.STRING, 
            6, new DbsArrayController(1, 15));
        pnd_Work_Rqst_Id = parameters.newFieldInRecord("pnd_Work_Rqst_Id", "#WORK-RQST-ID", FieldType.STRING, 6);
        parameters.reset();
    }

    public Adsm861a() throws Exception
    {
        super("Adsm861a");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=080 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm861a", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm861a"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiLabel("label_1", "Work Request ID.:", "BLUE", 3, 1, 17);
            uiForm.setUiControl("pnd_Work_Rqst_Id", pnd_Work_Rqst_Id, true, 3, 19, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_2", "Annuitant:", "BLUE", 3, 42, 10);
            uiForm.setUiControl("pnd_Partcipant_Name_1", pnd_Partcipant_Name_1, true, 3, 53, 25, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_3", "Pin Number:", "BLUE", 4, 1, 11);
            uiForm.setUiControl("pnd_Rqst_Id_Pnd_Unique_Id", pnd_Rqst_Id_Pnd_Unique_Id, true, 4, 13, 12, "WHITE", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_4", "IA Number:", "BLUE", 4, 42, 10);
            uiForm.setUiControl("pnd_Ia_Number", pnd_Ia_Number, true, 4, 53, 11, "WHITE", "XXXXXXX-XXX", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_5", "SSN:", "BLUE", 5, 1, 4);
            uiForm.setUiControl("pnd_Social_Sec_Nbr_1", pnd_Social_Sec_Nbr_1, true, 5, 6, 11, "WHITE", "999-99-9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_6", "Date of Birth:", "BLUE", 5, 42, 14);
            uiForm.setUiControl("pnd_Frst_Birth_Dte_Mdcy", pnd_Frst_Birth_Dte_Mdcy, true, 5, 57, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_7", "Sex:", "BLUE", 5, 73, 4);
            uiForm.setUiControl("pnd_Sex_1", pnd_Sex_1, true, 5, 78, 1, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_8", "Citizenship:", "BLUE", 6, 1, 12);
            uiForm.setUiControl("pnd_Frst_Citizenship", pnd_Frst_Citizenship, true, 6, 14, 2, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_9", "Residence:", "BLUE", 6, 42, 10);
            uiForm.setUiControl("pnd_Residence", pnd_Residence, true, 6, 53, 20, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_10", "Annuitant Partner:", "BLUE", 9, 1, 18);
            uiForm.setUiControl("pnd_Partcipant_Name_2", pnd_Partcipant_Name_2, true, 9, 20, 20, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_11", "Relationship :", "BLUE", 9, 42, 14);
            uiForm.setUiControl("pnd_Scnd_Spouse", pnd_Scnd_Spouse, true, 9, 57, 1, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_12", "Sex:", "BLUE", 9, 73, 4);
            uiForm.setUiControl("pnd_Sex_2", pnd_Sex_2, true, 9, 78, 1, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_13", "SSN:", "BLUE", 10, 1, 4);
            uiForm.setUiControl("pnd_Social_Sec_Nbr_2", pnd_Social_Sec_Nbr_2, true, 10, 6, 11, "WHITE", "999-99-9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_14", "Date of Birth:", "BLUE", 10, 42, 14);
            uiForm.setUiControl("pnd_Scnd_Birth_Dte_Mdcy", pnd_Scnd_Birth_Dte_Mdcy, true, 10, 57, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_15", "Contract Issue Date:", "BLUE", 12, 1, 20);
            uiForm.setUiControl("pnd_Cont_Issue_Dte_Mdcy", pnd_Cont_Issue_Dte_Mdcy, true, 12, 22, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_16", "Total Settlement Amt :", "BLUE", 12, 42, 22);
            uiForm.setUiControl("pnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Settle_Amount", pnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Settle_Amount, 
                true, 12, 65, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_17", "Annuity Start Date :", "BLUE", 13, 1, 20);
            uiForm.setUiControl("pnd_Annt_Start_Dte_Mdcy", pnd_Annt_Start_Dte_Mdcy, true, 13, 22, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_18", "First Periodic Pay Date:", "BLUE", 13, 42, 24);
            uiForm.setUiControl("pnd_Frst_Perod_Dte_Mdcy", pnd_Frst_Perod_Dte_Mdcy, true, 13, 67, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_19", "Last Premium Received Date:", "BLUE", 14, 1, 27);
            uiForm.setUiControl("pnd_Last_Prem_Dte_Mdcy", pnd_Last_Prem_Dte_Mdcy, true, 14, 29, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_20", "Forms Completion Date:", "BLUE", 14, 42, 22);
            uiForm.setUiControl("pnd_Form_Comp_Dte_Mdcy", pnd_Form_Comp_Dte_Mdcy, true, 14, 65, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_21", "Accounting Date:", "BLUE", 15, 1, 16);
            uiForm.setUiControl("pnd_Account_Dte_Mdcy", pnd_Account_Dte_Mdcy, true, 15, 18, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_22", "Non Premium Effective date:", "BLUE", 15, 42, 27);
            uiForm.setUiControl("pnd_Non_Prem_Dte_Mdcy", pnd_Non_Prem_Dte_Mdcy, true, 15, 70, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_23", "RTB:", "BLUE", 17, 1, 4);
            uiForm.setUiControl("pnd_Rtb", pnd_Rtb, true, 17, 6, 1, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_24", "Effective Date:", "BLUE", 17, 10, 15);
            uiForm.setUiControl("pnd_Effective_Dte_Mdcy", pnd_Effective_Dte_Mdcy, true, 17, 26, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_25", "RTB Accounting Date:", "BLUE", 17, 42, 20);
            uiForm.setUiControl("pnd_Rtb_Account_Dte_Mdcy", pnd_Rtb_Account_Dte_Mdcy, true, 17, 63, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_26", "RTB Settlement Date:", "BLUE", 18, 1, 20);
            uiForm.setUiControl("pnd_Rtb_Settle_Dte_Mdcy", pnd_Rtb_Settle_Dte_Mdcy, true, 18, 22, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_27", "RTB Settlement Amount:", "BLUE", 18, 42, 22);
            uiForm.setUiControl("pnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Rtb_Settle_Amount", pnd_Cref_Header_Settle_Amounts_Pnd_Cref_Header_Rtb_Settle_Amount, 
                true, 18, 65, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_28", "RTB Rollover:", "BLUE", 19, 1, 13);
            uiForm.setUiControl("pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde_1pls0", pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde.getValue(1+0), true, 19, 
                15, 2, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde_1pls1", pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde.getValue(1+1), true, 19, 
                18, 2, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde_1pls2", pnd_Rtb_Rollover_Code_Pnd_Rtb_Rollover_Cde.getValue(1+2), true, 19, 
                21, 2, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_29", "Destination Code:", "BLUE", 19, 42, 17);
            uiForm.setUiControl("pnd_Destination_Code_Pnd_Destination_Cde_1pls0", pnd_Destination_Code_Pnd_Destination_Cde.getValue(1+0), true, 19, 60, 
                5, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Destination_Code_Pnd_Destination_Cde_1pls1", pnd_Destination_Code_Pnd_Destination_Cde.getValue(1+1), true, 19, 66, 
                5, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Destination_Code_Pnd_Destination_Cde_1pls2", pnd_Destination_Code_Pnd_Destination_Cde.getValue(1+2), true, 19, 72, 
                5, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_30", "Rollover Contract #:", "BLUE", 20, 1, 20);
            uiForm.setUiControl("pnd_Rollover_Contract_Nbr_1", pnd_Rollover_Contract_Nbr.getValue(1), true, 20, 22, 10, "WHITE", true, false, null, null, 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_31", "Rollover Dollar Amount:", "BLUE", 20, 42, 23);
            uiForm.setUiControl("pnd_Rollover_Amount", pnd_Rollover_Amount, true, 20, 66, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Rollover_Contract_Nbr_2", pnd_Rollover_Contract_Nbr.getValue(2), true, 21, 22, 10, "WHITE", true, false, null, null, 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_32", "Option:", "BLUE", 22, 1, 7);
            uiForm.setUiControl("pnd_Nap_Annty_Optn", pnd_Nap_Annty_Optn, true, 22, 9, 2, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Slash", pnd_Slash, true, 22, 12, 1, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Nap_Grntee_Period", pnd_Nap_Grntee_Period, true, 22, 14, 2, "WHITE", "99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_33", "Employ Termination Date :", "BLUE", 22, 42, 25);
            uiForm.setUiControl("pnd_Empl_Term_Dte_Mdcy", pnd_Empl_Term_Dte_Mdcy, true, 22, 68, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_34", "Mode:", "BLUE", 23, 1, 5);
            uiForm.setUiControl("pnd_Nap_Pymnt_Mode_All", pnd_Nap_Pymnt_Mode_All, true, 23, 7, 20, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_35", "Last Guaranteed Pay Date:", "BLUE", 23, 42, 25);
            uiForm.setUiControl("pnd_Last_Guar_Dte_Mdcy", pnd_Last_Guar_Dte_Mdcy, true, 23, 68, 10, "WHITE", "99/99/9999", true, true, null, "0123456789+-, ", 
                "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiLabel("label_36", "TIAA Contract No:", "BLUE", 26, 1, 17);
            uiForm.setUiControl("pnd_Cref_Cert_Nbr_1pls0", pnd_Cref_Cert_Nbr.getValue(1+0), true, 26, 19, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cref_Cert_Ind_1pls0", pnd_Cref_Cert_Ind.getValue(1+0), true, 26, 31, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiLabel("label_37", "Target:", "BLUE", 26, 42, 7);
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls0", pnd_Target_All_Pnd_Target_Number.getValue(1+0), true, 26, 50, 6, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls0", pnd_Target_All_Pnd_Target_Disc.getValue(1+0), true, 26, 58, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Cert_Nbr_1pls1", pnd_Cref_Cert_Nbr.getValue(1+1), true, 27, 19, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cref_Cert_Ind_1pls1", pnd_Cref_Cert_Ind.getValue(1+1), true, 27, 31, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls1", pnd_Target_All_Pnd_Target_Number.getValue(1+1), true, 27, 50, 6, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls1", pnd_Target_All_Pnd_Target_Disc.getValue(1+1), true, 27, 58, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Cert_Nbr_1pls2", pnd_Cref_Cert_Nbr.getValue(1+2), true, 28, 19, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cref_Cert_Ind_1pls2", pnd_Cref_Cert_Ind.getValue(1+2), true, 28, 31, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls2", pnd_Target_All_Pnd_Target_Number.getValue(1+2), true, 28, 50, 6, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls2", pnd_Target_All_Pnd_Target_Disc.getValue(1+2), true, 28, 58, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Cert_Nbr_1pls3", pnd_Cref_Cert_Nbr.getValue(1+3), true, 29, 19, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cref_Cert_Ind_1pls3", pnd_Cref_Cert_Ind.getValue(1+3), true, 29, 31, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls3", pnd_Target_All_Pnd_Target_Number.getValue(1+3), true, 29, 50, 6, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls3", pnd_Target_All_Pnd_Target_Disc.getValue(1+3), true, 29, 58, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Cert_Nbr_1pls4", pnd_Cref_Cert_Nbr.getValue(1+4), true, 30, 19, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cref_Cert_Ind_1pls4", pnd_Cref_Cert_Ind.getValue(1+4), true, 30, 31, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls4", pnd_Target_All_Pnd_Target_Number.getValue(1+4), true, 30, 50, 6, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls4", pnd_Target_All_Pnd_Target_Disc.getValue(1+4), true, 30, 58, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Cert_Nbr_1pls5", pnd_Cref_Cert_Nbr.getValue(1+5), true, 31, 19, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cref_Cert_Ind_1pls5", pnd_Cref_Cert_Ind.getValue(1+5), true, 31, 31, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls5", pnd_Target_All_Pnd_Target_Number.getValue(1+5), true, 31, 50, 6, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls5", pnd_Target_All_Pnd_Target_Disc.getValue(1+5), true, 31, 58, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Cert_Nbr_1pls6", pnd_Cref_Cert_Nbr.getValue(1+6), true, 32, 19, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cref_Cert_Ind_1pls6", pnd_Cref_Cert_Ind.getValue(1+6), true, 32, 31, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls6", pnd_Target_All_Pnd_Target_Number.getValue(1+6), true, 32, 50, 6, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls6", pnd_Target_All_Pnd_Target_Disc.getValue(1+6), true, 32, 58, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Cert_Nbr_1pls7", pnd_Cref_Cert_Nbr.getValue(1+7), true, 33, 19, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cref_Cert_Ind_1pls7", pnd_Cref_Cert_Ind.getValue(1+7), true, 33, 31, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls7", pnd_Target_All_Pnd_Target_Number.getValue(1+7), true, 33, 50, 6, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls7", pnd_Target_All_Pnd_Target_Disc.getValue(1+7), true, 33, 58, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Cert_Nbr_1pls8", pnd_Cref_Cert_Nbr.getValue(1+8), true, 34, 19, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cref_Cert_Ind_1pls8", pnd_Cref_Cert_Ind.getValue(1+8), true, 34, 31, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls8", pnd_Target_All_Pnd_Target_Number.getValue(1+8), true, 34, 50, 6, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls8", pnd_Target_All_Pnd_Target_Disc.getValue(1+8), true, 34, 58, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Cert_Nbr_1pls9", pnd_Cref_Cert_Nbr.getValue(1+9), true, 35, 19, 11, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Cref_Cert_Ind_1pls9", pnd_Cref_Cert_Ind.getValue(1+9), true, 35, 31, 6, "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", 
                ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls9", pnd_Target_All_Pnd_Target_Number.getValue(1+9), true, 35, 50, 6, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls9", pnd_Target_All_Pnd_Target_Disc.getValue(1+9), true, 35, 58, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls10", pnd_Target_All_Pnd_Target_Number.getValue(1+10), true, 36, 50, 6, "WHITE", 
                true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls10", pnd_Target_All_Pnd_Target_Disc.getValue(1+10), true, 36, 58, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls11", pnd_Target_All_Pnd_Target_Number.getValue(1+11), true, 37, 50, 6, "WHITE", 
                true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls11", pnd_Target_All_Pnd_Target_Disc.getValue(1+11), true, 37, 58, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls12", pnd_Target_All_Pnd_Target_Number.getValue(1+12), true, 38, 50, 6, "WHITE", 
                true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls12", pnd_Target_All_Pnd_Target_Disc.getValue(1+12), true, 38, 58, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls13", pnd_Target_All_Pnd_Target_Number.getValue(1+13), true, 39, 50, 6, "WHITE", 
                true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls13", pnd_Target_All_Pnd_Target_Disc.getValue(1+13), true, 39, 58, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Number_1pls14", pnd_Target_All_Pnd_Target_Number.getValue(1+14), true, 40, 50, 6, "WHITE", 
                true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Target_All_Pnd_Target_Disc_1pls14", pnd_Target_All_Pnd_Target_Disc.getValue(1+14), true, 40, 58, 5, "WHITE", true, 
                false, null, null, "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
