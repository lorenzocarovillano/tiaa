/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 *   DATE
************************************************************
** INIT   *  INITIAL VERSION     *  2021-05-13 07:39:40 PM
**        *   FROM NATURAL MAP   :  Adsm880
************************************************************
**        * FILE NAME               : Adsm880.java
**        * CLASS NAME              : Adsm880
**        * INSTANCE NAME           : Adsm880
************************************************************
* MAP2: PROTOTYPE                                                                                                              * WRITE USING MAP 'XXXXXXXX' 
    *     #CREF-MONTHLY-TOTALS.#CREF-DA-RATE-MONTHLY                                                                               *     #CREF-MONTHLY-TOTALS.#CREF-IA-MONTHLY-PAYMENTS 
    *     #CREF-MONTHLY-TOTALS.#CREF-IA-MONTHLY-UNIT-VAL                                                                           *     #CREF-MONTHLY-TOTALS.#CREF-IA-MONTHLY-UNITS 
    *     #CREF-MONTHLY-TOTALS.#CREF-IA-RATE-MONTHLY                                                                               *     #CREF-MONTHLY-TOTALS.#CREF-PROD-MONTHLY 
    *     #CREF-MONTHLY-TOTALS.#CREF-SETTLED-MONTHLY-AMT
************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsm880 extends BLNatBase
{
    // from LocalVariables/Parameters
    private DbsField pnd_Cref_Monthly_Totals_Pnd_Cref_Da_Rate_Monthly;
    private DbsField pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Payments;
    private DbsField pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Unit_Val;
    private DbsField pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Units;
    private DbsField pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Rate_Monthly;
    private DbsField pnd_Cref_Monthly_Totals_Pnd_Cref_Prod_Monthly;
    private DbsField pnd_Cref_Monthly_Totals_Pnd_Cref_Settled_Monthly_Amt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        				

        // Parameters
        parameters = new DbsRecord();
        pnd_Cref_Monthly_Totals_Pnd_Cref_Da_Rate_Monthly = parameters.newFieldInRecord("pnd_Cref_Monthly_Totals_Pnd_Cref_Da_Rate_Monthly", "#CREF-MONTHLY-TOTALS.#CREF-DA-RATE-MONTHLY", 
            FieldType.STRING, 2);
        pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Payments = parameters.newFieldInRecord("pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Payments", "#CREF-MONTHLY-TOTALS.#CREF-IA-MONTHLY-PAYMENTS", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Unit_Val = parameters.newFieldInRecord("pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Unit_Val", "#CREF-MONTHLY-TOTALS.#CREF-IA-MONTHLY-UNIT-VAL", 
            FieldType.PACKED_DECIMAL, 12, 4);
        pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Units = parameters.newFieldInRecord("pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Units", "#CREF-MONTHLY-TOTALS.#CREF-IA-MONTHLY-UNITS", 
            FieldType.PACKED_DECIMAL, 11, 3);
        pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Rate_Monthly = parameters.newFieldInRecord("pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Rate_Monthly", "#CREF-MONTHLY-TOTALS.#CREF-IA-RATE-MONTHLY", 
            FieldType.STRING, 2);
        pnd_Cref_Monthly_Totals_Pnd_Cref_Prod_Monthly = parameters.newFieldInRecord("pnd_Cref_Monthly_Totals_Pnd_Cref_Prod_Monthly", "#CREF-MONTHLY-TOTALS.#CREF-PROD-MONTHLY", 
            FieldType.STRING, 1);
        pnd_Cref_Monthly_Totals_Pnd_Cref_Settled_Monthly_Amt = parameters.newFieldInRecord("pnd_Cref_Monthly_Totals_Pnd_Cref_Settled_Monthly_Amt", "#CREF-MONTHLY-TOTALS.#CREF-SETTLED-MONTHLY-AMT", 
            FieldType.PACKED_DECIMAL, 10, 2);
        parameters.reset();
    }

    public Adsm880() throws Exception
    {
        super("Adsm880");
        initializeFields(false);
    }

    @Override
    public void input(String localMethod, String label, IMappableProgram pgm, String message, Object mark, boolean alarm, IMapOption... iMapOptionArray) 
        throws Exception
    {
        Global.format(0, "PS=058 LS=132 ZP=OFF SG=OFF KD=ON IP=OFF");
        registerControls(pgm);

        UIINPUT:
        input("Adsm880", localMethod, label, message, mark, alarm, pgm, null, iMapOptionArray);
    }

    @Override
    public String getReportText(IMappableProgram pgm) throws Exception
    {
        registerControls(pgm);
        if (uiForm == null) { uiForm = new UiForm("Adsm880"); }
        uiForm.getFormControls().clear();
        setUiFormControls();
        return uiForm.formatReportString();
    }

    @Override
    protected void setUiFormControls() throws Exception
    {
        if (uiForm.getFormControls().size() == 0)
        {
            uiForm.setUiControl("pnd_Cref_Monthly_Totals_Pnd_Cref_Prod_Monthly", pnd_Cref_Monthly_Totals_Pnd_Cref_Prod_Monthly, true, 2, 2, 1, "WHITE", 
                true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Totals_Pnd_Cref_Da_Rate_Monthly", pnd_Cref_Monthly_Totals_Pnd_Cref_Da_Rate_Monthly, true, 2, 9, 2, "WHITE", 
                true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Rate_Monthly", pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Rate_Monthly, true, 2, 18, 2, 
                "WHITE", true, false, null, null, "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Totals_Pnd_Cref_Settled_Monthly_Amt", pnd_Cref_Monthly_Totals_Pnd_Cref_Settled_Monthly_Amt, true, 2, 
                24, 12, "WHITE", "Z,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Units", pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Units, true, 2, 37, 13, 
                "WHITE", "Z,ZZZ,ZZ9.999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Payments", pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Payments, true, 2, 
                51, 14, "WHITE", "ZZZ,ZZZ,ZZ9.99", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
            uiForm.setUiControl("pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Unit_Val", pnd_Cref_Monthly_Totals_Pnd_Cref_Ia_Monthly_Unit_Val, true, 2, 
                66, 14, "WHITE", "Z,ZZZ,ZZ9.9999", true, true, null, "0123456789+-, ", "AD=DLOFHW' '~TG=", ' ');
        }
        uiForm.setHelpRoutine("");
    }

    private void registerControls(IMappableProgram pgm) throws InvalidOperationException
    {
    }
}
