/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:55:39 PM
**        * FROM NATURAL SUBPROGRAM : Adsn605
************************************************************
**        * FILE NAME            : Adsn605.java
**        * CLASS NAME           : Adsn605
**        * INSTANCE NAME        : Adsn605
************************************************************
************************************************************************
* PROGRAM : ADSN605
* FUNCTION: IA ORIGIN CODE ASSIGNMENT
* AUTHOR  : CARLTON MASON
*********************  MAINTENANCE LOG ******************************
*
*  D A T E   PROGRAMMER     D E S C R I P T I O N
*
* 11/08/2010 E. MELNIK   INC1192514 - IA ORIGIN CODE CORRECTION.  MARKED
*                        BY EM - 110810.
* 01/04/2011 O. SOTTO    ADDED CHECKS FOR SUBPLANS ANS, ACS, TPS.
*                        SC 010411.
* 01/24/2011 O. SOTTO    PRODUCTION FIXES MARKED WITH 012411.
*                        - SUBPLAN PREFIXED GA2 AND GAM MUST HAVE
*                          ORIGIN CODE 83 EVEN IF 457B (INC1262823).
*                        - IF PLAN IS IRA201 AND FUND NOT TX OR X0,
*                          ASSIGN ORIGIN 20.
*                        - ADDED PREFIX IQ0 - IQ9 FOR IPRO.
*                        - ADDITIONAL CHANGES FOR KE7, GS1, IR4,
*                          GA6, SR1, SR2, TP7, TP8, TP9, IP7, IP8, IP9.
* 03/04/2011 O. SOTTO    PRODUCTION FIX FOR INC1303220 AND EMAIL
*                        FROM V. ENG. SC 030411.
* 03/09/2012 O. SOTTO    PRODUCTION FIX RELATED TO 467B AND GS1
*                        SUBPLANS.  SC 030912.
* 07/06/2012 O. SOTTO    FIXED 457B LOGIC.  SC 070612.
* 12/17/2012 O. SOTTO    TNG CHANGES. SC TNG0613.
* 10/04/2013 E. MELNIK   CREF/REA REDESIGN CHANGES.
*                        MARKED BY EM - 100413.
* 08/08/2016 J. BREMER   ONEIRA - PLAN HARD CODE CHANGES. SC 080816
* 12/01/2016 J. BREMER   ONEIRA - ADD PLANS IRA111/222/888 SC 120116
* 12/01/2016 E. MELNIK   ONEIRA - MARKED BY EM - 030917.
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn605 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAdsa605 pdaAdsa605;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I;
    private DbsField pnd_Sub_Plan_Prefix;

    private DbsGroup pnd_Sub_Plan_Prefix__R_Field_1;
    private DbsField pnd_Sub_Plan_Prefix_Pnd_Sub_Plan_Prefix_2;
    private DbsField pnd_Sub_Plan_Prefix_Pnd_Sub_Plan_Prefix_1;
    private DbsField pnd_457b;
    private DbsField pnd_Stable_Value;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaAdsa605 = new PdaAdsa605(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Sub_Plan_Prefix = localVariables.newFieldInRecord("pnd_Sub_Plan_Prefix", "#SUB-PLAN-PREFIX", FieldType.STRING, 3);

        pnd_Sub_Plan_Prefix__R_Field_1 = localVariables.newGroupInRecord("pnd_Sub_Plan_Prefix__R_Field_1", "REDEFINE", pnd_Sub_Plan_Prefix);
        pnd_Sub_Plan_Prefix_Pnd_Sub_Plan_Prefix_2 = pnd_Sub_Plan_Prefix__R_Field_1.newFieldInGroup("pnd_Sub_Plan_Prefix_Pnd_Sub_Plan_Prefix_2", "#SUB-PLAN-PREFIX-2", 
            FieldType.STRING, 2);
        pnd_Sub_Plan_Prefix_Pnd_Sub_Plan_Prefix_1 = pnd_Sub_Plan_Prefix__R_Field_1.newFieldInGroup("pnd_Sub_Plan_Prefix_Pnd_Sub_Plan_Prefix_1", "#SUB-PLAN-PREFIX-1", 
            FieldType.STRING, 1);
        pnd_457b = localVariables.newFieldInRecord("pnd_457b", "#457B", FieldType.BOOLEAN, 1);
        pnd_Stable_Value = localVariables.newFieldInRecord("pnd_Stable_Value", "#STABLE-VALUE", FieldType.STRING, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Stable_Value.setInitialValue("TSV0");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn605() throws Exception
    {
        super("Adsn605");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        getReports().write(0, "Executing",Global.getPROGRAM());                                                                                                           //Natural: WRITE 'Executing' *PROGRAM
        if (Global.isEscape()) return;
        //*                                                 /* 030912 START
        //*  457B PRIVATE
        if (condition((! ((pdaAdsa605.getPnd_Adsa605_Pnd_Adp_Irc_Cde().equals("17") && (pdaAdsa605.getPnd_Adsa605_Pnd_Sub_Plan_Nbr().getSubstring(1,3).equals("GA2")      //Natural: IF ( #ADP-IRC-CDE = '17' AND NOT SUBSTRING ( #SUB-PLAN-NBR,1,3 ) = 'GA2' OR = 'GAM' ) OR #SUB-PLAN-NBR = MASK ( 'GA1' )
            || pdaAdsa605.getPnd_Adsa605_Pnd_Sub_Plan_Nbr().getSubstring(1,3).equals("GAM")))) || DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Sub_Plan_Nbr(),
            "'GA1'"))))
        {
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(50);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 50
            pnd_457b.setValue(true);                                                                                                                                      //Natural: ASSIGN #457B := TRUE
            //*  030912 END
        }                                                                                                                                                                 //Natural: END-IF
        //*  COMMENTED-OUT THE FOLLOWING AND PUT IT ABOVE - 030912                                                                                                        //Natural: DECIDE FOR FIRST CONDITION
        //*  WHEN (#ADP-IRC-CDE = '17'
        //*      AND NOT SUBSTRING(#SUB-PLAN-NBR,1,3) = 'GA2' OR = 'GAM')
        //*      OR #SUB-PLAN-NBR = MASK ('GA1')
        //*    #ORIGIN-CODE := 50
        short decideConditionsMet72 = 0;                                                                                                                                  //Natural: WHEN #TIAA-ORGN-CDE
        if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Tiaa_Orgn_Cde().getBoolean()))
        {
            decideConditionsMet72++;
                                                                                                                                                                          //Natural: PERFORM GET-TIAA-ORIGIN-CODE
            sub_Get_Tiaa_Origin_Code();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #ROTH-TIAA-ORGN-CDE
        else if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Tiaa_Orgn_Cde().getBoolean()))
        {
            decideConditionsMet72++;
                                                                                                                                                                          //Natural: PERFORM GET-ROTH-TIAA-ORIGIN-CODE
            sub_Get_Roth_Tiaa_Origin_Code();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #CREF-ORGN-CDE
        else if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Cref_Orgn_Cde().getBoolean()))
        {
            decideConditionsMet72++;
                                                                                                                                                                          //Natural: PERFORM GET-CREF-ORIGIN-CODE
            sub_Get_Cref_Origin_Code();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #ROTH-CREF-ORGN-CDE
        else if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Cref_Orgn_Cde().getBoolean()))
        {
            decideConditionsMet72++;
                                                                                                                                                                          //Natural: PERFORM GET-ROTH-CREF-ORIGIN-CODE
            sub_Get_Roth_Cref_Origin_Code();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-TIAA-ORIGIN-CODE
        //*  VALUE 'GS1'
        //*    #ORIGIN-CODE :=09
        //* * #SUB-PLAN-PREFIX = 'GA3' OR = 'GA4' OR= 'GA5' OR = 'GR1'
        //* *IF #ACCT-CODE(*) = 'Y'
        //* *    OR #T395-FUND-ID(*) = 'SA'
        //* *  #ORIGIN-CODE := 01
        //* *ELSE
        //* *  #ORIGIN-CODE := 02
        //* *END-IF
        //* *ESCAPE ROUTINE
        //* *D-IF
        //*   SEP IRA
        //*   ROTH IRA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ROTH-TIAA-ORIGIN-CODE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CREF-ORIGIN-CODE
        //*  VALUE 'GS1'
        //*    #ORIGIN-CODE :=09
        //* * #SUB-PLAN-PREFIX = 'GA3' OR = 'GA4' OR= 'GA5' OR = 'GR1'
        //* *IF #ACCT-CODE(*) = 'Y'
        //* *    OR #T395-FUND-ID(*) = 'SA'
        //* *  #ORIGIN-CODE := 01
        //* *ELSE
        //* *  #ORIGIN-CODE := 02
        //* *END-IF
        //* *ESCAPE ROUTINE
        //* *D-IF
        //*      OR SUBSTR(ADSA600.ADI-DA-CREF-NBRS(#I),1,3) = 'L99'
        //*    OR SUBSTR(ADSA600.ADI-DA-CREF-NBRS(#I),1,3) = 'L90' THRU 'L98'
        //*   SEP IRA
        //*   ROTH IRA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ROTH-CREF-ORIGIN-CODE
        getReports().write(0, "Origin code ",pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code());                                                                                //Natural: WRITE 'Origin code ' #ORIGIN-CODE
        if (Global.isEscape()) return;
    }
    private void sub_Get_Tiaa_Origin_Code() throws Exception                                                                                                              //Natural: GET-TIAA-ORIGIN-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Sub_Plan_Prefix.setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Sub_Plan_Nbr());                                                                                       //Natural: MOVE #SUB-PLAN-NBR TO #SUB-PLAN-PREFIX
        //*  012411 END
        //*  KEOGH
        //*  RS0
        //*  ATRA
        //*  TPA-ATRA
        //*  IPRO-ATRA
        short decideConditionsMet172 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SUB-PLAN-PREFIX;//Natural: VALUE 'GA6','SR1','SR2'
        if (condition((pnd_Sub_Plan_Prefix.equals("GA6") || pnd_Sub_Plan_Prefix.equals("SR1") || pnd_Sub_Plan_Prefix.equals("SR2"))))
        {
            decideConditionsMet172++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(1);                                                                                                      //Natural: ASSIGN #ORIGIN-CODE := 01
        }                                                                                                                                                                 //Natural: VALUE 'KE1'
        else if (condition((pnd_Sub_Plan_Prefix.equals("KE1"))))
        {
            decideConditionsMet172++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(69);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 69
        }                                                                                                                                                                 //Natural: VALUE 'AA1'
        else if (condition((pnd_Sub_Plan_Prefix.equals("AA1"))))
        {
            decideConditionsMet172++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(42);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 42
        }                                                                                                                                                                 //Natural: VALUE 'TPA'
        else if (condition((pnd_Sub_Plan_Prefix.equals("TPA"))))
        {
            decideConditionsMet172++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(63);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 63
        }                                                                                                                                                                 //Natural: VALUE 'IPA'
        else if (condition((pnd_Sub_Plan_Prefix.equals("IPA"))))
        {
            decideConditionsMet172++;
            //*  030411
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Lob_Ind().getValue("*").equals("1")))                                                                      //Natural: IF #ORIGIN-LOB-IND ( * ) = '1'
            {
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(62);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 62
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(64);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 64
                //*  ATRA RA-ILB
                //*  SSRA
                //*  ATRA RA-OREGON
                //*  TPA-ATRA OREGON
                //*  IPRO-ATRA OREGON
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'AA2'
        else if (condition((pnd_Sub_Plan_Prefix.equals("AA2"))))
        {
            decideConditionsMet172++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(39);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 39
        }                                                                                                                                                                 //Natural: VALUE 'SS1'
        else if (condition((pnd_Sub_Plan_Prefix.equals("SS1"))))
        {
            decideConditionsMet172++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(41);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 41
        }                                                                                                                                                                 //Natural: VALUE 'AA3'
        else if (condition((pnd_Sub_Plan_Prefix.equals("AA3"))))
        {
            decideConditionsMet172++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(47);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 47
        }                                                                                                                                                                 //Natural: VALUE 'TPB'
        else if (condition((pnd_Sub_Plan_Prefix.equals("TPB"))))
        {
            decideConditionsMet172++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(67);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 67
        }                                                                                                                                                                 //Natural: VALUE 'IPB'
        else if (condition((pnd_Sub_Plan_Prefix.equals("IPB"))))
        {
            decideConditionsMet172++;
            //*  030411
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Lob_Ind().getValue("*").equals("1")))                                                                      //Natural: IF #ORIGIN-LOB-IND ( * ) = '1'
            {
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(62);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 62
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(68);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 68
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet172 > 0))
        {
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().notEquals(getZero())))                                                                              //Natural: IF #ORIGIN-CODE NE 0
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  RA/GRA
        //*  030912
        //*  030912
        if (condition(pnd_Sub_Plan_Prefix_Pnd_Sub_Plan_Prefix_2.equals("RA") || pnd_Sub_Plan_Prefix_Pnd_Sub_Plan_Prefix_2.equals("GR") || pnd_457b.getBoolean()           //Natural: IF #SUB-PLAN-PREFIX-2 = 'RA' OR = 'GR' OR #457B OR #SUB-PLAN-PREFIX = 'GS1'
            || pnd_Sub_Plan_Prefix.equals("GS1")))
        {
            //*  STABLE
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Acct_Code().getValue("*").equals("Y")))                                                                           //Natural: IF #ACCT-CODE ( * ) = 'Y'
            {
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(29);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 29
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
                //*  CM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    IF #T395-FUND-ID(*) = 'SA'               /* EM - 100413 START
                //*  EM - 100413 END
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Invstmnt_Grpng_Id().getValue("*").equals(pnd_Stable_Value)))                                                  //Natural: IF #INVSTMNT-GRPNG-ID ( * ) = #STABLE-VALUE
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(31);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 31
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  070312
            if (condition(pnd_457b.getBoolean()))                                                                                                                         //Natural: IF #457B
            {
                //*  070312
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
                //*  070312
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  030912 START
        //*  DEFAULT VALUE
        if (condition(pnd_Sub_Plan_Prefix.equals("GS1")))                                                                                                                 //Natural: IF #SUB-PLAN-PREFIX = 'GS1'
        {
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(9);                                                                                                      //Natural: ASSIGN #ORIGIN-CODE := 09
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED THE FOLLOWING PER ANTON's spreadsheet
        if (condition(pnd_Sub_Plan_Prefix.equals("GA3") || pnd_Sub_Plan_Prefix.equals("GA4") || pnd_Sub_Plan_Prefix.equals("GA5") || pnd_Sub_Plan_Prefix.equals("RA5")    //Natural: IF #SUB-PLAN-PREFIX = 'GA3' OR = 'GA4' OR = 'GA5' OR = 'RA5' OR = 'RA7' OR = 'RL1' OR = 'GR1'
            || pnd_Sub_Plan_Prefix.equals("RA7") || pnd_Sub_Plan_Prefix.equals("RL1") || pnd_Sub_Plan_Prefix.equals("GR1")))
        {
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(2);                                                                                                      //Natural: ASSIGN #ORIGIN-CODE := 02
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(2);                                                                                                          //Natural: ASSIGN #ORIGIN-CODE := 02
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            short decideConditionsMet254 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #CONTRACT-NBRS ( #I ) = MASK ( 'C8' ) OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'K83' THRU 'K89' OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'L99'
            if (condition(((DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I),"'C8'") || (pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("K83") 
                >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("K89") <= 0)) || pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,
                3).equals("L99"))))
            {
                decideConditionsMet254++;
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(8);                                                                                                  //Natural: ASSIGN #ORIGIN-CODE := 08
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN #CONTRACT-NBRS ( #I ) = MASK ( 'K' ) OR = MASK ( '2' ) OR = MASK ( '3' ) OR = MASK ( 'N' ) OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'L90' THRU 'L98'
            if (condition(((((DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I),"'K'") || DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I),"'2'")) 
                || DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I),"'3'")) || DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I),"'N'")) 
                || (pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("L90") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("L98") 
                <= 0))))
            {
                decideConditionsMet254++;
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().equals(2)))                                                                                     //Natural: IF #ORIGIN-CODE = 02
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(1);                                                                                              //Natural: ASSIGN #ORIGIN-CODE := 01
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #CONTRACT-NBRS ( #I ) = MASK ( 'K9' ) OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,2 ) = 'N7' THRU 'N9'
            if (condition((DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I),"'K9'") || (pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,2).compareTo("N7") 
                >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,2).compareTo("N9") <= 0))))
            {
                decideConditionsMet254++;
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().equals(1) || pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().equals(2)))                            //Natural: IF #ORIGIN-CODE = 01 OR = 02
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(19);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 19
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #CONTRACT-NBRS ( #I ) = MASK ( 'N6' )
            if (condition(DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I),"'N6'")))
            {
                decideConditionsMet254++;
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(19);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 19
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'N20' THRU 'N59'
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("N20") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("N59") 
                <= 0))
            {
                decideConditionsMet254++;
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(20);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 20
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'K81' THRU 'K82' OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,2 ) = 'K5' THRU 'K7' OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,2 ) = 'L0' THRU 'L8'
            if (condition((((pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("K81") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("K82") 
                <= 0) || (pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,2).compareTo("K5") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,2).compareTo("K7") 
                <= 0)) || (pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,2).compareTo("L0") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,2).compareTo("L8") 
                <= 0))))
            {
                decideConditionsMet254++;
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().equals(1) || pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().equals(2) || pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().equals(19))) //Natural: IF #ORIGIN-CODE = 01 OR = 02 OR = 19
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(9);                                                                                              //Natural: ASSIGN #ORIGIN-CODE := 09
                }                                                                                                                                                         //Natural: END-IF
                //*  08312005 MN
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'F00' THRU 'F09'
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("F00") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("F09") 
                <= 0))
            {
                decideConditionsMet254++;
                //*  EM - 012309 START
                //*  RC STABLE
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Acct_Code().getValue("*").equals("Y")))                                                                       //Natural: IF #ACCT-CODE ( * ) = 'Y'
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(28);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 28
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*        IF #T395-FUND-ID (*) = 'SA'               /* EM - 100413 START
                    //*  EM - 100413 END
                    //*  CM
                    if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Invstmnt_Grpng_Id().getValue("*").equals(pnd_Stable_Value)))                                              //Natural: IF #INVSTMNT-GRPNG-ID ( * ) = #STABLE-VALUE
                    {
                        pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(30);                                                                                         //Natural: ASSIGN #ORIGIN-CODE := 30
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(48);                                                                                         //Natural: ASSIGN #ORIGIN-CODE := 48
                    }                                                                                                                                                     //Natural: END-IF
                    //*  EM - 012309 END
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'F50' THRU 'F59'
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("F50") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("F59") 
                <= 0))
            {
                decideConditionsMet254++;
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(49);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 49
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'ID0' THRU 'ID9' OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'IE0' THRU 'IE9' OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'IF0' THRU 'IF9' OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'IG0' THRU 'IG9' OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'IH0' THRU 'IH9'
            if (condition((((((pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("ID0") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("ID9") 
                <= 0) || (pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IE0") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IE9") 
                <= 0)) || (pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IF0") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IF9") 
                <= 0)) || (pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IG0") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IG9") 
                <= 0)) || (pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IH0") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IH9") 
                <= 0))))
            {
                decideConditionsMet254++;
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(60);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 60
                //*  012411
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'IP0' THRU 'IP9' OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'IQ0' THRU 'IQ9'
            if (condition(((pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IP0") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IP9") 
                <= 0) || (pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IQ0") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IQ9") 
                <= 0))))
            {
                decideConditionsMet254++;
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Lob_Ind().getValue(pnd_I).equals("1")))                                                                //Natural: IF #ORIGIN-LOB-IND ( #I ) = '1'
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(62);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 62
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(61);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 61
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //*  NON-ROTH ICAP IO
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'II0' THRU 'II9'
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("II0") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("II9") 
                <= 0))
            {
                decideConditionsMet254++;
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(66);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 66
                //*  NON-ROTH ICAP TPA
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'IT0' THRU 'IT9'
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IT0") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IT9") 
                <= 0))
            {
                decideConditionsMet254++;
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(65);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 65
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet254 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CM-START
        //*  080816   120116
        //*  080816  120116
        //*  080816  120116
        short decideConditionsMet327 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #PLAN-NBR;//Natural: VALUE 'IRA101', 'IRA001','IRA011','IRA111'
        if (condition((pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA101") || pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA001") || pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA011") 
            || pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA111"))))
        {
            decideConditionsMet327++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(11);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 11
        }                                                                                                                                                                 //Natural: VALUE 'IRA201','IRA002','IRA022','IRA222'
        else if (condition((pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA201") || pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA002") || 
            pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA022") || pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA222"))))
        {
            decideConditionsMet327++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(12);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 12
        }                                                                                                                                                                 //Natural: VALUE 'IRA801','IRA008','IRA088','IRA888'
        else if (condition((pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA801") || pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA008") || 
            pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA088") || pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA888"))))
        {
            decideConditionsMet327++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(13);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 13
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet327 > 0))
        {
            //*  TNG0613 START
            //*    IF NOT (#T395-FUND-ID(*) = 'TX' OR = 'X0') /* EM - 110810
            //*  EM - 030917
            if (condition(! (DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(1),"'NE'") || DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(1),"'NA'")  //Natural: IF NOT ( #CONTRACT-NBRS ( 1 ) = MASK ( 'NE' ) OR = MASK ( 'NA' ) OR = MASK ( 'NC' ) OR = MASK ( 'NR' ) OR = MASK ( 'NS' ) OR = MASK ( 'NT' ) )
                || DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(1),"'NC'") || DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(1),"'NR'") 
                || DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(1),"'NS'") || DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(1),
                "'NT'"))))
            {
                //*  TNG0613 END
                //*  080816
                //*  120116
                //*  012411
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA201") || pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA002") ||                   //Natural: IF #PLAN-NBR = 'IRA201' OR = 'IRA002' OR = 'IRA022' OR = 'IRA222'
                    pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA022") || pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA222")))
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(20);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 20
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(19);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 19
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Sub_Plan_Prefix.setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Sub_Plan_Nbr().getSubstring(1,3));                                                                     //Natural: MOVE SUBSTRING ( #SUB-PLAN-NBR,1,3 ) TO #SUB-PLAN-PREFIX
        //*  010411 ADDED ANS AND TPS
        //*  010411 ADDED ACS
        //*  012411 ADDED IR4
        //*  012411 ADDED KE7
        //*  012411 START
        //*  NON-ROTH ICAP TPA
        //*  NON-ROTH ICAP IO    /* 012411 END
        short decideConditionsMet361 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SUB-PLAN-PREFIX;//Natural: VALUE 'IRQ'
        if (condition((pnd_Sub_Plan_Prefix.equals("IRQ"))))
        {
            decideConditionsMet361++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(79);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 79
        }                                                                                                                                                                 //Natural: VALUE 'IRS'
        else if (condition((pnd_Sub_Plan_Prefix.equals("IRS"))))
        {
            decideConditionsMet361++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(80);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 80
        }                                                                                                                                                                 //Natural: VALUE 'RAS','ANS','TPS'
        else if (condition((pnd_Sub_Plan_Prefix.equals("RAS") || pnd_Sub_Plan_Prefix.equals("ANS") || pnd_Sub_Plan_Prefix.equals("TPS"))))
        {
            decideConditionsMet361++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(81);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 81
        }                                                                                                                                                                 //Natural: VALUE 'SRS','ACS'
        else if (condition((pnd_Sub_Plan_Prefix.equals("SRS") || pnd_Sub_Plan_Prefix.equals("ACS"))))
        {
            decideConditionsMet361++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(82);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 82
        }                                                                                                                                                                 //Natural: VALUE 'GA2','GAM'
        else if (condition((pnd_Sub_Plan_Prefix.equals("GA2") || pnd_Sub_Plan_Prefix.equals("GAM"))))
        {
            decideConditionsMet361++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(83);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 83
        }                                                                                                                                                                 //Natural: VALUE 'GR9','GRM'
        else if (condition((pnd_Sub_Plan_Prefix.equals("GR9") || pnd_Sub_Plan_Prefix.equals("GRM"))))
        {
            decideConditionsMet361++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(84);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 84
        }                                                                                                                                                                 //Natural: VALUE 'GS4','GSM'
        else if (condition((pnd_Sub_Plan_Prefix.equals("GS4") || pnd_Sub_Plan_Prefix.equals("GSM"))))
        {
            decideConditionsMet361++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(85);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 85
        }                                                                                                                                                                 //Natural: VALUE 'IRM','IR4'
        else if (condition((pnd_Sub_Plan_Prefix.equals("IRM") || pnd_Sub_Plan_Prefix.equals("IR4"))))
        {
            decideConditionsMet361++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(86);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 86
        }                                                                                                                                                                 //Natural: VALUE 'KEM','KE7'
        else if (condition((pnd_Sub_Plan_Prefix.equals("KEM") || pnd_Sub_Plan_Prefix.equals("KE7"))))
        {
            decideConditionsMet361++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(87);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 87
        }                                                                                                                                                                 //Natural: VALUE 'RA8','RAM'
        else if (condition((pnd_Sub_Plan_Prefix.equals("RA8") || pnd_Sub_Plan_Prefix.equals("RAM"))))
        {
            decideConditionsMet361++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(88);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 88
        }                                                                                                                                                                 //Natural: VALUE 'SR3','SRM'
        else if (condition((pnd_Sub_Plan_Prefix.equals("SR3") || pnd_Sub_Plan_Prefix.equals("SRM"))))
        {
            decideConditionsMet361++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(89);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 89
        }                                                                                                                                                                 //Natural: VALUE 'TP7','TP8','TP9'
        else if (condition((pnd_Sub_Plan_Prefix.equals("TP7") || pnd_Sub_Plan_Prefix.equals("TP8") || pnd_Sub_Plan_Prefix.equals("TP9"))))
        {
            decideConditionsMet361++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(65);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 65
        }                                                                                                                                                                 //Natural: VALUE 'IP7','IP8','IP9'
        else if (condition((pnd_Sub_Plan_Prefix.equals("IP7") || pnd_Sub_Plan_Prefix.equals("IP8") || pnd_Sub_Plan_Prefix.equals("IP9"))))
        {
            decideConditionsMet361++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(66);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 66
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet361 > 0))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Roth_Tiaa_Origin_Code() throws Exception                                                                                                         //Natural: GET-ROTH-TIAA-ORIGIN-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  070612
        pnd_Sub_Plan_Prefix.setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Sub_Plan_Nbr().getSubstring(1,3));                                                                     //Natural: MOVE SUBSTRING ( #SUB-PLAN-NBR,1,3 ) TO #SUB-PLAN-PREFIX
        //*  030912 START
        //*  ROTH 403B STABLE
        if (condition(((pnd_457b.getBoolean() || (pnd_Sub_Plan_Prefix.equals("GS1") || pnd_Sub_Plan_Prefix.equals("GR1"))) && pdaAdsa605.getPnd_Adsa605_Pnd_Acct_Code().getValue("*").equals("Y")))) //Natural: IF ( #457B OR ( #SUB-PLAN-PREFIX = 'GS1' OR = 'GR1' ) ) AND #ACCT-CODE ( * ) = 'Y'
        {
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(24);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 24
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  030912 END
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("8")))                                                                                        //Natural: IF #ROTH-PLAN-TYPE = '8'
        {
            //*  EM - 012309 START
            //*  ROTH 403B STABLE
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Acct_Code().getValue("*").equals("Y")))                                                                           //Natural: IF #ACCT-CODE ( * ) = 'Y'
            {
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(24);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 24
                //*  ROTH 403B
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(51);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 51
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("0")))                                                                                    //Natural: IF #ROTH-PLAN-TYPE = '0'
            {
                //*  ROTH 401K STABLE
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Acct_Code().getValue("*").equals("Y")))                                                                       //Natural: IF #ACCT-CODE ( * ) = 'Y'
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(26);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 26
                    //*  ROTH 401K
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(71);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 71
                    //*  EM - 012309 END
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  030912 START
        if (condition(((pnd_Sub_Plan_Prefix.equals("GR1") || pnd_Sub_Plan_Prefix.equals("SR1")) && pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().greater(getZero()))))      //Natural: IF ( #SUB-PLAN-PREFIX = 'GR1' OR = 'SR1' ) AND #ORIGIN-CODE GT 0
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  030912 END
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            short decideConditionsMet435 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'F00' THRU 'F09'
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("F00") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("F09") 
                <= 0))
            {
                decideConditionsMet435++;
                //*  EM 012309 - START
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("8")))                                                                                //Natural: IF #ROTH-PLAN-TYPE = '8'
                {
                    //*  ROTH 403B STABLE
                    if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Acct_Code().getValue("*").equals("Y")))                                                                   //Natural: IF #ACCT-CODE ( * ) = 'Y'
                    {
                        pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(25);                                                                                         //Natural: ASSIGN #ORIGIN-CODE := 25
                        //*  ROTH 403B RC
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(57);                                                                                         //Natural: ASSIGN #ORIGIN-CODE := 57
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("0")))                                                                            //Natural: IF #ROTH-PLAN-TYPE = '0'
                    {
                        //*  ROTH 401K STABLE
                        if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Acct_Code().getValue("*").equals("Y")))                                                               //Natural: IF #ACCT-CODE ( * ) = 'Y'
                        {
                            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(27);                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 27
                            //*  ROTH 401K RC
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(77);                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 77
                            //*  EM 012309 - END
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'F50' THRU 'F59'
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("F50") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("F59") 
                <= 0))
            {
                decideConditionsMet435++;
                //*  ROTH 403B RCP
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("8")))                                                                                //Natural: IF #ROTH-PLAN-TYPE = '8'
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(58);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 58
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  ROTH 401K RCP
                    if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("0")))                                                                            //Natural: IF #ROTH-PLAN-TYPE = '0'
                    {
                        pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(78);                                                                                         //Natural: ASSIGN #ORIGIN-CODE := 78
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'ID0' THRU 'ID9' OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'IE0' THRU 'IE9' OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'IF0' THRU 'IF9' OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'IG0' THRU 'IG9' OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'IH0' THRU 'IH9'
            if (condition((((((pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("ID0") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("ID9") 
                <= 0) || (pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IE0") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IE9") 
                <= 0)) || (pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IF0") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IF9") 
                <= 0)) || (pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IG0") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IG9") 
                <= 0)) || (pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IH0") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IH9") 
                <= 0))))
            {
                decideConditionsMet435++;
                //*  ROTH 403B TPA
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("8")))                                                                                //Natural: IF #ROTH-PLAN-TYPE = '8'
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(52);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 52
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  ROTH 401K TPA
                    if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("0")))                                                                            //Natural: IF #ROTH-PLAN-TYPE = '0'
                    {
                        pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(72);                                                                                         //Natural: ASSIGN #ORIGIN-CODE := 72
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  012411
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'IP0' THRU 'IP9' OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'IQ0' THRU 'IQ9'
            if (condition(((pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IP0") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IP9") 
                <= 0) || (pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IQ0") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IQ9") 
                <= 0))))
            {
                decideConditionsMet435++;
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Lob_Ind().getValue(pnd_I).equals("1")))                                                                //Natural: IF #ORIGIN-LOB-IND ( #I ) = '1'
                {
                    //*  ROTH 403B IO FROM TPA
                    if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("8")))                                                                            //Natural: IF #ROTH-PLAN-TYPE = '8'
                    {
                        pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(54);                                                                                         //Natural: ASSIGN #ORIGIN-CODE := 54
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  ROTH 401K IO FROM TPA
                        if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("0")))                                                                        //Natural: IF #ROTH-PLAN-TYPE = '0'
                        {
                            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(74);                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 74
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  ROTH 403B IO
                    if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("8")))                                                                            //Natural: IF #ROTH-PLAN-TYPE = '8'
                    {
                        pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(53);                                                                                         //Natural: ASSIGN #ORIGIN-CODE := 53
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  ROTH 401K IO
                        if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("0")))                                                                        //Natural: IF #ROTH-PLAN-TYPE = '0'
                        {
                            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(73);                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 73
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'II0' THRU 'II9'
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("II0") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("II9") 
                <= 0))
            {
                decideConditionsMet435++;
                //*  ROTH 403B ICAP IO
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("8")))                                                                                //Natural: IF #ROTH-PLAN-TYPE = '8'
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(56);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 56
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  ROTH 401K ICAP IO
                    if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("0")))                                                                            //Natural: IF #ROTH-PLAN-TYPE = '0'
                    {
                        pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(76);                                                                                         //Natural: ASSIGN #ORIGIN-CODE := 76
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'IT0' THRU 'IT9'
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IT0") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("IT9") 
                <= 0))
            {
                decideConditionsMet435++;
                //*  ROTH 403B ICAP TPA
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("8")))                                                                                //Natural: IF #ROTH-PLAN-TYPE = '8'
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(55);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 55
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  ROTH 401K ICAP TPA
                    if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("0")))                                                                            //Natural: IF #ROTH-PLAN-TYPE = '0'
                    {
                        pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(75);                                                                                         //Natural: ASSIGN #ORIGIN-CODE := 75
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet435 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CM-START
        pnd_Sub_Plan_Prefix.setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Sub_Plan_Nbr().getSubstring(1,3));                                                                     //Natural: MOVE SUBSTRING ( #SUB-PLAN-NBR,1,3 ) TO #SUB-PLAN-PREFIX
        //*  012411 START
        //*  030912
        //*  030912
        //*  012411 END
        //*  010411 ADDED ANS AND TPS
        //*  010411 ADDED ACS
        //*  012411 ADDED IR4
        //*  012411 ADDED KE7
        //*  012411 START
        short decideConditionsMet544 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SUB-PLAN-PREFIX;//Natural: VALUE 'GS1'
        if (condition((pnd_Sub_Plan_Prefix.equals("GS1"))))
        {
            decideConditionsMet544++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(9);                                                                                                      //Natural: ASSIGN #ORIGIN-CODE := 09
        }                                                                                                                                                                 //Natural: VALUE 'GR1'
        else if (condition((pnd_Sub_Plan_Prefix.equals("GR1"))))
        {
            decideConditionsMet544++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(2);                                                                                                      //Natural: ASSIGN #ORIGIN-CODE := 02
        }                                                                                                                                                                 //Natural: VALUE 'GA6','SR1','SR2'
        else if (condition((pnd_Sub_Plan_Prefix.equals("GA6") || pnd_Sub_Plan_Prefix.equals("SR1") || pnd_Sub_Plan_Prefix.equals("SR2"))))
        {
            decideConditionsMet544++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(1);                                                                                                      //Natural: ASSIGN #ORIGIN-CODE := 01
        }                                                                                                                                                                 //Natural: VALUE 'IRQ'
        else if (condition((pnd_Sub_Plan_Prefix.equals("IRQ"))))
        {
            decideConditionsMet544++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(79);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 79
        }                                                                                                                                                                 //Natural: VALUE 'IRS'
        else if (condition((pnd_Sub_Plan_Prefix.equals("IRS"))))
        {
            decideConditionsMet544++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(80);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 80
        }                                                                                                                                                                 //Natural: VALUE 'RAS','ANS','TPS'
        else if (condition((pnd_Sub_Plan_Prefix.equals("RAS") || pnd_Sub_Plan_Prefix.equals("ANS") || pnd_Sub_Plan_Prefix.equals("TPS"))))
        {
            decideConditionsMet544++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(81);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 81
        }                                                                                                                                                                 //Natural: VALUE 'SRS','ACS'
        else if (condition((pnd_Sub_Plan_Prefix.equals("SRS") || pnd_Sub_Plan_Prefix.equals("ACS"))))
        {
            decideConditionsMet544++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(82);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 82
        }                                                                                                                                                                 //Natural: VALUE 'GA2','GAM'
        else if (condition((pnd_Sub_Plan_Prefix.equals("GA2") || pnd_Sub_Plan_Prefix.equals("GAM"))))
        {
            decideConditionsMet544++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(83);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 83
        }                                                                                                                                                                 //Natural: VALUE 'GR9','GRM'
        else if (condition((pnd_Sub_Plan_Prefix.equals("GR9") || pnd_Sub_Plan_Prefix.equals("GRM"))))
        {
            decideConditionsMet544++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(84);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 84
        }                                                                                                                                                                 //Natural: VALUE 'GS4','GSM'
        else if (condition((pnd_Sub_Plan_Prefix.equals("GS4") || pnd_Sub_Plan_Prefix.equals("GSM"))))
        {
            decideConditionsMet544++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(85);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 85
        }                                                                                                                                                                 //Natural: VALUE 'IRM','IR4'
        else if (condition((pnd_Sub_Plan_Prefix.equals("IRM") || pnd_Sub_Plan_Prefix.equals("IR4"))))
        {
            decideConditionsMet544++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(86);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 86
        }                                                                                                                                                                 //Natural: VALUE 'KEM','KE7'
        else if (condition((pnd_Sub_Plan_Prefix.equals("KEM") || pnd_Sub_Plan_Prefix.equals("KE7"))))
        {
            decideConditionsMet544++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(87);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 87
        }                                                                                                                                                                 //Natural: VALUE 'RA8','RAM'
        else if (condition((pnd_Sub_Plan_Prefix.equals("RA8") || pnd_Sub_Plan_Prefix.equals("RAM"))))
        {
            decideConditionsMet544++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(88);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 88
        }                                                                                                                                                                 //Natural: VALUE 'SR3','SRM'
        else if (condition((pnd_Sub_Plan_Prefix.equals("SR3") || pnd_Sub_Plan_Prefix.equals("SRM"))))
        {
            decideConditionsMet544++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(89);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 89
        }                                                                                                                                                                 //Natural: VALUE 'TP7','TP8','TP9'
        else if (condition((pnd_Sub_Plan_Prefix.equals("TP7") || pnd_Sub_Plan_Prefix.equals("TP8") || pnd_Sub_Plan_Prefix.equals("TP9"))))
        {
            decideConditionsMet544++;
            //*  ROTH 403B ICAP TPA
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("8")))                                                                                    //Natural: IF #ROTH-PLAN-TYPE = '8'
            {
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(55);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 55
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  ROTH 401K ICAP TPA
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("0")))                                                                                //Natural: IF #ROTH-PLAN-TYPE = '0'
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(75);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 75
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'IP7','IP8','IP9'
        else if (condition((pnd_Sub_Plan_Prefix.equals("IP7") || pnd_Sub_Plan_Prefix.equals("IP8") || pnd_Sub_Plan_Prefix.equals("IP9"))))
        {
            decideConditionsMet544++;
            //*  ROTH 403B ICAP IO
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("8")))                                                                                    //Natural: IF #ROTH-PLAN-TYPE = '8'
            {
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(56);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 56
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  ROTH 401K ICAP IO
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("0")))                                                                                //Natural: IF #ROTH-PLAN-TYPE = '0'
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(76);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 76
                }                                                                                                                                                         //Natural: END-IF
                //*  012411 END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet544 > 0))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Cref_Origin_Code() throws Exception                                                                                                              //Natural: GET-CREF-ORIGIN-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Sub_Plan_Prefix.setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Sub_Plan_Nbr());                                                                                       //Natural: MOVE #SUB-PLAN-NBR TO #SUB-PLAN-PREFIX
        //*  012411 END
        //*  KEOGH
        //*  RS0
        //*  ATRA
        //*  TPA-ATRA
        //*  IPRO-ATRA
        short decideConditionsMet610 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SUB-PLAN-PREFIX;//Natural: VALUE 'GA6','SR1','SR2'
        if (condition((pnd_Sub_Plan_Prefix.equals("GA6") || pnd_Sub_Plan_Prefix.equals("SR1") || pnd_Sub_Plan_Prefix.equals("SR2"))))
        {
            decideConditionsMet610++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(1);                                                                                                      //Natural: ASSIGN #ORIGIN-CODE := 01
        }                                                                                                                                                                 //Natural: VALUE 'KE1'
        else if (condition((pnd_Sub_Plan_Prefix.equals("KE1"))))
        {
            decideConditionsMet610++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(69);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 69
        }                                                                                                                                                                 //Natural: VALUE 'AA1'
        else if (condition((pnd_Sub_Plan_Prefix.equals("AA1"))))
        {
            decideConditionsMet610++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(42);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 42
        }                                                                                                                                                                 //Natural: VALUE 'TPA'
        else if (condition((pnd_Sub_Plan_Prefix.equals("TPA"))))
        {
            decideConditionsMet610++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(63);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 63
        }                                                                                                                                                                 //Natural: VALUE 'IPA'
        else if (condition((pnd_Sub_Plan_Prefix.equals("IPA"))))
        {
            decideConditionsMet610++;
            //*  030411
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Lob_Ind().getValue("*").equals("1")))                                                                      //Natural: IF #ORIGIN-LOB-IND ( * ) = '1'
            {
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(62);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 62
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(64);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 64
                //*  ATRA RA-ILB
                //*  SSRA
                //*  ATRA RA-OREGON
                //*  TPA-ATRA OREGON
                //*  IPRO-ATRA OREGON
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'AA2'
        else if (condition((pnd_Sub_Plan_Prefix.equals("AA2"))))
        {
            decideConditionsMet610++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(39);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 39
        }                                                                                                                                                                 //Natural: VALUE 'SS1'
        else if (condition((pnd_Sub_Plan_Prefix.equals("SS1"))))
        {
            decideConditionsMet610++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(41);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 41
        }                                                                                                                                                                 //Natural: VALUE 'AA3'
        else if (condition((pnd_Sub_Plan_Prefix.equals("AA3"))))
        {
            decideConditionsMet610++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(47);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 47
        }                                                                                                                                                                 //Natural: VALUE 'TPB'
        else if (condition((pnd_Sub_Plan_Prefix.equals("TPB"))))
        {
            decideConditionsMet610++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(67);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 67
        }                                                                                                                                                                 //Natural: VALUE 'IPB'
        else if (condition((pnd_Sub_Plan_Prefix.equals("IPB"))))
        {
            decideConditionsMet610++;
            //*  030411
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Lob_Ind().getValue("*").equals("1")))                                                                      //Natural: IF #ORIGIN-LOB-IND ( * ) = '1'
            {
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(62);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 62
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(68);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 68
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet610 > 0))
        {
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().notEquals(getZero())))                                                                              //Natural: IF #ORIGIN-CODE NE 0
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  RA/GRA
        //*  030912
        //*  030912
        if (condition(pnd_Sub_Plan_Prefix_Pnd_Sub_Plan_Prefix_2.equals("RA") || pnd_Sub_Plan_Prefix_Pnd_Sub_Plan_Prefix_2.equals("GR") || pnd_457b.getBoolean()           //Natural: IF #SUB-PLAN-PREFIX-2 = 'RA' OR = 'GR' OR #457B OR #SUB-PLAN-PREFIX = 'GS1'
            || pnd_Sub_Plan_Prefix.equals("GS1")))
        {
            //*  STABLE
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Acct_Code().getValue("*").equals("Y")))                                                                           //Natural: IF #ACCT-CODE ( * ) = 'Y'
            {
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(29);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 29
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
                //*  CM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    IF #T395-FUND-ID(*) = 'SA'                   /* EM - 100413 START
                //*  EM - 100413 END
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Invstmnt_Grpng_Id().getValue("*").equals(pnd_Stable_Value)))                                                  //Natural: IF #INVSTMNT-GRPNG-ID ( * ) = #STABLE-VALUE
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(31);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 31
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  070312
            if (condition(pnd_457b.getBoolean()))                                                                                                                         //Natural: IF #457B
            {
                //*  070312
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
                //*  070312
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  030912 START
        //*  DEFAULT VALUE
        if (condition(pnd_Sub_Plan_Prefix.equals("GS1")))                                                                                                                 //Natural: IF #SUB-PLAN-PREFIX = 'GS1'
        {
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(9);                                                                                                      //Natural: ASSIGN #ORIGIN-CODE := 09
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADDED THE FOLLOWING PER ANTON's spreadsheet
        if (condition(pnd_Sub_Plan_Prefix.equals("GA3") || pnd_Sub_Plan_Prefix.equals("GA4") || pnd_Sub_Plan_Prefix.equals("GA5") || pnd_Sub_Plan_Prefix.equals("RA5")    //Natural: IF #SUB-PLAN-PREFIX = 'GA3' OR = 'GA4' OR = 'GA5' OR = 'RA5' OR = 'RA7' OR = 'RL1' OR = 'GR1'
            || pnd_Sub_Plan_Prefix.equals("RA7") || pnd_Sub_Plan_Prefix.equals("RL1") || pnd_Sub_Plan_Prefix.equals("GR1")))
        {
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(2);                                                                                                      //Natural: ASSIGN #ORIGIN-CODE := 02
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(2);                                                                                                          //Natural: ASSIGN #ORIGIN-CODE := 02
        FOR03:                                                                                                                                                            //Natural: FOR #I 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            short decideConditionsMet692 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #CONTRACT-NBRS ( #I ) = MASK ( 'U8' ) OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'K83' THRU 'K89' OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'M99'
            if (condition(((DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I),"'U8'") || (pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("K83") 
                >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("K89") <= 0)) || pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,
                3).equals("M99"))))
            {
                decideConditionsMet692++;
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(8);                                                                                                  //Natural: ASSIGN #ORIGIN-CODE := 08
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN #CONTRACT-NBRS ( #I ) = MASK ( 'J' ) OR = MASK ( '1' ) OR = MASK ( '4' ) OR = MASK ( 'T' ) OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'M90' THRU 'M98'
            if (condition(((((DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I),"'J'") || DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I),"'1'")) 
                || DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I),"'4'")) || DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I),"'T'")) 
                || (pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("M90") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("M98") 
                <= 0))))
            {
                decideConditionsMet692++;
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().equals(2)))                                                                                     //Natural: IF #ORIGIN-CODE = 02
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(1);                                                                                              //Natural: ASSIGN #ORIGIN-CODE := 01
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #CONTRACT-NBRS ( #I ) = MASK ( 'J9' ) OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,2 ) = 'T7' THRU 'T9'
            if (condition((DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I),"'J9'") || (pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,2).compareTo("T7") 
                >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,2).compareTo("T9") <= 0))))
            {
                decideConditionsMet692++;
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().equals(1) || pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().equals(2)))                            //Natural: IF #ORIGIN-CODE = 01 OR = 02
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(19);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 19
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #CONTRACT-NBRS ( #I ) = MASK ( 'T6' )
            if (condition(DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I),"'T6'")))
            {
                decideConditionsMet692++;
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(19);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 19
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'T20' THRU 'T59'
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("T20") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("T59") 
                <= 0))
            {
                decideConditionsMet692++;
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(20);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 20
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'J81' THRU 'J82' OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,2 ) = 'J5' THRU 'J7' OR SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,2 ) = 'M0' THRU 'M8'
            if (condition((((pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("J81") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("J82") 
                <= 0) || (pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,2).compareTo("J5") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,2).compareTo("J7") 
                <= 0)) || (pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,2).compareTo("M0") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,2).compareTo("M8") 
                <= 0))))
            {
                decideConditionsMet692++;
                //*    ADSA600.ADI-DA-CREF-NBRS(#I) = MASK('M')
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().equals(1) || pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().equals(2) || pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().equals(19))) //Natural: IF #ORIGIN-CODE = 01 OR = 02 OR = 19
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(9);                                                                                              //Natural: ASSIGN #ORIGIN-CODE := 09
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'H00' THRU 'H09'
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("H00") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("H09") 
                <= 0))
            {
                decideConditionsMet692++;
                //*  EM - 012309 START
                //*  RC STABLE
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Acct_Code().getValue("*").equals("Y")))                                                                       //Natural: IF #ACCT-CODE ( * ) = 'Y'
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(28);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 28
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*        IF #T395-FUND-ID (*) = 'SA'                /* EM - 100413 START
                    //*  EM - 100413 END
                    //*  CM
                    if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Invstmnt_Grpng_Id().getValue("*").equals(pnd_Stable_Value)))                                              //Natural: IF #INVSTMNT-GRPNG-ID ( * ) = #STABLE-VALUE
                    {
                        pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(30);                                                                                         //Natural: ASSIGN #ORIGIN-CODE := 30
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(48);                                                                                         //Natural: ASSIGN #ORIGIN-CODE := 48
                        //*  EM - 012309 END
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'H50' THRU 'H59'
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("H50") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("H59") 
                <= 0))
            {
                decideConditionsMet692++;
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(49);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 49
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet692 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CM-START
        //*  080816 120116
        //*  080816  120116
        //*  080816  120116
        short decideConditionsMet745 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #PLAN-NBR;//Natural: VALUE 'IRA101', 'IRA001','IRA011','IRA111'
        if (condition((pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA101") || pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA001") || pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA011") 
            || pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA111"))))
        {
            decideConditionsMet745++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(11);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 11
        }                                                                                                                                                                 //Natural: VALUE 'IRA201','IRA002','IRA022','IRA222'
        else if (condition((pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA201") || pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA002") || 
            pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA022") || pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA222"))))
        {
            decideConditionsMet745++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(12);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 12
        }                                                                                                                                                                 //Natural: VALUE 'IRA801','IRA008','IRA088','IRA888'
        else if (condition((pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA801") || pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA008") || 
            pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA088") || pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA888"))))
        {
            decideConditionsMet745++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(13);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 13
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet745 > 0))
        {
            //*  TNG0613 START
            //*    IF NOT (#T395-FUND-ID(*) = 'TX' OR = 'X0') /* EM - 110810
            //*  EM - 030917
            if (condition(! (DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(1),"'NE'") || DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(1),"'NA'")  //Natural: IF NOT ( #CONTRACT-NBRS ( 1 ) = MASK ( 'NE' ) OR = MASK ( 'NA' ) OR = MASK ( 'NC' ) OR = MASK ( 'NR' ) OR = MASK ( 'NS' ) OR = MASK ( 'NT' ) )
                || DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(1),"'NC'") || DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(1),"'NR'") 
                || DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(1),"'NS'") || DbsUtil.maskMatches(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(1),
                "'NT'"))))
            {
                //*  TNG0613 END
                //*  080816
                //*  120116
                //*  012411
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA201") || pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA002") ||                   //Natural: IF #PLAN-NBR = 'IRA201' OR = 'IRA002' OR = 'IRA022' OR = 'IRA222'
                    pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA022") || pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().equals("IRA222")))
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(20);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 20
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(19);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 19
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Sub_Plan_Prefix.setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Sub_Plan_Nbr().getSubstring(1,3));                                                                     //Natural: MOVE SUBSTRING ( #SUB-PLAN-NBR,1,3 ) TO #SUB-PLAN-PREFIX
        //*  010411 ADDED ANS AND TPS
        //*  010411 ADDED ACS
        //*  012411 ADDED IR4
        //*  012411 ADDED KE7
        short decideConditionsMet776 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SUB-PLAN-PREFIX;//Natural: VALUE 'IRQ'
        if (condition((pnd_Sub_Plan_Prefix.equals("IRQ"))))
        {
            decideConditionsMet776++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(79);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 79
        }                                                                                                                                                                 //Natural: VALUE 'IRS'
        else if (condition((pnd_Sub_Plan_Prefix.equals("IRS"))))
        {
            decideConditionsMet776++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(80);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 80
        }                                                                                                                                                                 //Natural: VALUE 'RAS','ANS','TPS'
        else if (condition((pnd_Sub_Plan_Prefix.equals("RAS") || pnd_Sub_Plan_Prefix.equals("ANS") || pnd_Sub_Plan_Prefix.equals("TPS"))))
        {
            decideConditionsMet776++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(81);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 81
        }                                                                                                                                                                 //Natural: VALUE 'SRS','ACS'
        else if (condition((pnd_Sub_Plan_Prefix.equals("SRS") || pnd_Sub_Plan_Prefix.equals("ACS"))))
        {
            decideConditionsMet776++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(82);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 82
        }                                                                                                                                                                 //Natural: VALUE 'GA2','GAM'
        else if (condition((pnd_Sub_Plan_Prefix.equals("GA2") || pnd_Sub_Plan_Prefix.equals("GAM"))))
        {
            decideConditionsMet776++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(83);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 83
        }                                                                                                                                                                 //Natural: VALUE 'GR9','GRM'
        else if (condition((pnd_Sub_Plan_Prefix.equals("GR9") || pnd_Sub_Plan_Prefix.equals("GRM"))))
        {
            decideConditionsMet776++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(84);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 84
        }                                                                                                                                                                 //Natural: VALUE 'GS4','GSM'
        else if (condition((pnd_Sub_Plan_Prefix.equals("GS4") || pnd_Sub_Plan_Prefix.equals("GSM"))))
        {
            decideConditionsMet776++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(85);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 85
        }                                                                                                                                                                 //Natural: VALUE 'IRM','IR4'
        else if (condition((pnd_Sub_Plan_Prefix.equals("IRM") || pnd_Sub_Plan_Prefix.equals("IR4"))))
        {
            decideConditionsMet776++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(86);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 86
        }                                                                                                                                                                 //Natural: VALUE 'KEM','KE7'
        else if (condition((pnd_Sub_Plan_Prefix.equals("KEM") || pnd_Sub_Plan_Prefix.equals("KE7"))))
        {
            decideConditionsMet776++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(87);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 87
        }                                                                                                                                                                 //Natural: VALUE 'RA8','RAM'
        else if (condition((pnd_Sub_Plan_Prefix.equals("RA8") || pnd_Sub_Plan_Prefix.equals("RAM"))))
        {
            decideConditionsMet776++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(88);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 88
        }                                                                                                                                                                 //Natural: VALUE 'SR3','SRM'
        else if (condition((pnd_Sub_Plan_Prefix.equals("SR3") || pnd_Sub_Plan_Prefix.equals("SRM"))))
        {
            decideConditionsMet776++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(89);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 89
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet776 > 0))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*   CM-END
    }
    private void sub_Get_Roth_Cref_Origin_Code() throws Exception                                                                                                         //Natural: GET-ROTH-CREF-ORIGIN-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  070612
        pnd_Sub_Plan_Prefix.setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Sub_Plan_Nbr().getSubstring(1,3));                                                                     //Natural: MOVE SUBSTRING ( #SUB-PLAN-NBR,1,3 ) TO #SUB-PLAN-PREFIX
        //*  030912 START
        //*  ROTH 403B STABLE
        if (condition(((pnd_457b.getBoolean() || (pnd_Sub_Plan_Prefix.equals("GS1") || pnd_Sub_Plan_Prefix.equals("GR1"))) && pdaAdsa605.getPnd_Adsa605_Pnd_Acct_Code().getValue("*").equals("Y")))) //Natural: IF ( #457B OR ( #SUB-PLAN-PREFIX = 'GS1' OR = 'GR1' ) ) AND #ACCT-CODE ( * ) = 'Y'
        {
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(24);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 24
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  030912 END
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("8")))                                                                                        //Natural: IF #ROTH-PLAN-TYPE = '8'
        {
            //*  EM - 012309 START
            //*  ROTH 403B STABLE
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Acct_Code().getValue("*").equals("Y")))                                                                           //Natural: IF #ACCT-CODE ( * ) = 'Y'
            {
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(24);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 24
                //*  ROTH 403B
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(51);                                                                                                 //Natural: ASSIGN #ORIGIN-CODE := 51
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("0")))                                                                                    //Natural: IF #ROTH-PLAN-TYPE = '0'
            {
                //*  ROTH 401K STABLE
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Acct_Code().getValue("*").equals("Y")))                                                                       //Natural: IF #ACCT-CODE ( * ) = 'Y'
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(26);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 26
                    //*  ROTH 401K
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(71);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 71
                    //*  EM - 012309 END
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  030912 START
        if (condition(((pnd_Sub_Plan_Prefix.equals("GR1") || pnd_Sub_Plan_Prefix.equals("SR1")) && pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().greater(getZero()))))      //Natural: IF ( #SUB-PLAN-PREFIX = 'GR1' OR = 'SR1' ) AND #ORIGIN-CODE GT 0
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  030912 END
        }                                                                                                                                                                 //Natural: END-IF
        FOR04:                                                                                                                                                            //Natural: FOR #I 1 TO 12
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(12)); pnd_I.nadd(1))
        {
            short decideConditionsMet846 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'H00' THRU 'H09'
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("H00") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("H09") 
                <= 0))
            {
                decideConditionsMet846++;
                //*  EM 012309 - START
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("8")))                                                                                //Natural: IF #ROTH-PLAN-TYPE = '8'
                {
                    //*  ROTH 403B STABLE
                    if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Acct_Code().getValue("*").equals("Y")))                                                                   //Natural: IF #ACCT-CODE ( * ) = 'Y'
                    {
                        pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(25);                                                                                         //Natural: ASSIGN #ORIGIN-CODE := 25
                        //*  ROTH 403B RC
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(57);                                                                                         //Natural: ASSIGN #ORIGIN-CODE := 57
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("0")))                                                                            //Natural: IF #ROTH-PLAN-TYPE = '0'
                    {
                        //*  ROTH 401K STABLE
                        if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Acct_Code().getValue("*").equals("Y")))                                                               //Natural: IF #ACCT-CODE ( * ) = 'Y'
                        {
                            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(27);                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 27
                            //*  ROTH 401K RC
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(77);                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 77
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  EM 012309 - END
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #CONTRACT-NBRS ( #I ) ,1,3 ) = 'H50' THRU 'H59'
            if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("H50") >= 0 && pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue(pnd_I).getSubstring(1,3).compareTo("H59") 
                <= 0))
            {
                decideConditionsMet846++;
                //*  ROTH 403B RCP
                if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("8")))                                                                                //Natural: IF #ROTH-PLAN-TYPE = '8'
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(58);                                                                                             //Natural: ASSIGN #ORIGIN-CODE := 58
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  ROTH 401K RCP
                    if (condition(pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().equals("0")))                                                                            //Natural: IF #ROTH-PLAN-TYPE = '0'
                    {
                        pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(78);                                                                                         //Natural: ASSIGN #ORIGIN-CODE := 78
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet846 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Sub_Plan_Prefix.setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Sub_Plan_Nbr().getSubstring(1,3));                                                                     //Natural: MOVE SUBSTRING ( #SUB-PLAN-NBR,1,3 ) TO #SUB-PLAN-PREFIX
        //*  012411 START
        //*  030912
        //*  030912
        //*  012411 END
        //*  010411 ADDED ANS AND TPS
        //*  010411 ADDED ACS
        //*  012411 ADDED IR4
        //*  012411 ADDED KE7
        short decideConditionsMet896 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #SUB-PLAN-PREFIX;//Natural: VALUE 'GS1'
        if (condition((pnd_Sub_Plan_Prefix.equals("GS1"))))
        {
            decideConditionsMet896++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(9);                                                                                                      //Natural: ASSIGN #ORIGIN-CODE := 09
        }                                                                                                                                                                 //Natural: VALUE 'GR1'
        else if (condition((pnd_Sub_Plan_Prefix.equals("GR1"))))
        {
            decideConditionsMet896++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(2);                                                                                                      //Natural: ASSIGN #ORIGIN-CODE := 02
        }                                                                                                                                                                 //Natural: VALUE 'GA6','SR1','SR2'
        else if (condition((pnd_Sub_Plan_Prefix.equals("GA6") || pnd_Sub_Plan_Prefix.equals("SR1") || pnd_Sub_Plan_Prefix.equals("SR2"))))
        {
            decideConditionsMet896++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(1);                                                                                                      //Natural: ASSIGN #ORIGIN-CODE := 01
        }                                                                                                                                                                 //Natural: VALUE 'IRQ'
        else if (condition((pnd_Sub_Plan_Prefix.equals("IRQ"))))
        {
            decideConditionsMet896++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(79);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 79
        }                                                                                                                                                                 //Natural: VALUE 'IRS'
        else if (condition((pnd_Sub_Plan_Prefix.equals("IRS"))))
        {
            decideConditionsMet896++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(80);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 80
        }                                                                                                                                                                 //Natural: VALUE 'RAS','ANS','TPS'
        else if (condition((pnd_Sub_Plan_Prefix.equals("RAS") || pnd_Sub_Plan_Prefix.equals("ANS") || pnd_Sub_Plan_Prefix.equals("TPS"))))
        {
            decideConditionsMet896++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(81);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 81
        }                                                                                                                                                                 //Natural: VALUE 'SRS','ACS'
        else if (condition((pnd_Sub_Plan_Prefix.equals("SRS") || pnd_Sub_Plan_Prefix.equals("ACS"))))
        {
            decideConditionsMet896++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(82);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 82
        }                                                                                                                                                                 //Natural: VALUE 'GA2','GAM'
        else if (condition((pnd_Sub_Plan_Prefix.equals("GA2") || pnd_Sub_Plan_Prefix.equals("GAM"))))
        {
            decideConditionsMet896++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(83);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 83
        }                                                                                                                                                                 //Natural: VALUE 'GR9','GRM'
        else if (condition((pnd_Sub_Plan_Prefix.equals("GR9") || pnd_Sub_Plan_Prefix.equals("GRM"))))
        {
            decideConditionsMet896++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(84);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 84
        }                                                                                                                                                                 //Natural: VALUE 'GS4','GSM'
        else if (condition((pnd_Sub_Plan_Prefix.equals("GS4") || pnd_Sub_Plan_Prefix.equals("GSM"))))
        {
            decideConditionsMet896++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(85);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 85
        }                                                                                                                                                                 //Natural: VALUE 'IRM','IR4'
        else if (condition((pnd_Sub_Plan_Prefix.equals("IRM") || pnd_Sub_Plan_Prefix.equals("IR4"))))
        {
            decideConditionsMet896++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(86);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 86
        }                                                                                                                                                                 //Natural: VALUE 'KEM','KE7'
        else if (condition((pnd_Sub_Plan_Prefix.equals("KEM") || pnd_Sub_Plan_Prefix.equals("KE7"))))
        {
            decideConditionsMet896++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(87);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 87
        }                                                                                                                                                                 //Natural: VALUE 'RA8','RAM'
        else if (condition((pnd_Sub_Plan_Prefix.equals("RA8") || pnd_Sub_Plan_Prefix.equals("RAM"))))
        {
            decideConditionsMet896++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(88);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 88
        }                                                                                                                                                                 //Natural: VALUE 'SR3','SRM'
        else if (condition((pnd_Sub_Plan_Prefix.equals("SR3") || pnd_Sub_Plan_Prefix.equals("SRM"))))
        {
            decideConditionsMet896++;
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code().setValue(89);                                                                                                     //Natural: ASSIGN #ORIGIN-CODE := 89
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet896 > 0))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //
}
