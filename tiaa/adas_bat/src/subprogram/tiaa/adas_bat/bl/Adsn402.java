/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:54:06 PM
**        * FROM NATURAL SUBPROGRAM : Adsn402
************************************************************
**        * FILE NAME            : Adsn402.java
**        * CLASS NAME           : Adsn402
**        * INSTANCE NAME        : Adsn402
************************************************************
***********************************************************************
* PROGRAM  : ADSN402
* SYSTEM   : ADAS - ANNUITIZATION SUNGUARD
* TITLE    : ADAS BATCH - READ ADAS CONTROL RECORD
* GENERATED: MARCH 9, 2004
* AUTHOR   : EDWINA VILLANUEVA
* FUNCTION : THIS PROGRAM IS CLONED FROM THE ORIGINAL ADAM BATCH PROGRAM
*          : NAZN423. IT RETRIEVES BUSINESS DATES FROM ADAS CONTROL
*          : RECORD.
*********************  MAINTENANCE LOG ******************************
*
*  D A T E   PROGRAMMER     D E S C R I P T I O N
*  12/22/09  O. SOTTO  ACCESS ADAT TABLE TO CHECK IF AN OVERRIDE
*                      DATE EXISTS. THIS IS USED FOR RE-RUNNING
*                      PAN2000D IF NEEDED. SC 122209.
*********************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn402 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Cntl_Bsnss_Prev_Dte;
    private DbsField pnd_Cntl_Bsnss_Dte;
    private DbsField pnd_Cntl_Bsnss_Tmrrw_Dte;
    private DbsField pnd_Ads_Rcrd_Cde;

    private DataAccessProgramView vw_ads_Cntl_View;
    private DbsField ads_Cntl_View_Ads_Rcrd_Cde;

    private DbsGroup ads_Cntl_View_Ads_Cntl_Grp;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte;

    private DbsGroup ads_Cntl_View__R_Field_1;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte;

    private DbsGroup ads_Cntl_View__R_Field_2;
    private DbsField ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte_A;
    private DbsField pnd_Ads_Cntl_Super_De_1;

    private DbsGroup pnd_Ads_Cntl_Super_De_1__R_Field_3;
    private DbsField pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Rcrd_Typ;
    private DbsField pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Bsnss_Dt;
    private DbsField pnd_Cntl_Found;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;

    private DbsGroup naz_Table_Ddm__R_Field_4;
    private DbsField naz_Table_Ddm_Pnd_Naz_Ovrde_Dte;
    private DbsField pnd_Override;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Cntl_Bsnss_Prev_Dte = parameters.newFieldInRecord("pnd_Cntl_Bsnss_Prev_Dte", "#CNTL-BSNSS-PREV-DTE", FieldType.DATE);
        pnd_Cntl_Bsnss_Prev_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Cntl_Bsnss_Dte = parameters.newFieldInRecord("pnd_Cntl_Bsnss_Dte", "#CNTL-BSNSS-DTE", FieldType.DATE);
        pnd_Cntl_Bsnss_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Cntl_Bsnss_Tmrrw_Dte = parameters.newFieldInRecord("pnd_Cntl_Bsnss_Tmrrw_Dte", "#CNTL-BSNSS-TMRRW-DTE", FieldType.DATE);
        pnd_Cntl_Bsnss_Tmrrw_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Ads_Rcrd_Cde = parameters.newFieldInRecord("pnd_Ads_Rcrd_Cde", "#ADS-RCRD-CDE", FieldType.STRING, 2);
        pnd_Ads_Rcrd_Cde.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_ads_Cntl_View = new DataAccessProgramView(new NameInfo("vw_ads_Cntl_View", "ADS-CNTL-VIEW"), "ADS_CNTL", "ADS_CNTL");
        ads_Cntl_View_Ads_Rcrd_Cde = vw_ads_Cntl_View.getRecord().newFieldInGroup("ads_Cntl_View_Ads_Rcrd_Cde", "ADS-RCRD-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ADS_RCRD_CDE");

        ads_Cntl_View_Ads_Cntl_Grp = vw_ads_Cntl_View.getRecord().newGroupInGroup("ADS_CNTL_VIEW_ADS_CNTL_GRP", "ADS-CNTL-GRP");
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte", "ADS-CNTL-BSNSS-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_DTE");

        ads_Cntl_View__R_Field_1 = ads_Cntl_View_Ads_Cntl_Grp.newGroupInGroup("ads_Cntl_View__R_Field_1", "REDEFINE", ads_Cntl_View_Ads_Cntl_Bsnss_Dte);
        ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A = ads_Cntl_View__R_Field_1.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A", "ADS-CNTL-BSNSS-DTE-A", FieldType.STRING, 
            8);
        ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte = ads_Cntl_View_Ads_Cntl_Grp.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte", "ADS-CNTL-BSNSS-TMRRW-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "ADS_CNTL_BSNSS_TMRRW_DTE");

        ads_Cntl_View__R_Field_2 = ads_Cntl_View_Ads_Cntl_Grp.newGroupInGroup("ads_Cntl_View__R_Field_2", "REDEFINE", ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte);
        ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte_A = ads_Cntl_View__R_Field_2.newFieldInGroup("ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte_A", "ADS-CNTL-BSNSS-TMRRW-DTE-A", 
            FieldType.STRING, 8);
        registerRecord(vw_ads_Cntl_View);

        pnd_Ads_Cntl_Super_De_1 = localVariables.newFieldInRecord("pnd_Ads_Cntl_Super_De_1", "#ADS-CNTL-SUPER-DE-1", FieldType.STRING, 9);

        pnd_Ads_Cntl_Super_De_1__R_Field_3 = localVariables.newGroupInRecord("pnd_Ads_Cntl_Super_De_1__R_Field_3", "REDEFINE", pnd_Ads_Cntl_Super_De_1);
        pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Rcrd_Typ = pnd_Ads_Cntl_Super_De_1__R_Field_3.newFieldInGroup("pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Rcrd_Typ", 
            "#ADS-CNTL-RCRD-TYP", FieldType.STRING, 1);
        pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Bsnss_Dt = pnd_Ads_Cntl_Super_De_1__R_Field_3.newFieldInGroup("pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Bsnss_Dt", 
            "#ADS-CNTL-BSNSS-DT", FieldType.NUMERIC, 8);
        pnd_Cntl_Found = localVariables.newFieldInRecord("pnd_Cntl_Found", "#CNTL-FOUND", FieldType.BOOLEAN, 1);

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");

        naz_Table_Ddm__R_Field_4 = vw_naz_Table_Ddm.getRecord().newGroupInGroup("naz_Table_Ddm__R_Field_4", "REDEFINE", naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt);
        naz_Table_Ddm_Pnd_Naz_Ovrde_Dte = naz_Table_Ddm__R_Field_4.newFieldInGroup("naz_Table_Ddm_Pnd_Naz_Ovrde_Dte", "#NAZ-OVRDE-DTE", FieldType.NUMERIC, 
            8);
        registerRecord(vw_naz_Table_Ddm);

        pnd_Override = localVariables.newFieldInRecord("pnd_Override", "#OVERRIDE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Cntl_View.reset();
        vw_naz_Table_Ddm.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn402() throws Exception
    {
        super("Adsn402");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  CHECK IF AN OVERRIDE IS EXISTING              /* 122209 START
        pnd_Override.reset();                                                                                                                                             //Natural: RESET #OVERRIDE
        vw_naz_Table_Ddm.startDatabaseRead                                                                                                                                //Natural: READ NAZ-TABLE-DDM BY NAZ-TBL-SUPER2 STARTING FROM 'YNAZ060DTEOVRDE'
        (
        "RD1",
        new Wc[] { new Wc("NAZ_TBL_SUPER2", ">=", "YNAZ060DTEOVRDE", WcType.BY) },
        new Oc[] { new Oc("NAZ_TBL_SUPER2", "ASC") }
        );
        RD1:
        while (condition(vw_naz_Table_Ddm.readNextRow("RD1")))
        {
            if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.notEquals("NAZ060") || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.notEquals("DTE") || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.notEquals("OVRDE"))) //Natural: IF NAZ-TBL-RCRD-LVL1-ID NE 'NAZ060' OR NAZ-TBL-RCRD-LVL2-ID NE 'DTE' OR NAZ-TBL-RCRD-LVL3-ID NE 'OVRDE'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  DISABLE THE OVERRIDE
            if (condition(!(naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.equals("C") && naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.equals("Y"))))                                          //Natural: ACCEPT IF NAZ-TBL-RCRD-TYP-IND = 'C' AND NAZ-TBL-RCRD-ACTV-IND EQ 'Y'
            {
                continue;
            }
            pnd_Override.setValue(true);                                                                                                                                  //Natural: ASSIGN #OVERRIDE := TRUE
            naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setValue("N");                                                                                                            //Natural: ASSIGN NAZ-TBL-RCRD-ACTV-IND := 'N'
            vw_naz_Table_Ddm.updateDBRow("RD1");                                                                                                                          //Natural: UPDATE ( RD1. )
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
            //*  122209 END
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Rcrd_Typ.setValue("F");                                                                                                      //Natural: ASSIGN #ADS-CNTL-RCRD-TYP = 'F'
        pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Bsnss_Dt.reset();                                                                                                            //Natural: RESET #ADS-CNTL-BSNSS-DT
        //*  122209 START
        if (condition(pnd_Override.getBoolean()))                                                                                                                         //Natural: IF #OVERRIDE
        {
            getReports().write(0, Global.getPROGRAM());                                                                                                                   //Natural: WRITE *PROGRAM
            if (Global.isEscape()) return;
            getReports().write(0, "***************************************************");                                                                                 //Natural: WRITE '***************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "* An override date was found in ADAT table NAZ060 *");                                                                                 //Natural: WRITE '* An override date was found in ADAT table NAZ060 *'
            if (Global.isEscape()) return;
            getReports().write(0, "* Override date is",naz_Table_Ddm_Pnd_Naz_Ovrde_Dte,"                     *");                                                         //Natural: WRITE '* Override date is' #NAZ-OVRDE-DTE '                     *'
            if (Global.isEscape()) return;
            getReports().write(0, "***************************************************");                                                                                 //Natural: WRITE '***************************************************'
            if (Global.isEscape()) return;
            pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Bsnss_Dt.compute(new ComputeParameters(false, pnd_Ads_Cntl_Super_De_1_Pnd_Ads_Cntl_Bsnss_Dt), DbsField.subtract(100000000, //Natural: COMPUTE #ADS-CNTL-BSNSS-DT = 100000000 - #NAZ-OVRDE-DTE
                naz_Table_Ddm_Pnd_Naz_Ovrde_Dte));
            //*  122209 END
        }                                                                                                                                                                 //Natural: END-IF
        vw_ads_Cntl_View.startDatabaseRead                                                                                                                                //Natural: READ ( 1 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM #ADS-CNTL-SUPER-DE-1
        (
        "READ01",
        new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", pnd_Ads_Cntl_Super_De_1, WcType.BY) },
        new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
        1
        );
        READ01:
        while (condition(vw_ads_Cntl_View.readNextRow("READ01")))
        {
            pnd_Cntl_Found.setValue(true);                                                                                                                                //Natural: MOVE TRUE TO #CNTL-FOUND
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Cntl_Found.getBoolean()))                                                                                                                       //Natural: IF #CNTL-FOUND
        {
            pnd_Cntl_Bsnss_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A);                                                         //Natural: MOVE EDITED ADS-CNTL-BSNSS-DTE-A TO #CNTL-BSNSS-DTE ( EM = YYYYMMDD )
            pnd_Cntl_Bsnss_Tmrrw_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),ads_Cntl_View_Ads_Cntl_Bsnss_Tmrrw_Dte_A);                                             //Natural: MOVE EDITED ADS-CNTL-BSNSS-TMRRW-DTE-A TO #CNTL-BSNSS-TMRRW-DTE ( EM = YYYYMMDD )
            pnd_Ads_Rcrd_Cde.setValue(ads_Cntl_View_Ads_Rcrd_Cde);                                                                                                        //Natural: MOVE ADS-RCRD-CDE TO #ADS-RCRD-CDE
            vw_ads_Cntl_View.startDatabaseRead                                                                                                                            //Natural: READ ( 2 ) ADS-CNTL-VIEW BY ADS-CNTL-SUPER-DE-1 STARTING FROM #ADS-CNTL-SUPER-DE-1
            (
            "READ_2",
            new Wc[] { new Wc("ADS_CNTL_SUPER_DE_1", ">=", pnd_Ads_Cntl_Super_De_1, WcType.BY) },
            new Oc[] { new Oc("ADS_CNTL_SUPER_DE_1", "ASC") },
            2
            );
            READ_2:
            while (condition(vw_ads_Cntl_View.readNextRow("READ_2")))
            {
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            if (condition(vw_ads_Cntl_View.getAstCOUNTER().equals(2)))                                                                                                    //Natural: IF *COUNTER ( READ-2. ) EQ 2
            {
                pnd_Cntl_Bsnss_Prev_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),ads_Cntl_View_Ads_Cntl_Bsnss_Dte_A);                                                //Natural: MOVE EDITED ADS-CNTL-BSNSS-DTE-A TO #CNTL-BSNSS-PREV-DTE ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
