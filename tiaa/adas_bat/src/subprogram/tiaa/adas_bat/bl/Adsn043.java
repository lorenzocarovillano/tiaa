/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:53:21 PM
**        * FROM NATURAL SUBPROGRAM : Adsn043
************************************************************
**        * FILE NAME            : Adsn043.java
**        * CLASS NAME           : Adsn043
**        * INSTANCE NAME        : Adsn043
************************************************************
************************************************************************
* PROGRAM  : ADSN043
* SYSTEM   : ANNUITIZATION SUNGARD (ADAS)
* TITLE    : VERIFY ISSUE STATE
* GENERATED: MAR 10,04
* FUNCTION : THIS SUBPROGRAM CALLS THE CIS MODULE TO VERIFY ISSUE STATE
*          |
*
* MOD DATE   MOD BY       DESCRIPTION OF CHANGES
* --------   -------      ---------------------------------------------
* 3/10/2004  T.SHINE     CLONED NAZN043 FOR ANNUITIZATION SUNGARD (ADAS)
* 3/23/2004  VILLANUEVA  CALL CIS USING ADAS VERSION-CISN4072.ADD CNTRCT
*                        ISSUE STATE AND ISSUE DATE IN PARAMETER AREA
*                        FOR BATCH USE.
* 2/20/2008  O. SOTTO    IF RESIDENCE IS GUAM (GQ) AND POSTAGE TYPE IS
*                        U, PASS FOREIGN (F) POSTAGE TO CIS. FIX FOR
*                        INC205608. SC 022008.
* 12/16/2008  O. SOTTO   PROCESS FOREIGN RESIDENCE WITH US POSTAGE.
*                        FIX FOR INC516815.  SC 121608.
* 01/07/2009  E. MELNIK  TIAA ACCESS/STABLE RETURN ENHANCEMENTS.
*                        MARKED BY EM - 010709.
* 02/15/2017  E. CARREON PIN EXPANSION  02152017
* 11/13/2017  E. MELNIK  IA REPLATFORM PROJECT. CIS WILL NOT BE CALLED
*                        FOR APC REQUESTS.  MARKED BY EM - IA1117.
*
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn043 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAdspda_M pdaAdspda_M;
    private PdaAdsa042 pdaAdsa042;
    private PdaCisa1000 pdaCisa1000;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Rsdnce_Typ;
    private DbsField pnd_Rqst_Typ;
    private DbsField pnd_Frst_Annt_Rsdnce;
    private DbsField pnd_Frst_Annt_Ctznshp;
    private DbsField pnd_Annty_Strt_Dte;
    private DbsField pnd_Da_Tiaa_Nbr;
    private DbsField pnd_Cor_Pin_Nbr;
    private DbsField pnd_Frst_Annt_Ssn;
    private DbsField pnd_Frst_Annt_Dte_Of_Brth;
    private DbsField pnd_Org_Issue_St;
    private DbsField pnd_Rsdnce_Issue_St;
    private DbsField pnd_Apc_Rqst_Ind;
    private DbsField pnd_Ia_Cntrct_Apprvl_Ind;

    private DbsGroup pnd_Cis_Cntrct_Tbl;
    private DbsField pnd_Cis_Cntrct_Tbl_Pnd_Cntrct_Issue_State_Code;
    private DbsField pnd_Cis_Cntrct_Tbl_Pnd_Cntrct_Issue_Date;
    private DbsField pnd_Chgd_Rsdnce;
    private DbsField pnd_A;
    private DbsField pnd_Cis_Rqstr;
    private DbsField pnd_Alpha_Code;
    private DbsField pnd_Numeric_Code;
    private DbsField pnd_Cis_Verify_Result;

    private DbsGroup pnd_Cis_Verify_Result__R_Field_1;

    private DbsGroup pnd_Cis_Verify_Result_Pnd_Cis_Result;
    private DbsField pnd_Cis_Verify_Result_Pnd_Issue_State;
    private DbsField pnd_Cis_Verify_Result_Pnd_Apprvl_Ind;
    private DbsField pnd_Cis_Verify_Result_Pnd_Cis_Fill;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAdsa042 = new PdaAdsa042(localVariables);
        pdaCisa1000 = new PdaCisa1000(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaAdspda_M = new PdaAdspda_M(parameters);
        pnd_Rsdnce_Typ = parameters.newFieldInRecord("pnd_Rsdnce_Typ", "#RSDNCE-TYP", FieldType.STRING, 1);
        pnd_Rsdnce_Typ.setParameterOption(ParameterOption.ByReference);
        pnd_Rqst_Typ = parameters.newFieldInRecord("pnd_Rqst_Typ", "#RQST-TYP", FieldType.STRING, 1);
        pnd_Rqst_Typ.setParameterOption(ParameterOption.ByReference);
        pnd_Frst_Annt_Rsdnce = parameters.newFieldInRecord("pnd_Frst_Annt_Rsdnce", "#FRST-ANNT-RSDNCE", FieldType.STRING, 3);
        pnd_Frst_Annt_Rsdnce.setParameterOption(ParameterOption.ByReference);
        pnd_Frst_Annt_Ctznshp = parameters.newFieldInRecord("pnd_Frst_Annt_Ctznshp", "#FRST-ANNT-CTZNSHP", FieldType.STRING, 2);
        pnd_Frst_Annt_Ctznshp.setParameterOption(ParameterOption.ByReference);
        pnd_Annty_Strt_Dte = parameters.newFieldInRecord("pnd_Annty_Strt_Dte", "#ANNTY-STRT-DTE", FieldType.NUMERIC, 8);
        pnd_Annty_Strt_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Da_Tiaa_Nbr = parameters.newFieldArrayInRecord("pnd_Da_Tiaa_Nbr", "#DA-TIAA-NBR", FieldType.STRING, 10, new DbsArrayController(1, 6));
        pnd_Da_Tiaa_Nbr.setParameterOption(ParameterOption.ByReference);
        pnd_Cor_Pin_Nbr = parameters.newFieldInRecord("pnd_Cor_Pin_Nbr", "#COR-PIN-NBR", FieldType.NUMERIC, 12);
        pnd_Cor_Pin_Nbr.setParameterOption(ParameterOption.ByReference);
        pnd_Frst_Annt_Ssn = parameters.newFieldInRecord("pnd_Frst_Annt_Ssn", "#FRST-ANNT-SSN", FieldType.NUMERIC, 9);
        pnd_Frst_Annt_Ssn.setParameterOption(ParameterOption.ByReference);
        pnd_Frst_Annt_Dte_Of_Brth = parameters.newFieldInRecord("pnd_Frst_Annt_Dte_Of_Brth", "#FRST-ANNT-DTE-OF-BRTH", FieldType.NUMERIC, 8);
        pnd_Frst_Annt_Dte_Of_Brth.setParameterOption(ParameterOption.ByReference);
        pnd_Org_Issue_St = parameters.newFieldInRecord("pnd_Org_Issue_St", "#ORG-ISSUE-ST", FieldType.STRING, 2);
        pnd_Org_Issue_St.setParameterOption(ParameterOption.ByReference);
        pnd_Rsdnce_Issue_St = parameters.newFieldInRecord("pnd_Rsdnce_Issue_St", "#RSDNCE-ISSUE-ST", FieldType.STRING, 2);
        pnd_Rsdnce_Issue_St.setParameterOption(ParameterOption.ByReference);
        pnd_Apc_Rqst_Ind = parameters.newFieldInRecord("pnd_Apc_Rqst_Ind", "#APC-RQST-IND", FieldType.STRING, 1);
        pnd_Apc_Rqst_Ind.setParameterOption(ParameterOption.ByReference);
        pnd_Ia_Cntrct_Apprvl_Ind = parameters.newFieldArrayInRecord("pnd_Ia_Cntrct_Apprvl_Ind", "#IA-CNTRCT-APPRVL-IND", FieldType.STRING, 1, new DbsArrayController(1, 
            4));
        pnd_Ia_Cntrct_Apprvl_Ind.setParameterOption(ParameterOption.ByReference);

        pnd_Cis_Cntrct_Tbl = parameters.newGroupArrayInRecord("pnd_Cis_Cntrct_Tbl", "#CIS-CNTRCT-TBL", new DbsArrayController(1, 20));
        pnd_Cis_Cntrct_Tbl.setParameterOption(ParameterOption.ByReference);
        pnd_Cis_Cntrct_Tbl_Pnd_Cntrct_Issue_State_Code = pnd_Cis_Cntrct_Tbl.newFieldInGroup("pnd_Cis_Cntrct_Tbl_Pnd_Cntrct_Issue_State_Code", "#CNTRCT-ISSUE-STATE-CODE", 
            FieldType.STRING, 2);
        pnd_Cis_Cntrct_Tbl_Pnd_Cntrct_Issue_Date = pnd_Cis_Cntrct_Tbl.newFieldInGroup("pnd_Cis_Cntrct_Tbl_Pnd_Cntrct_Issue_Date", "#CNTRCT-ISSUE-DATE", 
            FieldType.NUMERIC, 8);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Chgd_Rsdnce = localVariables.newFieldInRecord("pnd_Chgd_Rsdnce", "#CHGD-RSDNCE", FieldType.BOOLEAN, 1);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_Cis_Rqstr = localVariables.newFieldInRecord("pnd_Cis_Rqstr", "#CIS-RQSTR", FieldType.STRING, 2);
        pnd_Alpha_Code = localVariables.newFieldInRecord("pnd_Alpha_Code", "#ALPHA-CODE", FieldType.STRING, 2);
        pnd_Numeric_Code = localVariables.newFieldInRecord("pnd_Numeric_Code", "#NUMERIC-CODE", FieldType.STRING, 2);
        pnd_Cis_Verify_Result = localVariables.newFieldArrayInRecord("pnd_Cis_Verify_Result", "#CIS-VERIFY-RESULT", FieldType.STRING, 10, new DbsArrayController(1, 
            20));

        pnd_Cis_Verify_Result__R_Field_1 = localVariables.newGroupInRecord("pnd_Cis_Verify_Result__R_Field_1", "REDEFINE", pnd_Cis_Verify_Result);

        pnd_Cis_Verify_Result_Pnd_Cis_Result = pnd_Cis_Verify_Result__R_Field_1.newGroupArrayInGroup("pnd_Cis_Verify_Result_Pnd_Cis_Result", "#CIS-RESULT", 
            new DbsArrayController(1, 20));
        pnd_Cis_Verify_Result_Pnd_Issue_State = pnd_Cis_Verify_Result_Pnd_Cis_Result.newFieldInGroup("pnd_Cis_Verify_Result_Pnd_Issue_State", "#ISSUE-STATE", 
            FieldType.STRING, 2);
        pnd_Cis_Verify_Result_Pnd_Apprvl_Ind = pnd_Cis_Verify_Result_Pnd_Cis_Result.newFieldInGroup("pnd_Cis_Verify_Result_Pnd_Apprvl_Ind", "#APPRVL-IND", 
            FieldType.STRING, 1);
        pnd_Cis_Verify_Result_Pnd_Cis_Fill = pnd_Cis_Verify_Result_Pnd_Cis_Result.newFieldInGroup("pnd_Cis_Verify_Result_Pnd_Cis_Fill", "#CIS-FILL", FieldType.STRING, 
            7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Cis_Rqstr.setInitialValue("IA");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn043() throws Exception
    {
        super("Adsn043");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  022008 START
        pnd_Org_Issue_St.reset();                                                                                                                                         //Natural: RESET #ORG-ISSUE-ST #RSDNCE-ISSUE-ST #IA-CNTRCT-APPRVL-IND ( * ) #CHGD-RSDNCE
        pnd_Rsdnce_Issue_St.reset();
        pnd_Ia_Cntrct_Apprvl_Ind.getValue("*").reset();
        pnd_Chgd_Rsdnce.reset();
        if (condition(((pnd_Frst_Annt_Rsdnce.equals("GQ") || pnd_Frst_Annt_Rsdnce.equals("GU")) && pnd_Rsdnce_Typ.equals("U"))))                                          //Natural: IF #FRST-ANNT-RSDNCE = 'GQ' OR = 'GU' AND #RSDNCE-TYP = 'U'
        {
            pnd_Rsdnce_Typ.setValue("F");                                                                                                                                 //Natural: ASSIGN #RSDNCE-TYP := 'F'
            pnd_Chgd_Rsdnce.setValue(true);                                                                                                                               //Natural: ASSIGN #CHGD-RSDNCE := TRUE
            //*  022008 END
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((DbsUtil.maskMatches(pnd_Frst_Annt_Rsdnce,"N.") && ((pnd_Frst_Annt_Rsdnce.greaterOrEqual("00 ") && pnd_Frst_Annt_Rsdnce.lessOrEqual("57 "))         //Natural: IF #FRST-ANNT-RSDNCE EQ MASK ( N. ) AND ( #FRST-ANNT-RSDNCE EQ '00 ' THRU '57 ' OR #FRST-ANNT-RSDNCE EQ '3E ' )
            || pnd_Frst_Annt_Rsdnce.equals("3E ")))))
        {
            pnd_Numeric_Code.setValue(pnd_Frst_Annt_Rsdnce);                                                                                                              //Natural: ASSIGN #NUMERIC-CODE := #FRST-ANNT-RSDNCE
            pnd_Alpha_Code.reset();                                                                                                                                       //Natural: RESET #ALPHA-CODE
                                                                                                                                                                          //Natural: PERFORM TRANSLATE-TO-ALPHA
            sub_Translate_To_Alpha();
            if (condition(Global.isEscape())) {return;}
            //*  121608 START
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Rsdnce_Typ.equals("U")))                                                                                                                    //Natural: IF #RSDNCE-TYP = 'U'
            {
                pnd_Rsdnce_Typ.setValue("F");                                                                                                                             //Natural: ASSIGN #RSDNCE-TYP := 'F'
                pnd_Chgd_Rsdnce.setValue(true);                                                                                                                           //Natural: ASSIGN #CHGD-RSDNCE := TRUE
                //*  121608 END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Alpha_Code.equals(" ")))                                                                                                                        //Natural: IF #ALPHA-CODE EQ ' '
        {
            pnd_Alpha_Code.setValue(pnd_Frst_Annt_Rsdnce);                                                                                                                //Natural: ASSIGN #ALPHA-CODE := #FRST-ANNT-RSDNCE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Rsdnce_Typ.equals("F")))                                                                                                                        //Natural: IF #RSDNCE-TYP EQ 'F'
        {
            pnd_Alpha_Code.setValue("NY");                                                                                                                                //Natural: ASSIGN #ALPHA-CODE := 'NY'
        }                                                                                                                                                                 //Natural: END-IF
        //*  EM - IA1117 START
        if (condition(pnd_Apc_Rqst_Ind.notEquals("A")))                                                                                                                   //Natural: IF #APC-RQST-IND NE 'A'
        {
                                                                                                                                                                          //Natural: PERFORM VERIFY-STATE
            sub_Verify_State();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Org_Issue_St.setValue(pnd_Cis_Cntrct_Tbl_Pnd_Cntrct_Issue_State_Code.getValue(1));                                                                        //Natural: ASSIGN #ORG-ISSUE-ST := #CNTRCT-ISSUE-STATE-CODE ( 1 )
            pnd_Rsdnce_Issue_St.setValue(pnd_Alpha_Code);                                                                                                                 //Natural: ASSIGN #RSDNCE-ISSUE-ST := #ALPHA-CODE
            //*  EM - IA1117 END
        }                                                                                                                                                                 //Natural: END-IF
        //*  022008 START
        if (condition(pnd_Chgd_Rsdnce.getBoolean()))                                                                                                                      //Natural: IF #CHGD-RSDNCE
        {
            pnd_Rsdnce_Typ.setValue("U");                                                                                                                                 //Natural: ASSIGN #RSDNCE-TYP := 'U'
            //*  022008 END
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OBTAIN-CIS-MSG
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRANSLATE-TO-ALPHA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VERIFY-STATE
        //* ***********************************************************************
        //* * IF *DEVICE = 'BATCH'
        //*  #ORG-ISSUE-ST  := #CIS-ORG-ISSUE-ST
    }
    private void sub_Obtain_Cis_Msg() throws Exception                                                                                                                    //Natural: OBTAIN-CIS-MSG
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Adsn042.class , getCurrentProcessState(), pdaAdsa042.getPnd_Cis_Msg_Table());                                                                     //Natural: CALLNAT 'ADSN042' #CIS-MSG-TABLE
        if (condition(Global.isEscape())) return;
    }
    private void sub_Translate_To_Alpha() throws Exception                                                                                                                //Natural: TRANSLATE-TO-ALPHA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *
        DbsUtil.callnat(Adsn031.class , getCurrentProcessState(), pdaAdspda_M.getMsg_Info_Sub(), pnd_Alpha_Code, pnd_Numeric_Code);                                       //Natural: CALLNAT 'ADSN031' MSG-INFO-SUB #ALPHA-CODE #NUMERIC-CODE
        if (condition(Global.isEscape())) return;
        pdaAdspda_M.getMsg_Info_Sub().reset();                                                                                                                            //Natural: RESET MSG-INFO-SUB
    }
    //*  VERIFY STATE
    private void sub_Verify_State() throws Exception                                                                                                                      //Natural: VERIFY-STATE
    {
        if (BLNatReinput.isReinput()) return;

        pdaCisa1000.getCisa1000_Pnd_Cis_Cntrct_Type().setValue(pnd_Rqst_Typ);                                                                                             //Natural: ASSIGN #CIS-CNTRCT-TYPE := #RQST-TYP
        pdaCisa1000.getCisa1000_Pnd_Cis_Residence_Issue_St().setValue(pnd_Alpha_Code);                                                                                    //Natural: ASSIGN #CIS-RESIDENCE-ISSUE-ST := #ALPHA-CODE
        pdaCisa1000.getCisa1000_Pnd_Cis_Issue_Dt().setValue(pnd_Annty_Strt_Dte);                                                                                          //Natural: ASSIGN #CIS-ISSUE-DT := #ANNTY-STRT-DTE
        pdaCisa1000.getCisa1000_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue(1,":",6).setValue(pnd_Da_Tiaa_Nbr.getValue("*"));                                                      //Natural: ASSIGN #CIS-TIAA-CNTRCT-NBR ( 1:6 ) := #DA-TIAA-NBR ( * )
        pdaCisa1000.getCisa1000_Pnd_Cis_Functions().getValue(10).setValue("YO");                                                                                          //Natural: ASSIGN #CIS-FUNCTIONS ( 10 ) := 'YO'
        pdaCisa1000.getCisa1000_Pnd_Cis_Requestor().setValue(pnd_Cis_Rqstr);                                                                                              //Natural: ASSIGN #CIS-REQUESTOR := #CIS-PRODUCT-CDE := #CIS-RQSTR
        pdaCisa1000.getCisa1000_Pnd_Cis_Product_Cde().setValue(pnd_Cis_Rqstr);
        pdaCisa1000.getCisa1000_Pnd_Cis_Pin_Number().setValue(pnd_Cor_Pin_Nbr);                                                                                           //Natural: ASSIGN #CIS-PIN-NUMBER := #COR-PIN-NBR
        pdaCisa1000.getCisa1000_Pnd_Cis_Soc_Sec_Number().setValue(pnd_Frst_Annt_Ssn);                                                                                     //Natural: ASSIGN #CIS-SOC-SEC-NUMBER := #FRST-ANNT-SSN
        pdaCisa1000.getCisa1000_Pnd_Cis_Dob().setValue(pnd_Frst_Annt_Dte_Of_Brth);                                                                                        //Natural: ASSIGN #CIS-DOB := #FRST-ANNT-DTE-OF-BRTH
        pdaCisa1000.getCisa1000_Pnd_Cis_Assign_Issue_Cntrct_Ind().setValue("I");                                                                                          //Natural: ASSIGN #CIS-ASSIGN-ISSUE-CNTRCT-IND := 'I'
        //*  CALLNAT 'CISN1000' CISA1000                 /* ESV 03.23.04
        DbsUtil.callnat(Cisn4072.class , getCurrentProcessState(), pdaCisa1000.getCisa1000(), pdaCisa1000.getPnd_Cis_Misc(), pnd_Cis_Cntrct_Tbl.getValue("*"));           //Natural: CALLNAT 'CISN4072' CISA1000 #CIS-MISC #CIS-CNTRCT-TBL ( * )
        if (condition(Global.isEscape())) return;
        if (condition(pdaCisa1000.getCisa1000_Pnd_Cis_Msg_Number().getValue("*").notEquals(" ")))                                                                         //Natural: IF #CIS-MSG-NUMBER ( * ) NE ' '
        {
            pdaAdsa042.getPnd_Cis_Msg_Table_Pnd_Cis_Msg_Nbr().getValue("*").setValue(pdaCisa1000.getCisa1000_Pnd_Cis_Msg_Number().getValue("*"));                         //Natural: ASSIGN #CIS-MSG-NBR ( * ) := #CIS-MSG-NUMBER ( * )
                                                                                                                                                                          //Natural: PERFORM OBTAIN-CIS-MSG
            sub_Obtain_Cis_Msg();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCisa1000.getCisa1000_Pnd_Cis_Msg_Number().getValue(1).notEquals(" ")))                                                                           //Natural: IF #CIS-MSG-NUMBER ( 1 ) NE ' '
        {
            pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
            if (condition(pdaAdsa042.getPnd_Cis_Msg_Table_Pnd_Cis_Msg().getValue(1).notEquals(" ")))                                                                      //Natural: IF #CIS-MSG ( 1 ) NE ' '
            {
                pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(pdaAdsa042.getPnd_Cis_Msg_Table_Pnd_Cis_Msg().getValue(1));                                            //Natural: ASSIGN MSG-INFO-SUB.##MSG := #CIS-MSG ( 1 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress(pdaCisa1000.getCisa1000_Pnd_Cis_Msg_Number().getValue(1), "return code from CIS"));   //Natural: COMPRESS #CIS-MSG-NUMBER ( 1 ) 'return code from CIS' INTO MSG-INFO-SUB.##MSG
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCisa1000.getCisa1000_Pnd_Cis_Msg_Number().getValue(10).notEquals(" ")))                                                                          //Natural: IF #CIS-MSG-NUMBER ( 10 ) NE ' '
        {
            pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
            if (condition(pdaAdsa042.getPnd_Cis_Msg_Table_Pnd_Cis_Msg().getValue(10).notEquals(" ")))                                                                     //Natural: IF #CIS-MSG ( 10 ) NE ' '
            {
                pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(pdaAdsa042.getPnd_Cis_Msg_Table_Pnd_Cis_Msg().getValue(10));                                           //Natural: ASSIGN MSG-INFO-SUB.##MSG := #CIS-MSG ( 10 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress(pdaCisa1000.getCisa1000_Pnd_Cis_Msg_Number().getValue(10), "return code from CIS"));  //Natural: COMPRESS #CIS-MSG-NUMBER ( 10 ) 'return code from CIS' INTO MSG-INFO-SUB.##MSG
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().notEquals("E")))                                                                                  //Natural: IF MSG-INFO-SUB.##RETURN-CODE NE 'E'
        {
            pnd_Cis_Verify_Result.getValue("*").setValue(pdaCisa1000.getCisa1000_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*"));                                                //Natural: ASSIGN #CIS-VERIFY-RESULT ( * ) := #CIS-TIAA-CNTRCT-NBR ( * )
            pnd_Org_Issue_St.setValue(pnd_Cis_Verify_Result_Pnd_Issue_State.getValue(1));                                                                                 //Natural: ASSIGN #ORG-ISSUE-ST := #ISSUE-STATE ( 1 )
            pnd_Rsdnce_Issue_St.setValue(pdaCisa1000.getCisa1000_Pnd_Cis_Residence_Issue_St());                                                                           //Natural: ASSIGN #RSDNCE-ISSUE-ST := #CIS-RESIDENCE-ISSUE-ST
            pnd_Ia_Cntrct_Apprvl_Ind.getValue("*").setValue(pnd_Cis_Verify_Result_Pnd_Apprvl_Ind.getValue(1,":",4));                                                      //Natural: ASSIGN #IA-CNTRCT-APPRVL-IND ( * ) := #APPRVL-IND ( 1:4 )
            //*  #IA-CNTRCT-APPRVL-IND := #CIS-CNTRCT-APPRVL-IND
            //* *  ELSE
            //* *    #RSDNCE-ISSUE-ST := #CIS-RESIDENCE-ISSUE-ST
            //* *  END-IF                             /* EM - 010709 END
        }                                                                                                                                                                 //Natural: END-IF
        //* * WRITE *PROGRAM '=' MSG-INFO-SUB.##MSG
    }

    //
}
