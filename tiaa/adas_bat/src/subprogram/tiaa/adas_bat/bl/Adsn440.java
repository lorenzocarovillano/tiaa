/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:54:16 PM
**        * FROM NATURAL SUBPROGRAM : Adsn440
************************************************************
**        * FILE NAME            : Adsn440.java
**        * CLASS NAME           : Adsn440
**        * INSTANCE NAME        : Adsn440
************************************************************
***********************************************************************
* PROGRAM  : ADSN440
* SYSTEM   : ADAS - ANNUITIZATION SUNGUARD
* TITLE    : ADAS BATCH - SUMMARIZE SETTLEMENTS
* GENERATED: MARCH 18, 2004
* AUTHOR   : EDWINA VILLANUEVA
* FUNCTION : THIS PROGRAM IS CLONED FROM THE ORIGINAL ADAM BATCH PROGRAM
*          : NAZN408. IT SUMMARIZES SETTLEMENT AMOUNTS OF ALL PROCESSED
*          : CONTRACTS ASSOCIATED WITH AN ADAS REQUEST. (PLEASE REFER
*          : TO THE ADAM VERSION OF THIS PROGRAM FOR THE ORIGINAL
*          : MAINTENANCE HISTORY)
*********************  MAINTENANCE LOG ******************************
*
*  D A T E   PROGRAMMER     D E S C R I P T I O N
*
* 05/19/2005 MARINA NACHBER - CHANGES FOR R5 : ACCORDING TO ED MELNIK
* 08/31/2005 MARINA NACHBER - CHANGES FOR R6.5: TPA & IPRO
*                             ADDING HANDLING OF A NEW AMOUNT FIELDS,
*                             GRADED AND STAND.GUARANTEED COMMUTED VALUE
* 01/22/2007 O SOTTO FIX FOR SC50851. ADC-TKR-SYMBL WAS FOUND TO
*                    CONTAIN VALUES EVEN IF ADC-ACCT-CDE IS SPACES.
*                    ADC-ACCT-CDE IS MORE ACCURATE BECAUSE IT COMES
*                    FROM THE OMNI TRANSACTION DURING THE MATCHING
*                    PROCESS. SC 012207.
* 02/20/2008 O SOTTO RECOMPILED TO USE UPDATED ADSA450 THAT WAS
*                    CHANGED TO 99 TIAA RATES OCCURS.
* 03/17/2008 E MELNIK ROTH 403/401K CHANGES. EM 031708
* 01142009 E. MELNIK   TIAA ACCESS/STABLE RETURN ENHANCEMENTS.  MARKED
*                      BY EM - 011409.
*
* 05/11/2010 C. MASON  RESTOW FOR CHANGES TO ADSA401
* 03/01/2012 O. SOTTO  RESTOWED FOR ADSA401 AND ADSA450 CHANGES.
* 06/13/2012 O. SOTTO  RBE CHANGE. SC 061312.
* 10/03/2013 E. MELNIK RECOMPILED FOR NEW ADSA401.  CREF/REA REDESIGN
*                      CHANGES.
* 03/23/2017 E. MELNIK RECOMPILED FOR PIN EXPANSION.
* 04/12/2019 E. MELNIK RESTOWED FOR UPDATED ADSA401 WITH IRC CDE.
*********************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn440 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAdsa401 pdaAdsa401;
    private PdaAdsa450 pdaAdsa450;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Constants;
    private DbsField pnd_Constants_Pnd_Cref_Cnt;
    private DbsField pnd_Constants_Pnd_Cntrct_Cnt;
    private DbsField pnd_Constants_Pnd_Tiaa;
    private DbsField pnd_Constants_Pnd_Stable;
    private DbsField pnd_Constants_Pnd_Rea;
    private DbsField pnd_Constants_Pnd_Access;

    private DbsGroup pnd_Counters;
    private DbsField pnd_Counters_Pnd_X;
    private DbsField pnd_Counters_Pnd_Y;
    private DbsField pnd_Tiaa_Ivc_Amt;
    private DbsField pnd_Cref_Ivc_Amt;
    private DbsField pnd_Total_Tiaa_Aftr_Tax_Amt;
    private DbsField pnd_Total_Aftr_Tax_Amt;
    private DbsField pnd_Ivc_Ratio;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaAdsa401 = new PdaAdsa401(parameters);
        pdaAdsa450 = new PdaAdsa450(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Constants = localVariables.newGroupInRecord("pnd_Constants", "#CONSTANTS");
        pnd_Constants_Pnd_Cref_Cnt = pnd_Constants.newFieldInGroup("pnd_Constants_Pnd_Cref_Cnt", "#CREF-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Constants_Pnd_Cntrct_Cnt = pnd_Constants.newFieldInGroup("pnd_Constants_Pnd_Cntrct_Cnt", "#CNTRCT-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Constants_Pnd_Tiaa = pnd_Constants.newFieldInGroup("pnd_Constants_Pnd_Tiaa", "#TIAA", FieldType.STRING, 1);
        pnd_Constants_Pnd_Stable = pnd_Constants.newFieldInGroup("pnd_Constants_Pnd_Stable", "#STABLE", FieldType.STRING, 1);
        pnd_Constants_Pnd_Rea = pnd_Constants.newFieldInGroup("pnd_Constants_Pnd_Rea", "#REA", FieldType.STRING, 1);
        pnd_Constants_Pnd_Access = pnd_Constants.newFieldInGroup("pnd_Constants_Pnd_Access", "#ACCESS", FieldType.STRING, 1);

        pnd_Counters = localVariables.newGroupInRecord("pnd_Counters", "#COUNTERS");
        pnd_Counters_Pnd_X = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);
        pnd_Counters_Pnd_Y = pnd_Counters.newFieldInGroup("pnd_Counters_Pnd_Y", "#Y", FieldType.PACKED_DECIMAL, 3);
        pnd_Tiaa_Ivc_Amt = localVariables.newFieldInRecord("pnd_Tiaa_Ivc_Amt", "#TIAA-IVC-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cref_Ivc_Amt = localVariables.newFieldInRecord("pnd_Cref_Ivc_Amt", "#CREF-IVC-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Total_Tiaa_Aftr_Tax_Amt = localVariables.newFieldInRecord("pnd_Total_Tiaa_Aftr_Tax_Amt", "#TOTAL-TIAA-AFTR-TAX-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Total_Aftr_Tax_Amt = localVariables.newFieldInRecord("pnd_Total_Aftr_Tax_Amt", "#TOTAL-AFTR-TAX-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ivc_Ratio = localVariables.newFieldInRecord("pnd_Ivc_Ratio", "#IVC-RATIO", FieldType.PACKED_DECIMAL, 9, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Constants_Pnd_Cref_Cnt.setInitialValue(20);
        pnd_Constants_Pnd_Cntrct_Cnt.setInitialValue(12);
        pnd_Constants_Pnd_Tiaa.setInitialValue("T");
        pnd_Constants_Pnd_Stable.setInitialValue("Y");
        pnd_Constants_Pnd_Rea.setInitialValue("R");
        pnd_Constants_Pnd_Access.setInitialValue("D");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn440() throws Exception
    {
        super("Adsn440");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
                                                                                                                                                                          //Natural: PERFORM BUILT-TIAA-TOTALS-FOR-ADSN450
        sub_Built_Tiaa_Totals_For_Adsn450();
        if (condition(Global.isEscape())) {return;}
        //*  08312005 MN
                                                                                                                                                                          //Natural: PERFORM BUILT-TIAA-GUARNT-COMMUT-TOTALS-FOR-ADSN450
        sub_Built_Tiaa_Guarnt_Commut_Totals_For_Adsn450();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BUILT-CREF-TOTALS-FOR-ADSN450
        sub_Built_Cref_Totals_For_Adsn450();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BUILT-CREF-ACCNTS-FOR-ADSN450
        sub_Built_Cref_Accnts_For_Adsn450();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BUILT-ACCOUNT-IND-FOR-ADSN450
        sub_Built_Account_Ind_For_Adsn450();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BUILT-CNTRCT-NMBR-FOR-ADSN450
        sub_Built_Cntrct_Nmbr_For_Adsn450();
        if (condition(Global.isEscape())) {return;}
        //*  EM 031708
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Roth_Rqst_Ind().notEquals("Y")))                                                                                      //Natural: IF ADP-ROTH-RQST-IND NE 'Y'
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Sttl_Ivc_Amt().greater(getZero())))                                                                               //Natural: IF ADC-STTL-IVC-AMT > 0
            {
                if (condition((pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Account_T().equals("Y") || pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Account_R().equals("Y"))  //Natural: IF ( #ACCOUNT-T = 'Y' OR #ACCOUNT-R = 'Y' ) AND ( #ACCOUNT-C = 'Y' )
                    && (pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Account_C().equals("Y"))))
                {
                                                                                                                                                                          //Natural: PERFORM PRORATE-IVC
                    sub_Prorate_Ivc();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Account_T().equals("Y") || pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Account_R().equals("Y"))) //Natural: IF #ACCOUNT-T = 'Y' OR #ACCOUNT-R = 'Y'
                    {
                        pdaAdsa401.getPnd_Adsa401_Pnd_Total_Tiaa_Ivc_Amount().nadd(pdaAdsa401.getPnd_Adsa401_Adc_Sttl_Ivc_Amt());                                         //Natural: ADD ADC-STTL-IVC-AMT TO #TOTAL-TIAA-IVC-AMOUNT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaAdsa401.getPnd_Adsa401_Pnd_Total_Cref_Ivc_Amount().nadd(pdaAdsa401.getPnd_Adsa401_Adc_Sttl_Ivc_Amt());                                         //Natural: ADD ADC-STTL-IVC-AMT TO #TOTAL-CREF-IVC-AMOUNT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  EM 031708
                                                                                                                                                                          //Natural: PERFORM MOVE-ROTH-CNTRBTN-AMTS-TO-IVC
            sub_Move_Roth_Cntrbtn_Amts_To_Ivc();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *********************** S U B R O U T I N E S ***********************
        //*  --------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILT-TIAA-TOTALS-FOR-ADSN450
        //* * 08312005 MN START
        //*  --------------------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILT-TIAA-GUARNT-COMMUT-TOTALS-FOR-ADSN450
        //* * 08312005 MN END
        //*  --------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILT-CREF-TOTALS-FOR-ADSN450
        //*  --------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILT-CREF-ACCNTS-FOR-ADSN450
        //*  --------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILT-ACCOUNT-IND-FOR-ADSN450
        //*  --------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILT-CNTRCT-NMBR-FOR-ADSN450
        //*  IF (#ACCOUNT-T = 'Y' OR #ACCOUNT-R = 'Y')
        //*   IF #ACCOUNT-C = 'Y'
        //*  --------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRORATE-IVC
        //*  --------------------------- *
        //*  EM 031708 START
        //*  --------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-ROTH-CNTRBTN-AMTS-TO-IVC
        //*  --------------------------- *
        //* * EM 031708 END
    }
    private void sub_Built_Tiaa_Totals_For_Adsn450() throws Exception                                                                                                     //Natural: BUILT-TIAA-TOTALS-FOR-ADSN450
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------- *
        pnd_Counters.reset();                                                                                                                                             //Natural: RESET #COUNTERS
        PND_PND_L0920:                                                                                                                                                    //Natural: FOR #X 1 TO #TIAA-RATE-CNT
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pdaAdsa401.getPnd_Adsa401_Pnd_Tiaa_Rate_Cnt())); pnd_Counters_Pnd_X.nadd(1))
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_Counters_Pnd_X).equals(" ")))                                                    //Natural: IF ADC-DTL-TIAA-RATE-CDE ( #X ) = ' '
            {
                if (true) break PND_PND_L0920;                                                                                                                            //Natural: ESCAPE BOTTOM ( ##L0920. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_Counters_Pnd_X).equals(getZero()) && pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_Counters_Pnd_X).equals(getZero()))) //Natural: IF ADC-DTL-TIAA-GRD-ACTL-AMT ( #X ) = 0 AND ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #X ) = 0
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            PND_PND_L1020:                                                                                                                                                //Natural: FOR #Y 1 TO #TIAA-RATE-CNT
            for (pnd_Counters_Pnd_Y.setValue(1); condition(pnd_Counters_Pnd_Y.lessOrEqual(pdaAdsa401.getPnd_Adsa401_Pnd_Tiaa_Rate_Cnt())); pnd_Counters_Pnd_Y.nadd(1))
            {
                if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde().getValue(pnd_Counters_Pnd_Y).equals(" ")))                                     //Natural: IF #TIAA-RATE-CDE ( #Y ) EQ ' '
                {
                    pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde().getValue(pnd_Counters_Pnd_Y).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_Counters_Pnd_X)); //Natural: MOVE ADC-DTL-TIAA-RATE-CDE ( #X ) TO #TIAA-RATE-CDE ( #Y )
                    //*  061312
                    pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Gic().getValue(pnd_Counters_Pnd_Y).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Contract_Id().getValue(pnd_Counters_Pnd_X)); //Natural: MOVE ADC-CONTRACT-ID ( #X ) TO #TIAA-GIC ( #Y )
                    pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Actl_Amt().getValue(pnd_Counters_Pnd_Y).nadd(pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_Counters_Pnd_X)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #X ) TO #TIAA-GRADED-ACTL-AMT ( #Y )
                    pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Actl_Amt().getValue(pnd_Counters_Pnd_Y).nadd(pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_Counters_Pnd_X)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #X ) TO #TIAA-STNDRD-ACTL-AMT ( #Y )
                    if (true) break PND_PND_L1020;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L1020. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_Counters_Pnd_X).equals(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde().getValue(pnd_Counters_Pnd_Y)))) //Natural: IF ADC-DTL-TIAA-RATE-CDE ( #X ) EQ #TIAA-RATE-CDE ( #Y )
                {
                    pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Actl_Amt().getValue(pnd_Counters_Pnd_Y).nadd(pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_Counters_Pnd_X)); //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #X ) TO #TIAA-GRADED-ACTL-AMT ( #Y )
                    pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Actl_Amt().getValue(pnd_Counters_Pnd_Y).nadd(pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_Counters_Pnd_X)); //Natural: ADD ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #X ) TO #TIAA-STNDRD-ACTL-AMT ( #Y )
                    if (true) break PND_PND_L1020;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L1020. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L0920"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L0920"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Built_Tiaa_Guarnt_Commut_Totals_For_Adsn450() throws Exception                                                                                       //Natural: BUILT-TIAA-GUARNT-COMMUT-TOTALS-FOR-ADSN450
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------------------- *
        pnd_Counters.reset();                                                                                                                                             //Natural: RESET #COUNTERS
        PND_PND_L1310:                                                                                                                                                    //Natural: FOR #X 1 TO #TIAA-RATE-CNT
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pdaAdsa401.getPnd_Adsa401_Pnd_Tiaa_Rate_Cnt())); pnd_Counters_Pnd_X.nadd(1))
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_Counters_Pnd_X).equals(" ")))                                                    //Natural: IF ADC-DTL-TIAA-RATE-CDE ( #X ) = ' '
            {
                if (true) break PND_PND_L1310;                                                                                                                            //Natural: ESCAPE BOTTOM ( ##L1310. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Grd_Amt().getValue(pnd_Counters_Pnd_X).equals(getZero()) && pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Std_Amt().getValue(pnd_Counters_Pnd_X).equals(getZero()))) //Natural: IF ADC-TPA-GUARANT-COMMUT-GRD-AMT ( #X ) = 0 AND ADC-TPA-GUARANT-COMMUT-STD-AMT ( #X ) = 0
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            PND_PND_L1410:                                                                                                                                                //Natural: FOR #Y 1 TO #TIAA-RATE-CNT
            for (pnd_Counters_Pnd_Y.setValue(1); condition(pnd_Counters_Pnd_Y.lessOrEqual(pdaAdsa401.getPnd_Adsa401_Pnd_Tiaa_Rate_Cnt())); pnd_Counters_Pnd_Y.nadd(1))
            {
                if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_Counters_Pnd_X).equals(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde().getValue(pnd_Counters_Pnd_Y)))) //Natural: IF ADC-DTL-TIAA-RATE-CDE ( #X ) EQ #TIAA-RATE-CDE ( #Y )
                {
                    pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Guar_Comm_Amt().getValue(pnd_Counters_Pnd_Y).nadd(pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Grd_Amt().getValue(pnd_Counters_Pnd_X)); //Natural: ADD ADC-TPA-GUARANT-COMMUT-GRD-AMT ( #X ) TO #TIAA-GRADED-GUAR-COMM-AMT ( #Y )
                    pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Guar_Comm_Amt().getValue(pnd_Counters_Pnd_Y).nadd(pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Std_Amt().getValue(pnd_Counters_Pnd_X)); //Natural: ADD ADC-TPA-GUARANT-COMMUT-STD-AMT ( #X ) TO #TIAA-STNDRD-GUAR-COMM-AMT ( #Y )
                    if (true) break PND_PND_L1410;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L1410. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1310"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1310"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Built_Cref_Totals_For_Adsn450() throws Exception                                                                                                     //Natural: BUILT-CREF-TOTALS-FOR-ADSN450
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------- *
        pnd_Counters.reset();                                                                                                                                             //Natural: RESET #COUNTERS
        FOR01:                                                                                                                                                            //Natural: FOR #X 1 TO #CREF-CNT
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pnd_Constants_Pnd_Cref_Cnt)); pnd_Counters_Pnd_X.nadd(1))
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cref_Rate_Cde().getValue(pnd_Counters_Pnd_X).equals(" ")))                                                   //Natural: IF ADC-ACCT-CREF-RATE-CDE ( #X ) = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            PND_PND_L1660:                                                                                                                                                //Natural: FOR #Y 1 TO #CREF-CNT
            for (pnd_Counters_Pnd_Y.setValue(1); condition(pnd_Counters_Pnd_Y.lessOrEqual(pnd_Constants_Pnd_Cref_Cnt)); pnd_Counters_Pnd_Y.nadd(1))
            {
                if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Rate_Cde().getValue(pnd_Counters_Pnd_Y).equals(" ")))                                     //Natural: IF #CREF-RATE-CDE ( #Y ) EQ ' '
                {
                    pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Rate_Cde().getValue(pnd_Counters_Pnd_Y).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cref_Rate_Cde().getValue(pnd_Counters_Pnd_X)); //Natural: MOVE ADC-ACCT-CREF-RATE-CDE ( #X ) TO #CREF-RATE-CDE ( #Y )
                    pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Graded_Mnthly_Actl_Amt().getValue(pnd_Counters_Pnd_Y).nadd(pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_Counters_Pnd_X)); //Natural: ADD ADC-GRD-MNTHLY-ACTL-AMT ( #X ) TO #CREF-GRADED-MNTHLY-ACTL-AMT ( #Y )
                    pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Stndrd_Annual_Actl_Amt().getValue(pnd_Counters_Pnd_Y).nadd(pdaAdsa401.getPnd_Adsa401_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_Counters_Pnd_X)); //Natural: ADD ADC-STNDRD-ANNL-ACTL-AMT ( #X ) TO #CREF-STNDRD-ANNUAL-ACTL-AMT ( #Y )
                    if (true) break PND_PND_L1660;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L1660. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cref_Rate_Cde().getValue(pnd_Counters_Pnd_X).equals(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Rate_Cde().getValue(pnd_Counters_Pnd_Y)))) //Natural: IF ADC-ACCT-CREF-RATE-CDE ( #X ) EQ #CREF-RATE-CDE ( #Y )
                {
                    pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Graded_Mnthly_Actl_Amt().getValue(pnd_Counters_Pnd_Y).nadd(pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_Counters_Pnd_X)); //Natural: ADD ADC-GRD-MNTHLY-ACTL-AMT ( #X ) TO #CREF-GRADED-MNTHLY-ACTL-AMT ( #Y )
                    pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Stndrd_Annual_Actl_Amt().getValue(pnd_Counters_Pnd_Y).nadd(pdaAdsa401.getPnd_Adsa401_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_Counters_Pnd_X)); //Natural: ADD ADC-STNDRD-ANNL-ACTL-AMT ( #X ) TO #CREF-STNDRD-ANNUAL-ACTL-AMT ( #Y )
                    if (true) break PND_PND_L1660;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L1660. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Built_Cref_Accnts_For_Adsn450() throws Exception                                                                                                     //Natural: BUILT-CREF-ACCNTS-FOR-ADSN450
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------- *
        pnd_Counters.reset();                                                                                                                                             //Natural: RESET #COUNTERS
        PND_PND_L1920:                                                                                                                                                    //Natural: FOR #X 1 TO #CREF-CNT
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pnd_Constants_Pnd_Cref_Cnt)); pnd_Counters_Pnd_X.nadd(1))
        {
            //*  IF ADC-TKR-SYMBL(#X) = ' ' /* 012207
            //*  012207
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).equals(" ")))                                                             //Natural: IF ADC-ACCT-CDE ( #X ) = ' '
            {
                if (true) break PND_PND_L1920;                                                                                                                            //Natural: ESCAPE BOTTOM ( ##L1920. )
            }                                                                                                                                                             //Natural: END-IF
            //*  EM - 011409
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).equals(pnd_Constants_Pnd_Tiaa) || pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).equals(pnd_Constants_Pnd_Stable))) //Natural: IF ADC-ACCT-CDE ( #X ) = #TIAA OR = #STABLE
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            PND_PND_L2010:                                                                                                                                                //Natural: FOR #Y 1 TO #CREF-CNT
            for (pnd_Counters_Pnd_Y.setValue(1); condition(pnd_Counters_Pnd_Y.lessOrEqual(pnd_Constants_Pnd_Cref_Cnt)); pnd_Counters_Pnd_Y.nadd(1))
            {
                if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Acct_Cde().getValue(pnd_Counters_Pnd_Y).equals(" ")))                                     //Natural: IF #CREF-ACCT-CDE ( #Y ) EQ ' '
                {
                    pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Acct_Cde().getValue(pnd_Counters_Pnd_Y).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X)); //Natural: MOVE ADC-ACCT-CDE ( #X ) TO #CREF-ACCT-CDE ( #Y )
                    if (true) break PND_PND_L2010;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L2010. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).equals(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Acct_Cde().getValue(pnd_Counters_Pnd_Y)))) //Natural: IF ADC-ACCT-CDE ( #X ) EQ #CREF-ACCT-CDE ( #Y )
                {
                    if (true) break PND_PND_L2010;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L2010. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PND_PND_L1920"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PND_PND_L1920"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Built_Account_Ind_For_Adsn450() throws Exception                                                                                                     //Natural: BUILT-ACCOUNT-IND-FOR-ADSN450
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------- *
        if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Account_T().equals(" ") || pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Account_R().equals(" ")        //Natural: IF #ACCOUNT-T EQ ' ' OR #ACCOUNT-R EQ ' ' OR #ACCOUNT-C EQ ' '
            || pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Account_C().equals(" ")))
        {
            FOR02:                                                                                                                                                        //Natural: FOR #X 1 TO #CREF-CNT
            for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pnd_Constants_Pnd_Cref_Cnt)); pnd_Counters_Pnd_X.nadd(1))
            {
                //*    IF ADC-TKR-SYMBL(#X) EQ ' ' /* 012207
                //*  012207
                if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).equals(" ")))                                                         //Natural: IF ADC-ACCT-CDE ( #X ) EQ ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                //*  EM - 011409
                short decideConditionsMet498 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE ADC-ACCT-CDE ( #X );//Natural: VALUE #TIAA, #STABLE
                if (condition((pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).equals(pnd_Constants_Pnd_Tiaa) || pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).equals(pnd_Constants_Pnd_Stable))))
                {
                    decideConditionsMet498++;
                    //*  EM - 011409
                    pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Account_T().setValue("Y");                                                                               //Natural: MOVE 'Y' TO #ACCOUNT-T
                }                                                                                                                                                         //Natural: VALUE #REA, #ACCESS
                else if (condition((pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).equals(pnd_Constants_Pnd_Rea) || pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).equals(pnd_Constants_Pnd_Access))))
                {
                    decideConditionsMet498++;
                    pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Account_R().setValue("Y");                                                                               //Natural: MOVE 'Y' TO #ACCOUNT-R
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Account_C().setValue("Y");                                                                               //Natural: MOVE 'Y' TO #ACCOUNT-C
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Built_Cntrct_Nmbr_For_Adsn450() throws Exception                                                                                                     //Natural: BUILT-CNTRCT-NMBR-FOR-ADSN450
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------- *
        pnd_Counters.reset();                                                                                                                                             //Natural: RESET #COUNTERS
        PND_PND_L2430:                                                                                                                                                    //Natural: FOR #X 1 TO #CNTRCT-CNT
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pnd_Constants_Pnd_Cntrct_Cnt)); pnd_Counters_Pnd_X.nadd(1))
        {
            if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_All_Tiaa_Contracts().getValue(pnd_Counters_Pnd_X).equals(" ")))                                    //Natural: IF #ALL-TIAA-CONTRACTS ( #X ) = ' '
            {
                pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_All_Tiaa_Contracts().getValue(pnd_Counters_Pnd_X).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Tiaa_Nbr());        //Natural: MOVE ADC-TIAA-NBR TO #ALL-TIAA-CONTRACTS ( #X )
                //* * 08312005 MN START
                //*  TO ADSA450
                pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Orig_Lob_Ind().getValue(pnd_Counters_Pnd_X).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Orig_Lob_Ind());          //Natural: MOVE ADC-ORIG-LOB-IND TO #ORIG-LOB-IND ( #X )
                //* * 08312005 MN END
                if (true) break PND_PND_L2430;                                                                                                                            //Natural: ESCAPE BOTTOM ( ##L2430. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        PND_PND_L2530:                                                                                                                                                    //Natural: FOR #Y 1 TO #CNTRCT-CNT
        for (pnd_Counters_Pnd_Y.setValue(1); condition(pnd_Counters_Pnd_Y.lessOrEqual(pnd_Constants_Pnd_Cntrct_Cnt)); pnd_Counters_Pnd_Y.nadd(1))
        {
            if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_All_Cref_Contracts().getValue(pnd_Counters_Pnd_Y).equals(" ")))                                    //Natural: IF #ALL-CREF-CONTRACTS ( #Y ) = ' '
            {
                pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_All_Cref_Contracts().getValue(pnd_Counters_Pnd_Y).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Cref_Nbr());        //Natural: MOVE ADC-CREF-NBR TO #ALL-CREF-CONTRACTS ( #Y )
                if (true) break PND_PND_L2530;                                                                                                                            //Natural: ESCAPE BOTTOM ( ##L2530. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* * 05192005  MN
        //*   END-IF
        //*  ELSE
        //*   FOR #Y 1 TO #CNTRCT-CNT
        //*     IF #ALL-CREF-CONTRACTS (#Y) = ' '
        //*       MOVE ADC-CREF-NBR TO #ALL-CREF-CONTRACTS (#Y)
        //*       ESCAPE BOTTOM (2620)
        //*    END-IF
        //*   END-FOR
        //*  END-IF
    }
    private void sub_Prorate_Ivc() throws Exception                                                                                                                       //Natural: PRORATE-IVC
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #X 1 TO #FUND-CNT
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pdaAdsa401.getPnd_Adsa401_Pnd_Fund_Cnt())); pnd_Counters_Pnd_X.nadd(1))
        {
            //*  IF ADC-TKR-SYMBL(#X) EQ ' ' /* 012207
            //*  012207
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).equals(" ")))                                                             //Natural: IF ADC-ACCT-CDE ( #X ) EQ ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  IF ADC-TKR-SYMBL(#X) = 'TIAA#' OR = 'TREA#' /* 012207
            //*  012207
            //*  EM - 011409
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).equals(pnd_Constants_Pnd_Tiaa) || pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).equals(pnd_Constants_Pnd_Rea)  //Natural: IF ADC-ACCT-CDE ( #X ) EQ #TIAA OR = #REA OR = #STABLE OR = #ACCESS
                || pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).equals(pnd_Constants_Pnd_Stable) || pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).equals(pnd_Constants_Pnd_Access)))
            {
                pnd_Total_Tiaa_Aftr_Tax_Amt.nadd(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Aftr_Tax_Amt().getValue(pnd_Counters_Pnd_X));                                         //Natural: ADD ADC-ACCT-AFTR-TAX-AMT ( #X ) TO #TOTAL-TIAA-AFTR-TAX-AMT
            }                                                                                                                                                             //Natural: END-IF
            pnd_Total_Aftr_Tax_Amt.nadd(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Aftr_Tax_Amt().getValue(pnd_Counters_Pnd_X));                                                  //Natural: ADD ADC-ACCT-AFTR-TAX-AMT ( #X ) TO #TOTAL-AFTR-TAX-AMT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  NO TIAA IVC
        if (condition(pnd_Total_Tiaa_Aftr_Tax_Amt.equals(getZero())))                                                                                                     //Natural: IF #TOTAL-TIAA-AFTR-TAX-AMT = 0
        {
            pdaAdsa401.getPnd_Adsa401_Pnd_Total_Cref_Ivc_Amount().nadd(pdaAdsa401.getPnd_Adsa401_Adc_Sttl_Ivc_Amt());                                                     //Natural: ADD ADC-STTL-IVC-AMT TO #TOTAL-CREF-IVC-AMOUNT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  NO CREF IVC
            if (condition(pnd_Total_Tiaa_Aftr_Tax_Amt.equals(pnd_Total_Aftr_Tax_Amt)))                                                                                    //Natural: IF #TOTAL-TIAA-AFTR-TAX-AMT = #TOTAL-AFTR-TAX-AMT
            {
                pdaAdsa401.getPnd_Adsa401_Pnd_Total_Tiaa_Ivc_Amount().nadd(pdaAdsa401.getPnd_Adsa401_Adc_Sttl_Ivc_Amt());                                                 //Natural: ADD ADC-STTL-IVC-AMT TO #TOTAL-TIAA-IVC-AMOUNT
                //*  PRORATE IVC
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ivc_Ratio.compute(new ComputeParameters(true, pnd_Ivc_Ratio), pnd_Total_Tiaa_Aftr_Tax_Amt.divide(pnd_Total_Aftr_Tax_Amt));                            //Natural: COMPUTE ROUNDED #IVC-RATIO = #TOTAL-TIAA-AFTR-TAX-AMT / #TOTAL-AFTR-TAX-AMT
                pnd_Tiaa_Ivc_Amt.compute(new ComputeParameters(true, pnd_Tiaa_Ivc_Amt), pnd_Ivc_Ratio.multiply(pdaAdsa401.getPnd_Adsa401_Adc_Sttl_Ivc_Amt()));            //Natural: COMPUTE ROUNDED #TIAA-IVC-AMT = #IVC-RATIO * ADC-STTL-IVC-AMT
                pdaAdsa401.getPnd_Adsa401_Pnd_Total_Tiaa_Ivc_Amount().nadd(pnd_Tiaa_Ivc_Amt);                                                                             //Natural: ADD #TIAA-IVC-AMT TO #TOTAL-TIAA-IVC-AMOUNT
                pnd_Cref_Ivc_Amt.compute(new ComputeParameters(false, pnd_Cref_Ivc_Amt), pdaAdsa401.getPnd_Adsa401_Adc_Sttl_Ivc_Amt().subtract(pnd_Tiaa_Ivc_Amt));        //Natural: ASSIGN #CREF-IVC-AMT := ADC-STTL-IVC-AMT - #TIAA-IVC-AMT
                pdaAdsa401.getPnd_Adsa401_Pnd_Total_Cref_Ivc_Amount().nadd(pnd_Cref_Ivc_Amt);                                                                             //Natural: ADD #CREF-IVC-AMT TO #TOTAL-CREF-IVC-AMOUNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Move_Roth_Cntrbtn_Amts_To_Ivc() throws Exception                                                                                                     //Natural: MOVE-ROTH-CNTRBTN-AMTS-TO-IVC
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR #X 1 TO #FUND-CNT
        for (pnd_Counters_Pnd_X.setValue(1); condition(pnd_Counters_Pnd_X.lessOrEqual(pdaAdsa401.getPnd_Adsa401_Pnd_Fund_Cnt())); pnd_Counters_Pnd_X.nadd(1))
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).equals(" ")))                                                             //Natural: IF ADC-ACCT-CDE ( #X ) EQ ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  EM - 011409
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).equals(pnd_Constants_Pnd_Tiaa) || pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).equals(pnd_Constants_Pnd_Rea)  //Natural: IF ADC-ACCT-CDE ( #X ) EQ #TIAA OR = #REA OR = #STABLE OR = #ACCESS
                || pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).equals(pnd_Constants_Pnd_Stable) || pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue(pnd_Counters_Pnd_X).equals(pnd_Constants_Pnd_Access)))
            {
                pdaAdsa401.getPnd_Adsa401_Pnd_Total_Tiaa_Ivc_Amount().nadd(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Aftr_Tax_Amt().getValue(pnd_Counters_Pnd_X));               //Natural: ADD ADC-ACCT-AFTR-TAX-AMT ( #X ) TO #TOTAL-TIAA-IVC-AMOUNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAdsa401.getPnd_Adsa401_Pnd_Total_Cref_Ivc_Amount().nadd(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Aftr_Tax_Amt().getValue(pnd_Counters_Pnd_X));               //Natural: ADD ADC-ACCT-AFTR-TAX-AMT ( #X ) TO #TOTAL-CREF-IVC-AMOUNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //
}
