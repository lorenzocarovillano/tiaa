/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:54:49 PM
**        * FROM NATURAL SUBPROGRAM : Adsn600a
************************************************************
**        * FILE NAME            : Adsn600a.java
**        * CLASS NAME           : Adsn600a
**        * INSTANCE NAME        : Adsn600a
************************************************************
************************************************************************
*
* PROGRAM:  ADSN600A (FORMERLY NAZN600A)
* DATE   :  07/11/1997
* FUNCTION: CREATE HISTORICAL RATES
*
*
*
*
*
*  02/26/08 O. SOTTO RATE EXPANSION TO 99 OCCURS. SC 022608.
*  03/15/12 O. SOTTO RATE EXPANSION. SC 031512.
*
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn600a extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Reval_Dte_A;
    private DbsField pnd_Nai_Pymnt_Mode;
    private DbsField pnd_Cntrct_Py_Dte_Key;

    private DbsGroup pnd_Cntrct_Py_Dte_Key__R_Field_1;

    private DbsGroup pnd_Cntrct_Py_Dte_Key_Hist_Structure;
    private DbsField pnd_Cntrct_Py_Dte_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Py_Dte_Key_Cntrct_Payee_Cde;
    private DbsField pnd_Cntrct_Py_Dte_Key_Fund_Invrse_Lst_Pd_Dte;
    private DbsField pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde;

    private DbsGroup pnd_Cntrct_Py_Dte_Key__R_Field_2;
    private DbsField pnd_Cntrct_Py_Dte_Key__Filler1;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Cde;
    private DbsField pnd_Nai_Dtl_Tiaa_Ia_Rate_Cd;
    private DbsField pnd_Nai_Dtl_Tiaa_Ia_Rate_Cd2;
    private DbsField pnd_Nai_Dtl_Tiaa_Grd_Grntd_Amt;
    private DbsField pnd_Nai_Dtl_Tiaa_Grd_Grntd_Amt2;
    private DbsField pnd_Nai_Dtl_Tiaa_Grd_Dvdnd_Amt;
    private DbsField pnd_Nai_Dtl_Tiaa_Grd_Dvdnd_Amt2;
    private DbsField pnd_Nai_Dtl_Fnl_Grd_Pay_Amt;
    private DbsField pnd_Nai_Dtl_Fnl_Grd_Pay_Amt2;
    private DbsField pnd_Nai_Dtl_Fnl_Grd_Dvdnd_Amt;
    private DbsField pnd_Nai_Dtl_Fnl_Grd_Dvdnd_Amt2;
    private DbsField pnd_Nai_Dtl_Tiaa_Std_Grntd_Amt;
    private DbsField pnd_Nai_Dtl_Tiaa_Std_Grntd_Amt2;
    private DbsField pnd_Nai_Dtl_Tiaa_Std_Dvdnd_Amt;
    private DbsField pnd_Nai_Dtl_Tiaa_Std_Dvdnd_Amt2;
    private DbsField pnd_Nai_Dtl_Fnl_Std_Pay_Amt;
    private DbsField pnd_Nai_Dtl_Fnl_Std_Pay_Amt2;
    private DbsField pnd_Nai_Dtl_Fnl_Std_Dvdnd_Amt;
    private DbsField pnd_Nai_Dtl_Fnl_Std_Dvdnd_Amt2;
    private DbsField pnd_Nai_Tiaa_Rate_Gic;
    private DbsField pnd_Nai_Tiaa_Rate_Gic2;
    private DbsField pnd_More;

    private DataAccessProgramView vw_iaa_Old_Tiaa_Rates;
    private DbsField iaa_Old_Tiaa_Rates_Fund_Lst_Pd_Dte;
    private DbsField iaa_Old_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte;
    private DbsField iaa_Old_Tiaa_Rates_Trans_Dte;
    private DbsField iaa_Old_Tiaa_Rates_Trans_User_Area;
    private DbsField iaa_Old_Tiaa_Rates_Trans_User_Id;
    private DbsField iaa_Old_Tiaa_Rates_Trans_Verify_Id;
    private DbsField iaa_Old_Tiaa_Rates_Trans_Verify_Dte;
    private DbsField iaa_Old_Tiaa_Rates_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Old_Tiaa_Rates_Cntrct_Payee_Cde;
    private DbsField iaa_Old_Tiaa_Rates_Cntrct_Mode_Ind;
    private DbsField iaa_Old_Tiaa_Rates_Cmpny_Fund_Cde;

    private DbsGroup iaa_Old_Tiaa_Rates__R_Field_3;
    private DbsField iaa_Old_Tiaa_Rates_Cmpny_Cde;
    private DbsField iaa_Old_Tiaa_Rates_Fund_Cde;
    private DbsField iaa_Old_Tiaa_Rates_Rcrd_Srce;
    private DbsField iaa_Old_Tiaa_Rates_Rcrd_Status;
    private DbsField iaa_Old_Tiaa_Rates_Cntrct_Tot_Per_Amt;
    private DbsField iaa_Old_Tiaa_Rates_Cntrct_Tot_Div_Amt;
    private DbsField iaa_Old_Tiaa_Rates_Count_Casttiaa_Rate_Data_Grp;

    private DbsGroup iaa_Old_Tiaa_Rates_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Old_Tiaa_Rates_Tiaa_Rate_Cde;
    private DbsField iaa_Old_Tiaa_Rates_Tiaa_Rate_Dte;
    private DbsField iaa_Old_Tiaa_Rates_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Old_Tiaa_Rates_Tiaa_Per_Div_Amt;
    private DbsField iaa_Old_Tiaa_Rates_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Old_Tiaa_Rates_Tiaa_Rate_Final_Div_Amt;
    private DbsField iaa_Old_Tiaa_Rates_Lst_Trans_Dte;
    private DbsGroup iaa_Old_Tiaa_Rates_Tiaa_Rate_GicMuGroup;
    private DbsField iaa_Old_Tiaa_Rates_Tiaa_Rate_Gic;
    private DbsField pnd_Wdte8;

    private DbsGroup pnd_Wdte8__R_Field_4;
    private DbsField pnd_Wdte8_Pnd_W_Yyyy;
    private DbsField pnd_Wdte8_Pnd_W_Mm;
    private DbsField pnd_Max_Rslt;
    private DbsField pnd_Max_Rate;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Reval_Dte_A = parameters.newFieldInRecord("pnd_Reval_Dte_A", "#REVAL-DTE-A", FieldType.STRING, 8);
        pnd_Reval_Dte_A.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Pymnt_Mode = parameters.newFieldInRecord("pnd_Nai_Pymnt_Mode", "#NAI-PYMNT-MODE", FieldType.NUMERIC, 3);
        pnd_Nai_Pymnt_Mode.setParameterOption(ParameterOption.ByReference);
        pnd_Cntrct_Py_Dte_Key = parameters.newFieldInRecord("pnd_Cntrct_Py_Dte_Key", "#CNTRCT-PY-DTE-KEY", FieldType.STRING, 23);
        pnd_Cntrct_Py_Dte_Key.setParameterOption(ParameterOption.ByReference);

        pnd_Cntrct_Py_Dte_Key__R_Field_1 = parameters.newGroupInRecord("pnd_Cntrct_Py_Dte_Key__R_Field_1", "REDEFINE", pnd_Cntrct_Py_Dte_Key);

        pnd_Cntrct_Py_Dte_Key_Hist_Structure = pnd_Cntrct_Py_Dte_Key__R_Field_1.newGroupInGroup("pnd_Cntrct_Py_Dte_Key_Hist_Structure", "HIST-STRUCTURE");
        pnd_Cntrct_Py_Dte_Key_Cntrct_Ppcn_Nbr = pnd_Cntrct_Py_Dte_Key_Hist_Structure.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Py_Dte_Key_Cntrct_Payee_Cde = pnd_Cntrct_Py_Dte_Key_Hist_Structure.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Py_Dte_Key_Fund_Invrse_Lst_Pd_Dte = pnd_Cntrct_Py_Dte_Key_Hist_Structure.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Fund_Invrse_Lst_Pd_Dte", 
            "FUND-INVRSE-LST-PD-DTE", FieldType.NUMERIC, 8);
        pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde = pnd_Cntrct_Py_Dte_Key_Hist_Structure.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde", "CMPNY-FUND-CDE", 
            FieldType.STRING, 3);

        pnd_Cntrct_Py_Dte_Key__R_Field_2 = pnd_Cntrct_Py_Dte_Key_Hist_Structure.newGroupInGroup("pnd_Cntrct_Py_Dte_Key__R_Field_2", "REDEFINE", pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde);
        pnd_Cntrct_Py_Dte_Key__Filler1 = pnd_Cntrct_Py_Dte_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Py_Dte_Key__Filler1", "_FILLER1", FieldType.STRING, 
            1);
        pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Cde = pnd_Cntrct_Py_Dte_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Cde", "#FUND-CDE", FieldType.STRING, 
            2);
        pnd_Nai_Dtl_Tiaa_Ia_Rate_Cd = parameters.newFieldArrayInRecord("pnd_Nai_Dtl_Tiaa_Ia_Rate_Cd", "#NAI-DTL-TIAA-IA-RATE-CD", FieldType.STRING, 2, 
            new DbsArrayController(1, 125));
        pnd_Nai_Dtl_Tiaa_Ia_Rate_Cd.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Dtl_Tiaa_Ia_Rate_Cd2 = parameters.newFieldArrayInRecord("pnd_Nai_Dtl_Tiaa_Ia_Rate_Cd2", "#NAI-DTL-TIAA-IA-RATE-CD2", FieldType.STRING, 
            2, new DbsArrayController(1, 125));
        pnd_Nai_Dtl_Tiaa_Ia_Rate_Cd2.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Dtl_Tiaa_Grd_Grntd_Amt = parameters.newFieldArrayInRecord("pnd_Nai_Dtl_Tiaa_Grd_Grntd_Amt", "#NAI-DTL-TIAA-GRD-GRNTD-AMT", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 125));
        pnd_Nai_Dtl_Tiaa_Grd_Grntd_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Dtl_Tiaa_Grd_Grntd_Amt2 = parameters.newFieldArrayInRecord("pnd_Nai_Dtl_Tiaa_Grd_Grntd_Amt2", "#NAI-DTL-TIAA-GRD-GRNTD-AMT2", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 125));
        pnd_Nai_Dtl_Tiaa_Grd_Grntd_Amt2.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Dtl_Tiaa_Grd_Dvdnd_Amt = parameters.newFieldArrayInRecord("pnd_Nai_Dtl_Tiaa_Grd_Dvdnd_Amt", "#NAI-DTL-TIAA-GRD-DVDND-AMT", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 125));
        pnd_Nai_Dtl_Tiaa_Grd_Dvdnd_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Dtl_Tiaa_Grd_Dvdnd_Amt2 = parameters.newFieldArrayInRecord("pnd_Nai_Dtl_Tiaa_Grd_Dvdnd_Amt2", "#NAI-DTL-TIAA-GRD-DVDND-AMT2", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 125));
        pnd_Nai_Dtl_Tiaa_Grd_Dvdnd_Amt2.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Dtl_Fnl_Grd_Pay_Amt = parameters.newFieldArrayInRecord("pnd_Nai_Dtl_Fnl_Grd_Pay_Amt", "#NAI-DTL-FNL-GRD-PAY-AMT", FieldType.NUMERIC, 9, 
            2, new DbsArrayController(1, 125));
        pnd_Nai_Dtl_Fnl_Grd_Pay_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Dtl_Fnl_Grd_Pay_Amt2 = parameters.newFieldArrayInRecord("pnd_Nai_Dtl_Fnl_Grd_Pay_Amt2", "#NAI-DTL-FNL-GRD-PAY-AMT2", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 125));
        pnd_Nai_Dtl_Fnl_Grd_Pay_Amt2.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Dtl_Fnl_Grd_Dvdnd_Amt = parameters.newFieldArrayInRecord("pnd_Nai_Dtl_Fnl_Grd_Dvdnd_Amt", "#NAI-DTL-FNL-GRD-DVDND-AMT", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 125));
        pnd_Nai_Dtl_Fnl_Grd_Dvdnd_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Dtl_Fnl_Grd_Dvdnd_Amt2 = parameters.newFieldArrayInRecord("pnd_Nai_Dtl_Fnl_Grd_Dvdnd_Amt2", "#NAI-DTL-FNL-GRD-DVDND-AMT2", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 125));
        pnd_Nai_Dtl_Fnl_Grd_Dvdnd_Amt2.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Dtl_Tiaa_Std_Grntd_Amt = parameters.newFieldArrayInRecord("pnd_Nai_Dtl_Tiaa_Std_Grntd_Amt", "#NAI-DTL-TIAA-STD-GRNTD-AMT", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 125));
        pnd_Nai_Dtl_Tiaa_Std_Grntd_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Dtl_Tiaa_Std_Grntd_Amt2 = parameters.newFieldArrayInRecord("pnd_Nai_Dtl_Tiaa_Std_Grntd_Amt2", "#NAI-DTL-TIAA-STD-GRNTD-AMT2", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 125));
        pnd_Nai_Dtl_Tiaa_Std_Grntd_Amt2.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Dtl_Tiaa_Std_Dvdnd_Amt = parameters.newFieldArrayInRecord("pnd_Nai_Dtl_Tiaa_Std_Dvdnd_Amt", "#NAI-DTL-TIAA-STD-DVDND-AMT", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 125));
        pnd_Nai_Dtl_Tiaa_Std_Dvdnd_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Dtl_Tiaa_Std_Dvdnd_Amt2 = parameters.newFieldArrayInRecord("pnd_Nai_Dtl_Tiaa_Std_Dvdnd_Amt2", "#NAI-DTL-TIAA-STD-DVDND-AMT2", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 125));
        pnd_Nai_Dtl_Tiaa_Std_Dvdnd_Amt2.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Dtl_Fnl_Std_Pay_Amt = parameters.newFieldArrayInRecord("pnd_Nai_Dtl_Fnl_Std_Pay_Amt", "#NAI-DTL-FNL-STD-PAY-AMT", FieldType.NUMERIC, 9, 
            2, new DbsArrayController(1, 125));
        pnd_Nai_Dtl_Fnl_Std_Pay_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Dtl_Fnl_Std_Pay_Amt2 = parameters.newFieldArrayInRecord("pnd_Nai_Dtl_Fnl_Std_Pay_Amt2", "#NAI-DTL-FNL-STD-PAY-AMT2", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 125));
        pnd_Nai_Dtl_Fnl_Std_Pay_Amt2.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Dtl_Fnl_Std_Dvdnd_Amt = parameters.newFieldArrayInRecord("pnd_Nai_Dtl_Fnl_Std_Dvdnd_Amt", "#NAI-DTL-FNL-STD-DVDND-AMT", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 125));
        pnd_Nai_Dtl_Fnl_Std_Dvdnd_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Dtl_Fnl_Std_Dvdnd_Amt2 = parameters.newFieldArrayInRecord("pnd_Nai_Dtl_Fnl_Std_Dvdnd_Amt2", "#NAI-DTL-FNL-STD-DVDND-AMT2", FieldType.NUMERIC, 
            9, 2, new DbsArrayController(1, 125));
        pnd_Nai_Dtl_Fnl_Std_Dvdnd_Amt2.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Tiaa_Rate_Gic = parameters.newFieldArrayInRecord("pnd_Nai_Tiaa_Rate_Gic", "#NAI-TIAA-RATE-GIC", FieldType.NUMERIC, 11, new DbsArrayController(1, 
            125));
        pnd_Nai_Tiaa_Rate_Gic.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Tiaa_Rate_Gic2 = parameters.newFieldArrayInRecord("pnd_Nai_Tiaa_Rate_Gic2", "#NAI-TIAA-RATE-GIC2", FieldType.NUMERIC, 11, new DbsArrayController(1, 
            125));
        pnd_Nai_Tiaa_Rate_Gic2.setParameterOption(ParameterOption.ByReference);
        pnd_More = parameters.newFieldInRecord("pnd_More", "#MORE", FieldType.BOOLEAN, 1);
        pnd_More.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Old_Tiaa_Rates = new DataAccessProgramView(new NameInfo("vw_iaa_Old_Tiaa_Rates", "IAA-OLD-TIAA-RATES"), "IAA_OLD_TIAA_RATES", "IA_OLD_RATES", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_OLD_TIAA_RATES"));
        iaa_Old_Tiaa_Rates_Fund_Lst_Pd_Dte = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Fund_Lst_Pd_Dte", "FUND-LST-PD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "FUND_LST_PD_DTE");
        iaa_Old_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte", "FUND-INVRSE-LST-PD-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "FUND_INVRSE_LST_PD_DTE");
        iaa_Old_Tiaa_Rates_Trans_Dte = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Old_Tiaa_Rates_Trans_User_Area = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Trans_User_Area", "TRANS-USER-AREA", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Old_Tiaa_Rates_Trans_User_Id = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Old_Tiaa_Rates_Trans_Verify_Id = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Trans_Verify_Id", "TRANS-VERIFY-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Old_Tiaa_Rates_Trans_Verify_Dte = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Trans_Verify_Dte", "TRANS-VERIFY-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TRANS_VERIFY_DTE");
        iaa_Old_Tiaa_Rates_Cntrct_Ppcn_Nbr = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Old_Tiaa_Rates_Cntrct_Payee_Cde = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        iaa_Old_Tiaa_Rates_Cntrct_Mode_Ind = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Old_Tiaa_Rates_Cmpny_Fund_Cde = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Cmpny_Fund_Cde", "CMPNY-FUND-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CMPNY_FUND_CDE");

        iaa_Old_Tiaa_Rates__R_Field_3 = vw_iaa_Old_Tiaa_Rates.getRecord().newGroupInGroup("iaa_Old_Tiaa_Rates__R_Field_3", "REDEFINE", iaa_Old_Tiaa_Rates_Cmpny_Fund_Cde);
        iaa_Old_Tiaa_Rates_Cmpny_Cde = iaa_Old_Tiaa_Rates__R_Field_3.newFieldInGroup("iaa_Old_Tiaa_Rates_Cmpny_Cde", "CMPNY-CDE", FieldType.STRING, 1);
        iaa_Old_Tiaa_Rates_Fund_Cde = iaa_Old_Tiaa_Rates__R_Field_3.newFieldInGroup("iaa_Old_Tiaa_Rates_Fund_Cde", "FUND-CDE", FieldType.STRING, 2);
        iaa_Old_Tiaa_Rates_Rcrd_Srce = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Rcrd_Srce", "RCRD-SRCE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "RCRD_SRCE");
        iaa_Old_Tiaa_Rates_Rcrd_Status = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Rcrd_Status", "RCRD-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_STATUS");
        iaa_Old_Tiaa_Rates_Cntrct_Tot_Per_Amt = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Cntrct_Tot_Per_Amt", "CNTRCT-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_TOT_PER_AMT");
        iaa_Old_Tiaa_Rates_Cntrct_Tot_Div_Amt = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Cntrct_Tot_Div_Amt", "CNTRCT-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_TOT_DIV_AMT");
        iaa_Old_Tiaa_Rates_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_OLD_RATES_TIAA_RATE_DATA_GRP");

        iaa_Old_Tiaa_Rates_Tiaa_Rate_Data_Grp = vw_iaa_Old_Tiaa_Rates.getRecord().newGroupInGroup("IAA_OLD_TIAA_RATES_TIAA_RATE_DATA_GRP", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Tiaa_Rates_Tiaa_Rate_Cde = iaa_Old_Tiaa_Rates_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Tiaa_Rate_Cde", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Tiaa_Rates_Tiaa_Rate_Dte = iaa_Old_Tiaa_Rates_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Tiaa_Rate_Dte", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_DTE", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Tiaa_Rates_Tiaa_Per_Pay_Amt = iaa_Old_Tiaa_Rates_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Tiaa_Rates_Tiaa_Per_Div_Amt = iaa_Old_Tiaa_Rates_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Tiaa_Rates_Tiaa_Rate_Final_Pay_Amt = iaa_Old_Tiaa_Rates_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Tiaa_Rates_Tiaa_Rate_Final_Div_Amt = iaa_Old_Tiaa_Rates_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Tiaa_Rates_Lst_Trans_Dte = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Old_Tiaa_Rates_Tiaa_Rate_GicMuGroup = vw_iaa_Old_Tiaa_Rates.getRecord().newGroupInGroup("IAA_OLD_TIAA_RATES_TIAA_RATE_GICMuGroup", "TIAA_RATE_GICMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_OLD_RATES_TIAA_RATE_GIC");
        iaa_Old_Tiaa_Rates_Tiaa_Rate_Gic = iaa_Old_Tiaa_Rates_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Tiaa_Rate_Gic", "TIAA-RATE-GIC", 
            FieldType.NUMERIC, 11, new DbsArrayController(1, 250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        registerRecord(vw_iaa_Old_Tiaa_Rates);

        pnd_Wdte8 = localVariables.newFieldInRecord("pnd_Wdte8", "#WDTE8", FieldType.STRING, 8);

        pnd_Wdte8__R_Field_4 = localVariables.newGroupInRecord("pnd_Wdte8__R_Field_4", "REDEFINE", pnd_Wdte8);
        pnd_Wdte8_Pnd_W_Yyyy = pnd_Wdte8__R_Field_4.newFieldInGroup("pnd_Wdte8_Pnd_W_Yyyy", "#W-YYYY", FieldType.NUMERIC, 4);
        pnd_Wdte8_Pnd_W_Mm = pnd_Wdte8__R_Field_4.newFieldInGroup("pnd_Wdte8_Pnd_W_Mm", "#W-MM", FieldType.NUMERIC, 2);
        pnd_Max_Rslt = localVariables.newFieldInRecord("pnd_Max_Rslt", "#MAX-RSLT", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Rate = localVariables.newFieldInRecord("pnd_Max_Rate", "#MAX-RATE", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Old_Tiaa_Rates.reset();

        parameters.reset();
        localVariables.reset();
        pnd_Max_Rslt.setInitialValue(125);
        pnd_Max_Rate.setInitialValue(250);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn600a() throws Exception
    {
        super("Adsn600a");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Wdte8.setValue(pnd_Reval_Dte_A);                                                                                                                              //Natural: ASSIGN #WDTE8 := #REVAL-DTE-A
        pnd_Wdte8_Pnd_W_Mm.nsubtract(1);                                                                                                                                  //Natural: SUBTRACT 1 FROM #W-MM
        if (condition(pnd_Wdte8_Pnd_W_Mm.equals(getZero())))                                                                                                              //Natural: IF #W-MM = 0
        {
            pnd_Wdte8_Pnd_W_Mm.setValue(12);                                                                                                                              //Natural: ASSIGN #W-MM := 12
            pnd_Wdte8_Pnd_W_Yyyy.nsubtract(1);                                                                                                                            //Natural: SUBTRACT 1 FROM #W-YYYY
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cntrct_Py_Dte_Key_Fund_Invrse_Lst_Pd_Dte.compute(new ComputeParameters(false, pnd_Cntrct_Py_Dte_Key_Fund_Invrse_Lst_Pd_Dte), DbsField.subtract(100000000,     //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.FUND-INVRSE-LST-PD-DTE := 100000000 - VAL ( #WDTE8 )
            pnd_Wdte8.val()));
        vw_iaa_Old_Tiaa_Rates.startDatabaseFind                                                                                                                           //Natural: FIND ( 1 ) IAA-OLD-TIAA-RATES WITH CNTRCT-PY-DTE-KEY = #CNTRCT-PY-DTE-KEY
        (
        "FT",
        new Wc[] { new Wc("CNTRCT_PY_DTE_KEY", "=", pnd_Cntrct_Py_Dte_Key, WcType.WITH) },
        1
        );
        FT:
        while (condition(vw_iaa_Old_Tiaa_Rates.readNextRow("FT", true)))
        {
            vw_iaa_Old_Tiaa_Rates.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Old_Tiaa_Rates.getAstCOUNTER().equals(0)))                                                                                               //Natural: IF NO RECORDS FOUND
            {
                vw_iaa_Old_Tiaa_Rates.setValuesByName(pnd_Cntrct_Py_Dte_Key_Hist_Structure);                                                                              //Natural: MOVE BY NAME HIST-STRUCTURE TO IAA-OLD-TIAA-RATES
                iaa_Old_Tiaa_Rates_Trans_Dte.setValue(Global.getTIMX());                                                                                                  //Natural: ASSIGN IAA-OLD-TIAA-RATES.TRANS-DTE := *TIMX
                iaa_Old_Tiaa_Rates_Lst_Trans_Dte.setValue(iaa_Old_Tiaa_Rates_Trans_Dte);                                                                                  //Natural: ASSIGN IAA-OLD-TIAA-RATES.LST-TRANS-DTE := IAA-OLD-TIAA-RATES.TRANS-DTE
                iaa_Old_Tiaa_Rates_Cntrct_Mode_Ind.setValue(pnd_Nai_Pymnt_Mode);                                                                                          //Natural: ASSIGN IAA-OLD-TIAA-RATES.CNTRCT-MODE-IND := #NAI-PYMNT-MODE
                //*  031512 START
                iaa_Old_Tiaa_Rates_Fund_Lst_Pd_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Wdte8);                                                              //Natural: MOVE EDITED #WDTE8 TO IAA-OLD-TIAA-RATES.FUND-LST-PD-DTE ( EM = YYYYMMDD )
                iaa_Old_Tiaa_Rates_Tiaa_Rate_Cde.getValue(1,":",pnd_Max_Rslt).setValue(pnd_Nai_Dtl_Tiaa_Ia_Rate_Cd.getValue(1,":",pnd_Max_Rslt));                         //Natural: ASSIGN IAA-OLD-TIAA-RATES.TIAA-RATE-CDE ( 1:#MAX-RSLT ) := #NAI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-RSLT )
                iaa_Old_Tiaa_Rates_Tiaa_Rate_Gic.getValue(1,":",pnd_Max_Rslt).setValue(pnd_Nai_Tiaa_Rate_Gic.getValue(1,":",pnd_Max_Rslt));                               //Natural: ASSIGN IAA-OLD-TIAA-RATES.TIAA-RATE-GIC ( 1:#MAX-RSLT ) := #NAI-TIAA-RATE-GIC ( 1:#MAX-RSLT )
                if (condition(pnd_More.getBoolean()))                                                                                                                     //Natural: IF #MORE
                {
                    iaa_Old_Tiaa_Rates_Tiaa_Rate_Cde.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(pnd_Nai_Dtl_Tiaa_Ia_Rate_Cd2.getValue(1,            //Natural: ASSIGN IAA-OLD-TIAA-RATES.TIAA-RATE-CDE ( #MAX-RSLT + 1:#MAX-RATE ) := #NAI-DTL-TIAA-IA-RATE-CD2 ( 1:#MAX-RSLT )
                        ":",pnd_Max_Rslt));
                    iaa_Old_Tiaa_Rates_Tiaa_Rate_Gic.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(pnd_Nai_Tiaa_Rate_Gic2.getValue(1,                  //Natural: ASSIGN IAA-OLD-TIAA-RATES.TIAA-RATE-GIC ( #MAX-RSLT + 1:#MAX-RATE ) := #NAI-TIAA-RATE-GIC2 ( 1:#MAX-RSLT )
                        ":",pnd_Max_Rslt));
                    //*  031512 END
                }                                                                                                                                                         //Natural: END-IF
                //*  031512 STAR
                if (condition(pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde.equals("T1G")))                                                                                        //Natural: IF #CNTRCT-PY-DTE-KEY.CMPNY-FUND-CDE = 'T1G'
                {
                    iaa_Old_Tiaa_Rates_Tiaa_Per_Pay_Amt.getValue(1,":",pnd_Max_Rslt).setValue(pnd_Nai_Dtl_Tiaa_Grd_Grntd_Amt.getValue(1,":",pnd_Max_Rslt));               //Natural: ASSIGN IAA-OLD-TIAA-RATES.TIAA-PER-PAY-AMT ( 1:#MAX-RSLT ) := #NAI-DTL-TIAA-GRD-GRNTD-AMT ( 1:#MAX-RSLT )
                    iaa_Old_Tiaa_Rates_Tiaa_Per_Div_Amt.getValue(1,":",pnd_Max_Rslt).setValue(pnd_Nai_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(1,":",pnd_Max_Rslt));               //Natural: ASSIGN IAA-OLD-TIAA-RATES.TIAA-PER-DIV-AMT ( 1:#MAX-RSLT ) := #NAI-DTL-TIAA-GRD-DVDND-AMT ( 1:#MAX-RSLT )
                    iaa_Old_Tiaa_Rates_Tiaa_Rate_Final_Pay_Amt.getValue(1,":",pnd_Max_Rslt).setValue(pnd_Nai_Dtl_Fnl_Grd_Pay_Amt.getValue(1,":",pnd_Max_Rslt));           //Natural: ASSIGN IAA-OLD-TIAA-RATES.TIAA-RATE-FINAL-PAY-AMT ( 1:#MAX-RSLT ) := #NAI-DTL-FNL-GRD-PAY-AMT ( 1:#MAX-RSLT )
                    iaa_Old_Tiaa_Rates_Tiaa_Rate_Final_Div_Amt.getValue(1,":",pnd_Max_Rslt).setValue(pnd_Nai_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(1,":",pnd_Max_Rslt));         //Natural: ASSIGN IAA-OLD-TIAA-RATES.TIAA-RATE-FINAL-DIV-AMT ( 1:#MAX-RSLT ) := #NAI-DTL-FNL-GRD-DVDND-AMT ( 1:#MAX-RSLT )
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        iaa_Old_Tiaa_Rates_Tiaa_Per_Pay_Amt.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(pnd_Nai_Dtl_Tiaa_Grd_Grntd_Amt2.getValue(1,  //Natural: ASSIGN IAA-OLD-TIAA-RATES.TIAA-PER-PAY-AMT ( #MAX-RSLT + 1:#MAX-RATE ) := #NAI-DTL-TIAA-GRD-GRNTD-AMT2 ( 1:#MAX-RSLT )
                            ":",pnd_Max_Rslt));
                        iaa_Old_Tiaa_Rates_Tiaa_Per_Div_Amt.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(pnd_Nai_Dtl_Tiaa_Grd_Dvdnd_Amt2.getValue(1,  //Natural: ASSIGN IAA-OLD-TIAA-RATES.TIAA-PER-DIV-AMT ( #MAX-RSLT + 1:#MAX-RATE ) := #NAI-DTL-TIAA-GRD-DVDND-AMT2 ( 1:#MAX-RSLT )
                            ":",pnd_Max_Rslt));
                        iaa_Old_Tiaa_Rates_Tiaa_Rate_Final_Pay_Amt.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(pnd_Nai_Dtl_Fnl_Grd_Pay_Amt2.getValue(1, //Natural: ASSIGN IAA-OLD-TIAA-RATES.TIAA-RATE-FINAL-PAY-AMT ( #MAX-RSLT + 1:#MAX-RATE ) := #NAI-DTL-FNL-GRD-PAY-AMT2 ( 1:#MAX-RSLT )
                            ":",pnd_Max_Rslt));
                        iaa_Old_Tiaa_Rates_Tiaa_Rate_Final_Div_Amt.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(pnd_Nai_Dtl_Fnl_Grd_Dvdnd_Amt2.getValue(1, //Natural: ASSIGN IAA-OLD-TIAA-RATES.TIAA-RATE-FINAL-DIV-AMT ( #MAX-RSLT + 1:#MAX-RATE ) := #NAI-DTL-FNL-GRD-DVDND-AMT2 ( 1:#MAX-RSLT )
                            ":",pnd_Max_Rslt));
                        //*  031512 END
                    }                                                                                                                                                     //Natural: END-IF
                    //*  031512 STAR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    iaa_Old_Tiaa_Rates_Tiaa_Per_Pay_Amt.getValue(1,":",pnd_Max_Rslt).setValue(pnd_Nai_Dtl_Tiaa_Std_Grntd_Amt.getValue(1,":",pnd_Max_Rslt));               //Natural: ASSIGN IAA-OLD-TIAA-RATES.TIAA-PER-PAY-AMT ( 1:#MAX-RSLT ) := #NAI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-RSLT )
                    iaa_Old_Tiaa_Rates_Tiaa_Per_Div_Amt.getValue(1,":",pnd_Max_Rslt).setValue(pnd_Nai_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(1,":",pnd_Max_Rslt));               //Natural: ASSIGN IAA-OLD-TIAA-RATES.TIAA-PER-DIV-AMT ( 1:#MAX-RSLT ) := #NAI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-RSLT )
                    iaa_Old_Tiaa_Rates_Tiaa_Rate_Final_Pay_Amt.getValue(1,":",pnd_Max_Rslt).setValue(pnd_Nai_Dtl_Fnl_Std_Pay_Amt.getValue(1,":",pnd_Max_Rslt));           //Natural: ASSIGN IAA-OLD-TIAA-RATES.TIAA-RATE-FINAL-PAY-AMT ( 1:#MAX-RSLT ) := #NAI-DTL-FNL-STD-PAY-AMT ( 1:#MAX-RSLT )
                    iaa_Old_Tiaa_Rates_Tiaa_Rate_Final_Div_Amt.getValue(1,":",pnd_Max_Rslt).setValue(pnd_Nai_Dtl_Fnl_Std_Dvdnd_Amt.getValue(1,":",pnd_Max_Rslt));         //Natural: ASSIGN IAA-OLD-TIAA-RATES.TIAA-RATE-FINAL-DIV-AMT ( 1:#MAX-RSLT ) := #NAI-DTL-FNL-STD-DVDND-AMT ( 1:#MAX-RSLT )
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        iaa_Old_Tiaa_Rates_Tiaa_Per_Pay_Amt.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(pnd_Nai_Dtl_Tiaa_Std_Grntd_Amt2.getValue(1,  //Natural: ASSIGN IAA-OLD-TIAA-RATES.TIAA-PER-PAY-AMT ( #MAX-RSLT + 1:#MAX-RATE ) := #NAI-DTL-TIAA-STD-GRNTD-AMT2 ( 1:#MAX-RSLT )
                            ":",pnd_Max_Rslt));
                        iaa_Old_Tiaa_Rates_Tiaa_Per_Div_Amt.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(pnd_Nai_Dtl_Tiaa_Std_Dvdnd_Amt2.getValue(1,  //Natural: ASSIGN IAA-OLD-TIAA-RATES.TIAA-PER-DIV-AMT ( #MAX-RSLT + 1:#MAX-RATE ) := #NAI-DTL-TIAA-STD-DVDND-AMT2 ( 1:#MAX-RSLT )
                            ":",pnd_Max_Rslt));
                        iaa_Old_Tiaa_Rates_Tiaa_Rate_Final_Pay_Amt.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(pnd_Nai_Dtl_Fnl_Std_Pay_Amt2.getValue(1, //Natural: ASSIGN IAA-OLD-TIAA-RATES.TIAA-RATE-FINAL-PAY-AMT ( #MAX-RSLT + 1:#MAX-RATE ) := #NAI-DTL-FNL-STD-PAY-AMT2 ( 1:#MAX-RSLT )
                            ":",pnd_Max_Rslt));
                        iaa_Old_Tiaa_Rates_Tiaa_Rate_Final_Div_Amt.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(pnd_Nai_Dtl_Fnl_Std_Dvdnd_Amt2.getValue(1, //Natural: ASSIGN IAA-OLD-TIAA-RATES.TIAA-RATE-FINAL-DIV-AMT ( #MAX-RSLT + 1:#MAX-RATE ) := #NAI-DTL-FNL-STD-DVDND-AMT2 ( 1:#MAX-RSLT )
                            ":",pnd_Max_Rslt));
                        //*  031512 END
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                iaa_Old_Tiaa_Rates_Cntrct_Tot_Per_Amt.compute(new ComputeParameters(false, iaa_Old_Tiaa_Rates_Cntrct_Tot_Per_Amt), DbsField.add(getZero(),                //Natural: ASSIGN IAA-OLD-TIAA-RATES.CNTRCT-TOT-PER-AMT := 0 + IAA-OLD-TIAA-RATES.TIAA-PER-PAY-AMT ( * )
                    iaa_Old_Tiaa_Rates_Tiaa_Per_Pay_Amt.getValue("*")));
                iaa_Old_Tiaa_Rates_Cntrct_Tot_Div_Amt.compute(new ComputeParameters(false, iaa_Old_Tiaa_Rates_Cntrct_Tot_Div_Amt), DbsField.add(getZero(),                //Natural: ASSIGN IAA-OLD-TIAA-RATES.CNTRCT-TOT-DIV-AMT := 0 + IAA-OLD-TIAA-RATES.TIAA-PER-DIV-AMT ( * )
                    iaa_Old_Tiaa_Rates_Tiaa_Per_Div_Amt.getValue("*")));
                iaa_Old_Tiaa_Rates_Rcrd_Srce.setValue("NZ");                                                                                                              //Natural: ASSIGN IAA-OLD-TIAA-RATES.RCRD-SRCE := 'NZ'
                iaa_Old_Tiaa_Rates_Rcrd_Status.setValue("A");                                                                                                             //Natural: ASSIGN IAA-OLD-TIAA-RATES.RCRD-STATUS := 'A'
                vw_iaa_Old_Tiaa_Rates.insertDBRow();                                                                                                                      //Natural: STORE IAA-OLD-TIAA-RATES
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //
}
