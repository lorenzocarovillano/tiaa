/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:54:56 PM
**        * FROM NATURAL SUBPROGRAM : Adsn601
************************************************************
**        * FILE NAME            : Adsn601.java
**        * CLASS NAME           : Adsn601
**        * INSTANCE NAME        : Adsn601
************************************************************
************************************************************************
*
* PROGRAM:  ADSN601
* DATE   :  03/04
* FUNCTION: INTERFACE TO CREATE IA MASTER AND TRANSACTION RECORDS
*
*
*
* HISTORY
* 12/06/02 - MOVE THE ALT-DEST-RLLVR-DEST INSTEAD OF RTB-DEST-CDE
*            TO THE PREV AND CURR DEST CODE.
*
* 12/23/04 - CHECK FOR NEW TNT RANGES
*
* 05/18/05 - M.NACHBER CHECK FOR ICAP
*
* 08312005 - M.NACHBER REL 6.5 FOR TPA'S and IPRO'S
*                      ASSIGNED NEW ORIG. CODES: 60,61,62
* 01112006 - M.NACHBER REL 8.0
*                     ASSINING NEW ORIG CODES TO ICAP, SEP IRA AND
*                     ROTH IRA
* 04032006 - M.NACHBER
*                     ADDED RANGES AND ORIGIN CODES FOR ICAP IPRO/TPA
* 03072007 - M. NACHBER  ATRA/KEOGH
* 08022007 - D.GEARY(DG) NEW 75% ANNUITY OPTION CHANGE.
* 10222007 - D.GEARY(DG) ALLOW REAL ESTATE FOR CALIFORNIA RESIDENTS
* 02212008 - O. SOTTO  TIAA RATE EXPANSION TO 99 OCCURS. SC 022108
* 03172008 - E. MELNIK ROTH 403B/401K. EM 031708
* 01/23/09  E MELNIK TIAA ACCESS/STABLE RETURN ENHANCEMENTS. MARKED
*                    BY EM - 012309.
*
* 3/17/10   C. MASON PCPOP CONVERSION ADD CODE TO HANDLE CANADIAN
*                    CONVERTED CONTRACTS
* 4/13/10   C. MASON ADD ADDITIONAL ORIGIN CODES
* 5/08/10   D.E.ANDER ADD TAX-EXMPT-IND, PLAN-NMBR, ORIGIN DOB AND DOD
*                                                           MARKED DEA
* 11/30/10  E. MELNIK STORE SURVIVOR RELATIONSHIP FIELD FOR SIPS.
*                     THIS IS A FIX FOR INC1213441.
*                     MARKED BY EM - 113010.
* 12/16/10  O. SOTTO  UPDATE CNTRCT-HOLD-CDE WITH 'P' WHEN ADP-HOLD-CDE
*                     IS 'RI00' (INTERNAL ROLLOVER). SC 121610.
* 06/14/11  O. SOTTO  PROD FIX, ASSIGN #ERR-CDE = 'L74' WHEN NUMBER OF
*                     UNITS EXCEED 9999.9999. SC 061411.
* 01/05/12  O. SOTTO  SUNY/CUNY CHANGES. SC 010512.
* 03/02/12  O. SOTTO  RATE EXPANSION. SC 030212.
* 06/20/12  O. SOTTO  PROD FIX - REA/ACCESS SHOULD BE ACCOUNTED FOR IN
*                     MOVE-TIAA-FIELDS ROUTINE.  SC 062012.
* 10/04/13  E. MELNIK CREF/REA REDESIGN CHANGES.
*                     MARKED BY EM - 100413.
* 5/07/2015  E. MELNIK BENE IN THE PLAN CHANGES.  MARKED BY EM - 050715.
* 02/21/2017 R. CARREON RESTOW FOR PIN EXPANSION 02212017
* 05/07/2020 E. MELNIK PROD FIX FOR INC5626347 TO ADDRESS ISSUE WITH
*                      CALCULATION OF IVC RESIDUAL AMOUNT FOR TIAA AND
*                      CREF FUNDS.  MARKED BY EM - 052020.
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn601 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAdsa600 pdaAdsa600;
    private LdaAdsl601 ldaAdsl601;
    private PdaNazpda_M pdaNazpda_M;
    private PdaAdsa605 pdaAdsa605;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Cntrl_Isn;
    private DbsField pnd_Bsnss_Dte;
    private DbsField pnd_Orgnl_State;
    private DbsField pnd_Err_Cde;
    private DbsField pnd_Ia_Orgn_Code;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_A;
    private DbsField pnd_Max_Cref;
    private DbsField pnd_Rea;
    private DbsField pnd_Access;
    private DbsField pnd_Fnd;
    private DbsField pnd_Wdte6;
    private DbsField pnd_Wdte4;
    private DbsField pnd_Wdte8;

    private DbsGroup pnd_Wdte8__R_Field_1;
    private DbsField pnd_Wdte8_Pnd_Issue_Dte;

    private DbsGroup pnd_Wdte8__R_Field_2;
    private DbsField pnd_Wdte8_Pnd_Wdte_Yyyy;
    private DbsField pnd_Wdte8_Pnd_Wdte_Mm;
    private DbsField pnd_Wdte8_Pnd_Wdte_Dd;
    private DbsField pnd_Ret_Cde;
    private DbsField pnd_Cref_Only;
    private DbsField pnd_Tiaa_Only;
    private DbsField pnd_Guar_Amt;
    private DbsField pnd_No_Pair;
    private DbsField pnd_Tiaa;
    private DbsField pnd_Cref;
    private DbsField pnd_State_N;
    private DbsField pnd_Ivc_Gd_Ind;
    private DbsField pnd_Next_Half;
    private DbsField pnd_Dd;
    private DbsField pnd_Rate_Dte_S;
    private DbsField pnd_Rate_Dte_G;
    private DbsField pnd_Grd;
    private DbsField pnd_Std;
    private DbsField pnd_Age_Yy;
    private DbsField pnd_Age_Mm;
    private DbsField pnd_Reval_Dte;

    private DbsGroup pnd_Reval_Dte__R_Field_3;
    private DbsField pnd_Reval_Dte_Pnd_Reval_Yyyy;
    private DbsField pnd_Reval_Dte_Pnd_Reval_Mm;
    private DbsField pnd_Check_Dte;
    private DbsField pnd_Internal_Rollover;
    private DbsField pnd_Roth_Ssnng_Dte;

    private DbsGroup pnd_Roth_Ssnng_Dte__R_Field_4;
    private DbsField pnd_Roth_Ssnng_Dte_Pnd_Roth_Ssnng_Yyyy;
    private DbsField pnd_Roth_Ssnng_Dte_Pnd_Roth_Ssnng_Mm;
    private DbsField pnd_Roth_Ssnng_Dte_Pnd_Roth_Ssnng_Dd;
    private DbsField pnd_Max_Rates;
    private DbsField pnd_Sub_Plan_Prefix;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl601 = new LdaAdsl601();
        registerRecord(ldaAdsl601);
        registerRecord(ldaAdsl601.getVw_ads_Ia_Rslt());
        registerRecord(ldaAdsl601.getVw_iaa_Cntrct());
        registerRecord(ldaAdsl601.getVw_iaa_Cntrct_Prtcpnt_Role());
        registerRecord(ldaAdsl601.getVw_iaa_Tiaa_Fund_Rcrd());
        registerRecord(ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1());
        registerRecord(ldaAdsl601.getVw_iaa_Trans_Rcrd());
        registerRecord(ldaAdsl601.getVw_iaa_Cntrl_Rcrd_1());
        registerRecord(ldaAdsl601.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaAdsl601.getVw_iaa_Cpr_Trans());
        registerRecord(ldaAdsl601.getVw_iaa_Tiaa_Fund_Trans());
        registerRecord(ldaAdsl601.getVw_iaa_Cref_Fund_Trans_1());
        localVariables = new DbsRecord();
        pdaNazpda_M = new PdaNazpda_M(localVariables);
        pdaAdsa605 = new PdaAdsa605(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaAdsa600 = new PdaAdsa600(parameters);
        pnd_Cntrl_Isn = parameters.newFieldInRecord("pnd_Cntrl_Isn", "#CNTRL-ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Cntrl_Isn.setParameterOption(ParameterOption.ByReference);
        pnd_Bsnss_Dte = parameters.newFieldInRecord("pnd_Bsnss_Dte", "#BSNSS-DTE", FieldType.DATE);
        pnd_Bsnss_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Orgnl_State = parameters.newFieldInRecord("pnd_Orgnl_State", "#ORGNL-STATE", FieldType.STRING, 2);
        pnd_Orgnl_State.setParameterOption(ParameterOption.ByReference);
        pnd_Err_Cde = parameters.newFieldInRecord("pnd_Err_Cde", "#ERR-CDE", FieldType.STRING, 3);
        pnd_Err_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Ia_Orgn_Code = parameters.newFieldInRecord("pnd_Ia_Orgn_Code", "#IA-ORGN-CODE", FieldType.NUMERIC, 2);
        pnd_Ia_Orgn_Code.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 3);
        pnd_Max_Cref = localVariables.newFieldInRecord("pnd_Max_Cref", "#MAX-CREF", FieldType.PACKED_DECIMAL, 3);
        pnd_Rea = localVariables.newFieldInRecord("pnd_Rea", "#REA", FieldType.STRING, 1);
        pnd_Access = localVariables.newFieldInRecord("pnd_Access", "#ACCESS", FieldType.STRING, 1);
        pnd_Fnd = localVariables.newFieldInRecord("pnd_Fnd", "#FND", FieldType.NUMERIC, 2);
        pnd_Wdte6 = localVariables.newFieldInRecord("pnd_Wdte6", "#WDTE6", FieldType.STRING, 6);
        pnd_Wdte4 = localVariables.newFieldInRecord("pnd_Wdte4", "#WDTE4", FieldType.STRING, 4);
        pnd_Wdte8 = localVariables.newFieldInRecord("pnd_Wdte8", "#WDTE8", FieldType.STRING, 8);

        pnd_Wdte8__R_Field_1 = localVariables.newGroupInRecord("pnd_Wdte8__R_Field_1", "REDEFINE", pnd_Wdte8);
        pnd_Wdte8_Pnd_Issue_Dte = pnd_Wdte8__R_Field_1.newFieldInGroup("pnd_Wdte8_Pnd_Issue_Dte", "#ISSUE-DTE", FieldType.STRING, 6);

        pnd_Wdte8__R_Field_2 = localVariables.newGroupInRecord("pnd_Wdte8__R_Field_2", "REDEFINE", pnd_Wdte8);
        pnd_Wdte8_Pnd_Wdte_Yyyy = pnd_Wdte8__R_Field_2.newFieldInGroup("pnd_Wdte8_Pnd_Wdte_Yyyy", "#WDTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Wdte8_Pnd_Wdte_Mm = pnd_Wdte8__R_Field_2.newFieldInGroup("pnd_Wdte8_Pnd_Wdte_Mm", "#WDTE-MM", FieldType.NUMERIC, 2);
        pnd_Wdte8_Pnd_Wdte_Dd = pnd_Wdte8__R_Field_2.newFieldInGroup("pnd_Wdte8_Pnd_Wdte_Dd", "#WDTE-DD", FieldType.NUMERIC, 2);
        pnd_Ret_Cde = localVariables.newFieldInRecord("pnd_Ret_Cde", "#RET-CDE", FieldType.NUMERIC, 2);
        pnd_Cref_Only = localVariables.newFieldInRecord("pnd_Cref_Only", "#CREF-ONLY", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Only = localVariables.newFieldInRecord("pnd_Tiaa_Only", "#TIAA-ONLY", FieldType.BOOLEAN, 1);
        pnd_Guar_Amt = localVariables.newFieldInRecord("pnd_Guar_Amt", "#GUAR-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_No_Pair = localVariables.newFieldInRecord("pnd_No_Pair", "#NO-PAIR", FieldType.BOOLEAN, 1);
        pnd_Tiaa = localVariables.newFieldInRecord("pnd_Tiaa", "#TIAA", FieldType.BOOLEAN, 1);
        pnd_Cref = localVariables.newFieldInRecord("pnd_Cref", "#CREF", FieldType.BOOLEAN, 1);
        pnd_State_N = localVariables.newFieldInRecord("pnd_State_N", "#STATE-N", FieldType.STRING, 2);
        pnd_Ivc_Gd_Ind = localVariables.newFieldInRecord("pnd_Ivc_Gd_Ind", "#IVC-GD-IND", FieldType.STRING, 1);
        pnd_Next_Half = localVariables.newFieldInRecord("pnd_Next_Half", "#NEXT-HALF", FieldType.BOOLEAN, 1);
        pnd_Dd = localVariables.newFieldInRecord("pnd_Dd", "#DD", FieldType.STRING, 2);
        pnd_Rate_Dte_S = localVariables.newFieldInRecord("pnd_Rate_Dte_S", "#RATE-DTE-S", FieldType.DATE);
        pnd_Rate_Dte_G = localVariables.newFieldInRecord("pnd_Rate_Dte_G", "#RATE-DTE-G", FieldType.DATE);
        pnd_Grd = localVariables.newFieldInRecord("pnd_Grd", "#GRD", FieldType.BOOLEAN, 1);
        pnd_Std = localVariables.newFieldInRecord("pnd_Std", "#STD", FieldType.BOOLEAN, 1);
        pnd_Age_Yy = localVariables.newFieldInRecord("pnd_Age_Yy", "#AGE-YY", FieldType.NUMERIC, 3);
        pnd_Age_Mm = localVariables.newFieldInRecord("pnd_Age_Mm", "#AGE-MM", FieldType.NUMERIC, 2);
        pnd_Reval_Dte = localVariables.newFieldInRecord("pnd_Reval_Dte", "#REVAL-DTE", FieldType.STRING, 6);

        pnd_Reval_Dte__R_Field_3 = localVariables.newGroupInRecord("pnd_Reval_Dte__R_Field_3", "REDEFINE", pnd_Reval_Dte);
        pnd_Reval_Dte_Pnd_Reval_Yyyy = pnd_Reval_Dte__R_Field_3.newFieldInGroup("pnd_Reval_Dte_Pnd_Reval_Yyyy", "#REVAL-YYYY", FieldType.STRING, 4);
        pnd_Reval_Dte_Pnd_Reval_Mm = pnd_Reval_Dte__R_Field_3.newFieldInGroup("pnd_Reval_Dte_Pnd_Reval_Mm", "#REVAL-MM", FieldType.STRING, 2);
        pnd_Check_Dte = localVariables.newFieldInRecord("pnd_Check_Dte", "#CHECK-DTE", FieldType.STRING, 6);
        pnd_Internal_Rollover = localVariables.newFieldInRecord("pnd_Internal_Rollover", "#INTERNAL-ROLLOVER", FieldType.STRING, 4);
        pnd_Roth_Ssnng_Dte = localVariables.newFieldInRecord("pnd_Roth_Ssnng_Dte", "#ROTH-SSNNG-DTE", FieldType.STRING, 8);

        pnd_Roth_Ssnng_Dte__R_Field_4 = localVariables.newGroupInRecord("pnd_Roth_Ssnng_Dte__R_Field_4", "REDEFINE", pnd_Roth_Ssnng_Dte);
        pnd_Roth_Ssnng_Dte_Pnd_Roth_Ssnng_Yyyy = pnd_Roth_Ssnng_Dte__R_Field_4.newFieldInGroup("pnd_Roth_Ssnng_Dte_Pnd_Roth_Ssnng_Yyyy", "#ROTH-SSNNG-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Roth_Ssnng_Dte_Pnd_Roth_Ssnng_Mm = pnd_Roth_Ssnng_Dte__R_Field_4.newFieldInGroup("pnd_Roth_Ssnng_Dte_Pnd_Roth_Ssnng_Mm", "#ROTH-SSNNG-MM", 
            FieldType.NUMERIC, 2);
        pnd_Roth_Ssnng_Dte_Pnd_Roth_Ssnng_Dd = pnd_Roth_Ssnng_Dte__R_Field_4.newFieldInGroup("pnd_Roth_Ssnng_Dte_Pnd_Roth_Ssnng_Dd", "#ROTH-SSNNG-DD", 
            FieldType.NUMERIC, 2);
        pnd_Max_Rates = localVariables.newFieldInRecord("pnd_Max_Rates", "#MAX-RATES", FieldType.NUMERIC, 3);
        pnd_Sub_Plan_Prefix = localVariables.newFieldInRecord("pnd_Sub_Plan_Prefix", "#SUB-PLAN-PREFIX", FieldType.STRING, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAdsl601.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Max_Cref.setInitialValue(20);
        pnd_Rea.setInitialValue("R");
        pnd_Access.setInitialValue("D");
        pnd_Internal_Rollover.setInitialValue("RI00");
        pnd_Max_Rates.setInitialValue(250);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn601() throws Exception
    {
        super("Adsn601");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* * IF #SEASONING-DTE GT 0 AND #ORGN-CDE GT 0
        //* *  MOVE EDITED #IA-CHECK-DTE TO #BASE-DTE(EM=YYYYMMDD)
        //* *  MOVE EDITED #BASE-DTE(EM=YYYY) TO #BASE-YYYY
        //* *  CALLNAT 'NAZN600C' ADSA600.ADP-FRST-ANNT-DTE-OF-BRTH
        //* *    #AGE-YY #AGE-MM #BASE-DTE
        //* *  IF #BASE-YYYYN - #SEASONING-YYYY GE 5 AND
        //* *      (#AGE-YY GT 59 OR (#AGE-YY = 59 AND #AGE-MM GE 6))
        //* *    #SEASONED := TRUE
        //* *  END-IF
        //* * END-IF
        //*                                                                                                                                                               //Natural: DECIDE FOR EVERY CONDITION
        short decideConditionsMet953 = 0;                                                                                                                                 //Natural: WHEN ADSA600.ADI-TIAA-STTLMNT = 'Y' OR ADSA600.ADI-TIAA-RE-STTLMNT = 'Y'
        if (condition(pdaAdsa600.getAdsa600_Adi_Tiaa_Sttlmnt().equals("Y") || pdaAdsa600.getAdsa600_Adi_Tiaa_Re_Sttlmnt().equals("Y")))
        {
            decideConditionsMet953++;
            pnd_Tiaa_Only.setValue(true);                                                                                                                                 //Natural: ASSIGN #TIAA-ONLY := TRUE
                                                                                                                                                                          //Natural: PERFORM MOVE-TIAA-FIELDS
            sub_Move_Tiaa_Fields();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CONTRACT-DRIVER
            sub_Contract_Driver();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Err_Cde.notEquals(" ")))                                                                                                                    //Natural: IF #ERR-CDE NE ' '
            {
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tiaa_Only.reset();                                                                                                                                        //Natural: RESET #TIAA-ONLY
            if (condition(pdaAdsa600.getAdsa600_Adi_Tiaa_Re_Sttlmnt().equals("Y") && pdaAdsa600.getAdsa600_Adi_Cref_Sttlmnt().notEquals("Y") && pdaAdsa600.getAdsa600_Adp_Ia_Cref_Nbr().notEquals(" "))) //Natural: IF ADSA600.ADI-TIAA-RE-STTLMNT = 'Y' AND ADSA600.ADI-CREF-STTLMNT NE 'Y' AND ADSA600.ADP-IA-CREF-NBR NE ' '
            {
                pnd_Next_Half.setValue(true);                                                                                                                             //Natural: ASSIGN #NEXT-HALF := TRUE
                pnd_No_Pair.setValue(true);                                                                                                                               //Natural: ASSIGN #NO-PAIR := TRUE
                                                                                                                                                                          //Natural: PERFORM MOVE-CREF-FIELDS
                sub_Move_Cref_Fields();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-CNTRCT-RCRD
                sub_Create_Cntrct_Rcrd();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Err_Cde.notEquals(" ")))                                                                                                                //Natural: IF #ERR-CDE NE ' '
                {
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-CNTRCT-TRANS
                sub_Create_Cntrct_Trans();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-CPR-RCRD
                sub_Create_Cpr_Rcrd();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-CPR-TRANS
                sub_Create_Cpr_Trans();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-CREF-FUND-RCRD
                sub_Create_Cref_Fund_Rcrd();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-TRANS-RCRD
                sub_Create_Trans_Rcrd();
                if (condition(Global.isEscape())) {return;}
                pnd_No_Pair.reset();                                                                                                                                      //Natural: RESET #NO-PAIR #NEXT-HALF
                pnd_Next_Half.reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN ADSA600.ADI-CREF-STTLMNT = 'Y'
        if (condition(pdaAdsa600.getAdsa600_Adi_Cref_Sttlmnt().equals("Y")))
        {
            decideConditionsMet953++;
            pnd_Cref_Only.setValue(true);                                                                                                                                 //Natural: ASSIGN #CREF-ONLY := TRUE
                                                                                                                                                                          //Natural: PERFORM MOVE-CREF-FIELDS
            sub_Move_Cref_Fields();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CONTRACT-DRIVER
            sub_Contract_Driver();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Err_Cde.notEquals(" ")))                                                                                                                    //Natural: IF #ERR-CDE NE ' '
            {
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cref_Only.reset();                                                                                                                                        //Natural: RESET #CREF-ONLY
            if (condition(pdaAdsa600.getAdsa600_Adi_Tiaa_Sttlmnt().notEquals("Y") && pdaAdsa600.getAdsa600_Adi_Tiaa_Re_Sttlmnt().notEquals("Y") && pdaAdsa600.getAdsa600_Adp_Ia_Tiaa_Nbr().notEquals(" "))) //Natural: IF ADSA600.ADI-TIAA-STTLMNT NE 'Y' AND ADSA600.ADI-TIAA-RE-STTLMNT NE 'Y' AND ADSA600.ADP-IA-TIAA-NBR NE ' '
            {
                pnd_No_Pair.setValue(true);                                                                                                                               //Natural: ASSIGN #NO-PAIR := TRUE
                pnd_Next_Half.setValue(true);                                                                                                                             //Natural: ASSIGN #NEXT-HALF := TRUE
                                                                                                                                                                          //Natural: PERFORM MOVE-TIAA-FIELDS
                sub_Move_Tiaa_Fields();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-CNTRCT-RCRD
                sub_Create_Cntrct_Rcrd();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Err_Cde.notEquals(" ")))                                                                                                                //Natural: IF #ERR-CDE NE ' '
                {
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-CNTRCT-TRANS
                sub_Create_Cntrct_Trans();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-CPR-RCRD
                sub_Create_Cpr_Rcrd();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-CPR-TRANS
                sub_Create_Cpr_Trans();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-TIAA-FUND-RCRD
                sub_Create_Tiaa_Fund_Rcrd();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-TRANS-RCRD
                sub_Create_Trans_Rcrd();
                if (condition(Global.isEscape())) {return;}
                pnd_Next_Half.reset();                                                                                                                                    //Natural: RESET #NEXT-HALF #NO-PAIR
                pnd_No_Pair.reset();
            }                                                                                                                                                             //Natural: END-IF
            //*  WHEN ANY
            //*    PERFORM UPDATE-CONTROL-RECORD
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet953 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-TIAA-FIELDS
        //* * IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-AMT(1) :=
        //* *  ADSA600.ADI-RTB-TIAA-TOT + ADSA600.ADI-RTB-RE-TOT
        //* *** EM - 052020 START
        //*  EM - 100413
        //*  TPA EGTRRA CODE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-CREF-FIELDS
        //*  IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-AMT(2) := ADSA600.ADI-RTB-CREF-TOT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONTRACT-DRIVER
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CNTRCT-RCRD
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CPR-RCRD
        //* ***********************************************************************
        //*    VALUE 'S'
        //*        ADSA600.ADP-SRVVR-RLTNSHP
        //*    VALUE 'B'
        //*        ADSA600.ADP-SRVVR-RLTNSHP
        //*    VALUE 'E','T'
        //*  EM - 050715 END
        //*  CM END
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-TIAA-FUND-RCRD
        //* ***********************************************************************
        //*  IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE(1:80) :=
        //*  IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT(1:80) :=
        //*  IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT(1:80) :=
        //*  IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT(1:80) :=
        //*  IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT(1:80) :=
        //*  IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE(1:80) :=
        //*  IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT(1:80) :=
        //*  IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT(1:80) :=
        //*  IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT(1:80) :=
        //*  IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT(1:80) :=
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-REA-FUND-RCRD
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CREF-FUND-RCRD
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CNTRCT-TRANS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CPR-TRANS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-TIAA-FUND-TRANS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CREF-FUND-TRANS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-TRANS-RCRD
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COLLAPSE-TIAA-RATES
        //* ***********************************************************************
        //*  COMPRESS RATE ARRAY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-RATE-DATE
    }
    private void sub_Move_Tiaa_Fields() throws Exception                                                                                                                  //Natural: MOVE-TIAA-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind().getValue("*").reset();                                                                              //Natural: RESET IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RCVRY-TYPE-IND ( * ) IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-AMT ( * ) IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( * ) IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-AMT ( * ) IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PER-IVC-AMT ( * ) IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-USED-AMT ( * ) IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RESDL-IVC-AMT ( * ) IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-PERCENT ( * ) IAA-CNTRCT.CNTRCT-TYPE-CDE #IVC-GD-IND
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt().getValue("*").reset();
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd().getValue("*").reset();
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt().getValue("*").reset();
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt().getValue("*").reset();
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt().getValue("*").reset();
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt().getValue("*").reset();
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent().getValue("*").reset();
        ldaAdsl601.getIaa_Cntrct_Cntrct_Type_Cde().reset();
        pnd_Ivc_Gd_Ind.reset();
        ldaAdsl601.getIaa_Cntrct_Cntrct_Ppcn_Nbr().setValue(pdaAdsa600.getAdsa600_Adp_Ia_Tiaa_Nbr());                                                                     //Natural: ASSIGN IAA-CNTRCT.CNTRCT-PPCN-NBR := ADSA600.ADP-IA-TIAA-NBR
        ldaAdsl601.getIaa_Cntrct_Cntrct_Mtch_Ppcn().setValue(pdaAdsa600.getAdsa600_Adp_Ia_Cref_Nbr());                                                                    //Natural: ASSIGN IAA-CNTRCT.CNTRCT-MTCH-PPCN := ADSA600.ADP-IA-CREF-NBR
        ldaAdsl601.getIaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr().setValue(pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue(1));                                                 //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ORIG-DA-CNTRCT-NBR := ADSA600.ADI-TIAA-NBRS ( 1 )
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd().getValue(1).setValue("T");                                                                              //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 1 ) := 'T'
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt().getValue(1).setValue(pdaAdsa600.getAdsa600_Adi_Tiaa_Ivc_Amt());                                            //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-AMT ( 1 ) := ADSA600.ADI-TIAA-IVC-AMT
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt().getValue(1).setValue(pdaAdsa600.getAdsa600_Adi_Tiaa_Prdc_Ivc_Amt());                                   //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PER-IVC-AMT ( 1 ) := ADSA600.ADI-TIAA-PRDC-IVC-AMT
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt().getValue(1).setValue(pdaAdsa600.getAdsa600_Pnd_Tiaa_Ivc_Used_Amt());                                  //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-USED-AMT ( 1 ) := ADSA600.#TIAA-IVC-USED-AMT
        if (condition(pdaAdsa600.getAdsa600_Adi_Tiaa_Ivc_Gd_Ind().equals("Y")))                                                                                           //Natural: IF ADSA600.ADI-TIAA-IVC-GD-IND = 'Y'
        {
            pnd_Ivc_Gd_Ind.setValue("Y");                                                                                                                                 //Natural: ASSIGN #IVC-GD-IND := 'Y'
            ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind().getValue(1).setValue(2);                                                                        //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RCVRY-TYPE-IND ( 1 ) := 2
            //*  030212
            //*  030212
            //*  030212
            //*  030212
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Guar_Amt.compute(new ComputeParameters(false, pnd_Guar_Amt), DbsField.add(getZero(),pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue("*")));   //Natural: ASSIGN #GUAR-AMT := 0 + ADSA600.#ADI-DTL-TIAA-STD-GRNTD-AMT ( * )
        pnd_Guar_Amt.nadd(pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue("*"));                                                                          //Natural: ASSIGN #GUAR-AMT := #GUAR-AMT + ADSA600.#ADI-DTL-TIAA-STD-DVDND-AMT ( * )
        pnd_Guar_Amt.nadd(pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue("*"));                                                                          //Natural: ASSIGN #GUAR-AMT := #GUAR-AMT + ADSA600.#ADI-DTL-TIAA-GRD-GRNTD-AMT ( * )
        pnd_Guar_Amt.nadd(pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue("*"));                                                                          //Natural: ASSIGN #GUAR-AMT := #GUAR-AMT + ADSA600.#ADI-DTL-TIAA-GRD-DVDND-AMT ( * )
        //* *** EM - 052020 END
        //*  062012 START
        //*  REA/ACCESS SHOULD BE ACCOUNTED FOR ON THE TIAA SIDE
        //*  SET WHEN SETTLING REA OR ACCESS
        if (condition(pdaAdsa600.getAdsa600_Adi_Tiaa_Re_Sttlmnt().equals("Y")))                                                                                           //Natural: IF ADSA600.ADI-TIAA-RE-STTLMNT = 'Y'
        {
            DbsUtil.examine(new ExamineSource(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue("*")), new ExamineSearch(pnd_Rea), new ExamineGivingIndex(pnd_A));    //Natural: EXAMINE ADSA600.ADI-DTL-CREF-ACCT-CD ( * ) FOR #REA GIVING INDEX #A
            if (condition(pnd_A.greater(getZero())))                                                                                                                      //Natural: IF #A GT 0
            {
                pnd_Guar_Amt.nadd(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_A));                                                                    //Natural: ASSIGN #GUAR-AMT := #GUAR-AMT + ADSA600.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A )
                pnd_Guar_Amt.nadd(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_A));                                                                      //Natural: ASSIGN #GUAR-AMT := #GUAR-AMT + ADSA600.ADI-DTL-CREF-DA-ANNL-AMT ( #A )
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue("*")), new ExamineSearch(pnd_Access), new ExamineGivingIndex(pnd_A)); //Natural: EXAMINE ADSA600.ADI-DTL-CREF-ACCT-CD ( * ) FOR #ACCESS GIVING INDEX #A
            if (condition(pnd_A.greater(getZero())))                                                                                                                      //Natural: IF #A GT 0
            {
                pnd_Guar_Amt.nadd(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_A));                                                                    //Natural: ASSIGN #GUAR-AMT := #GUAR-AMT + ADSA600.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A )
                pnd_Guar_Amt.nadd(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_A));                                                                      //Natural: ASSIGN #GUAR-AMT := #GUAR-AMT + ADSA600.ADI-DTL-CREF-DA-ANNL-AMT ( #A )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  062012 END
        if (condition(pdaAdsa600.getAdsa600_Adi_Tiaa_Prdc_Ivc_Amt().greater(pnd_Guar_Amt)))                                                                               //Natural: IF ADSA600.ADI-TIAA-PRDC-IVC-AMT GT #GUAR-AMT
        {
            ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt().getValue(1).compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt().getValue(1)),  //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RESDL-IVC-AMT ( 1 ) := ADSA600.ADI-TIAA-PRDC-IVC-AMT - #GUAR-AMT
                pdaAdsa600.getAdsa600_Adi_Tiaa_Prdc_Ivc_Amt().subtract(pnd_Guar_Amt));
        }                                                                                                                                                                 //Natural: END-IF
        //* * IF #ORGN-CDE = 20 THRU 22
        //*   RESET IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-AMT(1)
        //*     IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PER-IVC-AMT(1)
        //*     IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RESDL-IVC-AMT(1)
        //*   IF #SEASONED
        //*     IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RCVRY-TYPE-IND(1) := 1
        //*   ELSE
        //*     IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RCVRY-TYPE-IND(1) := 0
        //*     RESET IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-USED-AMT(1)
        //*     #IVC-GD-IND := 'N'
        //*   END-IF
        //* *  END-IF
        //* * IF #NO-PAIR = FALSE
        //* *  IF ADSA600.ADI-TIAA-IVC-AMT GT 0 AND
        //* *      ADSA600.ADI-TIAA-RTB-IVC-AMT GT 0
        //* *    COMPUTE ROUNDED #RTB-PCT =
        //* *      ADSA600.ADI-TIAA-RTB-IVC-AMT / ADSA600.ADI-TIAA-IVC-AMT
        //* *    COMPUTE #RTB-PCT = #RTB-PCT * 100
        //* *    IF #RTB-PCT GT 10.0000
        //* *      IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-PERCENT(1) := 10.0000
        //* *    ELSE
        //* *      MOVE ROUNDED #RTB-PCT TO
        //* *        IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-PERCENT(1)
        //* *    END-IF
        //* *  END-IF
        //* * END-IF
        //*  CM - SKIP IF PREV SET
        if (condition(pnd_Ia_Orgn_Code.equals(getZero())))                                                                                                                //Natural: IF #IA-ORGN-CODE EQ 0
        {
            //*  SET PARAMETERS FOR CALL TO ADSA605
            pdaAdsa605.getPnd_Adsa605().reset();                                                                                                                          //Natural: RESET #ADSA605
            pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().setValue(pdaAdsa600.getAdsa600_Adp_Plan_Nbr());                                                                      //Natural: ASSIGN #PLAN-NBR := ADSA600.ADP-PLAN-NBR
            pdaAdsa605.getPnd_Adsa605_Pnd_Sub_Plan_Nbr().setValue(pdaAdsa600.getAdsa600_Adc_Sub_Plan_Nbr());                                                              //Natural: ASSIGN #SUB-PLAN-NBR := ADSA600.ADC-SUB-PLAN-NBR
            pdaAdsa605.getPnd_Adsa605_Pnd_Acct_Code().getValue("*").setValue(pdaAdsa600.getAdsa600_Adc_Acct_Cde().getValue("*"));                                         //Natural: ASSIGN #ACCT-CODE ( * ) := ADSA600.ADC-ACCT-CDE ( * )
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Lob_Ind().getValue("*").setValue(pdaAdsa600.getAdsa600_Adi_Orig_Lob_Ind().getValue("*"));                                //Natural: ASSIGN #ORIGIN-LOB-IND ( * ) := ADSA600.ADI-ORIG-LOB-IND ( * )
            pdaAdsa605.getPnd_Adsa605_Pnd_T395_Fund_Id().getValue("*").setValue(pdaAdsa600.getAdsa600_Adc_T395_Fund_Id().getValue("*"));                                  //Natural: ASSIGN #T395-FUND-ID ( * ) := ADSA600.ADC-T395-FUND-ID ( * )
            pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().setValue(pdaAdsa600.getAdsa600_Adp_Roth_Plan_Type());                                                          //Natural: ASSIGN #ROTH-PLAN-TYPE := ADSA600.ADP-ROTH-PLAN-TYPE
            pdaAdsa605.getPnd_Adsa605_Pnd_Adp_Irc_Cde().setValue(pdaAdsa600.getAdsa600_Adp_Irc_Cde());                                                                    //Natural: ASSIGN #ADP-IRC-CDE := ADSA600.ADP-IRC-CDE
            pdaAdsa605.getPnd_Adsa605_Pnd_Invstmnt_Grpng_Id().getValue("*").setValue(pdaAdsa600.getAdsa600_Adc_Invstmnt_Grpng_Id().getValue("*"));                        //Natural: ASSIGN #INVSTMNT-GRPNG-ID ( * ) := ADSA600.ADC-INVSTMNT-GRPNG-ID ( * )
            //*  EM 031708 START
            if (condition(pdaAdsa600.getAdsa600_Adp_Roth_Rqst_Ind().notEquals("Y")))                                                                                      //Natural: IF ADSA600.ADP-ROTH-RQST-IND NE 'Y'
            {
                if (condition(pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue("*").notEquals(" ")))                                                                        //Natural: IF ADSA600.ADI-TIAA-NBRS ( * ) NE ' '
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Tiaa_Orgn_Cde().setValue(true);                                                                                         //Natural: ASSIGN #TIAA-ORGN-CDE := TRUE
                    pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue("*").setValue(pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue("*"));                            //Natural: ASSIGN #CONTRACT-NBRS ( * ) := ADSA600.ADI-TIAA-NBRS ( * )
                    DbsUtil.callnat(Adsn605.class , getCurrentProcessState(), pdaAdsa605.getPnd_Adsa605());                                                               //Natural: CALLNAT 'ADSN605' #ADSA605
                    if (condition(Global.isEscape())) return;
                    ldaAdsl601.getIaa_Cntrct_Cntrct_Orgn_Cde().setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code());                                                     //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ORGN-CDE := #ADSA605.#ORIGIN-CODE
                    getReports().write(0, "IN PROGRAM",Global.getPROGRAM());                                                                                              //Natural: WRITE 'IN PROGRAM' *PROGRAM
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaAdsl601.getIaa_Cntrct_Cntrct_Orgn_Cde());                                                                                //Natural: WRITE '=' IAA-CNTRCT.CNTRCT-ORGN-CDE
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Cref_Orgn_Cde().setValue(true);                                                                                         //Natural: ASSIGN #CREF-ORGN-CDE := TRUE
                    pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue("*").setValue(pdaAdsa600.getAdsa600_Adi_Cref_Nbrs().getValue("*"));                            //Natural: ASSIGN #CONTRACT-NBRS ( * ) := ADSA600.ADI-CREF-NBRS ( * )
                    DbsUtil.callnat(Adsn605.class , getCurrentProcessState(), pdaAdsa605.getPnd_Adsa605());                                                               //Natural: CALLNAT 'ADSN605' #ADSA605
                    if (condition(Global.isEscape())) return;
                    ldaAdsl601.getIaa_Cntrct_Cntrct_Orgn_Cde().setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code());                                                     //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ORGN-CDE := #ADSA605.#ORIGIN-CODE
                    getReports().write(0, "IN PROGRAM",Global.getPROGRAM());                                                                                              //Natural: WRITE 'IN PROGRAM' *PROGRAM
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaAdsl601.getIaa_Cntrct_Cntrct_Orgn_Cde());                                                                                //Natural: WRITE '=' IAA-CNTRCT.CNTRCT-ORGN-CDE
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue("*").notEquals(" ")))                                                                        //Natural: IF ADSA600.ADI-TIAA-NBRS ( * ) NE ' '
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Tiaa_Orgn_Cde().setValue(true);                                                                                    //Natural: ASSIGN #ROTH-TIAA-ORGN-CDE := TRUE
                    pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue("*").setValue(pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue("*"));                            //Natural: ASSIGN #CONTRACT-NBRS ( * ) := ADSA600.ADI-TIAA-NBRS ( * )
                    DbsUtil.callnat(Adsn605.class , getCurrentProcessState(), pdaAdsa605.getPnd_Adsa605());                                                               //Natural: CALLNAT 'ADSN605' #ADSA605
                    if (condition(Global.isEscape())) return;
                    ldaAdsl601.getIaa_Cntrct_Cntrct_Orgn_Cde().setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code());                                                     //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ORGN-CDE := #ADSA605.#ORIGIN-CODE
                    getReports().write(0, "IN PROGRAM",Global.getPROGRAM());                                                                                              //Natural: WRITE 'IN PROGRAM' *PROGRAM
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaAdsl601.getIaa_Cntrct_Cntrct_Orgn_Cde());                                                                                //Natural: WRITE '=' IAA-CNTRCT.CNTRCT-ORGN-CDE
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Cref_Orgn_Cde().setValue(true);                                                                                    //Natural: ASSIGN #ROTH-CREF-ORGN-CDE := TRUE
                    pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue("*").setValue(pdaAdsa600.getAdsa600_Adi_Cref_Nbrs().getValue("*"));                            //Natural: ASSIGN #CONTRACT-NBRS ( * ) := ADSA600.ADI-CREF-NBRS ( * )
                    DbsUtil.callnat(Adsn605.class , getCurrentProcessState(), pdaAdsa605.getPnd_Adsa605());                                                               //Natural: CALLNAT 'ADSN605' #ADSA605
                    if (condition(Global.isEscape())) return;
                    ldaAdsl601.getIaa_Cntrct_Cntrct_Orgn_Cde().setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code());                                                     //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ORGN-CDE := #ADSA605.#ORIGIN-CODE
                    getReports().write(0, "IN PROGRAM",Global.getPROGRAM());                                                                                              //Natural: WRITE 'IN PROGRAM' *PROGRAM
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaAdsl601.getIaa_Cntrct_Cntrct_Orgn_Cde());                                                                                //Natural: WRITE '=' IAA-CNTRCT.CNTRCT-ORGN-CDE
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //*  EM 031708 END
            }                                                                                                                                                             //Natural: END-IF
            //*  CM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAdsl601.getIaa_Cntrct_Cntrct_Orgn_Cde().setValue(pnd_Ia_Orgn_Code);                                                                                        //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ORGN-CDE := #IA-ORGN-CODE
            //*  CM
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 6
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(6)); pnd_I.nadd(1))
        {
            if (condition(((pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue(pnd_I).greaterOrEqual("K8000000") && pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue(pnd_I).lessOrEqual("K8099999"))  //Natural: IF ADSA600.ADI-TIAA-NBRS ( #I ) = 'K8000000' THRU 'K8099999' OR ADSA600.ADI-TIAA-NBRS ( #I ) = 'B8000000' THRU 'B8014999'
                || (pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue(pnd_I).greaterOrEqual("B8000000") && pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue(pnd_I).lessOrEqual("B8014999")))))
            {
                if (condition(ldaAdsl601.getIaa_Cntrct_Cntrct_Type_Cde().equals(" ")))                                                                                    //Natural: IF IAA-CNTRCT.CNTRCT-TYPE-CDE = ' '
                {
                    ldaAdsl601.getIaa_Cntrct_Cntrct_Type_Cde().setValue("Q");                                                                                             //Natural: ASSIGN IAA-CNTRCT.CNTRCT-TYPE-CDE := 'Q'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaAdsl601.getIaa_Cntrct_Cntrct_Type_Cde().equals("G")))                                                                                //Natural: IF IAA-CNTRCT.CNTRCT-TYPE-CDE = 'G'
                    {
                        ldaAdsl601.getIaa_Cntrct_Cntrct_Type_Cde().setValue("X");                                                                                         //Natural: ASSIGN IAA-CNTRCT.CNTRCT-TYPE-CDE := 'X'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue(pnd_I).greaterOrEqual("20000010") && pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue(pnd_I).lessOrEqual("28999999"))  //Natural: IF ADSA600.ADI-TIAA-NBRS ( #I ) = '20000010' THRU '28999999' OR ADSA600.ADI-TIAA-NBRS ( #I ) = '30000100' THRU '39999999'
                || (pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue(pnd_I).greaterOrEqual("30000100") && pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue(pnd_I).lessOrEqual("39999999")))))
            {
                if (condition(ldaAdsl601.getIaa_Cntrct_Cntrct_Type_Cde().equals(" ")))                                                                                    //Natural: IF IAA-CNTRCT.CNTRCT-TYPE-CDE = ' '
                {
                    ldaAdsl601.getIaa_Cntrct_Cntrct_Type_Cde().setValue("G");                                                                                             //Natural: ASSIGN IAA-CNTRCT.CNTRCT-TYPE-CDE := 'G'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaAdsl601.getIaa_Cntrct_Cntrct_Type_Cde().equals("Q")))                                                                                //Natural: IF IAA-CNTRCT.CNTRCT-TYPE-CDE = 'Q'
                    {
                        ldaAdsl601.getIaa_Cntrct_Cntrct_Type_Cde().setValue("X");                                                                                         //Natural: ASSIGN IAA-CNTRCT.CNTRCT-TYPE-CDE := 'X'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Rllvr_Ivc_Ind().setValue(pdaAdsa600.getAdsa600_Adp_Ivc_Rlvr_Cde());                                                         //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.RLLVR-IVC-IND := ADSA600.ADP-IVC-RLVR-CDE
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Rllvr_Dstrbtng_Irc_Cde().getValue("*").setValue(pdaAdsa600.getAdsa600_Adp_Frm_Irc_Cde().getValue(1,":",4));                 //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.RLLVR-DSTRBTNG-IRC-CDE ( * ) := ADSA600.ADP-FRM-IRC-CDE ( 1:4 )
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Rllvr_Accptng_Irc_Cde().setValue(pdaAdsa600.getAdsa600_Adp_To_Irc_Cde());                                                   //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.RLLVR-ACCPTNG-IRC-CDE := ADSA600.ADP-TO-IRC-CDE
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Rllvr_Pln_Admn_Ind().setValue(pdaAdsa600.getAdsa600_Adc_Trk_Ro_Ivc());                                                      //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.RLLVR-PLN-ADMN-IND := ADSA600.ADC-TRK-RO-IVC
        short decideConditionsMet1292 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE ADSA600.ADP-IVC-PYMNT-RULE;//Natural: VALUE 'O'
        if (condition((pdaAdsa600.getAdsa600_Adp_Ivc_Pymnt_Rule().equals("O"))))
        {
            decideConditionsMet1292++;
            ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind().setValue("Y");                                                                                       //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.RLLVR-ELGBLE-IND := 'Y'
        }                                                                                                                                                                 //Natural: VALUE 'P'
        else if (condition((pdaAdsa600.getAdsa600_Adp_Ivc_Pymnt_Rule().equals("P"))))
        {
            decideConditionsMet1292++;
            ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Rllvr_Elgble_Ind().setValue("N");                                                                                       //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.RLLVR-ELGBLE-IND := 'N'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Move_Cref_Fields() throws Exception                                                                                                                  //Natural: MOVE-CREF-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind().getValue("*").reset();                                                                              //Natural: RESET IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RCVRY-TYPE-IND ( * ) IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-AMT ( * ) IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( * ) IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-AMT ( * ) IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PER-IVC-AMT ( * ) IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-USED-AMT ( * ) IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RESDL-IVC-AMT ( * ) IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-PERCENT ( * ) IAA-CNTRCT.CNTRCT-TYPE-CDE #IVC-GD-IND
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt().getValue("*").reset();
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd().getValue("*").reset();
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Amt().getValue("*").reset();
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt().getValue("*").reset();
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt().getValue("*").reset();
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt().getValue("*").reset();
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rtb_Percent().getValue("*").reset();
        ldaAdsl601.getIaa_Cntrct_Cntrct_Type_Cde().reset();
        pnd_Ivc_Gd_Ind.reset();
        ldaAdsl601.getIaa_Cntrct_Cntrct_Ppcn_Nbr().setValue(pdaAdsa600.getAdsa600_Adp_Ia_Cref_Nbr());                                                                     //Natural: ASSIGN IAA-CNTRCT.CNTRCT-PPCN-NBR := ADSA600.ADP-IA-CREF-NBR
        ldaAdsl601.getIaa_Cntrct_Cntrct_Mtch_Ppcn().setValue(pdaAdsa600.getAdsa600_Adp_Ia_Tiaa_Nbr());                                                                    //Natural: ASSIGN IAA-CNTRCT.CNTRCT-MTCH-PPCN := ADSA600.ADP-IA-TIAA-NBR
        ldaAdsl601.getIaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr().setValue(pdaAdsa600.getAdsa600_Adi_Cref_Nbrs().getValue(1));                                                 //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ORIG-DA-CNTRCT-NBR := ADSA600.ADI-CREF-NBRS ( 1 )
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Company_Cd().getValue(2).setValue("C");                                                                              //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-COMPANY-CD ( 2 ) := 'C'
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt().getValue(2).setValue(pdaAdsa600.getAdsa600_Adi_Cref_Ivc_Amt());                                            //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-AMT ( 2 ) := ADSA600.ADI-CREF-IVC-AMT
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt().getValue(2).setValue(pdaAdsa600.getAdsa600_Adi_Cref_Prdc_Ivc_Amt());                                   //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PER-IVC-AMT ( 2 ) := ADSA600.ADI-CREF-PRDC-IVC-AMT
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Used_Amt().getValue(2).setValue(pdaAdsa600.getAdsa600_Pnd_Cref_Ivc_Used_Amt());                                  //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-USED-AMT ( 2 ) := ADSA600.#CREF-IVC-USED-AMT
        if (condition(pdaAdsa600.getAdsa600_Adi_Cref_Ivc_Gd_Ind().equals("Y")))                                                                                           //Natural: IF ADSA600.ADI-CREF-IVC-GD-IND = 'Y'
        {
            pnd_Ivc_Gd_Ind.setValue("Y");                                                                                                                                 //Natural: ASSIGN #IVC-GD-IND := 'Y'
            ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Rcvry_Type_Ind().getValue(2).setValue(2);                                                                        //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RCVRY-TYPE-IND ( 2 ) := 2
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #A 1 #MAX-CREF
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Cref)); pnd_A.nadd(1))
        {
            if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_A).equals(" ")))                                                                      //Natural: IF ADSA600.ADI-DTL-CREF-ACCT-CD ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //* *** EM - 052020
            if (condition(! (pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_A).equals(pnd_Rea) || pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_A).equals(pnd_Access)))) //Natural: IF NOT ADSA600.ADI-DTL-CREF-ACCT-CD ( #A ) = #REA OR = #ACCESS
            {
                pnd_Guar_Amt.nadd(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_A));                                                                    //Natural: ASSIGN #GUAR-AMT := #GUAR-AMT + ADSA600.ADI-DTL-CREF-DA-MNTHLY-AMT ( #A )
                pnd_Guar_Amt.nadd(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_A));                                                                      //Natural: ASSIGN #GUAR-AMT := #GUAR-AMT + ADSA600.ADI-DTL-CREF-DA-ANNL-AMT ( #A )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *#GUAR-AMT := 0 + ADSA600.ADI-DTL-CREF-DA-MNTHLY-AMT(*)
        //* *#GUAR-AMT := #GUAR-AMT + ADSA600.ADI-DTL-CREF-DA-ANNL-AMT(*)
        //*  062012 END
        if (condition(pdaAdsa600.getAdsa600_Adi_Cref_Prdc_Ivc_Amt().greater(pnd_Guar_Amt)))                                                                               //Natural: IF ADSA600.ADI-CREF-PRDC-IVC-AMT GT #GUAR-AMT
        {
            ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt().getValue(2).compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Resdl_Ivc_Amt().getValue(2)),  //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RESDL-IVC-AMT ( 2 ) := ADSA600.ADI-CREF-PRDC-IVC-AMT - #GUAR-AMT
                pdaAdsa600.getAdsa600_Adi_Cref_Prdc_Ivc_Amt().subtract(pnd_Guar_Amt));
        }                                                                                                                                                                 //Natural: END-IF
        //* * IF #ORGN-CDE = 20 THRU 22
        //* *  RESET IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-AMT(2)
        //* *    IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PER-IVC-AMT(2)
        //* *    IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RESDL-IVC-AMT(2)
        //* *  IF #SEASONED
        //* *    IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RCVRY-TYPE-IND(2) := 1
        //* *  ELSE
        //* *    RESET IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-USED-AMT(2)
        //* *    IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RCVRY-TYPE-IND(2) := 0
        //* *    #IVC-GD-IND := 'N'
        //* *  END-IF
        //* * END-IF
        //* * IF #NO-PAIR = FALSE
        //* *  IF ADSA600.ADI-CREF-IVC-AMT GT 0 AND
        //* *      ADSA600.ADI-RTB-CREF-TOT GT 0
        //* *    COMPUTE ROUNDED #RTB-PCT =
        //* *      ADSA600.ADI-CREF-RTB-IVC-AMT / ADSA600.ADI-CREF-IVC-AMT
        //* *    COMPUTE #RTB-PCT = #RTB-PCT * 100
        //* *    IF #RTB-PCT GT 10.0000
        //* *      IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-PERCENT(2) := 10.0000
        //* *    ELSE
        //* *      MOVE ROUNDED #RTB-PCT TO
        //* *        IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-RTB-PERCENT(2)
        //* *    END-IF
        //* *  END-IF
        //* * END-IF
        //*  CM - SKIP IF PREV SET
        if (condition(pnd_Ia_Orgn_Code.equals(getZero())))                                                                                                                //Natural: IF #IA-ORGN-CODE EQ 0
        {
            //*  SET PARAMETERS FOR CALL TO ADSA605
            pdaAdsa605.getPnd_Adsa605().reset();                                                                                                                          //Natural: RESET #ADSA605
            pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().setValue(pdaAdsa600.getAdsa600_Adp_Plan_Nbr());                                                                      //Natural: ASSIGN #PLAN-NBR := ADSA600.ADP-PLAN-NBR
            pdaAdsa605.getPnd_Adsa605_Pnd_Sub_Plan_Nbr().setValue(pdaAdsa600.getAdsa600_Adc_Sub_Plan_Nbr());                                                              //Natural: ASSIGN #SUB-PLAN-NBR := ADSA600.ADC-SUB-PLAN-NBR
            pdaAdsa605.getPnd_Adsa605_Pnd_Acct_Code().getValue("*").setValue(pdaAdsa600.getAdsa600_Adc_Acct_Cde().getValue("*"));                                         //Natural: ASSIGN #ACCT-CODE ( * ) := ADSA600.ADC-ACCT-CDE ( * )
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Lob_Ind().getValue("*").setValue(pdaAdsa600.getAdsa600_Adi_Orig_Lob_Ind().getValue("*"));                                //Natural: ASSIGN #ORIGIN-LOB-IND ( * ) := ADSA600.ADI-ORIG-LOB-IND ( * )
            pdaAdsa605.getPnd_Adsa605_Pnd_T395_Fund_Id().getValue("*").setValue(pdaAdsa600.getAdsa600_Adc_T395_Fund_Id().getValue("*"));                                  //Natural: ASSIGN #T395-FUND-ID ( * ) := ADSA600.ADC-T395-FUND-ID ( * )
            pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().setValue(pdaAdsa600.getAdsa600_Adp_Roth_Plan_Type());                                                          //Natural: ASSIGN #ROTH-PLAN-TYPE := ADSA600.ADP-ROTH-PLAN-TYPE
            pdaAdsa605.getPnd_Adsa605_Pnd_Adp_Irc_Cde().setValue(pdaAdsa600.getAdsa600_Adp_Irc_Cde());                                                                    //Natural: ASSIGN #ADP-IRC-CDE := ADSA600.ADP-IRC-CDE
            //*  EM 031708 START
            if (condition(pdaAdsa600.getAdsa600_Adp_Roth_Rqst_Ind().notEquals("Y")))                                                                                      //Natural: IF ADSA600.ADP-ROTH-RQST-IND NE 'Y'
            {
                if (condition(pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue("*").notEquals(" ")))                                                                        //Natural: IF ADSA600.ADI-TIAA-NBRS ( * ) NE ' '
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Tiaa_Orgn_Cde().setValue(true);                                                                                         //Natural: ASSIGN #TIAA-ORGN-CDE := TRUE
                    pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue("*").setValue(pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue("*"));                            //Natural: ASSIGN #CONTRACT-NBRS ( * ) := ADSA600.ADI-TIAA-NBRS ( * )
                    DbsUtil.callnat(Adsn605.class , getCurrentProcessState(), pdaAdsa605.getPnd_Adsa605());                                                               //Natural: CALLNAT 'ADSN605' #ADSA605
                    if (condition(Global.isEscape())) return;
                    ldaAdsl601.getIaa_Cntrct_Cntrct_Orgn_Cde().setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code());                                                     //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ORGN-CDE := #ADSA605.#ORIGIN-CODE
                    getReports().write(0, "IN PROGRAM",Global.getPROGRAM());                                                                                              //Natural: WRITE 'IN PROGRAM' *PROGRAM
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaAdsl601.getIaa_Cntrct_Cntrct_Orgn_Cde());                                                                                //Natural: WRITE '=' IAA-CNTRCT.CNTRCT-ORGN-CDE
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Cref_Orgn_Cde().setValue(true);                                                                                         //Natural: ASSIGN #CREF-ORGN-CDE := TRUE
                    pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue("*").setValue(pdaAdsa600.getAdsa600_Adi_Cref_Nbrs().getValue("*"));                            //Natural: ASSIGN #CONTRACT-NBRS ( * ) := ADSA600.ADI-CREF-NBRS ( * )
                    DbsUtil.callnat(Adsn605.class , getCurrentProcessState(), pdaAdsa605.getPnd_Adsa605());                                                               //Natural: CALLNAT 'ADSN605' #ADSA605
                    if (condition(Global.isEscape())) return;
                    ldaAdsl601.getIaa_Cntrct_Cntrct_Orgn_Cde().setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code());                                                     //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ORGN-CDE := #ADSA605.#ORIGIN-CODE
                    getReports().write(0, "IN PROGRAM",Global.getPROGRAM());                                                                                              //Natural: WRITE 'IN PROGRAM' *PROGRAM
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaAdsl601.getIaa_Cntrct_Cntrct_Orgn_Cde());                                                                                //Natural: WRITE '=' IAA-CNTRCT.CNTRCT-ORGN-CDE
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue("*").notEquals(" ")))                                                                        //Natural: IF ADSA600.ADI-TIAA-NBRS ( * ) NE ' '
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Tiaa_Orgn_Cde().setValue(true);                                                                                    //Natural: ASSIGN #ROTH-TIAA-ORGN-CDE := TRUE
                    pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue("*").setValue(pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue("*"));                            //Natural: ASSIGN #CONTRACT-NBRS ( * ) := ADSA600.ADI-TIAA-NBRS ( * )
                    DbsUtil.callnat(Adsn605.class , getCurrentProcessState(), pdaAdsa605.getPnd_Adsa605());                                                               //Natural: CALLNAT 'ADSN605' #ADSA605
                    if (condition(Global.isEscape())) return;
                    ldaAdsl601.getIaa_Cntrct_Cntrct_Orgn_Cde().setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code());                                                     //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ORGN-CDE := #ADSA605.#ORIGIN-CODE
                    getReports().write(0, "IN PROGRAM",Global.getPROGRAM());                                                                                              //Natural: WRITE 'IN PROGRAM' *PROGRAM
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaAdsl601.getIaa_Cntrct_Cntrct_Orgn_Cde());                                                                                //Natural: WRITE '=' IAA-CNTRCT.CNTRCT-ORGN-CDE
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Cref_Orgn_Cde().setValue(true);                                                                                    //Natural: ASSIGN #ROTH-CREF-ORGN-CDE := TRUE
                    pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue("*").setValue(pdaAdsa600.getAdsa600_Adi_Cref_Nbrs().getValue("*"));                            //Natural: ASSIGN #CONTRACT-NBRS ( * ) := ADSA600.ADI-CREF-NBRS ( * )
                    DbsUtil.callnat(Adsn605.class , getCurrentProcessState(), pdaAdsa605.getPnd_Adsa605());                                                               //Natural: CALLNAT 'ADSN605' #ADSA605
                    if (condition(Global.isEscape())) return;
                    ldaAdsl601.getIaa_Cntrct_Cntrct_Orgn_Cde().setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code());                                                     //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ORGN-CDE := #ADSA605.#ORIGIN-CODE
                    getReports().write(0, "IN PROGRAM",Global.getPROGRAM());                                                                                              //Natural: WRITE 'IN PROGRAM' *PROGRAM
                    if (Global.isEscape()) return;
                    getReports().write(0, "=",ldaAdsl601.getIaa_Cntrct_Cntrct_Orgn_Cde());                                                                                //Natural: WRITE '=' IAA-CNTRCT.CNTRCT-ORGN-CDE
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //*  EM 031708 END
            }                                                                                                                                                             //Natural: END-IF
            //*  CM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAdsl601.getIaa_Cntrct_Cntrct_Orgn_Cde().setValue(pnd_Ia_Orgn_Code);                                                                                        //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ORGN-CDE := #IA-ORGN-CODE
            //*  CM
        }                                                                                                                                                                 //Natural: END-IF
        FOR03:                                                                                                                                                            //Natural: FOR #I 1 TO 6
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(6)); pnd_I.nadd(1))
        {
            if (condition(((pdaAdsa600.getAdsa600_Adi_Cref_Nbrs().getValue(pnd_I).greaterOrEqual("J8000000") && pdaAdsa600.getAdsa600_Adi_Cref_Nbrs().getValue(pnd_I).lessOrEqual("J8099999"))  //Natural: IF ADSA600.ADI-CREF-NBRS ( #I ) = 'J8000000' THRU 'J8099999' OR ADSA600.ADI-CREF-NBRS ( #I ) = 'Q8000000' THRU 'Q8014999'
                || (pdaAdsa600.getAdsa600_Adi_Cref_Nbrs().getValue(pnd_I).greaterOrEqual("Q8000000") && pdaAdsa600.getAdsa600_Adi_Cref_Nbrs().getValue(pnd_I).lessOrEqual("Q8014999")))))
            {
                if (condition(ldaAdsl601.getIaa_Cntrct_Cntrct_Type_Cde().equals(" ")))                                                                                    //Natural: IF IAA-CNTRCT.CNTRCT-TYPE-CDE = ' '
                {
                    ldaAdsl601.getIaa_Cntrct_Cntrct_Type_Cde().setValue("Q");                                                                                             //Natural: ASSIGN IAA-CNTRCT.CNTRCT-TYPE-CDE := 'Q'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaAdsl601.getIaa_Cntrct_Cntrct_Type_Cde().equals("G")))                                                                                //Natural: IF IAA-CNTRCT.CNTRCT-TYPE-CDE = 'G'
                    {
                        ldaAdsl601.getIaa_Cntrct_Cntrct_Type_Cde().setValue("X");                                                                                         //Natural: ASSIGN IAA-CNTRCT.CNTRCT-TYPE-CDE := 'X'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((pdaAdsa600.getAdsa600_Adi_Cref_Nbrs().getValue(pnd_I).greaterOrEqual("10000010") && pdaAdsa600.getAdsa600_Adi_Cref_Nbrs().getValue(pnd_I).lessOrEqual("18999999"))  //Natural: IF ADSA600.ADI-CREF-NBRS ( #I ) = '10000010' THRU '18999999' OR ADSA600.ADI-CREF-NBRS ( #I ) = '40000100' THRU '49999999'
                || (pdaAdsa600.getAdsa600_Adi_Cref_Nbrs().getValue(pnd_I).greaterOrEqual("40000100") && pdaAdsa600.getAdsa600_Adi_Cref_Nbrs().getValue(pnd_I).lessOrEqual("49999999")))))
            {
                if (condition(ldaAdsl601.getIaa_Cntrct_Cntrct_Type_Cde().equals(" ")))                                                                                    //Natural: IF IAA-CNTRCT.CNTRCT-TYPE-CDE = ' '
                {
                    ldaAdsl601.getIaa_Cntrct_Cntrct_Type_Cde().setValue("G");                                                                                             //Natural: ASSIGN IAA-CNTRCT.CNTRCT-TYPE-CDE := 'G'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaAdsl601.getIaa_Cntrct_Cntrct_Type_Cde().equals("Q")))                                                                                //Natural: IF IAA-CNTRCT.CNTRCT-TYPE-CDE = 'Q'
                    {
                        ldaAdsl601.getIaa_Cntrct_Cntrct_Type_Cde().setValue("X");                                                                                         //Natural: ASSIGN IAA-CNTRCT.CNTRCT-TYPE-CDE := 'X'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Contract_Driver() throws Exception                                                                                                                   //Natural: CONTRACT-DRIVER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
                                                                                                                                                                          //Natural: PERFORM CREATE-CNTRCT-RCRD
        sub_Create_Cntrct_Rcrd();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Err_Cde.notEquals(" ")))                                                                                                                        //Natural: IF #ERR-CDE NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-CNTRCT-TRANS
        sub_Create_Cntrct_Trans();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-CPR-RCRD
        sub_Create_Cpr_Rcrd();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-CPR-TRANS
        sub_Create_Cpr_Trans();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Tiaa_Only.getBoolean()))                                                                                                                        //Natural: IF #TIAA-ONLY
        {
            if (condition(pdaAdsa600.getAdsa600_Adi_Tiaa_Sttlmnt().equals("Y")))                                                                                          //Natural: IF ADSA600.ADI-TIAA-STTLMNT = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM CREATE-TIAA-FUND-RCRD
                sub_Create_Tiaa_Fund_Rcrd();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaAdsa600.getAdsa600_Adi_Tiaa_Re_Sttlmnt().equals("Y")))                                                                                       //Natural: IF ADSA600.ADI-TIAA-RE-STTLMNT = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM CREATE-REA-FUND-RCRD
                sub_Create_Rea_Fund_Rcrd();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl601.getVw_iaa_Tiaa_Fund_Rcrd().reset();                                                                                                                //Natural: RESET IAA-TIAA-FUND-RCRD IAA-TIAA-FUND-TRANS IAA-CREF-FUND-RCRD-1 IAA-CREF-FUND-TRANS-1
            ldaAdsl601.getVw_iaa_Tiaa_Fund_Trans().reset();
            ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1().reset();
            ldaAdsl601.getVw_iaa_Cref_Fund_Trans_1().reset();
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Cref_Only.getBoolean()))                                                                                                                        //Natural: IF #CREF-ONLY
        {
                                                                                                                                                                          //Natural: PERFORM CREATE-CREF-FUND-RCRD
            sub_Create_Cref_Fund_Rcrd();
            if (condition(Global.isEscape())) {return;}
            ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1().reset();                                                                                                              //Natural: RESET IAA-CREF-FUND-RCRD-1 IAA-CREF-FUND-TRANS-1
            ldaAdsl601.getVw_iaa_Cref_Fund_Trans_1().reset();
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-TRANS-RCRD
        sub_Create_Trans_Rcrd();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Create_Cntrct_Rcrd() throws Exception                                                                                                                //Natural: CREATE-CNTRCT-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  CHECK IF CREATING CREF OR TIAA
        pnd_Wdte6.setValueEdited(pdaAdsa600.getAdsa600_Adi_Annty_Strt_Dte(),new ReportEditMask("YYYYMM"));                                                                //Natural: MOVE EDITED ADSA600.ADI-ANNTY-STRT-DTE ( EM = YYYYMM ) TO #WDTE6
        pnd_Dd.setValueEdited(pdaAdsa600.getAdsa600_Adi_Annty_Strt_Dte(),new ReportEditMask("DD"));                                                                       //Natural: MOVE EDITED ADSA600.ADI-ANNTY-STRT-DTE ( EM = DD ) TO #DD
        if (condition(DbsUtil.is(pnd_Wdte6.getText(),"N6")))                                                                                                              //Natural: IF #WDTE6 IS ( N6 )
        {
            ldaAdsl601.getIaa_Cntrct_Cntrct_Issue_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cntrct_Cntrct_Issue_Dte()), pnd_Wdte6.val());              //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ISSUE-DTE := VAL ( #WDTE6 )
            ldaAdsl601.getIaa_Cntrct_Cntrct_Issue_Dte_Dd().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cntrct_Cntrct_Issue_Dte_Dd()), pnd_Dd.val());           //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD := VAL ( #DD )
            ldaAdsl601.getIaa_Cntrct_Cntrct_Annty_Strt_Dte().setValue(pdaAdsa600.getAdsa600_Adi_Annty_Strt_Dte());                                                        //Natural: ASSIGN IAA-CNTRCT.CNTRCT-ANNTY-STRT-DTE := ADSA600.ADI-ANNTY-STRT-DTE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Wdte6.setValueEdited(pdaAdsa600.getAdsa600_Adi_Frst_Pymnt_Due_Dte(),new ReportEditMask("YYYYMM"));                                                            //Natural: MOVE EDITED ADSA600.ADI-FRST-PYMNT-DUE-DTE ( EM = YYYYMM ) TO #WDTE6
        pnd_Dd.setValueEdited(pdaAdsa600.getAdsa600_Adi_Frst_Pymnt_Due_Dte(),new ReportEditMask("DD"));                                                                   //Natural: MOVE EDITED ADSA600.ADI-FRST-PYMNT-DUE-DTE ( EM = DD ) TO #DD
        if (condition(DbsUtil.is(pnd_Wdte6.getText(),"N6")))                                                                                                              //Natural: IF #WDTE6 IS ( N6 )
        {
            ldaAdsl601.getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte()),            //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-PYMNT-DUE-DTE := VAL ( #WDTE6 )
                pnd_Wdte6.val());
            ldaAdsl601.getIaa_Cntrct_Cntrct_Fp_Due_Dte_Dd().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cntrct_Cntrct_Fp_Due_Dte_Dd()), pnd_Dd.val());         //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FP-DUE-DTE-DD := VAL ( #DD )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Wdte6.setValueEdited(pdaAdsa600.getAdsa600_Adi_Frst_Ck_Pd_Dte(),new ReportEditMask("YYYYMM"));                                                                //Natural: MOVE EDITED ADSA600.ADI-FRST-CK-PD-DTE ( EM = YYYYMM ) TO #WDTE6
        pnd_Dd.setValueEdited(pdaAdsa600.getAdsa600_Adi_Frst_Ck_Pd_Dte(),new ReportEditMask("DD"));                                                                       //Natural: MOVE EDITED ADSA600.ADI-FRST-CK-PD-DTE ( EM = DD ) TO #DD
        if (condition(DbsUtil.is(pnd_Wdte6.getText(),"N6")))                                                                                                              //Natural: IF #WDTE6 IS ( N6 )
        {
            ldaAdsl601.getIaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cntrct_Cntrct_First_Pymnt_Pd_Dte()),              //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-PYMNT-PD-DTE := VAL ( #WDTE6 )
                pnd_Wdte6.val());
            ldaAdsl601.getIaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cntrct_Cntrct_Fp_Pd_Dte_Dd()), pnd_Dd.val());           //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FP-PD-DTE-DD := VAL ( #DD )
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl601.getIaa_Cntrct_Cntrct_Crrncy_Cde().setValue(1);                                                                                                         //Natural: ASSIGN IAA-CNTRCT.CNTRCT-CRRNCY-CDE := 1
        ldaAdsl601.getIaa_Cntrct_Cntrct_Joint_Cnvrt_Rcrd_Ind().setValue("0");                                                                                             //Natural: ASSIGN IAA-CNTRCT.CNTRCT-JOINT-CNVRT-RCRD-IND := '0'
        ldaAdsl601.getIaa_Cntrct_Cntrct_Scnd_Annt_Ssn().setValue(pdaAdsa600.getAdsa600_Adp_Scnd_Annt_Ssn());                                                              //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-SSN := ADSA600.ADP-SCND-ANNT-SSN
        pnd_State_N.reset();                                                                                                                                              //Natural: RESET #STATE-N
        DbsUtil.callnat(Adsn031.class , getCurrentProcessState(), pdaNazpda_M.getMsg_Info_Sub(), pnd_Orgnl_State, pnd_State_N);                                           //Natural: CALLNAT 'ADSN031' MSG-INFO-SUB #ORGNL-STATE #STATE-N
        if (condition(Global.isEscape())) return;
        if (condition(pnd_State_N.equals(" ")))                                                                                                                           //Natural: IF #STATE-N = ' '
        {
            pnd_Err_Cde.setValue("L29");                                                                                                                                  //Natural: ASSIGN #ERR-CDE := 'L29'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl601.getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde().setValue(pnd_State_N, MoveOption.RightJustified);                                                           //Natural: MOVE RIGHT #STATE-N TO IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISSUE-CDE
        setValueToSubstring("0",ldaAdsl601.getIaa_Cntrct_Cntrct_Rsdncy_At_Issue_Cde(),1,1);                                                                               //Natural: MOVE '0' TO SUBSTR ( IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISSUE-CDE,1,1 )
        ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde().setValue(pdaAdsa600.getAdsa600_Adi_Optn_Cde());                                                                        //Natural: ASSIGN IAA-CNTRCT.CNTRCT-OPTN-CDE := ADSA600.ADI-OPTN-CDE
        ldaAdsl601.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind().setValue(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Lst_Nme().getSubstring(1,7));                                  //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND := SUBSTR ( ADSA600.ADP-FRST-ANNT-LST-NME,1,7 )
        setValueToSubstring(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Frst_Nme(),ldaAdsl601.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind(),8,1);                                    //Natural: MOVE ADSA600.ADP-FRST-ANNT-FRST-NME TO SUBSTR ( IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND,8,1 )
        setValueToSubstring(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Mid_Nme(),ldaAdsl601.getIaa_Cntrct_Cntrct_First_Annt_Xref_Ind(),9,1);                                     //Natural: MOVE ADSA600.ADP-FRST-ANNT-MID-NME TO SUBSTR ( IAA-CNTRCT.CNTRCT-FIRST-ANNT-XREF-IND,9,1 )
        ldaAdsl601.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind().setValue(pdaAdsa600.getAdsa600_Adp_Scnd_Annt_Lst_Nme().getSubstring(1,7));                                   //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND := SUBSTR ( ADSA600.ADP-SCND-ANNT-LST-NME,1,7 )
        setValueToSubstring(pdaAdsa600.getAdsa600_Adp_Scnd_Annt_Frst_Nme(),ldaAdsl601.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind(),8,1);                                     //Natural: MOVE ADSA600.ADP-SCND-ANNT-FRST-NME TO SUBSTR ( IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND,8,1 )
        setValueToSubstring(pdaAdsa600.getAdsa600_Adp_Scnd_Annt_Mid_Nme(),ldaAdsl601.getIaa_Cntrct_Cntrct_Scnd_Annt_Xref_Ind(),9,1);                                      //Natural: MOVE ADSA600.ADP-SCND-ANNT-MID-NME TO SUBSTR ( IAA-CNTRCT.CNTRCT-SCND-ANNT-XREF-IND,9,1 )
        ldaAdsl601.getIaa_Cntrct_Cntrct_Inst_Iss_Cde().setValue(pdaAdsa600.getAdsa600_Adc_Ppg_Cde().getSubstring(1,5));                                                   //Natural: MOVE SUBSTR ( ADSA600.ADC-PPG-CDE,1,5 ) TO IAA-CNTRCT.CNTRCT-INST-ISS-CDE
        pnd_Wdte8.setValueEdited(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Dte_Of_Brth(),new ReportEditMask("YYYYMMDD"));                                                       //Natural: MOVE EDITED ADSA600.ADP-FRST-ANNT-DTE-OF-BRTH ( EM = YYYYMMDD ) TO #WDTE8
        if (condition(DbsUtil.is(pnd_Wdte8.getText(),"N8")))                                                                                                              //Natural: IF #WDTE8 IS ( N8 )
        {
            ldaAdsl601.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cntrct_Cntrct_First_Annt_Dob_Dte()),              //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-DOB-DTE := VAL ( #WDTE8 )
                pnd_Wdte8.val());
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Wdte4.setValueEdited(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Dte_Of_Brth(),new ReportEditMask("YYYY"));                                                           //Natural: MOVE EDITED ADSA600.ADP-FRST-ANNT-DTE-OF-BRTH ( EM = YYYY ) TO #WDTE4
        if (condition(DbsUtil.is(pnd_Wdte4.getText(),"N4")))                                                                                                              //Natural: IF #WDTE4 IS ( N4 )
        {
            ldaAdsl601.getIaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cntrct_Cntrct_First_Annt_Mrtlty_Yob_Dte()),  //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-MRTLTY-YOB-DTE := VAL ( #WDTE4 )
                pnd_Wdte4.val());
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Wdte8.setValueEdited(pdaAdsa600.getAdsa600_Adp_Scnd_Annt_Dte_Of_Brth(),new ReportEditMask("YYYYMMDD"));                                                       //Natural: MOVE EDITED ADSA600.ADP-SCND-ANNT-DTE-OF-BRTH ( EM = YYYYMMDD ) TO #WDTE8
        if (condition(DbsUtil.is(pnd_Wdte8.getText(),"N8")))                                                                                                              //Natural: IF #WDTE8 IS ( N8 )
        {
            ldaAdsl601.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cntrct_Cntrct_Scnd_Annt_Dob_Dte()),                //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-DOB-DTE := VAL ( #WDTE8 )
                pnd_Wdte8.val());
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Wdte4.setValueEdited(pdaAdsa600.getAdsa600_Adp_Scnd_Annt_Dte_Of_Brth(),new ReportEditMask("YYYY"));                                                           //Natural: MOVE EDITED ADSA600.ADP-SCND-ANNT-DTE-OF-BRTH ( EM = YYYY ) TO #WDTE4
        if (condition(DbsUtil.is(pnd_Wdte4.getText(),"N4")))                                                                                                              //Natural: IF #WDTE4 IS ( N4 )
        {
            ldaAdsl601.getIaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cntrct_Cntrct_Scnd_Annt_Mrtlty_Yob_Dte()),  //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-MRTLTY-YOB-DTE := VAL ( #WDTE4 )
                pnd_Wdte4.val());
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Sex_Cde().equals("M")))                                                                                         //Natural: IF ADSA600.ADP-FRST-ANNT-SEX-CDE = 'M'
        {
            ldaAdsl601.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde().setValue(1);                                                                                             //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE := 1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Sex_Cde().equals("F")))                                                                                         //Natural: IF ADSA600.ADP-FRST-ANNT-SEX-CDE = 'F'
        {
            ldaAdsl601.getIaa_Cntrct_Cntrct_First_Annt_Sex_Cde().setValue(2);                                                                                             //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-SEX-CDE := 2
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa600.getAdsa600_Adp_Scnd_Annt_Sex_Cde().equals("M")))                                                                                         //Natural: IF ADSA600.ADP-SCND-ANNT-SEX-CDE = 'M'
        {
            ldaAdsl601.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde().setValue(1);                                                                                              //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE := 1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa600.getAdsa600_Adp_Scnd_Annt_Sex_Cde().equals("F")))                                                                                         //Natural: IF ADSA600.ADP-SCND-ANNT-SEX-CDE = 'F'
        {
            ldaAdsl601.getIaa_Cntrct_Cntrct_Scnd_Annt_Sex_Cde().setValue(2);                                                                                              //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-SEX-CDE := 2
        }                                                                                                                                                                 //Natural: END-IF
        //*  ANNUITY CERTAIN
        if (condition(ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde().equals(21)))                                                                                             //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = 21
        {
            ldaAdsl601.getIaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt().setValue(0);                                                                                             //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-LFE-CNT := 0
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAdsl601.getIaa_Cntrct_Cntrct_First_Annt_Lfe_Cnt().setValue(1);                                                                                             //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FIRST-ANNT-LFE-CNT := 1
        }                                                                                                                                                                 //Natural: END-IF
        //*  DG  080207
        if (condition((((((ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde().equals(3) || ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde().equals(4)) || ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde().equals(7))  //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = 03 OR = 04 OR = 07 OR = 08 OR IAA-CNTRCT.CNTRCT-OPTN-CDE = 10 THRU 18 OR IAA-CNTRCT.CNTRCT-OPTN-CDE = 54 THRU 57
            || ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde().equals(8)) || (ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(10) && ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(18))) 
            || (ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(54) && ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(57)))))
        {
            ldaAdsl601.getIaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt().setValue(1);                                                                                             //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-LIFE-CNT := 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAdsl601.getIaa_Cntrct_Cntrct_Scnd_Annt_Life_Cnt().setValue(0);                                                                                             //Natural: ASSIGN IAA-CNTRCT.CNTRCT-SCND-ANNT-LIFE-CNT := 0
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl601.getIaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re().reset();                                                                                                       //Natural: RESET IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISS-RE
        if (condition(DbsUtil.maskMatches(ldaAdsl601.getIaa_Cntrct_Cntrct_Ppcn_Nbr(),"'Y'")))                                                                             //Natural: IF IAA-CNTRCT.CNTRCT-PPCN-NBR = MASK ( 'Y' )
        {
            if (condition(pdaAdsa600.getAdsa600_Adp_Ia_Tiaa_Rea_Nbr().notEquals(" ")))                                                                                    //Natural: IF ADSA600.ADP-IA-TIAA-REA-NBR NE ' '
            {
                //*  DG 10/22/07
                if (condition(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Rsdnc_Cde().greaterOrEqual("01") && pdaAdsa600.getAdsa600_Adp_Frst_Annt_Rsdnc_Cde().lessOrEqual("05"))) //Natural: IF ADSA600.ADP-FRST-ANNT-RSDNC-CDE = '01' THRU '05'
                {
                    //*      ADSA600.ADP-FRST-ANNT-RSDNC-CDE = '06' THRU '60'  /* DG 10/22/07
                    ldaAdsl601.getIaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re().setValue(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Rsdnc_Cde(), MoveOption.RightJustified);              //Natural: MOVE RIGHT ADSA600.ADP-FRST-ANNT-RSDNC-CDE TO IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISS-RE
                    setValueToSubstring("0",ldaAdsl601.getIaa_Cntrct_Cntrct_Rsdncy_At_Iss_Re(),1,1);                                                                      //Natural: MOVE '0' TO SUBSTR ( IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISS-RE,1,1 )
                    //*    ELSE                                                /* DG 10/22/07
                    //*      IAA-CNTRCT.CNTRCT-RSDNCY-AT-ISS-RE := '035'       /* DG 10/22/07
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl601.getIaa_Cntrct_Cntrct_Type().setValue("N");                                                                                                             //Natural: ASSIGN IAA-CNTRCT.CNTRCT-TYPE := 'N'
        ldaAdsl601.getIaa_Cntrct_Lst_Trans_Dte().setValue(Global.getTIMX());                                                                                              //Natural: ASSIGN IAA-CNTRCT.LST-TRANS-DTE := *TIMX
        //*  EM 031708 START
        if (condition(pdaAdsa600.getAdsa600_Adp_Roth_Rqst_Ind().equals("Y")))                                                                                             //Natural: IF ADSA600.ADP-ROTH-RQST-IND = 'Y'
        {
            ldaAdsl601.getIaa_Cntrct_Roth_Frst_Cntrb_Dte().setValue(pdaAdsa600.getAdsa600_Adp_Roth_Frst_Cntrbtn_Dte());                                                   //Natural: ASSIGN IAA-CNTRCT.ROTH-FRST-CNTRB-DTE := ADSA600.ADP-ROTH-FRST-CNTRBTN-DTE
            pnd_Roth_Ssnng_Dte.setValueEdited(pdaAdsa600.getAdsa600_Adp_Roth_Frst_Cntrbtn_Dte(),new ReportEditMask("YYYYMMDD"));                                          //Natural: MOVE EDITED ADSA600.ADP-ROTH-FRST-CNTRBTN-DTE ( EM = YYYYMMDD ) TO #ROTH-SSNNG-DTE
            pnd_Roth_Ssnng_Dte_Pnd_Roth_Ssnng_Yyyy.nadd(5);                                                                                                               //Natural: COMPUTE #ROTH-SSNNG-YYYY = #ROTH-SSNNG-YYYY + 5
            pnd_Roth_Ssnng_Dte_Pnd_Roth_Ssnng_Mm.setValue(1);                                                                                                             //Natural: ASSIGN #ROTH-SSNNG-MM := 1
            pnd_Roth_Ssnng_Dte_Pnd_Roth_Ssnng_Dd.setValue(1);                                                                                                             //Natural: ASSIGN #ROTH-SSNNG-DD := 1
            ldaAdsl601.getIaa_Cntrct_Roth_Ssnng_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Roth_Ssnng_Dte);                                                  //Natural: MOVE EDITED #ROTH-SSNNG-DTE TO IAA-CNTRCT.ROTH-SSNNG-DTE ( EM = YYYYMMDD )
            //*  DEA
            //*  DEA
            //*  010512 START
            //*  010512 END
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl601.getIaa_Cntrct_Plan_Nmbr().setValue(pdaAdsa600.getAdsa600_Adp_Plan_Nbr());                                                                              //Natural: ASSIGN IAA-CNTRCT.PLAN-NMBR := ADSA600.ADP-PLAN-NBR
        ldaAdsl601.getIaa_Cntrct_Tax_Exmpt_Ind().setValue(pdaAdsa600.getAdsa600_Adp_Mndtry_Tax_Ovrd());                                                                   //Natural: ASSIGN IAA-CNTRCT.TAX-EXMPT-IND := ADP-MNDTRY-TAX-OVRD
        ldaAdsl601.getIaa_Cntrct_Sub_Plan_Nmbr().setValue(pdaAdsa600.getAdsa600_Adc_Sub_Plan_Nbr());                                                                      //Natural: ASSIGN IAA-CNTRCT.SUB-PLAN-NMBR := ADSA600.ADC-SUB-PLAN-NBR
        ldaAdsl601.getIaa_Cntrct_Orgntng_Cntrct_Nmbr().setValue(pdaAdsa600.getAdsa600_Adc_Original_Cntrct_Nbr());                                                         //Natural: ASSIGN IAA-CNTRCT.ORGNTNG-CNTRCT-NMBR := ADSA600.ADC-ORIGINAL-CNTRCT-NBR
        ldaAdsl601.getIaa_Cntrct_Orgntng_Sub_Plan_Nmbr().setValue(pdaAdsa600.getAdsa600_Adc_Original_Sub_Plan_Nbr());                                                     //Natural: ASSIGN IAA-CNTRCT.ORGNTNG-SUB-PLAN-NMBR := ADSA600.ADC-ORIGINAL-SUB-PLAN-NBR
        SC:                                                                                                                                                               //Natural: STORE IAA-CNTRCT
        ldaAdsl601.getVw_iaa_Cntrct().insertDBRow("SC");
    }
    private void sub_Create_Cpr_Rcrd() throws Exception                                                                                                                   //Natural: CREATE-CPR-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Ppcn_Nbr().setValue(ldaAdsl601.getIaa_Cntrct_Cntrct_Ppcn_Nbr());                                                //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PPCN-NBR := IAA-CNTRCT.CNTRCT-PPCN-NBR
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Part_Payee_Cde().setValue(1);                                                                                        //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PART-PAYEE-CDE := 01
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cpr_Id_Nbr().setValue(pdaAdsa600.getAdsa600_Adp_Unique_Id());                                                               //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CPR-ID-NBR := ADSA600.ADP-UNIQUE-ID
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().setValue(1);                                                                                            //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE := 1
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn().reset();                                                                                                //Natural: RESET IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-TRMNTE-RSN
        if (condition(DbsUtil.is(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Ctznshp().getText(),"N2")))                                                                          //Natural: IF ADSA600.ADP-FRST-ANNT-CTZNSHP IS ( N2 )
        {
            ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde()),  //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-CTZNSHP-CDE := VAL ( ADSA600.ADP-FRST-ANNT-CTZNSHP )
                pdaAdsa600.getAdsa600_Adp_Frst_Annt_Ctznshp().val());
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet1604 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE ADSA600.ADP-FRST-ANNT-CTZNSHP;//Natural: VALUE 'US'
            if (condition((pdaAdsa600.getAdsa600_Adp_Frst_Annt_Ctznshp().equals("US"))))
            {
                decideConditionsMet1604++;
                ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde().setValue(1);                                                                                  //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-CTZNSHP-CDE := 001
            }                                                                                                                                                             //Natural: VALUE 'CA'
            else if (condition((pdaAdsa600.getAdsa600_Adp_Frst_Annt_Ctznshp().equals("CA"))))
            {
                decideConditionsMet1604++;
                ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde().setValue(2);                                                                                  //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-CTZNSHP-CDE := 002
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Ctznshp_Cde().setValue(3);                                                                                  //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-CTZNSHP-CDE := 003
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Next_Half.getBoolean()))                                                                                                                        //Natural: IF #NEXT-HALF
        {
            ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().setValue(9);                                                                                        //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE := 9
            ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn().setValue("NI");                                                                                     //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-TRMNTE-RSN := 'NI'
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Pend_Cde().setValue("0");                                                                                            //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PEND-CDE := '0'
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde().setValue("0");                                                                                            //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-HOLD-CDE := '0'
        //*  121610 START
        if (condition(pdaAdsa600.getAdsa600_Adp_Hold_Cde().equals(pnd_Internal_Rollover)))                                                                                //Natural: IF ADSA600.ADP-HOLD-CDE = #INTERNAL-ROLLOVER
        {
            ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Hold_Cde().setValue("P");                                                                                        //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-HOLD-CDE := 'P'
            //*  121610 END
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde().setValue(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Rsdnc_Cde(), MoveOption.RightJustified);                  //Natural: MOVE RIGHT ADSA600.ADP-FRST-ANNT-RSDNC-CDE TO IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-RSDNCY-CDE
        setValueToSubstring("0",ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde(),1,1);                                                                          //Natural: MOVE '0' TO SUBSTR ( IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-RSDNCY-CDE,1,1 )
        if (condition(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Rsdnc_Cde().equals("3E")))                                                                                      //Natural: IF ADSA600.ADP-FRST-ANNT-RSDNC-CDE = '3E'
        {
            ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Rsdncy_Cde().setValue("035");                                                                                   //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-RSDNCY-CDE := '035'
            ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Local_Cde().setValue("03E");                                                                                     //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-LOCAL-CDE := '03E'
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Prtcpnt_Tax_Id_Nbr().setValue(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Ssn());                                                   //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.PRTCPNT-TAX-ID-NBR := ADSA600.ADP-FRST-ANNT-SSN
        pnd_Wdte6.setValueEdited(pdaAdsa600.getAdsa600_Adi_Finl_Periodic_Py_Dte(),new ReportEditMask("YYYYMM"));                                                          //Natural: MOVE EDITED ADSA600.ADI-FINL-PERIODIC-PY-DTE ( EM = YYYYMM ) TO #WDTE6
        if (condition(DbsUtil.is(pnd_Wdte6.getText(),"N6")))                                                                                                              //Natural: IF #WDTE6 IS ( N6 )
        {
            ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte()),  //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE := VAL ( #WDTE6 )
                pnd_Wdte6.val());
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Wdte8.setValueEdited(pdaAdsa600.getAdsa600_Adi_Finl_Prtl_Py_Dte(),new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED ADSA600.ADI-FINL-PRTL-PY-DTE ( EM = YYYYMMDD ) TO #WDTE8
        if (condition(DbsUtil.is(pnd_Wdte8.getText(),"N8")))                                                                                                              //Natural: IF #WDTE8 IS ( N8 )
        {
            ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Pay_Dte()),  //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PAY-DTE := VAL ( #WDTE8 )
                pnd_Wdte8.val());
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().setValue(pdaAdsa600.getAdsa600_Adi_Pymnt_Mode());                                                         //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND := ADSA600.ADI-PYMNT-MODE
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Lst_Trans_Dte());                                                         //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.LST-TRANS-DTE := IAA-CNTRCT.LST-TRANS-DTE
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Prev_Dist_Cde().setValue(pdaAdsa600.getAdsa600_Adp_Alt_Dest_Rlvr_Dest());                                            //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PREV-DIST-CDE := ADSA600.ADP-ALT-DEST-RLVR-DEST
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Curr_Dist_Cde().setValue(pdaAdsa600.getAdsa600_Adp_Alt_Dest_Rlvr_Dest());                                            //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-CURR-DIST-CDE := ADSA600.ADP-ALT-DEST-RLVR-DEST
        //*  EM 031708 START
        if (condition(pdaAdsa600.getAdsa600_Adp_Roth_Rqst_Ind().equals("Y")))                                                                                             //Natural: IF ADSA600.ADP-ROTH-RQST-IND = 'Y'
        {
            ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Roth_Dsblty_Dte().setValue(pdaAdsa600.getAdsa600_Adp_Roth_Dsblty_Dte());                                                //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.ROTH-DSBLTY-DTE := ADSA600.ADP-ROTH-DSBLTY-DTE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EM 031708 END
        //*  CM START STORE SURVIVOR RELATIONSHIP ON IA
        //*  EM - 113010 - STORE FOR SIPS
        //*  EM - 050715 - START
        //*  IF ADP-SRVVR-IND = 'Y' OR SUBSTRING (ADP-PLAN-NBR,1,3) = 'SIP'
        if (condition(pdaAdsa600.getAdsa600_Adp_Bene_Client_Ind().equals("1") || pdaAdsa600.getAdsa600_Adp_Bene_Client_Ind().equals("2") || pdaAdsa600.getAdsa600_Adp_Bene_Client_Ind().equals("3"))) //Natural: IF ADP-BENE-CLIENT-IND = '1' OR = '2' OR = '3'
        {
            short decideConditionsMet1654 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF ADSA600.ADP-SRVVR-RLTNSHP;//Natural: VALUE '1', '4', '7'
            if (condition((pdaAdsa600.getAdsa600_Adp_Srvvr_Rltnshp().equals("1") || pdaAdsa600.getAdsa600_Adp_Srvvr_Rltnshp().equals("4") || pdaAdsa600.getAdsa600_Adp_Srvvr_Rltnshp().equals("7"))))
            {
                decideConditionsMet1654++;
                ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde().setValue("S");                                                                          //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-EMPLYMNT-TRMNT-CDE := 'S'
            }                                                                                                                                                             //Natural: VALUE '2', '5', '6'
            else if (condition((pdaAdsa600.getAdsa600_Adp_Srvvr_Rltnshp().equals("2") || pdaAdsa600.getAdsa600_Adp_Srvvr_Rltnshp().equals("5") || pdaAdsa600.getAdsa600_Adp_Srvvr_Rltnshp().equals("6"))))
            {
                decideConditionsMet1654++;
                ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde().setValue("B");                                                                          //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-EMPLYMNT-TRMNT-CDE := 'B'
            }                                                                                                                                                             //Natural: VALUE '3'
            else if (condition((pdaAdsa600.getAdsa600_Adp_Srvvr_Rltnshp().equals("3"))))
            {
                decideConditionsMet1654++;
                ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Emplymnt_Trmnt_Cde().setValue("A");                                                                          //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-EMPLYMNT-TRMNT-CDE := 'A'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        SP:                                                                                                                                                               //Natural: STORE IAA-CNTRCT-PRTCPNT-ROLE
        ldaAdsl601.getVw_iaa_Cntrct_Prtcpnt_Role().insertDBRow("SP");
    }
    private void sub_Create_Tiaa_Fund_Rcrd() throws Exception                                                                                                             //Natural: CREATE-TIAA-FUND-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().setValue(ldaAdsl601.getIaa_Cntrct_Cntrct_Ppcn_Nbr());                                                     //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR := IAA-CNTRCT.CNTRCT-PPCN-NBR
        ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().setValue(1);                                                                                             //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE := 01
        ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Lst_Trans_Dte());                                                              //Natural: ASSIGN IAA-TIAA-FUND-RCRD.LST-TRANS-DTE := IAA-CNTRCT.LST-TRANS-DTE
        pnd_Rate_Dte_S.reset();                                                                                                                                           //Natural: RESET #RATE-DTE-S #RATE-DTE-G
        pnd_Rate_Dte_G.reset();
        if (condition(ldaAdsl601.getIaa_Cntrct_Cntrct_Issue_Dte_Dd().greater(1)))                                                                                         //Natural: IF IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD GT 01
        {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-RATE-DATE
            sub_Calculate_Rate_Date();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_No_Pair.getBoolean()))                                                                                                                          //Natural: IF #NO-PAIR
        {
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().setValue("T1S");                                                                                       //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE := 'T1S'
            ldaAdsl601.getVw_iaa_Tiaa_Fund_Rcrd().insertDBRow();                                                                                                          //Natural: STORE IAA-TIAA-FUND-RCRD
                                                                                                                                                                          //Natural: PERFORM CREATE-TIAA-FUND-TRANS
            sub_Create_Tiaa_Fund_Trans();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  030212
        //*  030212
        //*  030212
        //*  030212
        if (condition(pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue("*").greater(getZero()) || pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue("*").greater(getZero()))) //Natural: IF ADSA600.#ADI-DTL-TIAA-STD-DVDND-AMT ( * ) GT 0 OR ADSA600.#ADI-DTL-TIAA-STD-GRNTD-AMT ( * ) GT 0
        {
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().setValue("T1S");                                                                                       //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE := 'T1S'
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt()),                //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT := 0 + ADSA600.#ADI-DTL-TIAA-STD-GRNTD-AMT ( * )
                DbsField.add(getZero(),pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue("*")));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt()),                //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT := 0 + ADSA600.#ADI-DTL-TIAA-STD-DVDND-AMT ( * )
                DbsField.add(getZero(),pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue("*")));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt()),                //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-OLD-PER-AMT := 0 + #OLD-TIAA-STD-GRNTD-AMT
                DbsField.add(getZero(),pdaAdsa600.getAdsa600_Pnd_Old_Tiaa_Std_Grntd_Amt()));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt()),                //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-OLD-DIV-AMT := 0 + #OLD-TIAA-STD-DVDND-AMT
                DbsField.add(getZero(),pdaAdsa600.getAdsa600_Pnd_Old_Tiaa_Std_Dvdnd_Amt()));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue("*").setValue(pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue("*"));                   //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( * ) := ADSA600.#ADI-DTL-TIAA-IA-RATE-CD ( * )
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue("*").setValue(pdaAdsa600.getAdsa600_Pnd_Adi_Contract_Id().getValue("*"));                           //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( * ) := ADSA600.#ADI-CONTRACT-ID ( * )
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue("*").setValue(pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue("*"));             //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( * ) := ADSA600.#ADI-DTL-TIAA-STD-GRNTD-AMT ( * )
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue("*").setValue(pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue("*"));             //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( * ) := ADSA600.#ADI-DTL-TIAA-STD-DVDND-AMT ( * )
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue("*").setValue(pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue("*"));         //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( * ) := ADSA600.#ADI-DTL-FNL-STD-PAY-AMT ( * )
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue("*").setValue(pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue("*"));       //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( * ) := ADSA600.#ADI-DTL-FNL-STD-DVDND-AMT ( * )
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind().setValue(pdaAdsa600.getAdsa600_Adi_Pymnt_Mode());                                                            //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-MODE-IND := ADSA600.ADI-PYMNT-MODE
            pnd_Std.setValue(true);                                                                                                                                       //Natural: ASSIGN #STD := TRUE
                                                                                                                                                                          //Natural: PERFORM COLLAPSE-TIAA-RATES
            sub_Collapse_Tiaa_Rates();
            if (condition(Global.isEscape())) {return;}
            ldaAdsl601.getVw_iaa_Tiaa_Fund_Rcrd().insertDBRow();                                                                                                          //Natural: STORE IAA-TIAA-FUND-RCRD
                                                                                                                                                                          //Natural: PERFORM CREATE-TIAA-FUND-TRANS
            sub_Create_Tiaa_Fund_Trans();
            if (condition(Global.isEscape())) {return;}
            pnd_Std.reset();                                                                                                                                              //Natural: RESET #STD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue("*").greater(getZero()) || pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue("*").greater(getZero()))) //Natural: IF ADSA600.#ADI-DTL-TIAA-GRD-GRNTD-AMT ( * ) GT 0 OR ADSA600.#ADI-DTL-TIAA-GRD-DVDND-AMT ( * ) GT 0
        {
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().setValue("T1G");                                                                                       //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE := 'T1G'
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt()),                //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT := 0 + ADSA600.#ADI-DTL-TIAA-GRD-GRNTD-AMT ( * )
                DbsField.add(getZero(),pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue("*")));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt()),                //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT := 0 + ADSA600.#ADI-DTL-TIAA-GRD-DVDND-AMT ( * )
                DbsField.add(getZero(),pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue("*")));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt()),                //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-OLD-PER-AMT := 0 + #OLD-TIAA-GRD-GRNTD-AMT
                DbsField.add(getZero(),pdaAdsa600.getAdsa600_Pnd_Old_Tiaa_Grd_Grntd_Amt()));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt()),                //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-OLD-DIV-AMT := 0 + #OLD-TIAA-GRD-DVDND-AMT
                DbsField.add(getZero(),pdaAdsa600.getAdsa600_Pnd_Old_Tiaa_Grd_Dvdnd_Amt()));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue("*").setValue(pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue("*"));                   //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( * ) := ADSA600.#ADI-DTL-TIAA-IA-RATE-CD ( * )
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue("*").setValue(pdaAdsa600.getAdsa600_Pnd_Adi_Contract_Id().getValue("*"));                           //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( * ) := ADSA600.#ADI-CONTRACT-ID ( * )
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue("*").setValue(pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue("*"));             //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( * ) := ADSA600.#ADI-DTL-TIAA-GRD-GRNTD-AMT ( * )
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue("*").setValue(pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue("*"));             //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( * ) := ADSA600.#ADI-DTL-TIAA-GRD-DVDND-AMT ( * )
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue("*").setValue(pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue("*"));         //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( * ) := ADSA600.#ADI-DTL-FNL-GRD-PAY-AMT ( * )
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue("*").setValue(pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue("*"));       //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( * ) := ADSA600.#ADI-DTL-FNL-GRD-DVDND-AMT ( * )
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind().setValue(pdaAdsa600.getAdsa600_Adi_Pymnt_Mode());                                                            //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-MODE-IND := ADSA600.ADI-PYMNT-MODE
            pnd_Grd.setValue(true);                                                                                                                                       //Natural: ASSIGN #GRD := TRUE
                                                                                                                                                                          //Natural: PERFORM COLLAPSE-TIAA-RATES
            sub_Collapse_Tiaa_Rates();
            if (condition(Global.isEscape())) {return;}
            ldaAdsl601.getVw_iaa_Tiaa_Fund_Rcrd().insertDBRow();                                                                                                          //Natural: STORE IAA-TIAA-FUND-RCRD
                                                                                                                                                                          //Natural: PERFORM CREATE-TIAA-FUND-TRANS
            sub_Create_Tiaa_Fund_Trans();
            if (condition(Global.isEscape())) {return;}
            pnd_Grd.reset();                                                                                                                                              //Natural: RESET #GRD
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_Rea_Fund_Rcrd() throws Exception                                                                                                              //Natural: CREATE-REA-FUND-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr().setValue(ldaAdsl601.getIaa_Cntrct_Cntrct_Ppcn_Nbr());                                                   //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CNTRCT-PPCN-NBR := IAA-CNTRCT.CNTRCT-PPCN-NBR
        ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Payee_Cde().setValue(1);                                                                                           //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CNTRCT-PAYEE-CDE := 01
        DbsUtil.examine(new ExamineSource(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue("*")), new ExamineSearch("R"), new ExamineGivingIndex(pnd_I));            //Natural: EXAMINE ADSA600.ADI-DTL-CREF-ACCT-CD ( * ) FOR 'R' GIVING INDEX #I
        if (condition(pnd_I.greater(getZero())))                                                                                                                          //Natural: IF #I GT 0
        {
            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde().getValue(1).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Ia_Rate_Cd().getValue(pnd_I));                     //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-RATE-CDE ( 1 ) := ADSA600.ADI-DTL-CREF-IA-RATE-CD ( #I )
            //*  061411 START
            if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_I).greater(new DbsDecimal("9999.9999"))))                                    //Natural: IF ADSA600.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #I ) GT 9999.9999
            {
                pnd_Err_Cde.setValue("L74");                                                                                                                              //Natural: ASSIGN #ERR-CDE := 'L74'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  061411 END
            if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_I).greater(getZero())))                                                      //Natural: IF ADSA600.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #I ) GT 0
            {
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt().getValue(1).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_I));          //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-UNITS-CNT ( 1 ) := ADSA600.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #I )
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde().setValue("W");                                                                                        //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CMPNY-CDE := 'W'
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde().setValue("09");                                                                                        //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE := '09'
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Lst_Trans_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Lst_Trans_Dte());                                                    //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.LST-TRANS-DTE := IAA-CNTRCT.LST-TRANS-DTE
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt().reset();                                                                                            //Natural: RESET IAA-CREF-FUND-RCRD-1.CREF-TOT-PER-AMT IAA-CREF-FUND-RCRD-1.CREF-UNIT-VAL
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Unit_Val().reset();
                ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1().insertDBRow();                                                                                                    //Natural: STORE IAA-CREF-FUND-RCRD-1
                                                                                                                                                                          //Natural: PERFORM CREATE-CREF-FUND-TRANS
                sub_Create_Cref_Fund_Trans();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  061411 START
            if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_I).greater(new DbsDecimal("9999.9999"))))                                      //Natural: IF ADSA600.ADI-DTL-CREF-ANNL-NBR-UNITS ( #I ) GT 9999.9999
            {
                pnd_Err_Cde.setValue("L74");                                                                                                                              //Natural: ASSIGN #ERR-CDE := 'L74'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  061411 END
            if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_I).greater(getZero())))                                                        //Natural: IF ADSA600.ADI-DTL-CREF-ANNL-NBR-UNITS ( #I ) GT 0
            {
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt().getValue(1).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_I));            //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-UNITS-CNT ( 1 ) := ADSA600.ADI-DTL-CREF-ANNL-NBR-UNITS ( #I )
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde().setValue("U");                                                                                        //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CMPNY-CDE := 'U'
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde().setValue("09");                                                                                        //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE := '09'
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Lst_Trans_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Lst_Trans_Dte());                                                    //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.LST-TRANS-DTE := IAA-CNTRCT.LST-TRANS-DTE
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt().setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_I));                         //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-TOT-PER-AMT := ADSA600.ADI-DTL-CREF-DA-ANNL-AMT ( #I )
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Unit_Val().setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Ia_Annl_Unit_Val().getValue(pnd_I));                       //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-UNIT-VAL := ADSA600.ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #I )
                ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1().insertDBRow();                                                                                                    //Natural: STORE IAA-CREF-FUND-RCRD-1
                                                                                                                                                                          //Natural: PERFORM CREATE-CREF-FUND-TRANS
                sub_Create_Cref_Fund_Trans();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* * EM - 012309 START
        DbsUtil.examine(new ExamineSource(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue("*")), new ExamineSearch("D"), new ExamineGivingIndex(pnd_I));            //Natural: EXAMINE ADSA600.ADI-DTL-CREF-ACCT-CD ( * ) FOR 'D' GIVING INDEX #I
        if (condition(pnd_I.greater(getZero())))                                                                                                                          //Natural: IF #I GT 0
        {
            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde().getValue(1).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Ia_Rate_Cd().getValue(pnd_I));                     //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-RATE-CDE ( 1 ) := ADSA600.ADI-DTL-CREF-IA-RATE-CD ( #I )
            //*  061411 START
            if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_I).greater(new DbsDecimal("9999.9999"))))                                    //Natural: IF ADSA600.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #I ) GT 9999.9999
            {
                pnd_Err_Cde.setValue("L74");                                                                                                                              //Natural: ASSIGN #ERR-CDE := 'L74'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  061411 END
            if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_I).greater(getZero())))                                                      //Natural: IF ADSA600.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #I ) GT 0
            {
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt().getValue(1).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_I));          //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-UNITS-CNT ( 1 ) := ADSA600.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #I )
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde().setValue("W");                                                                                        //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CMPNY-CDE := 'W'
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde().setValue("11");                                                                                        //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE := '11'
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Lst_Trans_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Lst_Trans_Dte());                                                    //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.LST-TRANS-DTE := IAA-CNTRCT.LST-TRANS-DTE
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt().reset();                                                                                            //Natural: RESET IAA-CREF-FUND-RCRD-1.CREF-TOT-PER-AMT IAA-CREF-FUND-RCRD-1.CREF-UNIT-VAL
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Unit_Val().reset();
                ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1().insertDBRow();                                                                                                    //Natural: STORE IAA-CREF-FUND-RCRD-1
                                                                                                                                                                          //Natural: PERFORM CREATE-CREF-FUND-TRANS
                sub_Create_Cref_Fund_Trans();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  061411 START
            if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_I).greater(new DbsDecimal("9999.9999"))))                                      //Natural: IF ADSA600.ADI-DTL-CREF-ANNL-NBR-UNITS ( #I ) GT 9999.9999
            {
                pnd_Err_Cde.setValue("L74");                                                                                                                              //Natural: ASSIGN #ERR-CDE := 'L74'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  061411 END
            if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_I).greater(getZero())))                                                        //Natural: IF ADSA600.ADI-DTL-CREF-ANNL-NBR-UNITS ( #I ) GT 0
            {
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt().getValue(1).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_I));            //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-UNITS-CNT ( 1 ) := ADSA600.ADI-DTL-CREF-ANNL-NBR-UNITS ( #I )
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde().setValue("U");                                                                                        //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CMPNY-CDE := 'U'
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde().setValue("11");                                                                                        //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE := '11'
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Lst_Trans_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Lst_Trans_Dte());                                                    //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.LST-TRANS-DTE := IAA-CNTRCT.LST-TRANS-DTE
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt().setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_I));                         //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-TOT-PER-AMT := ADSA600.ADI-DTL-CREF-DA-ANNL-AMT ( #I )
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Unit_Val().setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Ia_Annl_Unit_Val().getValue(pnd_I));                       //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-UNIT-VAL := ADSA600.ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #I )
                ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1().insertDBRow();                                                                                                    //Natural: STORE IAA-CREF-FUND-RCRD-1
                                                                                                                                                                          //Natural: PERFORM CREATE-CREF-FUND-TRANS
                sub_Create_Cref_Fund_Trans();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* * EM - 012309 END
    }
    private void sub_Create_Cref_Fund_Rcrd() throws Exception                                                                                                             //Natural: CREATE-CREF-FUND-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr().setValue(ldaAdsl601.getIaa_Cntrct_Cntrct_Ppcn_Nbr());                                                   //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CNTRCT-PPCN-NBR := IAA-CNTRCT.CNTRCT-PPCN-NBR
        ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Payee_Cde().setValue(1);                                                                                           //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CNTRCT-PAYEE-CDE := 01
        if (condition(pnd_No_Pair.getBoolean()))                                                                                                                          //Natural: IF #NO-PAIR
        {
            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Fund_Cde().setValue("202");                                                                                     //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CMPNY-FUND-CDE := '202'
            ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1().insertDBRow();                                                                                                        //Natural: STORE IAA-CREF-FUND-RCRD-1
                                                                                                                                                                          //Natural: PERFORM CREATE-CREF-FUND-TRANS
            sub_Create_Cref_Fund_Trans();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR04:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_I).greater(" ")))                                                                     //Natural: IF ADSA600.ADI-DTL-CREF-ACCT-CD ( #I ) GT ' '
            {
                //*  EM - 012309
                if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_I).equals("T") || pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_I).equals("G")  //Natural: IF ADSA600.ADI-DTL-CREF-ACCT-CD ( #I ) = 'T' OR = 'G' OR = 'R' OR = 'D'
                    || pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_I).equals("R") || pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_I).equals("D")))
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde().reset();                                                                                               //Natural: RESET IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE
                //* ** TEMP ***
                getReports().write(0, "BEFORE CALL TO IAAN0511");                                                                                                         //Natural: WRITE 'BEFORE CALL TO IAAN0511'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "=",pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_I));                                                                  //Natural: WRITE '=' ADSA600.ADI-DTL-CREF-ACCT-CD ( #I )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* ** TEMP ***
                DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_I), ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde(),  //Natural: CALLNAT 'IAAN0511' ADSA600.ADI-DTL-CREF-ACCT-CD ( #I ) IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE #RET-CDE
                    pnd_Ret_Cde);
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                //* ** TEMP ***
                getReports().write(0, "AFTER CALL TO IAAN0511");                                                                                                          //Natural: WRITE 'AFTER CALL TO IAAN0511'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "=",ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde());                                                                            //Natural: WRITE '=' IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, "=",pnd_Ret_Cde);                                                                                                                   //Natural: WRITE '=' #RET-CDE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* ** TEMP ***
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde().getValue(1).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Ia_Rate_Cd().getValue(pnd_I));                     //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-RATE-CDE ( 1 ) := ADSA600.ADI-DTL-CREF-IA-RATE-CD ( #I )
            //*  061411 START
            if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_I).greater(new DbsDecimal("9999.9999"))))                                    //Natural: IF ADSA600.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #I ) GT 9999.9999
            {
                pnd_Err_Cde.setValue("L74");                                                                                                                              //Natural: ASSIGN #ERR-CDE := 'L74'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  061411 END
            if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_I).greater(getZero())))                                                      //Natural: IF ADSA600.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #I ) GT 0
            {
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt().getValue(1).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_I));          //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-UNITS-CNT ( 1 ) := ADSA600.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #I )
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt().reset();                                                                                            //Natural: RESET IAA-CREF-FUND-RCRD-1.CREF-TOT-PER-AMT IAA-CREF-FUND-RCRD-1.CREF-UNIT-VAL
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Unit_Val().reset();
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde().setValue("4");                                                                                        //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CMPNY-CDE := '4'
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Lst_Trans_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Lst_Trans_Dte());                                                    //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.LST-TRANS-DTE := IAA-CNTRCT.LST-TRANS-DTE
                ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1().insertDBRow();                                                                                                    //Natural: STORE IAA-CREF-FUND-RCRD-1
                                                                                                                                                                          //Natural: PERFORM CREATE-CREF-FUND-TRANS
                sub_Create_Cref_Fund_Trans();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  061411 START
            if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_I).greater(new DbsDecimal("9999.9999"))))                                      //Natural: IF ADSA600.ADI-DTL-CREF-ANNL-NBR-UNITS ( #I ) GT 9999.9999
            {
                pnd_Err_Cde.setValue("L74");                                                                                                                              //Natural: ASSIGN #ERR-CDE := 'L74'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  061411 END
            if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_I).greater(getZero())))                                                        //Natural: IF ADSA600.ADI-DTL-CREF-ANNL-NBR-UNITS ( #I ) GT 0
            {
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt().getValue(1).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_I));            //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-UNITS-CNT ( 1 ) := ADSA600.ADI-DTL-CREF-ANNL-NBR-UNITS ( #I )
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt().setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_I));                         //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-TOT-PER-AMT := ADSA600.ADI-DTL-CREF-DA-ANNL-AMT ( #I )
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Unit_Val().setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Ia_Annl_Unit_Val().getValue(pnd_I));                       //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-UNIT-VAL := ADSA600.ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #I )
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde().setValue("2");                                                                                        //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CMPNY-CDE := '2'
                ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Lst_Trans_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Lst_Trans_Dte());                                                    //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.LST-TRANS-DTE := IAA-CNTRCT.LST-TRANS-DTE
                ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1().insertDBRow();                                                                                                    //Natural: STORE IAA-CREF-FUND-RCRD-1
                                                                                                                                                                          //Natural: PERFORM CREATE-CREF-FUND-TRANS
                sub_Create_Cref_Fund_Trans();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Create_Cntrct_Trans() throws Exception                                                                                                               //Natural: CREATE-CNTRCT-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaAdsl601.getVw_iaa_Cntrct_Trans().setValuesByName(ldaAdsl601.getVw_iaa_Cntrct());                                                                               //Natural: MOVE BY NAME IAA-CNTRCT TO IAA-CNTRCT-TRANS
        ldaAdsl601.getIaa_Cntrct_Trans_Trans_Check_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cntrct_Trans_Trans_Check_Dte()), pdaAdsa600.getAdsa600_Pnd_Ia_Check_Dte().val()); //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-CHECK-DTE := VAL ( #IA-CHECK-DTE )
        ldaAdsl601.getIaa_Cntrct_Trans_Trans_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Lst_Trans_Dte());                                                                    //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-DTE := IAA-CNTRCT.LST-TRANS-DTE
        //*  010512 START
        //*  010512 END
        ldaAdsl601.getIaa_Cntrct_Trans_Invrse_Trans_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cntrct_Trans_Invrse_Trans_Dte()), new                    //Natural: COMPUTE IAA-CNTRCT-TRANS.INVRSE-TRANS-DTE = 1000000000000 - IAA-CNTRCT.LST-TRANS-DTE
            DbsDecimal("1000000000000").subtract(ldaAdsl601.getIaa_Cntrct_Lst_Trans_Dte()));
        ldaAdsl601.getIaa_Cntrct_Trans_Aftr_Imge_Id().setValue("2");                                                                                                      //Natural: ASSIGN IAA-CNTRCT-TRANS.AFTR-IMGE-ID := '2'
        ldaAdsl601.getIaa_Cntrct_Trans_Sub_Plan_Nmbr().setValue(pdaAdsa600.getAdsa600_Adc_Sub_Plan_Nbr());                                                                //Natural: ASSIGN IAA-CNTRCT-TRANS.SUB-PLAN-NMBR := ADSA600.ADC-SUB-PLAN-NBR
        ldaAdsl601.getIaa_Cntrct_Trans_Orgntng_Cntrct_Nmbr().setValue(pdaAdsa600.getAdsa600_Adc_Original_Cntrct_Nbr());                                                   //Natural: ASSIGN IAA-CNTRCT-TRANS.ORGNTNG-CNTRCT-NMBR := ADSA600.ADC-ORIGINAL-CNTRCT-NBR
        ldaAdsl601.getIaa_Cntrct_Trans_Orgntng_Sub_Plan_Nmbr().setValue(pdaAdsa600.getAdsa600_Adc_Original_Sub_Plan_Nbr());                                               //Natural: ASSIGN IAA-CNTRCT-TRANS.ORGNTNG-SUB-PLAN-NMBR := ADSA600.ADC-ORIGINAL-SUB-PLAN-NBR
        ldaAdsl601.getVw_iaa_Cntrct_Trans().insertDBRow();                                                                                                                //Natural: STORE IAA-CNTRCT-TRANS
    }
    private void sub_Create_Cpr_Trans() throws Exception                                                                                                                  //Natural: CREATE-CPR-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaAdsl601.getVw_iaa_Cpr_Trans().setValuesByName(ldaAdsl601.getVw_iaa_Cntrct_Prtcpnt_Role());                                                                     //Natural: MOVE BY NAME IAA-CNTRCT-PRTCPNT-ROLE TO IAA-CPR-TRANS
        ldaAdsl601.getIaa_Cpr_Trans_Trans_Check_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Trans_Trans_Check_Dte());                                                         //Natural: ASSIGN IAA-CPR-TRANS.TRANS-CHECK-DTE := IAA-CNTRCT-TRANS.TRANS-CHECK-DTE
        ldaAdsl601.getIaa_Cpr_Trans_Trans_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Lst_Trans_Dte());                                                                       //Natural: ASSIGN IAA-CPR-TRANS.TRANS-DTE := IAA-CNTRCT.LST-TRANS-DTE
        ldaAdsl601.getIaa_Cpr_Trans_Invrse_Trans_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Trans_Invrse_Trans_Dte());                                                       //Natural: ASSIGN IAA-CPR-TRANS.INVRSE-TRANS-DTE := IAA-CNTRCT-TRANS.INVRSE-TRANS-DTE
        ldaAdsl601.getIaa_Cpr_Trans_Aftr_Imge_Id().setValue("2");                                                                                                         //Natural: ASSIGN IAA-CPR-TRANS.AFTR-IMGE-ID := '2'
        ldaAdsl601.getVw_iaa_Cpr_Trans().insertDBRow();                                                                                                                   //Natural: STORE IAA-CPR-TRANS
    }
    private void sub_Create_Tiaa_Fund_Trans() throws Exception                                                                                                            //Natural: CREATE-TIAA-FUND-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaAdsl601.getVw_iaa_Tiaa_Fund_Trans().setValuesByName(ldaAdsl601.getVw_iaa_Tiaa_Fund_Rcrd());                                                                    //Natural: MOVE BY NAME IAA-TIAA-FUND-RCRD TO IAA-TIAA-FUND-TRANS
        ldaAdsl601.getIaa_Tiaa_Fund_Trans_Trans_Check_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Trans_Trans_Check_Dte());                                                   //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-CHECK-DTE := IAA-CNTRCT-TRANS.TRANS-CHECK-DTE
        ldaAdsl601.getIaa_Tiaa_Fund_Trans_Trans_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Lst_Trans_Dte());                                                                 //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-DTE := IAA-CNTRCT.LST-TRANS-DTE
        ldaAdsl601.getIaa_Tiaa_Fund_Trans_Invrse_Trans_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Trans_Invrse_Trans_Dte());                                                 //Natural: ASSIGN IAA-TIAA-FUND-TRANS.INVRSE-TRANS-DTE := IAA-CNTRCT-TRANS.INVRSE-TRANS-DTE
        ldaAdsl601.getIaa_Tiaa_Fund_Trans_Aftr_Imge_Id().setValue("2");                                                                                                   //Natural: ASSIGN IAA-TIAA-FUND-TRANS.AFTR-IMGE-ID := '2'
        ldaAdsl601.getVw_iaa_Tiaa_Fund_Trans().insertDBRow();                                                                                                             //Natural: STORE IAA-TIAA-FUND-TRANS
    }
    private void sub_Create_Cref_Fund_Trans() throws Exception                                                                                                            //Natural: CREATE-CREF-FUND-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaAdsl601.getVw_iaa_Cref_Fund_Trans_1().setValuesByName(ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1());                                                                //Natural: MOVE BY NAME IAA-CREF-FUND-RCRD-1 TO IAA-CREF-FUND-TRANS-1
        ldaAdsl601.getIaa_Cref_Fund_Trans_1_Trans_Check_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Trans_Trans_Check_Dte());                                                 //Natural: ASSIGN IAA-CREF-FUND-TRANS-1.TRANS-CHECK-DTE := IAA-CNTRCT-TRANS.TRANS-CHECK-DTE
        ldaAdsl601.getIaa_Cref_Fund_Trans_1_Trans_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Lst_Trans_Dte());                                                               //Natural: ASSIGN IAA-CREF-FUND-TRANS-1.TRANS-DTE := IAA-CNTRCT.LST-TRANS-DTE
        ldaAdsl601.getIaa_Cref_Fund_Trans_1_Invrse_Trans_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Trans_Invrse_Trans_Dte());                                               //Natural: ASSIGN IAA-CREF-FUND-TRANS-1.INVRSE-TRANS-DTE := IAA-CNTRCT-TRANS.INVRSE-TRANS-DTE
        ldaAdsl601.getIaa_Cref_Fund_Trans_1_Aftr_Imge_Id().setValue("2");                                                                                                 //Natural: ASSIGN IAA-CREF-FUND-TRANS-1.AFTR-IMGE-ID := '2'
        ldaAdsl601.getVw_iaa_Cref_Fund_Trans_1().insertDBRow();                                                                                                           //Natural: STORE IAA-CREF-FUND-TRANS-1
    }
    private void sub_Create_Trans_Rcrd() throws Exception                                                                                                                 //Natural: CREATE-TRANS-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        ldaAdsl601.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr().setValue(ldaAdsl601.getIaa_Cntrct_Cntrct_Ppcn_Nbr());                                                               //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PPCN-NBR := IAA-CNTRCT.CNTRCT-PPCN-NBR
        ldaAdsl601.getIaa_Trans_Rcrd_Trans_Payee_Cde().setValue(1);                                                                                                       //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PAYEE-CDE := 01
        ldaAdsl601.getIaa_Trans_Rcrd_Trans_Cde().setValue(31);                                                                                                            //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 31
        ldaAdsl601.getIaa_Trans_Rcrd_Trans_User_Area().setValue("ADS");                                                                                                   //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-AREA := 'ADS'
        ldaAdsl601.getIaa_Trans_Rcrd_Trans_User_Id().setValue("ADSN601");                                                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-ID := 'ADSN601'
        ldaAdsl601.getIaa_Trans_Rcrd_Lst_Trans_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Lst_Trans_Dte());                                                                  //Natural: ASSIGN IAA-TRANS-RCRD.LST-TRANS-DTE := IAA-CNTRCT.LST-TRANS-DTE
        ldaAdsl601.getIaa_Trans_Rcrd_Trans_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Lst_Trans_Dte());                                                                      //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-DTE := IAA-CNTRCT.LST-TRANS-DTE
        ldaAdsl601.getIaa_Trans_Rcrd_Invrse_Trans_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Trans_Invrse_Trans_Dte());                                                      //Natural: ASSIGN IAA-TRANS-RCRD.INVRSE-TRANS-DTE := IAA-CNTRCT-TRANS.INVRSE-TRANS-DTE
        ldaAdsl601.getIaa_Trans_Rcrd_Trans_Check_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Trans_Rcrd_Trans_Check_Dte()), pdaAdsa600.getAdsa600_Pnd_Ia_Check_Dte().val()); //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CHECK-DTE := VAL ( #IA-CHECK-DTE )
        pnd_Wdte8.setValueEdited(pnd_Bsnss_Dte,new ReportEditMask("YYYYMMDD"));                                                                                           //Natural: MOVE EDITED #BSNSS-DTE ( EM = YYYYMMDD ) TO #WDTE8
        ldaAdsl601.getIaa_Trans_Rcrd_Trans_Todays_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Trans_Rcrd_Trans_Todays_Dte()), pnd_Wdte8.val());          //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-TODAYS-DTE := VAL ( #WDTE8 )
        ldaAdsl601.getIaa_Trans_Rcrd_Trans_Sub_Cde().reset();                                                                                                             //Natural: RESET IAA-TRANS-RCRD.TRANS-SUB-CDE
        if (condition(pnd_Next_Half.getBoolean()))                                                                                                                        //Natural: IF #NEXT-HALF
        {
            ldaAdsl601.getIaa_Trans_Rcrd_Trans_Sub_Cde().setValue("NI");                                                                                                  //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-SUB-CDE := 'NI'
            ldaAdsl601.getVw_iaa_Trans_Rcrd().insertDBRow();                                                                                                              //Natural: STORE IAA-TRANS-RCRD
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl601.getVw_iaa_Trans_Rcrd().insertDBRow();                                                                                                                  //Natural: STORE IAA-TRANS-RCRD
        if (condition(pnd_Ivc_Gd_Ind.equals("Y")))                                                                                                                        //Natural: IF #IVC-GD-IND = 'Y'
        {
            pnd_Ivc_Gd_Ind.reset();                                                                                                                                       //Natural: RESET #IVC-GD-IND
            ldaAdsl601.getIaa_Trans_Rcrd_Trans_Cde().setValue(724);                                                                                                       //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 724
            ldaAdsl601.getVw_iaa_Trans_Rcrd().insertDBRow();                                                                                                              //Natural: STORE IAA-TRANS-RCRD
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  022108
    private void sub_Collapse_Tiaa_Rates() throws Exception                                                                                                               //Natural: COLLAPSE-TIAA-RATES
    {
        if (BLNatReinput.isReinput()) return;

        FOR05:                                                                                                                                                            //Natural: FOR #I 1 TO #MAX-RATES
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Rates)); pnd_I.nadd(1))
        {
            if (condition(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I).greater(" ")))                                                                 //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I ) GT ' '
            {
                if (condition(pnd_Std.getBoolean()))                                                                                                                      //Natural: IF #STD
                {
                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_I).setValue(pnd_Rate_Dte_S);                                                            //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( #I ) := #RATE-DTE-S
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Grd.getBoolean()))                                                                                                                      //Natural: IF #GRD
                {
                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_I).setValue(pnd_Rate_Dte_G);                                                            //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( #I ) := #RATE-DTE-G
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I).equals(getZero()) && ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I).equals(getZero())  //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #I ) = 0 AND IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #I ) = 0 AND IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #I ) = 0 AND IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #I ) = 0
                    && ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_I).equals(getZero()) && ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_I).equals(getZero())))
                {
                    //*  030212
                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I).reset();                                                                             //Natural: RESET IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I ) IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( #I ) IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( #I )
                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_I).reset();
                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(pnd_I).reset();
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_K.compute(new ComputeParameters(false, pnd_K), pnd_I.add(1));                                                                                         //Natural: ASSIGN #K := #I + 1
                //*  022108
                if (condition(pnd_K.greater(pnd_Max_Rates)))                                                                                                              //Natural: IF #K GT #MAX-RATES
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //*  022108
                }                                                                                                                                                         //Natural: END-IF
                FOR06:                                                                                                                                                    //Natural: FOR #J #K TO #MAX-RATES
                for (pnd_J.setValue(pnd_K); condition(pnd_J.lessOrEqual(pnd_Max_Rates)); pnd_J.nadd(1))
                {
                    if (condition(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_J).equals(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I)))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #J ) = IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I )
                    {
                        ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I).nadd(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_J));    //Natural: ADD IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #I )
                        ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I).nadd(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_J));    //Natural: ADD IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #I )
                        ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_I).nadd(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_J)); //Natural: ADD IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #I )
                        ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_I).nadd(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_J)); //Natural: ADD IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #I )
                        //*  030212
                        ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_J).reset();                                                                         //Natural: RESET IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #J ) IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( #J ) IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( #J ) IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #J ) IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #J ) IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #J ) IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #J )
                        ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(pnd_J).reset();
                        ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_J).reset();
                        ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_J).reset();
                        ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_J).reset();
                        ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_J).reset();
                        ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_J).reset();
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  022108
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR07:                                                                                                                                                            //Natural: FOR #I 1 TO #MAX-RATES
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Rates)); pnd_I.nadd(1))
        {
            if (condition(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I).equals(" ")))                                                                  //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I ) = ' '
            {
                //*  022108
                if (condition(pnd_I.equals(pnd_Max_Rates)))                                                                                                               //Natural: IF #I = #MAX-RATES
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_K.compute(new ComputeParameters(false, pnd_K), pnd_I.add(1));                                                                                         //Natural: ASSIGN #K := #I + 1
                //*    IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE(#K : 80) GT ' '       /* 022108
                //*  022108
                if (condition(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_K,":",pnd_Max_Rates).greater(" ")))                                           //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #K:#MAX-RATES ) GT ' '
                {
                    REPEAT01:                                                                                                                                             //Natural: REPEAT
                    while (condition(whileTrue))
                    {
                        //*  030212
                        if (condition(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_K).greater(" ")))                                                     //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #K ) GT ' '
                        {
                            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_K));  //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #K )
                            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(pnd_I).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(pnd_K));  //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( #I ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( #K )
                            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_K)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #I ) := IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #K )
                            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_K)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #I ) := IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #K )
                            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_I).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_K)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #I ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #K )
                            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_I).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_K)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #I ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #K )
                            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_I).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_K));  //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( #I ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( #K )
                            //*  030212
                            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_K).reset();                                                                     //Natural: RESET IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #K ) IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( #K ) IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( #K ) IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #K ) IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #K ) IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #K ) IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #K )
                            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(pnd_K).reset();
                            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_K).reset();
                            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_K).reset();
                            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_K).reset();
                            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_K).reset();
                            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_K).reset();
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_K.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #K
                    }                                                                                                                                                     //Natural: END-REPEAT
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Calculate_Rate_Date() throws Exception                                                                                                               //Natural: CALCULATE-RATE-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Wdte8.setValueEdited(ldaAdsl601.getIaa_Cntrct_Cntrct_Issue_Dte(),new ReportEditMask("999999"));                                                               //Natural: MOVE EDITED IAA-CNTRCT.CNTRCT-ISSUE-DTE ( EM = 999999 ) TO #WDTE8
        pnd_Wdte8_Pnd_Wdte_Dd.setValue(ldaAdsl601.getIaa_Cntrct_Cntrct_Issue_Dte_Dd());                                                                                   //Natural: ASSIGN #WDTE-DD := IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD
        pnd_Rate_Dte_S.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Wdte8);                                                                                          //Natural: MOVE EDITED #WDTE8 TO #RATE-DTE-S ( EM = YYYYMMDD )
        pnd_Rate_Dte_S.nsubtract(1);                                                                                                                                      //Natural: SUBTRACT 1 FROM #RATE-DTE-S
        pnd_Wdte8_Pnd_Wdte_Dd.setValue(1);                                                                                                                                //Natural: ASSIGN #WDTE-DD := 01
        pnd_Rate_Dte_G.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Wdte8);                                                                                          //Natural: MOVE EDITED #WDTE8 TO #RATE-DTE-G ( EM = YYYYMMDD )
        pnd_Rate_Dte_G.nsubtract(1);                                                                                                                                      //Natural: SUBTRACT 1 FROM #RATE-DTE-G
        //*  YEAR PORTION OF IA CHECK DATE FROMM PDA
        pnd_Check_Dte.setValue(pdaAdsa600.getAdsa600_Pnd_Ia_Check_Dte().getSubstring(1,6));                                                                               //Natural: MOVE SUBSTR ( #IA-CHECK-DTE,1,6 ) TO #CHECK-DTE
        pnd_Reval_Dte_Pnd_Reval_Yyyy.setValue(pdaAdsa600.getAdsa600_Pnd_Filler());                                                                                        //Natural: ASSIGN #REVAL-YYYY := #FILLER
        //*  ANNUITY CERTAIN
        if (condition(pdaAdsa600.getAdsa600_Adi_Optn_Cde().equals(21)))                                                                                                   //Natural: IF ADSA600.ADI-OPTN-CDE = 21
        {
            pnd_Reval_Dte_Pnd_Reval_Mm.setValue("04");                                                                                                                    //Natural: ASSIGN #REVAL-MM := '04'
            if (condition(pnd_Wdte8_Pnd_Issue_Dte.less(pnd_Reval_Dte) && pnd_Check_Dte.greater(pnd_Reval_Dte)))                                                           //Natural: IF #ISSUE-DTE LT #REVAL-DTE AND #CHECK-DTE GT #REVAL-DTE
            {
                pnd_Rate_Dte_S.reset();                                                                                                                                   //Natural: RESET #RATE-DTE-S #RATE-DTE-G
                pnd_Rate_Dte_G.reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Reval_Dte_Pnd_Reval_Mm.setValue("01");                                                                                                                    //Natural: ASSIGN #REVAL-MM := '01'
            if (condition(pnd_Wdte8_Pnd_Issue_Dte.less(pnd_Reval_Dte) && pnd_Check_Dte.greater(pnd_Reval_Dte)))                                                           //Natural: IF #ISSUE-DTE LT #REVAL-DTE AND #CHECK-DTE GT #REVAL-DTE
            {
                pnd_Rate_Dte_S.reset();                                                                                                                                   //Natural: RESET #RATE-DTE-S #RATE-DTE-G
                pnd_Rate_Dte_G.reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
