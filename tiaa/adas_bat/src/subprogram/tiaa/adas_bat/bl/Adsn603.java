/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:55:16 PM
**        * FROM NATURAL SUBPROGRAM : Adsn603
************************************************************
**        * FILE NAME            : Adsn603.java
**        * CLASS NAME           : Adsn603
**        * INSTANCE NAME        : Adsn603
************************************************************
**********************************************************************
*
* PROGRAM:  ADSN603
* DATE   :
* FUNCTION: THIS ROUTINE CREATES FINAL PREMIUM RECORDS AND UPDATES
*           IA MASTER FILES.
*
* HISTORY:
* JUN TINIO - INSTALLED THE ACTUARIAL INTERPOLATION MODULE TO DERIVE
* 11/21/03  - THE RATE DATE/RATE BASIS. - PLEASE SEE 11/21/03
* 05/27/04  - SUBTRACT 1 FROM RATE DATE CALCULATED BY AIAN091 AS PER
*             ACTUARIAL (LIEPING WANG) - PLEASE SEE 05/27/04
* 08/02/07  - NEW 75% ANNUITY OPTION CHANGE. MARKED BY: DG 080207
* 02/22/08  O SOTTO TIAA RATE EXPANSION TO 99 OCCURS. SC 022208.
* 03/17/08  E MELNIK ROTH 403B/401K CHANGES. EM 031708.
* 01/23/09  E MELNIK TIAA ACCESS/STABLE RETURN ENHANCEMENTS. MARKED
*                    BY EM - 012309.
* 04/09/10  D.E.ANDER ADABAS REPLATFORM PCPOP MARKED BY DEA
* 03/08/12  O. SOTTO SUNY/CUNY & RATE EXPANSION. SC 030812.
* 02/21/17  R. CARREON RESTOWED FOR PIN EXPANSION 02212017
* 02/08/18  J. TINIO   POPULATED  CREF-NO-UNITS. SCAN 022018
**********************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn603 extends BLNatBase
{
    // Data Areas
    private LdaAdsl601 ldaAdsl601;
    private PdaNaza600i pdaNaza600i;
    private PdaAiaa091 pdaAiaa091;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Rslt_Isn;
    private DbsField pnd_Dob_Dte;
    private DbsField pnd_Bsnss_Dte;
    private DbsField pnd_Effct_Dte;
    private DbsField pnd_Nap_Rsttlmnt_Cnt;

    private DbsGroup pnd_Nap_Rsttlmnt_Cnt__R_Field_1;
    private DbsField pnd_Nap_Rsttlmnt_Cnt_Pnd_Di;
    private DbsField pnd_2_Dob_Dte;
    private DbsField pnd_Err_Cde;

    private DataAccessProgramView vw_updt_Ia_Rslt;
    private DbsField updt_Ia_Rslt_Rqst_Id;
    private DbsField updt_Ia_Rslt_Adi_Ia_Rcrd_Cde;
    private DbsField updt_Ia_Rslt_Adi_Sqnce_Nbr;
    private DbsField updt_Ia_Rslt_Adi_Stts_Cde;
    private DbsGroup updt_Ia_Rslt_Adi_Tiaa_NbrsMuGroup;
    private DbsField updt_Ia_Rslt_Adi_Tiaa_Nbrs;
    private DbsGroup updt_Ia_Rslt_Adi_Cref_NbrsMuGroup;
    private DbsField updt_Ia_Rslt_Adi_Cref_Nbrs;
    private DbsField updt_Ia_Rslt_Adi_Ia_Tiaa_Nbr;
    private DbsField updt_Ia_Rslt_Adi_Ia_Tiaa_Payee_Cde;
    private DbsField updt_Ia_Rslt_Adi_Ia_Cref_Nbr;
    private DbsField updt_Ia_Rslt_Adi_Ia_Cref_Payee_Cde;
    private DbsField updt_Ia_Rslt_Adi_Curncy_Cde;
    private DbsField updt_Ia_Rslt_Adi_Pymnt_Cde;
    private DbsField updt_Ia_Rslt_Adi_Pymnt_Mode;
    private DbsField updt_Ia_Rslt_Adi_Lst_Actvty_Dte;
    private DbsField updt_Ia_Rslt_Adi_Annty_Strt_Dte;
    private DbsField updt_Ia_Rslt_Adi_Instllmnt_Dte;
    private DbsField updt_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte;
    private DbsField updt_Ia_Rslt_Adi_Frst_Ck_Pd_Dte;
    private DbsField updt_Ia_Rslt_Adi_Finl_Periodic_Py_Dte;
    private DbsField updt_Ia_Rslt_Adi_Finl_Prtl_Py_Dte;

    private DbsGroup updt_Ia_Rslt_Adi_Ivc_Data;
    private DbsField updt_Ia_Rslt_Adi_Tiaa_Ivc_Amt;
    private DbsField updt_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind;
    private DbsField updt_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt;
    private DbsField updt_Ia_Rslt_Adi_Cref_Ivc_Amt;
    private DbsField updt_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind;
    private DbsField updt_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt;

    private DbsGroup updt_Ia_Rslt_Adi_Dtl_Tiaa_Data;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Tiaa_Da_Rate_Cd;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Tiaa_Da_Std_Amt;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Tiaa_Da_Grd_Amt;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Tiaa_Acct_Cd;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Tiaa_Eff_Rte_Intrst;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt;

    private DbsGroup updt_Ia_Rslt_Adi_Dtl_Cref_Data;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Cref_Da_Rate_Cd;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Cref_Annl_Amt;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Amt;
    private DbsField updt_Ia_Rslt_Adi_Optn_Cde;
    private DbsField updt_Ia_Rslt_Adi_Tiaa_Sttlmnt;
    private DbsField updt_Ia_Rslt_Adi_Tiaa_Re_Sttlmnt;
    private DbsField updt_Ia_Rslt_Adi_Cref_Sttlmnt;

    private DbsGroup updt_Ia_Rslt_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt;
    private DbsField updt_Ia_Rslt_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt;
    private DbsGroup updt_Ia_Rslt_Adi_Orig_Lob_IndMuGroup;
    private DbsField updt_Ia_Rslt_Adi_Orig_Lob_Ind;
    private DbsField updt_Ia_Rslt_Adi_Roth_Rqst_Ind;
    private DbsGroup updt_Ia_Rslt_Adi_Contract_IdMuGroup;
    private DbsField updt_Ia_Rslt_Adi_Contract_Id;

    private DataAccessProgramView vw_updt_Ia_Rsl2;
    private DbsField updt_Ia_Rsl2_Rqst_Id;
    private DbsField updt_Ia_Rsl2_Adi_Ia_Rcrd_Cde;
    private DbsField updt_Ia_Rsl2_Adi_Sqnce_Nbr;
    private DbsField updt_Ia_Rsl2_Adi_Stts_Cde;
    private DbsGroup updt_Ia_Rsl2_Adi_Tiaa_NbrsMuGroup;
    private DbsField updt_Ia_Rsl2_Adi_Tiaa_Nbrs;
    private DbsGroup updt_Ia_Rsl2_Adi_Cref_NbrsMuGroup;
    private DbsField updt_Ia_Rsl2_Adi_Cref_Nbrs;
    private DbsField updt_Ia_Rsl2_Adi_Ia_Tiaa_Nbr;
    private DbsField updt_Ia_Rsl2_Adi_Ia_Tiaa_Payee_Cde;
    private DbsField updt_Ia_Rsl2_Adi_Ia_Cref_Nbr;
    private DbsField updt_Ia_Rsl2_Adi_Ia_Cref_Payee_Cde;
    private DbsField updt_Ia_Rsl2_Adi_Curncy_Cde;
    private DbsField updt_Ia_Rsl2_Adi_Pymnt_Cde;
    private DbsField updt_Ia_Rsl2_Adi_Pymnt_Mode;
    private DbsField updt_Ia_Rsl2_Adi_Lst_Actvty_Dte;
    private DbsField updt_Ia_Rsl2_Adi_Annty_Strt_Dte;
    private DbsField updt_Ia_Rsl2_Adi_Instllmnt_Dte;
    private DbsField updt_Ia_Rsl2_Adi_Frst_Pymnt_Due_Dte;
    private DbsField updt_Ia_Rsl2_Adi_Frst_Ck_Pd_Dte;
    private DbsField updt_Ia_Rsl2_Adi_Finl_Periodic_Py_Dte;
    private DbsField updt_Ia_Rsl2_Adi_Finl_Prtl_Py_Dte;

    private DbsGroup updt_Ia_Rsl2_Adi_Ivc_Data;
    private DbsField updt_Ia_Rsl2_Adi_Tiaa_Ivc_Amt;
    private DbsField updt_Ia_Rsl2_Adi_Tiaa_Ivc_Gd_Ind;
    private DbsField updt_Ia_Rsl2_Adi_Tiaa_Prdc_Ivc_Amt;
    private DbsField updt_Ia_Rsl2_Adi_Cref_Ivc_Amt;
    private DbsField updt_Ia_Rsl2_Adi_Cref_Ivc_Gd_Ind;
    private DbsField updt_Ia_Rsl2_Adi_Cref_Prdc_Ivc_Amt;

    private DbsGroup updt_Ia_Rsl2_Adi_Dtl_Tiaa_Data;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Rate_Cd;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Tiaa_Acct_Cd;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Tiaa_Eff_Rte_Intrst;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt;

    private DbsGroup updt_Ia_Rsl2_Adi_Dtl_Cref_Data;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Cref_Da_Rate_Cd;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Cref_Acct_Cd;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Cref_Ia_Rate_Cd;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Cref_Da_Annl_Amt;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Cref_Annl_Nbr_Units;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Cref_Ia_Annl_Unit_Val;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Cref_Annl_Amt;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Cref_Da_Mnthly_Amt;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Cref_Mnthly_Nbr_Units;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Cref_Mnthly_Amt;
    private DbsField updt_Ia_Rsl2_Adi_Optn_Cde;
    private DbsField updt_Ia_Rsl2_Adi_Tiaa_Sttlmnt;
    private DbsField updt_Ia_Rsl2_Adi_Tiaa_Re_Sttlmnt;
    private DbsField updt_Ia_Rsl2_Adi_Cref_Sttlmnt;

    private DbsGroup updt_Ia_Rsl2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt;
    private DbsField updt_Ia_Rsl2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt;
    private DbsGroup updt_Ia_Rsl2_Adi_Orig_Lob_IndMuGroup;
    private DbsField updt_Ia_Rsl2_Adi_Orig_Lob_Ind;
    private DbsField updt_Ia_Rsl2_Adi_Roth_Rqst_Ind;
    private DbsGroup updt_Ia_Rsl2_Adi_Contract_IdMuGroup;
    private DbsField updt_Ia_Rsl2_Adi_Contract_Id;

    private DataAccessProgramView vw_ads_Ia_Rsl2;
    private DbsField ads_Ia_Rsl2_Rqst_Id;
    private DbsField ads_Ia_Rsl2_Adi_Ia_Rcrd_Cde;
    private DbsField ads_Ia_Rsl2_Adi_Sqnce_Nbr;
    private DbsField ads_Ia_Rsl2_Adi_Stts_Cde;
    private DbsGroup ads_Ia_Rsl2_Adi_Tiaa_NbrsMuGroup;
    private DbsField ads_Ia_Rsl2_Adi_Tiaa_Nbrs;
    private DbsGroup ads_Ia_Rsl2_Adi_Cref_NbrsMuGroup;
    private DbsField ads_Ia_Rsl2_Adi_Cref_Nbrs;
    private DbsField ads_Ia_Rsl2_Adi_Ia_Tiaa_Nbr;
    private DbsField ads_Ia_Rsl2_Adi_Ia_Tiaa_Payee_Cde;
    private DbsField ads_Ia_Rsl2_Adi_Ia_Cref_Nbr;
    private DbsField ads_Ia_Rsl2_Adi_Ia_Cref_Payee_Cde;
    private DbsField ads_Ia_Rsl2_Adi_Curncy_Cde;
    private DbsField ads_Ia_Rsl2_Adi_Pymnt_Cde;
    private DbsField ads_Ia_Rsl2_Adi_Pymnt_Mode;
    private DbsField ads_Ia_Rsl2_Adi_Lst_Actvty_Dte;
    private DbsField ads_Ia_Rsl2_Adi_Annty_Strt_Dte;
    private DbsField ads_Ia_Rsl2_Adi_Instllmnt_Dte;
    private DbsField ads_Ia_Rsl2_Adi_Frst_Pymnt_Due_Dte;
    private DbsField ads_Ia_Rsl2_Adi_Frst_Ck_Pd_Dte;
    private DbsField ads_Ia_Rsl2_Adi_Finl_Periodic_Py_Dte;
    private DbsField ads_Ia_Rsl2_Adi_Finl_Prtl_Py_Dte;

    private DbsGroup ads_Ia_Rsl2_Adi_Ivc_Data;
    private DbsField ads_Ia_Rsl2_Adi_Tiaa_Ivc_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Tiaa_Ivc_Gd_Ind;
    private DbsField ads_Ia_Rsl2_Adi_Tiaa_Prdc_Ivc_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Cref_Ivc_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Cref_Ivc_Gd_Ind;
    private DbsField ads_Ia_Rsl2_Adi_Cref_Prdc_Ivc_Amt;

    private DbsGroup ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Rate_Cd;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Acct_Cd;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Eff_Rte_Intrst;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt;

    private DbsGroup ads_Ia_Rsl2_Adi_Dtl_Cref_Data;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Da_Rate_Cd;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Acct_Cd;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Ia_Rate_Cd;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Da_Annl_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Annl_Nbr_Units;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Ia_Annl_Unit_Val;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Annl_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Da_Mnthly_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Mnthly_Nbr_Units;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Mnthly_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Optn_Cde;
    private DbsField ads_Ia_Rsl2_Adi_Tiaa_Sttlmnt;
    private DbsField ads_Ia_Rsl2_Adi_Tiaa_Re_Sttlmnt;
    private DbsField ads_Ia_Rsl2_Adi_Cref_Sttlmnt;

    private DbsGroup ads_Ia_Rsl2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt;
    private DbsGroup ads_Ia_Rsl2_Adi_Orig_Lob_IndMuGroup;
    private DbsField ads_Ia_Rsl2_Adi_Orig_Lob_Ind;
    private DbsField ads_Ia_Rsl2_Adi_Roth_Rqst_Ind;
    private DbsGroup ads_Ia_Rsl2_Adi_Contract_IdMuGroup;
    private DbsField ads_Ia_Rsl2_Adi_Contract_Id;

    private DataAccessProgramView vw_iaa_Old_Tiaa_Rates;
    private DbsField iaa_Old_Tiaa_Rates_Fund_Lst_Pd_Dte;
    private DbsField iaa_Old_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte;
    private DbsField iaa_Old_Tiaa_Rates_Trans_Dte;
    private DbsField iaa_Old_Tiaa_Rates_Trans_User_Area;
    private DbsField iaa_Old_Tiaa_Rates_Trans_User_Id;
    private DbsField iaa_Old_Tiaa_Rates_Trans_Verify_Id;
    private DbsField iaa_Old_Tiaa_Rates_Trans_Verify_Dte;
    private DbsField iaa_Old_Tiaa_Rates_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Old_Tiaa_Rates_Cntrct_Payee_Cde;
    private DbsField iaa_Old_Tiaa_Rates_Cntrct_Mode_Ind;
    private DbsField iaa_Old_Tiaa_Rates_Cmpny_Fund_Cde;

    private DbsGroup iaa_Old_Tiaa_Rates__R_Field_2;
    private DbsField iaa_Old_Tiaa_Rates_Cmpny_Cde;
    private DbsField iaa_Old_Tiaa_Rates_Fund_Cde;
    private DbsField iaa_Old_Tiaa_Rates_Rcrd_Srce;
    private DbsField iaa_Old_Tiaa_Rates_Rcrd_Status;
    private DbsField iaa_Old_Tiaa_Rates_Cntrct_Tot_Per_Amt;
    private DbsField iaa_Old_Tiaa_Rates_Cntrct_Tot_Div_Amt;
    private DbsField iaa_Old_Tiaa_Rates_Count_Casttiaa_Rate_Data_Grp;

    private DbsGroup iaa_Old_Tiaa_Rates_Tiaa_Rate_Data_Grp;
    private DbsField iaa_Old_Tiaa_Rates_Tiaa_Rate_Cde;
    private DbsField iaa_Old_Tiaa_Rates_Tiaa_Rate_Dte;
    private DbsField iaa_Old_Tiaa_Rates_Tiaa_Per_Pay_Amt;
    private DbsField iaa_Old_Tiaa_Rates_Tiaa_Per_Div_Amt;
    private DbsField iaa_Old_Tiaa_Rates_Tiaa_Rate_Final_Pay_Amt;
    private DbsField iaa_Old_Tiaa_Rates_Tiaa_Rate_Final_Div_Amt;
    private DbsField iaa_Old_Tiaa_Rates_Lst_Trans_Dte;
    private DbsGroup iaa_Old_Tiaa_Rates_Tiaa_Rate_GicMuGroup;
    private DbsField iaa_Old_Tiaa_Rates_Tiaa_Rate_Gic;
    private DbsField iaa_Old_Tiaa_Rates_Tiaa_Mode_Ind;
    private DbsField iaa_Old_Tiaa_Rates_Tiaa_Old_Cmpny_Fund;

    private DataAccessProgramView vw_iaa_Old_Cref_Rates;
    private DbsField iaa_Old_Cref_Rates_Fund_Lst_Pd_Dte;
    private DbsField iaa_Old_Cref_Rates_Fund_Invrse_Lst_Pd_Dte;
    private DbsField iaa_Old_Cref_Rates_Trans_Dte;
    private DbsField iaa_Old_Cref_Rates_Trans_User_Area;
    private DbsField iaa_Old_Cref_Rates_Trans_User_Id;
    private DbsField iaa_Old_Cref_Rates_Trans_Verify_Id;
    private DbsField iaa_Old_Cref_Rates_Trans_Verify_Dte;
    private DbsField iaa_Old_Cref_Rates_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Old_Cref_Rates_Cntrct_Payee_Cde;
    private DbsField iaa_Old_Cref_Rates_Cntrct_Mode_Ind;
    private DbsField iaa_Old_Cref_Rates_Rcrd_Srce;
    private DbsField iaa_Old_Cref_Rates_Rcrd_Status;
    private DbsField iaa_Old_Cref_Rates_Cmpny_Fund_Cde;

    private DbsGroup iaa_Old_Cref_Rates__R_Field_3;
    private DbsField iaa_Old_Cref_Rates_Cmpny_Cde;
    private DbsField iaa_Old_Cref_Rates_Fund_Cde;
    private DbsField iaa_Old_Cref_Rates_Cntrct_Tot_Per_Amt;
    private DbsField iaa_Old_Cref_Rates_Cntrct_Unit_Val;
    private DbsField iaa_Old_Cref_Rates_Cntrct_Tot_Units;
    private DbsField iaa_Old_Cref_Rates_Lst_Trans_Dte;
    private DbsField iaa_Old_Cref_Rates_Count_Castcref_Rate_Data_Grp;

    private DbsGroup iaa_Old_Cref_Rates_Cref_Rate_Data_Grp;
    private DbsField iaa_Old_Cref_Rates_Cref_Rate_Cde;
    private DbsField iaa_Old_Cref_Rates_Cref_Rate_Dte;
    private DbsField iaa_Old_Cref_Rates_Cref_Per_Pay_Amt;
    private DbsField iaa_Old_Cref_Rates_Cref_No_Units;

    private DataAccessProgramView vw_cref_Fund_Rcrd;
    private DbsField cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr;
    private DbsField cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde;
    private DbsField cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde;

    private DbsGroup cref_Fund_Rcrd__R_Field_4;
    private DbsField cref_Fund_Rcrd_Cref_Cmpny_Cde;
    private DbsField cref_Fund_Rcrd_Cref_Fund_Cde;
    private DbsField cref_Fund_Rcrd_Cref_Tot_Per_Amt;
    private DbsField cref_Fund_Rcrd_Cref_Unit_Val;
    private DbsField cref_Fund_Rcrd_Cref_Old_Per_Amt;
    private DbsField cref_Fund_Rcrd_Cref_Old_Unit_Val;
    private DbsField cref_Fund_Rcrd_Count_Castcref_Rate_Data_Grp;

    private DbsGroup cref_Fund_Rcrd_Cref_Rate_Data_Grp;
    private DbsField cref_Fund_Rcrd_Cref_Rate_Cde;
    private DbsField cref_Fund_Rcrd_Cref_Rate_Dte;
    private DbsField cref_Fund_Rcrd_Cref_Units_Cnt;
    private DbsField cref_Fund_Rcrd_Lst_Trans_Dte;
    private DbsField cref_Fund_Rcrd_Cref_Xfr_Iss_Dte;
    private DbsField cref_Fund_Rcrd_Cref_Lst_Xfr_In_Dte;
    private DbsField cref_Fund_Rcrd_Cref_Lst_Xfr_Out_Dte;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_5;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_Cntrct_Fund_Key;

    private DbsGroup pnd_Cntrct_Fund_Key__R_Field_6;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Fund_Key_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Cref_Fund_Key;

    private DbsGroup pnd_Cref_Fund_Key__R_Field_7;
    private DbsField pnd_Cref_Fund_Key__Filler1;
    private DbsField pnd_Cref_Fund_Key_Pnd_Cmpny_Cde;
    private DbsField pnd_Cref_Fund_Key_Pnd_Fnd_Cde;
    private DbsField pnd_Tiaa;
    private DbsField pnd_Rea;
    private DbsField pnd_Cref;
    private DbsField pnd_Rslt;
    private DbsField pnd_Fund_1;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Z;
    private DbsField pnd_T_Cnt;
    private DbsField pnd_I_Cnt;
    private DbsField pnd_Rate_No;
    private DbsField pnd_Rate_Xx;
    private DbsField pnd_Rc;
    private DbsField pnd_Trans_Dte;
    private DbsField pnd_Trans_Check_Dte;
    private DbsField pnd_Trans_Prev_Dte;

    private DbsGroup pnd_Trans_Prev_Dte__R_Field_8;
    private DbsField pnd_Trans_Prev_Dte_Pnd_Trans_Prev_Yyyy;
    private DbsField pnd_Trans_Prev_Dte_Pnd_Trans_Prev_Mm;
    private DbsField pnd_Trans_Prev_Dte_Pnd_Trans_Prev_Dd;
    private DbsField pnd_Next_Pymnt_Dte;
    private DbsField pnd_Invrse_Trans_Dte;
    private DbsField pnd_Asd_A;

    private DbsGroup pnd_Asd_A__R_Field_9;
    private DbsField pnd_Asd_A_Pnd_Asd_N;

    private DbsGroup pnd_Asd_A__R_Field_10;
    private DbsField pnd_Asd_A_Pnd_Asd_Yyyy;
    private DbsField pnd_Asd_A_Pnd_Asd_Mm;
    private DbsField pnd_Asd_A_Pnd_Asd_Dd;

    private DbsGroup pnd_Asd_A__R_Field_11;
    private DbsField pnd_Asd_A_Pnd_Asd_N6;
    private DbsField pnd_Dob;

    private DbsGroup pnd_Dob__R_Field_12;
    private DbsField pnd_Dob_Pnd_Dob_Yyyy;
    private DbsField pnd_Dob_Pnd_Dob_Mm;
    private DbsField pnd_Dob_Pnd_Dob_Dd;

    private DbsGroup pnd_Dob__R_Field_13;
    private DbsField pnd_Dob_Pnd_Dob_N;
    private DbsField pnd_Dob2;

    private DbsGroup pnd_Dob2__R_Field_14;
    private DbsField pnd_Dob2_Pnd_Dob2_Yyyy;
    private DbsField pnd_Dob2_Pnd_Dob2_Mm;
    private DbsField pnd_Dob2_Pnd_Dob2_Dd;

    private DbsGroup pnd_Dob2__R_Field_15;
    private DbsField pnd_Dob2_Pnd_Dob2_N;
    private DbsField pnd_No_Per_Yr;
    private DbsField pnd_Sim_Age;
    private DbsField pnd_Sim_Rule;
    private DbsField pnd_No_Pays;
    private DbsField pnd_Tiaa_Ivc;
    private DbsField pnd_Cref_Ivc;
    private DbsField pnd_Cntrl_Per_Pay_Amt;
    private DbsField pnd_Cntrl_Per_Div_Amt;
    private DbsField pnd_Cntrl_Units_Cnt;
    private DbsField pnd_Cntrl_Final_Pay_Amt;
    private DbsField pnd_Cntrl_Final_Div_Amt;
    private DbsField pnd_Ivc_Amt;
    private DbsField pnd_Prdc_Ivc_Amt;
    private DbsField pnd_Ivc;
    private DbsField pnd_Datd;
    private DbsField pnd_Rate_Dte;
    private DbsField pnd_Wdte6;
    private DbsField pnd_Wdte8;

    private DbsGroup pnd_Wdte8__R_Field_16;
    private DbsField pnd_Wdte8_Pnd_Wdte_Yyyy;
    private DbsField pnd_Wdte8_Pnd_Wdte_Mm;
    private DbsField pnd_Wdte8_Pnd_Wdte_Dd;
    private DbsField pnd_Fp_Dte;
    private DbsField pnd_Cntrct_Py_Dte_Key;

    private DbsGroup pnd_Cntrct_Py_Dte_Key__R_Field_17;

    private DbsGroup pnd_Cntrct_Py_Dte_Key_Hist_Structure;
    private DbsField pnd_Cntrct_Py_Dte_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Py_Dte_Key_Cntrct_Payee_Cde;
    private DbsField pnd_Cntrct_Py_Dte_Key_Fund_Invrse_Lst_Pd_Dte;
    private DbsField pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde;
    private DbsField pnd_Settl_Dte_D;
    private DbsField pnd_Settl_Dte_Grd;

    private DbsGroup pnd_Settl_Dte_Grd__R_Field_18;
    private DbsField pnd_Settl_Dte_Grd_Pnd_Settl_Yyyy;
    private DbsField pnd_Settl_Dte_Grd_Pnd_Settl_Mm;
    private DbsField pnd_Settl_Dte_Grd_Pnd_Settl_Dd;
    private DbsField pnd_Create_Std;
    private DbsField pnd_Updt_Std;
    private DbsField pnd_Create_Grd;
    private DbsField pnd_Updt_Grd;
    private DbsField pnd_Inactive_Tiaa_Isn;
    private DbsField pnd_I1;
    private DbsField pnd_I2;
    private DbsField pnd_A;
    private DbsField pnd_B;
    private DbsField pnd_T;
    private DbsField pnd_C;
    private DbsField pnd_More;
    private DbsField pnd_Actve_T;
    private DbsField pnd_Actve_C;
    private DbsField pnd_Seasoned;
    private DbsField pnd_Base_Dte;
    private DbsField pnd_Age_Yy;
    private DbsField pnd_Age_Mm;
    private DbsField pnd_New_Isns;
    private DbsField pnd_First;
    private DbsField pnd_Max_Rte;
    private DbsField pnd_Max_Adi;
    private DbsField pnd_S_Rate_Cde;

    private DbsGroup pnd_S_Rate_Cde__R_Field_19;
    private DbsField pnd_S_Rate_Cde_Pnd_S_Rate;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl601 = new LdaAdsl601();
        registerRecord(ldaAdsl601);
        registerRecord(ldaAdsl601.getVw_ads_Ia_Rslt());
        registerRecord(ldaAdsl601.getVw_iaa_Cntrct());
        registerRecord(ldaAdsl601.getVw_iaa_Cntrct_Prtcpnt_Role());
        registerRecord(ldaAdsl601.getVw_iaa_Tiaa_Fund_Rcrd());
        registerRecord(ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1());
        registerRecord(ldaAdsl601.getVw_iaa_Trans_Rcrd());
        registerRecord(ldaAdsl601.getVw_iaa_Cntrl_Rcrd_1());
        registerRecord(ldaAdsl601.getVw_iaa_Cntrct_Trans());
        registerRecord(ldaAdsl601.getVw_iaa_Cpr_Trans());
        registerRecord(ldaAdsl601.getVw_iaa_Tiaa_Fund_Trans());
        registerRecord(ldaAdsl601.getVw_iaa_Cref_Fund_Trans_1());
        localVariables = new DbsRecord();
        pdaNaza600i = new PdaNaza600i(localVariables);
        pdaAiaa091 = new PdaAiaa091(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_Rslt_Isn = parameters.newFieldArrayInRecord("pnd_Rslt_Isn", "#RSLT-ISN", FieldType.PACKED_DECIMAL, 8, new DbsArrayController(1, 2));
        pnd_Rslt_Isn.setParameterOption(ParameterOption.ByReference);
        pnd_Dob_Dte = parameters.newFieldInRecord("pnd_Dob_Dte", "#DOB-DTE", FieldType.DATE);
        pnd_Dob_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Bsnss_Dte = parameters.newFieldInRecord("pnd_Bsnss_Dte", "#BSNSS-DTE", FieldType.DATE);
        pnd_Bsnss_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Effct_Dte = parameters.newFieldInRecord("pnd_Effct_Dte", "#EFFCT-DTE", FieldType.DATE);
        pnd_Effct_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Nap_Rsttlmnt_Cnt = parameters.newFieldInRecord("pnd_Nap_Rsttlmnt_Cnt", "#NAP-RSTTLMNT-CNT", FieldType.STRING, 1);
        pnd_Nap_Rsttlmnt_Cnt.setParameterOption(ParameterOption.ByReference);

        pnd_Nap_Rsttlmnt_Cnt__R_Field_1 = parameters.newGroupInRecord("pnd_Nap_Rsttlmnt_Cnt__R_Field_1", "REDEFINE", pnd_Nap_Rsttlmnt_Cnt);
        pnd_Nap_Rsttlmnt_Cnt_Pnd_Di = pnd_Nap_Rsttlmnt_Cnt__R_Field_1.newFieldInGroup("pnd_Nap_Rsttlmnt_Cnt_Pnd_Di", "#DI", FieldType.NUMERIC, 1);
        pnd_2_Dob_Dte = parameters.newFieldInRecord("pnd_2_Dob_Dte", "#2-DOB-DTE", FieldType.DATE);
        pnd_2_Dob_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Err_Cde = parameters.newFieldInRecord("pnd_Err_Cde", "#ERR-CDE", FieldType.STRING, 3);
        pnd_Err_Cde.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_updt_Ia_Rslt = new DataAccessProgramView(new NameInfo("vw_updt_Ia_Rslt", "UPDT-IA-RSLT"), "ADS_IA_RSLT", "ADS_IA_RSLT", DdmPeriodicGroups.getInstance().getGroups("ADS_IA_RSLT"));
        updt_Ia_Rslt_Rqst_Id = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "RQST_ID");
        updt_Ia_Rslt_Adi_Ia_Rcrd_Cde = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Ia_Rcrd_Cde", "ADI-IA-RCRD-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "ADI_IA_RCRD_CDE");
        updt_Ia_Rslt_Adi_Sqnce_Nbr = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Sqnce_Nbr", "ADI-SQNCE-NBR", FieldType.NUMERIC, 3, 
            RepeatingFieldStrategy.None, "ADI_SQNCE_NBR");
        updt_Ia_Rslt_Adi_Stts_Cde = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Stts_Cde", "ADI-STTS-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "ADI_STTS_CDE");
        updt_Ia_Rslt_Adi_Tiaa_NbrsMuGroup = vw_updt_Ia_Rslt.getRecord().newGroupInGroup("UPDT_IA_RSLT_ADI_TIAA_NBRSMuGroup", "ADI_TIAA_NBRSMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "ADS_IA_RSLT_ADI_TIAA_NBRS");
        updt_Ia_Rslt_Adi_Tiaa_Nbrs = updt_Ia_Rslt_Adi_Tiaa_NbrsMuGroup.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Tiaa_Nbrs", "ADI-TIAA-NBRS", FieldType.STRING, 
            10, new DbsArrayController(1, 12), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_TIAA_NBRS");
        updt_Ia_Rslt_Adi_Cref_NbrsMuGroup = vw_updt_Ia_Rslt.getRecord().newGroupInGroup("UPDT_IA_RSLT_ADI_CREF_NBRSMuGroup", "ADI_CREF_NBRSMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "ADS_IA_RSLT_ADI_CREF_NBRS");
        updt_Ia_Rslt_Adi_Cref_Nbrs = updt_Ia_Rslt_Adi_Cref_NbrsMuGroup.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Cref_Nbrs", "ADI-CREF-NBRS", FieldType.STRING, 
            10, new DbsArrayController(1, 12), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_CREF_NBRS");
        updt_Ia_Rslt_Adi_Ia_Tiaa_Nbr = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Ia_Tiaa_Nbr", "ADI-IA-TIAA-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "ADI_IA_TIAA_NBR");
        updt_Ia_Rslt_Adi_Ia_Tiaa_Payee_Cde = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Ia_Tiaa_Payee_Cde", "ADI-IA-TIAA-PAYEE-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ADI_IA_TIAA_PAYEE_CDE");
        updt_Ia_Rslt_Adi_Ia_Cref_Nbr = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Ia_Cref_Nbr", "ADI-IA-CREF-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "ADI_IA_CREF_NBR");
        updt_Ia_Rslt_Adi_Ia_Cref_Payee_Cde = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Ia_Cref_Payee_Cde", "ADI-IA-CREF-PAYEE-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ADI_IA_CREF_PAYEE_CDE");
        updt_Ia_Rslt_Adi_Curncy_Cde = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Curncy_Cde", "ADI-CURNCY-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "ADI_CURNCY_CDE");
        updt_Ia_Rslt_Adi_Pymnt_Cde = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Pymnt_Cde", "ADI-PYMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ADI_PYMNT_CDE");
        updt_Ia_Rslt_Adi_Pymnt_Mode = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Pymnt_Mode", "ADI-PYMNT-MODE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "ADI_PYMNT_MODE");
        updt_Ia_Rslt_Adi_Lst_Actvty_Dte = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Lst_Actvty_Dte", "ADI-LST-ACTVTY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_LST_ACTVTY_DTE");
        updt_Ia_Rslt_Adi_Annty_Strt_Dte = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Annty_Strt_Dte", "ADI-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_ANNTY_STRT_DTE");
        updt_Ia_Rslt_Adi_Instllmnt_Dte = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Instllmnt_Dte", "ADI-INSTLLMNT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_INSTLLMNT_DTE");
        updt_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte", "ADI-FRST-PYMNT-DUE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADI_FRST_PYMNT_DUE_DTE");
        updt_Ia_Rslt_Adi_Frst_Ck_Pd_Dte = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Frst_Ck_Pd_Dte", "ADI-FRST-CK-PD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_FRST_CK_PD_DTE");
        updt_Ia_Rslt_Adi_Finl_Periodic_Py_Dte = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Finl_Periodic_Py_Dte", "ADI-FINL-PERIODIC-PY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADI_FINL_PERIODIC_PY_DTE");
        updt_Ia_Rslt_Adi_Finl_Prtl_Py_Dte = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Finl_Prtl_Py_Dte", "ADI-FINL-PRTL-PY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_FINL_PRTL_PY_DTE");

        updt_Ia_Rslt_Adi_Ivc_Data = vw_updt_Ia_Rslt.getRecord().newGroupInGroup("UPDT_IA_RSLT_ADI_IVC_DATA", "ADI-IVC-DATA");
        updt_Ia_Rslt_Adi_Tiaa_Ivc_Amt = updt_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("updt_Ia_Rslt_Adi_Tiaa_Ivc_Amt", "ADI-TIAA-IVC-AMT", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "ADI_TIAA_IVC_AMT");
        updt_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind = updt_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("updt_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind", "ADI-TIAA-IVC-GD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_TIAA_IVC_GD_IND");
        updt_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt = updt_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("updt_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt", "ADI-TIAA-PRDC-IVC-AMT", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "ADI_TIAA_PRDC_IVC_AMT");
        updt_Ia_Rslt_Adi_Cref_Ivc_Amt = updt_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("updt_Ia_Rslt_Adi_Cref_Ivc_Amt", "ADI-CREF-IVC-AMT", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "ADI_CREF_IVC_AMT");
        updt_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind = updt_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("updt_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind", "ADI-CREF-IVC-GD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_CREF_IVC_GD_IND");
        updt_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt = updt_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("updt_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt", "ADI-CREF-PRDC-IVC-AMT", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "ADI_CREF_PRDC_IVC_AMT");

        updt_Ia_Rslt_Adi_Dtl_Tiaa_Data = vw_updt_Ia_Rslt.getRecord().newGroupInGroup("updt_Ia_Rslt_Adi_Dtl_Tiaa_Data", "ADI-DTL-TIAA-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Da_Rate_Cd = updt_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Tiaa_Da_Rate_Cd", "ADI-DTL-TIAA-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Da_Std_Amt = updt_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Tiaa_Da_Std_Amt", "ADI-DTL-TIAA-DA-STD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Da_Grd_Amt = updt_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Tiaa_Da_Grd_Amt", "ADI-DTL-TIAA-DA-GRD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Acct_Cd = updt_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Tiaa_Acct_Cd", "ADI-DTL-TIAA-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_ACCT_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd = updt_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd", "ADI-DTL-TIAA-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_IA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Eff_Rte_Intrst = updt_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Tiaa_Eff_Rte_Intrst", "ADI-DTL-TIAA-EFF-RTE-INTRST", 
            FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_EFF_RTE_INTRST", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt = updt_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt", "ADI-DTL-TIAA-GRD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt = updt_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt", "ADI-DTL-TIAA-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt = updt_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt", "ADI-DTL-FNL-GRD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt = updt_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt", "ADI-DTL-FNL-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt = updt_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt", "ADI-DTL-TIAA-STD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt = updt_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt", "ADI-DTL-TIAA-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt = updt_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt", "ADI-DTL-FNL-STD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt = updt_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt", "ADI-DTL-FNL-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");

        updt_Ia_Rslt_Adi_Dtl_Cref_Data = vw_updt_Ia_Rslt.getRecord().newGroupInGroup("updt_Ia_Rslt_Adi_Dtl_Cref_Data", "ADI-DTL-CREF-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rslt_Adi_Dtl_Cref_Da_Rate_Cd = updt_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Cref_Da_Rate_Cd", "ADI-DTL-CREF-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_DA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd = updt_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd", "ADI-DTL-CREF-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_ACCT_CD", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd = updt_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd", "ADI-DTL-CREF-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_IA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt = updt_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt", "ADI-DTL-CREF-DA-ANNL-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_DA_ANNL_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units = updt_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units", "ADI-DTL-CREF-ANNL-NBR-UNITS", 
            FieldType.NUMERIC, 10, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_ANNL_NBR_UNITS", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val = updt_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val", 
            "ADI-DTL-CREF-IA-ANNL-UNIT-VAL", FieldType.NUMERIC, 7, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_IA_ANNL_UNIT_VAL", 
            "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rslt_Adi_Dtl_Cref_Annl_Amt = updt_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Cref_Annl_Amt", "ADI-DTL-CREF-ANNL-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_ANNL_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt = updt_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt", "ADI-DTL-CREF-DA-MNTHLY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_DA_MNTHLY_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units = updt_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units", 
            "ADI-DTL-CREF-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 10, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_MNTHLY_NBR_UNITS", 
            "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val = updt_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val", 
            "ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL", FieldType.NUMERIC, 7, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_CREF_IA_MNTHLY_UNIT_VAL", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Amt = updt_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Amt", "ADI-DTL-CREF-MNTHLY-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_MNTHLY_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rslt_Adi_Optn_Cde = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Optn_Cde", "ADI-OPTN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "ADI_OPTN_CDE");
        updt_Ia_Rslt_Adi_Tiaa_Sttlmnt = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Tiaa_Sttlmnt", "ADI-TIAA-STTLMNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_TIAA_STTLMNT");
        updt_Ia_Rslt_Adi_Tiaa_Re_Sttlmnt = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Tiaa_Re_Sttlmnt", "ADI-TIAA-RE-STTLMNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_TIAA_RE_STTLMNT");
        updt_Ia_Rslt_Adi_Cref_Sttlmnt = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Cref_Sttlmnt", "ADI-CREF-STTLMNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_CREF_STTLMNT");

        updt_Ia_Rslt_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat = vw_updt_Ia_Rslt.getRecord().newGroupInGroup("updt_Ia_Rslt_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat", "ADI-DTL-TIAA-TPA-GUAR-COMMUT-DAT", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt = updt_Ia_Rslt_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt", 
            "ADI-DTL-TIAA-GUAR-COMMUT-STD-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_TIAA_GUAR_COMMUT_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt = updt_Ia_Rslt_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt", 
            "ADI-DTL-TIAA-GUAR-COMMUT-GRD-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_TIAA_GUAR_COMMUT_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        updt_Ia_Rslt_Adi_Orig_Lob_IndMuGroup = vw_updt_Ia_Rslt.getRecord().newGroupInGroup("UPDT_IA_RSLT_ADI_ORIG_LOB_INDMuGroup", "ADI_ORIG_LOB_INDMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_IA_RSLT_ADI_ORIG_LOB_IND");
        updt_Ia_Rslt_Adi_Orig_Lob_Ind = updt_Ia_Rslt_Adi_Orig_Lob_IndMuGroup.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Orig_Lob_Ind", "ADI-ORIG-LOB-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 12), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_ORIG_LOB_IND");
        updt_Ia_Rslt_Adi_Roth_Rqst_Ind = vw_updt_Ia_Rslt.getRecord().newFieldInGroup("updt_Ia_Rslt_Adi_Roth_Rqst_Ind", "ADI-ROTH-RQST-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_ROTH_RQST_IND");
        updt_Ia_Rslt_Adi_Contract_IdMuGroup = vw_updt_Ia_Rslt.getRecord().newGroupInGroup("UPDT_IA_RSLT_ADI_CONTRACT_IDMuGroup", "ADI_CONTRACT_IDMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_IA_RSLT_ADI_CONTRACT_ID");
        updt_Ia_Rslt_Adi_Contract_Id = updt_Ia_Rslt_Adi_Contract_IdMuGroup.newFieldArrayInGroup("updt_Ia_Rslt_Adi_Contract_Id", "ADI-CONTRACT-ID", FieldType.NUMERIC, 
            11, new DbsArrayController(1, 125), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_CONTRACT_ID");
        registerRecord(vw_updt_Ia_Rslt);

        vw_updt_Ia_Rsl2 = new DataAccessProgramView(new NameInfo("vw_updt_Ia_Rsl2", "UPDT-IA-RSL2"), "ADS_IA_RSLT", "ADS_IA_RSLT", DdmPeriodicGroups.getInstance().getGroups("ADS_IA_RSLT"));
        updt_Ia_Rsl2_Rqst_Id = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "RQST_ID");
        updt_Ia_Rsl2_Adi_Ia_Rcrd_Cde = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Ia_Rcrd_Cde", "ADI-IA-RCRD-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "ADI_IA_RCRD_CDE");
        updt_Ia_Rsl2_Adi_Sqnce_Nbr = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Sqnce_Nbr", "ADI-SQNCE-NBR", FieldType.NUMERIC, 3, 
            RepeatingFieldStrategy.None, "ADI_SQNCE_NBR");
        updt_Ia_Rsl2_Adi_Stts_Cde = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Stts_Cde", "ADI-STTS-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "ADI_STTS_CDE");
        updt_Ia_Rsl2_Adi_Tiaa_NbrsMuGroup = vw_updt_Ia_Rsl2.getRecord().newGroupInGroup("UPDT_IA_RSL2_ADI_TIAA_NBRSMuGroup", "ADI_TIAA_NBRSMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "ADS_IA_RSLT_ADI_TIAA_NBRS");
        updt_Ia_Rsl2_Adi_Tiaa_Nbrs = updt_Ia_Rsl2_Adi_Tiaa_NbrsMuGroup.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Tiaa_Nbrs", "ADI-TIAA-NBRS", FieldType.STRING, 
            10, new DbsArrayController(1, 12), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_TIAA_NBRS");
        updt_Ia_Rsl2_Adi_Cref_NbrsMuGroup = vw_updt_Ia_Rsl2.getRecord().newGroupInGroup("UPDT_IA_RSL2_ADI_CREF_NBRSMuGroup", "ADI_CREF_NBRSMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "ADS_IA_RSLT_ADI_CREF_NBRS");
        updt_Ia_Rsl2_Adi_Cref_Nbrs = updt_Ia_Rsl2_Adi_Cref_NbrsMuGroup.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Cref_Nbrs", "ADI-CREF-NBRS", FieldType.STRING, 
            10, new DbsArrayController(1, 12), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_CREF_NBRS");
        updt_Ia_Rsl2_Adi_Ia_Tiaa_Nbr = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Ia_Tiaa_Nbr", "ADI-IA-TIAA-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "ADI_IA_TIAA_NBR");
        updt_Ia_Rsl2_Adi_Ia_Tiaa_Payee_Cde = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Ia_Tiaa_Payee_Cde", "ADI-IA-TIAA-PAYEE-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ADI_IA_TIAA_PAYEE_CDE");
        updt_Ia_Rsl2_Adi_Ia_Cref_Nbr = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Ia_Cref_Nbr", "ADI-IA-CREF-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "ADI_IA_CREF_NBR");
        updt_Ia_Rsl2_Adi_Ia_Cref_Payee_Cde = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Ia_Cref_Payee_Cde", "ADI-IA-CREF-PAYEE-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ADI_IA_CREF_PAYEE_CDE");
        updt_Ia_Rsl2_Adi_Curncy_Cde = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Curncy_Cde", "ADI-CURNCY-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "ADI_CURNCY_CDE");
        updt_Ia_Rsl2_Adi_Pymnt_Cde = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Pymnt_Cde", "ADI-PYMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ADI_PYMNT_CDE");
        updt_Ia_Rsl2_Adi_Pymnt_Mode = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Pymnt_Mode", "ADI-PYMNT-MODE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "ADI_PYMNT_MODE");
        updt_Ia_Rsl2_Adi_Lst_Actvty_Dte = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Lst_Actvty_Dte", "ADI-LST-ACTVTY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_LST_ACTVTY_DTE");
        updt_Ia_Rsl2_Adi_Annty_Strt_Dte = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Annty_Strt_Dte", "ADI-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_ANNTY_STRT_DTE");
        updt_Ia_Rsl2_Adi_Instllmnt_Dte = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Instllmnt_Dte", "ADI-INSTLLMNT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_INSTLLMNT_DTE");
        updt_Ia_Rsl2_Adi_Frst_Pymnt_Due_Dte = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Frst_Pymnt_Due_Dte", "ADI-FRST-PYMNT-DUE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADI_FRST_PYMNT_DUE_DTE");
        updt_Ia_Rsl2_Adi_Frst_Ck_Pd_Dte = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Frst_Ck_Pd_Dte", "ADI-FRST-CK-PD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_FRST_CK_PD_DTE");
        updt_Ia_Rsl2_Adi_Finl_Periodic_Py_Dte = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Finl_Periodic_Py_Dte", "ADI-FINL-PERIODIC-PY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADI_FINL_PERIODIC_PY_DTE");
        updt_Ia_Rsl2_Adi_Finl_Prtl_Py_Dte = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Finl_Prtl_Py_Dte", "ADI-FINL-PRTL-PY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_FINL_PRTL_PY_DTE");

        updt_Ia_Rsl2_Adi_Ivc_Data = vw_updt_Ia_Rsl2.getRecord().newGroupInGroup("UPDT_IA_RSL2_ADI_IVC_DATA", "ADI-IVC-DATA");
        updt_Ia_Rsl2_Adi_Tiaa_Ivc_Amt = updt_Ia_Rsl2_Adi_Ivc_Data.newFieldInGroup("updt_Ia_Rsl2_Adi_Tiaa_Ivc_Amt", "ADI-TIAA-IVC-AMT", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "ADI_TIAA_IVC_AMT");
        updt_Ia_Rsl2_Adi_Tiaa_Ivc_Gd_Ind = updt_Ia_Rsl2_Adi_Ivc_Data.newFieldInGroup("updt_Ia_Rsl2_Adi_Tiaa_Ivc_Gd_Ind", "ADI-TIAA-IVC-GD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_TIAA_IVC_GD_IND");
        updt_Ia_Rsl2_Adi_Tiaa_Prdc_Ivc_Amt = updt_Ia_Rsl2_Adi_Ivc_Data.newFieldInGroup("updt_Ia_Rsl2_Adi_Tiaa_Prdc_Ivc_Amt", "ADI-TIAA-PRDC-IVC-AMT", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "ADI_TIAA_PRDC_IVC_AMT");
        updt_Ia_Rsl2_Adi_Cref_Ivc_Amt = updt_Ia_Rsl2_Adi_Ivc_Data.newFieldInGroup("updt_Ia_Rsl2_Adi_Cref_Ivc_Amt", "ADI-CREF-IVC-AMT", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "ADI_CREF_IVC_AMT");
        updt_Ia_Rsl2_Adi_Cref_Ivc_Gd_Ind = updt_Ia_Rsl2_Adi_Ivc_Data.newFieldInGroup("updt_Ia_Rsl2_Adi_Cref_Ivc_Gd_Ind", "ADI-CREF-IVC-GD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_CREF_IVC_GD_IND");
        updt_Ia_Rsl2_Adi_Cref_Prdc_Ivc_Amt = updt_Ia_Rsl2_Adi_Ivc_Data.newFieldInGroup("updt_Ia_Rsl2_Adi_Cref_Prdc_Ivc_Amt", "ADI-CREF-PRDC-IVC-AMT", 
            FieldType.NUMERIC, 9, 2, RepeatingFieldStrategy.None, "ADI_CREF_PRDC_IVC_AMT");

        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Data = vw_updt_Ia_Rsl2.getRecord().newGroupInGroup("updt_Ia_Rsl2_Adi_Dtl_Tiaa_Data", "ADI-DTL-TIAA-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Rate_Cd = updt_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Rate_Cd", "ADI-DTL-TIAA-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt = updt_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt", "ADI-DTL-TIAA-DA-STD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt = updt_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt", "ADI-DTL-TIAA-DA-GRD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Acct_Cd = updt_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Tiaa_Acct_Cd", "ADI-DTL-TIAA-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_ACCT_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd = updt_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd", "ADI-DTL-TIAA-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_IA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Eff_Rte_Intrst = updt_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Tiaa_Eff_Rte_Intrst", "ADI-DTL-TIAA-EFF-RTE-INTRST", 
            FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_EFF_RTE_INTRST", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt = updt_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt", "ADI-DTL-TIAA-GRD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt = updt_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt", "ADI-DTL-TIAA-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt = updt_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt", "ADI-DTL-FNL-GRD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt = updt_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt", "ADI-DTL-FNL-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt = updt_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt", "ADI-DTL-TIAA-STD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt = updt_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt", "ADI-DTL-TIAA-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt = updt_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt", "ADI-DTL-FNL-STD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt = updt_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt", "ADI-DTL-FNL-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");

        updt_Ia_Rsl2_Adi_Dtl_Cref_Data = vw_updt_Ia_Rsl2.getRecord().newGroupInGroup("updt_Ia_Rsl2_Adi_Dtl_Cref_Data", "ADI-DTL-CREF-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Cref_Da_Rate_Cd = updt_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Cref_Da_Rate_Cd", "ADI-DTL-CREF-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_DA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Cref_Acct_Cd = updt_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Cref_Acct_Cd", "ADI-DTL-CREF-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_ACCT_CD", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Cref_Ia_Rate_Cd = updt_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Cref_Ia_Rate_Cd", "ADI-DTL-CREF-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_IA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Cref_Da_Annl_Amt = updt_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Cref_Da_Annl_Amt", "ADI-DTL-CREF-DA-ANNL-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_DA_ANNL_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Cref_Annl_Nbr_Units = updt_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Cref_Annl_Nbr_Units", "ADI-DTL-CREF-ANNL-NBR-UNITS", 
            FieldType.NUMERIC, 10, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_ANNL_NBR_UNITS", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Cref_Ia_Annl_Unit_Val = updt_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Cref_Ia_Annl_Unit_Val", 
            "ADI-DTL-CREF-IA-ANNL-UNIT-VAL", FieldType.NUMERIC, 7, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_IA_ANNL_UNIT_VAL", 
            "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Cref_Annl_Amt = updt_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Cref_Annl_Amt", "ADI-DTL-CREF-ANNL-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_ANNL_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Cref_Da_Mnthly_Amt = updt_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Cref_Da_Mnthly_Amt", "ADI-DTL-CREF-DA-MNTHLY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_DA_MNTHLY_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Cref_Mnthly_Nbr_Units = updt_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Cref_Mnthly_Nbr_Units", 
            "ADI-DTL-CREF-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 10, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_MNTHLY_NBR_UNITS", 
            "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val = updt_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val", 
            "ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL", FieldType.NUMERIC, 7, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_CREF_IA_MNTHLY_UNIT_VAL", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rsl2_Adi_Dtl_Cref_Mnthly_Amt = updt_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Cref_Mnthly_Amt", "ADI-DTL-CREF-MNTHLY-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_MNTHLY_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        updt_Ia_Rsl2_Adi_Optn_Cde = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Optn_Cde", "ADI-OPTN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "ADI_OPTN_CDE");
        updt_Ia_Rsl2_Adi_Tiaa_Sttlmnt = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Tiaa_Sttlmnt", "ADI-TIAA-STTLMNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_TIAA_STTLMNT");
        updt_Ia_Rsl2_Adi_Tiaa_Re_Sttlmnt = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Tiaa_Re_Sttlmnt", "ADI-TIAA-RE-STTLMNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_TIAA_RE_STTLMNT");
        updt_Ia_Rsl2_Adi_Cref_Sttlmnt = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Cref_Sttlmnt", "ADI-CREF-STTLMNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_CREF_STTLMNT");

        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat = vw_updt_Ia_Rsl2.getRecord().newGroupInGroup("updt_Ia_Rsl2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat", "ADI-DTL-TIAA-TPA-GUAR-COMMUT-DAT", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt = updt_Ia_Rsl2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt", 
            "ADI-DTL-TIAA-GUAR-COMMUT-STD-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_TIAA_GUAR_COMMUT_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt = updt_Ia_Rsl2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt", 
            "ADI-DTL-TIAA-GUAR-COMMUT-GRD-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_TIAA_GUAR_COMMUT_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        updt_Ia_Rsl2_Adi_Orig_Lob_IndMuGroup = vw_updt_Ia_Rsl2.getRecord().newGroupInGroup("UPDT_IA_RSL2_ADI_ORIG_LOB_INDMuGroup", "ADI_ORIG_LOB_INDMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_IA_RSLT_ADI_ORIG_LOB_IND");
        updt_Ia_Rsl2_Adi_Orig_Lob_Ind = updt_Ia_Rsl2_Adi_Orig_Lob_IndMuGroup.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Orig_Lob_Ind", "ADI-ORIG-LOB-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 12), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_ORIG_LOB_IND");
        updt_Ia_Rsl2_Adi_Roth_Rqst_Ind = vw_updt_Ia_Rsl2.getRecord().newFieldInGroup("updt_Ia_Rsl2_Adi_Roth_Rqst_Ind", "ADI-ROTH-RQST-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_ROTH_RQST_IND");
        updt_Ia_Rsl2_Adi_Contract_IdMuGroup = vw_updt_Ia_Rsl2.getRecord().newGroupInGroup("UPDT_IA_RSL2_ADI_CONTRACT_IDMuGroup", "ADI_CONTRACT_IDMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_IA_RSLT_ADI_CONTRACT_ID");
        updt_Ia_Rsl2_Adi_Contract_Id = updt_Ia_Rsl2_Adi_Contract_IdMuGroup.newFieldArrayInGroup("updt_Ia_Rsl2_Adi_Contract_Id", "ADI-CONTRACT-ID", FieldType.NUMERIC, 
            11, new DbsArrayController(1, 125), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_CONTRACT_ID");
        registerRecord(vw_updt_Ia_Rsl2);

        vw_ads_Ia_Rsl2 = new DataAccessProgramView(new NameInfo("vw_ads_Ia_Rsl2", "ADS-IA-RSL2"), "ADS_IA_RSLT", "ADS_IA_RSLT", DdmPeriodicGroups.getInstance().getGroups("ADS_IA_RSLT"));
        ads_Ia_Rsl2_Rqst_Id = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "RQST_ID");
        ads_Ia_Rsl2_Adi_Ia_Rcrd_Cde = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Ia_Rcrd_Cde", "ADI-IA-RCRD-CDE", FieldType.STRING, 3, 
            RepeatingFieldStrategy.None, "ADI_IA_RCRD_CDE");
        ads_Ia_Rsl2_Adi_Sqnce_Nbr = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Sqnce_Nbr", "ADI-SQNCE-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "ADI_SQNCE_NBR");
        ads_Ia_Rsl2_Adi_Stts_Cde = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Stts_Cde", "ADI-STTS-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "ADI_STTS_CDE");
        ads_Ia_Rsl2_Adi_Tiaa_NbrsMuGroup = vw_ads_Ia_Rsl2.getRecord().newGroupInGroup("ADS_IA_RSL2_ADI_TIAA_NBRSMuGroup", "ADI_TIAA_NBRSMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "ADS_IA_RSLT_ADI_TIAA_NBRS");
        ads_Ia_Rsl2_Adi_Tiaa_Nbrs = ads_Ia_Rsl2_Adi_Tiaa_NbrsMuGroup.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Tiaa_Nbrs", "ADI-TIAA-NBRS", FieldType.STRING, 
            10, new DbsArrayController(1, 12), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_TIAA_NBRS");
        ads_Ia_Rsl2_Adi_Cref_NbrsMuGroup = vw_ads_Ia_Rsl2.getRecord().newGroupInGroup("ADS_IA_RSL2_ADI_CREF_NBRSMuGroup", "ADI_CREF_NBRSMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "ADS_IA_RSLT_ADI_CREF_NBRS");
        ads_Ia_Rsl2_Adi_Cref_Nbrs = ads_Ia_Rsl2_Adi_Cref_NbrsMuGroup.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Cref_Nbrs", "ADI-CREF-NBRS", FieldType.STRING, 
            10, new DbsArrayController(1, 12), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_CREF_NBRS");
        ads_Ia_Rsl2_Adi_Ia_Tiaa_Nbr = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Ia_Tiaa_Nbr", "ADI-IA-TIAA-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "ADI_IA_TIAA_NBR");
        ads_Ia_Rsl2_Adi_Ia_Tiaa_Payee_Cde = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Ia_Tiaa_Payee_Cde", "ADI-IA-TIAA-PAYEE-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADI_IA_TIAA_PAYEE_CDE");
        ads_Ia_Rsl2_Adi_Ia_Cref_Nbr = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Ia_Cref_Nbr", "ADI-IA-CREF-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "ADI_IA_CREF_NBR");
        ads_Ia_Rsl2_Adi_Ia_Cref_Payee_Cde = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Ia_Cref_Payee_Cde", "ADI-IA-CREF-PAYEE-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADI_IA_CREF_PAYEE_CDE");
        ads_Ia_Rsl2_Adi_Curncy_Cde = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Curncy_Cde", "ADI-CURNCY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ADI_CURNCY_CDE");
        ads_Ia_Rsl2_Adi_Pymnt_Cde = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Pymnt_Cde", "ADI-PYMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ADI_PYMNT_CDE");
        ads_Ia_Rsl2_Adi_Pymnt_Mode = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Pymnt_Mode", "ADI-PYMNT-MODE", FieldType.NUMERIC, 3, 
            RepeatingFieldStrategy.None, "ADI_PYMNT_MODE");
        ads_Ia_Rsl2_Adi_Lst_Actvty_Dte = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Lst_Actvty_Dte", "ADI-LST-ACTVTY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_LST_ACTVTY_DTE");
        ads_Ia_Rsl2_Adi_Annty_Strt_Dte = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Annty_Strt_Dte", "ADI-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_ANNTY_STRT_DTE");
        ads_Ia_Rsl2_Adi_Instllmnt_Dte = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Instllmnt_Dte", "ADI-INSTLLMNT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_INSTLLMNT_DTE");
        ads_Ia_Rsl2_Adi_Frst_Pymnt_Due_Dte = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Frst_Pymnt_Due_Dte", "ADI-FRST-PYMNT-DUE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADI_FRST_PYMNT_DUE_DTE");
        ads_Ia_Rsl2_Adi_Frst_Ck_Pd_Dte = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Frst_Ck_Pd_Dte", "ADI-FRST-CK-PD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_FRST_CK_PD_DTE");
        ads_Ia_Rsl2_Adi_Finl_Periodic_Py_Dte = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Finl_Periodic_Py_Dte", "ADI-FINL-PERIODIC-PY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADI_FINL_PERIODIC_PY_DTE");
        ads_Ia_Rsl2_Adi_Finl_Prtl_Py_Dte = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Finl_Prtl_Py_Dte", "ADI-FINL-PRTL-PY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_FINL_PRTL_PY_DTE");

        ads_Ia_Rsl2_Adi_Ivc_Data = vw_ads_Ia_Rsl2.getRecord().newGroupInGroup("ADS_IA_RSL2_ADI_IVC_DATA", "ADI-IVC-DATA");
        ads_Ia_Rsl2_Adi_Tiaa_Ivc_Amt = ads_Ia_Rsl2_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rsl2_Adi_Tiaa_Ivc_Amt", "ADI-TIAA-IVC-AMT", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "ADI_TIAA_IVC_AMT");
        ads_Ia_Rsl2_Adi_Tiaa_Ivc_Gd_Ind = ads_Ia_Rsl2_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rsl2_Adi_Tiaa_Ivc_Gd_Ind", "ADI-TIAA-IVC-GD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_TIAA_IVC_GD_IND");
        ads_Ia_Rsl2_Adi_Tiaa_Prdc_Ivc_Amt = ads_Ia_Rsl2_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rsl2_Adi_Tiaa_Prdc_Ivc_Amt", "ADI-TIAA-PRDC-IVC-AMT", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "ADI_TIAA_PRDC_IVC_AMT");
        ads_Ia_Rsl2_Adi_Cref_Ivc_Amt = ads_Ia_Rsl2_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rsl2_Adi_Cref_Ivc_Amt", "ADI-CREF-IVC-AMT", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "ADI_CREF_IVC_AMT");
        ads_Ia_Rsl2_Adi_Cref_Ivc_Gd_Ind = ads_Ia_Rsl2_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rsl2_Adi_Cref_Ivc_Gd_Ind", "ADI-CREF-IVC-GD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_CREF_IVC_GD_IND");
        ads_Ia_Rsl2_Adi_Cref_Prdc_Ivc_Amt = ads_Ia_Rsl2_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rsl2_Adi_Cref_Prdc_Ivc_Amt", "ADI-CREF-PRDC-IVC-AMT", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "ADI_CREF_PRDC_IVC_AMT");

        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data = vw_ads_Ia_Rsl2.getRecord().newGroupInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data", "ADI-DTL-TIAA-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Rate_Cd = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Rate_Cd", "ADI-DTL-TIAA-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt", "ADI-DTL-TIAA-DA-STD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt", "ADI-DTL-TIAA-DA-GRD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Acct_Cd = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Acct_Cd", "ADI-DTL-TIAA-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_ACCT_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd", "ADI-DTL-TIAA-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_IA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Eff_Rte_Intrst = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Eff_Rte_Intrst", "ADI-DTL-TIAA-EFF-RTE-INTRST", 
            FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_EFF_RTE_INTRST", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt", "ADI-DTL-TIAA-GRD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt", "ADI-DTL-TIAA-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt", "ADI-DTL-FNL-GRD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt", "ADI-DTL-FNL-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt", "ADI-DTL-TIAA-STD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt", "ADI-DTL-TIAA-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt", "ADI-DTL-FNL-STD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt", "ADI-DTL-FNL-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");

        ads_Ia_Rsl2_Adi_Dtl_Cref_Data = vw_ads_Ia_Rsl2.getRecord().newGroupInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Data", "ADI-DTL-CREF-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Da_Rate_Cd = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Da_Rate_Cd", "ADI-DTL-CREF-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_DA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Acct_Cd = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Acct_Cd", "ADI-DTL-CREF-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_ACCT_CD", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Ia_Rate_Cd = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Ia_Rate_Cd", "ADI-DTL-CREF-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_IA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Da_Annl_Amt = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Da_Annl_Amt", "ADI-DTL-CREF-DA-ANNL-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_DA_ANNL_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Annl_Nbr_Units = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Annl_Nbr_Units", "ADI-DTL-CREF-ANNL-NBR-UNITS", 
            FieldType.NUMERIC, 10, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_ANNL_NBR_UNITS", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Ia_Annl_Unit_Val = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Ia_Annl_Unit_Val", "ADI-DTL-CREF-IA-ANNL-UNIT-VAL", 
            FieldType.NUMERIC, 7, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_IA_ANNL_UNIT_VAL", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Annl_Amt = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Annl_Amt", "ADI-DTL-CREF-ANNL-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_ANNL_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Da_Mnthly_Amt = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Da_Mnthly_Amt", "ADI-DTL-CREF-DA-MNTHLY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_DA_MNTHLY_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Mnthly_Nbr_Units = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Mnthly_Nbr_Units", "ADI-DTL-CREF-MNTHLY-NBR-UNITS", 
            FieldType.NUMERIC, 10, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_MNTHLY_NBR_UNITS", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val", 
            "ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL", FieldType.NUMERIC, 7, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_CREF_IA_MNTHLY_UNIT_VAL", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Mnthly_Amt = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Mnthly_Amt", "ADI-DTL-CREF-MNTHLY-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_MNTHLY_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Optn_Cde = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Optn_Cde", "ADI-OPTN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "ADI_OPTN_CDE");
        ads_Ia_Rsl2_Adi_Tiaa_Sttlmnt = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Tiaa_Sttlmnt", "ADI-TIAA-STTLMNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_TIAA_STTLMNT");
        ads_Ia_Rsl2_Adi_Tiaa_Re_Sttlmnt = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Tiaa_Re_Sttlmnt", "ADI-TIAA-RE-STTLMNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_TIAA_RE_STTLMNT");
        ads_Ia_Rsl2_Adi_Cref_Sttlmnt = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Cref_Sttlmnt", "ADI-CREF-STTLMNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_CREF_STTLMNT");

        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat = vw_ads_Ia_Rsl2.getRecord().newGroupInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat", "ADI-DTL-TIAA-TPA-GUAR-COMMUT-DAT", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt", 
            "ADI-DTL-TIAA-GUAR-COMMUT-STD-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_TIAA_GUAR_COMMUT_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Tpa_Guar_Commut_Dat.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt", 
            "ADI-DTL-TIAA-GUAR-COMMUT-GRD-AMT", FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_TIAA_GUAR_COMMUT_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_TPA_GUAR_COMMUT_DAT");
        ads_Ia_Rsl2_Adi_Orig_Lob_IndMuGroup = vw_ads_Ia_Rsl2.getRecord().newGroupInGroup("ADS_IA_RSL2_ADI_ORIG_LOB_INDMuGroup", "ADI_ORIG_LOB_INDMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_IA_RSLT_ADI_ORIG_LOB_IND");
        ads_Ia_Rsl2_Adi_Orig_Lob_Ind = ads_Ia_Rsl2_Adi_Orig_Lob_IndMuGroup.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Orig_Lob_Ind", "ADI-ORIG-LOB-IND", FieldType.STRING, 
            1, new DbsArrayController(1, 12), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_ORIG_LOB_IND");
        ads_Ia_Rsl2_Adi_Roth_Rqst_Ind = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Roth_Rqst_Ind", "ADI-ROTH-RQST-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_ROTH_RQST_IND");
        ads_Ia_Rsl2_Adi_Contract_IdMuGroup = vw_ads_Ia_Rsl2.getRecord().newGroupInGroup("ADS_IA_RSL2_ADI_CONTRACT_IDMuGroup", "ADI_CONTRACT_IDMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_IA_RSLT_ADI_CONTRACT_ID");
        ads_Ia_Rsl2_Adi_Contract_Id = ads_Ia_Rsl2_Adi_Contract_IdMuGroup.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Contract_Id", "ADI-CONTRACT-ID", FieldType.NUMERIC, 
            11, new DbsArrayController(1, 125), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_CONTRACT_ID");
        registerRecord(vw_ads_Ia_Rsl2);

        vw_iaa_Old_Tiaa_Rates = new DataAccessProgramView(new NameInfo("vw_iaa_Old_Tiaa_Rates", "IAA-OLD-TIAA-RATES"), "IAA_OLD_TIAA_RATES", "IA_OLD_RATES", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_OLD_TIAA_RATES"));
        iaa_Old_Tiaa_Rates_Fund_Lst_Pd_Dte = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Fund_Lst_Pd_Dte", "FUND-LST-PD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "FUND_LST_PD_DTE");
        iaa_Old_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Fund_Invrse_Lst_Pd_Dte", "FUND-INVRSE-LST-PD-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "FUND_INVRSE_LST_PD_DTE");
        iaa_Old_Tiaa_Rates_Trans_Dte = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Old_Tiaa_Rates_Trans_User_Area = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Trans_User_Area", "TRANS-USER-AREA", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Old_Tiaa_Rates_Trans_User_Id = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Old_Tiaa_Rates_Trans_Verify_Id = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Trans_Verify_Id", "TRANS-VERIFY-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Old_Tiaa_Rates_Trans_Verify_Dte = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Trans_Verify_Dte", "TRANS-VERIFY-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TRANS_VERIFY_DTE");
        iaa_Old_Tiaa_Rates_Cntrct_Ppcn_Nbr = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Old_Tiaa_Rates_Cntrct_Payee_Cde = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        iaa_Old_Tiaa_Rates_Cntrct_Mode_Ind = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Old_Tiaa_Rates_Cmpny_Fund_Cde = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Cmpny_Fund_Cde", "CMPNY-FUND-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CMPNY_FUND_CDE");

        iaa_Old_Tiaa_Rates__R_Field_2 = vw_iaa_Old_Tiaa_Rates.getRecord().newGroupInGroup("iaa_Old_Tiaa_Rates__R_Field_2", "REDEFINE", iaa_Old_Tiaa_Rates_Cmpny_Fund_Cde);
        iaa_Old_Tiaa_Rates_Cmpny_Cde = iaa_Old_Tiaa_Rates__R_Field_2.newFieldInGroup("iaa_Old_Tiaa_Rates_Cmpny_Cde", "CMPNY-CDE", FieldType.STRING, 1);
        iaa_Old_Tiaa_Rates_Fund_Cde = iaa_Old_Tiaa_Rates__R_Field_2.newFieldInGroup("iaa_Old_Tiaa_Rates_Fund_Cde", "FUND-CDE", FieldType.STRING, 2);
        iaa_Old_Tiaa_Rates_Rcrd_Srce = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Rcrd_Srce", "RCRD-SRCE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "RCRD_SRCE");
        iaa_Old_Tiaa_Rates_Rcrd_Status = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Rcrd_Status", "RCRD-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_STATUS");
        iaa_Old_Tiaa_Rates_Cntrct_Tot_Per_Amt = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Cntrct_Tot_Per_Amt", "CNTRCT-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_TOT_PER_AMT");
        iaa_Old_Tiaa_Rates_Cntrct_Tot_Div_Amt = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Cntrct_Tot_Div_Amt", "CNTRCT-TOT-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_TOT_DIV_AMT");
        iaa_Old_Tiaa_Rates_Count_Casttiaa_Rate_Data_Grp = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Count_Casttiaa_Rate_Data_Grp", 
            "C*TIAA-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_OLD_RATES_TIAA_RATE_DATA_GRP");

        iaa_Old_Tiaa_Rates_Tiaa_Rate_Data_Grp = vw_iaa_Old_Tiaa_Rates.getRecord().newGroupInGroup("IAA_OLD_TIAA_RATES_TIAA_RATE_DATA_GRP", "TIAA-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Tiaa_Rates_Tiaa_Rate_Cde = iaa_Old_Tiaa_Rates_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Tiaa_Rate_Cde", "TIAA-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Tiaa_Rates_Tiaa_Rate_Dte = iaa_Old_Tiaa_Rates_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Tiaa_Rate_Dte", "TIAA-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_DTE", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Tiaa_Rates_Tiaa_Per_Pay_Amt = iaa_Old_Tiaa_Rates_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Tiaa_Per_Pay_Amt", "TIAA-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Tiaa_Rates_Tiaa_Per_Div_Amt = iaa_Old_Tiaa_Rates_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Tiaa_Per_Div_Amt", "TIAA-PER-DIV-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_DIV_AMT", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Tiaa_Rates_Tiaa_Rate_Final_Pay_Amt = iaa_Old_Tiaa_Rates_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Tiaa_Rate_Final_Pay_Amt", 
            "TIAA-RATE-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_PAY_AMT", 
            "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Tiaa_Rates_Tiaa_Rate_Final_Div_Amt = iaa_Old_Tiaa_Rates_Tiaa_Rate_Data_Grp.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Tiaa_Rate_Final_Div_Amt", 
            "TIAA-RATE-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 250) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_FINAL_DIV_AMT", 
            "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Tiaa_Rates_Lst_Trans_Dte = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Old_Tiaa_Rates_Tiaa_Rate_GicMuGroup = vw_iaa_Old_Tiaa_Rates.getRecord().newGroupInGroup("IAA_OLD_TIAA_RATES_TIAA_RATE_GICMuGroup", "TIAA_RATE_GICMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "IA_OLD_RATES_TIAA_RATE_GIC");
        iaa_Old_Tiaa_Rates_Tiaa_Rate_Gic = iaa_Old_Tiaa_Rates_Tiaa_Rate_GicMuGroup.newFieldArrayInGroup("iaa_Old_Tiaa_Rates_Tiaa_Rate_Gic", "TIAA-RATE-GIC", 
            FieldType.NUMERIC, 11, new DbsArrayController(1, 250), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "TIAA_RATE_GIC");
        iaa_Old_Tiaa_Rates_Tiaa_Mode_Ind = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Tiaa_Mode_Ind", "TIAA-MODE-IND", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "TIAA_MODE_IND");
        iaa_Old_Tiaa_Rates_Tiaa_Old_Cmpny_Fund = vw_iaa_Old_Tiaa_Rates.getRecord().newFieldInGroup("iaa_Old_Tiaa_Rates_Tiaa_Old_Cmpny_Fund", "TIAA-OLD-CMPNY-FUND", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_OLD_CMPNY_FUND");
        registerRecord(vw_iaa_Old_Tiaa_Rates);

        vw_iaa_Old_Cref_Rates = new DataAccessProgramView(new NameInfo("vw_iaa_Old_Cref_Rates", "IAA-OLD-CREF-RATES"), "IAA_OLD_CREF_RATES", "IA_OLD_RATES", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_OLD_CREF_RATES"));
        iaa_Old_Cref_Rates_Fund_Lst_Pd_Dte = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Fund_Lst_Pd_Dte", "FUND-LST-PD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "FUND_LST_PD_DTE");
        iaa_Old_Cref_Rates_Fund_Invrse_Lst_Pd_Dte = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Fund_Invrse_Lst_Pd_Dte", "FUND-INVRSE-LST-PD-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "FUND_INVRSE_LST_PD_DTE");
        iaa_Old_Cref_Rates_Trans_Dte = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Old_Cref_Rates_Trans_User_Area = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Trans_User_Area", "TRANS-USER-AREA", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Old_Cref_Rates_Trans_User_Id = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Old_Cref_Rates_Trans_Verify_Id = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Trans_Verify_Id", "TRANS-VERIFY-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Old_Cref_Rates_Trans_Verify_Dte = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Trans_Verify_Dte", "TRANS-VERIFY-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TRANS_VERIFY_DTE");
        iaa_Old_Cref_Rates_Cntrct_Ppcn_Nbr = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Old_Cref_Rates_Cntrct_Payee_Cde = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        iaa_Old_Cref_Rates_Cntrct_Mode_Ind = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Old_Cref_Rates_Rcrd_Srce = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Rcrd_Srce", "RCRD-SRCE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "RCRD_SRCE");
        iaa_Old_Cref_Rates_Rcrd_Status = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Rcrd_Status", "RCRD-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_STATUS");
        iaa_Old_Cref_Rates_Cmpny_Fund_Cde = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Cmpny_Fund_Cde", "CMPNY-FUND-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CMPNY_FUND_CDE");

        iaa_Old_Cref_Rates__R_Field_3 = vw_iaa_Old_Cref_Rates.getRecord().newGroupInGroup("iaa_Old_Cref_Rates__R_Field_3", "REDEFINE", iaa_Old_Cref_Rates_Cmpny_Fund_Cde);
        iaa_Old_Cref_Rates_Cmpny_Cde = iaa_Old_Cref_Rates__R_Field_3.newFieldInGroup("iaa_Old_Cref_Rates_Cmpny_Cde", "CMPNY-CDE", FieldType.STRING, 1);
        iaa_Old_Cref_Rates_Fund_Cde = iaa_Old_Cref_Rates__R_Field_3.newFieldInGroup("iaa_Old_Cref_Rates_Fund_Cde", "FUND-CDE", FieldType.STRING, 2);
        iaa_Old_Cref_Rates_Cntrct_Tot_Per_Amt = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Cntrct_Tot_Per_Amt", "CNTRCT-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_TOT_PER_AMT");
        iaa_Old_Cref_Rates_Cntrct_Unit_Val = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("IAA_OLD_CREF_RATES_CNTRCT_UNIT_VAL", "CNTRCT-UNIT-VAL", 
            FieldType.PACKED_DECIMAL, 9, 4, RepeatingFieldStrategy.None, "CNTRCT_TOT_DIV_AMT");
        iaa_Old_Cref_Rates_Cntrct_Tot_Units = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Cntrct_Tot_Units", "CNTRCT-TOT-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3, RepeatingFieldStrategy.None, "CNTRCT_TOT_UNITS");
        iaa_Old_Cref_Rates_Lst_Trans_Dte = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Old_Cref_Rates_Count_Castcref_Rate_Data_Grp = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Count_Castcref_Rate_Data_Grp", 
            "C*CREF-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_OLD_RATES_CREF_RATE_DATA_GRP");

        iaa_Old_Cref_Rates_Cref_Rate_Data_Grp = vw_iaa_Old_Cref_Rates.getRecord().newGroupInGroup("iaa_Old_Cref_Rates_Cref_Rate_Data_Grp", "CREF-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Cref_Rates_Cref_Rate_Cde = iaa_Old_Cref_Rates_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_OLD_CREF_RATES_CREF_RATE_CDE", "CREF-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Cref_Rates_Cref_Rate_Dte = iaa_Old_Cref_Rates_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_OLD_CREF_RATES_CREF_RATE_DTE", "CREF-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_DTE", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Cref_Rates_Cref_Per_Pay_Amt = iaa_Old_Cref_Rates_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_OLD_CREF_RATES_CREF_PER_PAY_AMT", "CREF-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Cref_Rates_Cref_No_Units = iaa_Old_Cref_Rates_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_OLD_CREF_RATES_CREF_NO_UNITS", "CREF-NO-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_NO_UNITS", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        registerRecord(vw_iaa_Old_Cref_Rates);

        vw_cref_Fund_Rcrd = new DataAccessProgramView(new NameInfo("vw_cref_Fund_Rcrd", "CREF-FUND-RCRD"), "IAA_CREF_FUND_RCRD_1", "IA_MULTI_FUNDS", DdmPeriodicGroups.getInstance().getGroups("IAA_CREF_FUND_RCRD_1"));
        cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr = vw_cref_Fund_Rcrd.getRecord().newFieldInGroup("cref_Fund_Rcrd_Cref_Cntrct_Ppcn_Nbr", "CREF-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CREF_CNTRCT_PPCN_NBR");
        cref_Fund_Rcrd_Cref_Cntrct_Payee_Cde = vw_cref_Fund_Rcrd.getRecord().newFieldInGroup("CREF_FUND_RCRD_CREF_CNTRCT_PAYEE_CDE", "CREF-CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "TIAA_CNTRCT_PAYEE_CDE");
        cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde = vw_cref_Fund_Rcrd.getRecord().newFieldInGroup("CREF_FUND_RCRD_CREF_CMPNY_FUND_CDE", "CREF-CMPNY-FUND-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "TIAA_CMPNY_FUND_CDE");

        cref_Fund_Rcrd__R_Field_4 = vw_cref_Fund_Rcrd.getRecord().newGroupInGroup("cref_Fund_Rcrd__R_Field_4", "REDEFINE", cref_Fund_Rcrd_Cref_Cmpny_Fund_Cde);
        cref_Fund_Rcrd_Cref_Cmpny_Cde = cref_Fund_Rcrd__R_Field_4.newFieldInGroup("cref_Fund_Rcrd_Cref_Cmpny_Cde", "CREF-CMPNY-CDE", FieldType.STRING, 
            1);
        cref_Fund_Rcrd_Cref_Fund_Cde = cref_Fund_Rcrd__R_Field_4.newFieldInGroup("cref_Fund_Rcrd_Cref_Fund_Cde", "CREF-FUND-CDE", FieldType.STRING, 2);
        cref_Fund_Rcrd_Cref_Tot_Per_Amt = vw_cref_Fund_Rcrd.getRecord().newFieldInGroup("cref_Fund_Rcrd_Cref_Tot_Per_Amt", "CREF-TOT-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "CREF_TOT_PER_AMT");
        cref_Fund_Rcrd_Cref_Unit_Val = vw_cref_Fund_Rcrd.getRecord().newFieldInGroup("CREF_FUND_RCRD_CREF_UNIT_VAL", "CREF-UNIT-VAL", FieldType.PACKED_DECIMAL, 
            9, 4, RepeatingFieldStrategy.None, "TIAA_TOT_DIV_AMT");
        cref_Fund_Rcrd_Cref_Old_Per_Amt = vw_cref_Fund_Rcrd.getRecord().newFieldInGroup("CREF_FUND_RCRD_CREF_OLD_PER_AMT", "CREF-OLD-PER-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "AJ");
        cref_Fund_Rcrd_Cref_Old_Unit_Val = vw_cref_Fund_Rcrd.getRecord().newFieldInGroup("cref_Fund_Rcrd_Cref_Old_Unit_Val", "CREF-OLD-UNIT-VAL", FieldType.PACKED_DECIMAL, 
            9, 4, RepeatingFieldStrategy.None, "CREF_OLD_UNIT_VAL");
        cref_Fund_Rcrd_Count_Castcref_Rate_Data_Grp = vw_cref_Fund_Rcrd.getRecord().newFieldInGroup("cref_Fund_Rcrd_Count_Castcref_Rate_Data_Grp", "C*CREF-RATE-DATA-GRP", 
            RepeatingFieldStrategy.CAsteriskVariable, "IA_MULTI_FUNDS_CREF_RATE_DATA_GRP");

        cref_Fund_Rcrd_Cref_Rate_Data_Grp = vw_cref_Fund_Rcrd.getRecord().newGroupInGroup("CREF_FUND_RCRD_CREF_RATE_DATA_GRP", "CREF-RATE-DATA-GRP", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        cref_Fund_Rcrd_Cref_Rate_Cde = cref_Fund_Rcrd_Cref_Rate_Data_Grp.newFieldArrayInGroup("CREF_FUND_RCRD_CREF_RATE_CDE", "CREF-RATE-CDE", FieldType.STRING, 
            2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AM", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        cref_Fund_Rcrd_Cref_Rate_Dte = cref_Fund_Rcrd_Cref_Rate_Data_Grp.newFieldArrayInGroup("CREF_FUND_RCRD_CREF_RATE_DTE", "CREF-RATE-DTE", FieldType.DATE, 
            new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AN", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        cref_Fund_Rcrd_Cref_Units_Cnt = cref_Fund_Rcrd_Cref_Rate_Data_Grp.newFieldArrayInGroup("CREF_FUND_RCRD_CREF_UNITS_CNT", "CREF-UNITS-CNT", FieldType.PACKED_DECIMAL, 
            9, 3, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "AQ", "IA_MULTI_FUNDS_TIAA_RATE_DATA_GRP");
        cref_Fund_Rcrd_Lst_Trans_Dte = vw_cref_Fund_Rcrd.getRecord().newFieldInGroup("cref_Fund_Rcrd_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        cref_Fund_Rcrd_Cref_Xfr_Iss_Dte = vw_cref_Fund_Rcrd.getRecord().newFieldInGroup("CREF_FUND_RCRD_CREF_XFR_ISS_DTE", "CREF-XFR-ISS-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "TIAA_XFR_ISS_DTE");
        cref_Fund_Rcrd_Cref_Lst_Xfr_In_Dte = vw_cref_Fund_Rcrd.getRecord().newFieldInGroup("cref_Fund_Rcrd_Cref_Lst_Xfr_In_Dte", "CREF-LST-XFR-IN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_IN_DTE");
        cref_Fund_Rcrd_Cref_Lst_Xfr_Out_Dte = vw_cref_Fund_Rcrd.getRecord().newFieldInGroup("cref_Fund_Rcrd_Cref_Lst_Xfr_Out_Dte", "CREF-LST-XFR-OUT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CREF_LST_XFR_OUT_DTE");
        registerRecord(vw_cref_Fund_Rcrd);

        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_5", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_5.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_Cntrct_Fund_Key = localVariables.newFieldInRecord("pnd_Cntrct_Fund_Key", "#CNTRCT-FUND-KEY", FieldType.STRING, 15);

        pnd_Cntrct_Fund_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Cntrct_Fund_Key__R_Field_6", "REDEFINE", pnd_Cntrct_Fund_Key);
        pnd_Cntrct_Fund_Key_Pnd_Cntrct_Ppcn_Nbr = pnd_Cntrct_Fund_Key__R_Field_6.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Fund_Key_Pnd_Cntrct_Payee_Cde = pnd_Cntrct_Fund_Key__R_Field_6.newFieldInGroup("pnd_Cntrct_Fund_Key_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cref_Fund_Key = localVariables.newFieldInRecord("pnd_Cref_Fund_Key", "#CREF-FUND-KEY", FieldType.STRING, 15);

        pnd_Cref_Fund_Key__R_Field_7 = localVariables.newGroupInRecord("pnd_Cref_Fund_Key__R_Field_7", "REDEFINE", pnd_Cref_Fund_Key);
        pnd_Cref_Fund_Key__Filler1 = pnd_Cref_Fund_Key__R_Field_7.newFieldInGroup("pnd_Cref_Fund_Key__Filler1", "_FILLER1", FieldType.STRING, 12);
        pnd_Cref_Fund_Key_Pnd_Cmpny_Cde = pnd_Cref_Fund_Key__R_Field_7.newFieldInGroup("pnd_Cref_Fund_Key_Pnd_Cmpny_Cde", "#CMPNY-CDE", FieldType.STRING, 
            1);
        pnd_Cref_Fund_Key_Pnd_Fnd_Cde = pnd_Cref_Fund_Key__R_Field_7.newFieldInGroup("pnd_Cref_Fund_Key_Pnd_Fnd_Cde", "#FND-CDE", FieldType.STRING, 2);
        pnd_Tiaa = localVariables.newFieldInRecord("pnd_Tiaa", "#TIAA", FieldType.BOOLEAN, 1);
        pnd_Rea = localVariables.newFieldInRecord("pnd_Rea", "#REA", FieldType.BOOLEAN, 1);
        pnd_Cref = localVariables.newFieldInRecord("pnd_Cref", "#CREF", FieldType.BOOLEAN, 1);
        pnd_Rslt = localVariables.newFieldInRecord("pnd_Rslt", "#RSLT", FieldType.BOOLEAN, 1);
        pnd_Fund_1 = localVariables.newFieldInRecord("pnd_Fund_1", "#FUND-1", FieldType.STRING, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.PACKED_DECIMAL, 3);
        pnd_T_Cnt = localVariables.newFieldInRecord("pnd_T_Cnt", "#T-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_I_Cnt = localVariables.newFieldInRecord("pnd_I_Cnt", "#I-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Rate_No = localVariables.newFieldInRecord("pnd_Rate_No", "#RATE-NO", FieldType.PACKED_DECIMAL, 3);
        pnd_Rate_Xx = localVariables.newFieldInRecord("pnd_Rate_Xx", "#RATE-XX", FieldType.PACKED_DECIMAL, 3);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 2);
        pnd_Trans_Dte = localVariables.newFieldInRecord("pnd_Trans_Dte", "#TRANS-DTE", FieldType.TIME);
        pnd_Trans_Check_Dte = localVariables.newFieldInRecord("pnd_Trans_Check_Dte", "#TRANS-CHECK-DTE", FieldType.STRING, 8);
        pnd_Trans_Prev_Dte = localVariables.newFieldInRecord("pnd_Trans_Prev_Dte", "#TRANS-PREV-DTE", FieldType.STRING, 8);

        pnd_Trans_Prev_Dte__R_Field_8 = localVariables.newGroupInRecord("pnd_Trans_Prev_Dte__R_Field_8", "REDEFINE", pnd_Trans_Prev_Dte);
        pnd_Trans_Prev_Dte_Pnd_Trans_Prev_Yyyy = pnd_Trans_Prev_Dte__R_Field_8.newFieldInGroup("pnd_Trans_Prev_Dte_Pnd_Trans_Prev_Yyyy", "#TRANS-PREV-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Trans_Prev_Dte_Pnd_Trans_Prev_Mm = pnd_Trans_Prev_Dte__R_Field_8.newFieldInGroup("pnd_Trans_Prev_Dte_Pnd_Trans_Prev_Mm", "#TRANS-PREV-MM", 
            FieldType.NUMERIC, 2);
        pnd_Trans_Prev_Dte_Pnd_Trans_Prev_Dd = pnd_Trans_Prev_Dte__R_Field_8.newFieldInGroup("pnd_Trans_Prev_Dte_Pnd_Trans_Prev_Dd", "#TRANS-PREV-DD", 
            FieldType.NUMERIC, 2);
        pnd_Next_Pymnt_Dte = localVariables.newFieldInRecord("pnd_Next_Pymnt_Dte", "#NEXT-PYMNT-DTE", FieldType.DATE);
        pnd_Invrse_Trans_Dte = localVariables.newFieldInRecord("pnd_Invrse_Trans_Dte", "#INVRSE-TRANS-DTE", FieldType.NUMERIC, 12);
        pnd_Asd_A = localVariables.newFieldInRecord("pnd_Asd_A", "#ASD-A", FieldType.STRING, 8);

        pnd_Asd_A__R_Field_9 = localVariables.newGroupInRecord("pnd_Asd_A__R_Field_9", "REDEFINE", pnd_Asd_A);
        pnd_Asd_A_Pnd_Asd_N = pnd_Asd_A__R_Field_9.newFieldInGroup("pnd_Asd_A_Pnd_Asd_N", "#ASD-N", FieldType.NUMERIC, 8);

        pnd_Asd_A__R_Field_10 = localVariables.newGroupInRecord("pnd_Asd_A__R_Field_10", "REDEFINE", pnd_Asd_A);
        pnd_Asd_A_Pnd_Asd_Yyyy = pnd_Asd_A__R_Field_10.newFieldInGroup("pnd_Asd_A_Pnd_Asd_Yyyy", "#ASD-YYYY", FieldType.NUMERIC, 4);
        pnd_Asd_A_Pnd_Asd_Mm = pnd_Asd_A__R_Field_10.newFieldInGroup("pnd_Asd_A_Pnd_Asd_Mm", "#ASD-MM", FieldType.NUMERIC, 2);
        pnd_Asd_A_Pnd_Asd_Dd = pnd_Asd_A__R_Field_10.newFieldInGroup("pnd_Asd_A_Pnd_Asd_Dd", "#ASD-DD", FieldType.STRING, 2);

        pnd_Asd_A__R_Field_11 = localVariables.newGroupInRecord("pnd_Asd_A__R_Field_11", "REDEFINE", pnd_Asd_A);
        pnd_Asd_A_Pnd_Asd_N6 = pnd_Asd_A__R_Field_11.newFieldInGroup("pnd_Asd_A_Pnd_Asd_N6", "#ASD-N6", FieldType.NUMERIC, 6);
        pnd_Dob = localVariables.newFieldInRecord("pnd_Dob", "#DOB", FieldType.STRING, 8);

        pnd_Dob__R_Field_12 = localVariables.newGroupInRecord("pnd_Dob__R_Field_12", "REDEFINE", pnd_Dob);
        pnd_Dob_Pnd_Dob_Yyyy = pnd_Dob__R_Field_12.newFieldInGroup("pnd_Dob_Pnd_Dob_Yyyy", "#DOB-YYYY", FieldType.NUMERIC, 4);
        pnd_Dob_Pnd_Dob_Mm = pnd_Dob__R_Field_12.newFieldInGroup("pnd_Dob_Pnd_Dob_Mm", "#DOB-MM", FieldType.NUMERIC, 2);
        pnd_Dob_Pnd_Dob_Dd = pnd_Dob__R_Field_12.newFieldInGroup("pnd_Dob_Pnd_Dob_Dd", "#DOB-DD", FieldType.NUMERIC, 2);

        pnd_Dob__R_Field_13 = localVariables.newGroupInRecord("pnd_Dob__R_Field_13", "REDEFINE", pnd_Dob);
        pnd_Dob_Pnd_Dob_N = pnd_Dob__R_Field_13.newFieldInGroup("pnd_Dob_Pnd_Dob_N", "#DOB-N", FieldType.NUMERIC, 8);
        pnd_Dob2 = localVariables.newFieldInRecord("pnd_Dob2", "#DOB2", FieldType.STRING, 8);

        pnd_Dob2__R_Field_14 = localVariables.newGroupInRecord("pnd_Dob2__R_Field_14", "REDEFINE", pnd_Dob2);
        pnd_Dob2_Pnd_Dob2_Yyyy = pnd_Dob2__R_Field_14.newFieldInGroup("pnd_Dob2_Pnd_Dob2_Yyyy", "#DOB2-YYYY", FieldType.NUMERIC, 4);
        pnd_Dob2_Pnd_Dob2_Mm = pnd_Dob2__R_Field_14.newFieldInGroup("pnd_Dob2_Pnd_Dob2_Mm", "#DOB2-MM", FieldType.NUMERIC, 2);
        pnd_Dob2_Pnd_Dob2_Dd = pnd_Dob2__R_Field_14.newFieldInGroup("pnd_Dob2_Pnd_Dob2_Dd", "#DOB2-DD", FieldType.NUMERIC, 2);

        pnd_Dob2__R_Field_15 = localVariables.newGroupInRecord("pnd_Dob2__R_Field_15", "REDEFINE", pnd_Dob2);
        pnd_Dob2_Pnd_Dob2_N = pnd_Dob2__R_Field_15.newFieldInGroup("pnd_Dob2_Pnd_Dob2_N", "#DOB2-N", FieldType.NUMERIC, 8);
        pnd_No_Per_Yr = localVariables.newFieldInRecord("pnd_No_Per_Yr", "#NO-PER-YR", FieldType.PACKED_DECIMAL, 3);
        pnd_Sim_Age = localVariables.newFieldInRecord("pnd_Sim_Age", "#SIM-AGE", FieldType.NUMERIC, 3);
        pnd_Sim_Rule = localVariables.newFieldInRecord("pnd_Sim_Rule", "#SIM-RULE", FieldType.STRING, 1);
        pnd_No_Pays = localVariables.newFieldInRecord("pnd_No_Pays", "#NO-PAYS", FieldType.PACKED_DECIMAL, 3);
        pnd_Tiaa_Ivc = localVariables.newFieldInRecord("pnd_Tiaa_Ivc", "#TIAA-IVC", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cref_Ivc = localVariables.newFieldInRecord("pnd_Cref_Ivc", "#CREF-IVC", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cntrl_Per_Pay_Amt = localVariables.newFieldInRecord("pnd_Cntrl_Per_Pay_Amt", "#CNTRL-PER-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cntrl_Per_Div_Amt = localVariables.newFieldInRecord("pnd_Cntrl_Per_Div_Amt", "#CNTRL-PER-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cntrl_Units_Cnt = localVariables.newFieldInRecord("pnd_Cntrl_Units_Cnt", "#CNTRL-UNITS-CNT", FieldType.PACKED_DECIMAL, 10, 3);
        pnd_Cntrl_Final_Pay_Amt = localVariables.newFieldInRecord("pnd_Cntrl_Final_Pay_Amt", "#CNTRL-FINAL-PAY-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cntrl_Final_Div_Amt = localVariables.newFieldInRecord("pnd_Cntrl_Final_Div_Amt", "#CNTRL-FINAL-DIV-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ivc_Amt = localVariables.newFieldInRecord("pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Prdc_Ivc_Amt = localVariables.newFieldInRecord("pnd_Prdc_Ivc_Amt", "#PRDC-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ivc = localVariables.newFieldInRecord("pnd_Ivc", "#IVC", FieldType.BOOLEAN, 1);
        pnd_Datd = localVariables.newFieldInRecord("pnd_Datd", "#DATD", FieldType.DATE);
        pnd_Rate_Dte = localVariables.newFieldArrayInRecord("pnd_Rate_Dte", "#RATE-DTE", FieldType.DATE, new DbsArrayController(1, 40));
        pnd_Wdte6 = localVariables.newFieldInRecord("pnd_Wdte6", "#WDTE6", FieldType.STRING, 6);
        pnd_Wdte8 = localVariables.newFieldInRecord("pnd_Wdte8", "#WDTE8", FieldType.STRING, 8);

        pnd_Wdte8__R_Field_16 = localVariables.newGroupInRecord("pnd_Wdte8__R_Field_16", "REDEFINE", pnd_Wdte8);
        pnd_Wdte8_Pnd_Wdte_Yyyy = pnd_Wdte8__R_Field_16.newFieldInGroup("pnd_Wdte8_Pnd_Wdte_Yyyy", "#WDTE-YYYY", FieldType.NUMERIC, 4);
        pnd_Wdte8_Pnd_Wdte_Mm = pnd_Wdte8__R_Field_16.newFieldInGroup("pnd_Wdte8_Pnd_Wdte_Mm", "#WDTE-MM", FieldType.NUMERIC, 2);
        pnd_Wdte8_Pnd_Wdte_Dd = pnd_Wdte8__R_Field_16.newFieldInGroup("pnd_Wdte8_Pnd_Wdte_Dd", "#WDTE-DD", FieldType.NUMERIC, 2);
        pnd_Fp_Dte = localVariables.newFieldInRecord("pnd_Fp_Dte", "#FP-DTE", FieldType.DATE);
        pnd_Cntrct_Py_Dte_Key = localVariables.newFieldInRecord("pnd_Cntrct_Py_Dte_Key", "#CNTRCT-PY-DTE-KEY", FieldType.STRING, 23);

        pnd_Cntrct_Py_Dte_Key__R_Field_17 = localVariables.newGroupInRecord("pnd_Cntrct_Py_Dte_Key__R_Field_17", "REDEFINE", pnd_Cntrct_Py_Dte_Key);

        pnd_Cntrct_Py_Dte_Key_Hist_Structure = pnd_Cntrct_Py_Dte_Key__R_Field_17.newGroupInGroup("pnd_Cntrct_Py_Dte_Key_Hist_Structure", "HIST-STRUCTURE");
        pnd_Cntrct_Py_Dte_Key_Cntrct_Ppcn_Nbr = pnd_Cntrct_Py_Dte_Key_Hist_Structure.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Py_Dte_Key_Cntrct_Payee_Cde = pnd_Cntrct_Py_Dte_Key_Hist_Structure.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Py_Dte_Key_Fund_Invrse_Lst_Pd_Dte = pnd_Cntrct_Py_Dte_Key_Hist_Structure.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Fund_Invrse_Lst_Pd_Dte", 
            "FUND-INVRSE-LST-PD-DTE", FieldType.NUMERIC, 8);
        pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde = pnd_Cntrct_Py_Dte_Key_Hist_Structure.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde", "CMPNY-FUND-CDE", 
            FieldType.STRING, 3);
        pnd_Settl_Dte_D = localVariables.newFieldInRecord("pnd_Settl_Dte_D", "#SETTL-DTE-D", FieldType.DATE);
        pnd_Settl_Dte_Grd = localVariables.newFieldInRecord("pnd_Settl_Dte_Grd", "#SETTL-DTE-GRD", FieldType.STRING, 8);

        pnd_Settl_Dte_Grd__R_Field_18 = localVariables.newGroupInRecord("pnd_Settl_Dte_Grd__R_Field_18", "REDEFINE", pnd_Settl_Dte_Grd);
        pnd_Settl_Dte_Grd_Pnd_Settl_Yyyy = pnd_Settl_Dte_Grd__R_Field_18.newFieldInGroup("pnd_Settl_Dte_Grd_Pnd_Settl_Yyyy", "#SETTL-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Settl_Dte_Grd_Pnd_Settl_Mm = pnd_Settl_Dte_Grd__R_Field_18.newFieldInGroup("pnd_Settl_Dte_Grd_Pnd_Settl_Mm", "#SETTL-MM", FieldType.NUMERIC, 
            2);
        pnd_Settl_Dte_Grd_Pnd_Settl_Dd = pnd_Settl_Dte_Grd__R_Field_18.newFieldInGroup("pnd_Settl_Dte_Grd_Pnd_Settl_Dd", "#SETTL-DD", FieldType.NUMERIC, 
            2);
        pnd_Create_Std = localVariables.newFieldInRecord("pnd_Create_Std", "#CREATE-STD", FieldType.BOOLEAN, 1);
        pnd_Updt_Std = localVariables.newFieldInRecord("pnd_Updt_Std", "#UPDT-STD", FieldType.BOOLEAN, 1);
        pnd_Create_Grd = localVariables.newFieldInRecord("pnd_Create_Grd", "#CREATE-GRD", FieldType.BOOLEAN, 1);
        pnd_Updt_Grd = localVariables.newFieldInRecord("pnd_Updt_Grd", "#UPDT-GRD", FieldType.BOOLEAN, 1);
        pnd_Inactive_Tiaa_Isn = localVariables.newFieldInRecord("pnd_Inactive_Tiaa_Isn", "#INACTIVE-TIAA-ISN", FieldType.PACKED_DECIMAL, 12);
        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.PACKED_DECIMAL, 3);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.PACKED_DECIMAL, 3);
        pnd_T = localVariables.newFieldInRecord("pnd_T", "#T", FieldType.BOOLEAN, 1);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.BOOLEAN, 1);
        pnd_More = localVariables.newFieldInRecord("pnd_More", "#MORE", FieldType.BOOLEAN, 1);
        pnd_Actve_T = localVariables.newFieldInRecord("pnd_Actve_T", "#ACTVE-T", FieldType.PACKED_DECIMAL, 3);
        pnd_Actve_C = localVariables.newFieldInRecord("pnd_Actve_C", "#ACTVE-C", FieldType.PACKED_DECIMAL, 3);
        pnd_Seasoned = localVariables.newFieldInRecord("pnd_Seasoned", "#SEASONED", FieldType.BOOLEAN, 1);
        pnd_Base_Dte = localVariables.newFieldInRecord("pnd_Base_Dte", "#BASE-DTE", FieldType.DATE);
        pnd_Age_Yy = localVariables.newFieldInRecord("pnd_Age_Yy", "#AGE-YY", FieldType.NUMERIC, 3);
        pnd_Age_Mm = localVariables.newFieldInRecord("pnd_Age_Mm", "#AGE-MM", FieldType.NUMERIC, 2);
        pnd_New_Isns = localVariables.newFieldArrayInRecord("pnd_New_Isns", "#NEW-ISNS", FieldType.PACKED_DECIMAL, 11, new DbsArrayController(1, 20));
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Max_Rte = localVariables.newFieldInRecord("pnd_Max_Rte", "#MAX-RTE", FieldType.NUMERIC, 3);
        pnd_Max_Adi = localVariables.newFieldInRecord("pnd_Max_Adi", "#MAX-ADI", FieldType.NUMERIC, 3);
        pnd_S_Rate_Cde = localVariables.newFieldInRecord("pnd_S_Rate_Cde", "#S-RATE-CDE", FieldType.STRING, 250);

        pnd_S_Rate_Cde__R_Field_19 = localVariables.newGroupInRecord("pnd_S_Rate_Cde__R_Field_19", "REDEFINE", pnd_S_Rate_Cde);
        pnd_S_Rate_Cde_Pnd_S_Rate = pnd_S_Rate_Cde__R_Field_19.newFieldArrayInGroup("pnd_S_Rate_Cde_Pnd_S_Rate", "#S-RATE", FieldType.STRING, 2, new DbsArrayController(1, 
            125));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_updt_Ia_Rslt.reset();
        vw_updt_Ia_Rsl2.reset();
        vw_ads_Ia_Rsl2.reset();
        vw_iaa_Old_Tiaa_Rates.reset();
        vw_iaa_Old_Cref_Rates.reset();
        vw_cref_Fund_Rcrd.reset();

        ldaAdsl601.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_First.setInitialValue(true);
        pnd_Max_Rte.setInitialValue(250);
        pnd_Max_Adi.setInitialValue(125);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn603() throws Exception
    {
        super("Adsn603");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  030812
        getReports().write(0, "INSIDE",Global.getPROGRAM());                                                                                                              //Natural: WRITE 'INSIDE' *PROGRAM
        if (Global.isEscape()) return;
        pnd_Trans_Dte.setValue(Global.getTIMX());                                                                                                                         //Natural: ASSIGN #TRANS-DTE := *TIMX
        pnd_Invrse_Trans_Dte.compute(new ComputeParameters(false, pnd_Invrse_Trans_Dte), new DbsDecimal("1000000000000").subtract(pnd_Trans_Dte));                        //Natural: ASSIGN #INVRSE-TRANS-DTE := 1000000000000 - #TRANS-DTE
        G0:                                                                                                                                                               //Natural: GET ADS-IA-RSLT #RSLT-ISN ( 1 )
        ldaAdsl601.getVw_ads_Ia_Rslt().readByID(pnd_Rslt_Isn.getValue(1).getLong(), "G0");
        vw_updt_Ia_Rslt.setValuesByName(ldaAdsl601.getVw_ads_Ia_Rslt());                                                                                                  //Natural: MOVE BY NAME ADS-IA-RSLT TO UPDT-IA-RSLT
        //*  030812 START
        if (condition(pnd_Rslt_Isn.getValue(2).greater(getZero())))                                                                                                       //Natural: IF #RSLT-ISN ( 2 ) GT 0
        {
            pnd_More.setValue(true);                                                                                                                                      //Natural: ASSIGN #MORE := TRUE
            GET01:                                                                                                                                                        //Natural: GET ADS-IA-RSL2 #RSLT-ISN ( 2 )
            vw_ads_Ia_Rsl2.readByID(pnd_Rslt_Isn.getValue(2).getLong(), "GET01");
            vw_updt_Ia_Rsl2.setValuesByName(vw_ads_Ia_Rsl2);                                                                                                              //Natural: MOVE BY NAME ADS-IA-RSL2 TO UPDT-IA-RSL2
            //*  030812 END
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl601.getVw_iaa_Cntrl_Rcrd_1().startDatabaseRead                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "R0",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        R0:
        while (condition(ldaAdsl601.getVw_iaa_Cntrl_Rcrd_1().readNextRow("R0")))
        {
            pnd_Trans_Check_Dte.setValueEdited(ldaAdsl601.getIaa_Cntrl_Rcrd_1_Cntrl_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                          //Natural: MOVE EDITED IAA-CNTRL-RCRD-1.CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #TRANS-CHECK-DTE
            pnd_Trans_Prev_Dte.setValue(pnd_Trans_Check_Dte);                                                                                                             //Natural: MOVE #TRANS-CHECK-DTE TO #TRANS-PREV-DTE
            if (condition(pnd_Trans_Prev_Dte_Pnd_Trans_Prev_Mm.equals(1)))                                                                                                //Natural: IF #TRANS-PREV-MM = 01
            {
                pnd_Trans_Prev_Dte_Pnd_Trans_Prev_Mm.setValue(12);                                                                                                        //Natural: ASSIGN #TRANS-PREV-MM := 12
                pnd_Trans_Prev_Dte_Pnd_Trans_Prev_Yyyy.nsubtract(1);                                                                                                      //Natural: SUBTRACT 1 FROM #TRANS-PREV-YYYY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Trans_Prev_Dte_Pnd_Trans_Prev_Mm.nsubtract(1);                                                                                                        //Natural: SUBTRACT 1 FROM #TRANS-PREV-MM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        ldaAdsl601.getVw_iaa_Cntrl_Rcrd_1().startDatabaseRead                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM 'DC'
        (
        "R1",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "DC", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        R1:
        while (condition(ldaAdsl601.getVw_iaa_Cntrl_Rcrd_1().readNextRow("R1")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  030812 START
        updt_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units.getValue("*").reset();                                                                                                 //Natural: RESET UPDT-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( * ) UPDT-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( * ) UPDT-IA-RSLT.ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL ( * ) UPDT-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( * ) UPDT-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( * ) UPDT-IA-RSLT.ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( * ) UPDT-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( * ) UPDT-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( * ) UPDT-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( * ) UPDT-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( * ) UPDT-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * ) UPDT-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( * ) UPDT-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( * ) UPDT-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( * ) UPDT-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( * ) UPDT-IA-RSLT.ADI-DTL-CREF-DA-RATE-CD ( * ) UPDT-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( * ) UPDT-IA-RSLT.ADI-DTL-TIAA-ACCT-CD ( * ) UPDT-IA-RSLT.ADI-DTL-CREF-IA-RATE-CD ( * ) UPDT-IA-RSLT.ADI-DTL-TIAA-DA-RATE-CD ( * ) UPDT-IA-RSLT.ADI-DTL-TIAA-DA-STD-AMT ( * ) UPDT-IA-RSLT.ADI-DTL-TIAA-DA-GRD-AMT ( * ) UPDT-IA-RSLT.ADI-CONTRACT-ID ( * )
        updt_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt.getValue("*").reset();
        updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val.getValue("*").reset();
        updt_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units.getValue("*").reset();
        updt_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt.getValue("*").reset();
        updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val.getValue("*").reset();
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue("*").reset();
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue("*").reset();
        updt_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt.getValue("*").reset();
        updt_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue("*").reset();
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue("*").reset();
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue("*").reset();
        updt_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue("*").reset();
        updt_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue("*").reset();
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue("*").reset();
        updt_Ia_Rslt_Adi_Dtl_Cref_Da_Rate_Cd.getValue("*").reset();
        updt_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd.getValue("*").reset();
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Acct_Cd.getValue("*").reset();
        updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd.getValue("*").reset();
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Da_Rate_Cd.getValue("*").reset();
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Da_Std_Amt.getValue("*").reset();
        updt_Ia_Rslt_Adi_Dtl_Tiaa_Da_Grd_Amt.getValue("*").reset();
        updt_Ia_Rslt_Adi_Contract_Id.getValue("*").reset();
        if (condition(pnd_More.getBoolean()))                                                                                                                             //Natural: IF #MORE
        {
            updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue("*").reset();                                                                                                //Natural: RESET UPDT-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( * ) UPDT-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( * ) UPDT-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( * ) UPDT-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( * ) UPDT-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * ) UPDT-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( * ) UPDT-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( * ) UPDT-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( * ) UPDT-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( * ) UPDT-IA-RSL2.ADI-DTL-TIAA-ACCT-CD ( * ) UPDT-IA-RSL2.ADI-DTL-TIAA-DA-RATE-CD ( * ) UPDT-IA-RSL2.ADI-DTL-TIAA-DA-STD-AMT ( * ) UPDT-IA-RSL2.ADI-DTL-TIAA-DA-GRD-AMT ( * ) UPDT-IA-RSL2.ADI-CONTRACT-ID ( * )
            updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue("*").reset();
            updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue("*").reset();
            updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue("*").reset();
            updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue("*").reset();
            updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue("*").reset();
            updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue("*").reset();
            updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue("*").reset();
            updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue("*").reset();
            updt_Ia_Rsl2_Adi_Dtl_Tiaa_Acct_Cd.getValue("*").reset();
            updt_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Rate_Cd.getValue("*").reset();
            updt_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt.getValue("*").reset();
            updt_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt.getValue("*").reset();
            updt_Ia_Rsl2_Adi_Contract_Id.getValue("*").reset();
            //*  030812 END
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Tiaa_Sttlmnt().equals("Y") || ldaAdsl601.getAds_Ia_Rslt_Adi_Tiaa_Re_Sttlmnt().equals("Y")))                           //Natural: IF ADS-IA-RSLT.ADI-TIAA-STTLMNT = 'Y' OR ADS-IA-RSLT.ADI-TIAA-RE-STTLMNT = 'Y'
        {
            pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Ia_Tiaa_Nbr());                                                          //Natural: ASSIGN #CNTRCT-PART-PPCN-NBR := ADS-IA-RSLT.ADI-IA-TIAA-NBR
            pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(1);                                                                                                   //Natural: ASSIGN #CNTRCT-PART-PAYEE-CDE := 01
            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Tiaa_Sttlmnt().equals("Y")))                                                                                      //Natural: IF ADS-IA-RSLT.ADI-TIAA-STTLMNT = 'Y'
            {
                pnd_Tiaa.setValue(true);                                                                                                                                  //Natural: ASSIGN #TIAA := TRUE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Tiaa_Re_Sttlmnt().equals("Y")))                                                                                   //Natural: IF ADS-IA-RSLT.ADI-TIAA-RE-STTLMNT = 'Y'
            {
                pnd_Rea.setValue(true);                                                                                                                                   //Natural: ASSIGN #REA := TRUE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-IA-INFO
            sub_Get_Ia_Info();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Err_Cde.notEquals(" ")))                                                                                                                    //Natural: IF #ERR-CDE NE ' '
            {
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tiaa.reset();                                                                                                                                             //Natural: RESET #TIAA #REA
            pnd_Rea.reset();
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Cref_Sttlmnt().equals("Y")))                                                                                          //Natural: IF ADS-IA-RSLT.ADI-CREF-STTLMNT = 'Y'
        {
            pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Ia_Cref_Nbr());                                                          //Natural: ASSIGN #CNTRCT-PART-PPCN-NBR := ADS-IA-RSLT.ADI-IA-CREF-NBR
            pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(1);                                                                                                   //Natural: ASSIGN #CNTRCT-PART-PAYEE-CDE := 01
            pnd_Cref.setValue(true);                                                                                                                                      //Natural: ASSIGN #CREF := TRUE
                                                                                                                                                                          //Natural: PERFORM GET-IA-INFO
            sub_Get_Ia_Info();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Err_Cde.notEquals(" ")))                                                                                                                    //Natural: IF #ERR-CDE NE ' '
            {
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cref.reset();                                                                                                                                             //Natural: RESET #CREF
        }                                                                                                                                                                 //Natural: END-IF
        updt_Ia_Rslt_Adi_Ia_Rcrd_Cde.setValue("RS");                                                                                                                      //Natural: ASSIGN UPDT-IA-RSLT.ADI-IA-RCRD-CDE := 'RS'
        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Sqnce_Nbr().equals(12)))                                                                                              //Natural: IF ADS-IA-RSLT.ADI-SQNCE-NBR = 12
        {
            updt_Ia_Rslt_Adi_Sqnce_Nbr.setValue(11);                                                                                                                      //Natural: ASSIGN UPDT-IA-RSLT.ADI-SQNCE-NBR := 11
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            updt_Ia_Rslt_Adi_Sqnce_Nbr.setValue(21);                                                                                                                      //Natural: ASSIGN UPDT-IA-RSLT.ADI-SQNCE-NBR := 21
        }                                                                                                                                                                 //Natural: END-IF
        vw_updt_Ia_Rslt.insertDBRow();                                                                                                                                    //Natural: STORE UPDT-IA-RSLT
        //*  030812 START
        if (condition(pnd_More.getBoolean()))                                                                                                                             //Natural: IF #MORE
        {
            updt_Ia_Rsl2_Adi_Ia_Rcrd_Cde.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, updt_Ia_Rslt_Adi_Ia_Rcrd_Cde, "1"));                                    //Natural: COMPRESS UPDT-IA-RSLT.ADI-IA-RCRD-CDE '1' INTO UPDT-IA-RSL2.ADI-IA-RCRD-CDE LEAVING NO
            updt_Ia_Rsl2_Adi_Sqnce_Nbr.setValue(updt_Ia_Rslt_Adi_Sqnce_Nbr);                                                                                              //Natural: ASSIGN UPDT-IA-RSL2.ADI-SQNCE-NBR := UPDT-IA-RSLT.ADI-SQNCE-NBR
            vw_updt_Ia_Rsl2.insertDBRow();                                                                                                                                //Natural: STORE UPDT-IA-RSL2
            //*  030812 END
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Updt_Std.reset();                                                                                                                                             //Natural: RESET #UPDT-STD #UPDT-GRD
        pnd_Updt_Grd.reset();
        short decideConditionsMet1218 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( * ) GT 0
        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue("*").greater(getZero())))
        {
            decideConditionsMet1218++;
            if (condition(updt_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue("*").greater(getZero())))                                                                      //Natural: IF UPDT-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( * ) GT 0
            {
                pnd_Updt_Std.setValue(true);                                                                                                                              //Natural: ASSIGN #UPDT-STD := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Create_Std.setValue(true);                                                                                                                            //Natural: ASSIGN #CREATE-STD := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * ) GT 0
        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue("*").greater(getZero())))
        {
            decideConditionsMet1218++;
            if (condition(updt_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue("*").greater(getZero())))                                                                      //Natural: IF UPDT-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * ) GT 0
            {
                pnd_Updt_Grd.setValue(true);                                                                                                                              //Natural: ASSIGN #UPDT-GRD := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Create_Grd.setValue(true);                                                                                                                            //Natural: ASSIGN #CREATE-GRD := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet1218 == 0))
        {
            if (condition(pnd_Inactive_Tiaa_Isn.greater(getZero())))                                                                                                      //Natural: IF #INACTIVE-TIAA-ISN GT 0
            {
                pnd_Updt_Std.setValue(true);                                                                                                                              //Natural: ASSIGN #UPDT-STD := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Create_Std.getBoolean() || pnd_Create_Grd.getBoolean()))                                                                                        //Natural: IF #CREATE-STD OR #CREATE-GRD
        {
                                                                                                                                                                          //Natural: PERFORM CREATE-TIAA-FUND-RECORD
            sub_Create_Tiaa_Fund_Record();
            if (condition(Global.isEscape())) {return;}
            //*  RESET #CREATE-STD #CREATE-GRD
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Updt_Std.getBoolean() || pnd_Updt_Grd.getBoolean() || pnd_Create_Std.getBoolean() || pnd_Create_Grd.getBoolean()))                              //Natural: IF #UPDT-STD OR #UPDT-GRD OR #CREATE-STD OR #CREATE-GRD
        {
            //*  030812 START
            DbsUtil.examine(new ExamineSource(updt_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue("*"),true), new ExamineSearch("  ", true), new ExamineGivingNumber(pnd_I));   //Natural: EXAMINE FULL UPDT-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( * ) FOR FULL '  ' GIVING NUMBER #I
            //* *#T-CNT := 80 - #I
            pnd_T_Cnt.compute(new ComputeParameters(false, pnd_T_Cnt), pnd_Max_Adi.subtract(pnd_I));                                                                      //Natural: ASSIGN #T-CNT := #MAX-ADI - #I
            //*  030812 END
            pnd_S_Rate_Cde.reset();                                                                                                                                       //Natural: RESET #S-RATE-CDE
            //*  FOR #I 1 TO 80
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 TO #MAX-ADI
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Adi)); pnd_I.nadd(1))
            {
                if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_I).equals(" ")))                                                           //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #I ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_I).equals(updt_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue("*"))))            //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #I ) = UPDT-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( * )
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  030812 START
                    if (condition(pnd_T_Cnt.equals(pnd_Max_Adi)))                                                                                                         //Natural: IF #T-CNT = #MAX-ADI
                    {
                        //*  SAVE THE RATE FOR FURTHER PROCESING
                        pnd_S_Rate_Cde.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_S_Rate_Cde, ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_I))); //Natural: COMPRESS #S-RATE-CDE ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #I ) INTO #S-RATE-CDE LEAVING NO
                        //*  THE RATE CAN NO LONGER BE ADDED
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                        //*  030812 END
                    }                                                                                                                                                     //Natural: END-IF
                    //*  030812
                    pnd_T_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #T-CNT
                    updt_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_T_Cnt).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_I));               //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #T-CNT ) := ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #I )
                    updt_Ia_Rslt_Adi_Contract_Id.getValue(pnd_T_Cnt).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Contract_Id().getValue(pnd_I));                               //Natural: ASSIGN UPDT-IA-RSLT.ADI-CONTRACT-ID ( #T-CNT ) := ADS-IA-RSLT.ADI-CONTRACT-ID ( #I )
                    if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_I).greater(getZero())))                                             //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #I ) GT 0
                    {
                        updt_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_T_Cnt).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_I));         //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #I ) TO UPDT-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #T-CNT )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_I).greater(getZero())))                                             //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #I ) GT 0
                    {
                        updt_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_T_Cnt).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_I));         //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #I ) TO UPDT-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #T-CNT )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_I).greater(getZero())))                                                //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( #I ) GT 0
                    {
                        updt_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_T_Cnt).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_I));               //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( #I ) TO UPDT-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( #T-CNT )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_I).greater(getZero())))                                              //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( #I ) GT 0
                    {
                        updt_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_T_Cnt).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_I));           //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( #I ) TO UPDT-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( #T-CNT )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_I).greater(getZero())))                                             //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #I ) GT 0
                    {
                        updt_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_T_Cnt).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_I));         //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #I ) TO UPDT-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #T-CNT )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_I).greater(getZero())))                                             //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #I ) GT 0
                    {
                        updt_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_T_Cnt).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_I));         //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #I ) TO UPDT-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #T-CNT )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_I).greater(getZero())))                                                //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( #I ) GT 0
                    {
                        updt_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_T_Cnt).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_I));               //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( #I ) TO UPDT-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( #T-CNT )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_I).greater(getZero())))                                              //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( #I ) GT 0
                    {
                        updt_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_T_Cnt).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_I));           //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( #I ) TO UPDT-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( #T-CNT )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                    //*  030812
                }                                                                                                                                                         //Natural: END-IF
                //*    FOR #J 1 TO 80
                FOR02:                                                                                                                                                    //Natural: FOR #J 1 TO #MAX-ADI
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Max_Adi)); pnd_J.nadd(1))
                {
                    if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_I).equals(updt_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_J))))      //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #I ) = UPDT-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #J )
                    {
                        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_I).greater(getZero())))                                         //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #I ) GT 0
                        {
                            updt_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_J).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_I));         //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #I ) TO UPDT-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_I).greater(getZero())))                                         //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #I ) GT 0
                        {
                            updt_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_J).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_I));         //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #I ) TO UPDT-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #J )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_I).greater(getZero())))                                            //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( #I ) GT 0
                        {
                            updt_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_J).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_I));               //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( #I ) TO UPDT-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( #J )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_I).greater(getZero())))                                          //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( #I ) GT 0
                        {
                            updt_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_J).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_I));           //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( #I ) TO UPDT-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( #J )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_I).greater(getZero())))                                         //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #I ) GT 0
                        {
                            updt_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_J).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_I));         //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #I ) TO UPDT-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_I).greater(getZero())))                                         //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #I ) GT 0
                        {
                            updt_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_J).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_I));         //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #I ) TO UPDT-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_I).greater(getZero())))                                            //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( #I ) GT 0
                        {
                            updt_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_J).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_I));               //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( #I ) TO UPDT-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( #J )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_I).greater(getZero())))                                          //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( #I ) GT 0
                        {
                            updt_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_J).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_I));           //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( #I ) TO UPDT-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( #J )
                        }                                                                                                                                                 //Natural: END-IF
                        //*  030812
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //*  030812 START
            if (condition(pnd_More.getBoolean()))                                                                                                                         //Natural: IF #MORE
            {
                DbsUtil.examine(new ExamineSource(updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue("*"),true), new ExamineSearch("  ", true), new ExamineGivingNumber(pnd_I)); //Natural: EXAMINE FULL UPDT-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( * ) FOR FULL '  ' GIVING NUMBER #I
                pnd_T_Cnt.compute(new ComputeParameters(false, pnd_T_Cnt), pnd_Max_Adi.subtract(pnd_I));                                                                  //Natural: ASSIGN #T-CNT := #MAX-ADI - #I
                //*  CHECK FIRST IF THERE ARE RATES FROM THE FIRST RECORD HAVE NOT BEEN
                //*  ACCOUNTED FOR
                if (condition(pnd_S_Rate_Cde.notEquals(" ")))                                                                                                             //Natural: IF #S-RATE-CDE NE ' '
                {
                    FOR03:                                                                                                                                                //Natural: FOR #I 1 #MAX-ADI
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Adi)); pnd_I.nadd(1))
                    {
                        if (condition(pnd_S_Rate_Cde_Pnd_S_Rate.getValue(pnd_I).equals(" ")))                                                                             //Natural: IF #S-RATE ( #I ) = ' '
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_S_Rate_Cde_Pnd_S_Rate.getValue(pnd_I).equals(updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue("*"))))                              //Natural: IF #S-RATE ( #I ) = UPDT-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( * )
                        {
                            DbsUtil.examine(new ExamineSource(updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue("*")), new ExamineSearch(pnd_S_Rate_Cde_Pnd_S_Rate.getValue(pnd_I)),  //Natural: EXAMINE UPDT-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( * ) FOR #S-RATE ( #I ) GIVING INDEX #A
                                new ExamineGivingIndex(pnd_A));
                            DbsUtil.examine(new ExamineSource(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue("*")), new ExamineSearch(pnd_S_Rate_Cde_Pnd_S_Rate.getValue(pnd_I)),  //Natural: EXAMINE ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( * ) FOR #S-RATE ( #I ) GIVING INDEX #B
                                new ExamineGivingIndex(pnd_B));
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_B).greater(getZero())))                                     //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #B ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_A).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_B));     //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #B ) TO UPDT-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #A )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_B).greater(getZero())))                                     //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #B ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_A).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_B));     //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #B ) TO UPDT-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #A )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_B).greater(getZero())))                                        //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( #B ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_A).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_B));           //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( #B ) TO UPDT-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( #A )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_B).greater(getZero())))                                      //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( #B ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_A).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_B));       //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( #B ) TO UPDT-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( #A )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_B).greater(getZero())))                                     //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_A).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_B));     //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B ) TO UPDT-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #A )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_B).greater(getZero())))                                     //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #B ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_A).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_B));     //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #B ) TO UPDT-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #A )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_B).greater(getZero())))                                        //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( #B ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_A).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_B));           //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( #B ) TO UPDT-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( #A )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_B).greater(getZero())))                                      //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( #B ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_A).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_B));       //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( #B ) TO UPDT-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( #A )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            DbsUtil.examine(new ExamineSource(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue("*")), new ExamineSearch(pnd_S_Rate_Cde_Pnd_S_Rate.getValue(pnd_I)),  //Natural: EXAMINE ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( * ) FOR #S-RATE ( #I ) GIVING INDEX #B
                                new ExamineGivingIndex(pnd_B));
                            pnd_T_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #T-CNT
                            updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_T_Cnt).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_B));       //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #T-CNT ) := ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #B )
                            updt_Ia_Rsl2_Adi_Contract_Id.getValue(pnd_T_Cnt).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Contract_Id().getValue(pnd_B));                       //Natural: ASSIGN UPDT-IA-RSL2.ADI-CONTRACT-ID ( #T-CNT ) := ADS-IA-RSLT.ADI-CONTRACT-ID ( #B )
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_B).greater(getZero())))                                     //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #B ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_T_Cnt).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_B)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #B ) TO UPDT-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #T-CNT )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_B).greater(getZero())))                                     //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #B ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_T_Cnt).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_B)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #B ) TO UPDT-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #T-CNT )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_B).greater(getZero())))                                        //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( #B ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_T_Cnt).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_B));       //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( #B ) TO UPDT-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( #T-CNT )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_B).greater(getZero())))                                      //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( #B ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_T_Cnt).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_B));   //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( #B ) TO UPDT-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( #T-CNT )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_B).greater(getZero())))                                     //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_T_Cnt).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_B)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B ) TO UPDT-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #T-CNT )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_B).greater(getZero())))                                     //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #B ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_T_Cnt).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_B)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #B ) TO UPDT-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #T-CNT )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_B).greater(getZero())))                                        //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( #B ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_T_Cnt).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_B));       //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( #B ) TO UPDT-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( #T-CNT )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_B).greater(getZero())))                                      //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( #B ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_T_Cnt).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_B));   //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( #B ) TO UPDT-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( #T-CNT )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                FOR04:                                                                                                                                                    //Natural: FOR #I 1 TO #MAX-ADI
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Adi)); pnd_I.nadd(1))
                {
                    if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_I).equals(" ")))                                                                       //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #I ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_I).equals(updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue("*"))))                        //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #I ) = UPDT-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( * )
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_T_Cnt.equals(pnd_Max_Adi)))                                                                                                     //Natural: IF #T-CNT = #MAX-ADI
                        {
                            pnd_Err_Cde.setValue("L76");                                                                                                                  //Natural: ASSIGN #ERR-CDE := 'L76'
                            if (condition(true)) return;                                                                                                                  //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                        //*  030812
                        pnd_T_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #T-CNT
                        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_T_Cnt).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_I));                           //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #T-CNT ) := ADS-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #I )
                        updt_Ia_Rsl2_Adi_Contract_Id.getValue(pnd_T_Cnt).setValue(ads_Ia_Rsl2_Adi_Contract_Id.getValue(pnd_I));                                           //Natural: ASSIGN UPDT-IA-RSL2.ADI-CONTRACT-ID ( #T-CNT ) := ADS-IA-RSL2.ADI-CONTRACT-ID ( #I )
                        if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_I).greater(getZero())))                                                         //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #I ) GT 0
                        {
                            updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_T_Cnt).nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_I));                     //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #I ) TO UPDT-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #T-CNT )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_I).greater(getZero())))                                                         //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #I ) GT 0
                        {
                            updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_T_Cnt).nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_I));                     //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #I ) TO UPDT-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #T-CNT )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_I).greater(getZero())))                                                            //Natural: IF ADS-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( #I ) GT 0
                        {
                            updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_T_Cnt).nadd(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_I));                           //Natural: ADD ADS-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( #I ) TO UPDT-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( #T-CNT )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_I).greater(getZero())))                                                          //Natural: IF ADS-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( #I ) GT 0
                        {
                            updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_T_Cnt).nadd(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_I));                       //Natural: ADD ADS-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( #I ) TO UPDT-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( #T-CNT )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_I).greater(getZero())))                                                         //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #I ) GT 0
                        {
                            updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_T_Cnt).nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_I));                     //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #I ) TO UPDT-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #T-CNT )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_I).greater(getZero())))                                                         //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #I ) GT 0
                        {
                            updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_T_Cnt).nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_I));                     //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #I ) TO UPDT-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #T-CNT )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_I).greater(getZero())))                                                            //Natural: IF ADS-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( #I ) GT 0
                        {
                            updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_T_Cnt).nadd(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_I));                           //Natural: ADD ADS-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( #I ) TO UPDT-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( #T-CNT )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_I).greater(getZero())))                                                          //Natural: IF ADS-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( #I ) GT 0
                        {
                            updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_T_Cnt).nadd(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_I));                       //Natural: ADD ADS-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( #I ) TO UPDT-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( #T-CNT )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    FOR05:                                                                                                                                                //Natural: FOR #J 1 TO #MAX-ADI
                    for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Max_Adi)); pnd_J.nadd(1))
                    {
                        if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_I).equals(updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_J))))                  //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #I ) = UPDT-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #J )
                        {
                            if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_I).greater(getZero())))                                                     //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #I ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_J).nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_I));                     //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #I ) TO UPDT-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_I).greater(getZero())))                                                     //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #I ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_J).nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_I));                     //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #I ) TO UPDT-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #J )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_I).greater(getZero())))                                                        //Natural: IF ADS-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( #I ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_J).nadd(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_I));                           //Natural: ADD ADS-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( #I ) TO UPDT-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( #J )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_I).greater(getZero())))                                                      //Natural: IF ADS-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( #I ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_J).nadd(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_I));                       //Natural: ADD ADS-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( #I ) TO UPDT-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( #J )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_I).greater(getZero())))                                                     //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #I ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_J).nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_I));                     //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #I ) TO UPDT-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_I).greater(getZero())))                                                     //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #I ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_J).nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_I));                     //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #I ) TO UPDT-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_I).greater(getZero())))                                                        //Natural: IF ADS-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( #I ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_J).nadd(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_I));                           //Natural: ADD ADS-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( #I ) TO UPDT-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( #J )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_I).greater(getZero())))                                                      //Natural: IF ADS-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( #I ) GT 0
                            {
                                updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_J).nadd(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_I));                       //Natural: ADD ADS-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( #I ) TO UPDT-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( #J )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*  030812 END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR06:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_I).equals(" ")))                                                                  //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_I).equals(updt_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd.getValue("*"))))                      //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #I ) = UPDT-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( * )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM CREATE-CREF-FUND-RECORD
                sub_Create_Cref_Fund_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_I1.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #I1
                updt_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd.getValue(pnd_I1).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_I));                            //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #I1 ) := ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #I )
                updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd.getValue(pnd_I1).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd().getValue(pnd_I));                      //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-CREF-IA-RATE-CD ( #I1 ) := ADS-IA-RSLT.ADI-DTL-CREF-IA-RATE-CD ( #I )
                updt_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units.getValue(pnd_I1).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_I));          //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #I1 ) := ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #I )
                updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val.getValue(pnd_I1).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val().getValue(pnd_I));      //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL ( #I1 ) := ADS-IA-RSLT.ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL ( #I )
                updt_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt.getValue(pnd_I1).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_I));                //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #I1 ) := ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #I )
                updt_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units.getValue(pnd_I1).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_I));              //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #I1 ) := ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #I )
                updt_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt.getValue(pnd_I1).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_I));                    //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #I1 ) := ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #I )
                updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val.getValue(pnd_I1).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val().getValue(pnd_I));          //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #I1 ) := ADS-IA-RSLT.ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #I )
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            FOR07:                                                                                                                                                        //Natural: FOR #J 1 TO 20
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(20)); pnd_J.nadd(1))
            {
                if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_I).equals(updt_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd.getValue(pnd_J))))                //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #I ) = UPDT-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #J )
                {
                    if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_I).greater(getZero())))                                          //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #I ) GT 0
                    {
                        updt_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units.getValue(pnd_J).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_I));       //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #I ) TO UPDT-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #J )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_I).greater(getZero())))                                             //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #I ) GT 0
                    {
                        updt_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt.getValue(pnd_J).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_I));             //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #I ) TO UPDT-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #J )
                    }                                                                                                                                                     //Natural: END-IF
                    updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val.getValue(pnd_J).setValue(0);                                                                             //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL ( #J ) := 0
                    if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_I).greater(getZero())))                                            //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #I ) GT 0
                    {
                        updt_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units.getValue(pnd_J).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_I));           //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #I ) TO UPDT-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #J )
                        updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val.getValue(pnd_J).setValue(0);                                                                           //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #J ) := 0
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_I).greater(getZero())))                                               //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #I ) GT 0
                    {
                        updt_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt.getValue(pnd_J).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_I));                 //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #I ) TO UPDT-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #J )
                        updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val.getValue(pnd_J).setValue(0);                                                                           //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #J ) := 0
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CREATE AFTER IMAGE
        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Sqnce_Nbr().equals(12)))                                                                                              //Natural: IF ADS-IA-RSLT.ADI-SQNCE-NBR = 12
        {
            updt_Ia_Rslt_Adi_Sqnce_Nbr.setValue(13);                                                                                                                      //Natural: ASSIGN UPDT-IA-RSLT.ADI-SQNCE-NBR := 13
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            updt_Ia_Rslt_Adi_Sqnce_Nbr.setValue(23);                                                                                                                      //Natural: ASSIGN UPDT-IA-RSLT.ADI-SQNCE-NBR := 23
        }                                                                                                                                                                 //Natural: END-IF
        vw_updt_Ia_Rslt.insertDBRow();                                                                                                                                    //Natural: STORE UPDT-IA-RSLT
        //*  030812 START
        if (condition(pnd_More.getBoolean()))                                                                                                                             //Natural: IF #MORE
        {
            updt_Ia_Rsl2_Adi_Sqnce_Nbr.setValue(updt_Ia_Rslt_Adi_Sqnce_Nbr);                                                                                              //Natural: ASSIGN UPDT-IA-RSL2.ADI-SQNCE-NBR := UPDT-IA-RSLT.ADI-SQNCE-NBR
            vw_updt_Ia_Rsl2.insertDBRow();                                                                                                                                //Natural: STORE UPDT-IA-RSL2
            //*  030812 END
        }                                                                                                                                                                 //Natural: END-IF
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-IA-INFO
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FUND-DATA
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-TRANS-RECORD
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-IVC-INFO
        //* *********************************************************************
        //* * EM - 012309 END
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-TIAA-FUND-RECORD
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-BEFORE-CNTRCT-RECORD
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-AFTER-CNTRCT-RECORD
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-BEFORE-CPR-RECORD
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-AFTER-CPR-RECORD
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CREF-FUND-RECORD
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-BFORE-TIAA-FUND
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-AFTER-TIAA-FUND
        //* ********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-BFORE-CREF-FUND
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-AFTER-CREF-FUND
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-HISTORICAL-RATES
        //* *********************************************************************
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-MONTHLY-CREF-FUND-RCRD
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-ANNUAL-CREF-FUND-RCRD
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-CREF-IF-NONE-EXISTS
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-TIAA-FUND
        //* *********************************************************************
        //*        FOR #I 1 TO 80
        //*            FOR #J 1 TO 80
        //*        FOR #J 1 TO 80
        //*        FOR #I 1 TO 80
        //*            FOR #J 1 TO 80
        //*        FOR #J 1 TO 80
        //*      UPDT-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD(*) :=
        //*        IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE(*)
        //* *********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CREF-FUND
        //* *********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-RATE-DATE
    }
    private void sub_Get_Ia_Info() throws Exception                                                                                                                       //Natural: GET-IA-INFO
    {
        if (BLNatReinput.isReinput()) return;

        ldaAdsl601.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                   //Natural: FIND ( 1 ) IAA-CNTRCT WITH CNTRCT-PPCN-NBR = #CNTRCT-PART-PPCN-NBR
        (
        "F0",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr, WcType.WITH) },
        1
        );
        F0:
        while (condition(ldaAdsl601.getVw_iaa_Cntrct().readNextRow("F0", true)))
        {
            ldaAdsl601.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaAdsl601.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                       //Natural: IF NO RECORDS FOUND
            {
                pnd_Err_Cde.setValue("L24");                                                                                                                              //Natural: ASSIGN #ERR-CDE := 'L24'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
                                                                                                                                                                          //Natural: PERFORM CREATE-BEFORE-CNTRCT-RECORD
            sub_Create_Before_Cntrct_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F0"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F0"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaAdsl601.getIaa_Cntrct_Lst_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                             //Natural: ASSIGN IAA-CNTRCT.LST-TRANS-DTE := #TRANS-DTE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        ldaAdsl601.getVw_iaa_Cntrct_Prtcpnt_Role().startDatabaseFind                                                                                                      //Natural: FIND ( 1 ) IAA-CNTRCT-PRTCPNT-ROLE WITH CNTRCT-PAYEE-KEY = #CNTRCT-PAYEE-KEY
        (
        "F1",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cntrct_Payee_Key, WcType.WITH) },
        1
        );
        F1:
        while (condition(ldaAdsl601.getVw_iaa_Cntrct_Prtcpnt_Role().readNextRow("F1", true)))
        {
            ldaAdsl601.getVw_iaa_Cntrct_Prtcpnt_Role().setIfNotFoundControlFlag(false);
            if (condition(ldaAdsl601.getVw_iaa_Cntrct_Prtcpnt_Role().getAstCOUNTER().equals(0)))                                                                          //Natural: IF NO RECORDS FOUND
            {
                pnd_Err_Cde.setValue("L24");                                                                                                                              //Natural: ASSIGN #ERR-CDE := 'L24'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind().notEquals(ldaAdsl601.getAds_Ia_Rslt_Adi_Pymnt_Mode())))                                 //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND NE ADS-IA-RSLT.ADI-PYMNT-MODE
            {
                pnd_Err_Cde.setValue("L25");                                                                                                                              //Natural: ASSIGN #ERR-CDE := 'L25'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().equals(9) && ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn().notEquals("NI")  //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE = 9 AND IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-TRMNTE-RSN NE 'NI' AND IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-TRMNTE-RSN NE 'TR'
                && ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn().notEquals("TR")))
            {
                pnd_Err_Cde.setValue("L26");                                                                                                                              //Natural: ASSIGN #ERR-CDE := 'L26'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.callnat(Nazn6031.class , getCurrentProcessState(), pnd_Datd, ldaAdsl601.getIaa_Cntrct_Cntrct_Issue_Dte(), pnd_Trans_Check_Dte, ldaAdsl601.getIaa_Cntrct_Cntrct_First_Pymnt_Due_Dte(),  //Natural: CALLNAT 'NAZN6031' #DATD IAA-CNTRCT.CNTRCT-ISSUE-DTE #TRANS-CHECK-DTE IAA-CNTRCT.CNTRCT-FIRST-PYMNT-DUE-DTE IAA-CNTRCT.CNTRCT-FP-DUE-DTE-DD IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND ADS-IA-RSLT.ADI-OPTN-CDE
                ldaAdsl601.getIaa_Cntrct_Cntrct_Fp_Due_Dte_Dd(), ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind(), ldaAdsl601.getAds_Ia_Rslt_Adi_Optn_Cde());
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            DbsUtil.callnat(Nazn6034.class , getCurrentProcessState(), pnd_Next_Pymnt_Dte, pnd_Trans_Check_Dte, ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind()); //Natural: CALLNAT 'NAZN6034' #NEXT-PYMNT-DTE #TRANS-CHECK-DTE IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            pnd_Wdte8.setValueEdited(ldaAdsl601.getIaa_Cntrct_Cntrct_Issue_Dte(),new ReportEditMask("999999"));                                                           //Natural: MOVE EDITED IAA-CNTRCT.CNTRCT-ISSUE-DTE ( EM = 999999 ) TO #WDTE8
            pnd_Wdte8_Pnd_Wdte_Dd.setValue(ldaAdsl601.getIaa_Cntrct_Cntrct_Issue_Dte_Dd());                                                                               //Natural: ASSIGN #WDTE-DD := IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD
            pnd_Settl_Dte_Grd.setValue(pnd_Wdte8);                                                                                                                        //Natural: ASSIGN #SETTL-DTE-GRD := #WDTE8
            pnd_Settl_Dte_Grd_Pnd_Settl_Dd.setValue(1);                                                                                                                   //Natural: ASSIGN #SETTL-DD := 01
            pnd_Settl_Dte_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Settl_Dte_Grd);                                                                             //Natural: MOVE EDITED #SETTL-DTE-GRD TO #SETTL-DTE-D ( EM = YYYYMMDD )
            pnd_Settl_Dte_D.nsubtract(1);                                                                                                                                 //Natural: SUBTRACT 1 FROM #SETTL-DTE-D
            pnd_Settl_Dte_Grd.setValueEdited(pnd_Settl_Dte_D,new ReportEditMask("YYYYMMDD"));                                                                             //Natural: MOVE EDITED #SETTL-DTE-D ( EM = YYYYMMDD ) TO #SETTL-DTE-GRD
            pnd_Fp_Dte.reset();                                                                                                                                           //Natural: RESET #FP-DTE
            if (condition(ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte().greater(getZero())))                                                           //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE GT 0
            {
                pnd_Wdte8.setValueEdited(ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Final_Per_Pay_Dte(),new ReportEditMask("999999"));                                  //Natural: MOVE EDITED IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-FINAL-PER-PAY-DTE ( EM = 999999 ) TO #WDTE8
                pnd_Wdte8_Pnd_Wdte_Dd.setValue(1);                                                                                                                        //Natural: ASSIGN #WDTE-DD := 01
                pnd_Fp_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Wdte8);                                                                                      //Natural: MOVE EDITED #WDTE8 TO #FP-DTE ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-BEFORE-CPR-RECORD
            sub_Create_Before_Cpr_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Lst_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.LST-TRANS-DTE := #TRANS-DTE
            pnd_Cntrct_Fund_Key.setValue(pnd_Cntrct_Payee_Key);                                                                                                           //Natural: ASSIGN #CNTRCT-FUND-KEY := #CNTRCT-PAYEE-KEY
                                                                                                                                                                          //Natural: PERFORM GET-FUND-DATA
            sub_Get_Fund_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("F1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("F1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(pnd_Err_Cde.notEquals(" ")))                                                                                                                        //Natural: IF #ERR-CDE NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* * IF #ORGN-CDE = 20 THRU 22
        //* *   PERFORM CALCULATE-AGE-ETC
        //* * ELSE
                                                                                                                                                                          //Natural: PERFORM UPDATE-IVC-INFO
        sub_Update_Ivc_Info();
        if (condition(Global.isEscape())) {return;}
        //* * END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-AFTER-CNTRCT-RECORD
        sub_Create_After_Cntrct_Record();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-AFTER-CPR-RECORD
        sub_Create_After_Cpr_Record();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-TRANS-RECORD
        sub_Create_Trans_Record();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Get_Fund_Data() throws Exception                                                                                                                     //Natural: GET-FUND-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(pnd_Tiaa.getBoolean()))                                                                                                                             //Natural: IF #TIAA
        {
            pnd_C.reset();                                                                                                                                                //Natural: RESET #C
                                                                                                                                                                          //Natural: PERFORM GET-TIAA-FUND
            sub_Get_Tiaa_Fund();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Err_Cde.notEquals(" ")))                                                                                                                    //Natural: IF #ERR-CDE NE ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Cref.getBoolean() || pnd_Rea.getBoolean()))                                                                                                     //Natural: IF #CREF OR #REA
        {
            pnd_T.reset();                                                                                                                                                //Natural: RESET #T
                                                                                                                                                                          //Natural: PERFORM GET-CREF-FUND
            sub_Get_Cref_Fund();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_Trans_Record() throws Exception                                                                                                               //Natural: CREATE-TRANS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        ldaAdsl601.getIaa_Trans_Rcrd_Trans_Ppcn_Nbr().setValue(pnd_Cntrct_Fund_Key_Pnd_Cntrct_Ppcn_Nbr);                                                                  //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PPCN-NBR := #CNTRCT-PPCN-NBR
        ldaAdsl601.getIaa_Trans_Rcrd_Trans_Payee_Cde().setValue(pnd_Cntrct_Fund_Key_Pnd_Cntrct_Payee_Cde);                                                                //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-PAYEE-CDE := #CNTRCT-PAYEE-CDE
        ldaAdsl601.getIaa_Trans_Rcrd_Trans_Cde().setValue(60);                                                                                                            //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 60
        ldaAdsl601.getIaa_Trans_Rcrd_Trans_User_Area().setValue("ADS");                                                                                                   //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-AREA := 'ADS'
        ldaAdsl601.getIaa_Trans_Rcrd_Trans_User_Id().setValue("ADSN600");                                                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-USER-ID := 'ADSN600'
        ldaAdsl601.getIaa_Trans_Rcrd_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                                 //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-DTE := #TRANS-DTE
        ldaAdsl601.getIaa_Trans_Rcrd_Invrse_Trans_Dte().setValue(pnd_Invrse_Trans_Dte);                                                                                   //Natural: ASSIGN IAA-TRANS-RCRD.INVRSE-TRANS-DTE := #INVRSE-TRANS-DTE
        ldaAdsl601.getIaa_Trans_Rcrd_Lst_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                             //Natural: ASSIGN IAA-TRANS-RCRD.LST-TRANS-DTE := #TRANS-DTE
        pnd_Wdte8.setValueEdited(pnd_Bsnss_Dte,new ReportEditMask("YYYYMMDD"));                                                                                           //Natural: MOVE EDITED #BSNSS-DTE ( EM = YYYYMMDD ) TO #WDTE8
        ldaAdsl601.getIaa_Trans_Rcrd_Trans_Todays_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Trans_Rcrd_Trans_Todays_Dte()), pnd_Wdte8.val());          //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-TODAYS-DTE := VAL ( #WDTE8 )
        ldaAdsl601.getIaa_Trans_Rcrd_Trans_Check_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Trans_Rcrd_Trans_Check_Dte()), pnd_Trans_Check_Dte.val());  //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CHECK-DTE := VAL ( #TRANS-CHECK-DTE )
        ldaAdsl601.getVw_iaa_Trans_Rcrd().insertDBRow();                                                                                                                  //Natural: STORE IAA-TRANS-RCRD
        if (condition(pnd_Ivc.getBoolean()))                                                                                                                              //Natural: IF #IVC
        {
            pnd_Ivc.reset();                                                                                                                                              //Natural: RESET #IVC
            ldaAdsl601.getIaa_Trans_Rcrd_Trans_Cde().setValue(725);                                                                                                       //Natural: ASSIGN IAA-TRANS-RCRD.TRANS-CDE := 725
            ldaAdsl601.getVw_iaa_Trans_Rcrd().insertDBRow();                                                                                                              //Natural: STORE IAA-TRANS-RCRD
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_Ivc_Info() throws Exception                                                                                                                   //Natural: UPDATE-IVC-INFO
    {
        if (BLNatReinput.isReinput()) return;

        G5:                                                                                                                                                               //Natural: GET IAA-CNTRCT-PRTCPNT-ROLE *ISN ( F1. )
        ldaAdsl601.getVw_iaa_Cntrct_Prtcpnt_Role().readByID(ldaAdsl601.getVw_iaa_Cntrct_Prtcpnt_Role().getAstISN("F1"), "G5");
        pnd_Asd_A.setValueEdited(ldaAdsl601.getAds_Ia_Rslt_Adi_Annty_Strt_Dte(),new ReportEditMask("YYYYMMDD"));                                                          //Natural: MOVE EDITED ADS-IA-RSLT.ADI-ANNTY-STRT-DTE ( EM = YYYYMMDD ) TO #ASD-A
        pnd_Dob.setValueEdited(pnd_Dob_Dte,new ReportEditMask("YYYYMMDD"));                                                                                               //Natural: MOVE EDITED #DOB-DTE ( EM = YYYYMMDD ) TO #DOB
        pnd_Dob2.setValueEdited(pnd_2_Dob_Dte,new ReportEditMask("YYYYMMDD"));                                                                                            //Natural: MOVE EDITED #2-DOB-DTE ( EM = YYYYMMDD ) TO #DOB2
        DbsUtil.callnat(Iaan037m.class , getCurrentProcessState(), pnd_Sim_Rule, pnd_Asd_A_Pnd_Asd_N6, ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde(), ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde(),  //Natural: CALLNAT 'IAAN037M' #SIM-RULE #ASD-N6 IAA-CNTRCT.CNTRCT-OPTN-CDE IAA-CNTRCT.CNTRCT-OPTN-CDE #DOB-N #DOB2-N #SIM-AGE
            pnd_Dob_Pnd_Dob_N, pnd_Dob2_Pnd_Dob2_N, pnd_Sim_Age);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Sim_Rule.equals("N")))                                                                                                                          //Natural: IF #SIM-RULE = 'N'
        {
            pdaNaza600i.getNaza600i_Check_D().setValue(ldaAdsl601.getIaa_Cntrl_Rcrd_1_Cntrl_Check_Dte());                                                                 //Natural: ASSIGN NAZA600I.CHECK-D := IAA-CNTRL-RCRD-1.CNTRL-CHECK-DTE
            pdaNaza600i.getNaza600i_Cntrct_Optn_Cde().setValue(ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde());                                                               //Natural: ASSIGN NAZA600I.CNTRCT-OPTN-CDE := IAA-CNTRCT.CNTRCT-OPTN-CDE
            pdaNaza600i.getNaza600i_Cntrct_Mode_Ind().setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Pymnt_Mode());                                                               //Natural: ASSIGN NAZA600I.CNTRCT-MODE-IND := ADS-IA-RSLT.ADI-PYMNT-MODE
            pdaNaza600i.getNaza600i_Cntrct_Issue_Dte().setValue(pnd_Asd_A_Pnd_Asd_N6);                                                                                    //Natural: ASSIGN NAZA600I.CNTRCT-ISSUE-DTE := #ASD-N6
            pdaNaza600i.getNaza600i_Cntrct_First_Pymnt_Due_Dte().setValue(pnd_Asd_A_Pnd_Asd_N6);                                                                          //Natural: ASSIGN NAZA600I.CNTRCT-FIRST-PYMNT-DUE-DTE := #ASD-N6
            pdaNaza600i.getNaza600i_Cntrct_First_Annt_Dob_Dte().setValue(pnd_Dob_Pnd_Dob_N);                                                                              //Natural: ASSIGN NAZA600I.CNTRCT-FIRST-ANNT-DOB-DTE := #DOB-N
            pnd_Wdte8.setValueEdited(pnd_2_Dob_Dte,new ReportEditMask("YYYYMMDD"));                                                                                       //Natural: MOVE EDITED #2-DOB-DTE ( EM = YYYYMMDD ) TO #WDTE8
            if (condition(DbsUtil.is(pnd_Wdte8.getText(),"N8")))                                                                                                          //Natural: IF #WDTE8 IS ( N8 )
            {
                pdaNaza600i.getNaza600i_Cntrct_Scnd_Annt_Dob_Dte().compute(new ComputeParameters(false, pdaNaza600i.getNaza600i_Cntrct_Scnd_Annt_Dob_Dte()),              //Natural: ASSIGN NAZA600I.CNTRCT-SCND-ANNT-DOB-DTE := VAL ( #WDTE8 )
                    pnd_Wdte8.val());
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Fp_Dte.greater(getZero())))                                                                                                                 //Natural: IF #FP-DTE GT 0
            {
                pnd_Wdte6.setValueEdited(pnd_Fp_Dte,new ReportEditMask("YYYYMM"));                                                                                        //Natural: MOVE EDITED #FP-DTE ( EM = YYYYMM ) TO #WDTE6
            }                                                                                                                                                             //Natural: END-IF
            if (condition(DbsUtil.is(pnd_Wdte6.getText(),"N6")))                                                                                                          //Natural: IF #WDTE6 IS ( N6 )
            {
                pdaNaza600i.getNaza600i_Cntrct_Final_Per_Pay_Dte().compute(new ComputeParameters(false, pdaNaza600i.getNaza600i_Cntrct_Final_Per_Pay_Dte()),              //Natural: ASSIGN NAZA600I.CNTRCT-FINAL-PER-PAY-DTE := VAL ( #WDTE6 )
                    pnd_Wdte6.val());
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Tiaa.getBoolean() || pnd_Rea.getBoolean()))                                                                                                 //Natural: IF #TIAA OR #REA
            {
                if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind().equals("Y")))                                                                               //Natural: IF ADS-IA-RSLT.ADI-TIAA-IVC-GD-IND = 'Y'
                {
                    pdaNaza600i.getNaza600i_Cntrct_Ivc_Amt().setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                                      //Natural: ASSIGN NAZA600I.CNTRCT-IVC-AMT := ADS-IA-RSLT.ADI-TIAA-IVC-AMT
                    pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().compute(new ComputeParameters(false, pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt()), DbsField.add(getZero(),    //Natural: ASSIGN NAZA600I.#PER-PAY-AMT := 0 + ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * )
                        ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue("*")));
                    pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue("*"));                                 //Natural: ASSIGN NAZA600I.#PER-PAY-AMT := NAZA600I.#PER-PAY-AMT + ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( * )
                    //*  030812 START
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue("*"));                                             //Natural: ASSIGN NAZA600I.#PER-PAY-AMT := NAZA600I.#PER-PAY-AMT + ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * )
                        pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue("*"));                                             //Natural: ASSIGN NAZA600I.#PER-PAY-AMT := NAZA600I.#PER-PAY-AMT + ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( * )
                        //*  030812 END
                    }                                                                                                                                                     //Natural: END-IF
                    DbsUtil.examine(new ExamineSource(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue("*"),true), new ExamineSearch("R"), new                   //Natural: EXAMINE FULL ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( * ) FOR 'R' GIVING INDEX #I
                        ExamineGivingIndex(pnd_I));
                    if (condition(pnd_I.greater(getZero())))                                                                                                              //Natural: IF #I GT 0
                    {
                        pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_I));                           //Natural: ASSIGN NAZA600I.#PER-PAY-AMT := NAZA600I.#PER-PAY-AMT + ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #I )
                        pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_I));                             //Natural: ASSIGN NAZA600I.#PER-PAY-AMT := NAZA600I.#PER-PAY-AMT + ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                    //* * EM - 012309 START
                    DbsUtil.examine(new ExamineSource(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue("*"),true), new ExamineSearch("D"), new                   //Natural: EXAMINE FULL ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( * ) FOR 'D' GIVING INDEX #I
                        ExamineGivingIndex(pnd_I));
                    if (condition(pnd_I.greater(getZero())))                                                                                                              //Natural: IF #I GT 0
                    {
                        pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_I));                           //Natural: ASSIGN NAZA600I.#PER-PAY-AMT := NAZA600I.#PER-PAY-AMT + ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #I )
                        pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_I));                             //Natural: ASSIGN NAZA600I.#PER-PAY-AMT := NAZA600I.#PER-PAY-AMT + ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                    pdaNaza600i.getNaza600i_Pnd_Final_Pay_Amt().compute(new ComputeParameters(false, pdaNaza600i.getNaza600i_Pnd_Final_Pay_Amt()), DbsField.add(getZero(), //Natural: ASSIGN NAZA600I.#FINAL-PAY-AMT := 0 + ADS-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( * )
                        ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt().getValue("*")));
                    pdaNaza600i.getNaza600i_Pnd_Final_Pay_Amt().nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue("*"));                                  //Natural: ASSIGN NAZA600I.#FINAL-PAY-AMT := NAZA600I.#FINAL-PAY-AMT + ADS-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( * )
                    //*  030812 START
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        pdaNaza600i.getNaza600i_Pnd_Final_Pay_Amt().nadd(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue("*"));                                              //Natural: ASSIGN NAZA600I.#FINAL-PAY-AMT := NAZA600I.#FINAL-PAY-AMT + ADS-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( * )
                        pdaNaza600i.getNaza600i_Pnd_Final_Pay_Amt().nadd(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue("*"));                                              //Natural: ASSIGN NAZA600I.#FINAL-PAY-AMT := NAZA600I.#FINAL-PAY-AMT + ADS-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( * )
                        //*  030812 END
                        //*  TIAA
                    }                                                                                                                                                     //Natural: END-IF
                    pdaNaza600i.getNaza600i_Cntrct_Company_Cd().setValue("1");                                                                                            //Natural: ASSIGN NAZA600I.CNTRCT-COMPANY-CD := '1'
                    DbsUtil.callnat(Nazn637e.class , getCurrentProcessState(), pdaNaza600i.getNaza600i());                                                                //Natural: CALLNAT 'NAZN637E' NAZA600I
                    if (condition(Global.isEscape())) return;
                    if (condition(pdaNaza600i.getNaza600i_Return_Code().equals("B")))                                                                                     //Natural: IF NAZA600I.RETURN-CODE = 'B'
                    {
                        pnd_Prdc_Ivc_Amt.reset();                                                                                                                         //Natural: RESET #PRDC-IVC-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Prdc_Ivc_Amt.setValue(pdaNaza600i.getNaza600i_Cntrct_Per_Ivc_Amt());                                                                          //Natural: ASSIGN #PRDC-IVC-AMT := NAZA600I.CNTRCT-PER-IVC-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref.getBoolean()))                                                                                                                         //Natural: IF #CREF
            {
                //*  CREF
                if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind().equals("Y")))                                                                               //Natural: IF ADS-IA-RSLT.ADI-CREF-IVC-GD-IND = 'Y'
                {
                    pdaNaza600i.getNaza600i_Cntrct_Company_Cd().setValue("2");                                                                                            //Natural: ASSIGN NAZA600I.CNTRCT-COMPANY-CD := '2'
                    pdaNaza600i.getNaza600i_Cntrct_Ivc_Amt().setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                                                      //Natural: ASSIGN NAZA600I.CNTRCT-IVC-AMT := ADS-IA-RSLT.ADI-CREF-IVC-AMT
                    pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().reset();                                                                                                    //Natural: RESET NAZA600I.#PER-PAY-AMT NAZA600I.#FINAL-PAY-AMT
                    pdaNaza600i.getNaza600i_Pnd_Final_Pay_Amt().reset();
                    FOR08:                                                                                                                                                //Natural: FOR #I 1 TO 20
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
                    {
                        //*  EM - 012309
                        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_I).notEquals(" ") && ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_I).notEquals("R")  //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #I ) NE ' ' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #I ) NE 'R' AND ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #I ) NE 'D'
                            && ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_I).notEquals("D")))
                        {
                            pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_I));                       //Natural: ASSIGN NAZA600I.#PER-PAY-AMT := NAZA600I.#PER-PAY-AMT + ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #I )
                            pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_I));                         //Natural: ASSIGN NAZA600I.#PER-PAY-AMT := NAZA600I.#PER-PAY-AMT + ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #I )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                    DbsUtil.callnat(Nazn637e.class , getCurrentProcessState(), pdaNaza600i.getNaza600i());                                                                //Natural: CALLNAT 'NAZN637E' NAZA600I
                    if (condition(Global.isEscape())) return;
                    if (condition(pdaNaza600i.getNaza600i_Return_Code().equals("B")))                                                                                     //Natural: IF NAZA600I.RETURN-CODE = 'B'
                    {
                        pnd_Prdc_Ivc_Amt.reset();                                                                                                                         //Natural: RESET #PRDC-IVC-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Prdc_Ivc_Amt.setValue(pdaNaza600i.getNaza600i_Cntrct_Per_Ivc_Amt());                                                                          //Natural: ASSIGN #PRDC-IVC-AMT := NAZA600I.CNTRCT-PER-IVC-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*   DG  080207
            if (condition((ldaAdsl601.getIaa_Cntrct_Cntrct_Issue_Dte().greater(199712) && (((((ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde().equals(3) ||                    //Natural: IF IAA-CNTRCT.CNTRCT-ISSUE-DTE GT 199712 AND ( IAA-CNTRCT.CNTRCT-OPTN-CDE = 03 OR = 04 OR = 07 OR = 08 OR IAA-CNTRCT.CNTRCT-OPTN-CDE = 10 THRU 17 OR IAA-CNTRCT.CNTRCT-OPTN-CDE = 54 THRU 57 )
                ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde().equals(4)) || ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde().equals(7)) || ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde().equals(8)) 
                || (ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(10) && ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(17))) || (ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde().greaterOrEqual(54) 
                && ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde().lessOrEqual(57))))))
            {
                short decideConditionsMet1893 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #SIM-AGE GT 140
                if (condition(pnd_Sim_Age.greater(140)))
                {
                    decideConditionsMet1893++;
                    pnd_No_Pays.setValue(210);                                                                                                                            //Natural: ASSIGN #NO-PAYS := 210
                }                                                                                                                                                         //Natural: WHEN #SIM-AGE GT 130
                else if (condition(pnd_Sim_Age.greater(130)))
                {
                    decideConditionsMet1893++;
                    pnd_No_Pays.setValue(260);                                                                                                                            //Natural: ASSIGN #NO-PAYS := 260
                }                                                                                                                                                         //Natural: WHEN #SIM-AGE GT 120
                else if (condition(pnd_Sim_Age.greater(120)))
                {
                    decideConditionsMet1893++;
                    pnd_No_Pays.setValue(310);                                                                                                                            //Natural: ASSIGN #NO-PAYS := 310
                }                                                                                                                                                         //Natural: WHEN #SIM-AGE GT 110
                else if (condition(pnd_Sim_Age.greater(110)))
                {
                    decideConditionsMet1893++;
                    pnd_No_Pays.setValue(360);                                                                                                                            //Natural: ASSIGN #NO-PAYS := 360
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_No_Pays.setValue(410);                                                                                                                            //Natural: ASSIGN #NO-PAYS := 410
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                short decideConditionsMet1908 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #SIM-AGE GT 70
                if (condition(pnd_Sim_Age.greater(70)))
                {
                    decideConditionsMet1908++;
                    pnd_No_Pays.setValue(160);                                                                                                                            //Natural: ASSIGN #NO-PAYS := 160
                }                                                                                                                                                         //Natural: WHEN #SIM-AGE GT 65
                else if (condition(pnd_Sim_Age.greater(65)))
                {
                    decideConditionsMet1908++;
                    pnd_No_Pays.setValue(210);                                                                                                                            //Natural: ASSIGN #NO-PAYS := 210
                }                                                                                                                                                         //Natural: WHEN #SIM-AGE GT 60
                else if (condition(pnd_Sim_Age.greater(60)))
                {
                    decideConditionsMet1908++;
                    pnd_No_Pays.setValue(260);                                                                                                                            //Natural: ASSIGN #NO-PAYS := 260
                }                                                                                                                                                         //Natural: WHEN #SIM-AGE GT 55
                else if (condition(pnd_Sim_Age.greater(55)))
                {
                    decideConditionsMet1908++;
                    pnd_No_Pays.setValue(310);                                                                                                                            //Natural: ASSIGN #NO-PAYS := 310
                }                                                                                                                                                         //Natural: WHEN #SIM-AGE LE 55
                else if (condition(pnd_Sim_Age.lessOrEqual(55)))
                {
                    decideConditionsMet1908++;
                    pnd_No_Pays.setValue(360);                                                                                                                            //Natural: ASSIGN #NO-PAYS := 360
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_No_Pays.setValue(360);                                                                                                                            //Natural: ASSIGN #NO-PAYS := 360
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet1925 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE ADS-IA-RSLT.ADI-PYMNT-MODE;//Natural: VALUE 100
            if (condition((ldaAdsl601.getAds_Ia_Rslt_Adi_Pymnt_Mode().equals(100))))
            {
                decideConditionsMet1925++;
                pnd_No_Per_Yr.setValue(12);                                                                                                                               //Natural: ASSIGN #NO-PER-YR := 12
            }                                                                                                                                                             //Natural: VALUE 601:603
            else if (condition(((ldaAdsl601.getAds_Ia_Rslt_Adi_Pymnt_Mode().greaterOrEqual(601) && ldaAdsl601.getAds_Ia_Rslt_Adi_Pymnt_Mode().lessOrEqual(603)))))
            {
                decideConditionsMet1925++;
                pnd_No_Per_Yr.setValue(4);                                                                                                                                //Natural: ASSIGN #NO-PER-YR := 4
            }                                                                                                                                                             //Natural: VALUE 701:706
            else if (condition(((ldaAdsl601.getAds_Ia_Rslt_Adi_Pymnt_Mode().greaterOrEqual(701) && ldaAdsl601.getAds_Ia_Rslt_Adi_Pymnt_Mode().lessOrEqual(706)))))
            {
                decideConditionsMet1925++;
                pnd_No_Per_Yr.setValue(2);                                                                                                                                //Natural: ASSIGN #NO-PER-YR := 2
            }                                                                                                                                                             //Natural: VALUE 801:812
            else if (condition(((ldaAdsl601.getAds_Ia_Rslt_Adi_Pymnt_Mode().greaterOrEqual(801) && ldaAdsl601.getAds_Ia_Rslt_Adi_Pymnt_Mode().lessOrEqual(812)))))
            {
                decideConditionsMet1925++;
                pnd_No_Per_Yr.setValue(1);                                                                                                                                //Natural: ASSIGN #NO-PER-YR := 1
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pnd_No_Per_Yr.setValue(12);                                                                                                                               //Natural: ASSIGN #NO-PER-YR := 12
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(pnd_Tiaa.getBoolean() || pnd_Rea.getBoolean()))                                                                                                 //Natural: IF #TIAA OR #REA
            {
                pnd_Ivc_Amt.setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                                                                       //Natural: ASSIGN #IVC-AMT := ADS-IA-RSLT.ADI-TIAA-IVC-AMT
                if (condition(pnd_Ivc_Amt.greater(getZero())))                                                                                                            //Natural: IF #IVC-AMT GT 0
                {
                    if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt().greater(getZero()) && ldaAdsl601.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind().equals("Y")))        //Natural: IF ADS-IA-RSLT.ADI-TIAA-IVC-AMT GT 0 AND ADS-IA-RSLT.ADI-TIAA-IVC-GD-IND = 'Y'
                    {
                        pnd_Prdc_Ivc_Amt.compute(new ComputeParameters(true, pnd_Prdc_Ivc_Amt), (pnd_Ivc_Amt.multiply(12)).divide((pnd_No_Pays.multiply(pnd_No_Per_Yr)))); //Natural: COMPUTE ROUNDED #PRDC-IVC-AMT = ( #IVC-AMT * 12 ) / ( #NO-PAYS * #NO-PER-YR )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ivc_Amt.setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                                                                                       //Natural: ASSIGN #IVC-AMT := ADS-IA-RSLT.ADI-CREF-IVC-AMT
                if (condition(pnd_Ivc_Amt.greater(getZero())))                                                                                                            //Natural: IF #IVC-AMT GT 0
                {
                    if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt().greater(getZero()) && ldaAdsl601.getAds_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind().equals("Y")))        //Natural: IF ADS-IA-RSLT.ADI-CREF-IVC-AMT GT 0 AND ADS-IA-RSLT.ADI-CREF-IVC-GD-IND = 'Y'
                    {
                        pnd_Prdc_Ivc_Amt.compute(new ComputeParameters(true, pnd_Prdc_Ivc_Amt), (pnd_Ivc_Amt.multiply(12)).divide((pnd_No_Pays.multiply(pnd_No_Per_Yr)))); //Natural: COMPUTE ROUNDED #PRDC-IVC-AMT = ( #IVC-AMT * 12 ) / ( #NO-PAYS * #NO-PER-YR )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Tiaa.getBoolean() || pnd_Rea.getBoolean()))                                                                                                     //Natural: IF #TIAA OR #REA
        {
            if (condition(pnd_Prdc_Ivc_Amt.greater(getZero())))                                                                                                           //Natural: IF #PRDC-IVC-AMT GT 0
            {
                ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt().getValue(1).nadd(pnd_Prdc_Ivc_Amt);                                                            //Natural: ADD #PRDC-IVC-AMT TO IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PER-IVC-AMT ( 1 )
                ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt().getValue(1).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Tiaa_Ivc_Amt());                                    //Natural: ADD ADS-IA-RSLT.ADI-TIAA-IVC-AMT TO IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-AMT ( 1 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Prdc_Ivc_Amt.greater(getZero())))                                                                                                           //Natural: IF #PRDC-IVC-AMT GT 0
            {
                ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Per_Ivc_Amt().getValue(2).nadd(pnd_Prdc_Ivc_Amt);                                                            //Natural: ADD #PRDC-IVC-AMT TO IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-PER-IVC-AMT ( 2 )
                ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Ivc_Amt().getValue(2).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Cref_Ivc_Amt());                                    //Natural: ADD ADS-IA-RSLT.ADI-CREF-IVC-AMT TO IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-IVC-AMT ( 2 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().equals(9)))                                                                               //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE = 9
        {
            ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().setValue(1);                                                                                        //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE := 1
            if (condition(pnd_Cref.getBoolean()))                                                                                                                         //Natural: IF #CREF
            {
                pnd_Actve_C.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #ACTVE-C
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Actve_T.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #ACTVE-T
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn().setValue(" ");                                                                                          //Natural: ASSIGN IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-TRMNTE-RSN := ' '
        ldaAdsl601.getVw_iaa_Cntrct_Prtcpnt_Role().updateDBRow("G5");                                                                                                     //Natural: UPDATE ( G5. )
        pnd_Ivc.setValue(true);                                                                                                                                           //Natural: ASSIGN #IVC := TRUE
        G6:                                                                                                                                                               //Natural: GET IAA-CNTRCT *ISN ( F0. )
        ldaAdsl601.getVw_iaa_Cntrct().readByID(ldaAdsl601.getVw_iaa_Cntrct().getAstISN("F0"), "G6");
        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Sqnce_Nbr().equals(12)))                                                                                              //Natural: IF ADS-IA-RSLT.ADI-SQNCE-NBR = 12
        {
            ldaAdsl601.getIaa_Cntrct_Cntrct_Fnl_Prm_Dte().getValue(1).setValue(pnd_Effct_Dte);                                                                            //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FNL-PRM-DTE ( 1 ) := #EFFCT-DTE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAdsl601.getIaa_Cntrct_Cntrct_Fnl_Prm_Dte().getValue(2).setValue(pnd_Effct_Dte);                                                                            //Natural: ASSIGN IAA-CNTRCT.CNTRCT-FNL-PRM-DTE ( 2 ) := #EFFCT-DTE
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl601.getVw_iaa_Cntrct().updateDBRow("G6");                                                                                                                  //Natural: UPDATE ( G6. )
    }
    private void sub_Create_Tiaa_Fund_Record() throws Exception                                                                                                           //Natural: CREATE-TIAA-FUND-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(pnd_Create_Std.getBoolean()))                                                                                                                       //Natural: IF #CREATE-STD
        {
            if (condition(pnd_Inactive_Tiaa_Isn.greater(getZero())))                                                                                                      //Natural: IF #INACTIVE-TIAA-ISN GT 0
            {
                G4:                                                                                                                                                       //Natural: GET IAA-TIAA-FUND-RCRD #INACTIVE-TIAA-ISN
                ldaAdsl601.getVw_iaa_Tiaa_Fund_Rcrd().readByID(pnd_Inactive_Tiaa_Isn.getLong(), "G4");
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Ia_Tiaa_Nbr());                                            //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR := ADS-IA-RSLT.ADI-IA-TIAA-NBR
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().setValue(1);                                                                                     //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE := 01
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().setValue("T1S");                                                                                   //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE := 'T1S'
                //*  030812 START
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt()),                //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT := 0 + ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( * )
                DbsField.add(getZero(),ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue("*")));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt()),                //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT := 0 + ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( * )
                DbsField.add(getZero(),ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue("*")));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind().setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Pymnt_Mode());                                                        //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-MODE-IND := ADS-IA-RSLT.ADI-PYMNT-MODE
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(1,         //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( 1:#MAX-ADI ) := ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-ADI )
                ":",pnd_Max_Adi));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Contract_Id().getValue(1,                 //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( 1:#MAX-ADI ) := ADS-IA-RSLT.ADI-CONTRACT-ID ( 1:#MAX-ADI )
                ":",pnd_Max_Adi));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(1,   //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( 1:#MAX-ADI ) := ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-ADI )
                ":",pnd_Max_Adi));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(1,   //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( 1:#MAX-ADI ) := ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-ADI )
                ":",pnd_Max_Adi));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(1, //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( 1:#MAX-ADI ) := ADS-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( 1:#MAX-ADI )
                ":",pnd_Max_Adi));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(1, //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( 1:#MAX-ADI ) := ADS-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( 1:#MAX-ADI )
                ":",pnd_Max_Adi));
            //*  030812 END
            if (condition(pnd_More.getBoolean()))                                                                                                                         //Natural: IF #MORE
            {
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue("*"));                                           //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT := IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT + ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( * )
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue("*"));                                           //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT := IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT + ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( * )
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_Max_Adi.getDec().add(1),":",pnd_Max_Rte).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(1, //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #MAX-ADI + 1:#MAX-RTE ) := ADS-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-ADI )
                    ":",pnd_Max_Adi));
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(pnd_Max_Adi.getDec().add(1),":",pnd_Max_Rte).setValue(ads_Ia_Rsl2_Adi_Contract_Id.getValue(1,   //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( #MAX-ADI + 1:#MAX-RTE ) := ADS-IA-RSL2.ADI-CONTRACT-ID ( 1:#MAX-ADI )
                    ":",pnd_Max_Adi));
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_Max_Adi.getDec().add(1),":",pnd_Max_Rte).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(1, //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #MAX-ADI + 1:#MAX-RTE ) := ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-ADI )
                    ":",pnd_Max_Adi));
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_Max_Adi.getDec().add(1),":",pnd_Max_Rte).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(1, //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #MAX-ADI + 1:#MAX-RTE ) := ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-ADI )
                    ":",pnd_Max_Adi));
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_Max_Adi.getDec().add(1),":",pnd_Max_Rte).setValue(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(1, //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #MAX-ADI + 1:#MAX-RTE ) := ADS-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( 1:#MAX-ADI )
                    ":",pnd_Max_Adi));
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_Max_Adi.getDec().add(1),":",pnd_Max_Rte).setValue(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(1, //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #MAX-ADI + 1:#MAX-RTE ) := ADS-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( 1:#MAX-ADI )
                    ":",pnd_Max_Adi));
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                     //Natural: ASSIGN IAA-TIAA-FUND-RCRD.LST-TRANS-DTE := #TRANS-DTE
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue("*").reset();                                                                                       //Natural: RESET IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( * )
            if (condition(pnd_Inactive_Tiaa_Isn.greater(getZero())))                                                                                                      //Natural: IF #INACTIVE-TIAA-ISN GT 0
            {
                ldaAdsl601.getVw_iaa_Tiaa_Fund_Rcrd().updateDBRow("G4");                                                                                                  //Natural: UPDATE ( G4. )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl601.getVw_iaa_Tiaa_Fund_Rcrd().insertDBRow();                                                                                                      //Natural: STORE IAA-TIAA-FUND-RCRD
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-AFTER-TIAA-FUND
            sub_Create_After_Tiaa_Fund();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  030812 START
        if (condition(pnd_Create_Grd.getBoolean()))                                                                                                                       //Natural: IF #CREATE-GRD
        {
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Ia_Tiaa_Nbr());                                                //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR := ADS-IA-RSLT.ADI-IA-TIAA-NBR
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().setValue(1);                                                                                         //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE := 01
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cmpny_Fund_Cde().setValue("T1G");                                                                                       //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-CMPNY-FUND-CDE := 'T1G'
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt()),                //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT := 0 + ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * )
                DbsField.add(getZero(),ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue("*")));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt()),                //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT := 0 + ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( * )
                DbsField.add(getZero(),ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue("*")));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(1,         //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( 1:#MAX-ADI ) := ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-ADI )
                ":",pnd_Max_Adi));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Contract_Id().getValue(1,                 //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( 1:#MAX-ADI ) := ADS-IA-RSLT.ADI-CONTRACT-ID ( 1:#MAX-ADI )
                ":",pnd_Max_Adi));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind().setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Pymnt_Mode());                                                        //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-MODE-IND := ADS-IA-RSLT.ADI-PYMNT-MODE
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(1,   //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( 1:#MAX-ADI ) := ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( 1:#MAX-ADI )
                ":",pnd_Max_Adi));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(1,   //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( 1:#MAX-ADI ) := ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( 1:#MAX-ADI )
                ":",pnd_Max_Adi));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(1, //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( 1:#MAX-ADI ) := ADS-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( 1:#MAX-ADI )
                ":",pnd_Max_Adi));
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(1, //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( 1:#MAX-ADI ) := ADS-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( 1:#MAX-ADI )
                ":",pnd_Max_Adi));
            if (condition(pnd_More.getBoolean()))                                                                                                                         //Natural: IF #MORE
            {
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue("*"));                                           //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT := IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT + ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * )
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue("*"));                                           //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT := IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT + ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( * )
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_Max_Adi.getDec().add(1),":",pnd_Max_Rte).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(1, //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #MAX-ADI + 1:#MAX-RTE ) := ADS-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-ADI )
                    ":",pnd_Max_Adi));
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(pnd_Max_Adi.getDec().add(1),":",pnd_Max_Rte).setValue(ads_Ia_Rsl2_Adi_Contract_Id.getValue(1,   //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( #MAX-ADI + 1:#MAX-RTE ) := ADS-IA-RSL2.ADI-CONTRACT-ID ( 1:#MAX-ADI )
                    ":",pnd_Max_Adi));
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind().setValue(ads_Ia_Rsl2_Adi_Pymnt_Mode);                                                                    //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-MODE-IND := ADS-IA-RSL2.ADI-PYMNT-MODE
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_Max_Adi.getDec().add(1),":",pnd_Max_Rte).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(1, //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #MAX-ADI + 1:#MAX-RTE ) := ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( 1:#MAX-ADI )
                    ":",pnd_Max_Adi));
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_Max_Adi.getDec().add(1),":",pnd_Max_Rte).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(1, //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #MAX-ADI + 1:#MAX-RTE ) := ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( 1:#MAX-ADI )
                    ":",pnd_Max_Adi));
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_Max_Adi.getDec().add(1),":",pnd_Max_Rte).setValue(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(1, //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #MAX-ADI + 1:#MAX-RTE ) := ADS-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( 1:#MAX-ADI )
                    ":",pnd_Max_Adi));
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_Max_Adi.getDec().add(1),":",pnd_Max_Rte).setValue(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(1, //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #MAX-ADI + 1:#MAX-RTE ) := ADS-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( 1:#MAX-ADI )
                    ":",pnd_Max_Adi));
                //*  030812 END
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                     //Natural: ASSIGN IAA-TIAA-FUND-RCRD.LST-TRANS-DTE := #TRANS-DTE
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue("*").reset();                                                                                       //Natural: RESET IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( * )
            ldaAdsl601.getVw_iaa_Tiaa_Fund_Rcrd().insertDBRow();                                                                                                          //Natural: STORE IAA-TIAA-FUND-RCRD
                                                                                                                                                                          //Natural: PERFORM CREATE-AFTER-TIAA-FUND
            sub_Create_After_Tiaa_Fund();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_Before_Cntrct_Record() throws Exception                                                                                                       //Natural: CREATE-BEFORE-CNTRCT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaAdsl601.getVw_iaa_Cntrct_Trans().setValuesByName(ldaAdsl601.getVw_iaa_Cntrct());                                                                               //Natural: MOVE BY NAME IAA-CNTRCT TO IAA-CNTRCT-TRANS
        ldaAdsl601.getIaa_Cntrct_Trans_Bfre_Imge_Id().setValue("1");                                                                                                      //Natural: ASSIGN IAA-CNTRCT-TRANS.BFRE-IMGE-ID := '1'
        ldaAdsl601.getIaa_Cntrct_Trans_Aftr_Imge_Id().setValue(" ");                                                                                                      //Natural: ASSIGN IAA-CNTRCT-TRANS.AFTR-IMGE-ID := ' '
        ldaAdsl601.getIaa_Cntrct_Trans_Trans_Check_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cntrct_Trans_Trans_Check_Dte()), pnd_Trans_Check_Dte.val()); //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-CHECK-DTE := VAL ( #TRANS-CHECK-DTE )
        ldaAdsl601.getIaa_Cntrct_Trans_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                               //Natural: ASSIGN IAA-CNTRCT-TRANS.TRANS-DTE := #TRANS-DTE
        ldaAdsl601.getIaa_Cntrct_Trans_Invrse_Trans_Dte().setValue(pnd_Invrse_Trans_Dte);                                                                                 //Natural: ASSIGN IAA-CNTRCT-TRANS.INVRSE-TRANS-DTE := #INVRSE-TRANS-DTE
        ldaAdsl601.getVw_iaa_Cntrct_Trans().insertDBRow();                                                                                                                //Natural: STORE IAA-CNTRCT-TRANS
    }
    private void sub_Create_After_Cntrct_Record() throws Exception                                                                                                        //Natural: CREATE-AFTER-CNTRCT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaAdsl601.getVw_iaa_Cntrct_Trans().setValuesByName(ldaAdsl601.getVw_iaa_Cntrct());                                                                               //Natural: MOVE BY NAME IAA-CNTRCT TO IAA-CNTRCT-TRANS
        ldaAdsl601.getIaa_Cntrct_Trans_Bfre_Imge_Id().setValue(" ");                                                                                                      //Natural: ASSIGN IAA-CNTRCT-TRANS.BFRE-IMGE-ID := ' '
        ldaAdsl601.getIaa_Cntrct_Trans_Aftr_Imge_Id().setValue("2");                                                                                                      //Natural: ASSIGN IAA-CNTRCT-TRANS.AFTR-IMGE-ID := '2'
        ldaAdsl601.getVw_iaa_Cntrct_Trans().insertDBRow();                                                                                                                //Natural: STORE IAA-CNTRCT-TRANS
    }
    private void sub_Create_Before_Cpr_Record() throws Exception                                                                                                          //Natural: CREATE-BEFORE-CPR-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaAdsl601.getVw_iaa_Cpr_Trans().setValuesByName(ldaAdsl601.getVw_iaa_Cntrct_Prtcpnt_Role());                                                                     //Natural: MOVE BY NAME IAA-CNTRCT-PRTCPNT-ROLE TO IAA-CPR-TRANS
        ldaAdsl601.getIaa_Cpr_Trans_Bfre_Imge_Id().setValue("1");                                                                                                         //Natural: ASSIGN IAA-CPR-TRANS.BFRE-IMGE-ID := '1'
        ldaAdsl601.getIaa_Cpr_Trans_Aftr_Imge_Id().setValue(" ");                                                                                                         //Natural: ASSIGN IAA-CPR-TRANS.AFTR-IMGE-ID := ' '
        ldaAdsl601.getIaa_Cpr_Trans_Trans_Check_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cpr_Trans_Trans_Check_Dte()), pnd_Trans_Check_Dte.val());    //Natural: ASSIGN IAA-CPR-TRANS.TRANS-CHECK-DTE := VAL ( #TRANS-CHECK-DTE )
        ldaAdsl601.getIaa_Cpr_Trans_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                                  //Natural: ASSIGN IAA-CPR-TRANS.TRANS-DTE := #TRANS-DTE
        ldaAdsl601.getIaa_Cpr_Trans_Invrse_Trans_Dte().setValue(pnd_Invrse_Trans_Dte);                                                                                    //Natural: ASSIGN IAA-CPR-TRANS.INVRSE-TRANS-DTE := #INVRSE-TRANS-DTE
        ldaAdsl601.getVw_iaa_Cpr_Trans().insertDBRow();                                                                                                                   //Natural: STORE IAA-CPR-TRANS
    }
    private void sub_Create_After_Cpr_Record() throws Exception                                                                                                           //Natural: CREATE-AFTER-CPR-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaAdsl601.getVw_iaa_Cpr_Trans().setValuesByName(ldaAdsl601.getVw_iaa_Cntrct_Prtcpnt_Role());                                                                     //Natural: MOVE BY NAME IAA-CNTRCT-PRTCPNT-ROLE TO IAA-CPR-TRANS
        ldaAdsl601.getIaa_Cpr_Trans_Bfre_Imge_Id().setValue(" ");                                                                                                         //Natural: ASSIGN IAA-CPR-TRANS.BFRE-IMGE-ID := ' '
        ldaAdsl601.getIaa_Cpr_Trans_Aftr_Imge_Id().setValue("2");                                                                                                         //Natural: ASSIGN IAA-CPR-TRANS.AFTR-IMGE-ID := '2'
        ldaAdsl601.getVw_iaa_Cpr_Trans().insertDBRow();                                                                                                                   //Natural: STORE IAA-CPR-TRANS
    }
    private void sub_Create_Cref_Fund_Record() throws Exception                                                                                                           //Natural: CREATE-CREF-FUND-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_I).greater(getZero())))                                                      //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #I ) GT 0
        {
                                                                                                                                                                          //Natural: PERFORM CREATE-MONTHLY-CREF-FUND-RCRD
            sub_Create_Monthly_Cref_Fund_Rcrd();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_I).greater(getZero())))                                                        //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #I ) GT 0
        {
                                                                                                                                                                          //Natural: PERFORM CREATE-ANNUAL-CREF-FUND-RCRD
            sub_Create_Annual_Cref_Fund_Rcrd();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_Bfore_Tiaa_Fund() throws Exception                                                                                                            //Natural: CREATE-BFORE-TIAA-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaAdsl601.getVw_iaa_Tiaa_Fund_Trans().setValuesByName(ldaAdsl601.getVw_iaa_Tiaa_Fund_Rcrd());                                                                    //Natural: MOVE BY NAME IAA-TIAA-FUND-RCRD TO IAA-TIAA-FUND-TRANS
        ldaAdsl601.getIaa_Tiaa_Fund_Trans_Bfre_Imge_Id().setValue("1");                                                                                                   //Natural: ASSIGN IAA-TIAA-FUND-TRANS.BFRE-IMGE-ID := '1'
        ldaAdsl601.getIaa_Tiaa_Fund_Trans_Aftr_Imge_Id().setValue(" ");                                                                                                   //Natural: ASSIGN IAA-TIAA-FUND-TRANS.AFTR-IMGE-ID := ' '
        ldaAdsl601.getIaa_Tiaa_Fund_Trans_Trans_Check_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Tiaa_Fund_Trans_Trans_Check_Dte()),                    //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-CHECK-DTE := VAL ( #TRANS-CHECK-DTE )
            pnd_Trans_Check_Dte.val());
        ldaAdsl601.getIaa_Tiaa_Fund_Trans_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                            //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-DTE := #TRANS-DTE
        ldaAdsl601.getIaa_Tiaa_Fund_Trans_Invrse_Trans_Dte().setValue(pnd_Invrse_Trans_Dte);                                                                              //Natural: ASSIGN IAA-TIAA-FUND-TRANS.INVRSE-TRANS-DTE := #INVRSE-TRANS-DTE
        ldaAdsl601.getVw_iaa_Tiaa_Fund_Trans().insertDBRow();                                                                                                             //Natural: STORE IAA-TIAA-FUND-TRANS
        pnd_Cntrct_Py_Dte_Key_Cntrct_Ppcn_Nbr.setValue(ldaAdsl601.getIaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Ppcn_Nbr());                                                         //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.CNTRCT-PPCN-NBR := IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PPCN-NBR
        pnd_Cntrct_Py_Dte_Key_Cntrct_Payee_Cde.setValue(ldaAdsl601.getIaa_Tiaa_Fund_Trans_Tiaa_Cntrct_Payee_Cde());                                                       //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.CNTRCT-PAYEE-CDE := IAA-TIAA-FUND-TRANS.TIAA-CNTRCT-PAYEE-CDE
        pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde.setValue(ldaAdsl601.getIaa_Tiaa_Fund_Trans_Tiaa_Cmpny_Fund_Cde());                                                           //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.CMPNY-FUND-CDE := IAA-TIAA-FUND-TRANS.TIAA-CMPNY-FUND-CDE
                                                                                                                                                                          //Natural: PERFORM CREATE-HISTORICAL-RATES
        sub_Create_Historical_Rates();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Create_After_Tiaa_Fund() throws Exception                                                                                                            //Natural: CREATE-AFTER-TIAA-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaAdsl601.getVw_iaa_Tiaa_Fund_Trans().setValuesByName(ldaAdsl601.getVw_iaa_Tiaa_Fund_Rcrd());                                                                    //Natural: MOVE BY NAME IAA-TIAA-FUND-RCRD TO IAA-TIAA-FUND-TRANS
        ldaAdsl601.getIaa_Tiaa_Fund_Trans_Bfre_Imge_Id().setValue(" ");                                                                                                   //Natural: ASSIGN IAA-TIAA-FUND-TRANS.BFRE-IMGE-ID := ' '
        ldaAdsl601.getIaa_Tiaa_Fund_Trans_Aftr_Imge_Id().setValue("2");                                                                                                   //Natural: ASSIGN IAA-TIAA-FUND-TRANS.AFTR-IMGE-ID := '2'
        ldaAdsl601.getIaa_Tiaa_Fund_Trans_Lst_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                        //Natural: ASSIGN IAA-TIAA-FUND-TRANS.LST-TRANS-DTE := #TRANS-DTE
        ldaAdsl601.getIaa_Tiaa_Fund_Trans_Trans_Check_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Tiaa_Fund_Trans_Trans_Check_Dte()),                    //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-CHECK-DTE := VAL ( #TRANS-CHECK-DTE )
            pnd_Trans_Check_Dte.val());
        ldaAdsl601.getIaa_Tiaa_Fund_Trans_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                            //Natural: ASSIGN IAA-TIAA-FUND-TRANS.TRANS-DTE := #TRANS-DTE
        ldaAdsl601.getIaa_Tiaa_Fund_Trans_Invrse_Trans_Dte().setValue(pnd_Invrse_Trans_Dte);                                                                              //Natural: ASSIGN IAA-TIAA-FUND-TRANS.INVRSE-TRANS-DTE := #INVRSE-TRANS-DTE
        ldaAdsl601.getVw_iaa_Tiaa_Fund_Trans().insertDBRow();                                                                                                             //Natural: STORE IAA-TIAA-FUND-TRANS
    }
    private void sub_Create_Bfore_Cref_Fund() throws Exception                                                                                                            //Natural: CREATE-BFORE-CREF-FUND
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaAdsl601.getVw_iaa_Cref_Fund_Trans_1().setValuesByName(ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1());                                                                //Natural: MOVE BY NAME IAA-CREF-FUND-RCRD-1 TO IAA-CREF-FUND-TRANS-1
        ldaAdsl601.getIaa_Cref_Fund_Trans_1_Bfre_Imge_Id().setValue("1");                                                                                                 //Natural: ASSIGN IAA-CREF-FUND-TRANS-1.BFRE-IMGE-ID := '1'
        ldaAdsl601.getIaa_Cref_Fund_Trans_1_Aftr_Imge_Id().setValue(" ");                                                                                                 //Natural: ASSIGN IAA-CREF-FUND-TRANS-1.AFTR-IMGE-ID := ' '
        ldaAdsl601.getIaa_Cref_Fund_Trans_1_Trans_Check_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cref_Fund_Trans_1_Trans_Check_Dte()),                //Natural: ASSIGN IAA-CREF-FUND-TRANS-1.TRANS-CHECK-DTE := VAL ( #TRANS-CHECK-DTE )
            pnd_Trans_Check_Dte.val());
        ldaAdsl601.getIaa_Cref_Fund_Trans_1_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                          //Natural: ASSIGN IAA-CREF-FUND-TRANS-1.TRANS-DTE := #TRANS-DTE
        ldaAdsl601.getIaa_Cref_Fund_Trans_1_Invrse_Trans_Dte().setValue(pnd_Invrse_Trans_Dte);                                                                            //Natural: ASSIGN IAA-CREF-FUND-TRANS-1.INVRSE-TRANS-DTE := #INVRSE-TRANS-DTE
        ldaAdsl601.getVw_iaa_Cref_Fund_Trans_1().insertDBRow();                                                                                                           //Natural: STORE IAA-CREF-FUND-TRANS-1
        pnd_Cntrct_Py_Dte_Key_Cntrct_Ppcn_Nbr.setValue(ldaAdsl601.getIaa_Cref_Fund_Trans_1_Cref_Cntrct_Ppcn_Nbr());                                                       //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.CNTRCT-PPCN-NBR := IAA-CREF-FUND-TRANS-1.CREF-CNTRCT-PPCN-NBR
        pnd_Cntrct_Py_Dte_Key_Cntrct_Payee_Cde.setValue(ldaAdsl601.getIaa_Cref_Fund_Trans_1_Cref_Cntrct_Payee_Cde());                                                     //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.CNTRCT-PAYEE-CDE := IAA-CREF-FUND-TRANS-1.CREF-CNTRCT-PAYEE-CDE
        pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde.setValue(ldaAdsl601.getIaa_Cref_Fund_Trans_1_Cref_Cmpny_Fund_Cde());                                                         //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.CMPNY-FUND-CDE := IAA-CREF-FUND-TRANS-1.CREF-CMPNY-FUND-CDE
                                                                                                                                                                          //Natural: PERFORM CREATE-HISTORICAL-RATES
        sub_Create_Historical_Rates();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Create_After_Cref_Fund() throws Exception                                                                                                            //Natural: CREATE-AFTER-CREF-FUND
    {
        if (BLNatReinput.isReinput()) return;

        ldaAdsl601.getIaa_Cref_Fund_Trans_1_Bfre_Imge_Id().setValue(" ");                                                                                                 //Natural: ASSIGN IAA-CREF-FUND-TRANS-1.BFRE-IMGE-ID := ' '
        ldaAdsl601.getIaa_Cref_Fund_Trans_1_Aftr_Imge_Id().setValue("2");                                                                                                 //Natural: ASSIGN IAA-CREF-FUND-TRANS-1.AFTR-IMGE-ID := '2'
        ldaAdsl601.getIaa_Cref_Fund_Trans_1_Lst_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                      //Natural: ASSIGN IAA-CREF-FUND-TRANS-1.LST-TRANS-DTE := #TRANS-DTE
        ldaAdsl601.getIaa_Cref_Fund_Trans_1_Trans_Check_Dte().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Cref_Fund_Trans_1_Trans_Check_Dte()),                //Natural: ASSIGN IAA-CREF-FUND-TRANS-1.TRANS-CHECK-DTE := VAL ( #TRANS-CHECK-DTE )
            pnd_Trans_Check_Dte.val());
        ldaAdsl601.getIaa_Cref_Fund_Trans_1_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                          //Natural: ASSIGN IAA-CREF-FUND-TRANS-1.TRANS-DTE := #TRANS-DTE
        ldaAdsl601.getIaa_Cref_Fund_Trans_1_Invrse_Trans_Dte().setValue(pnd_Invrse_Trans_Dte);                                                                            //Natural: ASSIGN IAA-CREF-FUND-TRANS-1.INVRSE-TRANS-DTE := #INVRSE-TRANS-DTE
        ldaAdsl601.getVw_iaa_Cref_Fund_Trans_1().insertDBRow();                                                                                                           //Natural: STORE IAA-CREF-FUND-TRANS-1
    }
    private void sub_Create_Historical_Rates() throws Exception                                                                                                           //Natural: CREATE-HISTORICAL-RATES
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Py_Dte_Key_Fund_Invrse_Lst_Pd_Dte.compute(new ComputeParameters(false, pnd_Cntrct_Py_Dte_Key_Fund_Invrse_Lst_Pd_Dte), DbsField.subtract(100000000,     //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.FUND-INVRSE-LST-PD-DTE := 100000000 - VAL ( #TRANS-PREV-DTE )
            pnd_Trans_Prev_Dte.val()));
        if (condition(pnd_T.getBoolean()))                                                                                                                                //Natural: IF #T
        {
            vw_iaa_Old_Tiaa_Rates.startDatabaseFind                                                                                                                       //Natural: FIND ( 1 ) IAA-OLD-TIAA-RATES WITH CNTRCT-PY-DTE-KEY = #CNTRCT-PY-DTE-KEY
            (
            "FT",
            new Wc[] { new Wc("CNTRCT_PY_DTE_KEY", "=", pnd_Cntrct_Py_Dte_Key, WcType.WITH) },
            1
            );
            FT:
            while (condition(vw_iaa_Old_Tiaa_Rates.readNextRow("FT", true)))
            {
                vw_iaa_Old_Tiaa_Rates.setIfNotFoundControlFlag(false);
                if (condition(vw_iaa_Old_Tiaa_Rates.getAstCOUNTER().equals(0)))                                                                                           //Natural: IF NO RECORDS FOUND
                {
                    vw_iaa_Old_Tiaa_Rates.setValuesByName(ldaAdsl601.getVw_iaa_Tiaa_Fund_Trans());                                                                        //Natural: MOVE BY NAME IAA-TIAA-FUND-TRANS TO IAA-OLD-TIAA-RATES
                    vw_iaa_Old_Tiaa_Rates.setValuesByName(pnd_Cntrct_Py_Dte_Key_Hist_Structure);                                                                          //Natural: MOVE BY NAME HIST-STRUCTURE TO IAA-OLD-TIAA-RATES
                    iaa_Old_Tiaa_Rates_Fund_Lst_Pd_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Trans_Prev_Dte);                                                 //Natural: MOVE EDITED #TRANS-PREV-DTE TO IAA-OLD-TIAA-RATES.FUND-LST-PD-DTE ( EM = YYYYMMDD )
                    iaa_Old_Tiaa_Rates_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                 //Natural: ASSIGN IAA-OLD-TIAA-RATES.TRANS-DTE := #TRANS-DTE
                    iaa_Old_Tiaa_Rates_Lst_Trans_Dte.setValue(iaa_Old_Tiaa_Rates_Trans_Dte);                                                                              //Natural: ASSIGN IAA-OLD-TIAA-RATES.LST-TRANS-DTE := IAA-OLD-TIAA-RATES.TRANS-DTE
                    iaa_Old_Tiaa_Rates_Cntrct_Mode_Ind.setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Pymnt_Mode());                                                              //Natural: ASSIGN IAA-OLD-TIAA-RATES.CNTRCT-MODE-IND := ADS-IA-RSLT.ADI-PYMNT-MODE
                    iaa_Old_Tiaa_Rates_Rcrd_Srce.setValue("FP");                                                                                                          //Natural: ASSIGN IAA-OLD-TIAA-RATES.RCRD-SRCE := 'FP'
                    iaa_Old_Tiaa_Rates_Rcrd_Status.setValue("A");                                                                                                         //Natural: ASSIGN IAA-OLD-TIAA-RATES.RCRD-STATUS := 'A'
                    iaa_Old_Tiaa_Rates_Cntrct_Tot_Per_Amt.setValue(ldaAdsl601.getIaa_Tiaa_Fund_Trans_Tiaa_Tot_Per_Amt());                                                 //Natural: ASSIGN IAA-OLD-TIAA-RATES.CNTRCT-TOT-PER-AMT := IAA-TIAA-FUND-TRANS.TIAA-TOT-PER-AMT
                    iaa_Old_Tiaa_Rates_Cntrct_Tot_Div_Amt.compute(new ComputeParameters(false, iaa_Old_Tiaa_Rates_Cntrct_Tot_Div_Amt), DbsField.add(getZero(),            //Natural: ASSIGN IAA-OLD-TIAA-RATES.CNTRCT-TOT-DIV-AMT := 0 + IAA-TIAA-FUND-TRANS.TIAA-TOT-DIV-AMT
                        ldaAdsl601.getIaa_Tiaa_Fund_Trans_Tiaa_Tot_Div_Amt()));
                    vw_iaa_Old_Tiaa_Rates.insertDBRow();                                                                                                                  //Natural: STORE IAA-OLD-TIAA-RATES
                }                                                                                                                                                         //Natural: END-NOREC
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_C.reset();                                                                                                                                                //Natural: RESET #C
            vw_iaa_Old_Cref_Rates.startDatabaseFind                                                                                                                       //Natural: FIND ( 1 ) IAA-OLD-CREF-RATES WITH CNTRCT-PY-DTE-KEY = #CNTRCT-PY-DTE-KEY
            (
            "FC",
            new Wc[] { new Wc("CNTRCT_PY_DTE_KEY", "=", pnd_Cntrct_Py_Dte_Key, WcType.WITH) },
            1
            );
            FC:
            while (condition(vw_iaa_Old_Cref_Rates.readNextRow("FC", true)))
            {
                vw_iaa_Old_Cref_Rates.setIfNotFoundControlFlag(false);
                if (condition(vw_iaa_Old_Cref_Rates.getAstCOUNTER().equals(0)))                                                                                           //Natural: IF NO RECORDS FOUND
                {
                    vw_iaa_Old_Cref_Rates.setValuesByName(ldaAdsl601.getVw_iaa_Cref_Fund_Trans_1());                                                                      //Natural: MOVE BY NAME IAA-CREF-FUND-TRANS-1 TO IAA-OLD-CREF-RATES
                    vw_iaa_Old_Cref_Rates.setValuesByName(pnd_Cntrct_Py_Dte_Key_Hist_Structure);                                                                          //Natural: MOVE BY NAME HIST-STRUCTURE TO IAA-OLD-CREF-RATES
                    //*  022018
                    //*  022018
                    iaa_Old_Cref_Rates_Fund_Lst_Pd_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Trans_Prev_Dte);                                                 //Natural: MOVE EDITED #TRANS-PREV-DTE TO IAA-OLD-CREF-RATES.FUND-LST-PD-DTE ( EM = YYYYMMDD )
                    iaa_Old_Cref_Rates_Trans_Dte.setValue(pnd_Trans_Dte);                                                                                                 //Natural: ASSIGN IAA-OLD-CREF-RATES.TRANS-DTE := #TRANS-DTE
                    iaa_Old_Cref_Rates_Lst_Trans_Dte.setValue(iaa_Old_Cref_Rates_Trans_Dte);                                                                              //Natural: ASSIGN IAA-OLD-CREF-RATES.LST-TRANS-DTE := IAA-OLD-CREF-RATES.TRANS-DTE
                    iaa_Old_Cref_Rates_Cntrct_Mode_Ind.setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Pymnt_Mode());                                                              //Natural: ASSIGN IAA-OLD-CREF-RATES.CNTRCT-MODE-IND := ADS-IA-RSLT.ADI-PYMNT-MODE
                    iaa_Old_Cref_Rates_Rcrd_Srce.setValue("FP");                                                                                                          //Natural: ASSIGN IAA-OLD-CREF-RATES.RCRD-SRCE := 'FP'
                    iaa_Old_Cref_Rates_Rcrd_Status.setValue("A");                                                                                                         //Natural: ASSIGN IAA-OLD-CREF-RATES.RCRD-STATUS := 'A'
                    iaa_Old_Cref_Rates_Cntrct_Tot_Per_Amt.setValue(ldaAdsl601.getIaa_Cref_Fund_Trans_1_Cref_Tot_Per_Amt());                                               //Natural: ASSIGN IAA-OLD-CREF-RATES.CNTRCT-TOT-PER-AMT := IAA-CREF-FUND-TRANS-1.CREF-TOT-PER-AMT
                    iaa_Old_Cref_Rates_Cntrct_Tot_Units.setValue(ldaAdsl601.getIaa_Cref_Fund_Trans_1_Cref_Units_Cnt().getValue(1));                                       //Natural: ASSIGN IAA-OLD-CREF-RATES.CNTRCT-TOT-UNITS := IAA-CREF-FUND-TRANS-1.CREF-UNITS-CNT ( 1 )
                    iaa_Old_Cref_Rates_Cref_No_Units.getValue(1).setValue(ldaAdsl601.getIaa_Cref_Fund_Trans_1_Cref_Units_Cnt().getValue(1));                              //Natural: ASSIGN IAA-OLD-CREF-RATES.CREF-NO-UNITS ( 1 ) := IAA-CREF-FUND-TRANS-1.CREF-UNITS-CNT ( 1 )
                    iaa_Old_Cref_Rates_Cntrct_Unit_Val.setValue(ldaAdsl601.getIaa_Cref_Fund_Trans_1_Cref_Unit_Val());                                                     //Natural: ASSIGN IAA-OLD-CREF-RATES.CNTRCT-UNIT-VAL := IAA-CREF-FUND-TRANS-1.CREF-UNIT-VAL
                    vw_iaa_Old_Cref_Rates.insertDBRow();                                                                                                                  //Natural: STORE IAA-OLD-CREF-RATES
                }                                                                                                                                                         //Natural: END-NOREC
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_Monthly_Cref_Fund_Rcrd() throws Exception                                                                                                     //Natural: CREATE-MONTHLY-CREF-FUND-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde().reset();                                                                                                       //Natural: RESET IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE IAA-CREF-FUND-RCRD-1.CREF-TOT-PER-AMT IAA-CREF-FUND-RCRD-1.CREF-UNIT-VAL
        ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt().reset();
        ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Unit_Val().reset();
        ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Payee_Cde().setValue(1);                                                                                           //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CNTRCT-PAYEE-CDE := 01
        DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_I), ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde(),  //Natural: CALLNAT 'IAAN0511' ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #I ) IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE #RC
            pnd_Rc);
        if (condition(Global.isEscape())) return;
        ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde().getValue(1).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd().getValue(pnd_I));                     //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-RATE-CDE ( 1 ) := ADS-IA-RSLT.ADI-DTL-CREF-IA-RATE-CD ( #I )
        ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt().getValue(1).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_I));              //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-UNITS-CNT ( 1 ) := ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #I )
        //*  EM - 012309
        if (condition(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde().equals("09") || ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde().equals("11")))                //Natural: IF IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE = '09' OR = '11'
        {
            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde().setValue("W");                                                                                            //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CMPNY-CDE := 'W'
            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr().setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Ia_Tiaa_Nbr());                                              //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CNTRCT-PPCN-NBR := ADS-IA-RSLT.ADI-IA-TIAA-NBR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde().setValue("4");                                                                                            //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CMPNY-CDE := '4'
            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr().setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Ia_Cref_Nbr());                                              //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CNTRCT-PPCN-NBR := ADS-IA-RSLT.ADI-IA-CREF-NBR
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Lst_Trans_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Lst_Trans_Dte());                                                            //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.LST-TRANS-DTE := IAA-CNTRCT.LST-TRANS-DTE
        ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Rate_Dte().getValue("*").reset();                                                                                         //Natural: RESET IAA-CREF-FUND-RCRD-1.CREF-RATE-DTE ( * )
        ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1().insertDBRow();                                                                                                            //Natural: STORE IAA-CREF-FUND-RCRD-1
        ldaAdsl601.getVw_iaa_Cref_Fund_Trans_1().setValuesByName(ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1());                                                                //Natural: MOVE BY NAME IAA-CREF-FUND-RCRD-1 TO IAA-CREF-FUND-TRANS-1
                                                                                                                                                                          //Natural: PERFORM CREATE-AFTER-CREF-FUND
        sub_Create_After_Cref_Fund();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Create_Annual_Cref_Fund_Rcrd() throws Exception                                                                                                      //Natural: CREATE-ANNUAL-CREF-FUND-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde().reset();                                                                                                       //Natural: RESET IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE
        ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Payee_Cde().setValue(1);                                                                                           //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CNTRCT-PAYEE-CDE := 01
        DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_I), ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde(),  //Natural: CALLNAT 'IAAN0511' ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #I ) IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE #RC
            pnd_Rc);
        if (condition(Global.isEscape())) return;
        ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde().getValue(1).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd().getValue(pnd_I));                     //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-RATE-CDE ( 1 ) := ADS-IA-RSLT.ADI-DTL-CREF-IA-RATE-CD ( #I )
        ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt().getValue(1).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_I));                //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-UNITS-CNT ( 1 ) := ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #I )
        ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt().setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_I));                             //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-TOT-PER-AMT := ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #I )
        ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Unit_Val().setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val().getValue(pnd_I));                           //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-UNIT-VAL := ADS-IA-RSLT.ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #I )
        //*  EM - 012309
        if (condition(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde().equals("09") || ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde().equals("11")))                //Natural: IF IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE = '09' OR = '11'
        {
            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde().setValue("U");                                                                                            //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CMPNY-CDE := 'U'
            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr().setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Ia_Tiaa_Nbr());                                              //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CNTRCT-PPCN-NBR := ADS-IA-RSLT.ADI-IA-TIAA-NBR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde().setValue("2");                                                                                            //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CMPNY-CDE := '2'
            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr().setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Ia_Cref_Nbr());                                              //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-CNTRCT-PPCN-NBR := ADS-IA-RSLT.ADI-IA-CREF-NBR
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Lst_Trans_Dte().setValue(ldaAdsl601.getIaa_Cntrct_Lst_Trans_Dte());                                                            //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.LST-TRANS-DTE := IAA-CNTRCT.LST-TRANS-DTE
        ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Rate_Dte().getValue("*").reset();                                                                                         //Natural: RESET IAA-CREF-FUND-RCRD-1.CREF-RATE-DTE ( * )
        ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1().insertDBRow();                                                                                                            //Natural: STORE IAA-CREF-FUND-RCRD-1
        ldaAdsl601.getVw_iaa_Cref_Fund_Trans_1().setValuesByName(ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1());                                                                //Natural: MOVE BY NAME IAA-CREF-FUND-RCRD-1 TO IAA-CREF-FUND-TRANS-1
                                                                                                                                                                          //Natural: PERFORM CREATE-AFTER-CREF-FUND
        sub_Create_After_Cref_Fund();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Create_Cref_If_None_Exists() throws Exception                                                                                                        //Natural: CREATE-CREF-IF-NONE-EXISTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_cref_Fund_Rcrd.startDatabaseFind                                                                                                                               //Natural: FIND CREF-FUND-RCRD WITH CREF-CNTRCT-FUND-KEY = #CREF-FUND-KEY
        (
        "FN1",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", "=", pnd_Cref_Fund_Key, WcType.WITH) }
        );
        FN1:
        while (condition(vw_cref_Fund_Rcrd.readNextRow("FN1", true)))
        {
            vw_cref_Fund_Rcrd.setIfNotFoundControlFlag(false);
            if (condition(vw_cref_Fund_Rcrd.getAstCOUNTER().equals(0)))                                                                                                   //Natural: IF NO RECORDS FOUND
            {
                vw_cref_Fund_Rcrd.setValuesByName(ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1());                                                                               //Natural: MOVE BY NAME IAA-CREF-FUND-RCRD-1 TO CREF-FUND-RCRD
                cref_Fund_Rcrd_Cref_Old_Per_Amt.reset();                                                                                                                  //Natural: RESET CREF-FUND-RCRD.CREF-OLD-PER-AMT CREF-FUND-RCRD.CREF-OLD-UNIT-VAL CREF-FUND-RCRD.CREF-UNIT-VAL CREF-FUND-RCRD.CREF-RATE-DTE ( * ) CREF-FUND-RCRD.CREF-TOT-PER-AMT
                cref_Fund_Rcrd_Cref_Old_Unit_Val.reset();
                cref_Fund_Rcrd_Cref_Unit_Val.reset();
                cref_Fund_Rcrd_Cref_Rate_Dte.getValue("*").reset();
                cref_Fund_Rcrd_Cref_Tot_Per_Amt.reset();
                cref_Fund_Rcrd_Cref_Cmpny_Cde.setValue(pnd_Cref_Fund_Key_Pnd_Cmpny_Cde);                                                                                  //Natural: ASSIGN CREF-FUND-RCRD.CREF-CMPNY-CDE := #CMPNY-CDE
                cref_Fund_Rcrd_Lst_Trans_Dte.setValue(ldaAdsl601.getIaa_Cntrct_Lst_Trans_Dte());                                                                          //Natural: ASSIGN CREF-FUND-RCRD.LST-TRANS-DTE := IAA-CNTRCT.LST-TRANS-DTE
                cref_Fund_Rcrd_Cref_Rate_Cde.getValue(1).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd().getValue(pnd_J));                                   //Natural: ASSIGN CREF-FUND-RCRD.CREF-RATE-CDE ( 1 ) := ADS-IA-RSLT.ADI-DTL-CREF-IA-RATE-CD ( #J )
                if (condition(cref_Fund_Rcrd_Cref_Cmpny_Cde.equals("2") || cref_Fund_Rcrd_Cref_Cmpny_Cde.equals("U")))                                                    //Natural: IF CREF-FUND-RCRD.CREF-CMPNY-CDE = '2' OR = 'U'
                {
                    cref_Fund_Rcrd_Cref_Units_Cnt.getValue(1).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_J));                          //Natural: ASSIGN CREF-FUND-RCRD.CREF-UNITS-CNT ( 1 ) := ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #J )
                    cref_Fund_Rcrd_Cref_Tot_Per_Amt.setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_J));                                       //Natural: ASSIGN CREF-FUND-RCRD.CREF-TOT-PER-AMT := ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #J )
                    cref_Fund_Rcrd_Cref_Unit_Val.setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val().getValue(pnd_J));                                     //Natural: ASSIGN CREF-FUND-RCRD.CREF-UNIT-VAL := ADS-IA-RSLT.ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #J )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    cref_Fund_Rcrd_Cref_Units_Cnt.getValue(1).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_J));                        //Natural: ASSIGN CREF-FUND-RCRD.CREF-UNITS-CNT ( 1 ) := ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #J )
                }                                                                                                                                                         //Natural: END-IF
                PND_PND_L3472:                                                                                                                                            //Natural: STORE CREF-FUND-RCRD
                vw_cref_Fund_Rcrd.insertDBRow("PND_PND_L3472");
                pnd_I_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #I-CNT
                pnd_New_Isns.getValue(pnd_I_Cnt).setValue(vw_cref_Fund_Rcrd.getAstISN("FN1"));                                                                            //Natural: ASSIGN #NEW-ISNS ( #I-CNT ) := *ISN ( ##L3472. )
                ldaAdsl601.getVw_iaa_Cref_Fund_Trans_1().setValuesByName(vw_cref_Fund_Rcrd);                                                                              //Natural: MOVE BY NAME CREF-FUND-RCRD TO IAA-CREF-FUND-TRANS-1
                                                                                                                                                                          //Natural: PERFORM CREATE-AFTER-CREF-FUND
                sub_Create_After_Cref_Fund();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FN1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FN1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Get_Tiaa_Fund() throws Exception                                                                                                                     //Natural: GET-TIAA-FUND
    {
        if (BLNatReinput.isReinput()) return;

        ldaAdsl601.getVw_iaa_Tiaa_Fund_Rcrd().startDatabaseRead                                                                                                           //Natural: READ IAA-TIAA-FUND-RCRD BY TIAA-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R2",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R2:
        while (condition(ldaAdsl601.getVw_iaa_Tiaa_Fund_Rcrd().readNextRow("R2")))
        {
            if (condition(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Ppcn_Nbr().notEquals(pnd_Cntrct_Fund_Key_Pnd_Cntrct_Ppcn_Nbr) || ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Cntrct_Payee_Cde().notEquals(pnd_Cntrct_Fund_Key_Pnd_Cntrct_Payee_Cde))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PPCN-NBR NE #CNTRCT-PPCN-NBR OR IAA-TIAA-FUND-RCRD.TIAA-CNTRCT-PAYEE-CDE NE #CNTRCT-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde().equals("1") || ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde().equals("1S")                 //Natural: ACCEPT IF IAA-TIAA-FUND-RCRD.TIAA-FUND-CDE = '1' OR = '1S' OR = '1G'
                || ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde().equals("1G"))))
            {
                continue;
            }
            pnd_T.setValue(true);                                                                                                                                         //Natural: ASSIGN #T := TRUE
                                                                                                                                                                          //Natural: PERFORM CREATE-BFORE-TIAA-FUND
            sub_Create_Bfore_Tiaa_Fund();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition((ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().equals(9) && (ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn().equals("NI") //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE = 9 AND IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-TRMNTE-RSN = 'NI' OR = 'TR'
                || ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn().equals("TR")))))
            {
                pnd_Inactive_Tiaa_Isn.setValue(ldaAdsl601.getVw_iaa_Tiaa_Fund_Rcrd().getAstISN("R2"));                                                                    //Natural: ASSIGN #INACTIVE-TIAA-ISN := *ISN ( R2. )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            G2:                                                                                                                                                           //Natural: GET IAA-TIAA-FUND-RCRD *ISN ( R2. )
            ldaAdsl601.getVw_iaa_Tiaa_Fund_Rcrd().readByID(ldaAdsl601.getVw_iaa_Tiaa_Fund_Rcrd().getAstISN("R2"), "G2");
            if (condition(pnd_First.getBoolean()))                                                                                                                        //Natural: IF #FIRST
            {
                pnd_Rate_Xx.setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp());                                                                    //Natural: ASSIGN #RATE-XX := IAA-TIAA-FUND-RCRD.C*TIAA-RATE-DATA-GRP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Rate_No.setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp());                                                                        //Natural: ASSIGN #RATE-NO := IAA-TIAA-FUND-RCRD.C*TIAA-RATE-DATA-GRP
            short decideConditionsMet2326 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE IAA-TIAA-FUND-RCRD.TIAA-FUND-CDE;//Natural: VALUE '1','1S'
            if (condition((ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde().equals("1") || ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde().equals("1S"))))
            {
                decideConditionsMet2326++;
                if (condition(pnd_First.getBoolean()))                                                                                                                    //Natural: IF #FIRST
                {
                    pnd_First.setValue(false);                                                                                                                            //Natural: ASSIGN #FIRST := FALSE
                    updt_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(1,                //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( 1:#MAX-ADI )
                        ":",pnd_Max_Adi));
                    updt_Ia_Rslt_Adi_Contract_Id.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(1,":",                    //Natural: ASSIGN UPDT-IA-RSLT.ADI-CONTRACT-ID ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( 1:#MAX-ADI )
                        pnd_Max_Adi));
                    updt_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(1,          //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( 1:#MAX-ADI )
                        ":",pnd_Max_Adi));
                    updt_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(1,          //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( 1:#MAX-ADI )
                        ":",pnd_Max_Adi));
                    updt_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(1,      //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( 1:#MAX-ADI )
                        ":",pnd_Max_Adi));
                    updt_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(1,    //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( 1:#MAX-ADI )
                        ":",pnd_Max_Adi));
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_Max_Adi.getDec().add(1), //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #MAX-ADI + 1:#MAX-RTE )
                            ":",pnd_Max_Rte));
                        updt_Ia_Rsl2_Adi_Contract_Id.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(pnd_Max_Adi.getDec().add(1), //Natural: ASSIGN UPDT-IA-RSL2.ADI-CONTRACT-ID ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( #MAX-ADI + 1:#MAX-RTE )
                            ":",pnd_Max_Rte));
                        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_Max_Adi.getDec().add(1), //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #MAX-ADI + 1:#MAX-RTE )
                            ":",pnd_Max_Rte));
                        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_Max_Adi.getDec().add(1), //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #MAX-ADI + 1:#MAX-RTE )
                            ":",pnd_Max_Rte));
                        updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_Max_Adi.getDec().add(1), //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #MAX-ADI + 1:#MAX-RTE )
                            ":",pnd_Max_Rte));
                        updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_Max_Adi.getDec().add(1), //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #MAX-ADI + 1:#MAX-RTE )
                            ":",pnd_Max_Rte));
                    }                                                                                                                                                     //Natural: END-IF
                    //*  030812 END
                    //*  030812 START
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    FOR09:                                                                                                                                                //Natural: FOR #I 1 TO #MAX-RTE
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Rte)); pnd_I.nadd(1))
                    {
                        if (condition(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I).equals(" ")))                                                      //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I ) = ' '
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                            //*  030812 END
                        }                                                                                                                                                 //Natural: END-IF
                        //*  030812
                        if (condition(updt_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue("*").equals(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I))))       //Natural: IF UPDT-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( * ) = IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I )
                        {
                            FOR10:                                                                                                                                        //Natural: FOR #J 1 TO #MAX-ADI
                            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Max_Adi)); pnd_J.nadd(1))
                            {
                                if (condition(updt_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_J).equals(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I)))) //Natural: IF UPDT-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #J ) = IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I )
                                {
                                    updt_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_J).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J ) := IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #I )
                                    updt_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_J).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #J ) := IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #I )
                                    updt_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_J).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( #J ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #I )
                                    updt_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_J).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( #J ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #I )
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  CHECK FIRST IF THERE IS A SECOND RESULT RECORD BECAUSE   /* 030812
                            //*  IF THERE IS, NO MORE SLOT IS AVAILABLE TO INSERT THE NEW RATE
                            //* *          ADD 1 TO #RATE-XX
                            //*  030812 START
                            if (condition(pnd_More.getBoolean()))                                                                                                         //Natural: IF #MORE
                            {
                                ignore();
                                //*  030812 END
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                FOR11:                                                                                                                                    //Natural: FOR #RATE-XX 1 #MAX-ADI
                                for (pnd_Rate_Xx.setValue(1); condition(pnd_Rate_Xx.lessOrEqual(pnd_Max_Adi)); pnd_Rate_Xx.nadd(1))
                                {
                                    //*  030812
                                    if (condition(updt_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_Rate_Xx).equals(" ")))                                                //Natural: IF UPDT-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #RATE-XX ) = ' '
                                    {
                                        updt_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I )
                                        updt_Ia_Rslt_Adi_Contract_Id.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(pnd_I));    //Natural: ASSIGN UPDT-IA-RSLT.ADI-CONTRACT-ID ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( #I )
                                        updt_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #I )
                                        updt_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #I )
                                        updt_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #I )
                                        updt_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #I )
                                        if (condition(true)) break;                                                                                                       //Natural: ESCAPE BOTTOM
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-FOR
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  030812 START
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        FOR12:                                                                                                                                            //Natural: FOR #I 1 TO #MAX-RTE
                        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Rte)); pnd_I.nadd(1))
                        {
                            if (condition(updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue("*").equals(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I))))   //Natural: IF UPDT-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( * ) = IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I )
                            {
                                FOR13:                                                                                                                                    //Natural: FOR #J 1 TO #MAX-ADI
                                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Max_Adi)); pnd_J.nadd(1))
                                {
                                    if (condition(updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_J).equals(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I)))) //Natural: IF UPDT-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #J ) = IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I )
                                    {
                                        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_J).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J ) := IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #I )
                                        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_J).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #J ) := IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #I )
                                        updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_J).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( #J ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #I )
                                        updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_J).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( #J ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #I )
                                        if (condition(true)) break;                                                                                                       //Natural: ESCAPE BOTTOM
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-FOR
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                //*  RATE ALREADY IN FIRST RESULT RECORD
                                if (condition(updt_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue("*").equals(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I)))) //Natural: IF UPDT-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( * ) = IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I )
                                {
                                    ignore();
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    FOR14:                                                                                                                                //Natural: FOR #RATE-XX 1 #MAX-ADI
                                    for (pnd_Rate_Xx.setValue(1); condition(pnd_Rate_Xx.lessOrEqual(pnd_Max_Adi)); pnd_Rate_Xx.nadd(1))
                                    {
                                        if (condition(updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_Rate_Xx).equals(" ")))                                            //Natural: IF UPDT-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #RATE-XX ) = ' '
                                        {
                                            updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I )
                                            updt_Ia_Rsl2_Adi_Contract_Id.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSL2.ADI-CONTRACT-ID ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( #I )
                                            updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #I )
                                            updt_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #I )
                                            updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #I )
                                            updt_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #I )
                                            if (condition(true)) break;                                                                                                   //Natural: ESCAPE BOTTOM
                                        }                                                                                                                                 //Natural: END-IF
                                    }                                                                                                                                     //Natural: END-FOR
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R2"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    updt_Ia_Rslt_Adi_Pymnt_Mode.setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind());                                                               //Natural: ASSIGN UPDT-IA-RSLT.ADI-PYMNT-MODE := IAA-TIAA-FUND-RCRD.TIAA-MODE-IND
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        updt_Ia_Rsl2_Adi_Pymnt_Mode.setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind());                                                           //Natural: ASSIGN UPDT-IA-RSL2.ADI-PYMNT-MODE := IAA-TIAA-FUND-RCRD.TIAA-MODE-IND
                        //*  030812 END
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue("*").greater(getZero())))                                                   //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( * ) GT 0
                {
                    pnd_Cntrl_Per_Pay_Amt.nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue("*"));                                                     //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( * ) TO #CNTRL-PER-PAY-AMT
                    //*  030812 START
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        pnd_Cntrl_Per_Pay_Amt.nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue("*"));                                                                 //Natural: ASSIGN #CNTRL-PER-PAY-AMT := #CNTRL-PER-PAY-AMT + ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( * )
                        //*  030812 END
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt()),        //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-OLD-PER-AMT := 0 + IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( * )
                        DbsField.add(getZero(),ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue("*")));
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue("*").greater(getZero())))                                                   //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( * ) GT 0
                {
                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt()),        //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-OLD-DIV-AMT := 0 + IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( * )
                        DbsField.add(getZero(),ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue("*")));
                    pnd_Cntrl_Per_Div_Amt.nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue("*"));                                                     //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( * ) TO #CNTRL-PER-DIV-AMT
                    //*  030812 START
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        pnd_Cntrl_Per_Div_Amt.nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue("*"));                                                                 //Natural: ASSIGN #CNTRL-PER-DIV-AMT := #CNTRL-PER-DIV-AMT + ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( * )
                        //*  030812 END
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt().getValue("*").greater(getZero())))                                                      //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( * ) GT 0
                {
                    pnd_Cntrl_Final_Pay_Amt.nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt().getValue("*"));                                                      //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( * ) TO #CNTRL-FINAL-PAY-AMT
                    //*  030812 START
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        pnd_Cntrl_Final_Pay_Amt.nadd(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue("*"));                                                                  //Natural: ASSIGN #CNTRL-FINAL-PAY-AMT := #CNTRL-FINAL-PAY-AMT + ADS-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( * )
                        //*  030812 END
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue("*").greater(getZero())))                                                    //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( * ) GT 0
                {
                    pnd_Cntrl_Final_Div_Amt.nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue("*"));                                                    //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( * ) TO #CNTRL-FINAL-DIV-AMT
                    //*  030812 START
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        pnd_Cntrl_Final_Div_Amt.nadd(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue("*"));                                                                //Natural: ASSIGN #CNTRL-FINAL-DIV-AMT := #CNTRL-FINAL-DIV-AMT + ADS-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( * )
                        //*  030812 END
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                FOR15:                                                                                                                                                    //Natural: FOR #I 1 TO IAA-TIAA-FUND-RCRD.C*TIAA-RATE-DATA-GRP
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp())); pnd_I.nadd(1))
                {
                    if (condition(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I).equals(" ")))                                                          //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                        //*  030812
                    }                                                                                                                                                     //Natural: END-IF
                    FOR16:                                                                                                                                                //Natural: FOR #J 1 TO #MAX-ADI
                    for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Max_Adi)); pnd_J.nadd(1))
                    {
                        if (condition(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I).equals(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_J)))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I ) = ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #J )
                        {
                            //*  030812
                            //*  CALL THE ACTUARIAL INTERPOLAT-
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_J).greater(getZero()) || ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_J).greater(getZero()))) //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J ) GT 0 OR ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #J ) GT 0
                            {
                                pnd_Rslt.setValue(true);                                                                                                                  //Natural: ASSIGN #RSLT := TRUE
                                pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Grd_Std().setValue("S");                                                                    //Natural: ASSIGN #AIAN091-GRD-STD := 'S'
                                //*  ION MODULE.
                                                                                                                                                                          //Natural: PERFORM GET-RATE-DATE
                                sub_Get_Rate_Date();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(pnd_Err_Cde.notEquals(" ")))                                                                                                //Natural: IF #ERR-CDE NE ' '
                                {
                                    if (true) break R2;                                                                                                                   //Natural: ESCAPE BOTTOM ( R2. )
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_J).greater(getZero())))                                     //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J ) GT 0
                            {
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_J)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #I )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_J).greater(getZero())))                                     //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #J ) GT 0
                            {
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_J)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #I )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_J).greater(getZero())))                                        //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( #J ) GT 0
                            {
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_I).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_J)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #I )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_J).greater(getZero())))                                      //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( #J ) GT 0
                            {
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_I).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_J)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #I )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_J).equals(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue("*")))) //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #J ) = IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( * )
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_J).greater(getZero()) || ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_J).greater(getZero()))) //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J ) GT 0 OR ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #J ) GT 0
                            {
                                //*  030812
                                pnd_Rate_No.nadd(1);                                                                                                                      //Natural: ADD 1 TO #RATE-NO
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_Rate_No).setValue(pnd_Effct_Dte);                                           //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( #RATE-NO ) := #EFFCT-DTE
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_Rate_No).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #RATE-NO ) := ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #J )
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(pnd_Rate_No).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Contract_Id().getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( #RATE-NO ) := ADS-IA-RSLT.ADI-CONTRACT-ID ( #J )
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_Rate_No).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #RATE-NO ) := ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J )
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_Rate_No).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #RATE-NO ) := ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #J )
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_Rate_No).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #RATE-NO ) := ADS-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( #J )
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_Rate_No).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #RATE-NO ) := ADS-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( #J )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  030812 START
                if (condition(pnd_More.getBoolean()))                                                                                                                     //Natural: IF #MORE
                {
                    FOR17:                                                                                                                                                //Natural: FOR #I 1 TO IAA-TIAA-FUND-RCRD.C*TIAA-RATE-DATA-GRP
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp())); pnd_I.nadd(1))
                    {
                        if (condition(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I).equals(" ")))                                                      //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I ) = ' '
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        FOR18:                                                                                                                                            //Natural: FOR #J 1 TO #MAX-ADI
                        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Max_Adi)); pnd_J.nadd(1))
                        {
                            if (condition(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I).equals(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_J))))  //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I ) = ADS-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #J )
                            {
                                //*  CALL THE ACTUARIAL INTERPOLAT-
                                if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_J).greater(getZero()) || ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_J).greater(getZero()))) //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J ) GT 0 OR ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #J ) GT 0
                                {
                                    pnd_Rslt.setValue(false);                                                                                                             //Natural: ASSIGN #RSLT := FALSE
                                    pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Grd_Std().setValue("S");                                                                //Natural: ASSIGN #AIAN091-GRD-STD := 'S'
                                    //*  ION MODULE.
                                                                                                                                                                          //Natural: PERFORM GET-RATE-DATE
                                    sub_Get_Rate_Date();
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                    if (condition(pnd_Err_Cde.notEquals(" ")))                                                                                            //Natural: IF #ERR-CDE NE ' '
                                    {
                                        if (true) break R2;                                                                                                               //Natural: ESCAPE BOTTOM ( R2. )
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-IF
                                if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_J).greater(getZero())))                                                 //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J ) GT 0
                                {
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I).nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_J));     //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #I )
                                }                                                                                                                                         //Natural: END-IF
                                if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_J).greater(getZero())))                                                 //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #J ) GT 0
                                {
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I).nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_J));     //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #I )
                                }                                                                                                                                         //Natural: END-IF
                                if (condition(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_J).greater(getZero())))                                                    //Natural: IF ADS-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( #J ) GT 0
                                {
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_I).nadd(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_J)); //Natural: ADD ADS-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #I )
                                }                                                                                                                                         //Natural: END-IF
                                if (condition(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_J).greater(getZero())))                                                  //Natural: IF ADS-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( #J ) GT 0
                                {
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_I).nadd(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_J)); //Natural: ADD ADS-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #I )
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_J).equals(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue("*"))))    //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #J ) = IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( * )
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_J).greater(getZero()) || ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_J).greater(getZero()))) //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J ) GT 0 OR ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #J ) GT 0
                                {
                                    //*  030812
                                    pnd_Rate_No.nadd(1);                                                                                                                  //Natural: ADD 1 TO #RATE-NO
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_Rate_No).setValue(pnd_Effct_Dte);                                       //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( #RATE-NO ) := #EFFCT-DTE
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_Rate_No).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #RATE-NO ) := ADS-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #J )
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(pnd_Rate_No).setValue(ads_Ia_Rsl2_Adi_Contract_Id.getValue(pnd_J));         //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( #RATE-NO ) := ADS-IA-RSL2.ADI-CONTRACT-ID ( #J )
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_Rate_No).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #RATE-NO ) := ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J )
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_Rate_No).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #RATE-NO ) := ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #J )
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_Rate_No).setValue(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #RATE-NO ) := ADS-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( #J )
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_Rate_No).setValue(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #RATE-NO ) := ADS-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( #J )
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  030812 END
                    //*  030812
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind().setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Pymnt_Mode());                                                    //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-MODE-IND := ADS-IA-RSLT.ADI-PYMNT-MODE
                if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue("*").greater(getZero())))                                                   //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( * ) GT 0
                {
                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt().nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue("*"));                       //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( * ) TO IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue("*").greater(getZero())))                                                   //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( * ) GT 0
                {
                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt().nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue("*"));                       //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( * ) TO IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT
                }                                                                                                                                                         //Natural: END-IF
                //*  030812 START
                if (condition(pnd_More.getBoolean()))                                                                                                                     //Natural: IF #MORE
                {
                    if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue("*").greater(getZero())))                                                               //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( * ) GT 0
                    {
                        ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue("*"));                                   //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( * ) TO IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue("*").greater(getZero())))                                                               //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( * ) GT 0
                    {
                        ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue("*"));                                   //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( * ) TO IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT
                    }                                                                                                                                                     //Natural: END-IF
                    //*  030812 END
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE '1G'
            else if (condition((ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Fund_Cde().equals("1G"))))
            {
                decideConditionsMet2326++;
                if (condition(pnd_First.getBoolean()))                                                                                                                    //Natural: IF #FIRST
                {
                    pnd_First.setValue(false);                                                                                                                            //Natural: ASSIGN #FIRST := FALSE
                    updt_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(1,                //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( 1:#MAX-ADI )
                        ":",pnd_Max_Adi));
                    updt_Ia_Rslt_Adi_Contract_Id.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(1,":",                    //Natural: ASSIGN UPDT-IA-RSLT.ADI-CONTRACT-ID ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( 1:#MAX-ADI )
                        pnd_Max_Adi));
                    updt_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(1,          //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( 1:#MAX-ADI )
                        ":",pnd_Max_Adi));
                    updt_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(1,          //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( 1:#MAX-ADI )
                        ":",pnd_Max_Adi));
                    updt_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(1,      //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( 1:#MAX-ADI )
                        ":",pnd_Max_Adi));
                    updt_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(1,    //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( 1:#MAX-ADI )
                        ":",pnd_Max_Adi));
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_Max_Adi.getDec().add(1), //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #MAX-ADI + 1:#MAX-RTE )
                            ":",pnd_Max_Rte));
                        updt_Ia_Rsl2_Adi_Contract_Id.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(pnd_Max_Adi.getDec().add(1), //Natural: ASSIGN UPDT-IA-RSL2.ADI-CONTRACT-ID ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( #MAX-ADI + 1:#MAX-RTE )
                            ":",pnd_Max_Rte));
                        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_Max_Adi.getDec().add(1), //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #MAX-ADI + 1:#MAX-RTE )
                            ":",pnd_Max_Rte));
                        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_Max_Adi.getDec().add(1), //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #MAX-ADI + 1:#MAX-RTE )
                            ":",pnd_Max_Rte));
                        updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_Max_Adi.getDec().add(1), //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #MAX-ADI + 1:#MAX-RTE )
                            ":",pnd_Max_Rte));
                        updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(1,":",pnd_Max_Adi).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_Max_Adi.getDec().add(1), //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( 1:#MAX-ADI ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #MAX-ADI + 1:#MAX-RTE )
                            ":",pnd_Max_Rte));
                    }                                                                                                                                                     //Natural: END-IF
                    //*  030812 END
                    //*  030812
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    FOR19:                                                                                                                                                //Natural: FOR #I 1 TO #MAX-RTE
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Rte)); pnd_I.nadd(1))
                    {
                        //*  030812
                        if (condition(updt_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue("*").equals(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I))))       //Natural: IF UPDT-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( * ) = IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I )
                        {
                            FOR20:                                                                                                                                        //Natural: FOR #J 1 TO #MAX-ADI
                            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Max_Adi)); pnd_J.nadd(1))
                            {
                                if (condition(updt_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_J).equals(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I)))) //Natural: IF UPDT-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #J ) = IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I )
                                {
                                    updt_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_J).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J ) := IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #I )
                                    updt_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_J).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J ) := IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #I )
                                    updt_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_J).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( #J ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #I )
                                    updt_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_J).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( #J ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #I )
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  CHECK FIRST IF THERE IS A SECOND RESULT RECORD BECAUSE   /* 030812
                            //*  IF THERE IS, NO MORE SLOT IS AVAILABLE TO INSERT THE NEW RATE
                            //*            ADD 1 TO #RATE-XX                    /* 030812 START
                            if (condition(pnd_More.getBoolean()))                                                                                                         //Natural: IF #MORE
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                FOR21:                                                                                                                                    //Natural: FOR #RATE-XX 1 #MAX-ADI
                                for (pnd_Rate_Xx.setValue(1); condition(pnd_Rate_Xx.lessOrEqual(pnd_Max_Adi)); pnd_Rate_Xx.nadd(1))
                                {
                                    //*  030812
                                    if (condition(updt_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_Rate_Xx).equals(" ")))                                                //Natural: IF UPDT-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #RATE-XX ) = ' '
                                    {
                                        updt_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I )
                                        updt_Ia_Rslt_Adi_Contract_Id.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(pnd_I));    //Natural: ASSIGN UPDT-IA-RSLT.ADI-CONTRACT-ID ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( #I )
                                        updt_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #I )
                                        updt_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #I )
                                        updt_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #I )
                                        updt_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #I )
                                        if (condition(true)) break;                                                                                                       //Natural: ESCAPE BOTTOM
                                        //*  030812
                                    }                                                                                                                                     //Natural: END-IF
                                    //*  030812
                                }                                                                                                                                         //Natural: END-FOR
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                //*  030812
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  030812 START
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        FOR22:                                                                                                                                            //Natural: FOR #I 1 TO #MAX-RTE
                        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Rte)); pnd_I.nadd(1))
                        {
                            //*  030812
                            if (condition(updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue("*").equals(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I))))   //Natural: IF UPDT-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( * ) = IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I )
                            {
                                FOR23:                                                                                                                                    //Natural: FOR #J 1 TO #MAX-ADI
                                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Max_Adi)); pnd_J.nadd(1))
                                {
                                    if (condition(updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_J).equals(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I)))) //Natural: IF UPDT-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #J ) = IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I )
                                    {
                                        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_J).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J ) := IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #I )
                                        updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_J).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J ) := IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #I )
                                        updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_J).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( #J ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #I )
                                        updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_J).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( #J ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #I )
                                        if (condition(true)) break;                                                                                                       //Natural: ESCAPE BOTTOM
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-FOR
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                //*  RATE ALREADY IN FIRST RESULT RECORD
                                if (condition(updt_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue("*").equals(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I)))) //Natural: IF UPDT-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( * ) = IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I )
                                {
                                    ignore();
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    FOR24:                                                                                                                                //Natural: FOR #RATE-XX 1 #MAX-ADI
                                    for (pnd_Rate_Xx.setValue(1); condition(pnd_Rate_Xx.lessOrEqual(pnd_Max_Adi)); pnd_Rate_Xx.nadd(1))
                                    {
                                        if (condition(updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_Rate_Xx).equals(" ")))                                            //Natural: IF UPDT-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #RATE-XX ) = ' '
                                        {
                                            updt_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I )
                                            updt_Ia_Rsl2_Adi_Contract_Id.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSL2.ADI-CONTRACT-ID ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( #I )
                                            updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #I )
                                            updt_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #I )
                                            updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #I )
                                            updt_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_Rate_Xx).setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_I)); //Natural: ASSIGN UPDT-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( #RATE-XX ) := IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #I )
                                            if (condition(true)) break;                                                                                                   //Natural: ESCAPE BOTTOM
                                        }                                                                                                                                 //Natural: END-IF
                                    }                                                                                                                                     //Natural: END-FOR
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("R2"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  030812 END
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue("*").greater(getZero())))                                                   //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * ) GT 0
                {
                    pnd_Cntrl_Per_Pay_Amt.nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue("*"));                                                     //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * ) TO #CNTRL-PER-PAY-AMT
                    //*  030812 START
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        pnd_Cntrl_Per_Pay_Amt.nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue("*"));                                                                 //Natural: ASSIGN #CNTRL-PER-PAY-AMT := #CNTRL-PER-PAY-AMT + ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * )
                        //*  030812 END
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Per_Amt()),        //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-OLD-PER-AMT := 0 + IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( * )
                        DbsField.add(getZero(),ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue("*")));
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue("*").greater(getZero())))                                                   //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( * ) GT 0
                {
                    pnd_Cntrl_Per_Div_Amt.nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue("*"));                                                     //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( * ) TO #CNTRL-PER-DIV-AMT
                    //*  030812 START
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        pnd_Cntrl_Per_Div_Amt.nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue("*"));                                                                 //Natural: ASSIGN #CNTRL-PER-DIV-AMT := #CNTRL-PER-DIV-AMT + ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( * )
                        //*  030812 END
                    }                                                                                                                                                     //Natural: END-IF
                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt().compute(new ComputeParameters(false, ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Old_Div_Amt()),        //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-OLD-DIV-AMT := 0 + IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( * )
                        DbsField.add(getZero(),ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue("*")));
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue("*").greater(getZero())))                                                      //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( * ) GT 0
                {
                    pnd_Cntrl_Final_Pay_Amt.nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue("*"));                                                      //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( * ) TO #CNTRL-FINAL-PAY-AMT
                    //*  030812 START
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        pnd_Cntrl_Final_Pay_Amt.nadd(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue("*"));                                                                  //Natural: ASSIGN #CNTRL-FINAL-PAY-AMT := #CNTRL-FINAL-PAY-AMT + ADS-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( * )
                        //*  030812 END
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue("*").greater(getZero())))                                                    //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( * ) GT 0
                {
                    pnd_Cntrl_Final_Div_Amt.nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue("*"));                                                    //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( * ) TO #CNTRL-FINAL-DIV-AMT
                    //*  030812 START
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        pnd_Cntrl_Final_Div_Amt.nadd(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue("*"));                                                                //Natural: ASSIGN #CNTRL-FINAL-DIV-AMT := #CNTRL-FINAL-DIV-AMT + ADS-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( * )
                        //*  030812 END
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                FOR25:                                                                                                                                                    //Natural: FOR #I 1 TO IAA-TIAA-FUND-RCRD.C*TIAA-RATE-DATA-GRP
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp())); pnd_I.nadd(1))
                {
                    if (condition(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I).equals(" ")))                                                          //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                        //*  030812
                    }                                                                                                                                                     //Natural: END-IF
                    FOR26:                                                                                                                                                //Natural: FOR #J 1 TO #MAX-ADI
                    for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Max_Adi)); pnd_J.nadd(1))
                    {
                        if (condition(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I).equals(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_J)))) //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I ) = ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #J )
                        {
                            //*  030812
                            //*  CALL THE ACTUARIAL INTERPOLAT-
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_J).greater(getZero()) || ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_J).greater(getZero()))) //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J ) GT 0 OR ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J ) GT 0
                            {
                                pnd_Rslt.setValue(true);                                                                                                                  //Natural: ASSIGN #RSLT := TRUE
                                pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Grd_Std().setValue("G");                                                                    //Natural: ASSIGN #AIAN091-GRD-STD := 'G'
                                //*  ION MODULE.
                                                                                                                                                                          //Natural: PERFORM GET-RATE-DATE
                                sub_Get_Rate_Date();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_J).greater(getZero())))                                     //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J ) GT 0
                            {
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_J)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #I )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_J).greater(getZero())))                                     //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J ) GT 0
                            {
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_J)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #I )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_J).greater(getZero())))                                        //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( #J ) GT 0
                            {
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_I).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_J)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #I )
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_J).greater(getZero())))                                      //Natural: IF ADS-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( #J ) GT 0
                            {
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_I).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_J)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #I )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_J).equals(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue("*")))) //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #J ) = IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( * )
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_J).greater(getZero()) || ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_J).greater(getZero()))) //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J ) GT 0 OR ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J ) GT 0
                            {
                                //*  030812
                                pnd_Rate_No.nadd(1);                                                                                                                      //Natural: ADD 1 TO #RATE-NO
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_Rate_No).setValue(pnd_Next_Pymnt_Dte);                                      //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( #RATE-NO ) := #NEXT-PYMNT-DTE
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_Rate_No).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #RATE-NO ) := ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #J )
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(pnd_Rate_No).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Contract_Id().getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( #RATE-NO ) := ADS-IA-RSLT.ADI-CONTRACT-ID ( #J )
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_Rate_No).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #RATE-NO ) := ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J )
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_Rate_No).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #RATE-NO ) := ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J )
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_Rate_No).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #RATE-NO ) := ADS-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( #J )
                                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_Rate_No).setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #RATE-NO ) := ADS-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( #J )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  030812 START
                if (condition(pnd_More.getBoolean()))                                                                                                                     //Natural: IF #MORE
                {
                    FOR27:                                                                                                                                                //Natural: FOR #I 1 TO IAA-TIAA-FUND-RCRD.C*TIAA-RATE-DATA-GRP
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Count_Casttiaa_Rate_Data_Grp())); pnd_I.nadd(1))
                    {
                        if (condition(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I).equals(" ")))                                                      //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I ) = ' '
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        FOR28:                                                                                                                                            //Natural: FOR #J 1 TO #MAX-ADI
                        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_Max_Adi)); pnd_J.nadd(1))
                        {
                            if (condition(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I).equals(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_J))))  //Natural: IF IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I ) = ADS-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #J )
                            {
                                //*  CALL THE ACTUARIAL INTERPOLAT-
                                if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_J).greater(getZero()) || ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_J).greater(getZero()))) //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J ) GT 0 OR ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J ) GT 0
                                {
                                    pnd_Rslt.setValue(false);                                                                                                             //Natural: ASSIGN #RSLT := FALSE
                                    pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Grd_Std().setValue("G");                                                                //Natural: ASSIGN #AIAN091-GRD-STD := 'G'
                                    //*  ION MODULE.
                                                                                                                                                                          //Natural: PERFORM GET-RATE-DATE
                                    sub_Get_Rate_Date();
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: END-IF
                                if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_J).greater(getZero())))                                                 //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J ) GT 0
                                {
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I).nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_J));     //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #I )
                                }                                                                                                                                         //Natural: END-IF
                                if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_J).greater(getZero())))                                                 //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J ) GT 0
                                {
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I).nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_J));     //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #I )
                                }                                                                                                                                         //Natural: END-IF
                                if (condition(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_J).greater(getZero())))                                                    //Natural: IF ADS-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( #J ) GT 0
                                {
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_I).nadd(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_J)); //Natural: ADD ADS-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #I )
                                }                                                                                                                                         //Natural: END-IF
                                if (condition(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_J).greater(getZero())))                                                  //Natural: IF ADS-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( #J ) GT 0
                                {
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_I).nadd(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_J)); //Natural: ADD ADS-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( #J ) TO IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #I )
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_J).equals(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue("*"))))    //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #J ) = IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( * )
                            {
                                ignore();
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_J).greater(getZero()) || ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_J).greater(getZero()))) //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J ) GT 0 OR ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J ) GT 0
                                {
                                    //*  030812
                                    pnd_Rate_No.nadd(1);                                                                                                                  //Natural: ADD 1 TO #RATE-NO
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_Rate_No).setValue(pnd_Next_Pymnt_Dte);                                  //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( #RATE-NO ) := #NEXT-PYMNT-DTE
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_Rate_No).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #RATE-NO ) := ADS-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #J )
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Gic().getValue(pnd_Rate_No).setValue(ads_Ia_Rsl2_Adi_Contract_Id.getValue(pnd_J));         //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-GIC ( #RATE-NO ) := ADS-IA-RSL2.ADI-CONTRACT-ID ( #J )
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_Rate_No).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #RATE-NO ) := ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J )
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_Rate_No).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #RATE-NO ) := ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J )
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Pay_Amt().getValue(pnd_Rate_No).setValue(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-PAY-AMT ( #RATE-NO ) := ADS-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( #J )
                                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Final_Div_Amt().getValue(pnd_Rate_No).setValue(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_J)); //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-RATE-FINAL-DIV-AMT ( #RATE-NO ) := ADS-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( #J )
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  030812 END
                    //*  030812
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Mode_Ind().setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Pymnt_Mode());                                                    //Natural: ASSIGN IAA-TIAA-FUND-RCRD.TIAA-MODE-IND := ADS-IA-RSLT.ADI-PYMNT-MODE
                if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue("*").greater(getZero())))                                                   //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * ) GT 0
                {
                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt().nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue("*"));                       //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * ) TO IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue("*").greater(getZero())))                                                   //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( * ) GT 0
                {
                    ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt().nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue("*"));                       //Natural: ADD ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( * ) TO IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT
                }                                                                                                                                                         //Natural: END-IF
                //*  030812 START
                if (condition(pnd_More.getBoolean()))                                                                                                                     //Natural: IF #MORE
                {
                    if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue("*").greater(getZero())))                                                               //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * ) GT 0
                    {
                        ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Per_Amt().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue("*"));                                   //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * ) TO IAA-TIAA-FUND-RCRD.TIAA-TOT-PER-AMT
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue("*").greater(getZero())))                                                               //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( * ) GT 0
                    {
                        ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Tot_Div_Amt().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue("*"));                                   //Natural: ADD ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( * ) TO IAA-TIAA-FUND-RCRD.TIAA-TOT-DIV-AMT
                    }                                                                                                                                                     //Natural: END-IF
                    //*  030812 END
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ANY VALUE
            if (condition(decideConditionsMet2326 > 0))
            {
                updt_Ia_Rslt_Adi_Finl_Periodic_Py_Dte.setValue(pnd_Fp_Dte);                                                                                               //Natural: ASSIGN UPDT-IA-RSLT.ADI-FINL-PERIODIC-PY-DTE := #FP-DTE
                //*  030812 START
                if (condition(pnd_More.getBoolean()))                                                                                                                     //Natural: IF #MORE
                {
                    updt_Ia_Rsl2_Adi_Finl_Periodic_Py_Dte.setValue(pnd_Fp_Dte);                                                                                           //Natural: ASSIGN UPDT-IA-RSL2.ADI-FINL-PERIODIC-PY-DTE := #FP-DTE
                    //*  030812 END
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Lst_Trans_Dte().setValue(pnd_Trans_Dte);                                                                                 //Natural: ASSIGN IAA-TIAA-FUND-RCRD.LST-TRANS-DTE := #TRANS-DTE
                ldaAdsl601.getVw_iaa_Tiaa_Fund_Rcrd().updateDBRow("G2");                                                                                                  //Natural: UPDATE ( G2. )
                                                                                                                                                                          //Natural: PERFORM CREATE-AFTER-TIAA-FUND
                sub_Create_After_Tiaa_Fund();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Cref_Fund() throws Exception                                                                                                                     //Natural: GET-CREF-FUND
    {
        if (BLNatReinput.isReinput()) return;

        ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1().startDatabaseRead                                                                                                         //Natural: READ IAA-CREF-FUND-RCRD-1 BY CREF-CNTRCT-FUND-KEY STARTING FROM #CNTRCT-FUND-KEY
        (
        "R3",
        new Wc[] { new Wc("CREF_CNTRCT_FUND_KEY", ">=", pnd_Cntrct_Fund_Key, WcType.BY) },
        new Oc[] { new Oc("CREF_CNTRCT_FUND_KEY", "ASC") }
        );
        R3:
        while (condition(ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1().readNextRow("R3")))
        {
            if (condition(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Ppcn_Nbr().notEquals(pnd_Cntrct_Fund_Key_Pnd_Cntrct_Ppcn_Nbr) || ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cntrct_Payee_Cde().notEquals(pnd_Cntrct_Fund_Key_Pnd_Cntrct_Payee_Cde))) //Natural: IF IAA-CREF-FUND-RCRD-1.CREF-CNTRCT-PPCN-NBR NE #CNTRCT-PPCN-NBR OR IAA-CREF-FUND-RCRD-1.CREF-CNTRCT-PAYEE-CDE NE #CNTRCT-PAYEE-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde().equals("1") || ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde().equals("1S")               //Natural: REJECT IF IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE = '1' OR = '1S' OR = '1G'
                || ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde().equals("1G")))
            {
                continue;
            }
            if (condition(pnd_Rea.getBoolean()))                                                                                                                          //Natural: IF #REA
            {
                //*  EM - 012309
                if (condition(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde().notEquals("09") && ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde().notEquals("11")))  //Natural: REJECT IF ( IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE NE '09' ) AND ( IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE NE '11' )
                {
                    continue;
                }
                //*  EM - 012309
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde().equals("09") || ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde().equals("11")))        //Natural: REJECT IF IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE = '09' OR = '11'
                {
                    continue;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1().getAstISN("R3") == pnd_New_Isns.getValue("*").getInt()))                                                //Natural: REJECT IF *ISN ( R3. ) = #NEW-ISNS ( * )
            {
                continue;
            }
            G3:                                                                                                                                                           //Natural: GET IAA-CREF-FUND-RCRD-1 *ISN ( R3. )
            ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1().readByID(ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1().getAstISN("R3"), "G3");
                                                                                                                                                                          //Natural: PERFORM CREATE-BFORE-CREF-FUND
            sub_Create_Bfore_Cref_Fund();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("R3"))) break;
                else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  DELETE INACTIVE FUND RECORD AND CREATE ACTIVE FUND RECORD
            //*  INACTIVE FUND EXISTED BECAUSE OF NEW ISSUE OR TRANSFER
            if (condition((ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Actvty_Cde().equals(9) && (ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn().equals("NI") //Natural: IF IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-ACTVTY-CDE = 9 AND IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-TRMNTE-RSN = 'NI' OR = 'TR'
                || ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Trmnte_Rsn().equals("TR")))))
            {
                ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1().deleteDBRow("G3");                                                                                                //Natural: DELETE ( G3. )
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Fund_1.reset();                                                                                                                                           //Natural: RESET #FUND-1
            DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), pnd_Fund_1, ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde(), pnd_Rc);                           //Natural: CALLNAT 'IAAN0511' #FUND-1 IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE #RC
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            if (condition(pnd_Rc.equals(getZero())))                                                                                                                      //Natural: IF #RC = 0
            {
                DbsUtil.examine(new ExamineSource(updt_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd.getValue("*")), new ExamineSearch(pnd_Fund_1), new ExamineGivingIndex(pnd_I2));       //Natural: EXAMINE UPDT-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( * ) FOR #FUND-1 GIVING INDEX #I2
                if (condition(pnd_I2.greater(getZero())))                                                                                                                 //Natural: IF #I2 GT 0
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_I1.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #I1
                    pnd_I2.setValue(pnd_I1);                                                                                                                              //Natural: ASSIGN #I2 := #I1
                }                                                                                                                                                         //Natural: END-IF
                updt_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd.getValue(pnd_I2).setValue(pnd_Fund_1);                                                                                  //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #I2 ) := #FUND-1
                updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd.getValue(pnd_I2).setValue(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Rate_Cde().getValue(1));                           //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-CREF-IA-RATE-CD ( #I2 ) := IAA-CREF-FUND-RCRD-1.CREF-RATE-CDE ( 1 )
                if (condition(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde().equals("W") || ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde().equals("4")))        //Natural: IF IAA-CREF-FUND-RCRD-1.CREF-CMPNY-CDE = 'W' OR = '4'
                {
                    updt_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units.getValue(pnd_I2).setValue(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt().getValue(1));                //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #I2 ) := IAA-CREF-FUND-RCRD-1.CREF-UNITS-CNT ( 1 )
                    updt_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt.getValue(pnd_I2).setValue(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt());                             //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #I2 ) := IAA-CREF-FUND-RCRD-1.CREF-TOT-PER-AMT
                    updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val.getValue(pnd_I2).setValue(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Unit_Val());                           //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL ( #I2 ) := IAA-CREF-FUND-RCRD-1.CREF-UNIT-VAL
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    updt_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units.getValue(pnd_I2).setValue(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt().getValue(1));                  //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #I2 ) := IAA-CREF-FUND-RCRD-1.CREF-UNITS-CNT ( 1 )
                    updt_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt.getValue(pnd_I2).setValue(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt());                               //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #I2 ) := IAA-CREF-FUND-RCRD-1.CREF-TOT-PER-AMT
                    updt_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val.getValue(pnd_I2).setValue(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Unit_Val());                             //Natural: ASSIGN UPDT-IA-RSLT.ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #I2 ) := IAA-CREF-FUND-RCRD-1.CREF-UNIT-VAL
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.examine(new ExamineSource(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd().getValue("*")), new ExamineSearch(pnd_Fund_1), new                     //Natural: EXAMINE ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( * ) FOR #FUND-1 GIVING INDEX #J
                    ExamineGivingIndex(pnd_J));
                if (condition(pnd_J.greater(getZero())))                                                                                                                  //Natural: IF #J GT 0
                {
                    if (condition(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde().equals("W") || ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Cmpny_Cde().equals("4")))    //Natural: IF IAA-CREF-FUND-RCRD-1.CREF-CMPNY-CDE = 'W' OR = '4'
                    {
                        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_J).greater(getZero())))                                      //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #J ) GT 0
                        {
                            pnd_C.setValue(true);                                                                                                                         //Natural: ASSIGN #C := TRUE
                            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt().getValue(1).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_J)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #J ) TO IAA-CREF-FUND-RCRD-1.CREF-UNITS-CNT ( 1 )
                            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt().setValue(0);                                                                            //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-TOT-PER-AMT := 0
                            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Old_Per_Amt().setValue(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt());                        //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-OLD-PER-AMT := IAA-CREF-FUND-RCRD-1.CREF-TOT-PER-AMT
                            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Old_Unit_Val().setValue(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Unit_Val());                          //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-OLD-UNIT-VAL := IAA-CREF-FUND-RCRD-1.CREF-UNIT-VAL
                            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Unit_Val().reset();                                                                                   //Natural: RESET IAA-CREF-FUND-RCRD-1.CREF-UNIT-VAL
                            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Lst_Trans_Dte().setValue(pnd_Trans_Dte);                                                                   //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.LST-TRANS-DTE := #TRANS-DTE
                            ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1().updateDBRow("G3");                                                                                    //Natural: UPDATE ( G3. )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_J).greater(getZero())))                                        //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #J ) GT 0
                        {
                            pnd_Cref_Fund_Key.setValue(pnd_Cntrct_Fund_Key);                                                                                              //Natural: ASSIGN #CREF-FUND-KEY := #CNTRCT-FUND-KEY
                            pnd_Cref_Fund_Key_Pnd_Fnd_Cde.setValue(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde());                                                   //Natural: ASSIGN #FND-CDE := IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE
                            pnd_Cref_Fund_Key_Pnd_Cmpny_Cde.setValue("2");                                                                                                //Natural: ASSIGN #CMPNY-CDE := '2'
                            //*  EM - 012309
                            if (condition(pnd_Cref_Fund_Key_Pnd_Fnd_Cde.equals("09") || pnd_Cref_Fund_Key_Pnd_Fnd_Cde.equals("11")))                                      //Natural: IF #FND-CDE = '09' OR = '11'
                            {
                                pnd_Cref_Fund_Key_Pnd_Cmpny_Cde.setValue("U");                                                                                            //Natural: ASSIGN #CMPNY-CDE := 'U'
                            }                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-CREF-IF-NONE-EXISTS
                            sub_Create_Cref_If_None_Exists();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("R3"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_J).greater(getZero()) || ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_J).greater(getZero()))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #J ) GT 0 OR ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #J ) GT 0
                        {
                            pnd_C.setValue(true);                                                                                                                         //Natural: ASSIGN #C := TRUE
                            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Units_Cnt().getValue(1).nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_J)); //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #J ) TO IAA-CREF-FUND-RCRD-1.CREF-UNITS-CNT ( 1 )
                            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt().nadd(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_J));             //Natural: ADD ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #J ) TO IAA-CREF-FUND-RCRD-1.CREF-TOT-PER-AMT
                            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Old_Per_Amt().setValue(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Tot_Per_Amt());                        //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-OLD-PER-AMT := IAA-CREF-FUND-RCRD-1.CREF-TOT-PER-AMT
                            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Old_Unit_Val().setValue(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Unit_Val());                          //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.CREF-OLD-UNIT-VAL := IAA-CREF-FUND-RCRD-1.CREF-UNIT-VAL
                            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Unit_Val().reset();                                                                                   //Natural: RESET IAA-CREF-FUND-RCRD-1.CREF-UNIT-VAL
                            ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Lst_Trans_Dte().setValue(pnd_Trans_Dte);                                                                   //Natural: ASSIGN IAA-CREF-FUND-RCRD-1.LST-TRANS-DTE := #TRANS-DTE
                            ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1().updateDBRow("G3");                                                                                    //Natural: UPDATE ( G3. )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_J).greater(getZero())))                                      //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #J ) GT 0
                        {
                            pnd_Cref_Fund_Key.setValue(pnd_Cntrct_Fund_Key);                                                                                              //Natural: ASSIGN #CREF-FUND-KEY := #CNTRCT-FUND-KEY
                            pnd_Cref_Fund_Key_Pnd_Fnd_Cde.setValue(ldaAdsl601.getIaa_Cref_Fund_Rcrd_1_Cref_Fund_Cde());                                                   //Natural: ASSIGN #FND-CDE := IAA-CREF-FUND-RCRD-1.CREF-FUND-CDE
                            pnd_Cref_Fund_Key_Pnd_Cmpny_Cde.setValue("4");                                                                                                //Natural: ASSIGN #CMPNY-CDE := '4'
                            //*  EM - 012309
                            if (condition(pnd_Cref_Fund_Key_Pnd_Fnd_Cde.equals("09") || pnd_Cref_Fund_Key_Pnd_Fnd_Cde.equals("11")))                                      //Natural: IF #FND-CDE = '09' OR = '11'
                            {
                                pnd_Cref_Fund_Key_Pnd_Cmpny_Cde.setValue("W");                                                                                            //Natural: ASSIGN #CMPNY-CDE := 'W'
                            }                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-CREF-IF-NONE-EXISTS
                            sub_Create_Cref_If_None_Exists();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("R3"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaAdsl601.getVw_iaa_Cref_Fund_Trans_1().setValuesByName(ldaAdsl601.getVw_iaa_Cref_Fund_Rcrd_1());                                                        //Natural: MOVE BY NAME IAA-CREF-FUND-RCRD-1 TO IAA-CREF-FUND-TRANS-1
                                                                                                                                                                          //Natural: PERFORM CREATE-AFTER-CREF-FUND
                sub_Create_After_Cref_Fund();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R3"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R3"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Rate_Date() throws Exception                                                                                                                     //Natural: GET-RATE-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(ldaAdsl601.getIaa_Cntrct_Cntrct_Optn_Cde().equals(21)))                                                                                             //Natural: IF IAA-CNTRCT.CNTRCT-OPTN-CDE = 21
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Mode().setValue(ldaAdsl601.getIaa_Cntrct_Prtcpnt_Role_Cntrct_Mode_Ind());                                           //Natural: ASSIGN #AIAN091-MODE := IAA-CNTRCT-PRTCPNT-ROLE.CNTRCT-MODE-IND
        pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Rate_Basis_In().setValue(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Cde().getValue(pnd_I));                         //Natural: ASSIGN #AIAN091-RATE-BASIS-IN := IAA-TIAA-FUND-RCRD.TIAA-RATE-CDE ( #I )
        pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Rate_Amt().compute(new ComputeParameters(false, pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Rate_Amt()),          //Natural: ASSIGN #AIAN091-RATE-AMT := IAA-TIAA-FUND-RCRD.TIAA-PER-PAY-AMT ( #I ) + IAA-TIAA-FUND-RCRD.TIAA-PER-DIV-AMT ( #I )
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Pay_Amt().getValue(pnd_I).add(ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Per_Div_Amt().getValue(pnd_I)));
        if (condition(pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Grd_Std().equals("S")))                                                                               //Natural: IF #AIAN091-GRD-STD = 'S'
        {
            //*  030812
            if (condition(pnd_Rslt.getBoolean()))                                                                                                                         //Natural: IF #RSLT
            {
                pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Sweep_Amt().compute(new ComputeParameters(false, pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Sweep_Amt()),  //Natural: ASSIGN #AIAN091-SWEEP-AMT := ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J ) + ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #J )
                    ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_J).add(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_J)));
                //*  030812 START
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Sweep_Amt().compute(new ComputeParameters(false, pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Sweep_Amt()),  //Natural: ASSIGN #AIAN091-SWEEP-AMT := ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J ) + ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #J )
                    ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_J).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_J)));
                //*  030812 END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  030812
            if (condition(pnd_Rslt.getBoolean()))                                                                                                                         //Natural: IF #RSLT
            {
                pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Sweep_Amt().compute(new ComputeParameters(false, pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Sweep_Amt()),  //Natural: ASSIGN #AIAN091-SWEEP-AMT := ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J ) + ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J )
                    ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_J).add(ldaAdsl601.getAds_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_J)));
                //*  030812 START
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Sweep_Amt().compute(new ComputeParameters(false, pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Sweep_Amt()),  //Natural: ASSIGN #AIAN091-SWEEP-AMT := ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J ) + ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J )
                    ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_J).add(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_J)));
                //*  030812 END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Sweep_Date().setValue(pnd_Effct_Dte);                                                                               //Natural: ASSIGN #AIAN091-SWEEP-DATE := #EFFCT-DTE
        pnd_Wdte8.reset();                                                                                                                                                //Natural: RESET #WDTE8 #WDTE6
        pnd_Wdte6.reset();
        pnd_Wdte8.setValueEdited(ldaAdsl601.getIaa_Cntrct_Cntrct_Issue_Dte(),new ReportEditMask("999999"));                                                               //Natural: MOVE EDITED IAA-CNTRCT.CNTRCT-ISSUE-DTE ( EM = 999999 ) TO #WDTE8
        pnd_Wdte6.setValueEdited(ldaAdsl601.getIaa_Cntrct_Cntrct_Issue_Dte_Dd(),new ReportEditMask("99"));                                                                //Natural: MOVE EDITED IAA-CNTRCT.CNTRCT-ISSUE-DTE-DD ( EM = 99 ) TO #WDTE6
        pnd_Wdte8.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Wdte8, pnd_Wdte6));                                                                        //Natural: COMPRESS #WDTE8 #WDTE6 INTO #WDTE8 LEAVE NO SPACE
        //*  EM - 012309
        //*  DEA
        pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Issue_Date().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Wdte8);                                              //Natural: MOVE EDITED #WDTE8 TO #AIAN091-ISSUE-DATE ( EM = YYYYMMDD )
        pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Origin().setValue(ldaAdsl601.getIaa_Cntrct_Cntrct_Orgn_Cde());                                                      //Natural: ASSIGN #AIAN091-ORIGIN := IAA-CNTRCT.CNTRCT-ORGN-CDE
        pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Contract().setValue(ldaAdsl601.getAds_Ia_Rslt_Adi_Ia_Tiaa_Nbr());                                                   //Natural: ASSIGN #AIAN091-CONTRACT := ADS-IA-RSLT.ADI-IA-TIAA-NBR
        DbsUtil.callnat(Aian091.class , getCurrentProcessState(), pdaAiaa091.getPnd_Aian091_Linkage());                                                                   //Natural: CALLNAT 'AIAN091' #AIAN091-LINKAGE
        if (condition(Global.isEscape())) return;
        //* *IF #AIAN091-RETURN-CODE = 0                       /* 030812
        //*  030812
        if (condition(pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Return_Code_Nbr().equals(getZero())))                                                                 //Natural: IF #AIAN091-RETURN-CODE-NBR = 0
        {
            //*  05/27/04 EMELNIK
            pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Date_Out().nsubtract(1);                                                                                        //Natural: SUBTRACT 1 FROM #AIAN091-DATE-OUT
            ldaAdsl601.getIaa_Tiaa_Fund_Rcrd_Tiaa_Rate_Dte().getValue(pnd_I).setValue(pdaAiaa091.getPnd_Aian091_Linkage_Pnd_Aian091_Date_Out());                          //Natural: MOVE #AIAN091-DATE-OUT TO IAA-TIAA-FUND-RCRD.TIAA-RATE-DTE ( #I )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Err_Cde.setValue("L32");                                                                                                                                  //Natural: ASSIGN #ERR-CDE := 'L32'
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
