/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:54:37 PM
**        * FROM NATURAL SUBPROGRAM : Adsn600
************************************************************
**        * FILE NAME            : Adsn600.java
**        * CLASS NAME           : Adsn600
**        * INSTANCE NAME        : Adsn600
************************************************************
************************************************************************
*
* PROGRAM:  ADSN600
* DATE   :  3/04
* FUNCTION: INTERFACE TO CREATE IA MASTER AND TRANSACTION RECORDS, CPS
*           PAYMENT RECORDS, AND PP INSTALLMENT RECORDS.
*
*
*  09/03  ADDED LOGIC TO HANDLE IVC FOR EGTTRA
*         DO SCAN ON 9/03
*
*  01/04  ADDED PDA AIAA025 FOR RENEWAL DIVIDEND MODULE AIAN025
*         DO SCAN ON 01/04.
*
*   08/04  COMMENTED OUT TRANSACTIN TYPE FOR ADAS
*
* 08312005 - MARINA NACHBER   REL 6.5 FOR TPA's and IPRO'S
*  11/22/06 O. SOTTO RECOMPILED DUE TO AIAA0380 CHANGE.
*  02/22/07 O. SOTTO DISPLAY PARMS WHEN ERROR = L28 FOR DEBUGGING.
*                    SC 022207.
* 03/01/07  O. SOTTO. RESOLVE L28 PROBLEM (IM387749). SC 030107.
* 03/07/07  M. NACHBER  ATRA
* 08/02/07  D. GEARY  NEW 75% ANNUITY OPTION. MARKED BY  DG 080207
* 02/21/08  O. SOTTO  TIAA RATE EXPANSION TO 99 OCCURS. SC 022108
* 03/17/08  E. MELNIK ROTH 403B/401K. EM 031708
* 01/21/09  E. MELNIK TIAA ACCESS/STABLE RETURN ENHANCEMENTS. MARKED
*                         BY EM - 012109.
* 03/18/10  C. MASON  PCPOP CONVERSION - SET PPG CODE FOR CANADIAN
*                     CONVERTED CONTRACTS
* 03/31/10  C. MASON  PASS PLAN-NBR, TAX OVRD & ADC-T395-FUND-ID TO
*                     ADSN601
* 05/14/2010 C. MASON ADD CODE TO LIMIT CALLS TO AIAN083
* 11/19/2010 J. TINIO POPULATE NEW FIELDS FOR AIAN083. SC 111910.
* 12/16/2010 O. SOTTO REVISED ADSA600 TO ADD HOLD CODES. SC 121610.
* 05/27/2011 E. MELNIK MEOW CHANGES.  MARKED BY EM - 052711.
* 12/06/2011 O. SOTTO  DI HUB CHANGES PLUS ADDED AIAN035 PARM DISPLAYS
*                      FOR DEBUGGING. SC 120611.
* 01/05/2012 O. SOTTO  SUNY/CUNY CHANGES.  SC 010512.
* 03/09/2012 O. SOTTO  RATE EXPANSION. SC 030912.
* 06/20/2012 O. SOTTO  PROD FIX - COMMENT-OUT CALL TO NAZN606. SC 062012
* 07/23/2012 E. MELNIK FIX FOR HISTORICAL RECORDS.  MARKED BY
*                      EM - 072312.
* 07/27/2012 J. TINIO  ADDITIONAL FIX FOR HISTORICAL RECORDS.  MARKED
*                      BY 072712.
* 08/02/2012 E. MELNIK RATE BASE EXPANSION. CHANGES FOR AC ROLLOVERS
*                      WITH IVC.  MARKED BY EM - 080212.
* 10/03/2013 E. MELNIK CREF/REA REDESIGN CHANGES.
*                      MARKED BY EM - 100313.
* 5/07/2015  E. MELNIK BENE IN THE PLAN CHANGES.  MARKED BY EM - 050715.
* 02/21/2017R. CARREON PIN EXPANSION 02212017
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn600 extends BLNatBase
{
    // Data Areas
    private PdaAdsa600 pdaAdsa600;
    private PdaNaza600i pdaNaza600i;
    private PdaFcpa140f pdaFcpa140f;
    private PdaAiaa0380 pdaAiaa0380;
    private PdaAiaa025 pdaAiaa025;
    private PdaAiaa026 pdaAiaa026;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Rslt_Isn;
    private DbsField pnd_Prtcpnt_Isn;
    private DbsField pnd_Da_Cntrct_Isn;
    private DbsField pnd_Bsnss_Dte;
    private DbsField pnd_Acctg_Dte;
    private DbsField pnd_Orgnl_State;
    private DbsField pnd_Adp_Alt_Dest_Hold_Cde;
    private DbsField pnd_Adp_Crrspndnce_Perm_Addr_Txt;
    private DbsField pnd_Adp_Crrspndnce_Perm_Addr_Zip;
    private DbsField pnd_Adp_Crrspndnce_Perm_Addr_Typ_Cd;
    private DbsField pnd_Prdc_Ivc;
    private DbsField pnd_Ivc_Pymnt_Rule;
    private DbsField pnd_Err_Cde_X;
    private DbsField pnd_Program_Nme;
    private DbsField pnd_Err_Nr;
    private DbsField pnd_Err_Line_X;
    private DbsField pnd_Ia_Orgn_Code;

    private DataAccessProgramView vw_ads_Ia_Rslt;
    private DbsField ads_Ia_Rslt_Rqst_Id;
    private DbsField ads_Ia_Rslt_Adi_Ia_Rcrd_Cde;
    private DbsField ads_Ia_Rslt_Adi_Sqnce_Nbr;
    private DbsField ads_Ia_Rslt_Adi_Stts_Cde;
    private DbsGroup ads_Ia_Rslt_Adi_Tiaa_NbrsMuGroup;
    private DbsField ads_Ia_Rslt_Adi_Tiaa_Nbrs;
    private DbsGroup ads_Ia_Rslt_Adi_Cref_NbrsMuGroup;
    private DbsField ads_Ia_Rslt_Adi_Cref_Nbrs;
    private DbsField ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr;
    private DbsField ads_Ia_Rslt_Adi_Ia_Tiaa_Payee_Cde;
    private DbsField ads_Ia_Rslt_Adi_Ia_Cref_Nbr;
    private DbsField ads_Ia_Rslt_Adi_Ia_Cref_Payee_Cde;
    private DbsField ads_Ia_Rslt_Adi_Curncy_Cde;
    private DbsField ads_Ia_Rslt_Adi_Pymnt_Cde;
    private DbsField ads_Ia_Rslt_Adi_Pymnt_Mode;
    private DbsField ads_Ia_Rslt_Adi_Lst_Actvty_Dte;
    private DbsField ads_Ia_Rslt_Adi_Annty_Strt_Dte;
    private DbsField ads_Ia_Rslt_Adi_Instllmnt_Dte;
    private DbsField ads_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte;
    private DbsField ads_Ia_Rslt_Adi_Frst_Ck_Pd_Dte;
    private DbsField ads_Ia_Rslt_Adi_Finl_Periodic_Py_Dte;
    private DbsField ads_Ia_Rslt_Adi_Finl_Prtl_Py_Dte;

    private DbsGroup ads_Ia_Rslt_Adi_Ivc_Data;
    private DbsField ads_Ia_Rslt_Adi_Tiaa_Ivc_Amt;
    private DbsField ads_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind;
    private DbsField ads_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt;
    private DbsField ads_Ia_Rslt_Adi_Cref_Ivc_Amt;
    private DbsField ads_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind;
    private DbsField ads_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt;
    private DbsField ads_Ia_Rslt_Count_Castadi_Dtl_Tiaa_Data;

    private DbsGroup ads_Ia_Rslt_Adi_Dtl_Tiaa_Data;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Rate_Cd;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Std_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Grd_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Acct_Cd;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Eff_Rte_Intrst;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt;

    private DbsGroup ads_Ia_Rslt_Adi_Dtl_Cref_Data;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Da_Rate_Cd;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val;
    private DbsField ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Amt;
    private DbsField ads_Ia_Rslt_Adi_Optn_Cde;
    private DbsField ads_Ia_Rslt_Adi_Tiaa_Sttlmnt;
    private DbsField ads_Ia_Rslt_Adi_Tiaa_Re_Sttlmnt;
    private DbsField ads_Ia_Rslt_Adi_Cref_Sttlmnt;
    private DbsGroup ads_Ia_Rslt_Adi_Orig_Lob_IndMuGroup;
    private DbsField ads_Ia_Rslt_Adi_Orig_Lob_Ind;
    private DbsField ads_Ia_Rslt_Adi_Roth_Rqst_Ind;
    private DbsGroup ads_Ia_Rslt_Adi_Contract_IdMuGroup;
    private DbsField ads_Ia_Rslt_Adi_Contract_Id;

    private DataAccessProgramView vw_ads_Ia_Rsl2;
    private DbsField ads_Ia_Rsl2_Rqst_Id;
    private DbsField ads_Ia_Rsl2_Adi_Ia_Rcrd_Cde;
    private DbsField ads_Ia_Rsl2_Adi_Sqnce_Nbr;
    private DbsField ads_Ia_Rsl2_Adi_Stts_Cde;
    private DbsGroup ads_Ia_Rsl2_Adi_Tiaa_NbrsMuGroup;
    private DbsField ads_Ia_Rsl2_Adi_Tiaa_Nbrs;
    private DbsGroup ads_Ia_Rsl2_Adi_Cref_NbrsMuGroup;
    private DbsField ads_Ia_Rsl2_Adi_Cref_Nbrs;
    private DbsField ads_Ia_Rsl2_Adi_Ia_Tiaa_Nbr;
    private DbsField ads_Ia_Rsl2_Adi_Ia_Tiaa_Payee_Cde;
    private DbsField ads_Ia_Rsl2_Adi_Ia_Cref_Nbr;
    private DbsField ads_Ia_Rsl2_Adi_Ia_Cref_Payee_Cde;
    private DbsField ads_Ia_Rsl2_Adi_Curncy_Cde;
    private DbsField ads_Ia_Rsl2_Adi_Pymnt_Cde;
    private DbsField ads_Ia_Rsl2_Adi_Pymnt_Mode;
    private DbsField ads_Ia_Rsl2_Adi_Lst_Actvty_Dte;
    private DbsField ads_Ia_Rsl2_Adi_Annty_Strt_Dte;
    private DbsField ads_Ia_Rsl2_Adi_Instllmnt_Dte;
    private DbsField ads_Ia_Rsl2_Adi_Frst_Pymnt_Due_Dte;
    private DbsField ads_Ia_Rsl2_Adi_Frst_Ck_Pd_Dte;
    private DbsField ads_Ia_Rsl2_Adi_Finl_Periodic_Py_Dte;
    private DbsField ads_Ia_Rsl2_Adi_Finl_Prtl_Py_Dte;

    private DbsGroup ads_Ia_Rsl2_Adi_Ivc_Data;
    private DbsField ads_Ia_Rsl2_Adi_Tiaa_Ivc_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Tiaa_Ivc_Gd_Ind;
    private DbsField ads_Ia_Rsl2_Adi_Tiaa_Prdc_Ivc_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Cref_Ivc_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Cref_Ivc_Gd_Ind;
    private DbsField ads_Ia_Rsl2_Adi_Cref_Prdc_Ivc_Amt;
    private DbsField ads_Ia_Rsl2_Count_Castadi_Dtl_Tiaa_Data;

    private DbsGroup ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Rate_Cd;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Acct_Cd;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Eff_Rte_Intrst;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt;

    private DbsGroup ads_Ia_Rsl2_Adi_Dtl_Cref_Data;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Da_Rate_Cd;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Acct_Cd;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Ia_Rate_Cd;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Da_Annl_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Annl_Nbr_Units;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Ia_Annl_Unit_Val;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Annl_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Da_Mnthly_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Mnthly_Nbr_Units;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val;
    private DbsField ads_Ia_Rsl2_Adi_Dtl_Cref_Mnthly_Amt;
    private DbsField ads_Ia_Rsl2_Adi_Optn_Cde;
    private DbsField ads_Ia_Rsl2_Adi_Tiaa_Sttlmnt;
    private DbsField ads_Ia_Rsl2_Adi_Tiaa_Re_Sttlmnt;
    private DbsField ads_Ia_Rsl2_Adi_Cref_Sttlmnt;
    private DbsGroup ads_Ia_Rsl2_Adi_Orig_Lob_IndMuGroup;
    private DbsField ads_Ia_Rsl2_Adi_Orig_Lob_Ind;
    private DbsField ads_Ia_Rsl2_Adi_Roth_Rqst_Ind;
    private DbsGroup ads_Ia_Rsl2_Adi_Contract_IdMuGroup;
    private DbsField ads_Ia_Rsl2_Adi_Contract_Id;

    private DataAccessProgramView vw_ads_Prtcpnt;
    private DbsField ads_Prtcpnt_Adp_Frst_Annt_Dte_Of_Brth;
    private DbsField ads_Prtcpnt_Adp_Scnd_Annt_Dte_Of_Brth;
    private DbsField ads_Prtcpnt_Adp_Annt_Typ_Cde;
    private DbsField ads_Prtcpnt_Adp_Rsttlmnt_Cnt;
    private DbsField ads_Prtcpnt_Adp_State_Of_Issue;
    private DbsField ads_Prtcpnt_Adp_Frst_Annt_Sex_Cde;
    private DbsField ads_Prtcpnt_Adp_Scnd_Annt_Sex_Cde;
    private DbsField ads_Prtcpnt_Adp_Effctv_Dte;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Rlvr_Dest;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Acct_Nbr;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Hold_Cde;
    private DbsField ads_Prtcpnt_Adp_Ivc_Rlvr_Cde;
    private DbsField ads_Prtcpnt_Adp_Ivc_Pymnt_Rule;
    private DbsField ads_Prtcpnt_Adp_Hold_Cde;

    private DbsGroup ads_Prtcpnt_Adp_Frm_Irc_Dta;
    private DbsField ads_Prtcpnt_Adp_Frm_Irc_Cde;
    private DbsField ads_Prtcpnt_Adp_To_Irc_Cde;
    private DbsField ads_Prtcpnt_Adp_Ia_Tiaa_Nbr;
    private DbsField ads_Prtcpnt_Adp_Ia_Cref_Nbr;
    private DbsField ads_Prtcpnt_Adp_Roth_Plan_Type;
    private DbsField ads_Prtcpnt_Adp_Roth_Frst_Cntrbtn_Dte;
    private DbsField ads_Prtcpnt_Adp_Roth_Dsblty_Dte;
    private DbsField ads_Prtcpnt_Adp_Roth_Rqst_Ind;
    private DbsField ads_Prtcpnt_Adp_Srvvr_Ind;
    private DbsField ads_Prtcpnt_Adp_Srvvr_Rltnshp;
    private DbsField ads_Prtcpnt_Adp_Plan_Nbr;
    private DbsField ads_Prtcpnt_Adp_Mndtry_Tax_Ovrd;
    private DbsField ads_Prtcpnt_Adp_Bene_Client_Ind;

    private DataAccessProgramView vw_ads_Cntrct;
    private DbsField ads_Cntrct_Rqst_Id;
    private DbsField ads_Cntrct_Adc_Sqnce_Nbr;
    private DbsField ads_Cntrct_Adc_Stts_Cde;
    private DbsField ads_Cntrct_Adc_Lst_Actvty_Dte;
    private DbsField ads_Cntrct_Adc_Tiaa_Nbr;
    private DbsField ads_Cntrct_Adc_Cref_Nbr;

    private DbsGroup ads_Cntrct_Adc_Ppg_Data;
    private DbsField ads_Cntrct_Adc_Ppg_Cde;
    private DbsField ads_Cntrct_Adc_Ppg_Trgtd;

    private DbsGroup ads_Cntrct_Adc_Rqst_Info;
    private DbsField ads_Cntrct_Adc_Acct_Cde;
    private DbsField ads_Cntrct_Adc_Trk_Ro_Ivc;
    private DbsField ads_Cntrct_Adc_Sub_Plan_Nbr;

    private DbsGroup ads_Cntrct_Adc_T395_Fund_Request_Info;
    private DbsField ads_Cntrct_Adc_T395_Fund_Id;
    private DbsField ads_Cntrct_Adc_Original_Cntrct_Nbr;
    private DbsField ads_Cntrct_Adc_Original_Sub_Plan_Nbr;
    private DbsGroup ads_Cntrct_Adc_Invstmnt_Grpng_IdMuGroup;
    private DbsField ads_Cntrct_Adc_Invstmnt_Grpng_Id;

    private DataAccessProgramView vw_iaa_Cntrl_Rcrd_1;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Cde;
    private DbsField iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde;
    private DbsField pnd_Adi_Finl_Periodic_Py_Dte;
    private DbsField pnd_Calc_Mthd;

    private DbsGroup pnd_Calc_Mthd__R_Field_1;
    private DbsField pnd_Calc_Mthd__Filler1;
    private DbsField pnd_Calc_Mthd_Pnd_C_Mthd;
    private DbsField pnd_Rc;
    private DbsField pnd_Final_Per_Pay_Dte;
    private DbsField pnd_Final_Pay_Dte;
    private DbsField pnd_Mode;

    private DbsGroup pnd_Mode__R_Field_2;
    private DbsField pnd_Mode__Filler2;
    private DbsField pnd_Mode_Pnd_Mode_Mm;
    private DbsField pnd_Nxt_Pmt_Dte;

    private DbsGroup pnd_Nxt_Pmt_Dte__R_Field_3;
    private DbsField pnd_Nxt_Pmt_Dte__Filler3;
    private DbsField pnd_Nxt_Pmt_Dte_Pnd_Nxt_Mm;

    private DbsGroup pnd_Nxt_Pmt_Dte__R_Field_4;
    private DbsField pnd_Nxt_Pmt_Dte_Pnd_Nxt_Pmt_Dte_N;
    private DbsField pnd_A;
    private DbsField pnd_B;
    private DbsField pnd_Max_Rate;
    private DbsField pnd_Max_Rslt;
    private DbsField pnd_More;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Factor_Dte;
    private DbsField pnd_Wdte6;
    private DbsField pnd_Wdte4;
    private DbsField pnd_Check_A6;
    private DbsField pnd_Instllmnt_A6;
    private DbsField pnd_Wdte8;

    private DbsGroup pnd_Wdte8__R_Field_5;
    private DbsField pnd_Wdte8_Pnd_W_Yyyy;
    private DbsField pnd_Wdte8_Pnd_W_Mm;
    private DbsField pnd_Wdte8_Pnd_W_Dd;
    private DbsField pnd_Asd_A;

    private DbsGroup pnd_Asd_A__R_Field_6;
    private DbsField pnd_Asd_A_Pnd_Asd_N;

    private DbsGroup pnd_Asd_A__R_Field_7;
    private DbsField pnd_Asd_A_Pnd_Asd_Yyyy;
    private DbsField pnd_Asd_A_Pnd_Asd_Mm;
    private DbsField pnd_Asd_A_Pnd_Asd_Dd;

    private DbsGroup pnd_Asd_A__R_Field_8;
    private DbsField pnd_Asd_A_Pnd_Asd_N6;
    private DbsField pnd_Io_Dte_A;

    private DbsGroup pnd_Io_Dte_A__R_Field_9;
    private DbsField pnd_Io_Dte_A_Pnd_Iod_Yyyy;
    private DbsField pnd_Io_Dte_A_Pnd_Iod_Mm;
    private DbsField pnd_Io_Dte_A_Pnd_Iod_Dd;
    private DbsField pnd_Next_Dte_A;

    private DbsGroup pnd_Next_Dte_A__R_Field_10;
    private DbsField pnd_Next_Dte_A_Pnd_Next_Dte_N;

    private DbsGroup pnd_Next_Dte_A__R_Field_11;
    private DbsField pnd_Next_Dte_A_Pnd_Next_N6;

    private DbsGroup pnd_Next_Dte_A__R_Field_12;
    private DbsField pnd_Next_Dte_A_Pnd_Next_Dte_Yyyy;
    private DbsField pnd_Next_Dte_A_Pnd_Next_Dte_Mm;
    private DbsField pnd_Next_Dte_A_Pnd_Next_Dte_Dd;
    private DbsField pnd_Stop_Dte_A;

    private DbsGroup pnd_Stop_Dte_A__R_Field_13;
    private DbsField pnd_Stop_Dte_A_Pnd_Stop_N6;

    private DbsGroup pnd_Stop_Dte_A__R_Field_14;
    private DbsField pnd_Stop_Dte_A_Pnd_Stop_Dte_N;

    private DbsGroup pnd_Stop_Dte_A__R_Field_15;
    private DbsField pnd_Stop_Dte_A_Pnd_Stop_Dte_Yyyy;
    private DbsField pnd_Stop_Dte_A_Pnd_Stop_Dte_Mm;
    private DbsField pnd_Stop_Dte_A_Pnd_Stop_Dte_Dd;
    private DbsField pnd_Acctg_Dte_A;

    private DbsGroup pnd_Acctg_Dte_A__R_Field_16;
    private DbsField pnd_Acctg_Dte_A_Pnd_Acctg_Dte_N;

    private DbsGroup pnd_Acctg_Dte_A__R_Field_17;
    private DbsField pnd_Acctg_Dte_A_Pnd_Acctg_N6;
    private DbsField pnd_Bsnss_Dte_A;

    private DbsGroup pnd_Bsnss_Dte_A__R_Field_18;
    private DbsField pnd_Bsnss_Dte_A_Pnd_Bsnss_Dte_N;
    private DbsField pnd_Factor_Mm;
    private DbsField pnd_Adi_Super1;

    private DbsGroup pnd_Adi_Super1__R_Field_19;

    private DbsGroup pnd_Adi_Super1_Super_Structure;
    private DbsField pnd_Adi_Super1_Rqst_Id;
    private DbsField pnd_Adi_Super1_Adi_Ia_Rcrd_Cde;
    private DbsField pnd_Adi_Super1_Adi_Sqnce_Nbr;
    private DbsField pnd_Adi_Super2;

    private DbsGroup pnd_Adi_Super2__R_Field_20;

    private DbsGroup pnd_Adi_Super2_Super_Structure2;
    private DbsField pnd_Adi_Super2_Rqst_Id;
    private DbsField pnd_Adi_Super2_Adi_Ia_Rcrd_Cde;
    private DbsField pnd_Adi_Super2_Adi_Sqnce_Nbr;
    private DbsField pnd_Ret_Cde;
    private DbsField pnd_Cntrl_Isn;
    private DbsField pnd_Tiaa;
    private DbsField pnd_Cref;
    private DbsField pnd_First;
    private DbsField pnd_Cmpny;
    private DbsField pnd_No_Pays;
    private DbsField pnd_No_Per_Yr;
    private DbsField pnd_Dob;

    private DbsGroup pnd_Dob__R_Field_21;
    private DbsField pnd_Dob_Pnd_Dob_Yyyy;
    private DbsField pnd_Dob_Pnd_Dob_Mm;
    private DbsField pnd_Dob_Pnd_Dob_Dd;

    private DbsGroup pnd_Dob__R_Field_22;
    private DbsField pnd_Dob_Pnd_Dob_N;
    private DbsField pnd_Dob2;

    private DbsGroup pnd_Dob2__R_Field_23;
    private DbsField pnd_Dob2_Pnd_Dob2_Yyyy;
    private DbsField pnd_Dob2_Pnd_Dob2_Mm;
    private DbsField pnd_Dob2_Pnd_Dob2_Dd;

    private DbsGroup pnd_Dob2__R_Field_24;
    private DbsField pnd_Dob2_Pnd_Dob2_N;
    private DbsField pnd_Last_Inst_Dte_A;

    private DbsGroup pnd_Last_Inst_Dte_A__R_Field_25;
    private DbsField pnd_Last_Inst_Dte_A_Pnd_Last_Inst_Yyyy;
    private DbsField pnd_Last_Inst_Dte_A_Pnd_Last_Inst_Mm;
    private DbsField pnd_Last_Inst_Dte_A_Pnd_Last_Inst_Dd;
    private DbsField pnd_C_Reval_Dte_A;

    private DbsGroup pnd_C_Reval_Dte_A__R_Field_26;
    private DbsField pnd_C_Reval_Dte_A_Pnd_C_Reval_Dte_6;

    private DbsGroup pnd_C_Reval_Dte_A__R_Field_27;
    private DbsField pnd_C_Reval_Dte_A_Pnd_C_Reval_Yyyy;
    private DbsField pnd_C_Reval_Dte_A_Pnd_C_Reval_Mm;
    private DbsField pnd_C_Reval_Dte_A_Pnd_C_Reval_Dd;
    private DbsField pnd_T_Reval_Dte_A;

    private DbsGroup pnd_T_Reval_Dte_A__R_Field_28;
    private DbsField pnd_T_Reval_Dte_A_Pnd_T_Reval_Dte_6;

    private DbsGroup pnd_T_Reval_Dte_A__R_Field_29;
    private DbsField pnd_T_Reval_Dte_A_Pnd_T_Reval_Yyyy;
    private DbsField pnd_T_Reval_Dte_A_Pnd_T_Reval_Mm;
    private DbsField pnd_T_Reval_Dte_A_Pnd_T_Reval_Dd;
    private DbsField pnd_Tiaa_Ivc;
    private DbsField pnd_Cref_Ivc;
    private DbsField pnd_Name;
    private DbsField pnd_F_Or_N;
    private DbsField pnd_Rtb_Inst_Added;
    private DbsField pnd_Revalue_Tiaa;
    private DbsField pnd_Revalue_Cref;
    private DbsField pnd_Tiaa_Ok;
    private DbsField pnd_Cref_Ok;
    private DbsField pnd_Callnat;
    private DbsField pnd_Sim_Rule;
    private DbsField pnd_Cntrct_Orgn_Cde;
    private DbsField pnd_Sim_Age;
    private DbsField pnd_Cntrct_Py_Dte_Key;

    private DbsGroup pnd_Cntrct_Py_Dte_Key__R_Field_30;

    private DbsGroup pnd_Cntrct_Py_Dte_Key_Hist_Structure;
    private DbsField pnd_Cntrct_Py_Dte_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Py_Dte_Key_Cntrct_Payee_Cde;
    private DbsField pnd_Cntrct_Py_Dte_Key_Fund_Invrse_Lst_Pd_Dte;
    private DbsField pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde;

    private DbsGroup pnd_Cntrct_Py_Dte_Key__R_Field_31;
    private DbsField pnd_Cntrct_Py_Dte_Key__Filler4;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Cde;
    private DbsField pnd_T;
    private DbsField pnd_C;
    private DbsField pnd_Len;
    private DbsField pnd_Dest_Cde;
    private DbsField pnd_Ivc;
    private DbsField pnd_Periodic_Ivc;
    private DbsField cpnd_Adi_Dtl_Tiaa_Data;
    private DbsField pnd_Tiaa_Std_Grntd_Amt;
    private DbsField pnd_Tiaa_Std_Dvdnd_Amt;
    private DbsField pnd_Isn_Pos;

    private DbsGroup pnd_Rstl_Isn;
    private DbsField pnd_Rstl_Isn_Pnd_Pp_Isn;
    private DbsField pnd_Rstl_Isn_Pnd_Pp1_Isn;
    private DbsField pnd_Ivc_Call;
    private DbsField pnd_Sve_Ivc_Pymnt_Rule;
    private DbsField pnd_Rtn_Cde;
    private DbsField pls_Sub_Sw;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAdsa600 = new PdaAdsa600(localVariables);
        pdaNaza600i = new PdaNaza600i(localVariables);
        pdaFcpa140f = new PdaFcpa140f(localVariables);
        pdaAiaa0380 = new PdaAiaa0380(localVariables);
        pdaAiaa025 = new PdaAiaa025(localVariables);
        pdaAiaa026 = new PdaAiaa026(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_Rslt_Isn = parameters.newFieldArrayInRecord("pnd_Rslt_Isn", "#RSLT-ISN", FieldType.PACKED_DECIMAL, 8, new DbsArrayController(1, 2));
        pnd_Rslt_Isn.setParameterOption(ParameterOption.ByReference);
        pnd_Prtcpnt_Isn = parameters.newFieldInRecord("pnd_Prtcpnt_Isn", "#PRTCPNT-ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Prtcpnt_Isn.setParameterOption(ParameterOption.ByReference);
        pnd_Da_Cntrct_Isn = parameters.newFieldInRecord("pnd_Da_Cntrct_Isn", "#DA-CNTRCT-ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Da_Cntrct_Isn.setParameterOption(ParameterOption.ByReference);
        pnd_Bsnss_Dte = parameters.newFieldInRecord("pnd_Bsnss_Dte", "#BSNSS-DTE", FieldType.DATE);
        pnd_Bsnss_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Acctg_Dte = parameters.newFieldInRecord("pnd_Acctg_Dte", "#ACCTG-DTE", FieldType.DATE);
        pnd_Acctg_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Orgnl_State = parameters.newFieldInRecord("pnd_Orgnl_State", "#ORGNL-STATE", FieldType.STRING, 2);
        pnd_Orgnl_State.setParameterOption(ParameterOption.ByReference);
        pnd_Adp_Alt_Dest_Hold_Cde = parameters.newFieldInRecord("pnd_Adp_Alt_Dest_Hold_Cde", "#ADP-ALT-DEST-HOLD-CDE", FieldType.STRING, 4);
        pnd_Adp_Alt_Dest_Hold_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Adp_Crrspndnce_Perm_Addr_Txt = parameters.newFieldArrayInRecord("pnd_Adp_Crrspndnce_Perm_Addr_Txt", "#ADP-CRRSPNDNCE-PERM-ADDR-TXT", FieldType.STRING, 
            35, new DbsArrayController(1, 5));
        pnd_Adp_Crrspndnce_Perm_Addr_Txt.setParameterOption(ParameterOption.ByReference);
        pnd_Adp_Crrspndnce_Perm_Addr_Zip = parameters.newFieldInRecord("pnd_Adp_Crrspndnce_Perm_Addr_Zip", "#ADP-CRRSPNDNCE-PERM-ADDR-ZIP", FieldType.STRING, 
            9);
        pnd_Adp_Crrspndnce_Perm_Addr_Zip.setParameterOption(ParameterOption.ByReference);
        pnd_Adp_Crrspndnce_Perm_Addr_Typ_Cd = parameters.newFieldInRecord("pnd_Adp_Crrspndnce_Perm_Addr_Typ_Cd", "#ADP-CRRSPNDNCE-PERM-ADDR-TYP-CD", FieldType.STRING, 
            1);
        pnd_Adp_Crrspndnce_Perm_Addr_Typ_Cd.setParameterOption(ParameterOption.ByReference);
        pnd_Prdc_Ivc = parameters.newFieldInRecord("pnd_Prdc_Ivc", "#PRDC-IVC", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Prdc_Ivc.setParameterOption(ParameterOption.ByReference);
        pnd_Ivc_Pymnt_Rule = parameters.newFieldInRecord("pnd_Ivc_Pymnt_Rule", "#IVC-PYMNT-RULE", FieldType.STRING, 1);
        pnd_Ivc_Pymnt_Rule.setParameterOption(ParameterOption.ByReference);
        pnd_Err_Cde_X = parameters.newFieldInRecord("pnd_Err_Cde_X", "#ERR-CDE-X", FieldType.STRING, 3);
        pnd_Err_Cde_X.setParameterOption(ParameterOption.ByReference);
        pnd_Program_Nme = parameters.newFieldInRecord("pnd_Program_Nme", "#PROGRAM-NME", FieldType.STRING, 8);
        pnd_Program_Nme.setParameterOption(ParameterOption.ByReference);
        pnd_Err_Nr = parameters.newFieldInRecord("pnd_Err_Nr", "#ERR-NR", FieldType.STRING, 4);
        pnd_Err_Nr.setParameterOption(ParameterOption.ByReference);
        pnd_Err_Line_X = parameters.newFieldInRecord("pnd_Err_Line_X", "#ERR-LINE-X", FieldType.STRING, 4);
        pnd_Err_Line_X.setParameterOption(ParameterOption.ByReference);
        pnd_Ia_Orgn_Code = parameters.newFieldInRecord("pnd_Ia_Orgn_Code", "#IA-ORGN-CODE", FieldType.NUMERIC, 2);
        pnd_Ia_Orgn_Code.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_ads_Ia_Rslt = new DataAccessProgramView(new NameInfo("vw_ads_Ia_Rslt", "ADS-IA-RSLT"), "ADS_IA_RSLT", "ADS_IA_RSLT", DdmPeriodicGroups.getInstance().getGroups("ADS_IA_RSLT"));
        ads_Ia_Rslt_Rqst_Id = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "RQST_ID");
        ads_Ia_Rslt_Adi_Ia_Rcrd_Cde = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Ia_Rcrd_Cde", "ADI-IA-RCRD-CDE", FieldType.STRING, 3, 
            RepeatingFieldStrategy.None, "ADI_IA_RCRD_CDE");
        ads_Ia_Rslt_Adi_Sqnce_Nbr = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Sqnce_Nbr", "ADI-SQNCE-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "ADI_SQNCE_NBR");
        ads_Ia_Rslt_Adi_Stts_Cde = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Stts_Cde", "ADI-STTS-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "ADI_STTS_CDE");
        ads_Ia_Rslt_Adi_Tiaa_NbrsMuGroup = vw_ads_Ia_Rslt.getRecord().newGroupInGroup("ADS_IA_RSLT_ADI_TIAA_NBRSMuGroup", "ADI_TIAA_NBRSMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "ADS_IA_RSLT_ADI_TIAA_NBRS");
        ads_Ia_Rslt_Adi_Tiaa_Nbrs = ads_Ia_Rslt_Adi_Tiaa_NbrsMuGroup.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Tiaa_Nbrs", "ADI-TIAA-NBRS", FieldType.STRING, 
            10, new DbsArrayController(1, 12), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_TIAA_NBRS");
        ads_Ia_Rslt_Adi_Cref_NbrsMuGroup = vw_ads_Ia_Rslt.getRecord().newGroupInGroup("ADS_IA_RSLT_ADI_CREF_NBRSMuGroup", "ADI_CREF_NBRSMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "ADS_IA_RSLT_ADI_CREF_NBRS");
        ads_Ia_Rslt_Adi_Cref_Nbrs = ads_Ia_Rslt_Adi_Cref_NbrsMuGroup.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Cref_Nbrs", "ADI-CREF-NBRS", FieldType.STRING, 
            10, new DbsArrayController(1, 12), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_CREF_NBRS");
        ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr", "ADI-IA-TIAA-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "ADI_IA_TIAA_NBR");
        ads_Ia_Rslt_Adi_Ia_Tiaa_Payee_Cde = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Ia_Tiaa_Payee_Cde", "ADI-IA-TIAA-PAYEE-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADI_IA_TIAA_PAYEE_CDE");
        ads_Ia_Rslt_Adi_Ia_Cref_Nbr = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Ia_Cref_Nbr", "ADI-IA-CREF-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "ADI_IA_CREF_NBR");
        ads_Ia_Rslt_Adi_Ia_Cref_Payee_Cde = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Ia_Cref_Payee_Cde", "ADI-IA-CREF-PAYEE-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADI_IA_CREF_PAYEE_CDE");
        ads_Ia_Rslt_Adi_Curncy_Cde = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Curncy_Cde", "ADI-CURNCY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ADI_CURNCY_CDE");
        ads_Ia_Rslt_Adi_Pymnt_Cde = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Pymnt_Cde", "ADI-PYMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ADI_PYMNT_CDE");
        ads_Ia_Rslt_Adi_Pymnt_Mode = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Pymnt_Mode", "ADI-PYMNT-MODE", FieldType.NUMERIC, 3, 
            RepeatingFieldStrategy.None, "ADI_PYMNT_MODE");
        ads_Ia_Rslt_Adi_Lst_Actvty_Dte = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Lst_Actvty_Dte", "ADI-LST-ACTVTY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_LST_ACTVTY_DTE");
        ads_Ia_Rslt_Adi_Annty_Strt_Dte = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Annty_Strt_Dte", "ADI-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_ANNTY_STRT_DTE");
        ads_Ia_Rslt_Adi_Instllmnt_Dte = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Instllmnt_Dte", "ADI-INSTLLMNT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_INSTLLMNT_DTE");
        ads_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte", "ADI-FRST-PYMNT-DUE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADI_FRST_PYMNT_DUE_DTE");
        ads_Ia_Rslt_Adi_Frst_Ck_Pd_Dte = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Frst_Ck_Pd_Dte", "ADI-FRST-CK-PD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_FRST_CK_PD_DTE");
        ads_Ia_Rslt_Adi_Finl_Periodic_Py_Dte = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Finl_Periodic_Py_Dte", "ADI-FINL-PERIODIC-PY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADI_FINL_PERIODIC_PY_DTE");
        ads_Ia_Rslt_Adi_Finl_Prtl_Py_Dte = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Finl_Prtl_Py_Dte", "ADI-FINL-PRTL-PY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_FINL_PRTL_PY_DTE");

        ads_Ia_Rslt_Adi_Ivc_Data = vw_ads_Ia_Rslt.getRecord().newGroupInGroup("ADS_IA_RSLT_ADI_IVC_DATA", "ADI-IVC-DATA");
        ads_Ia_Rslt_Adi_Tiaa_Ivc_Amt = ads_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rslt_Adi_Tiaa_Ivc_Amt", "ADI-TIAA-IVC-AMT", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "ADI_TIAA_IVC_AMT");
        ads_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind = ads_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind", "ADI-TIAA-IVC-GD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_TIAA_IVC_GD_IND");
        ads_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt = ads_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt", "ADI-TIAA-PRDC-IVC-AMT", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "ADI_TIAA_PRDC_IVC_AMT");
        ads_Ia_Rslt_Adi_Cref_Ivc_Amt = ads_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rslt_Adi_Cref_Ivc_Amt", "ADI-CREF-IVC-AMT", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "ADI_CREF_IVC_AMT");
        ads_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind = ads_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind", "ADI-CREF-IVC-GD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_CREF_IVC_GD_IND");
        ads_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt = ads_Ia_Rslt_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt", "ADI-CREF-PRDC-IVC-AMT", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "ADI_CREF_PRDC_IVC_AMT");
        ads_Ia_Rslt_Count_Castadi_Dtl_Tiaa_Data = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Count_Castadi_Dtl_Tiaa_Data", "C*ADI-DTL-TIAA-DATA", 
            RepeatingFieldStrategy.CAsteriskVariable, "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");

        ads_Ia_Rslt_Adi_Dtl_Tiaa_Data = vw_ads_Ia_Rslt.getRecord().newGroupInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Data", "ADI-DTL-TIAA-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Rate_Cd = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Rate_Cd", "ADI-DTL-TIAA-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Std_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Std_Amt", "ADI-DTL-TIAA-DA-STD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Grd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Grd_Amt", "ADI-DTL-TIAA-DA-GRD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Acct_Cd = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Acct_Cd", "ADI-DTL-TIAA-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_ACCT_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd", "ADI-DTL-TIAA-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_IA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Eff_Rte_Intrst = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Eff_Rte_Intrst", "ADI-DTL-TIAA-EFF-RTE-INTRST", 
            FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_EFF_RTE_INTRST", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt", "ADI-DTL-TIAA-GRD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt", "ADI-DTL-TIAA-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt", "ADI-DTL-FNL-GRD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt", "ADI-DTL-FNL-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt", "ADI-DTL-TIAA-STD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt", "ADI-DTL-TIAA-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt", "ADI-DTL-FNL-STD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt = ads_Ia_Rslt_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt", "ADI-DTL-FNL-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");

        ads_Ia_Rslt_Adi_Dtl_Cref_Data = vw_ads_Ia_Rslt.getRecord().newGroupInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Data", "ADI-DTL-CREF-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Da_Rate_Cd = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Da_Rate_Cd", "ADI-DTL-CREF-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_DA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd", "ADI-DTL-CREF-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_ACCT_CD", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd", "ADI-DTL-CREF-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_IA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt", "ADI-DTL-CREF-DA-ANNL-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_DA_ANNL_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units", "ADI-DTL-CREF-ANNL-NBR-UNITS", 
            FieldType.NUMERIC, 10, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_ANNL_NBR_UNITS", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val", "ADI-DTL-CREF-IA-ANNL-UNIT-VAL", 
            FieldType.NUMERIC, 7, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_IA_ANNL_UNIT_VAL", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Amt = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Amt", "ADI-DTL-CREF-ANNL-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_ANNL_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt", "ADI-DTL-CREF-DA-MNTHLY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_DA_MNTHLY_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units", "ADI-DTL-CREF-MNTHLY-NBR-UNITS", 
            FieldType.NUMERIC, 10, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_MNTHLY_NBR_UNITS", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val", 
            "ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL", FieldType.NUMERIC, 7, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_CREF_IA_MNTHLY_UNIT_VAL", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Amt = ads_Ia_Rslt_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Amt", "ADI-DTL-CREF-MNTHLY-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_MNTHLY_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rslt_Adi_Optn_Cde = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Optn_Cde", "ADI-OPTN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "ADI_OPTN_CDE");
        ads_Ia_Rslt_Adi_Tiaa_Sttlmnt = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Tiaa_Sttlmnt", "ADI-TIAA-STTLMNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_TIAA_STTLMNT");
        ads_Ia_Rslt_Adi_Tiaa_Re_Sttlmnt = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Tiaa_Re_Sttlmnt", "ADI-TIAA-RE-STTLMNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_TIAA_RE_STTLMNT");
        ads_Ia_Rslt_Adi_Cref_Sttlmnt = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Cref_Sttlmnt", "ADI-CREF-STTLMNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_CREF_STTLMNT");
        ads_Ia_Rslt_Adi_Orig_Lob_IndMuGroup = vw_ads_Ia_Rslt.getRecord().newGroupInGroup("ADS_IA_RSLT_ADI_ORIG_LOB_INDMuGroup", "ADI_ORIG_LOB_INDMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_IA_RSLT_ADI_ORIG_LOB_IND");
        ads_Ia_Rslt_Adi_Orig_Lob_Ind = ads_Ia_Rslt_Adi_Orig_Lob_IndMuGroup.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Orig_Lob_Ind", "ADI-ORIG-LOB-IND", FieldType.STRING, 
            1, new DbsArrayController(1, 12), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_ORIG_LOB_IND");
        ads_Ia_Rslt_Adi_Roth_Rqst_Ind = vw_ads_Ia_Rslt.getRecord().newFieldInGroup("ads_Ia_Rslt_Adi_Roth_Rqst_Ind", "ADI-ROTH-RQST-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_ROTH_RQST_IND");
        ads_Ia_Rslt_Adi_Contract_IdMuGroup = vw_ads_Ia_Rslt.getRecord().newGroupInGroup("ADS_IA_RSLT_ADI_CONTRACT_IDMuGroup", "ADI_CONTRACT_IDMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_IA_RSLT_ADI_CONTRACT_ID");
        ads_Ia_Rslt_Adi_Contract_Id = ads_Ia_Rslt_Adi_Contract_IdMuGroup.newFieldArrayInGroup("ads_Ia_Rslt_Adi_Contract_Id", "ADI-CONTRACT-ID", FieldType.NUMERIC, 
            11, new DbsArrayController(1, 125), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_CONTRACT_ID");
        registerRecord(vw_ads_Ia_Rslt);

        vw_ads_Ia_Rsl2 = new DataAccessProgramView(new NameInfo("vw_ads_Ia_Rsl2", "ADS-IA-RSL2"), "ADS_IA_RSLT", "ADS_IA_RSLT", DdmPeriodicGroups.getInstance().getGroups("ADS_IA_RSLT"));
        ads_Ia_Rsl2_Rqst_Id = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "RQST_ID");
        ads_Ia_Rsl2_Adi_Ia_Rcrd_Cde = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Ia_Rcrd_Cde", "ADI-IA-RCRD-CDE", FieldType.STRING, 3, 
            RepeatingFieldStrategy.None, "ADI_IA_RCRD_CDE");
        ads_Ia_Rsl2_Adi_Sqnce_Nbr = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Sqnce_Nbr", "ADI-SQNCE-NBR", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "ADI_SQNCE_NBR");
        ads_Ia_Rsl2_Adi_Stts_Cde = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Stts_Cde", "ADI-STTS-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "ADI_STTS_CDE");
        ads_Ia_Rsl2_Adi_Tiaa_NbrsMuGroup = vw_ads_Ia_Rsl2.getRecord().newGroupInGroup("ADS_IA_RSL2_ADI_TIAA_NBRSMuGroup", "ADI_TIAA_NBRSMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "ADS_IA_RSLT_ADI_TIAA_NBRS");
        ads_Ia_Rsl2_Adi_Tiaa_Nbrs = ads_Ia_Rsl2_Adi_Tiaa_NbrsMuGroup.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Tiaa_Nbrs", "ADI-TIAA-NBRS", FieldType.STRING, 
            10, new DbsArrayController(1, 12), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_TIAA_NBRS");
        ads_Ia_Rsl2_Adi_Cref_NbrsMuGroup = vw_ads_Ia_Rsl2.getRecord().newGroupInGroup("ADS_IA_RSL2_ADI_CREF_NBRSMuGroup", "ADI_CREF_NBRSMuGroup", RepeatingFieldStrategy.SubTableFieldArray, 
            "ADS_IA_RSLT_ADI_CREF_NBRS");
        ads_Ia_Rsl2_Adi_Cref_Nbrs = ads_Ia_Rsl2_Adi_Cref_NbrsMuGroup.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Cref_Nbrs", "ADI-CREF-NBRS", FieldType.STRING, 
            10, new DbsArrayController(1, 12), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_CREF_NBRS");
        ads_Ia_Rsl2_Adi_Ia_Tiaa_Nbr = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Ia_Tiaa_Nbr", "ADI-IA-TIAA-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "ADI_IA_TIAA_NBR");
        ads_Ia_Rsl2_Adi_Ia_Tiaa_Payee_Cde = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Ia_Tiaa_Payee_Cde", "ADI-IA-TIAA-PAYEE-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADI_IA_TIAA_PAYEE_CDE");
        ads_Ia_Rsl2_Adi_Ia_Cref_Nbr = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Ia_Cref_Nbr", "ADI-IA-CREF-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "ADI_IA_CREF_NBR");
        ads_Ia_Rsl2_Adi_Ia_Cref_Payee_Cde = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Ia_Cref_Payee_Cde", "ADI-IA-CREF-PAYEE-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADI_IA_CREF_PAYEE_CDE");
        ads_Ia_Rsl2_Adi_Curncy_Cde = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Curncy_Cde", "ADI-CURNCY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ADI_CURNCY_CDE");
        ads_Ia_Rsl2_Adi_Pymnt_Cde = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Pymnt_Cde", "ADI-PYMNT-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ADI_PYMNT_CDE");
        ads_Ia_Rsl2_Adi_Pymnt_Mode = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Pymnt_Mode", "ADI-PYMNT-MODE", FieldType.NUMERIC, 3, 
            RepeatingFieldStrategy.None, "ADI_PYMNT_MODE");
        ads_Ia_Rsl2_Adi_Lst_Actvty_Dte = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Lst_Actvty_Dte", "ADI-LST-ACTVTY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_LST_ACTVTY_DTE");
        ads_Ia_Rsl2_Adi_Annty_Strt_Dte = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Annty_Strt_Dte", "ADI-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_ANNTY_STRT_DTE");
        ads_Ia_Rsl2_Adi_Instllmnt_Dte = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Instllmnt_Dte", "ADI-INSTLLMNT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_INSTLLMNT_DTE");
        ads_Ia_Rsl2_Adi_Frst_Pymnt_Due_Dte = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Frst_Pymnt_Due_Dte", "ADI-FRST-PYMNT-DUE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADI_FRST_PYMNT_DUE_DTE");
        ads_Ia_Rsl2_Adi_Frst_Ck_Pd_Dte = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Frst_Ck_Pd_Dte", "ADI-FRST-CK-PD-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_FRST_CK_PD_DTE");
        ads_Ia_Rsl2_Adi_Finl_Periodic_Py_Dte = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Finl_Periodic_Py_Dte", "ADI-FINL-PERIODIC-PY-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADI_FINL_PERIODIC_PY_DTE");
        ads_Ia_Rsl2_Adi_Finl_Prtl_Py_Dte = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Finl_Prtl_Py_Dte", "ADI-FINL-PRTL-PY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADI_FINL_PRTL_PY_DTE");

        ads_Ia_Rsl2_Adi_Ivc_Data = vw_ads_Ia_Rsl2.getRecord().newGroupInGroup("ADS_IA_RSL2_ADI_IVC_DATA", "ADI-IVC-DATA");
        ads_Ia_Rsl2_Adi_Tiaa_Ivc_Amt = ads_Ia_Rsl2_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rsl2_Adi_Tiaa_Ivc_Amt", "ADI-TIAA-IVC-AMT", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "ADI_TIAA_IVC_AMT");
        ads_Ia_Rsl2_Adi_Tiaa_Ivc_Gd_Ind = ads_Ia_Rsl2_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rsl2_Adi_Tiaa_Ivc_Gd_Ind", "ADI-TIAA-IVC-GD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_TIAA_IVC_GD_IND");
        ads_Ia_Rsl2_Adi_Tiaa_Prdc_Ivc_Amt = ads_Ia_Rsl2_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rsl2_Adi_Tiaa_Prdc_Ivc_Amt", "ADI-TIAA-PRDC-IVC-AMT", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "ADI_TIAA_PRDC_IVC_AMT");
        ads_Ia_Rsl2_Adi_Cref_Ivc_Amt = ads_Ia_Rsl2_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rsl2_Adi_Cref_Ivc_Amt", "ADI-CREF-IVC-AMT", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "ADI_CREF_IVC_AMT");
        ads_Ia_Rsl2_Adi_Cref_Ivc_Gd_Ind = ads_Ia_Rsl2_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rsl2_Adi_Cref_Ivc_Gd_Ind", "ADI-CREF-IVC-GD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_CREF_IVC_GD_IND");
        ads_Ia_Rsl2_Adi_Cref_Prdc_Ivc_Amt = ads_Ia_Rsl2_Adi_Ivc_Data.newFieldInGroup("ads_Ia_Rsl2_Adi_Cref_Prdc_Ivc_Amt", "ADI-CREF-PRDC-IVC-AMT", FieldType.NUMERIC, 
            9, 2, RepeatingFieldStrategy.None, "ADI_CREF_PRDC_IVC_AMT");
        ads_Ia_Rsl2_Count_Castadi_Dtl_Tiaa_Data = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Count_Castadi_Dtl_Tiaa_Data", "C*ADI-DTL-TIAA-DATA", 
            RepeatingFieldStrategy.CAsteriskVariable, "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");

        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data = vw_ads_Ia_Rsl2.getRecord().newGroupInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data", "ADI-DTL-TIAA-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Rate_Cd = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Rate_Cd", "ADI-DTL-TIAA-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt", "ADI-DTL-TIAA-DA-STD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_STD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt", "ADI-DTL-TIAA-DA-GRD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_DA_GRD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Acct_Cd = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Acct_Cd", "ADI-DTL-TIAA-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_ACCT_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd", "ADI-DTL-TIAA-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_IA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Eff_Rte_Intrst = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Eff_Rte_Intrst", "ADI-DTL-TIAA-EFF-RTE-INTRST", 
            FieldType.NUMERIC, 5, 3, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_EFF_RTE_INTRST", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt", "ADI-DTL-TIAA-GRD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt", "ADI-DTL-TIAA-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt", "ADI-DTL-FNL-GRD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt", "ADI-DTL-FNL-GRD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_GRD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt", "ADI-DTL-TIAA-STD-GRNTD-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_GRNTD_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt", "ADI-DTL-TIAA-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_TIAA_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt", "ADI-DTL-FNL-STD-PAY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_PAY_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt = ads_Ia_Rsl2_Adi_Dtl_Tiaa_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt", "ADI-DTL-FNL-STD-DVDND-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 125) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_FNL_STD_DVDND_AMT", "ADS_IA_RSLT_ADI_DTL_TIAA_DATA");

        ads_Ia_Rsl2_Adi_Dtl_Cref_Data = vw_ads_Ia_Rsl2.getRecord().newGroupInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Data", "ADI-DTL-CREF-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Da_Rate_Cd = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Da_Rate_Cd", "ADI-DTL-CREF-DA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_DA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Acct_Cd = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Acct_Cd", "ADI-DTL-CREF-ACCT-CD", 
            FieldType.STRING, 1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_ACCT_CD", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Ia_Rate_Cd = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Ia_Rate_Cd", "ADI-DTL-CREF-IA-RATE-CD", 
            FieldType.STRING, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_IA_RATE_CD", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Da_Annl_Amt = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Da_Annl_Amt", "ADI-DTL-CREF-DA-ANNL-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_DA_ANNL_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Annl_Nbr_Units = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Annl_Nbr_Units", "ADI-DTL-CREF-ANNL-NBR-UNITS", 
            FieldType.NUMERIC, 10, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_ANNL_NBR_UNITS", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Ia_Annl_Unit_Val = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Ia_Annl_Unit_Val", "ADI-DTL-CREF-IA-ANNL-UNIT-VAL", 
            FieldType.NUMERIC, 7, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_IA_ANNL_UNIT_VAL", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Annl_Amt = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Annl_Amt", "ADI-DTL-CREF-ANNL-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_ANNL_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Da_Mnthly_Amt = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Da_Mnthly_Amt", "ADI-DTL-CREF-DA-MNTHLY-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_DA_MNTHLY_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Mnthly_Nbr_Units = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Mnthly_Nbr_Units", "ADI-DTL-CREF-MNTHLY-NBR-UNITS", 
            FieldType.NUMERIC, 10, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_MNTHLY_NBR_UNITS", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val", 
            "ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL", FieldType.NUMERIC, 7, 4, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADI_DTL_CREF_IA_MNTHLY_UNIT_VAL", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Dtl_Cref_Mnthly_Amt = ads_Ia_Rsl2_Adi_Dtl_Cref_Data.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Dtl_Cref_Mnthly_Amt", "ADI-DTL-CREF-MNTHLY-AMT", 
            FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADI_DTL_CREF_MNTHLY_AMT", "ADS_IA_RSLT_ADI_DTL_CREF_DATA");
        ads_Ia_Rsl2_Adi_Optn_Cde = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Optn_Cde", "ADI-OPTN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "ADI_OPTN_CDE");
        ads_Ia_Rsl2_Adi_Tiaa_Sttlmnt = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Tiaa_Sttlmnt", "ADI-TIAA-STTLMNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_TIAA_STTLMNT");
        ads_Ia_Rsl2_Adi_Tiaa_Re_Sttlmnt = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Tiaa_Re_Sttlmnt", "ADI-TIAA-RE-STTLMNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_TIAA_RE_STTLMNT");
        ads_Ia_Rsl2_Adi_Cref_Sttlmnt = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Cref_Sttlmnt", "ADI-CREF-STTLMNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_CREF_STTLMNT");
        ads_Ia_Rsl2_Adi_Orig_Lob_IndMuGroup = vw_ads_Ia_Rsl2.getRecord().newGroupInGroup("ADS_IA_RSL2_ADI_ORIG_LOB_INDMuGroup", "ADI_ORIG_LOB_INDMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_IA_RSLT_ADI_ORIG_LOB_IND");
        ads_Ia_Rsl2_Adi_Orig_Lob_Ind = ads_Ia_Rsl2_Adi_Orig_Lob_IndMuGroup.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Orig_Lob_Ind", "ADI-ORIG-LOB-IND", FieldType.STRING, 
            1, new DbsArrayController(1, 12), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_ORIG_LOB_IND");
        ads_Ia_Rsl2_Adi_Roth_Rqst_Ind = vw_ads_Ia_Rsl2.getRecord().newFieldInGroup("ads_Ia_Rsl2_Adi_Roth_Rqst_Ind", "ADI-ROTH-RQST-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADI_ROTH_RQST_IND");
        ads_Ia_Rsl2_Adi_Contract_IdMuGroup = vw_ads_Ia_Rsl2.getRecord().newGroupInGroup("ADS_IA_RSL2_ADI_CONTRACT_IDMuGroup", "ADI_CONTRACT_IDMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_IA_RSLT_ADI_CONTRACT_ID");
        ads_Ia_Rsl2_Adi_Contract_Id = ads_Ia_Rsl2_Adi_Contract_IdMuGroup.newFieldArrayInGroup("ads_Ia_Rsl2_Adi_Contract_Id", "ADI-CONTRACT-ID", FieldType.NUMERIC, 
            11, new DbsArrayController(1, 125), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADI_CONTRACT_ID");
        registerRecord(vw_ads_Ia_Rsl2);

        vw_ads_Prtcpnt = new DataAccessProgramView(new NameInfo("vw_ads_Prtcpnt", "ADS-PRTCPNT"), "ADS_PRTCPNT", "ADS_PRTCPNT", DdmPeriodicGroups.getInstance().getGroups("ADS_PRTCPNT"));
        ads_Prtcpnt_Adp_Frst_Annt_Dte_Of_Brth = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Frst_Annt_Dte_Of_Brth", "ADP-FRST-ANNT-DTE-OF-BRTH", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_DTE_OF_BRTH");
        ads_Prtcpnt_Adp_Scnd_Annt_Dte_Of_Brth = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Scnd_Annt_Dte_Of_Brth", "ADP-SCND-ANNT-DTE-OF-BRTH", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_DTE_OF_BRTH");
        ads_Prtcpnt_Adp_Annt_Typ_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Annt_Typ_Cde", "ADP-ANNT-TYP-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_ANNT_TYP_CDE");
        ads_Prtcpnt_Adp_Rsttlmnt_Cnt = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Rsttlmnt_Cnt", "ADP-RSTTLMNT-CNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_RSTTLMNT_CNT");
        ads_Prtcpnt_Adp_State_Of_Issue = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_State_Of_Issue", "ADP-STATE-OF-ISSUE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADP_STATE_OF_ISSUE");
        ads_Prtcpnt_Adp_Frst_Annt_Sex_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Frst_Annt_Sex_Cde", "ADP-FRST-ANNT-SEX-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_SEX_CDE");
        ads_Prtcpnt_Adp_Scnd_Annt_Sex_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Scnd_Annt_Sex_Cde", "ADP-SCND-ANNT-SEX-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_SEX_CDE");
        ads_Prtcpnt_Adp_Effctv_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Effctv_Dte", "ADP-EFFCTV-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADP_EFFCTV_DTE");
        ads_Prtcpnt_Adp_Alt_Dest_Rlvr_Dest = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Rlvr_Dest", "ADP-ALT-DEST-RLVR-DEST", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADP_ALT_DEST_RLVR_DEST");
        ads_Prtcpnt_Adp_Alt_Dest_Acct_Nbr = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Acct_Nbr", "ADP-ALT-DEST-ACCT-NBR", FieldType.STRING, 
            21, RepeatingFieldStrategy.None, "ADP_ALT_DEST_ACCT_NBR");
        ads_Prtcpnt_Adp_Alt_Dest_Hold_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Hold_Cde", "ADP-ALT-DEST-HOLD-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADP_ALT_DEST_HOLD_CDE");
        ads_Prtcpnt_Adp_Ivc_Rlvr_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Ivc_Rlvr_Cde", "ADP-IVC-RLVR-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_IVC_RLVR_CDE");
        ads_Prtcpnt_Adp_Ivc_Pymnt_Rule = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Ivc_Pymnt_Rule", "ADP-IVC-PYMNT-RULE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_IVC_PYMNT_RULE");
        ads_Prtcpnt_Adp_Hold_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Hold_Cde", "ADP-HOLD-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADP_HOLD_CDE");

        ads_Prtcpnt_Adp_Frm_Irc_Dta = vw_ads_Prtcpnt.getRecord().newGroupInGroup("ads_Prtcpnt_Adp_Frm_Irc_Dta", "ADP-FRM-IRC-DTA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "ADS_PRTCPNT_ADP_FRM_IRC_DTA");
        ads_Prtcpnt_Adp_Frm_Irc_Cde = ads_Prtcpnt_Adp_Frm_Irc_Dta.newFieldArrayInGroup("ads_Prtcpnt_Adp_Frm_Irc_Cde", "ADP-FRM-IRC-CDE", FieldType.STRING, 
            2, new DbsArrayController(1, 4) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADP_FRM_IRC_CDE", "ADS_PRTCPNT_ADP_FRM_IRC_DTA");
        ads_Prtcpnt_Adp_To_Irc_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_To_Irc_Cde", "ADP-TO-IRC-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ADP_TO_IRC_CDE");
        ads_Prtcpnt_Adp_Ia_Tiaa_Nbr = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Ia_Tiaa_Nbr", "ADP-IA-TIAA-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "ADP_IA_TIAA_NBR");
        ads_Prtcpnt_Adp_Ia_Cref_Nbr = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Ia_Cref_Nbr", "ADP-IA-CREF-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "ADP_IA_CREF_NBR");
        ads_Prtcpnt_Adp_Roth_Plan_Type = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Roth_Plan_Type", "ADP-ROTH-PLAN-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_ROTH_PLAN_TYPE");
        ads_Prtcpnt_Adp_Roth_Frst_Cntrbtn_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Roth_Frst_Cntrbtn_Dte", "ADP-ROTH-FRST-CNTRBTN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_ROTH_FRST_CNTRBTN_DTE");
        ads_Prtcpnt_Adp_Roth_Dsblty_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Roth_Dsblty_Dte", "ADP-ROTH-DSBLTY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADP_ROTH_DSBLTY_DTE");
        ads_Prtcpnt_Adp_Roth_Rqst_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Roth_Rqst_Ind", "ADP-ROTH-RQST-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_ROTH_RQST_IND");
        ads_Prtcpnt_Adp_Srvvr_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Srvvr_Ind", "ADP-SRVVR-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ADP_SRVVR_IND");
        ads_Prtcpnt_Adp_Srvvr_Rltnshp = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Srvvr_Rltnshp", "ADP-SRVVR-RLTNSHP", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_SRVVR_RLTNSHP");
        ads_Prtcpnt_Adp_Plan_Nbr = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Plan_Nbr", "ADP-PLAN-NBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "ADP_PLAN_NBR");
        ads_Prtcpnt_Adp_Mndtry_Tax_Ovrd = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Mndtry_Tax_Ovrd", "ADP-MNDTRY-TAX-OVRD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_MNDTRY_TAX_OVRD");
        ads_Prtcpnt_Adp_Bene_Client_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Bene_Client_Ind", "ADP-BENE-CLIENT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_BENE_CLIENT_IND");
        registerRecord(vw_ads_Prtcpnt);

        vw_ads_Cntrct = new DataAccessProgramView(new NameInfo("vw_ads_Cntrct", "ADS-CNTRCT"), "ADS_CNTRCT", "DS_CNTRCT", DdmPeriodicGroups.getInstance().getGroups("ADS_CNTRCT"));
        ads_Cntrct_Rqst_Id = vw_ads_Cntrct.getRecord().newFieldInGroup("ads_Cntrct_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "RQST_ID");
        ads_Cntrct_Adc_Sqnce_Nbr = vw_ads_Cntrct.getRecord().newFieldInGroup("ads_Cntrct_Adc_Sqnce_Nbr", "ADC-SQNCE-NBR", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "ADC_SQNCE_NBR");
        ads_Cntrct_Adc_Stts_Cde = vw_ads_Cntrct.getRecord().newFieldInGroup("ads_Cntrct_Adc_Stts_Cde", "ADC-STTS-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "ADC_STTS_CDE");
        ads_Cntrct_Adc_Lst_Actvty_Dte = vw_ads_Cntrct.getRecord().newFieldInGroup("ads_Cntrct_Adc_Lst_Actvty_Dte", "ADC-LST-ACTVTY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADC_LST_ACTVTY_DTE");
        ads_Cntrct_Adc_Tiaa_Nbr = vw_ads_Cntrct.getRecord().newFieldInGroup("ads_Cntrct_Adc_Tiaa_Nbr", "ADC-TIAA-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "ADC_TIAA_NBR");
        ads_Cntrct_Adc_Cref_Nbr = vw_ads_Cntrct.getRecord().newFieldInGroup("ads_Cntrct_Adc_Cref_Nbr", "ADC-CREF-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "ADC_CREF_NBR");

        ads_Cntrct_Adc_Ppg_Data = vw_ads_Cntrct.getRecord().newGroupInGroup("ads_Cntrct_Adc_Ppg_Data", "ADC-PPG-DATA", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "DS_CNTRCT_ADC_PPG_DATA");
        ads_Cntrct_Adc_Ppg_Cde = ads_Cntrct_Adc_Ppg_Data.newFieldArrayInGroup("ads_Cntrct_Adc_Ppg_Cde", "ADC-PPG-CDE", FieldType.STRING, 6, new DbsArrayController(1, 
            15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADC_PPG_CDE", "DS_CNTRCT_ADC_PPG_DATA");
        ads_Cntrct_Adc_Ppg_Trgtd = ads_Cntrct_Adc_Ppg_Data.newFieldArrayInGroup("ads_Cntrct_Adc_Ppg_Trgtd", "ADC-PPG-TRGTD", FieldType.STRING, 1, new 
            DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADC_PPG_TRGTD", "DS_CNTRCT_ADC_PPG_DATA");

        ads_Cntrct_Adc_Rqst_Info = vw_ads_Cntrct.getRecord().newGroupInGroup("ads_Cntrct_Adc_Rqst_Info", "ADC-RQST-INFO", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "DS_CNTRCT_ADC_RQST_INFO");
        ads_Cntrct_Adc_Acct_Cde = ads_Cntrct_Adc_Rqst_Info.newFieldArrayInGroup("ads_Cntrct_Adc_Acct_Cde", "ADC-ACCT-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADC_ACCT_CDE", "DS_CNTRCT_ADC_RQST_INFO");
        ads_Cntrct_Adc_Trk_Ro_Ivc = vw_ads_Cntrct.getRecord().newFieldInGroup("ads_Cntrct_Adc_Trk_Ro_Ivc", "ADC-TRK-RO-IVC", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "ADC_TRK_RO_IVC");
        ads_Cntrct_Adc_Sub_Plan_Nbr = vw_ads_Cntrct.getRecord().newFieldInGroup("ads_Cntrct_Adc_Sub_Plan_Nbr", "ADC-SUB-PLAN-NBR", FieldType.STRING, 6, 
            RepeatingFieldStrategy.None, "ADC_SUB_PLAN_NBR");

        ads_Cntrct_Adc_T395_Fund_Request_Info = vw_ads_Cntrct.getRecord().newGroupInGroup("ads_Cntrct_Adc_T395_Fund_Request_Info", "ADC-T395-FUND-REQUEST-INFO", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "DS_CNTRCT_ADC_T395_FUND_REQUEST_INFO");
        ads_Cntrct_Adc_T395_Fund_Id = ads_Cntrct_Adc_T395_Fund_Request_Info.newFieldArrayInGroup("ads_Cntrct_Adc_T395_Fund_Id", "ADC-T395-FUND-ID", FieldType.STRING, 
            2, new DbsArrayController(1, 14) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "ADC_T395_FUND_ID", "DS_CNTRCT_ADC_T395_FUND_REQUEST_INFO");
        ads_Cntrct_Adc_Original_Cntrct_Nbr = vw_ads_Cntrct.getRecord().newFieldInGroup("ads_Cntrct_Adc_Original_Cntrct_Nbr", "ADC-ORIGINAL-CNTRCT-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "ADC_ORIGINAL_CNTRCT_NBR");
        ads_Cntrct_Adc_Original_Sub_Plan_Nbr = vw_ads_Cntrct.getRecord().newFieldInGroup("ads_Cntrct_Adc_Original_Sub_Plan_Nbr", "ADC-ORIGINAL-SUB-PLAN-NBR", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "ADC_ORIGINAL_SUB_PLAN_NBR");
        ads_Cntrct_Adc_Invstmnt_Grpng_IdMuGroup = vw_ads_Cntrct.getRecord().newGroupInGroup("ADS_CNTRCT_ADC_INVSTMNT_GRPNG_IDMuGroup", "ADC_INVSTMNT_GRPNG_IDMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "DS_CNTRCT_ADC_INVSTMNT_GRPNG_ID");
        ads_Cntrct_Adc_Invstmnt_Grpng_Id = ads_Cntrct_Adc_Invstmnt_Grpng_IdMuGroup.newFieldArrayInGroup("ads_Cntrct_Adc_Invstmnt_Grpng_Id", "ADC-INVSTMNT-GRPNG-ID", 
            FieldType.STRING, 4, new DbsArrayController(1, 20), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADC_INVSTMNT_GRPNG_ID");
        registerRecord(vw_ads_Cntrct);

        vw_iaa_Cntrl_Rcrd_1 = new DataAccessProgramView(new NameInfo("vw_iaa_Cntrl_Rcrd_1", "IAA-CNTRL-RCRD-1"), "IAA_CNTRL_RCRD_1", "IA_TRANS_FILE");
        iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte", "CNTRL-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRL_CHECK_DTE");
        iaa_Cntrl_Rcrd_1_Cntrl_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Cde", "CNTRL-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRL_CDE");
        iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde = vw_iaa_Cntrl_Rcrd_1.getRecord().newFieldInGroup("iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde", "CNTRL-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRL_ACTVTY_CDE");
        registerRecord(vw_iaa_Cntrl_Rcrd_1);

        pnd_Adi_Finl_Periodic_Py_Dte = localVariables.newFieldInRecord("pnd_Adi_Finl_Periodic_Py_Dte", "#ADI-FINL-PERIODIC-PY-DTE", FieldType.DATE);
        pnd_Calc_Mthd = localVariables.newFieldInRecord("pnd_Calc_Mthd", "#CALC-MTHD", FieldType.STRING, 9);

        pnd_Calc_Mthd__R_Field_1 = localVariables.newGroupInRecord("pnd_Calc_Mthd__R_Field_1", "REDEFINE", pnd_Calc_Mthd);
        pnd_Calc_Mthd__Filler1 = pnd_Calc_Mthd__R_Field_1.newFieldInGroup("pnd_Calc_Mthd__Filler1", "_FILLER1", FieldType.STRING, 8);
        pnd_Calc_Mthd_Pnd_C_Mthd = pnd_Calc_Mthd__R_Field_1.newFieldInGroup("pnd_Calc_Mthd_Pnd_C_Mthd", "#C-MTHD", FieldType.STRING, 1);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 2);
        pnd_Final_Per_Pay_Dte = localVariables.newFieldInRecord("pnd_Final_Per_Pay_Dte", "#FINAL-PER-PAY-DTE", FieldType.STRING, 6);
        pnd_Final_Pay_Dte = localVariables.newFieldInRecord("pnd_Final_Pay_Dte", "#FINAL-PAY-DTE", FieldType.STRING, 8);
        pnd_Mode = localVariables.newFieldInRecord("pnd_Mode", "#MODE", FieldType.NUMERIC, 3);

        pnd_Mode__R_Field_2 = localVariables.newGroupInRecord("pnd_Mode__R_Field_2", "REDEFINE", pnd_Mode);
        pnd_Mode__Filler2 = pnd_Mode__R_Field_2.newFieldInGroup("pnd_Mode__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Mode_Pnd_Mode_Mm = pnd_Mode__R_Field_2.newFieldInGroup("pnd_Mode_Pnd_Mode_Mm", "#MODE-MM", FieldType.STRING, 2);
        pnd_Nxt_Pmt_Dte = localVariables.newFieldInRecord("pnd_Nxt_Pmt_Dte", "#NXT-PMT-DTE", FieldType.STRING, 8);

        pnd_Nxt_Pmt_Dte__R_Field_3 = localVariables.newGroupInRecord("pnd_Nxt_Pmt_Dte__R_Field_3", "REDEFINE", pnd_Nxt_Pmt_Dte);
        pnd_Nxt_Pmt_Dte__Filler3 = pnd_Nxt_Pmt_Dte__R_Field_3.newFieldInGroup("pnd_Nxt_Pmt_Dte__Filler3", "_FILLER3", FieldType.STRING, 4);
        pnd_Nxt_Pmt_Dte_Pnd_Nxt_Mm = pnd_Nxt_Pmt_Dte__R_Field_3.newFieldInGroup("pnd_Nxt_Pmt_Dte_Pnd_Nxt_Mm", "#NXT-MM", FieldType.STRING, 2);

        pnd_Nxt_Pmt_Dte__R_Field_4 = localVariables.newGroupInRecord("pnd_Nxt_Pmt_Dte__R_Field_4", "REDEFINE", pnd_Nxt_Pmt_Dte);
        pnd_Nxt_Pmt_Dte_Pnd_Nxt_Pmt_Dte_N = pnd_Nxt_Pmt_Dte__R_Field_4.newFieldInGroup("pnd_Nxt_Pmt_Dte_Pnd_Nxt_Pmt_Dte_N", "#NXT-PMT-DTE-N", FieldType.NUMERIC, 
            8);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Rate = localVariables.newFieldInRecord("pnd_Max_Rate", "#MAX-RATE", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Rslt = localVariables.newFieldInRecord("pnd_Max_Rslt", "#MAX-RSLT", FieldType.PACKED_DECIMAL, 3);
        pnd_More = localVariables.newFieldInRecord("pnd_More", "#MORE", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.PACKED_DECIMAL, 3);
        pnd_Factor_Dte = localVariables.newFieldInRecord("pnd_Factor_Dte", "#FACTOR-DTE", FieldType.NUMERIC, 8);
        pnd_Wdte6 = localVariables.newFieldInRecord("pnd_Wdte6", "#WDTE6", FieldType.STRING, 6);
        pnd_Wdte4 = localVariables.newFieldInRecord("pnd_Wdte4", "#WDTE4", FieldType.STRING, 4);
        pnd_Check_A6 = localVariables.newFieldInRecord("pnd_Check_A6", "#CHECK-A6", FieldType.STRING, 6);
        pnd_Instllmnt_A6 = localVariables.newFieldInRecord("pnd_Instllmnt_A6", "#INSTLLMNT-A6", FieldType.STRING, 6);
        pnd_Wdte8 = localVariables.newFieldInRecord("pnd_Wdte8", "#WDTE8", FieldType.STRING, 8);

        pnd_Wdte8__R_Field_5 = localVariables.newGroupInRecord("pnd_Wdte8__R_Field_5", "REDEFINE", pnd_Wdte8);
        pnd_Wdte8_Pnd_W_Yyyy = pnd_Wdte8__R_Field_5.newFieldInGroup("pnd_Wdte8_Pnd_W_Yyyy", "#W-YYYY", FieldType.NUMERIC, 4);
        pnd_Wdte8_Pnd_W_Mm = pnd_Wdte8__R_Field_5.newFieldInGroup("pnd_Wdte8_Pnd_W_Mm", "#W-MM", FieldType.NUMERIC, 2);
        pnd_Wdte8_Pnd_W_Dd = pnd_Wdte8__R_Field_5.newFieldInGroup("pnd_Wdte8_Pnd_W_Dd", "#W-DD", FieldType.NUMERIC, 2);
        pnd_Asd_A = localVariables.newFieldInRecord("pnd_Asd_A", "#ASD-A", FieldType.STRING, 8);

        pnd_Asd_A__R_Field_6 = localVariables.newGroupInRecord("pnd_Asd_A__R_Field_6", "REDEFINE", pnd_Asd_A);
        pnd_Asd_A_Pnd_Asd_N = pnd_Asd_A__R_Field_6.newFieldInGroup("pnd_Asd_A_Pnd_Asd_N", "#ASD-N", FieldType.NUMERIC, 8);

        pnd_Asd_A__R_Field_7 = localVariables.newGroupInRecord("pnd_Asd_A__R_Field_7", "REDEFINE", pnd_Asd_A);
        pnd_Asd_A_Pnd_Asd_Yyyy = pnd_Asd_A__R_Field_7.newFieldInGroup("pnd_Asd_A_Pnd_Asd_Yyyy", "#ASD-YYYY", FieldType.NUMERIC, 4);
        pnd_Asd_A_Pnd_Asd_Mm = pnd_Asd_A__R_Field_7.newFieldInGroup("pnd_Asd_A_Pnd_Asd_Mm", "#ASD-MM", FieldType.NUMERIC, 2);
        pnd_Asd_A_Pnd_Asd_Dd = pnd_Asd_A__R_Field_7.newFieldInGroup("pnd_Asd_A_Pnd_Asd_Dd", "#ASD-DD", FieldType.STRING, 2);

        pnd_Asd_A__R_Field_8 = localVariables.newGroupInRecord("pnd_Asd_A__R_Field_8", "REDEFINE", pnd_Asd_A);
        pnd_Asd_A_Pnd_Asd_N6 = pnd_Asd_A__R_Field_8.newFieldInGroup("pnd_Asd_A_Pnd_Asd_N6", "#ASD-N6", FieldType.NUMERIC, 6);
        pnd_Io_Dte_A = localVariables.newFieldInRecord("pnd_Io_Dte_A", "#IO-DTE-A", FieldType.STRING, 8);

        pnd_Io_Dte_A__R_Field_9 = localVariables.newGroupInRecord("pnd_Io_Dte_A__R_Field_9", "REDEFINE", pnd_Io_Dte_A);
        pnd_Io_Dte_A_Pnd_Iod_Yyyy = pnd_Io_Dte_A__R_Field_9.newFieldInGroup("pnd_Io_Dte_A_Pnd_Iod_Yyyy", "#IOD-YYYY", FieldType.NUMERIC, 4);
        pnd_Io_Dte_A_Pnd_Iod_Mm = pnd_Io_Dte_A__R_Field_9.newFieldInGroup("pnd_Io_Dte_A_Pnd_Iod_Mm", "#IOD-MM", FieldType.NUMERIC, 2);
        pnd_Io_Dte_A_Pnd_Iod_Dd = pnd_Io_Dte_A__R_Field_9.newFieldInGroup("pnd_Io_Dte_A_Pnd_Iod_Dd", "#IOD-DD", FieldType.STRING, 2);
        pnd_Next_Dte_A = localVariables.newFieldInRecord("pnd_Next_Dte_A", "#NEXT-DTE-A", FieldType.STRING, 8);

        pnd_Next_Dte_A__R_Field_10 = localVariables.newGroupInRecord("pnd_Next_Dte_A__R_Field_10", "REDEFINE", pnd_Next_Dte_A);
        pnd_Next_Dte_A_Pnd_Next_Dte_N = pnd_Next_Dte_A__R_Field_10.newFieldInGroup("pnd_Next_Dte_A_Pnd_Next_Dte_N", "#NEXT-DTE-N", FieldType.NUMERIC, 
            8);

        pnd_Next_Dte_A__R_Field_11 = localVariables.newGroupInRecord("pnd_Next_Dte_A__R_Field_11", "REDEFINE", pnd_Next_Dte_A);
        pnd_Next_Dte_A_Pnd_Next_N6 = pnd_Next_Dte_A__R_Field_11.newFieldInGroup("pnd_Next_Dte_A_Pnd_Next_N6", "#NEXT-N6", FieldType.NUMERIC, 6);

        pnd_Next_Dte_A__R_Field_12 = localVariables.newGroupInRecord("pnd_Next_Dte_A__R_Field_12", "REDEFINE", pnd_Next_Dte_A);
        pnd_Next_Dte_A_Pnd_Next_Dte_Yyyy = pnd_Next_Dte_A__R_Field_12.newFieldInGroup("pnd_Next_Dte_A_Pnd_Next_Dte_Yyyy", "#NEXT-DTE-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Next_Dte_A_Pnd_Next_Dte_Mm = pnd_Next_Dte_A__R_Field_12.newFieldInGroup("pnd_Next_Dte_A_Pnd_Next_Dte_Mm", "#NEXT-DTE-MM", FieldType.NUMERIC, 
            2);
        pnd_Next_Dte_A_Pnd_Next_Dte_Dd = pnd_Next_Dte_A__R_Field_12.newFieldInGroup("pnd_Next_Dte_A_Pnd_Next_Dte_Dd", "#NEXT-DTE-DD", FieldType.STRING, 
            2);
        pnd_Stop_Dte_A = localVariables.newFieldInRecord("pnd_Stop_Dte_A", "#STOP-DTE-A", FieldType.STRING, 8);

        pnd_Stop_Dte_A__R_Field_13 = localVariables.newGroupInRecord("pnd_Stop_Dte_A__R_Field_13", "REDEFINE", pnd_Stop_Dte_A);
        pnd_Stop_Dte_A_Pnd_Stop_N6 = pnd_Stop_Dte_A__R_Field_13.newFieldInGroup("pnd_Stop_Dte_A_Pnd_Stop_N6", "#STOP-N6", FieldType.NUMERIC, 6);

        pnd_Stop_Dte_A__R_Field_14 = localVariables.newGroupInRecord("pnd_Stop_Dte_A__R_Field_14", "REDEFINE", pnd_Stop_Dte_A);
        pnd_Stop_Dte_A_Pnd_Stop_Dte_N = pnd_Stop_Dte_A__R_Field_14.newFieldInGroup("pnd_Stop_Dte_A_Pnd_Stop_Dte_N", "#STOP-DTE-N", FieldType.NUMERIC, 
            8);

        pnd_Stop_Dte_A__R_Field_15 = localVariables.newGroupInRecord("pnd_Stop_Dte_A__R_Field_15", "REDEFINE", pnd_Stop_Dte_A);
        pnd_Stop_Dte_A_Pnd_Stop_Dte_Yyyy = pnd_Stop_Dte_A__R_Field_15.newFieldInGroup("pnd_Stop_Dte_A_Pnd_Stop_Dte_Yyyy", "#STOP-DTE-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Stop_Dte_A_Pnd_Stop_Dte_Mm = pnd_Stop_Dte_A__R_Field_15.newFieldInGroup("pnd_Stop_Dte_A_Pnd_Stop_Dte_Mm", "#STOP-DTE-MM", FieldType.NUMERIC, 
            2);
        pnd_Stop_Dte_A_Pnd_Stop_Dte_Dd = pnd_Stop_Dte_A__R_Field_15.newFieldInGroup("pnd_Stop_Dte_A_Pnd_Stop_Dte_Dd", "#STOP-DTE-DD", FieldType.STRING, 
            2);
        pnd_Acctg_Dte_A = localVariables.newFieldInRecord("pnd_Acctg_Dte_A", "#ACCTG-DTE-A", FieldType.STRING, 8);

        pnd_Acctg_Dte_A__R_Field_16 = localVariables.newGroupInRecord("pnd_Acctg_Dte_A__R_Field_16", "REDEFINE", pnd_Acctg_Dte_A);
        pnd_Acctg_Dte_A_Pnd_Acctg_Dte_N = pnd_Acctg_Dte_A__R_Field_16.newFieldInGroup("pnd_Acctg_Dte_A_Pnd_Acctg_Dte_N", "#ACCTG-DTE-N", FieldType.NUMERIC, 
            8);

        pnd_Acctg_Dte_A__R_Field_17 = localVariables.newGroupInRecord("pnd_Acctg_Dte_A__R_Field_17", "REDEFINE", pnd_Acctg_Dte_A);
        pnd_Acctg_Dte_A_Pnd_Acctg_N6 = pnd_Acctg_Dte_A__R_Field_17.newFieldInGroup("pnd_Acctg_Dte_A_Pnd_Acctg_N6", "#ACCTG-N6", FieldType.NUMERIC, 6);
        pnd_Bsnss_Dte_A = localVariables.newFieldInRecord("pnd_Bsnss_Dte_A", "#BSNSS-DTE-A", FieldType.STRING, 8);

        pnd_Bsnss_Dte_A__R_Field_18 = localVariables.newGroupInRecord("pnd_Bsnss_Dte_A__R_Field_18", "REDEFINE", pnd_Bsnss_Dte_A);
        pnd_Bsnss_Dte_A_Pnd_Bsnss_Dte_N = pnd_Bsnss_Dte_A__R_Field_18.newFieldInGroup("pnd_Bsnss_Dte_A_Pnd_Bsnss_Dte_N", "#BSNSS-DTE-N", FieldType.NUMERIC, 
            8);
        pnd_Factor_Mm = localVariables.newFieldInRecord("pnd_Factor_Mm", "#FACTOR-MM", FieldType.PACKED_DECIMAL, 3);
        pnd_Adi_Super1 = localVariables.newFieldInRecord("pnd_Adi_Super1", "#ADI-SUPER1", FieldType.STRING, 41);

        pnd_Adi_Super1__R_Field_19 = localVariables.newGroupInRecord("pnd_Adi_Super1__R_Field_19", "REDEFINE", pnd_Adi_Super1);

        pnd_Adi_Super1_Super_Structure = pnd_Adi_Super1__R_Field_19.newGroupInGroup("pnd_Adi_Super1_Super_Structure", "SUPER-STRUCTURE");
        pnd_Adi_Super1_Rqst_Id = pnd_Adi_Super1_Super_Structure.newFieldInGroup("pnd_Adi_Super1_Rqst_Id", "RQST-ID", FieldType.STRING, 35);
        pnd_Adi_Super1_Adi_Ia_Rcrd_Cde = pnd_Adi_Super1_Super_Structure.newFieldInGroup("pnd_Adi_Super1_Adi_Ia_Rcrd_Cde", "ADI-IA-RCRD-CDE", FieldType.STRING, 
            3);
        pnd_Adi_Super1_Adi_Sqnce_Nbr = pnd_Adi_Super1_Super_Structure.newFieldInGroup("pnd_Adi_Super1_Adi_Sqnce_Nbr", "ADI-SQNCE-NBR", FieldType.NUMERIC, 
            3);
        pnd_Adi_Super2 = localVariables.newFieldInRecord("pnd_Adi_Super2", "#ADI-SUPER2", FieldType.STRING, 41);

        pnd_Adi_Super2__R_Field_20 = localVariables.newGroupInRecord("pnd_Adi_Super2__R_Field_20", "REDEFINE", pnd_Adi_Super2);

        pnd_Adi_Super2_Super_Structure2 = pnd_Adi_Super2__R_Field_20.newGroupInGroup("pnd_Adi_Super2_Super_Structure2", "SUPER-STRUCTURE2");
        pnd_Adi_Super2_Rqst_Id = pnd_Adi_Super2_Super_Structure2.newFieldInGroup("pnd_Adi_Super2_Rqst_Id", "RQST-ID", FieldType.STRING, 35);
        pnd_Adi_Super2_Adi_Ia_Rcrd_Cde = pnd_Adi_Super2_Super_Structure2.newFieldInGroup("pnd_Adi_Super2_Adi_Ia_Rcrd_Cde", "ADI-IA-RCRD-CDE", FieldType.STRING, 
            3);
        pnd_Adi_Super2_Adi_Sqnce_Nbr = pnd_Adi_Super2_Super_Structure2.newFieldInGroup("pnd_Adi_Super2_Adi_Sqnce_Nbr", "ADI-SQNCE-NBR", FieldType.NUMERIC, 
            3);
        pnd_Ret_Cde = localVariables.newFieldInRecord("pnd_Ret_Cde", "#RET-CDE", FieldType.NUMERIC, 2);
        pnd_Cntrl_Isn = localVariables.newFieldInRecord("pnd_Cntrl_Isn", "#CNTRL-ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Tiaa = localVariables.newFieldInRecord("pnd_Tiaa", "#TIAA", FieldType.BOOLEAN, 1);
        pnd_Cref = localVariables.newFieldInRecord("pnd_Cref", "#CREF", FieldType.BOOLEAN, 1);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Cmpny = localVariables.newFieldInRecord("pnd_Cmpny", "#CMPNY", FieldType.STRING, 1);
        pnd_No_Pays = localVariables.newFieldInRecord("pnd_No_Pays", "#NO-PAYS", FieldType.PACKED_DECIMAL, 3);
        pnd_No_Per_Yr = localVariables.newFieldInRecord("pnd_No_Per_Yr", "#NO-PER-YR", FieldType.PACKED_DECIMAL, 3);
        pnd_Dob = localVariables.newFieldInRecord("pnd_Dob", "#DOB", FieldType.STRING, 8);

        pnd_Dob__R_Field_21 = localVariables.newGroupInRecord("pnd_Dob__R_Field_21", "REDEFINE", pnd_Dob);
        pnd_Dob_Pnd_Dob_Yyyy = pnd_Dob__R_Field_21.newFieldInGroup("pnd_Dob_Pnd_Dob_Yyyy", "#DOB-YYYY", FieldType.NUMERIC, 4);
        pnd_Dob_Pnd_Dob_Mm = pnd_Dob__R_Field_21.newFieldInGroup("pnd_Dob_Pnd_Dob_Mm", "#DOB-MM", FieldType.NUMERIC, 2);
        pnd_Dob_Pnd_Dob_Dd = pnd_Dob__R_Field_21.newFieldInGroup("pnd_Dob_Pnd_Dob_Dd", "#DOB-DD", FieldType.NUMERIC, 2);

        pnd_Dob__R_Field_22 = localVariables.newGroupInRecord("pnd_Dob__R_Field_22", "REDEFINE", pnd_Dob);
        pnd_Dob_Pnd_Dob_N = pnd_Dob__R_Field_22.newFieldInGroup("pnd_Dob_Pnd_Dob_N", "#DOB-N", FieldType.NUMERIC, 8);
        pnd_Dob2 = localVariables.newFieldInRecord("pnd_Dob2", "#DOB2", FieldType.STRING, 8);

        pnd_Dob2__R_Field_23 = localVariables.newGroupInRecord("pnd_Dob2__R_Field_23", "REDEFINE", pnd_Dob2);
        pnd_Dob2_Pnd_Dob2_Yyyy = pnd_Dob2__R_Field_23.newFieldInGroup("pnd_Dob2_Pnd_Dob2_Yyyy", "#DOB2-YYYY", FieldType.NUMERIC, 4);
        pnd_Dob2_Pnd_Dob2_Mm = pnd_Dob2__R_Field_23.newFieldInGroup("pnd_Dob2_Pnd_Dob2_Mm", "#DOB2-MM", FieldType.NUMERIC, 2);
        pnd_Dob2_Pnd_Dob2_Dd = pnd_Dob2__R_Field_23.newFieldInGroup("pnd_Dob2_Pnd_Dob2_Dd", "#DOB2-DD", FieldType.NUMERIC, 2);

        pnd_Dob2__R_Field_24 = localVariables.newGroupInRecord("pnd_Dob2__R_Field_24", "REDEFINE", pnd_Dob2);
        pnd_Dob2_Pnd_Dob2_N = pnd_Dob2__R_Field_24.newFieldInGroup("pnd_Dob2_Pnd_Dob2_N", "#DOB2-N", FieldType.NUMERIC, 8);
        pnd_Last_Inst_Dte_A = localVariables.newFieldInRecord("pnd_Last_Inst_Dte_A", "#LAST-INST-DTE-A", FieldType.STRING, 8);

        pnd_Last_Inst_Dte_A__R_Field_25 = localVariables.newGroupInRecord("pnd_Last_Inst_Dte_A__R_Field_25", "REDEFINE", pnd_Last_Inst_Dte_A);
        pnd_Last_Inst_Dte_A_Pnd_Last_Inst_Yyyy = pnd_Last_Inst_Dte_A__R_Field_25.newFieldInGroup("pnd_Last_Inst_Dte_A_Pnd_Last_Inst_Yyyy", "#LAST-INST-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Last_Inst_Dte_A_Pnd_Last_Inst_Mm = pnd_Last_Inst_Dte_A__R_Field_25.newFieldInGroup("pnd_Last_Inst_Dte_A_Pnd_Last_Inst_Mm", "#LAST-INST-MM", 
            FieldType.NUMERIC, 2);
        pnd_Last_Inst_Dte_A_Pnd_Last_Inst_Dd = pnd_Last_Inst_Dte_A__R_Field_25.newFieldInGroup("pnd_Last_Inst_Dte_A_Pnd_Last_Inst_Dd", "#LAST-INST-DD", 
            FieldType.STRING, 2);
        pnd_C_Reval_Dte_A = localVariables.newFieldInRecord("pnd_C_Reval_Dte_A", "#C-REVAL-DTE-A", FieldType.STRING, 8);

        pnd_C_Reval_Dte_A__R_Field_26 = localVariables.newGroupInRecord("pnd_C_Reval_Dte_A__R_Field_26", "REDEFINE", pnd_C_Reval_Dte_A);
        pnd_C_Reval_Dte_A_Pnd_C_Reval_Dte_6 = pnd_C_Reval_Dte_A__R_Field_26.newFieldInGroup("pnd_C_Reval_Dte_A_Pnd_C_Reval_Dte_6", "#C-REVAL-DTE-6", FieldType.STRING, 
            6);

        pnd_C_Reval_Dte_A__R_Field_27 = localVariables.newGroupInRecord("pnd_C_Reval_Dte_A__R_Field_27", "REDEFINE", pnd_C_Reval_Dte_A);
        pnd_C_Reval_Dte_A_Pnd_C_Reval_Yyyy = pnd_C_Reval_Dte_A__R_Field_27.newFieldInGroup("pnd_C_Reval_Dte_A_Pnd_C_Reval_Yyyy", "#C-REVAL-YYYY", FieldType.NUMERIC, 
            4);
        pnd_C_Reval_Dte_A_Pnd_C_Reval_Mm = pnd_C_Reval_Dte_A__R_Field_27.newFieldInGroup("pnd_C_Reval_Dte_A_Pnd_C_Reval_Mm", "#C-REVAL-MM", FieldType.NUMERIC, 
            2);
        pnd_C_Reval_Dte_A_Pnd_C_Reval_Dd = pnd_C_Reval_Dte_A__R_Field_27.newFieldInGroup("pnd_C_Reval_Dte_A_Pnd_C_Reval_Dd", "#C-REVAL-DD", FieldType.STRING, 
            2);
        pnd_T_Reval_Dte_A = localVariables.newFieldInRecord("pnd_T_Reval_Dte_A", "#T-REVAL-DTE-A", FieldType.STRING, 8);

        pnd_T_Reval_Dte_A__R_Field_28 = localVariables.newGroupInRecord("pnd_T_Reval_Dte_A__R_Field_28", "REDEFINE", pnd_T_Reval_Dte_A);
        pnd_T_Reval_Dte_A_Pnd_T_Reval_Dte_6 = pnd_T_Reval_Dte_A__R_Field_28.newFieldInGroup("pnd_T_Reval_Dte_A_Pnd_T_Reval_Dte_6", "#T-REVAL-DTE-6", FieldType.STRING, 
            6);

        pnd_T_Reval_Dte_A__R_Field_29 = localVariables.newGroupInRecord("pnd_T_Reval_Dte_A__R_Field_29", "REDEFINE", pnd_T_Reval_Dte_A);
        pnd_T_Reval_Dte_A_Pnd_T_Reval_Yyyy = pnd_T_Reval_Dte_A__R_Field_29.newFieldInGroup("pnd_T_Reval_Dte_A_Pnd_T_Reval_Yyyy", "#T-REVAL-YYYY", FieldType.NUMERIC, 
            4);
        pnd_T_Reval_Dte_A_Pnd_T_Reval_Mm = pnd_T_Reval_Dte_A__R_Field_29.newFieldInGroup("pnd_T_Reval_Dte_A_Pnd_T_Reval_Mm", "#T-REVAL-MM", FieldType.NUMERIC, 
            2);
        pnd_T_Reval_Dte_A_Pnd_T_Reval_Dd = pnd_T_Reval_Dte_A__R_Field_29.newFieldInGroup("pnd_T_Reval_Dte_A_Pnd_T_Reval_Dd", "#T-REVAL-DD", FieldType.STRING, 
            2);
        pnd_Tiaa_Ivc = localVariables.newFieldInRecord("pnd_Tiaa_Ivc", "#TIAA-IVC", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cref_Ivc = localVariables.newFieldInRecord("pnd_Cref_Ivc", "#CREF-IVC", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 38);
        pnd_F_Or_N = localVariables.newFieldInRecord("pnd_F_Or_N", "#F-OR-N", FieldType.STRING, 1);
        pnd_Rtb_Inst_Added = localVariables.newFieldInRecord("pnd_Rtb_Inst_Added", "#RTB-INST-ADDED", FieldType.BOOLEAN, 1);
        pnd_Revalue_Tiaa = localVariables.newFieldInRecord("pnd_Revalue_Tiaa", "#REVALUE-TIAA", FieldType.BOOLEAN, 1);
        pnd_Revalue_Cref = localVariables.newFieldInRecord("pnd_Revalue_Cref", "#REVALUE-CREF", FieldType.BOOLEAN, 1);
        pnd_Tiaa_Ok = localVariables.newFieldInRecord("pnd_Tiaa_Ok", "#TIAA-OK", FieldType.BOOLEAN, 1);
        pnd_Cref_Ok = localVariables.newFieldInRecord("pnd_Cref_Ok", "#CREF-OK", FieldType.BOOLEAN, 1);
        pnd_Callnat = localVariables.newFieldInRecord("pnd_Callnat", "#CALLNAT", FieldType.STRING, 8);
        pnd_Sim_Rule = localVariables.newFieldInRecord("pnd_Sim_Rule", "#SIM-RULE", FieldType.STRING, 1);
        pnd_Cntrct_Orgn_Cde = localVariables.newFieldInRecord("pnd_Cntrct_Orgn_Cde", "#CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2);
        pnd_Sim_Age = localVariables.newFieldInRecord("pnd_Sim_Age", "#SIM-AGE", FieldType.NUMERIC, 3);
        pnd_Cntrct_Py_Dte_Key = localVariables.newFieldInRecord("pnd_Cntrct_Py_Dte_Key", "#CNTRCT-PY-DTE-KEY", FieldType.STRING, 23);

        pnd_Cntrct_Py_Dte_Key__R_Field_30 = localVariables.newGroupInRecord("pnd_Cntrct_Py_Dte_Key__R_Field_30", "REDEFINE", pnd_Cntrct_Py_Dte_Key);

        pnd_Cntrct_Py_Dte_Key_Hist_Structure = pnd_Cntrct_Py_Dte_Key__R_Field_30.newGroupInGroup("pnd_Cntrct_Py_Dte_Key_Hist_Structure", "HIST-STRUCTURE");
        pnd_Cntrct_Py_Dte_Key_Cntrct_Ppcn_Nbr = pnd_Cntrct_Py_Dte_Key_Hist_Structure.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Py_Dte_Key_Cntrct_Payee_Cde = pnd_Cntrct_Py_Dte_Key_Hist_Structure.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Py_Dte_Key_Fund_Invrse_Lst_Pd_Dte = pnd_Cntrct_Py_Dte_Key_Hist_Structure.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Fund_Invrse_Lst_Pd_Dte", 
            "FUND-INVRSE-LST-PD-DTE", FieldType.NUMERIC, 8);
        pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde = pnd_Cntrct_Py_Dte_Key_Hist_Structure.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde", "CMPNY-FUND-CDE", 
            FieldType.STRING, 3);

        pnd_Cntrct_Py_Dte_Key__R_Field_31 = pnd_Cntrct_Py_Dte_Key_Hist_Structure.newGroupInGroup("pnd_Cntrct_Py_Dte_Key__R_Field_31", "REDEFINE", pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde);
        pnd_Cntrct_Py_Dte_Key__Filler4 = pnd_Cntrct_Py_Dte_Key__R_Field_31.newFieldInGroup("pnd_Cntrct_Py_Dte_Key__Filler4", "_FILLER4", FieldType.STRING, 
            1);
        pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Cde = pnd_Cntrct_Py_Dte_Key__R_Field_31.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Cde", "#FUND-CDE", FieldType.STRING, 
            2);
        pnd_T = localVariables.newFieldInRecord("pnd_T", "#T", FieldType.BOOLEAN, 1);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.BOOLEAN, 1);
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.PACKED_DECIMAL, 3);
        pnd_Dest_Cde = localVariables.newFieldInRecord("pnd_Dest_Cde", "#DEST-CDE", FieldType.STRING, 4);
        pnd_Ivc = localVariables.newFieldInRecord("pnd_Ivc", "#IVC", FieldType.BOOLEAN, 1);
        pnd_Periodic_Ivc = localVariables.newFieldInRecord("pnd_Periodic_Ivc", "#PERIODIC-IVC", FieldType.BOOLEAN, 1);
        cpnd_Adi_Dtl_Tiaa_Data = localVariables.newFieldInRecord("cpnd_Adi_Dtl_Tiaa_Data", "C#ADI-DTL-TIAA-DATA", FieldType.PACKED_DECIMAL, 3);
        pnd_Tiaa_Std_Grntd_Amt = localVariables.newFieldArrayInRecord("pnd_Tiaa_Std_Grntd_Amt", "#TIAA-STD-GRNTD-AMT", FieldType.PACKED_DECIMAL, 9, 2, 
            new DbsArrayController(1, 250));
        pnd_Tiaa_Std_Dvdnd_Amt = localVariables.newFieldArrayInRecord("pnd_Tiaa_Std_Dvdnd_Amt", "#TIAA-STD-DVDND-AMT", FieldType.PACKED_DECIMAL, 9, 2, 
            new DbsArrayController(1, 250));
        pnd_Isn_Pos = localVariables.newFieldInRecord("pnd_Isn_Pos", "#ISN-POS", FieldType.PACKED_DECIMAL, 3);

        pnd_Rstl_Isn = localVariables.newGroupArrayInRecord("pnd_Rstl_Isn", "#RSTL-ISN", new DbsArrayController(1, 12));
        pnd_Rstl_Isn_Pnd_Pp_Isn = pnd_Rstl_Isn.newFieldInGroup("pnd_Rstl_Isn_Pnd_Pp_Isn", "#PP-ISN", FieldType.PACKED_DECIMAL, 7);
        pnd_Rstl_Isn_Pnd_Pp1_Isn = pnd_Rstl_Isn.newFieldInGroup("pnd_Rstl_Isn_Pnd_Pp1_Isn", "#PP1-ISN", FieldType.PACKED_DECIMAL, 7);
        pnd_Ivc_Call = localVariables.newFieldInRecord("pnd_Ivc_Call", "#IVC-CALL", FieldType.STRING, 1);
        pnd_Sve_Ivc_Pymnt_Rule = localVariables.newFieldInRecord("pnd_Sve_Ivc_Pymnt_Rule", "#SVE-IVC-PYMNT-RULE", FieldType.STRING, 1);
        pnd_Rtn_Cde = localVariables.newFieldInRecord("pnd_Rtn_Cde", "#RTN-CDE", FieldType.NUMERIC, 2);
        pls_Sub_Sw = WsIndependent.getInstance().newFieldInRecord("pls_Sub_Sw", "+SUB-SW", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Ia_Rslt.reset();
        vw_ads_Ia_Rsl2.reset();
        vw_ads_Prtcpnt.reset();
        vw_ads_Cntrct.reset();
        vw_iaa_Cntrl_Rcrd_1.reset();

        parameters.reset();
        localVariables.reset();
        pnd_Max_Rate.setInitialValue(250);
        pnd_Max_Rslt.setInitialValue(125);
        pnd_Callnat.setInitialValue("AIAN083");
        pnd_Cntrct_Orgn_Cde.setInitialValue(2);
        pls_Sub_Sw.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn600() throws Exception
    {
        super("Adsn600");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  030912 START
        pnd_More.setValue(false);                                                                                                                                         //Natural: ASSIGN #MORE := FALSE
        if (condition(pnd_Rslt_Isn.getValue(2).greater(getZero())))                                                                                                       //Natural: IF #RSLT-ISN ( 2 ) GT 0
        {
            pnd_More.setValue(true);                                                                                                                                      //Natural: ASSIGN #MORE := TRUE
            //*  030912 END
            //*  EM - 052711 START
            //*  EM - 052711 END
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Call_Type().setValue("L");                                                                                          //Natural: ASSIGN #AIAN026-CALL-TYPE := 'L'
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Ia_Fund_Code().setValue("C");                                                                                       //Natural: ASSIGN #AIAN026-IA-FUND-CODE := 'C'
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Revaluation_Method().setValue("A");                                                                                 //Natural: ASSIGN #AIAN026-REVALUATION-METHOD := 'A'
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Iauv_Request_Date().setValue(99999999);                                                                             //Natural: ASSIGN #AIAN026-IAUV-REQUEST-DATE := 99999999
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Participation_Date().setValue(0);                                                                                   //Natural: ASSIGN #AIAN026-PARTICIPATION-DATE := 0
        //*  EM - 052711
        DbsUtil.callnat(Aian026.class , getCurrentProcessState(), pdaAiaa026.getPnd_Aian026_Linkage());                                                                   //Natural: CALLNAT 'AIAN026' #AIAN026-LINKAGE
        if (condition(Global.isEscape())) return;
        pnd_Factor_Dte.setValue(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Iauv_Date_Returned());                                                                      //Natural: ASSIGN #FACTOR-DTE := #AIAN026-IAUV-DATE-RETURNED
        //*  030107 END /* EM - 052711
        pdaAiaa026.getPnd_Aian026_Linkage().reset();                                                                                                                      //Natural: RESET #AIAN026-LINKAGE
        pnd_Program_Nme.reset();                                                                                                                                          //Natural: RESET #PROGRAM-NME #ERR-NR
        pnd_Err_Nr.reset();
        vw_iaa_Cntrl_Rcrd_1.startDatabaseRead                                                                                                                             //Natural: READ ( 1 ) IAA-CNTRL-RCRD-1 BY CNTRL-RCRD-KEY STARTING FROM 'AA'
        (
        "R0",
        new Wc[] { new Wc("CNTRL_RCRD_KEY", ">=", "AA", WcType.BY) },
        new Oc[] { new Oc("CNTRL_RCRD_KEY", "ASC") },
        1
        );
        R0:
        while (condition(vw_iaa_Cntrl_Rcrd_1.readNextRow("R0")))
        {
            if (condition(iaa_Cntrl_Rcrd_1_Cntrl_Cde.equals("AA")))                                                                                                       //Natural: IF IAA-CNTRL-RCRD-1.CNTRL-CDE = 'AA'
            {
                if (condition(iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde.equals("9") || iaa_Cntrl_Rcrd_1_Cntrl_Actvty_Cde.notEquals("A")))                                         //Natural: IF IAA-CNTRL-RCRD-1.CNTRL-ACTVTY-CDE = '9' OR IAA-CNTRL-RCRD-1.CNTRL-ACTVTY-CDE NE 'A'
                {
                    pnd_Err_Cde_X.setValue("L22");                                                                                                                        //Natural: ASSIGN #ERR-CDE-X := 'L22'
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Err_Cde_X.setValue("L23");                                                                                                                            //Natural: ASSIGN #ERR-CDE-X := 'L23'
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pdaAdsa600.getAdsa600_Pnd_Ia_Check_Dte().setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                     //Natural: MOVE EDITED IAA-CNTRL-RCRD-1.CNTRL-CHECK-DTE ( EM = YYYYMMDD ) TO #IA-CHECK-DTE
            pnd_Check_A6.setValueEdited(iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte,new ReportEditMask("YYYYMM"));                                                                   //Natural: MOVE EDITED IAA-CNTRL-RCRD-1.CNTRL-CHECK-DTE ( EM = YYYYMM ) TO #CHECK-A6
            pnd_Cntrl_Isn.setValue(vw_iaa_Cntrl_Rcrd_1.getAstISN("R0"));                                                                                                  //Natural: ASSIGN #CNTRL-ISN := *ISN ( R0. )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  111910
        //*  111910
        DbsUtil.callnat(Iaan0395.class , getCurrentProcessState(), pnd_Nxt_Pmt_Dte_Pnd_Nxt_Pmt_Dte_N, pnd_Calc_Mthd, pnd_Rc);                                             //Natural: CALLNAT 'IAAN0395' #NXT-PMT-DTE-N #CALC-MTHD #RC
        if (condition(Global.isEscape())) return;
        pnd_Nxt_Pmt_Dte.setValue(pdaAdsa600.getAdsa600_Pnd_Ia_Check_Dte());                                                                                               //Natural: ASSIGN #NXT-PMT-DTE := #IA-CHECK-DTE
        //*  121610
        //*  121610
        GET01:                                                                                                                                                            //Natural: GET ADS-PRTCPNT #PRTCPNT-ISN
        vw_ads_Prtcpnt.readByID(pnd_Prtcpnt_Isn.getLong(), "GET01");
        ads_Prtcpnt_Adp_Ivc_Pymnt_Rule.setValue(pnd_Ivc_Pymnt_Rule);                                                                                                      //Natural: ASSIGN ADS-PRTCPNT.ADP-IVC-PYMNT-RULE := #IVC-PYMNT-RULE
        pdaAdsa600.getAdsa600_Adp_Frm_Irc_Cde().getValue(1,":",4).setValue(ads_Prtcpnt_Adp_Frm_Irc_Cde.getValue(1,":",4));                                                //Natural: ASSIGN ADSA600.ADP-FRM-IRC-CDE ( 1:4 ) := ADS-PRTCPNT.ADP-FRM-IRC-CDE ( 1:4 )
        pdaAdsa600.getAdsa600_Adp_To_Irc_Cde().setValue(ads_Prtcpnt_Adp_To_Irc_Cde);                                                                                      //Natural: ASSIGN ADSA600.ADP-TO-IRC-CDE := ADS-PRTCPNT.ADP-TO-IRC-CDE
        pdaAdsa600.getAdsa600_Adp_Ivc_Rlvr_Cde().setValue(ads_Prtcpnt_Adp_Ivc_Rlvr_Cde);                                                                                  //Natural: ASSIGN ADSA600.ADP-IVC-RLVR-CDE := ADS-PRTCPNT.ADP-IVC-RLVR-CDE
        pdaAdsa600.getAdsa600_Adp_Alt_Dest_Hold_Cde().setValue(ads_Prtcpnt_Adp_Alt_Dest_Hold_Cde);                                                                        //Natural: ASSIGN ADSA600.ADP-ALT-DEST-HOLD-CDE := ADS-PRTCPNT.ADP-ALT-DEST-HOLD-CDE
        pdaAdsa600.getAdsa600_Adp_Hold_Cde().setValue(ads_Prtcpnt_Adp_Hold_Cde);                                                                                          //Natural: ASSIGN ADSA600.ADP-HOLD-CDE := ADS-PRTCPNT.ADP-HOLD-CDE
        //*  ADDED FOLLOWING 9/03 TO POPULATE ADSA600 FIELD
        pdaAdsa600.getAdsa600_Adp_Alt_Dest_Rlvr_Dest().setValue(ads_Prtcpnt_Adp_Alt_Dest_Rlvr_Dest);                                                                      //Natural: ASSIGN ADSA600.ADP-ALT-DEST-RLVR-DEST := ADS-PRTCPNT.ADP-ALT-DEST-RLVR-DEST
        //*  END OF ADD 9/03 TO POPULATE NAZA600 FIELD
        //*  EM 031708 START
        if (condition(ads_Prtcpnt_Adp_Roth_Rqst_Ind.equals("Y")))                                                                                                         //Natural: IF ADS-PRTCPNT.ADP-ROTH-RQST-IND = 'Y'
        {
            pdaAdsa600.getAdsa600_Adp_Roth_Plan_Type().setValue(ads_Prtcpnt_Adp_Roth_Plan_Type);                                                                          //Natural: ASSIGN ADSA600.ADP-ROTH-PLAN-TYPE := ADS-PRTCPNT.ADP-ROTH-PLAN-TYPE
            pdaAdsa600.getAdsa600_Adp_Roth_Frst_Cntrbtn_Dte().setValue(ads_Prtcpnt_Adp_Roth_Frst_Cntrbtn_Dte);                                                            //Natural: ASSIGN ADSA600.ADP-ROTH-FRST-CNTRBTN-DTE := ADS-PRTCPNT.ADP-ROTH-FRST-CNTRBTN-DTE
            pdaAdsa600.getAdsa600_Adp_Roth_Dsblty_Dte().setValue(ads_Prtcpnt_Adp_Roth_Dsblty_Dte);                                                                        //Natural: ASSIGN ADSA600.ADP-ROTH-DSBLTY-DTE := ADS-PRTCPNT.ADP-ROTH-DSBLTY-DTE
            pdaAdsa600.getAdsa600_Adp_Roth_Rqst_Ind().setValue(ads_Prtcpnt_Adp_Roth_Rqst_Ind);                                                                            //Natural: ASSIGN ADSA600.ADP-ROTH-RQST-IND := ADS-PRTCPNT.ADP-ROTH-RQST-IND
        }                                                                                                                                                                 //Natural: END-IF
        //*  EM 031708 END
        //*  CM START
        pdaAdsa600.getAdsa600_Adp_Plan_Nbr().setValue(ads_Prtcpnt_Adp_Plan_Nbr);                                                                                          //Natural: ASSIGN ADSA600.ADP-PLAN-NBR := ADS-PRTCPNT.ADP-PLAN-NBR
        pdaAdsa600.getAdsa600_Adp_Mndtry_Tax_Ovrd().setValue(ads_Prtcpnt_Adp_Mndtry_Tax_Ovrd);                                                                            //Natural: ASSIGN ADSA600.ADP-MNDTRY-TAX-OVRD := ADS-PRTCPNT.ADP-MNDTRY-TAX-OVRD
        pdaAdsa600.getAdsa600_Adp_Srvvr_Ind().setValue(ads_Prtcpnt_Adp_Srvvr_Ind);                                                                                        //Natural: ASSIGN ADSA600.ADP-SRVVR-IND := ADS-PRTCPNT.ADP-SRVVR-IND
        pdaAdsa600.getAdsa600_Adp_Srvvr_Rltnshp().setValue(ads_Prtcpnt_Adp_Srvvr_Rltnshp);                                                                                //Natural: ASSIGN ADSA600.ADP-SRVVR-RLTNSHP := ADS-PRTCPNT.ADP-SRVVR-RLTNSHP
        //*  EM - 050715
        pdaAdsa600.getAdsa600_Adp_Bene_Client_Ind().setValue(ads_Prtcpnt_Adp_Bene_Client_Ind);                                                                            //Natural: ASSIGN ADSA600.ADP-BENE-CLIENT-IND := ADS-PRTCPNT.ADP-BENE-CLIENT-IND
        //*  CM END
        if (condition(ads_Prtcpnt_Adp_Annt_Typ_Cde.notEquals("M")))                                                                                                       //Natural: IF ADS-PRTCPNT.ADP-ANNT-TYP-CDE NE 'M'
        {
            //*  030912
            DbsUtil.callnat(Adsn603.class , getCurrentProcessState(), pnd_Rslt_Isn.getValue("*"), ads_Prtcpnt_Adp_Frst_Annt_Dte_Of_Brth, pnd_Bsnss_Dte,                   //Natural: CALLNAT 'ADSN603' #RSLT-ISN ( * ) ADS-PRTCPNT.ADP-FRST-ANNT-DTE-OF-BRTH #BSNSS-DTE ADS-PRTCPNT.ADP-EFFCTV-DTE ADSA600.ADP-RSTTLMNT-CNT ADS-PRTCPNT.ADP-SCND-ANNT-DTE-OF-BRTH #ERR-CDE-X
                ads_Prtcpnt_Adp_Effctv_Dte, pdaAdsa600.getAdsa600_Adp_Rsttlmnt_Cnt(), ads_Prtcpnt_Adp_Scnd_Annt_Dte_Of_Brth, pnd_Err_Cde_X);
            if (condition(Global.isEscape())) return;
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        GET02:                                                                                                                                                            //Natural: GET ADS-IA-RSLT #RSLT-ISN ( 1 )
        vw_ads_Ia_Rslt.readByID(pnd_Rslt_Isn.getValue(1).getLong(), "GET02");
        //*  030912 START
        if (condition(pnd_More.getBoolean()))                                                                                                                             //Natural: IF #MORE
        {
            GET03:                                                                                                                                                        //Natural: GET ADS-IA-RSL2 #RSLT-ISN ( 2 )
            vw_ads_Ia_Rsl2.readByID(pnd_Rslt_Isn.getValue(2).getLong(), "GET03");
            //*  030912 END
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ads_Ia_Rslt_Adi_Tiaa_Sttlmnt.notEquals("Y") && ads_Ia_Rslt_Adi_Tiaa_Re_Sttlmnt.notEquals("Y") && ads_Ia_Rslt_Adi_Cref_Sttlmnt.notEquals("Y")))      //Natural: IF ADS-IA-RSLT.ADI-TIAA-STTLMNT NE 'Y' AND ADS-IA-RSLT.ADI-TIAA-RE-STTLMNT NE 'Y' AND ADS-IA-RSLT.ADI-CREF-STTLMNT NE 'Y'
        {
            pnd_Err_Cde_X.setValue("L27");                                                                                                                                //Natural: ASSIGN #ERR-CDE-X := 'L27'
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaAdsa600.getAdsa600().setValuesByName(vw_ads_Ia_Rslt);                                                                                                          //Natural: MOVE BY NAME ADS-IA-RSLT TO ADSA600
        //*  030912 START
        pnd_B.reset();                                                                                                                                                    //Natural: RESET #B
                                                                                                                                                                          //Natural: PERFORM MOVE-TIAA-FIELDS1
        sub_Move_Tiaa_Fields1();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_More.getBoolean()))                                                                                                                             //Natural: IF #MORE
        {
            pnd_B.setValue(pnd_Max_Rslt);                                                                                                                                 //Natural: ASSIGN #B := #MAX-RSLT
                                                                                                                                                                          //Natural: PERFORM MOVE-TIAA-FIELDS2
            sub_Move_Tiaa_Fields2();
            if (condition(Global.isEscape())) {return;}
            //*  030912 END
        }                                                                                                                                                                 //Natural: END-IF
        //*  MOVE BY NAME ADS-IA-RSLT TO SUPER-STRUCTURE
        pnd_Adi_Finl_Periodic_Py_Dte.setValue(ads_Ia_Rslt_Adi_Finl_Periodic_Py_Dte);                                                                                      //Natural: ASSIGN #ADI-FINL-PERIODIC-PY-DTE := ADS-IA-RSLT.ADI-FINL-PERIODIC-PY-DTE
        //*  111910 START
        if (condition(pnd_Adi_Finl_Periodic_Py_Dte.greater(getZero())))                                                                                                   //Natural: IF #ADI-FINL-PERIODIC-PY-DTE GT 0
        {
            pnd_Final_Per_Pay_Dte.setValueEdited(pnd_Adi_Finl_Periodic_Py_Dte,new ReportEditMask("YYYYMM"));                                                              //Natural: MOVE EDITED #ADI-FINL-PERIODIC-PY-DTE ( EM = YYYYMM ) TO #FINAL-PER-PAY-DTE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa600.getAdsa600_Adi_Finl_Prtl_Py_Dte().greater(getZero())))                                                                                   //Natural: IF ADSA600.ADI-FINL-PRTL-PY-DTE GT 0
        {
            pnd_Final_Pay_Dte.setValueEdited(pdaAdsa600.getAdsa600_Adi_Finl_Prtl_Py_Dte(),new ReportEditMask("YYYYMMDD"));                                                //Natural: MOVE EDITED ADSA600.ADI-FINL-PRTL-PY-DTE ( EM = YYYYMMDD ) TO #FINAL-PAY-DTE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Mode.setValue(pdaAdsa600.getAdsa600_Adi_Pymnt_Mode());                                                                                                        //Natural: ASSIGN #MODE := ADSA600.ADI-PYMNT-MODE
        //*  111910 END
                                                                                                                                                                          //Natural: PERFORM CREATE-INSTALLMENT-RECORDS
        sub_Create_Installment_Records();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Err_Cde_X.notEquals(" ")))                                                                                                                      //Natural: IF #ERR-CDE-X NE ' '
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM INTERFACE-WITH-CPS
        sub_Interface_With_Cps();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Err_Cde_X.notEquals(" ")))                                                                                                                      //Natural: IF #ERR-CDE-X NE ' '
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #PRDC-IVC GT 0 AND (ADSA600.ADI-OPTN-CDE = 21) /* EM - 080212
        //*  EM - 080212
        if (condition(pnd_Periodic_Ivc.getBoolean() && (pdaAdsa600.getAdsa600_Adi_Optn_Cde().equals(21))))                                                                //Natural: IF #PERIODIC-IVC AND ( ADSA600.ADI-OPTN-CDE = 21 )
        {
            if (condition(((((DbsUtil.maskMatches(pdaAdsa600.getAdsa600_Adp_Alt_Dest_Rlvr_Dest(),"'IRA'") || DbsUtil.maskMatches(pdaAdsa600.getAdsa600_Adp_Alt_Dest_Rlvr_Dest(),"'03B'"))  //Natural: IF ( ADSA600.ADP-ALT-DEST-RLVR-DEST = MASK ( 'IRA' ) OR = MASK ( '03B' ) OR = MASK ( 'QPL' ) OR = MASK ( '57B' ) ) AND ADS-PRTCPNT.ADP-IVC-RLVR-CDE = 'N'
                || DbsUtil.maskMatches(pdaAdsa600.getAdsa600_Adp_Alt_Dest_Rlvr_Dest(),"'QPL'")) || DbsUtil.maskMatches(pdaAdsa600.getAdsa600_Adp_Alt_Dest_Rlvr_Dest(),"'57B'")) 
                && ads_Prtcpnt_Adp_Ivc_Rlvr_Cde.equals("N"))))
            {
                pnd_Ivc.setValue(true);                                                                                                                                   //Natural: ASSIGN #IVC := TRUE
                                                                                                                                                                          //Natural: PERFORM INTERFACE-WITH-CPS
                sub_Interface_With_Cps();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Err_Cde_X.notEquals(" ")))                                                                                                              //Natural: IF #ERR-CDE-X NE ' '
                {
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-IA-MASTERS-AND-TRANS
        sub_Create_Ia_Masters_And_Trans();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Err_Cde_X.notEquals(" ")))                                                                                                                      //Natural: IF #ERR-CDE-X NE ' '
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-INSTALLMENT-RECORDS
        //* *    IF ADS-CNTRCT.ADC-PPG-TRGTD(#I) = 'S'
        //* * EM - 012109
        //*  TPA EGTRRA CODE
        //*  ADSA600.ADC-FRM-IRC-CDE(1:4) := ADS-CNTRCT.ADC-FRM-IRC-CDE(1:4)
        //*  ADSA600.ADC-TO-IRC-CDE := ADS-CNTRCT.ADC-TO-IRC-CDE
        //*  ADSA600.ADP-IVC-RLVR-CDE := ADS-PRTCPNT.ADP-IVC-RLVR-CDE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INTERFACE-WITH-CPS
        //*        ADS-IA-RSLT.ADI-IA-RCRD-CDE GT 'PP'
        //* *            #RTB-INST-ADDED
        //* *            #ADP-ALT-DEST-TYP
        //* *            #RTB-INST-ADDED
        //* *            #ADP-ALT-DEST-TYP
        //* *          #RTB-INST-ADDED
        //* *          #ADP-ALT-DEST-TYP
        //* *          #ADP-ALT-DEST-TYP
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-IA-MASTERS-AND-TRANS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-PERIODIC-IVC
        //* *#TIAA-IVC := ADSA600.ADI-TIAA-IVC-AMT - ADSA600.ADI-TIAA-RTB-IVC-AMT
        //* *#CREF-IVC := ADSA600.ADI-CREF-IVC-AMT - ADSA600.ADI-CREF-RTB-IVC-AMT
        //*  IF ADSA600.ADI-OPTN-CDE = 21
        //*    IGNORE
        //*  ELSE
        //*    IF ADSA600.ADI-OPTN-CDE = 21
        //*      IF #TIAA-IVC GT 0
        //*        #IVC-CALL := 'T'
        //*        PERFORM CALL-IVC-CALC-MODULE
        //*        IF #ERR-CDE-X NE ' '
        //*          ESCAPE ROUTINE
        //*        END-IF
        //*      END-IF
        //*    ELSE
        //*  062012 END
        //*      NAZA600I.#PER-PAY-AMT := 0 + ADSA600.ADI-DTL-TIAA-GRD-GRNTD-AMT(*)
        //*        ADSA600.ADI-DTL-TIAA-STD-GRNTD-AMT(*)
        //* * EM - 012109 END
        //*      NAZA600I.#FINAL-PAY-AMT := 0 + ADSA600.ADI-DTL-FNL-STD-PAY-AMT(*)
        //*        ADSA600.ADI-DTL-FNL-GRD-PAY-AMT(*)
        //*    IF ADSA600.ADI-OPTN-CDE = 21
        //*      IF #CREF-IVC GT 0
        //*        #IVC-CALL := 'C'
        //*        PERFORM CALL-IVC-CALC-MODULE
        //*        IF #ERR-CDE-X NE ' '
        //*          ESCAPE ROUTINE
        //*        END-IF
        //*      END-IF
        //*    ELSE
        //*  062012 END
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-REVALUE-FLAGS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-REVALUE-FLAGS-FOR-ODD-MODES
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REVALUE-CREF
        //* ***********************************************************************
        //* **   ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD(#J) NE 'R' AND
        //* **   ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD(#J) NE 'D'
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REVALUE-CREF-MONTHLY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REVALUE-TIAA
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-HISTORICAL-RATES
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-IVC-CALC-MODULE
        //*  C#ADI-DTL-TIAA-DATA := C*ADI-DTL-TIAA-DATA
        //*  #TIAA-STD-GRNTD-AMT(*) :=
        //*    ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT(*)
        //*  #TIAA-STD-DVDND-AMT(*) :=
        //*    ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT(*)
        //*  ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD(1:99)
        //*  #TIAA-STD-GRNTD-AMT(1:99)
        //*  #TIAA-STD-DVDND-AMT(1:99)
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-AIAA025
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-OUT-AIAA0380
        //* ***********************************************************************
        //* ******************************************022207***********************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-PARMS
        //* ******************************************120611***********************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-AIAN035-PARMS
        //* ************************************************ 030912 ***************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-TIAA-FIELDS1
        //* ***********************************************************************
        //* ************************************************ 030912 ***************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-TIAA-FIELDS2
        //* ***********************************************************************
    }
    private void sub_Create_Installment_Records() throws Exception                                                                                                        //Natural: CREATE-INSTALLMENT-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pdaAdsa600.getAdsa600_Adi_Tiaa_Sttlmnt().equals("Y") || pdaAdsa600.getAdsa600_Adi_Tiaa_Re_Sttlmnt().equals("Y")))                                   //Natural: IF ADSA600.ADI-TIAA-STTLMNT = 'Y' OR ADSA600.ADI-TIAA-RE-STTLMNT = 'Y'
        {
            pnd_Tiaa.setValue(true);                                                                                                                                      //Natural: ASSIGN #TIAA := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa600.getAdsa600_Adi_Cref_Sttlmnt().equals("Y")))                                                                                              //Natural: IF ADSA600.ADI-CREF-STTLMNT = 'Y'
        {
            pnd_Cref.setValue(true);                                                                                                                                      //Natural: ASSIGN #CREF := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Asd_A.setValueEdited(ads_Ia_Rslt_Adi_Annty_Strt_Dte,new ReportEditMask("YYYYMMDD"));                                                                          //Natural: MOVE EDITED ADS-IA-RSLT.ADI-ANNTY-STRT-DTE ( EM = YYYYMMDD ) TO #ASD-A
        pnd_Stop_Dte_A.setValue(pdaAdsa600.getAdsa600_Pnd_Ia_Check_Dte());                                                                                                //Natural: ASSIGN #STOP-DTE-A := #IA-CHECK-DTE
        pnd_Stop_Dte_A_Pnd_Stop_Dte_Mm.nsubtract(1);                                                                                                                      //Natural: ASSIGN #STOP-DTE-MM := #STOP-DTE-MM - 1
        if (condition(pnd_Stop_Dte_A_Pnd_Stop_Dte_Mm.equals(getZero())))                                                                                                  //Natural: IF #STOP-DTE-MM = 0
        {
            pnd_Stop_Dte_A_Pnd_Stop_Dte_Yyyy.nsubtract(1);                                                                                                                //Natural: SUBTRACT 1 FROM #STOP-DTE-YYYY
            pnd_Stop_Dte_A_Pnd_Stop_Dte_Mm.setValue(12);                                                                                                                  //Natural: ASSIGN #STOP-DTE-MM := 12
        }                                                                                                                                                                 //Natural: END-IF
        ads_Ia_Rslt_Adi_Sqnce_Nbr.reset();                                                                                                                                //Natural: RESET ADS-IA-RSLT.ADI-SQNCE-NBR #INST-NBR
        pdaAdsa600.getAdsa600_Pnd_Inst_Nbr().reset();
        ads_Ia_Rslt_Adi_Ia_Rcrd_Cde.setValue("PP");                                                                                                                       //Natural: ASSIGN ADS-IA-RSLT.ADI-IA-RCRD-CDE := 'PP'
        ads_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte.setValue(ads_Ia_Rslt_Adi_Annty_Strt_Dte);                                                                                      //Natural: ASSIGN ADS-IA-RSLT.ADI-FRST-PYMNT-DUE-DTE := ADS-IA-RSLT.ADI-ANNTY-STRT-DTE
        pdaAdsa600.getAdsa600_Adi_Frst_Pymnt_Due_Dte().setValue(ads_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte);                                                                      //Natural: ASSIGN ADSA600.ADI-FRST-PYMNT-DUE-DTE := ADS-IA-RSLT.ADI-FRST-PYMNT-DUE-DTE
        ads_Ia_Rslt_Adi_Frst_Ck_Pd_Dte.setValue(pnd_Acctg_Dte);                                                                                                           //Natural: ASSIGN ADS-IA-RSLT.ADI-FRST-CK-PD-DTE := #ACCTG-DTE
        pdaAdsa600.getAdsa600_Adi_Frst_Ck_Pd_Dte().setValue(pnd_Acctg_Dte);                                                                                               //Natural: ASSIGN ADSA600.ADI-FRST-CK-PD-DTE := #ACCTG-DTE
        //*  030912 START
        if (condition(pnd_More.getBoolean()))                                                                                                                             //Natural: IF #MORE
        {
            ads_Ia_Rsl2_Adi_Ia_Rcrd_Cde.setValue("PP1");                                                                                                                  //Natural: ASSIGN ADS-IA-RSL2.ADI-IA-RCRD-CDE := 'PP1'
            ads_Ia_Rsl2_Adi_Frst_Pymnt_Due_Dte.setValue(ads_Ia_Rslt_Adi_Frst_Pymnt_Due_Dte);                                                                              //Natural: ASSIGN ADS-IA-RSL2.ADI-FRST-PYMNT-DUE-DTE := ADS-IA-RSLT.ADI-FRST-PYMNT-DUE-DTE
            ads_Ia_Rsl2_Adi_Frst_Ck_Pd_Dte.setValue(ads_Ia_Rslt_Adi_Frst_Ck_Pd_Dte);                                                                                      //Natural: ASSIGN ADS-IA-RSL2.ADI-FRST-CK-PD-DTE := ADS-IA-RSLT.ADI-FRST-CK-PD-DTE
            //*  030912 END
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Next_Dte_A.setValue(pnd_Asd_A);                                                                                                                               //Natural: ASSIGN #NEXT-DTE-A := #ASD-A
        short decideConditionsMet1481 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE ADS-IA-RSLT.ADI-PYMNT-MODE;//Natural: VALUE 100
        if (condition((ads_Ia_Rslt_Adi_Pymnt_Mode.equals(100))))
        {
            decideConditionsMet1481++;
            pnd_Factor_Mm.setValue(1);                                                                                                                                    //Natural: ASSIGN #FACTOR-MM := 1
            pnd_No_Per_Yr.setValue(12);                                                                                                                                   //Natural: ASSIGN #NO-PER-YR := 12
        }                                                                                                                                                                 //Natural: VALUE 601:603
        else if (condition(((ads_Ia_Rslt_Adi_Pymnt_Mode.greaterOrEqual(601) && ads_Ia_Rslt_Adi_Pymnt_Mode.lessOrEqual(603)))))
        {
            decideConditionsMet1481++;
            pnd_Factor_Mm.setValue(3);                                                                                                                                    //Natural: ASSIGN #FACTOR-MM := 3
            pnd_No_Per_Yr.setValue(4);                                                                                                                                    //Natural: ASSIGN #NO-PER-YR := 4
        }                                                                                                                                                                 //Natural: VALUE 701:706
        else if (condition(((ads_Ia_Rslt_Adi_Pymnt_Mode.greaterOrEqual(701) && ads_Ia_Rslt_Adi_Pymnt_Mode.lessOrEqual(706)))))
        {
            decideConditionsMet1481++;
            pnd_Factor_Mm.setValue(6);                                                                                                                                    //Natural: ASSIGN #FACTOR-MM := 6
            pnd_No_Per_Yr.setValue(2);                                                                                                                                    //Natural: ASSIGN #NO-PER-YR := 2
        }                                                                                                                                                                 //Natural: VALUE 801:812
        else if (condition(((ads_Ia_Rslt_Adi_Pymnt_Mode.greaterOrEqual(801) && ads_Ia_Rslt_Adi_Pymnt_Mode.lessOrEqual(812)))))
        {
            decideConditionsMet1481++;
            pnd_Factor_Mm.setValue(12);                                                                                                                                   //Natural: ASSIGN #FACTOR-MM := 12
            pnd_No_Per_Yr.setValue(1);                                                                                                                                    //Natural: ASSIGN #NO-PER-YR := 1
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Factor_Mm.setValue(1);                                                                                                                                    //Natural: ASSIGN #FACTOR-MM := 1
            pnd_No_Per_Yr.setValue(12);                                                                                                                                   //Natural: ASSIGN #NO-PER-YR := 12
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Asd_A_Pnd_Asd_N6.lessOrEqual(pnd_Stop_Dte_A_Pnd_Stop_N6)))                                                                                      //Natural: IF #ASD-N6 LE #STOP-N6
        {
            if (condition(pnd_Factor_Mm.equals(1) && pnd_Asd_A_Pnd_Asd_N6.equals(pnd_Stop_Dte_A_Pnd_Stop_N6)))                                                            //Natural: IF #FACTOR-MM = 1 AND #ASD-N6 = #STOP-N6
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAdsa600.getAdsa600_Pnd_Inst_Nbr().setValue(1);                                                                                                         //Natural: ASSIGN #INST-NBR := 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALCULATE-PERIODIC-IVC
        sub_Calculate_Periodic_Ivc();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Err_Cde_X.notEquals(" ")))                                                                                                                      //Natural: IF #ERR-CDE-X NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Bsnss_Dte_A.setValueEdited(pnd_Bsnss_Dte,new ReportEditMask("YYYYMMDD"));                                                                                     //Natural: MOVE EDITED #BSNSS-DTE ( EM = YYYYMMDD ) TO #BSNSS-DTE-A
        pnd_Acctg_Dte_A.setValueEdited(pnd_Acctg_Dte,new ReportEditMask("YYYYMMDD"));                                                                                     //Natural: MOVE EDITED #ACCTG-DTE ( EM = YYYYMMDD ) TO #ACCTG-DTE-A
        //*  DETERMINE NUMBER OF INSTALLMENTS
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pdaAdsa600.getAdsa600_Pnd_Inst_Nbr().greater(getZero())))                                                                                       //Natural: IF #INST-NBR GT 0
            {
                pnd_Next_Dte_A_Pnd_Next_Dte_Mm.nadd(pnd_Factor_Mm);                                                                                                       //Natural: ADD #FACTOR-MM TO #NEXT-DTE-MM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Next_Dte_A_Pnd_Next_Dte_Mm.greater(12)))                                                                                                    //Natural: IF #NEXT-DTE-MM GT 12
            {
                pnd_Next_Dte_A_Pnd_Next_Dte_Yyyy.nadd(1);                                                                                                                 //Natural: ADD 1 TO #NEXT-DTE-YYYY
                pnd_Next_Dte_A_Pnd_Next_Dte_Mm.nsubtract(12);                                                                                                             //Natural: SUBTRACT 12 FROM #NEXT-DTE-MM
            }                                                                                                                                                             //Natural: END-IF
            pdaAdsa600.getAdsa600_Pnd_Inst_Nbr().nadd(1);                                                                                                                 //Natural: ADD 1 TO #INST-NBR
            if (condition(pnd_Next_Dte_A_Pnd_Next_N6.greater(pnd_Stop_Dte_A_Pnd_Stop_N6)))                                                                                //Natural: IF #NEXT-N6 GT #STOP-N6
            {
                pnd_F_Or_N.setValue("N");                                                                                                                                 //Natural: ASSIGN #F-OR-N := 'N'
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Next_Dte_A_Pnd_Next_N6.equals(pnd_Stop_Dte_A_Pnd_Stop_N6)))                                                                                 //Natural: IF #NEXT-N6 = #STOP-N6
            {
                if (condition(pnd_Stop_Dte_A_Pnd_Stop_N6.greater(pnd_Acctg_Dte_A_Pnd_Acctg_N6)))                                                                          //Natural: IF #STOP-N6 GT #ACCTG-N6
                {
                    pnd_F_Or_N.setValue("F");                                                                                                                             //Natural: ASSIGN #F-OR-N := 'F'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        if (condition(pdaAdsa600.getAdsa600_Pnd_Inst_Nbr().equals(getZero())))                                                                                            //Natural: IF #INST-NBR = 0
        {
            pnd_Err_Cde_X.setValue("L21");                                                                                                                                //Natural: ASSIGN #ERR-CDE-X := 'L21'
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Next_Dte_A.setValue(pnd_Asd_A);                                                                                                                               //Natural: ASSIGN #NEXT-DTE-A := #ASD-A
        pnd_Last_Inst_Dte_A.setValue(pnd_Next_Dte_A);                                                                                                                     //Natural: ASSIGN #LAST-INST-DTE-A := #NEXT-DTE-A
        pnd_C_Reval_Dte_A.setValue(pnd_Next_Dte_A);                                                                                                                       //Natural: ASSIGN #C-REVAL-DTE-A := #T-REVAL-DTE-A := #NEXT-DTE-A
        pnd_T_Reval_Dte_A.setValue(pnd_Next_Dte_A);
        pnd_C_Reval_Dte_A_Pnd_C_Reval_Dd.setValue("01");                                                                                                                  //Natural: ASSIGN #C-REVAL-DD := '01'
        pnd_T_Reval_Dte_A_Pnd_T_Reval_Dd.setValue("01");                                                                                                                  //Natural: ASSIGN #T-REVAL-DD := '01'
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO #INST-NBR
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaAdsa600.getAdsa600_Pnd_Inst_Nbr())); pnd_I.nadd(1))
        {
            if (condition(pnd_I.greater(1)))                                                                                                                              //Natural: IF #I GT 1
            {
                pnd_Next_Dte_A_Pnd_Next_Dte_Mm.nadd(pnd_Factor_Mm);                                                                                                       //Natural: ADD #FACTOR-MM TO #NEXT-DTE-MM
                if (condition(pnd_Next_Dte_A_Pnd_Next_Dte_Mm.greater(12)))                                                                                                //Natural: IF #NEXT-DTE-MM GT 12
                {
                    pnd_Next_Dte_A_Pnd_Next_Dte_Yyyy.nadd(1);                                                                                                             //Natural: ADD 1 TO #NEXT-DTE-YYYY
                    pnd_Next_Dte_A_Pnd_Next_Dte_Mm.nsubtract(12);                                                                                                         //Natural: SUBTRACT 12 FROM #NEXT-DTE-MM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Next_Dte_A_Pnd_Next_Dte_Dd.setValue("01");                                                                                                            //Natural: ASSIGN #NEXT-DTE-DD := '01'
            }                                                                                                                                                             //Natural: END-IF
            ads_Ia_Rslt_Adi_Instllmnt_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Next_Dte_A);                                                                  //Natural: MOVE EDITED #NEXT-DTE-A TO ADS-IA-RSLT.ADI-INSTLLMNT-DTE ( EM = YYYYMMDD )
            if (condition(pnd_I.equals(pdaAdsa600.getAdsa600_Pnd_Inst_Nbr())))                                                                                            //Natural: IF #I = #INST-NBR
            {
                ads_Ia_Rslt_Adi_Sqnce_Nbr.setValue(999);                                                                                                                  //Natural: ASSIGN ADS-IA-RSLT.ADI-SQNCE-NBR := 999
                if (condition(pnd_F_Or_N.notEquals(" ")))                                                                                                                 //Natural: IF #F-OR-N NE ' '
                {
                    ads_Ia_Rslt_Adi_Pymnt_Cde.setValue(pnd_F_Or_N);                                                                                                       //Natural: ASSIGN ADS-IA-RSLT.ADI-PYMNT-CDE := #F-OR-N
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ads_Ia_Rslt_Adi_Sqnce_Nbr.setValue(pnd_I);                                                                                                                //Natural: ASSIGN ADS-IA-RSLT.ADI-SQNCE-NBR := #I
            }                                                                                                                                                             //Natural: END-IF
            //*  030912 START
            if (condition(pnd_More.getBoolean()))                                                                                                                         //Natural: IF #MORE
            {
                ads_Ia_Rsl2_Adi_Instllmnt_Dte.setValue(ads_Ia_Rslt_Adi_Instllmnt_Dte);                                                                                    //Natural: ASSIGN ADS-IA-RSL2.ADI-INSTLLMNT-DTE := ADS-IA-RSLT.ADI-INSTLLMNT-DTE
                ads_Ia_Rsl2_Adi_Pymnt_Cde.setValue(ads_Ia_Rslt_Adi_Pymnt_Cde);                                                                                            //Natural: ASSIGN ADS-IA-RSL2.ADI-PYMNT-CDE := ADS-IA-RSLT.ADI-PYMNT-CDE
                ads_Ia_Rsl2_Adi_Sqnce_Nbr.setValue(ads_Ia_Rslt_Adi_Sqnce_Nbr);                                                                                            //Natural: ASSIGN ADS-IA-RSL2.ADI-SQNCE-NBR := ADS-IA-RSLT.ADI-SQNCE-NBR
                //*  030912 END
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_I.greater(1)))                                                                                                                              //Natural: IF #I GT 1
            {
                if (condition(pnd_Asd_A_Pnd_Asd_Mm.equals(4)))                                                                                                            //Natural: IF #ASD-MM = 04
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM SET-REVALUE-FLAGS
                    sub_Set_Revalue_Flags();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Revalue_Tiaa.getBoolean()))                                                                                                         //Natural: IF #REVALUE-TIAA
                    {
                                                                                                                                                                          //Natural: PERFORM REVALUE-TIAA
                        sub_Revalue_Tiaa();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Err_Cde_X.notEquals(" ")))                                                                                                      //Natural: IF #ERR-CDE-X NE ' '
                        {
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Revalue_Cref.getBoolean()))                                                                                                         //Natural: IF #REVALUE-CREF
                    {
                                                                                                                                                                          //Natural: PERFORM REVALUE-CREF
                        sub_Revalue_Cref();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Err_Cde_X.notEquals(" ")))                                                                                                      //Natural: IF #ERR-CDE-X NE ' '
                        {
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units.getValue("*").greater(getZero()) && pnd_Next_Dte_A.lessOrEqual(pdaAdsa600.getAdsa600_Pnd_Ia_Check_Dte()))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( * ) GT 0 AND #NEXT-DTE-A LE #IA-CHECK-DTE
                {
                                                                                                                                                                          //Natural: PERFORM REVALUE-CREF-MONTHLY
                    sub_Revalue_Cref_Monthly();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Revalue_Tiaa.reset();                                                                                                                                 //Natural: RESET #REVALUE-TIAA #REVALUE-CREF
                pnd_Revalue_Cref.reset();
            }                                                                                                                                                             //Natural: END-IF
            vw_ads_Ia_Rslt.insertDBRow();                                                                                                                                 //Natural: STORE ADS-IA-RSLT
            //*  030912 START
            if (condition(pnd_More.getBoolean()))                                                                                                                         //Natural: IF #MORE
            {
                vw_ads_Ia_Rsl2.insertDBRow();                                                                                                                             //Natural: STORE ADS-IA-RSL2
                //*  030912 END
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Adi_Super1.equals(" ")))                                                                                                                    //Natural: IF #ADI-SUPER1 = ' '
            {
                pnd_Adi_Super1_Super_Structure.setValuesByName(vw_ads_Ia_Rslt);                                                                                           //Natural: MOVE BY NAME ADS-IA-RSLT TO SUPER-STRUCTURE
                pnd_Adi_Super1_Adi_Ia_Rcrd_Cde.setValue("PP");                                                                                                            //Natural: ASSIGN #ADI-SUPER1.ADI-IA-RCRD-CDE := 'PP'
                pnd_Adi_Super1_Adi_Sqnce_Nbr.reset();                                                                                                                     //Natural: RESET #ADI-SUPER1.ADI-SQNCE-NBR
            }                                                                                                                                                             //Natural: END-IF
            pnd_Last_Inst_Dte_A.setValue(pnd_Next_Dte_A);                                                                                                                 //Natural: ASSIGN #LAST-INST-DTE-A := #NEXT-DTE-A
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        vw_ads_Cntrct.startDatabaseFind                                                                                                                                   //Natural: FIND ( 1 ) ADS-CNTRCT WITH RQST-ID = ADS-IA-RSLT.RQST-ID
        (
        "FIND01",
        new Wc[] { new Wc("RQST_ID", "=", ads_Ia_Rslt_Rqst_Id, WcType.WITH) },
        1
        );
        FIND01:
        while (condition(vw_ads_Cntrct.readNextRow("FIND01")))
        {
            vw_ads_Cntrct.setIfNotFoundControlFlag(false);
            FOR02:                                                                                                                                                        //Natural: FOR #I 1 TO 15
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(15)); pnd_I.nadd(1))
            {
                pdaAdsa600.getAdsa600_Adc_Ppg_Cde().setValue(ads_Cntrct_Adc_Ppg_Cde.getValue(pnd_I));                                                                     //Natural: ASSIGN ADSA600.ADC-PPG-CDE := ADS-CNTRCT.ADC-PPG-CDE ( #I )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //* *    END-IF
                //*  03072007
                //*  010512 START
                //*  010512 END
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaAdsa600.getAdsa600_Adc_Acct_Cde().getValue("*").setValue(ads_Cntrct_Adc_Acct_Cde.getValue("*"));                                                           //Natural: ASSIGN ADSA600.ADC-ACCT-CDE ( * ) := ADS-CNTRCT.ADC-ACCT-CDE ( * )
            pdaAdsa600.getAdsa600_Adc_Trk_Ro_Ivc().setValue(ads_Cntrct_Adc_Trk_Ro_Ivc);                                                                                   //Natural: ASSIGN ADSA600.ADC-TRK-RO-IVC := ADS-CNTRCT.ADC-TRK-RO-IVC
            pdaAdsa600.getAdsa600_Adc_Sub_Plan_Nbr().setValue(ads_Cntrct_Adc_Sub_Plan_Nbr);                                                                               //Natural: ASSIGN ADSA600.ADC-SUB-PLAN-NBR := ADS-CNTRCT.ADC-SUB-PLAN-NBR
            pdaAdsa600.getAdsa600_Adc_Original_Cntrct_Nbr().setValue(ads_Cntrct_Adc_Original_Cntrct_Nbr);                                                                 //Natural: ASSIGN ADSA600.ADC-ORIGINAL-CNTRCT-NBR := ADS-CNTRCT.ADC-ORIGINAL-CNTRCT-NBR
            pdaAdsa600.getAdsa600_Adc_Original_Sub_Plan_Nbr().setValue(ads_Cntrct_Adc_Original_Sub_Plan_Nbr);                                                             //Natural: ASSIGN ADSA600.ADC-ORIGINAL-SUB-PLAN-NBR := ADS-CNTRCT.ADC-ORIGINAL-SUB-PLAN-NBR
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*                                              /* CM 3/18/10 CANADIAN
        if (condition(ads_Prtcpnt_Adp_Plan_Nbr.equals("152004")))                                                                                                         //Natural: IF ADS-PRTCPNT.ADP-PLAN-NBR = '152004'
        {
            pdaAdsa600.getAdsa600_Adc_Ppg_Cde().setValue("Y1650");                                                                                                        //Natural: ASSIGN ADSA600.ADC-PPG-CDE := 'Y1650'
            //*  CM
            //*  EM - 100313
        }                                                                                                                                                                 //Natural: END-IF
        pdaAdsa600.getAdsa600_Adc_T395_Fund_Id().getValue("*").setValue(ads_Cntrct_Adc_T395_Fund_Id.getValue("*"));                                                       //Natural: ASSIGN ADSA600.ADC-T395-FUND-ID ( * ) := ADS-CNTRCT.ADC-T395-FUND-ID ( * )
        pdaAdsa600.getAdsa600_Adc_Invstmnt_Grpng_Id().getValue("*").setValue(ads_Cntrct_Adc_Invstmnt_Grpng_Id.getValue("*"));                                             //Natural: ASSIGN ADSA600.ADC-INVSTMNT-GRPNG-ID ( * ) := ADS-CNTRCT.ADC-INVSTMNT-GRPNG-ID ( * )
    }
    private void sub_Interface_With_Cps() throws Exception                                                                                                                //Natural: INTERFACE-WITH-CPS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_F_Or_N.equals("N") && pdaAdsa600.getAdsa600_Pnd_Inst_Nbr().equals(1)))                                                                          //Natural: IF #F-OR-N = 'N' AND #INST-NBR = 1
        {
            DbsUtil.callnat(Adsn604.class , getCurrentProcessState(), pdaAdsa600.getAdsa600(), pnd_Prtcpnt_Isn);                                                          //Natural: CALLNAT 'ADSN604' ADSA600 #PRTCPNT-ISN
            if (condition(Global.isEscape())) return;
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Tiaa.getBoolean()))                                                                                                                             //Natural: IF #TIAA
        {
            pnd_Cmpny.setValue("T");                                                                                                                                      //Natural: ASSIGN #CMPNY := 'T'
            pnd_First.setValue(true);                                                                                                                                     //Natural: ASSIGN #FIRST := TRUE
            vw_ads_Ia_Rslt.startDatabaseRead                                                                                                                              //Natural: READ ADS-IA-RSLT BY ADI-SUPER1 STARTING FROM #ADI-SUPER1
            (
            "R2",
            new Wc[] { new Wc("ADI_SUPER1", ">=", pnd_Adi_Super1, WcType.BY) },
            new Oc[] { new Oc("ADI_SUPER1", "ASC") }
            );
            R2:
            while (condition(vw_ads_Ia_Rslt.readNextRow("R2")))
            {
                //*  030912
                if (condition(ads_Ia_Rslt_Rqst_Id.notEquals(pnd_Adi_Super1_Rqst_Id) || ads_Ia_Rslt_Adi_Ia_Rcrd_Cde.greater("PP ")))                                       //Natural: IF ADS-IA-RSLT.RQST-ID NE #ADI-SUPER1.RQST-ID OR ADS-IA-RSLT.ADI-IA-RCRD-CDE GT 'PP '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pdaAdsa600.getAdsa600().setValuesByName(vw_ads_Ia_Rslt);                                                                                                  //Natural: MOVE BY NAME ADS-IA-RSLT TO ADSA600
                //*  030912 START
                pnd_B.reset();                                                                                                                                            //Natural: RESET #B
                                                                                                                                                                          //Natural: PERFORM MOVE-TIAA-FIELDS1
                sub_Move_Tiaa_Fields1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("R2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_More.getBoolean()))                                                                                                                     //Natural: IF #MORE
                {
                    pnd_Adi_Super2_Super_Structure2.setValuesByName(vw_ads_Ia_Rslt);                                                                                      //Natural: MOVE BY NAME ADS-IA-RSLT TO SUPER-STRUCTURE2
                    pnd_Adi_Super2_Adi_Ia_Rcrd_Cde.setValue("PP1");                                                                                                       //Natural: ASSIGN #ADI-SUPER2.ADI-IA-RCRD-CDE := 'PP1'
                    vw_ads_Ia_Rsl2.startDatabaseFind                                                                                                                      //Natural: FIND ADS-IA-RSL2 WITH ADI-SUPER1 = #ADI-SUPER2
                    (
                    "FIND02",
                    new Wc[] { new Wc("ADI_SUPER1", "=", pnd_Adi_Super2, WcType.WITH) }
                    );
                    FIND02:
                    while (condition(vw_ads_Ia_Rsl2.readNextRow("FIND02", true)))
                    {
                        vw_ads_Ia_Rsl2.setIfNotFoundControlFlag(false);
                        if (condition(vw_ads_Ia_Rsl2.getAstCOUNTER().equals(0)))                                                                                          //Natural: IF NO RECORDS FOUND
                        {
                            getReports().write(0, "IA result not found for ",pnd_Adi_Super2);                                                                             //Natural: WRITE 'IA result not found for ' #ADI-SUPER2
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Err_Cde_X.setValue("L75");                                                                                                                //Natural: ASSIGN #ERR-CDE-X := 'L75'
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-NOREC
                        pnd_B.setValue(pnd_Max_Rslt);                                                                                                                     //Natural: ASSIGN #B := #MAX-RSLT
                                                                                                                                                                          //Natural: PERFORM MOVE-TIAA-FIELDS2
                        sub_Move_Tiaa_Fields2();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-FIND
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("R2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("R2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  030912 END
                }                                                                                                                                                         //Natural: END-IF
                pnd_Instllmnt_A6.setValueEdited(ads_Ia_Rslt_Adi_Instllmnt_Dte,new ReportEditMask("YYYYMM"));                                                              //Natural: MOVE EDITED ADS-IA-RSLT.ADI-INSTLLMNT-DTE ( EM = YYYYMM ) TO #INSTLLMNT-A6
                if (condition(pnd_Instllmnt_A6.lessOrEqual(pnd_Check_A6)))                                                                                                //Natural: IF #INSTLLMNT-A6 LE #CHECK-A6
                {
                    if (condition(ads_Ia_Rslt_Adi_Pymnt_Cde.equals("N")))                                                                                                 //Natural: IF ADS-IA-RSLT.ADI-PYMNT-CDE = 'N'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        short decideConditionsMet1694 = 0;                                                                                                                //Natural: DECIDE ON FIRST VALUE ADS-IA-RSLT.ADI-OPTN-CDE;//Natural: VALUE 21
                        if (condition((ads_Ia_Rslt_Adi_Optn_Cde.equals(21))))
                        {
                            decideConditionsMet1694++;
                            //*  120611
                            DbsUtil.callnat(Adsn602b.class , getCurrentProcessState(), pdaAdsa600.getAdsa600(), pdaFcpa140f.getGtn_Pda_F(), pnd_Cmpny,                    //Natural: CALLNAT 'ADSN602B' ADSA600 GTN-PDA-F #CMPNY #PRTCPNT-ISN #FIRST #BSNSS-DTE #ACCTG-DTE #ERR-CDE-X #NAME #F-OR-N #ADP-ALT-DEST-HOLD-CDE #ADP-CRRSPNDNCE-PERM-ADDR-TXT ( * ) #ADP-CRRSPNDNCE-PERM-ADDR-ZIP #ADP-CRRSPNDNCE-PERM-ADDR-TYP-CD #IVC #IVC-PYMNT-RULE #IA-ORGN-CODE
                                pnd_Prtcpnt_Isn, pnd_First, pnd_Bsnss_Dte, pnd_Acctg_Dte, pnd_Err_Cde_X, pnd_Name, pnd_F_Or_N, pnd_Adp_Alt_Dest_Hold_Cde, 
                                pnd_Adp_Crrspndnce_Perm_Addr_Txt.getValue("*"), pnd_Adp_Crrspndnce_Perm_Addr_Zip, pnd_Adp_Crrspndnce_Perm_Addr_Typ_Cd, pnd_Ivc, 
                                pnd_Ivc_Pymnt_Rule, pnd_Ia_Orgn_Code);
                            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                        }                                                                                                                                                 //Natural: NONE VALUE
                        else if (condition())
                        {
                            //*  120611
                            DbsUtil.callnat(Adsn602.class , getCurrentProcessState(), pdaAdsa600.getAdsa600(), pdaFcpa140f.getGtn_Pda_F(), pnd_Cmpny,                     //Natural: CALLNAT 'ADSN602' ADSA600 GTN-PDA-F #CMPNY #PRTCPNT-ISN #FIRST #BSNSS-DTE #ACCTG-DTE #ERR-CDE-X #NAME #F-OR-N #ADP-ALT-DEST-HOLD-CDE #ADP-CRRSPNDNCE-PERM-ADDR-TXT ( * ) #ADP-CRRSPNDNCE-PERM-ADDR-ZIP #ADP-CRRSPNDNCE-PERM-ADDR-TYP-CD #IA-ORGN-CODE
                                pnd_Prtcpnt_Isn, pnd_First, pnd_Bsnss_Dte, pnd_Acctg_Dte, pnd_Err_Cde_X, pnd_Name, pnd_F_Or_N, pnd_Adp_Alt_Dest_Hold_Cde, 
                                pnd_Adp_Crrspndnce_Perm_Addr_Txt.getValue("*"), pnd_Adp_Crrspndnce_Perm_Addr_Zip, pnd_Adp_Crrspndnce_Perm_Addr_Typ_Cd, pnd_Ia_Orgn_Code);
                            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                        }                                                                                                                                                 //Natural: END-DECIDE
                        if (condition(pnd_Err_Cde_X.notEquals(" ")))                                                                                                      //Natural: IF #ERR-CDE-X NE ' '
                        {
                            if (condition(DbsUtil.is(pdaFcpa140f.getGtn_Pda_F_Gtn_Ret_Code().getText(),"N4")))                                                            //Natural: IF GTN-PDA-F.GTN-RET-CODE IS ( N4 )
                            {
                                DbsUtil.examine(new ExamineSource(pdaFcpa140f.getGtn_Pda_F_Gtn_Ret_Msg()), new ExamineSearch(" "), new ExamineGivingLength(pnd_Len));     //Natural: EXAMINE GTN-PDA-F.GTN-RET-MSG FOR ' ' GIVING LENGTH #LEN
                                pnd_Err_Nr.setValue(pdaFcpa140f.getGtn_Pda_F_Gtn_Ret_Code());                                                                             //Natural: ASSIGN #ERR-NR := GTN-PDA-F.GTN-RET-CODE
                                if (condition(pnd_Len.equals(29)))                                                                                                        //Natural: IF #LEN = 29
                                {
                                    pnd_Program_Nme.setValue(pdaFcpa140f.getGtn_Pda_F_Gtn_Ret_Msg().getSubstring(1,8));                                                   //Natural: ASSIGN #PROGRAM-NME := SUBSTR ( GTN-PDA-F.GTN-RET-MSG,1,8 )
                                    pnd_Err_Line_X.setValue(pdaFcpa140f.getGtn_Pda_F_Gtn_Ret_Msg().getSubstring(26,4));                                                   //Natural: ASSIGN #ERR-LINE-X := SUBSTR ( GTN-PDA-F.GTN-RET-MSG,26,4 )
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    pnd_Program_Nme.setValue(pdaFcpa140f.getGtn_Pda_F_Gtn_Ret_Msg().getSubstring(1,8));                                                   //Natural: ASSIGN #PROGRAM-NME := SUBSTR ( GTN-PDA-F.GTN-RET-MSG,1,8 )
                                    pnd_Err_Line_X.setValue(pdaFcpa140f.getGtn_Pda_F_Gtn_Ret_Msg().getSubstring(25,4));                                                   //Natural: ASSIGN #ERR-LINE-X := SUBSTR ( GTN-PDA-F.GTN-RET-MSG,25,4 )
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Cref.getBoolean()))                                                                                                                             //Natural: IF #CREF
        {
            pnd_Cmpny.setValue("C");                                                                                                                                      //Natural: ASSIGN #CMPNY := 'C'
            pnd_First.setValue(true);                                                                                                                                     //Natural: ASSIGN #FIRST := TRUE
            vw_ads_Ia_Rslt.startDatabaseRead                                                                                                                              //Natural: READ ADS-IA-RSLT BY ADI-SUPER1 STARTING FROM #ADI-SUPER1
            (
            "R3",
            new Wc[] { new Wc("ADI_SUPER1", ">=", pnd_Adi_Super1, WcType.BY) },
            new Oc[] { new Oc("ADI_SUPER1", "ASC") }
            );
            R3:
            while (condition(vw_ads_Ia_Rslt.readNextRow("R3")))
            {
                if (condition(ads_Ia_Rslt_Rqst_Id.notEquals(pnd_Adi_Super1_Rqst_Id) || ads_Ia_Rslt_Adi_Ia_Rcrd_Cde.greater("PP")))                                        //Natural: IF ADS-IA-RSLT.RQST-ID NE #ADI-SUPER1.RQST-ID OR ADS-IA-RSLT.ADI-IA-RCRD-CDE GT 'PP'
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pdaAdsa600.getAdsa600().setValuesByName(vw_ads_Ia_Rslt);                                                                                                  //Natural: MOVE BY NAME ADS-IA-RSLT TO ADSA600
                pnd_Instllmnt_A6.setValueEdited(ads_Ia_Rslt_Adi_Instllmnt_Dte,new ReportEditMask("YYYYMM"));                                                              //Natural: MOVE EDITED ADS-IA-RSLT.ADI-INSTLLMNT-DTE ( EM = YYYYMM ) TO #INSTLLMNT-A6
                if (condition(pnd_Instllmnt_A6.lessOrEqual(pnd_Check_A6)))                                                                                                //Natural: IF #INSTLLMNT-A6 LE #CHECK-A6
                {
                    if (condition(ads_Ia_Rslt_Adi_Pymnt_Cde.equals("N")))                                                                                                 //Natural: IF ADS-IA-RSLT.ADI-PYMNT-CDE = 'N'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ads_Ia_Rslt_Adi_Optn_Cde.equals(21)))                                                                                               //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 21
                        {
                            //*  120611
                            DbsUtil.callnat(Adsn602b.class , getCurrentProcessState(), pdaAdsa600.getAdsa600(), pdaFcpa140f.getGtn_Pda_F(), pnd_Cmpny,                    //Natural: CALLNAT 'ADSN602B' ADSA600 GTN-PDA-F #CMPNY #PRTCPNT-ISN #FIRST #BSNSS-DTE #ACCTG-DTE #ERR-CDE-X #NAME #F-OR-N #ADP-ALT-DEST-HOLD-CDE #ADP-CRRSPNDNCE-PERM-ADDR-TXT ( * ) #ADP-CRRSPNDNCE-PERM-ADDR-ZIP #ADP-CRRSPNDNCE-PERM-ADDR-TYP-CD #IVC #IVC-PYMNT-RULE #IA-ORGN-CODE
                                pnd_Prtcpnt_Isn, pnd_First, pnd_Bsnss_Dte, pnd_Acctg_Dte, pnd_Err_Cde_X, pnd_Name, pnd_F_Or_N, pnd_Adp_Alt_Dest_Hold_Cde, 
                                pnd_Adp_Crrspndnce_Perm_Addr_Txt.getValue("*"), pnd_Adp_Crrspndnce_Perm_Addr_Zip, pnd_Adp_Crrspndnce_Perm_Addr_Typ_Cd, pnd_Ivc, 
                                pnd_Ivc_Pymnt_Rule, pnd_Ia_Orgn_Code);
                            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  120611
                            DbsUtil.callnat(Adsn602.class , getCurrentProcessState(), pdaAdsa600.getAdsa600(), pdaFcpa140f.getGtn_Pda_F(), pnd_Cmpny,                     //Natural: CALLNAT 'ADSN602' ADSA600 GTN-PDA-F #CMPNY #PRTCPNT-ISN #FIRST #BSNSS-DTE #ACCTG-DTE #ERR-CDE-X #NAME #F-OR-N #ADP-ALT-DEST-HOLD-CDE #ADP-CRRSPNDNCE-PERM-ADDR-TXT ( * ) #ADP-CRRSPNDNCE-PERM-ADDR-ZIP #ADP-CRRSPNDNCE-PERM-ADDR-TYP-CD #IA-ORGN-CODE
                                pnd_Prtcpnt_Isn, pnd_First, pnd_Bsnss_Dte, pnd_Acctg_Dte, pnd_Err_Cde_X, pnd_Name, pnd_F_Or_N, pnd_Adp_Alt_Dest_Hold_Cde, 
                                pnd_Adp_Crrspndnce_Perm_Addr_Txt.getValue("*"), pnd_Adp_Crrspndnce_Perm_Addr_Zip, pnd_Adp_Crrspndnce_Perm_Addr_Typ_Cd, pnd_Ia_Orgn_Code);
                            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Err_Cde_X.notEquals(" ")))                                                                                                      //Natural: IF #ERR-CDE-X NE ' '
                        {
                            if (condition(DbsUtil.is(pdaFcpa140f.getGtn_Pda_F_Gtn_Ret_Code().getText(),"N4")))                                                            //Natural: IF GTN-PDA-F.GTN-RET-CODE IS ( N4 )
                            {
                                DbsUtil.examine(new ExamineSource(pdaFcpa140f.getGtn_Pda_F_Gtn_Ret_Msg()), new ExamineSearch(" "), new ExamineGivingLength(pnd_Len));     //Natural: EXAMINE GTN-PDA-F.GTN-RET-MSG FOR ' ' GIVING LENGTH #LEN
                                pnd_Err_Nr.setValue(pdaFcpa140f.getGtn_Pda_F_Gtn_Ret_Code());                                                                             //Natural: ASSIGN #ERR-NR := GTN-PDA-F.GTN-RET-CODE
                                if (condition(pnd_Len.equals(29)))                                                                                                        //Natural: IF #LEN = 29
                                {
                                    pnd_Program_Nme.setValue(pdaFcpa140f.getGtn_Pda_F_Gtn_Ret_Msg().getSubstring(1,8));                                                   //Natural: ASSIGN #PROGRAM-NME := SUBSTR ( GTN-PDA-F.GTN-RET-MSG,1,8 )
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    pnd_Program_Nme.setValue(pdaFcpa140f.getGtn_Pda_F_Gtn_Ret_Msg().getSubstring(1,8));                                                   //Natural: ASSIGN #PROGRAM-NME := SUBSTR ( GTN-PDA-F.GTN-RET-MSG,1,8 )
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                            if (true) return;                                                                                                                             //Natural: ESCAPE ROUTINE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Dest_Cde.setValue(pdaFcpa140f.getGtn_Pda_F_Cntrct_Roll_Dest_Cde());                                                                                           //Natural: ASSIGN #DEST-CDE := GTN-PDA-F.CNTRCT-ROLL-DEST-CDE
    }
    private void sub_Create_Ia_Masters_And_Trans() throws Exception                                                                                                       //Natural: CREATE-IA-MASTERS-AND-TRANS
    {
        if (BLNatReinput.isReinput()) return;

        pdaAdsa600.getAdsa600_Adp_Ivc_Pymnt_Rule().setValue(pnd_Ivc_Pymnt_Rule);                                                                                          //Natural: ASSIGN ADSA600.ADP-IVC-PYMNT-RULE := #IVC-PYMNT-RULE
        DbsUtil.callnat(Adsn601.class , getCurrentProcessState(), pdaAdsa600.getAdsa600(), pnd_Cntrl_Isn, pnd_Bsnss_Dte, pnd_Orgnl_State, pnd_Err_Cde_X,                  //Natural: CALLNAT 'ADSN601' ADSA600 #CNTRL-ISN #BSNSS-DTE #ORGNL-STATE #ERR-CDE-X #IA-ORGN-CODE
            pnd_Ia_Orgn_Code);
        if (condition(Global.isEscape())) return;
    }
    private void sub_Calculate_Periodic_Ivc() throws Exception                                                                                                            //Natural: CALCULATE-PERIODIC-IVC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Cntrct_Orgn_Cde.resetInitial();                                                                                                                               //Natural: RESET INITIAL #CNTRCT-ORGN-CDE
        //*  EM - 080212
        pnd_Periodic_Ivc.reset();                                                                                                                                         //Natural: RESET #PERIODIC-IVC
        pnd_Dob.setValueEdited(ads_Prtcpnt_Adp_Frst_Annt_Dte_Of_Brth,new ReportEditMask("YYYYMMDD"));                                                                     //Natural: MOVE EDITED ADS-PRTCPNT.ADP-FRST-ANNT-DTE-OF-BRTH ( EM = YYYYMMDD ) TO #DOB
        pnd_Dob2.setValueEdited(ads_Prtcpnt_Adp_Scnd_Annt_Dte_Of_Brth,new ReportEditMask("YYYYMMDD"));                                                                    //Natural: MOVE EDITED ADS-PRTCPNT.ADP-SCND-ANNT-DTE-OF-BRTH ( EM = YYYYMMDD ) TO #DOB2
        pnd_Tiaa_Ivc.setValue(pdaAdsa600.getAdsa600_Adi_Tiaa_Ivc_Amt());                                                                                                  //Natural: ASSIGN #TIAA-IVC := ADSA600.ADI-TIAA-IVC-AMT
        pnd_Cref_Ivc.setValue(pdaAdsa600.getAdsa600_Adi_Cref_Ivc_Amt());                                                                                                  //Natural: ASSIGN #CREF-IVC := ADSA600.ADI-CREF-IVC-AMT
        if (condition(pdaAdsa600.getAdsa600_Adi_Optn_Cde().equals(21)))                                                                                                   //Natural: IF ADSA600.ADI-OPTN-CDE = 21
        {
            pnd_Sim_Rule.setValue("N");                                                                                                                                   //Natural: ASSIGN #SIM-RULE := 'N'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.callnat(Iaan037m.class , getCurrentProcessState(), pnd_Sim_Rule, pnd_Asd_A_Pnd_Asd_N6, pdaAdsa600.getAdsa600_Adi_Optn_Cde(), pnd_Cntrct_Orgn_Cde,     //Natural: CALLNAT 'IAAN037M' #SIM-RULE #ASD-N6 ADSA600.ADI-OPTN-CDE #CNTRCT-ORGN-CDE #DOB-N #DOB2-N #SIM-AGE
                pnd_Dob_Pnd_Dob_N, pnd_Dob2_Pnd_Dob2_N, pnd_Sim_Age);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Sim_Rule.equals("N")))                                                                                                                          //Natural: IF #SIM-RULE = 'N'
        {
            pdaNaza600i.getNaza600i_Check_D().setValue(iaa_Cntrl_Rcrd_1_Cntrl_Check_Dte);                                                                                 //Natural: ASSIGN NAZA600I.CHECK-D := IAA-CNTRL-RCRD-1.CNTRL-CHECK-DTE
            pdaNaza600i.getNaza600i_Cntrct_Optn_Cde().setValue(pdaAdsa600.getAdsa600_Adi_Optn_Cde());                                                                     //Natural: ASSIGN NAZA600I.CNTRCT-OPTN-CDE := ADSA600.ADI-OPTN-CDE
            pdaNaza600i.getNaza600i_Cntrct_Mode_Ind().setValue(pdaAdsa600.getAdsa600_Adi_Pymnt_Mode());                                                                   //Natural: ASSIGN NAZA600I.CNTRCT-MODE-IND := ADSA600.ADI-PYMNT-MODE
            pdaNaza600i.getNaza600i_Cntrct_Issue_Dte().setValue(pnd_Asd_A_Pnd_Asd_N6);                                                                                    //Natural: ASSIGN NAZA600I.CNTRCT-ISSUE-DTE := #ASD-N6
            pdaNaza600i.getNaza600i_Cntrct_First_Pymnt_Due_Dte().setValue(pnd_Asd_A_Pnd_Asd_N6);                                                                          //Natural: ASSIGN NAZA600I.CNTRCT-FIRST-PYMNT-DUE-DTE := #ASD-N6
            pdaNaza600i.getNaza600i_Cntrct_First_Annt_Dob_Dte().setValue(pnd_Dob_Pnd_Dob_N);                                                                              //Natural: ASSIGN NAZA600I.CNTRCT-FIRST-ANNT-DOB-DTE := #DOB-N
            pnd_Wdte8.setValueEdited(ads_Prtcpnt_Adp_Scnd_Annt_Dte_Of_Brth,new ReportEditMask("YYYYMMDD"));                                                               //Natural: MOVE EDITED ADS-PRTCPNT.ADP-SCND-ANNT-DTE-OF-BRTH ( EM = YYYYMMDD ) TO #WDTE8
            if (condition(DbsUtil.is(pnd_Wdte8.getText(),"N8")))                                                                                                          //Natural: IF #WDTE8 IS ( N8 )
            {
                pdaNaza600i.getNaza600i_Cntrct_Scnd_Annt_Dob_Dte().compute(new ComputeParameters(false, pdaNaza600i.getNaza600i_Cntrct_Scnd_Annt_Dob_Dte()),              //Natural: ASSIGN NAZA600I.CNTRCT-SCND-ANNT-DOB-DTE := VAL ( #WDTE8 )
                    pnd_Wdte8.val());
            }                                                                                                                                                             //Natural: END-IF
            pnd_Wdte6.setValueEdited(pdaAdsa600.getAdsa600_Adi_Finl_Periodic_Py_Dte(),new ReportEditMask("YYYYMM"));                                                      //Natural: MOVE EDITED ADSA600.ADI-FINL-PERIODIC-PY-DTE ( EM = YYYYMM ) TO #WDTE6
            if (condition(DbsUtil.is(pnd_Wdte6.getText(),"N6")))                                                                                                          //Natural: IF #WDTE6 IS ( N6 )
            {
                pdaNaza600i.getNaza600i_Cntrct_Final_Per_Pay_Dte().compute(new ComputeParameters(false, pdaNaza600i.getNaza600i_Cntrct_Final_Per_Pay_Dte()),              //Natural: ASSIGN NAZA600I.CNTRCT-FINAL-PER-PAY-DTE := VAL ( #WDTE6 )
                    pnd_Wdte6.val());
            }                                                                                                                                                             //Natural: END-IF
            //*  END-IF                        /* 062012
            if (condition(ads_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind.equals("Y")))                                                                                                   //Natural: IF ADS-IA-RSLT.ADI-TIAA-IVC-GD-IND = 'Y'
            {
                pdaNaza600i.getNaza600i_Cntrct_Ivc_Amt().setValue(pnd_Tiaa_Ivc);                                                                                          //Natural: ASSIGN NAZA600I.CNTRCT-IVC-AMT := #TIAA-IVC
                pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().compute(new ComputeParameters(false, pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt()), DbsField.add(getZero(),        //Natural: ASSIGN NAZA600I.#PER-PAY-AMT := 0 + ADSA600.#ADI-DTL-TIAA-GRD-GRNTD-AMT ( * )
                    pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue("*")));
                pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().nadd(pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue("*"));                                     //Natural: ASSIGN NAZA600I.#PER-PAY-AMT := NAZA600I.#PER-PAY-AMT + ADSA600.#ADI-DTL-TIAA-STD-GRNTD-AMT ( * )
                //*  030912 END
                DbsUtil.examine(new ExamineSource(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue("*"),true), new ExamineSearch("R"), new ExamineGivingIndex(pnd_I)); //Natural: EXAMINE FULL ADSA600.ADI-DTL-CREF-ACCT-CD ( * ) FOR 'R' GIVING INDEX #I
                if (condition(pnd_I.greater(getZero())))                                                                                                                  //Natural: IF #I GT 0
                {
                    pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().nadd(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_I));                                   //Natural: ASSIGN NAZA600I.#PER-PAY-AMT := NAZA600I.#PER-PAY-AMT + ADSA600.ADI-DTL-CREF-DA-MNTHLY-AMT ( #I )
                    pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().nadd(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_I));                                     //Natural: ASSIGN NAZA600I.#PER-PAY-AMT := NAZA600I.#PER-PAY-AMT + ADSA600.ADI-DTL-CREF-DA-ANNL-AMT ( #I )
                }                                                                                                                                                         //Natural: END-IF
                //* * EM - 012109 START
                DbsUtil.examine(new ExamineSource(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue("*"),true), new ExamineSearch("D"), new ExamineGivingIndex(pnd_I)); //Natural: EXAMINE FULL ADSA600.ADI-DTL-CREF-ACCT-CD ( * ) FOR 'D' GIVING INDEX #I
                if (condition(pnd_I.greater(getZero())))                                                                                                                  //Natural: IF #I GT 0
                {
                    pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().nadd(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_I));                                   //Natural: ASSIGN NAZA600I.#PER-PAY-AMT := NAZA600I.#PER-PAY-AMT + ADSA600.ADI-DTL-CREF-DA-MNTHLY-AMT ( #I )
                    pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().nadd(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_I));                                     //Natural: ASSIGN NAZA600I.#PER-PAY-AMT := NAZA600I.#PER-PAY-AMT + ADSA600.ADI-DTL-CREF-DA-ANNL-AMT ( #I )
                    //*  TIAA
                }                                                                                                                                                         //Natural: END-IF
                pdaNaza600i.getNaza600i_Pnd_Final_Pay_Amt().compute(new ComputeParameters(false, pdaNaza600i.getNaza600i_Pnd_Final_Pay_Amt()), DbsField.add(getZero(),    //Natural: ASSIGN NAZA600I.#FINAL-PAY-AMT := 0 + ADSA600.#ADI-DTL-FNL-STD-PAY-AMT ( * )
                    pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue("*")));
                pdaNaza600i.getNaza600i_Pnd_Final_Pay_Amt().nadd(pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue("*"));                                      //Natural: ASSIGN NAZA600I.#FINAL-PAY-AMT := NAZA600I.#FINAL-PAY-AMT + ADSA600.#ADI-DTL-FNL-GRD-PAY-AMT ( * )
                pdaNaza600i.getNaza600i_Cntrct_Company_Cd().setValue("1");                                                                                                //Natural: ASSIGN NAZA600I.CNTRCT-COMPANY-CD := '1'
                DbsUtil.callnat(Nazn637e.class , getCurrentProcessState(), pdaNaza600i.getNaza600i());                                                                    //Natural: CALLNAT 'NAZN637E' NAZA600I
                if (condition(Global.isEscape())) return;
                if (condition(pdaNaza600i.getNaza600i_Return_Code().equals("B")))                                                                                         //Natural: IF NAZA600I.RETURN-CODE = 'B'
                {
                    ads_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind.setValue("N");                                                                                                        //Natural: ASSIGN ADS-IA-RSLT.ADI-TIAA-IVC-GD-IND := 'N'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ads_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt.setValue(pdaNaza600i.getNaza600i_Cntrct_Per_Ivc_Amt());                                                             //Natural: ASSIGN ADS-IA-RSLT.ADI-TIAA-PRDC-IVC-AMT := #PRDC-IVC := NAZA600I.CNTRCT-PER-IVC-AMT
                    pnd_Prdc_Ivc.setValue(pdaNaza600i.getNaza600i_Cntrct_Per_Ivc_Amt());
                    //*  EM - 08012 - START
                    if (condition(ads_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt.greater(getZero())))                                                                                  //Natural: IF ADS-IA-RSLT.ADI-TIAA-PRDC-IVC-AMT > 0
                    {
                        pnd_Periodic_Ivc.setValue(true);                                                                                                                  //Natural: ASSIGN #PERIODIC-IVC := TRUE
                        //*  EM - 080212 - END
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*    END-IF /* 062012 END
            }                                                                                                                                                             //Natural: END-IF
            //*  CREF
            if (condition(ads_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind.equals("Y")))                                                                                                   //Natural: IF ADS-IA-RSLT.ADI-CREF-IVC-GD-IND = 'Y'
            {
                pdaNaza600i.getNaza600i_Cntrct_Company_Cd().setValue("2");                                                                                                //Natural: ASSIGN NAZA600I.CNTRCT-COMPANY-CD := '2'
                pdaNaza600i.getNaza600i_Cntrct_Ivc_Amt().setValue(pnd_Cref_Ivc);                                                                                          //Natural: ASSIGN NAZA600I.CNTRCT-IVC-AMT := #CREF-IVC
                pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().reset();                                                                                                        //Natural: RESET NAZA600I.#PER-PAY-AMT NAZA600I.#FINAL-PAY-AMT
                pdaNaza600i.getNaza600i_Pnd_Final_Pay_Amt().reset();
                FOR03:                                                                                                                                                    //Natural: FOR #I 1 TO 20
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
                {
                    //*  EM - 012109
                    if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_I).notEquals(" ") && pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_I).notEquals("R")  //Natural: IF ADSA600.ADI-DTL-CREF-ACCT-CD ( #I ) NE ' ' AND ADSA600.ADI-DTL-CREF-ACCT-CD ( #I ) NE 'R' AND ADSA600.ADI-DTL-CREF-ACCT-CD ( #I ) NE 'D'
                        && pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_I).notEquals("D")))
                    {
                        pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().nadd(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_I));                               //Natural: ASSIGN NAZA600I.#PER-PAY-AMT := NAZA600I.#PER-PAY-AMT + ADSA600.ADI-DTL-CREF-DA-MNTHLY-AMT ( #I )
                        pdaNaza600i.getNaza600i_Pnd_Per_Pay_Amt().nadd(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_I));                                 //Natural: ASSIGN NAZA600I.#PER-PAY-AMT := NAZA600I.#PER-PAY-AMT + ADSA600.ADI-DTL-CREF-DA-ANNL-AMT ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                DbsUtil.callnat(Nazn637e.class , getCurrentProcessState(), pdaNaza600i.getNaza600i());                                                                    //Natural: CALLNAT 'NAZN637E' NAZA600I
                if (condition(Global.isEscape())) return;
                if (condition(pdaNaza600i.getNaza600i_Return_Code().equals("B")))                                                                                         //Natural: IF NAZA600I.RETURN-CODE = 'B'
                {
                    ads_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind.setValue("N");                                                                                                        //Natural: ASSIGN ADS-IA-RSLT.ADI-CREF-IVC-GD-IND := 'N'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ads_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt.setValue(pdaNaza600i.getNaza600i_Cntrct_Per_Ivc_Amt());                                                             //Natural: ASSIGN ADS-IA-RSLT.ADI-CREF-PRDC-IVC-AMT := #PRDC-IVC := NAZA600I.CNTRCT-PER-IVC-AMT
                    pnd_Prdc_Ivc.setValue(pdaNaza600i.getNaza600i_Cntrct_Per_Ivc_Amt());
                    //*  EM - 08012 - START
                    if (condition(ads_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt.greater(getZero())))                                                                                  //Natural: IF ADS-IA-RSLT.ADI-CREF-PRDC-IVC-AMT > 0
                    {
                        pnd_Periodic_Ivc.setValue(true);                                                                                                                  //Natural: ASSIGN #PERIODIC-IVC := TRUE
                        //*  EM - 080212 - END
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  END-IF 062012
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Tiaa_Ivc.equals(getZero()) && pnd_Cref_Ivc.equals(getZero())))                                                                                  //Natural: IF #TIAA-IVC = 0 AND #CREF-IVC = 0
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  DG 080207
        if (condition((pnd_Asd_A_Pnd_Asd_N6.greater(199712) && (((((pdaAdsa600.getAdsa600_Adi_Optn_Cde().equals(3) || pdaAdsa600.getAdsa600_Adi_Optn_Cde().equals(4))     //Natural: IF #ASD-N6 GT 199712 AND ( ADSA600.ADI-OPTN-CDE = 03 OR = 04 OR = 07 OR = 08 OR ADSA600.ADI-OPTN-CDE = 10 THRU 17 OR ADSA600.ADI-OPTN-CDE = 54 THRU 57 )
            || pdaAdsa600.getAdsa600_Adi_Optn_Cde().equals(7)) || pdaAdsa600.getAdsa600_Adi_Optn_Cde().equals(8)) || (pdaAdsa600.getAdsa600_Adi_Optn_Cde().greaterOrEqual(10) 
            && pdaAdsa600.getAdsa600_Adi_Optn_Cde().lessOrEqual(17))) || (pdaAdsa600.getAdsa600_Adi_Optn_Cde().greaterOrEqual(54) && pdaAdsa600.getAdsa600_Adi_Optn_Cde().lessOrEqual(57))))))
        {
            short decideConditionsMet1884 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #SIM-AGE GT 140
            if (condition(pnd_Sim_Age.greater(140)))
            {
                decideConditionsMet1884++;
                pnd_No_Pays.setValue(210);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 210
            }                                                                                                                                                             //Natural: WHEN #SIM-AGE GT 130
            else if (condition(pnd_Sim_Age.greater(130)))
            {
                decideConditionsMet1884++;
                pnd_No_Pays.setValue(260);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 260
            }                                                                                                                                                             //Natural: WHEN #SIM-AGE GT 120
            else if (condition(pnd_Sim_Age.greater(120)))
            {
                decideConditionsMet1884++;
                pnd_No_Pays.setValue(310);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 310
            }                                                                                                                                                             //Natural: WHEN #SIM-AGE GT 110
            else if (condition(pnd_Sim_Age.greater(110)))
            {
                decideConditionsMet1884++;
                pnd_No_Pays.setValue(360);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 360
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_No_Pays.setValue(410);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 410
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet1899 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #SIM-AGE GT 70
            if (condition(pnd_Sim_Age.greater(70)))
            {
                decideConditionsMet1899++;
                pnd_No_Pays.setValue(160);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 160
            }                                                                                                                                                             //Natural: WHEN #SIM-AGE GT 65
            else if (condition(pnd_Sim_Age.greater(65)))
            {
                decideConditionsMet1899++;
                pnd_No_Pays.setValue(210);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 210
            }                                                                                                                                                             //Natural: WHEN #SIM-AGE GT 60
            else if (condition(pnd_Sim_Age.greater(60)))
            {
                decideConditionsMet1899++;
                pnd_No_Pays.setValue(260);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 260
            }                                                                                                                                                             //Natural: WHEN #SIM-AGE GT 55
            else if (condition(pnd_Sim_Age.greater(55)))
            {
                decideConditionsMet1899++;
                pnd_No_Pays.setValue(310);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 310
            }                                                                                                                                                             //Natural: WHEN #SIM-AGE LE 55
            else if (condition(pnd_Sim_Age.lessOrEqual(55)))
            {
                decideConditionsMet1899++;
                pnd_No_Pays.setValue(360);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 360
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_No_Pays.setValue(360);                                                                                                                                //Natural: ASSIGN #NO-PAYS := 360
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ads_Ia_Rslt_Adi_Tiaa_Ivc_Amt.greater(getZero()) && ads_Ia_Rslt_Adi_Tiaa_Ivc_Gd_Ind.equals("Y")))                                                    //Natural: IF ADS-IA-RSLT.ADI-TIAA-IVC-AMT GT 0 AND ADS-IA-RSLT.ADI-TIAA-IVC-GD-IND = 'Y'
        {
            ads_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt.compute(new ComputeParameters(true, ads_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt), (pnd_Tiaa_Ivc.multiply(12)).divide((pnd_No_Pays.multiply(pnd_No_Per_Yr)))); //Natural: COMPUTE ROUNDED ADS-IA-RSLT.ADI-TIAA-PRDC-IVC-AMT = ( #TIAA-IVC * 12 ) / ( #NO-PAYS * #NO-PER-YR )
            pnd_Prdc_Ivc.setValue(ads_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt);                                                                                                     //Natural: ASSIGN #PRDC-IVC := ADS-IA-RSLT.ADI-TIAA-PRDC-IVC-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ads_Ia_Rslt_Adi_Cref_Ivc_Amt.greater(getZero()) && ads_Ia_Rslt_Adi_Cref_Ivc_Gd_Ind.equals("Y")))                                                    //Natural: IF ADS-IA-RSLT.ADI-CREF-IVC-AMT GT 0 AND ADS-IA-RSLT.ADI-CREF-IVC-GD-IND = 'Y'
        {
            ads_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt.compute(new ComputeParameters(true, ads_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt), (pnd_Cref_Ivc.multiply(12)).divide((pnd_No_Pays.multiply(pnd_No_Per_Yr)))); //Natural: COMPUTE ROUNDED ADS-IA-RSLT.ADI-CREF-PRDC-IVC-AMT = ( #CREF-IVC * 12 ) / ( #NO-PAYS * #NO-PER-YR )
            pnd_Prdc_Ivc.setValue(ads_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt);                                                                                                     //Natural: ASSIGN #PRDC-IVC := ADS-IA-RSLT.ADI-CREF-PRDC-IVC-AMT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Set_Revalue_Flags() throws Exception                                                                                                                 //Natural: SET-REVALUE-FLAGS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Revalue_Tiaa.reset();                                                                                                                                         //Natural: RESET #REVALUE-TIAA #REVALUE-CREF #CREF-OK #TIAA-OK
        pnd_Revalue_Cref.reset();
        pnd_Cref_Ok.reset();
        pnd_Tiaa_Ok.reset();
        if (condition(pnd_Factor_Mm.equals(1)))                                                                                                                           //Natural: IF #FACTOR-MM = 1
        {
            if (condition(ads_Ia_Rslt_Adi_Optn_Cde.equals(21)))                                                                                                           //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 21
            {
                pnd_Callnat.setValue("AIAN025");                                                                                                                          //Natural: ASSIGN #CALLNAT := 'AIAN025'
                if (condition(pnd_Next_Dte_A_Pnd_Next_Dte_Mm.equals(4)))                                                                                                  //Natural: IF #NEXT-DTE-MM = 04
                {
                    pnd_Revalue_Tiaa.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-TIAA := TRUE
                    pnd_T_Reval_Dte_A.setValue(pnd_Next_Dte_A);                                                                                                           //Natural: ASSIGN #T-REVAL-DTE-A := #NEXT-DTE-A
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Callnat.setValue("AIAN083");                                                                                                                          //Natural: ASSIGN #CALLNAT := 'AIAN083'
                if (condition(pnd_Next_Dte_A_Pnd_Next_Dte_Mm.equals(1)))                                                                                                  //Natural: IF #NEXT-DTE-MM = 01
                {
                    pnd_Revalue_Tiaa.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-TIAA := TRUE
                    pnd_T_Reval_Dte_A.setValue(pnd_Next_Dte_A);                                                                                                           //Natural: ASSIGN #T-REVAL-DTE-A := #NEXT-DTE-A
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Next_Dte_A_Pnd_Next_Dte_Mm.equals(5)))                                                                                                      //Natural: IF #NEXT-DTE-MM = 05
            {
                pnd_Revalue_Cref.setValue(true);                                                                                                                          //Natural: ASSIGN #REVALUE-CREF := TRUE
                pnd_C_Reval_Dte_A.setValue(pnd_Next_Dte_A);                                                                                                               //Natural: ASSIGN #C-REVAL-DTE-A := #NEXT-DTE-A
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_C_Reval_Dte_A.setValue(pnd_Next_Dte_A);                                                                                                                   //Natural: ASSIGN #C-REVAL-DTE-A := #NEXT-DTE-A
            pnd_C_Reval_Dte_A_Pnd_C_Reval_Mm.setValue(5);                                                                                                                 //Natural: ASSIGN #C-REVAL-MM := 05
            if (condition(pnd_Last_Inst_Dte_A_Pnd_Last_Inst_Mm.less(5)))                                                                                                  //Natural: IF #LAST-INST-MM LT 5
            {
                pnd_C_Reval_Dte_A_Pnd_C_Reval_Yyyy.setValue(pnd_Last_Inst_Dte_A_Pnd_Last_Inst_Yyyy);                                                                      //Natural: ASSIGN #C-REVAL-YYYY := #LAST-INST-YYYY
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_C_Reval_Dte_A_Pnd_C_Reval_Yyyy.compute(new ComputeParameters(false, pnd_C_Reval_Dte_A_Pnd_C_Reval_Yyyy), pnd_Last_Inst_Dte_A_Pnd_Last_Inst_Yyyy.add(1)); //Natural: ASSIGN #C-REVAL-YYYY := #LAST-INST-YYYY + 1
            }                                                                                                                                                             //Natural: END-IF
            pnd_T_Reval_Dte_A_Pnd_T_Reval_Dd.setValue("01");                                                                                                              //Natural: ASSIGN #T-REVAL-DD := '01'
            if (condition(ads_Ia_Rslt_Adi_Optn_Cde.equals(21)))                                                                                                           //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 21
            {
                pnd_Callnat.setValue("AIAN025");                                                                                                                          //Natural: ASSIGN #CALLNAT := 'AIAN025'
                pnd_T_Reval_Dte_A_Pnd_T_Reval_Mm.setValue(4);                                                                                                             //Natural: ASSIGN #T-REVAL-MM := 04
                if (condition(pnd_Last_Inst_Dte_A_Pnd_Last_Inst_Mm.less(4)))                                                                                              //Natural: IF #LAST-INST-MM LT 04
                {
                    pnd_T_Reval_Dte_A_Pnd_T_Reval_Yyyy.setValue(pnd_Last_Inst_Dte_A_Pnd_Last_Inst_Yyyy);                                                                  //Natural: ASSIGN #T-REVAL-YYYY := #LAST-INST-YYYY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_T_Reval_Dte_A_Pnd_T_Reval_Yyyy.compute(new ComputeParameters(false, pnd_T_Reval_Dte_A_Pnd_T_Reval_Yyyy), pnd_Last_Inst_Dte_A_Pnd_Last_Inst_Yyyy.add(1)); //Natural: ASSIGN #T-REVAL-YYYY := #LAST-INST-YYYY + 1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Callnat.setValue("AIAN083");                                                                                                                          //Natural: ASSIGN #CALLNAT := 'AIAN083'
                pnd_T_Reval_Dte_A_Pnd_T_Reval_Yyyy.compute(new ComputeParameters(false, pnd_T_Reval_Dte_A_Pnd_T_Reval_Yyyy), pnd_Last_Inst_Dte_A_Pnd_Last_Inst_Yyyy.add(1)); //Natural: ASSIGN #T-REVAL-YYYY := #LAST-INST-YYYY + 1
                pnd_T_Reval_Dte_A_Pnd_T_Reval_Mm.setValue(1);                                                                                                             //Natural: ASSIGN #T-REVAL-MM := 01
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Next_Dte_A.greaterOrEqual(pnd_T_Reval_Dte_A) && pdaAdsa600.getAdsa600_Pnd_Ia_Check_Dte().greater(pnd_T_Reval_Dte_A)))                       //Natural: IF #NEXT-DTE-A GE #T-REVAL-DTE-A AND #IA-CHECK-DTE GT #T-REVAL-DTE-A
            {
                pnd_Tiaa_Ok.setValue(true);                                                                                                                               //Natural: ASSIGN #TIAA-OK := TRUE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Next_Dte_A.greaterOrEqual(pnd_C_Reval_Dte_A) && pdaAdsa600.getAdsa600_Pnd_Ia_Check_Dte().greater(pnd_C_Reval_Dte_A)))                       //Natural: IF #NEXT-DTE-A GE #C-REVAL-DTE-A AND #IA-CHECK-DTE GT #C-REVAL-DTE-A
            {
                pnd_Cref_Ok.setValue(true);                                                                                                                               //Natural: ASSIGN #CREF-OK := TRUE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cref_Ok.getBoolean() || pnd_Tiaa_Ok.getBoolean()))                                                                                          //Natural: IF #CREF-OK OR #TIAA-OK
            {
                                                                                                                                                                          //Natural: PERFORM SET-REVALUE-FLAGS-FOR-ODD-MODES
                sub_Set_Revalue_Flags_For_Odd_Modes();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Set_Revalue_Flags_For_Odd_Modes() throws Exception                                                                                                   //Natural: SET-REVALUE-FLAGS-FOR-ODD-MODES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet1987 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #NEXT-DTE-MM;//Natural: VALUE 01
        if (condition((pnd_Next_Dte_A_Pnd_Next_Dte_Mm.equals(1))))
        {
            decideConditionsMet1987++;
            if (condition(ads_Ia_Rslt_Adi_Pymnt_Mode.equals(801)))                                                                                                        //Natural: IF ADS-IA-RSLT.ADI-PYMNT-MODE = 801
            {
                if (condition(pnd_Cref_Ok.getBoolean()))                                                                                                                  //Natural: IF #CREF-OK
                {
                    pnd_Revalue_Cref.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-CREF := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Tiaa_Ok.getBoolean()))                                                                                                                  //Natural: IF #TIAA-OK
                {
                    pnd_Revalue_Tiaa.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-TIAA := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition((ads_Ia_Rslt_Adi_Optn_Cde.notEquals(21) && (ads_Ia_Rslt_Adi_Pymnt_Mode.equals(601) || ads_Ia_Rslt_Adi_Pymnt_Mode.equals(701)))))                //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE NE 21 AND ( ADS-IA-RSLT.ADI-PYMNT-MODE = 601 OR = 701 )
            {
                if (condition(pnd_Tiaa_Ok.getBoolean()))                                                                                                                  //Natural: IF #TIAA-OK
                {
                    pnd_Revalue_Tiaa.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-TIAA := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 02
        else if (condition((pnd_Next_Dte_A_Pnd_Next_Dte_Mm.equals(2))))
        {
            decideConditionsMet1987++;
            if (condition(ads_Ia_Rslt_Adi_Pymnt_Mode.equals(802)))                                                                                                        //Natural: IF ADS-IA-RSLT.ADI-PYMNT-MODE = 802
            {
                if (condition(pnd_Cref_Ok.getBoolean()))                                                                                                                  //Natural: IF #CREF-OK
                {
                    pnd_Revalue_Cref.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-CREF := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Tiaa_Ok.getBoolean()))                                                                                                                  //Natural: IF #TIAA-OK
                {
                    pnd_Revalue_Tiaa.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-TIAA := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition((ads_Ia_Rslt_Adi_Optn_Cde.notEquals(21) && (ads_Ia_Rslt_Adi_Pymnt_Mode.equals(602) || ads_Ia_Rslt_Adi_Pymnt_Mode.equals(702)))))                //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE NE 21 AND ( ADS-IA-RSLT.ADI-PYMNT-MODE = 602 OR = 702 )
            {
                if (condition(pnd_Tiaa_Ok.getBoolean()))                                                                                                                  //Natural: IF #TIAA-OK
                {
                    pnd_Revalue_Tiaa.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-TIAA := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 03
        else if (condition((pnd_Next_Dte_A_Pnd_Next_Dte_Mm.equals(3))))
        {
            decideConditionsMet1987++;
            if (condition(ads_Ia_Rslt_Adi_Pymnt_Mode.equals(803)))                                                                                                        //Natural: IF ADS-IA-RSLT.ADI-PYMNT-MODE = 803
            {
                if (condition(pnd_Cref_Ok.getBoolean()))                                                                                                                  //Natural: IF #CREF-OK
                {
                    pnd_Revalue_Cref.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-CREF := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Tiaa_Ok.getBoolean()))                                                                                                                  //Natural: IF #TIAA-OK
                {
                    pnd_Revalue_Tiaa.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-TIAA := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition((ads_Ia_Rslt_Adi_Optn_Cde.notEquals(21) && (ads_Ia_Rslt_Adi_Pymnt_Mode.equals(603) || ads_Ia_Rslt_Adi_Pymnt_Mode.equals(703)))))                //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE NE 21 AND ( ADS-IA-RSLT.ADI-PYMNT-MODE = 603 OR = 703 )
            {
                if (condition(pnd_Tiaa_Ok.getBoolean()))                                                                                                                  //Natural: IF #TIAA-OK
                {
                    pnd_Revalue_Tiaa.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-TIAA := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 04
        else if (condition((pnd_Next_Dte_A_Pnd_Next_Dte_Mm.equals(4))))
        {
            decideConditionsMet1987++;
            if (condition(ads_Ia_Rslt_Adi_Pymnt_Mode.equals(704) || ads_Ia_Rslt_Adi_Pymnt_Mode.equals(804)))                                                              //Natural: IF ADS-IA-RSLT.ADI-PYMNT-MODE = 704 OR = 804
            {
                if (condition(pnd_Tiaa_Ok.getBoolean()))                                                                                                                  //Natural: IF #TIAA-OK
                {
                    pnd_Revalue_Tiaa.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-TIAA := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ads_Ia_Rslt_Adi_Pymnt_Mode.equals(804)))                                                                                                    //Natural: IF ADS-IA-RSLT.ADI-PYMNT-MODE = 804
                {
                    if (condition(pnd_Cref_Ok.getBoolean()))                                                                                                              //Natural: IF #CREF-OK
                    {
                        pnd_Revalue_Cref.setValue(true);                                                                                                                  //Natural: ASSIGN #REVALUE-CREF := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition((ads_Ia_Rslt_Adi_Optn_Cde.equals(21)) && ads_Ia_Rslt_Adi_Pymnt_Mode.equals(601)))                                                               //Natural: IF ( ADS-IA-RSLT.ADI-OPTN-CDE = 21 ) AND ADS-IA-RSLT.ADI-PYMNT-MODE = 601
            {
                if (condition(pnd_Tiaa_Ok.getBoolean()))                                                                                                                  //Natural: IF #TIAA-OK
                {
                    pnd_Revalue_Tiaa.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-TIAA := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 05
        else if (condition((pnd_Next_Dte_A_Pnd_Next_Dte_Mm.equals(5))))
        {
            decideConditionsMet1987++;
            if (condition(ads_Ia_Rslt_Adi_Pymnt_Mode.equals(705) || ads_Ia_Rslt_Adi_Pymnt_Mode.equals(805)))                                                              //Natural: IF ADS-IA-RSLT.ADI-PYMNT-MODE = 705 OR = 805
            {
                if (condition(pnd_Cref_Ok.getBoolean()))                                                                                                                  //Natural: IF #CREF-OK
                {
                    pnd_Revalue_Cref.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-CREF := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Tiaa_Ok.getBoolean()))                                                                                                                  //Natural: IF #TIAA-OK
                {
                    pnd_Revalue_Tiaa.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-TIAA := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ads_Ia_Rslt_Adi_Pymnt_Mode.equals(602)))                                                                                                        //Natural: IF ADS-IA-RSLT.ADI-PYMNT-MODE = 602
            {
                pnd_Revalue_Cref.setValue(true);                                                                                                                          //Natural: ASSIGN #REVALUE-CREF := TRUE
                if (condition(ads_Ia_Rslt_Adi_Optn_Cde.equals(21)))                                                                                                       //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 21
                {
                    if (condition(pnd_Tiaa_Ok.getBoolean()))                                                                                                              //Natural: IF #TIAA-OK
                    {
                        pnd_Revalue_Tiaa.setValue(true);                                                                                                                  //Natural: ASSIGN #REVALUE-TIAA := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 06
        else if (condition((pnd_Next_Dte_A_Pnd_Next_Dte_Mm.equals(6))))
        {
            decideConditionsMet1987++;
            if (condition(ads_Ia_Rslt_Adi_Pymnt_Mode.equals(706) || ads_Ia_Rslt_Adi_Pymnt_Mode.equals(806)))                                                              //Natural: IF ADS-IA-RSLT.ADI-PYMNT-MODE = 706 OR = 806
            {
                if (condition(pnd_Cref_Ok.getBoolean()))                                                                                                                  //Natural: IF #CREF-OK
                {
                    pnd_Revalue_Cref.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-CREF := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Tiaa_Ok.getBoolean()))                                                                                                                  //Natural: IF #TIAA-OK
                {
                    pnd_Revalue_Tiaa.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-TIAA := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ads_Ia_Rslt_Adi_Pymnt_Mode.equals(603)))                                                                                                        //Natural: IF ADS-IA-RSLT.ADI-PYMNT-MODE = 603
            {
                if (condition(pnd_Cref_Ok.getBoolean()))                                                                                                                  //Natural: IF #CREF-OK
                {
                    pnd_Revalue_Cref.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-CREF := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ads_Ia_Rslt_Adi_Optn_Cde.equals(21)))                                                                                                       //Natural: IF ADS-IA-RSLT.ADI-OPTN-CDE = 21
                {
                    if (condition(pnd_Tiaa_Ok.getBoolean()))                                                                                                              //Natural: IF #TIAA-OK
                    {
                        pnd_Revalue_Tiaa.setValue(true);                                                                                                                  //Natural: ASSIGN #REVALUE-TIAA := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 07
        else if (condition((pnd_Next_Dte_A_Pnd_Next_Dte_Mm.equals(7))))
        {
            decideConditionsMet1987++;
            if (condition(ads_Ia_Rslt_Adi_Pymnt_Mode.equals(601) || ads_Ia_Rslt_Adi_Pymnt_Mode.equals(701) || ads_Ia_Rslt_Adi_Pymnt_Mode.equals(807)))                    //Natural: IF ADS-IA-RSLT.ADI-PYMNT-MODE = 601 OR = 701 OR = 807
            {
                if (condition(pnd_Cref_Ok.getBoolean()))                                                                                                                  //Natural: IF #CREF-OK
                {
                    pnd_Revalue_Cref.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-CREF := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ads_Ia_Rslt_Adi_Pymnt_Mode.equals(807) || (ads_Ia_Rslt_Adi_Pymnt_Mode.equals(701) && ads_Ia_Rslt_Adi_Optn_Cde.equals(21))))                 //Natural: IF ADS-IA-RSLT.ADI-PYMNT-MODE = 807 OR ( ADS-IA-RSLT.ADI-PYMNT-MODE = 701 AND ADS-IA-RSLT.ADI-OPTN-CDE = 21 )
                {
                    if (condition(pnd_Tiaa_Ok.getBoolean()))                                                                                                              //Natural: IF #TIAA-OK
                    {
                        pnd_Revalue_Tiaa.setValue(true);                                                                                                                  //Natural: ASSIGN #REVALUE-TIAA := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 08
        else if (condition((pnd_Next_Dte_A_Pnd_Next_Dte_Mm.equals(8))))
        {
            decideConditionsMet1987++;
            if (condition(ads_Ia_Rslt_Adi_Pymnt_Mode.equals(702) || ads_Ia_Rslt_Adi_Pymnt_Mode.equals(808)))                                                              //Natural: IF ADS-IA-RSLT.ADI-PYMNT-MODE = 702 OR = 808
            {
                if (condition(pnd_Cref_Ok.getBoolean()))                                                                                                                  //Natural: IF #CREF-OK
                {
                    pnd_Revalue_Cref.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-CREF := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ads_Ia_Rslt_Adi_Pymnt_Mode.equals(808) || ads_Ia_Rslt_Adi_Optn_Cde.equals(21)))                                                             //Natural: IF ADS-IA-RSLT.ADI-PYMNT-MODE = 808 OR ADS-IA-RSLT.ADI-OPTN-CDE = 21
                {
                    if (condition(pnd_Tiaa_Ok.getBoolean()))                                                                                                              //Natural: IF #TIAA-OK
                    {
                        pnd_Revalue_Tiaa.setValue(true);                                                                                                                  //Natural: ASSIGN #REVALUE-TIAA := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 09
        else if (condition((pnd_Next_Dte_A_Pnd_Next_Dte_Mm.equals(9))))
        {
            decideConditionsMet1987++;
            if (condition(ads_Ia_Rslt_Adi_Pymnt_Mode.equals(703) || ads_Ia_Rslt_Adi_Pymnt_Mode.equals(809)))                                                              //Natural: IF ADS-IA-RSLT.ADI-PYMNT-MODE = 703 OR = 809
            {
                if (condition(pnd_Cref_Ok.getBoolean()))                                                                                                                  //Natural: IF #CREF-OK
                {
                    pnd_Revalue_Cref.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-CREF := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ads_Ia_Rslt_Adi_Pymnt_Mode.equals(809) || ads_Ia_Rslt_Adi_Optn_Cde.equals(21)))                                                             //Natural: IF ADS-IA-RSLT.ADI-PYMNT-MODE = 809 OR ADS-IA-RSLT.ADI-OPTN-CDE = 21
                {
                    if (condition(pnd_Tiaa_Ok.getBoolean()))                                                                                                              //Natural: IF #TIAA-OK
                    {
                        pnd_Revalue_Tiaa.setValue(true);                                                                                                                  //Natural: ASSIGN #REVALUE-TIAA := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 10
        else if (condition((pnd_Next_Dte_A_Pnd_Next_Dte_Mm.equals(10))))
        {
            decideConditionsMet1987++;
            if (condition(ads_Ia_Rslt_Adi_Pymnt_Mode.equals(704) || ads_Ia_Rslt_Adi_Pymnt_Mode.equals(810)))                                                              //Natural: IF ADS-IA-RSLT.ADI-PYMNT-MODE = 704 OR = 810
            {
                if (condition(pnd_Cref_Ok.getBoolean()))                                                                                                                  //Natural: IF #CREF-OK
                {
                    pnd_Revalue_Cref.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-CREF := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ads_Ia_Rslt_Adi_Pymnt_Mode.equals(810)))                                                                                                    //Natural: IF ADS-IA-RSLT.ADI-PYMNT-MODE = 810
                {
                    if (condition(pnd_Tiaa_Ok.getBoolean()))                                                                                                              //Natural: IF #TIAA-OK
                    {
                        pnd_Revalue_Tiaa.setValue(true);                                                                                                                  //Natural: ASSIGN #REVALUE-TIAA := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 11
        else if (condition((pnd_Next_Dte_A_Pnd_Next_Dte_Mm.equals(11))))
        {
            decideConditionsMet1987++;
            if (condition(ads_Ia_Rslt_Adi_Pymnt_Mode.equals(811)))                                                                                                        //Natural: IF ADS-IA-RSLT.ADI-PYMNT-MODE = 811
            {
                if (condition(pnd_Cref_Ok.getBoolean()))                                                                                                                  //Natural: IF #CREF-OK
                {
                    pnd_Revalue_Cref.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-CREF := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Tiaa_Ok.getBoolean()))                                                                                                                  //Natural: IF #TIAA-OK
                {
                    pnd_Revalue_Tiaa.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-TIAA := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 12
        else if (condition((pnd_Next_Dte_A_Pnd_Next_Dte_Mm.equals(12))))
        {
            decideConditionsMet1987++;
            if (condition(ads_Ia_Rslt_Adi_Pymnt_Mode.equals(812)))                                                                                                        //Natural: IF ADS-IA-RSLT.ADI-PYMNT-MODE = 812
            {
                if (condition(pnd_Cref_Ok.getBoolean()))                                                                                                                  //Natural: IF #CREF-OK
                {
                    pnd_Revalue_Cref.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-CREF := TRUE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Tiaa_Ok.getBoolean()))                                                                                                                  //Natural: IF #TIAA-OK
                {
                    pnd_Revalue_Tiaa.setValue(true);                                                                                                                      //Natural: ASSIGN #REVALUE-TIAA := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    //*  EM - 052711 START
    private void sub_Revalue_Cref() throws Exception                                                                                                                      //Natural: REVALUE-CREF
    {
        if (BLNatReinput.isReinput()) return;

        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Call_Type().setValue("P");                                                                                          //Natural: ASSIGN #AIAN026-CALL-TYPE := 'P'
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Revaluation_Method().setValue("A");                                                                                 //Natural: ASSIGN #AIAN026-REVALUATION-METHOD := 'A'
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Iauv_Request_Date().compute(new ComputeParameters(false, pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Iauv_Request_Date()),  //Natural: ASSIGN #AIAN026-IAUV-REQUEST-DATE := VAL ( #C-REVAL-DTE-A )
            pnd_C_Reval_Dte_A.val());
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Participation_Date().setValue(pnd_Asd_A_Pnd_Asd_N);                                                                 //Natural: ASSIGN #AIAN026-PARTICIPATION-DATE := #ASD-N
        //*  030107 START
        //*  USE LATEST FACTOR
        if (condition(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Iauv_Request_Date().greater(pnd_Factor_Dte)))                                                         //Natural: IF #AIAN026-IAUV-REQUEST-DATE GT #FACTOR-DTE
        {
            pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Call_Type().setValue("L");                                                                                      //Natural: ASSIGN #AIAN026-CALL-TYPE := 'L'
            //*  EM - 052711 END
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Call_Type().setValue("P");                                                                                      //Natural: ASSIGN #AIAN026-CALL-TYPE := 'P'
            //*  030107 END
        }                                                                                                                                                                 //Natural: END-IF
        FOR04:                                                                                                                                                            //Natural: FOR #J 1 TO 20
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(20)); pnd_J.nadd(1))
        {
            //*  EM - 072312 START
            if (condition(ads_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd.getValue(pnd_J).notEquals(" ")))                                                                               //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #J ) NE ' '
            {
                pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Ia_Fund_Code().setValue(ads_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd.getValue(pnd_J));                                  //Natural: ASSIGN #AIAN026-IA-FUND-CODE := ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #J )
                DbsUtil.callnat(Aian026.class , getCurrentProcessState(), pdaAiaa026.getPnd_Aian026_Linkage());                                                           //Natural: CALLNAT 'AIAN026' #AIAN026-LINKAGE
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                //*    IF #AIAN026-RETURN-CODE = 0                  /* EM - 052711
                //*  030912
                if (condition(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code_Nbr().equals(getZero())))                                                         //Natural: IF #AIAN026-RETURN-CODE-NBR = 0
                {
                    pnd_C.setValue(true);                                                                                                                                 //Natural: ASSIGN #C := TRUE
                    pnd_Cntrct_Py_Dte_Key_Cntrct_Ppcn_Nbr.setValue(ads_Ia_Rslt_Adi_Ia_Cref_Nbr);                                                                          //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.CNTRCT-PPCN-NBR := ADS-IA-RSLT.ADI-IA-CREF-NBR
                    pnd_Cntrct_Py_Dte_Key_Cntrct_Payee_Cde.setValue(1);                                                                                                   //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.CNTRCT-PAYEE-CDE := 01
                    pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde.reset();                                                                                                         //Natural: RESET #CNTRCT-PY-DTE-KEY.CMPNY-FUND-CDE
                    //*  EM - 052711
                    DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Ia_Fund_Code(), pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Cde,  //Natural: CALLNAT 'IAAN0511' #AIAN026-IA-FUND-CODE #FUND-CDE #RTN-CDE
                        pnd_Rtn_Cde);
                    if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                    //*  EM - 072312 START
                    if (condition(pnd_Rtn_Cde.equals(getZero())))                                                                                                         //Natural: IF #RTN-CDE = 0
                    {
                        if (condition(ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units.getValue(pnd_J).greater(getZero())))                                                        //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #J ) > 0
                        {
                            if (condition(ads_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd.getValue(pnd_J).equals("R") || ads_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd.getValue(pnd_J).equals("D")))  //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #J ) EQ 'R' OR EQ 'D'
                            {
                                pnd_Cntrct_Py_Dte_Key_Cntrct_Ppcn_Nbr.setValue(ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr);                                                              //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.CNTRCT-PPCN-NBR := ADS-IA-RSLT.ADI-IA-TIAA-NBR
                                setValueToSubstring("U",pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde,1,1);                                                                        //Natural: MOVE 'U' TO SUBSTR ( #CNTRCT-PY-DTE-KEY.CMPNY-FUND-CDE,1,1 )
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                setValueToSubstring("2",pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde,1,1);                                                                        //Natural: MOVE '2' TO SUBSTR ( #CNTRCT-PY-DTE-KEY.CMPNY-FUND-CDE,1,1 )
                            }                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-HISTORICAL-RATES
                            sub_Create_Historical_Rates();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_C.reset();                                                                                                                                //Natural: RESET #C
                        }                                                                                                                                                 //Natural: END-IF
                        //*  EM - 072312 END
                        //*  EM 052711
                    }                                                                                                                                                     //Natural: END-IF
                    ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val.getValue(pnd_J).setValue(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Interim_Annuity_Unit_Va());          //Natural: ASSIGN ADS-IA-RSLT.ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #J ) := #AIAN026-INTERIM-ANNUITY-UNIT-VA
                    //*  EM 052711
                    ads_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt.getValue(pnd_J).compute(new ComputeParameters(true, ads_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt.getValue(pnd_J)),       //Natural: COMPUTE ROUNDED ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #J ) = ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #J ) * #AIAN026-INTERIM-ANNUITY-UNIT-VA
                        ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units.getValue(pnd_J).multiply(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Interim_Annuity_Unit_Va()));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  022207
                                                                                                                                                                          //Natural: PERFORM DISPLAY-PARMS
                    sub_Display_Parms();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Err_Cde_X.setValue("L28");                                                                                                                        //Natural: ASSIGN #ERR-CDE-X := 'L28'
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Revalue_Cref_Monthly() throws Exception                                                                                                              //Natural: REVALUE-CREF-MONTHLY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *#CALL-TYPE := 'P'                             /* 030107 START
        //*  USE LATEST FACTOR     /* EM - 052711
        if (condition(pnd_Next_Dte_A_Pnd_Next_Dte_N.greater(pnd_Factor_Dte)))                                                                                             //Natural: IF #NEXT-DTE-N GT #FACTOR-DTE
        {
            pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Call_Type().setValue("L");                                                                                      //Natural: ASSIGN #AIAN026-CALL-TYPE := 'L'
            //*  EM - 052711
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Call_Type().setValue("P");                                                                                      //Natural: ASSIGN #AIAN026-CALL-TYPE := 'P'
            //*  030107 END
            //*  EM - 052711 START
            //*  EM - 05271 END
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Revaluation_Method().setValue("M");                                                                                 //Natural: ASSIGN #AIAN026-REVALUATION-METHOD := 'M'
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Iauv_Request_Date().compute(new ComputeParameters(false, pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Iauv_Request_Date()),  //Natural: ASSIGN #AIAN026-IAUV-REQUEST-DATE := VAL ( #NEXT-DTE-A )
            pnd_Next_Dte_A.val());
        pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Participation_Date().setValue(pnd_Asd_A_Pnd_Asd_N);                                                                 //Natural: ASSIGN #AIAN026-PARTICIPATION-DATE := #ASD-N
        FOR05:                                                                                                                                                            //Natural: FOR #J 1 TO 20
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(20)); pnd_J.nadd(1))
        {
            if (condition(ads_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd.getValue(pnd_J).notEquals(" ") && ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units.getValue(pnd_J).greater(getZero()))) //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #J ) NE ' ' AND ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #J ) GT 0
            {
                pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Ia_Fund_Code().setValue(ads_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd.getValue(pnd_J));                                  //Natural: ASSIGN #AIAN026-IA-FUND-CODE := ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #J )
                DbsUtil.callnat(Aian026.class , getCurrentProcessState(), pdaAiaa026.getPnd_Aian026_Linkage());                                                           //Natural: CALLNAT 'AIAN026' #AIAN026-LINKAGE
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                //*    IF #AIAN026-RETURN-CODE = 0                    /* EM 052711 - START
                //*  030912
                //*  072712 - START
                if (condition(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code_Nbr().equals(getZero())))                                                         //Natural: IF #AIAN026-RETURN-CODE-NBR = 0
                {
                    pnd_Cntrct_Py_Dte_Key_Cntrct_Ppcn_Nbr.setValue(ads_Ia_Rslt_Adi_Ia_Cref_Nbr);                                                                          //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.CNTRCT-PPCN-NBR := ADS-IA-RSLT.ADI-IA-CREF-NBR
                    pnd_Cntrct_Py_Dte_Key_Cntrct_Payee_Cde.setValue(1);                                                                                                   //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.CNTRCT-PAYEE-CDE := 01
                    pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde.reset();                                                                                                         //Natural: RESET #CNTRCT-PY-DTE-KEY.CMPNY-FUND-CDE
                    //*  072712 - END
                    DbsUtil.callnat(Iaan0511.class , getCurrentProcessState(), pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Ia_Fund_Code(), pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Cde,  //Natural: CALLNAT 'IAAN0511' #AIAN026-IA-FUND-CODE #FUND-CDE #RTN-CDE
                        pnd_Rtn_Cde);
                    if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                    ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val.getValue(pnd_J).setValue(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Interim_Annuity_Unit_Va());        //Natural: ASSIGN ADS-IA-RSLT.ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL ( #J ) := #AIAN026-INTERIM-ANNUITY-UNIT-VA
                    //*  EM - 072312 START
                    if (condition(pnd_Revalue_Cref.getBoolean()))                                                                                                         //Natural: IF #REVALUE-CREF
                    {
                        if (condition(ads_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd.getValue(pnd_J).equals("R") || ads_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd.getValue(pnd_J).equals("D")))      //Natural: IF ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( #J ) EQ 'R' OR EQ 'D'
                        {
                            pnd_Cntrct_Py_Dte_Key_Cntrct_Ppcn_Nbr.setValue(ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr);                                                                  //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.CNTRCT-PPCN-NBR := ADS-IA-RSLT.ADI-IA-TIAA-NBR
                            setValueToSubstring("W",pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde,1,1);                                                                            //Natural: MOVE 'W' TO SUBSTR ( #CNTRCT-PY-DTE-KEY.CMPNY-FUND-CDE,1,1 )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            setValueToSubstring("4",pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde,1,1);                                                                            //Natural: MOVE '4' TO SUBSTR ( #CNTRCT-PY-DTE-KEY.CMPNY-FUND-CDE,1,1 )
                        }                                                                                                                                                 //Natural: END-IF
                        ads_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt.getValue(pnd_J).reset();                                                                                   //Natural: RESET ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #J )
                        //*  072712
                        DbsUtil.callnat(Adsn600b.class , getCurrentProcessState(), pnd_C_Reval_Dte_A, ads_Ia_Rslt_Adi_Pymnt_Mode, pnd_Cntrct_Py_Dte_Key,                  //Natural: CALLNAT 'ADSN600B' #C-REVAL-DTE-A ADS-IA-RSLT.ADI-PYMNT-MODE #CNTRCT-PY-DTE-KEY ADS-IA-RSLT.ADI-DTL-CREF-IA-RATE-CD ( #J ) ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #J ) ADS-IA-RSLT.ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL ( #J ) ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #J )
                            ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd.getValue(pnd_J), ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units.getValue(pnd_J), ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val.getValue(pnd_J), 
                            ads_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt.getValue(pnd_J));
                        if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                        //*  EM - 072312 END
                    }                                                                                                                                                     //Natural: END-IF
                    //*  EM 052711 - END
                    ads_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt.getValue(pnd_J).compute(new ComputeParameters(true, ads_Ia_Rslt_Adi_Dtl_Cref_Da_Mnthly_Amt.getValue(pnd_J)),   //Natural: COMPUTE ROUNDED ADS-IA-RSLT.ADI-DTL-CREF-DA-MNTHLY-AMT ( #J ) = ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #J ) * #AIAN026-INTERIM-ANNUITY-UNIT-VA
                        ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units.getValue(pnd_J).multiply(pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Interim_Annuity_Unit_Va()));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  022207
                                                                                                                                                                          //Natural: PERFORM DISPLAY-PARMS
                    sub_Display_Parms();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Err_Cde_X.setValue("L28");                                                                                                                        //Natural: ASSIGN #ERR-CDE-X := 'L28'
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    //*  111910
    private void sub_Revalue_Tiaa() throws Exception                                                                                                                      //Natural: REVALUE-TIAA
    {
        if (BLNatReinput.isReinput()) return;

        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Calc_Method().setValue(pnd_Calc_Mthd_Pnd_C_Mthd);                                                                       //Natural: ASSIGN #IN-CALC-METHOD := #C-MTHD
        //*  111910
        //*  111910
        if (condition(DbsUtil.is(pnd_Final_Per_Pay_Dte.getText(),"N6")))                                                                                                  //Natural: IF #FINAL-PER-PAY-DTE IS ( N6 )
        {
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Act_Std_Fnl_Pp_Pay_Dte().compute(new ComputeParameters(false, pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Act_Std_Fnl_Pp_Pay_Dte()),  //Natural: ASSIGN #IN-ACT-STD-FNL-PP-PAY-DTE := VAL ( #FINAL-PER-PAY-DTE )
                pnd_Final_Per_Pay_Dte.val());
            //*  111910
        }                                                                                                                                                                 //Natural: END-IF
        //*  111910
        //*  111910
        if (condition(DbsUtil.is(pnd_Final_Pay_Dte.getText(),"N8")))                                                                                                      //Natural: IF #FINAL-PAY-DTE IS ( N8 )
        {
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Act_Std_Fnl_Pay_Dte().compute(new ComputeParameters(false, pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Act_Std_Fnl_Pay_Dte()),  //Natural: ASSIGN #IN-ACT-STD-FNL-PAY-DTE := VAL ( #FINAL-PAY-DTE )
                pnd_Final_Pay_Dte.val());
        }                                                                                                                                                                 //Natural: END-IF
        //*  111910
        if (condition(pnd_Mode.equals(100)))                                                                                                                              //Natural: IF #MODE = 100
        {
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Next_Payment_Date().compute(new ComputeParameters(false, pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Next_Payment_Date()),  //Natural: ASSIGN #IN-NEXT-PAYMENT-DATE := VAL ( #IA-CHECK-DTE )
                pdaAdsa600.getAdsa600_Pnd_Ia_Check_Dte().val());
            //*  111910
            //*  111910
            //*  111910
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Nxt_Pmt_Dte_Pnd_Nxt_Mm.setValue(pnd_Mode_Pnd_Mode_Mm);                                                                                                    //Natural: ASSIGN #NXT-MM := #MODE-MM
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Next_Payment_Date().setValue(pnd_Nxt_Pmt_Dte_Pnd_Nxt_Pmt_Dte_N);                                                    //Natural: ASSIGN #IN-NEXT-PAYMENT-DATE := #NXT-PMT-DTE-N
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Processing_Date().compute(new ComputeParameters(false, pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Processing_Date()),    //Natural: ASSIGN #AIAN035-LINKAGE.#IN-PROCESSING-DATE := VAL ( #T-REVAL-DTE-6 )
            pnd_T_Reval_Dte_A_Pnd_T_Reval_Dte_6.val());
        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract().setValue(ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr);                                                                       //Natural: ASSIGN #AIAN035-LINKAGE.#IN-CONTRACT := ADS-IA-RSLT.ADI-IA-TIAA-NBR
        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payee().setValue(1);                                                                                                    //Natural: ASSIGN #AIAN035-LINKAGE.#IN-PAYEE := 01
        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin().setValue(2);                                                                                          //Natural: ASSIGN #AIAN035-LINKAGE.#IN-CONTRACT-ORIGIN := 02
        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option().setValue(ads_Ia_Rslt_Adi_Optn_Cde);                                                                    //Natural: ASSIGN #AIAN035-LINKAGE.#IN-PAYMENT-OPTION := ADS-IA-RSLT.ADI-OPTN-CDE
        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Mode().setValue(ads_Ia_Rslt_Adi_Pymnt_Mode);                                                                    //Natural: ASSIGN #AIAN035-LINKAGE.#IN-PAYMENT-MODE := ADS-IA-RSLT.ADI-PYMNT-MODE
        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Issue_Date().setValue(pnd_Asd_A_Pnd_Asd_N);                                                                             //Natural: ASSIGN #AIAN035-LINKAGE.#IN-ISSUE-DATE := #ASD-N
        if (condition(ads_Prtcpnt_Adp_Frst_Annt_Sex_Cde.equals("M")))                                                                                                     //Natural: IF ADS-PRTCPNT.ADP-FRST-ANNT-SEX-CDE = 'M'
        {
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_First_Ann_Sex().setValue(1);                                                                                        //Natural: ASSIGN #AIAN035-LINKAGE.#IN-FIRST-ANN-SEX := 1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ads_Prtcpnt_Adp_Frst_Annt_Sex_Cde.equals("F")))                                                                                                     //Natural: IF ADS-PRTCPNT.ADP-FRST-ANNT-SEX-CDE = 'F'
        {
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_First_Ann_Sex().setValue(2);                                                                                        //Natural: ASSIGN #AIAN035-LINKAGE.#IN-FIRST-ANN-SEX := 2
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ads_Prtcpnt_Adp_Scnd_Annt_Sex_Cde.equals("M")))                                                                                                     //Natural: IF ADS-PRTCPNT.ADP-SCND-ANNT-SEX-CDE = 'M'
        {
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Second_Ann_Sex().setValue(1);                                                                                       //Natural: ASSIGN #AIAN035-LINKAGE.#IN-SECOND-ANN-SEX := 1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ads_Prtcpnt_Adp_Scnd_Annt_Sex_Cde.equals("F")))                                                                                                     //Natural: IF ADS-PRTCPNT.ADP-SCND-ANNT-SEX-CDE = 'F'
        {
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Second_Ann_Sex().setValue(2);                                                                                       //Natural: ASSIGN #AIAN035-LINKAGE.#IN-SECOND-ANN-SEX := 2
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Wdte8.setValueEdited(ads_Prtcpnt_Adp_Frst_Annt_Dte_Of_Brth,new ReportEditMask("YYYYMMDD"));                                                                   //Natural: MOVE EDITED ADS-PRTCPNT.ADP-FRST-ANNT-DTE-OF-BRTH ( EM = YYYYMMDD ) TO #WDTE8
        if (condition(DbsUtil.is(pnd_Wdte8.getText(),"N8")))                                                                                                              //Natural: IF #WDTE8 IS ( N8 )
        {
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_First_Ann_Birth_Date().compute(new ComputeParameters(false, pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_First_Ann_Birth_Date()),  //Natural: ASSIGN #AIAN035-LINKAGE.#IN-FIRST-ANN-BIRTH-DATE := VAL ( #WDTE8 )
                pnd_Wdte8.val());
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Wdte8.setValueEdited(ads_Prtcpnt_Adp_Scnd_Annt_Dte_Of_Brth,new ReportEditMask("YYYYMMDD"));                                                                   //Natural: MOVE EDITED ADS-PRTCPNT.ADP-SCND-ANNT-DTE-OF-BRTH ( EM = YYYYMMDD ) TO #WDTE8
        if (condition(DbsUtil.is(pnd_Wdte8.getText(),"N8")))                                                                                                              //Natural: IF #WDTE8 IS ( N8 )
        {
            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Second_Ann_Birth_Date().compute(new ComputeParameters(false, pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Second_Ann_Birth_Date()),  //Natural: ASSIGN #AIAN035-LINKAGE.#IN-SECOND-ANN-BIRTH-DATE := VAL ( #WDTE8 )
                pnd_Wdte8.val());
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue("*").greater(getZero()) || ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue("*").greater(getZero())  //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * ) GT 0 OR ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( * ) GT 0 OR ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( * ) GT 0 OR ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( * ) GT 0
            || ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue("*").greater(getZero()) || ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue("*").greater(getZero())))
        {
            if (condition(ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue("*").greater(getZero()) || ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue("*").greater(getZero()))) //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * ) GT 0 OR ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( * ) GT 0
            {
                pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Method().setValue("G");                                                                                 //Natural: ASSIGN #AIAN035-LINKAGE.#IN-PAYMENT-METHOD := 'G'
                pnd_K.reset();                                                                                                                                            //Natural: RESET #K
                FOR06:                                                                                                                                                    //Natural: FOR #J 1 TO ADS-IA-RSLT.C*ADI-DTL-TIAA-DATA
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(ads_Ia_Rslt_Count_Castadi_Dtl_Tiaa_Data)); pnd_J.nadd(1))
                {
                    if (condition(ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_J).greater(getZero()) || ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_J).greater(getZero()))) //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J ) GT 0 OR ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J ) GT 0
                    {
                        pnd_K.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #K
                        if (condition(pnd_K.greater(ads_Ia_Rslt_Count_Castadi_Dtl_Tiaa_Data)))                                                                            //Natural: IF #K GT ADS-IA-RSLT.C*ADI-DTL-TIAA-DATA
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Rate_Code().getValue(pnd_K).setValue(ads_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_J));              //Natural: ASSIGN #AIAN035-LINKAGE.#IN-RATE-CODE ( #K ) := ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #J )
                        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Guaranteed_Payment().getValue(pnd_K).setValue(ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_J));  //Natural: ASSIGN #AIAN035-LINKAGE.#IN-GUARANTEED-PAYMENT ( #K ) := ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J )
                        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Dividend_Payment().getValue(pnd_K).setValue(ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_J));    //Natural: ASSIGN #AIAN035-LINKAGE.#IN-DIVIDEND-PAYMENT ( #K ) := ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J )
                        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Final_Guaranteed_Payment().getValue(pnd_K).setValue(ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_J)); //Natural: ASSIGN #AIAN035-LINKAGE.#IN-FINAL-GUARANTEED-PAYMENT ( #K ) := ADS-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( #J )
                        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Final_Dividend_Payment().getValue(pnd_K).setValue(ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_J)); //Natural: ASSIGN #AIAN035-LINKAGE.#IN-FINAL-DIVIDEND-PAYMENT ( #K ) := ADS-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( #J )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*  030912 START
                if (condition(pnd_More.getBoolean()))                                                                                                                     //Natural: IF #MORE
                {
                    pnd_K.setValue(pnd_Max_Rslt);                                                                                                                         //Natural: ASSIGN #K := #MAX-RSLT
                    FOR07:                                                                                                                                                //Natural: FOR #J 1 TO ADS-IA-RSL2.C*ADI-DTL-TIAA-DATA
                    for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(ads_Ia_Rsl2_Count_Castadi_Dtl_Tiaa_Data)); pnd_J.nadd(1))
                    {
                        if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_J).greater(getZero()) || ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_J).greater(getZero()))) //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J ) GT 0 OR ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J ) GT 0
                        {
                            pnd_K.nadd(1);                                                                                                                                //Natural: ADD 1 TO #K
                            if (condition(pnd_K.greater(pnd_Max_Rate)))                                                                                                   //Natural: IF #K GT #MAX-RATE
                            {
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: END-IF
                            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Rate_Code().getValue(pnd_K).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_J));          //Natural: ASSIGN #AIAN035-LINKAGE.#IN-RATE-CODE ( #K ) := ADS-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #J )
                            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Guaranteed_Payment().getValue(pnd_K).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_J)); //Natural: ASSIGN #AIAN035-LINKAGE.#IN-GUARANTEED-PAYMENT ( #K ) := ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J )
                            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Dividend_Payment().getValue(pnd_K).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_J)); //Natural: ASSIGN #AIAN035-LINKAGE.#IN-DIVIDEND-PAYMENT ( #K ) := ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J )
                            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Final_Guaranteed_Payment().getValue(pnd_K).setValue(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_J)); //Natural: ASSIGN #AIAN035-LINKAGE.#IN-FINAL-GUARANTEED-PAYMENT ( #K ) := ADS-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( #J )
                            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Final_Dividend_Payment().getValue(pnd_K).setValue(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_J)); //Natural: ASSIGN #AIAN035-LINKAGE.#IN-FINAL-DIVIDEND-PAYMENT ( #K ) := ADS-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( #J )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                    //*  030912 END
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Callnat.equals("AIAN025")))                                                                                                             //Natural: IF #CALLNAT = 'AIAN025'
                {
                                                                                                                                                                          //Natural: PERFORM POPULATE-AIAA025
                    sub_Populate_Aiaa025();
                    if (condition(Global.isEscape())) {return;}
                    DbsUtil.callnat(DbsUtil.getBlType(pnd_Callnat), getCurrentProcessState(), pdaAiaa025.getPnd_Aian025_Linkage());                                       //Natural: CALLNAT #CALLNAT #AIAN025-LINKAGE
                    if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM POPULATE-OUT-AIAA0380
                    sub_Populate_Out_Aiaa0380();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Write_Output_Switch().setValue("N");                                                                        //Natural: ASSIGN #IN-WRITE-OUTPUT-SWITCH := 'N'
                    //*  CM - RESET AFTER FIRST CALL TO AIAN083
                    DbsUtil.callnat(DbsUtil.getBlType(pnd_Callnat), getCurrentProcessState(), pdaAiaa0380.getPnd_Aian035_Linkage());                                      //Natural: CALLNAT #CALLNAT #AIAN035-LINKAGE
                    if (condition(Global.isEscape())) return;
                    pls_Sub_Sw.setValue(false);                                                                                                                           //Natural: ASSIGN +SUB-SW := FALSE
                }                                                                                                                                                         //Natural: END-IF
                //*    IF #OUT-AIAN035-RETURN-CODE = 0              /* 030912
                //*  030912
                if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code_Nbr().equals(getZero())))                                                    //Natural: IF #OUT-AIAN035-RETURN-CODE-NBR = 0
                {
                    pnd_Cntrct_Py_Dte_Key_Cntrct_Ppcn_Nbr.setValue(ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr);                                                                          //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.CNTRCT-PPCN-NBR := ADS-IA-RSLT.ADI-IA-TIAA-NBR
                    pnd_Cntrct_Py_Dte_Key_Cntrct_Payee_Cde.setValue(1);                                                                                                   //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.CNTRCT-PAYEE-CDE := 01
                    pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde.setValue("T1G");                                                                                                 //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.CMPNY-FUND-CDE := 'T1G'
                    pnd_T.setValue(true);                                                                                                                                 //Natural: ASSIGN #T := TRUE
                                                                                                                                                                          //Natural: PERFORM CREATE-HISTORICAL-RATES
                    sub_Create_Historical_Rates();
                    if (condition(Global.isEscape())) {return;}
                    pnd_T.reset();                                                                                                                                        //Natural: RESET #T #K
                    pnd_K.reset();
                    pdaAdsa600.getAdsa600_Pnd_Old_Tiaa_Grd_Grntd_Amt().compute(new ComputeParameters(false, pdaAdsa600.getAdsa600_Pnd_Old_Tiaa_Grd_Grntd_Amt()),          //Natural: ASSIGN #OLD-TIAA-GRD-GRNTD-AMT := 0 + ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * )
                        DbsField.add(getZero(),ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue("*")));
                    pdaAdsa600.getAdsa600_Pnd_Old_Tiaa_Grd_Dvdnd_Amt().compute(new ComputeParameters(false, pdaAdsa600.getAdsa600_Pnd_Old_Tiaa_Grd_Dvdnd_Amt()),          //Natural: ASSIGN #OLD-TIAA-GRD-DVDND-AMT := 0 + ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( * )
                        DbsField.add(getZero(),ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue("*")));
                    //*  030912 START
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        pdaAdsa600.getAdsa600_Pnd_Old_Tiaa_Grd_Grntd_Amt().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue("*"));                                    //Natural: ASSIGN #OLD-TIAA-GRD-GRNTD-AMT := #OLD-TIAA-GRD-GRNTD-AMT + ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * )
                        pdaAdsa600.getAdsa600_Pnd_Old_Tiaa_Grd_Dvdnd_Amt().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue("*"));                                    //Natural: ASSIGN #OLD-TIAA-GRD-DVDND-AMT := #OLD-TIAA-GRD-DVDND-AMT + ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( * )
                        //*  030912 END
                    }                                                                                                                                                     //Natural: END-IF
                    FOR08:                                                                                                                                                //Natural: FOR #J 1 TO ADS-IA-RSLT.C*ADI-DTL-TIAA-DATA
                    for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(ads_Ia_Rslt_Count_Castadi_Dtl_Tiaa_Data)); pnd_J.nadd(1))
                    {
                        if (condition(ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_J).greater(getZero()) || ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_J).greater(getZero()))) //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J ) GT 0 OR ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J ) GT 0
                        {
                            pnd_K.nadd(1);                                                                                                                                //Natural: ADD 1 TO #K
                            if (condition(pnd_K.greater(ads_Ia_Rslt_Count_Castadi_Dtl_Tiaa_Data)))                                                                        //Natural: IF #K GT ADS-IA-RSLT.C*ADI-DTL-TIAA-DATA
                            {
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: END-IF
                            ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_J).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Guaranteed_Py().getValue(pnd_K)); //Natural: ASSIGN ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J ) := #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-GUARANTEED-PY ( #K )
                            ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_J).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Dividend_Pay().getValue(pnd_K)); //Natural: ASSIGN ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J ) := #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-DIVIDEND-PAY ( #K )
                            ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_J).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Final_Guar_Py().getValue(pnd_K)); //Natural: ASSIGN ADS-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( #J ) := #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-FINAL-GUAR-PY ( #K )
                            ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_J).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Final_Div_Pay().getValue(pnd_K)); //Natural: ASSIGN ADS-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( #J ) := #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-FINAL-DIV-PAY ( #K )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                    //*  030912 START
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        pnd_K.setValue(pnd_Max_Rslt);                                                                                                                     //Natural: ASSIGN #K := #MAX-RSLT
                        FOR09:                                                                                                                                            //Natural: FOR #J 1 TO ADS-IA-RSL2.C*ADI-DTL-TIAA-DATA
                        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(ads_Ia_Rsl2_Count_Castadi_Dtl_Tiaa_Data)); pnd_J.nadd(1))
                        {
                            if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_J).greater(getZero()) || ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_J).greater(getZero()))) //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J ) GT 0 OR ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J ) GT 0
                            {
                                pnd_K.nadd(1);                                                                                                                            //Natural: ADD 1 TO #K
                                if (condition(pnd_K.greater(pnd_Max_Rate)))                                                                                               //Natural: IF #K GT #MAX-RATE
                                {
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                                ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_J).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Guaranteed_Py().getValue(pnd_K)); //Natural: ASSIGN ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #J ) := #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-GUARANTEED-PY ( #K )
                                ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_J).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Dividend_Pay().getValue(pnd_K)); //Natural: ASSIGN ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #J ) := #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-DIVIDEND-PAY ( #K )
                                ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_J).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Final_Guar_Py().getValue(pnd_K)); //Natural: ASSIGN ADS-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( #J ) := #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-FINAL-GUAR-PY ( #K )
                                ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_J).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Final_Div_Pay().getValue(pnd_K)); //Natural: ASSIGN ADS-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( #J ) := #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-FINAL-DIV-PAY ( #K )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (Global.isEscape()) return;
                        //*  030912 END
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //* *    PERFORM DISPLAY-PARMS   /* 022207/120611
                    //*  120611
                                                                                                                                                                          //Natural: PERFORM DISPLAY-AIAN035-PARMS
                    sub_Display_Aian035_Parms();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Err_Cde_X.setValue("L28");                                                                                                                        //Natural: ASSIGN #ERR-CDE-X := 'L28'
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue("*").greater(getZero()) || ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue("*").greater(getZero()))) //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( * ) GT 0 OR ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( * ) GT 0
            {
                pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Method().setValue("S");                                                                                 //Natural: ASSIGN #AIAN035-LINKAGE.#IN-PAYMENT-METHOD := 'S'
                pnd_K.reset();                                                                                                                                            //Natural: RESET #K #AIAN035-LINKAGE.#IN-RATE-CODE ( * ) #AIAN025-LINKAGE.#IN-RATE-CODE ( * ) #AIAN035-LINKAGE.#IN-GUARANTEED-PAYMENT ( * ) #AIAN025-LINKAGE.#IN-GUARANTEED-PAYMENT ( * ) #AIAN035-LINKAGE.#IN-DIVIDEND-PAYMENT ( * ) #AIAN025-LINKAGE.#IN-DIVIDEND-PAYMENT ( * ) #AIAN035-LINKAGE.#IN-FINAL-GUARANTEED-PAYMENT ( * ) #AIAN025-LINKAGE.#IN-FINAL-GUARANTEED-PAYMENT ( * ) #AIAN035-LINKAGE.#IN-FINAL-DIVIDEND-PAYMENT ( * ) #AIAN025-LINKAGE.#IN-FINAL-DIVIDEND-PAYMENT ( * )
                pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Rate_Code().getValue("*").reset();
                pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Rate_Code().getValue("*").reset();
                pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Guaranteed_Payment().getValue("*").reset();
                pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Guaranteed_Payment().getValue("*").reset();
                pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Dividend_Payment().getValue("*").reset();
                pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Dividend_Payment().getValue("*").reset();
                pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Final_Guaranteed_Payment().getValue("*").reset();
                pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Final_Guaranteed_Payment().getValue("*").reset();
                pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Final_Dividend_Payment().getValue("*").reset();
                pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Final_Dividend_Payment().getValue("*").reset();
                FOR10:                                                                                                                                                    //Natural: FOR #J 1 TO ADS-IA-RSLT.C*ADI-DTL-TIAA-DATA
                for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(ads_Ia_Rslt_Count_Castadi_Dtl_Tiaa_Data)); pnd_J.nadd(1))
                {
                    if (condition(ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_J).greater(getZero()) || ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_J).greater(getZero()))) //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J ) GT 0 OR ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #J ) GT 0
                    {
                        pnd_K.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #K
                        if (condition(pnd_K.greater(ads_Ia_Rslt_Count_Castadi_Dtl_Tiaa_Data)))                                                                            //Natural: IF #K GT ADS-IA-RSLT.C*ADI-DTL-TIAA-DATA
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Rate_Code().getValue(pnd_K).setValue(ads_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_J));              //Natural: ASSIGN #AIAN035-LINKAGE.#IN-RATE-CODE ( #K ) := ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #J )
                        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Guaranteed_Payment().getValue(pnd_K).setValue(ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_J));  //Natural: ASSIGN #AIAN035-LINKAGE.#IN-GUARANTEED-PAYMENT ( #K ) := ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J )
                        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Dividend_Payment().getValue(pnd_K).setValue(ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_J));    //Natural: ASSIGN #AIAN035-LINKAGE.#IN-DIVIDEND-PAYMENT ( #K ) := ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #J )
                        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Final_Guaranteed_Payment().getValue(pnd_K).setValue(ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_J)); //Natural: ASSIGN #AIAN035-LINKAGE.#IN-FINAL-GUARANTEED-PAYMENT ( #K ) := ADS-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( #J )
                        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Final_Dividend_Payment().getValue(pnd_K).setValue(ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_J)); //Natural: ASSIGN #AIAN035-LINKAGE.#IN-FINAL-DIVIDEND-PAYMENT ( #K ) := ADS-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( #J )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*  030912 START
                if (condition(pnd_More.getBoolean()))                                                                                                                     //Natural: IF #MORE
                {
                    pnd_K.setValue(pnd_Max_Rslt);                                                                                                                         //Natural: ASSIGN #K := #MAX-RSLT
                    FOR11:                                                                                                                                                //Natural: FOR #J 1 TO ADS-IA-RSL2.C*ADI-DTL-TIAA-DATA
                    for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(ads_Ia_Rsl2_Count_Castadi_Dtl_Tiaa_Data)); pnd_J.nadd(1))
                    {
                        if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_J).greater(getZero()) || ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_J).greater(getZero()))) //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J ) GT 0 OR ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #J ) GT 0
                        {
                            pnd_K.nadd(1);                                                                                                                                //Natural: ADD 1 TO #K
                            if (condition(pnd_K.greater(pnd_Max_Rate)))                                                                                                   //Natural: IF #K GT #MAX-RATE
                            {
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: END-IF
                            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Rate_Code().getValue(pnd_K).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_J));          //Natural: ASSIGN #AIAN035-LINKAGE.#IN-RATE-CODE ( #K ) := ADS-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #J )
                            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Guaranteed_Payment().getValue(pnd_K).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_J)); //Natural: ASSIGN #AIAN035-LINKAGE.#IN-GUARANTEED-PAYMENT ( #K ) := ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J )
                            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Dividend_Payment().getValue(pnd_K).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_J)); //Natural: ASSIGN #AIAN035-LINKAGE.#IN-DIVIDEND-PAYMENT ( #K ) := ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #J )
                            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Final_Guaranteed_Payment().getValue(pnd_K).setValue(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_J)); //Natural: ASSIGN #AIAN035-LINKAGE.#IN-FINAL-GUARANTEED-PAYMENT ( #K ) := ADS-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( #J )
                            pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Final_Dividend_Payment().getValue(pnd_K).setValue(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_J)); //Natural: ASSIGN #AIAN035-LINKAGE.#IN-FINAL-DIVIDEND-PAYMENT ( #K ) := ADS-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( #J )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                    //*  030912 END
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Callnat.equals("AIAN025")))                                                                                                             //Natural: IF #CALLNAT = 'AIAN025'
                {
                                                                                                                                                                          //Natural: PERFORM POPULATE-AIAA025
                    sub_Populate_Aiaa025();
                    if (condition(Global.isEscape())) {return;}
                    DbsUtil.callnat(DbsUtil.getBlType(pnd_Callnat), getCurrentProcessState(), pdaAiaa025.getPnd_Aian025_Linkage());                                       //Natural: CALLNAT #CALLNAT #AIAN025-LINKAGE
                    if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM POPULATE-OUT-AIAA0380
                    sub_Populate_Out_Aiaa0380();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Write_Output_Switch().setValue("N");                                                                        //Natural: ASSIGN #IN-WRITE-OUTPUT-SWITCH := 'N'
                    //*  CM - RESET AFTER FIRST CALL TO AIAN083
                    DbsUtil.callnat(DbsUtil.getBlType(pnd_Callnat), getCurrentProcessState(), pdaAiaa0380.getPnd_Aian035_Linkage());                                      //Natural: CALLNAT #CALLNAT #AIAN035-LINKAGE
                    if (condition(Global.isEscape())) return;
                    pls_Sub_Sw.setValue(false);                                                                                                                           //Natural: ASSIGN +SUB-SW := FALSE
                }                                                                                                                                                         //Natural: END-IF
                //*    IF #AIAN035-LINKAGE.#OUT-AIAN035-RETURN-CODE = 0       /* 030912
                //*  030912
                if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code_Nbr().equals(getZero())))                                                    //Natural: IF #AIAN035-LINKAGE.#OUT-AIAN035-RETURN-CODE-NBR = 0
                {
                    pnd_Cntrct_Py_Dte_Key_Cntrct_Ppcn_Nbr.setValue(ads_Ia_Rslt_Adi_Ia_Tiaa_Nbr);                                                                          //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.CNTRCT-PPCN-NBR := ADS-IA-RSLT.ADI-IA-TIAA-NBR
                    pnd_Cntrct_Py_Dte_Key_Cntrct_Payee_Cde.setValue(1);                                                                                                   //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.CNTRCT-PAYEE-CDE := 01
                    pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde.setValue("T1S");                                                                                                 //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.CMPNY-FUND-CDE := 'T1S'
                    pnd_T.setValue(true);                                                                                                                                 //Natural: ASSIGN #T := TRUE
                                                                                                                                                                          //Natural: PERFORM CREATE-HISTORICAL-RATES
                    sub_Create_Historical_Rates();
                    if (condition(Global.isEscape())) {return;}
                    pnd_T.reset();                                                                                                                                        //Natural: RESET #T #K
                    pnd_K.reset();
                    pdaAdsa600.getAdsa600_Pnd_Old_Tiaa_Std_Grntd_Amt().compute(new ComputeParameters(false, pdaAdsa600.getAdsa600_Pnd_Old_Tiaa_Std_Grntd_Amt()),          //Natural: ASSIGN #OLD-TIAA-STD-GRNTD-AMT := 0 + ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( * )
                        DbsField.add(getZero(),ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue("*")));
                    pdaAdsa600.getAdsa600_Pnd_Old_Tiaa_Std_Dvdnd_Amt().compute(new ComputeParameters(false, pdaAdsa600.getAdsa600_Pnd_Old_Tiaa_Std_Dvdnd_Amt()),          //Natural: ASSIGN #OLD-TIAA-STD-DVDND-AMT := 0 + ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( * )
                        DbsField.add(getZero(),ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue("*")));
                    //*  030912 START
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        pdaAdsa600.getAdsa600_Pnd_Old_Tiaa_Std_Grntd_Amt().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue("*"));                                    //Natural: ASSIGN #OLD-TIAA-STD-GRNTD-AMT := #OLD-TIAA-STD-GRNTD-AMT + ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( * )
                        pdaAdsa600.getAdsa600_Pnd_Old_Tiaa_Std_Dvdnd_Amt().nadd(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue("*"));                                    //Natural: ASSIGN #OLD-TIAA-STD-DVDND-AMT := #OLD-TIAA-STD-DVDND-AMT + ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( * )
                        //*  030912 END
                    }                                                                                                                                                     //Natural: END-IF
                    FOR12:                                                                                                                                                //Natural: FOR #J 1 TO ADS-IA-RSLT.C*ADI-DTL-TIAA-DATA
                    for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(ads_Ia_Rslt_Count_Castadi_Dtl_Tiaa_Data)); pnd_J.nadd(1))
                    {
                        if (condition(ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_J).greater(getZero()) || ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_J).greater(getZero()))) //Natural: IF ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J ) GT 0 OR ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #J ) GT 0
                        {
                            pnd_K.nadd(1);                                                                                                                                //Natural: ADD 1 TO #K
                            if (condition(pnd_K.greater(ads_Ia_Rslt_Count_Castadi_Dtl_Tiaa_Data)))                                                                        //Natural: IF #K GT ADS-IA-RSLT.C*ADI-DTL-TIAA-DATA
                            {
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: END-IF
                            ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_J).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Guaranteed_Py().getValue(pnd_K)); //Natural: ASSIGN ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J ) := #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-GUARANTEED-PY ( #K )
                            ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_J).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Dividend_Pay().getValue(pnd_K)); //Natural: ASSIGN ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #J ) := #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-DIVIDEND-PAY ( #K )
                            ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_J).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Final_Guar_Py().getValue(pnd_K)); //Natural: ASSIGN ADS-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( #J ) := #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-FINAL-GUAR-PY ( #K )
                            ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_J).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Final_Div_Pay().getValue(pnd_K)); //Natural: ASSIGN ADS-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( #J ) := #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-FINAL-DIV-PAY ( #K )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (Global.isEscape()) return;
                    //*  030912 START
                    if (condition(pnd_More.getBoolean()))                                                                                                                 //Natural: IF #MORE
                    {
                        pnd_K.setValue(pnd_Max_Rslt);                                                                                                                     //Natural: ASSIGN #K := #MAX-RSLT
                        FOR13:                                                                                                                                            //Natural: FOR #J 1 TO ADS-IA-RSL2.C*ADI-DTL-TIAA-DATA
                        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(ads_Ia_Rsl2_Count_Castadi_Dtl_Tiaa_Data)); pnd_J.nadd(1))
                        {
                            if (condition(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_J).greater(getZero()) || ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_J).greater(getZero()))) //Natural: IF ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J ) GT 0 OR ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #J ) GT 0
                            {
                                pnd_K.nadd(1);                                                                                                                            //Natural: ADD 1 TO #K
                                if (condition(pnd_K.greater(pnd_Max_Rate)))                                                                                               //Natural: IF #K GT #MAX-RATE
                                {
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                                ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_J).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Guaranteed_Py().getValue(pnd_K)); //Natural: ASSIGN ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #J ) := #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-GUARANTEED-PY ( #K )
                                ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_J).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Dividend_Pay().getValue(pnd_K)); //Natural: ASSIGN ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #J ) := #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-DIVIDEND-PAY ( #K )
                                ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_J).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Final_Guar_Py().getValue(pnd_K)); //Natural: ASSIGN ADS-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( #J ) := #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-FINAL-GUAR-PY ( #K )
                                ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_J).setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Final_Div_Pay().getValue(pnd_K)); //Natural: ASSIGN ADS-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( #J ) := #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-FINAL-DIV-PAY ( #K )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (Global.isEscape()) return;
                        //*  030912 END
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //* *    PERFORM DISPLAY-PARMS   /* 022207/120611
                    //*  120611
                                                                                                                                                                          //Natural: PERFORM DISPLAY-AIAN035-PARMS
                    sub_Display_Aian035_Parms();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Err_Cde_X.setValue("L28");                                                                                                                        //Natural: ASSIGN #ERR-CDE-X := 'L28'
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_Historical_Rates() throws Exception                                                                                                           //Natural: CREATE-HISTORICAL-RATES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_T.getBoolean()))                                                                                                                                //Natural: IF #T
        {
            //*  030912
            //*  030912
            //*  030912
            //*  030912
            //*  030912
            //*  030912
            //*  030912
            //*  030912
            //*  030912
            //*  030912
            //*  030912
            //*  030912
            //*  030912
            DbsUtil.callnat(Adsn600a.class , getCurrentProcessState(), pnd_T_Reval_Dte_A, ads_Ia_Rslt_Adi_Pymnt_Mode, pnd_Cntrct_Py_Dte_Key, ads_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue("*"),  //Natural: CALLNAT 'ADSN600A' #T-REVAL-DTE-A ADS-IA-RSLT.ADI-PYMNT-MODE #CNTRCT-PY-DTE-KEY ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( * ) ADS-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( * ) ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * ) ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( * ) ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( * ) ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( * ) ADS-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( * ) ADS-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( * ) ADS-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( * ) ADS-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( * ) ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( * ) ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( * ) ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( * ) ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( * ) ADS-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( * ) ADS-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( * ) ADS-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( * ) ADS-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( * ) ADS-IA-RSLT.ADI-CONTRACT-ID ( * ) ADS-IA-RSL2.ADI-CONTRACT-ID ( * ) #MORE
                ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue("*"), ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue("*"), ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue("*"), 
                ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue("*"), ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue("*"), ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue("*"), 
                ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue("*"), ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue("*"), ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue("*"), 
                ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue("*"), ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue("*"), ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue("*"), 
                ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue("*"), ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt.getValue("*"), ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue("*"), 
                ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue("*"), ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue("*"), ads_Ia_Rslt_Adi_Contract_Id.getValue("*"), 
                ads_Ia_Rsl2_Adi_Contract_Id.getValue("*"), pnd_More);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  030912
            //*  072712
            DbsUtil.callnat(Adsn600b.class , getCurrentProcessState(), pnd_C_Reval_Dte_A, ads_Ia_Rslt_Adi_Pymnt_Mode, pnd_Cntrct_Py_Dte_Key, ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Rate_Cd.getValue(pnd_J),  //Natural: CALLNAT 'ADSN600B' #C-REVAL-DTE-A ADS-IA-RSLT.ADI-PYMNT-MODE #CNTRCT-PY-DTE-KEY ADS-IA-RSLT.ADI-DTL-CREF-IA-RATE-CD ( #J ) ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( #J ) ADS-IA-RSLT.ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #J ) ADS-IA-RSLT.ADI-DTL-CREF-DA-ANNL-AMT ( #J )
                ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units.getValue(pnd_J), ads_Ia_Rslt_Adi_Dtl_Cref_Ia_Annl_Unit_Val.getValue(pnd_J), ads_Ia_Rslt_Adi_Dtl_Cref_Da_Annl_Amt.getValue(pnd_J));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Call_Ivc_Calc_Module() throws Exception                                                                                                              //Natural: CALL-IVC-CALC-MODULE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Ivc_Call.equals("T")))                                                                                                                          //Natural: IF #IVC-CALL = 'T'
        {
            cpnd_Adi_Dtl_Tiaa_Data.setValue(ads_Ia_Rslt_Count_Castadi_Dtl_Tiaa_Data);                                                                                     //Natural: ASSIGN C#ADI-DTL-TIAA-DATA := ADS-IA-RSLT.C*ADI-DTL-TIAA-DATA
            if (condition(pnd_More.getBoolean()))                                                                                                                         //Natural: IF #MORE
            {
                cpnd_Adi_Dtl_Tiaa_Data.nadd(ads_Ia_Rsl2_Count_Castadi_Dtl_Tiaa_Data);                                                                                     //Natural: ASSIGN C#ADI-DTL-TIAA-DATA := C#ADI-DTL-TIAA-DATA + ADS-IA-RSL2.C*ADI-DTL-TIAA-DATA
            }                                                                                                                                                             //Natural: END-IF
            pnd_Tiaa_Std_Grntd_Amt.getValue(1,":",pnd_Max_Rslt).setValue(ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(1,":",pnd_Max_Rslt));                            //Natural: ASSIGN #TIAA-STD-GRNTD-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-RSLT )
            pnd_Tiaa_Std_Dvdnd_Amt.getValue(1,":",pnd_Max_Rslt).setValue(ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(1,":",pnd_Max_Rslt));                            //Natural: ASSIGN #TIAA-STD-DVDND-AMT ( 1:#MAX-RSLT ) := ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-RSLT )
            if (condition(pnd_More.getBoolean()))                                                                                                                         //Natural: IF #MORE
            {
                pnd_Tiaa_Std_Grntd_Amt.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(1,                //Natural: ASSIGN #TIAA-STD-GRNTD-AMT ( #MAX-RSLT + 1:#MAX-RATE ) := ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
                pnd_Tiaa_Std_Dvdnd_Amt.getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(1,                //Natural: ASSIGN #TIAA-STD-DVDND-AMT ( #MAX-RSLT + 1:#MAX-RATE ) := ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-RSLT )
                    ":",pnd_Max_Rslt));
            }                                                                                                                                                             //Natural: END-IF
            //*  030912 END
            //*  ADDED 9/03
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Sve_Ivc_Pymnt_Rule.setValue(pnd_Ivc_Pymnt_Rule);                                                                                                              //Natural: ASSIGN #SVE-IVC-PYMNT-RULE := #IVC-PYMNT-RULE
        if (condition(DbsUtil.maskMatches(pdaAdsa600.getAdsa600_Adp_Alt_Dest_Rlvr_Dest(),"'IRA'") || DbsUtil.maskMatches(pdaAdsa600.getAdsa600_Adp_Alt_Dest_Rlvr_Dest(),"'03B'")  //Natural: IF ( ADSA600.ADP-ALT-DEST-RLVR-DEST = MASK ( 'IRA' ) OR = MASK ( '03B' ) OR = MASK ( 'QPL' ) OR = MASK ( '57B' ) )
            || DbsUtil.maskMatches(pdaAdsa600.getAdsa600_Adp_Alt_Dest_Rlvr_Dest(),"'QPL'") || DbsUtil.maskMatches(pdaAdsa600.getAdsa600_Adp_Alt_Dest_Rlvr_Dest(),
            "'57B'")))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ivc_Pymnt_Rule.setValue("P");                                                                                                                             //Natural: ASSIGN #IVC-PYMNT-RULE := 'P'
        }                                                                                                                                                                 //Natural: END-IF
        //*  030912
        //*  030912
        //*  030912
        //*  030912
        //*  030912
        //*  030912
        DbsUtil.callnat(Adsn606.class , getCurrentProcessState(), pnd_Ivc_Call, ads_Ia_Rslt_Adi_Annty_Strt_Dte, ads_Ia_Rslt_Adi_Annty_Strt_Dte, ads_Prtcpnt_Adp_Ia_Tiaa_Nbr,  //Natural: CALLNAT 'ADSN606' #IVC-CALL ADS-IA-RSLT.ADI-ANNTY-STRT-DTE ADS-IA-RSLT.ADI-ANNTY-STRT-DTE ADS-PRTCPNT.ADP-IA-TIAA-NBR ADS-PRTCPNT.ADP-IA-CREF-NBR ADS-IA-RSLT.ADI-FINL-PERIODIC-PY-DTE ADS-IA-RSLT.ADI-PYMNT-MODE ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-RSLT ) ADS-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-RSLT ) #TIAA-STD-GRNTD-AMT ( 1:#MAX-RATE ) #TIAA-STD-DVDND-AMT ( 1:#MAX-RATE ) C#ADI-DTL-TIAA-DATA ADS-IA-RSLT.ADI-DTL-CREF-ACCT-CD ( 1:20 ) ADS-IA-RSLT.ADI-DTL-CREF-ANNL-NBR-UNITS ( 1:20 ) ADS-IA-RSLT.ADI-DTL-CREF-ANNL-AMT ( 1:20 ) ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( 1:20 ) ADS-IA-RSLT.ADI-DTL-CREF-MNTHLY-AMT ( 1:20 ) ADS-PRTCPNT.ADP-ALT-DEST-RLVR-DEST #IVC-PYMNT-RULE ADS-PRTCPNT.ADP-IVC-RLVR-CDE ADS-CNTRCT.ADC-TRK-RO-IVC #TIAA-IVC #CREF-IVC #PRDC-IVC #ERR-CDE-X #MORE
            ads_Prtcpnt_Adp_Ia_Cref_Nbr, ads_Ia_Rslt_Adi_Finl_Periodic_Py_Dte, ads_Ia_Rslt_Adi_Pymnt_Mode, ads_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(1,":",pnd_Max_Rslt), 
            ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(1,":",pnd_Max_Rslt), pnd_Tiaa_Std_Grntd_Amt.getValue(1,":",pnd_Max_Rate), pnd_Tiaa_Std_Dvdnd_Amt.getValue(1,":",pnd_Max_Rate), 
            cpnd_Adi_Dtl_Tiaa_Data, ads_Ia_Rslt_Adi_Dtl_Cref_Acct_Cd.getValue(1,":",20), ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Nbr_Units.getValue(1,":",20), ads_Ia_Rslt_Adi_Dtl_Cref_Annl_Amt.getValue(1,":",20), 
            ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Nbr_Units.getValue(1,":",20), ads_Ia_Rslt_Adi_Dtl_Cref_Mnthly_Amt.getValue(1,":",20), ads_Prtcpnt_Adp_Alt_Dest_Rlvr_Dest, 
            pnd_Ivc_Pymnt_Rule, ads_Prtcpnt_Adp_Ivc_Rlvr_Cde, ads_Cntrct_Adc_Trk_Ro_Ivc, pnd_Tiaa_Ivc, pnd_Cref_Ivc, pnd_Prdc_Ivc, pnd_Err_Cde_X, pnd_More);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Err_Cde_X.notEquals(" ")))                                                                                                                      //Natural: IF #ERR-CDE-X NE ' '
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  ADDED 9/03
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ivc_Pymnt_Rule.setValue(pnd_Sve_Ivc_Pymnt_Rule);                                                                                                              //Natural: ASSIGN #IVC-PYMNT-RULE := #SVE-IVC-PYMNT-RULE
        if (condition(pnd_Ivc_Call.equals("T")))                                                                                                                          //Natural: IF #IVC-CALL = 'T'
        {
            ads_Ia_Rslt_Adi_Tiaa_Prdc_Ivc_Amt.setValue(pnd_Prdc_Ivc);                                                                                                     //Natural: ASSIGN ADS-IA-RSLT.ADI-TIAA-PRDC-IVC-AMT := #PRDC-IVC
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ads_Ia_Rslt_Adi_Cref_Prdc_Ivc_Amt.setValue(pnd_Prdc_Ivc);                                                                                                     //Natural: ASSIGN ADS-IA-RSLT.ADI-CREF-PRDC-IVC-AMT := #PRDC-IVC
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Populate_Aiaa025() throws Exception                                                                                                                  //Natural: POPULATE-AIAA025
    {
        if (BLNatReinput.isReinput()) return;

        pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Processing_Date().setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Processing_Date());                                 //Natural: ASSIGN #AIAN025-LINKAGE.#IN-PROCESSING-DATE := #AIAN035-LINKAGE.#IN-PROCESSING-DATE
        pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Contract_Payee().setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Payee());                                   //Natural: ASSIGN #AIAN025-LINKAGE.#IN-CONTRACT-PAYEE := #AIAN035-LINKAGE.#IN-CONTRACT-PAYEE
        pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Contract_Origin().setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin());                                 //Natural: ASSIGN #AIAN025-LINKAGE.#IN-CONTRACT-ORIGIN := #AIAN035-LINKAGE.#IN-CONTRACT-ORIGIN
        pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Payment_Option().setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option());                                   //Natural: ASSIGN #AIAN025-LINKAGE.#IN-PAYMENT-OPTION := #AIAN035-LINKAGE.#IN-PAYMENT-OPTION
        pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Payment_Mode().setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Mode());                                       //Natural: ASSIGN #AIAN025-LINKAGE.#IN-PAYMENT-MODE := #AIAN035-LINKAGE.#IN-PAYMENT-MODE
        pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Issue_Date().setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Issue_Date());                                           //Natural: ASSIGN #AIAN025-LINKAGE.#IN-ISSUE-DATE := #AIAN035-LINKAGE.#IN-ISSUE-DATE
        pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_First_Ann_Sex().setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_First_Ann_Sex());                                     //Natural: ASSIGN #AIAN025-LINKAGE.#IN-FIRST-ANN-SEX := #AIAN035-LINKAGE.#IN-FIRST-ANN-SEX
        pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Second_Ann_Sex().setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Second_Ann_Sex());                                   //Natural: ASSIGN #AIAN025-LINKAGE.#IN-SECOND-ANN-SEX := #AIAN035-LINKAGE.#IN-SECOND-ANN-SEX
        pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_First_Ann_Birth_Date().setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_First_Ann_Birth_Date());                       //Natural: ASSIGN #AIAN025-LINKAGE.#IN-FIRST-ANN-BIRTH-DATE := #AIAN035-LINKAGE.#IN-FIRST-ANN-BIRTH-DATE
        pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Second_Ann_Birth_Date().setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Second_Ann_Birth_Date());                     //Natural: ASSIGN #AIAN025-LINKAGE.#IN-SECOND-ANN-BIRTH-DATE := #AIAN035-LINKAGE.#IN-SECOND-ANN-BIRTH-DATE
        pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Payment_Method().setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Method());                                   //Natural: ASSIGN #AIAN025-LINKAGE.#IN-PAYMENT-METHOD := #AIAN035-LINKAGE.#IN-PAYMENT-METHOD
        pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Rate_Code().getValue("*").setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Rate_Code().getValue("*"));                 //Natural: ASSIGN #AIAN025-LINKAGE.#IN-RATE-CODE ( * ) := #AIAN035-LINKAGE.#IN-RATE-CODE ( * )
        pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Guaranteed_Payment().getValue("*").setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Guaranteed_Payment().getValue("*")); //Natural: ASSIGN #AIAN025-LINKAGE.#IN-GUARANTEED-PAYMENT ( * ) := #AIAN035-LINKAGE.#IN-GUARANTEED-PAYMENT ( * )
        pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Dividend_Payment().getValue("*").setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Dividend_Payment().getValue("*"));   //Natural: ASSIGN #AIAN025-LINKAGE.#IN-DIVIDEND-PAYMENT ( * ) := #AIAN035-LINKAGE.#IN-DIVIDEND-PAYMENT ( * )
        pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Final_Guaranteed_Payment().getValue("*").setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Final_Guaranteed_Payment().getValue("*")); //Natural: ASSIGN #AIAN025-LINKAGE.#IN-FINAL-GUARANTEED-PAYMENT ( * ) := #AIAN035-LINKAGE.#IN-FINAL-GUARANTEED-PAYMENT ( * )
        pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Final_Dividend_Payment().getValue("*").setValue(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Final_Dividend_Payment().getValue("*")); //Natural: ASSIGN #AIAN025-LINKAGE.#IN-FINAL-DIVIDEND-PAYMENT ( * ) := #AIAN035-LINKAGE.#IN-FINAL-DIVIDEND-PAYMENT ( * )
        pnd_Wdte6.setValueEdited(ads_Ia_Rslt_Adi_Finl_Periodic_Py_Dte,new ReportEditMask("YYYYMM"));                                                                      //Natural: MOVE EDITED ADS-IA-RSLT.ADI-FINL-PERIODIC-PY-DTE ( EM = YYYYMM ) TO #WDTE6
        if (condition(DbsUtil.is(pnd_Wdte6.getText(),"N6")))                                                                                                              //Natural: IF #WDTE6 IS ( N6 )
        {
            pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Last_Payment_Date().compute(new ComputeParameters(false, pdaAiaa025.getPnd_Aian025_Linkage_Pnd_In_Last_Payment_Date()),  //Natural: ASSIGN #IN-LAST-PAYMENT-DATE := VAL ( #WDTE6 )
                pnd_Wdte6.val());
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Populate_Out_Aiaa0380() throws Exception                                                                                                             //Natural: POPULATE-OUT-AIAA0380
    {
        if (BLNatReinput.isReinput()) return;

        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code().setValue(pdaAiaa025.getPnd_Aian025_Linkage_Pnd_Out_Aian025_Return_Code());                       //Natural: ASSIGN #OUT-AIAN035-RETURN-CODE := #OUT-AIAN025-RETURN-CODE
        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Guaranteed_Py().getValue("*").setValue(pdaAiaa025.getPnd_Aian025_Linkage_Pnd_Out_Yr_Std_Or_Grd_Guaranteed_Py().getValue("*")); //Natural: ASSIGN #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-GUARANTEED-PY ( * ) := #AIAN025-LINKAGE.#OUT-YR-STD-OR-GRD-GUARANTEED-PY ( * )
        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Dividend_Pay().getValue("*").setValue(pdaAiaa025.getPnd_Aian025_Linkage_Pnd_Out_Yr_Std_Or_Grd_Dividend_Pay().getValue("*")); //Natural: ASSIGN #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-DIVIDEND-PAY ( * ) := #AIAN025-LINKAGE.#OUT-YR-STD-OR-GRD-DIVIDEND-PAY ( * )
        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Final_Guar_Py().getValue("*").setValue(pdaAiaa025.getPnd_Aian025_Linkage_Pnd_Out_Yr_Std_Or_Grd_Final_Guar_Py().getValue("*")); //Natural: ASSIGN #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-FINAL-GUAR-PY ( * ) := #AIAN025-LINKAGE.#OUT-YR-STD-OR-GRD-FINAL-GUAR-PY ( * )
        pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Final_Div_Pay().getValue("*").setValue(pdaAiaa025.getPnd_Aian025_Linkage_Pnd_Out_Yr_Std_Or_Grd_Final_Div_Pay().getValue("*")); //Natural: ASSIGN #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-FINAL-DIV-PAY ( * ) := #AIAN025-LINKAGE.#OUT-YR-STD-OR-GRD-FINAL-DIV-PAY ( * )
    }
    private void sub_Display_Parms() throws Exception                                                                                                                     //Natural: DISPLAY-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  EM 052711 - START
        //*  EM - 052711 - END
        getReports().write(0, "=",pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Call_Type(),"=",pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Ia_Fund_Code(),             //Natural: WRITE '=' #AIAN026-CALL-TYPE '=' #AIAN026-IA-FUND-CODE '=' #AIAN026-REVALUATION-METHOD / '=' #AIAN026-IAUV-REQUEST-DATE / '=' #AIAN026-PARTICIPATION-DATE / '=' #AIAN026-RETURN-CODE '=' #AIAN026-INTERIM-ANNUITY-UNIT-VA '=' #AIAN026-IAUV-DATE-RETURNED
            "=",pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Revaluation_Method(),NEWLINE,"=",pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Iauv_Request_Date(),
            NEWLINE,"=",pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Participation_Date(),NEWLINE,"=",pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Return_Code(),
            "=",pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Interim_Annuity_Unit_Va(),"=",pdaAiaa026.getPnd_Aian026_Linkage_Pnd_Aian026_Iauv_Date_Returned());
        if (Global.isEscape()) return;
    }
    private void sub_Display_Aian035_Parms() throws Exception                                                                                                             //Natural: DISPLAY-AIAN035-PARMS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(0, "*** BAD RETURN FROM AIAN035 ***");                                                                                                         //Natural: WRITE '*** BAD RETURN FROM AIAN035 ***'
        if (Global.isEscape()) return;
        getReports().write(0, "PROGRAM ",pnd_Callnat,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code(),"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code_Nbr(),"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Calc_Method(),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Processing_Date(),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Payee(),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Contract_Origin(),  //Natural: WRITE 'PROGRAM ' #CALLNAT '=' #AIAN035-LINKAGE.#OUT-AIAN035-RETURN-CODE '=' #AIAN035-LINKAGE.#OUT-AIAN035-RETURN-CODE-NBR '=' #IN-CALC-METHOD / '=' #AIAN035-LINKAGE.#IN-PROCESSING-DATE / '=' #AIAN035-LINKAGE.#IN-CONTRACT-PAYEE / '=' #AIAN035-LINKAGE.#IN-CONTRACT-ORIGIN ( EM = 99 ) / '=' #AIAN035-LINKAGE.#IN-PAYMENT-METHOD / '=' #AIAN035-LINKAGE.#IN-PAYMENT-OPTION ( EM = 99 ) / '=' #AIAN035-LINKAGE.#IN-PAYMENT-MODE ( EM = 999 ) / '=' #AIAN035-LINKAGE.#IN-ISSUE-DATE ( EM = 99999999 ) / '=' #AIAN035-LINKAGE.#IN-FIRST-ANN-SEX ( EM = 9 ) / '=' #AIAN035-LINKAGE.#IN-FIRST-ANN-BIRTH-DATE ( EM = 99999999 ) / '=' #AIAN035-LINKAGE.#IN-FIRST-ANN-DEATH-DATE ( EM = 999999 ) / '=' #AIAN035-LINKAGE.#IN-SECOND-ANN-SEX ( EM = 9 ) / '=' #AIAN035-LINKAGE.#IN-SECOND-ANN-BIRTH-DATE ( EM = 99999999 ) / '=' #AIAN035-LINKAGE.#IN-SECOND-ANN-DEATH-DATE ( EM = 999999 ) / '=' #AIAN035-LINKAGE.#IN-ACT-STD-FNL-PP-PAY-DTE ( EM = 999999 ) / '=' #AIAN035-LINKAGE.#IN-ACT-STD-FNL-PAY-DTE ( EM = 99999999 ) / '=' #AIAN035-LINKAGE.#IN-NEXT-PAYMENT-DATE ( EM = 99999999 )
            new ReportEditMask ("99"),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Method(),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Option(), 
            new ReportEditMask ("99"),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Payment_Mode(), new ReportEditMask ("999"),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Issue_Date(), 
            new ReportEditMask ("99999999"),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_First_Ann_Sex(), new ReportEditMask ("9"),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_First_Ann_Birth_Date(), 
            new ReportEditMask ("99999999"),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_First_Ann_Death_Date(), new ReportEditMask ("999999"),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Second_Ann_Sex(), 
            new ReportEditMask ("9"),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Second_Ann_Birth_Date(), new ReportEditMask ("99999999"),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Second_Ann_Death_Date(), 
            new ReportEditMask ("999999"),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Act_Std_Fnl_Pp_Pay_Dte(), new ReportEditMask ("999999"),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Act_Std_Fnl_Pay_Dte(), 
            new ReportEditMask ("99999999"),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Next_Payment_Date(), new ReportEditMask ("99999999"));
        if (Global.isEscape()) return;
        FOR14:                                                                                                                                                            //Natural: FOR #A 1 #MAX-RATE
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Rate)); pnd_A.nadd(1))
        {
            if (condition(pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Rate_Code().getValue(pnd_A).equals(" ")))                                                             //Natural: IF #AIAN035-LINKAGE.#IN-RATE-CODE ( #A ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Rate_Code().getValue(pnd_A),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Rate_Transfer_Eff_Date().getValue(pnd_A),  //Natural: WRITE '=' #AIAN035-LINKAGE.#IN-RATE-CODE ( #A ) / '=' #AIAN035-LINKAGE.#IN-RATE-TRANSFER-EFF-DATE ( #A ) ( EM = 99999999 ) / '=' #AIAN035-LINKAGE.#IN-GUARANTEED-PAYMENT ( #A ) / '=' #AIAN035-LINKAGE.#IN-DIVIDEND-PAYMENT ( #A ) / '=' #AIAN035-LINKAGE.#IN-FINAL-GUARANTEED-PAYMENT ( #A ) / '=' #AIAN035-LINKAGE.#IN-FINAL-DIVIDEND-PAYMENT ( #A ) /
                new ReportEditMask ("99999999"),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Guaranteed_Payment().getValue(pnd_A),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Dividend_Payment().getValue(pnd_A),
                NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Final_Guaranteed_Payment().getValue(pnd_A),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Final_Dividend_Payment().getValue(pnd_A),
                NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_B.setValue(pnd_A);                                                                                                                                        //Natural: ASSIGN #B := #A
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  030912
        getReports().write(0, "=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_In_Write_Output_Switch(),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Aian035_Return_Code_Nbr(),  //Natural: WRITE '=' #IN-WRITE-OUTPUT-SWITCH / '=' #OUT-AIAN035-RETURN-CODE-NBR ( EM = 999 )
            new ReportEditMask ("999"));
        if (Global.isEscape()) return;
        FOR15:                                                                                                                                                            //Natural: FOR #A 1 #B
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_B)); pnd_A.nadd(1))
        {
            getReports().write(0, "=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Guaranteed_Py().getValue(pnd_A),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Dividend_Pay().getValue(pnd_A), //Natural: WRITE '=' #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-GUARANTEED-PY ( #A ) / '=' #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-DIVIDEND-PAY ( #A ) / '=' #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-FINAL-GUAR-PY ( #A ) / '=' #AIAN035-LINKAGE.#OUT-YR-STD-OR-GRD-FINAL-DIV-PAY ( #A ) / '=' #AIAN035-LINKAGE.#OUT-NXT-YR-GRD-GUAR-PAY ( #A ) / '=' #AIAN035-LINKAGE.#OUT-NXT-YR-GRD-DIVIDEND-PAY ( #A ) /
                NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Final_Guar_Py().getValue(pnd_A),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Yr_Std_Or_Grd_Final_Div_Pay().getValue(pnd_A),
                NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Nxt_Yr_Grd_Guar_Pay().getValue(pnd_A),NEWLINE,"=",pdaAiaa0380.getPnd_Aian035_Linkage_Pnd_Out_Nxt_Yr_Grd_Dividend_Pay().getValue(pnd_A),
                NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Move_Tiaa_Fields1() throws Exception                                                                                                                 //Natural: MOVE-TIAA-FIELDS1
    {
        if (BLNatReinput.isReinput()) return;

        FOR16:                                                                                                                                                            //Natural: FOR #A 1 #MAX-RSLT
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Rslt)); pnd_A.nadd(1))
        {
            pnd_B.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #B
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_B).setValue(ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_A));                            //Natural: ASSIGN ADSA600.#ADI-DTL-FNL-STD-PAY-AMT ( #B ) := ADS-IA-RSLT.ADI-DTL-FNL-STD-PAY-AMT ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_B).setValue(ads_Ia_Rslt_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_A));                        //Natural: ASSIGN ADSA600.#ADI-DTL-FNL-STD-DVDND-AMT ( #B ) := ADS-IA-RSLT.ADI-DTL-FNL-STD-DVDND-AMT ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_B).setValue(ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_A));                            //Natural: ASSIGN ADSA600.#ADI-DTL-FNL-GRD-PAY-AMT ( #B ) := ADS-IA-RSLT.ADI-DTL-FNL-GRD-PAY-AMT ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_B).setValue(ads_Ia_Rslt_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_A));                        //Natural: ASSIGN ADSA600.#ADI-DTL-FNL-GRD-DVDND-AMT ( #B ) := ADS-IA-RSLT.ADI-DTL-FNL-GRD-DVDND-AMT ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Acct_Cd().getValue(pnd_B).setValue(ads_Ia_Rslt_Adi_Dtl_Tiaa_Acct_Cd.getValue(pnd_A));                                  //Natural: ASSIGN ADSA600.#ADI-DTL-TIAA-ACCT-CD ( #B ) := ADS-IA-RSLT.ADI-DTL-TIAA-ACCT-CD ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_B).setValue(ads_Ia_Rslt_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_A));                            //Natural: ASSIGN ADSA600.#ADI-DTL-TIAA-IA-RATE-CD ( #B ) := ADS-IA-RSLT.ADI-DTL-TIAA-IA-RATE-CD ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(pnd_B).setValue(ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Std_Amt.getValue(pnd_A));                            //Natural: ASSIGN ADSA600.#ADI-DTL-TIAA-DA-STD-AMT ( #B ) := ADS-IA-RSLT.ADI-DTL-TIAA-DA-STD-AMT ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_B).setValue(ads_Ia_Rslt_Adi_Dtl_Tiaa_Da_Grd_Amt.getValue(pnd_A));                            //Natural: ASSIGN ADSA600.#ADI-DTL-TIAA-DA-GRD-AMT ( #B ) := ADS-IA-RSLT.ADI-DTL-TIAA-DA-GRD-AMT ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_B).setValue(ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_A));                      //Natural: ASSIGN ADSA600.#ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B ) := ADS-IA-RSLT.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_B).setValue(ads_Ia_Rslt_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_A));                      //Natural: ASSIGN ADSA600.#ADI-DTL-TIAA-GRD-DVDND-AMT ( #B ) := ADS-IA-RSLT.ADI-DTL-TIAA-GRD-DVDND-AMT ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_B).setValue(ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_A));                      //Natural: ASSIGN ADSA600.#ADI-DTL-TIAA-STD-GRNTD-AMT ( #B ) := ADS-IA-RSLT.ADI-DTL-TIAA-STD-GRNTD-AMT ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_B).setValue(ads_Ia_Rslt_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_A));                      //Natural: ASSIGN ADSA600.#ADI-DTL-TIAA-STD-DVDND-AMT ( #B ) := ADS-IA-RSLT.ADI-DTL-TIAA-STD-DVDND-AMT ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Contract_Id().getValue(pnd_B).setValue(ads_Ia_Rslt_Adi_Contract_Id.getValue(pnd_A));                                            //Natural: ASSIGN ADSA600.#ADI-CONTRACT-ID ( #B ) := ADS-IA-RSLT.ADI-CONTRACT-ID ( #A )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Move_Tiaa_Fields2() throws Exception                                                                                                                 //Natural: MOVE-TIAA-FIELDS2
    {
        if (BLNatReinput.isReinput()) return;

        FOR17:                                                                                                                                                            //Natural: FOR #A 1 #MAX-RSLT
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Rslt)); pnd_A.nadd(1))
        {
            pnd_B.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #B
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_B).setValue(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Pay_Amt.getValue(pnd_A));                            //Natural: ASSIGN ADSA600.#ADI-DTL-FNL-STD-PAY-AMT ( #B ) := ADS-IA-RSL2.ADI-DTL-FNL-STD-PAY-AMT ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_B).setValue(ads_Ia_Rsl2_Adi_Dtl_Fnl_Std_Dvdnd_Amt.getValue(pnd_A));                        //Natural: ASSIGN ADSA600.#ADI-DTL-FNL-STD-DVDND-AMT ( #B ) := ADS-IA-RSL2.ADI-DTL-FNL-STD-DVDND-AMT ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Fnl_Grd_Pay_Amt().getValue(pnd_B).setValue(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Pay_Amt.getValue(pnd_A));                            //Natural: ASSIGN ADSA600.#ADI-DTL-FNL-GRD-PAY-AMT ( #B ) := ADS-IA-RSL2.ADI-DTL-FNL-GRD-PAY-AMT ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Fnl_Grd_Dvdnd_Amt().getValue(pnd_B).setValue(ads_Ia_Rsl2_Adi_Dtl_Fnl_Grd_Dvdnd_Amt.getValue(pnd_A));                        //Natural: ASSIGN ADSA600.#ADI-DTL-FNL-GRD-DVDND-AMT ( #B ) := ADS-IA-RSL2.ADI-DTL-FNL-GRD-DVDND-AMT ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Acct_Cd().getValue(pnd_B).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Acct_Cd.getValue(pnd_A));                                  //Natural: ASSIGN ADSA600.#ADI-DTL-TIAA-ACCT-CD ( #B ) := ADS-IA-RSL2.ADI-DTL-TIAA-ACCT-CD ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_B).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Ia_Rate_Cd.getValue(pnd_A));                            //Natural: ASSIGN ADSA600.#ADI-DTL-TIAA-IA-RATE-CD ( #B ) := ADS-IA-RSL2.ADI-DTL-TIAA-IA-RATE-CD ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(pnd_B).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Std_Amt.getValue(pnd_A));                            //Natural: ASSIGN ADSA600.#ADI-DTL-TIAA-DA-STD-AMT ( #B ) := ADS-IA-RSL2.ADI-DTL-TIAA-DA-STD-AMT ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_B).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Da_Grd_Amt.getValue(pnd_A));                            //Natural: ASSIGN ADSA600.#ADI-DTL-TIAA-DA-GRD-AMT ( #B ) := ADS-IA-RSL2.ADI-DTL-TIAA-DA-GRD-AMT ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_B).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Grntd_Amt.getValue(pnd_A));                      //Natural: ASSIGN ADSA600.#ADI-DTL-TIAA-GRD-GRNTD-AMT ( #B ) := ADS-IA-RSL2.ADI-DTL-TIAA-GRD-GRNTD-AMT ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_B).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt.getValue(pnd_A));                      //Natural: ASSIGN ADSA600.#ADI-DTL-TIAA-GRD-DVDND-AMT ( #B ) := ADS-IA-RSL2.ADI-DTL-TIAA-GRD-DVDND-AMT ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_B).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Grntd_Amt.getValue(pnd_A));                      //Natural: ASSIGN ADSA600.#ADI-DTL-TIAA-STD-GRNTD-AMT ( #B ) := ADS-IA-RSL2.ADI-DTL-TIAA-STD-GRNTD-AMT ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_B).setValue(ads_Ia_Rsl2_Adi_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(pnd_A));                      //Natural: ASSIGN ADSA600.#ADI-DTL-TIAA-STD-DVDND-AMT ( #B ) := ADS-IA-RSL2.ADI-DTL-TIAA-STD-DVDND-AMT ( #A )
            pdaAdsa600.getAdsa600_Pnd_Adi_Contract_Id().getValue(pnd_B).setValue(ads_Ia_Rsl2_Adi_Contract_Id.getValue(pnd_A));                                            //Natural: ASSIGN ADSA600.#ADI-CONTRACT-ID ( #B ) := ADS-IA-RSL2.ADI-CONTRACT-ID ( #A )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //
}
