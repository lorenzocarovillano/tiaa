/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:54:33 PM
**        * FROM NATURAL SUBPROGRAM : Adsn555
************************************************************
**        * FILE NAME            : Adsn555.java
**        * CLASS NAME           : Adsn555
**        * INSTANCE NAME        : Adsn555
************************************************************
************************************************************************
* PROGRAM  : ADSN555
* SYSTEM   : ADAS - ANNUITIAZTION SUNGUARD
* TITLE    : BENEFICIARY FILE CALL.
* FUNCTION : ACCESSES BENEFICIARY FILE. TRANSLATES THE BENEFICIARY
*          : ALLOCATION INTO WORDS.
* WRITTEN  : JUNE, 2006
* AUTHOR   : COPIED FROM NAZN555 AND MODIFIED BY MARNA NACHBER
*
* 04/12/07 O. SOTTO CHECK FOR BENEFICIARY NAME AND ESCAPE THE LOOP IF
*                   SPACES. SC 041207.
* 05/08/07 O. SOTTO RESET 'primary' DESIGNATION AREA TO PREVENT
*                   GARBAGE DATA FROM BEING INCLUDED. SC 050807.
* 06/18/07 O. SOTTO FIX TO IM425595 (SPECIAL TEXT DON't show if
*                   NO BENEFICIARY NAME IS ENTERED). SC 061807.
* 10/22/12 O. SOTTO IF CHILD PROVISION IS Y AND THERE IS NO SPECIAL
*                   TEXT, ASSIGN THE STANDARD CHILD PROVISION TEXT.
*                   ALSO TRANSLATE THE SPECIAL TEXT TO LOWER CASE.
*                   SC 102212.
* 11/27/12 O. SOTTO ACCOUNT FOR THE CONDITION WHERE SPECIAL TEXT IS
*                   ENTERED FOR SOME AND NOT FOR SOME BENEFICIARIES.
*                   SC 112712.
* 05/09/14 O. SOTTO FIX TO PRODUCTION PROBLEM. CHANGE MARKED 050914.
* 02/21/17 R. CARREON PIN EXPANSION 02212017
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn555 extends BLNatBase
{
    // Data Areas
    private LdaAdsl555 ldaAdsl555;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;

    private DbsGroup beneficiary;
    private DbsField beneficiary_Designation_Text;

    private DbsGroup beneficiary__R_Field_1;
    private DbsField beneficiary_Designation_Txt;
    private DbsField beneficiary_Designation_Txt_1;
    private DbsField beneficiary_Designation_Text_Cont;

    private DbsGroup primary;
    private DbsField primary_Bene_Name;
    private DbsField primary_Allocation_Pct;

    private DbsGroup contingent;
    private DbsField contingent_Cont_Name;
    private DbsField contingent_Cont_Alloc_Pct;
    private DbsField pnd_Request_Id;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Z;
    private DbsField pnd_S;
    private DbsField pnd_Desc;
    private DbsField pnd_Desc1;
    private DbsField pnd_Desc2;
    private DbsField pnd_Name;
    private DbsField pnd_W_Desig_Text;

    private DbsGroup pnd_W_Desig_Text__R_Field_2;
    private DbsField pnd_W_Desig_Text_Pnd_W_Desig;

    private DbsGroup pnd_W_Desig_Text__R_Field_3;
    private DbsField pnd_W_Desig_Text_Pnd_W_Desig_R;
    private DbsField pnd_A;
    private DbsField pnd_B;
    private DbsField pnd_C;
    private DbsField pnd_D;
    private DbsField pnd_E;
    private DbsField pnd_F;
    private DbsField pnd_Result;
    private DbsField pnd_Nbr1;

    private DbsGroup pnd_Nbr1__R_Field_4;
    private DbsField pnd_Nbr1_Pnd_Nbr1_1;
    private DbsField pnd_Nbr1_Pnd_Nbr1_2;
    private DbsField pnd_Nbr2;

    private DbsGroup pnd_Nbr2__R_Field_5;
    private DbsField pnd_Nbr2_Pnd_Nbr2_1;
    private DbsField pnd_Nbr2_Pnd_Nbr2_2;
    private DbsField pnd_Super_3;

    private DbsGroup pnd_Super_3__R_Field_6;
    private DbsField pnd_Super_3_Pnd_Rec_Typ;
    private DbsField pnd_Super_3_Pnd_Rqst_Id;
    private DbsField pnd_Wrd1;
    private DbsField pnd_Wrd1_1;
    private DbsField pnd_Wrd1_2;
    private DbsField pnd_Wrd2;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl555 = new LdaAdsl555();
        registerRecord(ldaAdsl555);
        registerRecord(ldaAdsl555.getVw_cis_Bene_File_01_View());

        // parameters
        parameters = new DbsRecord();

        beneficiary = parameters.newGroupInRecord("beneficiary", "BENEFICIARY");
        beneficiary.setParameterOption(ParameterOption.ByReference);
        beneficiary_Designation_Text = beneficiary.newFieldArrayInGroup("beneficiary_Designation_Text", "DESIGNATION-TEXT", FieldType.STRING, 72, new 
            DbsArrayController(1, 60));

        beneficiary__R_Field_1 = beneficiary.newGroupInGroup("beneficiary__R_Field_1", "REDEFINE", beneficiary_Designation_Text);
        beneficiary_Designation_Txt = beneficiary__R_Field_1.newFieldArrayInGroup("beneficiary_Designation_Txt", "DESIGNATION-TXT", FieldType.STRING, 
            72, new DbsArrayController(1, 4, 1, 13));
        beneficiary_Designation_Txt_1 = beneficiary__R_Field_1.newFieldArrayInGroup("beneficiary_Designation_Txt_1", "DESIGNATION-TXT-1", FieldType.STRING, 
            72, new DbsArrayController(1, 8));
        beneficiary_Designation_Text_Cont = beneficiary.newFieldArrayInGroup("beneficiary_Designation_Text_Cont", "DESIGNATION-TEXT-CONT", FieldType.STRING, 
            72, new DbsArrayController(1, 60));

        primary = parameters.newGroupInRecord("primary", "PRIMARY");
        primary.setParameterOption(ParameterOption.ByReference);
        primary_Bene_Name = primary.newFieldArrayInGroup("primary_Bene_Name", "BENE-NAME", FieldType.STRING, 35, new DbsArrayController(1, 20));
        primary_Allocation_Pct = primary.newFieldArrayInGroup("primary_Allocation_Pct", "ALLOCATION-PCT", FieldType.STRING, 30, new DbsArrayController(1, 
            20));

        contingent = parameters.newGroupInRecord("contingent", "CONTINGENT");
        contingent.setParameterOption(ParameterOption.ByReference);
        contingent_Cont_Name = contingent.newFieldArrayInGroup("contingent_Cont_Name", "CONT-NAME", FieldType.STRING, 35, new DbsArrayController(1, 20));
        contingent_Cont_Alloc_Pct = contingent.newFieldArrayInGroup("contingent_Cont_Alloc_Pct", "CONT-ALLOC-PCT", FieldType.STRING, 30, new DbsArrayController(1, 
            20));
        pnd_Request_Id = parameters.newFieldInRecord("pnd_Request_Id", "#REQUEST-ID", FieldType.STRING, 35);
        pnd_Request_Id.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 2);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.PACKED_DECIMAL, 2);
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.PACKED_DECIMAL, 2);
        pnd_S = localVariables.newFieldInRecord("pnd_S", "#S", FieldType.STRING, 1);
        pnd_Desc = localVariables.newFieldInRecord("pnd_Desc", "#DESC", FieldType.STRING, 30);
        pnd_Desc1 = localVariables.newFieldInRecord("pnd_Desc1", "#DESC1", FieldType.STRING, 30);
        pnd_Desc2 = localVariables.newFieldInRecord("pnd_Desc2", "#DESC2", FieldType.STRING, 40);
        pnd_Name = localVariables.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 50);
        pnd_W_Desig_Text = localVariables.newFieldInRecord("pnd_W_Desig_Text", "#W-DESIG-TEXT", FieldType.STRING, 216);

        pnd_W_Desig_Text__R_Field_2 = localVariables.newGroupInRecord("pnd_W_Desig_Text__R_Field_2", "REDEFINE", pnd_W_Desig_Text);
        pnd_W_Desig_Text_Pnd_W_Desig = pnd_W_Desig_Text__R_Field_2.newFieldArrayInGroup("pnd_W_Desig_Text_Pnd_W_Desig", "#W-DESIG", FieldType.STRING, 
            72, new DbsArrayController(1, 3));

        pnd_W_Desig_Text__R_Field_3 = localVariables.newGroupInRecord("pnd_W_Desig_Text__R_Field_3", "REDEFINE", pnd_W_Desig_Text);
        pnd_W_Desig_Text_Pnd_W_Desig_R = pnd_W_Desig_Text__R_Field_3.newFieldArrayInGroup("pnd_W_Desig_Text_Pnd_W_Desig_R", "#W-DESIG-R", FieldType.STRING, 
            1, new DbsArrayController(1, 216));
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.PACKED_DECIMAL, 3);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.PACKED_DECIMAL, 3);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.PACKED_DECIMAL, 3);
        pnd_E = localVariables.newFieldInRecord("pnd_E", "#E", FieldType.PACKED_DECIMAL, 3);
        pnd_F = localVariables.newFieldInRecord("pnd_F", "#F", FieldType.NUMERIC, 7, 5);
        pnd_Result = localVariables.newFieldInRecord("pnd_Result", "#RESULT", FieldType.NUMERIC, 5, 2);
        pnd_Nbr1 = localVariables.newFieldInRecord("pnd_Nbr1", "#NBR1", FieldType.NUMERIC, 3);

        pnd_Nbr1__R_Field_4 = localVariables.newGroupInRecord("pnd_Nbr1__R_Field_4", "REDEFINE", pnd_Nbr1);
        pnd_Nbr1_Pnd_Nbr1_1 = pnd_Nbr1__R_Field_4.newFieldInGroup("pnd_Nbr1_Pnd_Nbr1_1", "#NBR1-1", FieldType.NUMERIC, 2);
        pnd_Nbr1_Pnd_Nbr1_2 = pnd_Nbr1__R_Field_4.newFieldInGroup("pnd_Nbr1_Pnd_Nbr1_2", "#NBR1-2", FieldType.NUMERIC, 1);
        pnd_Nbr2 = localVariables.newFieldInRecord("pnd_Nbr2", "#NBR2", FieldType.NUMERIC, 3);

        pnd_Nbr2__R_Field_5 = localVariables.newGroupInRecord("pnd_Nbr2__R_Field_5", "REDEFINE", pnd_Nbr2);
        pnd_Nbr2_Pnd_Nbr2_1 = pnd_Nbr2__R_Field_5.newFieldInGroup("pnd_Nbr2_Pnd_Nbr2_1", "#NBR2-1", FieldType.NUMERIC, 2);
        pnd_Nbr2_Pnd_Nbr2_2 = pnd_Nbr2__R_Field_5.newFieldInGroup("pnd_Nbr2_Pnd_Nbr2_2", "#NBR2-2", FieldType.NUMERIC, 1);
        pnd_Super_3 = localVariables.newFieldInRecord("pnd_Super_3", "#SUPER-3", FieldType.STRING, 36);

        pnd_Super_3__R_Field_6 = localVariables.newGroupInRecord("pnd_Super_3__R_Field_6", "REDEFINE", pnd_Super_3);
        pnd_Super_3_Pnd_Rec_Typ = pnd_Super_3__R_Field_6.newFieldInGroup("pnd_Super_3_Pnd_Rec_Typ", "#REC-TYP", FieldType.STRING, 1);
        pnd_Super_3_Pnd_Rqst_Id = pnd_Super_3__R_Field_6.newFieldInGroup("pnd_Super_3_Pnd_Rqst_Id", "#RQST-ID", FieldType.STRING, 35);
        pnd_Wrd1 = localVariables.newFieldArrayInRecord("pnd_Wrd1", "#WRD1", FieldType.STRING, 9, new DbsArrayController(1, 20));
        pnd_Wrd1_1 = localVariables.newFieldArrayInRecord("pnd_Wrd1_1", "#WRD1-1", FieldType.STRING, 7, new DbsArrayController(1, 9));
        pnd_Wrd1_2 = localVariables.newFieldArrayInRecord("pnd_Wrd1_2", "#WRD1-2", FieldType.STRING, 5, new DbsArrayController(1, 9));
        pnd_Wrd2 = localVariables.newFieldArrayInRecord("pnd_Wrd2", "#WRD2", FieldType.STRING, 11, new DbsArrayController(1, 20));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAdsl555.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Wrd1.getValue(1).setInitialValue("One");
        pnd_Wrd1.getValue(2).setInitialValue("Two");
        pnd_Wrd1.getValue(3).setInitialValue("Three");
        pnd_Wrd1.getValue(4).setInitialValue("Four");
        pnd_Wrd1.getValue(5).setInitialValue("Five");
        pnd_Wrd1.getValue(6).setInitialValue("Six");
        pnd_Wrd1.getValue(7).setInitialValue("Seven");
        pnd_Wrd1.getValue(8).setInitialValue("Eight");
        pnd_Wrd1.getValue(9).setInitialValue("Nine");
        pnd_Wrd1.getValue(10).setInitialValue("Ten");
        pnd_Wrd1.getValue(11).setInitialValue("Eleven");
        pnd_Wrd1.getValue(12).setInitialValue("Twelve");
        pnd_Wrd1.getValue(13).setInitialValue("Thirteen");
        pnd_Wrd1.getValue(14).setInitialValue("Fourteen");
        pnd_Wrd1.getValue(15).setInitialValue("Fifteen");
        pnd_Wrd1.getValue(16).setInitialValue("Sixteen");
        pnd_Wrd1.getValue(17).setInitialValue("Seventeen");
        pnd_Wrd1.getValue(18).setInitialValue("Eighteen");
        pnd_Wrd1.getValue(19).setInitialValue("Nineteen");
        pnd_Wrd1.getValue(20).setInitialValue("Twenty");
        pnd_Wrd1_1.getValue(1).setInitialValue(" ");
        pnd_Wrd1_1.getValue(2).setInitialValue("Twenty");
        pnd_Wrd1_1.getValue(3).setInitialValue("Thirty");
        pnd_Wrd1_1.getValue(4).setInitialValue("Forty");
        pnd_Wrd1_1.getValue(5).setInitialValue("Fifty");
        pnd_Wrd1_1.getValue(6).setInitialValue("Sixty");
        pnd_Wrd1_1.getValue(7).setInitialValue("Seventy");
        pnd_Wrd1_1.getValue(8).setInitialValue("Eighty");
        pnd_Wrd1_1.getValue(9).setInitialValue("Ninety");
        pnd_Wrd1_2.getValue(1).setInitialValue("one");
        pnd_Wrd1_2.getValue(2).setInitialValue("two");
        pnd_Wrd1_2.getValue(3).setInitialValue("three");
        pnd_Wrd1_2.getValue(4).setInitialValue("four");
        pnd_Wrd1_2.getValue(5).setInitialValue("five");
        pnd_Wrd1_2.getValue(6).setInitialValue("six");
        pnd_Wrd1_2.getValue(7).setInitialValue("seven");
        pnd_Wrd1_2.getValue(8).setInitialValue("eight");
        pnd_Wrd1_2.getValue(9).setInitialValue("nine");
        pnd_Wrd2.getValue(1).setInitialValue(" ");
        pnd_Wrd2.getValue(2).setInitialValue("Half");
        pnd_Wrd2.getValue(3).setInitialValue("Third");
        pnd_Wrd2.getValue(4).setInitialValue("Fourth");
        pnd_Wrd2.getValue(5).setInitialValue("Fifth");
        pnd_Wrd2.getValue(6).setInitialValue("Sixth");
        pnd_Wrd2.getValue(7).setInitialValue("Seventh");
        pnd_Wrd2.getValue(8).setInitialValue("Eighth");
        pnd_Wrd2.getValue(9).setInitialValue("Ninth");
        pnd_Wrd2.getValue(10).setInitialValue("Tenth");
        pnd_Wrd2.getValue(11).setInitialValue("Eleventh");
        pnd_Wrd2.getValue(12).setInitialValue("Twelfth");
        pnd_Wrd2.getValue(13).setInitialValue("Thirteenth");
        pnd_Wrd2.getValue(14).setInitialValue("Fourteenth");
        pnd_Wrd2.getValue(15).setInitialValue("Fifteenth");
        pnd_Wrd2.getValue(16).setInitialValue("Sixteenth");
        pnd_Wrd2.getValue(17).setInitialValue("Seventeenth");
        pnd_Wrd2.getValue(18).setInitialValue("Eighteenth");
        pnd_Wrd2.getValue(19).setInitialValue("Nineteenth");
        pnd_Wrd2.getValue(20).setInitialValue("Twentieth");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn555() throws Exception
    {
        super("Adsn555");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        getReports().write(0, "EXECUTING",Global.getPROGRAM());                                                                                                           //Natural: WRITE 'EXECUTING' *PROGRAM
        if (Global.isEscape()) return;
        //*  050807
        beneficiary.reset();                                                                                                                                              //Natural: RESET BENEFICIARY CONTINGENT PRIMARY
        contingent.reset();
        primary.reset();
        pnd_Super_3_Pnd_Rec_Typ.setValue(1);                                                                                                                              //Natural: MOVE 1 TO #REC-TYP
        pnd_Super_3_Pnd_Rqst_Id.setValue(pnd_Request_Id);                                                                                                                 //Natural: MOVE #REQUEST-ID TO #RQST-ID
        ldaAdsl555.getVw_cis_Bene_File_01_View().startDatabaseFind                                                                                                        //Natural: FIND CIS-BENE-FILE-01-VIEW WITH CIS-BEN-SUPER-3 = #SUPER-3
        (
        "FIND01",
        new Wc[] { new Wc("CIS_BEN_SUPER_3", "=", pnd_Super_3, WcType.WITH) }
        );
        FIND01:
        while (condition(ldaAdsl555.getVw_cis_Bene_File_01_View().readNextRow("FIND01", true)))
        {
            ldaAdsl555.getVw_cis_Bene_File_01_View().setIfNotFoundControlFlag(false);
            if (condition(ldaAdsl555.getVw_cis_Bene_File_01_View().getAstCOUNTER().equals(0)))                                                                            //Natural: IF NO RECORD FOUND
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM IMMEDIATE
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(ldaAdsl555.getCis_Bene_File_01_View_Cis_Bene_Std_Free().equals("Y")))                                                                           //Natural: IF CIS-BENE-STD-FREE = 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM GET-MODE-OF-SETTLEMENT-INFO
                sub_Get_Mode_Of_Settlement_Info();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM GET-PRIMARY-BENE-INFO
                sub_Get_Primary_Bene_Info();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM GET-CONTGNT-BENE-INFO
                sub_Get_Contgnt_Bene_Info();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-MODE-OF-SETTLEMENT-INFO
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PRIMARY-BENE-INFO
        //* ********************************
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTGNT-BENE-INFO
        //* ********************************
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRANSLATE-NUMBERS-TO-WORDS
        //* ************************************************102212*****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRANSLATE-INTO-LOWER-CASE
    }
    private void sub_Get_Mode_Of_Settlement_Info() throws Exception                                                                                                       //Natural: GET-MODE-OF-SETTLEMENT-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        //*  061807
        //*  102212
        pnd_Z.reset();                                                                                                                                                    //Natural: RESET #Z #W-DESIG-TEXT #C #D
        pnd_W_Desig_Text.reset();
        pnd_C.reset();
        pnd_D.reset();
        FOR01:                                                                                                                                                            //Natural: FOR #X 1 20
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(20)); pnd_X.nadd(1))
        {
            FOR02:                                                                                                                                                        //Natural: FOR #Y 1 3
            for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(3)); pnd_Y.nadd(1))
            {
                pnd_Z.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #Z
                //*  102212 START
                if (condition(pnd_Y.equals(1)))                                                                                                                           //Natural: IF #Y = 1
                {
                    //*  SAVE THE 1ST DESIGNATION OCCURRENCE
                    pnd_D.reset();                                                                                                                                        //Natural: RESET #D
                    pnd_C.setValue(pnd_Z);                                                                                                                                //Natural: ASSIGN #C := #Z
                    //*  102212 END
                }                                                                                                                                                         //Natural: END-IF
                //*  061807
                if (condition(ldaAdsl555.getCis_Bene_File_01_View_Cis_Prmry_Bene_Spcl_Txt().getValue(pnd_X,pnd_Y).notEquals(" ")))                                        //Natural: IF CIS-PRMRY-BENE-SPCL-TXT ( #X,#Y ) NE ' '
                {
                    beneficiary_Designation_Text.getValue(pnd_Z).setValue(ldaAdsl555.getCis_Bene_File_01_View_Cis_Prmry_Bene_Spcl_Txt().getValue(pnd_X,                   //Natural: MOVE CIS-PRMRY-BENE-SPCL-TXT ( #X,#Y ) TO DESIGNATION-TEXT ( #Z )
                        pnd_Y));
                    //*  102212 START
                    pnd_D.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #D
                    pnd_W_Desig_Text_Pnd_W_Desig.getValue(pnd_D).setValue(beneficiary_Designation_Text.getValue(pnd_Z));                                                  //Natural: ASSIGN #W-DESIG ( #D ) := DESIGNATION-TEXT ( #Z )
                    //*  102212 END
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  102212 START
            if (condition(pnd_W_Desig_Text.notEquals(" ")))                                                                                                               //Natural: IF #W-DESIG-TEXT NE ' '
            {
                                                                                                                                                                          //Natural: PERFORM TRANSLATE-INTO-LOWER-CASE
                sub_Translate_Into_Lower_Case();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR03:                                                                                                                                                    //Natural: FOR #D 1 3
                for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(3)); pnd_D.nadd(1))
                {
                    if (condition(pnd_W_Desig_Text_Pnd_W_Desig.getValue(pnd_D).equals(" ")))                                                                              //Natural: IF #W-DESIG ( #D ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    beneficiary_Designation_Text.getValue(pnd_C).setValue(pnd_W_Desig_Text_Pnd_W_Desig.getValue(pnd_D));                                                  //Natural: ASSIGN DESIGNATION-TEXT ( #C ) := #W-DESIG ( #D )
                    pnd_C.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #C
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_W_Desig_Text.reset();                                                                                                                                     //Natural: RESET #W-DESIG-TEXT
            //*  102212 END
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  061807 START
        //*  102212
        pnd_Z.reset();                                                                                                                                                    //Natural: RESET #Z #W-DESIG-TEXT #C #D
        pnd_W_Desig_Text.reset();
        pnd_C.reset();
        pnd_D.reset();
        FOR04:                                                                                                                                                            //Natural: FOR #X 1 20
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(20)); pnd_X.nadd(1))
        {
            FOR05:                                                                                                                                                        //Natural: FOR #Y 1 3
            for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(3)); pnd_Y.nadd(1))
            {
                pnd_Z.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #Z
                //*  102212 START
                if (condition(pnd_Y.equals(1)))                                                                                                                           //Natural: IF #Y = 1
                {
                    //*  SAVE THE 1ST DESIGNATION OCCURRENCE
                    pnd_D.reset();                                                                                                                                        //Natural: RESET #D
                    pnd_C.setValue(pnd_Z);                                                                                                                                //Natural: ASSIGN #C := #Z
                    //*  102212 END
                }                                                                                                                                                         //Natural: END-IF
                //*  061807
                if (condition(ldaAdsl555.getCis_Bene_File_01_View_Cis_Cntgnt_Bene_Spcl_Txt().getValue(pnd_X,pnd_Y).notEquals(" ")))                                       //Natural: IF CIS-CNTGNT-BENE-SPCL-TXT ( #X,#Y ) NE ' '
                {
                    beneficiary_Designation_Text_Cont.getValue(pnd_Z).setValue(ldaAdsl555.getCis_Bene_File_01_View_Cis_Cntgnt_Bene_Spcl_Txt().getValue(pnd_X,             //Natural: MOVE CIS-CNTGNT-BENE-SPCL-TXT ( #X,#Y ) TO DESIGNATION-TEXT-CONT ( #Z )
                        pnd_Y));
                    //*  102212 START
                    pnd_D.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #D
                    pnd_W_Desig_Text_Pnd_W_Desig.getValue(pnd_D).setValue(beneficiary_Designation_Text_Cont.getValue(pnd_Z));                                             //Natural: ASSIGN #W-DESIG ( #D ) := DESIGNATION-TEXT-CONT ( #Z )
                    //*  102212 END
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  102212 START
            if (condition(pnd_W_Desig_Text.notEquals(" ")))                                                                                                               //Natural: IF #W-DESIG-TEXT NE ' '
            {
                                                                                                                                                                          //Natural: PERFORM TRANSLATE-INTO-LOWER-CASE
                sub_Translate_Into_Lower_Case();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR06:                                                                                                                                                    //Natural: FOR #D 1 3
                for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(3)); pnd_D.nadd(1))
                {
                    if (condition(pnd_W_Desig_Text_Pnd_W_Desig.getValue(pnd_D).equals(" ")))                                                                              //Natural: IF #W-DESIG ( #D ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    beneficiary_Designation_Text_Cont.getValue(pnd_C).setValue(pnd_W_Desig_Text_Pnd_W_Desig.getValue(pnd_D));                                             //Natural: ASSIGN DESIGNATION-TEXT-CONT ( #C ) := #W-DESIG ( #D )
                    pnd_C.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #C
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_W_Desig_Text.reset();                                                                                                                                     //Natural: RESET #W-DESIG-TEXT
            //*  102212 END
            //*  061807 END
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Get_Primary_Bene_Info() throws Exception                                                                                                             //Natural: GET-PRIMARY-BENE-INFO
    {
        if (BLNatReinput.isReinput()) return;

        FOR07:                                                                                                                                                            //Natural: FOR #X 1 TO 20
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(20)); pnd_X.nadd(1))
        {
            //*  041207
            //*  061807
            if (condition(ldaAdsl555.getCis_Bene_File_01_View_Cis_Prmry_Bene_Name().getValue(pnd_X).equals(" ") && ldaAdsl555.getCis_Bene_File_01_View_Cis_Prmry_Bene_Spcl_Txt().getValue(pnd_X, //Natural: IF CIS-PRMRY-BENE-NAME ( #X ) = ' ' AND CIS-PRMRY-BENE-SPCL-TXT ( #X,1 ) = ' '
                1).equals(" ")))
            {
                //*  041207
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  041207
            }                                                                                                                                                             //Natural: END-IF
            //*  061807
            if (condition(ldaAdsl555.getCis_Bene_File_01_View_Cis_Prmry_Bene_Name().getValue(pnd_X).notEquals(" ")))                                                      //Natural: IF CIS-PRMRY-BENE-NAME ( #X ) NE ' '
            {
                pnd_Name.setValue(ldaAdsl555.getCis_Bene_File_01_View_Cis_Prmry_Bene_Name().getValue(pnd_X));                                                             //Natural: MOVE CIS-PRMRY-BENE-NAME ( #X ) TO #NAME
                DbsUtil.callnat(Adsn553.class , getCurrentProcessState(), pnd_Name);                                                                                      //Natural: CALLNAT 'ADSN553' #NAME
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                primary_Bene_Name.getValue(pnd_X).setValue(pnd_Name);                                                                                                     //Natural: MOVE #NAME TO BENE-NAME ( #X )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl555.getCis_Bene_File_01_View_Cis_Prmry_Bene_Dnmntr_Nbr().getValue(pnd_X).equals(100)))                                                   //Natural: IF CIS-PRMRY-BENE-DNMNTR-NBR ( #X ) = 100
            {
                primary_Allocation_Pct.getValue(pnd_X).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAdsl555.getCis_Bene_File_01_View_Cis_Prmry_Bene_Nmrtr_Nbr().getValue(pnd_X),  //Natural: COMPRESS CIS-PRMRY-BENE-NMRTR-NBR ( #X ) '%' INTO ALLOCATION-PCT ( #X ) LEAVING NO
                    "%"));
                //*  050914 START
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaAdsl555.getCis_Bene_File_01_View_Cis_Prmry_Bene_Dnmntr_Nbr().getValue(pnd_X).greater(100)))                                              //Natural: IF CIS-PRMRY-BENE-DNMNTR-NBR ( #X ) GT 100
                {
                    pnd_F.compute(new ComputeParameters(true, pnd_F), ldaAdsl555.getCis_Bene_File_01_View_Cis_Prmry_Bene_Nmrtr_Nbr().getValue(pnd_X).divide(ldaAdsl555.getCis_Bene_File_01_View_Cis_Prmry_Bene_Dnmntr_Nbr().getValue(pnd_X))); //Natural: COMPUTE ROUNDED #F = CIS-PRMRY-BENE-NMRTR-NBR ( #X ) / CIS-PRMRY-BENE-DNMNTR-NBR ( #X )
                    pnd_Result.compute(new ComputeParameters(true, pnd_Result), pnd_F.multiply(100));                                                                     //Natural: COMPUTE ROUNDED #RESULT = #F * 100
                    primary_Allocation_Pct.getValue(pnd_X).setValueEdited(pnd_Result,new ReportEditMask("ZZ9.99"));                                                       //Natural: MOVE EDITED #RESULT ( EM = ZZ9.99 ) TO ALLOCATION-PCT ( #X )
                    primary_Allocation_Pct.getValue(pnd_X).setValue(primary_Allocation_Pct.getValue(pnd_X), MoveOption.LeftJustified);                                    //Natural: MOVE LEFT ALLOCATION-PCT ( #X ) TO ALLOCATION-PCT ( #X )
                    primary_Allocation_Pct.getValue(pnd_X).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, primary_Allocation_Pct.getValue(pnd_X),               //Natural: COMPRESS ALLOCATION-PCT ( #X ) '%' INTO ALLOCATION-PCT ( #X ) LEAVING NO
                        "%"));
                    //*  050914 END
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Nbr1.setValue(ldaAdsl555.getCis_Bene_File_01_View_Cis_Prmry_Bene_Nmrtr_Nbr().getValue(pnd_X));                                                    //Natural: MOVE CIS-PRMRY-BENE-NMRTR-NBR ( #X ) TO #NBR1
                    pnd_Nbr2.setValue(ldaAdsl555.getCis_Bene_File_01_View_Cis_Prmry_Bene_Dnmntr_Nbr().getValue(pnd_X));                                                   //Natural: MOVE CIS-PRMRY-BENE-DNMNTR-NBR ( #X ) TO #NBR2
                                                                                                                                                                          //Natural: PERFORM TRANSLATE-NUMBERS-TO-WORDS
                    sub_Translate_Numbers_To_Words();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    primary_Allocation_Pct.getValue(pnd_X).setValue(pnd_Desc2);                                                                                           //Natural: MOVE #DESC2 TO ALLOCATION-PCT ( #X )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  102212
            pnd_W_Desig_Text.reset();                                                                                                                                     //Natural: RESET #W-DESIG-TEXT #C #D
            pnd_C.reset();
            pnd_D.reset();
            if (condition(ldaAdsl555.getCis_Bene_File_01_View_Cis_Prmry_Bene_Spcl_Txt().getValue(pnd_X,1).notEquals(" ")))                                                //Natural: IF CIS-PRMRY-BENE-SPCL-TXT ( #X,1 ) NE ' '
            {
                pnd_Z.compute(new ComputeParameters(false, pnd_Z), (pnd_X.subtract(1)).multiply(3));                                                                      //Natural: COMPUTE #Z = ( #X -1 ) * 3
                FOR08:                                                                                                                                                    //Natural: FOR #Y 1 3
                for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(3)); pnd_Y.nadd(1))
                {
                    pnd_Z.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #Z
                    //*  102212 START
                    if (condition(pnd_Y.equals(1)))                                                                                                                       //Natural: IF #Y = 1
                    {
                        //*  SAVE THE 1ST DESIGNATION OCCURRENCE
                        pnd_D.reset();                                                                                                                                    //Natural: RESET #D
                        pnd_C.setValue(pnd_Z);                                                                                                                            //Natural: ASSIGN #C := #Z
                        //*  102212 END
                    }                                                                                                                                                     //Natural: END-IF
                    beneficiary_Designation_Text.getValue(pnd_Z).setValue(ldaAdsl555.getCis_Bene_File_01_View_Cis_Prmry_Bene_Spcl_Txt().getValue(pnd_X,                   //Natural: MOVE CIS-PRMRY-BENE-SPCL-TXT ( #X,#Y ) TO DESIGNATION-TEXT ( #Z )
                        pnd_Y));
                    //*  102212 START
                    pnd_D.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #D
                    pnd_W_Desig_Text_Pnd_W_Desig.getValue(pnd_D).setValue(beneficiary_Designation_Text.getValue(pnd_Z));                                                  //Natural: ASSIGN #W-DESIG ( #D ) := DESIGNATION-TEXT ( #Z )
                    //*  102212 END
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  102212 START
                if (condition(pnd_W_Desig_Text.notEquals(" ")))                                                                                                           //Natural: IF #W-DESIG-TEXT NE ' '
                {
                                                                                                                                                                          //Natural: PERFORM TRANSLATE-INTO-LOWER-CASE
                    sub_Translate_Into_Lower_Case();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    FOR09:                                                                                                                                                //Natural: FOR #D 1 3
                    for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(3)); pnd_D.nadd(1))
                    {
                        if (condition(pnd_W_Desig_Text_Pnd_W_Desig.getValue(pnd_D).equals(" ")))                                                                          //Natural: IF #W-DESIG ( #D ) = ' '
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        beneficiary_Designation_Text.getValue(pnd_C).setValue(pnd_W_Desig_Text_Pnd_W_Desig.getValue(pnd_D));                                              //Natural: ASSIGN DESIGNATION-TEXT ( #C ) := #W-DESIG ( #D )
                        pnd_C.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #C
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_W_Desig_Text.reset();                                                                                                                                 //Natural: RESET #W-DESIG-TEXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaAdsl555.getCis_Bene_File_01_View_Cis_Prmry_Bene_Child_Prvsn().equals("Y")))                                                              //Natural: IF CIS-PRMRY-BENE-CHILD-PRVSN = 'Y'
                {
                    pnd_Z.compute(new ComputeParameters(false, pnd_Z), (pnd_X.subtract(1)).multiply(3));                                                                  //Natural: COMPUTE #Z = ( #X -1 ) * 3
                    pnd_Z.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #Z
                    beneficiary_Designation_Text.getValue(pnd_Z).setValue("With provision for payment to surviving children of a deceased beneficia");                    //Natural: ASSIGN DESIGNATION-TEXT ( #Z ) := 'With provision for payment to surviving children of a deceased beneficia'
                    pnd_Z.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #Z
                    beneficiary_Designation_Text.getValue(pnd_Z).setValue("ry of this class");                                                                            //Natural: ASSIGN DESIGNATION-TEXT ( #Z ) := 'ry of this class'
                    //*  112812 START
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaAdsl555.getCis_Bene_File_01_View_Cis_Prmry_Bene_Spcl_Txt().getValue("*",1).notEquals(" ")))                                          //Natural: IF CIS-PRMRY-BENE-SPCL-TXT ( *,1 ) NE ' '
                    {
                        pnd_Z.compute(new ComputeParameters(false, pnd_Z), (pnd_X.subtract(1)).multiply(3));                                                              //Natural: COMPUTE #Z = ( #X -1 ) * 3
                        //*  NEED THIS SO THE LETTER
                        pnd_Z.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #Z
                        beneficiary_Designation_Text.getValue(pnd_Z).setValue("H'00'");                                                                                   //Natural: ASSIGN DESIGNATION-TEXT ( #Z ) := H'00'
                        //*  COMES OUT PROPERLY IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  112812 END                           /* A SPECIAL TEXT IS SPACES
                    //*  FOR A BENEFICIARY
                }                                                                                                                                                         //Natural: END-IF
                //*  102212 END                           /* EX. PIN 2864917 IN PROD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Get_Contgnt_Bene_Info() throws Exception                                                                                                             //Natural: GET-CONTGNT-BENE-INFO
    {
        if (BLNatReinput.isReinput()) return;

        FOR10:                                                                                                                                                            //Natural: FOR #X 1 TO 20
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(20)); pnd_X.nadd(1))
        {
            //*  041207
            //*  061807
            if (condition(ldaAdsl555.getCis_Bene_File_01_View_Cis_Cntgnt_Bene_Name().getValue(pnd_X).equals(" ") && ldaAdsl555.getCis_Bene_File_01_View_Cis_Cntgnt_Bene_Spcl_Txt().getValue(pnd_X, //Natural: IF CIS-CNTGNT-BENE-NAME ( #X ) = ' ' AND CIS-CNTGNT-BENE-SPCL-TXT ( #X,1 ) = ' '
                1).equals(" ")))
            {
                //*  041207
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  041207
            }                                                                                                                                                             //Natural: END-IF
            //*  061807
            if (condition(ldaAdsl555.getCis_Bene_File_01_View_Cis_Cntgnt_Bene_Name().getValue(pnd_X).notEquals(" ")))                                                     //Natural: IF CIS-CNTGNT-BENE-NAME ( #X ) NE' '
            {
                pnd_Name.setValue(ldaAdsl555.getCis_Bene_File_01_View_Cis_Cntgnt_Bene_Name().getValue(pnd_X));                                                            //Natural: MOVE CIS-CNTGNT-BENE-NAME ( #X ) TO #NAME
                DbsUtil.callnat(Adsn553.class , getCurrentProcessState(), pnd_Name);                                                                                      //Natural: CALLNAT 'ADSN553' #NAME
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                contingent_Cont_Name.getValue(pnd_X).setValue(pnd_Name);                                                                                                  //Natural: MOVE #NAME TO CONT-NAME ( #X )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl555.getCis_Bene_File_01_View_Cis_Cntgnt_Bene_Dnmntr_Nbr().getValue(pnd_X).equals(100)))                                                  //Natural: IF CIS-CNTGNT-BENE-DNMNTR-NBR ( #X ) = 100
            {
                contingent_Cont_Alloc_Pct.getValue(pnd_X).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAdsl555.getCis_Bene_File_01_View_Cis_Cntgnt_Bene_Nmrtr_Nbr().getValue(pnd_X),  //Natural: COMPRESS CIS-CNTGNT-BENE-NMRTR-NBR ( #X ) '%' INTO CONT-ALLOC-PCT ( #X ) LEAVING NO
                    "%"));
                //*  050914 START
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaAdsl555.getCis_Bene_File_01_View_Cis_Cntgnt_Bene_Dnmntr_Nbr().getValue(pnd_X).greater(100)))                                             //Natural: IF CIS-CNTGNT-BENE-DNMNTR-NBR ( #X ) GT 100
                {
                    pnd_F.compute(new ComputeParameters(true, pnd_F), ldaAdsl555.getCis_Bene_File_01_View_Cis_Cntgnt_Bene_Nmrtr_Nbr().getValue(pnd_X).divide(ldaAdsl555.getCis_Bene_File_01_View_Cis_Cntgnt_Bene_Dnmntr_Nbr().getValue(pnd_X))); //Natural: COMPUTE ROUNDED #F = CIS-CNTGNT-BENE-NMRTR-NBR ( #X ) / CIS-CNTGNT-BENE-DNMNTR-NBR ( #X )
                    pnd_Result.compute(new ComputeParameters(true, pnd_Result), pnd_F.multiply(100));                                                                     //Natural: COMPUTE ROUNDED #RESULT = #F * 100
                    contingent_Cont_Alloc_Pct.getValue(pnd_X).setValueEdited(pnd_Result,new ReportEditMask("ZZ9.99"));                                                    //Natural: MOVE EDITED #RESULT ( EM = ZZ9.99 ) TO CONT-ALLOC-PCT ( #X )
                    contingent_Cont_Alloc_Pct.getValue(pnd_X).setValue(contingent_Cont_Alloc_Pct.getValue(pnd_X), MoveOption.LeftJustified);                              //Natural: MOVE LEFT CONT-ALLOC-PCT ( #X ) TO CONT-ALLOC-PCT ( #X )
                    contingent_Cont_Alloc_Pct.getValue(pnd_X).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, contingent_Cont_Alloc_Pct.getValue(pnd_X),         //Natural: COMPRESS CONT-ALLOC-PCT ( #X ) '%' INTO CONT-ALLOC-PCT ( #X ) LEAVING NO
                        "%"));
                    //*  050914 END
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Nbr1.setValue(ldaAdsl555.getCis_Bene_File_01_View_Cis_Cntgnt_Bene_Nmrtr_Nbr().getValue(pnd_X));                                                   //Natural: MOVE CIS-CNTGNT-BENE-NMRTR-NBR ( #X ) TO #NBR1
                    pnd_Nbr2.setValue(ldaAdsl555.getCis_Bene_File_01_View_Cis_Cntgnt_Bene_Dnmntr_Nbr().getValue(pnd_X));                                                  //Natural: MOVE CIS-CNTGNT-BENE-DNMNTR-NBR ( #X ) TO #NBR2
                                                                                                                                                                          //Natural: PERFORM TRANSLATE-NUMBERS-TO-WORDS
                    sub_Translate_Numbers_To_Words();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    contingent_Cont_Alloc_Pct.getValue(pnd_X).setValue(pnd_Desc2);                                                                                        //Natural: MOVE #DESC2 TO CONT-ALLOC-PCT ( #X )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  102212
            pnd_W_Desig_Text.reset();                                                                                                                                     //Natural: RESET #W-DESIG-TEXT #C #D
            pnd_C.reset();
            pnd_D.reset();
            if (condition(ldaAdsl555.getCis_Bene_File_01_View_Cis_Cntgnt_Bene_Spcl_Txt().getValue(pnd_X,1).notEquals(" ")))                                               //Natural: IF CIS-CNTGNT-BENE-SPCL-TXT ( #X,1 ) NE ' '
            {
                pnd_Z.compute(new ComputeParameters(false, pnd_Z), (pnd_X.subtract(1)).multiply(3));                                                                      //Natural: COMPUTE #Z = ( #X -1 ) * 3
                FOR11:                                                                                                                                                    //Natural: FOR #Y 1 3
                for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(3)); pnd_Y.nadd(1))
                {
                    pnd_Z.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #Z
                    //*  102212 START
                    if (condition(pnd_Y.equals(1)))                                                                                                                       //Natural: IF #Y = 1
                    {
                        //*  SAVE THE 1ST DESIGNATION OCCURRENCE
                        pnd_D.reset();                                                                                                                                    //Natural: RESET #D
                        pnd_C.setValue(pnd_Z);                                                                                                                            //Natural: ASSIGN #C := #Z
                        //*  102212 END
                    }                                                                                                                                                     //Natural: END-IF
                    beneficiary_Designation_Text_Cont.getValue(pnd_Z).setValue(ldaAdsl555.getCis_Bene_File_01_View_Cis_Cntgnt_Bene_Spcl_Txt().getValue(pnd_X,             //Natural: MOVE CIS-CNTGNT-BENE-SPCL-TXT ( #X,#Y ) TO DESIGNATION-TEXT-CONT ( #Z )
                        pnd_Y));
                    //*  102212 START TRANSLATE TO LOWER CASE
                    pnd_D.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #D
                    pnd_W_Desig_Text_Pnd_W_Desig.getValue(pnd_D).setValue(beneficiary_Designation_Text_Cont.getValue(pnd_Z));                                             //Natural: ASSIGN #W-DESIG ( #D ) := DESIGNATION-TEXT-CONT ( #Z )
                    //*  102212 END
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  102212 START
                if (condition(pnd_W_Desig_Text.notEquals(" ")))                                                                                                           //Natural: IF #W-DESIG-TEXT NE ' '
                {
                                                                                                                                                                          //Natural: PERFORM TRANSLATE-INTO-LOWER-CASE
                    sub_Translate_Into_Lower_Case();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    FOR12:                                                                                                                                                //Natural: FOR #D 1 3
                    for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(3)); pnd_D.nadd(1))
                    {
                        if (condition(pnd_W_Desig_Text_Pnd_W_Desig.getValue(pnd_D).equals(" ")))                                                                          //Natural: IF #W-DESIG ( #D ) = ' '
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        beneficiary_Designation_Text_Cont.getValue(pnd_C).setValue(pnd_W_Desig_Text_Pnd_W_Desig.getValue(pnd_D));                                         //Natural: ASSIGN DESIGNATION-TEXT-CONT ( #C ) := #W-DESIG ( #D )
                        pnd_C.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #C
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_W_Desig_Text.reset();                                                                                                                                 //Natural: RESET #W-DESIG-TEXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaAdsl555.getCis_Bene_File_01_View_Cis_Cntgnt_Bene_Child_Prvsn().equals("Y")))                                                             //Natural: IF CIS-CNTGNT-BENE-CHILD-PRVSN = 'Y'
                {
                    pnd_Z.compute(new ComputeParameters(false, pnd_Z), (pnd_X.subtract(1)).multiply(3));                                                                  //Natural: COMPUTE #Z = ( #X -1 ) * 3
                    pnd_Z.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #Z
                    beneficiary_Designation_Text_Cont.getValue(pnd_Z).setValue("With provision for payment to surviving children of a deceased beneficia");               //Natural: ASSIGN DESIGNATION-TEXT-CONT ( #Z ) := 'With provision for payment to surviving children of a deceased beneficia'
                    pnd_Z.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #Z
                    beneficiary_Designation_Text_Cont.getValue(pnd_Z).setValue("ry of this class");                                                                       //Natural: ASSIGN DESIGNATION-TEXT-CONT ( #Z ) := 'ry of this class'
                    //*  112712 START
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaAdsl555.getCis_Bene_File_01_View_Cis_Cntgnt_Bene_Spcl_Txt().getValue("*",1).notEquals(" ")))                                         //Natural: IF CIS-CNTGNT-BENE-SPCL-TXT ( *,1 ) NE ' '
                    {
                        pnd_Z.compute(new ComputeParameters(false, pnd_Z), (pnd_X.subtract(1)).multiply(3));                                                              //Natural: COMPUTE #Z = ( #X -1 ) * 3
                        pnd_Z.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #Z
                        beneficiary_Designation_Text_Cont.getValue(pnd_Z).setValue("H'00'");                                                                              //Natural: ASSIGN DESIGNATION-TEXT-CONT ( #Z ) := H'00'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  102212 END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Translate_Numbers_To_Words() throws Exception                                                                                                        //Natural: TRANSLATE-NUMBERS-TO-WORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pnd_Desc.reset();                                                                                                                                                 //Natural: RESET #DESC #DESC1 #DESC2
        pnd_Desc1.reset();
        pnd_Desc2.reset();
        if (condition((pnd_Nbr1.equals(getZero()) && pnd_Nbr2.equals(getZero()))))                                                                                        //Natural: IF ( #NBR1 = 0 AND #NBR2 = 0 )
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Nbr1.equals(pnd_Nbr2)))                                                                                                                         //Natural: IF #NBR1 = #NBR2
        {
            pnd_Desc2.setValue("100%");                                                                                                                                   //Natural: MOVE '100%' TO #DESC2
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Nbr1.greater(20)))                                                                                                                              //Natural: IF #NBR1 > 20
        {
            if (condition(pnd_Nbr1_Pnd_Nbr1_2.greater(getZero())))                                                                                                        //Natural: IF #NBR1-2 > 0
            {
                pnd_Desc.setValue(DbsUtil.compress(pnd_Wrd1_1.getValue(pnd_Nbr1_Pnd_Nbr1_1), pnd_Wrd1_2.getValue(pnd_Nbr1_Pnd_Nbr1_2)));                                  //Natural: COMPRESS #WRD1-1 ( #NBR1-1 ) #WRD1-2 ( #NBR1-2 ) INTO #DESC
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Desc.setValue(pnd_Wrd1_1.getValue(pnd_Nbr1_Pnd_Nbr1_1));                                                                                              //Natural: MOVE #WRD1-1 ( #NBR1-1 ) TO #DESC
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Desc.setValue(pnd_Wrd1.getValue(pnd_Nbr1));                                                                                                               //Natural: MOVE #WRD1 ( #NBR1 ) TO #DESC
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Nbr1.greater(1)))                                                                                                                               //Natural: IF #NBR1 > 1
        {
            pnd_S.setValue("s");                                                                                                                                          //Natural: MOVE 's' TO #S
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_S.setValue(" ");                                                                                                                                          //Natural: MOVE ' ' TO #S
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Nbr2.greater(20)))                                                                                                                              //Natural: IF #NBR2 > 20
        {
            if (condition(pnd_Nbr2_Pnd_Nbr2_2.greater(getZero())))                                                                                                        //Natural: IF #NBR2-2 > 0
            {
                pnd_Wrd2.getValue(1).setValue("first");                                                                                                                   //Natural: MOVE 'first' TO #WRD2 ( 1 )
                pnd_Wrd2.getValue(2).setValue("second");                                                                                                                  //Natural: MOVE 'second' TO #WRD2 ( 2 )
                pnd_Desc1.setValue(DbsUtil.compress(pnd_Wrd1_1.getValue(pnd_Nbr2_Pnd_Nbr2_1), pnd_Wrd2.getValue(pnd_Nbr2_Pnd_Nbr2_2)));                                   //Natural: COMPRESS #WRD1-1 ( #NBR2-1 ) #WRD2 ( #NBR2-2 ) INTO #DESC1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Desc1.setValue(pnd_Wrd1_1.getValue(pnd_Nbr2_Pnd_Nbr2_1));                                                                                             //Natural: MOVE #WRD1-1 ( #NBR2-1 ) TO #DESC1
                DbsUtil.examine(new ExamineSource(pnd_Desc1), new ExamineSearch("y"), new ExamineReplace("ieth"));                                                        //Natural: EXAMINE #DESC1 FOR 'y' REPLACE WITH 'ieth'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Wrd2.getValue(2).setValue("half");                                                                                                                        //Natural: MOVE 'half' TO #WRD2 ( 2 )
            pnd_Desc1.setValue(pnd_Wrd2.getValue(pnd_Nbr2));                                                                                                              //Natural: MOVE #WRD2 ( #NBR2 ) TO #DESC1
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Desc2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Desc, "-", pnd_Desc1, pnd_S));                                                             //Natural: COMPRESS #DESC '-' #DESC1 #S INTO #DESC2 LEAVING NO
    }
    private void sub_Translate_Into_Lower_Case() throws Exception                                                                                                         //Natural: TRANSLATE-INTO-LOWER-CASE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.examine(new ExamineSource(pnd_W_Desig_Text), new ExamineTranslate(TranslateOption.Lower));                                                                //Natural: EXAMINE #W-DESIG-TEXT AND TRANSLATE INTO LOWER CASE
        DbsUtil.examine(new ExamineSource(pnd_W_Desig_Text,1,1), new ExamineTranslate(TranslateOption.Upper));                                                            //Natural: EXAMINE SUBSTR ( #W-DESIG-TEXT,1,1 ) AND TRANSLATE INTO UPPER CASE
        FOR13:                                                                                                                                                            //Natural: FOR #A 2 216
        for (pnd_A.setValue(2); condition(pnd_A.lessOrEqual(216)); pnd_A.nadd(1))
        {
            if (condition(pnd_W_Desig_Text_Pnd_W_Desig_R.getValue(pnd_A).equals(".")))                                                                                    //Natural: IF #W-DESIG-R ( #A ) = '.'
            {
                FOR14:                                                                                                                                                    //Natural: FOR #B #A 216
                for (pnd_B.setValue(pnd_A); condition(pnd_B.lessOrEqual(216)); pnd_B.nadd(1))
                {
                    if (condition(pnd_W_Desig_Text_Pnd_W_Desig_R.getValue(pnd_B).notEquals(" ") && pnd_W_Desig_Text_Pnd_W_Desig_R.getValue(pnd_B).notEquals(".")))        //Natural: IF #W-DESIG-R ( #B ) NE ' ' AND #W-DESIG-R ( #B ) NE '.'
                    {
                        DbsUtil.examine(new ExamineSource(pnd_W_Desig_Text_Pnd_W_Desig_R.getValue(pnd_B)), new ExamineTranslate(TranslateOption.Upper));                  //Natural: EXAMINE #W-DESIG-R ( #B ) AND TRANSLATE INTO UPPER CASE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //
}
