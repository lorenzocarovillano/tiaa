/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:55:35 PM
**        * FROM NATURAL SUBPROGRAM : Adsn604
************************************************************
**        * FILE NAME            : Adsn604.java
**        * CLASS NAME           : Adsn604
**        * INSTANCE NAME        : Adsn604
************************************************************
************************************************************************
*
* PROGRAM:  ADSN604
* DATE   :  03/04
* FUNCTION: INTERFACE TO CREATE IA-MASTER RECORDS FOR TPA REINVESTMENTS
* M.NACHBER    09/06/2005  REL 6.5 FOR IPRO/TPA
*                          NO CHANGES, JUST RECOMPLILED TO PICK UP
*                          A NEW VERSION OF ADSA600
* M.NACHBER    03/07/2007  ATRA/KEOGH
*                          NO CHANGES, JUST RECOMPLILED TO PICK UP
*                          A NEW VERSION OF ADSA600
* O. SOTTO     02/22/2008  RECOMPILED FOR NEW ADSA600 - RATE EXPANSION.
* E. MELNIK    03/17/2008  ROTH 403B/401K. RECOMPILED FOR NEW ADSA600
* E. MELNIK    01/23/2009  TIAA ACCESS/STABLE RETURN ENHANCEMENTS.
*                          RECOMPLILED FOR NEW ADSA600.
* O. SOTTO     12/16/2010  RE-STOWED FOR CHANGED ADSA600.
* O. SOTTO     01/05/2012  SUNY/CUNY - RECOMPILED FOR UPDATED ADSA600.
* O. SOTTO     03/06/2012  RATE EXPNSN - RECOMPILED FOR UPDATED ADSA600.
* E  MELNIK    10/04/2013  RECOMPILED FOR NEW ADSA600.  CREF/REA
*                          REDESIGN CHANGES.
* E. MELNIK    05/07/2015  BENE IN THE PLAN - RECOMPILED FOR UPDATED
*                          ADSA600.
* R. CARREON   02/21/2017  RESTOWED FOR PIN EXPANSION 02212017
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn604 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAdsa600 pdaAdsa600;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Prtcpnt_Isn;

    private DataAccessProgramView vw_ads_Prtcpnt;
    private DbsField ads_Prtcpnt_Rqst_Id;
    private DbsField ads_Prtcpnt_Adp_Ia_Tiaa_Nbr;
    private DbsField ads_Prtcpnt_Adp_Ia_Tiaa_Rea_Nbr;
    private DbsField ads_Prtcpnt_Adp_Bps_Unit;
    private DbsField ads_Prtcpnt_Adp_Ia_Tiaa_Payee_Cd;
    private DbsField ads_Prtcpnt_Adp_Ia_Cref_Nbr;
    private DbsField ads_Prtcpnt_Adp_Ia_Cref_Payee_Cd;
    private DbsField ads_Prtcpnt_Adp_Annt_Typ_Cde;
    private DbsField ads_Prtcpnt_Adp_Emplymnt_Term_Dte;
    private DbsGroup ads_Prtcpnt_Adp_Cntrcts_In_RqstMuGroup;
    private DbsField ads_Prtcpnt_Adp_Cntrcts_In_Rqst;
    private DbsField ads_Prtcpnt_Adp_Unique_Id;
    private DbsField ads_Prtcpnt_Adp_Effctv_Dte;
    private DbsField ads_Prtcpnt_Adp_Cntr_Prt_Issue_Dte;
    private DbsField ads_Prtcpnt_Adp_Entry_Dte;
    private DbsField ads_Prtcpnt_Adp_Entry_Tme;
    private DbsField ads_Prtcpnt_Adp_Entry_User_Id;
    private DbsField ads_Prtcpnt_Adp_Rep_Nme;
    private DbsField ads_Prtcpnt_Adp_Lst_Actvty_Dte;
    private DbsField ads_Prtcpnt_Adp_Wpid;
    private DbsField ads_Prtcpnt_Adp_Mit_Log_Dte_Tme;
    private DbsField ads_Prtcpnt_Adp_Opn_Clsd_Ind;
    private DbsField ads_Prtcpnt_Adp_In_Prgrss_Ind;
    private DbsField ads_Prtcpnt_Adp_Rcprcl_Dte;
    private DbsField ads_Prtcpnt_Adp_Hold_Cde;
    private DbsField ads_Prtcpnt_Adp_Stts_Cde;

    private DbsGroup ads_Prtcpnt_Adp_Crrspndce_Mlng_Dta;
    private DbsGroup ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_TxtMuGroup;
    private DbsField ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Txt;
    private DbsField ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Zip;
    private DbsField ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Typ_Cd;
    private DbsField ads_Prtcpnt_Adp_Crrspndnce_Addr_Lst_Chg_Dte;
    private DbsField ads_Prtcpnt_Adp_Forms_Cmplte_Ind;
    private DbsField ads_Prtcpnt_Adp_Frst_Annt_Ctznshp;
    private DbsField ads_Prtcpnt_Adp_Frst_Annt_Rsdnc_Cde;
    private DbsField ads_Prtcpnt_Adp_Frst_Annt_Ssn;
    private DbsField ads_Prtcpnt_Adp_Frst_Annt_Title_Cde;
    private DbsField ads_Prtcpnt_Adp_Frst_Annt_Frst_Nme;
    private DbsField ads_Prtcpnt_Adp_Frst_Annt_Mid_Nme;
    private DbsField ads_Prtcpnt_Adp_Frst_Annt_Lst_Nme;
    private DbsField ads_Prtcpnt_Adp_Frst_Annt_Dte_Of_Brth;
    private DbsField ads_Prtcpnt_Adp_Frst_Annt_Sex_Cde;
    private DbsField ads_Prtcpnt_Adp_Scnd_Annt_Ssn;
    private DbsField ads_Prtcpnt_Adp_Scnd_Annt_Frst_Nme;
    private DbsField ads_Prtcpnt_Adp_Scnd_Annt_Mid_Nme;
    private DbsField ads_Prtcpnt_Adp_Scnd_Annt_Lst_Nme;
    private DbsField ads_Prtcpnt_Adp_Scnd_Annt_Dte_Of_Brth;
    private DbsField ads_Prtcpnt_Adp_Scnd_Annt_Sex_Cde;
    private DbsField ads_Prtcpnt_Adp_Annty_Optn;
    private DbsField ads_Prtcpnt_Adp_Grntee_Period;
    private DbsField ads_Prtcpnt_Adp_Grntee_Period_Mnths;
    private DbsField ads_Prtcpnt_Adp_Annty_Strt_Dte;
    private DbsField ads_Prtcpnt_Adp_Pymnt_Mode;
    private DbsField ads_Prtcpnt_Adp_Settl_All_Tiaa_Grd_Pct;
    private DbsField ads_Prtcpnt_Adp_Settl_All_Cref_Mon_Pct;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Addr_Ind;
    private DbsField ads_Prtcpnt_Adp_Spcfc_Brkdwn_Ind;

    private DbsGroup ads_Prtcpnt_Adp_Alt_Dest_Data;
    private DbsField ads_Prtcpnt_Adp_Alt_Payee_Ind;
    private DbsField ads_Prtcpnt_Adp_Alt_Carrier_Cde;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Nme;
    private DbsGroup ads_Prtcpnt_Adp_Alt_Dest_Addr_TxtMuGroup;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Addr_Txt;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Addr_Zip;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Addr_Typ_Cde;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Acct_Nbr;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Acct_Typ;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Trnst_Cde;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Hold_Cde;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Rlvr_Ind;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Rlvr_Dest;
    private DbsField ads_Prtcpnt_Adp_Glbl_Pay_Ind;
    private DbsField ads_Prtcpnt_Adp_Pull_Ia_Cntrct_Pckg_Ind;
    private DbsField ads_Prtcpnt_Adp_Pull_Ia_Cntrct_Pckg_Rsn;
    private DbsField ads_Prtcpnt_Adp_Pull_Crrspndce_Ind;
    private DbsField ads_Prtcpnt_Adp_Pull_Crrspndce_Prntr_Id;
    private DbsField ads_Prtcpnt_Adp_Rsttlmnt_Cnt;
    private DbsField ads_Prtcpnt_Adp_State_Of_Issue;
    private DbsField ads_Prtcpnt_Adp_Orgnl_Issue_State;
    private DbsField ads_Prtcpnt_Adp_Ivc_Rlvr_Cde;
    private DbsField ads_Prtcpnt_Adp_Ivc_Pymnt_Rule;
    private DbsField ads_Prtcpnt_Adp_Roth_Rqst_Ind;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaAdsa600 = new PdaAdsa600(parameters);
        pnd_Prtcpnt_Isn = parameters.newFieldInRecord("pnd_Prtcpnt_Isn", "#PRTCPNT-ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Prtcpnt_Isn.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_ads_Prtcpnt = new DataAccessProgramView(new NameInfo("vw_ads_Prtcpnt", "ADS-PRTCPNT"), "ADS_PRTCPNT", "ADS_PRTCPNT");
        ads_Prtcpnt_Rqst_Id = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "RQST_ID");
        ads_Prtcpnt_Adp_Ia_Tiaa_Nbr = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Ia_Tiaa_Nbr", "ADP-IA-TIAA-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "ADP_IA_TIAA_NBR");
        ads_Prtcpnt_Adp_Ia_Tiaa_Rea_Nbr = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Ia_Tiaa_Rea_Nbr", "ADP-IA-TIAA-REA-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "ADP_IA_TIAA_REA_NBR");
        ads_Prtcpnt_Adp_Bps_Unit = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Bps_Unit", "ADP-BPS-UNIT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADP_BPS_UNIT");
        ads_Prtcpnt_Adp_Ia_Tiaa_Payee_Cd = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Ia_Tiaa_Payee_Cd", "ADP-IA-TIAA-PAYEE-CD", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADP_IA_TIAA_PAYEE_CD");
        ads_Prtcpnt_Adp_Ia_Cref_Nbr = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Ia_Cref_Nbr", "ADP-IA-CREF-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "ADP_IA_CREF_NBR");
        ads_Prtcpnt_Adp_Ia_Cref_Payee_Cd = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Ia_Cref_Payee_Cd", "ADP-IA-CREF-PAYEE-CD", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADP_IA_CREF_PAYEE_CD");
        ads_Prtcpnt_Adp_Annt_Typ_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Annt_Typ_Cde", "ADP-ANNT-TYP-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_ANNT_TYP_CDE");
        ads_Prtcpnt_Adp_Emplymnt_Term_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Emplymnt_Term_Dte", "ADP-EMPLYMNT-TERM-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADP_EMPLYMNT_TERM_DTE");
        ads_Prtcpnt_Adp_Cntrcts_In_RqstMuGroup = vw_ads_Prtcpnt.getRecord().newGroupInGroup("ADS_PRTCPNT_ADP_CNTRCTS_IN_RQSTMuGroup", "ADP_CNTRCTS_IN_RQSTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_PRTCPNT_ADP_CNTRCTS_IN_RQST");
        ads_Prtcpnt_Adp_Cntrcts_In_Rqst = ads_Prtcpnt_Adp_Cntrcts_In_RqstMuGroup.newFieldArrayInGroup("ads_Prtcpnt_Adp_Cntrcts_In_Rqst", "ADP-CNTRCTS-IN-RQST", 
            FieldType.STRING, 10, new DbsArrayController(1, 6), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADP_CNTRCTS_IN_RQST");
        ads_Prtcpnt_Adp_Unique_Id = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Unique_Id", "ADP-UNIQUE-ID", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "ADP_UNIQUE_ID");
        ads_Prtcpnt_Adp_Effctv_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Effctv_Dte", "ADP-EFFCTV-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADP_EFFCTV_DTE");
        ads_Prtcpnt_Adp_Cntr_Prt_Issue_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Cntr_Prt_Issue_Dte", "ADP-CNTR-PRT-ISSUE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_CNTR_PRT_ISSUE_DTE");
        ads_Prtcpnt_Adp_Entry_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Entry_Dte", "ADP-ENTRY-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADP_ENTRY_DTE");
        ads_Prtcpnt_Adp_Entry_Tme = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Entry_Tme", "ADP-ENTRY-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "ADP_ENTRY_TME");
        ads_Prtcpnt_Adp_Entry_User_Id = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Entry_User_Id", "ADP-ENTRY-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADP_ENTRY_USER_ID");
        ads_Prtcpnt_Adp_Rep_Nme = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Rep_Nme", "ADP-REP-NME", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "ADP_REP_NME");
        ads_Prtcpnt_Adp_Lst_Actvty_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Lst_Actvty_Dte", "ADP-LST-ACTVTY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADP_LST_ACTVTY_DTE");
        ads_Prtcpnt_Adp_Wpid = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Wpid", "ADP-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "ADP_WPID");
        ads_Prtcpnt_Adp_Mit_Log_Dte_Tme = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Mit_Log_Dte_Tme", "ADP-MIT-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "ADP_MIT_LOG_DTE_TME");
        ads_Prtcpnt_Adp_Opn_Clsd_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Opn_Clsd_Ind", "ADP-OPN-CLSD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_OPN_CLSD_IND");
        ads_Prtcpnt_Adp_In_Prgrss_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_In_Prgrss_Ind", "ADP-IN-PRGRSS-IND", FieldType.BOOLEAN, 
            1, RepeatingFieldStrategy.None, "ADP_IN_PRGRSS_IND");
        ads_Prtcpnt_Adp_Rcprcl_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Rcprcl_Dte", "ADP-RCPRCL-DTE", FieldType.NUMERIC, 8, 
            RepeatingFieldStrategy.None, "ADP_RCPRCL_DTE");
        ads_Prtcpnt_Adp_Hold_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Hold_Cde", "ADP-HOLD-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADP_HOLD_CDE");
        ads_Prtcpnt_Adp_Stts_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Stts_Cde", "ADP-STTS-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "ADP_STTS_CDE");

        ads_Prtcpnt_Adp_Crrspndce_Mlng_Dta = vw_ads_Prtcpnt.getRecord().newGroupInGroup("ADS_PRTCPNT_ADP_CRRSPNDCE_MLNG_DTA", "ADP-CRRSPNDCE-MLNG-DTA");
        ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_TxtMuGroup = ads_Prtcpnt_Adp_Crrspndce_Mlng_Dta.newGroupInGroup("ADS_PRTCPNT_ADP_CRRSPNDNCE_PERM_ADDR_TXTMuGroup", 
            "ADP_CRRSPNDNCE_PERM_ADDR_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "ADS_PRTCPNT_ADP_CRRSPNDNCE_PERM_ADDR_TXT");
        ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Txt = ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_TxtMuGroup.newFieldArrayInGroup("ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Txt", 
            "ADP-CRRSPNDNCE-PERM-ADDR-TXT", FieldType.STRING, 35, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArray, "ADP_CRRSPNDNCE_PERM_ADDR_TXT");
        ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Zip = ads_Prtcpnt_Adp_Crrspndce_Mlng_Dta.newFieldInGroup("ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Zip", "ADP-CRRSPNDNCE-PERM-ADDR-ZIP", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "ADP_CRRSPNDNCE_PERM_ADDR_ZIP");
        ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Typ_Cd = ads_Prtcpnt_Adp_Crrspndce_Mlng_Dta.newFieldInGroup("ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Typ_Cd", 
            "ADP-CRRSPNDNCE-PERM-ADDR-TYP-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_CRRSPNDNCE_PERM_ADDR_TYP_CD");
        ads_Prtcpnt_Adp_Crrspndnce_Addr_Lst_Chg_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Crrspndnce_Addr_Lst_Chg_Dte", "ADP-CRRSPNDNCE-ADDR-LST-CHG-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_CRRSPNDNCE_ADDR_LST_CHG_DTE");
        ads_Prtcpnt_Adp_Forms_Cmplte_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Forms_Cmplte_Ind", "ADP-FORMS-CMPLTE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_FORMS_CMPLTE_IND");
        ads_Prtcpnt_Adp_Frst_Annt_Ctznshp = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Frst_Annt_Ctznshp", "ADP-FRST-ANNT-CTZNSHP", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_CTZNSHP");
        ads_Prtcpnt_Adp_Frst_Annt_Rsdnc_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Frst_Annt_Rsdnc_Cde", "ADP-FRST-ANNT-RSDNC-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_RSDNC_CDE");
        ads_Prtcpnt_Adp_Frst_Annt_Ssn = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Frst_Annt_Ssn", "ADP-FRST-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_SSN");
        ads_Prtcpnt_Adp_Frst_Annt_Title_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Frst_Annt_Title_Cde", "ADP-FRST-ANNT-TITLE-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_TITLE_CDE");
        ads_Prtcpnt_Adp_Frst_Annt_Frst_Nme = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Frst_Annt_Frst_Nme", "ADP-FRST-ANNT-FRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_FRST_NME");
        ads_Prtcpnt_Adp_Frst_Annt_Mid_Nme = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Frst_Annt_Mid_Nme", "ADP-FRST-ANNT-MID-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_MID_NME");
        ads_Prtcpnt_Adp_Frst_Annt_Lst_Nme = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Frst_Annt_Lst_Nme", "ADP-FRST-ANNT-LST-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_LST_NME");
        ads_Prtcpnt_Adp_Frst_Annt_Dte_Of_Brth = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Frst_Annt_Dte_Of_Brth", "ADP-FRST-ANNT-DTE-OF-BRTH", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_DTE_OF_BRTH");
        ads_Prtcpnt_Adp_Frst_Annt_Sex_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Frst_Annt_Sex_Cde", "ADP-FRST-ANNT-SEX-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_SEX_CDE");
        ads_Prtcpnt_Adp_Scnd_Annt_Ssn = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Scnd_Annt_Ssn", "ADP-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_SSN");
        ads_Prtcpnt_Adp_Scnd_Annt_Frst_Nme = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Scnd_Annt_Frst_Nme", "ADP-SCND-ANNT-FRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_FRST_NME");
        ads_Prtcpnt_Adp_Scnd_Annt_Mid_Nme = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Scnd_Annt_Mid_Nme", "ADP-SCND-ANNT-MID-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_MID_NME");
        ads_Prtcpnt_Adp_Scnd_Annt_Lst_Nme = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Scnd_Annt_Lst_Nme", "ADP-SCND-ANNT-LST-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_LST_NME");
        ads_Prtcpnt_Adp_Scnd_Annt_Dte_Of_Brth = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Scnd_Annt_Dte_Of_Brth", "ADP-SCND-ANNT-DTE-OF-BRTH", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_DTE_OF_BRTH");
        ads_Prtcpnt_Adp_Scnd_Annt_Sex_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Scnd_Annt_Sex_Cde", "ADP-SCND-ANNT-SEX-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_SEX_CDE");
        ads_Prtcpnt_Adp_Annty_Optn = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Annty_Optn", "ADP-ANNTY-OPTN", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ADP_ANNTY_OPTN");
        ads_Prtcpnt_Adp_Grntee_Period = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Grntee_Period", "ADP-GRNTEE-PERIOD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "ADP_GRNTEE_PERIOD");
        ads_Prtcpnt_Adp_Grntee_Period_Mnths = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Grntee_Period_Mnths", "ADP-GRNTEE-PERIOD-MNTHS", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "ADP_GRNTEE_PERIOD_MNTHS");
        ads_Prtcpnt_Adp_Annty_Strt_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Annty_Strt_Dte", "ADP-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADP_ANNTY_STRT_DTE");
        ads_Prtcpnt_Adp_Pymnt_Mode = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Pymnt_Mode", "ADP-PYMNT-MODE", FieldType.NUMERIC, 3, 
            RepeatingFieldStrategy.None, "ADP_PYMNT_MODE");
        ads_Prtcpnt_Adp_Settl_All_Tiaa_Grd_Pct = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Settl_All_Tiaa_Grd_Pct", "ADP-SETTL-ALL-TIAA-GRD-PCT", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "ADP_SETTL_ALL_TIAA_GRD_PCT");
        ads_Prtcpnt_Adp_Settl_All_Cref_Mon_Pct = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Settl_All_Cref_Mon_Pct", "ADP-SETTL-ALL-CREF-MON-PCT", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "ADP_SETTL_ALL_CREF_MON_PCT");
        ads_Prtcpnt_Adp_Alt_Dest_Addr_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Addr_Ind", "ADP-ALT-DEST-ADDR-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_ALT_DEST_ADDR_IND");
        ads_Prtcpnt_Adp_Spcfc_Brkdwn_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Spcfc_Brkdwn_Ind", "ADP-SPCFC-BRKDWN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_SPCFC_BRKDWN_IND");

        ads_Prtcpnt_Adp_Alt_Dest_Data = vw_ads_Prtcpnt.getRecord().newGroupInGroup("ADS_PRTCPNT_ADP_ALT_DEST_DATA", "ADP-ALT-DEST-DATA");
        ads_Prtcpnt_Adp_Alt_Payee_Ind = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Payee_Ind", "ADP-ALT-PAYEE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_ALT_PAYEE_IND");
        ads_Prtcpnt_Adp_Alt_Carrier_Cde = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Carrier_Cde", "ADP-ALT-CARRIER-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADP_ALT_CARRIER_CDE");
        ads_Prtcpnt_Adp_Alt_Dest_Nme = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Nme", "ADP-ALT-DEST-NME", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "ADP_ALT_DEST_NME");
        ads_Prtcpnt_Adp_Alt_Dest_Addr_TxtMuGroup = ads_Prtcpnt_Adp_Alt_Dest_Data.newGroupInGroup("ADS_PRTCPNT_ADP_ALT_DEST_ADDR_TXTMuGroup", "ADP_ALT_DEST_ADDR_TXTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_PRTCPNT_ADP_ALT_DEST_ADDR_TXT");
        ads_Prtcpnt_Adp_Alt_Dest_Addr_Txt = ads_Prtcpnt_Adp_Alt_Dest_Addr_TxtMuGroup.newFieldArrayInGroup("ads_Prtcpnt_Adp_Alt_Dest_Addr_Txt", "ADP-ALT-DEST-ADDR-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADP_ALT_DEST_ADDR_TXT");
        ads_Prtcpnt_Adp_Alt_Dest_Addr_Zip = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Addr_Zip", "ADP-ALT-DEST-ADDR-ZIP", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "ADP_ALT_DEST_ADDR_ZIP");
        ads_Prtcpnt_Adp_Alt_Dest_Addr_Typ_Cde = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Addr_Typ_Cde", "ADP-ALT-DEST-ADDR-TYP-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_ALT_DEST_ADDR_TYP_CDE");
        ads_Prtcpnt_Adp_Alt_Dest_Acct_Nbr = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Acct_Nbr", "ADP-ALT-DEST-ACCT-NBR", 
            FieldType.STRING, 21, RepeatingFieldStrategy.None, "ADP_ALT_DEST_ACCT_NBR");
        ads_Prtcpnt_Adp_Alt_Dest_Acct_Typ = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Acct_Typ", "ADP-ALT-DEST-ACCT-TYP", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_ALT_DEST_ACCT_TYP");
        ads_Prtcpnt_Adp_Alt_Dest_Trnst_Cde = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Trnst_Cde", "ADP-ALT-DEST-TRNST-CDE", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "ADP_ALT_DEST_TRNST_CDE");
        ads_Prtcpnt_Adp_Alt_Dest_Hold_Cde = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Hold_Cde", "ADP-ALT-DEST-HOLD-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADP_ALT_DEST_HOLD_CDE");
        ads_Prtcpnt_Adp_Alt_Dest_Rlvr_Ind = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Rlvr_Ind", "ADP-ALT-DEST-RLVR-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_ALT_DEST_RLVR_IND");
        ads_Prtcpnt_Adp_Alt_Dest_Rlvr_Dest = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Rlvr_Dest", "ADP-ALT-DEST-RLVR-DEST", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADP_ALT_DEST_RLVR_DEST");
        ads_Prtcpnt_Adp_Glbl_Pay_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Glbl_Pay_Ind", "ADP-GLBL-PAY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_GLBL_PAY_IND");
        ads_Prtcpnt_Adp_Pull_Ia_Cntrct_Pckg_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Pull_Ia_Cntrct_Pckg_Ind", "ADP-PULL-IA-CNTRCT-PCKG-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_PULL_IA_CNTRCT_PCKG_IND");
        ads_Prtcpnt_Adp_Pull_Ia_Cntrct_Pckg_Rsn = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Pull_Ia_Cntrct_Pckg_Rsn", "ADP-PULL-IA-CNTRCT-PCKG-RSN", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADP_PULL_IA_CNTRCT_PCKG_RSN");
        ads_Prtcpnt_Adp_Pull_Crrspndce_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Pull_Crrspndce_Ind", "ADP-PULL-CRRSPNDCE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_PULL_CRRSPNDCE_IND");
        ads_Prtcpnt_Adp_Pull_Crrspndce_Prntr_Id = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Pull_Crrspndce_Prntr_Id", "ADP-PULL-CRRSPNDCE-PRNTR-ID", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADP_PULL_CRRSPNDCE_PRNTR_ID");
        ads_Prtcpnt_Adp_Rsttlmnt_Cnt = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Rsttlmnt_Cnt", "ADP-RSTTLMNT-CNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_RSTTLMNT_CNT");
        ads_Prtcpnt_Adp_State_Of_Issue = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_State_Of_Issue", "ADP-STATE-OF-ISSUE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADP_STATE_OF_ISSUE");
        ads_Prtcpnt_Adp_Orgnl_Issue_State = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Orgnl_Issue_State", "ADP-ORGNL-ISSUE-STATE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADP_ORGNL_ISSUE_STATE");
        ads_Prtcpnt_Adp_Ivc_Rlvr_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Ivc_Rlvr_Cde", "ADP-IVC-RLVR-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_IVC_RLVR_CDE");
        ads_Prtcpnt_Adp_Ivc_Pymnt_Rule = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Ivc_Pymnt_Rule", "ADP-IVC-PYMNT-RULE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_IVC_PYMNT_RULE");
        ads_Prtcpnt_Adp_Roth_Rqst_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Roth_Rqst_Ind", "ADP-ROTH-RQST-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_ROTH_RQST_IND");
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Prtcpnt.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn604() throws Exception
    {
        super("Adsn604");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        GET01:                                                                                                                                                            //Natural: GET ADS-PRTCPNT #PRTCPNT-ISN
        vw_ads_Prtcpnt.readByID(pnd_Prtcpnt_Isn.getLong(), "GET01");
        pdaAdsa600.getAdsa600().setValuesByName(vw_ads_Prtcpnt);                                                                                                          //Natural: MOVE BY NAME ADS-PRTCPNT TO ADSA600
        //* * ADSA600.ADI-RTB-DEST-CDE := ADS-PRTCPNT.ADP-ALT-DEST-RLVR-DEST
    }

    //
}
