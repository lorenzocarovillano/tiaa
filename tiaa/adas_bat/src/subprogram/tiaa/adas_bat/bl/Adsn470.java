/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:54:22 PM
**        * FROM NATURAL SUBPROGRAM : Adsn470
************************************************************
**        * FILE NAME            : Adsn470.java
**        * CLASS NAME           : Adsn470
**        * INSTANCE NAME        : Adsn470
************************************************************
***********************************************************************
* PROGRAM  : ADSN470
* SYSTEM   : ADAS - ANNUITIZATION SUNGUARD
* TITLE    : ADAS BATCH - UPDATE ADAS KDO RECORDS
* GENERATED: MARCH 12, 2004
* AUTHOR   : EDWINA VILLANUEVA
* FUNCTION : THIS PROGRAM IS CLONED FROM THE ORIGINAL ADAM BATCH PROGRAM
*          : NAZN530. BASED ON RESULTS FROM ANNUITIZATION BATCH PROCESS,
*          : IT UPDATES RECORDS IN ADS-PRTCPNT AND ADS-CNTRCT FILES.
*********************  MAINTENANCE LOG ******************************
*
*  D A T E   PROGRAMMER     D E S C R I P T I O N
*  08302005 MARINA NACHBER CHANGED TO UPDATE NEW TPA FIELDS ON CONTRACT
*                          RECORD
*  10062005 MARINA NACHBER PRODUCTION ISSUE:
*                          SWITCHED 2 MOVES TO STATUS CODE ACCORDING
*                          TO ED MELNIK
*  03/17/2008 E MELNIK     ROTH 403B/401K.  RECOMPILED FOR NEW ADL401.
*  03/01/2012 O SOTTO      RECOMPILED FOR RATE EXPANSION.
*  01/18/13   E MELNIK     TNG PROJECT.  RESTOW TO INCLUDE UPDATED
*                          LINKAGE AREAS.
*  10/03/13   E MELNIK     RECOMPILED FOR NEW ADSL401A.  CREF/REA
*                          REDESIGN CHANGES.
* 03/23/17    E. MELNIK    RESTOWED FOR PIN EXPANSION.
*********************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn470 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAdsa470 pdaAdsa470;
    private LdaAdsl401 ldaAdsl401;
    private LdaAdsl401a ldaAdsl401a;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Constant_Vars;
    private DbsField pnd_Constant_Vars_Pnd_Resettlement;
    private DbsField pnd_Constant_Vars_Pnd_Max_Cntrct;
    private DbsField pnd_Constant_Vars_Pnd_Closed;

    private DbsGroup pnd_Constant_Vars_Pnd_Assign_Stat_Code;
    private DbsField pnd_Constant_Vars_Pnd_Comp_Calc_Eft;
    private DbsField pnd_Constant_Vars_Pnd_Comp_Calc_Noeft;
    private DbsField pnd_Constant_Vars_Pnd_Restlmnt_Complete;
    private DbsField pnd_Constant_Vars_Pnd_No_Stat_Found;
    private DbsField pnd_Constant_Vars_Pnd_System_Error;

    private DbsGroup pnd_Other_Vars;
    private DbsField pnd_Other_Vars_Pnd_Status_Code;
    private DbsField pnd_Other_Vars_Pnd_X;
    private DbsField pnd_Other_Vars_Pnd_Y;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAdsl401 = new LdaAdsl401();
        registerRecord(ldaAdsl401);
        registerRecord(ldaAdsl401.getVw_ads_Prtcpnt_View());
        ldaAdsl401a = new LdaAdsl401a();
        registerRecord(ldaAdsl401a);
        registerRecord(ldaAdsl401a.getVw_ads_Cntrct_View());

        // parameters
        parameters = new DbsRecord();
        pdaAdsa470 = new PdaAdsa470(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Constant_Vars = localVariables.newGroupInRecord("pnd_Constant_Vars", "#CONSTANT-VARS");
        pnd_Constant_Vars_Pnd_Resettlement = pnd_Constant_Vars.newFieldInGroup("pnd_Constant_Vars_Pnd_Resettlement", "#RESETTLEMENT", FieldType.STRING, 
            1);
        pnd_Constant_Vars_Pnd_Max_Cntrct = pnd_Constant_Vars.newFieldInGroup("pnd_Constant_Vars_Pnd_Max_Cntrct", "#MAX-CNTRCT", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Constant_Vars_Pnd_Closed = pnd_Constant_Vars.newFieldInGroup("pnd_Constant_Vars_Pnd_Closed", "#CLOSED", FieldType.STRING, 1);

        pnd_Constant_Vars_Pnd_Assign_Stat_Code = pnd_Constant_Vars.newGroupInGroup("pnd_Constant_Vars_Pnd_Assign_Stat_Code", "#ASSIGN-STAT-CODE");
        pnd_Constant_Vars_Pnd_Comp_Calc_Eft = pnd_Constant_Vars_Pnd_Assign_Stat_Code.newFieldInGroup("pnd_Constant_Vars_Pnd_Comp_Calc_Eft", "#COMP-CALC-EFT", 
            FieldType.STRING, 3);
        pnd_Constant_Vars_Pnd_Comp_Calc_Noeft = pnd_Constant_Vars_Pnd_Assign_Stat_Code.newFieldInGroup("pnd_Constant_Vars_Pnd_Comp_Calc_Noeft", "#COMP-CALC-NOEFT", 
            FieldType.STRING, 3);
        pnd_Constant_Vars_Pnd_Restlmnt_Complete = pnd_Constant_Vars_Pnd_Assign_Stat_Code.newFieldInGroup("pnd_Constant_Vars_Pnd_Restlmnt_Complete", "#RESTLMNT-COMPLETE", 
            FieldType.STRING, 3);
        pnd_Constant_Vars_Pnd_No_Stat_Found = pnd_Constant_Vars_Pnd_Assign_Stat_Code.newFieldInGroup("pnd_Constant_Vars_Pnd_No_Stat_Found", "#NO-STAT-FOUND", 
            FieldType.STRING, 3);
        pnd_Constant_Vars_Pnd_System_Error = pnd_Constant_Vars_Pnd_Assign_Stat_Code.newFieldInGroup("pnd_Constant_Vars_Pnd_System_Error", "#SYSTEM-ERROR", 
            FieldType.STRING, 3);

        pnd_Other_Vars = localVariables.newGroupInRecord("pnd_Other_Vars", "#OTHER-VARS");
        pnd_Other_Vars_Pnd_Status_Code = pnd_Other_Vars.newFieldInGroup("pnd_Other_Vars_Pnd_Status_Code", "#STATUS-CODE", FieldType.STRING, 3);
        pnd_Other_Vars_Pnd_X = pnd_Other_Vars.newFieldInGroup("pnd_Other_Vars_Pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);
        pnd_Other_Vars_Pnd_Y = pnd_Other_Vars.newFieldInGroup("pnd_Other_Vars_Pnd_Y", "#Y", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaAdsl401.initializeValues();
        ldaAdsl401a.initializeValues();

        localVariables.reset();
        pnd_Constant_Vars_Pnd_Resettlement.setInitialValue("R");
        pnd_Constant_Vars_Pnd_Max_Cntrct.setInitialValue(12);
        pnd_Constant_Vars_Pnd_Closed.setInitialValue("C");
        pnd_Constant_Vars_Pnd_Comp_Calc_Eft.setInitialValue("T00");
        pnd_Constant_Vars_Pnd_Comp_Calc_Noeft.setInitialValue("T05");
        pnd_Constant_Vars_Pnd_Restlmnt_Complete.setInitialValue("T10");
        pnd_Constant_Vars_Pnd_No_Stat_Found.setInitialValue("L02");
        pnd_Constant_Vars_Pnd_System_Error.setInitialValue("M99");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn470() throws Exception
    {
        super("Adsn470");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        if (condition(pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Debug_On().getBoolean()))                                                                                     //Natural: IF #ADSA470-DEBUG-ON
        {
            getReports().write(0, "*",NEWLINE,"... INSIDE ADSN470 -UPDATE PRTCPNT CONTRACT","=",pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Prtcpnt_Isn(),"=",                  //Natural: WRITE '*' / '... INSIDE ADSN470 -UPDATE PRTCPNT CONTRACT' '=' #ADSA470-PRTCPNT-ISN '=' #ADSA470-CNTRCT-ISN ( * ) '=' #ADSA470.ADC-STTS-CDE ( * )
                pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Cntrct_Isn().getValue("*"),"=",pdaAdsa470.getPnd_Adsa470_Adc_Stts_Cde().getValue("*"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        GET_PRTCPNT:                                                                                                                                                      //Natural: GET ADS-PRTCPNT-VIEW #ADSA470-PRTCPNT-ISN
        ldaAdsl401.getVw_ads_Prtcpnt_View().readByID(pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Prtcpnt_Isn().getLong(), "GET_PRTCPNT");
        FOR01:                                                                                                                                                            //Natural: FOR #X 1 TO #MAX-CNTRCT
        for (pnd_Other_Vars_Pnd_X.setValue(1); condition(pnd_Other_Vars_Pnd_X.lessOrEqual(pnd_Constant_Vars_Pnd_Max_Cntrct)); pnd_Other_Vars_Pnd_X.nadd(1))
        {
            if (condition(pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Cntrct_Isn().getValue(pnd_Other_Vars_Pnd_X).equals(getZero())))                                           //Natural: IF #ADSA470-CNTRCT-ISN ( #X ) EQ 0
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            GET_CNTRCT:                                                                                                                                                   //Natural: GET ADS-CNTRCT-VIEW #ADSA470-CNTRCT-ISN ( #X )
            ldaAdsl401a.getVw_ads_Cntrct_View().readByID(pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Cntrct_Isn().getValue(pnd_Other_Vars_Pnd_X).getLong(), 
                "GET_CNTRCT");
            if (condition(pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Error_Status().notEquals(" ")))                                                                           //Natural: IF #ADSA470-ERROR-STATUS NE ' '
            {
                pdaAdsa470.getPnd_Adsa470_Adc_Stts_Cde().getValue(pnd_Other_Vars_Pnd_X).setValue(pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Error_Status());                   //Natural: MOVE #ADSA470-ERROR-STATUS TO #ADSA470.ADC-STTS-CDE ( #X )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Other_Vars_Pnd_Status_Code.reset();                                                                                                                   //Natural: RESET #STATUS-CODE
                if (condition(!pdaAdsa470.getPnd_Adsa470_Adc_Stts_Cde().getValue(pnd_Other_Vars_Pnd_X).getSubstring(1,1).equals("L")))                                    //Natural: IF SUBSTRING ( #ADSA470.ADC-STTS-CDE ( #X ) ,1,1 ) NE 'L'
                {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-STATUS-CODE
                    sub_Determine_Status_Code();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pdaAdsa470.getPnd_Adsa470_Adc_Stts_Cde().getValue(pnd_Other_Vars_Pnd_X).setValue(pnd_Other_Vars_Pnd_Status_Code);                                     //Natural: MOVE #STATUS-CODE TO #ADSA470.ADC-STTS-CDE ( #X )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Other_Vars_Pnd_Status_Code.setValue(pdaAdsa470.getPnd_Adsa470_Adc_Stts_Cde().getValue(pnd_Other_Vars_Pnd_X));                                     //Natural: MOVE #ADSA470.ADC-STTS-CDE ( #X ) TO #STATUS-CODE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl401a.getVw_ads_Cntrct_View().setValuesByName(pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Cntrct_Rcrd().getValue(pnd_Other_Vars_Pnd_X));                      //Natural: MOVE BY NAME #ADSA470-CNTRCT-RCRD ( #X ) TO ADS-CNTRCT-VIEW
            //* * 08302005 MN START
            ldaAdsl401a.getAds_Cntrct_View_Adc_Tpa_Guarant_Commut_Grd_Amt().getValue("*").setValue(pdaAdsa470.getPnd_Adsa470_Adc_Tpa_Guarant_Commut_Grd_Amt().getValue(pnd_Other_Vars_Pnd_X, //Natural: MOVE #ADSA470.ADC-TPA-GUARANT-COMMUT-GRD-AMT ( #X,* ) TO ADS-CNTRCT-VIEW.ADC-TPA-GUARANT-COMMUT-GRD-AMT ( * )
                "*"));
            ldaAdsl401a.getAds_Cntrct_View_Adc_Tpa_Guarant_Commut_Std_Amt().getValue("*").setValue(pdaAdsa470.getPnd_Adsa470_Adc_Tpa_Guarant_Commut_Std_Amt().getValue(pnd_Other_Vars_Pnd_X, //Natural: MOVE #ADSA470.ADC-TPA-GUARANT-COMMUT-STD-AMT ( #X,* ) TO ADS-CNTRCT-VIEW.ADC-TPA-GUARANT-COMMUT-STD-AMT ( * )
                "*"));
            ldaAdsl401a.getAds_Cntrct_View_Adc_Orig_Lob_Ind().setValue(pdaAdsa470.getPnd_Adsa470_Adc_Orig_Lob_Ind().getValue(pnd_Other_Vars_Pnd_X));                      //Natural: MOVE #ADSA470.ADC-ORIG-LOB-IND ( #X ) TO ADS-CNTRCT-VIEW.ADC-ORIG-LOB-IND
            ldaAdsl401a.getAds_Cntrct_View_Adc_Tpa_Paymnt_Cnt().setValue(pdaAdsa470.getPnd_Adsa470_Adc_Tpa_Paymnt_Cnt().getValue(pnd_Other_Vars_Pnd_X));                  //Natural: MOVE #ADSA470.ADC-TPA-PAYMNT-CNT ( #X ) TO ADS-CNTRCT-VIEW.ADC-TPA-PAYMNT-CNT
            //* * 08302005 MN END
            ldaAdsl401a.getAds_Cntrct_View_Adc_Lst_Actvty_Dte().setValue(pdaAdsa470.getPnd_Adsa470_Adp_Lst_Actvty_Dte());                                                 //Natural: MOVE #ADSA470.ADP-LST-ACTVTY-DTE TO ADS-CNTRCT-VIEW.ADC-LST-ACTVTY-DTE
            ldaAdsl401a.getVw_ads_Cntrct_View().updateDBRow("GET_CNTRCT");                                                                                                //Natural: UPDATE ( GET-CNTRCT. )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaAdsl401.getVw_ads_Prtcpnt_View().setValuesByName(pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Prtcpnt_Rcrd());                                                        //Natural: MOVE BY NAME #ADSA470-PRTCPNT-RCRD TO ADS-PRTCPNT-VIEW
        if (condition(pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Error_Status().notEquals(" ")))                                                                               //Natural: IF #ADSA470-ERROR-STATUS NE ' '
        {
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().setValue(pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Error_Status());                                                 //Natural: MOVE #ADSA470-ERROR-STATUS TO ADS-PRTCPNT-VIEW.ADP-STTS-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().setValue(pnd_Other_Vars_Pnd_Status_Code);                                                                       //Natural: MOVE #STATUS-CODE TO ADS-PRTCPNT-VIEW.ADP-STTS-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde().getSubstring(1,1).equals("L")))                                                                       //Natural: IF SUBSTRING ( ADS-PRTCPNT-VIEW.ADP-STTS-CDE,1,1 ) EQ 'L'
        {
            ldaAdsl401.getAds_Prtcpnt_View_Adp_Opn_Clsd_Ind().setValue(pnd_Constant_Vars_Pnd_Closed);                                                                     //Natural: MOVE #CLOSED TO ADS-PRTCPNT-VIEW.ADP-OPN-CLSD-IND
        }                                                                                                                                                                 //Natural: END-IF
        pdaAdsa470.getPnd_Adsa470_Adp_Stts_Cde().setValue(ldaAdsl401.getAds_Prtcpnt_View_Adp_Stts_Cde());                                                                 //Natural: MOVE ADS-PRTCPNT-VIEW.ADP-STTS-CDE TO #ADSA470.ADP-STTS-CDE
        ldaAdsl401.getAds_Prtcpnt_View_Adp_In_Prgrss_Ind().reset();                                                                                                       //Natural: RESET ADS-PRTCPNT-VIEW.ADP-IN-PRGRSS-IND
        ldaAdsl401.getVw_ads_Prtcpnt_View().updateDBRow("GET_PRTCPNT");                                                                                                   //Natural: UPDATE ( GET-PRTCPNT. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  ------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-STATUS-CODE
    }
    private void sub_Determine_Status_Code() throws Exception                                                                                                             //Natural: DETERMINE-STATUS-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------- *
        if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Annt_Typ_Cde().equals(pnd_Constant_Vars_Pnd_Resettlement)))                                                      //Natural: IF ADS-PRTCPNT-VIEW.ADP-ANNT-TYP-CDE = #RESETTLEMENT
        {
            pnd_Other_Vars_Pnd_Status_Code.setValue(pnd_Constant_Vars_Pnd_Restlmnt_Complete);                                                                             //Natural: MOVE #RESTLMNT-COMPLETE TO #STATUS-CODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR02:                                                                                                                                                        //Natural: FOR #Y 1 TO #ADSA470-FUND-CNT
            for (pnd_Other_Vars_Pnd_Y.setValue(1); condition(pnd_Other_Vars_Pnd_Y.lessOrEqual(pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Fund_Cnt())); pnd_Other_Vars_Pnd_Y.nadd(1))
            {
                if (condition(ldaAdsl401a.getAds_Cntrct_View_Adc_Acct_Actl_Amt().getValue(pnd_Other_Vars_Pnd_Y).notEquals(getZero())))                                    //Natural: IF ADS-CNTRCT-VIEW.ADC-ACCT-ACTL-AMT ( #Y ) NE 0
                {
                    if (condition(ldaAdsl401.getAds_Prtcpnt_View_Adp_Alt_Dest_Trnst_Cde().equals(" ") || ldaAdsl401.getAds_Prtcpnt_View_Adp_Alt_Dest_Trnst_Cde().equals("000000000"))) //Natural: IF ADS-PRTCPNT-VIEW.ADP-ALT-DEST-TRNST-CDE = ' ' OR = '000000000'
                    {
                        //* * 10062005 MN SWITCHED 2 MOVES TO STATUS CODE ACCORDING TO ED MELNIK
                        //* * MN        MOVE #COMP-CALC-EFT TO #STATUS-CODE
                        //*  MN
                        pnd_Other_Vars_Pnd_Status_Code.setValue(pnd_Constant_Vars_Pnd_Comp_Calc_Noeft);                                                                   //Natural: MOVE #COMP-CALC-NOEFT TO #STATUS-CODE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //* * MN        MOVE #COMP-CALC-NOEFT TO #STATUS-CODE
                        //*  MN
                        pnd_Other_Vars_Pnd_Status_Code.setValue(pnd_Constant_Vars_Pnd_Comp_Calc_Eft);                                                                     //Natural: MOVE #COMP-CALC-EFT TO #STATUS-CODE
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Other_Vars_Pnd_Status_Code.equals(" ")))                                                                                                        //Natural: IF #STATUS-CODE EQ ' '
        {
            pnd_Other_Vars_Pnd_Status_Code.setValue(pnd_Constant_Vars_Pnd_No_Stat_Found);                                                                                 //Natural: MOVE #NO-STAT-FOUND TO #STATUS-CODE #ADSA470-ERROR-STATUS
            pdaAdsa470.getPnd_Adsa470_Pnd_Adsa470_Error_Status().setValue(pnd_Constant_Vars_Pnd_No_Stat_Found);
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
