/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:57:33 PM
**        * FROM NATURAL SUBPROGRAM : Aian033
************************************************************
**        * FILE NAME            : Aian033.java
**        * CLASS NAME           : Aian033
**        * INSTANCE NAME        : Aian033
************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Aian033 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAial0330 pdaAial0330;
    private PdaAiaa510 pdaAiaa510;
    private LdaAial034 ldaAial034;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_uta;
    private DbsField uta_Md_Key;

    private DbsGroup uta__R_Field_1;
    private DbsField uta_Pnd_Md_1st_Ann_Age;
    private DbsField uta_Pnd_Md_2nd_Ann_Age;
    private DbsField uta_Md_Multiple;

    private DbsGroup uta__R_Field_2;
    private DbsField uta_Pnd_Md_Multiple;
    private DbsField pnd_Md_Key_A;

    private DbsGroup pnd_Md_Key_A__R_Field_3;
    private DbsField pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_A;

    private DbsGroup pnd_Md_Key_A__R_Field_4;
    private DbsField pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_1_2;
    private DbsField pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_3;
    private DbsField pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_A;

    private DbsGroup pnd_Md_Key_A__R_Field_5;
    private DbsField pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_1_2;
    private DbsField pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_3;

    private DataAccessProgramView vw_cwf_State_Age_Tbl;
    private DbsField cwf_State_Age_Tbl_Tbl_Key_Field;

    private DbsGroup cwf_State_Age_Tbl__R_Field_6;
    private DbsField cwf_State_Age_Tbl_Tbl_Key_Annty_Start;
    private DbsField cwf_State_Age_Tbl__Filler1;
    private DbsField cwf_State_Age_Tbl_Tbl_State_Code;
    private DbsField cwf_State_Age_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_State_Age_Tbl__R_Field_7;

    private DbsGroup cwf_State_Age_Tbl_Tbl_State_Codes;
    private DbsField cwf_State_Age_Tbl_Tbl_State_Cde_A;
    private DbsField cwf_State_Age_Tbl__Filler2;
    private DbsField cwf_State_Age_Tbl_Tbl_Age_Limit;

    private DataAccessProgramView vw_cwf_Product_Tbl;
    private DbsField cwf_Product_Tbl_Tbl_Key_Field;

    private DbsGroup cwf_Product_Tbl__R_Field_8;
    private DbsField cwf_Product_Tbl_Tbl_Key_Prdct;
    private DbsField cwf_Product_Tbl__Filler3;
    private DbsField cwf_Product_Tbl_Tbl_Prdct_Cde;
    private DbsField cwf_Product_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Product_Tbl__R_Field_9;

    private DbsGroup cwf_Product_Tbl_Tbl_Prdct_Orgn_Cdes;
    private DbsField cwf_Product_Tbl_Tbl_Prdct_Orgn_Cde_A;
    private DbsField cwf_Product_Tbl__Filler4;
    private DbsField pnd_Tbl_Prime_Key;

    private DbsGroup pnd_Tbl_Prime_Key__R_Field_10;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme;
    private DbsField pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field;
    private DbsField pnd_Tbl_Key_Annty_Start;
    private DbsField pnd_Tbl_Product_Cde;
    private DbsField pnd_Tbl_Age_Limit;
    private DbsField pnd_Ws_Asd_Date_A;

    private DbsGroup pnd_Ws_Asd_Date_A__R_Field_11;
    private DbsField pnd_Ws_Asd_Date_A_Pnd_Ws_Asd_Date;
    private DbsField pnd_Issue_Date_A;

    private DbsGroup pnd_Issue_Date_A__R_Field_12;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_N8;

    private DbsGroup pnd_Issue_Date_A__R_Field_13;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Yyyy;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Date_Mmdd;

    private DbsGroup pnd_Issue_Date_A__R_Field_14;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Year;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Month;
    private DbsField pnd_Issue_Date_A_Pnd_Issue_Day;
    private DbsField pnd_1st_Ann_Birth_Date_A;

    private DbsGroup pnd_1st_Ann_Birth_Date_A__R_Field_15;
    private DbsField pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Date_N8;

    private DbsGroup pnd_1st_Ann_Birth_Date_A__R_Field_16;
    private DbsField pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Date_Yyyy;
    private DbsField pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Date_Mmdd;

    private DbsGroup pnd_1st_Ann_Birth_Date_A__R_Field_17;
    private DbsField pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Year;
    private DbsField pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Month;
    private DbsField pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Day;
    private DbsField pnd_2nd_Ann_Birth_Date_A;

    private DbsGroup pnd_2nd_Ann_Birth_Date_A__R_Field_18;
    private DbsField pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_N8;

    private DbsGroup pnd_2nd_Ann_Birth_Date_A__R_Field_19;
    private DbsField pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_Yyyy;
    private DbsField pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_Mmdd;

    private DbsGroup pnd_2nd_Ann_Birth_Date_A__R_Field_20;
    private DbsField pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Year;
    private DbsField pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Month;
    private DbsField pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Day;
    private DbsField pnd_Ws_Dob_Date;

    private DbsGroup pnd_Ws_Dob_Date__R_Field_21;
    private DbsField pnd_Ws_Dob_Date_Pnd_Ws_Dob_Yr;
    private DbsField pnd_Ws_Dob_Date_Pnd_Ws_Dob_Mo;
    private DbsField pnd_Ws_Dob_Date_Pnd_Ws_Dob_Da;
    private DbsField pnd_Ws_Age;
    private DbsField pnd_Next_Month;
    private DbsField pnd_Beneficiary_Birth_Date_A;

    private DbsGroup pnd_Beneficiary_Birth_Date_A__R_Field_22;
    private DbsField pnd_Beneficiary_Birth_Date_A_Pnd_Beneficiary_Birth_Date_N8;

    private DbsGroup pnd_Beneficiary_Birth_Date_A__R_Field_23;
    private DbsField pnd_Beneficiary_Birth_Date_A_Pnd_Beneficiary_Birth_Year;
    private DbsField pnd_Beneficiary_Birth_Date_A_Pnd_Beneficiary_Birth_Month;
    private DbsField pnd_Beneficiary_Birth_Date_A_Pnd_Beneficiary_Birth_Day;
    private DbsField pnd_Ws_Other_Ann_Birth_Date_A;

    private DbsGroup pnd_Ws_Other_Ann_Birth_Date_A__R_Field_24;
    private DbsField pnd_Ws_Other_Ann_Birth_Date_A_Pnd_Ws_Other_Ann_Birth_Date_N8;

    private DbsGroup pnd_Ws_Other_Ann_Birth_Date_A__R_Field_25;
    private DbsField pnd_Ws_Other_Ann_Birth_Date_A_Pnd_Ws_Other_Ann_Birth_Year;
    private DbsField pnd_Ws_Other_Ann_Birth_Date_A_Pnd_Ws_Other_Ann_Birth_Month;
    private DbsField pnd_Ws_Other_Ann_Birth_Date_A_Pnd_Ws_Other_Ann_Birth_Day;
    private DbsField pnd_Ws_1st_Ann_Age_N;

    private DbsGroup pnd_Ws_1st_Ann_Age_N__R_Field_26;
    private DbsField pnd_Ws_1st_Ann_Age_N_Pnd_Ws_1st_Ann_Age_A;
    private DbsField pnd_Ws_Other_Ann_Age_N;

    private DbsGroup pnd_Ws_Other_Ann_Age_N__R_Field_27;
    private DbsField pnd_Ws_Other_Ann_Age_N_Pnd_Ws_Other_Ann_Age_A;
    private DbsField pnd_Ws2_1st_Ann_Age_N;
    private DbsField pnd_Md_Other_Ann_Age_N;

    private DbsGroup pnd_Mdib_Age_Table;
    private DbsField pnd_Mdib_Age_Table_Pnd_Mdib_Age_1;
    private DbsField pnd_Mdib_Age_Table_Pnd_Mdib_Age_2;
    private DbsField pnd_Mdib_Age_Table_Pnd_Mdib_Age_3;

    private DbsGroup pnd_Mdib_Age_Table__R_Field_28;
    private DbsField pnd_Mdib_Age_Table_Pnd_Mdib_Age;

    private DbsGroup pnd_Mdib_Value_Table;
    private DbsField pnd_Mdib_Value_Table_Pnd_Mdib_Value_1;
    private DbsField pnd_Mdib_Value_Table_Pnd_Mdib_Value_2;
    private DbsField pnd_Mdib_Value_Table_Pnd_Mdib_Value_3;

    private DbsGroup pnd_Mdib_Value_Table__R_Field_29;
    private DbsField pnd_Mdib_Value_Table_Pnd_Mdib_Value;

    private DbsGroup pnd_Mdib_Diff_Table;
    private DbsField pnd_Mdib_Diff_Table_Pnd_Mdib_Diff_1;
    private DbsField pnd_Mdib_Diff_Table_Pnd_Mdib_Diff_2;

    private DbsGroup pnd_Mdib_Diff_Table__R_Field_30;
    private DbsField pnd_Mdib_Diff_Table_Pnd_Mdib_Diff;

    private DbsGroup pnd_Mdib_Pct_Table;
    private DbsField pnd_Mdib_Pct_Table_Pnd_Mdib_Pct_1;
    private DbsField pnd_Mdib_Pct_Table_Pnd_Mdib_Pct_2;

    private DbsGroup pnd_Mdib_Pct_Table__R_Field_31;
    private DbsField pnd_Mdib_Pct_Table_Pnd_Mdib_Pct;
    private DbsField pnd_Mdib_Min_Age;
    private DbsField pnd_Mdib_Min_Diff;
    private DbsField pnd_Mdib_Max_Age;
    private DbsField pnd_Mdib_Max_Diff;
    private DbsField pnd_Ws_Age_Diff;
    private DbsField pnd_Ws_1st_Ann_Birth_Month_At_70_Half;
    private DbsField pnd_Ws_1st_Ann_Birth_Year_At_70_Half;
    private DbsField pnd_Ws_Expected_Life_Mdib;
    private DbsField pnd_Ws_Expected_Life_Md;
    private DbsField pnd_Ws_Minimum_Life_Exp;
    private DbsField pnd_Ws_Current_Guar;
    private DbsField pnd_Ws_1st_Ann_Age_Mdib;
    private DbsField pnd_Ws_2nd_Ann_Age_Mdib;
    private DbsField pnd_Ws_Diff_1st_And_2nd_Ann_X;

    private DbsGroup pnd_Ws_Diff_1st_And_2nd_Ann_X__R_Field_32;
    private DbsField pnd_Ws_Diff_1st_And_2nd_Ann_X_Pnd_Ws_Diff_1st_And_2nd_Ann_1st;
    private DbsField pnd_Ws_Diff_1st_And_2nd_Ann_X_Pnd_Ws_Diff_1st_And_2nd_Ann;
    private DbsField pnd_Ws_Mdib_Value_Index;
    private DbsField pnd_Ws_Mdib_Diff_Index;
    private DbsField pnd_Ws_Mdib_Appl_Pct;
    private DbsField pnd_Ws_Pct_To_2nd_Ann;
    private DbsField pnd_I;
    private DbsField pnd_Ws_Guar_Year;
    private DbsField pnd_Ws_Guar_Month;
    private DbsField actual_Snd_Age;
    private DbsField pnd_Age_In_Years_2nd_Ann;
    private DbsField pnd_Age_In_Years_1st_Ann;
    private DbsField pnd_Complete_Years_1st_Ann;
    private DbsField pnd_Complete_Years_2nd_Ann;
    private DbsField pnd_Diff_In_Years;
    private DbsField pnd_Number_Of_Years_Less_Than_70;
    private DbsField pnd_Pct_1st_Ann_Birth_Date_A;

    private DbsGroup pnd_Pct_1st_Ann_Birth_Date_A__R_Field_33;
    private DbsField pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Date_N8;

    private DbsGroup pnd_Pct_1st_Ann_Birth_Date_A__R_Field_34;
    private DbsField pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Yyyy;
    private DbsField pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Mmdd;

    private DbsGroup pnd_Pct_1st_Ann_Birth_Date_A__R_Field_35;
    private DbsField pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Year;
    private DbsField pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Month;
    private DbsField pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Day;
    private DbsField pnd_Pct_2nd_Ann_Birth_Date_A;

    private DbsGroup pnd_Pct_2nd_Ann_Birth_Date_A__R_Field_36;
    private DbsField pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Date_N8;

    private DbsGroup pnd_Pct_2nd_Ann_Birth_Date_A__R_Field_37;
    private DbsField pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Yyyy;
    private DbsField pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Mmdd;

    private DbsGroup pnd_Pct_2nd_Ann_Birth_Date_A__R_Field_38;
    private DbsField pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Year;
    private DbsField pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Month;
    private DbsField pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Day;
    private DbsField pnd_Temp_Retrn_Msg_1stann90;
    private DbsField pnd_Temp_Retrn_Msg_2ndann90;
    private DbsField pnd_Temp_Retrn_Msg_Bothann90;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaAial034 = new LdaAial034();
        registerRecord(ldaAial034);

        // parameters
        parameters = new DbsRecord();
        pdaAial0330 = new PdaAial0330(parameters);
        pdaAiaa510 = new PdaAiaa510(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_uta = new DataAccessProgramView(new NameInfo("vw_uta", "UTA"), "MINDIST_VALUES_FILE", "MINDIST_VALUES_FILE");
        uta_Md_Key = vw_uta.getRecord().newFieldInGroup("uta_Md_Key", "MD-KEY", FieldType.STRING, 6, RepeatingFieldStrategy.None, "MD_KEY");

        uta__R_Field_1 = vw_uta.getRecord().newGroupInGroup("uta__R_Field_1", "REDEFINE", uta_Md_Key);
        uta_Pnd_Md_1st_Ann_Age = uta__R_Field_1.newFieldInGroup("uta_Pnd_Md_1st_Ann_Age", "#MD-1ST-ANN-AGE", FieldType.NUMERIC, 3);
        uta_Pnd_Md_2nd_Ann_Age = uta__R_Field_1.newFieldInGroup("uta_Pnd_Md_2nd_Ann_Age", "#MD-2ND-ANN-AGE", FieldType.NUMERIC, 3);
        uta_Md_Multiple = vw_uta.getRecord().newFieldInGroup("uta_Md_Multiple", "MD-MULTIPLE", FieldType.NUMERIC, 4, 1, RepeatingFieldStrategy.None, "MD_MULTIPLE");

        uta__R_Field_2 = vw_uta.getRecord().newGroupInGroup("uta__R_Field_2", "REDEFINE", uta_Md_Multiple);
        uta_Pnd_Md_Multiple = uta__R_Field_2.newFieldInGroup("uta_Pnd_Md_Multiple", "#MD-MULTIPLE", FieldType.NUMERIC, 4, 1);
        registerRecord(vw_uta);

        pnd_Md_Key_A = localVariables.newFieldInRecord("pnd_Md_Key_A", "#MD-KEY-A", FieldType.STRING, 6);

        pnd_Md_Key_A__R_Field_3 = localVariables.newGroupInRecord("pnd_Md_Key_A__R_Field_3", "REDEFINE", pnd_Md_Key_A);
        pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_A = pnd_Md_Key_A__R_Field_3.newFieldInGroup("pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_A", "#MD-1ST-ANN-AGE-A", FieldType.STRING, 
            3);

        pnd_Md_Key_A__R_Field_4 = pnd_Md_Key_A__R_Field_3.newGroupInGroup("pnd_Md_Key_A__R_Field_4", "REDEFINE", pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_A);
        pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_1_2 = pnd_Md_Key_A__R_Field_4.newFieldInGroup("pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_1_2", "#MD-1ST-ANN-AGE-1-2", FieldType.STRING, 
            2);
        pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_3 = pnd_Md_Key_A__R_Field_4.newFieldInGroup("pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_3", "#MD-1ST-ANN-AGE-3", FieldType.STRING, 
            1);
        pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_A = pnd_Md_Key_A__R_Field_3.newFieldInGroup("pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_A", "#MD-OTHER-ANN-AGE-A", FieldType.STRING, 
            3);

        pnd_Md_Key_A__R_Field_5 = pnd_Md_Key_A__R_Field_3.newGroupInGroup("pnd_Md_Key_A__R_Field_5", "REDEFINE", pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_A);
        pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_1_2 = pnd_Md_Key_A__R_Field_5.newFieldInGroup("pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_1_2", "#MD-OTHER-ANN-AGE-1-2", 
            FieldType.STRING, 2);
        pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_3 = pnd_Md_Key_A__R_Field_5.newFieldInGroup("pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_3", "#MD-OTHER-ANN-AGE-3", FieldType.STRING, 
            1);

        vw_cwf_State_Age_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_State_Age_Tbl", "CWF-STATE-AGE-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_State_Age_Tbl_Tbl_Key_Field = vw_cwf_State_Age_Tbl.getRecord().newFieldInGroup("cwf_State_Age_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        cwf_State_Age_Tbl__R_Field_6 = vw_cwf_State_Age_Tbl.getRecord().newGroupInGroup("cwf_State_Age_Tbl__R_Field_6", "REDEFINE", cwf_State_Age_Tbl_Tbl_Key_Field);
        cwf_State_Age_Tbl_Tbl_Key_Annty_Start = cwf_State_Age_Tbl__R_Field_6.newFieldInGroup("cwf_State_Age_Tbl_Tbl_Key_Annty_Start", "TBL-KEY-ANNTY-START", 
            FieldType.STRING, 15);
        cwf_State_Age_Tbl__Filler1 = cwf_State_Age_Tbl__R_Field_6.newFieldInGroup("cwf_State_Age_Tbl__Filler1", "_FILLER1", FieldType.STRING, 1);
        cwf_State_Age_Tbl_Tbl_State_Code = cwf_State_Age_Tbl__R_Field_6.newFieldInGroup("cwf_State_Age_Tbl_Tbl_State_Code", "TBL-STATE-CODE", FieldType.STRING, 
            2);
        cwf_State_Age_Tbl_Tbl_Data_Field = vw_cwf_State_Age_Tbl.getRecord().newFieldInGroup("cwf_State_Age_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_State_Age_Tbl__R_Field_7 = vw_cwf_State_Age_Tbl.getRecord().newGroupInGroup("cwf_State_Age_Tbl__R_Field_7", "REDEFINE", cwf_State_Age_Tbl_Tbl_Data_Field);

        cwf_State_Age_Tbl_Tbl_State_Codes = cwf_State_Age_Tbl__R_Field_7.newGroupArrayInGroup("cwf_State_Age_Tbl_Tbl_State_Codes", "TBL-STATE-CODES", 
            new DbsArrayController(1, 2));
        cwf_State_Age_Tbl_Tbl_State_Cde_A = cwf_State_Age_Tbl_Tbl_State_Codes.newFieldInGroup("cwf_State_Age_Tbl_Tbl_State_Cde_A", "TBL-STATE-CDE-A", 
            FieldType.STRING, 2);
        cwf_State_Age_Tbl__Filler2 = cwf_State_Age_Tbl_Tbl_State_Codes.newFieldInGroup("cwf_State_Age_Tbl__Filler2", "_FILLER2", FieldType.STRING, 1);
        cwf_State_Age_Tbl_Tbl_Age_Limit = cwf_State_Age_Tbl__R_Field_7.newFieldInGroup("cwf_State_Age_Tbl_Tbl_Age_Limit", "TBL-AGE-LIMIT", FieldType.NUMERIC, 
            2);
        registerRecord(vw_cwf_State_Age_Tbl);

        vw_cwf_Product_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Product_Tbl", "CWF-PRODUCT-TBL"), "CWF_SUPPORT_TBL", "CWF_DCMNT_TABLE");
        cwf_Product_Tbl_Tbl_Key_Field = vw_cwf_Product_Tbl.getRecord().newFieldInGroup("cwf_Product_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        cwf_Product_Tbl__R_Field_8 = vw_cwf_Product_Tbl.getRecord().newGroupInGroup("cwf_Product_Tbl__R_Field_8", "REDEFINE", cwf_Product_Tbl_Tbl_Key_Field);
        cwf_Product_Tbl_Tbl_Key_Prdct = cwf_Product_Tbl__R_Field_8.newFieldInGroup("cwf_Product_Tbl_Tbl_Key_Prdct", "TBL-KEY-PRDCT", FieldType.STRING, 
            15);
        cwf_Product_Tbl__Filler3 = cwf_Product_Tbl__R_Field_8.newFieldInGroup("cwf_Product_Tbl__Filler3", "_FILLER3", FieldType.STRING, 1);
        cwf_Product_Tbl_Tbl_Prdct_Cde = cwf_Product_Tbl__R_Field_8.newFieldInGroup("cwf_Product_Tbl_Tbl_Prdct_Cde", "TBL-PRDCT-CDE", FieldType.STRING, 
            10);
        cwf_Product_Tbl_Tbl_Data_Field = vw_cwf_Product_Tbl.getRecord().newFieldInGroup("cwf_Product_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", FieldType.STRING, 
            253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Product_Tbl__R_Field_9 = vw_cwf_Product_Tbl.getRecord().newGroupInGroup("cwf_Product_Tbl__R_Field_9", "REDEFINE", cwf_Product_Tbl_Tbl_Data_Field);

        cwf_Product_Tbl_Tbl_Prdct_Orgn_Cdes = cwf_Product_Tbl__R_Field_9.newGroupArrayInGroup("cwf_Product_Tbl_Tbl_Prdct_Orgn_Cdes", "TBL-PRDCT-ORGN-CDES", 
            new DbsArrayController(1, 20));
        cwf_Product_Tbl_Tbl_Prdct_Orgn_Cde_A = cwf_Product_Tbl_Tbl_Prdct_Orgn_Cdes.newFieldInGroup("cwf_Product_Tbl_Tbl_Prdct_Orgn_Cde_A", "TBL-PRDCT-ORGN-CDE-A", 
            FieldType.NUMERIC, 2);
        cwf_Product_Tbl__Filler4 = cwf_Product_Tbl_Tbl_Prdct_Orgn_Cdes.newFieldInGroup("cwf_Product_Tbl__Filler4", "_FILLER4", FieldType.STRING, 1);
        registerRecord(vw_cwf_Product_Tbl);

        pnd_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_Tbl_Prime_Key", "#TBL-PRIME-KEY", FieldType.STRING, 53);

        pnd_Tbl_Prime_Key__R_Field_10 = localVariables.newGroupInRecord("pnd_Tbl_Prime_Key__R_Field_10", "REDEFINE", pnd_Tbl_Prime_Key);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind = pnd_Tbl_Prime_Key__R_Field_10.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind", "#TBL-SCRTY-LEVEL-IND", 
            FieldType.STRING, 2);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme = pnd_Tbl_Prime_Key__R_Field_10.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme", "#TBL-TABLE-NME", FieldType.STRING, 
            20);
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field = pnd_Tbl_Prime_Key__R_Field_10.newFieldInGroup("pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field", "#TBL-KEY-FIELD", FieldType.STRING, 
            15);
        pnd_Tbl_Key_Annty_Start = localVariables.newFieldInRecord("pnd_Tbl_Key_Annty_Start", "#TBL-KEY-ANNTY-START", FieldType.STRING, 22);
        pnd_Tbl_Product_Cde = localVariables.newFieldInRecord("pnd_Tbl_Product_Cde", "#TBL-PRODUCT-CDE", FieldType.STRING, 10);
        pnd_Tbl_Age_Limit = localVariables.newFieldInRecord("pnd_Tbl_Age_Limit", "#TBL-AGE-LIMIT", FieldType.NUMERIC, 2);
        pnd_Ws_Asd_Date_A = localVariables.newFieldInRecord("pnd_Ws_Asd_Date_A", "#WS-ASD-DATE-A", FieldType.STRING, 8);

        pnd_Ws_Asd_Date_A__R_Field_11 = localVariables.newGroupInRecord("pnd_Ws_Asd_Date_A__R_Field_11", "REDEFINE", pnd_Ws_Asd_Date_A);
        pnd_Ws_Asd_Date_A_Pnd_Ws_Asd_Date = pnd_Ws_Asd_Date_A__R_Field_11.newFieldInGroup("pnd_Ws_Asd_Date_A_Pnd_Ws_Asd_Date", "#WS-ASD-DATE", FieldType.NUMERIC, 
            8);
        pnd_Issue_Date_A = localVariables.newFieldInRecord("pnd_Issue_Date_A", "#ISSUE-DATE-A", FieldType.STRING, 8);

        pnd_Issue_Date_A__R_Field_12 = localVariables.newGroupInRecord("pnd_Issue_Date_A__R_Field_12", "REDEFINE", pnd_Issue_Date_A);
        pnd_Issue_Date_A_Pnd_Issue_Date_N8 = pnd_Issue_Date_A__R_Field_12.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_N8", "#ISSUE-DATE-N8", FieldType.NUMERIC, 
            8);

        pnd_Issue_Date_A__R_Field_13 = localVariables.newGroupInRecord("pnd_Issue_Date_A__R_Field_13", "REDEFINE", pnd_Issue_Date_A);
        pnd_Issue_Date_A_Pnd_Issue_Date_Yyyy = pnd_Issue_Date_A__R_Field_13.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Yyyy", "#ISSUE-DATE-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Issue_Date_A_Pnd_Issue_Date_Mmdd = pnd_Issue_Date_A__R_Field_13.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Date_Mmdd", "#ISSUE-DATE-MMDD", 
            FieldType.STRING, 4);

        pnd_Issue_Date_A__R_Field_14 = localVariables.newGroupInRecord("pnd_Issue_Date_A__R_Field_14", "REDEFINE", pnd_Issue_Date_A);
        pnd_Issue_Date_A_Pnd_Issue_Year = pnd_Issue_Date_A__R_Field_14.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Year", "#ISSUE-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Issue_Date_A_Pnd_Issue_Month = pnd_Issue_Date_A__R_Field_14.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Month", "#ISSUE-MONTH", FieldType.NUMERIC, 
            2);
        pnd_Issue_Date_A_Pnd_Issue_Day = pnd_Issue_Date_A__R_Field_14.newFieldInGroup("pnd_Issue_Date_A_Pnd_Issue_Day", "#ISSUE-DAY", FieldType.NUMERIC, 
            2);
        pnd_1st_Ann_Birth_Date_A = localVariables.newFieldInRecord("pnd_1st_Ann_Birth_Date_A", "#1ST-ANN-BIRTH-DATE-A", FieldType.STRING, 8);

        pnd_1st_Ann_Birth_Date_A__R_Field_15 = localVariables.newGroupInRecord("pnd_1st_Ann_Birth_Date_A__R_Field_15", "REDEFINE", pnd_1st_Ann_Birth_Date_A);
        pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Date_N8 = pnd_1st_Ann_Birth_Date_A__R_Field_15.newFieldInGroup("pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Date_N8", 
            "#1ST-ANN-BIRTH-DATE-N8", FieldType.NUMERIC, 8);

        pnd_1st_Ann_Birth_Date_A__R_Field_16 = localVariables.newGroupInRecord("pnd_1st_Ann_Birth_Date_A__R_Field_16", "REDEFINE", pnd_1st_Ann_Birth_Date_A);
        pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Date_Yyyy = pnd_1st_Ann_Birth_Date_A__R_Field_16.newFieldInGroup("pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Date_Yyyy", 
            "#1ST-ANN-BIRTH-DATE-YYYY", FieldType.STRING, 4);
        pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Date_Mmdd = pnd_1st_Ann_Birth_Date_A__R_Field_16.newFieldInGroup("pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Date_Mmdd", 
            "#1ST-ANN-BIRTH-DATE-MMDD", FieldType.STRING, 4);

        pnd_1st_Ann_Birth_Date_A__R_Field_17 = localVariables.newGroupInRecord("pnd_1st_Ann_Birth_Date_A__R_Field_17", "REDEFINE", pnd_1st_Ann_Birth_Date_A);
        pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Year = pnd_1st_Ann_Birth_Date_A__R_Field_17.newFieldInGroup("pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Year", 
            "#1ST-ANN-BIRTH-YEAR", FieldType.NUMERIC, 4);
        pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Month = pnd_1st_Ann_Birth_Date_A__R_Field_17.newFieldInGroup("pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Month", 
            "#1ST-ANN-BIRTH-MONTH", FieldType.NUMERIC, 2);
        pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Day = pnd_1st_Ann_Birth_Date_A__R_Field_17.newFieldInGroup("pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Day", 
            "#1ST-ANN-BIRTH-DAY", FieldType.NUMERIC, 2);
        pnd_2nd_Ann_Birth_Date_A = localVariables.newFieldInRecord("pnd_2nd_Ann_Birth_Date_A", "#2ND-ANN-BIRTH-DATE-A", FieldType.STRING, 8);

        pnd_2nd_Ann_Birth_Date_A__R_Field_18 = localVariables.newGroupInRecord("pnd_2nd_Ann_Birth_Date_A__R_Field_18", "REDEFINE", pnd_2nd_Ann_Birth_Date_A);
        pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_N8 = pnd_2nd_Ann_Birth_Date_A__R_Field_18.newFieldInGroup("pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_N8", 
            "#2ND-ANN-BIRTH-DATE-N8", FieldType.NUMERIC, 8);

        pnd_2nd_Ann_Birth_Date_A__R_Field_19 = localVariables.newGroupInRecord("pnd_2nd_Ann_Birth_Date_A__R_Field_19", "REDEFINE", pnd_2nd_Ann_Birth_Date_A);
        pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_Yyyy = pnd_2nd_Ann_Birth_Date_A__R_Field_19.newFieldInGroup("pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_Yyyy", 
            "#2ND-ANN-BIRTH-DATE-YYYY", FieldType.STRING, 4);
        pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_Mmdd = pnd_2nd_Ann_Birth_Date_A__R_Field_19.newFieldInGroup("pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_Mmdd", 
            "#2ND-ANN-BIRTH-DATE-MMDD", FieldType.STRING, 4);

        pnd_2nd_Ann_Birth_Date_A__R_Field_20 = localVariables.newGroupInRecord("pnd_2nd_Ann_Birth_Date_A__R_Field_20", "REDEFINE", pnd_2nd_Ann_Birth_Date_A);
        pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Year = pnd_2nd_Ann_Birth_Date_A__R_Field_20.newFieldInGroup("pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Year", 
            "#2ND-ANN-BIRTH-YEAR", FieldType.NUMERIC, 4);
        pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Month = pnd_2nd_Ann_Birth_Date_A__R_Field_20.newFieldInGroup("pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Month", 
            "#2ND-ANN-BIRTH-MONTH", FieldType.NUMERIC, 2);
        pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Day = pnd_2nd_Ann_Birth_Date_A__R_Field_20.newFieldInGroup("pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Day", 
            "#2ND-ANN-BIRTH-DAY", FieldType.NUMERIC, 2);
        pnd_Ws_Dob_Date = localVariables.newFieldInRecord("pnd_Ws_Dob_Date", "#WS-DOB-DATE", FieldType.NUMERIC, 8);

        pnd_Ws_Dob_Date__R_Field_21 = localVariables.newGroupInRecord("pnd_Ws_Dob_Date__R_Field_21", "REDEFINE", pnd_Ws_Dob_Date);
        pnd_Ws_Dob_Date_Pnd_Ws_Dob_Yr = pnd_Ws_Dob_Date__R_Field_21.newFieldInGroup("pnd_Ws_Dob_Date_Pnd_Ws_Dob_Yr", "#WS-DOB-YR", FieldType.NUMERIC, 
            4);
        pnd_Ws_Dob_Date_Pnd_Ws_Dob_Mo = pnd_Ws_Dob_Date__R_Field_21.newFieldInGroup("pnd_Ws_Dob_Date_Pnd_Ws_Dob_Mo", "#WS-DOB-MO", FieldType.NUMERIC, 
            2);
        pnd_Ws_Dob_Date_Pnd_Ws_Dob_Da = pnd_Ws_Dob_Date__R_Field_21.newFieldInGroup("pnd_Ws_Dob_Date_Pnd_Ws_Dob_Da", "#WS-DOB-DA", FieldType.NUMERIC, 
            2);
        pnd_Ws_Age = localVariables.newFieldInRecord("pnd_Ws_Age", "#WS-AGE", FieldType.NUMERIC, 3);
        pnd_Next_Month = localVariables.newFieldInRecord("pnd_Next_Month", "#NEXT-MONTH", FieldType.NUMERIC, 2);
        pnd_Beneficiary_Birth_Date_A = localVariables.newFieldInRecord("pnd_Beneficiary_Birth_Date_A", "#BENEFICIARY-BIRTH-DATE-A", FieldType.STRING, 
            8);

        pnd_Beneficiary_Birth_Date_A__R_Field_22 = localVariables.newGroupInRecord("pnd_Beneficiary_Birth_Date_A__R_Field_22", "REDEFINE", pnd_Beneficiary_Birth_Date_A);
        pnd_Beneficiary_Birth_Date_A_Pnd_Beneficiary_Birth_Date_N8 = pnd_Beneficiary_Birth_Date_A__R_Field_22.newFieldInGroup("pnd_Beneficiary_Birth_Date_A_Pnd_Beneficiary_Birth_Date_N8", 
            "#BENEFICIARY-BIRTH-DATE-N8", FieldType.NUMERIC, 8);

        pnd_Beneficiary_Birth_Date_A__R_Field_23 = localVariables.newGroupInRecord("pnd_Beneficiary_Birth_Date_A__R_Field_23", "REDEFINE", pnd_Beneficiary_Birth_Date_A);
        pnd_Beneficiary_Birth_Date_A_Pnd_Beneficiary_Birth_Year = pnd_Beneficiary_Birth_Date_A__R_Field_23.newFieldInGroup("pnd_Beneficiary_Birth_Date_A_Pnd_Beneficiary_Birth_Year", 
            "#BENEFICIARY-BIRTH-YEAR", FieldType.NUMERIC, 4);
        pnd_Beneficiary_Birth_Date_A_Pnd_Beneficiary_Birth_Month = pnd_Beneficiary_Birth_Date_A__R_Field_23.newFieldInGroup("pnd_Beneficiary_Birth_Date_A_Pnd_Beneficiary_Birth_Month", 
            "#BENEFICIARY-BIRTH-MONTH", FieldType.NUMERIC, 2);
        pnd_Beneficiary_Birth_Date_A_Pnd_Beneficiary_Birth_Day = pnd_Beneficiary_Birth_Date_A__R_Field_23.newFieldInGroup("pnd_Beneficiary_Birth_Date_A_Pnd_Beneficiary_Birth_Day", 
            "#BENEFICIARY-BIRTH-DAY", FieldType.NUMERIC, 2);
        pnd_Ws_Other_Ann_Birth_Date_A = localVariables.newFieldInRecord("pnd_Ws_Other_Ann_Birth_Date_A", "#WS-OTHER-ANN-BIRTH-DATE-A", FieldType.STRING, 
            8);

        pnd_Ws_Other_Ann_Birth_Date_A__R_Field_24 = localVariables.newGroupInRecord("pnd_Ws_Other_Ann_Birth_Date_A__R_Field_24", "REDEFINE", pnd_Ws_Other_Ann_Birth_Date_A);
        pnd_Ws_Other_Ann_Birth_Date_A_Pnd_Ws_Other_Ann_Birth_Date_N8 = pnd_Ws_Other_Ann_Birth_Date_A__R_Field_24.newFieldInGroup("pnd_Ws_Other_Ann_Birth_Date_A_Pnd_Ws_Other_Ann_Birth_Date_N8", 
            "#WS-OTHER-ANN-BIRTH-DATE-N8", FieldType.NUMERIC, 8);

        pnd_Ws_Other_Ann_Birth_Date_A__R_Field_25 = localVariables.newGroupInRecord("pnd_Ws_Other_Ann_Birth_Date_A__R_Field_25", "REDEFINE", pnd_Ws_Other_Ann_Birth_Date_A);
        pnd_Ws_Other_Ann_Birth_Date_A_Pnd_Ws_Other_Ann_Birth_Year = pnd_Ws_Other_Ann_Birth_Date_A__R_Field_25.newFieldInGroup("pnd_Ws_Other_Ann_Birth_Date_A_Pnd_Ws_Other_Ann_Birth_Year", 
            "#WS-OTHER-ANN-BIRTH-YEAR", FieldType.NUMERIC, 4);
        pnd_Ws_Other_Ann_Birth_Date_A_Pnd_Ws_Other_Ann_Birth_Month = pnd_Ws_Other_Ann_Birth_Date_A__R_Field_25.newFieldInGroup("pnd_Ws_Other_Ann_Birth_Date_A_Pnd_Ws_Other_Ann_Birth_Month", 
            "#WS-OTHER-ANN-BIRTH-MONTH", FieldType.NUMERIC, 2);
        pnd_Ws_Other_Ann_Birth_Date_A_Pnd_Ws_Other_Ann_Birth_Day = pnd_Ws_Other_Ann_Birth_Date_A__R_Field_25.newFieldInGroup("pnd_Ws_Other_Ann_Birth_Date_A_Pnd_Ws_Other_Ann_Birth_Day", 
            "#WS-OTHER-ANN-BIRTH-DAY", FieldType.NUMERIC, 2);
        pnd_Ws_1st_Ann_Age_N = localVariables.newFieldInRecord("pnd_Ws_1st_Ann_Age_N", "#WS-1ST-ANN-AGE-N", FieldType.NUMERIC, 3);

        pnd_Ws_1st_Ann_Age_N__R_Field_26 = localVariables.newGroupInRecord("pnd_Ws_1st_Ann_Age_N__R_Field_26", "REDEFINE", pnd_Ws_1st_Ann_Age_N);
        pnd_Ws_1st_Ann_Age_N_Pnd_Ws_1st_Ann_Age_A = pnd_Ws_1st_Ann_Age_N__R_Field_26.newFieldInGroup("pnd_Ws_1st_Ann_Age_N_Pnd_Ws_1st_Ann_Age_A", "#WS-1ST-ANN-AGE-A", 
            FieldType.STRING, 3);
        pnd_Ws_Other_Ann_Age_N = localVariables.newFieldInRecord("pnd_Ws_Other_Ann_Age_N", "#WS-OTHER-ANN-AGE-N", FieldType.NUMERIC, 3);

        pnd_Ws_Other_Ann_Age_N__R_Field_27 = localVariables.newGroupInRecord("pnd_Ws_Other_Ann_Age_N__R_Field_27", "REDEFINE", pnd_Ws_Other_Ann_Age_N);
        pnd_Ws_Other_Ann_Age_N_Pnd_Ws_Other_Ann_Age_A = pnd_Ws_Other_Ann_Age_N__R_Field_27.newFieldInGroup("pnd_Ws_Other_Ann_Age_N_Pnd_Ws_Other_Ann_Age_A", 
            "#WS-OTHER-ANN-AGE-A", FieldType.STRING, 3);
        pnd_Ws2_1st_Ann_Age_N = localVariables.newFieldInRecord("pnd_Ws2_1st_Ann_Age_N", "#WS2-1ST-ANN-AGE-N", FieldType.NUMERIC, 3);
        pnd_Md_Other_Ann_Age_N = localVariables.newFieldInRecord("pnd_Md_Other_Ann_Age_N", "#MD-OTHER-ANN-AGE-N", FieldType.NUMERIC, 3);

        pnd_Mdib_Age_Table = localVariables.newGroupInRecord("pnd_Mdib_Age_Table", "#MDIB-AGE-TABLE");
        pnd_Mdib_Age_Table_Pnd_Mdib_Age_1 = pnd_Mdib_Age_Table.newFieldInGroup("pnd_Mdib_Age_Table_Pnd_Mdib_Age_1", "#MDIB-AGE-1", FieldType.STRING, 60);
        pnd_Mdib_Age_Table_Pnd_Mdib_Age_2 = pnd_Mdib_Age_Table.newFieldInGroup("pnd_Mdib_Age_Table_Pnd_Mdib_Age_2", "#MDIB-AGE-2", FieldType.STRING, 60);
        pnd_Mdib_Age_Table_Pnd_Mdib_Age_3 = pnd_Mdib_Age_Table.newFieldInGroup("pnd_Mdib_Age_Table_Pnd_Mdib_Age_3", "#MDIB-AGE-3", FieldType.STRING, 18);

        pnd_Mdib_Age_Table__R_Field_28 = localVariables.newGroupInRecord("pnd_Mdib_Age_Table__R_Field_28", "REDEFINE", pnd_Mdib_Age_Table);
        pnd_Mdib_Age_Table_Pnd_Mdib_Age = pnd_Mdib_Age_Table__R_Field_28.newFieldArrayInGroup("pnd_Mdib_Age_Table_Pnd_Mdib_Age", "#MDIB-AGE", FieldType.NUMERIC, 
            3, new DbsArrayController(1, 46));

        pnd_Mdib_Value_Table = localVariables.newGroupInRecord("pnd_Mdib_Value_Table", "#MDIB-VALUE-TABLE");
        pnd_Mdib_Value_Table_Pnd_Mdib_Value_1 = pnd_Mdib_Value_Table.newFieldInGroup("pnd_Mdib_Value_Table_Pnd_Mdib_Value_1", "#MDIB-VALUE-1", FieldType.STRING, 
            60);
        pnd_Mdib_Value_Table_Pnd_Mdib_Value_2 = pnd_Mdib_Value_Table.newFieldInGroup("pnd_Mdib_Value_Table_Pnd_Mdib_Value_2", "#MDIB-VALUE-2", FieldType.STRING, 
            60);
        pnd_Mdib_Value_Table_Pnd_Mdib_Value_3 = pnd_Mdib_Value_Table.newFieldInGroup("pnd_Mdib_Value_Table_Pnd_Mdib_Value_3", "#MDIB-VALUE-3", FieldType.STRING, 
            18);

        pnd_Mdib_Value_Table__R_Field_29 = localVariables.newGroupInRecord("pnd_Mdib_Value_Table__R_Field_29", "REDEFINE", pnd_Mdib_Value_Table);
        pnd_Mdib_Value_Table_Pnd_Mdib_Value = pnd_Mdib_Value_Table__R_Field_29.newFieldArrayInGroup("pnd_Mdib_Value_Table_Pnd_Mdib_Value", "#MDIB-VALUE", 
            FieldType.NUMERIC, 3, 1, new DbsArrayController(1, 46));

        pnd_Mdib_Diff_Table = localVariables.newGroupInRecord("pnd_Mdib_Diff_Table", "#MDIB-DIFF-TABLE");
        pnd_Mdib_Diff_Table_Pnd_Mdib_Diff_1 = pnd_Mdib_Diff_Table.newFieldInGroup("pnd_Mdib_Diff_Table_Pnd_Mdib_Diff_1", "#MDIB-DIFF-1", FieldType.STRING, 
            60);
        pnd_Mdib_Diff_Table_Pnd_Mdib_Diff_2 = pnd_Mdib_Diff_Table.newFieldInGroup("pnd_Mdib_Diff_Table_Pnd_Mdib_Diff_2", "#MDIB-DIFF-2", FieldType.STRING, 
            45);

        pnd_Mdib_Diff_Table__R_Field_30 = localVariables.newGroupInRecord("pnd_Mdib_Diff_Table__R_Field_30", "REDEFINE", pnd_Mdib_Diff_Table);
        pnd_Mdib_Diff_Table_Pnd_Mdib_Diff = pnd_Mdib_Diff_Table__R_Field_30.newFieldArrayInGroup("pnd_Mdib_Diff_Table_Pnd_Mdib_Diff", "#MDIB-DIFF", FieldType.NUMERIC, 
            3, new DbsArrayController(1, 35));

        pnd_Mdib_Pct_Table = localVariables.newGroupInRecord("pnd_Mdib_Pct_Table", "#MDIB-PCT-TABLE");
        pnd_Mdib_Pct_Table_Pnd_Mdib_Pct_1 = pnd_Mdib_Pct_Table.newFieldInGroup("pnd_Mdib_Pct_Table_Pnd_Mdib_Pct_1", "#MDIB-PCT-1", FieldType.STRING, 60);
        pnd_Mdib_Pct_Table_Pnd_Mdib_Pct_2 = pnd_Mdib_Pct_Table.newFieldInGroup("pnd_Mdib_Pct_Table_Pnd_Mdib_Pct_2", "#MDIB-PCT-2", FieldType.STRING, 45);

        pnd_Mdib_Pct_Table__R_Field_31 = localVariables.newGroupInRecord("pnd_Mdib_Pct_Table__R_Field_31", "REDEFINE", pnd_Mdib_Pct_Table);
        pnd_Mdib_Pct_Table_Pnd_Mdib_Pct = pnd_Mdib_Pct_Table__R_Field_31.newFieldArrayInGroup("pnd_Mdib_Pct_Table_Pnd_Mdib_Pct", "#MDIB-PCT", FieldType.NUMERIC, 
            3, 2, new DbsArrayController(1, 35));
        pnd_Mdib_Min_Age = localVariables.newFieldInRecord("pnd_Mdib_Min_Age", "#MDIB-MIN-AGE", FieldType.NUMERIC, 3);
        pnd_Mdib_Min_Diff = localVariables.newFieldInRecord("pnd_Mdib_Min_Diff", "#MDIB-MIN-DIFF", FieldType.NUMERIC, 3);
        pnd_Mdib_Max_Age = localVariables.newFieldInRecord("pnd_Mdib_Max_Age", "#MDIB-MAX-AGE", FieldType.NUMERIC, 3);
        pnd_Mdib_Max_Diff = localVariables.newFieldInRecord("pnd_Mdib_Max_Diff", "#MDIB-MAX-DIFF", FieldType.NUMERIC, 3);
        pnd_Ws_Age_Diff = localVariables.newFieldInRecord("pnd_Ws_Age_Diff", "#WS-AGE-DIFF", FieldType.NUMERIC, 4);
        pnd_Ws_1st_Ann_Birth_Month_At_70_Half = localVariables.newFieldInRecord("pnd_Ws_1st_Ann_Birth_Month_At_70_Half", "#WS-1ST-ANN-BIRTH-MONTH-AT-70-HALF", 
            FieldType.NUMERIC, 2);
        pnd_Ws_1st_Ann_Birth_Year_At_70_Half = localVariables.newFieldInRecord("pnd_Ws_1st_Ann_Birth_Year_At_70_Half", "#WS-1ST-ANN-BIRTH-YEAR-AT-70-HALF", 
            FieldType.NUMERIC, 4);
        pnd_Ws_Expected_Life_Mdib = localVariables.newFieldInRecord("pnd_Ws_Expected_Life_Mdib", "#WS-EXPECTED-LIFE-MDIB", FieldType.NUMERIC, 4, 1);
        pnd_Ws_Expected_Life_Md = localVariables.newFieldInRecord("pnd_Ws_Expected_Life_Md", "#WS-EXPECTED-LIFE-MD", FieldType.NUMERIC, 4, 1);
        pnd_Ws_Minimum_Life_Exp = localVariables.newFieldInRecord("pnd_Ws_Minimum_Life_Exp", "#WS-MINIMUM-LIFE-EXP", FieldType.NUMERIC, 4, 1);
        pnd_Ws_Current_Guar = localVariables.newFieldInRecord("pnd_Ws_Current_Guar", "#WS-CURRENT-GUAR", FieldType.NUMERIC, 4, 1);
        pnd_Ws_1st_Ann_Age_Mdib = localVariables.newFieldInRecord("pnd_Ws_1st_Ann_Age_Mdib", "#WS-1ST-ANN-AGE-MDIB", FieldType.NUMERIC, 3);
        pnd_Ws_2nd_Ann_Age_Mdib = localVariables.newFieldInRecord("pnd_Ws_2nd_Ann_Age_Mdib", "#WS-2ND-ANN-AGE-MDIB", FieldType.NUMERIC, 3);
        pnd_Ws_Diff_1st_And_2nd_Ann_X = localVariables.newFieldInRecord("pnd_Ws_Diff_1st_And_2nd_Ann_X", "#WS-DIFF-1ST-AND-2ND-ANN-X", FieldType.NUMERIC, 
            4);

        pnd_Ws_Diff_1st_And_2nd_Ann_X__R_Field_32 = localVariables.newGroupInRecord("pnd_Ws_Diff_1st_And_2nd_Ann_X__R_Field_32", "REDEFINE", pnd_Ws_Diff_1st_And_2nd_Ann_X);
        pnd_Ws_Diff_1st_And_2nd_Ann_X_Pnd_Ws_Diff_1st_And_2nd_Ann_1st = pnd_Ws_Diff_1st_And_2nd_Ann_X__R_Field_32.newFieldInGroup("pnd_Ws_Diff_1st_And_2nd_Ann_X_Pnd_Ws_Diff_1st_And_2nd_Ann_1st", 
            "#WS-DIFF-1ST-AND-2ND-ANN-1ST", FieldType.NUMERIC, 1);
        pnd_Ws_Diff_1st_And_2nd_Ann_X_Pnd_Ws_Diff_1st_And_2nd_Ann = pnd_Ws_Diff_1st_And_2nd_Ann_X__R_Field_32.newFieldInGroup("pnd_Ws_Diff_1st_And_2nd_Ann_X_Pnd_Ws_Diff_1st_And_2nd_Ann", 
            "#WS-DIFF-1ST-AND-2ND-ANN", FieldType.NUMERIC, 3);
        pnd_Ws_Mdib_Value_Index = localVariables.newFieldInRecord("pnd_Ws_Mdib_Value_Index", "#WS-MDIB-VALUE-INDEX", FieldType.NUMERIC, 3);
        pnd_Ws_Mdib_Diff_Index = localVariables.newFieldInRecord("pnd_Ws_Mdib_Diff_Index", "#WS-MDIB-DIFF-INDEX", FieldType.NUMERIC, 3);
        pnd_Ws_Mdib_Appl_Pct = localVariables.newFieldInRecord("pnd_Ws_Mdib_Appl_Pct", "#WS-MDIB-APPL-PCT", FieldType.NUMERIC, 3, 2);
        pnd_Ws_Pct_To_2nd_Ann = localVariables.newFieldInRecord("pnd_Ws_Pct_To_2nd_Ann", "#WS-PCT-TO-2ND-ANN", FieldType.NUMERIC, 3, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 6);
        pnd_Ws_Guar_Year = localVariables.newFieldInRecord("pnd_Ws_Guar_Year", "#WS-GUAR-YEAR", FieldType.NUMERIC, 2);
        pnd_Ws_Guar_Month = localVariables.newFieldInRecord("pnd_Ws_Guar_Month", "#WS-GUAR-MONTH", FieldType.NUMERIC, 2);
        actual_Snd_Age = localVariables.newFieldInRecord("actual_Snd_Age", "ACTUAL-SND-AGE", FieldType.BOOLEAN, 1);
        pnd_Age_In_Years_2nd_Ann = localVariables.newFieldInRecord("pnd_Age_In_Years_2nd_Ann", "#AGE-IN-YEARS-2ND-ANN", FieldType.NUMERIC, 5, 1);
        pnd_Age_In_Years_1st_Ann = localVariables.newFieldInRecord("pnd_Age_In_Years_1st_Ann", "#AGE-IN-YEARS-1ST-ANN", FieldType.NUMERIC, 5, 1);
        pnd_Complete_Years_1st_Ann = localVariables.newFieldInRecord("pnd_Complete_Years_1st_Ann", "#COMPLETE-YEARS-1ST-ANN", FieldType.NUMERIC, 4, 1);
        pnd_Complete_Years_2nd_Ann = localVariables.newFieldInRecord("pnd_Complete_Years_2nd_Ann", "#COMPLETE-YEARS-2ND-ANN", FieldType.NUMERIC, 4, 1);
        pnd_Diff_In_Years = localVariables.newFieldInRecord("pnd_Diff_In_Years", "#DIFF-IN-YEARS", FieldType.NUMERIC, 3);
        pnd_Number_Of_Years_Less_Than_70 = localVariables.newFieldInRecord("pnd_Number_Of_Years_Less_Than_70", "#NUMBER-OF-YEARS-LESS-THAN-70", FieldType.NUMERIC, 
            3);
        pnd_Pct_1st_Ann_Birth_Date_A = localVariables.newFieldInRecord("pnd_Pct_1st_Ann_Birth_Date_A", "#PCT-1ST-ANN-BIRTH-DATE-A", FieldType.STRING, 
            8);

        pnd_Pct_1st_Ann_Birth_Date_A__R_Field_33 = localVariables.newGroupInRecord("pnd_Pct_1st_Ann_Birth_Date_A__R_Field_33", "REDEFINE", pnd_Pct_1st_Ann_Birth_Date_A);
        pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Date_N8 = pnd_Pct_1st_Ann_Birth_Date_A__R_Field_33.newFieldInGroup("pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Date_N8", 
            "#PCT-1ST-ANN-BIRTH-DATE-N8", FieldType.NUMERIC, 8);

        pnd_Pct_1st_Ann_Birth_Date_A__R_Field_34 = localVariables.newGroupInRecord("pnd_Pct_1st_Ann_Birth_Date_A__R_Field_34", "REDEFINE", pnd_Pct_1st_Ann_Birth_Date_A);
        pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Yyyy = pnd_Pct_1st_Ann_Birth_Date_A__R_Field_34.newFieldInGroup("pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Yyyy", 
            "#PCT-1ST-ANN-BIRTH-YYYY", FieldType.STRING, 4);
        pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Mmdd = pnd_Pct_1st_Ann_Birth_Date_A__R_Field_34.newFieldInGroup("pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Mmdd", 
            "#PCT-1ST-ANN-BIRTH-MMDD", FieldType.STRING, 4);

        pnd_Pct_1st_Ann_Birth_Date_A__R_Field_35 = localVariables.newGroupInRecord("pnd_Pct_1st_Ann_Birth_Date_A__R_Field_35", "REDEFINE", pnd_Pct_1st_Ann_Birth_Date_A);
        pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Year = pnd_Pct_1st_Ann_Birth_Date_A__R_Field_35.newFieldInGroup("pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Year", 
            "#PCT-1ST-ANN-BIRTH-YEAR", FieldType.NUMERIC, 4);
        pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Month = pnd_Pct_1st_Ann_Birth_Date_A__R_Field_35.newFieldInGroup("pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Month", 
            "#PCT-1ST-ANN-BIRTH-MONTH", FieldType.NUMERIC, 2);
        pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Day = pnd_Pct_1st_Ann_Birth_Date_A__R_Field_35.newFieldInGroup("pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Day", 
            "#PCT-1ST-ANN-BIRTH-DAY", FieldType.NUMERIC, 2);
        pnd_Pct_2nd_Ann_Birth_Date_A = localVariables.newFieldInRecord("pnd_Pct_2nd_Ann_Birth_Date_A", "#PCT-2ND-ANN-BIRTH-DATE-A", FieldType.STRING, 
            8);

        pnd_Pct_2nd_Ann_Birth_Date_A__R_Field_36 = localVariables.newGroupInRecord("pnd_Pct_2nd_Ann_Birth_Date_A__R_Field_36", "REDEFINE", pnd_Pct_2nd_Ann_Birth_Date_A);
        pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Date_N8 = pnd_Pct_2nd_Ann_Birth_Date_A__R_Field_36.newFieldInGroup("pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Date_N8", 
            "#PCT-2ND-ANN-BIRTH-DATE-N8", FieldType.NUMERIC, 8);

        pnd_Pct_2nd_Ann_Birth_Date_A__R_Field_37 = localVariables.newGroupInRecord("pnd_Pct_2nd_Ann_Birth_Date_A__R_Field_37", "REDEFINE", pnd_Pct_2nd_Ann_Birth_Date_A);
        pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Yyyy = pnd_Pct_2nd_Ann_Birth_Date_A__R_Field_37.newFieldInGroup("pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Yyyy", 
            "#PCT-2ND-ANN-BIRTH-YYYY", FieldType.STRING, 4);
        pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Mmdd = pnd_Pct_2nd_Ann_Birth_Date_A__R_Field_37.newFieldInGroup("pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Mmdd", 
            "#PCT-2ND-ANN-BIRTH-MMDD", FieldType.STRING, 4);

        pnd_Pct_2nd_Ann_Birth_Date_A__R_Field_38 = localVariables.newGroupInRecord("pnd_Pct_2nd_Ann_Birth_Date_A__R_Field_38", "REDEFINE", pnd_Pct_2nd_Ann_Birth_Date_A);
        pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Year = pnd_Pct_2nd_Ann_Birth_Date_A__R_Field_38.newFieldInGroup("pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Year", 
            "#PCT-2ND-ANN-BIRTH-YEAR", FieldType.NUMERIC, 4);
        pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Month = pnd_Pct_2nd_Ann_Birth_Date_A__R_Field_38.newFieldInGroup("pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Month", 
            "#PCT-2ND-ANN-BIRTH-MONTH", FieldType.NUMERIC, 2);
        pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Day = pnd_Pct_2nd_Ann_Birth_Date_A__R_Field_38.newFieldInGroup("pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Day", 
            "#PCT-2ND-ANN-BIRTH-DAY", FieldType.NUMERIC, 2);
        pnd_Temp_Retrn_Msg_1stann90 = localVariables.newFieldInRecord("pnd_Temp_Retrn_Msg_1stann90", "#TEMP-RETRN-MSG-1STANN90", FieldType.STRING, 30);
        pnd_Temp_Retrn_Msg_2ndann90 = localVariables.newFieldInRecord("pnd_Temp_Retrn_Msg_2ndann90", "#TEMP-RETRN-MSG-2NDANN90", FieldType.STRING, 30);
        pnd_Temp_Retrn_Msg_Bothann90 = localVariables.newFieldInRecord("pnd_Temp_Retrn_Msg_Bothann90", "#TEMP-RETRN-MSG-BOTHANN90", FieldType.STRING, 
            30);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_uta.reset();
        vw_cwf_State_Age_Tbl.reset();
        vw_cwf_Product_Tbl.reset();

        ldaAial034.initializeValues();

        localVariables.reset();
        pnd_Tbl_Age_Limit.setInitialValue(90);
        pnd_Mdib_Age_Table_Pnd_Mdib_Age_1.setInitialValue("070071072073074075076077078079080081082083084085086087088089");
        pnd_Mdib_Age_Table_Pnd_Mdib_Age_2.setInitialValue("090091092093094095096097098099100101102103104105106107108109");
        pnd_Mdib_Age_Table_Pnd_Mdib_Age_3.setInitialValue("110111112113114115");
        pnd_Mdib_Value_Table_Pnd_Mdib_Value_1.setInitialValue("274265256247238229220212203195187179171163155148141134127120");
        pnd_Mdib_Value_Table_Pnd_Mdib_Value_2.setInitialValue("114108102096091086081076071067063059055052049045042039037034");
        pnd_Mdib_Value_Table_Pnd_Mdib_Value_3.setInitialValue("031029026024021019");
        pnd_Mdib_Diff_Table_Pnd_Mdib_Diff_1.setInitialValue("010011012013014015016017018019020021022023024025026027028029");
        pnd_Mdib_Diff_Table_Pnd_Mdib_Diff_2.setInitialValue("030031032033034035036037038039040041042043044");
        pnd_Mdib_Pct_Table_Pnd_Mdib_Pct_1.setInitialValue("100096093090087084082079077075073072070068067066064063062061");
        pnd_Mdib_Pct_Table_Pnd_Mdib_Pct_2.setInitialValue("060059059058057056056055055054054053053053052");
        pnd_Mdib_Min_Age.setInitialValue(70);
        pnd_Mdib_Min_Diff.setInitialValue(10);
        pnd_Mdib_Max_Age.setInitialValue(115);
        pnd_Mdib_Max_Diff.setInitialValue(44);
        pnd_Ws_Expected_Life_Mdib.setInitialValue(0);
        pnd_Ws_Expected_Life_Md.setInitialValue(0);
        actual_Snd_Age.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Aian033() throws Exception
    {
        super("Aian033");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* ***********************************************************************
        //* ***********************************************************************
        //* ***********************************************************************
        //* ***                        MAIN ROUTINE
        //* ***********************************************************************
        //*  WRITE '**** ENTERING AIAN033 *****'
        //*  PERFORM DISPLAY-AIAN033-PARAMETERS
        pnd_Ws_Other_Ann_Birth_Date_A.setValue("00000000");                                                                                                               //Natural: MOVE '00000000' TO #WS-OTHER-ANN-BIRTH-DATE-A
        pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Output_Data().reset();                                                                                             //Natural: RESET #AIAN033-OUTPUT-DATA
        pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar().setValue(300);                                                                                  //Natural: MOVE 300 TO #AIAN033-RETURN-CODE-GUAR
        //*   TREP
        //*   TREP
        pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm().setValue(Global.getPROGRAM());                                                              //Natural: MOVE *PROGRAM TO #AIAN033-RETURN-CODE-GUAR-PGM NAZ-ACT-RETURN-CODE-PGM
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue(Global.getPROGRAM());
        pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct().setValue(300);                                                                                   //Natural: MOVE 300 TO #AIAN033-RETURN-CODE-PCT
        //*   TREP
        //*   TREP
        pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_Pgm().setValue(Global.getPROGRAM());                                                               //Natural: MOVE *PROGRAM TO #AIAN033-RETURN-CODE-PCT-PGM NAZ-ACT-RETURN-CODE-PGM
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue(Global.getPROGRAM());
                                                                                                                                                                          //Natural: PERFORM FIND-GUARANTEED-PERIOD-AND-PCT-TO-SEC-ANN
        sub_Find_Guaranteed_Period_And_Pct_To_Sec_Ann();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM MOVE-DATES-INTO-NUMERIC-FIELDS
        sub_Move_Dates_Into_Numeric_Fields();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM IS-THERE-A-2ND-ANN-OR-BEN
        sub_Is_There_A_2nd_Ann_Or_Ben();
        if (condition(Global.isEscape())) {return;}
        //*  DY7 FIX BEGINS >>
        if (condition(((pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Origin_Cde().equals(35) || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Origin_Cde().equals(36))   //Natural: IF ( NAZ-ACT-ORIGIN-CDE = 35 OR = 36 ) AND ( #AIAN033-OPTION-CODE = 21 )
            && pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(21))))
        {
            //*   DY2
                                                                                                                                                                          //Natural: PERFORM CONVERT-IAORIGIN-TO-PRODUCT-CDE
            sub_Convert_Iaorigin_To_Product_Cde();
            if (condition(Global.isEscape())) {return;}
            //*   DY2
                                                                                                                                                                          //Natural: PERFORM GET-ANNUITY-START-AGE
            sub_Get_Annuity_Start_Age();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  DY7 FIX ENDS   <<
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-ERRORS-IN-DATA
        sub_Check_For_Errors_In_Data();
        if (condition(Global.isEscape())) {return;}
        //*   WRITE '=' #AIAN033-RETURN-CODE-GUAR
        if (condition(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar().notEquals(300)))                                                                  //Natural: IF #AIAN033-RETURN-CODE-GUAR NE 300
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE '=' #WS-GUAR-YEAR '=' #WS-GUAR-MONTH
        if (condition(pnd_Ws_Guar_Year.greater(getZero()) || pnd_Ws_Guar_Month.greater(getZero())))                                                                       //Natural: IF #WS-GUAR-YEAR > 0 OR #WS-GUAR-MONTH > 0
        {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-AGES-FOR-MINIMUM-DISTRIBUTION
            sub_Calculate_Ages_For_Minimum_Distribution();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CONVERT-INPUT-DATA-FOR-IRS-TABLE
            sub_Convert_Input_Data_For_Irs_Table();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM READ-IRS-TABLE
            sub_Read_Irs_Table();
            if (condition(Global.isEscape())) {return;}
            //*  NBC 08/09
            //*   WRITE '=' #AIAN033-OPTION-CODE
            //*         '=' #AIAN033-BEN-TYPE '=' #AIAN033-2ND-ANN-TYPE
            //*         '=' #WS-AGE-DIFF
            //*  MDRI
            if (condition(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(21)))                                                                       //Natural: IF #AIAN033-OPTION-CODE = 21
            {
                if (condition((pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Ben_Type().equals("S") && pnd_Ws_Age_Diff.greater(10)) || (pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Type().equals("S")  //Natural: IF ( #AIAN033-BEN-TYPE = 'S' AND #WS-AGE-DIFF > 10 ) OR ( #AIAN033-2ND-ANN-TYPE = 'S' AND #WS-AGE-DIFF > 10 )
                    && pnd_Ws_Age_Diff.greater(10))))
                {
                                                                                                                                                                          //Natural: PERFORM READ-IRS-TABLE
                    sub_Read_Irs_Table();
                    if (condition(Global.isEscape())) {return;}
                    //*     WRITE 'N033: I AM READING JOINT & LAST SURV TABLE'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM READ-MDIB-TABLE-FOR-LIFE-EXP
                    sub_Read_Mdib_Table_For_Life_Exp();
                    if (condition(Global.isEscape())) {return;}
                    //*     WRITE 'N033: I AM READING UNIFORM           TABLE'
                }                                                                                                                                                         //Natural: END-IF
                //*  MDRI
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  MDRI
                                                                                                                                                                          //Natural: PERFORM READ-MDIB-TABLE-FOR-LIFE-EXP
                sub_Read_Mdib_Table_For_Life_Exp();
                if (condition(Global.isEscape())) {return;}
                //*     WRITE 'N033: I AM READING UNIFORM           TABLE'
                //*  MDRI
            }                                                                                                                                                             //Natural: END-IF
            //*  WRITE '=' #AIAN033-MD-EXP-LIFE
            //*  CODE BELOW COMMENTED OUT NBC 8/09
            //*  CODE ABOVE IS REPLACEMENT CODE
            //*  LINES WITH ** HAD PREVIOUSLY BEEN COMMENTED OUT
            //*  << DY FIX 6/8/06
            //*  DECIDE ON FIRST VALUE OF #AIAN033-OPTION-CODE
            //*    VALUE 17, 15, 12, 3, 16, 14, 11, 4, 8, 13, 10, 7,
            //*          54, 55, 56, 57                             /* JOINT LIFE
            //* *     IF #AIAN033-2ND-ANN-TYPE = 'N' OR #AIAN033-BEN-TYPE = 'N'
            //* *        PERFORM READ-MDIB-TABLE-FOR-LIFE-EXP
            //* *     END-IF
            //* *     IF #AIAN033-2ND-ANN-TYPE = 'S' AND (#WS-AGE-DIFF < 10
            //* *                                    AND  #WS-AGE-DIFF > 0)
            //* *        PERFORM READ-MDIB-TABLE-FOR-LIFE-EXP
            //*       IF #AIAN033-2ND-ANN-TYPE = 'S' AND #WS-AGE-DIFF > 10
            //*          PERFORM READ-IRS-TABLE
            //*       ELSE
            //*          PERFORM READ-MDIB-TABLE-FOR-LIFE-EXP
            //*       END-IF
            //*     NONE
            //*       PERFORM READ-MDIB-TABLE-FOR-LIFE-EXP
            //*       WRITE '** SLA OPTION SELECTED ***'
            //*   END-DECIDE
            //* *IF #AIAN033-2ND-ANN-TYPE = 'N' OR #AIAN033-BEN-TYPE = 'N'
            //* *  PERFORM READ-MDIB-TABLE-FOR-LIFE-EXP
            //* *END-IF
            //* *  DY FIX ENDS >>
                                                                                                                                                                          //Natural: PERFORM COMPARE-MD-WITH-MDIB
            sub_Compare_Md_With_Mdib();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  DY6
        //*  DY6
        if (condition(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Settlement_Type().equals("M") || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Settlement_Type().equals("S"))) //Natural: IF #AIAN033-SETTLEMENT-TYPE = 'M' OR = 'S'
        {
            ignore();
            //*  DY6
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Ws_Pct_To_2nd_Ann.greater(getZero()) && (pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Type().equals("N") || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Ben_Type().equals("N")))) //Natural: IF #WS-PCT-TO-2ND-ANN > 0 AND ( #AIAN033-2ND-ANN-TYPE = 'N' OR #AIAN033-BEN-TYPE = 'N' )
            {
                                                                                                                                                                          //Natural: PERFORM READ-MDIB-TABLE-FOR-PCT-ALLOWED
                sub_Read_Mdib_Table_For_Pct_Allowed();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM COMPARE-PCT-WITH-MDIB
                sub_Compare_Pct_With_Mdib();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  DY6
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE '**** LEAVING  AIAN033 *****'
        //*   PERFORM DISPLAY-AIAN033-PARAMETERS
        //*  WRITE '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-IAORIGIN-TO-PRODUCT-CDE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ANNUITY-START-AGE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-ERRORS-IN-DATA
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-DATES-INTO-NUMERIC-FIELDS
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IS-THERE-A-2ND-ANN-OR-BEN
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FIND-GUARANTEED-PERIOD-AND-PCT-TO-SEC-ANN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-AGES-FOR-MINIMUM-DISTRIBUTION
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-INPUT-DATA-FOR-IRS-TABLE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IRS-TABLE
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-MDIB-TABLE-FOR-LIFE-EXP
        //*     70 - #MD-1ST-ANN-AGE-N
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-MDIB-TABLE-FOR-PCT-ALLOWED
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-AGES-FOR-PCT-ALLOWED
        //* ***********************************************************************
        //*  WRITE '** CALCULATE-AGES-FOR-PCT-ALLOWED **'
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPARE-MD-WITH-MDIB
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPARE-PCT-WITH-MDIB
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-ACTUAL-AGE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-AIAN033-PARAMETERS
        //* ***********************************************************************
    }
    private void sub_Convert_Iaorigin_To_Product_Cde() throws Exception                                                                                                   //Natural: CONVERT-IAORIGIN-TO-PRODUCT-CDE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                          //Natural: ASSIGN #TBL-SCRTY-LEVEL-IND := 'A'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("CWF-WKS-WARRANT-TBL");                                                                                              //Natural: ASSIGN #TBL-TABLE-NME := 'CWF-WKS-WARRANT-TBL'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue("IA-PRODUCT-CODE");                                                                                                  //Natural: ASSIGN #TBL-KEY-FIELD := 'IA-PRODUCT-CODE'
        vw_cwf_Product_Tbl.startDatabaseRead                                                                                                                              //Natural: READ CWF-PRODUCT-TBL BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY
        (
        "READ01",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ01:
        while (condition(vw_cwf_Product_Tbl.readNextRow("READ01")))
        {
            if (condition(cwf_Product_Tbl_Tbl_Key_Prdct.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field)))                                                                  //Natural: IF CWF-PRODUCT-TBL.TBL-KEY-PRDCT NOT = #TBL-KEY-FIELD
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Origin_Cde().equals(cwf_Product_Tbl_Tbl_Prdct_Orgn_Cde_A.getValue("*"))))                         //Natural: IF NAZ-ACT-ORIGIN-CDE = CWF-PRODUCT-TBL.TBL-PRDCT-ORGN-CDE-A ( * )
            {
                pnd_Tbl_Product_Cde.setValue(cwf_Product_Tbl_Tbl_Prdct_Cde);                                                                                              //Natural: MOVE CWF-PRODUCT-TBL.TBL-PRDCT-CDE TO #TBL-PRODUCT-CDE
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  CONVERT-IAORIGIN-TO-PRODUCT-CDE
    }
    private void sub_Get_Annuity_Start_Age() throws Exception                                                                                                             //Natural: GET-ANNUITY-START-AGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Tbl_Key_Annty_Start.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "ANNTY-START-", pnd_Tbl_Product_Cde));                                           //Natural: COMPRESS 'ANNTY-START-' #TBL-PRODUCT-CDE INTO #TBL-KEY-ANNTY-START LEAVING NO SPACE
        pnd_Tbl_Prime_Key_Pnd_Tbl_Scrty_Level_Ind.setValue("A");                                                                                                          //Natural: ASSIGN #TBL-SCRTY-LEVEL-IND := 'A'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Table_Nme.setValue("CWF-WKS-WARRANT-TBL");                                                                                              //Natural: ASSIGN #TBL-TABLE-NME := 'CWF-WKS-WARRANT-TBL'
        pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field.setValue(pnd_Tbl_Key_Annty_Start);                                                                                            //Natural: ASSIGN #TBL-KEY-FIELD := #TBL-KEY-ANNTY-START
        //*  #TBL-KEY-FIELD        := 'ANNTY-START-IHA'
        vw_cwf_State_Age_Tbl.startDatabaseRead                                                                                                                            //Natural: READ CWF-STATE-AGE-TBL BY TBL-PRIME-KEY STARTING FROM #TBL-PRIME-KEY
        (
        "READ02",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ02:
        while (condition(vw_cwf_State_Age_Tbl.readNextRow("READ02")))
        {
            if (condition(cwf_State_Age_Tbl_Tbl_Key_Annty_Start.notEquals(pnd_Tbl_Prime_Key_Pnd_Tbl_Key_Field)))                                                          //Natural: IF CWF-STATE-AGE-TBL.TBL-KEY-ANNTY-START NOT = #TBL-KEY-FIELD
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(cwf_State_Age_Tbl_Tbl_State_Cde_A.getValue("*").equals(" ")))                                                                                   //Natural: IF CWF-STATE-AGE-TBL.TBL-STATE-CDE-A ( * ) = ' '
            {
                pnd_Tbl_Age_Limit.setValue(cwf_State_Age_Tbl_Tbl_Age_Limit);                                                                                              //Natural: ASSIGN #TBL-AGE-LIMIT := CWF-STATE-AGE-TBL.TBL-AGE-LIMIT
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_State_At_Issue().equals(cwf_State_Age_Tbl_Tbl_State_Cde_A.getValue("*"))))                        //Natural: IF NAZ-ACT-STATE-AT-ISSUE = CWF-STATE-AGE-TBL.TBL-STATE-CDE-A ( * )
            {
                pnd_Tbl_Age_Limit.setValue(cwf_State_Age_Tbl_Tbl_Age_Limit);                                                                                              //Natural: ASSIGN #TBL-AGE-LIMIT := CWF-STATE-AGE-TBL.TBL-AGE-LIMIT
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  #TBL-AGE-LIMIT    := 95             /* TO BE REMOVED BEFORE PROD
        //*  GET-ANNUITY-START-AGE
    }
    private void sub_Check_For_Errors_In_Data() throws Exception                                                                                                          //Natural: CHECK-FOR-ERRORS-IN-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*   DY2 FIX BEGINS >>
        //*  WRITE '*** CHECK-FOR-ERRORS-IN-DATA ***'
        pnd_Temp_Retrn_Msg_Bothann90.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "BOTH ANNUITANTS >_", pnd_Tbl_Age_Limit, " YRS 0 MOS"));                    //Natural: COMPRESS 'BOTH ANNUITANTS >_' #TBL-AGE-LIMIT ' YRS 0 MOS' INTO #TEMP-RETRN-MSG-BOTHANN90 LEAVING NO SPACE
        pnd_Temp_Retrn_Msg_1stann90.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "1ST ANNUITANT >_", pnd_Tbl_Age_Limit, " YRS 0 MOS"));                       //Natural: COMPRESS '1ST ANNUITANT >_' #TBL-AGE-LIMIT ' YRS 0 MOS' INTO #TEMP-RETRN-MSG-1STANN90 LEAVING NO SPACE
        pnd_Temp_Retrn_Msg_2ndann90.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "2ND ANNUITANT >_", pnd_Tbl_Age_Limit, " YRS 0 MOS"));                       //Natural: COMPRESS '2ND ANNUITANT >_' #TBL-AGE-LIMIT ' YRS 0 MOS' INTO #TEMP-RETRN-MSG-2NDANN90 LEAVING NO SPACE
        //*   DY2 FIX ENDS   <<
        //*  DY4  FIX BEGINS  >>
        pnd_Ws_Asd_Date_A.setValueEdited(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Asd_Date(),new ReportEditMask("YYYYMMDD"));                                        //Natural: MOVE EDITED NAZ-ACT-ASD-DATE ( EM = YYYYMMDD ) TO #WS-ASD-DATE-A
        //*  WRITE '=' #WS-ASD-DATE '=' #AIAN033-SETTLEMENT-TYPE
        if (condition(pnd_Ws_Asd_Date_A_Pnd_Ws_Asd_Date.less(20161001)))                                                                                                  //Natural: IF #WS-ASD-DATE < 20161001
        {
            //*  NON-PENSIONS */
            if (condition(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Settlement_Type().equals("M") || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Settlement_Type().equals("S"))) //Natural: IF #AIAN033-SETTLEMENT-TYPE = 'M' OR = 'S'
            {
                pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar().setValue(299);                                                                          //Natural: MOVE 299 TO #AIAN033-RETURN-CODE-GUAR
                pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm().setValue(Global.getPROGRAM());                                                      //Natural: MOVE *PROGRAM TO #AIAN033-RETURN-CODE-GUAR-PGM NAZ-ACT-RETURN-CODE-PGM
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue(Global.getPROGRAM());
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  DY4  FIX ENDS    <<
        if (condition((pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_N8.greater(getZero()) && (pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Type().notEquals("N")  //Natural: IF ( #2ND-ANN-BIRTH-DATE-N8 > 0 AND ( #AIAN033-2ND-ANN-TYPE NOT = 'N' AND #AIAN033-2ND-ANN-TYPE NOT = 'S' ) )
            && pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Type().notEquals("S")))))
        {
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar().setValue("INVALID 2ND-ANN-TYPE      ");                                                      //Natural: MOVE 'INVALID 2ND-ANN-TYPE      ' TO #AIAN033-RETURN-MSG-GUAR
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar().setValue(303);                                                                              //Natural: MOVE 303 TO #AIAN033-RETURN-CODE-GUAR
            //*   TREP
            //*   TREP
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm().setValue(Global.getPROGRAM());                                                          //Natural: MOVE *PROGRAM TO #AIAN033-RETURN-CODE-GUAR-PGM NAZ-ACT-RETURN-CODE-PGM
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue(Global.getPROGRAM());
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Beneficiary_Birth_Date_A_Pnd_Beneficiary_Birth_Date_N8.greater(getZero()) && pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Ben_Type().notEquals("N"))) //Natural: IF #BENEFICIARY-BIRTH-DATE-N8 > 0 AND #AIAN033-BEN-TYPE NOT = 'N'
        {
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar().setValue("INVALID BEN-ANN-TYPE      ");                                                      //Natural: MOVE 'INVALID BEN-ANN-TYPE      ' TO #AIAN033-RETURN-MSG-GUAR
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar().setValue(304);                                                                              //Natural: MOVE 304 TO #AIAN033-RETURN-CODE-GUAR
            //*   TREP
            //*   TREP
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm().setValue(Global.getPROGRAM());                                                          //Natural: MOVE *PROGRAM TO #AIAN033-RETURN-CODE-GUAR-PGM NAZ-ACT-RETURN-CODE-PGM
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue(Global.getPROGRAM());
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Other_Ann_Birth_Date_A_Pnd_Ws_Other_Ann_Birth_Date_N8.equals(getZero()) && pnd_Ws_Pct_To_2nd_Ann.greater(getZero())))                        //Natural: IF #WS-OTHER-ANN-BIRTH-DATE-N8 = 0 AND #WS-PCT-TO-2ND-ANN > 0
        {
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar().setValue("JOINT OPTION WITH NO 2ND  ");                                                      //Natural: MOVE 'JOINT OPTION WITH NO 2ND  ' TO #AIAN033-RETURN-MSG-GUAR
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar().setValue(305);                                                                              //Natural: MOVE 305 TO #AIAN033-RETURN-CODE-GUAR
            //*   TREP
            //*   TREP
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm().setValue(Global.getPROGRAM());                                                          //Natural: MOVE *PROGRAM TO #AIAN033-RETURN-CODE-GUAR-PGM NAZ-ACT-RETURN-CODE-PGM
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue(Global.getPROGRAM());
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  DY3 FIX BEGINS  >>
        //*  IF #AIAN033-SETTLEMENT-TYPE = 'M' OR= 'S'   /* NON-PENSIONS */
        //*   MOVE 299 TO #AIAN033-RETURN-CODE-GUAR
        //*   ESCAPE ROUTINE
        //*  END-IF
        //*  DY3 FIX ENDS    <<
        //*  NBC 08/09 CHECK TO MAKE SURE BOTH ANNUITANTS ARE NOT YET 90
        //*  IF #ISSUE-DATE-MMDD GE #PCT-1ST-ANN-BIRTH-MMDD     DY 11-23-09
        //*   DY 11-23-09
        if (condition(pnd_Issue_Date_A_Pnd_Issue_Date_Mmdd.greaterOrEqual(pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Date_Mmdd)))                                         //Natural: IF #ISSUE-DATE-MMDD GE #1ST-ANN-BIRTH-DATE-MMDD
        {
            pnd_Age_In_Years_1st_Ann.compute(new ComputeParameters(false, pnd_Age_In_Years_1st_Ann), pnd_Issue_Date_A_Pnd_Issue_Year.subtract(pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Year)); //Natural: ASSIGN #AGE-IN-YEARS-1ST-ANN := #ISSUE-YEAR - #1ST-ANN-BIRTH-YEAR
            //*  DY 11-23-09
            //*  ADDED TO ENSURE THAT 1ST ANNUITANT AGE > 90 YR 0 MOS AND
            //*  AGE < 91 YRS 0 MOS IS NOT ALLOWED TO SETTLE
            //*  IF #ISSUE-DATE-MMDD > #1ST-ANN-BIRTH-DATE-MMDD  /* GLWB
            //*    ADD 0.5 TO #AGE-IN-YEARS-1ST-ANN              /* GLWB
            //*  END-IF                                          /* GLWB
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Age_In_Years_1st_Ann.compute(new ComputeParameters(false, pnd_Age_In_Years_1st_Ann), pnd_Issue_Date_A_Pnd_Issue_Year.subtract(pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Year).subtract(1)); //Natural: ASSIGN #AGE-IN-YEARS-1ST-ANN := #ISSUE-YEAR - #1ST-ANN-BIRTH-YEAR - 1
            //*                                                     DY 11-23-09
            //*  ADD 0.5 TO #AGE-IN-YEARS-1ST-ANN                /* GLWB
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_2nd_Ann_Birth_Date_A.greater("00000000")))                                                                                                      //Natural: IF #2ND-ANN-BIRTH-DATE-A > '00000000'
        {
            //*   IF #ISSUE-DATE-MMDD GE #PCT-2ND-ANN-BIRTH-MMDD    DY 11-23-09
            //*   DY 11-23-09
            if (condition(pnd_Issue_Date_A_Pnd_Issue_Date_Mmdd.greaterOrEqual(pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_Mmdd)))                                     //Natural: IF #ISSUE-DATE-MMDD GE #2ND-ANN-BIRTH-DATE-MMDD
            {
                pnd_Age_In_Years_2nd_Ann.compute(new ComputeParameters(false, pnd_Age_In_Years_2nd_Ann), pnd_Issue_Date_A_Pnd_Issue_Year.subtract(pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Year)); //Natural: ASSIGN #AGE-IN-YEARS-2ND-ANN := #ISSUE-YEAR - #2ND-ANN-BIRTH-YEAR
                //*  DY 11-23-09
                //*  ADDED TO ENSURE THAT 2ND ANNUITANT AGE > 90 YR 0 MOS AND
                //*  AGE < 91 YRS 0 MOS IS NOT ALLOWED TO SETTLE
                //*    IF #ISSUE-DATE-MMDD > #2ND-ANN-BIRTH-DATE-MMDD /* GLWB
                //*      ADD 0.5 TO #AGE-IN-YEARS-2ND-ANN             /* GLWB
                //*    END-IF                                         /* GLWB
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Age_In_Years_2nd_Ann.compute(new ComputeParameters(false, pnd_Age_In_Years_2nd_Ann), pnd_Issue_Date_A_Pnd_Issue_Year.subtract(pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Year).subtract(1)); //Natural: ASSIGN #AGE-IN-YEARS-2ND-ANN := #ISSUE-YEAR - #2ND-ANN-BIRTH-YEAR - 1
                //*                                                     DY 11-23-09
                //*    ADD 0.5 TO #AGE-IN-YEARS-2ND-ANN               /* GLWB
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  DY5 FIX BEGINS >>
        if (condition(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Settlement_Type().equals("M") || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Settlement_Type().equals("S"))) //Natural: IF #AIAN033-SETTLEMENT-TYPE = 'M' OR = 'S'
        {
            pnd_Ws_Dob_Date.reset();                                                                                                                                      //Natural: RESET #WS-DOB-DATE
            pnd_Ws_Dob_Date.setValue(pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Date_N8);                                                                                 //Natural: MOVE #1ST-ANN-BIRTH-DATE-N8 TO #WS-DOB-DATE
                                                                                                                                                                          //Natural: PERFORM CALCULATE-ACTUAL-AGE
            sub_Calculate_Actual_Age();
            if (condition(Global.isEscape())) {return;}
            pnd_Age_In_Years_1st_Ann.setValue(pnd_Ws_Age);                                                                                                                //Natural: MOVE #WS-AGE TO #AGE-IN-YEARS-1ST-ANN
            if (condition(pnd_2nd_Ann_Birth_Date_A.greater("00000000")))                                                                                                  //Natural: IF #2ND-ANN-BIRTH-DATE-A > '00000000'
            {
                pnd_Ws_Dob_Date.reset();                                                                                                                                  //Natural: RESET #WS-DOB-DATE
                pnd_Ws_Dob_Date.setValue(pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_N8);                                                                             //Natural: MOVE #2ND-ANN-BIRTH-DATE-N8 TO #WS-DOB-DATE
                                                                                                                                                                          //Natural: PERFORM CALCULATE-ACTUAL-AGE
                sub_Calculate_Actual_Age();
                if (condition(Global.isEscape())) {return;}
                pnd_Age_In_Years_2nd_Ann.setValue(pnd_Ws_Age);                                                                                                            //Natural: MOVE #WS-AGE TO #AGE-IN-YEARS-2ND-ANN
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  DY5 FIX ENDS <<
        //*   DY 02-19-10  FIX BEGINS
        //*   ALLOW ANN > 90 TO SETTLE TO AN AC OR TPA SETTLEMENT
        //*   AC = 21; TPA = {28, 30}
        //*  GLWB FIX BEGINS >>
        //*  IF #AIAN033-OPTION-CODE = 21 OR= 28 OR= 30
        //*   ESCAPE ROUTINE
        //*  END-IF
        //* * IF NAZ-ACT-ORIGIN-CDE = 35                             DY7
        //*   DY7
        if (condition(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Settlement_Type().equals("M") || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Settlement_Type().equals("S"))) //Natural: IF #AIAN033-SETTLEMENT-TYPE = 'M' OR = 'S'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(21) || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(28)  //Natural: IF #AIAN033-OPTION-CODE = 21 OR = 28 OR = 30
                || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(30)))
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  GLWB FIX ENDS   <<
        //*  IF #AGE-IN-YEARS-1ST-ANN GE 90 AND   >> DY 11-23-09 FIX BEGINS
        //*     #AGE-IN-YEARS-2ND-ANN GE 90
        //*  IF #AGE-IN-YEARS-1ST-ANN > 90 AND
        //*     #AGE-IN-YEARS-2ND-ANN > 90             /*  DY FIX ENDS 11-23-09 <<
        //*  WRITE '=' #AGE-IN-YEARS-1ST-ANN '=' #AGE-IN-YEARS-2ND-ANN
        //*  WRITE '=' #TBL-AGE-LIMIT
        //*  DY2
        //*  DY2
        if (condition(pnd_Age_In_Years_1st_Ann.greater(pnd_Tbl_Age_Limit) && pnd_Age_In_Years_2nd_Ann.greater(pnd_Tbl_Age_Limit)))                                        //Natural: IF #AGE-IN-YEARS-1ST-ANN > #TBL-AGE-LIMIT AND #AGE-IN-YEARS-2ND-ANN > #TBL-AGE-LIMIT
        {
            //*   MOVE 'BOTH ANNUITANTS OLDER THAN 90'              DY 11-23-09
            //*  MOVE 'BOTH ANNUITANTS > 90 YRS 0 MOS'           /* DY 11-23-09
            //*  DY2
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar().setValue(pnd_Temp_Retrn_Msg_Bothann90);                                                      //Natural: MOVE #TEMP-RETRN-MSG-BOTHANN90 TO #AIAN033-RETURN-MSG-GUAR
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar().setValue(310);                                                                              //Natural: MOVE 310 TO #AIAN033-RETURN-CODE-GUAR
            //*   TREP
            //*   TREP
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm().setValue(Global.getPROGRAM());                                                          //Natural: MOVE *PROGRAM TO #AIAN033-RETURN-CODE-GUAR-PGM NAZ-ACT-RETURN-CODE-PGM
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue(Global.getPROGRAM());
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #AGE-IN-YEARS-1ST-ANN GE 90           /* DY 11-23-09
        //*  IF #AGE-IN-YEARS-1ST-ANN > 90            /* DY 11-23-09
        //*  DY2
        if (condition(pnd_Age_In_Years_1st_Ann.greater(pnd_Tbl_Age_Limit)))                                                                                               //Natural: IF #AGE-IN-YEARS-1ST-ANN > #TBL-AGE-LIMIT
        {
            //*   MOVE '1ST ANNUITANT OLDER THAN 90'      /* DY 11-23-09
            //*  MOVE '1ST ANNUITANT > 90 YRS 0 MOS'      /* DY2
            //*  DY2
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar().setValue(pnd_Temp_Retrn_Msg_1stann90);                                                       //Natural: MOVE #TEMP-RETRN-MSG-1STANN90 TO #AIAN033-RETURN-MSG-GUAR
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar().setValue(308);                                                                              //Natural: MOVE 308 TO #AIAN033-RETURN-CODE-GUAR
            //*   TREP
            //*   TREP
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm().setValue(Global.getPROGRAM());                                                          //Natural: MOVE *PROGRAM TO #AIAN033-RETURN-CODE-GUAR-PGM NAZ-ACT-RETURN-CODE-PGM
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue(Global.getPROGRAM());
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #AGE-IN-YEARS-2ND-ANN GE 90           /* DY 11-23-09
        //*  IF #AGE-IN-YEARS-2ND-ANN >  90           /* DY 11-23-09
        //*  DY2
        if (condition(pnd_Age_In_Years_2nd_Ann.greater(pnd_Tbl_Age_Limit)))                                                                                               //Natural: IF #AGE-IN-YEARS-2ND-ANN > #TBL-AGE-LIMIT
        {
            //*   MOVE '2ND ANNUITANT OLDER THAN 90'      /* DY 11-23-09
            //*   MOVE '2ND ANNUITANT > 90 YRS 0 MOS'     /* DY 11-23-09
            //*  DY2
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar().setValue(pnd_Temp_Retrn_Msg_2ndann90);                                                       //Natural: MOVE #TEMP-RETRN-MSG-2NDANN90 TO #AIAN033-RETURN-MSG-GUAR
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar().setValue(309);                                                                              //Natural: MOVE 309 TO #AIAN033-RETURN-CODE-GUAR
            //*   TREP
            //*   TREP
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm().setValue(Global.getPROGRAM());                                                          //Natural: MOVE *PROGRAM TO #AIAN033-RETURN-CODE-GUAR-PGM NAZ-ACT-RETURN-CODE-PGM
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue(Global.getPROGRAM());
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  GLWB FIX BEGINS >>
        if (condition(pnd_Age_In_Years_1st_Ann.equals(pnd_Tbl_Age_Limit)))                                                                                                //Natural: IF #AGE-IN-YEARS-1ST-ANN = #TBL-AGE-LIMIT
        {
            pnd_Next_Month.compute(new ComputeParameters(false, pnd_Next_Month), pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Month.add(1));                                //Natural: ASSIGN #NEXT-MONTH := #1ST-ANN-BIRTH-MONTH + 1
            if (condition(pnd_Next_Month.greater(12)))                                                                                                                    //Natural: IF #NEXT-MONTH > 12
            {
                pnd_Next_Month.setValue(1);                                                                                                                               //Natural: ASSIGN #NEXT-MONTH := 1
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet839 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ( #1ST-ANN-BIRTH-MONTH = #ISSUE-MONTH AND #1ST-ANN-BIRTH-DAY < #ISSUE-DAY AND #1ST-ANN-BIRTH-DAY = 1 )
            if (condition(pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Month.equals(pnd_Issue_Date_A_Pnd_Issue_Month) && pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Day.less(pnd_Issue_Date_A_Pnd_Issue_Day) 
                && pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Day.equals(1)))
            {
                decideConditionsMet839++;
                pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar().setValue(pnd_Temp_Retrn_Msg_1stann90);                                                   //Natural: MOVE #TEMP-RETRN-MSG-1STANN90 TO #AIAN033-RETURN-MSG-GUAR
            }                                                                                                                                                             //Natural: WHEN ( #1ST-ANN-BIRTH-MONTH = #ISSUE-MONTH ) AND ( #1ST-ANN-BIRTH-DAY LE #ISSUE-DAY )
            else if (condition(pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Month.equals(pnd_Issue_Date_A_Pnd_Issue_Month) && pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Day.lessOrEqual(pnd_Issue_Date_A_Pnd_Issue_Day)))
            {
                decideConditionsMet839++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN ( #1ST-ANN-BIRTH-DAY = 1 )
            else if (condition(pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Day.equals(1)))
            {
                decideConditionsMet839++;
                pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar().setValue(pnd_Temp_Retrn_Msg_1stann90);                                                   //Natural: MOVE #TEMP-RETRN-MSG-1STANN90 TO #AIAN033-RETURN-MSG-GUAR
                pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar().setValue(308);                                                                          //Natural: MOVE 308 TO #AIAN033-RETURN-CODE-GUAR
                pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm().setValue(Global.getPROGRAM());                                                      //Natural: MOVE *PROGRAM TO #AIAN033-RETURN-CODE-GUAR-PGM NAZ-ACT-RETURN-CODE-PGM
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue(Global.getPROGRAM());
            }                                                                                                                                                             //Natural: WHEN ( #ISSUE-MONTH = #NEXT-MONTH ) AND ( #ISSUE-DAY = 1 )
            else if (condition(pnd_Issue_Date_A_Pnd_Issue_Month.equals(pnd_Next_Month) && pnd_Issue_Date_A_Pnd_Issue_Day.equals(1)))
            {
                decideConditionsMet839++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar().setValue(pnd_Temp_Retrn_Msg_1stann90);                                                   //Natural: MOVE #TEMP-RETRN-MSG-1STANN90 TO #AIAN033-RETURN-MSG-GUAR
                pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar().setValue(308);                                                                          //Natural: MOVE 308 TO #AIAN033-RETURN-CODE-GUAR
                pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm().setValue(Global.getPROGRAM());                                                      //Natural: MOVE *PROGRAM TO #AIAN033-RETURN-CODE-GUAR-PGM NAZ-ACT-RETURN-CODE-PGM
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue(Global.getPROGRAM());
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Age_In_Years_2nd_Ann.equals(pnd_Tbl_Age_Limit)))                                                                                                //Natural: IF #AGE-IN-YEARS-2ND-ANN = #TBL-AGE-LIMIT
        {
            pnd_Next_Month.compute(new ComputeParameters(false, pnd_Next_Month), pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Month.add(1));                                //Natural: ASSIGN #NEXT-MONTH := #2ND-ANN-BIRTH-MONTH + 1
            if (condition(pnd_Next_Month.greater(12)))                                                                                                                    //Natural: IF #NEXT-MONTH > 12
            {
                pnd_Next_Month.setValue(1);                                                                                                                               //Natural: ASSIGN #NEXT-MONTH := 1
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet861 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ( #2ND-ANN-BIRTH-MONTH = #ISSUE-MONTH AND #2ND-ANN-BIRTH-DAY < #ISSUE-DAY AND #2ND-ANN-BIRTH-DAY = 1 )
            if (condition(pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Month.equals(pnd_Issue_Date_A_Pnd_Issue_Month) && pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Day.less(pnd_Issue_Date_A_Pnd_Issue_Day) 
                && pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Day.equals(1)))
            {
                decideConditionsMet861++;
                pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar().setValue(pnd_Temp_Retrn_Msg_2ndann90);                                                   //Natural: MOVE #TEMP-RETRN-MSG-2NDANN90 TO #AIAN033-RETURN-MSG-GUAR
            }                                                                                                                                                             //Natural: WHEN ( #2ND-ANN-BIRTH-MONTH = #ISSUE-MONTH ) AND ( #2ND-ANN-BIRTH-DAY LE #ISSUE-DAY )
            else if (condition(pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Month.equals(pnd_Issue_Date_A_Pnd_Issue_Month) && pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Day.lessOrEqual(pnd_Issue_Date_A_Pnd_Issue_Day)))
            {
                decideConditionsMet861++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN ( #2ND-ANN-BIRTH-DAY = 1 )
            else if (condition(pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Day.equals(1)))
            {
                decideConditionsMet861++;
                pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar().setValue(pnd_Temp_Retrn_Msg_2ndann90);                                                   //Natural: MOVE #TEMP-RETRN-MSG-2NDANN90 TO #AIAN033-RETURN-MSG-GUAR
                pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar().setValue(308);                                                                          //Natural: MOVE 308 TO #AIAN033-RETURN-CODE-GUAR
                pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm().setValue(Global.getPROGRAM());                                                      //Natural: MOVE *PROGRAM TO #AIAN033-RETURN-CODE-GUAR-PGM NAZ-ACT-RETURN-CODE-PGM
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue(Global.getPROGRAM());
            }                                                                                                                                                             //Natural: WHEN ( #ISSUE-MONTH = #NEXT-MONTH ) AND ( #ISSUE-DAY = 1 )
            else if (condition(pnd_Issue_Date_A_Pnd_Issue_Month.equals(pnd_Next_Month) && pnd_Issue_Date_A_Pnd_Issue_Day.equals(1)))
            {
                decideConditionsMet861++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar().setValue(pnd_Temp_Retrn_Msg_2ndann90);                                                   //Natural: MOVE #TEMP-RETRN-MSG-2NDANN90 TO #AIAN033-RETURN-MSG-GUAR
                pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar().setValue(308);                                                                          //Natural: MOVE 308 TO #AIAN033-RETURN-CODE-GUAR
                pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm().setValue(Global.getPROGRAM());                                                      //Natural: MOVE *PROGRAM TO #AIAN033-RETURN-CODE-GUAR-PGM NAZ-ACT-RETURN-CODE-PGM
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue(Global.getPROGRAM());
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar().equals(308)))                                                                     //Natural: IF #AIAN033-RETURN-CODE-GUAR = 308
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  GLWB FIX ENDS   <<
        //*  DY4 FIX BEGINS >>
        //* * LE01 FIX BEGINS  >>
        //* * MOVE EDITED NAZ-ACT-ASD-DATE (EM=YYYYMMDD) TO #WS-ASD-DATE-A
        //* * IF #WS-ASD-DATE  < 20161001
        //* *** DY3 FIX BEGINS  >>
        //* *  IF #AIAN033-SETTLEMENT-TYPE = 'M' OR= 'S'   /* NON-PENSIONS */
        //* *   MOVE 299 TO #AIAN033-RETURN-CODE-GUAR
        //* *   MOVE *PROGRAM TO #AIAN033-RETURN-CODE-GUAR-PGM      /*  TREP
        //* *                    NAZ-ACT-RETURN-CODE-PGM            /*  TREP
        //* *   ESCAPE ROUTINE
        //* *  END-IF
        //* *** DY3 FIX ENDS    <<
        //* * END-IF
        //* * LE01 FIX ENDS    <<
        //*  DY4 FIX ENDS    <<
    }
    private void sub_Move_Dates_Into_Numeric_Fields() throws Exception                                                                                                    //Natural: MOVE-DATES-INTO-NUMERIC-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Issue_Date_A.setValueEdited(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Issue_Date(),new ReportEditMask("YYYYMMDD"));                                      //Natural: MOVE EDITED #AIAN033-ISSUE-DATE ( EM = YYYYMMDD ) TO #ISSUE-DATE-A
        pnd_1st_Ann_Birth_Date_A.setValueEdited(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_1st_Ann_Birth_Date(),new ReportEditMask("YYYYMMDD"));                      //Natural: MOVE EDITED #AIAN033-1ST-ANN-BIRTH-DATE ( EM = YYYYMMDD ) TO #1ST-ANN-BIRTH-DATE-A
        pnd_2nd_Ann_Birth_Date_A.setValueEdited(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Birth_Date(),new ReportEditMask("YYYYMMDD"));                      //Natural: MOVE EDITED #AIAN033-2ND-ANN-BIRTH-DATE ( EM = YYYYMMDD ) TO #2ND-ANN-BIRTH-DATE-A
        pnd_Beneficiary_Birth_Date_A.setValueEdited(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Beneficiary_Birth_Date(),new ReportEditMask("YYYYMMDD"));              //Natural: MOVE EDITED #AIAN033-BENEFICIARY-BIRTH-DATE ( EM = YYYYMMDD ) TO #BENEFICIARY-BIRTH-DATE-A
    }
    private void sub_Is_There_A_2nd_Ann_Or_Ben() throws Exception                                                                                                         //Natural: IS-THERE-A-2ND-ANN-OR-BEN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_2nd_Ann_Birth_Date_A.greater("00000000")))                                                                                                      //Natural: IF #2ND-ANN-BIRTH-DATE-A > '00000000'
        {
            pnd_Ws_Other_Ann_Birth_Date_A.setValue(pnd_2nd_Ann_Birth_Date_A);                                                                                             //Natural: MOVE #2ND-ANN-BIRTH-DATE-A TO #WS-OTHER-ANN-BIRTH-DATE-A
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Beneficiary_Birth_Date_A.greater("00000000")))                                                                                              //Natural: IF #BENEFICIARY-BIRTH-DATE-A > '00000000'
            {
                pnd_Ws_Other_Ann_Birth_Date_A.setValue(pnd_Beneficiary_Birth_Date_A);                                                                                     //Natural: MOVE #BENEFICIARY-BIRTH-DATE-A TO #WS-OTHER-ANN-BIRTH-DATE-A
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Find_Guaranteed_Period_And_Pct_To_Sec_Ann() throws Exception                                                                                         //Natural: FIND-GUARANTEED-PERIOD-AND-PCT-TO-SEC-ANN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Guar_Years().lessOrEqual(getZero()) && pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Guar_Months().lessOrEqual(getZero()))) //Natural: IF #AIAN033-GUAR-YEARS NOT > 0 AND #AIAN033-GUAR-MONTHS NOT > 0
        {
            //*  DY 8-2007
            short decideConditionsMet920 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #AIAN033-OPTION-CODE;//Natural: VALUE 5, 16, 17, 8, 54
            if (condition((pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(5) || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(16) 
                || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(17) || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(8) 
                || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(54))))
            {
                decideConditionsMet920++;
                pnd_Ws_Guar_Year.setValue(10);                                                                                                                            //Natural: MOVE 10 TO #WS-GUAR-YEAR
                //*  DY 8-2007
                pnd_Ws_Guar_Month.setValue(0);                                                                                                                            //Natural: MOVE 0 TO #WS-GUAR-MONTH
            }                                                                                                                                                             //Natural: VALUE 9, 14, 15, 13, 55
            else if (condition((pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(9) || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(14) 
                || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(15) || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(13) 
                || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(55))))
            {
                decideConditionsMet920++;
                pnd_Ws_Guar_Year.setValue(15);                                                                                                                            //Natural: MOVE 15 TO #WS-GUAR-YEAR
                //*  DY 8-2007
                pnd_Ws_Guar_Month.setValue(0);                                                                                                                            //Natural: MOVE 0 TO #WS-GUAR-MONTH
            }                                                                                                                                                             //Natural: VALUE 6, 11, 12, 10, 56
            else if (condition((pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(6) || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(11) 
                || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(12) || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(10) 
                || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(56))))
            {
                decideConditionsMet920++;
                pnd_Ws_Guar_Year.setValue(20);                                                                                                                            //Natural: MOVE 20 TO #WS-GUAR-YEAR
                pnd_Ws_Guar_Month.setValue(0);                                                                                                                            //Natural: MOVE 0 TO #WS-GUAR-MONTH
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Ws_Guar_Year.setValue(0);                                                                                                                             //Natural: MOVE 0 TO #WS-GUAR-YEAR
                pnd_Ws_Guar_Month.setValue(0);                                                                                                                            //Natural: MOVE 0 TO #WS-GUAR-MONTH
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Guar_Year.setValue(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Guar_Years());                                                                       //Natural: MOVE #AIAN033-GUAR-YEARS TO #WS-GUAR-YEAR
            pnd_Ws_Guar_Month.setValue(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Guar_Months());                                                                     //Natural: MOVE #AIAN033-GUAR-MONTHS TO #WS-GUAR-MONTH
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet941 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #AIAN033-OPTION-CODE;//Natural: VALUE 4, 11, 14, 16
        if (condition((pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(4) || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(11) 
            || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(14) || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(16))))
        {
            decideConditionsMet941++;
            //*  DY 8-2007  >>
            pnd_Ws_Pct_To_2nd_Ann.setValue(1);                                                                                                                            //Natural: MOVE 1.00 TO #WS-PCT-TO-2ND-ANN
        }                                                                                                                                                                 //Natural: VALUE 54, 55, 56, 57
        else if (condition((pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(54) || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(55) 
            || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(56) || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(57))))
        {
            decideConditionsMet941++;
            //*  DY 8-2007  <<
            pnd_Ws_Pct_To_2nd_Ann.setValue(0);                                                                                                                            //Natural: MOVE 0.75 TO #WS-PCT-TO-2ND-ANN
        }                                                                                                                                                                 //Natural: VALUE 3, 12, 15, 17
        else if (condition((pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(3) || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(12) 
            || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(15) || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(17))))
        {
            decideConditionsMet941++;
            pnd_Ws_Pct_To_2nd_Ann.setValue(0);                                                                                                                            //Natural: MOVE 0.50 TO #WS-PCT-TO-2ND-ANN
        }                                                                                                                                                                 //Natural: VALUE 7, 8, 10, 13, 18
        else if (condition((pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(7) || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(8) 
            || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(10) || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(13) 
            || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().equals(18))))
        {
            decideConditionsMet941++;
            pnd_Ws_Pct_To_2nd_Ann.setValue(0);                                                                                                                            //Natural: MOVE 0.67 TO #WS-PCT-TO-2ND-ANN
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Ws_Pct_To_2nd_Ann.setValue(0);                                                                                                                            //Natural: MOVE 0.00 TO #WS-PCT-TO-2ND-ANN
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *ITE (3) 'INPUT: GUARANTEED PERIOD ' #WS-GUAR-YEAR ' YEARS'
        //* *                               #WS-GUAR-MONTH ' MONTHS'
        //* *ITE (3) 'INPUT: PCT TO 2ND ANN    ' #WS-PCT-TO-2ND-ANN
    }
    private void sub_Calculate_Ages_For_Minimum_Distribution() throws Exception                                                                                           //Natural: CALCULATE-AGES-FOR-MINIMUM-DISTRIBUTION
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Ws_1st_Ann_Age_N.compute(new ComputeParameters(false, pnd_Ws_1st_Ann_Age_N), pnd_Issue_Date_A_Pnd_Issue_Year.subtract(pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Year)); //Natural: COMPUTE #WS-1ST-ANN-AGE-N = #ISSUE-YEAR - #1ST-ANN-BIRTH-YEAR
        if (condition(pnd_Ws_Other_Ann_Birth_Date_A_Pnd_Ws_Other_Ann_Birth_Date_N8.greater(getZero())))                                                                   //Natural: IF #WS-OTHER-ANN-BIRTH-DATE-N8 > 0
        {
            pnd_Ws_Other_Ann_Age_N.compute(new ComputeParameters(false, pnd_Ws_Other_Ann_Age_N), pnd_Issue_Date_A_Pnd_Issue_Year.subtract(pnd_Ws_Other_Ann_Birth_Date_A_Pnd_Ws_Other_Ann_Birth_Year)); //Natural: COMPUTE #WS-OTHER-ANN-AGE-N = #ISSUE-YEAR - #WS-OTHER-ANN-BIRTH-YEAR
            actual_Snd_Age.setValue(true);                                                                                                                                //Natural: MOVE TRUE TO ACTUAL-SND-AGE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Other_Ann_Age_N.setValue(0);                                                                                                                           //Natural: MOVE 0 TO #WS-OTHER-ANN-AGE-N
            actual_Snd_Age.setValue(false);                                                                                                                               //Natural: MOVE FALSE TO ACTUAL-SND-AGE
        }                                                                                                                                                                 //Natural: END-IF
        //*  DY5 FIX BEGINS >>
        if (condition(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Settlement_Type().equals("M") || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Settlement_Type().equals("S"))) //Natural: IF #AIAN033-SETTLEMENT-TYPE = 'M' OR = 'S'
        {
            pnd_Ws_Dob_Date.reset();                                                                                                                                      //Natural: RESET #WS-DOB-DATE
            pnd_Ws_Dob_Date.setValue(pnd_1st_Ann_Birth_Date_A_Pnd_1st_Ann_Birth_Date_N8);                                                                                 //Natural: MOVE #1ST-ANN-BIRTH-DATE-N8 TO #WS-DOB-DATE
                                                                                                                                                                          //Natural: PERFORM CALCULATE-ACTUAL-AGE
            sub_Calculate_Actual_Age();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_1st_Ann_Age_N.setValue(pnd_Ws_Age);                                                                                                                    //Natural: MOVE #WS-AGE TO #WS-1ST-ANN-AGE-N
            if (condition(pnd_Ws_Other_Ann_Birth_Date_A_Pnd_Ws_Other_Ann_Birth_Date_N8.greater(getZero())))                                                               //Natural: IF #WS-OTHER-ANN-BIRTH-DATE-N8 > 0
            {
                pnd_Ws_Dob_Date.reset();                                                                                                                                  //Natural: RESET #WS-DOB-DATE
                pnd_Ws_Dob_Date.setValue(pnd_2nd_Ann_Birth_Date_A_Pnd_2nd_Ann_Birth_Date_N8);                                                                             //Natural: MOVE #2ND-ANN-BIRTH-DATE-N8 TO #WS-DOB-DATE
                                                                                                                                                                          //Natural: PERFORM CALCULATE-ACTUAL-AGE
                sub_Calculate_Actual_Age();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Other_Ann_Age_N.setValue(pnd_Ws_Age);                                                                                                              //Natural: MOVE #WS-AGE TO #WS-OTHER-ANN-AGE-N
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  DY5 FIX ENDS   <<
        pnd_Ws_Age_Diff.compute(new ComputeParameters(false, pnd_Ws_Age_Diff), pnd_Ws_1st_Ann_Age_N.subtract(pnd_Ws_Other_Ann_Age_N));                                    //Natural: COMPUTE #WS-AGE-DIFF = #WS-1ST-ANN-AGE-N - #WS-OTHER-ANN-AGE-N
        //*  OLDER AGE IS USED FOR THE IRS SINGLE & JOINT TABLE LOOK-UP
        //*     FOR PENSIONS
        //*  WRITE 'PENSIONS :' '=' #WS-1ST-ANN-AGE-N '=' #WS-OTHER-ANN-AGE-N
        //*  DY4
        pnd_Ws2_1st_Ann_Age_N.setValue(pnd_Ws_1st_Ann_Age_N);                                                                                                             //Natural: MOVE #WS-1ST-ANN-AGE-N TO #WS2-1ST-ANN-AGE-N
        if (condition(pnd_Ws_1st_Ann_Age_N.less(pnd_Ws_Other_Ann_Age_N)))                                                                                                 //Natural: IF #WS-1ST-ANN-AGE-N < #WS-OTHER-ANN-AGE-N
        {
            pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_A.setValue(pnd_Ws_1st_Ann_Age_N_Pnd_Ws_1st_Ann_Age_A);                                                                      //Natural: MOVE #WS-1ST-ANN-AGE-A TO #MD-OTHER-ANN-AGE-A
            pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_A.setValue(pnd_Ws_Other_Ann_Age_N_Pnd_Ws_Other_Ann_Age_A);                                                                    //Natural: MOVE #WS-OTHER-ANN-AGE-A TO #MD-1ST-ANN-AGE-A
            //*   MOVE #WS-1ST-ANN-AGE-N   TO #MD-OTHER-ANN-AGE-N       /* LE01 *DY4
            //*   MOVE #WS-OTHER-ANN-AGE-N TO #MD-1ST-ANN-AGE-N         /* LE01 *DY4
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_A.setValue(pnd_Ws_1st_Ann_Age_N_Pnd_Ws_1st_Ann_Age_A);                                                                        //Natural: MOVE #WS-1ST-ANN-AGE-A TO #MD-1ST-ANN-AGE-A
            pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_A.setValue(pnd_Ws_Other_Ann_Age_N_Pnd_Ws_Other_Ann_Age_A);                                                                  //Natural: MOVE #WS-OTHER-ANN-AGE-A TO #MD-OTHER-ANN-AGE-A
            //*   MOVE #WS-1ST-ANN-AGE-N   TO #MD-1ST-ANN-AGE-N         /* LE01 *DY4
            //*   MOVE #WS-OTHER-ANN-AGE-N TO #MD-OTHER-ANN-AGE-N       /* LE01 *DY4
        }                                                                                                                                                                 //Natural: END-IF
        //*  LE01 FIX BEGINS >>
        //*  YOUNGER AGE IS USED FOR THE IRS TABLE LOOK-UP FOR NON-PENSIONS
        if (condition(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Settlement_Type().equals("M") || pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Settlement_Type().equals("S"))) //Natural: IF #AIAN033-SETTLEMENT-TYPE = 'M' OR = 'S'
        {
            //*  WRITE 'NON-PENSN:' '=' #WS-1ST-ANN-AGE-N '=' #WS-OTHER-ANN-AGE-N
            if (condition(pnd_Ws_Other_Ann_Age_N.equals(getZero())))                                                                                                      //Natural: IF #WS-OTHER-ANN-AGE-N = 0
            {
                pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_A.setValue(pnd_Ws_1st_Ann_Age_N_Pnd_Ws_1st_Ann_Age_A);                                                                    //Natural: MOVE #WS-1ST-ANN-AGE-A TO #MD-1ST-ANN-AGE-A
                pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_A.setValue(pnd_Ws_Other_Ann_Age_N_Pnd_Ws_Other_Ann_Age_A);                                                              //Natural: MOVE #WS-OTHER-ANN-AGE-A TO #MD-OTHER-ANN-AGE-A
                //*    MOVE #WS-1ST-ANN-AGE-N   TO #MD-1ST-ANN-AGE-N           /* DY4
                //*  DY4
                pnd_Ws2_1st_Ann_Age_N.setValue(pnd_Ws_1st_Ann_Age_N);                                                                                                     //Natural: MOVE #WS-1ST-ANN-AGE-N TO #WS2-1ST-ANN-AGE-N
                //*    MOVE #WS-OTHER-ANN-AGE-N TO #MD-OTHER-ANN-AGE-N         /* DY4
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Ws_1st_Ann_Age_N.less(pnd_Ws_Other_Ann_Age_N)))                                                                                         //Natural: IF #WS-1ST-ANN-AGE-N < #WS-OTHER-ANN-AGE-N
                {
                    pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_A.setValue(pnd_Ws_1st_Ann_Age_N_Pnd_Ws_1st_Ann_Age_A);                                                                //Natural: MOVE #WS-1ST-ANN-AGE-A TO #MD-1ST-ANN-AGE-A
                    pnd_Md_Other_Ann_Age_N.compute(new ComputeParameters(false, pnd_Md_Other_Ann_Age_N), pnd_Ws_1st_Ann_Age_N.subtract(10));                              //Natural: COMPUTE #MD-OTHER-ANN-AGE-N = #WS-1ST-ANN-AGE-N - 10
                    pnd_Ws_Other_Ann_Age_N.setValue(pnd_Md_Other_Ann_Age_N);                                                                                              //Natural: MOVE #MD-OTHER-ANN-AGE-N TO #WS-OTHER-ANN-AGE-N
                    pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_A.setValue(pnd_Ws_Other_Ann_Age_N_Pnd_Ws_Other_Ann_Age_A);                                                          //Natural: MOVE #WS-OTHER-ANN-AGE-A TO #MD-OTHER-ANN-AGE-A
                    //*      MOVE #WS-1ST-ANN-AGE-N   TO #MD-1ST-ANN-AGE-N         /* DY4
                    //*  DY4
                    pnd_Ws2_1st_Ann_Age_N.setValue(pnd_Ws_1st_Ann_Age_N);                                                                                                 //Natural: MOVE #WS-1ST-ANN-AGE-N TO #WS2-1ST-ANN-AGE-N
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_A.setValue(pnd_Ws_Other_Ann_Age_N_Pnd_Ws_Other_Ann_Age_A);                                                            //Natural: MOVE #WS-OTHER-ANN-AGE-A TO #MD-1ST-ANN-AGE-A
                    //*      MOVE #WS-OTHER-ANN-AGE-N TO #MD-1ST-ANN-AGE-N         /* DY4
                    //*  DY4
                    pnd_Ws2_1st_Ann_Age_N.setValue(pnd_Ws_Other_Ann_Age_N);                                                                                               //Natural: MOVE #WS-OTHER-ANN-AGE-N TO #WS2-1ST-ANN-AGE-N
                    pnd_Md_Other_Ann_Age_N.compute(new ComputeParameters(false, pnd_Md_Other_Ann_Age_N), pnd_Ws_Other_Ann_Age_N.subtract(10));                            //Natural: COMPUTE #MD-OTHER-ANN-AGE-N = #WS-OTHER-ANN-AGE-N - 10
                    pnd_Ws_Other_Ann_Age_N.setValue(pnd_Md_Other_Ann_Age_N);                                                                                              //Natural: MOVE #MD-OTHER-ANN-AGE-N TO #WS-OTHER-ANN-AGE-N
                    pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_A.setValue(pnd_Ws_Other_Ann_Age_N_Pnd_Ws_Other_Ann_Age_A);                                                          //Natural: MOVE #WS-OTHER-ANN-AGE-A TO #MD-OTHER-ANN-AGE-A
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  WRITE '=' #WS2-1ST-ANN-AGE-N
            //*  WRITE '=' #MD-1ST-ANN-AGE-A '=' #MD-OTHER-ANN-AGE-A
            //*  WRITE '=' #WS-1ST-ANN-AGE-N '=' #WS-OTHER-ANN-AGE-N
        }                                                                                                                                                                 //Natural: END-IF
        //*  LE01 FIX ENDS   <<
        if (condition(! (actual_Snd_Age.getBoolean())))                                                                                                                   //Natural: IF NOT ACTUAL-SND-AGE
        {
            pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_A.setValue("999");                                                                                                          //Natural: MOVE '999' TO #MD-OTHER-ANN-AGE-A
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Convert_Input_Data_For_Irs_Table() throws Exception                                                                                                  //Natural: CONVERT-INPUT-DATA-FOR-IRS-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.examine(new ExamineSource(pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_3), new ExamineSearch("0"), new ExamineReplace("{"));                                           //Natural: EXAMINE #MD-1ST-ANN-AGE-3 FOR '0' REPLACE '{'
        DbsUtil.examine(new ExamineSource(pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_3), new ExamineSearch("1"), new ExamineReplace("A"));                                           //Natural: EXAMINE #MD-1ST-ANN-AGE-3 FOR '1' REPLACE 'A'
        DbsUtil.examine(new ExamineSource(pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_3), new ExamineSearch("2"), new ExamineReplace("B"));                                           //Natural: EXAMINE #MD-1ST-ANN-AGE-3 FOR '2' REPLACE 'B'
        DbsUtil.examine(new ExamineSource(pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_3), new ExamineSearch("3"), new ExamineReplace("C"));                                           //Natural: EXAMINE #MD-1ST-ANN-AGE-3 FOR '3' REPLACE 'C'
        DbsUtil.examine(new ExamineSource(pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_3), new ExamineSearch("4"), new ExamineReplace("D"));                                           //Natural: EXAMINE #MD-1ST-ANN-AGE-3 FOR '4' REPLACE 'D'
        DbsUtil.examine(new ExamineSource(pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_3), new ExamineSearch("5"), new ExamineReplace("E"));                                           //Natural: EXAMINE #MD-1ST-ANN-AGE-3 FOR '5' REPLACE 'E'
        DbsUtil.examine(new ExamineSource(pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_3), new ExamineSearch("6"), new ExamineReplace("F"));                                           //Natural: EXAMINE #MD-1ST-ANN-AGE-3 FOR '6' REPLACE 'F'
        DbsUtil.examine(new ExamineSource(pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_3), new ExamineSearch("7"), new ExamineReplace("G"));                                           //Natural: EXAMINE #MD-1ST-ANN-AGE-3 FOR '7' REPLACE 'G'
        DbsUtil.examine(new ExamineSource(pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_3), new ExamineSearch("8"), new ExamineReplace("H"));                                           //Natural: EXAMINE #MD-1ST-ANN-AGE-3 FOR '8' REPLACE 'H'
        DbsUtil.examine(new ExamineSource(pnd_Md_Key_A_Pnd_Md_1st_Ann_Age_3), new ExamineSearch("9"), new ExamineReplace("I"));                                           //Natural: EXAMINE #MD-1ST-ANN-AGE-3 FOR '9' REPLACE 'I'
        DbsUtil.examine(new ExamineSource(pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_3), new ExamineSearch("0"), new ExamineReplace("{"));                                         //Natural: EXAMINE #MD-OTHER-ANN-AGE-3 FOR '0' REPLACE '{'
        DbsUtil.examine(new ExamineSource(pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_3), new ExamineSearch("1"), new ExamineReplace("A"));                                         //Natural: EXAMINE #MD-OTHER-ANN-AGE-3 FOR '1' REPLACE 'A'
        DbsUtil.examine(new ExamineSource(pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_3), new ExamineSearch("2"), new ExamineReplace("B"));                                         //Natural: EXAMINE #MD-OTHER-ANN-AGE-3 FOR '2' REPLACE 'B'
        DbsUtil.examine(new ExamineSource(pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_3), new ExamineSearch("3"), new ExamineReplace("C"));                                         //Natural: EXAMINE #MD-OTHER-ANN-AGE-3 FOR '3' REPLACE 'C'
        DbsUtil.examine(new ExamineSource(pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_3), new ExamineSearch("4"), new ExamineReplace("D"));                                         //Natural: EXAMINE #MD-OTHER-ANN-AGE-3 FOR '4' REPLACE 'D'
        DbsUtil.examine(new ExamineSource(pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_3), new ExamineSearch("5"), new ExamineReplace("E"));                                         //Natural: EXAMINE #MD-OTHER-ANN-AGE-3 FOR '5' REPLACE 'E'
        DbsUtil.examine(new ExamineSource(pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_3), new ExamineSearch("6"), new ExamineReplace("F"));                                         //Natural: EXAMINE #MD-OTHER-ANN-AGE-3 FOR '6' REPLACE 'F'
        DbsUtil.examine(new ExamineSource(pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_3), new ExamineSearch("7"), new ExamineReplace("G"));                                         //Natural: EXAMINE #MD-OTHER-ANN-AGE-3 FOR '7' REPLACE 'G'
        DbsUtil.examine(new ExamineSource(pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_3), new ExamineSearch("8"), new ExamineReplace("H"));                                         //Natural: EXAMINE #MD-OTHER-ANN-AGE-3 FOR '8' REPLACE 'H'
        DbsUtil.examine(new ExamineSource(pnd_Md_Key_A_Pnd_Md_Other_Ann_Age_3), new ExamineSearch("9"), new ExamineReplace("I"));                                         //Natural: EXAMINE #MD-OTHER-ANN-AGE-3 FOR '9' REPLACE 'I'
    }
    private void sub_Read_Irs_Table() throws Exception                                                                                                                    //Natural: READ-IRS-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  WRITE '=' #MD-KEY-A
        vw_uta.startDatabaseFind                                                                                                                                          //Natural: FIND UTA WITH MD-KEY = #MD-KEY-A
        (
        "FIND01",
        new Wc[] { new Wc("MD_KEY", "=", pnd_Md_Key_A, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_uta.readNextRow("FIND01", true)))
        {
            vw_uta.setIfNotFoundControlFlag(false);
            if (condition(vw_uta.getAstCOUNTER().equals(0)))                                                                                                              //Natural: IF NO RECORD FOUND
            {
                //* *    WRITE 'NOT FOUND'
                pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar().setValue("AGES CANT BE FOUND ON FILE");                                                  //Natural: MOVE 'AGES CANT BE FOUND ON FILE' TO #AIAN033-RETURN-MSG-GUAR
                pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar().setValue(306);                                                                          //Natural: MOVE 306 TO #AIAN033-RETURN-CODE-GUAR
                //*   TREP
                //*   TREP
                pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm().setValue(Global.getPROGRAM());                                                      //Natural: MOVE *PROGRAM TO #AIAN033-RETURN-CODE-GUAR-PGM NAZ-ACT-RETURN-CODE-PGM
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue(Global.getPROGRAM());
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            //* *ITE (3) 'AGE OF 1ST ANNUITANT AND 2ND ANNUITANT          '
            //* *                       #WS-1ST-ANN-AGE-N
            //* *                       #WS-OTHER-ANN-AGE-N
            pnd_Ws_Expected_Life_Md.setValue(uta_Pnd_Md_Multiple);                                                                                                        //Natural: MOVE #MD-MULTIPLE TO #WS-EXPECTED-LIFE-MD
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Md_Exp_Life().setValue(pnd_Ws_Expected_Life_Md);                                                               //Natural: MOVE #WS-EXPECTED-LIFE-MD TO #AIAN033-MD-EXP-LIFE
            //*  WRITE 'LIFE EXPECTANCY >>>' '=' #AIAN033-MD-EXP-LIFE
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Read_Mdib_Table_For_Life_Exp() throws Exception                                                                                                      //Natural: READ-MDIB-TABLE-FOR-LIFE-EXP
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  WRITE ' <<<<<< READ-MDIB-TABLE-FOR-LIFE-EXP >>>>'
        //*  WRITE ' <<<< READS UNIFORM TABLE >>>>>>'
        //* * DY FIX 6/8/06
        //* *  # OF LINES WERE COMMENTED AND NEW CODE WAS WRITTEN TO READ THE
        //* *  NEW EXTERNAL TABLE WITH REPRESENT IRS TABLE III.
        //* ********************************************************************
        //* *COMPUTE #WS-1ST-ANN-BIRTH-MONTH-AT-70-HALF = #1ST-ANN-BIRTH-MONTH + 6
        //* *IF #WS-1ST-ANN-BIRTH-MONTH-AT-70-HALF > 12
        //* *  SUBTRACT 12 FROM #WS-1ST-ANN-BIRTH-MONTH-AT-70-HALF
        //* *  COMPUTE #WS-1ST-ANN-BIRTH-YEAR-AT-70-HALF = #1ST-ANN-BIRTH-YEAR + 71
        //* *ELSE
        //* *  COMPUTE #WS-1ST-ANN-BIRTH-YEAR-AT-70-HALF = #1ST-ANN-BIRTH-YEAR + 70
        //* *END-IF
        //* *IF #ISSUE-YEAR < #WS-1ST-ANN-BIRTH-YEAR-AT-70-HALF
        //* *  COMPUTE #WS-EXPECTED-LIFE-MDIB = #MDIB-VALUE (1)
        //* *    +  #WS-1ST-ANN-BIRTH-YEAR-AT-70-HALF
        //* *    - #ISSUE-YEAR
        //* *ELSE
        //* *  COMPUTE #WS-1ST-ANN-AGE-MDIB = #ISSUE-YEAR - #1ST-ANN-BIRTH-YEAR
        //* *  COMPUTE #WS-MDIB-VALUE-INDEX =
        //* *    #WS-1ST-ANN-AGE-MDIB - #MDIB-MIN-AGE + 1
        //* *  MOVE #MDIB-VALUE (#WS-MDIB-VALUE-INDEX) TO #WS-EXPECTED-LIFE-MDIB
        //* *END-IF
        //* *MOVE #WS-EXPECTED-LIFE-MDIB TO #AIAN033-MDIB-EXP-LIFE
        //*  IF #MD-1ST-ANN-AGE-N < 70                            /* LE01 *DY4
        //*  DY4
        if (condition(pnd_Ws2_1st_Ann_Age_N.less(70)))                                                                                                                    //Natural: IF #WS2-1ST-ANN-AGE-N < 70
        {
            //*  DY4
            pnd_Ws_Expected_Life_Md.compute(new ComputeParameters(false, pnd_Ws_Expected_Life_Md), ldaAial034.getAge_Life_Expectancy_Life_Expectancy().getValue(1).add(70).subtract(pnd_Ws2_1st_Ann_Age_N)); //Natural: COMPUTE #WS-EXPECTED-LIFE-MD = LIFE-EXPECTANCY ( 1 ) + 70 - #WS2-1ST-ANN-AGE-N
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 50
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(50)); pnd_I.nadd(1))
            {
                //*    IF #MD-1ST-ANN-AGE-N = AGE(#I)                      /* LE01 *DY4
                //*  DY4
                if (condition(pnd_Ws2_1st_Ann_Age_N.equals(ldaAial034.getAge_Life_Expectancy_Age().getValue(pnd_I))))                                                     //Natural: IF #WS2-1ST-ANN-AGE-N = AGE ( #I )
                {
                    pnd_Ws_Expected_Life_Md.setValue(ldaAial034.getAge_Life_Expectancy_Life_Expectancy().getValue(pnd_I));                                                //Natural: MOVE LIFE-EXPECTANCY ( #I ) TO #WS-EXPECTED-LIFE-MD
                    //*  LE01
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //*    ELSE
                    //*      MOVE 'AGES CANT BE FOUND ON FILE' TO #AIAN033-RETURN-MSG-PCT
                    //*      MOVE 307 TO #AIAN033-RETURN-CODE-PCT
                    //*      ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE 'FROM UNIFORM TABLE-->'
        //*  WRITE '=' #WS2-1ST-ANN-AGE-N '=' #WS-EXPECTED-LIFE-MD
        //*  IF IRS TABLE I+II LOOK-UP FAILS BEC 2ND ANN > 1ST ANN RECORDS NOT
        //*    AVAILABLE ON UTA490 VSAM FILE, RC 306 OCCURS. SO TO BYPASS THIS
        //*    LOGIC BEC THE UNIFORM TABLE WILL BE USED INSTEAD, THE CODE BELOW
        //*    WAS CREATED. YOUNGER AGE IS USED FOR NON-PENSIONS.
        //*  LE01
        if (condition(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar().equals(306)))                                                                     //Natural: IF #AIAN033-RETURN-CODE-GUAR = 306
        {
            //*  LE01
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar().reset();                                                                                     //Natural: RESET #AIAN033-RETURN-MSG-GUAR
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar().setValue(300);                                                                              //Natural: MOVE 300 TO #AIAN033-RETURN-CODE-GUAR
            //*  LE01
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Read_Mdib_Table_For_Pct_Allowed() throws Exception                                                                                                   //Natural: READ-MDIB-TABLE-FOR-PCT-ALLOWED
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  WRITE '** READ-MDIB-TABLE-FOR-PCT-ALLOWED **'
        //*  NBC 8 /09
                                                                                                                                                                          //Natural: PERFORM CALCULATE-AGES-FOR-PCT-ALLOWED
        sub_Calculate_Ages_For_Pct_Allowed();
        if (condition(Global.isEscape())) {return;}
        //*  WRITE '=' #DIFF-IN-YEARS '=' #MDIB-MAX-DIFF
        if (condition(pnd_Diff_In_Years.greater(pnd_Mdib_Max_Diff)))                                                                                                      //Natural: IF #DIFF-IN-YEARS > #MDIB-MAX-DIFF
        {
            pnd_Ws_Mdib_Appl_Pct.setValue(0);                                                                                                                             //Natural: MOVE 0.50 TO #WS-MDIB-APPL-PCT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Diff_In_Years.greater(pnd_Mdib_Min_Diff)))                                                                                                  //Natural: IF #DIFF-IN-YEARS > #MDIB-MIN-DIFF
            {
                pnd_Ws_Mdib_Diff_Index.compute(new ComputeParameters(false, pnd_Ws_Mdib_Diff_Index), pnd_Diff_In_Years.subtract(pnd_Mdib_Min_Diff).add(1));               //Natural: COMPUTE #WS-MDIB-DIFF-INDEX = #DIFF-IN-YEARS - #MDIB-MIN-DIFF + 1
                pnd_Ws_Mdib_Appl_Pct.setValue(pnd_Mdib_Pct_Table_Pnd_Mdib_Pct.getValue(pnd_Ws_Mdib_Diff_Index));                                                          //Natural: COMPUTE #WS-MDIB-APPL-PCT = #MDIB-PCT ( #WS-MDIB-DIFF-INDEX )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Mdib_Appl_Pct.setValue(1);                                                                                                                         //Natural: MOVE 1.00 TO #WS-MDIB-APPL-PCT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  NBC 08/09 OLD CODE BELOW COMMENTED OUT-PREVIOUS COMMENTS *** OR **
        //* ** PUTE #WS-DIFF-1ST-AND-2ND-ANN = #WS-OTHER-ANN-BIRTH-YEAR -
        //*  COMPUTE #WS-DIFF-1ST-AND-2ND-ANN-X = #WS-OTHER-ANN-BIRTH-YEAR -
        //*   #1ST-ANN-BIRTH-YEAR
        //*  IF #WS-DIFF-1ST-AND-2ND-ANN > #MDIB-MAX-DIFF
        //*     MOVE 0.50 TO #WS-MDIB-APPL-PCT
        //*  ELSE
        //*   IF #WS-DIFF-1ST-AND-2ND-ANN > #MDIB-MIN-DIFF
        //*     COMPUTE #WS-MDIB-DIFF-INDEX = #WS-DIFF-1ST-AND-2ND-ANN
        //*       - #MDIB-MIN-DIFF + 1
        //*     COMPUTE #WS-MDIB-APPL-PCT = #MDIB-PCT (#WS-MDIB-DIFF-INDEX)
        //*   ELSE
        //*     MOVE 1.00 TO #WS-MDIB-APPL-PCT
        //*   END-IF
        //*  END-IF
        //* *ITE (3) 'DIFFERENCE IN AGES BETWEEN FIRST AND SECOND ANN '
        //* *                                #WS-DIFF-1ST-AND-2ND-ANN
        pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Minimum_Pct_Allow().setValue(pnd_Ws_Mdib_Appl_Pct);                                                                //Natural: MOVE #WS-MDIB-APPL-PCT TO #AIAN033-MINIMUM-PCT-ALLOW
        //*  WRITE 'AIAN033: READ-MDIB-TABLE-FOR-PCT-ALLOWED: '
        //*              '=' #WS-MDIB-APPL-PCT
    }
    private void sub_Calculate_Ages_For_Pct_Allowed() throws Exception                                                                                                    //Natural: CALCULATE-AGES-FOR-PCT-ALLOWED
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Pct_1st_Ann_Birth_Date_A.setValue(pnd_1st_Ann_Birth_Date_A);                                                                                                  //Natural: ASSIGN #PCT-1ST-ANN-BIRTH-DATE-A := #1ST-ANN-BIRTH-DATE-A
        pnd_Pct_2nd_Ann_Birth_Date_A.setValue(pnd_2nd_Ann_Birth_Date_A);                                                                                                  //Natural: ASSIGN #PCT-2ND-ANN-BIRTH-DATE-A := #2ND-ANN-BIRTH-DATE-A
        //*  ADJUST BIRTH DATES TO FIRST OF THE FOLLOWING MONTH
        if (condition(pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Month.equals(12)))                                                                               //Natural: IF #PCT-1ST-ANN-BIRTH-MONTH = 12
        {
            pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Year.nadd(1);                                                                                              //Natural: ADD 1 TO #PCT-1ST-ANN-BIRTH-YEAR
            pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Month.setValue(1);                                                                                         //Natural: MOVE 01 TO #PCT-1ST-ANN-BIRTH-MONTH
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Month.nadd(1);                                                                                             //Natural: ADD 01 TO #PCT-1ST-ANN-BIRTH-MONTH
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Day.setValue(1);                                                                                               //Natural: ASSIGN #PCT-1ST-ANN-BIRTH-DAY := 01
        if (condition(pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Month.equals(12)))                                                                               //Natural: IF #PCT-2ND-ANN-BIRTH-MONTH = 12
        {
            pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Year.nadd(1);                                                                                              //Natural: ADD 1 TO #PCT-2ND-ANN-BIRTH-YEAR
            pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Month.setValue(1);                                                                                         //Natural: MOVE 01 TO #PCT-2ND-ANN-BIRTH-MONTH
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Month.nadd(1);                                                                                             //Natural: ADD 01 TO #PCT-2ND-ANN-BIRTH-MONTH
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Day.setValue(1);                                                                                               //Natural: ASSIGN #PCT-2ND-ANN-BIRTH-DAY := 01
        //*  CALCUTE NUMBER OF COMPLETE YEARS
        if (condition(pnd_Issue_Date_A_Pnd_Issue_Date_Mmdd.greaterOrEqual(pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Mmdd)))                                      //Natural: IF #ISSUE-DATE-MMDD GE #PCT-1ST-ANN-BIRTH-MMDD
        {
            pnd_Age_In_Years_1st_Ann.compute(new ComputeParameters(false, pnd_Age_In_Years_1st_Ann), pnd_Issue_Date_A_Pnd_Issue_Year.subtract(pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Year)); //Natural: ASSIGN #AGE-IN-YEARS-1ST-ANN := #ISSUE-YEAR - #PCT-1ST-ANN-BIRTH-YEAR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Age_In_Years_1st_Ann.compute(new ComputeParameters(false, pnd_Age_In_Years_1st_Ann), pnd_Issue_Date_A_Pnd_Issue_Year.subtract(pnd_Pct_1st_Ann_Birth_Date_A_Pnd_Pct_1st_Ann_Birth_Year).subtract(1)); //Natural: ASSIGN #AGE-IN-YEARS-1ST-ANN := #ISSUE-YEAR - #PCT-1ST-ANN-BIRTH-YEAR - 1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Issue_Date_A_Pnd_Issue_Date_Mmdd.greaterOrEqual(pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Mmdd)))                                      //Natural: IF #ISSUE-DATE-MMDD GE #PCT-2ND-ANN-BIRTH-MMDD
        {
            pnd_Age_In_Years_2nd_Ann.compute(new ComputeParameters(false, pnd_Age_In_Years_2nd_Ann), pnd_Issue_Date_A_Pnd_Issue_Year.subtract(pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Year)); //Natural: ASSIGN #AGE-IN-YEARS-2ND-ANN := #ISSUE-YEAR - #PCT-2ND-ANN-BIRTH-YEAR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Age_In_Years_2nd_Ann.compute(new ComputeParameters(false, pnd_Age_In_Years_2nd_Ann), pnd_Issue_Date_A_Pnd_Issue_Year.subtract(pnd_Pct_2nd_Ann_Birth_Date_A_Pnd_Pct_2nd_Ann_Birth_Year).subtract(1)); //Natural: ASSIGN #AGE-IN-YEARS-2ND-ANN := #ISSUE-YEAR - #PCT-2ND-ANN-BIRTH-YEAR - 1
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Diff_In_Years.compute(new ComputeParameters(false, pnd_Diff_In_Years), pnd_Age_In_Years_1st_Ann.subtract(pnd_Age_In_Years_2nd_Ann));                          //Natural: ASSIGN #DIFF-IN-YEARS := #AGE-IN-YEARS-1ST-ANN - #AGE-IN-YEARS-2ND-ANN
        //*  WRITE '*** 1ST AGE CALC FOR MDIB TEST ****'
        //*    WRITE '=' #DIFF-IN-YEARS
        //*    WRITE '=' #AGE-IN-YEARS-1ST-ANN
        //*    WRITE '=' #AGE-IN-YEARS-2ND-ANN
        if (condition(pnd_Age_In_Years_1st_Ann.less(70)))                                                                                                                 //Natural: IF #AGE-IN-YEARS-1ST-ANN LT 70
        {
            pnd_Number_Of_Years_Less_Than_70.compute(new ComputeParameters(false, pnd_Number_Of_Years_Less_Than_70), DbsField.subtract(70,pnd_Age_In_Years_1st_Ann));     //Natural: ASSIGN #NUMBER-OF-YEARS-LESS-THAN-70 := 70 - #AGE-IN-YEARS-1ST-ANN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Number_Of_Years_Less_Than_70.setValue(0);                                                                                                                 //Natural: ASSIGN #NUMBER-OF-YEARS-LESS-THAN-70 := 0
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE '=' #DIFF-IN-YEARS '=' #NUMBER-OF-YEARS-LESS-THAN-70
        if (condition(pnd_Diff_In_Years.less(pnd_Number_Of_Years_Less_Than_70)))                                                                                          //Natural: IF #DIFF-IN-YEARS LT #NUMBER-OF-YEARS-LESS-THAN-70
        {
            pnd_Diff_In_Years.setValue(0);                                                                                                                                //Natural: ASSIGN #DIFF-IN-YEARS := 0
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Diff_In_Years.nsubtract(pnd_Number_Of_Years_Less_Than_70);                                                                                                //Natural: ASSIGN #DIFF-IN-YEARS := #DIFF-IN-YEARS - #NUMBER-OF-YEARS-LESS-THAN-70
        }                                                                                                                                                                 //Natural: END-IF
        //*    WRITE
        //*     /'=' #AGE-IN-YEARS-2ND-ANN
        //*     /'=' #AGE-IN-YEARS-1ST-ANN
        //*     /'=' #DIFF-IN-YEARS
        //*     /'=' #NUMBER-OF-YEARS-LESS-THAN-70
    }
    private void sub_Compare_Md_With_Mdib() throws Exception                                                                                                              //Natural: COMPARE-MD-WITH-MDIB
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *IF #WS-EXPECTED-LIFE-MDIB NOT = 0
        //* *  IF #WS-EXPECTED-LIFE-MDIB < #WS-EXPECTED-LIFE-MD
        //* *    MOVE #WS-EXPECTED-LIFE-MDIB TO #WS-MINIMUM-LIFE-EXP
        //* *  ELSE
        //* *    MOVE #WS-EXPECTED-LIFE-MD TO #WS-MINIMUM-LIFE-EXP
        //* *  END-IF
        //* *ELSE
        pnd_Ws_Minimum_Life_Exp.setValue(pnd_Ws_Expected_Life_Md);                                                                                                        //Natural: MOVE #WS-EXPECTED-LIFE-MD TO #WS-MINIMUM-LIFE-EXP
        //* *END-IF
        pnd_Ws_Current_Guar.compute(new ComputeParameters(true, pnd_Ws_Current_Guar), pnd_Ws_Guar_Year.add((pnd_Ws_Guar_Month.divide(new DbsDecimal("12.0")))));          //Natural: COMPUTE ROUNDED #WS-CURRENT-GUAR = #WS-GUAR-YEAR + ( #WS-GUAR-MONTH /12.0 )
        //*  WRITE '=' #WS-CURRENT-GUAR '>'  '=' #WS-MINIMUM-LIFE-EXP
        //*     ' THEN ERROR MSG << CANNOT TAKE GUAR '
        if (condition(pnd_Ws_Current_Guar.greater(pnd_Ws_Minimum_Life_Exp)))                                                                                              //Natural: IF #WS-CURRENT-GUAR > #WS-MINIMUM-LIFE-EXP
        {
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar().setValue("CANNOT TAKE GUARANTEED PERIOD");                                                   //Natural: MOVE 'CANNOT TAKE GUARANTEED PERIOD' TO #AIAN033-RETURN-MSG-GUAR
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar().setValue(301);                                                                              //Natural: MOVE 301 TO #AIAN033-RETURN-CODE-GUAR
            //*   TREP
            //*   TREP
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm().setValue(Global.getPROGRAM());                                                          //Natural: MOVE *PROGRAM TO #AIAN033-RETURN-CODE-GUAR-PGM NAZ-ACT-RETURN-CODE-PGM
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue(Global.getPROGRAM());
        }                                                                                                                                                                 //Natural: END-IF
        pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Minimum_Life_Exp().setValue(pnd_Ws_Minimum_Life_Exp);                                                              //Natural: MOVE #WS-MINIMUM-LIFE-EXP TO #AIAN033-MINIMUM-LIFE-EXP
    }
    private void sub_Compare_Pct_With_Mdib() throws Exception                                                                                                             //Natural: COMPARE-PCT-WITH-MDIB
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  WRITE '=' #WS-PCT-TO-2ND-ANN '=' #WS-MDIB-APPL-PCT
        if (condition(pnd_Ws_Pct_To_2nd_Ann.greater(pnd_Ws_Mdib_Appl_Pct) && pnd_Ws_Pct_To_2nd_Ann.greater(new DbsDecimal("0.5"))))                                       //Natural: IF #WS-PCT-TO-2ND-ANN > #WS-MDIB-APPL-PCT AND #WS-PCT-TO-2ND-ANN > 0.5
        {
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Pct().setValue("CANNOT TAKE PCT TO 2ND ANN");                                                       //Natural: MOVE 'CANNOT TAKE PCT TO 2ND ANN' TO #AIAN033-RETURN-MSG-PCT
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct().setValue(302);                                                                               //Natural: MOVE 302 TO #AIAN033-RETURN-CODE-PCT
            //*   TREP
            //*   TREP
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm().setValue(Global.getPROGRAM());                                                          //Natural: MOVE *PROGRAM TO #AIAN033-RETURN-CODE-GUAR-PGM NAZ-ACT-RETURN-CODE-PGM
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Pgm().setValue(Global.getPROGRAM());
        }                                                                                                                                                                 //Natural: END-IF
    }
    //* * DY5
    private void sub_Calculate_Actual_Age() throws Exception                                                                                                              //Natural: CALCULATE-ACTUAL-AGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet1283 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ( #ISSUE-MONTH = #WS-DOB-MO )
        if (condition(pnd_Issue_Date_A_Pnd_Issue_Month.equals(pnd_Ws_Dob_Date_Pnd_Ws_Dob_Mo)))
        {
            decideConditionsMet1283++;
            if (condition(pnd_Issue_Date_A_Pnd_Issue_Day.less(pnd_Ws_Dob_Date_Pnd_Ws_Dob_Da)))                                                                            //Natural: IF #ISSUE-DAY < #WS-DOB-DA THEN
            {
                pnd_Ws_Age.compute(new ComputeParameters(false, pnd_Ws_Age), pnd_Issue_Date_A_Pnd_Issue_Year.subtract(pnd_Ws_Dob_Date_Pnd_Ws_Dob_Yr).subtract(1));        //Natural: COMPUTE #WS-AGE = #ISSUE-YEAR - #WS-DOB-YR - 1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Age.compute(new ComputeParameters(false, pnd_Ws_Age), pnd_Issue_Date_A_Pnd_Issue_Year.subtract(pnd_Ws_Dob_Date_Pnd_Ws_Dob_Yr));                    //Natural: COMPUTE #WS-AGE = #ISSUE-YEAR - #WS-DOB-YR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN ( #ISSUE-MONTH > #WS-DOB-MO )
        else if (condition(pnd_Issue_Date_A_Pnd_Issue_Month.greater(pnd_Ws_Dob_Date_Pnd_Ws_Dob_Mo)))
        {
            decideConditionsMet1283++;
            pnd_Ws_Age.compute(new ComputeParameters(false, pnd_Ws_Age), pnd_Issue_Date_A_Pnd_Issue_Year.subtract(pnd_Ws_Dob_Date_Pnd_Ws_Dob_Yr));                        //Natural: COMPUTE #WS-AGE = #ISSUE-YEAR - #WS-DOB-YR
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Ws_Age.compute(new ComputeParameters(false, pnd_Ws_Age), pnd_Issue_Date_A_Pnd_Issue_Year.subtract(pnd_Ws_Dob_Date_Pnd_Ws_Dob_Yr).subtract(1));            //Natural: COMPUTE #WS-AGE = #ISSUE-YEAR - #WS-DOB-YR - 1
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  WRITE 'ACTUAL AGE: ' #WS-AGE
    }
    private void sub_Display_Aian033_Parameters() throws Exception                                                                                                        //Natural: DISPLAY-AIAN033-PARAMETERS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(0, "*******  INPUT  *********");                                                                                                               //Natural: WRITE '*******  INPUT  *********'
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code());                                                                          //Natural: WRITE '=' #AIAN033-OPTION-CODE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Guar_Years());                                                                           //Natural: WRITE '=' #AIAN033-GUAR-YEARS
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Guar_Months());                                                                          //Natural: WRITE '=' #AIAN033-GUAR-MONTHS
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Issue_Date());                                                                           //Natural: WRITE '=' #AIAN033-ISSUE-DATE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_1st_Ann_Birth_Date());                                                                   //Natural: WRITE '=' #AIAN033-1ST-ANN-BIRTH-DATE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Birth_Date());                                                                   //Natural: WRITE '=' #AIAN033-2ND-ANN-BIRTH-DATE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Beneficiary_Birth_Date());                                                               //Natural: WRITE '=' #AIAN033-BENEFICIARY-BIRTH-DATE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Type());                                                                         //Natural: WRITE '=' #AIAN033-2ND-ANN-TYPE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Ben_Type());                                                                             //Natural: WRITE '=' #AIAN033-BEN-TYPE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Settlement_Type());                                                                      //Natural: WRITE '=' #AIAN033-SETTLEMENT-TYPE
        if (Global.isEscape()) return;
        getReports().write(0, "*******  OUTPUT *********");                                                                                                               //Natural: WRITE '*******  OUTPUT *********'
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Md_Exp_Life());                                                                          //Natural: WRITE '=' #AIAN033-MD-EXP-LIFE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Mdib_Exp_Life());                                                                        //Natural: WRITE '=' #AIAN033-MDIB-EXP-LIFE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Minimum_Life_Exp());                                                                     //Natural: WRITE '=' #AIAN033-MINIMUM-LIFE-EXP
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Minimum_Pct_Allow());                                                                    //Natural: WRITE '=' #AIAN033-MINIMUM-PCT-ALLOW
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar());                                                                     //Natural: WRITE '=' #AIAN033-RETURN-CODE-GUAR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar());                                                                      //Natural: WRITE '=' #AIAN033-RETURN-MSG-GUAR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct());                                                                      //Natural: WRITE '=' #AIAN033-RETURN-CODE-PCT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Pct());                                                                       //Natural: WRITE '=' #AIAN033-RETURN-MSG-PCT
        if (Global.isEscape()) return;
    }

    //
}
