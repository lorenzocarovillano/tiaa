/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:54:59 PM
**        * FROM NATURAL SUBPROGRAM : Adsn602
************************************************************
**        * FILE NAME            : Adsn602.java
**        * CLASS NAME           : Adsn602
**        * INSTANCE NAME        : Adsn602
************************************************************
************************************************************************
*
* PROGRAM:  ADSN602
* DATE   :  03/04
* FUNCTION: INTERFACE WITH CPS TO CREATE PAYMENT RECORDS
*
* 01/27/99   ZAFAR KHAN  ADP-BPS-UNIT EXTENDED FROM 6 TO 8 BYTES
*
*
* 01/10/03   JOHN VLISMAS  CHANGE CALENDAR FILE ACCESS
*
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* --------   -------   ---------------------------------------------
*  1/18/2005 T.SHINE   TNT CHANGES
* 09/06/2005 M.NACHBER REL 6.5 FOR IPRO/TPA
*                      NO CHANGES, JUST RECOMPLILED TO PICK UP
*                      A NEW VERSION OF ADSA600
* 04/03/2006 M.NACHBER ICAP  IPRO/TPA
*                      ASSINED LOB  TO ICAP IPRO/TPA CONTRACT RANGES
*                   AND CHANGED RANGES FOR RETIREMENT CHOICE (ORIG.ICAP)
* 03/07/2007 M.NACHBER ATRA
* 02/21/2008 O. SOTTO  RECOMPILED FOR RATE EXPANSION IN ADSA600.
* 03/17/2008 E. MELNIK ROTH 403/401K CHANGES. EM 031708.
* 01/22/09  E. MELNIK TIAA ACCESS/STABLE RETURN ENHANCEMENTS. MARKED
*                         BY EM - 012209.
* 3/16/2010 C. MASON PCPOP CONVERSION - ADD CODE TO HANDLE 457B PRIVATE,
*                    TIAA05/06, TIAA07-10, & CLERGY PLANS
* 11/18/2010 E. MELNIK SVSAA ENHANCEMENTS.  MARKED BY EM - 111810.
* 05/27/2011 E. MELNIK FIF CHANGES.  MARKED BY EM - 052711.
* 12/06/2012 O. SOTTO DI HUB CHANGES.  SC 120611.
* 01/05/2012 O. SOTTO SUNY/CUNY - RECOMPILED FOR UPDATED ADSA600.
* 03/05/2012 O. SOTTO RATE EXPANSION. SC 032912.
* 10/04/2013 E. MELNIK CREF/REA REDESIGN CHANGES.
*                      MARKED BY EM - 100413.
* 02/25/2015 E. MELNIK IRA ROTH CHANGE - INC2708926.
*                      MARKED BY EM - 022515.
* 05/07/2015 E. MELNIK BENE IN THE PLAN - RECOMPILED FOR UPDATED ADSA600
* 10/29/2015 E. MELNIK RIPC CHANGES.  MARKED BY EM - 102915.
* 08/08/2016 J. BREMER   ONEIRA - PLAN HARD CODE CHANGES. SC 080816
* 02/21/2017 R. CARREON RESTOWED FOR PIN EXPANSION 02212017
* 08/05/2019 CHANGE PDQ CALLS TO ADSN. PDQ MODULES SUNSET. SC 082019.
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn602 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAdsa600 pdaAdsa600;
    private PdaFcpa140f pdaFcpa140f;
    private PdaCpwa115 pdaCpwa115;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Cmpny;
    private DbsField pnd_Prtcpnt_Isn;
    private DbsField pnd_First;
    private DbsField pnd_Bsnss_Dte;
    private DbsField pnd_Acctg_Dte;
    private DbsField pnd_Err_Cde;
    private DbsField pnd_Name;
    private DbsField pnd_F_Or_N;
    private DbsField pnd_Adp_Alt_Dest_Hold_Cde;
    private DbsField pnd_Adp_Crrspndnce_Perm_Addr_Txt;
    private DbsField pnd_Adp_Crrspndnce_Perm_Addr_Zip;
    private DbsField pnd_Adp_Crrspndnce_Perm_Addr_Typ_Cd;
    private DbsField pnd_Ia_Orgn_Code;

    private DataAccessProgramView vw_ads_Prtcpnt;
    private DbsField ads_Prtcpnt_Rqst_Id;
    private DbsField ads_Prtcpnt_Adp_Ia_Tiaa_Nbr;
    private DbsField ads_Prtcpnt_Adp_Ia_Tiaa_Rea_Nbr;
    private DbsField ads_Prtcpnt_Adp_Bps_Unit;
    private DbsField ads_Prtcpnt_Adp_Ia_Tiaa_Payee_Cd;
    private DbsField ads_Prtcpnt_Adp_Ia_Cref_Nbr;
    private DbsField ads_Prtcpnt_Adp_Ia_Cref_Payee_Cd;
    private DbsField ads_Prtcpnt_Adp_Annt_Typ_Cde;
    private DbsField ads_Prtcpnt_Adp_Emplymnt_Term_Dte;
    private DbsGroup ads_Prtcpnt_Adp_Cntrcts_In_RqstMuGroup;
    private DbsField ads_Prtcpnt_Adp_Cntrcts_In_Rqst;
    private DbsField ads_Prtcpnt_Adp_Unique_Id;
    private DbsField ads_Prtcpnt_Adp_Effctv_Dte;
    private DbsField ads_Prtcpnt_Adp_Cntr_Prt_Issue_Dte;
    private DbsField ads_Prtcpnt_Adp_Entry_Dte;
    private DbsField ads_Prtcpnt_Adp_Entry_Tme;
    private DbsField ads_Prtcpnt_Adp_Entry_User_Id;
    private DbsField ads_Prtcpnt_Adp_Rep_Nme;
    private DbsField ads_Prtcpnt_Adp_Lst_Actvty_Dte;
    private DbsField ads_Prtcpnt_Adp_Wpid;
    private DbsField ads_Prtcpnt_Adp_Mit_Log_Dte_Tme;
    private DbsField ads_Prtcpnt_Adp_Opn_Clsd_Ind;
    private DbsField ads_Prtcpnt_Adp_In_Prgrss_Ind;
    private DbsField ads_Prtcpnt_Adp_Rcprcl_Dte;
    private DbsField ads_Prtcpnt_Adp_Hold_Cde;
    private DbsField ads_Prtcpnt_Adp_Plan_Nbr;
    private DbsField ads_Prtcpnt_Adp_Irc_Cde;
    private DbsField ads_Prtcpnt_Adp_Stts_Cde;

    private DbsGroup ads_Prtcpnt_Adp_Crrspndce_Mlng_Dta;
    private DbsGroup ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_TxtMuGroup;
    private DbsField ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Txt;
    private DbsField ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Zip;
    private DbsField ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Typ_Cd;
    private DbsField ads_Prtcpnt_Adp_Crrspndnce_Addr_Lst_Chg_Dte;
    private DbsField ads_Prtcpnt_Adp_Forms_Cmplte_Ind;
    private DbsField ads_Prtcpnt_Adp_Frst_Annt_Ctznshp;
    private DbsField ads_Prtcpnt_Adp_Frst_Annt_Rsdnc_Cde;
    private DbsField ads_Prtcpnt_Adp_Frst_Annt_Ssn;
    private DbsField ads_Prtcpnt_Adp_Frst_Annt_Title_Cde;
    private DbsField ads_Prtcpnt_Adp_Frst_Annt_Frst_Nme;
    private DbsField ads_Prtcpnt_Adp_Frst_Annt_Mid_Nme;
    private DbsField ads_Prtcpnt_Adp_Frst_Annt_Lst_Nme;
    private DbsField ads_Prtcpnt_Adp_Frst_Annt_Dte_Of_Brth;
    private DbsField ads_Prtcpnt_Adp_Frst_Annt_Sex_Cde;
    private DbsField ads_Prtcpnt_Adp_Scnd_Annt_Ssn;
    private DbsField ads_Prtcpnt_Adp_Scnd_Annt_Frst_Nme;
    private DbsField ads_Prtcpnt_Adp_Scnd_Annt_Mid_Nme;
    private DbsField ads_Prtcpnt_Adp_Scnd_Annt_Lst_Nme;
    private DbsField ads_Prtcpnt_Adp_Scnd_Annt_Dte_Of_Brth;
    private DbsField ads_Prtcpnt_Adp_Scnd_Annt_Sex_Cde;
    private DbsField ads_Prtcpnt_Adp_Annty_Optn;
    private DbsField ads_Prtcpnt_Adp_Grntee_Period;
    private DbsField ads_Prtcpnt_Adp_Grntee_Period_Mnths;
    private DbsField ads_Prtcpnt_Adp_Annty_Strt_Dte;
    private DbsField ads_Prtcpnt_Adp_Pymnt_Mode;
    private DbsField ads_Prtcpnt_Adp_Settl_All_Tiaa_Grd_Pct;
    private DbsField ads_Prtcpnt_Adp_Settl_All_Cref_Mon_Pct;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Addr_Ind;
    private DbsField ads_Prtcpnt_Adp_Spcfc_Brkdwn_Ind;

    private DbsGroup ads_Prtcpnt_Adp_Alt_Dest_Data;
    private DbsField ads_Prtcpnt_Adp_Alt_Payee_Ind;
    private DbsField ads_Prtcpnt_Adp_Alt_Carrier_Cde;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Nme;
    private DbsGroup ads_Prtcpnt_Adp_Alt_Dest_Addr_TxtMuGroup;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Addr_Txt;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Addr_Zip;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Addr_Typ_Cde;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Acct_Nbr;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Acct_Typ;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Trnst_Cde;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Hold_Cde;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Rlvr_Ind;
    private DbsField ads_Prtcpnt_Adp_Alt_Dest_Rlvr_Dest;
    private DbsField ads_Prtcpnt_Adp_Glbl_Pay_Ind;
    private DbsField ads_Prtcpnt_Adp_Pull_Ia_Cntrct_Pckg_Ind;
    private DbsField ads_Prtcpnt_Adp_Pull_Ia_Cntrct_Pckg_Rsn;
    private DbsField ads_Prtcpnt_Adp_Pull_Crrspndce_Ind;
    private DbsField ads_Prtcpnt_Adp_Pull_Crrspndce_Prntr_Id;
    private DbsField ads_Prtcpnt_Adp_Rsttlmnt_Cnt;
    private DbsField ads_Prtcpnt_Adp_State_Of_Issue;
    private DbsField ads_Prtcpnt_Adp_Orgnl_Issue_State;
    private DbsField ads_Prtcpnt_Adp_Roth_Plan_Type;
    private DbsField ads_Prtcpnt_Adp_Roth_Frst_Cntrbtn_Dte;
    private DbsField ads_Prtcpnt_Adp_Roth_Dsblty_Dte;
    private DbsField ads_Prtcpnt_Adp_Roth_Rqst_Ind;
    private DbsField ads_Prtcpnt_Adp_Mndtry_Tax_Ovrd;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_X;
    private DbsField pnd_In_Data;

    private DbsGroup pnd_In_Data__R_Field_1;
    private DbsField pnd_In_Data_Pnd_Prefix;
    private DbsField pnd_In_Data_Pnd_First_Name;
    private DbsField pnd_In_Data_Pnd_Middle_Name;
    private DbsField pnd_In_Data_Pnd_Last_Name;
    private DbsField pnd_In_Data_Pnd_Suffix;
    private DbsField pnd_In_Data_Pnd_Length;
    private DbsField pnd_In_Data_Pnd_Format;
    private DbsField pnd_Out_Data;

    private DbsGroup pnd_Out_Data__R_Field_2;
    private DbsField pnd_Out_Data_Pnd_Name_Out;
    private DbsField pnd_Out_Data_Pnd_Length_Out;
    private DbsField pnd_Out_Data_Pnd_Return_Code;
    private DbsField pnd_In_Middle_Name;
    private DbsField pnd_Out_Middle_Name;
    private DbsField pnd_Out_Suffix;
    private DbsField pnd_Out_Prefix;
    private DbsField pnd_Out_Return_Code;
    private DbsField pnd_Dte8;
    private DbsField pnd_Start_Dte;

    private DbsGroup pnd_Start_Dte__R_Field_3;
    private DbsField pnd_Start_Dte_Pnd_S_Yyyy;
    private DbsField pnd_Start_Dte_Pnd_S_Mm;
    private DbsField pnd_Start_Dte__Filler1;
    private DbsField pnd_End_Dte;

    private DbsGroup pnd_End_Dte__R_Field_4;
    private DbsField pnd_End_Dte_Pnd_E_Yyyy;
    private DbsField pnd_End_Dte_Pnd_E_Mm;
    private DbsField pnd_End_Dte__Filler2;
    private DbsField pnd_Re_A_Amt;
    private DbsField pnd_Re_M_Amt;
    private DbsField pnd_Age_Yy;
    private DbsField pnd_Age_Mm;
    private DbsField pnd_Ivc_Gd_Ind;
    private DbsField pnd_Prdct_Cde;
    private DbsField pnd_Ret_Cde;
    private DbsField pnd_Roth_Dte;

    private DbsGroup pnd_Roth_Dte__R_Field_5;
    private DbsField pnd_Roth_Dte_Pnd_Roth_Dte_N;
    private DbsField pnd_Stable_Value;
    private DbsField pnd_Stable_Return;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCpwa115 = new PdaCpwa115(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaAdsa600 = new PdaAdsa600(parameters);
        pdaFcpa140f = new PdaFcpa140f(parameters);
        pnd_Cmpny = parameters.newFieldInRecord("pnd_Cmpny", "#CMPNY", FieldType.STRING, 1);
        pnd_Cmpny.setParameterOption(ParameterOption.ByReference);
        pnd_Prtcpnt_Isn = parameters.newFieldInRecord("pnd_Prtcpnt_Isn", "#PRTCPNT-ISN", FieldType.PACKED_DECIMAL, 8);
        pnd_Prtcpnt_Isn.setParameterOption(ParameterOption.ByReference);
        pnd_First = parameters.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_First.setParameterOption(ParameterOption.ByReference);
        pnd_Bsnss_Dte = parameters.newFieldInRecord("pnd_Bsnss_Dte", "#BSNSS-DTE", FieldType.DATE);
        pnd_Bsnss_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Acctg_Dte = parameters.newFieldInRecord("pnd_Acctg_Dte", "#ACCTG-DTE", FieldType.DATE);
        pnd_Acctg_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Err_Cde = parameters.newFieldInRecord("pnd_Err_Cde", "#ERR-CDE", FieldType.STRING, 3);
        pnd_Err_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Name = parameters.newFieldInRecord("pnd_Name", "#NAME", FieldType.STRING, 38);
        pnd_Name.setParameterOption(ParameterOption.ByReference);
        pnd_F_Or_N = parameters.newFieldInRecord("pnd_F_Or_N", "#F-OR-N", FieldType.STRING, 1);
        pnd_F_Or_N.setParameterOption(ParameterOption.ByReference);
        pnd_Adp_Alt_Dest_Hold_Cde = parameters.newFieldInRecord("pnd_Adp_Alt_Dest_Hold_Cde", "#ADP-ALT-DEST-HOLD-CDE", FieldType.STRING, 4);
        pnd_Adp_Alt_Dest_Hold_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_Adp_Crrspndnce_Perm_Addr_Txt = parameters.newFieldArrayInRecord("pnd_Adp_Crrspndnce_Perm_Addr_Txt", "#ADP-CRRSPNDNCE-PERM-ADDR-TXT", FieldType.STRING, 
            35, new DbsArrayController(1, 5));
        pnd_Adp_Crrspndnce_Perm_Addr_Txt.setParameterOption(ParameterOption.ByReference);
        pnd_Adp_Crrspndnce_Perm_Addr_Zip = parameters.newFieldInRecord("pnd_Adp_Crrspndnce_Perm_Addr_Zip", "#ADP-CRRSPNDNCE-PERM-ADDR-ZIP", FieldType.STRING, 
            9);
        pnd_Adp_Crrspndnce_Perm_Addr_Zip.setParameterOption(ParameterOption.ByReference);
        pnd_Adp_Crrspndnce_Perm_Addr_Typ_Cd = parameters.newFieldInRecord("pnd_Adp_Crrspndnce_Perm_Addr_Typ_Cd", "#ADP-CRRSPNDNCE-PERM-ADDR-TYP-CD", FieldType.STRING, 
            1);
        pnd_Adp_Crrspndnce_Perm_Addr_Typ_Cd.setParameterOption(ParameterOption.ByReference);
        pnd_Ia_Orgn_Code = parameters.newFieldInRecord("pnd_Ia_Orgn_Code", "#IA-ORGN-CODE", FieldType.NUMERIC, 2);
        pnd_Ia_Orgn_Code.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_ads_Prtcpnt = new DataAccessProgramView(new NameInfo("vw_ads_Prtcpnt", "ADS-PRTCPNT"), "ADS_PRTCPNT", "ADS_PRTCPNT");
        ads_Prtcpnt_Rqst_Id = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "RQST_ID");
        ads_Prtcpnt_Adp_Ia_Tiaa_Nbr = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Ia_Tiaa_Nbr", "ADP-IA-TIAA-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "ADP_IA_TIAA_NBR");
        ads_Prtcpnt_Adp_Ia_Tiaa_Rea_Nbr = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Ia_Tiaa_Rea_Nbr", "ADP-IA-TIAA-REA-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "ADP_IA_TIAA_REA_NBR");
        ads_Prtcpnt_Adp_Bps_Unit = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Bps_Unit", "ADP-BPS-UNIT", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADP_BPS_UNIT");
        ads_Prtcpnt_Adp_Ia_Tiaa_Payee_Cd = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Ia_Tiaa_Payee_Cd", "ADP-IA-TIAA-PAYEE-CD", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADP_IA_TIAA_PAYEE_CD");
        ads_Prtcpnt_Adp_Ia_Cref_Nbr = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Ia_Cref_Nbr", "ADP-IA-CREF-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "ADP_IA_CREF_NBR");
        ads_Prtcpnt_Adp_Ia_Cref_Payee_Cd = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Ia_Cref_Payee_Cd", "ADP-IA-CREF-PAYEE-CD", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADP_IA_CREF_PAYEE_CD");
        ads_Prtcpnt_Adp_Annt_Typ_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Annt_Typ_Cde", "ADP-ANNT-TYP-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_ANNT_TYP_CDE");
        ads_Prtcpnt_Adp_Emplymnt_Term_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Emplymnt_Term_Dte", "ADP-EMPLYMNT-TERM-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADP_EMPLYMNT_TERM_DTE");
        ads_Prtcpnt_Adp_Cntrcts_In_RqstMuGroup = vw_ads_Prtcpnt.getRecord().newGroupInGroup("ADS_PRTCPNT_ADP_CNTRCTS_IN_RQSTMuGroup", "ADP_CNTRCTS_IN_RQSTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_PRTCPNT_ADP_CNTRCTS_IN_RQST");
        ads_Prtcpnt_Adp_Cntrcts_In_Rqst = ads_Prtcpnt_Adp_Cntrcts_In_RqstMuGroup.newFieldArrayInGroup("ads_Prtcpnt_Adp_Cntrcts_In_Rqst", "ADP-CNTRCTS-IN-RQST", 
            FieldType.STRING, 10, new DbsArrayController(1, 6), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADP_CNTRCTS_IN_RQST");
        ads_Prtcpnt_Adp_Unique_Id = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Unique_Id", "ADP-UNIQUE-ID", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "ADP_UNIQUE_ID");
        ads_Prtcpnt_Adp_Effctv_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Effctv_Dte", "ADP-EFFCTV-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADP_EFFCTV_DTE");
        ads_Prtcpnt_Adp_Cntr_Prt_Issue_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Cntr_Prt_Issue_Dte", "ADP-CNTR-PRT-ISSUE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_CNTR_PRT_ISSUE_DTE");
        ads_Prtcpnt_Adp_Entry_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Entry_Dte", "ADP-ENTRY-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "ADP_ENTRY_DTE");
        ads_Prtcpnt_Adp_Entry_Tme = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Entry_Tme", "ADP-ENTRY-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "ADP_ENTRY_TME");
        ads_Prtcpnt_Adp_Entry_User_Id = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Entry_User_Id", "ADP-ENTRY-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "ADP_ENTRY_USER_ID");
        ads_Prtcpnt_Adp_Rep_Nme = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Rep_Nme", "ADP-REP-NME", FieldType.STRING, 40, RepeatingFieldStrategy.None, 
            "ADP_REP_NME");
        ads_Prtcpnt_Adp_Lst_Actvty_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Lst_Actvty_Dte", "ADP-LST-ACTVTY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADP_LST_ACTVTY_DTE");
        ads_Prtcpnt_Adp_Wpid = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Wpid", "ADP-WPID", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "ADP_WPID");
        ads_Prtcpnt_Adp_Mit_Log_Dte_Tme = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Mit_Log_Dte_Tme", "ADP-MIT-LOG-DTE-TME", FieldType.STRING, 
            15, RepeatingFieldStrategy.None, "ADP_MIT_LOG_DTE_TME");
        ads_Prtcpnt_Adp_Opn_Clsd_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Opn_Clsd_Ind", "ADP-OPN-CLSD-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_OPN_CLSD_IND");
        ads_Prtcpnt_Adp_In_Prgrss_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_In_Prgrss_Ind", "ADP-IN-PRGRSS-IND", FieldType.BOOLEAN, 
            1, RepeatingFieldStrategy.None, "ADP_IN_PRGRSS_IND");
        ads_Prtcpnt_Adp_Rcprcl_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Rcprcl_Dte", "ADP-RCPRCL-DTE", FieldType.NUMERIC, 8, 
            RepeatingFieldStrategy.None, "ADP_RCPRCL_DTE");
        ads_Prtcpnt_Adp_Hold_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Hold_Cde", "ADP-HOLD-CDE", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "ADP_HOLD_CDE");
        ads_Prtcpnt_Adp_Plan_Nbr = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Plan_Nbr", "ADP-PLAN-NBR", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "ADP_PLAN_NBR");
        ads_Prtcpnt_Adp_Irc_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Irc_Cde", "ADP-IRC-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ADP_IRC_CDE");
        ads_Prtcpnt_Adp_Stts_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Stts_Cde", "ADP-STTS-CDE", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "ADP_STTS_CDE");

        ads_Prtcpnt_Adp_Crrspndce_Mlng_Dta = vw_ads_Prtcpnt.getRecord().newGroupInGroup("ADS_PRTCPNT_ADP_CRRSPNDCE_MLNG_DTA", "ADP-CRRSPNDCE-MLNG-DTA");
        ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_TxtMuGroup = ads_Prtcpnt_Adp_Crrspndce_Mlng_Dta.newGroupInGroup("ADS_PRTCPNT_ADP_CRRSPNDNCE_PERM_ADDR_TXTMuGroup", 
            "ADP_CRRSPNDNCE_PERM_ADDR_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "ADS_PRTCPNT_ADP_CRRSPNDNCE_PERM_ADDR_TXT");
        ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Txt = ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_TxtMuGroup.newFieldArrayInGroup("ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Txt", 
            "ADP-CRRSPNDNCE-PERM-ADDR-TXT", FieldType.STRING, 35, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArray, "ADP_CRRSPNDNCE_PERM_ADDR_TXT");
        ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Zip = ads_Prtcpnt_Adp_Crrspndce_Mlng_Dta.newFieldInGroup("ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Zip", "ADP-CRRSPNDNCE-PERM-ADDR-ZIP", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "ADP_CRRSPNDNCE_PERM_ADDR_ZIP");
        ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Typ_Cd = ads_Prtcpnt_Adp_Crrspndce_Mlng_Dta.newFieldInGroup("ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Typ_Cd", 
            "ADP-CRRSPNDNCE-PERM-ADDR-TYP-CD", FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_CRRSPNDNCE_PERM_ADDR_TYP_CD");
        ads_Prtcpnt_Adp_Crrspndnce_Addr_Lst_Chg_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Crrspndnce_Addr_Lst_Chg_Dte", "ADP-CRRSPNDNCE-ADDR-LST-CHG-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_CRRSPNDNCE_ADDR_LST_CHG_DTE");
        ads_Prtcpnt_Adp_Forms_Cmplte_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Forms_Cmplte_Ind", "ADP-FORMS-CMPLTE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_FORMS_CMPLTE_IND");
        ads_Prtcpnt_Adp_Frst_Annt_Ctznshp = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Frst_Annt_Ctznshp", "ADP-FRST-ANNT-CTZNSHP", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_CTZNSHP");
        ads_Prtcpnt_Adp_Frst_Annt_Rsdnc_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Frst_Annt_Rsdnc_Cde", "ADP-FRST-ANNT-RSDNC-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_RSDNC_CDE");
        ads_Prtcpnt_Adp_Frst_Annt_Ssn = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Frst_Annt_Ssn", "ADP-FRST-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_SSN");
        ads_Prtcpnt_Adp_Frst_Annt_Title_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Frst_Annt_Title_Cde", "ADP-FRST-ANNT-TITLE-CDE", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_TITLE_CDE");
        ads_Prtcpnt_Adp_Frst_Annt_Frst_Nme = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Frst_Annt_Frst_Nme", "ADP-FRST-ANNT-FRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_FRST_NME");
        ads_Prtcpnt_Adp_Frst_Annt_Mid_Nme = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Frst_Annt_Mid_Nme", "ADP-FRST-ANNT-MID-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_MID_NME");
        ads_Prtcpnt_Adp_Frst_Annt_Lst_Nme = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Frst_Annt_Lst_Nme", "ADP-FRST-ANNT-LST-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_LST_NME");
        ads_Prtcpnt_Adp_Frst_Annt_Dte_Of_Brth = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Frst_Annt_Dte_Of_Brth", "ADP-FRST-ANNT-DTE-OF-BRTH", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_DTE_OF_BRTH");
        ads_Prtcpnt_Adp_Frst_Annt_Sex_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Frst_Annt_Sex_Cde", "ADP-FRST-ANNT-SEX-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_FRST_ANNT_SEX_CDE");
        ads_Prtcpnt_Adp_Scnd_Annt_Ssn = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Scnd_Annt_Ssn", "ADP-SCND-ANNT-SSN", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_SSN");
        ads_Prtcpnt_Adp_Scnd_Annt_Frst_Nme = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Scnd_Annt_Frst_Nme", "ADP-SCND-ANNT-FRST-NME", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_FRST_NME");
        ads_Prtcpnt_Adp_Scnd_Annt_Mid_Nme = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Scnd_Annt_Mid_Nme", "ADP-SCND-ANNT-MID-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_MID_NME");
        ads_Prtcpnt_Adp_Scnd_Annt_Lst_Nme = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Scnd_Annt_Lst_Nme", "ADP-SCND-ANNT-LST-NME", FieldType.STRING, 
            30, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_LST_NME");
        ads_Prtcpnt_Adp_Scnd_Annt_Dte_Of_Brth = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Scnd_Annt_Dte_Of_Brth", "ADP-SCND-ANNT-DTE-OF-BRTH", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_DTE_OF_BRTH");
        ads_Prtcpnt_Adp_Scnd_Annt_Sex_Cde = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Scnd_Annt_Sex_Cde", "ADP-SCND-ANNT-SEX-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_SCND_ANNT_SEX_CDE");
        ads_Prtcpnt_Adp_Annty_Optn = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Annty_Optn", "ADP-ANNTY-OPTN", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ADP_ANNTY_OPTN");
        ads_Prtcpnt_Adp_Grntee_Period = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Grntee_Period", "ADP-GRNTEE-PERIOD", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "ADP_GRNTEE_PERIOD");
        ads_Prtcpnt_Adp_Grntee_Period_Mnths = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Grntee_Period_Mnths", "ADP-GRNTEE-PERIOD-MNTHS", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "ADP_GRNTEE_PERIOD_MNTHS");
        ads_Prtcpnt_Adp_Annty_Strt_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Annty_Strt_Dte", "ADP-ANNTY-STRT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADP_ANNTY_STRT_DTE");
        ads_Prtcpnt_Adp_Pymnt_Mode = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Pymnt_Mode", "ADP-PYMNT-MODE", FieldType.NUMERIC, 3, 
            RepeatingFieldStrategy.None, "ADP_PYMNT_MODE");
        ads_Prtcpnt_Adp_Settl_All_Tiaa_Grd_Pct = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Settl_All_Tiaa_Grd_Pct", "ADP-SETTL-ALL-TIAA-GRD-PCT", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "ADP_SETTL_ALL_TIAA_GRD_PCT");
        ads_Prtcpnt_Adp_Settl_All_Cref_Mon_Pct = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Settl_All_Cref_Mon_Pct", "ADP-SETTL-ALL-CREF-MON-PCT", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "ADP_SETTL_ALL_CREF_MON_PCT");
        ads_Prtcpnt_Adp_Alt_Dest_Addr_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Addr_Ind", "ADP-ALT-DEST-ADDR-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_ALT_DEST_ADDR_IND");
        ads_Prtcpnt_Adp_Spcfc_Brkdwn_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Spcfc_Brkdwn_Ind", "ADP-SPCFC-BRKDWN-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_SPCFC_BRKDWN_IND");

        ads_Prtcpnt_Adp_Alt_Dest_Data = vw_ads_Prtcpnt.getRecord().newGroupInGroup("ADS_PRTCPNT_ADP_ALT_DEST_DATA", "ADP-ALT-DEST-DATA");
        ads_Prtcpnt_Adp_Alt_Payee_Ind = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Payee_Ind", "ADP-ALT-PAYEE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_ALT_PAYEE_IND");
        ads_Prtcpnt_Adp_Alt_Carrier_Cde = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Carrier_Cde", "ADP-ALT-CARRIER-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "ADP_ALT_CARRIER_CDE");
        ads_Prtcpnt_Adp_Alt_Dest_Nme = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Nme", "ADP-ALT-DEST-NME", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "ADP_ALT_DEST_NME");
        ads_Prtcpnt_Adp_Alt_Dest_Addr_TxtMuGroup = ads_Prtcpnt_Adp_Alt_Dest_Data.newGroupInGroup("ADS_PRTCPNT_ADP_ALT_DEST_ADDR_TXTMuGroup", "ADP_ALT_DEST_ADDR_TXTMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "ADS_PRTCPNT_ADP_ALT_DEST_ADDR_TXT");
        ads_Prtcpnt_Adp_Alt_Dest_Addr_Txt = ads_Prtcpnt_Adp_Alt_Dest_Addr_TxtMuGroup.newFieldArrayInGroup("ads_Prtcpnt_Adp_Alt_Dest_Addr_Txt", "ADP-ALT-DEST-ADDR-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 5), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "ADP_ALT_DEST_ADDR_TXT");
        ads_Prtcpnt_Adp_Alt_Dest_Addr_Zip = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Addr_Zip", "ADP-ALT-DEST-ADDR-ZIP", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "ADP_ALT_DEST_ADDR_ZIP");
        ads_Prtcpnt_Adp_Alt_Dest_Addr_Typ_Cde = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Addr_Typ_Cde", "ADP-ALT-DEST-ADDR-TYP-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_ALT_DEST_ADDR_TYP_CDE");
        ads_Prtcpnt_Adp_Alt_Dest_Acct_Nbr = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Acct_Nbr", "ADP-ALT-DEST-ACCT-NBR", 
            FieldType.STRING, 21, RepeatingFieldStrategy.None, "ADP_ALT_DEST_ACCT_NBR");
        ads_Prtcpnt_Adp_Alt_Dest_Acct_Typ = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Acct_Typ", "ADP-ALT-DEST-ACCT-TYP", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_ALT_DEST_ACCT_TYP");
        ads_Prtcpnt_Adp_Alt_Dest_Trnst_Cde = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Trnst_Cde", "ADP-ALT-DEST-TRNST-CDE", 
            FieldType.STRING, 9, RepeatingFieldStrategy.None, "ADP_ALT_DEST_TRNST_CDE");
        ads_Prtcpnt_Adp_Alt_Dest_Hold_Cde = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Hold_Cde", "ADP-ALT-DEST-HOLD-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADP_ALT_DEST_HOLD_CDE");
        ads_Prtcpnt_Adp_Alt_Dest_Rlvr_Ind = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Rlvr_Ind", "ADP-ALT-DEST-RLVR-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_ALT_DEST_RLVR_IND");
        ads_Prtcpnt_Adp_Alt_Dest_Rlvr_Dest = ads_Prtcpnt_Adp_Alt_Dest_Data.newFieldInGroup("ads_Prtcpnt_Adp_Alt_Dest_Rlvr_Dest", "ADP-ALT-DEST-RLVR-DEST", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADP_ALT_DEST_RLVR_DEST");
        ads_Prtcpnt_Adp_Glbl_Pay_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Glbl_Pay_Ind", "ADP-GLBL-PAY-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_GLBL_PAY_IND");
        ads_Prtcpnt_Adp_Pull_Ia_Cntrct_Pckg_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Pull_Ia_Cntrct_Pckg_Ind", "ADP-PULL-IA-CNTRCT-PCKG-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_PULL_IA_CNTRCT_PCKG_IND");
        ads_Prtcpnt_Adp_Pull_Ia_Cntrct_Pckg_Rsn = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Pull_Ia_Cntrct_Pckg_Rsn", "ADP-PULL-IA-CNTRCT-PCKG-RSN", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADP_PULL_IA_CNTRCT_PCKG_RSN");
        ads_Prtcpnt_Adp_Pull_Crrspndce_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Pull_Crrspndce_Ind", "ADP-PULL-CRRSPNDCE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "ADP_PULL_CRRSPNDCE_IND");
        ads_Prtcpnt_Adp_Pull_Crrspndce_Prntr_Id = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Pull_Crrspndce_Prntr_Id", "ADP-PULL-CRRSPNDCE-PRNTR-ID", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "ADP_PULL_CRRSPNDCE_PRNTR_ID");
        ads_Prtcpnt_Adp_Rsttlmnt_Cnt = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Rsttlmnt_Cnt", "ADP-RSTTLMNT-CNT", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_RSTTLMNT_CNT");
        ads_Prtcpnt_Adp_State_Of_Issue = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_State_Of_Issue", "ADP-STATE-OF-ISSUE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADP_STATE_OF_ISSUE");
        ads_Prtcpnt_Adp_Orgnl_Issue_State = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Orgnl_Issue_State", "ADP-ORGNL-ISSUE-STATE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ADP_ORGNL_ISSUE_STATE");
        ads_Prtcpnt_Adp_Roth_Plan_Type = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Roth_Plan_Type", "ADP-ROTH-PLAN-TYPE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_ROTH_PLAN_TYPE");
        ads_Prtcpnt_Adp_Roth_Frst_Cntrbtn_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Roth_Frst_Cntrbtn_Dte", "ADP-ROTH-FRST-CNTRBTN-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_ROTH_FRST_CNTRBTN_DTE");
        ads_Prtcpnt_Adp_Roth_Dsblty_Dte = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Roth_Dsblty_Dte", "ADP-ROTH-DSBLTY-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "ADP_ROTH_DSBLTY_DTE");
        ads_Prtcpnt_Adp_Roth_Rqst_Ind = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Roth_Rqst_Ind", "ADP-ROTH-RQST-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_ROTH_RQST_IND");
        ads_Prtcpnt_Adp_Mndtry_Tax_Ovrd = vw_ads_Prtcpnt.getRecord().newFieldInGroup("ads_Prtcpnt_Adp_Mndtry_Tax_Ovrd", "ADP-MNDTRY-TAX-OVRD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_MNDTRY_TAX_OVRD");
        registerRecord(vw_ads_Prtcpnt);

        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 3);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 3);
        pnd_In_Data = localVariables.newFieldInRecord("pnd_In_Data", "#IN-DATA", FieldType.STRING, 134);

        pnd_In_Data__R_Field_1 = localVariables.newGroupInRecord("pnd_In_Data__R_Field_1", "REDEFINE", pnd_In_Data);
        pnd_In_Data_Pnd_Prefix = pnd_In_Data__R_Field_1.newFieldInGroup("pnd_In_Data_Pnd_Prefix", "#PREFIX", FieldType.STRING, 20);
        pnd_In_Data_Pnd_First_Name = pnd_In_Data__R_Field_1.newFieldInGroup("pnd_In_Data_Pnd_First_Name", "#FIRST-NAME", FieldType.STRING, 30);
        pnd_In_Data_Pnd_Middle_Name = pnd_In_Data__R_Field_1.newFieldInGroup("pnd_In_Data_Pnd_Middle_Name", "#MIDDLE-NAME", FieldType.STRING, 30);
        pnd_In_Data_Pnd_Last_Name = pnd_In_Data__R_Field_1.newFieldInGroup("pnd_In_Data_Pnd_Last_Name", "#LAST-NAME", FieldType.STRING, 30);
        pnd_In_Data_Pnd_Suffix = pnd_In_Data__R_Field_1.newFieldInGroup("pnd_In_Data_Pnd_Suffix", "#SUFFIX", FieldType.STRING, 20);
        pnd_In_Data_Pnd_Length = pnd_In_Data__R_Field_1.newFieldInGroup("pnd_In_Data_Pnd_Length", "#LENGTH", FieldType.NUMERIC, 3);
        pnd_In_Data_Pnd_Format = pnd_In_Data__R_Field_1.newFieldInGroup("pnd_In_Data_Pnd_Format", "#FORMAT", FieldType.STRING, 1);
        pnd_Out_Data = localVariables.newFieldInRecord("pnd_Out_Data", "#OUT-DATA", FieldType.STRING, 134);

        pnd_Out_Data__R_Field_2 = localVariables.newGroupInRecord("pnd_Out_Data__R_Field_2", "REDEFINE", pnd_Out_Data);
        pnd_Out_Data_Pnd_Name_Out = pnd_Out_Data__R_Field_2.newFieldInGroup("pnd_Out_Data_Pnd_Name_Out", "#NAME-OUT", FieldType.STRING, 130);
        pnd_Out_Data_Pnd_Length_Out = pnd_Out_Data__R_Field_2.newFieldInGroup("pnd_Out_Data_Pnd_Length_Out", "#LENGTH-OUT", FieldType.NUMERIC, 3);
        pnd_Out_Data_Pnd_Return_Code = pnd_Out_Data__R_Field_2.newFieldInGroup("pnd_Out_Data_Pnd_Return_Code", "#RETURN-CODE", FieldType.STRING, 1);
        pnd_In_Middle_Name = localVariables.newFieldInRecord("pnd_In_Middle_Name", "#IN-MIDDLE-NAME", FieldType.STRING, 30);
        pnd_Out_Middle_Name = localVariables.newFieldInRecord("pnd_Out_Middle_Name", "#OUT-MIDDLE-NAME", FieldType.STRING, 30);
        pnd_Out_Suffix = localVariables.newFieldInRecord("pnd_Out_Suffix", "#OUT-SUFFIX", FieldType.STRING, 3);
        pnd_Out_Prefix = localVariables.newFieldInRecord("pnd_Out_Prefix", "#OUT-PREFIX", FieldType.STRING, 3);
        pnd_Out_Return_Code = localVariables.newFieldInRecord("pnd_Out_Return_Code", "#OUT-RETURN-CODE", FieldType.STRING, 1);
        pnd_Dte8 = localVariables.newFieldInRecord("pnd_Dte8", "#DTE8", FieldType.STRING, 8);
        pnd_Start_Dte = localVariables.newFieldInRecord("pnd_Start_Dte", "#START-DTE", FieldType.NUMERIC, 8);

        pnd_Start_Dte__R_Field_3 = localVariables.newGroupInRecord("pnd_Start_Dte__R_Field_3", "REDEFINE", pnd_Start_Dte);
        pnd_Start_Dte_Pnd_S_Yyyy = pnd_Start_Dte__R_Field_3.newFieldInGroup("pnd_Start_Dte_Pnd_S_Yyyy", "#S-YYYY", FieldType.NUMERIC, 4);
        pnd_Start_Dte_Pnd_S_Mm = pnd_Start_Dte__R_Field_3.newFieldInGroup("pnd_Start_Dte_Pnd_S_Mm", "#S-MM", FieldType.NUMERIC, 2);
        pnd_Start_Dte__Filler1 = pnd_Start_Dte__R_Field_3.newFieldInGroup("pnd_Start_Dte__Filler1", "_FILLER1", FieldType.STRING, 2);
        pnd_End_Dte = localVariables.newFieldInRecord("pnd_End_Dte", "#END-DTE", FieldType.NUMERIC, 8);

        pnd_End_Dte__R_Field_4 = localVariables.newGroupInRecord("pnd_End_Dte__R_Field_4", "REDEFINE", pnd_End_Dte);
        pnd_End_Dte_Pnd_E_Yyyy = pnd_End_Dte__R_Field_4.newFieldInGroup("pnd_End_Dte_Pnd_E_Yyyy", "#E-YYYY", FieldType.NUMERIC, 4);
        pnd_End_Dte_Pnd_E_Mm = pnd_End_Dte__R_Field_4.newFieldInGroup("pnd_End_Dte_Pnd_E_Mm", "#E-MM", FieldType.NUMERIC, 2);
        pnd_End_Dte__Filler2 = pnd_End_Dte__R_Field_4.newFieldInGroup("pnd_End_Dte__Filler2", "_FILLER2", FieldType.STRING, 2);
        pnd_Re_A_Amt = localVariables.newFieldInRecord("pnd_Re_A_Amt", "#RE-A-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Re_M_Amt = localVariables.newFieldInRecord("pnd_Re_M_Amt", "#RE-M-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Age_Yy = localVariables.newFieldInRecord("pnd_Age_Yy", "#AGE-YY", FieldType.NUMERIC, 3);
        pnd_Age_Mm = localVariables.newFieldInRecord("pnd_Age_Mm", "#AGE-MM", FieldType.NUMERIC, 2);
        pnd_Ivc_Gd_Ind = localVariables.newFieldInRecord("pnd_Ivc_Gd_Ind", "#IVC-GD-IND", FieldType.STRING, 1);
        pnd_Prdct_Cde = localVariables.newFieldInRecord("pnd_Prdct_Cde", "#PRDCT-CDE", FieldType.STRING, 10);
        pnd_Ret_Cde = localVariables.newFieldInRecord("pnd_Ret_Cde", "#RET-CDE", FieldType.NUMERIC, 2);
        pnd_Roth_Dte = localVariables.newFieldInRecord("pnd_Roth_Dte", "#ROTH-DTE", FieldType.STRING, 8);

        pnd_Roth_Dte__R_Field_5 = localVariables.newGroupInRecord("pnd_Roth_Dte__R_Field_5", "REDEFINE", pnd_Roth_Dte);
        pnd_Roth_Dte_Pnd_Roth_Dte_N = pnd_Roth_Dte__R_Field_5.newFieldInGroup("pnd_Roth_Dte_Pnd_Roth_Dte_N", "#ROTH-DTE-N", FieldType.NUMERIC, 8);
        pnd_Stable_Value = localVariables.newFieldInRecord("pnd_Stable_Value", "#STABLE-VALUE", FieldType.STRING, 4);
        pnd_Stable_Return = localVariables.newFieldInRecord("pnd_Stable_Return", "#STABLE-RETURN", FieldType.STRING, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ads_Prtcpnt.reset();

        parameters.reset();
        localVariables.reset();
        pnd_Stable_Value.setInitialValue("TSV0");
        pnd_Stable_Return.setInitialValue("TSA0");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn602() throws Exception
    {
        super("Adsn602");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        if (condition(pnd_First.getBoolean()))                                                                                                                            //Natural: IF #FIRST
        {
            pnd_Name.reset();                                                                                                                                             //Natural: RESET #NAME ADSA600.#THIS-INST GTN-PDA-F.PYMNT-EFT-TRANSIT-ID GTN-PDA-F.CNTRCT-HOLD-CDE GTN-PDA-F.CNTRCT-AC-LT-10YRS GTN-PDA-F.PYMNT-EFT-ACCT-NBR
            pdaAdsa600.getAdsa600_Pnd_This_Inst().reset();
            pdaFcpa140f.getGtn_Pda_F_Pymnt_Eft_Transit_Id().reset();
            pdaFcpa140f.getGtn_Pda_F_Cntrct_Hold_Cde().reset();
            pdaFcpa140f.getGtn_Pda_F_Cntrct_Ac_Lt_10yrs().reset();
            pdaFcpa140f.getGtn_Pda_F_Pymnt_Eft_Acct_Nbr().reset();
            pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_First_Sttlmnt_Call().setValue(true);                                                                                         //Natural: ASSIGN GTN-PDA-F.##FIRST-STTLMNT-CALL := TRUE
            GET01:                                                                                                                                                        //Natural: GET ADS-PRTCPNT #PRTCPNT-ISN
            vw_ads_Prtcpnt.readByID(pnd_Prtcpnt_Isn.getLong(), "GET01");
                                                                                                                                                                          //Natural: PERFORM MOVE-PDA-FIELDS
            sub_Move_Pda_Fields();
            if (condition(Global.isEscape())) {return;}
            pdaAdsa600.getAdsa600().setValuesByName(vw_ads_Prtcpnt);                                                                                                      //Natural: MOVE BY NAME ADS-PRTCPNT TO ADSA600
            pdaFcpa140f.getGtn_Pda_F_Annt_Soc_Sec_Nbr().setValue(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Ssn());                                                              //Natural: ASSIGN GTN-PDA-F.ANNT-SOC-SEC-NBR := ADSA600.ADP-FRST-ANNT-SSN
            short decideConditionsMet660 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE ADSA600.ADP-FRST-ANNT-CTZNSHP;//Natural: VALUE 'US'
            if (condition((pdaAdsa600.getAdsa600_Adp_Frst_Annt_Ctznshp().equals("US"))))
            {
                decideConditionsMet660++;
                pdaFcpa140f.getGtn_Pda_F_Annt_Ctznshp_Cde().setValue(1);                                                                                                  //Natural: ASSIGN GTN-PDA-F.ANNT-CTZNSHP-CDE := 01
            }                                                                                                                                                             //Natural: VALUE 'CA'
            else if (condition((pdaAdsa600.getAdsa600_Adp_Frst_Annt_Ctznshp().equals("CA"))))
            {
                decideConditionsMet660++;
                pdaFcpa140f.getGtn_Pda_F_Annt_Ctznshp_Cde().setValue(2);                                                                                                  //Natural: ASSIGN GTN-PDA-F.ANNT-CTZNSHP-CDE := 02
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pdaFcpa140f.getGtn_Pda_F_Annt_Ctznshp_Cde().setValue(3);                                                                                                  //Natural: ASSIGN GTN-PDA-F.ANNT-CTZNSHP-CDE := 03
            }                                                                                                                                                             //Natural: END-DECIDE
            pdaFcpa140f.getGtn_Pda_F_Annt_Rsdncy_Cde().setValue(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Rsdnc_Cde());                                                         //Natural: ASSIGN GTN-PDA-F.ANNT-RSDNCY-CDE := ADSA600.ADP-FRST-ANNT-RSDNC-CDE
            pdaFcpa140f.getGtn_Pda_F_Ph_Last_Name().setValue(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Lst_Nme());                                                              //Natural: ASSIGN GTN-PDA-F.PH-LAST-NAME := ADSA600.ADP-FRST-ANNT-LST-NME
            pdaFcpa140f.getGtn_Pda_F_Ph_Middle_Name().setValue(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Mid_Nme());                                                            //Natural: ASSIGN GTN-PDA-F.PH-MIDDLE-NAME := ADSA600.ADP-FRST-ANNT-MID-NME
            pdaFcpa140f.getGtn_Pda_F_Ph_First_Name().setValue(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Frst_Nme());                                                            //Natural: ASSIGN GTN-PDA-F.PH-FIRST-NAME := ADSA600.ADP-FRST-ANNT-FRST-NME
            pdaFcpa140f.getGtn_Pda_F_Pymnt_Dob().setValue(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Dte_Of_Brth());                                                             //Natural: ASSIGN GTN-PDA-F.PYMNT-DOB := ADSA600.ADP-FRST-ANNT-DTE-OF-BRTH
            pdaFcpa140f.getGtn_Pda_F_Ph_Sex().setValue(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Sex_Cde());                                                                    //Natural: ASSIGN GTN-PDA-F.PH-SEX := ADSA600.ADP-FRST-ANNT-SEX-CDE
            pdaFcpa140f.getGtn_Pda_F_Cntrct_Option_Cde().setValue(pdaAdsa600.getAdsa600_Adi_Optn_Cde());                                                                  //Natural: ASSIGN GTN-PDA-F.CNTRCT-OPTION-CDE := ADSA600.ADI-OPTN-CDE
            pdaFcpa140f.getGtn_Pda_F_Pymnt_Corp_Wpid().setValue(pdaAdsa600.getAdsa600_Adp_Wpid());                                                                        //Natural: ASSIGN GTN-PDA-F.PYMNT-CORP-WPID := ADSA600.ADP-WPID
            pdaFcpa140f.getGtn_Pda_F_Pymnt_Reqst_Log_Dte_Time().setValue(pdaAdsa600.getAdsa600_Adp_Mit_Log_Dte_Tme());                                                    //Natural: ASSIGN GTN-PDA-F.PYMNT-REQST-LOG-DTE-TIME := ADSA600.ADP-MIT-LOG-DTE-TME
            pdaFcpa140f.getGtn_Pda_F_Cntrct_Unq_Id_Nbr().setValue(pdaAdsa600.getAdsa600_Adp_Unique_Id());                                                                 //Natural: ASSIGN GTN-PDA-F.CNTRCT-UNQ-ID-NBR := ADSA600.ADP-UNIQUE-ID
            pdaFcpa140f.getGtn_Pda_F_Pymnt_Ia_Issue_Dte().setValue(pdaAdsa600.getAdsa600_Adi_Annty_Strt_Dte());                                                           //Natural: ASSIGN GTN-PDA-F.PYMNT-IA-ISSUE-DTE := ADSA600.ADI-ANNTY-STRT-DTE
            pdaFcpa140f.getGtn_Pda_F_Cntrct_Mode_Cde().setValue(pdaAdsa600.getAdsa600_Adi_Pymnt_Mode());                                                                  //Natural: ASSIGN GTN-PDA-F.CNTRCT-MODE-CDE := ADSA600.ADI-PYMNT-MODE
            pdaFcpa140f.getGtn_Pda_F_Pymnt_Method_Cde().setValue(pdaAdsa600.getAdsa600_Adi_Pymnt_Cde());                                                                  //Natural: ASSIGN GTN-PDA-F.PYMNT-METHOD-CDE := ADSA600.ADI-PYMNT-CDE
            pdaFcpa140f.getGtn_Pda_F_Cntrct_Check_Crrncy_Cde().setValue(1);                                                                                               //Natural: ASSIGN GTN-PDA-F.CNTRCT-CHECK-CRRNCY-CDE := 1
            //*  CM 3/21 START
            //*  IF ADSA600.ADP-SRVVR-IND = 'Y'     /* EM - 102915 START
            //*  EM - 102915 END
            if (condition(pdaAdsa600.getAdsa600_Adp_Srvvr_Ind().equals("B")))                                                                                             //Natural: IF ADSA600.ADP-SRVVR-IND = 'B'
            {
                short decideConditionsMet689 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SUBSTRING ( ADSA600.ADP-IA-TIAA-NBR,1,2 ) = 'GW' OR = 'W0'
                if (condition(pdaAdsa600.getAdsa600_Adp_Ia_Tiaa_Nbr().getSubstring(1,2).equals("GW") || pdaAdsa600.getAdsa600_Adp_Ia_Tiaa_Nbr().getSubstring(1,
                    2).equals("W0")))
                {
                    decideConditionsMet689++;
                    pdaFcpa140f.getGtn_Pda_F_Cntrct_Annty_Ins_Type().setValue("A");                                                                                       //Natural: ASSIGN GTN-PDA-F.CNTRCT-ANNTY-INS-TYPE := 'A'
                    pdaFcpa140f.getGtn_Pda_F_Cntrct_Annty_Type_Cde().setValue("D");                                                                                       //Natural: ASSIGN GTN-PDA-F.CNTRCT-ANNTY-TYPE-CDE := 'D'
                    pdaFcpa140f.getGtn_Pda_F_Cntrct_Insurance_Option().setValue(" ");                                                                                     //Natural: ASSIGN GTN-PDA-F.CNTRCT-INSURANCE-OPTION := ' '
                    pdaFcpa140f.getGtn_Pda_F_Cntrct_Life_Contingency().setValue("Y");                                                                                     //Natural: ASSIGN GTN-PDA-F.CNTRCT-LIFE-CONTINGENCY := 'Y'
                }                                                                                                                                                         //Natural: WHEN SUBSTRING ( ADSA600.ADP-IA-TIAA-NBR,1,2 ) = 'S0'
                else if (condition(pdaAdsa600.getAdsa600_Adp_Ia_Tiaa_Nbr().getSubstring(1,2).equals("S0")))
                {
                    decideConditionsMet689++;
                    pdaFcpa140f.getGtn_Pda_F_Cntrct_Annty_Ins_Type().setValue("A");                                                                                       //Natural: ASSIGN GTN-PDA-F.CNTRCT-ANNTY-INS-TYPE := 'A'
                    pdaFcpa140f.getGtn_Pda_F_Cntrct_Annty_Type_Cde().setValue("D");                                                                                       //Natural: ASSIGN GTN-PDA-F.CNTRCT-ANNTY-TYPE-CDE := 'D'
                    pdaFcpa140f.getGtn_Pda_F_Cntrct_Insurance_Option().setValue(" ");                                                                                     //Natural: ASSIGN GTN-PDA-F.CNTRCT-INSURANCE-OPTION := ' '
                    pdaFcpa140f.getGtn_Pda_F_Cntrct_Life_Contingency().setValue("N");                                                                                     //Natural: ASSIGN GTN-PDA-F.CNTRCT-LIFE-CONTINGENCY := 'N'
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  CM 3/21 END
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Annty_Ins_Type().setValue("A");                                                                                           //Natural: ASSIGN GTN-PDA-F.CNTRCT-ANNTY-INS-TYPE := 'A'
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Annty_Type_Cde().setValue("M");                                                                                           //Natural: ASSIGN GTN-PDA-F.CNTRCT-ANNTY-TYPE-CDE := 'M'
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Insurance_Option().setValue(" ");                                                                                         //Natural: ASSIGN GTN-PDA-F.CNTRCT-INSURANCE-OPTION := ' '
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Life_Contingency().setValue(" ");                                                                                         //Natural: ASSIGN GTN-PDA-F.CNTRCT-LIFE-CONTINGENCY := ' '
                //*  CM 3/21
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa140f.getGtn_Pda_F_Pymnt_Check_Dte().setValue(pnd_Acctg_Dte);                                                                                           //Natural: ASSIGN GTN-PDA-F.PYMNT-CHECK-DTE := #ACCTG-DTE
            pdaFcpa140f.getGtn_Pda_F_Pymnt_Ia_Issue_Dte().setValue(pdaAdsa600.getAdsa600_Adi_Annty_Strt_Dte());                                                           //Natural: ASSIGN GTN-PDA-F.PYMNT-IA-ISSUE-DTE := ADSA600.ADI-ANNTY-STRT-DTE
            pdaFcpa140f.getGtn_Pda_F_Pymnt_Acctg_Dte().setValue(pnd_Acctg_Dte);                                                                                           //Natural: ASSIGN GTN-PDA-F.PYMNT-ACCTG-DTE := #ACCTG-DTE
            pdaFcpa140f.getGtn_Pda_F_Pymnt_Intrfce_Dte().setValue(pnd_Acctg_Dte);                                                                                         //Natural: ASSIGN GTN-PDA-F.PYMNT-INTRFCE-DTE := #ACCTG-DTE
            pnd_In_Data_Pnd_First_Name.setValue(DbsUtil.compress(ads_Prtcpnt_Adp_Frst_Annt_Title_Cde, ads_Prtcpnt_Adp_Frst_Annt_Frst_Nme));                               //Natural: COMPRESS ADS-PRTCPNT.ADP-FRST-ANNT-TITLE-CDE ADS-PRTCPNT.ADP-FRST-ANNT-FRST-NME INTO #FIRST-NAME
            pnd_In_Middle_Name.setValue(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Mid_Nme());                                                                                   //Natural: ASSIGN #IN-MIDDLE-NAME := ADSA600.ADP-FRST-ANNT-MID-NME
            pnd_In_Data_Pnd_Last_Name.setValue(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Lst_Nme());                                                                            //Natural: ASSIGN #LAST-NAME := ADSA600.ADP-FRST-ANNT-LST-NME
            //*  082019 START
            //* *   CALLNAT 'PDQN362' #IN-MIDDLE-NAME #OUT-MIDDLE-NAME #OUT-SUFFIX
            DbsUtil.callnat(Adsn362.class , getCurrentProcessState(), pnd_In_Middle_Name, pnd_Out_Middle_Name, pnd_Out_Suffix, pnd_Out_Prefix, pnd_Out_Return_Code);      //Natural: CALLNAT 'ADSN362' #IN-MIDDLE-NAME #OUT-MIDDLE-NAME #OUT-SUFFIX #OUT-PREFIX #OUT-RETURN-CODE
            if (condition(Global.isEscape())) return;
            //*  082019 END
            pnd_In_Data_Pnd_Prefix.setValue(pnd_Out_Prefix);                                                                                                              //Natural: MOVE #OUT-PREFIX TO #PREFIX
            pnd_In_Data_Pnd_Middle_Name.setValue(pnd_Out_Middle_Name);                                                                                                    //Natural: MOVE #OUT-MIDDLE-NAME TO #MIDDLE-NAME
            pnd_In_Data_Pnd_Suffix.setValue(pnd_Out_Suffix);                                                                                                              //Natural: MOVE #OUT-SUFFIX TO #SUFFIX
            pnd_In_Data_Pnd_Length.setValue(38);                                                                                                                          //Natural: MOVE 38 TO #LENGTH
            pnd_In_Data_Pnd_Format.setValue("1");                                                                                                                         //Natural: MOVE '1' TO #FORMAT
            //* *  CALLNAT 'PDQN360' #IN-DATA #OUT-DATA /* 082019
            //*  082019
            DbsUtil.callnat(Adsn360.class , getCurrentProcessState(), pnd_In_Data, pnd_Out_Data);                                                                         //Natural: CALLNAT 'ADSN360' #IN-DATA #OUT-DATA
            if (condition(Global.isEscape())) return;
            pnd_Name.setValue(pnd_Out_Data_Pnd_Name_Out);                                                                                                                 //Natural: ASSIGN #NAME := #NAME-OUT
        }                                                                                                                                                                 //Natural: END-IF
        pdaAdsa600.getAdsa600_Pnd_This_Inst().nadd(1);                                                                                                                    //Natural: ADD 1 TO ADSA600.#THIS-INST
        pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_This_Pymnt().setValue(pdaAdsa600.getAdsa600_Pnd_This_Inst());                                                                    //Natural: ASSIGN GTN-PDA-F.##THIS-PYMNT := ADSA600.#THIS-INST
        if (condition(pnd_F_Or_N.equals("N") || pnd_F_Or_N.equals("F")))                                                                                                  //Natural: IF #F-OR-N = 'N' OR = 'F'
        {
            pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Nbr_Of_Pymnts().compute(new ComputeParameters(false, pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Nbr_Of_Pymnts()), pdaAdsa600.getAdsa600_Pnd_Inst_Nbr().subtract(1)); //Natural: ASSIGN GTN-PDA-F.##NBR-OF-PYMNTS := #INST-NBR - 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Nbr_Of_Pymnts().setValue(pdaAdsa600.getAdsa600_Pnd_Inst_Nbr());                                                              //Natural: ASSIGN GTN-PDA-F.##NBR-OF-PYMNTS := #INST-NBR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa600.getAdsa600_Adi_Ia_Rcrd_Cde().equals("PP") && pdaAdsa600.getAdsa600_Pnd_This_Inst().equals(1)))                                           //Natural: IF ADSA600.ADI-IA-RCRD-CDE = 'PP' AND ADSA600.#THIS-INST = 1
        {
            if (condition(! (pnd_First.getBoolean())))                                                                                                                    //Natural: IF NOT #FIRST
            {
                GET02:                                                                                                                                                    //Natural: GET ADS-PRTCPNT #PRTCPNT-ISN
                vw_ads_Prtcpnt.readByID(pnd_Prtcpnt_Isn.getLong(), "GET02");
                                                                                                                                                                          //Natural: PERFORM MOVE-PDA-FIELDS
                sub_Move_Pda_Fields();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa140f.getGtn_Pda_F_Cntrct_Roll_Dest_Cde().setValue(ads_Prtcpnt_Adp_Alt_Dest_Rlvr_Dest);                                                                 //Natural: ASSIGN GTN-PDA-F.CNTRCT-ROLL-DEST-CDE := ADS-PRTCPNT.ADP-ALT-DEST-RLVR-DEST
            pdaFcpa140f.getGtn_Pda_F_Cntrct_Hold_Cde().setValue(ads_Prtcpnt_Adp_Alt_Dest_Hold_Cde);                                                                       //Natural: ASSIGN GTN-PDA-F.CNTRCT-HOLD-CDE := ADS-PRTCPNT.ADP-ALT-DEST-HOLD-CDE
            if (condition(ads_Prtcpnt_Adp_Alt_Dest_Addr_Txt.getValue("*").notEquals(" ")))                                                                                //Natural: IF ADS-PRTCPNT.ADP-ALT-DEST-ADDR-TXT ( * ) NE ' '
            {
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Nme().getValue(1).setValue(ads_Prtcpnt_Adp_Alt_Dest_Nme);                                                                  //Natural: ASSIGN GTN-PDA-F.PYMNT-NME ( 1 ) := ADS-PRTCPNT.ADP-ALT-DEST-NME
                if (condition(ads_Prtcpnt_Adp_Alt_Payee_Ind.equals("Y")))                                                                                                 //Natural: IF ADS-PRTCPNT.ADP-ALT-PAYEE-IND = 'Y'
                {
                    pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Line_Txt().getValue(1,1,":",5).setValue(ads_Prtcpnt_Adp_Alt_Dest_Addr_Txt.getValue(1,":",5));                     //Natural: ASSIGN GTN-PDA-F.PYMNT-ADDR-LINE-TXT ( 1,1:5 ) := ADS-PRTCPNT.ADP-ALT-DEST-ADDR-TXT ( 1:5 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Line_Txt().getValue(1,1,":",5).setValue(ads_Prtcpnt_Adp_Alt_Dest_Addr_Txt.getValue(1,":",5));                     //Natural: ASSIGN GTN-PDA-F.PYMNT-ADDR-LINE-TXT ( 1,1:5 ) := ADS-PRTCPNT.ADP-ALT-DEST-ADDR-TXT ( 1:5 )
                }                                                                                                                                                         //Natural: END-IF
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Zip_Cde().getValue(1).setValue(ads_Prtcpnt_Adp_Alt_Dest_Addr_Zip);                                                    //Natural: ASSIGN GTN-PDA-F.PYMNT-ADDR-ZIP-CDE ( 1 ) := ADS-PRTCPNT.ADP-ALT-DEST-ADDR-ZIP
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Foreign_Cde().getValue(1).setValue(ads_Prtcpnt_Adp_Alt_Dest_Addr_Typ_Cde);                                                 //Natural: ASSIGN GTN-PDA-F.PYMNT-FOREIGN-CDE ( 1 ) := ADS-PRTCPNT.ADP-ALT-DEST-ADDR-TYP-CDE
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Eft_Acct_Nbr().setValue(ads_Prtcpnt_Adp_Alt_Dest_Acct_Nbr);                                                                //Natural: ASSIGN GTN-PDA-F.PYMNT-EFT-ACCT-NBR := ADS-PRTCPNT.ADP-ALT-DEST-ACCT-NBR
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Roll_Dest_Cde().setValue(ads_Prtcpnt_Adp_Alt_Dest_Rlvr_Dest);                                                             //Natural: ASSIGN GTN-PDA-F.CNTRCT-ROLL-DEST-CDE := ADS-PRTCPNT.ADP-ALT-DEST-RLVR-DEST
                if (condition(DbsUtil.is(ads_Prtcpnt_Adp_Alt_Dest_Trnst_Cde.getText(),"N9")))                                                                             //Natural: IF ADS-PRTCPNT.ADP-ALT-DEST-TRNST-CDE IS ( N9 )
                {
                    pdaFcpa140f.getGtn_Pda_F_Pymnt_Eft_Transit_Id().compute(new ComputeParameters(false, pdaFcpa140f.getGtn_Pda_F_Pymnt_Eft_Transit_Id()),                //Natural: ASSIGN GTN-PDA-F.PYMNT-EFT-TRANSIT-ID := VAL ( ADS-PRTCPNT.ADP-ALT-DEST-TRNST-CDE )
                        ads_Prtcpnt_Adp_Alt_Dest_Trnst_Cde.val());
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa140f.getGtn_Pda_F_Pymnt_Eft_Transit_Id().reset();                                                                                              //Natural: RESET GTN-PDA-F.PYMNT-EFT-TRANSIT-ID
                }                                                                                                                                                         //Natural: END-IF
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Chk_Sav_Ind().setValue(ads_Prtcpnt_Adp_Alt_Dest_Acct_Typ);                                                                 //Natural: ASSIGN GTN-PDA-F.PYMNT-CHK-SAV-IND := ADS-PRTCPNT.ADP-ALT-DEST-ACCT-TYP
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Last_Chg_Dte().getValue(1).reset();                                                                                   //Natural: RESET GTN-PDA-F.PYMNT-ADDR-LAST-CHG-DTE ( 1 ) GTN-PDA-F.PYMNT-ADDR-LAST-CHG-TME ( 1 ) GTN-PDA-F.PYMNT-ADDR-CHG-IND ( 1 )
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Last_Chg_Tme().getValue(1).reset();
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Chg_Ind().getValue(1).reset();
                if (condition(pdaFcpa140f.getGtn_Pda_F_Pymnt_Nme().getValue(1).equals(" ")))                                                                              //Natural: IF GTN-PDA-F.PYMNT-NME ( 1 ) = ' '
                {
                    pdaFcpa140f.getGtn_Pda_F_Pymnt_Nme().getValue(1).setValue(pnd_Name);                                                                                  //Natural: ASSIGN GTN-PDA-F.PYMNT-NME ( 1 ) := #NAME
                }                                                                                                                                                         //Natural: END-IF
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Nme().getValue(2).setValue(pnd_Name);                                                                                      //Natural: ASSIGN GTN-PDA-F.PYMNT-NME ( 2 ) := #NAME
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Line_Txt().getValue(2,1,":",5).setValue(ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Txt.getValue(1,":",5));                  //Natural: ASSIGN GTN-PDA-F.PYMNT-ADDR-LINE-TXT ( 2,1:5 ) := ADS-PRTCPNT.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( 1:5 )
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Zip_Cde().getValue(2).setValue(ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Zip);                                             //Natural: ASSIGN GTN-PDA-F.PYMNT-ADDR-ZIP-CDE ( 2 ) := ADS-PRTCPNT.ADP-CRRSPNDNCE-PERM-ADDR-ZIP
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Foreign_Cde().getValue(2).setValue(ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Typ_Cd);                                           //Natural: ASSIGN GTN-PDA-F.PYMNT-FOREIGN-CDE ( 2 ) := ADS-PRTCPNT.ADP-CRRSPNDNCE-PERM-ADDR-TYP-CD
                if (condition(ads_Prtcpnt_Adp_Crrspndnce_Addr_Lst_Chg_Dte.greater(getZero())))                                                                            //Natural: IF ADS-PRTCPNT.ADP-CRRSPNDNCE-ADDR-LST-CHG-DTE GT 0
                {
                    pnd_Dte8.setValueEdited(ads_Prtcpnt_Adp_Crrspndnce_Addr_Lst_Chg_Dte,new ReportEditMask("YYYYMMDD"));                                                  //Natural: MOVE EDITED ADS-PRTCPNT.ADP-CRRSPNDNCE-ADDR-LST-CHG-DTE ( EM = YYYYMMDD ) TO #DTE8
                }                                                                                                                                                         //Natural: END-IF
                if (condition(DbsUtil.is(pnd_Dte8.getText(),"N8")))                                                                                                       //Natural: IF #DTE8 IS ( N8 )
                {
                    pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Last_Chg_Dte().getValue(2).compute(new ComputeParameters(false, pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Last_Chg_Dte().getValue(2)),  //Natural: ASSIGN GTN-PDA-F.PYMNT-ADDR-LAST-CHG-DTE ( 2 ) := VAL ( #DTE8 )
                        pnd_Dte8.val());
                }                                                                                                                                                         //Natural: END-IF
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Last_Chg_Tme().getValue(2).setValue(0);                                                                               //Natural: ASSIGN GTN-PDA-F.PYMNT-ADDR-LAST-CHG-TME ( 2 ) := 0
                //* *  GTN-PDA-F.PYMNT-ADDR-CHG-IND(2) :=
                //* *    ADS-PRTCPNT.ADP-CRRSPNDNCE-CHG-IND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Nme().getValue("*").reset();                                                                                               //Natural: RESET GTN-PDA-F.PYMNT-NME ( * ) GTN-PDA-F.PYMNT-ADDR-LINE-TXT ( *,* ) GTN-PDA-F.PYMNT-ADDR-ZIP-CDE ( * ) GTN-PDA-F.PYMNT-POSTL-DATA ( * ) GTN-PDA-F.PYMNT-EFT-TRANSIT-ID GTN-PDA-F.PYMNT-EFT-ACCT-NBR #DTE8
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Line_Txt().getValue("*","*").reset();
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Zip_Cde().getValue("*").reset();
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Postl_Data().getValue("*").reset();
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Eft_Transit_Id().reset();
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Eft_Acct_Nbr().reset();
                pnd_Dte8.reset();
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Nme().getValue(1,":",2).setValue(pnd_Name);                                                                                //Natural: ASSIGN GTN-PDA-F.PYMNT-NME ( 1:2 ) := #NAME
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Line_Txt().getValue(1,":",2,1,":",5).setValue(ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Txt.getValue(1,                    //Natural: ASSIGN GTN-PDA-F.PYMNT-ADDR-LINE-TXT ( 1:2,1:5 ) := ADS-PRTCPNT.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( 1:5 )
                    ":",5));
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Zip_Cde().getValue(1,":",2).setValue(ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Zip);                                       //Natural: ASSIGN GTN-PDA-F.PYMNT-ADDR-ZIP-CDE ( 1:2 ) := ADS-PRTCPNT.ADP-CRRSPNDNCE-PERM-ADDR-ZIP
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Foreign_Cde().getValue(1,":",2).setValue(ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Typ_Cd);                                     //Natural: ASSIGN GTN-PDA-F.PYMNT-FOREIGN-CDE ( 1:2 ) := ADS-PRTCPNT.ADP-CRRSPNDNCE-PERM-ADDR-TYP-CD
                if (condition(ads_Prtcpnt_Adp_Crrspndnce_Addr_Lst_Chg_Dte.greater(getZero())))                                                                            //Natural: IF ADS-PRTCPNT.ADP-CRRSPNDNCE-ADDR-LST-CHG-DTE GT 0
                {
                    pnd_Dte8.setValueEdited(ads_Prtcpnt_Adp_Crrspndnce_Addr_Lst_Chg_Dte,new ReportEditMask("YYYYMMDD"));                                                  //Natural: MOVE EDITED ADS-PRTCPNT.ADP-CRRSPNDNCE-ADDR-LST-CHG-DTE ( EM = YYYYMMDD ) TO #DTE8
                }                                                                                                                                                         //Natural: END-IF
                if (condition(DbsUtil.is(pnd_Dte8.getText(),"N8")))                                                                                                       //Natural: IF #DTE8 IS ( N8 )
                {
                    pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Last_Chg_Dte().getValue(1).compute(new ComputeParameters(false, pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Last_Chg_Dte().getValue(1)),  //Natural: ASSIGN GTN-PDA-F.PYMNT-ADDR-LAST-CHG-DTE ( 1 ) := VAL ( #DTE8 )
                        pnd_Dte8.val());
                }                                                                                                                                                         //Natural: END-IF
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Last_Chg_Tme().getValue(1).setValue(0);                                                                               //Natural: ASSIGN GTN-PDA-F.PYMNT-ADDR-LAST-CHG-TME ( 1 ) := 0
                //* *  GTN-PDA-F.PYMNT-ADDR-CHG-IND(1) :=
                //* *    ADS-PRTCPNT.ADP-CRRSPNDNCE-CHG-IND
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_I.reset();                                                                                                                                                    //Natural: RESET #I GTN-PDA-F.INV-ACCT-CDE ( * ) GTN-PDA-F.INV-ACCT-CNTRCT-AMT ( * ) GTN-PDA-F.INV-ACCT-DVDND-AMT ( * ) GTN-PDA-F.INV-ACCT-SETTL-AMT ( * ) GTN-PDA-F.INV-ACCT-UNIT-QTY ( * ) GTN-PDA-F.INV-ACCT-UNIT-VALUE ( * ) GTN-PDA-F.CNTRCT-LOB-CDE GTN-PDA-F.INV-ACCT-IVC-AMT ( * ) GTN-PDA-F.PYMNT-ALT-ADDR-IND GTN-PDA-F.INV-ACCT-VALUAT-PERIOD ( * )
        pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cde().getValue("*").reset();
        pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cntrct_Amt().getValue("*").reset();
        pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Dvdnd_Amt().getValue("*").reset();
        pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Settl_Amt().getValue("*").reset();
        pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Unit_Qty().getValue("*").reset();
        pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Unit_Value().getValue("*").reset();
        pdaFcpa140f.getGtn_Pda_F_Cntrct_Lob_Cde().reset();
        pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Ivc_Amt().getValue("*").reset();
        pdaFcpa140f.getGtn_Pda_F_Pymnt_Alt_Addr_Ind().reset();
        pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Valuat_Period().getValue("*").reset();
        //*  PERFORM CALCULATE-AGE-ETC
        if (condition(pnd_Cmpny.equals("T")))                                                                                                                             //Natural: IF #CMPNY = 'T'
        {
            pdaFcpa140f.getGtn_Pda_F_Cntrct_Da_Nbr().getValue("*").setValue(pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue(1,":",6));                                     //Natural: ASSIGN GTN-PDA-F.CNTRCT-DA-NBR ( * ) := ADSA600.ADI-TIAA-NBRS ( 1:6 )
            pdaFcpa140f.getGtn_Pda_F_Cntrct_Ppcn_Nbr().setValue(pdaAdsa600.getAdsa600_Adp_Ia_Tiaa_Nbr());                                                                 //Natural: ASSIGN GTN-PDA-F.CNTRCT-PPCN-NBR := ADSA600.ADP-IA-TIAA-NBR
            pnd_Ivc_Gd_Ind.setValue(pdaAdsa600.getAdsa600_Adi_Tiaa_Ivc_Gd_Ind());                                                                                         //Natural: ASSIGN #IVC-GD-IND := ADSA600.ADI-TIAA-IVC-GD-IND
            if (condition((DbsUtil.maskMatches(pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue("*"),"'K9'") || (pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue(1).getSubstring(1,4).compareTo("K830")  //Natural: IF ADSA600.ADI-TIAA-NBRS ( * ) = MASK ( 'K9' ) OR SUBSTR ( ADSA600.ADI-TIAA-NBRS ( 1 ) ,1,4 ) = 'K830' THRU 'K849'
                >= 0 && pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue(1).getSubstring(1,4).compareTo("K849") <= 0))))
            {
                //* *    OR #ORGN-CDE = 19 THRU 22 OR #ORGN-CDE = 41 OR = 43 OR = 44
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Lob_Cde().setValue("IRA");                                                                                                //Natural: ASSIGN GTN-PDA-F.CNTRCT-LOB-CDE := 'IRA'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ivc_Gd_Ind.equals("Y")))                                                                                                                    //Natural: IF #IVC-GD-IND = 'Y'
            {
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Settl_Ivc_Ind().setValue("2");                                                                                             //Natural: ASSIGN GTN-PDA-F.PYMNT-SETTL-IVC-IND := '2'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Settl_Ivc_Ind().setValue("0");                                                                                             //Natural: ASSIGN GTN-PDA-F.PYMNT-SETTL-IVC-IND := '0'
            }                                                                                                                                                             //Natural: END-IF
            pdaAdsa600.getAdsa600_Pnd_Tiaa_Ivc_Used_Amt().nadd(pdaAdsa600.getAdsa600_Adi_Tiaa_Prdc_Ivc_Amt());                                                            //Natural: ASSIGN #TIAA-IVC-USED-AMT := #TIAA-IVC-USED-AMT + ADSA600.ADI-TIAA-PRDC-IVC-AMT
            //*  032912
            //*  032912
            if (condition(pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue("*").greater(getZero()) || pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue("*").greater(getZero()))) //Natural: IF ADSA600.#ADI-DTL-TIAA-STD-GRNTD-AMT ( * ) GT 0 OR ADSA600.#ADI-DTL-TIAA-STD-DVDND-AMT ( * ) GT 0
            {
                //*  032912
                //*  032912
                pnd_I.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #I
                pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cde().getValue(pnd_I).setValue("T");                                                                                    //Natural: ASSIGN GTN-PDA-F.INV-ACCT-CDE ( #I ) := 'T'
                pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cntrct_Amt().getValue(pnd_I).compute(new ComputeParameters(false, pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cntrct_Amt().getValue(pnd_I)),  //Natural: ASSIGN GTN-PDA-F.INV-ACCT-CNTRCT-AMT ( #I ) := 0 + ADSA600.#ADI-DTL-TIAA-STD-GRNTD-AMT ( * )
                    DbsField.add(getZero(),pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue("*")));
                pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Dvdnd_Amt().getValue(pnd_I).compute(new ComputeParameters(false, pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Dvdnd_Amt().getValue(pnd_I)),  //Natural: ASSIGN GTN-PDA-F.INV-ACCT-DVDND-AMT ( #I ) := 0 + ADSA600.#ADI-DTL-TIAA-STD-DVDND-AMT ( * )
                    DbsField.add(getZero(),pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue("*")));
                pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Settl_Amt().getValue(pnd_I).compute(new ComputeParameters(false, pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Settl_Amt().getValue(pnd_I)),  //Natural: ASSIGN GTN-PDA-F.INV-ACCT-SETTL-AMT ( #I ) := GTN-PDA-F.INV-ACCT-CNTRCT-AMT ( #I ) + GTN-PDA-F.INV-ACCT-DVDND-AMT ( #I )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cntrct_Amt().getValue(pnd_I).add(pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Dvdnd_Amt().getValue(pnd_I)));
            }                                                                                                                                                             //Natural: END-IF
            //*  032912
            //*  032912
            if (condition(pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue("*").greater(getZero()) || pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue("*").greater(getZero()))) //Natural: IF ADSA600.#ADI-DTL-TIAA-GRD-GRNTD-AMT ( * ) GT 0 OR ADSA600.#ADI-DTL-TIAA-GRD-DVDND-AMT ( * ) GT 0
            {
                //*  032912
                //*  032912
                pnd_I.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #I
                pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cde().getValue(pnd_I).setValue("TG");                                                                                   //Natural: ASSIGN GTN-PDA-F.INV-ACCT-CDE ( #I ) := 'TG'
                pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cntrct_Amt().getValue(pnd_I).compute(new ComputeParameters(false, pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cntrct_Amt().getValue(pnd_I)),  //Natural: ASSIGN GTN-PDA-F.INV-ACCT-CNTRCT-AMT ( #I ) := 0 + ADSA600.#ADI-DTL-TIAA-GRD-GRNTD-AMT ( * )
                    DbsField.add(getZero(),pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue("*")));
                pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Dvdnd_Amt().getValue(pnd_I).compute(new ComputeParameters(false, pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Dvdnd_Amt().getValue(pnd_I)),  //Natural: ASSIGN GTN-PDA-F.INV-ACCT-DVDND-AMT ( #I ) := 0 + ADSA600.#ADI-DTL-TIAA-GRD-DVDND-AMT ( * )
                    DbsField.add(getZero(),pdaAdsa600.getAdsa600_Pnd_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue("*")));
                pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Settl_Amt().getValue(pnd_I).compute(new ComputeParameters(false, pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Settl_Amt().getValue(pnd_I)),  //Natural: ASSIGN GTN-PDA-F.INV-ACCT-SETTL-AMT ( #I ) := GTN-PDA-F.INV-ACCT-CNTRCT-AMT ( #I ) + GTN-PDA-F.INV-ACCT-DVDND-AMT ( #I )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cntrct_Amt().getValue(pnd_I).add(pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Dvdnd_Amt().getValue(pnd_I)));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaAdsa600.getAdsa600_Adp_Alt_Dest_Addr_Ind().equals("Y") && pdaAdsa600.getAdsa600_Adp_Alt_Payee_Ind().equals("N")))                            //Natural: IF ADSA600.ADP-ALT-DEST-ADDR-IND = 'Y' AND ADSA600.ADP-ALT-PAYEE-IND = 'N'
            {
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Alt_Addr_Ind().setValue(true);                                                                                             //Natural: ASSIGN GTN-PDA-F.PYMNT-ALT-ADDR-IND := TRUE
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Ivc_Amt().getValue(1).setValue(pdaAdsa600.getAdsa600_Adi_Tiaa_Prdc_Ivc_Amt());                                              //Natural: ASSIGN GTN-PDA-F.INV-ACCT-IVC-AMT ( 1 ) := ADSA600.ADI-TIAA-PRDC-IVC-AMT
            //* *  IF #ORGN-CDE = 20 THRU 22
            //* *    IF #SEASONED
            //* *      GTN-PDA-F.INV-ACCT-IVC-AMT(1) := 0 +
            //* *        GTN-PDA-F.INV-ACCT-SETTL-AMT(*)
            //* *    ELSE
            //* *      RESET GTN-PDA-F.INV-ACCT-IVC-AMT(1)
            //* *    END-IF
            //* *  END-IF
            //*                                                     /* CM - BEGIN PCPOP
            //*  457B PRIVATE
            //*  TIAA05/06
            //*  TIAA07 - 10
            //*  CANADIAN CONVERTED
            //*  CLERGY AND ALL OTHER
            short decideConditionsMet880 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ADSA600.ADP-IRC-CDE = '17'
            if (condition(pdaAdsa600.getAdsa600_Adp_Irc_Cde().equals("17")))
            {
                decideConditionsMet880++;
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Pymnt_Type_Ind().setValue("M");                                                                                           //Natural: ASSIGN GTN-PDA-F.CNTRCT-PYMNT-TYPE-IND := 'M'
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Sttlmnt_Type_Ind().setValue("S");                                                                                         //Natural: ASSIGN GTN-PDA-F.CNTRCT-STTLMNT-TYPE-IND := 'S'
            }                                                                                                                                                             //Natural: WHEN ADSA600.ADP-PLAN-NBR = 'TIAA05' OR = 'TIAA06'
            else if (condition(pdaAdsa600.getAdsa600_Adp_Plan_Nbr().equals("TIAA05") || pdaAdsa600.getAdsa600_Adp_Plan_Nbr().equals("TIAA06")))
            {
                decideConditionsMet880++;
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Pymnt_Type_Ind().setValue("M");                                                                                           //Natural: ASSIGN GTN-PDA-F.CNTRCT-PYMNT-TYPE-IND := 'M'
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Sttlmnt_Type_Ind().setValue("T");                                                                                         //Natural: ASSIGN GTN-PDA-F.CNTRCT-STTLMNT-TYPE-IND := 'T'
            }                                                                                                                                                             //Natural: WHEN ADSA600.ADP-PLAN-NBR = 'TIAA07' OR = 'TIAA08' OR = 'TIAA09' OR = 'TIAA10'
            else if (condition(pdaAdsa600.getAdsa600_Adp_Plan_Nbr().equals("TIAA07") || pdaAdsa600.getAdsa600_Adp_Plan_Nbr().equals("TIAA08") || pdaAdsa600.getAdsa600_Adp_Plan_Nbr().equals("TIAA09") 
                || pdaAdsa600.getAdsa600_Adp_Plan_Nbr().equals("TIAA10")))
            {
                decideConditionsMet880++;
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Pymnt_Type_Ind().setValue("X");                                                                                           //Natural: ASSIGN GTN-PDA-F.CNTRCT-PYMNT-TYPE-IND := 'X'
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Sttlmnt_Type_Ind().setValue("Z");                                                                                         //Natural: ASSIGN GTN-PDA-F.CNTRCT-STTLMNT-TYPE-IND := 'Z'
            }                                                                                                                                                             //Natural: WHEN ADSA600.ADP-PLAN-NBR = '152004'
            else if (condition(pdaAdsa600.getAdsa600_Adp_Plan_Nbr().equals("152004")))
            {
                decideConditionsMet880++;
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Pymnt_Type_Ind().setValue("C");                                                                                           //Natural: ASSIGN GTN-PDA-F.CNTRCT-PYMNT-TYPE-IND := 'C'
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Sttlmnt_Type_Ind().setValue("C");                                                                                         //Natural: ASSIGN GTN-PDA-F.CNTRCT-STTLMNT-TYPE-IND := 'C'
                //*  EM - 022515 START
                //*    WHEN ADSA600.ADP-PLAN-NBR = 'IRA201'
            }                                                                                                                                                             //Natural: WHEN ADSA600.ADP-PLAN-NBR = 'IRA201' OR = 'IRA002' OR = 'IRA022'
            else if (condition(pdaAdsa600.getAdsa600_Adp_Plan_Nbr().equals("IRA201") || pdaAdsa600.getAdsa600_Adp_Plan_Nbr().equals("IRA002") || pdaAdsa600.getAdsa600_Adp_Plan_Nbr().equals("IRA022")))
            {
                decideConditionsMet880++;
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Pymnt_Type_Ind().setValue("C");                                                                                           //Natural: ASSIGN GTN-PDA-F.CNTRCT-PYMNT-TYPE-IND := 'C'
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Sttlmnt_Type_Ind().setValue("T");                                                                                         //Natural: ASSIGN GTN-PDA-F.CNTRCT-STTLMNT-TYPE-IND := 'T'
                //*  EM - 022515 END
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                //*      IF ADSA600.ADC-ACCT-CDE (*) = 'Y'         /* EM - 012209 START
                //*      IF ADSA600.ADC-ACCT-CDE (*) = 'Y' OR       /* EM - 111810
                //*          ADC-T395-FUND-ID (*) = #SA-FND-ID
                //*  EM - 100413 START
                if (condition(pdaAdsa600.getAdsa600_Adc_Invstmnt_Grpng_Id().getValue("*").equals(pnd_Stable_Return) || pdaAdsa600.getAdsa600_Adc_Invstmnt_Grpng_Id().getValue("*").equals(pnd_Stable_Value))) //Natural: IF ADSA600.ADC-INVSTMNT-GRPNG-ID ( * ) = #STABLE-RETURN OR = #STABLE-VALUE
                {
                    //*  EM - 100413 - END
                    pdaFcpa140f.getGtn_Pda_F_Cntrct_Pymnt_Type_Ind().setValue("A");                                                                                       //Natural: ASSIGN GTN-PDA-F.CNTRCT-PYMNT-TYPE-IND := 'A'
                    pdaFcpa140f.getGtn_Pda_F_Cntrct_Sttlmnt_Type_Ind().setValue("S");                                                                                     //Natural: ASSIGN GTN-PDA-F.CNTRCT-STTLMNT-TYPE-IND := 'S'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa140f.getGtn_Pda_F_Cntrct_Pymnt_Type_Ind().setValue("A");                                                                                       //Natural: ASSIGN GTN-PDA-F.CNTRCT-PYMNT-TYPE-IND := 'A'
                    pdaFcpa140f.getGtn_Pda_F_Cntrct_Sttlmnt_Type_Ind().setValue("P");                                                                                     //Natural: ASSIGN GTN-PDA-F.CNTRCT-STTLMNT-TYPE-IND := 'P'
                    //*  EM - 012209 END
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  CM - 3/16/10 EXCEPTION WITHHOLDING FOR AC < 10 YRS TAX OVERRIDE
            if (condition(pdaAdsa600.getAdsa600_Adp_Mndtry_Tax_Ovrd().equals("Y")))                                                                                       //Natural: IF ADSA600.ADP-MNDTRY-TAX-OVRD EQ 'Y'
            {
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Tax_Exempt_Ind().setValue("Y");                                                                                            //Natural: ASSIGN GTN-PDA-F.PYMNT-TAX-EXEMPT-IND := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa140f.getGtn_Pda_F_Pymnt_Settlmnt_Dte().setValue(pdaAdsa600.getAdsa600_Adi_Instllmnt_Dte());                                                            //Natural: ASSIGN GTN-PDA-F.PYMNT-SETTLMNT-DTE := ADSA600.ADI-INSTLLMNT-DTE
            DbsUtil.examine(new ExamineSource(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue("*")), new ExamineSearch("R"), new ExamineGivingIndex(pnd_J));        //Natural: EXAMINE ADSA600.ADI-DTL-CREF-ACCT-CD ( * ) FOR 'R' GIVING INDEX #J
            if (condition(pnd_J.greater(getZero())))                                                                                                                      //Natural: IF #J GT 0
            {
                if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_J).greater(getZero())))                                                    //Natural: IF ADSA600.ADI-DTL-CREF-ANNL-NBR-UNITS ( #J ) GT 0
                {
                    pnd_I.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #I
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cde().getValue(pnd_I).setValue("R");                                                                                //Natural: ASSIGN GTN-PDA-F.INV-ACCT-CDE ( #I ) := 'R'
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Valuat_Period().getValue(pnd_I).setValue("A");                                                                      //Natural: ASSIGN GTN-PDA-F.INV-ACCT-VALUAT-PERIOD ( #I ) := 'A'
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Unit_Qty().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_J));           //Natural: ASSIGN GTN-PDA-F.INV-ACCT-UNIT-QTY ( #I ) := ADSA600.ADI-DTL-CREF-ANNL-NBR-UNITS ( #J )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Unit_Value().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Ia_Annl_Unit_Val().getValue(pnd_J));       //Natural: ASSIGN GTN-PDA-F.INV-ACCT-UNIT-VALUE ( #I ) := ADSA600.ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #J )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cntrct_Amt().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_J));            //Natural: ASSIGN GTN-PDA-F.INV-ACCT-CNTRCT-AMT ( #I ) := ADSA600.ADI-DTL-CREF-DA-ANNL-AMT ( #J )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Settl_Amt().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_J));             //Natural: ASSIGN GTN-PDA-F.INV-ACCT-SETTL-AMT ( #I ) := ADSA600.ADI-DTL-CREF-DA-ANNL-AMT ( #J )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_J).greater(getZero())))                                                  //Natural: IF ADSA600.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #J ) GT 0
                {
                    pnd_I.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #I
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cde().getValue(pnd_I).setValue("R");                                                                                //Natural: ASSIGN GTN-PDA-F.INV-ACCT-CDE ( #I ) := 'R'
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Valuat_Period().getValue(pnd_I).setValue("M");                                                                      //Natural: ASSIGN GTN-PDA-F.INV-ACCT-VALUAT-PERIOD ( #I ) := 'M'
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Unit_Qty().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_J));         //Natural: ASSIGN GTN-PDA-F.INV-ACCT-UNIT-QTY ( #I ) := ADSA600.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #J )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Unit_Value().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val().getValue(pnd_J));     //Natural: ASSIGN GTN-PDA-F.INV-ACCT-UNIT-VALUE ( #I ) := ADSA600.ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL ( #J )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cntrct_Amt().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_J));          //Natural: ASSIGN GTN-PDA-F.INV-ACCT-CNTRCT-AMT ( #I ) := ADSA600.ADI-DTL-CREF-DA-MNTHLY-AMT ( #J )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Settl_Amt().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_J));           //Natural: ASSIGN GTN-PDA-F.INV-ACCT-SETTL-AMT ( #I ) := ADSA600.ADI-DTL-CREF-DA-MNTHLY-AMT ( #J )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* *  EM - 012209 START
            DbsUtil.examine(new ExamineSource(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue("*")), new ExamineSearch("D"), new ExamineGivingIndex(pnd_J));        //Natural: EXAMINE ADSA600.ADI-DTL-CREF-ACCT-CD ( * ) FOR 'D' GIVING INDEX #J
            if (condition(pnd_J.greater(getZero())))                                                                                                                      //Natural: IF #J GT 0
            {
                if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_J).greater(getZero())))                                                    //Natural: IF ADSA600.ADI-DTL-CREF-ANNL-NBR-UNITS ( #J ) GT 0
                {
                    pnd_I.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #I
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cde().getValue(pnd_I).setValue("D");                                                                                //Natural: ASSIGN GTN-PDA-F.INV-ACCT-CDE ( #I ) := 'D'
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Valuat_Period().getValue(pnd_I).setValue("A");                                                                      //Natural: ASSIGN GTN-PDA-F.INV-ACCT-VALUAT-PERIOD ( #I ) := 'A'
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Unit_Qty().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_J));           //Natural: ASSIGN GTN-PDA-F.INV-ACCT-UNIT-QTY ( #I ) := ADSA600.ADI-DTL-CREF-ANNL-NBR-UNITS ( #J )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Unit_Value().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Ia_Annl_Unit_Val().getValue(pnd_J));       //Natural: ASSIGN GTN-PDA-F.INV-ACCT-UNIT-VALUE ( #I ) := ADSA600.ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #J )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cntrct_Amt().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_J));            //Natural: ASSIGN GTN-PDA-F.INV-ACCT-CNTRCT-AMT ( #I ) := ADSA600.ADI-DTL-CREF-DA-ANNL-AMT ( #J )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Settl_Amt().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_J));             //Natural: ASSIGN GTN-PDA-F.INV-ACCT-SETTL-AMT ( #I ) := ADSA600.ADI-DTL-CREF-DA-ANNL-AMT ( #J )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_J).greater(getZero())))                                                  //Natural: IF ADSA600.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #J ) GT 0
                {
                    pnd_I.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #I
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cde().getValue(pnd_I).setValue("D");                                                                                //Natural: ASSIGN GTN-PDA-F.INV-ACCT-CDE ( #I ) := 'D'
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Valuat_Period().getValue(pnd_I).setValue("M");                                                                      //Natural: ASSIGN GTN-PDA-F.INV-ACCT-VALUAT-PERIOD ( #I ) := 'M'
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Unit_Qty().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_J));         //Natural: ASSIGN GTN-PDA-F.INV-ACCT-UNIT-QTY ( #I ) := ADSA600.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #J )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Unit_Value().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val().getValue(pnd_J));     //Natural: ASSIGN GTN-PDA-F.INV-ACCT-UNIT-VALUE ( #I ) := ADSA600.ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL ( #J )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cntrct_Amt().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_J));          //Natural: ASSIGN GTN-PDA-F.INV-ACCT-CNTRCT-AMT ( #I ) := ADSA600.ADI-DTL-CREF-DA-MNTHLY-AMT ( #J )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Settl_Amt().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_J));           //Natural: ASSIGN GTN-PDA-F.INV-ACCT-SETTL-AMT ( #I ) := ADSA600.ADI-DTL-CREF-DA-MNTHLY-AMT ( #J )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* * EM 012209 END
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa140f.getGtn_Pda_F_Cntrct_Da_Nbr().getValue("*").setValue(pdaAdsa600.getAdsa600_Adi_Cref_Nbrs().getValue(1,":",6));                                     //Natural: ASSIGN GTN-PDA-F.CNTRCT-DA-NBR ( * ) := ADSA600.ADI-CREF-NBRS ( 1:6 )
            pdaFcpa140f.getGtn_Pda_F_Cntrct_Ppcn_Nbr().setValue(pdaAdsa600.getAdsa600_Adp_Ia_Cref_Nbr());                                                                 //Natural: ASSIGN GTN-PDA-F.CNTRCT-PPCN-NBR := ADSA600.ADP-IA-CREF-NBR
            pnd_Ivc_Gd_Ind.setValue(pdaAdsa600.getAdsa600_Adi_Cref_Ivc_Gd_Ind());                                                                                         //Natural: ASSIGN #IVC-GD-IND := ADSA600.ADI-CREF-IVC-GD-IND
            if (condition((DbsUtil.maskMatches(pdaAdsa600.getAdsa600_Adi_Cref_Nbrs().getValue("*"),"'J9'") || (pdaAdsa600.getAdsa600_Adi_Cref_Nbrs().getValue(1).getSubstring(1,4).compareTo("J830")  //Natural: IF ADSA600.ADI-CREF-NBRS ( * ) = MASK ( 'J9' ) OR SUBSTR ( ADSA600.ADI-CREF-NBRS ( 1 ) ,1,4 ) = 'J830' THRU 'J849'
                >= 0 && pdaAdsa600.getAdsa600_Adi_Cref_Nbrs().getValue(1).getSubstring(1,4).compareTo("J849") <= 0))))
            {
                //* *    OR #ORGN-CDE = 19 THRU 22 OR #ORGN-CDE = 41 OR = 43 OR = 44
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Lob_Cde().setValue("IRA");                                                                                                //Natural: ASSIGN GTN-PDA-F.CNTRCT-LOB-CDE := 'IRA'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ivc_Gd_Ind.equals("Y")))                                                                                                                    //Natural: IF #IVC-GD-IND = 'Y'
            {
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Settl_Ivc_Ind().setValue("2");                                                                                             //Natural: ASSIGN GTN-PDA-F.PYMNT-SETTL-IVC-IND := '2'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Settl_Ivc_Ind().setValue("0");                                                                                             //Natural: ASSIGN GTN-PDA-F.PYMNT-SETTL-IVC-IND := '0'
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #J 1 TO 20
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(20)); pnd_J.nadd(1))
            {
                if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_J).equals(" ")))                                                                  //Natural: IF ADSA600.ADI-DTL-CREF-ACCT-CD ( #J ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                //*  EM - 012209
                if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_J).equals("R") || pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_J).equals("D"))) //Natural: IF ADSA600.ADI-DTL-CREF-ACCT-CD ( #J ) = 'R' OR = 'D'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_J).greater(getZero())))                                                    //Natural: IF ADSA600.ADI-DTL-CREF-ANNL-NBR-UNITS ( #J ) GT 0
                {
                    pnd_I.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #I
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cde().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_J));                       //Natural: ASSIGN GTN-PDA-F.INV-ACCT-CDE ( #I ) := ADSA600.ADI-DTL-CREF-ACCT-CD ( #J )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Valuat_Period().getValue(pnd_I).setValue("A");                                                                      //Natural: ASSIGN GTN-PDA-F.INV-ACCT-VALUAT-PERIOD ( #I ) := 'A'
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Unit_Qty().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(pnd_J));           //Natural: ASSIGN GTN-PDA-F.INV-ACCT-UNIT-QTY ( #I ) := ADSA600.ADI-DTL-CREF-ANNL-NBR-UNITS ( #J )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Unit_Value().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Ia_Annl_Unit_Val().getValue(pnd_J));       //Natural: ASSIGN GTN-PDA-F.INV-ACCT-UNIT-VALUE ( #I ) := ADSA600.ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( #J )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cntrct_Amt().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_J));            //Natural: ASSIGN GTN-PDA-F.INV-ACCT-CNTRCT-AMT ( #I ) := ADSA600.ADI-DTL-CREF-DA-ANNL-AMT ( #J )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Settl_Amt().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Annl_Amt().getValue(pnd_J));             //Natural: ASSIGN GTN-PDA-F.INV-ACCT-SETTL-AMT ( #I ) := ADSA600.ADI-DTL-CREF-DA-ANNL-AMT ( #J )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            FOR02:                                                                                                                                                        //Natural: FOR #J 1 TO 20
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(20)); pnd_J.nadd(1))
            {
                if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_J).equals(" ")))                                                                  //Natural: IF ADSA600.ADI-DTL-CREF-ACCT-CD ( #J ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                //*  EM - 012209
                if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_J).equals("R") || pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_J).equals("D"))) //Natural: IF ADSA600.ADI-DTL-CREF-ACCT-CD ( #J ) = 'R' OR = 'D'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_J).greater(getZero())))                                                  //Natural: IF ADSA600.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #J ) GT 0
                {
                    pnd_I.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #I
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cde().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Acct_Cd().getValue(pnd_J));                       //Natural: ASSIGN GTN-PDA-F.INV-ACCT-CDE ( #I ) := ADSA600.ADI-DTL-CREF-ACCT-CD ( #J )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Valuat_Period().getValue(pnd_I).setValue("M");                                                                      //Natural: ASSIGN GTN-PDA-F.INV-ACCT-VALUAT-PERIOD ( #I ) := 'M'
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Unit_Qty().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(pnd_J));         //Natural: ASSIGN GTN-PDA-F.INV-ACCT-UNIT-QTY ( #I ) := ADSA600.ADI-DTL-CREF-MNTHLY-NBR-UNITS ( #J )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Unit_Value().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val().getValue(pnd_J));     //Natural: ASSIGN GTN-PDA-F.INV-ACCT-UNIT-VALUE ( #I ) := ADSA600.ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL ( #J )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cntrct_Amt().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_J));          //Natural: ASSIGN GTN-PDA-F.INV-ACCT-CNTRCT-AMT ( #I ) := ADSA600.ADI-DTL-CREF-DA-MNTHLY-AMT ( #J )
                    pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Settl_Amt().getValue(pnd_I).setValue(pdaAdsa600.getAdsa600_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(pnd_J));           //Natural: ASSIGN GTN-PDA-F.INV-ACCT-SETTL-AMT ( #I ) := ADSA600.ADI-DTL-CREF-DA-MNTHLY-AMT ( #J )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pdaAdsa600.getAdsa600_Pnd_Cref_Ivc_Used_Amt().nadd(pdaAdsa600.getAdsa600_Adi_Cref_Prdc_Ivc_Amt());                                                            //Natural: ASSIGN ADSA600.#CREF-IVC-USED-AMT := ADSA600.#CREF-IVC-USED-AMT + ADSA600.ADI-CREF-PRDC-IVC-AMT
            if (condition(pdaAdsa600.getAdsa600_Adp_Alt_Dest_Addr_Ind().equals("Y") && pdaAdsa600.getAdsa600_Adp_Alt_Payee_Ind().equals("N")))                            //Natural: IF ADSA600.ADP-ALT-DEST-ADDR-IND = 'Y' AND ADSA600.ADP-ALT-PAYEE-IND = 'N'
            {
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Alt_Addr_Ind().setValue(true);                                                                                             //Natural: ASSIGN GTN-PDA-F.PYMNT-ALT-ADDR-IND := TRUE
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Ivc_Amt().getValue(1).setValue(pdaAdsa600.getAdsa600_Adi_Cref_Prdc_Ivc_Amt());                                              //Natural: ASSIGN GTN-PDA-F.INV-ACCT-IVC-AMT ( 1 ) := ADSA600.ADI-CREF-PRDC-IVC-AMT
            //*                                                         /* CM - PCPOP
            //*  457B PRIVATE
            //*  TIAA05/06
            //*  TIAA07 - 10
            //*  CANADIAN CONVERTED
            //*  CLERGY AND ALL OTHER
            short decideConditionsMet1045 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ADSA600.ADP-IRC-CDE = '17'
            if (condition(pdaAdsa600.getAdsa600_Adp_Irc_Cde().equals("17")))
            {
                decideConditionsMet1045++;
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Pymnt_Type_Ind().setValue("M");                                                                                           //Natural: ASSIGN GTN-PDA-F.CNTRCT-PYMNT-TYPE-IND := 'M'
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Sttlmnt_Type_Ind().setValue("S");                                                                                         //Natural: ASSIGN GTN-PDA-F.CNTRCT-STTLMNT-TYPE-IND := 'S'
            }                                                                                                                                                             //Natural: WHEN ADSA600.ADP-PLAN-NBR = 'TIAA05' OR = 'TIAA06'
            else if (condition(pdaAdsa600.getAdsa600_Adp_Plan_Nbr().equals("TIAA05") || pdaAdsa600.getAdsa600_Adp_Plan_Nbr().equals("TIAA06")))
            {
                decideConditionsMet1045++;
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Pymnt_Type_Ind().setValue("M");                                                                                           //Natural: ASSIGN GTN-PDA-F.CNTRCT-PYMNT-TYPE-IND := 'M'
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Sttlmnt_Type_Ind().setValue("T");                                                                                         //Natural: ASSIGN GTN-PDA-F.CNTRCT-STTLMNT-TYPE-IND := 'T'
            }                                                                                                                                                             //Natural: WHEN ADSA600.ADP-PLAN-NBR = 'TIAA07' OR = 'TIAA08' OR = 'TIAA09' OR = 'TIAA10'
            else if (condition(pdaAdsa600.getAdsa600_Adp_Plan_Nbr().equals("TIAA07") || pdaAdsa600.getAdsa600_Adp_Plan_Nbr().equals("TIAA08") || pdaAdsa600.getAdsa600_Adp_Plan_Nbr().equals("TIAA09") 
                || pdaAdsa600.getAdsa600_Adp_Plan_Nbr().equals("TIAA10")))
            {
                decideConditionsMet1045++;
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Pymnt_Type_Ind().setValue("X");                                                                                           //Natural: ASSIGN GTN-PDA-F.CNTRCT-PYMNT-TYPE-IND := 'X'
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Sttlmnt_Type_Ind().setValue("Z");                                                                                         //Natural: ASSIGN GTN-PDA-F.CNTRCT-STTLMNT-TYPE-IND := 'Z'
            }                                                                                                                                                             //Natural: WHEN ADSA600.ADP-PLAN-NBR = '152004'
            else if (condition(pdaAdsa600.getAdsa600_Adp_Plan_Nbr().equals("152004")))
            {
                decideConditionsMet1045++;
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Pymnt_Type_Ind().setValue("C");                                                                                           //Natural: ASSIGN GTN-PDA-F.CNTRCT-PYMNT-TYPE-IND := 'C'
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Sttlmnt_Type_Ind().setValue("C");                                                                                         //Natural: ASSIGN GTN-PDA-F.CNTRCT-STTLMNT-TYPE-IND := 'C'
                //*  EM - 022515 START
                //*    WHEN ADSA600.ADP-PLAN-NBR = 'IRA201'
            }                                                                                                                                                             //Natural: WHEN ADSA600.ADP-PLAN-NBR = 'IRA201' OR = 'IRA002' OR = 'IRA022'
            else if (condition(pdaAdsa600.getAdsa600_Adp_Plan_Nbr().equals("IRA201") || pdaAdsa600.getAdsa600_Adp_Plan_Nbr().equals("IRA002") || pdaAdsa600.getAdsa600_Adp_Plan_Nbr().equals("IRA022")))
            {
                decideConditionsMet1045++;
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Pymnt_Type_Ind().setValue("C");                                                                                           //Natural: ASSIGN GTN-PDA-F.CNTRCT-PYMNT-TYPE-IND := 'C'
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Sttlmnt_Type_Ind().setValue("T");                                                                                         //Natural: ASSIGN GTN-PDA-F.CNTRCT-STTLMNT-TYPE-IND := 'T'
                //*  EM - 022515 END
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Pymnt_Type_Ind().setValue("A");                                                                                           //Natural: ASSIGN GTN-PDA-F.CNTRCT-PYMNT-TYPE-IND := 'A'
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Sttlmnt_Type_Ind().setValue("P");                                                                                         //Natural: ASSIGN GTN-PDA-F.CNTRCT-STTLMNT-TYPE-IND := 'P'
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  CM - 3/16/10 EXCEPTION WITHHOLDING FOR AC < 10 YRS TAX OVERRIDE
            if (condition(pdaAdsa600.getAdsa600_Adp_Mndtry_Tax_Ovrd().equals("Y")))                                                                                       //Natural: IF ADSA600.ADP-MNDTRY-TAX-OVRD EQ 'Y'
            {
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Tax_Exempt_Ind().setValue("Y");                                                                                            //Natural: ASSIGN GTN-PDA-F.PYMNT-TAX-EXEMPT-IND := 'Y'
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa140f.getGtn_Pda_F_Pymnt_Settlmnt_Dte().setValue(pdaAdsa600.getAdsa600_Adi_Instllmnt_Dte());                                                            //Natural: ASSIGN GTN-PDA-F.PYMNT-SETTLMNT-DTE := ADSA600.ADI-INSTLLMNT-DTE
            //* *  IF #ORGN-CDE = 20 THRU 22
            //* *    IF #SEASONED
            //* *      GTN-PDA-F.INV-ACCT-IVC-AMT(1) := 0 +
            //* *        GTN-PDA-F.INV-ACCT-SETTL-AMT(*)
            //* *    ELSE
            //* *      RESET GTN-PDA-F.INV-ACCT-IVC-AMT(1)
            //* *    END-IF
            //* *  END-IF
            //* *  DECIDE ON FIRST VALUE #ORGN-CDE
            //* *    VALUE 20
            //* *      GTN-PDA-F.CNTRCT-STTLMNT-TYPE-IND := 'T'
            //* *    VALUE 21,22
            //* *      GTN-PDA-F.CNTRCT-STTLMNT-TYPE-IND := 'H'
            //* *    ANY
            //* *      GTN-PDA-F.CNTRCT-PYMNT-TYPE-IND := 'C'
            //* *    NONE VALUE IGNORE
            //* *  END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa140f.getGtn_Pda_F_Pymnt_Ftre_Ind().setValue(pdaAdsa600.getAdsa600_Adi_Pymnt_Cde());                                                                        //Natural: ASSIGN GTN-PDA-F.PYMNT-FTRE-IND := ADSA600.ADI-PYMNT-CDE
        pdaFcpa140f.getGtn_Pda_F_Cntrct_Payee_Cde().setValue("01");                                                                                                       //Natural: ASSIGN GTN-PDA-F.CNTRCT-PAYEE-CDE := '01'
        if (condition(pdaFcpa140f.getGtn_Pda_F_Pymnt_Eft_Transit_Id().notEquals(getZero())))                                                                              //Natural: IF GTN-PDA-F.PYMNT-EFT-TRANSIT-ID NE 0
        {
            pdaFcpa140f.getGtn_Pda_F_Pymnt_Pay_Type_Req_Ind().setValue(2);                                                                                                //Natural: ASSIGN GTN-PDA-F.PYMNT-PAY-TYPE-REQ-IND := 2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa140f.getGtn_Pda_F_Pymnt_Pay_Type_Req_Ind().setValue(1);                                                                                                //Natural: ASSIGN GTN-PDA-F.PYMNT-PAY-TYPE-REQ-IND := 1
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1111 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN GTN-PDA-F.CNTRCT-ROLL-DEST-CDE = 'IRAT' OR = 'IRAC' OR = '03BT' OR = '03BC' OR = 'QPLT' OR = 'QPLC' OR = 'RTHC' OR = 'RTHT' OR = 'DTRA'
        if (condition(pdaFcpa140f.getGtn_Pda_F_Cntrct_Roll_Dest_Cde().equals("IRAT") || pdaFcpa140f.getGtn_Pda_F_Cntrct_Roll_Dest_Cde().equals("IRAC") 
            || pdaFcpa140f.getGtn_Pda_F_Cntrct_Roll_Dest_Cde().equals("03BT") || pdaFcpa140f.getGtn_Pda_F_Cntrct_Roll_Dest_Cde().equals("03BC") || pdaFcpa140f.getGtn_Pda_F_Cntrct_Roll_Dest_Cde().equals("QPLT") 
            || pdaFcpa140f.getGtn_Pda_F_Cntrct_Roll_Dest_Cde().equals("QPLC") || pdaFcpa140f.getGtn_Pda_F_Cntrct_Roll_Dest_Cde().equals("RTHC") || pdaFcpa140f.getGtn_Pda_F_Cntrct_Roll_Dest_Cde().equals("RTHT") 
            || pdaFcpa140f.getGtn_Pda_F_Cntrct_Roll_Dest_Cde().equals("DTRA")))
        {
            decideConditionsMet1111++;
            pdaFcpa140f.getGtn_Pda_F_Cntrct_Hold_Cde().setValue("IRNZ");                                                                                                  //Natural: ASSIGN GTN-PDA-F.CNTRCT-HOLD-CDE := 'IRNZ'
            //*  ADDED 12/01/1998 TO MAKE SURE THAT A CHECK IS MADE FOR NON-RTB CASES
            pdaFcpa140f.getGtn_Pda_F_Pymnt_Pay_Type_Req_Ind().setValue(1);                                                                                                //Natural: ASSIGN GTN-PDA-F.PYMNT-PAY-TYPE-REQ-IND := 1
        }                                                                                                                                                                 //Natural: WHEN GTN-PDA-F.CNTRCT-OPTION-CDE = 21
        else if (condition(pdaFcpa140f.getGtn_Pda_F_Cntrct_Option_Cde().equals(21)))
        {
            decideConditionsMet1111++;
            pnd_Dte8.setValueEdited(pdaAdsa600.getAdsa600_Adi_Annty_Strt_Dte(),new ReportEditMask("YYYYMMDD"));                                                           //Natural: MOVE EDITED ADSA600.ADI-ANNTY-STRT-DTE ( EM = YYYYMMDD ) TO #DTE8
            pnd_Start_Dte.compute(new ComputeParameters(false, pnd_Start_Dte), pnd_Dte8.val());                                                                           //Natural: ASSIGN #START-DTE := VAL ( #DTE8 )
            pnd_Dte8.setValueEdited(pdaAdsa600.getAdsa600_Adi_Finl_Periodic_Py_Dte(),new ReportEditMask("YYYYMMDD"));                                                     //Natural: MOVE EDITED ADSA600.ADI-FINL-PERIODIC-PY-DTE ( EM = YYYYMMDD ) TO #DTE8
            pnd_End_Dte.compute(new ComputeParameters(false, pnd_End_Dte), pnd_Dte8.val());                                                                               //Natural: ASSIGN #END-DTE := VAL ( #DTE8 )
            short decideConditionsMet1125 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE ADSA600.ADI-PYMNT-MODE;//Natural: VALUE 100
            if (condition((pdaAdsa600.getAdsa600_Adi_Pymnt_Mode().equals(100))))
            {
                decideConditionsMet1125++;
                pnd_End_Dte_Pnd_E_Mm.nadd(1);                                                                                                                             //Natural: ADD 1 TO #E-MM
            }                                                                                                                                                             //Natural: VALUE 601:603
            else if (condition(((pdaAdsa600.getAdsa600_Adi_Pymnt_Mode().greaterOrEqual(601) && pdaAdsa600.getAdsa600_Adi_Pymnt_Mode().lessOrEqual(603)))))
            {
                decideConditionsMet1125++;
                pnd_End_Dte_Pnd_E_Mm.nadd(3);                                                                                                                             //Natural: ADD 3 TO #E-MM
            }                                                                                                                                                             //Natural: VALUE 701:706
            else if (condition(((pdaAdsa600.getAdsa600_Adi_Pymnt_Mode().greaterOrEqual(701) && pdaAdsa600.getAdsa600_Adi_Pymnt_Mode().lessOrEqual(706)))))
            {
                decideConditionsMet1125++;
                pnd_End_Dte_Pnd_E_Mm.nadd(6);                                                                                                                             //Natural: ADD 6 TO #E-MM
            }                                                                                                                                                             //Natural: VALUE 801:812
            else if (condition(((pdaAdsa600.getAdsa600_Adi_Pymnt_Mode().greaterOrEqual(801) && pdaAdsa600.getAdsa600_Adi_Pymnt_Mode().lessOrEqual(812)))))
            {
                decideConditionsMet1125++;
                pnd_End_Dte_Pnd_E_Yyyy.nadd(1);                                                                                                                           //Natural: ADD 1 TO #E-YYYY
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(pnd_End_Dte_Pnd_E_Mm.greater(12)))                                                                                                              //Natural: IF #E-MM GT 12
            {
                pnd_End_Dte_Pnd_E_Mm.nsubtract(12);                                                                                                                       //Natural: SUBTRACT 12 FROM #E-MM
                pnd_End_Dte_Pnd_E_Yyyy.nadd(1);                                                                                                                           //Natural: ADD 1 TO #E-YYYY
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Start_Dte_Pnd_S_Mm.greater(pnd_End_Dte_Pnd_E_Mm)))                                                                                          //Natural: IF #S-MM GT #E-MM
            {
                pnd_End_Dte_Pnd_E_Yyyy.nsubtract(1);                                                                                                                      //Natural: SUBTRACT 1 FROM #E-YYYY
            }                                                                                                                                                             //Natural: END-IF
            if (condition((pnd_End_Dte_Pnd_E_Yyyy.subtract(pnd_Start_Dte_Pnd_S_Yyyy)).less(10)))                                                                          //Natural: IF ( #E-YYYY - #S-YYYY ) LT 10
            {
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Ac_Lt_10yrs().setValue(true);                                                                                             //Natural: ASSIGN GTN-PDA-F.CNTRCT-AC-LT-10YRS := TRUE
                //*      GTN-PDA-F.CNTRCT-HOLD-CDE := 'ACNZ'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pdaAdsa600.getAdsa600_Adi_Optn_Cde().equals(21)))                                                                                                   //Natural: IF ADSA600.ADI-OPTN-CDE = 21
        {
            pdaFcpa140f.getGtn_Pda_F_Egtrra_Eligibility_Ind().setValue(pdaAdsa600.getAdsa600_Adp_Ivc_Pymnt_Rule());                                                       //Natural: ASSIGN EGTRRA-ELIGIBILITY-IND := ADSA600.ADP-IVC-PYMNT-RULE
            pdaFcpa140f.getGtn_Pda_F_Originating_Irc_Cde().getValue(1,":",4).setValue(pdaAdsa600.getAdsa600_Adp_Frm_Irc_Cde().getValue(1,":",4));                         //Natural: ASSIGN ORIGINATING-IRC-CDE ( 1:4 ) := ADSA600.ADP-FRM-IRC-CDE ( 1:4 )
            pdaFcpa140f.getGtn_Pda_F_Receiving_Irc_Cde().setValue(pdaAdsa600.getAdsa600_Adp_To_Irc_Cde());                                                                //Natural: ASSIGN RECEIVING-IRC-CDE := ADSA600.ADP-TO-IRC-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa600.getAdsa600_Adi_Pymnt_Cde().equals("F")))                                                                                                 //Natural: IF ADSA600.ADI-PYMNT-CDE = 'F'
        {
            pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_This_Pymnt().setValue(1);                                                                                                    //Natural: ASSIGN GTN-PDA-F.##THIS-PYMNT := 1
            pdaAdsa600.getAdsa600_Pnd_This_Inst().setValue(1);                                                                                                            //Natural: ASSIGN ADSA600.#THIS-INST := 1
            pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Nbr_Of_Pymnts().setValue(1);                                                                                                 //Natural: ASSIGN GTN-PDA-F.##NBR-OF-PYMNTS := 1
            if (condition(pdaFcpa140f.getGtn_Pda_F_Cntrct_Hold_Cde().notEquals(" ") && pdaFcpa140f.getGtn_Pda_F_Cntrct_Hold_Cde().notEquals("0000")))                     //Natural: IF GTN-PDA-F.CNTRCT-HOLD-CDE NE ' ' AND GTN-PDA-F.CNTRCT-HOLD-CDE NE '0000'
            {
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Pay_Type_Req_Ind().setValue(1);                                                                                            //Natural: ASSIGN GTN-PDA-F.PYMNT-PAY-TYPE-REQ-IND := 1
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Hold_Cde().setValue("FTNZ");                                                                                              //Natural: ASSIGN GTN-PDA-F.CNTRCT-HOLD-CDE := 'FTNZ'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa140f.getGtn_Pda_F_Pymnt_Pay_Type_Req_Ind().equals(1)))                                                                               //Natural: IF GTN-PDA-F.PYMNT-PAY-TYPE-REQ-IND = 1
                {
                    pdaFcpa140f.getGtn_Pda_F_Cntrct_Hold_Cde().setValue("FTNZ");                                                                                          //Natural: ASSIGN GTN-PDA-F.CNTRCT-HOLD-CDE := 'FTNZ'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa140f.getGtn_Pda_F_Pymnt_Check_Dte().setValue(pdaAdsa600.getAdsa600_Adi_Instllmnt_Dte());                                                               //Natural: ASSIGN GTN-PDA-F.PYMNT-CHECK-DTE := ADSA600.ADI-INSTLLMNT-DTE
            pnd_Dte8.setValueEdited(pdaAdsa600.getAdsa600_Adi_Instllmnt_Dte(),new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED ADSA600.ADI-INSTLLMNT-DTE ( EM = YYYYMMDD ) TO #DTE8
            pdaCpwa115.getCpwa115_For_Date().compute(new ComputeParameters(false, pdaCpwa115.getCpwa115_For_Date()), pnd_Dte8.val());                                     //Natural: ASSIGN CPWA115.FOR-DATE := VAL ( #DTE8 )
            DbsUtil.callnat(Cpwn119.class , getCurrentProcessState(), pdaCpwa115.getCpwa115());                                                                           //Natural: CALLNAT 'CPWN119' CPWA115
            if (condition(Global.isEscape())) return;
            if (condition(pdaCpwa115.getCpwa115_Error_Code().equals(getZero())))                                                                                          //Natural: IF CPWA115.ERROR-CODE = 0
            {
                pnd_Dte8.setValueEdited(pdaCpwa115.getCpwa115_Accounting_Date(),new ReportEditMask("99999999"));                                                          //Natural: MOVE EDITED CPWA115.ACCOUNTING-DATE ( EM = 99999999 ) TO #DTE8
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Acctg_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Dte8);                                                       //Natural: MOVE EDITED #DTE8 TO GTN-PDA-F.PYMNT-ACCTG-DTE ( EM = YYYYMMDD )
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Ia_Issue_Dte().setValue(pdaAdsa600.getAdsa600_Adi_Annty_Strt_Dte());                                                       //Natural: ASSIGN GTN-PDA-F.PYMNT-IA-ISSUE-DTE := ADSA600.ADI-ANNTY-STRT-DTE
            }                                                                                                                                                             //Natural: END-IF
            //* ** EM - 052711 START
            pdaFcpa140f.getGtn_Pda_F_Pymnt_Intrfce_Dte().setValue(pdaAdsa600.getAdsa600_Adi_Instllmnt_Dte());                                                             //Natural: COMPUTE GTN-PDA-F.PYMNT-INTRFCE-DTE := ADSA600.ADI-INSTLLMNT-DTE
            DbsUtil.callnat(Adsn607a.class , getCurrentProcessState(), pdaFcpa140f.getGtn_Pda_F_Pymnt_Intrfce_Dte(), pnd_Ret_Cde);                                        //Natural: CALLNAT 'ADSN607A' GTN-PDA-F.PYMNT-INTRFCE-DTE #RET-CDE
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Ret_Cde.notEquals(getZero())))                                                                                                              //Natural: IF #RET-CDE NE 0
            {
                pnd_Err_Cde.setValue("L33");                                                                                                                              //Natural: ASSIGN #ERR-CDE := 'L33'
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //* ** EM - 052711 END
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa140f.getGtn_Pda_F_Cntrct_Hold_Cde().notEquals(" ") && pdaFcpa140f.getGtn_Pda_F_Cntrct_Hold_Cde().notEquals("0000")))                     //Natural: IF GTN-PDA-F.CNTRCT-HOLD-CDE NE ' ' AND GTN-PDA-F.CNTRCT-HOLD-CDE NE '0000'
            {
                pdaFcpa140f.getGtn_Pda_F_Pymnt_Pay_Type_Req_Ind().setValue(1);                                                                                            //Natural: ASSIGN GTN-PDA-F.PYMNT-PAY-TYPE-REQ-IND := 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR03:                                                                                                                                                            //Natural: FOR #X 1 6
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(6)); pnd_X.nadd(1))
        {
            //* * 04032006  MN START
            //*  IF SUBSTR(ADSA600.ADI-TIAA-NBRS(#X),1,3) = 'F00' THRU 'F49'
            //*  ICAP
            //*  ICAP IPRO
            //*  ICAP TPA
            if (condition((((pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue(pnd_X).getSubstring(1,3).compareTo("F00") >= 0 && pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue(pnd_X).getSubstring(1,3).compareTo("F09")  //Natural: IF SUBSTR ( ADSA600.ADI-TIAA-NBRS ( #X ) ,1,3 ) = 'F00' THRU 'F09' OR SUBSTR ( ADSA600.ADI-TIAA-NBRS ( #X ) ,1,3 ) = 'II0' THRU 'II9' OR SUBSTR ( ADSA600.ADI-TIAA-NBRS ( #X ) ,1,3 ) = 'IT0' THRU 'IT9'
                <= 0) || (pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue(pnd_X).getSubstring(1,3).compareTo("II0") >= 0 && pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue(pnd_X).getSubstring(1,3).compareTo("II9") 
                <= 0)) || (pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue(pnd_X).getSubstring(1,3).compareTo("IT0") >= 0 && pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue(pnd_X).getSubstring(1,3).compareTo("IT9") 
                <= 0))))
            {
                //* * 04032006  MN END
                pdaFcpa140f.getGtn_Pda_F_Cntrct_Lob_Cde().setValue("RS");                                                                                                 //Natural: ASSIGN GTN-PDA-F.CNTRCT-LOB-CDE := 'RS'
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* * 04032006  MN START
                //* *  IF SUBSTR(ADSA600.ADI-TIAA-NBRS(#X),1,3) = 'F50' THRU 'F74'
                if (condition(pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue(pnd_X).getSubstring(1,3).compareTo("F50") >= 0 && pdaAdsa600.getAdsa600_Adi_Tiaa_Nbrs().getValue(pnd_X).getSubstring(1,3).compareTo("F59")  //Natural: IF SUBSTR ( ADSA600.ADI-TIAA-NBRS ( #X ) ,1,3 ) = 'F50' THRU 'F59'
                    <= 0))
                {
                    //* * 04032006  MN END
                    pdaFcpa140f.getGtn_Pda_F_Cntrct_Lob_Cde().setValue("RSP");                                                                                            //Natural: ASSIGN GTN-PDA-F.CNTRCT-LOB-CDE := 'RSP'
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //* * 04032006  MN START
                    //* *  ELSE
                    //* *    IF SUBSTR(ADSA600.ADI-TIAA-NBRS(#X),1,3) = 'F75' THRU 'F99'
                    //* *      GTN-PDA-F.CNTRCT-LOB-CDE := 'RSP2'
                    //* *      ESCAPE BOTTOM
                    //* *    END-IF
                    //* * 04032006  MN END
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  03072007  MN  START
        if (condition(pdaAdsa600.getAdsa600_Adc_Sub_Plan_Nbr().getSubstring(1,3).equals("AA1") || pdaAdsa600.getAdsa600_Adc_Sub_Plan_Nbr().getSubstring(1,3).equals("TPA")  //Natural: IF SUBSTRING ( ADSA600.ADC-SUB-PLAN-NBR,1,3 ) = 'AA1' OR = 'TPA' OR = 'IPA' OR = 'AA2' OR = 'AA3' OR = 'TPB' OR = 'IPB'
            || pdaAdsa600.getAdsa600_Adc_Sub_Plan_Nbr().getSubstring(1,3).equals("IPA") || pdaAdsa600.getAdsa600_Adc_Sub_Plan_Nbr().getSubstring(1,3).equals("AA2") 
            || pdaAdsa600.getAdsa600_Adc_Sub_Plan_Nbr().getSubstring(1,3).equals("AA3") || pdaAdsa600.getAdsa600_Adc_Sub_Plan_Nbr().getSubstring(1,3).equals("TPB") 
            || pdaAdsa600.getAdsa600_Adc_Sub_Plan_Nbr().getSubstring(1,3).equals("IPB")))
        {
            pdaFcpa140f.getGtn_Pda_F_Cntrct_Ia_Lob_Cde().setValue("S1");                                                                                                  //Natural: ASSIGN GTN-PDA-F.CNTRCT-IA-LOB-CDE := 'S1'
        }                                                                                                                                                                 //Natural: END-IF
        //*  03072007  MN  END
        //*  EM 031708 START
        if (condition(pdaAdsa600.getAdsa600_Adp_Roth_Rqst_Ind().equals("Y")))                                                                                             //Natural: IF ADSA600.ADP-ROTH-RQST-IND = 'Y'
        {
            pnd_Roth_Dte.setValueEdited(pdaAdsa600.getAdsa600_Adp_Frst_Annt_Dte_Of_Brth(),new ReportEditMask("YYYYMMDD"));                                                //Natural: MOVE EDITED ADSA600.ADP-FRST-ANNT-DTE-OF-BRTH ( EM = YYYYMMDD ) TO #ROTH-DTE
            pdaFcpa140f.getGtn_Pda_F_Roth_Dob().setValue(pnd_Roth_Dte_Pnd_Roth_Dte_N);                                                                                    //Natural: ASSIGN ROTH-DOB := #ROTH-DTE-N
            pnd_Roth_Dte.setValueEdited(pdaAdsa600.getAdsa600_Adp_Roth_Frst_Cntrbtn_Dte(),new ReportEditMask("YYYYMMDD"));                                                //Natural: MOVE EDITED ADSA600.ADP-ROTH-FRST-CNTRBTN-DTE ( EM = YYYYMMDD ) TO #ROTH-DTE
            pdaFcpa140f.getGtn_Pda_F_Roth_First_Contrib_Dte().setValue(pnd_Roth_Dte_Pnd_Roth_Dte_N);                                                                      //Natural: ASSIGN ROTH-FIRST-CONTRIB-DTE := #ROTH-DTE-N
            pdaFcpa140f.getGtn_Pda_F_Roth_Death_Dte().setValue(0);                                                                                                        //Natural: ASSIGN ROTH-DEATH-DTE := 0
            if (condition(pdaAdsa600.getAdsa600_Adp_Roth_Dsblty_Dte().notEquals(getZero())))                                                                              //Natural: IF ADSA600.ADP-ROTH-DSBLTY-DTE NE 0
            {
                pnd_Roth_Dte.setValueEdited(pdaAdsa600.getAdsa600_Adp_Roth_Dsblty_Dte(),new ReportEditMask("YYYYMMDD"));                                                  //Natural: MOVE EDITED ADSA600.ADP-ROTH-DSBLTY-DTE ( EM = YYYYMMDD ) TO #ROTH-DTE
                pdaFcpa140f.getGtn_Pda_F_Roth_Disability_Dte().setValue(pnd_Roth_Dte_Pnd_Roth_Dte_N);                                                                     //Natural: ASSIGN ROTH-DISABILITY-DTE := #ROTH-DTE-N
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa140f.getGtn_Pda_F_Roth_Disability_Dte().reset();                                                                                                   //Natural: RESET ROTH-DISABILITY-DTE
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa140f.getGtn_Pda_F_Roth_Money_Source().setValue("ROTHP");                                                                                               //Natural: ASSIGN ROTH-MONEY-SOURCE := 'ROTHP'
            //*  EM 031708 END
        }                                                                                                                                                                 //Natural: END-IF
        //*  120611
        pdaFcpa140f.getGtn_Pda_F_Ia_Orgn_Cde().setValueEdited(pnd_Ia_Orgn_Code,new ReportEditMask("99"));                                                                 //Natural: MOVE EDITED #IA-ORGN-CODE ( EM = 99 ) TO GTN-PDA-F.IA-ORGN-CDE
        if (condition(pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cntrct_Amt().getValue("*").greater(getZero()) || pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Settl_Amt().getValue("*").greater(getZero()))) //Natural: IF GTN-PDA-F.INV-ACCT-CNTRCT-AMT ( * ) GT 0 OR GTN-PDA-F.INV-ACCT-SETTL-AMT ( * ) GT 0
        {
            //*   WRITE
            //*   '>>>>>>  **  DISPLAY GTN-PDA-F BEFORE THE CALL TO FCPN140F ***  <<<'
            //*   PERFORM DISPLAY-GTN-PDA-F
            DbsUtil.callnat(Fcpn140f.class , getCurrentProcessState(), pdaFcpa140f.getGtn_Pda_F());                                                                       //Natural: CALLNAT 'FCPN140F' GTN-PDA-F
            if (condition(Global.isEscape())) return;
            //*   WRITE
            //*  '>>>>>>  **  DISPLAY GTN-PDA-F AFTER THE CALL TO FCPN140F ***  <<<'
            //*  PERFORM DISPLAY-GTN-PDA-F
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cntrct_Amt().getValue("*").less(getZero()) || pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Settl_Amt().getValue("*").less(getZero()))) //Natural: IF GTN-PDA-F.INV-ACCT-CNTRCT-AMT ( * ) LT 0 OR GTN-PDA-F.INV-ACCT-SETTL-AMT ( * ) LT 0
            {
                pnd_Err_Cde.setValue("L30");                                                                                                                              //Natural: ASSIGN #ERR-CDE := 'L30'
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_First.reset();                                                                                                                                                //Natural: RESET #FIRST GTN-PDA-F.##FIRST-STTLMNT-CALL
        pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_First_Sttlmnt_Call().reset();
        if (condition(pdaFcpa140f.getGtn_Pda_F_Gtn_Ret_Code().notEquals("0000")))                                                                                         //Natural: IF GTN-PDA-F.GTN-RET-CODE NE '0000'
        {
            pnd_Err_Cde.setValue(pdaFcpa140f.getGtn_Pda_F_Gtn_Ret_Code());                                                                                                //Natural: ASSIGN #ERR-CDE := GTN-PDA-F.GTN-RET-CODE
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-PDA-FIELDS
        //* *  TEST
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-GTN-PDA-F
        //* ***********************************************************************
        //*  DEFINE SUBROUTINE CALCULATE-AGE-ETC
        //* ************************T**********************************************
        //*  RESET #SEASONED
        //*  IF #SEASONING-DTE GT 0 AND #ORGN-CDE = 20 THRU 22
        //*    MOVE GTN-PDA-F.PYMNT-SETTLMNT-DTE TO #BASE-DTE
        //*    MOVE EDITED #BASE-DTE(EM=YYYY) TO #BASE-YYYY
        //*   CALLNAT 'NAZN600C' ADSA600.ADP-FRST-ANNT-DTE-OF-BRTH
        //*      #AGE-YY #AGE-MM #BASE-DTE
        //*    IF #BASE-YYYYN - #SEASONING-YYYY GE 5 AND
        //*        (#AGE-YY GT 59 OR (#AGE-YY = 59 AND #AGE-MM GE 6))
        //*     #SEASONED := TRUE
        //*   END-IF
        //*  END-IF
        //*  END-SUBROUTINE
    }
    private void sub_Move_Pda_Fields() throws Exception                                                                                                                   //Natural: MOVE-PDA-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Adp_Alt_Dest_Hold_Cde.notEquals(" ")))                                                                                                          //Natural: IF #ADP-ALT-DEST-HOLD-CDE NE ' '
        {
            ads_Prtcpnt_Adp_Alt_Dest_Hold_Cde.setValue(pnd_Adp_Alt_Dest_Hold_Cde);                                                                                        //Natural: ASSIGN ADS-PRTCPNT.ADP-ALT-DEST-HOLD-CDE := #ADP-ALT-DEST-HOLD-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Adp_Crrspndnce_Perm_Addr_Txt.getValue("*").greater(" ")))                                                                                       //Natural: IF #ADP-CRRSPNDNCE-PERM-ADDR-TXT ( * ) GT ' '
        {
            ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Txt.getValue("*").setValue(pnd_Adp_Crrspndnce_Perm_Addr_Txt.getValue("*"));                                              //Natural: ASSIGN ADS-PRTCPNT.ADP-CRRSPNDNCE-PERM-ADDR-TXT ( * ) := #ADP-CRRSPNDNCE-PERM-ADDR-TXT ( * )
            ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Zip.setValue(pnd_Adp_Crrspndnce_Perm_Addr_Zip);                                                                          //Natural: ASSIGN ADS-PRTCPNT.ADP-CRRSPNDNCE-PERM-ADDR-ZIP := #ADP-CRRSPNDNCE-PERM-ADDR-ZIP
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Adp_Crrspndnce_Perm_Addr_Typ_Cd.greater(" ")))                                                                                                  //Natural: IF #ADP-CRRSPNDNCE-PERM-ADDR-TYP-CD GT ' '
        {
            ads_Prtcpnt_Adp_Crrspndnce_Perm_Addr_Typ_Cd.setValue(pnd_Adp_Crrspndnce_Perm_Addr_Typ_Cd);                                                                    //Natural: ASSIGN ADS-PRTCPNT.ADP-CRRSPNDNCE-PERM-ADDR-TYP-CD := #ADP-CRRSPNDNCE-PERM-ADDR-TYP-CD
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Display_Gtn_Pda_F() throws Exception                                                                                                                 //Natural: DISPLAY-GTN-PDA-F
    {
        if (BLNatReinput.isReinput()) return;

        //*  GTN-PDA-F
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Corp_Wpid());                                                                                            //Natural: WRITE '=' PYMNT-CORP-WPID
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Reqst_Log_Dte_Time());                                                                                   //Natural: WRITE '=' PYMNT-REQST-LOG-DTE-TIME
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Unq_Id_Nbr());                                                                                          //Natural: WRITE '=' CNTRCT-UNQ-ID-NBR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Ppcn_Nbr());                                                                                            //Natural: WRITE '=' CNTRCT-PPCN-NBR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Da_Nbrs());                                                                                             //Natural: WRITE '=' CNTRCT-DA-NBRS
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Annt_Soc_Sec_Ind());                                                                                           //Natural: WRITE '=' ANNT-SOC-SEC-IND
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Annt_Soc_Sec_Nbr());                                                                                           //Natural: WRITE '=' ANNT-SOC-SEC-NBR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Annt_Ctznshp_Cde());                                                                                           //Natural: WRITE '=' ANNT-CTZNSHP-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Annt_Rsdncy_Cde());                                                                                            //Natural: WRITE '=' ANNT-RSDNCY-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Annt_Locality_Cde());                                                                                          //Natural: WRITE '=' ANNT-LOCALITY-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Ph_Name());                                                                                                    //Natural: WRITE '=' PH-NAME
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Dob());                                                                                                  //Natural: WRITE '=' PYMNT-DOB
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Ph_Sex());                                                                                                     //Natural: WRITE '=' PH-SEX
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Zip_Cde().getValue("*"));                                                                           //Natural: WRITE '=' PYMNT-ADDR-ZIP-CDE ( * )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Postl_Data().getValue("*"));                                                                             //Natural: WRITE '=' PYMNT-POSTL-DATA ( * )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Type_Ind().getValue("*"));                                                                          //Natural: WRITE '=' PYMNT-ADDR-TYPE-IND ( * )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Foreign_Cde().getValue("*"));                                                                            //Natural: WRITE '=' PYMNT-FOREIGN-CDE ( * )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Addr_Chg_Ind().getValue("*"));                                                                           //Natural: WRITE '=' PYMNT-ADDR-CHG-IND ( * )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Alt_Addr_Ind());                                                                                         //Natural: WRITE '=' PYMNT-ALT-ADDR-IND
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Pymnt_Prcss_Seq_Num_X());                                                                              //Natural: WRITE '=' ##PYMNT-PRCSS-SEQ-NUM-X
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Pymnt_Instmt_Nbr());                                                                                   //Natural: WRITE '=' ##PYMNT-INSTMT-NBR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Settl_Ivc_Ind());                                                                                        //Natural: WRITE '=' PYMNT-SETTL-IVC-IND
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_First_Sttlmnt_Call());                                                                                 //Natural: WRITE '=' ##FIRST-STTLMNT-CALL
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Rtb_Total_Amt());                                                                                      //Natural: WRITE '=' ##RTB-TOTAL-AMT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Rtb_Gross_Amt_For_Tax());                                                                              //Natural: WRITE '=' ##RTB-GROSS-AMT-FOR-TAX
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Rtb_Ivc_Amt());                                                                                        //Natural: WRITE '=' ##RTB-IVC-AMT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Rtb_Tot_Fed_Tax_Amt());                                                                                //Natural: WRITE '=' ##RTB-TOT-FED-TAX-AMT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Rtb_Tot_Sta_Tax_Amt());                                                                                //Natural: WRITE '=' ##RTB-TOT-STA-TAX-AMT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Rtb_Tot_Loc_Tax_Amt());                                                                                //Natural: WRITE '=' ##RTB-TOT-LOC-TAX-AMT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Rtb_Fed_Tax_Amt_Alloc());                                                                              //Natural: WRITE '=' ##RTB-FED-TAX-AMT-ALLOC
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Rtb_Sta_Tax_Amt_Alloc());                                                                              //Natural: WRITE '=' ##RTB-STA-TAX-AMT-ALLOC
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Rtb_Loc_Tax_Amt_Alloc());                                                                              //Natural: WRITE '=' ##RTB-LOC-TAX-AMT-ALLOC
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Rtb_Gross_Amt_Alloc());                                                                                //Natural: WRITE '=' ##RTB-GROSS-AMT-ALLOC
        if (Global.isEscape()) return;
        //*  ##RTB-DPI-INFO(3)
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Rtb_Amt_For_Dpi().getValue("*"));                                                                      //Natural: WRITE '=' ##RTB-AMT-FOR-DPI ( * )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Rtb_Amt_For_Dpi_Alloc().getValue("*"));                                                                //Natural: WRITE '=' ##RTB-AMT-FOR-DPI-ALLOC ( * )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Rtb_Total_Dpi_Amt().getValue("*"));                                                                    //Natural: WRITE '=' ##RTB-TOTAL-DPI-AMT ( * )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Rtb_Dpi_Amt_Alloc().getValue("*"));                                                                    //Natural: WRITE '=' ##RTB-DPI-AMT-ALLOC ( * )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Total_Check_Amt());                                                                                    //Natural: WRITE '=' ##TOTAL-CHECK-AMT
        if (Global.isEscape()) return;
        //*  ##FUND-PAYEE(40)
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cde().getValue("*"));                                                                                 //Natural: WRITE '=' INV-ACCT-CDE ( * )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Valuat_Period().getValue("*"));                                                                       //Natural: WRITE '=' INV-ACCT-VALUAT-PERIOD ( * )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Settl_Amt().getValue("*"));                                                                           //Natural: WRITE '=' INV-ACCT-SETTL-AMT ( * )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Cntrct_Amt().getValue("*"));                                                                          //Natural: WRITE '=' INV-ACCT-CNTRCT-AMT ( * )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Dvdnd_Amt().getValue("*"));                                                                           //Natural: WRITE '=' INV-ACCT-DVDND-AMT ( * )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Unit_Value().getValue("*"));                                                                          //Natural: WRITE '=' INV-ACCT-UNIT-VALUE ( * )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Unit_Qty().getValue("*"));                                                                            //Natural: WRITE '=' INV-ACCT-UNIT-QTY ( * )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Inv_Acct_Ivc_Amt().getValue("*"));                                                                             //Natural: WRITE '=' INV-ACCT-IVC-AMT ( * )
        if (Global.isEscape()) return;
        //*  2 ##DEDUCTIONS(10)
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Ded_Cde().getValue("*"));                                                                                //Natural: WRITE '=' PYMNT-DED-CDE ( * )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Ded_Amt().getValue("*"));                                                                                //Natural: WRITE '=' PYMNT-DED-AMT ( * )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Ded_Payee_Cde().getValue("*"));                                                                          //Natural: WRITE '=' PYMNT-DED-PAYEE-CDE ( * )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Payee_Tax_Ind());                                                                                        //Natural: WRITE '=' PYMNT-PAYEE-TAX-IND
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Ftre_Ind());                                                                                             //Natural: WRITE '=' PYMNT-FTRE-IND
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Settlmnt_Dte());                                                                                         //Natural: WRITE '=' PYMNT-SETTLMNT-DTE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Check_Dte());                                                                                            //Natural: WRITE '=' PYMNT-CHECK-DTE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Cycle_Dte());                                                                                            //Natural: WRITE '=' PYMNT-CYCLE-DTE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Acctg_Dte());                                                                                            //Natural: WRITE '=' PYMNT-ACCTG-DTE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Ia_Issue_Dte());                                                                                         //Natural: WRITE '=' PYMNT-IA-ISSUE-DTE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Intrfce_Dte());                                                                                          //Natural: WRITE '=' PYMNT-INTRFCE-DTE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Check_Crrncy_Cde());                                                                                    //Natural: WRITE '=' CNTRCT-CHECK-CRRNCY-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Type_Cde());                                                                                            //Natural: WRITE '=' CNTRCT-TYPE-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Payee_Cde());                                                                                           //Natural: WRITE '=' CNTRCT-PAYEE-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Option_Cde());                                                                                          //Natural: WRITE '=' CNTRCT-OPTION-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Ac_Lt_10yrs());                                                                                         //Natural: WRITE '=' CNTRCT-AC-LT-10YRS
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Mode_Cde());                                                                                            //Natural: WRITE '=' CNTRCT-MODE-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Method_Cde());                                                                                           //Natural: WRITE '=' PYMNT-METHOD-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Pay_Type_Req_Ind());                                                                                     //Natural: WRITE '=' PYMNT-PAY-TYPE-REQ-IND
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Spouse_Pay_Stats());                                                                                     //Natural: WRITE '=' PYMNT-SPOUSE-PAY-STATS
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Pymnt_Type_Ind());                                                                                      //Natural: WRITE '=' CNTRCT-PYMNT-TYPE-IND
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Sttlmnt_Type_Ind());                                                                                    //Natural: WRITE '=' CNTRCT-STTLMNT-TYPE-IND
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Rtb_Dest_Typ());                                                                                         //Natural: WRITE '=' PYMNT-RTB-DEST-TYP
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Pymnt_Dest_Cde());                                                                                      //Natural: WRITE '=' CNTRCT-PYMNT-DEST-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Roll_Dest_Cde());                                                                                       //Natural: WRITE '=' CNTRCT-ROLL-DEST-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Eft_Acct_Nbr());                                                                                         //Natural: WRITE '=' PYMNT-EFT-ACCT-NBR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Eft_Transit_Id_A());                                                                                     //Natural: WRITE '=' PYMNT-EFT-TRANSIT-ID-A
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Chk_Sav_Ind());                                                                                          //Natural: WRITE '=' PYMNT-CHK-SAV-IND
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Hold_Cde());                                                                                            //Natural: WRITE '=' CNTRCT-HOLD-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Hold_Ind());                                                                                            //Natural: WRITE '=' CNTRCT-HOLD-IND
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Hold_Grp());                                                                                            //Natural: WRITE '=' CNTRCT-HOLD-GRP
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Hold_User_Id());                                                                                        //Natural: WRITE '=' CNTRCT-HOLD-USER-ID
        if (Global.isEscape()) return;
        //*  ##TX-WITHHOLDING-TAX-TYPES
        //*  REDEFINE ##TX-WITHHOLDING-TAX-TYPES
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Payee_Tx_Elct_Trggr());                                                                                  //Natural: WRITE '=' PYMNT-PAYEE-TX-ELCT-TRGGR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_State_Tax_Code());                                                                                     //Natural: WRITE '=' ##STATE-TAX-CODE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Local_Tax_Code());                                                                                     //Natural: WRITE '=' ##LOCAL-TAX-CODE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Tax_Exempt_Ind());                                                                                       //Natural: WRITE '=' PYMNT-TAX-EXEMPT-IND
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Tax_Form());                                                                                             //Natural: WRITE '=' PYMNT-TAX-FORM
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pymnt_Tax_Calc_Cde());                                                                                         //Natural: WRITE '=' PYMNT-TAX-CALC-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Qlfied_Cde());                                                                                          //Natural: WRITE '=' CNTRCT-QLFIED-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Lob_Cde());                                                                                             //Natural: WRITE '=' CNTRCT-LOB-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Sub_Lob_Cde());                                                                                         //Natural: WRITE '=' CNTRCT-SUB-LOB-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Ia_Lob_Cde());                                                                                          //Natural: WRITE '=' CNTRCT-IA-LOB-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Annty_Ins_Type());                                                                                      //Natural: WRITE '=' CNTRCT-ANNTY-INS-TYPE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Annty_Type_Cde());                                                                                      //Natural: WRITE '=' CNTRCT-ANNTY-TYPE-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Insurance_Option());                                                                                    //Natural: WRITE '=' CNTRCT-INSURANCE-OPTION
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Cntrct_Life_Contingency());                                                                                    //Natural: WRITE '=' CNTRCT-LIFE-CONTINGENCY
        if (Global.isEscape()) return;
        //*  TAX-INFO
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Tax_Fed_C_Resp_Cde());                                                                                         //Natural: WRITE '=' TAX-FED-C-RESP-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Tax_Fed_C_Filing_Stat());                                                                                      //Natural: WRITE '=' TAX-FED-C-FILING-STAT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Tax_Fed_C_Allow_Cnt());                                                                                        //Natural: WRITE '=' TAX-FED-C-ALLOW-CNT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Tax_Fed_C_Fixed_Amt());                                                                                        //Natural: WRITE '=' TAX-FED-C-FIXED-AMT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Tax_Fed_C_Fixed_Pct());                                                                                        //Natural: WRITE '=' TAX-FED-C-FIXED-PCT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Tax_Sta_C_Resp_Cde());                                                                                         //Natural: WRITE '=' TAX-STA-C-RESP-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Tax_Sta_C_Filing_Stat());                                                                                      //Natural: WRITE '=' TAX-STA-C-FILING-STAT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Tax_Sta_C_Allow_Cnt());                                                                                        //Natural: WRITE '=' TAX-STA-C-ALLOW-CNT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Tax_Sta_C_Fixed_Amt());                                                                                        //Natural: WRITE '=' TAX-STA-C-FIXED-AMT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Tax_Sta_C_Fixed_Pct());                                                                                        //Natural: WRITE '=' TAX-STA-C-FIXED-PCT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Tax_Sta_C_Spouse_Cnt());                                                                                       //Natural: WRITE '=' TAX-STA-C-SPOUSE-CNT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Tax_Sta_C_Blind_Cnt());                                                                                        //Natural: WRITE '=' TAX-STA-C-BLIND-CNT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Tax_Loc_C_Resp_Code());                                                                                        //Natural: WRITE '=' TAX-LOC-C-RESP-CODE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Tax_Loc_C_Fixed_Amt());                                                                                        //Natural: WRITE '=' TAX-LOC-C-FIXED-AMT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Tax_Loc_C_Fixed_Pct());                                                                                        //Natural: WRITE '=' TAX-LOC-C-FIXED-PCT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Pda_Count());                                                                                          //Natural: WRITE '=' ##PDA-COUNT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_This_Pymnt());                                                                                         //Natural: WRITE '=' ##THIS-PYMNT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Pnd_Pnd_Nbr_Of_Pymnts());                                                                                      //Natural: WRITE '=' ##NBR-OF-PYMNTS
        if (Global.isEscape()) return;
        getReports().write(0, "!!!!!! GTN-RET-CODE !!!!!!!:");                                                                                                            //Natural: WRITE '!!!!!! GTN-RET-CODE !!!!!!!:'
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Gtn_Ret_Code());                                                                                               //Natural: WRITE '=' GTN-RET-CODE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Gtn_Ret_Msg());                                                                                                //Natural: WRITE '=' GTN-RET-MSG
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Egtrra_Eligibility_Ind());                                                                                     //Natural: WRITE '=' EGTRRA-ELIGIBILITY-IND
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Originating_Irc_Cde().getValue("*"));                                                                          //Natural: WRITE '=' ORIGINATING-IRC-CDE ( * )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Receiving_Irc_Cde());                                                                                          //Natural: WRITE '=' RECEIVING-IRC-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Roth_Dob());                                                                                                   //Natural: WRITE '=' ROTH-DOB
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Roth_First_Contrib_Dte());                                                                                     //Natural: WRITE '=' ROTH-FIRST-CONTRIB-DTE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Roth_Death_Dte());                                                                                             //Natural: WRITE '=' ROTH-DEATH-DTE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Roth_Disability_Dte());                                                                                        //Natural: WRITE '=' ROTH-DISABILITY-DTE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Roth_Money_Source());                                                                                          //Natural: WRITE '=' ROTH-MONEY-SOURCE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140f.getGtn_Pda_F_Roth_Qual_Non_Qual_Distrib());                                                                                 //Natural: WRITE '=' ROTH-QUAL-NON-QUAL-DISTRIB
        if (Global.isEscape()) return;
    }

    //
}
