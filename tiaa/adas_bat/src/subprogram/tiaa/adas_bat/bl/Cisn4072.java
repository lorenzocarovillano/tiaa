/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 11:09:26 PM
**        * FROM NATURAL SUBPROGRAM : Cisn4072
************************************************************
**        * FILE NAME            : Cisn4072.java
**        * CLASS NAME           : Cisn4072
**        * INSTANCE NAME        : Cisn4072
************************************************************
*   --------------------------------------------------------------------
*
*  SUBPROGRAM-ID:  CISN4070
*
*  DESCRIPTION  :  THIS MODULE RETURNS THE STATE WHICH APPROVED THE
*                  CONTRACT WITH THE LATEST ISSUE DATE
*
*  UPDATED      :
* 06/07/06 O SOTTO CHANGED AS PART OF MDO LEGAL SPLIT. SC 060706.
* 12/16/08 H.KAKADIA - ADDED TIAA-ACCESS APPROVAL
* 01/27/09 H.KAKADIA - CHANGED TIAA-ACCESS APPROVAL TO USE RESIDENCY
* 03/12/13 RJ FRANKLIN - ADAS CALLS THIS CODE WITH THE RESIDENT STATE
*                      AND A LIST OF ISSUE STATES FROM THE ORIGINAL
*                      ISSUE STATES.  CODE WAS PLACED IN THIS PROGRAM
*                      TO OVERRIDE THE DA ISSUE STATES WITH THE STATE
*                      OF RESIDENCE. THIS CHANGE REVERSES THAT CHANGE.
*                                   SCAN FOR TAG RJ01
* 05/10/17  (MUKHR)   PIN EXPANSION CHANGES. (C420007) STOW ONLY
**----------------------------------------------------------------------
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cisn4072 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaCisa1000 pdaCisa1000;
    private PdaCisa4070 pdaCisa4070;
    private PdaCisa4060 pdaCisa4060;
    private LdaCisl4070 ldaCisl4070;
    private PdaCisa8001 pdaCisa8001;
    private PdaAcia4020 pdaAcia4020;

    // Local Variables
    public DbsRecord localVariables;

    // parameters

    private DbsGroup pnd_Contract_Tbl;
    private DbsField pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code;
    private DbsField pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_Date;
    private DbsField pnd_Contract_Tbl_Cnt;
    private DbsField pnd_Contract_Tbl_Max;

    private DbsGroup pnd_Temp_Contract;
    private DbsField pnd_Temp_Contract_Pnd_Temp_Contract_No;
    private DbsField pnd_Temp_Contract_Pnd_Temp_Ia_Found;
    private DbsField pnd_Temp_Contract_Pnd_Temp_Da_Found;
    private DbsField pnd_Temp_Contract_Pnd_Temp_Issue_State_Code;
    private DbsField pnd_Temp_Contract_Pnd_Temp_Issue_State_Nbr_A;
    private DbsField pnd_Temp_Contract_Pnd_Temp_State_Code;
    private DbsField pnd_Temp_Contract_Pnd_Temp_State_Nbr_A;
    private DbsField pnd_Temp_Contract_Pnd_Temp_Issue_Date;
    private DbsField pnd_Temp_Date_N8;

    private DbsGroup pnd_Temp_Date_N8__R_Field_1;
    private DbsField pnd_Temp_Date_N8_Pnd_Temp_Date_Mmddccyy_Mm;
    private DbsField pnd_Temp_Date_N8_Pnd_Temp_Date_Mmddccyy_Dd;
    private DbsField pnd_Temp_Date_N8_Pnd_Temp_Date_Mmddccyy_Cc;
    private DbsField pnd_Temp_Date_N8_Pnd_Temp_Date_Mmddccyy_Yy;

    private DbsGroup pnd_Temp_Date_N8__R_Field_2;
    private DbsField pnd_Temp_Date_N8_Pnd_Temp_Date_Ccyymmdd_Cc;
    private DbsField pnd_Temp_Date_N8_Pnd_Temp_Date_Ccyymmdd_Yy;
    private DbsField pnd_Temp_Date_N8_Pnd_Temp_Date_Ccyymmdd_Mm;
    private DbsField pnd_Temp_Date_N8_Pnd_Temp_Date_Ccyymmdd_Dd;

    private DbsGroup pnd_Temp_Date_N8__R_Field_3;
    private DbsField pnd_Temp_Date_N8_Pnd_Temp_Date_Ccyymm;
    private DbsField pnd_Temp_Date_N4;

    private DbsGroup pnd_Temp_Date_N4__R_Field_4;
    private DbsField pnd_Temp_Date_N4_Pnd_Temp_Date_Mmyy_Mm;
    private DbsField pnd_Temp_Date_N4_Pnd_Temp_Date_Mmyy_Yy;
    private DbsField pnd_W_4060_Entry_Code;

    private DbsGroup pnd_W_4060_Entry_Code__R_Field_5;
    private DbsField pnd_W_4060_Entry_Code_Pnd_W_4060_State_Code;
    private DbsField pnd_W_4060_Entry_Code_Pnd_W_4060_Prdct_Code;
    private DbsField pnd_W_4060_Entry_Dscrptn_Txt;

    private DbsGroup pnd_W_4060_Entry_Dscrptn_Txt__R_Field_6;
    private DbsField pnd_W_4060_Entry_Dscrptn_Txt_Pnd_W_4060_Approval_Date_1;
    private DbsField pnd_W_4060_Entry_Dscrptn_Txt_Pnd_W_4060_Approval_Date_2;
    private DbsField pnd_I;
    private DbsField pnd_I2;
    private DbsField pnd_J;
    private DbsField pnd_J2;
    private DbsField pnd_Ia_State_Code;

    private DbsGroup pnd_Ia_State_Code__R_Field_7;
    private DbsField pnd_Ia_State_Code_Pnd_Ia_State_Code_1_1;
    private DbsField pnd_Ia_State_Code_Pnd_Ia_State_Code_2_3;
    private DbsField pnd_State_Code;
    private DbsField pnd_State_Nbr_A;
    private DbsField pnd_Found;
    private DbsField pnd_Debug_1;
    private DbsField pnd_State_Cde_Coll;
    private DbsField pnd_State_Cde_Coll_1st;
    private DbsField pnd_California;
    private DbsField pnd_State_Found;
    private DbsField pnd_M;
    private DbsField pnd_N;
    private DbsField pnd_L;
    private DbsField pnd_Prev_Source;
    private DbsField pnd_California_Apprv;
    private DbsField pnd_Np_Super_De_1;

    private DbsGroup pnd_Np_Super_De_1__R_Field_8;
    private DbsField pnd_Np_Super_De_1_Pnd_Aa_Physical_Record_Cd;
    private DbsField pnd_Np_Super_De_1_Pnd_Aa_Tiaa_Contract_No;
    private DbsField pnd_Different_Coll_State_Found;
    private DbsField pnd_Found_Non_California;
    private DbsField pnd_Found_State;
    private DbsField pnd_Hold_State;
    private DbsField pnd_K;
    private DbsField pnd_Perform_Approval;

    private DbsGroup pnd_Cis_Tiaa_Cntrct_Nbr_Input;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data;

    private DbsGroup pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_9;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_To_State_Input;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_To_Approval_Ind_Input;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_State_Input;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_Approval_Ind_Input;

    private DbsGroup pnd_Cis_Tiaa_Cntrct_Nbr_Hold;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Data;

    private DbsGroup pnd_Cis_Tiaa_Cntrct_Nbr_Hold__R_Field_10;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State1_Hold;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind1_Hold;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State2_Hold;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind2_Hold;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State3_Hold;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind3_Hold;

    private DbsGroup pnd_Cis_Tiaa_Cntrct_Nbr_Res;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Cis_Tiaa_Cntrct_Nbr_Res_Data;

    private DbsGroup pnd_Cis_Tiaa_Cntrct_Nbr_Res__R_Field_11;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State1_Hold;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind1_Hold;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State2_Hold;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind2_Hold;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State3_Hold;
    private DbsField pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind3_Hold;
    private DbsField pnd_Issue_State_Found;
    private DbsField pnd_Return;
    private DbsField pnd_Return_Single;
    private DbsField pnd_Return_Contract;
    private DbsField pnd_Return_Multi;
    private DbsField pnd_Return_Annual;
    private DbsField pnd_Cntrct_Payee_Key;

    private DbsGroup pnd_Cntrct_Payee_Key__R_Field_12;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde;
    private DbsField pnd_C;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCisa4070 = new PdaCisa4070(localVariables);
        pdaCisa4060 = new PdaCisa4060(localVariables);
        ldaCisl4070 = new LdaCisl4070();
        registerRecord(ldaCisl4070);
        pdaCisa8001 = new PdaCisa8001(localVariables);
        pdaAcia4020 = new PdaAcia4020(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaCisa1000 = new PdaCisa1000(parameters);

        pnd_Contract_Tbl = parameters.newGroupArrayInRecord("pnd_Contract_Tbl", "#CONTRACT-TBL", new DbsArrayController(1, 20));
        pnd_Contract_Tbl.setParameterOption(ParameterOption.ByReference);
        pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code = pnd_Contract_Tbl.newFieldInGroup("pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code", "#CONTRACT-TBL-ISSUE-STATE-CODE", 
            FieldType.STRING, 2);
        pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_Date = pnd_Contract_Tbl.newFieldInGroup("pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_Date", "#CONTRACT-TBL-ISSUE-DATE", 
            FieldType.NUMERIC, 8);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Contract_Tbl_Cnt = localVariables.newFieldInRecord("pnd_Contract_Tbl_Cnt", "#CONTRACT-TBL-CNT", FieldType.PACKED_DECIMAL, 2);
        pnd_Contract_Tbl_Max = localVariables.newFieldInRecord("pnd_Contract_Tbl_Max", "#CONTRACT-TBL-MAX", FieldType.PACKED_DECIMAL, 2);

        pnd_Temp_Contract = localVariables.newGroupInRecord("pnd_Temp_Contract", "#TEMP-CONTRACT");
        pnd_Temp_Contract_Pnd_Temp_Contract_No = pnd_Temp_Contract.newFieldInGroup("pnd_Temp_Contract_Pnd_Temp_Contract_No", "#TEMP-CONTRACT-NO", FieldType.STRING, 
            10);
        pnd_Temp_Contract_Pnd_Temp_Ia_Found = pnd_Temp_Contract.newFieldInGroup("pnd_Temp_Contract_Pnd_Temp_Ia_Found", "#TEMP-IA-FOUND", FieldType.BOOLEAN, 
            1);
        pnd_Temp_Contract_Pnd_Temp_Da_Found = pnd_Temp_Contract.newFieldInGroup("pnd_Temp_Contract_Pnd_Temp_Da_Found", "#TEMP-DA-FOUND", FieldType.BOOLEAN, 
            1);
        pnd_Temp_Contract_Pnd_Temp_Issue_State_Code = pnd_Temp_Contract.newFieldInGroup("pnd_Temp_Contract_Pnd_Temp_Issue_State_Code", "#TEMP-ISSUE-STATE-CODE", 
            FieldType.STRING, 2);
        pnd_Temp_Contract_Pnd_Temp_Issue_State_Nbr_A = pnd_Temp_Contract.newFieldInGroup("pnd_Temp_Contract_Pnd_Temp_Issue_State_Nbr_A", "#TEMP-ISSUE-STATE-NBR-A", 
            FieldType.STRING, 2);
        pnd_Temp_Contract_Pnd_Temp_State_Code = pnd_Temp_Contract.newFieldInGroup("pnd_Temp_Contract_Pnd_Temp_State_Code", "#TEMP-STATE-CODE", FieldType.STRING, 
            2);
        pnd_Temp_Contract_Pnd_Temp_State_Nbr_A = pnd_Temp_Contract.newFieldInGroup("pnd_Temp_Contract_Pnd_Temp_State_Nbr_A", "#TEMP-STATE-NBR-A", FieldType.STRING, 
            2);
        pnd_Temp_Contract_Pnd_Temp_Issue_Date = pnd_Temp_Contract.newFieldInGroup("pnd_Temp_Contract_Pnd_Temp_Issue_Date", "#TEMP-ISSUE-DATE", FieldType.NUMERIC, 
            8);
        pnd_Temp_Date_N8 = localVariables.newFieldInRecord("pnd_Temp_Date_N8", "#TEMP-DATE-N8", FieldType.NUMERIC, 8);

        pnd_Temp_Date_N8__R_Field_1 = localVariables.newGroupInRecord("pnd_Temp_Date_N8__R_Field_1", "REDEFINE", pnd_Temp_Date_N8);
        pnd_Temp_Date_N8_Pnd_Temp_Date_Mmddccyy_Mm = pnd_Temp_Date_N8__R_Field_1.newFieldInGroup("pnd_Temp_Date_N8_Pnd_Temp_Date_Mmddccyy_Mm", "#TEMP-DATE-MMDDCCYY-MM", 
            FieldType.STRING, 2);
        pnd_Temp_Date_N8_Pnd_Temp_Date_Mmddccyy_Dd = pnd_Temp_Date_N8__R_Field_1.newFieldInGroup("pnd_Temp_Date_N8_Pnd_Temp_Date_Mmddccyy_Dd", "#TEMP-DATE-MMDDCCYY-DD", 
            FieldType.STRING, 2);
        pnd_Temp_Date_N8_Pnd_Temp_Date_Mmddccyy_Cc = pnd_Temp_Date_N8__R_Field_1.newFieldInGroup("pnd_Temp_Date_N8_Pnd_Temp_Date_Mmddccyy_Cc", "#TEMP-DATE-MMDDCCYY-CC", 
            FieldType.STRING, 2);
        pnd_Temp_Date_N8_Pnd_Temp_Date_Mmddccyy_Yy = pnd_Temp_Date_N8__R_Field_1.newFieldInGroup("pnd_Temp_Date_N8_Pnd_Temp_Date_Mmddccyy_Yy", "#TEMP-DATE-MMDDCCYY-YY", 
            FieldType.STRING, 2);

        pnd_Temp_Date_N8__R_Field_2 = localVariables.newGroupInRecord("pnd_Temp_Date_N8__R_Field_2", "REDEFINE", pnd_Temp_Date_N8);
        pnd_Temp_Date_N8_Pnd_Temp_Date_Ccyymmdd_Cc = pnd_Temp_Date_N8__R_Field_2.newFieldInGroup("pnd_Temp_Date_N8_Pnd_Temp_Date_Ccyymmdd_Cc", "#TEMP-DATE-CCYYMMDD-CC", 
            FieldType.STRING, 2);
        pnd_Temp_Date_N8_Pnd_Temp_Date_Ccyymmdd_Yy = pnd_Temp_Date_N8__R_Field_2.newFieldInGroup("pnd_Temp_Date_N8_Pnd_Temp_Date_Ccyymmdd_Yy", "#TEMP-DATE-CCYYMMDD-YY", 
            FieldType.STRING, 2);
        pnd_Temp_Date_N8_Pnd_Temp_Date_Ccyymmdd_Mm = pnd_Temp_Date_N8__R_Field_2.newFieldInGroup("pnd_Temp_Date_N8_Pnd_Temp_Date_Ccyymmdd_Mm", "#TEMP-DATE-CCYYMMDD-MM", 
            FieldType.STRING, 2);
        pnd_Temp_Date_N8_Pnd_Temp_Date_Ccyymmdd_Dd = pnd_Temp_Date_N8__R_Field_2.newFieldInGroup("pnd_Temp_Date_N8_Pnd_Temp_Date_Ccyymmdd_Dd", "#TEMP-DATE-CCYYMMDD-DD", 
            FieldType.STRING, 2);

        pnd_Temp_Date_N8__R_Field_3 = localVariables.newGroupInRecord("pnd_Temp_Date_N8__R_Field_3", "REDEFINE", pnd_Temp_Date_N8);
        pnd_Temp_Date_N8_Pnd_Temp_Date_Ccyymm = pnd_Temp_Date_N8__R_Field_3.newFieldInGroup("pnd_Temp_Date_N8_Pnd_Temp_Date_Ccyymm", "#TEMP-DATE-CCYYMM", 
            FieldType.NUMERIC, 6);
        pnd_Temp_Date_N4 = localVariables.newFieldInRecord("pnd_Temp_Date_N4", "#TEMP-DATE-N4", FieldType.NUMERIC, 4);

        pnd_Temp_Date_N4__R_Field_4 = localVariables.newGroupInRecord("pnd_Temp_Date_N4__R_Field_4", "REDEFINE", pnd_Temp_Date_N4);
        pnd_Temp_Date_N4_Pnd_Temp_Date_Mmyy_Mm = pnd_Temp_Date_N4__R_Field_4.newFieldInGroup("pnd_Temp_Date_N4_Pnd_Temp_Date_Mmyy_Mm", "#TEMP-DATE-MMYY-MM", 
            FieldType.STRING, 2);
        pnd_Temp_Date_N4_Pnd_Temp_Date_Mmyy_Yy = pnd_Temp_Date_N4__R_Field_4.newFieldInGroup("pnd_Temp_Date_N4_Pnd_Temp_Date_Mmyy_Yy", "#TEMP-DATE-MMYY-YY", 
            FieldType.STRING, 2);
        pnd_W_4060_Entry_Code = localVariables.newFieldInRecord("pnd_W_4060_Entry_Code", "#W-4060-ENTRY-CODE", FieldType.STRING, 20);

        pnd_W_4060_Entry_Code__R_Field_5 = localVariables.newGroupInRecord("pnd_W_4060_Entry_Code__R_Field_5", "REDEFINE", pnd_W_4060_Entry_Code);
        pnd_W_4060_Entry_Code_Pnd_W_4060_State_Code = pnd_W_4060_Entry_Code__R_Field_5.newFieldInGroup("pnd_W_4060_Entry_Code_Pnd_W_4060_State_Code", 
            "#W-4060-STATE-CODE", FieldType.STRING, 2);
        pnd_W_4060_Entry_Code_Pnd_W_4060_Prdct_Code = pnd_W_4060_Entry_Code__R_Field_5.newFieldInGroup("pnd_W_4060_Entry_Code_Pnd_W_4060_Prdct_Code", 
            "#W-4060-PRDCT-CODE", FieldType.STRING, 10);
        pnd_W_4060_Entry_Dscrptn_Txt = localVariables.newFieldInRecord("pnd_W_4060_Entry_Dscrptn_Txt", "#W-4060-ENTRY-DSCRPTN-TXT", FieldType.STRING, 
            60);

        pnd_W_4060_Entry_Dscrptn_Txt__R_Field_6 = localVariables.newGroupInRecord("pnd_W_4060_Entry_Dscrptn_Txt__R_Field_6", "REDEFINE", pnd_W_4060_Entry_Dscrptn_Txt);
        pnd_W_4060_Entry_Dscrptn_Txt_Pnd_W_4060_Approval_Date_1 = pnd_W_4060_Entry_Dscrptn_Txt__R_Field_6.newFieldInGroup("pnd_W_4060_Entry_Dscrptn_Txt_Pnd_W_4060_Approval_Date_1", 
            "#W-4060-APPROVAL-DATE-1", FieldType.NUMERIC, 8);
        pnd_W_4060_Entry_Dscrptn_Txt_Pnd_W_4060_Approval_Date_2 = pnd_W_4060_Entry_Dscrptn_Txt__R_Field_6.newFieldInGroup("pnd_W_4060_Entry_Dscrptn_Txt_Pnd_W_4060_Approval_Date_2", 
            "#W-4060-APPROVAL-DATE-2", FieldType.NUMERIC, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_J2 = localVariables.newFieldInRecord("pnd_J2", "#J2", FieldType.PACKED_DECIMAL, 3);
        pnd_Ia_State_Code = localVariables.newFieldInRecord("pnd_Ia_State_Code", "#IA-STATE-CODE", FieldType.STRING, 3);

        pnd_Ia_State_Code__R_Field_7 = localVariables.newGroupInRecord("pnd_Ia_State_Code__R_Field_7", "REDEFINE", pnd_Ia_State_Code);
        pnd_Ia_State_Code_Pnd_Ia_State_Code_1_1 = pnd_Ia_State_Code__R_Field_7.newFieldInGroup("pnd_Ia_State_Code_Pnd_Ia_State_Code_1_1", "#IA-STATE-CODE-1-1", 
            FieldType.STRING, 1);
        pnd_Ia_State_Code_Pnd_Ia_State_Code_2_3 = pnd_Ia_State_Code__R_Field_7.newFieldInGroup("pnd_Ia_State_Code_Pnd_Ia_State_Code_2_3", "#IA-STATE-CODE-2-3", 
            FieldType.STRING, 2);
        pnd_State_Code = localVariables.newFieldInRecord("pnd_State_Code", "#STATE-CODE", FieldType.STRING, 2);
        pnd_State_Nbr_A = localVariables.newFieldInRecord("pnd_State_Nbr_A", "#STATE-NBR-A", FieldType.STRING, 2);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        pnd_Debug_1 = localVariables.newFieldInRecord("pnd_Debug_1", "#DEBUG-1", FieldType.BOOLEAN, 1);
        pnd_State_Cde_Coll = localVariables.newFieldArrayInRecord("pnd_State_Cde_Coll", "#STATE-CDE-COLL", FieldType.STRING, 2, new DbsArrayController(1, 
            99));
        pnd_State_Cde_Coll_1st = localVariables.newFieldInRecord("pnd_State_Cde_Coll_1st", "#STATE-CDE-COLL-1ST", FieldType.STRING, 2);
        pnd_California = localVariables.newFieldInRecord("pnd_California", "#CALIFORNIA", FieldType.BOOLEAN, 1);
        pnd_State_Found = localVariables.newFieldInRecord("pnd_State_Found", "#STATE-FOUND", FieldType.BOOLEAN, 1);
        pnd_M = localVariables.newFieldInRecord("pnd_M", "#M", FieldType.NUMERIC, 3);
        pnd_N = localVariables.newFieldInRecord("pnd_N", "#N", FieldType.NUMERIC, 3);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.NUMERIC, 3);
        pnd_Prev_Source = localVariables.newFieldInRecord("pnd_Prev_Source", "#PREV-SOURCE", FieldType.STRING, 6);
        pnd_California_Apprv = localVariables.newFieldInRecord("pnd_California_Apprv", "#CALIFORNIA-APPRV", FieldType.BOOLEAN, 1);
        pnd_Np_Super_De_1 = localVariables.newFieldInRecord("pnd_Np_Super_De_1", "#NP-SUPER-DE-1", FieldType.STRING, 9);

        pnd_Np_Super_De_1__R_Field_8 = localVariables.newGroupInRecord("pnd_Np_Super_De_1__R_Field_8", "REDEFINE", pnd_Np_Super_De_1);
        pnd_Np_Super_De_1_Pnd_Aa_Physical_Record_Cd = pnd_Np_Super_De_1__R_Field_8.newFieldInGroup("pnd_Np_Super_De_1_Pnd_Aa_Physical_Record_Cd", "#AA-PHYSICAL-RECORD-CD", 
            FieldType.STRING, 1);
        pnd_Np_Super_De_1_Pnd_Aa_Tiaa_Contract_No = pnd_Np_Super_De_1__R_Field_8.newFieldInGroup("pnd_Np_Super_De_1_Pnd_Aa_Tiaa_Contract_No", "#AA-TIAA-CONTRACT-NO", 
            FieldType.STRING, 8);
        pnd_Different_Coll_State_Found = localVariables.newFieldInRecord("pnd_Different_Coll_State_Found", "#DIFFERENT-COLL-STATE-FOUND", FieldType.BOOLEAN, 
            1);
        pnd_Found_Non_California = localVariables.newFieldInRecord("pnd_Found_Non_California", "#FOUND-NON-CALIFORNIA", FieldType.BOOLEAN, 1);
        pnd_Found_State = localVariables.newFieldInRecord("pnd_Found_State", "#FOUND-STATE", FieldType.BOOLEAN, 1);
        pnd_Hold_State = localVariables.newFieldInRecord("pnd_Hold_State", "#HOLD-STATE", FieldType.STRING, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 5);
        pnd_Perform_Approval = localVariables.newFieldInRecord("pnd_Perform_Approval", "#PERFORM-APPROVAL", FieldType.BOOLEAN, 1);

        pnd_Cis_Tiaa_Cntrct_Nbr_Input = localVariables.newGroupArrayInRecord("pnd_Cis_Tiaa_Cntrct_Nbr_Input", "#CIS-TIAA-CNTRCT-NBR-INPUT", new DbsArrayController(1, 
            20));
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data = pnd_Cis_Tiaa_Cntrct_Nbr_Input.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data", 
            "#CIS-TIAA-CNTRCT-NBR-INPUT-DATA", FieldType.STRING, 10);

        pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_9 = pnd_Cis_Tiaa_Cntrct_Nbr_Input.newGroupInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_9", "REDEFINE", 
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data);
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input = pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_9.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input", 
            "#FROM-STATE-INPUT", FieldType.STRING, 2);
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input = pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_9.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input", 
            "#FROM-APPROVAL-IND-INPUT", FieldType.STRING, 1);
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_To_State_Input = pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_9.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_To_State_Input", 
            "#TO-STATE-INPUT", FieldType.STRING, 2);
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_To_Approval_Ind_Input = pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_9.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_To_Approval_Ind_Input", 
            "#TO-APPROVAL-IND-INPUT", FieldType.STRING, 1);
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_State_Input = pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_9.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_State_Input", 
            "#RES-STATE-INPUT", FieldType.STRING, 2);
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_Approval_Ind_Input = pnd_Cis_Tiaa_Cntrct_Nbr_Input__R_Field_9.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Res_Approval_Ind_Input", 
            "#RES-APPROVAL-IND-INPUT", FieldType.STRING, 1);

        pnd_Cis_Tiaa_Cntrct_Nbr_Hold = localVariables.newGroupArrayInRecord("pnd_Cis_Tiaa_Cntrct_Nbr_Hold", "#CIS-TIAA-CNTRCT-NBR-HOLD", new DbsArrayController(1, 
            20));
        pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Data = pnd_Cis_Tiaa_Cntrct_Nbr_Hold.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Data", 
            "#CIS-TIAA-CNTRCT-NBR-HOLD-DATA", FieldType.STRING, 10);

        pnd_Cis_Tiaa_Cntrct_Nbr_Hold__R_Field_10 = pnd_Cis_Tiaa_Cntrct_Nbr_Hold.newGroupInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Hold__R_Field_10", "REDEFINE", 
            pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Data);
        pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State1_Hold = pnd_Cis_Tiaa_Cntrct_Nbr_Hold__R_Field_10.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State1_Hold", 
            "#STATE1-HOLD", FieldType.STRING, 2);
        pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind1_Hold = pnd_Cis_Tiaa_Cntrct_Nbr_Hold__R_Field_10.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind1_Hold", 
            "#APPROVAL-IND1-HOLD", FieldType.STRING, 1);
        pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State2_Hold = pnd_Cis_Tiaa_Cntrct_Nbr_Hold__R_Field_10.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State2_Hold", 
            "#STATE2-HOLD", FieldType.STRING, 2);
        pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind2_Hold = pnd_Cis_Tiaa_Cntrct_Nbr_Hold__R_Field_10.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind2_Hold", 
            "#APPROVAL-IND2-HOLD", FieldType.STRING, 1);
        pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State3_Hold = pnd_Cis_Tiaa_Cntrct_Nbr_Hold__R_Field_10.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State3_Hold", 
            "#STATE3-HOLD", FieldType.STRING, 2);
        pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind3_Hold = pnd_Cis_Tiaa_Cntrct_Nbr_Hold__R_Field_10.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind3_Hold", 
            "#APPROVAL-IND3-HOLD", FieldType.STRING, 1);

        pnd_Cis_Tiaa_Cntrct_Nbr_Res = localVariables.newGroupInRecord("pnd_Cis_Tiaa_Cntrct_Nbr_Res", "#CIS-TIAA-CNTRCT-NBR-RES");
        pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Cis_Tiaa_Cntrct_Nbr_Res_Data = pnd_Cis_Tiaa_Cntrct_Nbr_Res.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Cis_Tiaa_Cntrct_Nbr_Res_Data", 
            "#CIS-TIAA-CNTRCT-NBR-RES-DATA", FieldType.STRING, 10);

        pnd_Cis_Tiaa_Cntrct_Nbr_Res__R_Field_11 = pnd_Cis_Tiaa_Cntrct_Nbr_Res.newGroupInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Res__R_Field_11", "REDEFINE", pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Cis_Tiaa_Cntrct_Nbr_Res_Data);
        pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State1_Hold = pnd_Cis_Tiaa_Cntrct_Nbr_Res__R_Field_11.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State1_Hold", 
            "#RES-STATE1-HOLD", FieldType.STRING, 2);
        pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind1_Hold = pnd_Cis_Tiaa_Cntrct_Nbr_Res__R_Field_11.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind1_Hold", 
            "#RES-APPROVAL-IND1-HOLD", FieldType.STRING, 1);
        pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State2_Hold = pnd_Cis_Tiaa_Cntrct_Nbr_Res__R_Field_11.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State2_Hold", 
            "#RES-STATE2-HOLD", FieldType.STRING, 2);
        pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind2_Hold = pnd_Cis_Tiaa_Cntrct_Nbr_Res__R_Field_11.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind2_Hold", 
            "#RES-APPROVAL-IND2-HOLD", FieldType.STRING, 1);
        pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State3_Hold = pnd_Cis_Tiaa_Cntrct_Nbr_Res__R_Field_11.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State3_Hold", 
            "#RES-STATE3-HOLD", FieldType.STRING, 2);
        pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind3_Hold = pnd_Cis_Tiaa_Cntrct_Nbr_Res__R_Field_11.newFieldInGroup("pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind3_Hold", 
            "#RES-APPROVAL-IND3-HOLD", FieldType.STRING, 1);
        pnd_Issue_State_Found = localVariables.newFieldInRecord("pnd_Issue_State_Found", "#ISSUE-STATE-FOUND", FieldType.BOOLEAN, 1);
        pnd_Return = localVariables.newFieldInRecord("pnd_Return", "#RETURN", FieldType.BOOLEAN, 1);
        pnd_Return_Single = localVariables.newFieldInRecord("pnd_Return_Single", "#RETURN-SINGLE", FieldType.BOOLEAN, 1);
        pnd_Return_Contract = localVariables.newFieldInRecord("pnd_Return_Contract", "#RETURN-CONTRACT", FieldType.BOOLEAN, 1);
        pnd_Return_Multi = localVariables.newFieldInRecord("pnd_Return_Multi", "#RETURN-MULTI", FieldType.BOOLEAN, 1);
        pnd_Return_Annual = localVariables.newFieldInRecord("pnd_Return_Annual", "#RETURN-ANNUAL", FieldType.BOOLEAN, 1);
        pnd_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Key", "#CNTRCT-PAYEE-KEY", FieldType.STRING, 12);

        pnd_Cntrct_Payee_Key__R_Field_12 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Key__R_Field_12", "REDEFINE", pnd_Cntrct_Payee_Key);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Cntrct_Payee_Key__R_Field_12.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Ppcn_Nbr", 
            "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cntrct_Payee_Key__R_Field_12.newFieldInGroup("pnd_Cntrct_Payee_Key_Pnd_Cntrct_Part_Payee_Cde", 
            "#CNTRCT-PART-PAYEE-CDE", FieldType.NUMERIC, 2);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCisl4070.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Contract_Tbl_Max.setInitialValue(20);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Cisn4072() throws Exception
    {
        super("Cisn4072");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* *----------------------------------------------------------------------
        //*                             MAINLINE
        //* *----------------------------------------------------------------------
        //*  OR  /* VCR
        if (condition(Global.getINIT_USER().equals("KAKADIA") || Global.getINIT_USER().equals("RODGER")))                                                                 //Natural: IF *INIT-USER = 'KAKADIA' OR = 'RODGER'
        {
            pnd_Debug_1.setValue(true);                                                                                                                                   //Natural: ASSIGN #DEBUG-1 := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_L.setValue(1);                                                                                                                                                //Natural: ASSIGN #L := 1
        //* *IF *DEVICE NE 'BATCH'
        //* *  ESCAPE ROUTINE
        //* *END-IF
        pdaCisa4070.getCisa4070_Pnd_Cis_Org_Issue_St().setValue(pdaCisa1000.getCisa1000_Pnd_Cis_Org_Issue_St());                                                          //Natural: ASSIGN CISA4070.#CIS-ORG-ISSUE-ST := CISA1000.#CIS-ORG-ISSUE-ST
        pdaCisa4070.getCisa4070_Pnd_Cis_Residence_Issue_St().setValue(pdaCisa1000.getCisa1000_Pnd_Cis_Residence_Issue_St());                                              //Natural: ASSIGN CISA4070.#CIS-RESIDENCE-ISSUE-ST := CISA1000.#CIS-RESIDENCE-ISSUE-ST
        pdaCisa4070.getCisa4070_Pnd_Cis_Issue_Dt().setValue(pdaCisa1000.getCisa1000_Pnd_Cis_Issue_Dt());                                                                  //Natural: ASSIGN CISA4070.#CIS-ISSUE-DT := CISA1000.#CIS-ISSUE-DT
        pdaCisa4070.getCisa4070_Pnd_Cis_Requestor().setValue(pdaCisa1000.getCisa1000_Pnd_Cis_Requestor());                                                                //Natural: ASSIGN CISA4070.#CIS-REQUESTOR := CISA1000.#CIS-REQUESTOR
        if (condition(pnd_Debug_1.getBoolean()))                                                                                                                          //Natural: IF #DEBUG-1
        {
            getReports().eject(0, true);                                                                                                                                  //Natural: EJECT
            getReports().write(0, NEWLINE,Global.getPROGRAM(),"(0740) ENTRY ..........................",NEWLINE,"=",pnd_Debug_1,NEWLINE,"=",pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*"), //Natural: WRITE / *PROGRAM '(0740) ENTRY ..........................' / '=' #DEBUG-1 / '=' CISA4070.#CIS-TIAA-CNTRCT-NBR ( * ) / '=' CISA4070.#CIS-PRODUCT-CDE / '=' CISA4070.#CIS-RESIDENCE-ISSUE-ST / '=' CISA4070.#CIS-ISSUE-DT
                NEWLINE,"=",pdaCisa4070.getCisa4070_Pnd_Cis_Product_Cde(),NEWLINE,"=",pdaCisa4070.getCisa4070_Pnd_Cis_Residence_Issue_St(),NEWLINE,"=",pdaCisa4070.getCisa4070_Pnd_Cis_Issue_Dt());
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  N=NOT APPROVED
        pdaCisa4070.getCisa4070_Pnd_Cis_Msg_Text().reset();                                                                                                               //Natural: RESET CISA4070.#CIS-MSG-TEXT CISA4070.#CIS-MSG-NUMBER
        pdaCisa4070.getCisa4070_Pnd_Cis_Msg_Number().reset();
        pdaCisa4070.getCisa4070_Pnd_Cis_Cntrct_Apprvl_Ind().setValue("N");                                                                                                //Natural: ASSIGN CISA4070.#CIS-CNTRCT-APPRVL-IND := 'N'
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 #CONTRACT-TBL-MAX
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Contract_Tbl_Max)); pnd_I.nadd(1))
        {
            if (condition(pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(pnd_I).equals(" ")))                                                                //Natural: IF #CONTRACT-TBL-ISSUE-STATE-CODE ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Contract_Tbl_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #CONTRACT-TBL-CNT
            //* *  MOVE CISA4070.#CIS-TIAA-CNTRCT-NBR(#I) TO
            //* *    #CONTRACT-TBL-CONTRACT-NO(#I)
            pnd_Temp_Date_N8_Pnd_Temp_Date_Ccyymm.reset();                                                                                                                //Natural: RESET #TEMP-DATE-CCYYMM #TEMP-DATE-N8
            pnd_Temp_Date_N8.reset();
            pnd_Temp_Date_N8.setValue(pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_Date.getValue(pnd_I));                                                                      //Natural: MOVE #CONTRACT-TBL-ISSUE-DATE ( #I ) TO #TEMP-DATE-N8
            pnd_Temp_Date_N8_Pnd_Temp_Date_Ccyymmdd_Dd.setValue("01");                                                                                                    //Natural: MOVE '01' TO #TEMP-DATE-CCYYMMDD-DD
            pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_Date.getValue(pnd_I).setValue(pnd_Temp_Date_N8);                                                                      //Natural: MOVE #TEMP-DATE-N8 TO #CONTRACT-TBL-ISSUE-DATE ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        short decideConditionsMet413 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #CONTRACT-TBL-CNT;//Natural: VALUE 0
        if (condition((pnd_Contract_Tbl_Cnt.equals(0))))
        {
            decideConditionsMet413++;
            if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                            //Natural: IF *DEVICE EQ 'BATCH'
            {
                pdaCisa4070.getCisa4070_Pnd_Cis_Msg_Number().setValue("0001");                                                                                            //Natural: MOVE '0001' TO CISA4070.#CIS-MSG-NUMBER
                pdaCisa4070.getCisa4070_Pnd_Cis_Msg_Text().setValue(DbsUtil.compress(Global.getPROGRAM(), "(1030) NO CONTRACTS PASSED"));                                 //Natural: COMPRESS *PROGRAM '(1030) NO CONTRACTS PASSED' INTO CISA4070.#CIS-MSG-TEXT
                getReports().write(0, Global.getPROGRAM(),"=",pdaCisa4070.getCisa4070_Pnd_Cis_Msg_Text());                                                                //Natural: WRITE *PROGRAM '=' CISA4070.#CIS-MSG-TEXT
                if (Global.isEscape()) return;
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 1
        else if (condition((pnd_Contract_Tbl_Cnt.equals(1))))
        {
            decideConditionsMet413++;
            pnd_I.setValue(1);                                                                                                                                            //Natural: ASSIGN #I := 1
            ignore();
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM SORT-CONTRACT-TBL
            sub_Sort_Contract_Tbl();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Debug_1.getBoolean()))                                                                                                                          //Natural: IF #DEBUG-1
        {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CONTRACT-TBL
            sub_Display_Contract_Tbl();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *IF CISA4070.#CIS-REQUESTOR EQ 'IA'
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*").reset();                                                                           //Natural: RESET #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * )
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(pdaCisa4070.getCisa4070_Pnd_Cis_Residence_Issue_St());                                    //Natural: MOVE CISA4070.#CIS-RESIDENCE-ISSUE-ST TO #FROM-STATE-INPUT ( 1 )
                                                                                                                                                                          //Natural: PERFORM GET-STATE-INFO-RES
        sub_Get_State_Info_Res();
        if (condition(Global.isEscape())) {return;}
        //* *END-IF
        //*  060706
        if (condition(! (pnd_Found_State.getBoolean()) && pdaCisa4070.getCisa4070_Pnd_Cis_Residence_Issue_St().equals("CA") && DbsUtil.maskMatches(pdaCisa4070.getCisa4070_Pnd_Cis_Requestor(), //Natural: IF NOT #FOUND-STATE AND CISA4070.#CIS-RESIDENCE-ISSUE-ST EQ 'CA' AND CISA4070.#CIS-REQUESTOR EQ MASK ( 'MDO' )
            "'MDO'")))
        {
            pnd_California.setValue(true);                                                                                                                                //Natural: ASSIGN #CALIFORNIA := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Debug_1.getBoolean()))                                                                                                                          //Natural: IF #DEBUG-1
        {
            getReports().write(0, Global.getPROGRAM(),"CALIFORNIA G=",pnd_California);                                                                                    //Natural: WRITE *PROGRAM 'CALIFORNIA ''=' #CALIFORNIA
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 #CONTRACT-TBL-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Contract_Tbl_Cnt)); pnd_I.nadd(1))
        {
            //*  REJECTED
            if (condition(pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_Date.getValue(pnd_I).equals(99999999)))                                                                 //Natural: IF #CONTRACT-TBL-ISSUE-DATE ( #I ) = 99999999
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*     DECIDE ON FIRST VALUE OF #CONTRACT-TBL-CONTRACT-NO(#I)  /* VCR
            //*         VALUE 'C3032574'  #CIS-PRODUCT-CDE := 'IA'  /* 'RA'
            //*         VALUE 'C3032574'  #CIS-PRODUCT-CDE := 'RA'  /* 'RA'
            //*         VALUE 'K0601530'  #CIS-PRODUCT-CDE := 'IA'  /* 'SRA'
            //*         VALUE 'K0601530'  #CIS-PRODUCT-CDE := 'SRA' /* 'SRA'
            //*         VALUE '25976473'  #CIS-PRODUCT-CDE := 'IA'  /* 'GRA'
            //*         VALUE '25976473'  #CIS-PRODUCT-CDE := 'GRA' /* 'GRA'
            //*         NONE              #CIS-PRODUCT-CDE := 'IA'  /*
            //*     END-DECIDE
            if (condition(! (pnd_California.getBoolean()) || pdaCisa4070.getCisa4070_Pnd_Cis_Requestor().equals("IA")))                                                   //Natural: IF NOT #CALIFORNIA OR CISA4070.#CIS-REQUESTOR EQ 'IA'
            {
                if (condition(pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(pnd_I).equals(" ")))                                                            //Natural: IF #CONTRACT-TBL-ISSUE-STATE-CODE ( #I ) EQ ' '
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  060706
                if (condition(pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(pnd_I).equals("CA") && DbsUtil.maskMatches(pdaCisa4070.getCisa4070_Pnd_Cis_Requestor(), //Natural: IF #CONTRACT-TBL-ISSUE-STATE-CODE ( #I ) EQ 'CA' AND CISA4070.#CIS-REQUESTOR EQ MASK ( 'MDO' )
                    "'MDO'")))
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Found_Non_California.setValue(true);                                                                                                                  //Natural: ASSIGN #FOUND-NON-CALIFORNIA := TRUE
                pnd_Issue_State_Found.setValue(true);                                                                                                                     //Natural: ASSIGN #ISSUE-STATE-FOUND := TRUE
                if (condition(pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(pnd_I).notEquals(" ")))                                                         //Natural: IF #CONTRACT-TBL-ISSUE-STATE-CODE ( #I ) NE ' '
                {
                    pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*").reset();                                                               //Natural: RESET #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * )
                    //*  START RJ01
                    //*  #CONTRACT-TBL-ISSUE-STATE-CODE(#I) := CISA1000.#CIS-RESIDENCE-ISSUE-ST
                    //*  END RJ01
                    pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(pnd_I));          //Natural: MOVE #CONTRACT-TBL-ISSUE-STATE-CODE ( #I ) TO #FROM-STATE-INPUT ( 1 )
                                                                                                                                                                          //Natural: PERFORM GET-STATE-INFO
                    sub_Get_State_Info();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  GET RESIDENCY STATE FOR REAL ESTATE
        if (condition(pdaCisa4070.getCisa4070_Pnd_Cis_Requestor().equals("IA") && pnd_Issue_State_Found.getBoolean()))                                                    //Natural: IF CISA4070.#CIS-REQUESTOR EQ 'IA' AND #ISSUE-STATE-FOUND
        {
                                                                                                                                                                          //Natural: PERFORM RETURN-STATE-INFO
            sub_Return_State_Info();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Return.getBoolean() && pnd_Return_Multi.getBoolean()))                                                                                      //Natural: IF #RETURN AND #RETURN-MULTI
            {
                pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").reset();                                                                                  //Natural: RESET CISA4070.#CIS-TIAA-CNTRCT-NBR ( * )
                pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*")); //Natural: ASSIGN CISA4070.#CIS-TIAA-CNTRCT-NBR ( * ) := #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * )
                pnd_Return_Contract.setValue(true);                                                                                                                       //Natural: ASSIGN #RETURN-CONTRACT := TRUE
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //* *
            //* * IF APPROVAL IS 'M' THIS IS MULTIFUNDED CONTRACT THEREFORE TIAA-ACCESS
            //*   IS ALSO APPROVED, M IS USED TO MANIPULATE BOTH
            if (condition(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind2_Hold.equals("M")))                                                                            //Natural: IF #RES-APPROVAL-IND2-HOLD EQ 'M'
            {
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State1_Hold);                                 //Natural: ASSIGN #FROM-STATE-INPUT ( 1 ) := #RES-STATE1-HOLD
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind1_Hold);                   //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 1 ) := #RES-APPROVAL-IND1-HOLD
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State2_Hold);                                 //Natural: ASSIGN #FROM-STATE-INPUT ( 2 ) := #RES-STATE2-HOLD
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind2_Hold);                   //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 2 ) := #RES-APPROVAL-IND2-HOLD
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State3_Hold);                                 //Natural: ASSIGN #FROM-STATE-INPUT ( 3 ) := #RES-STATE3-HOLD
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind3_Hold);                   //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 3 ) := #RES-APPROVAL-IND3-HOLD
                pnd_Return_Contract.setValue(true);                                                                                                                       //Natural: ASSIGN #RETURN-CONTRACT := TRUE
                pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").reset();                                                                                  //Natural: RESET CISA4070.#CIS-TIAA-CNTRCT-NBR ( * )
                pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*")); //Natural: ASSIGN CISA4070.#CIS-TIAA-CNTRCT-NBR ( * ) := #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * )
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM RETURN-STATE-INFO
                sub_Return_State_Info();
                if (condition(Global.isEscape())) {return;}
                if (condition(pnd_Return.getBoolean() && pnd_Return_Annual.getBoolean()))                                                                                 //Natural: IF #RETURN AND #RETURN-ANNUAL
                {
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Return.getBoolean() && pnd_Return_Single.getBoolean()))                                                                                 //Natural: IF #RETURN AND #RETURN-SINGLE
                {
                    if (condition(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind2_Hold.equals("A")))                                                                    //Natural: IF #RES-APPROVAL-IND2-HOLD EQ 'A'
                    {
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State1_Hold);                         //Natural: ASSIGN #FROM-STATE-INPUT ( 1 ) := #RES-STATE1-HOLD
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind1_Hold);           //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 1 ) := #RES-APPROVAL-IND1-HOLD
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State2_Hold);                         //Natural: ASSIGN #FROM-STATE-INPUT ( 2 ) := #RES-STATE2-HOLD
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind2_Hold);           //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 2 ) := #RES-APPROVAL-IND2-HOLD
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State3_Hold);                         //Natural: ASSIGN #FROM-STATE-INPUT ( 3 ) := #RES-STATE3-HOLD
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind3_Hold);           //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 3 ) := #RES-APPROVAL-IND3-HOLD
                        pnd_Return_Contract.setValue(true);                                                                                                               //Natural: ASSIGN #RETURN-CONTRACT := TRUE
                        pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").reset();                                                                          //Natural: RESET CISA4070.#CIS-TIAA-CNTRCT-NBR ( * )
                        pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*")); //Natural: ASSIGN CISA4070.#CIS-TIAA-CNTRCT-NBR ( * ) := #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * )
                        if (condition(true)) return;                                                                                                                      //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM RETURN-STATE-INFO
                        sub_Return_State_Info();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(true)) return;                                                                                                                      //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  060706
        if (condition(DbsUtil.maskMatches(pdaCisa4070.getCisa4070_Pnd_Cis_Requestor(),"'MDO'")))                                                                          //Natural: IF CISA4070.#CIS-REQUESTOR EQ MASK ( 'MDO' )
        {
            //*   IF #FOUND-NON-CALIFORNIA
                                                                                                                                                                          //Natural: PERFORM RETURN-STATE-INFO-MDO
            sub_Return_State_Info_Mdo();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Return.getBoolean() && ! (pnd_Return_Single.getBoolean())))                                                                                 //Natural: IF #RETURN AND NOT #RETURN-SINGLE
            {
                pnd_Return_Contract.setValue(true);                                                                                                                       //Natural: ASSIGN #RETURN-CONTRACT := TRUE
                if (condition(pnd_Debug_1.getBoolean()))                                                                                                                  //Natural: IF #DEBUG-1
                {
                    getReports().write(0, Global.getPROGRAM(),"DDDDDDDDDDDDD","=",pnd_Return_Contract);                                                                   //Natural: WRITE *PROGRAM 'DDDDDDDDDDDDD' '=' #RETURN-CONTRACT
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM RETURN-CONTRACT-APPROVED
                sub_Return_Contract_Approved();
                if (condition(Global.isEscape())) {return;}
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            //*   END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Return_Contract.getBoolean())))                                                                                                              //Natural: IF NOT #RETURN-CONTRACT
        {
            if (condition(pdaCisa4070.getCisa4070_Pnd_Cis_Requestor().equals("IA")))                                                                                      //Natural: IF CISA4070.#CIS-REQUESTOR EQ 'IA'
            {
                if (condition(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind2_Hold.equals("M")))                                                                        //Natural: IF #RES-APPROVAL-IND2-HOLD EQ 'M'
                {
                    pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State1_Hold);                             //Natural: ASSIGN #FROM-STATE-INPUT ( 1 ) := #RES-STATE1-HOLD
                    pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind1_Hold);               //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 1 ) := #RES-APPROVAL-IND1-HOLD
                    pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State2_Hold);                             //Natural: ASSIGN #FROM-STATE-INPUT ( 2 ) := #RES-STATE2-HOLD
                    pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind2_Hold);               //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 2 ) := #RES-APPROVAL-IND2-HOLD
                    pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State3_Hold);                             //Natural: ASSIGN #FROM-STATE-INPUT ( 3 ) := #RES-STATE3-HOLD
                    pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind3_Hold);               //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 3 ) := #RES-APPROVAL-IND3-HOLD
                    if (condition(pnd_Debug_1.getBoolean()))                                                                                                              //Natural: IF #DEBUG-1
                    {
                        getReports().write(0, "=",pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue(1,":",3),"return resede ddddd");              //Natural: WRITE '=' #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( 1:3 ) 'return resede ddddd'
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State1_Hold.getValue(1).notEquals(" ")))                                                               //Natural: IF #STATE1-HOLD ( 1 ) NE ' '
                    {
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State1_Hold.getValue(1));                //Natural: ASSIGN #FROM-STATE-INPUT ( 1 ) := #STATE1-HOLD ( 1 )
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind1_Hold.getValue(1));  //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 1 ) := #APPROVAL-IND1-HOLD ( 1 )
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State1_Hold.getValue(2));                //Natural: ASSIGN #FROM-STATE-INPUT ( 2 ) := #STATE1-HOLD ( 2 )
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind1_Hold.getValue(2));  //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 2 ) := #APPROVAL-IND1-HOLD ( 2 )
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State1_Hold.getValue(3));                //Natural: ASSIGN #FROM-STATE-INPUT ( 3 ) := #STATE1-HOLD ( 3 )
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind1_Hold.getValue(3));  //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 3 ) := #APPROVAL-IND1-HOLD ( 3 )
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(4).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State1_Hold.getValue(4));                //Natural: ASSIGN #FROM-STATE-INPUT ( 4 ) := #STATE1-HOLD ( 4 )
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(4).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind1_Hold.getValue(4));  //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 4 ) := #APPROVAL-IND1-HOLD ( 4 )
                        if (condition(pnd_Debug_1.getBoolean()))                                                                                                          //Natural: IF #DEBUG-1
                        {
                            getReports().write(0, "=",pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue(1,":",4),"return oldest");                //Natural: WRITE '=' #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( 1:4 ) 'return oldest'
                            if (Global.isEscape()) return;
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State1_Hold);                         //Natural: ASSIGN #FROM-STATE-INPUT ( 1 ) := #RES-STATE1-HOLD
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind1_Hold);           //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 1 ) := #RES-APPROVAL-IND1-HOLD
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State2_Hold);                         //Natural: ASSIGN #FROM-STATE-INPUT ( 2 ) := #RES-STATE2-HOLD
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind2_Hold);           //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 2 ) := #RES-APPROVAL-IND2-HOLD
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(4).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State3_Hold);                         //Natural: ASSIGN #FROM-STATE-INPUT ( 4 ) := #RES-STATE3-HOLD
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(4).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind3_Hold);           //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 4 ) := #RES-APPROVAL-IND3-HOLD
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").reset();                                                                                  //Natural: RESET CISA4070.#CIS-TIAA-CNTRCT-NBR ( * )
                pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*")); //Natural: ASSIGN CISA4070.#CIS-TIAA-CNTRCT-NBR ( * ) := #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * )
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  060706
            if (condition(DbsUtil.maskMatches(pdaCisa4070.getCisa4070_Pnd_Cis_Requestor(),"'MDO'")))                                                                      //Natural: IF CISA4070.#CIS-REQUESTOR EQ MASK ( 'MDO' )
            {
                if (condition(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind1_Hold.equals("A")))                                                                        //Natural: IF #RES-APPROVAL-IND1-HOLD EQ 'A'
                {
                    pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State1_Hold);                             //Natural: ASSIGN #FROM-STATE-INPUT ( 1 ) := #RES-STATE1-HOLD
                    pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind1_Hold);               //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 1 ) := #RES-APPROVAL-IND1-HOLD
                    pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State2_Hold);                             //Natural: ASSIGN #FROM-STATE-INPUT ( 2 ) := #RES-STATE2-HOLD
                    pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind2_Hold);               //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 2 ) := #RES-APPROVAL-IND2-HOLD
                    pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State3_Hold);                             //Natural: ASSIGN #FROM-STATE-INPUT ( 3 ) := #RES-STATE3-HOLD
                    pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind3_Hold);               //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 3 ) := #RES-APPROVAL-IND3-HOLD
                    //* *  PERFORM RETURN-STATE-INFO-MDO
                    //*    IF #RETURN AND NOT #RETURN-SINGLE
                                                                                                                                                                          //Natural: PERFORM RETURN-CONTRACT-APPROVED
                    sub_Return_Contract_Approved();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM RETURN-CONTRACT-NOT-APPROVED
                sub_Return_Contract_Not_Approved();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*    END OF MAINLINE
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REJECT-CONTRACT
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOOKUP-STATE-CODE-ARRAY
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SORT-CONTRACT-TBL
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-CONTRACT-TBL
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-APPROVAL-DATE
        //*  ELSE
        //*    #CIS-MSG-TEXT              :=  #O-4060-RETURN-MSG
        //*    #CIS-MSG-NUMBER            :=  #O-4060-RETURN-CODE
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RETURN-CONTRACT-APPROVED
        //* *----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RETURN-CONTRACT-NOT-APPROVED
        //* *--------------------------------*
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATE-INFO
        //* *--------------------------------*
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATE-INFO-RES
        //* *--------------------------------*
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RETURN-STATE-INFO
        //*  RESET CISA4070.#CIS-TIAA-CNTRCT-NBR(*)
        //*  CISA4070.#CIS-TIAA-CNTRCT-NBR(*) :=
        //*    #CIS-TIAA-CNTRCT-NBR-INPUT-DATA(*)
        //* *--------------------------------*
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RETURN-STATE-INFO-MDO
        //*  RESET CISA4070.#CIS-TIAA-CNTRCT-NBR(*)
        //*  CISA4070.#CIS-TIAA-CNTRCT-NBR(*) :=
        //*    #CIS-TIAA-CNTRCT-NBR-INPUT-DATA(*)
    }
    private void sub_Reject_Contract() throws Exception                                                                                                                   //Natural: REJECT-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        //*   ASSIGN DUMMY VALUES TO REJECTED CONTRACTS
        pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_Date.getValue(pnd_I).setValue(99999999);                                                                                  //Natural: MOVE 99999999 TO #CONTRACT-TBL-ISSUE-DATE ( #I )
        pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(pnd_I).setValue(pdaCisa4070.getCisa4070_Pnd_Cis_Residence_Issue_St());                                //Natural: MOVE CISA4070.#CIS-RESIDENCE-ISSUE-ST TO #CONTRACT-TBL-ISSUE-STATE-CODE ( #I )
        //*  REJECT-CONTRACT
    }
    private void sub_Lookup_State_Code_Array() throws Exception                                                                                                           //Natural: LOOKUP-STATE-CODE-ARRAY
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pnd_State_Code.reset();                                                                                                                                           //Natural: RESET #STATE-CODE #STATE-FOUND
        pnd_State_Found.reset();
        FOR03:                                                                                                                                                            //Natural: FOR #J = 1 #STATE-TBL-MAX
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(ldaCisl4070.getCisl4070_Pnd_State_Tbl_Max())); pnd_J.nadd(1))
        {
            if (condition(pnd_State_Nbr_A.equals(ldaCisl4070.getCisl4070_Pnd_State_Tbl_Nbr_A().getValue(pnd_J))))                                                         //Natural: IF #STATE-NBR-A = #STATE-TBL-NBR-A ( #J )
            {
                pnd_State_Code.setValue(ldaCisl4070.getCisl4070_Pnd_State_Tbl_Code().getValue(pnd_J));                                                                    //Natural: MOVE #STATE-TBL-CODE ( #J ) TO #STATE-CODE
                pnd_State_Found.setValue(true);                                                                                                                           //Natural: ASSIGN #STATE-FOUND := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  LOOKUP-STATE-CODE-ARRAY
    }
    private void sub_Sort_Contract_Tbl() throws Exception                                                                                                                 //Natural: SORT-CONTRACT-TBL
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pnd_I2.compute(new ComputeParameters(false, pnd_I2), pnd_Contract_Tbl_Cnt.subtract(1));                                                                           //Natural: COMPUTE #I2 = #CONTRACT-TBL-CNT - 1
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 #I2
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_I2)); pnd_I.nadd(1))
        {
            pnd_J2.compute(new ComputeParameters(false, pnd_J2), pnd_Contract_Tbl_Cnt.subtract(pnd_I));                                                                   //Natural: COMPUTE #J2 = #CONTRACT-TBL-CNT - #I
            FOR05:                                                                                                                                                        //Natural: FOR #J = 1 #J2
            for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pnd_J2)); pnd_J.nadd(1))
            {
                if (condition(pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_Date.getValue(pnd_J).greater(pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_Date.getValue(pnd_J.getDec().add(1))))) //Natural: IF #CONTRACT-TBL-ISSUE-DATE ( #J ) GT #CONTRACT-TBL-ISSUE-DATE ( #J+1 )
                {
                    pnd_Temp_Contract_Pnd_Temp_Issue_State_Code.setValue(pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(pnd_J.getDec().add(1)));             //Natural: MOVE #CONTRACT-TBL-ISSUE-STATE-CODE ( #J+1 ) TO #TEMP-ISSUE-STATE-CODE
                    pnd_Temp_Contract_Pnd_Temp_Issue_Date.setValue(pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_Date.getValue(pnd_J.getDec().add(1)));                         //Natural: MOVE #CONTRACT-TBL-ISSUE-DATE ( #J+1 ) TO #TEMP-ISSUE-DATE
                    pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(pnd_J.getDec().add(1)).setValue(pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(pnd_J)); //Natural: MOVE #CONTRACT-TBL-ISSUE-STATE-CODE ( #J ) TO #CONTRACT-TBL-ISSUE-STATE-CODE ( #J+1 )
                    pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_Date.getValue(pnd_J.getDec().add(1)).setValue(pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_Date.getValue(pnd_J));  //Natural: MOVE #CONTRACT-TBL-ISSUE-DATE ( #J ) TO #CONTRACT-TBL-ISSUE-DATE ( #J+1 )
                    pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(pnd_J).setValue(pnd_Temp_Contract_Pnd_Temp_Issue_State_Code);                             //Natural: MOVE #TEMP-ISSUE-STATE-CODE TO #CONTRACT-TBL-ISSUE-STATE-CODE ( #J )
                    pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_Date.getValue(pnd_J).setValue(pnd_Temp_Contract_Pnd_Temp_Issue_Date);                                         //Natural: MOVE #TEMP-ISSUE-DATE TO #CONTRACT-TBL-ISSUE-DATE ( #J )
                }                                                                                                                                                         //Natural: END-IF
                //*  FOR #J
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  FOR #I
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  SORT-CONTRACT-TBL
    }
    private void sub_Display_Contract_Tbl() throws Exception                                                                                                              //Natural: DISPLAY-CONTRACT-TBL
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        getReports().write(0, Global.getPROGRAM(),"(4210) SORTED #CONTRACT-TBL");                                                                                         //Natural: WRITE *PROGRAM '(4210) SORTED #CONTRACT-TBL'
        if (Global.isEscape()) return;
        getReports().write(0, new TabSetting(1),"#I",new TabSetting(5),"CNTRCT-NO",new TabSetting(16),"IA",new TabSetting(19),"DA",new TabSetting(23),"ISS-ST",new        //Natural: WRITE 1T '#I' 5T 'CNTRCT-NO' 16T 'IA' 19T 'DA' 23T 'ISS-ST' 30T 'CURR-ST' 38T 'ISSUE-DATE'
            TabSetting(30),"CURR-ST",new TabSetting(38),"ISSUE-DATE");
        if (Global.isEscape()) return;
        FOR06:                                                                                                                                                            //Natural: FOR #I = 1 #CONTRACT-TBL-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Contract_Tbl_Cnt)); pnd_I.nadd(1))
        {
            getReports().write(0, new TabSetting(1),pnd_I, new NumericLength (2),new TabSetting(23),pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(pnd_I),new  //Natural: WRITE 1T #I ( NL = 2 ) 23T #CONTRACT-TBL-ISSUE-STATE-CODE ( #I ) 38T #CONTRACT-TBL-ISSUE-DATE ( #I )
                TabSetting(38),pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_Date.getValue(pnd_I));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  DISPLAY-CONTRACT-TBL
    }
    private void sub_Get_Approval_Date() throws Exception                                                                                                                 //Natural: GET-APPROVAL-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        //*  READ APP-TABLE-ENTRY BY SUPERDESC 'AW'
        //*  STATE-TABLE
        pnd_Found.reset();                                                                                                                                                //Natural: RESET #FOUND #I-4060-SEARCH-VALUE #W-4060-ENTRY-DSCRPTN-TXT
        pdaCisa4060.getCisa4060_Pnd_I_4060_Search_Value().reset();
        pnd_W_4060_Entry_Dscrptn_Txt.reset();
        pdaCisa4060.getCisa4060_Pnd_I_4060_Search_Mode().setValue("AW");                                                                                                  //Natural: ASSIGN #I-4060-SEARCH-MODE := 'AW'
        pdaCisa4060.getCisa4060_Pnd_I_4060_Aw_Entry_Actve_Ind().setValue("Y");                                                                                            //Natural: ASSIGN #I-4060-AW-ENTRY-ACTVE-IND := 'Y'
        pdaCisa4060.getCisa4060_Pnd_I_4060_Aw_Entry_Table_Id_Nbr().setValue(200);                                                                                         //Natural: ASSIGN #I-4060-AW-ENTRY-TABLE-ID-NBR := 000200
        pdaCisa4060.getCisa4060_Pnd_I_4060_Aw_Entry_Table_Sub_Id().setValue("ST");                                                                                        //Natural: ASSIGN #I-4060-AW-ENTRY-TABLE-SUB-ID := 'ST'
        pnd_W_4060_Entry_Code_Pnd_W_4060_State_Code.setValue(pnd_State_Code);                                                                                             //Natural: ASSIGN #W-4060-STATE-CODE := #STATE-CODE
        pnd_W_4060_Entry_Code_Pnd_W_4060_Prdct_Code.setValue(pdaCisa4070.getCisa4070_Pnd_Cis_Product_Cde());                                                              //Natural: ASSIGN #W-4060-PRDCT-CODE := CISA4070.#CIS-PRODUCT-CDE
        pdaCisa4060.getCisa4060_Pnd_I_4060_Aw_Entry_Cde().setValue(pnd_W_4060_Entry_Code);                                                                                //Natural: ASSIGN #I-4060-AW-ENTRY-CDE := #W-4060-ENTRY-CODE
        DbsUtil.callnat(Cisn4060.class , getCurrentProcessState(), pdaCisa4060.getCisa4060(), pnd_Debug_1);                                                               //Natural: CALLNAT 'CISN4060' CISA4060 #DEBUG-1
        if (condition(Global.isEscape())) return;
        if (condition(pdaCisa4060.getCisa4060_Pnd_O_4060_Return_Code().equals(" ")))                                                                                      //Natural: IF #O-4060-RETURN-CODE = ' '
        {
            pnd_Found.setValue(true);                                                                                                                                     //Natural: ASSIGN #FOUND := TRUE
            pnd_W_4060_Entry_Dscrptn_Txt.setValue(pdaCisa4060.getCisa4060_Pnd_O_4060_Entry_Dscrptn_Txt());                                                                //Natural: ASSIGN #W-4060-ENTRY-DSCRPTN-TXT := #O-4060-ENTRY-DSCRPTN-TXT
            pnd_California_Apprv.setValue(true);                                                                                                                          //Natural: ASSIGN #CALIFORNIA-APPRV := TRUE
            pnd_California_Apprv.setValue(false);                                                                                                                         //Natural: ASSIGN #CALIFORNIA-APPRV := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-APPROVAL-DATE
    }
    private void sub_Return_Contract_Approved() throws Exception                                                                                                          //Natural: RETURN-CONTRACT-APPROVED
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pdaCisa4070.getCisa4070_Pnd_Cis_Cntrct_Apprvl_Ind().setValue("Y");                                                                                                //Natural: MOVE 'Y' TO CISA4070.#CIS-CNTRCT-APPRVL-IND
        pdaCisa4070.getCisa4070_Pnd_Cis_Org_Issue_St().setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1));                                          //Natural: ASSIGN CISA4070.#CIS-ORG-ISSUE-ST := #FROM-STATE-INPUT ( 1 )
        //* *MOVE #STATE-CODE  TO  CISA4070.#CIS-ORG-ISSUE-ST
        if (condition(pdaCisa4070.getCisa4070_Pnd_Cis_Org_Issue_St().equals("CA")))                                                                                       //Natural: IF CISA4070.#CIS-ORG-ISSUE-ST EQ 'CA'
        {
            FOR07:                                                                                                                                                        //Natural: FOR #I = 1 TO 10
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
            {
                if (condition(pnd_California.getBoolean()))                                                                                                               //Natural: IF #CALIFORNIA
                {
                    if (condition(pnd_State_Cde_Coll.getValue(pnd_I).notEquals(" ")))                                                                                     //Natural: IF #STATE-CDE-COLL ( #I ) NE ' '
                    {
                        pnd_State_Code.setValue(pnd_State_Cde_Coll.getValue(1));                                                                                          //Natural: ASSIGN #STATE-CODE := #STATE-CDE-COLL ( 1 )
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*").reset();                                                           //Natural: RESET #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * )
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(pdaCisa8001.getCisa8001_Pnd_State_Cde());                                 //Natural: MOVE #STATE-CDE TO #FROM-STATE-INPUT ( 1 )
                        pnd_Perform_Approval.setValue(true);                                                                                                              //Natural: ASSIGN #PERFORM-APPROVAL := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(pnd_I).notEquals("CA") && pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(pnd_I).notEquals(" "))) //Natural: IF #CONTRACT-TBL-ISSUE-STATE-CODE ( #I ) NE 'CA' AND #CONTRACT-TBL-ISSUE-STATE-CODE ( #I ) NE ' '
                    {
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*").reset();                                                           //Natural: RESET #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * )
                        pnd_State_Code.setValue(pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(pnd_I));                                                      //Natural: ASSIGN #STATE-CODE := #CONTRACT-TBL-ISSUE-STATE-CODE ( #I )
                        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(pdaCisa8001.getCisa8001_Pnd_State_Cde());                                 //Natural: MOVE #STATE-CDE TO #FROM-STATE-INPUT ( 1 )
                        pnd_Perform_Approval.setValue(true);                                                                                                              //Natural: ASSIGN #PERFORM-APPROVAL := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Perform_Approval.getBoolean()))                                                                                                         //Natural: IF #PERFORM-APPROVAL
                {
                                                                                                                                                                          //Natural: PERFORM GET-STATE-INFO
                    sub_Get_State_Info();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM RETURN-STATE-INFO-MDO
                    sub_Return_State_Info_Mdo();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Return.getBoolean() && ! (pnd_Return_Single.getBoolean())))                                                                         //Natural: IF #RETURN AND NOT #RETURN-SINGLE
                    {
                        pdaCisa4070.getCisa4070_Pnd_Cis_Cntrct_Apprvl_Ind().setValue("Y");                                                                                //Natural: MOVE 'Y' TO CISA4070.#CIS-CNTRCT-APPRVL-IND
                        pdaCisa4070.getCisa4070_Pnd_Cis_Org_Issue_St().setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1));                          //Natural: ASSIGN CISA4070.#CIS-ORG-ISSUE-ST := #FROM-STATE-INPUT ( 1 )
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM RETURN-CONTRACT-NOT-APPROVED
                        sub_Return_Contract_Not_Approved();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Debug_1.getBoolean()))                                                                                                                          //Natural: IF #DEBUG-1
        {
            getReports().write(0, Global.getPROGRAM(),"(4840) CONTRACT APPROVED");                                                                                        //Natural: WRITE *PROGRAM '(4840) CONTRACT APPROVED'
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*"));                                                                   //Natural: WRITE '=' CISA4070.#CIS-TIAA-CNTRCT-NBR ( * )
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaCisa4070.getCisa4070_Pnd_Cis_Product_Cde());                                                                                     //Natural: WRITE '=' CISA4070.#CIS-PRODUCT-CDE
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaCisa4070.getCisa4070_Pnd_Cis_Residence_Issue_St());                                                                              //Natural: WRITE '=' CISA4070.#CIS-RESIDENCE-ISSUE-ST
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaCisa4070.getCisa4070_Pnd_Cis_Cntrct_Apprvl_Ind());                                                                               //Natural: WRITE '=' CISA4070.#CIS-CNTRCT-APPRVL-IND
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaCisa4070.getCisa4070_Pnd_Cis_Org_Issue_St());                                                                                    //Natural: WRITE '=' CISA4070.#CIS-ORG-ISSUE-ST
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaCisa4070.getCisa4070_Pnd_Cis_Issue_Dt());                                                                                        //Natural: WRITE '=' CISA4070.#CIS-ISSUE-DT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        //*  RETURN-CONTRACT-APPROVED
    }
    private void sub_Return_Contract_Not_Approved() throws Exception                                                                                                      //Natural: RETURN-CONTRACT-NOT-APPROVED
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------------------------------------
        pdaCisa4070.getCisa4070_Pnd_Cis_Cntrct_Apprvl_Ind().setValue("N");                                                                                                //Natural: MOVE 'N' TO CISA4070.#CIS-CNTRCT-APPRVL-IND
        if (condition(! (pnd_California.getBoolean()) || pdaCisa4070.getCisa4070_Pnd_Cis_Requestor().equals("IA")))                                                       //Natural: IF NOT #CALIFORNIA OR CISA4070.#CIS-REQUESTOR EQ 'IA'
        {
            if (condition(pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(1).equals("00") || pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(1).equals(" "))) //Natural: IF #CONTRACT-TBL-ISSUE-STATE-CODE ( 1 ) EQ '00' OR #CONTRACT-TBL-ISSUE-STATE-CODE ( 1 ) EQ ' '
            {
                pdaCisa4070.getCisa4070_Pnd_Cis_Org_Issue_St().setValue(pdaCisa4070.getCisa4070_Pnd_Cis_Residence_Issue_St());                                            //Natural: MOVE CISA4070.#CIS-RESIDENCE-ISSUE-ST TO CISA4070.#CIS-ORG-ISSUE-ST
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                FOR08:                                                                                                                                                    //Natural: FOR #I = 1 TO 10
                for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(10)); pnd_I.nadd(1))
                {
                    if (condition(pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(pnd_I).equals("CA") || pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(pnd_I).equals("  "))) //Natural: IF #CONTRACT-TBL-ISSUE-STATE-CODE ( #I ) EQ 'CA' OR #CONTRACT-TBL-ISSUE-STATE-CODE ( #I ) EQ '  '
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Found_Non_California.setValue(true);                                                                                                          //Natural: ASSIGN #FOUND-NON-CALIFORNIA := TRUE
                        pdaCisa4070.getCisa4070_Pnd_Cis_Org_Issue_St().setValue(pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(pnd_I));                      //Natural: MOVE #CONTRACT-TBL-ISSUE-STATE-CODE ( #I ) TO CISA4070.#CIS-ORG-ISSUE-ST
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                if (condition(! (pnd_Found_Non_California.getBoolean())))                                                                                                 //Natural: IF NOT #FOUND-NON-CALIFORNIA
                {
                    pdaCisa4070.getCisa4070_Pnd_Cis_Org_Issue_St().setValue(pdaCisa4070.getCisa4070_Pnd_Cis_Residence_Issue_St());                                        //Natural: MOVE CISA4070.#CIS-RESIDENCE-ISSUE-ST TO CISA4070.#CIS-ORG-ISSUE-ST
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  060706
        if (condition(DbsUtil.maskMatches(pdaCisa4070.getCisa4070_Pnd_Cis_Requestor(),"'MDO'") && pnd_California.getBoolean()))                                           //Natural: IF CISA4070.#CIS-REQUESTOR EQ MASK ( 'MDO' ) AND #CALIFORNIA
        {
            if (condition(pnd_State_Cde_Coll.getValue(1).equals(" ")))                                                                                                    //Natural: IF #STATE-CDE-COLL ( 1 ) EQ ' '
            {
                if (condition(pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(1).equals(" ")))                                                                //Natural: IF #CONTRACT-TBL-ISSUE-STATE-CODE ( 1 ) EQ ' '
                {
                    pdaCisa4070.getCisa4070_Pnd_Cis_Org_Issue_St().setValue(pdaCisa4070.getCisa4070_Pnd_Cis_Residence_Issue_St());                                        //Natural: MOVE CISA4070.#CIS-RESIDENCE-ISSUE-ST TO CISA4070.#CIS-ORG-ISSUE-ST
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaCisa4070.getCisa4070_Pnd_Cis_Org_Issue_St().setValue(pnd_Contract_Tbl_Pnd_Contract_Tbl_Issue_State_Code.getValue(1));                              //Natural: MOVE #CONTRACT-TBL-ISSUE-STATE-CODE ( 1 ) TO CISA4070.#CIS-ORG-ISSUE-ST
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCisa4070.getCisa4070_Pnd_Cis_Org_Issue_St().setValue(pnd_State_Cde_Coll_1st);                                                                          //Natural: MOVE #STATE-CDE-COLL-1ST TO CISA4070.#CIS-ORG-ISSUE-ST
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Debug_1.getBoolean()))                                                                                                                          //Natural: IF #DEBUG-1
        {
            getReports().write(0, Global.getPROGRAM(),"(5310) NO CONTRACTS APPROVED");                                                                                    //Natural: WRITE *PROGRAM '(5310) NO CONTRACTS APPROVED'
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*"));                                                                   //Natural: WRITE '=' CISA4070.#CIS-TIAA-CNTRCT-NBR ( * )
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaCisa4070.getCisa4070_Pnd_Cis_Product_Cde());                                                                                     //Natural: WRITE '=' CISA4070.#CIS-PRODUCT-CDE
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaCisa4070.getCisa4070_Pnd_Cis_Residence_Issue_St());                                                                              //Natural: WRITE '=' CISA4070.#CIS-RESIDENCE-ISSUE-ST
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaCisa4070.getCisa4070_Pnd_Cis_Cntrct_Apprvl_Ind());                                                                               //Natural: WRITE '=' CISA4070.#CIS-CNTRCT-APPRVL-IND
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaCisa4070.getCisa4070_Pnd_Cis_Org_Issue_St());                                                                                    //Natural: WRITE '=' CISA4070.#CIS-ORG-ISSUE-ST
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaCisa4070.getCisa4070_Pnd_Cis_Issue_Dt());                                                                                        //Natural: WRITE '=' CISA4070.#CIS-ISSUE-DT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  RETURN-CONTRACT-NOT-APPROVED
    }
    private void sub_Get_State_Info() throws Exception                                                                                                                    //Natural: GET-STATE-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------*
        pdaCisa1000.getCisa1000_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*"));         //Natural: MOVE #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * ) TO CISA1000.#CIS-TIAA-CNTRCT-NBR ( * )
        pdaCisa1000.getCisa1000_Pnd_Cis_Requestor().setValue(pdaCisa4070.getCisa4070_Pnd_Cis_Requestor());                                                                //Natural: MOVE CISA4070.#CIS-REQUESTOR TO CISA1000.#CIS-REQUESTOR
        pdaCisa1000.getCisa1000_Pnd_Cis_Issue_Dt().setValue(pdaCisa4070.getCisa4070_Pnd_Cis_Issue_Dt());                                                                  //Natural: MOVE CISA4070.#CIS-ISSUE-DT TO CISA1000.#CIS-ISSUE-DT
        DbsUtil.callnat(Cisn4071.class , getCurrentProcessState(), pdaCisa1000.getCisa1000(), pdaCisa1000.getPnd_Cis_Misc());                                             //Natural: CALLNAT 'CISN4071' CISA1000 #CIS-MISC
        if (condition(Global.isEscape())) return;
        if (condition(pdaCisa1000.getCisa1000_Pnd_Cis_Msg_Number().getValue(10).notEquals(" ")))                                                                          //Natural: IF CISA1000.#CIS-MSG-NUMBER ( 10 ) NE ' '
        {
            if (condition(pdaCisa1000.getCisa1000_Pnd_Cis_Msg_Number().getValue(10).equals("4072")))                                                                      //Natural: IF CISA1000.#CIS-MSG-NUMBER ( 10 ) EQ '4072'
            {
                if (condition(pnd_Debug_1.getBoolean()))                                                                                                                  //Natural: IF #DEBUG-1
                {
                    getReports().write(0, "=",pdaCisa1000.getCisa1000_Pnd_Cis_Msg_Text());                                                                                //Natural: WRITE '=' CISA1000.#CIS-MSG-TEXT
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*").setValue(pdaCisa1000.getCisa1000_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*"));         //Natural: MOVE CISA1000.#CIS-TIAA-CNTRCT-NBR ( * ) TO #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * )
        pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State1_Hold.getValue(pnd_L).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1));                            //Natural: ASSIGN #STATE1-HOLD ( #L ) := #FROM-STATE-INPUT ( 1 )
        pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind1_Hold.getValue(pnd_L).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1));              //Natural: ASSIGN #APPROVAL-IND1-HOLD ( #L ) := #FROM-APPROVAL-IND-INPUT ( 1 )
        pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State2_Hold.getValue(pnd_L).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(2));                            //Natural: ASSIGN #STATE2-HOLD ( #L ) := #FROM-STATE-INPUT ( 2 )
        pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind2_Hold.getValue(pnd_L).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2));              //Natural: ASSIGN #APPROVAL-IND2-HOLD ( #L ) := #FROM-APPROVAL-IND-INPUT ( 2 )
        pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State3_Hold.getValue(pnd_L).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(3));                            //Natural: ASSIGN #STATE3-HOLD ( #L ) := #FROM-STATE-INPUT ( 3 )
        pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind3_Hold.getValue(pnd_L).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(3));              //Natural: ASSIGN #APPROVAL-IND3-HOLD ( #L ) := #FROM-APPROVAL-IND-INPUT ( 3 )
        pnd_L.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #L
        if (condition(pnd_Debug_1.getBoolean()))                                                                                                                          //Natural: IF #DEBUG-1
        {
            getReports().write(0, "=",pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Data.getValue(1),"hold cntrct FFFFFFFDDDDD",pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1), //Natural: WRITE '=' #CIS-TIAA-CNTRCT-NBR-HOLD ( 1 ) 'hold cntrct FFFFFFFDDDDD' #FROM-STATE-INPUT ( 1 ) #FROM-APPROVAL-IND-INPUT ( 1 ) #FROM-STATE-INPUT ( 2 ) #FROM-APPROVAL-IND-INPUT ( 2 ) #FROM-STATE-INPUT ( 3 ) #FROM-APPROVAL-IND-INPUT ( 3 ) #FROM-STATE-INPUT ( 4 ) #FROM-APPROVAL-IND-INPUT ( 4 )
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1),pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(2),pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2),
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(3),pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(3),pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(4),
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(4));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_State_Info_Res() throws Exception                                                                                                                //Natural: GET-STATE-INFO-RES
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------*
        pdaCisa1000.getCisa1000_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*"));         //Natural: MOVE #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * ) TO CISA1000.#CIS-TIAA-CNTRCT-NBR ( * )
        pdaCisa1000.getCisa1000_Pnd_Cis_Requestor().setValue(pdaCisa4070.getCisa4070_Pnd_Cis_Requestor());                                                                //Natural: MOVE CISA4070.#CIS-REQUESTOR TO CISA1000.#CIS-REQUESTOR
        pdaCisa1000.getCisa1000_Pnd_Cis_Issue_Dt().setValue(pdaCisa4070.getCisa4070_Pnd_Cis_Issue_Dt());                                                                  //Natural: MOVE CISA4070.#CIS-ISSUE-DT TO CISA1000.#CIS-ISSUE-DT
        DbsUtil.callnat(Cisn4071.class , getCurrentProcessState(), pdaCisa1000.getCisa1000(), pdaCisa1000.getPnd_Cis_Misc());                                             //Natural: CALLNAT 'CISN4071' CISA1000 #CIS-MISC
        if (condition(Global.isEscape())) return;
        pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*").setValue(pdaCisa1000.getCisa1000_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*"));         //Natural: MOVE CISA1000.#CIS-TIAA-CNTRCT-NBR ( * ) TO #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * )
        pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State1_Hold.setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1));                                         //Natural: ASSIGN #RES-STATE1-HOLD := #FROM-STATE-INPUT ( 1 )
        pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind1_Hold.setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1));                           //Natural: ASSIGN #RES-APPROVAL-IND1-HOLD := #FROM-APPROVAL-IND-INPUT ( 1 )
        pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State2_Hold.setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(2));                                         //Natural: ASSIGN #RES-STATE2-HOLD := #FROM-STATE-INPUT ( 2 )
        pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind2_Hold.setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2));                           //Natural: ASSIGN #RES-APPROVAL-IND2-HOLD := #FROM-APPROVAL-IND-INPUT ( 2 )
        pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State3_Hold.setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(3));                                         //Natural: ASSIGN #RES-STATE3-HOLD := #FROM-STATE-INPUT ( 3 )
        pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind3_Hold.setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(3));                           //Natural: ASSIGN #RES-APPROVAL-IND3-HOLD := #FROM-APPROVAL-IND-INPUT ( 3 )
        if (condition(pnd_Debug_1.getBoolean()))                                                                                                                          //Natural: IF #DEBUG-1
        {
            getReports().write(0, "=",pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Cis_Tiaa_Cntrct_Nbr_Res_Data,"residency");                                                          //Natural: WRITE '=' #CIS-TIAA-CNTRCT-NBR-RES 'residency'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Return_State_Info() throws Exception                                                                                                                 //Natural: RETURN-STATE-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------*
        pnd_Return.reset();                                                                                                                                               //Natural: RESET #RETURN #RETURN-SINGLE #RETURN-MULTI
        pnd_Return_Single.reset();
        pnd_Return_Multi.reset();
        FOR09:                                                                                                                                                            //Natural: FOR #K = 1 TO 20
        for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(20)); pnd_K.nadd(1))
        {
            //* *EXAMINE #APPROVAL-IND2-HOLD(*) FOR 'M' GIVING INDEX #K
            //* *IF #K GT 0
            if (condition(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind2_Hold.getValue(pnd_K).equals("M") && pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind1_Hold.getValue(pnd_K).notEquals("S"))) //Natural: IF #APPROVAL-IND2-HOLD ( #K ) EQ 'M' AND #APPROVAL-IND1-HOLD ( #K ) NE 'S'
            {
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State1_Hold.getValue(pnd_K));                    //Natural: ASSIGN #FROM-STATE-INPUT ( 1 ) := #STATE1-HOLD ( #K )
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind1_Hold.getValue(pnd_K));      //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 1 ) := #APPROVAL-IND1-HOLD ( #K )
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State2_Hold.getValue(pnd_K));                    //Natural: ASSIGN #FROM-STATE-INPUT ( 2 ) := #STATE2-HOLD ( #K )
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind2_Hold.getValue(pnd_K));      //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 2 ) := #APPROVAL-IND2-HOLD ( #K )
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State3_Hold);                                 //Natural: ASSIGN #FROM-STATE-INPUT ( 3 ) := #RES-STATE3-HOLD
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind3_Hold);                   //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 3 ) := #RES-APPROVAL-IND3-HOLD
                if (condition(pnd_Debug_1.getBoolean()))                                                                                                                  //Natural: IF #DEBUG-1
                {
                    getReports().write(0, "=",pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue(1,":",4),"monthly units");                        //Natural: WRITE '=' #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( 1:4 ) 'monthly units'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").reset();                                                                                  //Natural: RESET CISA4070.#CIS-TIAA-CNTRCT-NBR ( * )
                pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*")); //Natural: ASSIGN CISA4070.#CIS-TIAA-CNTRCT-NBR ( * ) := #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * )
                pnd_Return.setValue(true);                                                                                                                                //Natural: ASSIGN #RETURN := TRUE
                pnd_Return_Multi.setValue(true);                                                                                                                          //Natural: ASSIGN #RETURN-MULTI := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind2_Hold.equals("M") && pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind1_Hold.notEquals("S")))       //Natural: IF #RES-APPROVAL-IND2-HOLD EQ 'M' AND #RES-APPROVAL-IND1-HOLD NE 'S'
        {
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State1_Hold);                                     //Natural: ASSIGN #FROM-STATE-INPUT ( 1 ) := #RES-STATE1-HOLD
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind1_Hold);                       //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 1 ) := #RES-APPROVAL-IND1-HOLD
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State2_Hold);                                     //Natural: ASSIGN #FROM-STATE-INPUT ( 2 ) := #RES-STATE2-HOLD
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind2_Hold);                       //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 2 ) := #RES-APPROVAL-IND2-HOLD
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State3_Hold);                                     //Natural: ASSIGN #FROM-STATE-INPUT ( 3 ) := #RES-STATE3-HOLD
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind3_Hold);                       //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 3 ) := #RES-APPROVAL-IND3-HOLD
            pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").reset();                                                                                      //Natural: RESET CISA4070.#CIS-TIAA-CNTRCT-NBR ( * )
            pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*"));     //Natural: ASSIGN CISA4070.#CIS-TIAA-CNTRCT-NBR ( * ) := #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * )
            pnd_Return.setValue(true);                                                                                                                                    //Natural: ASSIGN #RETURN := TRUE
            pnd_Return_Multi.setValue(true);                                                                                                                              //Natural: ASSIGN #RETURN-MULTI := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind2_Hold.getValue("*")), new ExamineSearch("M"), new ExamineGivingIndex(pnd_K));     //Natural: EXAMINE #APPROVAL-IND2-HOLD ( * ) FOR 'M' GIVING INDEX #K
        if (condition(pnd_K.greater(getZero())))                                                                                                                          //Natural: IF #K GT 0
        {
            if (condition(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind2_Hold.getValue(pnd_K).equals("M")))                                                               //Natural: IF #APPROVAL-IND2-HOLD ( #K ) EQ 'M'
            {
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State1_Hold.getValue(pnd_K));                    //Natural: ASSIGN #FROM-STATE-INPUT ( 1 ) := #STATE1-HOLD ( #K )
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind1_Hold.getValue(pnd_K));      //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 1 ) := #APPROVAL-IND1-HOLD ( #K )
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State2_Hold.getValue(pnd_K));                    //Natural: ASSIGN #FROM-STATE-INPUT ( 2 ) := #STATE2-HOLD ( #K )
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind2_Hold.getValue(pnd_K));      //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 2 ) := #APPROVAL-IND2-HOLD ( #K )
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State3_Hold);                                 //Natural: ASSIGN #FROM-STATE-INPUT ( 3 ) := #RES-STATE3-HOLD
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind3_Hold);                   //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 3 ) := #RES-APPROVAL-IND3-HOLD
                if (condition(pnd_Debug_1.getBoolean()))                                                                                                                  //Natural: IF #DEBUG-1
                {
                    getReports().write(0, "=",pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue(1,":",3),"monthly units");                        //Natural: WRITE '=' #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( 1:3 ) 'monthly units'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").reset();                                                                                  //Natural: RESET CISA4070.#CIS-TIAA-CNTRCT-NBR ( * )
                pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*")); //Natural: ASSIGN CISA4070.#CIS-TIAA-CNTRCT-NBR ( * ) := #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * )
                pnd_Return.setValue(true);                                                                                                                                //Natural: ASSIGN #RETURN := TRUE
                pnd_Return_Multi.setValue(true);                                                                                                                          //Natural: ASSIGN #RETURN-MULTI := TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_K.reset();                                                                                                                                                    //Natural: RESET #K
        DbsUtil.examine(new ExamineSource(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind2_Hold.getValue("*")), new ExamineSearch("A"), new ExamineGivingIndex(pnd_K));     //Natural: EXAMINE #APPROVAL-IND2-HOLD ( * ) FOR 'A' GIVING INDEX #K
        if (condition(pnd_K.greater(getZero())))                                                                                                                          //Natural: IF #K GT 0
        {
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State1_Hold.getValue(pnd_K));                        //Natural: ASSIGN #FROM-STATE-INPUT ( 1 ) := #STATE1-HOLD ( #K )
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind1_Hold.getValue(pnd_K));          //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 1 ) := #APPROVAL-IND1-HOLD ( #K )
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State2_Hold.getValue(pnd_K));                        //Natural: ASSIGN #FROM-STATE-INPUT ( 2 ) := #STATE2-HOLD ( #K )
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind2_Hold.getValue(pnd_K));          //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 2 ) := #APPROVAL-IND2-HOLD ( #K )
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State3_Hold);                                     //Natural: ASSIGN #FROM-STATE-INPUT ( 3 ) := #RES-STATE3-HOLD
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind3_Hold);                       //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 3 ) := #RES-APPROVAL-IND3-HOLD
            if (condition(pnd_Debug_1.getBoolean()))                                                                                                                      //Natural: IF #DEBUG-1
            {
                getReports().write(0, "=",pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue(1,":",3),"annual units");                             //Natural: WRITE '=' #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( 1:3 ) 'annual units'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").reset();                                                                                      //Natural: RESET CISA4070.#CIS-TIAA-CNTRCT-NBR ( * )
            pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*"));     //Natural: ASSIGN CISA4070.#CIS-TIAA-CNTRCT-NBR ( * ) := #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * )
            pnd_Return.setValue(true);                                                                                                                                    //Natural: ASSIGN #RETURN := TRUE
            pnd_Return_Annual.setValue(true);                                                                                                                             //Natural: ASSIGN #RETURN-ANNUAL := TRUE
            //*  ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* **********************************************************************
        pnd_Return_Single.reset();                                                                                                                                        //Natural: RESET #RETURN-SINGLE
        if (condition(pnd_K.equals(getZero())))                                                                                                                           //Natural: IF #K EQ 0
        {
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State1_Hold.getValue(1));                            //Natural: ASSIGN #FROM-STATE-INPUT ( 1 ) := #STATE1-HOLD ( 1 )
            if (condition(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind1_Hold.getValue(1).equals(" ")))                                                                   //Natural: IF #APPROVAL-IND1-HOLD ( 1 ) EQ ' '
            {
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1).setValue("S");                                                                      //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 1 ) := 'S'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind1_Hold.getValue(1));          //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 1 ) := #APPROVAL-IND1-HOLD ( 1 )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State2_Hold.getValue(1));                            //Natural: ASSIGN #FROM-STATE-INPUT ( 2 ) := #STATE2-HOLD ( 1 )
            if (condition(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind2_Hold.getValue(2).equals(" ")))                                                                   //Natural: IF #APPROVAL-IND2-HOLD ( 2 ) EQ ' '
            {
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2).setValue("S");                                                                      //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 2 ) := 'S'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind2_Hold.getValue(1));          //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 2 ) := #APPROVAL-IND2-HOLD ( 1 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State1_Hold.equals(" ")))                                                                                   //Natural: IF #RES-STATE1-HOLD EQ ' '
            {
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(3).setValue("A");                                                                             //Natural: ASSIGN #FROM-STATE-INPUT ( 3 ) := 'A'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State3_Hold);                                 //Natural: ASSIGN #FROM-STATE-INPUT ( 3 ) := #RES-STATE3-HOLD
                pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind3_Hold);                   //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 3 ) := #RES-APPROVAL-IND3-HOLD
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Debug_1.getBoolean()))                                                                                                                      //Natural: IF #DEBUG-1
            {
                getReports().write(0, "=",pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue(1,":",3),"single funded");                            //Natural: WRITE '=' #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( 1:3 ) 'single funded'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").reset();                                                                                      //Natural: RESET CISA4070.#CIS-TIAA-CNTRCT-NBR ( * )
            pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*"));     //Natural: ASSIGN CISA4070.#CIS-TIAA-CNTRCT-NBR ( * ) := #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * )
            pnd_Return.setValue(true);                                                                                                                                    //Natural: ASSIGN #RETURN := TRUE
            pnd_Return_Single.setValue(true);                                                                                                                             //Natural: ASSIGN #RETURN-SINGLE := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* ************
        if (condition(pnd_K.equals(getZero())))                                                                                                                           //Natural: IF #K EQ 0
        {
            //*  #FROM-STATE-INPUT(1)        := #STATE1-HOLD(1)
            //*  #FROM-APPROVAL-IND-INPUT(1) := 'S'
            //*  #FROM-STATE-INPUT(2)        := #STATE2-HOLD(1)
            //*  #FROM-APPROVAL-IND-INPUT(2) := 'S'
            //*  #FROM-STATE-INPUT(3)        := #RES-STATE3-HOLD
            //*  #FROM-APPROVAL-IND-INPUT(3) := 'S'
            if (condition(pnd_Debug_1.getBoolean()))                                                                                                                      //Natural: IF #DEBUG-1
            {
                getReports().write(0, "=",pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue(1,":",3),"single funded");                            //Natural: WRITE '=' #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( 1:3 ) 'single funded'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_Return.setValue(true);                                                                                                                                    //Natural: ASSIGN #RETURN := TRUE
            pnd_Return_Single.setValue(true);                                                                                                                             //Natural: ASSIGN #RETURN-SINGLE := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Return_State_Info_Mdo() throws Exception                                                                                                             //Natural: RETURN-STATE-INFO-MDO
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------*
        pnd_Return.reset();                                                                                                                                               //Natural: RESET #RETURN #RETURN-SINGLE
        pnd_Return_Single.reset();
        DbsUtil.examine(new ExamineSource(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind1_Hold.getValue("*")), new ExamineSearch("A"), new ExamineGivingIndex(pnd_K));     //Natural: EXAMINE #APPROVAL-IND1-HOLD ( * ) FOR 'A' GIVING INDEX #K
        if (condition(pnd_K.greater(getZero())))                                                                                                                          //Natural: IF #K GT 0
        {
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State1_Hold.getValue(pnd_K));                        //Natural: ASSIGN #FROM-STATE-INPUT ( 1 ) := #STATE1-HOLD ( #K )
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind1_Hold.getValue(pnd_K));          //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 1 ) := #APPROVAL-IND1-HOLD ( #K )
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State2_Hold.getValue(pnd_K));                        //Natural: ASSIGN #FROM-STATE-INPUT ( 2 ) := #STATE2-HOLD ( #K )
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(2).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_Approval_Ind2_Hold.getValue(pnd_K));          //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 2 ) := #APPROVAL-IND2-HOLD ( #K )
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_State3_Hold);                                     //Natural: ASSIGN #FROM-STATE-INPUT ( 3 ) := #RES-STATE3-HOLD
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(3).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Res_Pnd_Res_Approval_Ind3_Hold);                       //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 3 ) := #RES-APPROVAL-IND3-HOLD
            if (condition(pnd_Debug_1.getBoolean()))                                                                                                                      //Natural: IF #DEBUG-1
            {
                getReports().write(0, "=",pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue(1,":",3),"annual units mdo");                         //Natural: WRITE '=' #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( 1:3 ) 'annual units mdo'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").reset();                                                                                      //Natural: RESET CISA4070.#CIS-TIAA-CNTRCT-NBR ( * )
            pdaCisa4070.getCisa4070_Pnd_Cis_Tiaa_Cntrct_Nbr().getValue("*").setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue("*"));     //Natural: ASSIGN CISA4070.#CIS-TIAA-CNTRCT-NBR ( * ) := #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( * )
            pnd_Return.setValue(true);                                                                                                                                    //Natural: ASSIGN #RETURN := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Return_Single.reset();                                                                                                                                        //Natural: RESET #RETURN-SINGLE
        if (condition(pnd_K.equals(getZero())))                                                                                                                           //Natural: IF #K EQ 0
        {
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_State_Input.getValue(1).setValue(pnd_Cis_Tiaa_Cntrct_Nbr_Hold_Pnd_State1_Hold.getValue(1));                            //Natural: ASSIGN #FROM-STATE-INPUT ( 1 ) := #STATE1-HOLD ( 1 )
            pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_From_Approval_Ind_Input.getValue(1).setValue("S");                                                                          //Natural: ASSIGN #FROM-APPROVAL-IND-INPUT ( 1 ) := 'S'
            //*  #FROM-STATE-INPUT(2)        := #STATE2-HOLD(1)
            //*  #FROM-APPROVAL-IND-INPUT(2) := 'S'
            //*  #FROM-STATE-INPUT(3)        := #RES-STATE3-HOLD
            //*  #FROM-APPROVAL-IND-INPUT(3) := 'S'
            if (condition(pnd_Debug_1.getBoolean()))                                                                                                                      //Natural: IF #DEBUG-1
            {
                getReports().write(0, "=",pnd_Cis_Tiaa_Cntrct_Nbr_Input_Pnd_Cis_Tiaa_Cntrct_Nbr_Input_Data.getValue(1,":",3),"single funded mdo");                        //Natural: WRITE '=' #CIS-TIAA-CNTRCT-NBR-INPUT-DATA ( 1:3 ) 'single funded mdo'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_Return.setValue(true);                                                                                                                                    //Natural: ASSIGN #RETURN := TRUE
            pnd_Return_Single.setValue(true);                                                                                                                             //Natural: ASSIGN #RETURN-SINGLE := TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
