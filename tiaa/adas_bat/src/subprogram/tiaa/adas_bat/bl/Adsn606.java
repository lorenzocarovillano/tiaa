/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:55:42 PM
**        * FROM NATURAL SUBPROGRAM : Adsn606
************************************************************
**        * FILE NAME            : Adsn606.java
**        * CLASS NAME           : Adsn606
**        * INSTANCE NAME        : Adsn606
************************************************************
************************************************************************
*
* PROGRAM:  ADSN606 (FORMERLY NAZN606)
* DATE   :  06/25/2003
* FUNCTION: INTERFACE TO THE ACTUARIAL IVC CALC MODULE TO DETERMINE
*           PERIODIC IVC AMOUNT FOR TPA AND AC CONTRACTS FOR IA ONLINE.
*
* 02/27/08 O. SOTTO RATE EXPANSION TO 99 OCCURS. SC 022708.
* 03/15/12 O. SOTTO RATE EXPANSION. SC 031512.
* 11/26/13 E. MELNIK RECOMPILED FOR NEW NECA4000.  CREF/REA REDESIGN
*                    CHANGES.
*
*
*
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn606 extends BLNatBase
{
    // Data Areas
    private PdaNeca4000 pdaNeca4000;
    private PdaAiaa085 pdaAiaa085;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Ivc_Call;
    private DbsField nai_Annty_Strt_Dte;
    private DbsField prcss_Dte;
    private DbsField nap_Ia_Tiaa_Nbr;
    private DbsField nap_Ia_Cref_Nbr;
    private DbsField nai_Finl_Prtl_Py_Dte;
    private DbsField nai_Pymnt_Mode;
    private DbsField nai_Dtl_Tiaa_Ia_Rate_Cd;
    private DbsField nai_Dtl2_Tiaa_Ia_Rate_Cd;
    private DbsField nai_Dtl_Tiaa_Std_Grntd_Amt;
    private DbsField nai_Dtl_Tiaa_Std_Dvdnd_Amt;
    private DbsField cpnd_Nai_Dtl_Tiaa_Data;
    private DbsField nai_Dtl_Cref_Acct_Cd;
    private DbsField nai_Dtl_Cref_Annl_Nbr_Units;
    private DbsField nai_Dtl_Cref_Annl_Amt;
    private DbsField nai_Dtl_Cref_Mnthly_Nbr_Units;
    private DbsField nai_Dtl_Cref_Mnthly_Amt;
    private DbsField nap_Alt_Dest_Rlvr_Dest;
    private DbsField nap_Ivc_Pymnt_Rule;
    private DbsField nap_Ivc_Rlvr_Cde;
    private DbsField nac_Trk_Ro_Ivc;
    private DbsField tiaa_Ivc_Amt;
    private DbsField cref_Ivc_Amt;
    private DbsField per_Ivc_Amt;
    private DbsField pnd_Err_Cde;
    private DbsField pnd_More;
    private DbsField pnd_Max_Rslt;
    private DbsField pnd_Max_Rate;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Cal_Date;

    private DbsGroup pnd_Cal_Date__R_Field_1;
    private DbsField pnd_Cal_Date_Pnd_Cal_Date_N;

    private DbsGroup pnd_Cal_Date__R_Field_2;
    private DbsField pnd_Cal_Date__Filler1;
    private DbsField pnd_Cal_Date_Pnd_Day;
    private DbsField pnd_W_Date;

    private DbsGroup pnd_W_Date__R_Field_3;
    private DbsField pnd_W_Date_Pnd_W_Date_N;
    private DbsField pnd_W_Date2;

    private DbsGroup pnd_W_Date2__R_Field_4;
    private DbsField pnd_W_Date2_Pnd_W_Date_N2;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaNeca4000 = new PdaNeca4000(localVariables);
        pdaAiaa085 = new PdaAiaa085(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_Ivc_Call = parameters.newFieldInRecord("pnd_Ivc_Call", "#IVC-CALL", FieldType.STRING, 1);
        pnd_Ivc_Call.setParameterOption(ParameterOption.ByReference);
        nai_Annty_Strt_Dte = parameters.newFieldInRecord("nai_Annty_Strt_Dte", "NAI-ANNTY-STRT-DTE", FieldType.DATE);
        nai_Annty_Strt_Dte.setParameterOption(ParameterOption.ByReference);
        prcss_Dte = parameters.newFieldInRecord("prcss_Dte", "PRCSS-DTE", FieldType.DATE);
        prcss_Dte.setParameterOption(ParameterOption.ByReference);
        nap_Ia_Tiaa_Nbr = parameters.newFieldInRecord("nap_Ia_Tiaa_Nbr", "NAP-IA-TIAA-NBR", FieldType.STRING, 10);
        nap_Ia_Tiaa_Nbr.setParameterOption(ParameterOption.ByReference);
        nap_Ia_Cref_Nbr = parameters.newFieldInRecord("nap_Ia_Cref_Nbr", "NAP-IA-CREF-NBR", FieldType.STRING, 10);
        nap_Ia_Cref_Nbr.setParameterOption(ParameterOption.ByReference);
        nai_Finl_Prtl_Py_Dte = parameters.newFieldInRecord("nai_Finl_Prtl_Py_Dte", "NAI-FINL-PRTL-PY-DTE", FieldType.DATE);
        nai_Finl_Prtl_Py_Dte.setParameterOption(ParameterOption.ByReference);
        nai_Pymnt_Mode = parameters.newFieldInRecord("nai_Pymnt_Mode", "NAI-PYMNT-MODE", FieldType.NUMERIC, 3);
        nai_Pymnt_Mode.setParameterOption(ParameterOption.ByReference);
        nai_Dtl_Tiaa_Ia_Rate_Cd = parameters.newFieldArrayInRecord("nai_Dtl_Tiaa_Ia_Rate_Cd", "NAI-DTL-TIAA-IA-RATE-CD", FieldType.STRING, 2, new DbsArrayController(1, 
            125));
        nai_Dtl_Tiaa_Ia_Rate_Cd.setParameterOption(ParameterOption.ByReference);
        nai_Dtl2_Tiaa_Ia_Rate_Cd = parameters.newFieldArrayInRecord("nai_Dtl2_Tiaa_Ia_Rate_Cd", "NAI-DTL2-TIAA-IA-RATE-CD", FieldType.STRING, 2, new DbsArrayController(1, 
            125));
        nai_Dtl2_Tiaa_Ia_Rate_Cd.setParameterOption(ParameterOption.ByReference);
        nai_Dtl_Tiaa_Std_Grntd_Amt = parameters.newFieldArrayInRecord("nai_Dtl_Tiaa_Std_Grntd_Amt", "NAI-DTL-TIAA-STD-GRNTD-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 250));
        nai_Dtl_Tiaa_Std_Grntd_Amt.setParameterOption(ParameterOption.ByReference);
        nai_Dtl_Tiaa_Std_Dvdnd_Amt = parameters.newFieldArrayInRecord("nai_Dtl_Tiaa_Std_Dvdnd_Amt", "NAI-DTL-TIAA-STD-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 250));
        nai_Dtl_Tiaa_Std_Dvdnd_Amt.setParameterOption(ParameterOption.ByReference);
        cpnd_Nai_Dtl_Tiaa_Data = parameters.newFieldInRecord("cpnd_Nai_Dtl_Tiaa_Data", "C#NAI-DTL-TIAA-DATA", FieldType.PACKED_DECIMAL, 3);
        cpnd_Nai_Dtl_Tiaa_Data.setParameterOption(ParameterOption.ByReference);
        nai_Dtl_Cref_Acct_Cd = parameters.newFieldArrayInRecord("nai_Dtl_Cref_Acct_Cd", "NAI-DTL-CREF-ACCT-CD", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        nai_Dtl_Cref_Acct_Cd.setParameterOption(ParameterOption.ByReference);
        nai_Dtl_Cref_Annl_Nbr_Units = parameters.newFieldArrayInRecord("nai_Dtl_Cref_Annl_Nbr_Units", "NAI-DTL-CREF-ANNL-NBR-UNITS", FieldType.NUMERIC, 
            10, 4, new DbsArrayController(1, 20));
        nai_Dtl_Cref_Annl_Nbr_Units.setParameterOption(ParameterOption.ByReference);
        nai_Dtl_Cref_Annl_Amt = parameters.newFieldArrayInRecord("nai_Dtl_Cref_Annl_Amt", "NAI-DTL-CREF-ANNL-AMT", FieldType.NUMERIC, 11, 2, new DbsArrayController(1, 
            20));
        nai_Dtl_Cref_Annl_Amt.setParameterOption(ParameterOption.ByReference);
        nai_Dtl_Cref_Mnthly_Nbr_Units = parameters.newFieldArrayInRecord("nai_Dtl_Cref_Mnthly_Nbr_Units", "NAI-DTL-CREF-MNTHLY-NBR-UNITS", FieldType.NUMERIC, 
            10, 4, new DbsArrayController(1, 20));
        nai_Dtl_Cref_Mnthly_Nbr_Units.setParameterOption(ParameterOption.ByReference);
        nai_Dtl_Cref_Mnthly_Amt = parameters.newFieldArrayInRecord("nai_Dtl_Cref_Mnthly_Amt", "NAI-DTL-CREF-MNTHLY-AMT", FieldType.NUMERIC, 11, 2, new 
            DbsArrayController(1, 20));
        nai_Dtl_Cref_Mnthly_Amt.setParameterOption(ParameterOption.ByReference);
        nap_Alt_Dest_Rlvr_Dest = parameters.newFieldInRecord("nap_Alt_Dest_Rlvr_Dest", "NAP-ALT-DEST-RLVR-DEST", FieldType.STRING, 4);
        nap_Alt_Dest_Rlvr_Dest.setParameterOption(ParameterOption.ByReference);
        nap_Ivc_Pymnt_Rule = parameters.newFieldInRecord("nap_Ivc_Pymnt_Rule", "NAP-IVC-PYMNT-RULE", FieldType.STRING, 1);
        nap_Ivc_Pymnt_Rule.setParameterOption(ParameterOption.ByReference);
        nap_Ivc_Rlvr_Cde = parameters.newFieldInRecord("nap_Ivc_Rlvr_Cde", "NAP-IVC-RLVR-CDE", FieldType.STRING, 1);
        nap_Ivc_Rlvr_Cde.setParameterOption(ParameterOption.ByReference);
        nac_Trk_Ro_Ivc = parameters.newFieldInRecord("nac_Trk_Ro_Ivc", "NAC-TRK-RO-IVC", FieldType.STRING, 1);
        nac_Trk_Ro_Ivc.setParameterOption(ParameterOption.ByReference);
        tiaa_Ivc_Amt = parameters.newFieldInRecord("tiaa_Ivc_Amt", "TIAA-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        tiaa_Ivc_Amt.setParameterOption(ParameterOption.ByReference);
        cref_Ivc_Amt = parameters.newFieldInRecord("cref_Ivc_Amt", "CREF-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        cref_Ivc_Amt.setParameterOption(ParameterOption.ByReference);
        per_Ivc_Amt = parameters.newFieldInRecord("per_Ivc_Amt", "PER-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        per_Ivc_Amt.setParameterOption(ParameterOption.ByReference);
        pnd_Err_Cde = parameters.newFieldInRecord("pnd_Err_Cde", "#ERR-CDE", FieldType.STRING, 3);
        pnd_Err_Cde.setParameterOption(ParameterOption.ByReference);
        pnd_More = parameters.newFieldInRecord("pnd_More", "#MORE", FieldType.BOOLEAN, 1);
        pnd_More.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Max_Rslt = localVariables.newFieldInRecord("pnd_Max_Rslt", "#MAX-RSLT", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Rate = localVariables.newFieldInRecord("pnd_Max_Rate", "#MAX-RATE", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_Cal_Date = localVariables.newFieldInRecord("pnd_Cal_Date", "#CAL-DATE", FieldType.STRING, 8);

        pnd_Cal_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Cal_Date__R_Field_1", "REDEFINE", pnd_Cal_Date);
        pnd_Cal_Date_Pnd_Cal_Date_N = pnd_Cal_Date__R_Field_1.newFieldInGroup("pnd_Cal_Date_Pnd_Cal_Date_N", "#CAL-DATE-N", FieldType.NUMERIC, 8);

        pnd_Cal_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Cal_Date__R_Field_2", "REDEFINE", pnd_Cal_Date);
        pnd_Cal_Date__Filler1 = pnd_Cal_Date__R_Field_2.newFieldInGroup("pnd_Cal_Date__Filler1", "_FILLER1", FieldType.STRING, 6);
        pnd_Cal_Date_Pnd_Day = pnd_Cal_Date__R_Field_2.newFieldInGroup("pnd_Cal_Date_Pnd_Day", "#DAY", FieldType.NUMERIC, 2);
        pnd_W_Date = localVariables.newFieldInRecord("pnd_W_Date", "#W-DATE", FieldType.STRING, 6);

        pnd_W_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_W_Date__R_Field_3", "REDEFINE", pnd_W_Date);
        pnd_W_Date_Pnd_W_Date_N = pnd_W_Date__R_Field_3.newFieldInGroup("pnd_W_Date_Pnd_W_Date_N", "#W-DATE-N", FieldType.NUMERIC, 6);
        pnd_W_Date2 = localVariables.newFieldInRecord("pnd_W_Date2", "#W-DATE2", FieldType.STRING, 8);

        pnd_W_Date2__R_Field_4 = localVariables.newGroupInRecord("pnd_W_Date2__R_Field_4", "REDEFINE", pnd_W_Date2);
        pnd_W_Date2_Pnd_W_Date_N2 = pnd_W_Date2__R_Field_4.newFieldInGroup("pnd_W_Date2_Pnd_W_Date_N2", "#W-DATE-N2", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Max_Rslt.setInitialValue(125);
        pnd_Max_Rate.setInitialValue(250);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn606() throws Exception
    {
        super("Adsn606");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Teachers_Or_Cref().setValue(pnd_Ivc_Call);                                                                                  //Natural: ASSIGN IVC-TEACHERS-OR-CREF := #IVC-CALL
        //*  031512 START
        //* *MOVE EDITED NAI-ANNTY-STRT-DTE(EM=YYYYMM) TO ANNTY-STRT-DTE
        pnd_W_Date.setValueEdited(nai_Annty_Strt_Dte,new ReportEditMask("YYYYMM"));                                                                                       //Natural: MOVE EDITED NAI-ANNTY-STRT-DTE ( EM = YYYYMM ) TO #W-DATE
        pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Issue_Date().setValue(pnd_W_Date_Pnd_W_Date_N);                                                                             //Natural: ASSIGN IVC-ISSUE-DATE := #W-DATE-N
        //* *MOVE EDITED PRCSS-DTE(EM=YYYYMMDD) TO TDYS-DTE
        pnd_W_Date2.setValueEdited(prcss_Dte,new ReportEditMask("YYYYMMDD"));                                                                                             //Natural: MOVE EDITED PRCSS-DTE ( EM = YYYYMMDD ) TO #W-DATE2
        pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Value_Date().setValue(pnd_W_Date2_Pnd_W_Date_N2);                                                                           //Natural: ASSIGN IVC-VALUE-DATE := #W-DATE-N2
        //* *#CAL-DATE := TDYS-DTE
        pnd_Cal_Date.setValue(pnd_W_Date2);                                                                                                                               //Natural: ASSIGN #CAL-DATE := #W-DATE2
        pnd_Cal_Date_Pnd_Day.setValue(20);                                                                                                                                //Natural: ASSIGN #DAY := 20
                                                                                                                                                                          //Natural: PERFORM DETERMINE-CUTOFF-DAY
        sub_Determine_Cutoff_Day();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Err_Cde.notEquals(" ")))                                                                                                                        //Natural: IF #ERR-CDE NE ' '
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ADJUST IVC VALUE DATE - ENSURE IT's always the beginning of month
        //*  = 20 OR LESS IF 20TH NOT A BUS DAY
        if (condition(pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Value_Day().greaterOrEqual(2) && pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Value_Day().lessOrEqual(pnd_Cal_Date_Pnd_Day))) //Natural: IF IVC-VALUE-DAY = 2 THRU #DAY
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  PAST THE CUTOFF
            pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Value_Mo().nadd(1);                                                                                                     //Natural: ADD 1 TO IVC-VALUE-MO
            if (condition(pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Value_Mo().greater(12)))                                                                                  //Natural: IF IVC-VALUE-MO GT 12
            {
                pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Value_Mo().setValue(1);                                                                                             //Natural: ASSIGN IVC-VALUE-MO := 1
                pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Value_Yr().nadd(1);                                                                                                 //Natural: ADD 1 TO IVC-VALUE-YR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Value_Day().setValue(1);                                                                                                    //Natural: ASSIGN IVC-VALUE-DAY := 01
        //*  END OF IVC VALUE DATE CODE
        //*  031512 END
        if (condition(pnd_Ivc_Call.equals("T")))                                                                                                                          //Natural: IF #IVC-CALL = 'T'
        {
            //* *IVC-CNTRCT-NBR := NAP-IA-TIAA-NBR
            pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Contract_Payee().setValue(nap_Ia_Tiaa_Nbr);                                                                             //Natural: ASSIGN IVC-CONTRACT-PAYEE := NAP-IA-TIAA-NBR
            //*  RTE-CDE(1:99) := NAI-DTL-TIAA-IA-RATE-CD(1:99)
            pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Rate().getValue(1,":",pnd_Max_Rslt).setValue(nai_Dtl_Tiaa_Ia_Rate_Cd.getValue(1,":",pnd_Max_Rslt));                     //Natural: ASSIGN IVC-RATE ( 1:#MAX-RSLT ) := NAI-DTL-TIAA-IA-RATE-CD ( 1:#MAX-RSLT )
            pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Rate().getValue(pnd_Max_Rslt.getDec().add(1),":",pnd_Max_Rate).setValue(nai_Dtl2_Tiaa_Ia_Rate_Cd.getValue(1,            //Natural: ASSIGN IVC-RATE ( #MAX-RSLT + 1:#MAX-RATE ) := NAI-DTL2-TIAA-IA-RATE-CD ( 1:#MAX-RSLT )
                ":",pnd_Max_Rslt));
            pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Rate_Guar_Payment().getValue(1,":",pnd_Max_Rate).setValue(nai_Dtl_Tiaa_Std_Grntd_Amt.getValue(1,":",                    //Natural: ASSIGN IVC-RATE-GUAR-PAYMENT ( 1:#MAX-RATE ) := NAI-DTL-TIAA-STD-GRNTD-AMT ( 1:#MAX-RATE )
                pnd_Max_Rate));
            pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Rate_Div_Payment().getValue(1,":",pnd_Max_Rate).setValue(nai_Dtl_Tiaa_Std_Dvdnd_Amt.getValue(1,":",pnd_Max_Rate));      //Natural: ASSIGN IVC-RATE-DIV-PAYMENT ( 1:#MAX-RATE ) := NAI-DTL-TIAA-STD-DVDND-AMT ( 1:#MAX-RATE )
            pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Number_Of_Rates().setValue(cpnd_Nai_Dtl_Tiaa_Data);                                                                     //Natural: ASSIGN IVC-NUMBER-OF-RATES := C#NAI-DTL-TIAA-DATA
            pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Remaining_Amount().setValue(tiaa_Ivc_Amt);                                                                              //Natural: ASSIGN IVC-REMAINING-AMOUNT := TIAA-IVC-AMT
            //*  031512
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //* *IVC-CNTRCT-NBR := NAP-IA-CREF-NBR
            pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Contract_Payee().setValue(nap_Ia_Cref_Nbr);                                                                             //Natural: ASSIGN IVC-CONTRACT-PAYEE := NAP-IA-CREF-NBR
            pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Remaining_Amount().setValue(cref_Ivc_Amt);                                                                              //Natural: ASSIGN IVC-REMAINING-AMOUNT := CREF-IVC-AMT
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 TO 20
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
            {
                if (condition(nai_Dtl_Cref_Acct_Cd.getValue(pnd_I).notEquals(" ")))                                                                                       //Natural: IF NAI-DTL-CREF-ACCT-CD ( #I ) NE ' '
                {
                    if (condition(nai_Dtl_Cref_Annl_Nbr_Units.getValue(pnd_I).greater(getZero())))                                                                        //Natural: IF NAI-DTL-CREF-ANNL-NBR-UNITS ( #I ) GT 0
                    {
                        //*  031512
                        pnd_J.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #J
                        //*        IVC-CREF-FUND-CDE(#J) := NAI-DTL-CREF-ACCT-CD(#I)
                        pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Cref_Fund_Code().getValue(pnd_J).setValue(nai_Dtl_Cref_Acct_Cd.getValue(pnd_I));                            //Natural: ASSIGN IVC-CREF-FUND-CODE ( #J ) := NAI-DTL-CREF-ACCT-CD ( #I )
                        pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Cref_Units().getValue(pnd_J).setValue(nai_Dtl_Cref_Annl_Nbr_Units.getValue(pnd_I));                         //Natural: ASSIGN IVC-CREF-UNITS ( #J ) := NAI-DTL-CREF-ANNL-NBR-UNITS ( #I )
                        pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Cref_Reval().getValue(pnd_J).setValue("A");                                                                 //Natural: ASSIGN IVC-CREF-REVAL ( #J ) := 'A'
                        pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Cref_Payment().getValue(pnd_J).setValue(nai_Dtl_Cref_Annl_Amt.getValue(pnd_I));                             //Natural: ASSIGN IVC-CREF-PAYMENT ( #J ) := NAI-DTL-CREF-ANNL-AMT ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(nai_Dtl_Cref_Mnthly_Nbr_Units.getValue(pnd_I).greater(getZero())))                                                                      //Natural: IF NAI-DTL-CREF-MNTHLY-NBR-UNITS ( #I ) GT 0
                    {
                        //*  031512
                        pnd_J.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #J
                        //*        IVC-CREF-FUND-CDE(#J) := NAI-DTL-CREF-ACCT-CD(#I)
                        pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Cref_Fund_Code().getValue(pnd_J).setValue(nai_Dtl_Cref_Acct_Cd.getValue(pnd_I));                            //Natural: ASSIGN IVC-CREF-FUND-CODE ( #J ) := NAI-DTL-CREF-ACCT-CD ( #I )
                        pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Cref_Reval().getValue(pnd_J).setValue("M");                                                                 //Natural: ASSIGN IVC-CREF-REVAL ( #J ) := 'M'
                        pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Cref_Payment().getValue(pnd_J).setValue(nai_Dtl_Cref_Mnthly_Amt.getValue(pnd_I));                           //Natural: ASSIGN IVC-CREF-PAYMENT ( #J ) := NAI-DTL-CREF-MNTHLY-AMT ( #I )
                        pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Cref_Units().getValue(pnd_J).setValue(nai_Dtl_Cref_Mnthly_Nbr_Units.getValue(pnd_I));                       //Natural: ASSIGN IVC-CREF-UNITS ( #J ) := NAI-DTL-CREF-MNTHLY-NBR-UNITS ( #I )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *IVC-PYEE-CDE := '01'                           /* 031512 START
        pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Contract_Payee().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Contract_Payee(),  //Natural: COMPRESS IVC-CONTRACT-PAYEE '01' INTO IVC-CONTRACT-PAYEE LEAVING NO
            "01"));
        //* *MOVE EDITED NAI-FINL-PRTL-PY-DTE(EM=YYYYMM) TO IVC-FINAL-PER-DTE
        //*  031512 END
        pnd_W_Date.setValueEdited(nai_Finl_Prtl_Py_Dte,new ReportEditMask("YYYYMM"));                                                                                     //Natural: MOVE EDITED NAI-FINL-PRTL-PY-DTE ( EM = YYYYMM ) TO #W-DATE
        pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Final_Date().setValue(pnd_W_Date_Pnd_W_Date_N);                                                                             //Natural: ASSIGN IVC-FINAL-DATE := #W-DATE-N
        pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Mode().setValue(nai_Pymnt_Mode);                                                                                            //Natural: ASSIGN IVC-MODE := NAI-PYMNT-MODE
        pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Calc_Method().setValue(nap_Ivc_Pymnt_Rule);                                                                                 //Natural: ASSIGN IVC-CALC-METHOD := NAP-IVC-PYMNT-RULE
        //* *CALLNAT 'AIAN085' AIAN085-LINKAGE              /* 031512 START
        DbsUtil.terminateApplication("Program AIAN085 is missing from the collection!");                                                                                  //Natural: CALLNAT 'AIAN085' #AIAA085-LINKAGE
        if (condition(Global.isEscape())) return;
        //* * IVC-RETURN-CODE-ALPHA = 0                     /* 031512 END
        if (condition(pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Return_Code_Nbr().equals(getZero())))                                                                         //Natural: IF IVC-RETURN-CODE-NBR = 0
        {
            if (condition(pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Calc_Method().equals("P")))                                                                               //Natural: IF IVC-CALC-METHOD = 'P'
            {
                per_Ivc_Amt.setValue(pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Amount());                                                                                     //Natural: ASSIGN PER-IVC-AMT := IVC-AMOUNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Begin_Date().lessOrEqual(pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Value_Date())))                        //Natural: IF IVC-BEGIN-DATE LE IVC-VALUE-DATE
                {
                    per_Ivc_Amt.setValue(pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Amount());                                                                                 //Natural: ASSIGN PER-IVC-AMT := IVC-AMOUNT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  031512 START
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  MOVE EDITED IVC-RETURN-CODE-ALPHA (EM=Z99) TO #ERR-CDE
            pnd_Err_Cde.setValueEdited(pdaAiaa085.getPnd_Aiaa085_Linkage_Ivc_Return_Code_Nbr(),new ReportEditMask("Z99"));                                                //Natural: MOVE EDITED IVC-RETURN-CODE-NBR ( EM = Z99 ) TO #ERR-CDE
            //*  031512 END
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-CUTOFF-DAY
    }
    private void sub_Determine_Cutoff_Day() throws Exception                                                                                                              //Natural: DETERMINE-CUTOFF-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            pdaNeca4000.getNeca4000().reset();                                                                                                                            //Natural: RESET NECA4000
            pdaNeca4000.getNeca4000_Function_Cde().setValue("CAL");                                                                                                       //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'CAL'
            pdaNeca4000.getNeca4000_Cal_Key_For_Dte().setValue(pnd_Cal_Date_Pnd_Cal_Date_N);                                                                              //Natural: ASSIGN NECA4000.CAL-KEY-FOR-DTE := #CAL-DATE-N
            DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                        //Natural: CALLNAT 'NECN4000' NECA4000
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            if (condition(pdaNeca4000.getNeca4000_Cal_Business_Day_Ind().getValue(1).equals("Y")))                                                                        //Natural: IF NECA4000.CAL-BUSINESS-DAY-IND ( 1 ) = 'Y'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cal_Date_Pnd_Day.nsubtract(1);                                                                                                                        //Natural: SUBTRACT 1 FROM #DAY
                if (condition(pnd_Cal_Date_Pnd_Day.lessOrEqual(getZero())))                                                                                               //Natural: IF #DAY LE 0
                {
                    pnd_Err_Cde.setValue("L31");                                                                                                                          //Natural: ASSIGN #ERR-CDE := 'L31'
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }

    //
}
