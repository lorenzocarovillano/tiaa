/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:54:07 PM
**        * FROM NATURAL SUBPROGRAM : Adsn403
************************************************************
**        * FILE NAME            : Adsn403.java
**        * CLASS NAME           : Adsn403
**        * INSTANCE NAME        : Adsn403
************************************************************
************************************************************************
* PROGRAM  : NAZN412
* SYSTEM   : NEW ANNUITIZATION SYSTEM
* TITLE    : ACCESS NAS-NAME-ADDRESS
* FUNCTION : THIS SUBPROGRAM READS THE NAM FILE TO CHECK IF THE NAM
*            ADDR CHANGE IS WITHIN 14 DAYS OF THE PROCESSING DATE
*            OF THE RQST.  IF IT IS, A HOLD WILL BE PLACED ON THE
*            SETTLEMENT.
*
* MOD DATE   MOD BY       DESCRIPTION OF CHANGES
* 09/06/2005 M.NACHBER    REL 6.5 FOR IPRO/TPA
*                         NO CHANGES, JUST RECOMPLILED TO PICK UP
*                         A NEW VERSION OF ADSA401
* 09/08/2006 E.MELNIK     AS PER IM288752, HOLD CHECK PAYMENTS FOR
*                         FOREIGN RESIDENCES. CHANGES INDICATED BY
*                         060908.
* 03/17/2008 E.MELNIK     ROTH 403B/401K CHANGES.  RECOMPILED FOR
*                         NEW ADSA401.
* 07/10/2009 C.MASON      CHANGES FOR MDM CONVERSION
* 03/04/2010 C.MASON      CHANGE CALL FROM MDMN200 TO MDMN200A
* 02/29/2012 O.SOTTO      RECOMPILED FOR RATE EXPANSION.
* 09/24/2013 E.MELNIK     RECOMPILED FOR NEW ADSA401.  CREF/REA REDESIGN
*                         CHANGES.
* 02/15/2017 R.CARREON    PIN EXPANSION 02152017
* 10/24/2018 E.MELNIK     CANZ HOLD CODE - BYPASS CHECKING ADDRESS
*                         LAST UPDATE DATE OF ALL IA CONTRACTS.
*                         MARKED BY 102418.
* 04/12/2019 E. MELNIK    RESTOWED FOR UPDATED ADSA401 WITH IRC CDE.
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn403 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAdsa401 pdaAdsa401;
    private PdaMdma201 pdaMdma201;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_All_Tiaa_Contracts;
    private DbsField pnd_All_Cref_Contracts;
    private DbsField pnd_Address_Found;
    private DbsField pnd_Max_Cntrct;
    private DbsField pnd_Max_Days;
    private DbsField pnd_A;
    private DbsField pnd_I;
    private DbsField pnd_Mdm;
    private DbsField pnd_Save_Lst_Chg;

    private DbsGroup pnd_Save_Lst_Chg__R_Field_1;

    private DbsGroup pnd_Save_Lst_Chg_Pnd_Save_Lst_Chg_R;
    private DbsField pnd_Save_Lst_Chg_Pnd_Lst_Chg_Yyyy;
    private DbsField pnd_Save_Lst_Chg_Pnd_Lst_Chg_Mnth;
    private DbsField pnd_Save_Lst_Chg_Pnd_Lst_Chg_Day;
    private DbsField pnd_Forms_Rcvd_Dte_A;

    private DbsGroup pnd_Forms_Rcvd_Dte_A__R_Field_2;
    private DbsField pnd_Forms_Rcvd_Dte_A_Pnd_Forms_Rcvd_Dte_N;

    private DbsGroup pnd_Forms_Rcvd_Dte_A__R_Field_3;
    private DbsField pnd_Forms_Rcvd_Dte_A_Pnd_Forms_Year;
    private DbsField pnd_Forms_Rcvd_Dte_A_Pnd_Forms_Mnth;
    private DbsField pnd_Forms_Rcvd_Dte_A_Pnd_Forms_Day;
    private DbsField pnd_Processing_Dte_A;

    private DbsGroup pnd_Processing_Dte_A__R_Field_4;
    private DbsField pnd_Processing_Dte_A_Pnd_Processing_Dte_N;

    private DbsGroup pnd_Processing_Dte_A__R_Field_5;
    private DbsField pnd_Processing_Dte_A_Pnd_Processing_Year;
    private DbsField pnd_Processing_Dte_A_Pnd_Processing_Mnth;
    private DbsField pnd_Processing_Dte_A_Pnd_Processing_Day;
    private DbsField pnd_Difference;
    private DbsField pnd_Result;
    private DbsField pnd_Rmndr;
    private DbsField pnd_Mnth_Table;

    private DbsGroup pnd_Mnth_Table__R_Field_6;
    private DbsField pnd_Mnth_Table_Pnd_Mnth_Tbl;
    private DbsField pnd_Day_Table;

    private DbsGroup pnd_Day_Table__R_Field_7;
    private DbsField pnd_Day_Table_Pnd_Day_Tbl;
    private DbsField pnd_Pin_Cntrct_Key;

    private DbsGroup pnd_Pin_Cntrct_Key__R_Field_8;

    private DbsGroup pnd_Pin_Cntrct_Key_Pnd_Pin_Cntrct_Key_R;
    private DbsField pnd_Pin_Cntrct_Key_Pnd_Pin_Nbr;
    private DbsField pnd_Pin_Cntrct_Key_Pnd_Cntrct_Nbr;
    private DbsField pnd_V_Corr_Cntrct_Payee_Key;
    private DbsField pnd_Crrspndce_Type_Zip;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaMdma201 = new PdaMdma201(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaAdsa401 = new PdaAdsa401(parameters);
        pnd_All_Tiaa_Contracts = parameters.newFieldArrayInRecord("pnd_All_Tiaa_Contracts", "#ALL-TIAA-CONTRACTS", FieldType.STRING, 10, new DbsArrayController(1, 
            12));
        pnd_All_Tiaa_Contracts.setParameterOption(ParameterOption.ByReference);
        pnd_All_Cref_Contracts = parameters.newFieldArrayInRecord("pnd_All_Cref_Contracts", "#ALL-CREF-CONTRACTS", FieldType.STRING, 10, new DbsArrayController(1, 
            12));
        pnd_All_Cref_Contracts.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Address_Found = localVariables.newFieldInRecord("pnd_Address_Found", "#ADDRESS-FOUND", FieldType.BOOLEAN, 1);
        pnd_Max_Cntrct = localVariables.newFieldInRecord("pnd_Max_Cntrct", "#MAX-CNTRCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Days = localVariables.newFieldInRecord("pnd_Max_Days", "#MAX-DAYS", FieldType.PACKED_DECIMAL, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Mdm = localVariables.newFieldInRecord("pnd_Mdm", "#MDM", FieldType.PACKED_DECIMAL, 3);
        pnd_Save_Lst_Chg = localVariables.newFieldInRecord("pnd_Save_Lst_Chg", "#SAVE-LST-CHG", FieldType.NUMERIC, 8);

        pnd_Save_Lst_Chg__R_Field_1 = localVariables.newGroupInRecord("pnd_Save_Lst_Chg__R_Field_1", "REDEFINE", pnd_Save_Lst_Chg);

        pnd_Save_Lst_Chg_Pnd_Save_Lst_Chg_R = pnd_Save_Lst_Chg__R_Field_1.newGroupInGroup("pnd_Save_Lst_Chg_Pnd_Save_Lst_Chg_R", "#SAVE-LST-CHG-R");
        pnd_Save_Lst_Chg_Pnd_Lst_Chg_Yyyy = pnd_Save_Lst_Chg_Pnd_Save_Lst_Chg_R.newFieldInGroup("pnd_Save_Lst_Chg_Pnd_Lst_Chg_Yyyy", "#LST-CHG-YYYY", 
            FieldType.NUMERIC, 4);
        pnd_Save_Lst_Chg_Pnd_Lst_Chg_Mnth = pnd_Save_Lst_Chg_Pnd_Save_Lst_Chg_R.newFieldInGroup("pnd_Save_Lst_Chg_Pnd_Lst_Chg_Mnth", "#LST-CHG-MNTH", 
            FieldType.NUMERIC, 2);
        pnd_Save_Lst_Chg_Pnd_Lst_Chg_Day = pnd_Save_Lst_Chg_Pnd_Save_Lst_Chg_R.newFieldInGroup("pnd_Save_Lst_Chg_Pnd_Lst_Chg_Day", "#LST-CHG-DAY", FieldType.NUMERIC, 
            2);
        pnd_Forms_Rcvd_Dte_A = localVariables.newFieldInRecord("pnd_Forms_Rcvd_Dte_A", "#FORMS-RCVD-DTE-A", FieldType.STRING, 8);

        pnd_Forms_Rcvd_Dte_A__R_Field_2 = localVariables.newGroupInRecord("pnd_Forms_Rcvd_Dte_A__R_Field_2", "REDEFINE", pnd_Forms_Rcvd_Dte_A);
        pnd_Forms_Rcvd_Dte_A_Pnd_Forms_Rcvd_Dte_N = pnd_Forms_Rcvd_Dte_A__R_Field_2.newFieldInGroup("pnd_Forms_Rcvd_Dte_A_Pnd_Forms_Rcvd_Dte_N", "#FORMS-RCVD-DTE-N", 
            FieldType.NUMERIC, 8);

        pnd_Forms_Rcvd_Dte_A__R_Field_3 = pnd_Forms_Rcvd_Dte_A__R_Field_2.newGroupInGroup("pnd_Forms_Rcvd_Dte_A__R_Field_3", "REDEFINE", pnd_Forms_Rcvd_Dte_A_Pnd_Forms_Rcvd_Dte_N);
        pnd_Forms_Rcvd_Dte_A_Pnd_Forms_Year = pnd_Forms_Rcvd_Dte_A__R_Field_3.newFieldInGroup("pnd_Forms_Rcvd_Dte_A_Pnd_Forms_Year", "#FORMS-YEAR", FieldType.NUMERIC, 
            4);
        pnd_Forms_Rcvd_Dte_A_Pnd_Forms_Mnth = pnd_Forms_Rcvd_Dte_A__R_Field_3.newFieldInGroup("pnd_Forms_Rcvd_Dte_A_Pnd_Forms_Mnth", "#FORMS-MNTH", FieldType.NUMERIC, 
            2);
        pnd_Forms_Rcvd_Dte_A_Pnd_Forms_Day = pnd_Forms_Rcvd_Dte_A__R_Field_3.newFieldInGroup("pnd_Forms_Rcvd_Dte_A_Pnd_Forms_Day", "#FORMS-DAY", FieldType.NUMERIC, 
            2);
        pnd_Processing_Dte_A = localVariables.newFieldInRecord("pnd_Processing_Dte_A", "#PROCESSING-DTE-A", FieldType.STRING, 8);

        pnd_Processing_Dte_A__R_Field_4 = localVariables.newGroupInRecord("pnd_Processing_Dte_A__R_Field_4", "REDEFINE", pnd_Processing_Dte_A);
        pnd_Processing_Dte_A_Pnd_Processing_Dte_N = pnd_Processing_Dte_A__R_Field_4.newFieldInGroup("pnd_Processing_Dte_A_Pnd_Processing_Dte_N", "#PROCESSING-DTE-N", 
            FieldType.NUMERIC, 8);

        pnd_Processing_Dte_A__R_Field_5 = pnd_Processing_Dte_A__R_Field_4.newGroupInGroup("pnd_Processing_Dte_A__R_Field_5", "REDEFINE", pnd_Processing_Dte_A_Pnd_Processing_Dte_N);
        pnd_Processing_Dte_A_Pnd_Processing_Year = pnd_Processing_Dte_A__R_Field_5.newFieldInGroup("pnd_Processing_Dte_A_Pnd_Processing_Year", "#PROCESSING-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Processing_Dte_A_Pnd_Processing_Mnth = pnd_Processing_Dte_A__R_Field_5.newFieldInGroup("pnd_Processing_Dte_A_Pnd_Processing_Mnth", "#PROCESSING-MNTH", 
            FieldType.NUMERIC, 2);
        pnd_Processing_Dte_A_Pnd_Processing_Day = pnd_Processing_Dte_A__R_Field_5.newFieldInGroup("pnd_Processing_Dte_A_Pnd_Processing_Day", "#PROCESSING-DAY", 
            FieldType.NUMERIC, 2);
        pnd_Difference = localVariables.newFieldInRecord("pnd_Difference", "#DIFFERENCE", FieldType.NUMERIC, 2);
        pnd_Result = localVariables.newFieldInRecord("pnd_Result", "#RESULT", FieldType.NUMERIC, 4);
        pnd_Rmndr = localVariables.newFieldInRecord("pnd_Rmndr", "#RMNDR", FieldType.NUMERIC, 4);
        pnd_Mnth_Table = localVariables.newFieldInRecord("pnd_Mnth_Table", "#MNTH-TABLE", FieldType.STRING, 24);

        pnd_Mnth_Table__R_Field_6 = localVariables.newGroupInRecord("pnd_Mnth_Table__R_Field_6", "REDEFINE", pnd_Mnth_Table);
        pnd_Mnth_Table_Pnd_Mnth_Tbl = pnd_Mnth_Table__R_Field_6.newFieldArrayInGroup("pnd_Mnth_Table_Pnd_Mnth_Tbl", "#MNTH-TBL", FieldType.NUMERIC, 2, 
            new DbsArrayController(1, 12));
        pnd_Day_Table = localVariables.newFieldInRecord("pnd_Day_Table", "#DAY-TABLE", FieldType.STRING, 24);

        pnd_Day_Table__R_Field_7 = localVariables.newGroupInRecord("pnd_Day_Table__R_Field_7", "REDEFINE", pnd_Day_Table);
        pnd_Day_Table_Pnd_Day_Tbl = pnd_Day_Table__R_Field_7.newFieldArrayInGroup("pnd_Day_Table_Pnd_Day_Tbl", "#DAY-TBL", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            12));
        pnd_Pin_Cntrct_Key = localVariables.newFieldInRecord("pnd_Pin_Cntrct_Key", "#PIN-CNTRCT-KEY", FieldType.STRING, 22);

        pnd_Pin_Cntrct_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Pin_Cntrct_Key__R_Field_8", "REDEFINE", pnd_Pin_Cntrct_Key);

        pnd_Pin_Cntrct_Key_Pnd_Pin_Cntrct_Key_R = pnd_Pin_Cntrct_Key__R_Field_8.newGroupInGroup("pnd_Pin_Cntrct_Key_Pnd_Pin_Cntrct_Key_R", "#PIN-CNTRCT-KEY-R");
        pnd_Pin_Cntrct_Key_Pnd_Pin_Nbr = pnd_Pin_Cntrct_Key_Pnd_Pin_Cntrct_Key_R.newFieldInGroup("pnd_Pin_Cntrct_Key_Pnd_Pin_Nbr", "#PIN-NBR", FieldType.NUMERIC, 
            12);
        pnd_Pin_Cntrct_Key_Pnd_Cntrct_Nbr = pnd_Pin_Cntrct_Key_Pnd_Pin_Cntrct_Key_R.newFieldInGroup("pnd_Pin_Cntrct_Key_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", 
            FieldType.STRING, 10);
        pnd_V_Corr_Cntrct_Payee_Key = localVariables.newFieldInRecord("pnd_V_Corr_Cntrct_Payee_Key", "#V-CORR-CNTRCT-PAYEE-KEY", FieldType.STRING, 13);
        pnd_Crrspndce_Type_Zip = localVariables.newFieldInRecord("pnd_Crrspndce_Type_Zip", "#CRRSPNDCE-TYPE-ZIP", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Max_Cntrct.setInitialValue(12);
        pnd_Max_Days.setInitialValue(14);
        pnd_Mnth_Table.setInitialValue("010203040506070809101112");
        pnd_Day_Table.setInitialValue("312831303130313130313031");
        pnd_Pin_Cntrct_Key.setInitialValue(" ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn403() throws Exception
    {
        super("Adsn403");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* ******************** START OF MAIN PROGRAM LOGIC **********************
        //* * REPLACE READ OF ADDRESS FILE PER MDM CONVERSION
        FOR01:                                                                                                                                                            //Natural: FOR #A 1 #MAX-CNTRCT
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Cntrct)); pnd_A.nadd(1))
        {
            if (condition(pnd_All_Tiaa_Contracts.getValue(pnd_A).equals(" ")))                                                                                            //Natural: IF #ALL-TIAA-CONTRACTS ( #A ) EQ ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pdaMdma201.getPnd_Mdma201_Pnd_I_Pin_N12().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Unique_Id());                                                                //Natural: ASSIGN #I-PIN-N12 := ADP-UNIQUE-ID
            //*  END PIN EXPANSION 02152017
            DbsUtil.callnat(Mdmn201a.class , getCurrentProcessState(), pdaMdma201.getPnd_Mdma201());                                                                      //Natural: CALLNAT 'MDMN201A' #MDMA201
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            if (condition(pdaMdma201.getPnd_Mdma201_Pnd_O_Return_Code().notEquals("0000")))                                                                               //Natural: IF #O-RETURN-CODE NE '0000'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                FOR02:                                                                                                                                                    //Natural: FOR #MDM = 1 TO 50
                for (pnd_Mdm.setValue(1); condition(pnd_Mdm.lessOrEqual(50)); pnd_Mdm.nadd(1))
                {
                    if (condition(pdaMdma201.getPnd_Mdma201_Pnd_O_Contract_Number().getValue(pnd_Mdm).equals(" ")))                                                       //Natural: IF #O-CONTRACT-NUMBER ( #MDM ) EQ ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                        //*  102418 - START
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(DbsUtil.maskMatches(pdaMdma201.getPnd_Mdma201_Pnd_O_Contract_Number().getValue(pnd_Mdm),"'Y'") || DbsUtil.maskMatches(pdaMdma201.getPnd_Mdma201_Pnd_O_Contract_Number().getValue(pnd_Mdm),"'Z'")  //Natural: IF ( #O-CONTRACT-NUMBER ( #MDM ) = MASK ( 'Y' ) OR = MASK ( 'Z' ) OR = MASK ( 'S' ) OR = MASK ( 'W' ) OR = MASK ( '0' ) OR = MASK ( 'G' ) )
                            || DbsUtil.maskMatches(pdaMdma201.getPnd_Mdma201_Pnd_O_Contract_Number().getValue(pnd_Mdm),"'S'") || DbsUtil.maskMatches(pdaMdma201.getPnd_Mdma201_Pnd_O_Contract_Number().getValue(pnd_Mdm),"'W'") 
                            || DbsUtil.maskMatches(pdaMdma201.getPnd_Mdma201_Pnd_O_Contract_Number().getValue(pnd_Mdm),"'0'") || DbsUtil.maskMatches(pdaMdma201.getPnd_Mdma201_Pnd_O_Contract_Number().getValue(pnd_Mdm),
                            "'G'")))
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                            //*  102418 - END
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  PERMANENT
                    if (condition(pdaMdma201.getPnd_Mdma201_Pnd_O_Address_Permanent_Ind().getValue(pnd_Mdm).equals("P")))                                                 //Natural: IF #O-ADDRESS-PERMANENT-IND ( #MDM ) EQ 'P'
                    {
                        if (condition(pdaMdma201.getPnd_Mdma201_Pnd_O_Address_Status_Code().getValue(pnd_Mdm).equals("R") || pdaMdma201.getPnd_Mdma201_Pnd_O_Address_Status_Code().getValue(pnd_Mdm).equals(" ")  //Natural: IF ( #O-ADDRESS-STATUS-CODE ( #MDM ) EQ 'R' OR EQ ' ' ) OR #O-CONTRACT-STATUS-CODE ( #MDM ) EQ ' '
                            || pdaMdma201.getPnd_Mdma201_Pnd_O_Contract_Status_Code().getValue(pnd_Mdm).equals(" ")))
                        {
                            if (condition(pdaMdma201.getPnd_Mdma201_Pnd_O_Address_Last_Update_Date().getValue(pnd_Mdm).greater(pnd_Save_Lst_Chg)))                        //Natural: IF #O-ADDRESS-LAST-UPDATE-DATE ( #MDM ) GT #SAVE-LST-CHG
                            {
                                pnd_Save_Lst_Chg.setValue(pdaMdma201.getPnd_Mdma201_Pnd_O_Address_Last_Update_Date().getValue(pnd_Mdm));                                  //Natural: ASSIGN #SAVE-LST-CHG := #O-ADDRESS-LAST-UPDATE-DATE ( #MDM )
                                pnd_Crrspndce_Type_Zip.setValue(pdaMdma201.getPnd_Mdma201_Pnd_O_Address_Type_Code().getValue(pnd_Mdm));                                   //Natural: ASSIGN #CRRSPNDCE-TYPE-ZIP := #O-ADDRESS-TYPE-CODE ( #MDM )
                                pnd_Address_Found.setValue(true);                                                                                                         //Natural: ASSIGN #ADDRESS-FOUND := TRUE
                                //*  ADDRESS LAST UPDATE
                            }                                                                                                                                             //Natural: END-IF
                            //*  ADDRESS STATUS
                        }                                                                                                                                                 //Natural: END-IF
                        //*  PERMANENT ADDRESS
                    }                                                                                                                                                     //Natural: END-IF
                    //*  MDM FOR LOOP
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  RETURN CODE
            }                                                                                                                                                             //Natural: END-IF
            //*  MAX CONTRACT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* * COMMENTED OUT PER MDM CONVERSION
        //* *  READ NAS-NAME-ADDRESS BY PIN-CNTRCT-KEY
        //* *      STARTING FROM #PIN-CNTRCT-KEY
        //* *    IF NAS-NAME-ADDRESS.PH-UNQUE-ID-NMBR NE #PIN-NBR
        //* *        OR NAS-NAME-ADDRESS.CNTRCT-NMBR NE #ALL-TIAA-CONTRACTS (#A)
        //* *      ESCAPE BOTTOM
        //* *    END-IF
        //* *    REJECT IF RCRD-STATUS-CDE EQ 'D' OR
        //* *      ADDRSS-STATUS-CDE EQ 'B'
        //* *    ACCEPT IF PERMANENT-ADDRSS-IND EQ 'P' /* PERMANENT
        //* *    IF (ADDRSS-STATUS-CDE EQ 'R' OR EQ ' ') OR
        //* *        CNTRCT-STATUS-CDE EQ ' ' OR
        //* *        CORRESPONDENCE-ADDRSS-IND EQ 'O'
        //* *      IF ADDRSS-LAST-CHNGE-DTE GT #SAVE-LST-CHG
        //* *        #SAVE-LST-CHG := ADDRSS-LAST-CHNGE-DTE
        //* *        #CRRSPNDCE-TYPE-ZIP := ADDRSS-TYPE-CDE
        //* *        #ADDRESS-FOUND := TRUE
        //* *      END-IF
        //* *    END-IF
        //* *  END-READ
        //* * IF NOT #ADDRESS-FOUND
        //* *  FOR #A 1 #MAX-CNTRCT
        //* *    IF #ALL-TIAA-CONTRACTS (#A) EQ ' '
        //* *      ESCAPE BOTTOM
        //* *    END-IF
        //* *    COMPRESS 'O' #ALL-TIAA-CONTRACTS (#A)
        //* *      INTO #V-CORR-CNTRCT-PAYEE-KEY LEAVING NO SPACE
        //* *    READ NAS-VACATION-ADDRESS BY V-CORR-CNTRCT-PAYEE-KEY
        //* *        STARTING FROM #V-CORR-CNTRCT-PAYEE-KEY
        //*       IF NAS-VACATION-ADDRESS.V-CNTRCT-NMBR NE #ALL-TIAA-CONTRACTS (#A)
        //* *        ESCAPE BOTTOM
        //* *      END-IF
        //* *      REJECT IF V-RCRD-STATUS-CDE EQ 'D' OR
        //* *        V-ADDRSS-STATUS-CDE EQ 'B'
        //* *      ACCEPT IF V-PERMANENT-ADDRSS-IND EQ 'P' /* PERMANENT
        //* *        AND V-PENDING-PERM-ADDRSS-CHNGE-DTE EQ 0
        //* *      IF (V-ADDRSS-STATUS-CDE EQ 'R' OR EQ ' ') OR
        //* *          V-CNTRCT-STATUS-CDE EQ ' ' OR
        //* *          V-CORRESPONDENCE-ADDRSS-IND EQ 'O'
        //* *        IF V-ADDRSS-LAST-CHNGE-DTE GT #SAVE-LST-CHG
        //* *          #SAVE-LST-CHG := V-ADDRSS-LAST-CHNGE-DTE
        //* *          #CRRSPNDCE-TYPE-ZIP := V-ADDRSS-TYPE-CDE
        //* *        END-IF
        //* *      END-IF
        //* *    END-READ
        //* *  END-FOR
        //* * END-IF
        pnd_Processing_Dte_A.setValueEdited(pdaAdsa401.getPnd_Adsa401_Pnd_Cntl_Bsnss_Dte(),new ReportEditMask("YYYYMMDD"));                                               //Natural: MOVE EDITED #CNTL-BSNSS-DTE ( EM = YYYYMMDD ) TO #PROCESSING-DTE-A
        if (condition(pnd_Save_Lst_Chg.greater(getZero())))                                                                                                               //Natural: IF #SAVE-LST-CHG GT 0
        {
            //*  HOLD PROCESSING APPLIES TO CHECKS
            if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Alt_Dest_Trnst_Cde().equals(" ")))                                                                                //Natural: IF ADP-ALT-DEST-TRNST-CDE = ' '
            {
                                                                                                                                                                          //Natural: PERFORM CHECK-IF-LST-CHG-DTE-WITHIN-RANGE
                sub_Check_If_Lst_Chg_Dte_Within_Range();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  060908 START - HOLD CHECK PAYMENTS FOR FOREIGN RESIDENCES
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Annt_Typ_Cde().equals("M")))                                                                                          //Natural: IF ADP-ANNT-TYP-CDE = 'M'
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Alt_Dest_Trnst_Cde().equals(" ")))                                                                                //Natural: IF ADP-ALT-DEST-TRNST-CDE = ' '
            {
                if (condition(pnd_Crrspndce_Type_Zip.equals("F") && pdaAdsa401.getPnd_Adsa401_Adp_Alt_Dest_Hold_Cde().equals(" ")))                                       //Natural: IF #CRRSPNDCE-TYPE-ZIP = 'F' AND ADP-ALT-DEST-HOLD-CDE = ' '
                {
                                                                                                                                                                          //Natural: PERFORM SET-HOLD-CODES
                    sub_Set_Hold_Codes();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  060908 END
        //* **************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-IF-LST-CHG-DTE-WITHIN-RANGE
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-HOLD-CODES
    }
    private void sub_Check_If_Lst_Chg_Dte_Within_Range() throws Exception                                                                                                 //Natural: CHECK-IF-LST-CHG-DTE-WITHIN-RANGE
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************************
        if (condition(pnd_Save_Lst_Chg_Pnd_Lst_Chg_Yyyy.less(pnd_Processing_Dte_A_Pnd_Processing_Year)))                                                                  //Natural: IF #LST-CHG-YYYY < #PROCESSING-YEAR
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Lst_Chg_Pnd_Lst_Chg_Mnth.equals(2)))                                                                                                       //Natural: IF #LST-CHG-MNTH = 02
        {
            pnd_Rmndr.compute(new ComputeParameters(false, pnd_Rmndr), pnd_Save_Lst_Chg_Pnd_Lst_Chg_Yyyy.mod(4));                                                         //Natural: DIVIDE 4 INTO #LST-CHG-YYYY GIVING #RESULT REMAINDER #RMNDR
            pnd_Result.compute(new ComputeParameters(false, pnd_Result), pnd_Save_Lst_Chg_Pnd_Lst_Chg_Yyyy.divide(4));
            //*  LEAP YEAR
            if (condition(pnd_Rmndr.equals(getZero())))                                                                                                                   //Natural: IF #RMNDR = 0
            {
                pnd_Day_Table_Pnd_Day_Tbl.getValue(pnd_Save_Lst_Chg_Pnd_Lst_Chg_Mnth).setValue(29);                                                                       //Natural: ASSIGN #DAY-TBL ( #LST-CHG-MNTH ) = 29
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Save_Lst_Chg_Pnd_Lst_Chg_Mnth.equals(pnd_Processing_Dte_A_Pnd_Processing_Mnth)))                                                                //Natural: IF #LST-CHG-MNTH = #PROCESSING-MNTH
        {
            pnd_Difference.compute(new ComputeParameters(true, pnd_Difference), pnd_Processing_Dte_A_Pnd_Processing_Day.subtract(pnd_Save_Lst_Chg_Pnd_Lst_Chg_Day));      //Natural: SUBTRACT ROUNDED #LST-CHG-DAY FROM #PROCESSING-DAY GIVING #DIFFERENCE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Difference.compute(new ComputeParameters(false, pnd_Difference), pnd_Processing_Dte_A_Pnd_Processing_Mnth.subtract(pnd_Save_Lst_Chg_Pnd_Lst_Chg_Mnth));   //Natural: SUBTRACT #LST-CHG-MNTH FROM #PROCESSING-MNTH GIVING #DIFFERENCE
            if (condition(pnd_Difference.greater(1)))                                                                                                                     //Natural: IF #DIFFERENCE > 1
            {
                pnd_Difference.setValue(99);                                                                                                                              //Natural: ASSIGN #DIFFERENCE = 99
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Difference.compute(new ComputeParameters(true, pnd_Difference), pnd_Day_Table_Pnd_Day_Tbl.getValue(pnd_Save_Lst_Chg_Pnd_Lst_Chg_Mnth).subtract(pnd_Save_Lst_Chg_Pnd_Lst_Chg_Day)); //Natural: SUBTRACT ROUNDED #LST-CHG-DAY FROM #DAY-TBL ( #LST-CHG-MNTH ) GIVING #DIFFERENCE
                pnd_Difference.nadd(pnd_Processing_Dte_A_Pnd_Processing_Day);                                                                                             //Natural: ADD ROUNDED #PROCESSING-DAY TO #DIFFERENCE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Difference.lessOrEqual(pnd_Max_Days)))                                                                                                          //Natural: IF #DIFFERENCE LE #MAX-DAYS
        {
                                                                                                                                                                          //Natural: PERFORM SET-HOLD-CODES
            sub_Set_Hold_Codes();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Set_Hold_Codes() throws Exception                                                                                                                    //Natural: SET-HOLD-CODES
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Alt_Dest_Hold_Cde().equals(" ")))                                                                                     //Natural: IF ADP-ALT-DEST-HOLD-CDE EQ ' '
        {
            //*  060908
            if (condition(pnd_Crrspndce_Type_Zip.equals("F")))                                                                                                            //Natural: IF #CRRSPNDCE-TYPE-ZIP = 'F'
            {
                //*  060908
                pdaAdsa401.getPnd_Adsa401_Adp_Alt_Dest_Hold_Cde().setValue("B0");                                                                                         //Natural: ASSIGN ADP-ALT-DEST-HOLD-CDE = 'B0'
                //*  060908
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAdsa401.getPnd_Adsa401_Adp_Alt_Dest_Hold_Cde().setValue("CANZ");                                                                                       //Natural: ASSIGN ADP-ALT-DEST-HOLD-CDE = 'CANZ'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
