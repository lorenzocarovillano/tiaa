/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:55:46 PM
**        * FROM NATURAL SUBPROGRAM : Adsn607a
************************************************************
**        * FILE NAME            : Adsn607a.java
**        * CLASS NAME           : Adsn607a
**        * INSTANCE NAME        : Adsn607a
************************************************************
************************************************************************
*
* PROGRAM:  ADSN607A
* DATE   :  05/11
* FUNCTION: SET CPS INTERFACE DATE FOR FUTURE PAYMENTS.
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* --------   -------   ---------------------------------------------
* 11/26/2013 E. MELNIK RECOMPILED FOR NEW NECA4000.  CREF/REA REDESIGN
*                      CHANGES.
*
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn607a extends BLNatBase
{
    // Data Areas
    private PdaNeca4000 pdaNeca4000;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Pymnt_Intrfce_Dte;
    private DbsField pnd_Rtrn_Cde;
    private DbsField pnd_I;
    private DbsField pnd_W_Dte;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaNeca4000 = new PdaNeca4000(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_Pymnt_Intrfce_Dte = parameters.newFieldInRecord("pnd_Pymnt_Intrfce_Dte", "#PYMNT-INTRFCE-DTE", FieldType.DATE);
        pnd_Pymnt_Intrfce_Dte.setParameterOption(ParameterOption.ByReference);
        pnd_Rtrn_Cde = parameters.newFieldInRecord("pnd_Rtrn_Cde", "#RTRN-CDE", FieldType.NUMERIC, 2);
        pnd_Rtrn_Cde.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_W_Dte = localVariables.newFieldInRecord("pnd_W_Dte", "#W-DTE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn607a() throws Exception
    {
        super("Adsn607a");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Rtrn_Cde.reset();                                                                                                                                             //Natural: RESET #RTRN-CDE
        //*  DATE IS ZERO
        if (condition(pnd_Pymnt_Intrfce_Dte.equals(getZero())))                                                                                                           //Natural: IF #PYMNT-INTRFCE-DTE = 0
        {
            pnd_Rtrn_Cde.setValue(1);                                                                                                                                     //Natural: ASSIGN #RTRN-CDE := 01
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaNeca4000.getNeca4000_Function_Cde().setValue("CAL");                                                                                                           //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'CAL'
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pdaNeca4000.getNeca4000_Cal_Business_Day_Ind().getValue(1).equals("Y") || pnd_I.greater(20))) {break;}                                          //Natural: UNTIL NECA4000.CAL-BUSINESS-DAY-IND ( 1 ) = 'Y' OR #I GT 20
            pnd_W_Dte.setValueEdited(pnd_Pymnt_Intrfce_Dte,new ReportEditMask("YYYYMMDD"));                                                                               //Natural: MOVE EDITED #PYMNT-INTRFCE-DTE ( EM = YYYYMMDD ) TO #W-DTE
            pdaNeca4000.getNeca4000_Cal_Key_For_Dte().compute(new ComputeParameters(false, pdaNeca4000.getNeca4000_Cal_Key_For_Dte()), pnd_W_Dte.val());                  //Natural: ASSIGN NECA4000.CAL-KEY-FOR-DTE := VAL ( #W-DTE )
            DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                        //Natural: CALLNAT 'NECN4000' NECA4000
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            if (condition(pdaNeca4000.getNeca4000_Cal_Business_Day_Ind().getValue(1).equals("Y")))                                                                        //Natural: IF NECA4000.CAL-BUSINESS-DAY-IND ( 1 ) = 'Y'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Pymnt_Intrfce_Dte.nadd(1);                                                                                                                            //Natural: ADD 1 TO #PYMNT-INTRFCE-DTE
            }                                                                                                                                                             //Natural: END-IF
            pnd_I.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #I
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  NO VALID INTERFACE DATE IS FOUND
        if (condition(pdaNeca4000.getNeca4000_Cal_Business_Day_Ind().getValue(1).notEquals("Y")))                                                                         //Natural: IF NECA4000.CAL-BUSINESS-DAY-IND ( 1 ) NE 'Y'
        {
            pnd_Rtrn_Cde.setValue(99);                                                                                                                                    //Natural: ASSIGN #RTRN-CDE := 99
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
