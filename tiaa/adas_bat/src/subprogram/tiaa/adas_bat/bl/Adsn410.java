/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:54:10 PM
**        * FROM NATURAL SUBPROGRAM : Adsn410
************************************************************
**        * FILE NAME            : Adsn410.java
**        * CLASS NAME           : Adsn410
**        * INSTANCE NAME        : Adsn410
************************************************************
***********************************************************************
* PROGRAM  : ADSN410
* SYSTEM   : ADAS - ANNUITIZATION SUNGUARD
* TITLE    : ADAS BATCH - PERFORM ADAS CONTRACT EDITS
* GENERATED: MARCH 12, 2004
* AUTHOR   : EDWINA VILLANUEVA
* FUNCTION : THIS PROGRAM PERFORMS VALIDATION OF CONTRACTS BASED ON
*          : THE FF. ADAS EDIT RULES:
*
*  1. GUARANTEED PERIOD LESS THAN 5 YRS. IS ONLY ALLOWED FOR RA, SRA
*     AND GRA WITHOUT TIAA FUNDS
*  2. TIAA FUND CANNOT BE INCLUDED IN A SETTLEMENT OF RA CONTRACT
*     WITH AC OPTION
*  3. CREF OR REA FUNDS CANNOT BE INCLUDED IN A SETTLEMENT WITH
*     IR OPTION
*
*********************  MAINTENANCE LOG ******************************
*
*  D A T E   PROGRAMMER     D E S C R I P T I O N
*
* 07-19-04   E.VILLANUEVA   INCLUDE THE VALIDATION - TIAA CANNOT BE
*                           INCLUDED FOR AN AC OPTION WITH LESS THAN 5
*                           YRS. GURANTEED PERIOD FOR GRA & GSRA
*                           CONTRACTS.  ALSO, APPLY VALIDATION #1 ABOVE
*                           ONLY IF OPTION IS AC (ANNUITY CERTAIN)
* 08/31/2005 M.NACHBER      CHANGES FOR TPA
*                           IF TPA GENERATED FROM NON-GRA, ANNUITY
*                           CERTAIN OPTION IS NOT ALLOWED
*                           A NEW INDICATOR FIELD IS ADDED TO ADSA401.
*                           IN CASES OF TPA CONTRACTS IT SHOWS IF THE
*                           CONTRACT WAS ORIGINATED AS GRA
* 04/12/2007 M.NACHBER      FOR AC OPTION AND LOB IS RA, INCLUDE
*                           SUB-LOB CHECK TO MAKE SURE IT IS NOT
*                           GRA. SC 041207.
* 03/17/2008 E.MELNIK     ROTH 403B/401K CHANGES.  RECOMPILED FOR
*                         NEW ADSA401.
* 01142009   E. MELNIK   TIAA ACCESS/STABLE RETURN ENHANCEMENTS.  MARKED
*                        BY EM - 011409.
* 3/19/10    C. MASON    PCPOP PROJECT - SKIP EDITS FOR TIAA07-10
* 10/01/10    D.E.ANDER   PCPOP PROJECT - SKIP EDITS FOR TIAA07-8
* 11/17/10   E. MELNIK   SVSAA CHANGES.  MARKED BY EM - 111710.
* 04/07/11   O. SOTTO    457B CONTRACTS ARE ALLOWED < 5 YRS GUARANTEED
*                        PERIOD. SC 040711.
* 03/01/12   O. SOTTO    RECOMPILED DUE TO ADSA401 CHANGES.
* 09/24/13   E. MELNIK   CREF/REA REDESIGN CHANGES.  MARKED BY
*                        EM - 092413.
* 03/31/16   R. CARREON  RIPC - REMOVED VALIDATION.  MARKED BY
*                        RIPC.
* 02/21/17   R. CARREON  RESTOW FOR PIN EXPANSION 02212017
*                        CHANGE IN ADSA401
* 04/12/2019 E. MELNIK   RESTOWED WITH UPDATED ADSA401 WITH IRC CDE.
*********************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn410 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAdsa401 pdaAdsa401;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_part;
    private DbsField part_Rqst_Id;
    private DbsField part_Adp_Irc_Cde;

    private DbsGroup pnd_Constant_Variables;
    private DbsField pnd_Constant_Variables_Pnd_Gra;
    private DbsField pnd_Constant_Variables_Pnd_Gsra;
    private DbsField pnd_Constant_Variables_Pnd_Ra;
    private DbsField pnd_Constant_Variables_Pnd_Sra;
    private DbsField pnd_Constant_Variables_Pnd_Tpa;
    private DbsField pnd_Constant_Variables_Pnd_Ipro;
    private DbsField pnd_Constant_Variables_Pnd_Tiaa;
    private DbsField pnd_Constant_Variables_Pnd_Stable;
    private DbsField pnd_Constant_Variables_Pnd_Sa_Fnd_Id;
    private DbsField pnd_Constant_Variables_Pnd_Tiaa_Ic654;
    private DbsField pnd_Constant_Variables_Pnd_Stable_Return_Ic654;
    private DbsField pnd_Constant_Variables_Pnd_Stable_Value_Ic654;
    private DbsField pnd_Constant_Variables_Pnd_Five_Yrs;
    private DbsField pnd_Constant_Variables_Pnd_Ac_Annty;
    private DbsField pnd_Constant_Variables_Pnd_Ir_Annty;

    private DbsGroup pnd_Other_Variables;
    private DbsField pnd_Other_Variables_Pnd_With_Tiaa_Fund;
    private DbsField pnd_Other_Variables_Pnd_With_Cref_Rea_Funds;
    private DbsField pnd_Other_Variables_Pnd_I;
    private DbsField pnd_Tiaa07_10;
    private DbsField pnd_Number;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaAdsa401 = new PdaAdsa401(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_part = new DataAccessProgramView(new NameInfo("vw_part", "PART"), "ADS_PRTCPNT", "ADS_PRTCPNT");
        part_Rqst_Id = vw_part.getRecord().newFieldInGroup("part_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, "RQST_ID");
        part_Adp_Irc_Cde = vw_part.getRecord().newFieldInGroup("part_Adp_Irc_Cde", "ADP-IRC-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "ADP_IRC_CDE");
        registerRecord(vw_part);

        pnd_Constant_Variables = localVariables.newGroupInRecord("pnd_Constant_Variables", "#CONSTANT-VARIABLES");
        pnd_Constant_Variables_Pnd_Gra = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Gra", "#GRA", FieldType.STRING, 10);
        pnd_Constant_Variables_Pnd_Gsra = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Gsra", "#GSRA", FieldType.STRING, 10);
        pnd_Constant_Variables_Pnd_Ra = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Ra", "#RA", FieldType.STRING, 10);
        pnd_Constant_Variables_Pnd_Sra = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Sra", "#SRA", FieldType.STRING, 10);
        pnd_Constant_Variables_Pnd_Tpa = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Tpa", "#TPA", FieldType.STRING, 10);
        pnd_Constant_Variables_Pnd_Ipro = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Ipro", "#IPRO", FieldType.STRING, 10);
        pnd_Constant_Variables_Pnd_Tiaa = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Tiaa", "#TIAA", FieldType.STRING, 1);
        pnd_Constant_Variables_Pnd_Stable = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Stable", "#STABLE", FieldType.STRING, 1);
        pnd_Constant_Variables_Pnd_Sa_Fnd_Id = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Sa_Fnd_Id", "#SA-FND-ID", FieldType.STRING, 
            2);
        pnd_Constant_Variables_Pnd_Tiaa_Ic654 = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Tiaa_Ic654", "#TIAA-IC654", FieldType.STRING, 
            4);
        pnd_Constant_Variables_Pnd_Stable_Return_Ic654 = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Stable_Return_Ic654", "#STABLE-RETURN-IC654", 
            FieldType.STRING, 4);
        pnd_Constant_Variables_Pnd_Stable_Value_Ic654 = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Stable_Value_Ic654", "#STABLE-VALUE-IC654", 
            FieldType.STRING, 4);
        pnd_Constant_Variables_Pnd_Five_Yrs = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Five_Yrs", "#FIVE-YRS", FieldType.NUMERIC, 
            2);
        pnd_Constant_Variables_Pnd_Ac_Annty = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Ac_Annty", "#AC-ANNTY", FieldType.STRING, 
            2);
        pnd_Constant_Variables_Pnd_Ir_Annty = pnd_Constant_Variables.newFieldInGroup("pnd_Constant_Variables_Pnd_Ir_Annty", "#IR-ANNTY", FieldType.STRING, 
            2);

        pnd_Other_Variables = localVariables.newGroupInRecord("pnd_Other_Variables", "#OTHER-VARIABLES");
        pnd_Other_Variables_Pnd_With_Tiaa_Fund = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_With_Tiaa_Fund", "#WITH-TIAA-FUND", FieldType.BOOLEAN, 
            1);
        pnd_Other_Variables_Pnd_With_Cref_Rea_Funds = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_With_Cref_Rea_Funds", "#WITH-CREF-REA-FUNDS", 
            FieldType.BOOLEAN, 1);
        pnd_Other_Variables_Pnd_I = pnd_Other_Variables.newFieldInGroup("pnd_Other_Variables_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Tiaa07_10 = localVariables.newFieldArrayInRecord("pnd_Tiaa07_10", "#TIAA07-10", FieldType.STRING, 6, new DbsArrayController(1, 4));
        pnd_Number = localVariables.newFieldInRecord("pnd_Number", "#NUMBER", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_part.reset();

        localVariables.reset();
        pnd_Constant_Variables_Pnd_Gra.setInitialValue("GRA");
        pnd_Constant_Variables_Pnd_Gsra.setInitialValue("GRSA");
        pnd_Constant_Variables_Pnd_Ra.setInitialValue("RA");
        pnd_Constant_Variables_Pnd_Sra.setInitialValue("SRA");
        pnd_Constant_Variables_Pnd_Tpa.setInitialValue("TPA");
        pnd_Constant_Variables_Pnd_Ipro.setInitialValue("IPRO");
        pnd_Constant_Variables_Pnd_Tiaa.setInitialValue("T");
        pnd_Constant_Variables_Pnd_Stable.setInitialValue("Y");
        pnd_Constant_Variables_Pnd_Sa_Fnd_Id.setInitialValue("SA");
        pnd_Constant_Variables_Pnd_Tiaa_Ic654.setInitialValue("TIA0");
        pnd_Constant_Variables_Pnd_Stable_Return_Ic654.setInitialValue("TSA0");
        pnd_Constant_Variables_Pnd_Stable_Value_Ic654.setInitialValue("TSV0");
        pnd_Constant_Variables_Pnd_Five_Yrs.setInitialValue(5);
        pnd_Constant_Variables_Pnd_Ac_Annty.setInitialValue("AC");
        pnd_Constant_Variables_Pnd_Ir_Annty.setInitialValue("IR");
        pnd_Tiaa07_10.getValue(1).setInitialValue("TIAA07");
        pnd_Tiaa07_10.getValue(2).setInitialValue("TIAA08");
        pnd_Tiaa07_10.getValue(3).setInitialValue("TIAA09");
        pnd_Tiaa07_10.getValue(4).setInitialValue("TIAA10");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn410() throws Exception
    {
        super("Adsn410");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  EM - 011409
        if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue("*").equals(pnd_Constant_Variables_Pnd_Tiaa) || pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue("*").equals(pnd_Constant_Variables_Pnd_Stable))) //Natural: IF ADC-ACCT-CDE ( * ) EQ #TIAA OR EQ #STABLE
        {
            pnd_Other_Variables_Pnd_With_Tiaa_Fund.setValue(true);                                                                                                        //Natural: MOVE TRUE TO #WITH-TIAA-FUND
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet315 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ADP-ANNTY-OPTN EQ #IR-ANNTY
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Annty_Optn().equals(pnd_Constant_Variables_Pnd_Ir_Annty)))
        {
            decideConditionsMet315++;
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 TO #FUND-CNT
            for (pnd_Other_Variables_Pnd_I.setValue(1); condition(pnd_Other_Variables_Pnd_I.lessOrEqual(pdaAdsa401.getPnd_Adsa401_Pnd_Fund_Cnt())); pnd_Other_Variables_Pnd_I.nadd(1))
            {
                //*      IF ADC-TKR-SYMBL(#I) EQ ' '
                //*        ESCAPE BOTTOM
                //*      END-IF
                //*      IF ADC-ACCT-CDE(#I) EQ #TIAA
                //*          AND                                       /* EM - 111710 START
                //*          (ADC-T395-FUND-ID(#I) NE #SA-FND-ID)      /* EM - 111710 END
                if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Invstmnt_Grpng_Id().getValue(pnd_Other_Variables_Pnd_I).equals(" ")))                                         //Natural: IF ADC-INVSTMNT-GRPNG-ID ( #I ) EQ ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Invstmnt_Grpng_Id().getValue(pnd_Other_Variables_Pnd_I).equals(pnd_Constant_Variables_Pnd_Tiaa_Ic654)))       //Natural: IF ADC-INVSTMNT-GRPNG-ID ( #I ) EQ #TIAA-IC654
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  EM - 092413 - END
                pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L06");                                                                                             //Natural: MOVE 'L06' TO #ERROR-STATUS
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  EM - 011409 START
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN ADP-ANNTY-OPTN EQ #AC-ANNTY AND ( ADC-INVSTMNT-GRPNG-ID ( * ) = #STABLE-RETURN-IC654 OR = #STABLE-VALUE-IC654 )
        else if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Annty_Optn().equals(pnd_Constant_Variables_Pnd_Ac_Annty) && (pdaAdsa401.getPnd_Adsa401_Adc_Invstmnt_Grpng_Id().getValue("*").equals(pnd_Constant_Variables_Pnd_Stable_Return_Ic654) 
            || pdaAdsa401.getPnd_Adsa401_Adc_Invstmnt_Grpng_Id().getValue("*").equals(pnd_Constant_Variables_Pnd_Stable_Value_Ic654))))
        {
            decideConditionsMet315++;
            //*      AND (ADC-ACCT-CDE(*) = #STABLE OR
            //*      ADC-T395-FUND-ID(*) = #SA-FND-ID)
            //*  EM - 092413 - END
            //*  EM - 011409 END
            //*  041207
            pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L71");                                                                                                 //Natural: MOVE 'L71' TO #ERROR-STATUS
        }                                                                                                                                                                 //Natural: WHEN #CNTRCT-LOB EQ #RA AND ADP-ANNTY-OPTN EQ #AC-ANNTY AND #CNTRCT-SUB-LOB NE MASK ( 'GRA' )
        else if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Lob().equals(pnd_Constant_Variables_Pnd_Ra) && pdaAdsa401.getPnd_Adsa401_Adp_Annty_Optn().equals(pnd_Constant_Variables_Pnd_Ac_Annty) 
            && ! (DbsUtil.maskMatches(pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Sub_Lob(),"'GRA'"))))
        {
            decideConditionsMet315++;
            if (condition(pnd_Other_Variables_Pnd_With_Tiaa_Fund.getBoolean()))                                                                                           //Natural: IF #WITH-TIAA-FUND
            {
                //*  DEA
                DbsUtil.examine(new ExamineSource(pnd_Tiaa07_10.getValue("*")), new ExamineSearch(pdaAdsa401.getPnd_Adsa401_Adp_Plan_Nbr()), new ExamineGivingNumber(pnd_Number)); //Natural: EXAMINE #TIAA07-10 ( * ) FOR ADP-PLAN-NBR GIVING #NUMBER
                if (condition(pnd_Number.equals(getZero())))                                                                                                              //Natural: IF #NUMBER = 0
                {
                    pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L05");                                                                                         //Natural: MOVE 'L05' TO #ERROR-STATUS
                    //*  DEA
                }                                                                                                                                                         //Natural: END-IF
                //*  2 - 'TPA from GRA'
            }                                                                                                                                                             //Natural: END-IF
            //* * 08312005   MN   START
            //* * REJECT ANNUITY CERTAIN REQUESTS WITH TPAS AND IPRO (IO)S
            //* * ORIGINATED FROM NON-GRA CONTRACTS.
        }                                                                                                                                                                 //Natural: WHEN #CNTRCT-LOB EQ #TPA AND ADP-ANNTY-OPTN EQ #AC-ANNTY AND ADC-ORIG-LOB-IND NE '2'
        else if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Lob().equals(pnd_Constant_Variables_Pnd_Tpa) && pdaAdsa401.getPnd_Adsa401_Adp_Annty_Optn().equals(pnd_Constant_Variables_Pnd_Ac_Annty) 
            && pdaAdsa401.getPnd_Adsa401_Adc_Orig_Lob_Ind().notEquals("2")))
        {
            decideConditionsMet315++;
            if (condition(pnd_Other_Variables_Pnd_With_Tiaa_Fund.getBoolean()))                                                                                           //Natural: IF #WITH-TIAA-FUND
            {
                pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L19");                                                                                             //Natural: MOVE 'L19' TO #ERROR-STATUS
                //*  3 - 'IPRO from GRA'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #CNTRCT-LOB EQ #IPRO AND ADP-ANNTY-OPTN EQ #AC-ANNTY AND ADC-ORIG-LOB-IND NE '3'
        else if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Lob().equals(pnd_Constant_Variables_Pnd_Ipro) && pdaAdsa401.getPnd_Adsa401_Adp_Annty_Optn().equals(pnd_Constant_Variables_Pnd_Ac_Annty) 
            && pdaAdsa401.getPnd_Adsa401_Adc_Orig_Lob_Ind().notEquals("3")))
        {
            decideConditionsMet315++;
            if (condition(pnd_Other_Variables_Pnd_With_Tiaa_Fund.getBoolean()))                                                                                           //Natural: IF #WITH-TIAA-FUND
            {
                pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L19");                                                                                             //Natural: MOVE 'L19' TO #ERROR-STATUS
            }                                                                                                                                                             //Natural: END-IF
            //* * REJECT TPA ANNUITY CERTAIN REQUESTS ORIGINATED FROM GRA CONTRACTS
            //* * IF GUARANTEED PERIOD IS GREATER THAN THE NUMBER OF REMAINING TPA
            //* * PAYMENTS.
            //*   WHEN  #CNTRCT-LOB EQ #TPA  AND ADP-ANNTY-OPTN EQ #AC-ANNTY
            //*       AND ADP-GRNTEE-PERIOD > ADC-TPA-PAYMNT-CNT
            //*       AND ADC-ORIG-LOB-IND = '2'
            //*     IF #WITH-TIAA-FUND
            //*       MOVE 'L20' TO #ERROR-STATUS
            //*     END-IF
            //* * 08312005   MN   END
        }                                                                                                                                                                 //Natural: WHEN ADP-ANNTY-OPTN EQ #AC-ANNTY AND ADP-GRNTEE-PERIOD LT #FIVE-YRS
        else if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Annty_Optn().equals(pnd_Constant_Variables_Pnd_Ac_Annty) && pdaAdsa401.getPnd_Adsa401_Adp_Grntee_Period().less(pnd_Constant_Variables_Pnd_Five_Yrs)))
        {
            decideConditionsMet315++;
            //*  040711
            vw_part.startDatabaseFind                                                                                                                                     //Natural: FIND ( 1 ) PART WITH PART.RQST-ID = #ADSA401.RQST-ID
            (
            "FIND01",
            new Wc[] { new Wc("RQST_ID", "=", pdaAdsa401.getPnd_Adsa401_Rqst_Id(), WcType.WITH) },
            1
            );
            FIND01:
            while (condition(vw_part.readNextRow("FIND01")))
            {
                vw_part.setIfNotFoundControlFlag(false);
                //*  040711
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
            //*  041207
            //*  041207
            if (condition((pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Lob().equals(pnd_Constant_Variables_Pnd_Gra) || (pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Lob().equals(pnd_Constant_Variables_Pnd_Ra)  //Natural: IF #CNTRCT-LOB EQ #GRA OR ( #CNTRCT-LOB = #RA AND #CNTRCT-SUB-LOB = MASK ( 'GRA' ) )
                && DbsUtil.maskMatches(pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Sub_Lob(),"'GRA'")))))
            {
                if (condition(pnd_Other_Variables_Pnd_With_Tiaa_Fund.getBoolean()))                                                                                       //Natural: IF #WITH-TIAA-FUND
                {
                    pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L04");                                                                                         //Natural: MOVE 'L04' TO #ERROR-STATUS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(! (pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Lob().equals(pnd_Constant_Variables_Pnd_Ra) || pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Lob().equals(pnd_Constant_Variables_Pnd_Sra)))) //Natural: IF NOT ( #CNTRCT-LOB EQ #RA OR EQ #SRA )
                {
                    //*  040711
                    //*  040711
                    if (condition(part_Adp_Irc_Cde.equals("17")))                                                                                                         //Natural: IF PART.ADP-IRC-CDE = '17'
                    {
                        ignore();
                        //*  040711
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L04");                                                                                     //Natural: MOVE 'L04' TO #ERROR-STATUS
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //
}
