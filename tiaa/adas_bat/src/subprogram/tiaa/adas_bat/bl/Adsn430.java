/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:54:14 PM
**        * FROM NATURAL SUBPROGRAM : Adsn430
************************************************************
**        * FILE NAME            : Adsn430.java
**        * CLASS NAME           : Adsn430
**        * INSTANCE NAME        : Adsn430
************************************************************
************************************************************************
*
* CALLS "CALM" SYSTEM MODULE LAMN1001, WHICH WRITES RECORDS TO
* THE COMMON LEDGER TRANSACTION FILE (DB 003, FILE 150).
*
* AS PART OF THE CALM SUNSET PROJECT THE MODULE WILL NO LONGER
* INTERFACE WITH CALM AND WILL WRITE DIRECTLY TO A CTLS DATASET.
*
* MAINTENANCE:
*
* NAME          DATE       DESCRIPTION
* ----          ----       -----------
* T.SHINE      12/14/04   TNT CHANGES FOR ADAS
* M.NACHBER    09/06/2005  REL 6.5 FOR IPRO/TPA
*                          NO CHANGES, JUST RECOMPLILED TO PICK UP
*                          A NEW VERSION OF ADSA401
* 03/17/2008 E.MELNIK     ROTH 403B/401K CHANGES.  RECOMPILED FOR
*                         NEW ADSA401.
* 01142009 E. MELNIK   TIAA ACCESS/STABLE RETURN ENHANCEMENTS.  MARKED
*                      BY EM - 011409.
* 5/24/2010 C.MASON    RESTOW DUE TO CHANGE IN ADSA401
* 11/17/2010 E.MELNIK  SVSAA ENHANCEMENTS.  MARKED BY EM - 111710.
* 03/01/2012 O.SOTTO   RECOMPILED DUE TO ADSA401 CHANGES.
* 01/30/2013 E. MELNIK TNG PROJECT.  CHANGES MARKED BY EM - TNG0613.
* 10/04/2013 E. MELNIK CREF/REA REDESIGN CHANGES.
*                      CHANGES MARKED BY EM - 100413.
* 03/11/2015 E. MELNIK CALM REMOVAL CHANGES.  MARKED BY EM - CALM15.
* 05/07/2015 E. MELNIK BENE IN THE PLAN CHANGES.
*                      MARKED BY EM - 050715.
* 10/29/2015 E. MELNIK RIPC CHANGES.  MARKED BY EM 102915.
* 08/30/2016 E. MELNIK PIN EXPANSION.  RECOMPILED DUE TO
*                      IAALCTLS CHANGES TO EXPAND PIN TO 12 BYTES.
* 03/13/2017 R.CARREON RESTOWED FOR PIN EXPANSION - NAZ-TABLE-DDM
* 04/12/2019 E.MELNIK  RESTOWED FOR UPDATED ADSA401 WITH IRC CDE.
************************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn430 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAdsa401 pdaAdsa401;
    private LdaIaalctls ldaIaalctls;
    private PdaIaaactls pdaIaaactls;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Bene_Accptnce_Ind;
    private DbsField pnd_Tot_Trans_Amt;
    private DbsField pnd_Actl_Amt;
    private DbsField pnd_Dx;
    private DbsField pnd_Stts_Cde;
    private DbsField pnd_Settl_Ind;
    private DbsField pnd_Dte_Alpha;

    private DbsGroup pnd_Dte_Alpha__R_Field_1;
    private DbsField pnd_Dte_Alpha_Pnd_Dte;
    private DbsField pnd_Trlr_Run_Tme;

    private DbsGroup pnd_Trlr_Run_Tme__R_Field_2;
    private DbsField pnd_Trlr_Run_Tme_Pnd_Trlr_Run_Time;
    private DbsField pnd_I;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Z;
    private DbsField pnd_Indx;
    private DbsField pnd_Indx1;
    private DbsField pnd_Tiaa_Stbl_Tckr;
    private DbsField pnd_Tiaa_Stbl_Ic654;
    private DbsField pnd_Tiaa_Stbl_Access_Rea_Ic654;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;
    private DbsGroup naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup;
    private DbsField naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt;
    private DbsField pnd_Naz_Table_Key;

    private DbsGroup pnd_Naz_Table_Key__R_Field_3;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id;
    private DbsField pnd_Trans_Record;

    private DbsGroup pnd_Trans_Record__R_Field_4;
    private DbsField pnd_Trans_Record_Pnd_Trans_Rcrd;
    private DbsField pnd_Trans_Record_Pnd_Trans_Eor;

    private DbsGroup pnd_Trans_Record__R_Field_5;
    private DbsField pnd_Trans_Record_Pnd_Trans_Rcrd_Typ;
    private DbsField pnd_Trans_Record_Pnd_Trans_Sqnce;
    private DbsField pnd_Trans_Record_Pnd_Trans_Typ;
    private DbsField pnd_Trans_Record_Pnd_Trans_Typ_Rcrd;
    private DbsField pnd_Trans_Record_Pnd_Trans_Occrnce_Nbr;
    private DbsField pnd_Trans_Record_Pnd_Trans_Rte_Basis;
    private DbsField pnd_Trans_Record_Pnd_Trans_Undldgr_Ind;
    private DbsField pnd_Trans_Record_Pnd_Trans_Rvrsl_Cde;
    private DbsField pnd_Trans_Record_Pnd_Trans_Dept_Id;
    private DbsField pnd_Trans_Record_Pnd_Trans_Allctn_Ndx;
    private DbsField pnd_Trans_Record_Pnd_Trans_Vltn_Basis;
    private DbsField pnd_Trans_Record_Pnd_Trans_Amt_Sign;
    private DbsField pnd_Trans_Record_Pnd_Trans_Amt;
    private DbsField pnd_Trans_Record_Pnd_Trans_From_Tkr;
    private DbsField pnd_Trans_Record_Pnd_Trans_From_Lob;
    private DbsField pnd_Trans_Record_Pnd_Trans_From_Sub_Lob;
    private DbsField pnd_Trans_Record_Pnd_Trans_From_Tiaa_No;
    private DbsField pnd_Trans_Record_Pnd_Trans_From_Cref_No;
    private DbsField pnd_Trans_Record_Pnd_Trans_From_Num_Units;
    private DbsField pnd_Trans_Record_Pnd_Trans_To_Tkr;
    private DbsField pnd_Trans_Record_Pnd_Trans_To_Lob;
    private DbsField pnd_Trans_Record_Pnd_Trans_To_Sub_Lob;
    private DbsField pnd_Trans_Record_Pnd_Trans_To_Tiaa_No;
    private DbsField pnd_Trans_Record_Pnd_Trans_To_Cref_No;
    private DbsField pnd_Trans_Record_Pnd_Trans_To_Num_Units;
    private DbsField pnd_Trans_Record_Pnd_Trans_Pits_Rqst_Id;
    private DbsField pnd_Trans_Record_Pnd_Trans_Ldgr_Typ;
    private DbsField pnd_Trans_Record_Pnd_Trans_Cncl_Rdrw;
    private DbsField pnd_Trans_Record_Pnd_Trans_Ind2;
    private DbsField pnd_Trans_Record_Pnd_Trans_Ind;
    private DbsField pnd_Trans_Record_Pnd_Trans_Tax_Ind;
    private DbsField pnd_Trans_Record_Pnd_Trans_Net_Cash_Ind;
    private DbsField pnd_Trans_Record_Pnd_Trans_Instllmnt_Dte;
    private DbsField pnd_Trans_Record_Pnd_Trans_1st_Yr_Rnwl_Ind;
    private DbsField pnd_Trans_Record_Pnd_Trans_Subplan;
    private DbsField pls_Ticker;
    private DbsField pls_Invstmnt_Grpng_Id;
    private DbsField pls_Ctls_Sqnce_Nbr;
    private DbsField pls_Ctls_Trlr_Rcrd;
    private DbsField pls_Ctls_Trlr_Hdr_Rcrd_Cnt;
    private DbsField pls_Ctls_Trlr_Dtl_Rcrd_Cnt;
    private DbsField pls_Ctls_Trlr_Amt_Ttl;
    private DbsField pls_Ic654;
    private DbsField pls_Short_Name;
    private DbsField pls_Debug_On;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaIaalctls = new LdaIaalctls();
        registerRecord(ldaIaalctls);
        localVariables = new DbsRecord();
        pdaIaaactls = new PdaIaaactls(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaAdsa401 = new PdaAdsa401(parameters);
        pnd_Bene_Accptnce_Ind = parameters.newFieldInRecord("pnd_Bene_Accptnce_Ind", "#BENE-ACCPTNCE-IND", FieldType.NUMERIC, 1);
        pnd_Bene_Accptnce_Ind.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Tot_Trans_Amt = localVariables.newFieldInRecord("pnd_Tot_Trans_Amt", "#TOT-TRANS-AMT", FieldType.PACKED_DECIMAL, 16, 2);
        pnd_Actl_Amt = localVariables.newFieldInRecord("pnd_Actl_Amt", "#ACTL-AMT", FieldType.PACKED_DECIMAL, 16, 2);
        pnd_Dx = localVariables.newFieldInRecord("pnd_Dx", "#DX", FieldType.INTEGER, 4);
        pnd_Stts_Cde = localVariables.newFieldInRecord("pnd_Stts_Cde", "#STTS-CDE", FieldType.STRING, 3);
        pnd_Settl_Ind = localVariables.newFieldInRecord("pnd_Settl_Ind", "#SETTL-IND", FieldType.STRING, 1);
        pnd_Dte_Alpha = localVariables.newFieldInRecord("pnd_Dte_Alpha", "#DTE-ALPHA", FieldType.STRING, 8);

        pnd_Dte_Alpha__R_Field_1 = localVariables.newGroupInRecord("pnd_Dte_Alpha__R_Field_1", "REDEFINE", pnd_Dte_Alpha);
        pnd_Dte_Alpha_Pnd_Dte = pnd_Dte_Alpha__R_Field_1.newFieldInGroup("pnd_Dte_Alpha_Pnd_Dte", "#DTE", FieldType.NUMERIC, 8);
        pnd_Trlr_Run_Tme = localVariables.newFieldInRecord("pnd_Trlr_Run_Tme", "#TRLR-RUN-TME", FieldType.NUMERIC, 7);

        pnd_Trlr_Run_Tme__R_Field_2 = localVariables.newGroupInRecord("pnd_Trlr_Run_Tme__R_Field_2", "REDEFINE", pnd_Trlr_Run_Tme);
        pnd_Trlr_Run_Tme_Pnd_Trlr_Run_Time = pnd_Trlr_Run_Tme__R_Field_2.newFieldInGroup("pnd_Trlr_Run_Tme_Pnd_Trlr_Run_Time", "#TRLR-RUN-TIME", FieldType.NUMERIC, 
            6);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 4);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.INTEGER, 4);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.INTEGER, 4);
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.INTEGER, 4);
        pnd_Indx = localVariables.newFieldInRecord("pnd_Indx", "#INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Indx1 = localVariables.newFieldInRecord("pnd_Indx1", "#INDX1", FieldType.PACKED_DECIMAL, 3);
        pnd_Tiaa_Stbl_Tckr = localVariables.newFieldArrayInRecord("pnd_Tiaa_Stbl_Tckr", "#TIAA-STBL-TCKR", FieldType.STRING, 10, new DbsArrayController(1, 
            3));
        pnd_Tiaa_Stbl_Ic654 = localVariables.newFieldArrayInRecord("pnd_Tiaa_Stbl_Ic654", "#TIAA-STBL-IC654", FieldType.STRING, 4, new DbsArrayController(1, 
            3));
        pnd_Tiaa_Stbl_Access_Rea_Ic654 = localVariables.newFieldArrayInRecord("pnd_Tiaa_Stbl_Access_Rea_Ic654", "#TIAA-STBL-ACCESS-REA-IC654", FieldType.STRING, 
            4, new DbsArrayController(1, 6));

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup = vw_naz_Table_Ddm.getRecord().newGroupInGroup("NAZ_TABLE_DDM_NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", 
            "NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt = naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup.newFieldArrayInGroup("naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt", 
            "NAZ-TBL-SECNDRY-DSCRPTN-TXT", FieldType.STRING, 80, new DbsArrayController(1, 2), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt.setDdmHeader("CDE/2ND/DSC");
        registerRecord(vw_naz_Table_Ddm);

        pnd_Naz_Table_Key = localVariables.newFieldInRecord("pnd_Naz_Table_Key", "#NAZ-TABLE-KEY", FieldType.STRING, 30);

        pnd_Naz_Table_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Naz_Table_Key__R_Field_3", "REDEFINE", pnd_Naz_Table_Key);
        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind = pnd_Naz_Table_Key__R_Field_3.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind", "#NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id = pnd_Naz_Table_Key__R_Field_3.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id", "#NAZ-TABLE-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id = pnd_Naz_Table_Key__R_Field_3.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id", "#NAZ-TABLE-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id = pnd_Naz_Table_Key__R_Field_3.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id", "#NAZ-TABLE-LVL3-ID", 
            FieldType.STRING, 20);
        pnd_Trans_Record = localVariables.newFieldInRecord("pnd_Trans_Record", "#TRANS-RECORD", FieldType.STRING, 250);

        pnd_Trans_Record__R_Field_4 = localVariables.newGroupInRecord("pnd_Trans_Record__R_Field_4", "REDEFINE", pnd_Trans_Record);
        pnd_Trans_Record_Pnd_Trans_Rcrd = pnd_Trans_Record__R_Field_4.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Rcrd", "#TRANS-RCRD", FieldType.STRING, 
            249);
        pnd_Trans_Record_Pnd_Trans_Eor = pnd_Trans_Record__R_Field_4.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Eor", "#TRANS-EOR", FieldType.STRING, 
            1);

        pnd_Trans_Record__R_Field_5 = localVariables.newGroupInRecord("pnd_Trans_Record__R_Field_5", "REDEFINE", pnd_Trans_Record);
        pnd_Trans_Record_Pnd_Trans_Rcrd_Typ = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Rcrd_Typ", "#TRANS-RCRD-TYP", FieldType.STRING, 
            1);
        pnd_Trans_Record_Pnd_Trans_Sqnce = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Sqnce", "#TRANS-SQNCE", FieldType.NUMERIC, 
            9);
        pnd_Trans_Record_Pnd_Trans_Typ = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Typ", "#TRANS-TYP", FieldType.STRING, 
            2);
        pnd_Trans_Record_Pnd_Trans_Typ_Rcrd = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Typ_Rcrd", "#TRANS-TYP-RCRD", FieldType.STRING, 
            2);
        pnd_Trans_Record_Pnd_Trans_Occrnce_Nbr = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Occrnce_Nbr", "#TRANS-OCCRNCE-NBR", 
            FieldType.NUMERIC, 4);
        pnd_Trans_Record_Pnd_Trans_Rte_Basis = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Rte_Basis", "#TRANS-RTE-BASIS", 
            FieldType.STRING, 2);
        pnd_Trans_Record_Pnd_Trans_Undldgr_Ind = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Undldgr_Ind", "#TRANS-UNDLDGR-IND", 
            FieldType.STRING, 1);
        pnd_Trans_Record_Pnd_Trans_Rvrsl_Cde = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Rvrsl_Cde", "#TRANS-RVRSL-CDE", 
            FieldType.STRING, 1);
        pnd_Trans_Record_Pnd_Trans_Dept_Id = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Dept_Id", "#TRANS-DEPT-ID", FieldType.STRING, 
            10);
        pnd_Trans_Record_Pnd_Trans_Allctn_Ndx = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Allctn_Ndx", "#TRANS-ALLCTN-NDX", 
            FieldType.STRING, 6);
        pnd_Trans_Record_Pnd_Trans_Vltn_Basis = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Vltn_Basis", "#TRANS-VLTN-BASIS", 
            FieldType.STRING, 5);
        pnd_Trans_Record_Pnd_Trans_Amt_Sign = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Amt_Sign", "#TRANS-AMT-SIGN", FieldType.STRING, 
            1);
        pnd_Trans_Record_Pnd_Trans_Amt = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Amt", "#TRANS-AMT", FieldType.NUMERIC, 
            16, 2);
        pnd_Trans_Record_Pnd_Trans_From_Tkr = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_From_Tkr", "#TRANS-FROM-TKR", FieldType.STRING, 
            10);
        pnd_Trans_Record_Pnd_Trans_From_Lob = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_From_Lob", "#TRANS-FROM-LOB", FieldType.STRING, 
            10);
        pnd_Trans_Record_Pnd_Trans_From_Sub_Lob = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_From_Sub_Lob", "#TRANS-FROM-SUB-LOB", 
            FieldType.STRING, 12);
        pnd_Trans_Record_Pnd_Trans_From_Tiaa_No = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_From_Tiaa_No", "#TRANS-FROM-TIAA-NO", 
            FieldType.STRING, 10);
        pnd_Trans_Record_Pnd_Trans_From_Cref_No = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_From_Cref_No", "#TRANS-FROM-CREF-NO", 
            FieldType.STRING, 10);
        pnd_Trans_Record_Pnd_Trans_From_Num_Units = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_From_Num_Units", "#TRANS-FROM-NUM-UNITS", 
            FieldType.NUMERIC, 11, 3);
        pnd_Trans_Record_Pnd_Trans_To_Tkr = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_To_Tkr", "#TRANS-TO-TKR", FieldType.STRING, 
            10);
        pnd_Trans_Record_Pnd_Trans_To_Lob = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_To_Lob", "#TRANS-TO-LOB", FieldType.STRING, 
            10);
        pnd_Trans_Record_Pnd_Trans_To_Sub_Lob = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_To_Sub_Lob", "#TRANS-TO-SUB-LOB", 
            FieldType.STRING, 12);
        pnd_Trans_Record_Pnd_Trans_To_Tiaa_No = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_To_Tiaa_No", "#TRANS-TO-TIAA-NO", 
            FieldType.STRING, 10);
        pnd_Trans_Record_Pnd_Trans_To_Cref_No = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_To_Cref_No", "#TRANS-TO-CREF-NO", 
            FieldType.STRING, 10);
        pnd_Trans_Record_Pnd_Trans_To_Num_Units = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_To_Num_Units", "#TRANS-TO-NUM-UNITS", 
            FieldType.NUMERIC, 11, 3);
        pnd_Trans_Record_Pnd_Trans_Pits_Rqst_Id = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Pits_Rqst_Id", "#TRANS-PITS-RQST-ID", 
            FieldType.STRING, 29);
        pnd_Trans_Record_Pnd_Trans_Ldgr_Typ = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Ldgr_Typ", "#TRANS-LDGR-TYP", FieldType.STRING, 
            1);
        pnd_Trans_Record_Pnd_Trans_Cncl_Rdrw = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Cncl_Rdrw", "#TRANS-CNCL-RDRW", 
            FieldType.STRING, 2);
        pnd_Trans_Record_Pnd_Trans_Ind2 = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Ind2", "#TRANS-IND2", FieldType.STRING, 
            1);
        pnd_Trans_Record_Pnd_Trans_Ind = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Ind", "#TRANS-IND", FieldType.STRING, 
            1);
        pnd_Trans_Record_Pnd_Trans_Tax_Ind = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Tax_Ind", "#TRANS-TAX-IND", FieldType.STRING, 
            1);
        pnd_Trans_Record_Pnd_Trans_Net_Cash_Ind = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Net_Cash_Ind", "#TRANS-NET-CASH-IND", 
            FieldType.STRING, 1);
        pnd_Trans_Record_Pnd_Trans_Instllmnt_Dte = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Instllmnt_Dte", "#TRANS-INSTLLMNT-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Trans_Record_Pnd_Trans_1st_Yr_Rnwl_Ind = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_1st_Yr_Rnwl_Ind", "#TRANS-1ST-YR-RNWL-IND", 
            FieldType.STRING, 1);
        pnd_Trans_Record_Pnd_Trans_Subplan = pnd_Trans_Record__R_Field_5.newFieldInGroup("pnd_Trans_Record_Pnd_Trans_Subplan", "#TRANS-SUBPLAN", FieldType.STRING, 
            6);
        pls_Ticker = WsIndependent.getInstance().newFieldArrayInRecord("pls_Ticker", "+TICKER", FieldType.STRING, 10, new DbsArrayController(1, 100));
        pls_Invstmnt_Grpng_Id = WsIndependent.getInstance().newFieldArrayInRecord("pls_Invstmnt_Grpng_Id", "+INVSTMNT-GRPNG-ID", FieldType.STRING, 4, 
            new DbsArrayController(1, 100));
        pls_Ctls_Sqnce_Nbr = WsIndependent.getInstance().newFieldInRecord("pls_Ctls_Sqnce_Nbr", "+CTLS-SQNCE-NBR", FieldType.NUMERIC, 9);
        pls_Ctls_Trlr_Rcrd = WsIndependent.getInstance().newFieldInRecord("pls_Ctls_Trlr_Rcrd", "+CTLS-TRLR-RCRD", FieldType.BOOLEAN, 1);
        pls_Ctls_Trlr_Hdr_Rcrd_Cnt = WsIndependent.getInstance().newFieldInRecord("pls_Ctls_Trlr_Hdr_Rcrd_Cnt", "+CTLS-TRLR-HDR-RCRD-CNT", FieldType.NUMERIC, 
            8);
        pls_Ctls_Trlr_Dtl_Rcrd_Cnt = WsIndependent.getInstance().newFieldInRecord("pls_Ctls_Trlr_Dtl_Rcrd_Cnt", "+CTLS-TRLR-DTL-RCRD-CNT", FieldType.NUMERIC, 
            8);
        pls_Ctls_Trlr_Amt_Ttl = WsIndependent.getInstance().newFieldInRecord("pls_Ctls_Trlr_Amt_Ttl", "+CTLS-TRLR-AMT-TTL", FieldType.NUMERIC, 16, 2);
        pls_Ic654 = WsIndependent.getInstance().newFieldArrayInRecord("pls_Ic654", "+IC654", FieldType.STRING, 4, new DbsArrayController(1, 20));
        pls_Short_Name = WsIndependent.getInstance().newFieldArrayInRecord("pls_Short_Name", "+SHORT-NAME", FieldType.STRING, 5, new DbsArrayController(1, 
            20));
        pls_Debug_On = WsIndependent.getInstance().newFieldInRecord("pls_Debug_On", "+DEBUG-ON", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_naz_Table_Ddm.reset();

        ldaIaalctls.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Dx.setInitialValue(1);
        pnd_Tiaa_Stbl_Tckr.getValue(1).setInitialValue("TIAA#");
        pnd_Tiaa_Stbl_Tckr.getValue(2).setInitialValue("TSAF#");
        pnd_Tiaa_Stbl_Tckr.getValue(3).setInitialValue("TSVX#");
        pnd_Tiaa_Stbl_Ic654.getValue(1).setInitialValue("TIA0");
        pnd_Tiaa_Stbl_Ic654.getValue(2).setInitialValue("TSA0");
        pnd_Tiaa_Stbl_Ic654.getValue(3).setInitialValue("TSV0");
        pnd_Tiaa_Stbl_Access_Rea_Ic654.getValue(1).setInitialValue("TIA0");
        pnd_Tiaa_Stbl_Access_Rea_Ic654.getValue(2).setInitialValue("TSA0");
        pnd_Tiaa_Stbl_Access_Rea_Ic654.getValue(3).setInitialValue("TSV0");
        pnd_Tiaa_Stbl_Access_Rea_Ic654.getValue(4).setInitialValue("WA50");
        pnd_Tiaa_Stbl_Access_Rea_Ic654.getValue(5).setInitialValue("REA1");
        pnd_Tiaa_Stbl_Access_Rea_Ic654.getValue(6).setInitialValue("REX1");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn430() throws Exception
    {
        super("Adsn430");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 24 SG = OFF
        //*  RESET LAMA1001.GROUP-PDA                    /* EM - CALM15 START
        //*  EM - CALM15 END
        ldaIaalctls.getPnd_Ctls_Hdr_Record().reset();                                                                                                                     //Natural: RESET #CTLS-HDR-RECORD #CTLS-TRANS-RECORD ( * ) #TRANS-RECORD #TOT-TRANS-AMT #CTLS-TRANS-CNT
        ldaIaalctls.getPnd_Ctls_Trans_Record().getValue("*").reset();
        pnd_Trans_Record.reset();
        pnd_Tot_Trans_Amt.reset();
        ldaIaalctls.getPnd_Ctls_Trans_Cnt().reset();
        //*  EM - 100413 START
        if (condition(pls_Ic654.getValue(1).equals(" ")))                                                                                                                 //Natural: IF +IC654 ( 1 ) = ' '
        {
                                                                                                                                                                          //Natural: PERFORM GET-SHORT-NAMES
            sub_Get_Short_Names();
            if (condition(Global.isEscape())) {return;}
            //*  EM - 100413 END
        }                                                                                                                                                                 //Natural: END-IF
        //*  EM - CALM15 START
        if (condition(pls_Ctls_Trlr_Rcrd.getBoolean()))                                                                                                                   //Natural: IF +CTLS-TRLR-RCRD
        {
            pdaIaaactls.getPnd_Ctls_Trlr_Record().reset();                                                                                                                //Natural: RESET #CTLS-TRLR-RECORD
                                                                                                                                                                          //Natural: PERFORM MOVE-TRLR-RCRD-DATA
            sub_Move_Trlr_Rcrd_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM DETAIL-RECORDS
            sub_Detail_Records();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM MOVE-HDR-RCRD-DATA
            sub_Move_Hdr_Rcrd_Data();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM MOVE-TRANS-RCRD-DATA
            sub_Move_Trans_Rcrd_Data();
            if (condition(Global.isEscape())) {return;}
            pls_Ctls_Sqnce_Nbr.nadd(1);                                                                                                                                   //Natural: ADD 1 TO +CTLS-SQNCE-NBR
            //*  EM - CALM15 END
        }                                                                                                                                                                 //Natural: END-IF
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETAIL-RECORDS
        //* *******************************
        //*  PERFORM WRITE-HDR-RCRD-FIELDS               /* EM - CALM15
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-DETAIL-DATA
        //* *********************************
        //* **************************************       /* EM - CALM15 START
        //*  DEFINE SUBROUTINE WRITE-HDR-RCRD-FIELDS
        //* **************************************
        //*  PERFORM WRITE-HDR-DATA
        //*  CALLNAT 'LAMN1001' LAMA1001
        //*  IF LAMA1001.RETURN-CDE NE ' '
        //*    MOVE 'L14' TO #STTS-CDE
        //*    PERFORM SET-STTS-CDS
        //*  END-IF
        //*  END-SUBROUTINE
        //* *******************************
        //*  DEFINE SUBROUTINE WRITE-HDR-DATA
        //* *******************************
        //*  IF #DX > 50
        //*   #DX                       := 1
        //*   MORE-DATA                 := 'Y'
        //*   DETAIL-REC-CNT            := 50
        //*  ELSE
        //*   MORE-DATA                 := 'N'
        //*   DETAIL-REC-CNT            := #DX - 1
        //*   HDR-TOTAL-TRANSACTION-AMT := #TOT-TRANS-AMT
        //*  END-IF
        //*  HDR-ORIGIN-SYSTEM           := 'ADAS'
        //*  HDR-RECORD-TYPE             := 'H '
        //*  HDR-CREATION-DATE-STAMP   := *DATN
        //*  HDR-CREATION-TIME-STAMP   := *TIMN
        //*  IF +EOM-INDR
        //*   MOVE EDITED +EOM-FACTOR-DTE (EM=YYYYMMDD) TO #DTE-ALPHA
        //*  ELSE
        //*   MOVE EDITED #CNTL-BSNSS-DTE (EM=YYYYMMDD) TO #DTE-ALPHA
        //*  END-IF
        //*  HDR-JOURNAL-DATE          := #DTE
        //*  MOVE EDITED ADP-EFFCTV-DTE (EM=YYYYMMDD) TO #DTE-ALPHA
        //*  HDR-PARTICIPATION-DATE    := #DTE
        //*  HDR-PIN-NUMBER            := ADP-UNIQUE-ID
        //*  HDR-RECORD-FLOW-STATUS    := 'O'
        //*  HDR-STATE-CODE          := ADP-STATE-OF-ISSUE
        //*  HDR-STATE-CODE            := ADP-FRST-ANNT-RSDNC-CDE
        //*  /* EM - TNG0613 START
        //*  COMPRESS ADP-FRST-ANNT-FRST-NME SUBSTR(ADP-FRST-ANNT-MID-NME,1,1)
        //*    ADP-FRST-ANNT-LST-NME INTO HDR-NAME
        //*  HDR-SOCIAL-SECURITY-NUMBER := ADP-FRST-ANNT-SSN
        //*  /* EM - TNG0613 END
        //*  END-SUBROUTINE                              /* EM - CALM15 END
        //* ***********************************          /* EM - CALM15 START
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-HDR-RCRD-DATA
        //* ***********************************
        //* *************************************        /* EM - CALM15 START
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-TRANS-RCRD-DATA
        //* *************************************
        //* ************************************         /* EM - CALM15 START
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-TRLR-RCRD-DATA
        //* ************************************
        //* ***********************************          /* EM - CALM15 START
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-HDR-RECORD
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-TRANS-RECORD
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-TRLR-RECORD
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-SHORT-NAMES
        //* ********************************
        //*  GET IA PAYOUT FUND SHORT NAMES
        //* **********************************
        //*  DEFINE SUBROUTINE RESET-DETAIL-DATA
        //* **********************************
        //*  RESET DTL-TRANSACTION-AMT (*)
        //*   DTL-TRANSACTION-TYPE (*)
        //*   DTL-TYPE-RECORD (*)
        //*   DTL-OCCURRENCE-NBR (*)
        //*   DTL-FROM-TIAA-CONTRACT (*)
        //*   DTL-FROM-CREF-CONTRACT (*)
        //*   DTL-FROM-LOB (*)
        //*   DTL-FROM-SUB-LOB (*)
        //*   DTL-TO-TIAA-CONTRACT (*)
        //*   DTL-TO-CREF-CONTRACT (*)
        //*   DTL-TO-FUND-TICKER-SYMBOL (*)
        //*   DTL-TO-LOB (*)
        //*   DTL-TO-SUB-LOB (*)
        //*   DTL-UNLEDGER-INDICATOR (*)
        //*   DTL-REVERSAL-CODE (*)
        //*   DTL-SETTLEMENT-IND (*)
        //*  END-SUBROUTINE
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-STTS-CDS
    }
    private void sub_Detail_Records() throws Exception                                                                                                                    //Natural: DETAIL-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #X = 1 TO #FUND-CNT
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pdaAdsa401.getPnd_Adsa401_Pnd_Fund_Cnt())); pnd_X.nadd(1))
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Tkr_Symbl().getValue(pnd_X).equals(" ")))                                                                         //Natural: IF ADC-TKR-SYMBL ( #X ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_X).greater(getZero())))                                                   //Natural: IF ADC-STNDRD-ANNL-ACTL-AMT ( #X ) GT 0
                {
                    pnd_Actl_Amt.reset();                                                                                                                                 //Natural: RESET #ACTL-AMT
                    pnd_Actl_Amt.setValue(pdaAdsa401.getPnd_Adsa401_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_X));                                                          //Natural: ASSIGN #ACTL-AMT := ADC-STNDRD-ANNL-ACTL-AMT ( #X )
                    //*  EM - 100413 - CHANGE TO USE IC654                /* EM - 100413 START
                    //*      IF ADC-TKR-SYMBL (#X) = #TIAA-STBL-TCKR (*)  /* EM - 011409/111710
                    pnd_Indx.reset();                                                                                                                                     //Natural: RESET #INDX
                    DbsUtil.examine(new ExamineSource(pls_Ticker.getValue("*")), new ExamineSearch(pdaAdsa401.getPnd_Adsa401_Adc_Tkr_Symbl().getValue(pnd_X)),            //Natural: EXAMINE +TICKER ( * ) FOR ADC-TKR-SYMBL ( #X ) GIVING INDEX #INDX
                        new ExamineGivingIndex(pnd_Indx));
                    if (condition(pnd_Indx.equals(getZero())))                                                                                                            //Natural: IF #INDX = 0
                    {
                        pnd_Stts_Cde.setValue("L78");                                                                                                                     //Natural: MOVE 'L78' TO #STTS-CDE
                                                                                                                                                                          //Natural: PERFORM SET-STTS-CDS
                        sub_Set_Stts_Cds();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                        //*  EM - 100413 END
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pls_Invstmnt_Grpng_Id.getValue(pnd_Indx).equals(pnd_Tiaa_Stbl_Ic654.getValue("*"))))                                                    //Natural: IF +INVSTMNT-GRPNG-ID ( #INDX ) = #TIAA-STBL-IC654 ( * )
                    {
                        pnd_Settl_Ind.setValue(" ");                                                                                                                      //Natural: ASSIGN #SETTL-IND := ' '
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Settl_Ind.setValue("A");                                                                                                                      //Natural: ASSIGN #SETTL-IND := 'A'
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM MOVE-DETAIL-DATA
                    sub_Move_Detail_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Stts_Cde.notEquals(" ")))                                                                                                           //Natural: IF #STTS-CDE NE ' '
                    {
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_X).greater(getZero())))                                                    //Natural: IF ADC-GRD-MNTHLY-ACTL-AMT ( #X ) GT 0
                {
                    pnd_Actl_Amt.reset();                                                                                                                                 //Natural: RESET #ACTL-AMT
                    pnd_Actl_Amt.setValue(pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_X));                                                           //Natural: ASSIGN #ACTL-AMT := ADC-GRD-MNTHLY-ACTL-AMT ( #X )
                    //*  EM - 100413 - CHANGE TO USE IC654               /* EM - 100413 - START
                    //*      IF ADC-TKR-SYMBL (#X) = #TIAA-STBL-TCKR (*) /* EM - 011409/111710
                    pnd_Indx.reset();                                                                                                                                     //Natural: RESET #INDX
                    DbsUtil.examine(new ExamineSource(pls_Ticker.getValue("*")), new ExamineSearch(pdaAdsa401.getPnd_Adsa401_Adc_Tkr_Symbl().getValue(pnd_X)),            //Natural: EXAMINE +TICKER ( * ) FOR ADC-TKR-SYMBL ( #X ) GIVING INDEX #INDX
                        new ExamineGivingIndex(pnd_Indx));
                    if (condition(pnd_Indx.equals(getZero())))                                                                                                            //Natural: IF #INDX = 0
                    {
                        pnd_Stts_Cde.setValue("L78");                                                                                                                     //Natural: MOVE 'L78' TO #STTS-CDE
                                                                                                                                                                          //Natural: PERFORM SET-STTS-CDS
                        sub_Set_Stts_Cds();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                        //*  EM - 100413 - END
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pls_Invstmnt_Grpng_Id.getValue(pnd_Indx).equals(pnd_Tiaa_Stbl_Ic654.getValue("*"))))                                                    //Natural: IF +INVSTMNT-GRPNG-ID ( #INDX ) = #TIAA-STBL-IC654 ( * )
                    {
                        pnd_Settl_Ind.setValue("G");                                                                                                                      //Natural: ASSIGN #SETTL-IND := 'G'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Settl_Ind.setValue("M");                                                                                                                      //Natural: ASSIGN #SETTL-IND := 'M'
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM MOVE-DETAIL-DATA
                    sub_Move_Detail_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Stts_Cde.notEquals(" ")))                                                                                                           //Natural: IF #STTS-CDE NE ' '
                    {
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    //*  EM - CALM15 START
    private void sub_Move_Detail_Data() throws Exception                                                                                                                  //Natural: MOVE-DETAIL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rcrd_Typ().getValue(pnd_Dx).setValue("T");                                                                    //Natural: ASSIGN #CTLS-TRANS-RCRD-TYP ( #DX ) := 'T'
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Sqnce().getValue(pnd_Dx).setValue(pls_Ctls_Sqnce_Nbr);                                                        //Natural: ASSIGN #CTLS-TRANS-SQNCE ( #DX ) := +CTLS-SQNCE-NBR
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Annty_Optn().equals("AC")))                                                                                           //Natural: IF ADP-ANNTY-OPTN = 'AC'
        {
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Typ().getValue(pnd_Dx).setValue("AS");                                                                    //Natural: ASSIGN #CTLS-TRANS-TYP ( #DX ) := 'AS'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Typ().getValue(pnd_Dx).setValue("CS");                                                                    //Natural: ASSIGN #CTLS-TRANS-TYP ( #DX ) := 'CS'
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Typ_Rcrd().getValue(pnd_Dx).setValue("AD");                                                                   //Natural: ASSIGN #CTLS-TRANS-TYP-RCRD ( #DX ) := 'AD'
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Occrnce_Nbr().getValue(pnd_Dx).setValue(pnd_Dx);                                                              //Natural: ASSIGN #CTLS-TRANS-OCCRNCE-NBR ( #DX ) := #DX
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rte_Basis().getValue(pnd_Dx).setValue(" ");                                                                   //Natural: ASSIGN #CTLS-TRANS-RTE-BASIS ( #DX ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Undldgr_Ind().getValue(pnd_Dx).setValue("N");                                                                 //Natural: ASSIGN #CTLS-TRANS-UNDLDGR-IND ( #DX ) := 'N'
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rvrsl_Cde().getValue(pnd_Dx).setValue("N");                                                                   //Natural: ASSIGN #CTLS-TRANS-RVRSL-CDE ( #DX ) := 'N'
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Dept_Id().getValue(pnd_Dx).setValue(" ");                                                                     //Natural: ASSIGN #CTLS-TRANS-DEPT-ID ( #DX ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Allctn_Ndx().getValue(pnd_Dx).setValue(" ");                                                                  //Natural: ASSIGN #CTLS-TRANS-ALLCTN-NDX ( #DX ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Vltn_Basis().getValue(pnd_Dx).setValue(" ");                                                                  //Natural: ASSIGN #CTLS-TRANS-VLTN-BASIS ( #DX ) := ' '
        if (condition(pnd_Actl_Amt.less(getZero())))                                                                                                                      //Natural: IF #ACTL-AMT < 0
        {
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt_Sign().getValue(pnd_Dx).setValue("-");                                                                //Natural: ASSIGN #CTLS-TRANS-AMT-SIGN ( #DX ) := '-'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt_Sign().getValue(pnd_Dx).setValue(" ");                                                                //Natural: ASSIGN #CTLS-TRANS-AMT-SIGN ( #DX ) := ' '
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt().getValue(pnd_Dx).setValue(pnd_Actl_Amt);                                                                //Natural: ASSIGN #CTLS-TRANS-AMT ( #DX ) := #ACTL-AMT
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tkr().getValue(pnd_Dx).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Inv_Short_Name().getValue(pnd_X));         //Natural: ASSIGN #CTLS-TRANS-FROM-TKR ( #DX ) := ADC-INV-SHORT-NAME ( #X )
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Lob().getValue(pnd_Dx).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Sub_Plan_Nbr().getSubstring(1,             //Natural: ASSIGN #CTLS-TRANS-FROM-LOB ( #DX ) := SUBSTRING ( ADC-SUB-PLAN-NBR,1,3 )
            3));
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Sub_Lob().getValue(pnd_Dx).setValue(" ");                                                                //Natural: ASSIGN #CTLS-TRANS-FROM-SUB-LOB ( #DX ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tiaa_No().getValue(pnd_Dx).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Tiaa_Nbr());                           //Natural: ASSIGN #CTLS-TRANS-FROM-TIAA-NO ( #DX ) := ADC-TIAA-NBR
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Cref_No().getValue(pnd_Dx).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Cref_Nbr());                           //Natural: ASSIGN #CTLS-TRANS-FROM-CREF-NO ( #DX ) := ADC-CREF-NBR
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Num_Units().getValue(pnd_Dx).setValue(0);                                                                //Natural: ASSIGN #CTLS-TRANS-FROM-NUM-UNITS ( #DX ) := 0
        if (condition(pls_Invstmnt_Grpng_Id.getValue(pnd_Indx).equals(pnd_Tiaa_Stbl_Access_Rea_Ic654.getValue("*"))))                                                     //Natural: IF +INVSTMNT-GRPNG-ID ( #INDX ) = #TIAA-STBL-ACCESS-REA-IC654 ( * )
        {
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tkr().getValue(pnd_Dx).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Inv_Short_Name().getValue(pnd_X));       //Natural: ASSIGN #CTLS-TRANS-TO-TKR ( #DX ) := ADC-INV-SHORT-NAME ( #X )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.examine(new ExamineSource(pls_Ic654.getValue("*")), new ExamineSearch(pls_Invstmnt_Grpng_Id.getValue(pnd_Indx)), new ExamineGivingIndex(pnd_Indx1));  //Natural: EXAMINE +IC654 ( * ) FOR +INVSTMNT-GRPNG-ID ( #INDX ) GIVING INDEX #INDX1
            if (condition(pnd_Indx1.greater(getZero())))                                                                                                                  //Natural: IF #INDX1 > 0
            {
                ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tkr().getValue(pnd_Dx).setValue(pls_Short_Name.getValue(pnd_Indx1));                               //Natural: ASSIGN #CTLS-TRANS-TO-TKR ( #DX ) := +SHORT-NAME ( #INDX1 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, pls_Invstmnt_Grpng_Id.getValue(pnd_Indx)," NOT FOUND IN NAZ084 ");                                                                  //Natural: WRITE +INVSTMNT-GRPNG-ID ( #INDX ) ' NOT FOUND IN NAZ084 '
                if (Global.isEscape()) return;
                pnd_Stts_Cde.setValue("L81");                                                                                                                             //Natural: MOVE 'L81' TO #STTS-CDE
                                                                                                                                                                          //Natural: PERFORM SET-STTS-CDS
                sub_Set_Stts_Cds();
                if (condition(Global.isEscape())) {return;}
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Lob().getValue(pnd_Dx).setValue(pdaAdsa401.getPnd_Adsa401_Pnd_Ia_Cntrct_Lob());                            //Natural: ASSIGN #CTLS-TRANS-TO-LOB ( #DX ) := #IA-CNTRCT-LOB
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Sub_Lob().getValue(pnd_Dx).setValue(pdaAdsa401.getPnd_Adsa401_Pnd_Ia_Cntrct_Sub_Lob());                    //Natural: ASSIGN #CTLS-TRANS-TO-SUB-LOB ( #DX ) := #IA-CNTRCT-SUB-LOB
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tiaa_No().getValue(pnd_Dx).setValue(pdaAdsa401.getPnd_Adsa401_Adp_Ia_Tiaa_Nbr());                          //Natural: ASSIGN #CTLS-TRANS-TO-TIAA-NO ( #DX ) := ADP-IA-TIAA-NBR
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Cref_No().getValue(pnd_Dx).setValue(pdaAdsa401.getPnd_Adsa401_Adp_Ia_Cref_Nbr());                          //Natural: ASSIGN #CTLS-TRANS-TO-CREF-NO ( #DX ) := ADP-IA-CREF-NBR
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Num_Units().getValue(pnd_Dx).setValue(0);                                                                  //Natural: ASSIGN #CTLS-TRANS-TO-NUM-UNITS ( #DX ) := 0
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Pits_Rqst_Id().getValue(pnd_Dx).setValue(" ");                                                                //Natural: ASSIGN #CTLS-TRANS-PITS-RQST-ID ( #DX ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ldgr_Typ().getValue(pnd_Dx).setValue(" ");                                                                    //Natural: ASSIGN #CTLS-TRANS-LDGR-TYP ( #DX ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Cncl_Rdrw().getValue(pnd_Dx).setValue(" ");                                                                   //Natural: ASSIGN #CTLS-TRANS-CNCL-RDRW ( #DX ) := ' '
        //*  IF ADP-SRVVR-IND = 'Y'
        //*  EM - 102915
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Srvvr_Ind().equals("B")))                                                                                             //Natural: IF ADP-SRVVR-IND = 'B'
        {
            ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind2().getValue(pnd_Dx).setValue("S");                                                                    //Natural: ASSIGN #CTLS-TRANS-IND2 ( #DX ) := 'S'
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind().getValue(pnd_Dx).setValue(pnd_Settl_Ind);                                                               //Natural: ASSIGN #CTLS-TRANS-IND ( #DX ) := #SETTL-IND
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Tax_Ind().getValue(pnd_Dx).setValue(" ");                                                                     //Natural: ASSIGN #CTLS-TRANS-TAX-IND ( #DX ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Net_Cash_Ind().getValue(pnd_Dx).setValue(" ");                                                                //Natural: ASSIGN #CTLS-TRANS-NET-CASH-IND ( #DX ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Instllmnt_Dte().getValue(pnd_Dx).setValue(0);                                                                 //Natural: ASSIGN #CTLS-TRANS-INSTLLMNT-DTE ( #DX ) := 0
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_1st_Yr_Rnwl_Ind().getValue(pnd_Dx).setValue(" ");                                                             //Natural: ASSIGN #CTLS-TRANS-1ST-YR-RNWL-IND ( #DX ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Subplan().getValue(pnd_Dx).setValue(" ");                                                                     //Natural: ASSIGN #CTLS-TRANS-SUBPLAN ( #DX ) := ' '
        ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Eor().getValue(pnd_Dx).setValue("X");                                                                         //Natural: ASSIGN #CTLS-TRANS-EOR ( #DX ) := 'X'
        pnd_Tot_Trans_Amt.nadd(pnd_Actl_Amt);                                                                                                                             //Natural: ADD #ACTL-AMT TO #TOT-TRANS-AMT
        pnd_Dx.nadd(1);                                                                                                                                                   //Natural: ADD 1 TO #DX
        pls_Ctls_Trlr_Dtl_Rcrd_Cnt.nadd(1);                                                                                                                               //Natural: ADD 1 TO +CTLS-TRLR-DTL-RCRD-CNT
        ldaIaalctls.getPnd_Ctls_Trans_Cnt().nadd(1);                                                                                                                      //Natural: ADD 1 TO #CTLS-TRANS-CNT
        //* **
        //*  DTL-TRANSACTION-AMT (#DX)         := #ACTL-AMT
        //*  IF ADP-ANNTY-OPTN = 'AC'
        //*   DTL-TRANSACTION-TYPE (#DX)      := 'AS'
        //*  ELSE
        //*   DTL-TRANSACTION-TYPE (#DX)      := 'CS'
        //*  END-IF
        //*  DTL-TYPE-RECORD (#DX)             := 'AD'
        //*  DTL-OCCURRENCE-NBR (#DX)         := #DX /* START WITH 1 ON SECOND CALL
        //*  DTL-FROM-TIAA-CONTRACT (#DX)      := ADC-TIAA-NBR
        //*  DTL-FROM-CREF-CONTRACT (#DX)      := ADC-CREF-NBR
        //* **DTL-FROM-LOB (#DX)                := #CNTRCT-LOB
        //*  MOVE SUBSTRING (ADC-SUB-PLAN-NBR,1,3) TO DTL-FROM-LOB(#DX)
        //*  EM - 100413
        //*  DTL-FROM-FUND-TICKER-SYMBOL (#DX) := ADC-INV-SHORT-NAME (#X)
        //* *DTL-FROM-SUB-LOB (#DX)            := #CNTRCT-SUB-LOB
        //*  DTL-TO-TIAA-CONTRACT (#DX)        := ADP-IA-TIAA-NBR
        //*  DTL-TO-CREF-CONTRACT (#DX)        := ADP-IA-CREF-NBR
        //* *DTL-TO-FUND-TICKER-SYMBOL (#DX)   := ADC-TKR-SYMBL (#X)
        //*  EM - 100413 START
        //*  IF +INVSTMNT-GRPNG-ID (#INDX) = #TIAA-STBL-ACCESS-REA-IC654 (*)
        //*    DTL-TO-FUND-TICKER-SYMBOL (#DX)   := ADC-INV-SHORT-NAME (#X)
        //*  ELSE
        //*    EXAMINE +IC654 (*) FOR +INVSTMNT-GRPNG-ID (#INDX)
        //*      GIVING INDEX #INDX1
        //*    IF #INDX1 > 0
        //*     DTL-TO-FUND-TICKER-SYMBOL (#DX)   := +SHORT-NAME (#INDX1)
        //*   ELSE
        //*     WRITE +INVSTMNT-GRPNG-ID (#INDX) ' NOT FOUND IN NAZ084 '
        //*     MOVE 'L81' TO #STTS-CDE
        //*     PERFORM SET-STTS-CDS
        //*     ESCAPE ROUTINE
        //*   END-IF
        //*  END-IF
        //*  EM - 100413 END
        //*  DTL-TO-LOB (#DX)                  := #IA-CNTRCT-LOB
        //*  DTL-TO-SUB-LOB (#DX)              := #IA-CNTRCT-SUB-LOB
        //*  DTL-UNLEDGER-INDICATOR (#DX)      := 'N'
        //*  DTL-REVERSAL-CODE (#DX)           := 'N'
        //*  CM 3/31 START
        //*  WRITE 'INTO ADSN430'                                          /* DEA
        //*  WRITE '=' ADP-SRVVR-IND                                       /* DEA
        //*  IF ADP-SRVVR-IND = 'Y'
        //*   WRITE 'INTO IF STATEMENT TO MOVE S'                         /* DEA
        //*   DTL-SETTLEMENT-IND2 (#DX)          := 'S'
        //*  END-IF
        //*  WRITE '=' DTL-SETTLEMENT-IND2 (#DX)                           /* DEA
        //*  DTL-SETTLEMENT-IND (#DX)          := #SETTL-IND
        //*  CM 3/31 END
        //* *DTL-SUB-PLAN-NUMBER (#DX)         := ADC-SUB-PLAN-NBR  /* TS 12/14/04
        //*  ADD 1 TO #DX
        //*  IF #DX > 50
        //*   PERFORM WRITE-HDR-RCRD-FIELDS
        //*   RESET LAMA1001.GROUP-PDA
        //*  PERFORM RESET-DETAIL-DATA
        //*  END-IF
        //*  EM - CALM15 END
    }
    private void sub_Move_Hdr_Rcrd_Data() throws Exception                                                                                                                //Natural: MOVE-HDR-RCRD-DATA
    {
        if (BLNatReinput.isReinput()) return;

        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Rcrd_Typ().setValue("H");                                                                                         //Natural: ASSIGN #CTLS-HDR-RCRD-TYP := 'H'
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Sqnce().setValue(pls_Ctls_Sqnce_Nbr);                                                                             //Natural: ASSIGN #CTLS-HDR-SQNCE := +CTLS-SQNCE-NBR
        pnd_Dte_Alpha.setValueEdited(pdaAdsa401.getPnd_Adsa401_Pnd_Cntl_Bsnss_Dte(),new ReportEditMask("YYYYMMDD"));                                                      //Natural: MOVE EDITED #CNTL-BSNSS-DTE ( EM = YYYYMMDD ) TO #DTE-ALPHA
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Jrnl_Dte().setValue(pnd_Dte_Alpha_Pnd_Dte);                                                                       //Natural: ASSIGN #CTLS-HDR-JRNL-DTE := #DTE
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Orgn_Sys().setValue("ADAS");                                                                                      //Natural: ASSIGN #CTLS-HDR-ORGN-SYS := 'ADAS'
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Rcrd_Flow_Stts().setValue("P");                                                                                   //Natural: ASSIGN #CTLS-HDR-RCRD-FLOW-STTS := 'P'
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Err_Cde().setValue(0);                                                                                            //Natural: ASSIGN #CTLS-HDR-ERR-CDE := 0
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Crte_Dte_Stmp().setValue(Global.getDATN());                                                                       //Natural: ASSIGN #CTLS-HDR-CRTE-DTE-STMP := *DATN
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Crte_Tme_Stmp().setValue(Global.getTIMN());                                                                       //Natural: ASSIGN #CTLS-HDR-CRTE-TME-STMP := *TIMN
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Rcvd_Dte_Stmp().setValue(ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Crte_Dte_Stmp());                        //Natural: ASSIGN #CTLS-HDR-RCVD-DTE-STMP := #CTLS-HDR-CRTE-DTE-STMP
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Rcvd_Tme_Stmp().setValue(ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Crte_Tme_Stmp());                        //Natural: ASSIGN #CTLS-HDR-RCVD-TME-STMP := #CTLS-HDR-CRTE-TME-STMP
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Prcss_Dte_Stmp().setValue(0);                                                                                     //Natural: ASSIGN #CTLS-HDR-PRCSS-DTE-STMP := 0
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Prcss_Tme_Stmp().setValue(0);                                                                                     //Natural: ASSIGN #CTLS-HDR-PRCSS-TME-STMP := 0
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Sent_To_Ps_Dte_Stmp().setValue(0);                                                                                //Natural: ASSIGN #CTLS-HDR-SENT-TO-PS-DTE-STMP := 0
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Sent_To_Ps_Tme_Stmp().setValue(0);                                                                                //Natural: ASSIGN #CTLS-HDR-SENT-TO-PS-TME-STMP := 0
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Intrfce_Dte().setValue(0);                                                                                        //Natural: ASSIGN #CTLS-HDR-INTRFCE-DTE := 0
        if (condition(ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ttl_Trns_Amt().less(getZero())))                                                                    //Natural: IF #CTLS-HDR-TTL-TRNS-AMT < 0
        {
            ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ttl_Trns_Amt_Sgn().setValue("-");                                                                             //Natural: ASSIGN #CTLS-HDR-TTL-TRNS-AMT-SGN := '-'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ttl_Trns_Amt_Sgn().setValue(" ");                                                                             //Natural: ASSIGN #CTLS-HDR-TTL-TRNS-AMT-SGN := ' '
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ttl_Trns_Amt().setValue(pnd_Tot_Trans_Amt);                                                                       //Natural: ASSIGN #CTLS-HDR-TTL-TRNS-AMT := #TOT-TRANS-AMT
        pnd_Dte_Alpha.setValueEdited(pdaAdsa401.getPnd_Adsa401_Adp_Effctv_Dte(),new ReportEditMask("YYYYMMDD"));                                                          //Natural: MOVE EDITED ADP-EFFCTV-DTE ( EM = YYYYMMDD ) TO #DTE-ALPHA
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Part_Dte().setValue(pnd_Dte_Alpha_Pnd_Dte);                                                                       //Natural: ASSIGN #CTLS-HDR-PART-DTE := #DTE
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ph_Num().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Unique_Id());                                                     //Natural: ASSIGN #CTLS-HDR-PH-NUM := ADP-UNIQUE-ID
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_St_Cde().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Frst_Annt_Rsdnc_Cde());                                           //Natural: ASSIGN #CTLS-HDR-ST-CDE := ADP-FRST-ANNT-RSDNC-CDE
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Bank_Cde().setValue(" ");                                                                                         //Natural: ASSIGN #CTLS-HDR-BANK-CDE := ' '
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ph_Nme().setValue(DbsUtil.compress(pdaAdsa401.getPnd_Adsa401_Adp_Frst_Annt_Frst_Nme(), pdaAdsa401.getPnd_Adsa401_Adp_Frst_Annt_Mid_Nme().getSubstring(1,1),  //Natural: COMPRESS ADP-FRST-ANNT-FRST-NME SUBSTR ( ADP-FRST-ANNT-MID-NME,1,1 ) ADP-FRST-ANNT-LST-NME INTO #CTLS-HDR-PH-NME
            pdaAdsa401.getPnd_Adsa401_Adp_Frst_Annt_Lst_Nme()));
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Cntrct_Typ().setValue(" ");                                                                                       //Natural: ASSIGN #CTLS-HDR-CNTRCT-TYP := ' '
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Pymnt_Rqst_Log_Dte().setValue(" ");                                                                               //Natural: ASSIGN #CTLS-HDR-PYMNT-RQST-LOG-DTE := ' '
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Pymnt_Rqst_Log_Tme().setValue(" ");                                                                               //Natural: ASSIGN #CTLS-HDR-PYMNT-RQST-LOG-TME := ' '
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Wpid().setValue(" ");                                                                                             //Natural: ASSIGN #CTLS-HDR-WPID := ' '
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Pymnmt_Dte().setValue(0);                                                                                         //Natural: ASSIGN #CTLS-HDR-PYMNMT-DTE := 0
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Pln_Nbr().setValue(" ");                                                                                          //Natural: ASSIGN #CTLS-HDR-PLN-NBR := ' '
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Schdl_Dte().setValue(0);                                                                                          //Natural: ASSIGN #CTLS-HDR-SCHDL-DTE := 0
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Mde_Of_Pymnt().setValue(" ");                                                                                     //Natural: ASSIGN #CTLS-HDR-MDE-OF-PYMNT := ' '
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ssn().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Frst_Annt_Ssn());                                                    //Natural: ASSIGN #CTLS-HDR-SSN := ADP-FRST-ANNT-SSN
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Csh_Cntrl_Acct_Id().setValue(" ");                                                                                //Natural: ASSIGN #CTLS-HDR-CSH-CNTRL-ACCT-ID := ' '
        ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Eor().setValue("X");                                                                                              //Natural: ASSIGN #CTLS-HDR-EOR := 'X'
        pls_Ctls_Trlr_Hdr_Rcrd_Cnt.nadd(1);                                                                                                                               //Natural: ADD 1 TO +CTLS-TRLR-HDR-RCRD-CNT
        pls_Ctls_Trlr_Amt_Ttl.nadd(pnd_Tot_Trans_Amt);                                                                                                                    //Natural: ADD #TOT-TRANS-AMT TO +CTLS-TRLR-AMT-TTL
        getWorkFiles().write(1, false, ldaIaalctls.getPnd_Ctls_Hdr_Record());                                                                                             //Natural: WRITE WORK FILE 1 #CTLS-HDR-RECORD
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-HDR-RECORD
            sub_Display_Hdr_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  EM - CALM15 END
    }
    private void sub_Move_Trans_Rcrd_Data() throws Exception                                                                                                              //Natural: MOVE-TRANS-RCRD-DATA
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO #CTLS-TRANS-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaIaalctls.getPnd_Ctls_Trans_Cnt())); pnd_I.nadd(1))
        {
            pnd_Trans_Record_Pnd_Trans_Rcrd_Typ.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rcrd_Typ().getValue(pnd_I));                                 //Natural: ASSIGN #TRANS-RCRD-TYP := #CTLS-TRANS-RCRD-TYP ( #I )
            pnd_Trans_Record_Pnd_Trans_Sqnce.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Sqnce().getValue(pnd_I));                                       //Natural: ASSIGN #TRANS-SQNCE := #CTLS-TRANS-SQNCE ( #I )
            pnd_Trans_Record_Pnd_Trans_Typ.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Typ().getValue(pnd_I));                                           //Natural: ASSIGN #TRANS-TYP := #CTLS-TRANS-TYP ( #I )
            pnd_Trans_Record_Pnd_Trans_Typ_Rcrd.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Typ_Rcrd().getValue(pnd_I));                                 //Natural: ASSIGN #TRANS-TYP-RCRD := #CTLS-TRANS-TYP-RCRD ( #I )
            pnd_Trans_Record_Pnd_Trans_Occrnce_Nbr.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Occrnce_Nbr().getValue(pnd_I));                           //Natural: ASSIGN #TRANS-OCCRNCE-NBR := #CTLS-TRANS-OCCRNCE-NBR ( #I )
            pnd_Trans_Record_Pnd_Trans_Rte_Basis.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rte_Basis().getValue(pnd_I));                               //Natural: ASSIGN #TRANS-RTE-BASIS := #CTLS-TRANS-RTE-BASIS ( #I )
            pnd_Trans_Record_Pnd_Trans_Undldgr_Ind.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Undldgr_Ind().getValue(pnd_I));                           //Natural: ASSIGN #TRANS-UNDLDGR-IND := #CTLS-TRANS-UNDLDGR-IND ( #I )
            pnd_Trans_Record_Pnd_Trans_Rvrsl_Cde.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Rvrsl_Cde().getValue(pnd_I));                               //Natural: ASSIGN #TRANS-RVRSL-CDE := #CTLS-TRANS-RVRSL-CDE ( #I )
            pnd_Trans_Record_Pnd_Trans_Dept_Id.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Dept_Id().getValue(pnd_I));                                   //Natural: ASSIGN #TRANS-DEPT-ID := #CTLS-TRANS-DEPT-ID ( #I )
            pnd_Trans_Record_Pnd_Trans_Allctn_Ndx.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Allctn_Ndx().getValue(pnd_I));                             //Natural: ASSIGN #TRANS-ALLCTN-NDX := #CTLS-TRANS-ALLCTN-NDX ( #I )
            pnd_Trans_Record_Pnd_Trans_Vltn_Basis.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Vltn_Basis().getValue(pnd_I));                             //Natural: ASSIGN #TRANS-VLTN-BASIS := #CTLS-TRANS-VLTN-BASIS ( #I )
            pnd_Trans_Record_Pnd_Trans_Amt_Sign.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt_Sign().getValue(pnd_I));                                 //Natural: ASSIGN #TRANS-AMT-SIGN := #CTLS-TRANS-AMT-SIGN ( #I )
            pnd_Trans_Record_Pnd_Trans_Amt.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Amt().getValue(pnd_I));                                           //Natural: ASSIGN #TRANS-AMT := #CTLS-TRANS-AMT ( #I )
            pnd_Trans_Record_Pnd_Trans_From_Tkr.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tkr().getValue(pnd_I));                                 //Natural: ASSIGN #TRANS-FROM-TKR := #CTLS-TRANS-FROM-TKR ( #I )
            pnd_Trans_Record_Pnd_Trans_From_Lob.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Lob().getValue(pnd_I));                                 //Natural: ASSIGN #TRANS-FROM-LOB := #CTLS-TRANS-FROM-LOB ( #I )
            pnd_Trans_Record_Pnd_Trans_From_Sub_Lob.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Sub_Lob().getValue(pnd_I));                         //Natural: ASSIGN #TRANS-FROM-SUB-LOB := #CTLS-TRANS-FROM-SUB-LOB ( #I )
            pnd_Trans_Record_Pnd_Trans_From_Tiaa_No.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Tiaa_No().getValue(pnd_I));                         //Natural: ASSIGN #TRANS-FROM-TIAA-NO := #CTLS-TRANS-FROM-TIAA-NO ( #I )
            pnd_Trans_Record_Pnd_Trans_From_Cref_No.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Cref_No().getValue(pnd_I));                         //Natural: ASSIGN #TRANS-FROM-CREF-NO := #CTLS-TRANS-FROM-CREF-NO ( #I )
            pnd_Trans_Record_Pnd_Trans_From_Num_Units.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_From_Num_Units().getValue(pnd_I));                     //Natural: ASSIGN #TRANS-FROM-NUM-UNITS := #CTLS-TRANS-FROM-NUM-UNITS ( #I )
            pnd_Trans_Record_Pnd_Trans_To_Tkr.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tkr().getValue(pnd_I));                                     //Natural: ASSIGN #TRANS-TO-TKR := #CTLS-TRANS-TO-TKR ( #I )
            pnd_Trans_Record_Pnd_Trans_To_Lob.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Lob().getValue(pnd_I));                                     //Natural: ASSIGN #TRANS-TO-LOB := #CTLS-TRANS-TO-LOB ( #I )
            pnd_Trans_Record_Pnd_Trans_To_Sub_Lob.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Sub_Lob().getValue(pnd_I));                             //Natural: ASSIGN #TRANS-TO-SUB-LOB := #CTLS-TRANS-TO-SUB-LOB ( #I )
            pnd_Trans_Record_Pnd_Trans_To_Tiaa_No.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Tiaa_No().getValue(pnd_I));                             //Natural: ASSIGN #TRANS-TO-TIAA-NO := #CTLS-TRANS-TO-TIAA-NO ( #I )
            pnd_Trans_Record_Pnd_Trans_To_Cref_No.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Cref_No().getValue(pnd_I));                             //Natural: ASSIGN #TRANS-TO-CREF-NO := #CTLS-TRANS-TO-CREF-NO ( #I )
            pnd_Trans_Record_Pnd_Trans_To_Num_Units.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_To_Num_Units().getValue(pnd_I));                         //Natural: ASSIGN #TRANS-TO-NUM-UNITS := #CTLS-TRANS-TO-NUM-UNITS ( #I )
            pnd_Trans_Record_Pnd_Trans_Pits_Rqst_Id.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Pits_Rqst_Id().getValue(pnd_I));                         //Natural: ASSIGN #TRANS-PITS-RQST-ID := #CTLS-TRANS-PITS-RQST-ID ( #I )
            pnd_Trans_Record_Pnd_Trans_Ldgr_Typ.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ldgr_Typ().getValue(pnd_I));                                 //Natural: ASSIGN #TRANS-LDGR-TYP := #CTLS-TRANS-LDGR-TYP ( #I )
            pnd_Trans_Record_Pnd_Trans_Cncl_Rdrw.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Cncl_Rdrw().getValue(pnd_I));                               //Natural: ASSIGN #TRANS-CNCL-RDRW := #CTLS-TRANS-CNCL-RDRW ( #I )
            pnd_Trans_Record_Pnd_Trans_Ind2.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind2().getValue(pnd_I));                                         //Natural: ASSIGN #TRANS-IND2 := #CTLS-TRANS-IND2 ( #I )
            pnd_Trans_Record_Pnd_Trans_Ind.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Ind().getValue(pnd_I));                                           //Natural: ASSIGN #TRANS-IND := #CTLS-TRANS-IND ( #I )
            pnd_Trans_Record_Pnd_Trans_Tax_Ind.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Tax_Ind().getValue(pnd_I));                                   //Natural: ASSIGN #TRANS-TAX-IND := #CTLS-TRANS-TAX-IND ( #I )
            pnd_Trans_Record_Pnd_Trans_Net_Cash_Ind.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Net_Cash_Ind().getValue(pnd_I));                         //Natural: ASSIGN #TRANS-NET-CASH-IND := #CTLS-TRANS-NET-CASH-IND ( #I )
            pnd_Trans_Record_Pnd_Trans_Instllmnt_Dte.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Instllmnt_Dte().getValue(pnd_I));                       //Natural: ASSIGN #TRANS-INSTLLMNT-DTE := #CTLS-TRANS-INSTLLMNT-DTE ( #I )
            pnd_Trans_Record_Pnd_Trans_1st_Yr_Rnwl_Ind.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_1st_Yr_Rnwl_Ind().getValue(pnd_I));                   //Natural: ASSIGN #TRANS-1ST-YR-RNWL-IND := #CTLS-TRANS-1ST-YR-RNWL-IND ( #I )
            pnd_Trans_Record_Pnd_Trans_Subplan.setValue(ldaIaalctls.getPnd_Ctls_Trans_Record_Pnd_Ctls_Trans_Subplan().getValue(pnd_I));                                   //Natural: ASSIGN #TRANS-SUBPLAN := #CTLS-TRANS-SUBPLAN ( #I )
            pnd_Trans_Record_Pnd_Trans_Eor.setValue("X");                                                                                                                 //Natural: ASSIGN #TRANS-EOR := 'X'
            getWorkFiles().write(1, false, pnd_Trans_Record);                                                                                                             //Natural: WRITE WORK FILE 1 #TRANS-RECORD
            if (condition(pls_Debug_On.getBoolean()))                                                                                                                     //Natural: IF +DEBUG-ON
            {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-TRANS-RECORD
                sub_Display_Trans_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  EM - CALM15 END
    }
    private void sub_Move_Trlr_Rcrd_Data() throws Exception                                                                                                               //Natural: MOVE-TRLR-RCRD-DATA
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Trlr_Run_Tme.setValue(Global.getTIMN());                                                                                                                      //Natural: ASSIGN #TRLR-RUN-TME := *TIMN
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Rcrd_Typ().setValue("C");                                                                                       //Natural: ASSIGN #CTLS-TRLR-RCRD-TYP := 'C'
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Run_Dte().setValue(Global.getDATN());                                                                           //Natural: ASSIGN #CTLS-TRLR-RUN-DTE := *DATN
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Begin_Run_Tme().setValue(pnd_Trlr_Run_Tme_Pnd_Trlr_Run_Time);                                                   //Natural: ASSIGN #CTLS-TRLR-BEGIN-RUN-TME := #TRLR-RUN-TIME
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_End_Run_Tme().setValue(pnd_Trlr_Run_Tme_Pnd_Trlr_Run_Time);                                                     //Natural: ASSIGN #CTLS-TRLR-END-RUN-TME := #TRLR-RUN-TIME
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Hdr_Rcrd_Cnt().setValue(pls_Ctls_Trlr_Hdr_Rcrd_Cnt);                                                            //Natural: ASSIGN #CTLS-TRLR-HDR-RCRD-CNT := +CTLS-TRLR-HDR-RCRD-CNT
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Dtl_Rcrd_Cnt().setValue(pls_Ctls_Trlr_Dtl_Rcrd_Cnt);                                                            //Natural: ASSIGN #CTLS-TRLR-DTL-RCRD-CNT := +CTLS-TRLR-DTL-RCRD-CNT
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Ttl_Rcrd_Cnt().compute(new ComputeParameters(false, pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Ttl_Rcrd_Cnt()),  //Natural: ASSIGN #CTLS-TRLR-TTL-RCRD-CNT := +CTLS-TRLR-HDR-RCRD-CNT + +CTLS-TRLR-DTL-RCRD-CNT
            pls_Ctls_Trlr_Hdr_Rcrd_Cnt.add(pls_Ctls_Trlr_Dtl_Rcrd_Cnt));
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Hdr_Amt_Ttl().setValue(pls_Ctls_Trlr_Amt_Ttl);                                                                  //Natural: ASSIGN #CTLS-TRLR-HDR-AMT-TTL := +CTLS-TRLR-AMT-TTL
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Dtl_Amt_Ttl().setValue(pls_Ctls_Trlr_Amt_Ttl);                                                                  //Natural: ASSIGN #CTLS-TRLR-DTL-AMT-TTL := +CTLS-TRLR-AMT-TTL
        pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Eor().setValue("X");                                                                                            //Natural: ASSIGN #CTLS-TRLR-EOR := 'X'
        getWorkFiles().write(1, false, pdaIaaactls.getPnd_Ctls_Trlr_Record());                                                                                            //Natural: WRITE WORK FILE 1 #CTLS-TRLR-RECORD
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-TRLR-RECORD
            sub_Display_Trlr_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  EM - CALM15 END
    }
    private void sub_Display_Hdr_Record() throws Exception                                                                                                                //Natural: DISPLAY-HDR-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        getReports().write(0, NEWLINE,"*",new RepeatItem(25));                                                                                                            //Natural: WRITE / '*' ( 25 )
        if (Global.isEscape()) return;
        getReports().write(0, "CTLS HEADER RECORD",NEWLINE,NEWLINE,"*",new RepeatItem(25),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Rcrd_Typ(),         //Natural: WRITE 'CTLS HEADER RECORD' / / '*' ( 25 ) / '=' #CTLS-HDR-RCRD-TYP / '=' #CTLS-HDR-SQNCE / '=' #CTLS-HDR-JRNL-DTE / '=' #CTLS-HDR-ORGN-SYS / '=' #CTLS-HDR-RCRD-FLOW-STTS / '=' #CTLS-HDR-ERR-CDE / '=' #CTLS-HDR-CRTE-DTE-STMP / '=' #CTLS-HDR-CRTE-TME-STMP / '=' #CTLS-HDR-RCVD-DTE-STMP / '=' #CTLS-HDR-RCVD-TME-STMP / '=' #CTLS-HDR-PRCSS-DTE-STMP / '=' #CTLS-HDR-PRCSS-TME-STMP / '=' #CTLS-HDR-SENT-TO-PS-DTE-STMP / '=' #CTLS-HDR-SENT-TO-PS-TME-STMP / '=' #CTLS-HDR-INTRFCE-DTE / '=' #CTLS-HDR-TTL-TRNS-AMT-SGN / '=' #CTLS-HDR-TTL-TRNS-AMT / '=' #CTLS-HDR-PART-DTE / '=' #CTLS-HDR-PH-NUM / '=' #CTLS-HDR-ST-CDE / '=' #CTLS-HDR-BANK-CDE
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Sqnce(),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Jrnl_Dte(),NEWLINE,
            "=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Orgn_Sys(),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Rcrd_Flow_Stts(),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Err_Cde(),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Crte_Dte_Stmp(),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Crte_Tme_Stmp(),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Rcvd_Dte_Stmp(),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Rcvd_Tme_Stmp(),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Prcss_Dte_Stmp(),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Prcss_Tme_Stmp(),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Sent_To_Ps_Dte_Stmp(),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Sent_To_Ps_Tme_Stmp(),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Intrfce_Dte(),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ttl_Trns_Amt_Sgn(),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ttl_Trns_Amt(),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Part_Dte(),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ph_Num(),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_St_Cde(),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Bank_Cde());
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, "=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ph_Nme(),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Cntrct_Typ(),      //Natural: WRITE '=' #CTLS-HDR-PH-NME / '=' #CTLS-HDR-CNTRCT-TYP / '=' #CTLS-HDR-PYMNT-RQST-LOG-DTE / '=' #CTLS-HDR-PYMNT-RQST-LOG-TME / '=' #CTLS-HDR-WPID / '=' #CTLS-HDR-PYMNMT-DTE / '=' #CTLS-HDR-PLN-NBR / '=' #CTLS-HDR-SCHDL-DTE / '=' #CTLS-HDR-MDE-OF-PYMNT / '=' #CTLS-HDR-SSN / '=' #CTLS-HDR-CSH-CNTRL-ACCT-ID / '=' #CTLS-HDR-EOR /
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Pymnt_Rqst_Log_Dte(),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Pymnt_Rqst_Log_Tme(),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Wpid(),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Pymnmt_Dte(),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Pln_Nbr(),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Schdl_Dte(),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Mde_Of_Pymnt(),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Ssn(),
            NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Csh_Cntrl_Acct_Id(),NEWLINE,"=",ldaIaalctls.getPnd_Ctls_Hdr_Record_Pnd_Ctls_Hdr_Eor(),
            NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Display_Trans_Record() throws Exception                                                                                                              //Natural: DISPLAY-TRANS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        getReports().write(0, NEWLINE,"*",new RepeatItem(25));                                                                                                            //Natural: WRITE / '*' ( 25 )
        if (Global.isEscape()) return;
        getReports().write(0, "CTLS TRANS RECORD",NEWLINE,NEWLINE,"*",new RepeatItem(25),NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Rcrd_Typ,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Sqnce, //Natural: WRITE 'CTLS TRANS RECORD' / / '*' ( 25 ) / '=' #TRANS-RCRD-TYP / '=' #TRANS-SQNCE / '=' #TRANS-TYP / '=' #TRANS-TYP-RCRD / '=' #TRANS-OCCRNCE-NBR / '=' #TRANS-RTE-BASIS / '=' #TRANS-UNDLDGR-IND / '=' #TRANS-RVRSL-CDE / '=' #TRANS-DEPT-ID / '=' #TRANS-ALLCTN-NDX / '=' #TRANS-VLTN-BASIS / '=' #TRANS-AMT-SIGN / '=' #TRANS-AMT / '=' #TRANS-FROM-TKR / '=' #TRANS-FROM-LOB / '=' #TRANS-FROM-SUB-LOB / '=' #TRANS-FROM-TIAA-NO / '=' #TRANS-FROM-CREF-NO / '=' #TRANS-FROM-NUM-UNITS / '=' #TRANS-TO-TKR / '=' #TRANS-TO-LOB
            NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Typ,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Typ_Rcrd,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Occrnce_Nbr,
            NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Rte_Basis,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Undldgr_Ind,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Rvrsl_Cde,
            NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Dept_Id,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Allctn_Ndx,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Vltn_Basis,
            NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Amt_Sign,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Amt,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_From_Tkr,NEWLINE,
            "=",pnd_Trans_Record_Pnd_Trans_From_Lob,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_From_Sub_Lob,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_From_Tiaa_No,
            NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_From_Cref_No,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_From_Num_Units,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_To_Tkr,
            NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_To_Lob);
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, "=",pnd_Trans_Record_Pnd_Trans_To_Sub_Lob,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_To_Tiaa_No,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_To_Cref_No, //Natural: WRITE '=' #TRANS-TO-SUB-LOB / '=' #TRANS-TO-TIAA-NO / '=' #TRANS-TO-CREF-NO / '=' #TRANS-TO-NUM-UNITS / '=' #TRANS-PITS-RQST-ID / '=' #TRANS-LDGR-TYP / '=' #TRANS-CNCL-RDRW / '=' #TRANS-IND2 / '=' #TRANS-IND / '=' #TRANS-TAX-IND / '=' #TRANS-NET-CASH-IND / '=' #TRANS-INSTLLMNT-DTE / '=' #TRANS-1ST-YR-RNWL-IND / '=' #TRANS-SUBPLAN / '=' #TRANS-EOR
            NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_To_Num_Units,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Pits_Rqst_Id,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Ldgr_Typ,
            NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Cncl_Rdrw,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Ind2,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Ind,NEWLINE,
            "=",pnd_Trans_Record_Pnd_Trans_Tax_Ind,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Net_Cash_Ind,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Instllmnt_Dte,
            NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_1st_Yr_Rnwl_Ind,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Subplan,NEWLINE,"=",pnd_Trans_Record_Pnd_Trans_Eor);
        if (Global.isEscape()) return;
    }
    private void sub_Display_Trlr_Record() throws Exception                                                                                                               //Natural: DISPLAY-TRLR-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().write(0, NEWLINE,"*",new RepeatItem(25));                                                                                                            //Natural: WRITE / '*' ( 25 )
        if (Global.isEscape()) return;
        getReports().write(0, "CTLS TRLR RECORD",NEWLINE,NEWLINE,"*",new RepeatItem(25),NEWLINE,"=",pnd_Trlr_Run_Tme,NEWLINE,"=",pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Rcrd_Typ(), //Natural: WRITE 'CTLS TRLR RECORD' / / '*' ( 25 ) / '=' #TRLR-RUN-TME / '=' #CTLS-TRLR-RCRD-TYP / '=' #CTLS-TRLR-RUN-DTE / '=' #CTLS-TRLR-BEGIN-RUN-TME / '=' #CTLS-TRLR-END-RUN-TME / '=' #CTLS-TRLR-TTL-RCRD-CNT / '=' #CTLS-TRLR-HDR-AMT-TTL / '=' #CTLS-TRLR-DTL-AMT-TTL / '=' #CTLS-TRLR-EOR
            NEWLINE,"=",pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Run_Dte(),NEWLINE,"=",pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Begin_Run_Tme(),
            NEWLINE,"=",pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_End_Run_Tme(),NEWLINE,"=",pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Ttl_Rcrd_Cnt(),
            NEWLINE,"=",pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Hdr_Amt_Ttl(),NEWLINE,"=",pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Dtl_Amt_Ttl(),
            NEWLINE,"=",pdaIaaactls.getPnd_Ctls_Trlr_Record_Pnd_Ctls_Trlr_Eor());
        if (Global.isEscape()) return;
        //*  EM - CALM15 END
    }
    //*  EM 100413 START
    private void sub_Get_Short_Names() throws Exception                                                                                                                   //Natural: GET-SHORT-NAMES
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                         //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ084");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ084'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("ADS");                                                                                                          //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'ADS'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id.setValue("BND2");                                                                                                         //Natural: ASSIGN #NAZ-TABLE-LVL3-ID := 'BND2'
        vw_naz_Table_Ddm.startDatabaseRead                                                                                                                                //Natural: READ NAZ-TABLE-DDM BY NAZ-TBL-SUPER3 STARTING FROM #NAZ-TABLE-KEY
        (
        "READ01",
        new Wc[] { new Wc("NAZ_TBL_SUPER3", ">=", pnd_Naz_Table_Key, WcType.BY) },
        new Oc[] { new Oc("NAZ_TBL_SUPER3", "ASC") }
        );
        READ01:
        while (condition(vw_naz_Table_Ddm.readNextRow("READ01")))
        {
            if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Tbl_Rcrd_Typ_Ind) || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id)  //Natural: IF NAZ-TBL-RCRD-TYP-IND NE #NAZ-TBL-RCRD-TYP-IND OR NAZ-TBL-RCRD-LVL1-ID NE #NAZ-TABLE-LVL1-ID OR NAZ-TBL-RCRD-LVL2-ID NE #NAZ-TABLE-LVL2-ID
                || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id)))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_I.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #I
            if (condition(pnd_I.greater(pdaAdsa401.getPnd_Adsa401_Pnd_Fund_Cnt())))                                                                                       //Natural: IF #I GT #FUND-CNT
            {
                getReports().write(0, pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id," fund table overflow in ",Global.getPROGRAM());                                            //Natural: WRITE #NAZ-TABLE-LVL1-ID ' fund table overflow in ' *PROGRAM
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(99);  if (true) return;                                                                                                                 //Natural: TERMINATE 99
            }                                                                                                                                                             //Natural: END-IF
            pls_Ic654.getValue(pnd_I).setValue(naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.getSubstring(1,4));                                                                     //Natural: ASSIGN +IC654 ( #I ) := SUBSTR ( NAZ-TBL-RCRD-LVL3-ID,1,4 )
            pls_Short_Name.getValue(pnd_I).setValue(naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.getSubstring(1,5));                                                            //Natural: ASSIGN +SHORT-NAME ( #I ) := SUBSTR ( NAZ-TBL-RCRD-DSCRPTN-TXT,1,5 )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pls_Ic654.getValue(1).equals(" ")))                                                                                                                 //Natural: IF +IC654 ( 1 ) = ' '
        {
            getReports().write(0, pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id," fund table is not found");                                                                    //Natural: WRITE #NAZ-TABLE-LVL1-ID ' fund table is not found'
            if (Global.isEscape()) return;
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
        //*  EM - 100413 END
    }
    private void sub_Set_Stts_Cds() throws Exception                                                                                                                      //Natural: SET-STTS-CDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        //*  BACKOUT TRANSACTION
        pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue(pnd_Stts_Cde);                                                                                              //Natural: MOVE #STTS-CDE TO #ERROR-STATUS
        //*  ADC-ERR-CDE (1)
        //*  COMPRESS LAMA1001.RETURN-CDE '-' LAMA1001.RETURN-MSG INTO
        //*   ADC-ERR-MSG (1)
        //*  FOR #I = 1 TO #FUND-CNT
        //*   IF ADC-TKR-SYMBL (#I) = ' '
        //*     ESCAPE BOTTOM
        //*   END-IF
        //*  MOVE #STTS-CDE TO ADC-ACCT-STTS-CDE (#I)
        //*  END-FOR
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=24 SG=OFF");
    }
}
