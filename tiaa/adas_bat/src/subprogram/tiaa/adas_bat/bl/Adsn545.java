/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:54:29 PM
**        * FROM NATURAL SUBPROGRAM : Adsn545
************************************************************
**        * FILE NAME            : Adsn545.java
**        * CLASS NAME           : Adsn545
**        * INSTANCE NAME        : Adsn545
************************************************************
************************************************************************
* PROGRAM  : ADSN545
* SYSTEM   : ADAS - ANNUITIAZTION SUNGUARD
* TITLE    : CALLS TAX WITHHOLDING SYSTEM
* WRITTEN  : MAY, 2006
* AUTHOR   : COPIED FROM NAZN545 AND MODIFIED BY MARNA NACHBER
*
* E. MELNIK 03172008   ROTH 403/401K CHANGES.  EM 031708
* O. SOTTO  03052012   FIX FOR INC1648735 - TAX ELECTION FOR
*                      DEFERRED COMP PLANS.  SC 030512.
* O. SOTTO  07/29/2014 FIX FOR 100% TAX ELECTION SHOWING AS 0.00%.
*                      CHANGES MARKED 072014.
* O. MELNIK 09/28/2018 CONNECTICUT TAX CHANGES MARKED 092818.
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn545 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAdsa540 pdaAdsa540;
    private PdaTxwa0010 pdaTxwa0010;

    // Local Variables
    public DbsRecord localVariables;

    // parameters

    private DbsGroup pnd_Tax_Errors_Info;
    private DbsField pnd_Tax_Errors_Info_Pnd_Tax_Error_Code;
    private DbsField pnd_Tax_Errors_Info_Pnd_Tax_Error_Desc;
    private DbsField pnd_Nra_Frm_W8_Info;
    private DbsField pnd_Nra_Frm_W9_Info;
    private DbsField pnd_Fed_Amt_Lit;
    private DbsField pnd_Sp_Allw;
    private DbsField pnd_Age_Allw;
    private DbsField pnd_Sp_Age_Allw;
    private DbsField pnd_Bl_Allw;
    private DbsField pnd_Sp_Bl_Allw;
    private DbsField pnd_Tax_Addl_Amt;
    private DbsField pnd_Tax_Flat_Amt;
    private DbsField pnd_Tax_Fix_Pct;
    private DbsField pnd_Tax_Exempt;
    private DbsField pnd_Allw_Cnt;
    private DbsField pnd_Tax_D_Amt;
    private DbsField pnd_Filing_Stat;
    private DbsField pnd_W9_Not_Rcvd;
    private DbsField pnd_W9_Rcvd;
    private DbsField pnd_W8_Not_Rcvd;
    private DbsField pnd_W8_Rcvd;
    private DbsField pnd_And_Txt;
    private DbsField pnd_And_Txt2;
    private DbsField pnd_With_Txt;
    private DbsField pnd_Allw_Txt;
    private DbsField pnd_Flat_Txt2;
    private DbsField pnd_Flat_Txt;
    private DbsField pnd_Addl_Txt;
    private DbsField pnd_Exempt;
    private DbsField pnd_Ls_Default;
    private DbsField pnd_Pp_Default;
    private DbsField pnd_Tax_Amt_Lit;
    private DbsField pnd_Mdo_Default;
    private DbsField pnd_Rtb_Default;
    private DbsField pnd_No_With_Allowed;
    private DbsField pnd_Do_Not_Withhold;
    private DbsField pnd_Stat_Dscr;
    private DbsField pnd_Deflt1;
    private DbsField pnd_Deflt2;
    private DbsField pnd_Deflt3;
    private DbsField pnd_Nra_Msg;
    private DbsField pnd_Defltac1;
    private DbsField pnd_Defltac2;
    private DbsField pnd_Ct_Cds;

    private DbsGroup pnd_Ct_Cds__R_Field_1;

    private DbsGroup pnd_Ct_Cds_Pnd_Ct_Cds_Array;
    private DbsField pnd_Ct_Cds_Pnd_Ct_Filing_Stts;

    private DbsGroup pnd_Ct_Cds__R_Field_2;
    private DbsField pnd_Ct_Cds_Pnd_Ct_Filing_Stts_N;
    private DbsField pnd_Ct_Cds_Pnd_Ct_With_Cde;
    private DbsField pnd_Ct_Filing_Status;
    private DbsField pnd_Ct_With_Txt;
    private DbsField pnd_Ct_Addl_Txt;
    private DbsField pnd_Ct_With_Cde_E;
    private DbsField pnd_Var_Text_Msg;

    private DbsGroup pnd_Var_Text_Msg__R_Field_3;
    private DbsField pnd_Var_Text_Msg_Pnd_Var_Text1;
    private DbsField pnd_Var_Text_Msg_Pnd_Var_Text2;
    private DbsField pnd_Var_Text_Msg_Pnd_Var_Text3;
    private DbsField pnd_Var_Text_Msg_Pnd_Var_Text4;
    private DbsField pnd_Var_Text_Msg_Pnd_Var_Text5;
    private DbsField pnd_Var_Text_Msg_Pnd_Var_Text6;
    private DbsField pnd_Var_Text_Msg_Pnd_Var_Text7;
    private DbsField pnd_Var_Texta;
    private DbsField pnd_Comma;
    private DbsField pnd_Nra_1001_Flag;
    private DbsField pnd_Nra_Flag;
    private DbsField pnd_Us_Res_Flag;
    private DbsField ws_Edit_Pct;
    private DbsField ws_Amt_Out;
    private DbsField ws_Work_Flat_Amt;
    private DbsField ws_Edit_Flat_Amt;
    private DbsField ws_Msg_Out;
    private DbsField pnd_A;
    private DbsField pnd_I;
    private DbsField pnd_Pp;
    private DbsField pnd_Ls;
    private DbsField testing_Mode;
    private DbsField pnd_Prod_Field;
    private DbsField pnd_Disp_Msg;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTxwa0010 = new PdaTxwa0010(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaAdsa540 = new PdaAdsa540(parameters);

        pnd_Tax_Errors_Info = parameters.newGroupInRecord("pnd_Tax_Errors_Info", "#TAX-ERRORS-INFO");
        pnd_Tax_Errors_Info.setParameterOption(ParameterOption.ByReference);
        pnd_Tax_Errors_Info_Pnd_Tax_Error_Code = pnd_Tax_Errors_Info.newFieldInGroup("pnd_Tax_Errors_Info_Pnd_Tax_Error_Code", "#TAX-ERROR-CODE", FieldType.STRING, 
            4);
        pnd_Tax_Errors_Info_Pnd_Tax_Error_Desc = pnd_Tax_Errors_Info.newFieldInGroup("pnd_Tax_Errors_Info_Pnd_Tax_Error_Desc", "#TAX-ERROR-DESC", FieldType.STRING, 
            44);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Nra_Frm_W8_Info = localVariables.newFieldInRecord("pnd_Nra_Frm_W8_Info", "#NRA-FRM-W8-INFO", FieldType.STRING, 1);
        pnd_Nra_Frm_W9_Info = localVariables.newFieldInRecord("pnd_Nra_Frm_W9_Info", "#NRA-FRM-W9-INFO", FieldType.STRING, 1);
        pnd_Fed_Amt_Lit = localVariables.newFieldInRecord("pnd_Fed_Amt_Lit", "#FED-AMT-LIT", FieldType.STRING, 34);
        pnd_Sp_Allw = localVariables.newFieldInRecord("pnd_Sp_Allw", "#SP-ALLW", FieldType.STRING, 18);
        pnd_Age_Allw = localVariables.newFieldInRecord("pnd_Age_Allw", "#AGE-ALLW", FieldType.STRING, 14);
        pnd_Sp_Age_Allw = localVariables.newFieldInRecord("pnd_Sp_Age_Allw", "#SP-AGE-ALLW", FieldType.STRING, 22);
        pnd_Bl_Allw = localVariables.newFieldInRecord("pnd_Bl_Allw", "#BL-ALLW", FieldType.STRING, 16);
        pnd_Sp_Bl_Allw = localVariables.newFieldInRecord("pnd_Sp_Bl_Allw", "#SP-BL-ALLW", FieldType.STRING, 23);
        pnd_Tax_Addl_Amt = localVariables.newFieldInRecord("pnd_Tax_Addl_Amt", "#TAX-ADDL-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Tax_Flat_Amt = localVariables.newFieldInRecord("pnd_Tax_Flat_Amt", "#TAX-FLAT-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Tax_Fix_Pct = localVariables.newFieldInRecord("pnd_Tax_Fix_Pct", "#TAX-FIX-PCT", FieldType.STRING, 7);
        pnd_Tax_Exempt = localVariables.newFieldInRecord("pnd_Tax_Exempt", "#TAX-EXEMPT", FieldType.STRING, 2);
        pnd_Allw_Cnt = localVariables.newFieldInRecord("pnd_Allw_Cnt", "#ALLW-CNT", FieldType.STRING, 2);
        pnd_Tax_D_Amt = localVariables.newFieldInRecord("pnd_Tax_D_Amt", "#TAX-D-AMT", FieldType.STRING, 11);
        pnd_Filing_Stat = localVariables.newFieldInRecord("pnd_Filing_Stat", "#FILING-STAT", FieldType.STRING, 45);
        pnd_W9_Not_Rcvd = localVariables.newFieldInRecord("pnd_W9_Not_Rcvd", "#W9-NOT-RCVD", FieldType.STRING, 42);
        pnd_W9_Rcvd = localVariables.newFieldInRecord("pnd_W9_Rcvd", "#W9-RCVD", FieldType.STRING, 42);
        pnd_W8_Not_Rcvd = localVariables.newFieldInRecord("pnd_W8_Not_Rcvd", "#W8-NOT-RCVD", FieldType.STRING, 42);
        pnd_W8_Rcvd = localVariables.newFieldInRecord("pnd_W8_Rcvd", "#W8-RCVD", FieldType.STRING, 42);
        pnd_And_Txt = localVariables.newFieldInRecord("pnd_And_Txt", "#AND-TXT", FieldType.STRING, 3);
        pnd_And_Txt2 = localVariables.newFieldInRecord("pnd_And_Txt2", "#AND-TXT2", FieldType.STRING, 4);
        pnd_With_Txt = localVariables.newFieldInRecord("pnd_With_Txt", "#WITH-TXT", FieldType.STRING, 4);
        pnd_Allw_Txt = localVariables.newFieldInRecord("pnd_Allw_Txt", "#ALLW-TXT", FieldType.STRING, 13);
        pnd_Flat_Txt2 = localVariables.newFieldInRecord("pnd_Flat_Txt2", "#FLAT-TXT2", FieldType.STRING, 17);
        pnd_Flat_Txt = localVariables.newFieldInRecord("pnd_Flat_Txt", "#FLAT-TXT", FieldType.STRING, 15);
        pnd_Addl_Txt = localVariables.newFieldInRecord("pnd_Addl_Txt", "#ADDL-TXT", FieldType.STRING, 23);
        pnd_Exempt = localVariables.newFieldInRecord("pnd_Exempt", "#EXEMPT", FieldType.STRING, 32);
        pnd_Ls_Default = localVariables.newFieldInRecord("pnd_Ls_Default", "#LS-DEFAULT", FieldType.STRING, 49);
        pnd_Pp_Default = localVariables.newFieldInRecord("pnd_Pp_Default", "#PP-DEFAULT", FieldType.STRING, 49);
        pnd_Tax_Amt_Lit = localVariables.newFieldInRecord("pnd_Tax_Amt_Lit", "#TAX-AMT-LIT", FieldType.STRING, 21);
        pnd_Mdo_Default = localVariables.newFieldInRecord("pnd_Mdo_Default", "#MDO-DEFAULT", FieldType.STRING, 4);
        pnd_Rtb_Default = localVariables.newFieldInRecord("pnd_Rtb_Default", "#RTB-DEFAULT", FieldType.STRING, 6);
        pnd_No_With_Allowed = localVariables.newFieldInRecord("pnd_No_With_Allowed", "#NO-WITH-ALLOWED", FieldType.STRING, 23);
        pnd_Do_Not_Withhold = localVariables.newFieldInRecord("pnd_Do_Not_Withhold", "#DO-NOT-WITHHOLD", FieldType.STRING, 16);
        pnd_Stat_Dscr = localVariables.newFieldArrayInRecord("pnd_Stat_Dscr", "#STAT-DSCR", FieldType.STRING, 45, new DbsArrayController(1, 9));
        pnd_Deflt1 = localVariables.newFieldInRecord("pnd_Deflt1", "#DEFLT1", FieldType.STRING, 46);
        pnd_Deflt2 = localVariables.newFieldInRecord("pnd_Deflt2", "#DEFLT2", FieldType.STRING, 45);
        pnd_Deflt3 = localVariables.newFieldInRecord("pnd_Deflt3", "#DEFLT3", FieldType.STRING, 47);
        pnd_Nra_Msg = localVariables.newFieldInRecord("pnd_Nra_Msg", "#NRA-MSG", FieldType.STRING, 49);
        pnd_Defltac1 = localVariables.newFieldInRecord("pnd_Defltac1", "#DEFLTAC1", FieldType.STRING, 46);
        pnd_Defltac2 = localVariables.newFieldInRecord("pnd_Defltac2", "#DEFLTAC2", FieldType.STRING, 45);
        pnd_Ct_Cds = localVariables.newFieldInRecord("pnd_Ct_Cds", "#CT-CDS", FieldType.STRING, 10);

        pnd_Ct_Cds__R_Field_1 = localVariables.newGroupInRecord("pnd_Ct_Cds__R_Field_1", "REDEFINE", pnd_Ct_Cds);

        pnd_Ct_Cds_Pnd_Ct_Cds_Array = pnd_Ct_Cds__R_Field_1.newGroupArrayInGroup("pnd_Ct_Cds_Pnd_Ct_Cds_Array", "#CT-CDS-ARRAY", new DbsArrayController(1, 
            5));
        pnd_Ct_Cds_Pnd_Ct_Filing_Stts = pnd_Ct_Cds_Pnd_Ct_Cds_Array.newFieldInGroup("pnd_Ct_Cds_Pnd_Ct_Filing_Stts", "#CT-FILING-STTS", FieldType.STRING, 
            1);

        pnd_Ct_Cds__R_Field_2 = pnd_Ct_Cds_Pnd_Ct_Cds_Array.newGroupInGroup("pnd_Ct_Cds__R_Field_2", "REDEFINE", pnd_Ct_Cds_Pnd_Ct_Filing_Stts);
        pnd_Ct_Cds_Pnd_Ct_Filing_Stts_N = pnd_Ct_Cds__R_Field_2.newFieldInGroup("pnd_Ct_Cds_Pnd_Ct_Filing_Stts_N", "#CT-FILING-STTS-N", FieldType.NUMERIC, 
            1);
        pnd_Ct_Cds_Pnd_Ct_With_Cde = pnd_Ct_Cds_Pnd_Ct_Cds_Array.newFieldInGroup("pnd_Ct_Cds_Pnd_Ct_With_Cde", "#CT-WITH-CDE", FieldType.STRING, 1);
        pnd_Ct_Filing_Status = localVariables.newFieldInRecord("pnd_Ct_Filing_Status", "#CT-FILING-STATUS", FieldType.STRING, 1);
        pnd_Ct_With_Txt = localVariables.newFieldInRecord("pnd_Ct_With_Txt", "#CT-WITH-TXT", FieldType.STRING, 29);
        pnd_Ct_Addl_Txt = localVariables.newFieldInRecord("pnd_Ct_Addl_Txt", "#CT-ADDL-TXT", FieldType.STRING, 25);
        pnd_Ct_With_Cde_E = localVariables.newFieldInRecord("pnd_Ct_With_Cde_E", "#CT-WITH-CDE-E", FieldType.STRING, 1);
        pnd_Var_Text_Msg = localVariables.newFieldInRecord("pnd_Var_Text_Msg", "#VAR-TEXT-MSG", FieldType.STRING, 210);

        pnd_Var_Text_Msg__R_Field_3 = localVariables.newGroupInRecord("pnd_Var_Text_Msg__R_Field_3", "REDEFINE", pnd_Var_Text_Msg);
        pnd_Var_Text_Msg_Pnd_Var_Text1 = pnd_Var_Text_Msg__R_Field_3.newFieldInGroup("pnd_Var_Text_Msg_Pnd_Var_Text1", "#VAR-TEXT1", FieldType.STRING, 
            30);
        pnd_Var_Text_Msg_Pnd_Var_Text2 = pnd_Var_Text_Msg__R_Field_3.newFieldInGroup("pnd_Var_Text_Msg_Pnd_Var_Text2", "#VAR-TEXT2", FieldType.STRING, 
            30);
        pnd_Var_Text_Msg_Pnd_Var_Text3 = pnd_Var_Text_Msg__R_Field_3.newFieldInGroup("pnd_Var_Text_Msg_Pnd_Var_Text3", "#VAR-TEXT3", FieldType.STRING, 
            30);
        pnd_Var_Text_Msg_Pnd_Var_Text4 = pnd_Var_Text_Msg__R_Field_3.newFieldInGroup("pnd_Var_Text_Msg_Pnd_Var_Text4", "#VAR-TEXT4", FieldType.STRING, 
            30);
        pnd_Var_Text_Msg_Pnd_Var_Text5 = pnd_Var_Text_Msg__R_Field_3.newFieldInGroup("pnd_Var_Text_Msg_Pnd_Var_Text5", "#VAR-TEXT5", FieldType.STRING, 
            30);
        pnd_Var_Text_Msg_Pnd_Var_Text6 = pnd_Var_Text_Msg__R_Field_3.newFieldInGroup("pnd_Var_Text_Msg_Pnd_Var_Text6", "#VAR-TEXT6", FieldType.STRING, 
            30);
        pnd_Var_Text_Msg_Pnd_Var_Text7 = pnd_Var_Text_Msg__R_Field_3.newFieldInGroup("pnd_Var_Text_Msg_Pnd_Var_Text7", "#VAR-TEXT7", FieldType.STRING, 
            30);
        pnd_Var_Texta = localVariables.newFieldInRecord("pnd_Var_Texta", "#VAR-TEXTA", FieldType.STRING, 180);
        pnd_Comma = localVariables.newFieldInRecord("pnd_Comma", "#COMMA", FieldType.STRING, 25);
        pnd_Nra_1001_Flag = localVariables.newFieldInRecord("pnd_Nra_1001_Flag", "#NRA-1001-FLAG", FieldType.STRING, 1);
        pnd_Nra_Flag = localVariables.newFieldInRecord("pnd_Nra_Flag", "#NRA-FLAG", FieldType.STRING, 1);
        pnd_Us_Res_Flag = localVariables.newFieldInRecord("pnd_Us_Res_Flag", "#US-RES-FLAG", FieldType.STRING, 1);
        ws_Edit_Pct = localVariables.newFieldInRecord("ws_Edit_Pct", "WS-EDIT-PCT", FieldType.NUMERIC, 5, 2);
        ws_Amt_Out = localVariables.newFieldInRecord("ws_Amt_Out", "WS-AMT-OUT", FieldType.STRING, 11);
        ws_Work_Flat_Amt = localVariables.newFieldInRecord("ws_Work_Flat_Amt", "WS-WORK-FLAT-AMT", FieldType.STRING, 10);
        ws_Edit_Flat_Amt = localVariables.newFieldInRecord("ws_Edit_Flat_Amt", "WS-EDIT-FLAT-AMT", FieldType.NUMERIC, 7, 2);
        ws_Msg_Out = localVariables.newFieldInRecord("ws_Msg_Out", "WS-MSG-OUT", FieldType.STRING, 250);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Pp = localVariables.newFieldInRecord("pnd_Pp", "#PP", FieldType.STRING, 1);
        pnd_Ls = localVariables.newFieldInRecord("pnd_Ls", "#LS", FieldType.STRING, 1);
        testing_Mode = localVariables.newFieldInRecord("testing_Mode", "TESTING-MODE", FieldType.BOOLEAN, 1);
        pnd_Prod_Field = localVariables.newFieldInRecord("pnd_Prod_Field", "#PROD-FIELD", FieldType.STRING, 4);
        pnd_Disp_Msg = localVariables.newFieldInRecord("pnd_Disp_Msg", "#DISP-MSG", FieldType.STRING, 60);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        parameters.reset();
        localVariables.reset();
        pnd_Fed_Amt_Lit.setInitialValue("of your Federal withholding amount");
        pnd_Sp_Allw.setInitialValue(" Spousal Allowance");
        pnd_Age_Allw.setInitialValue(" Age Allowance");
        pnd_Sp_Age_Allw.setInitialValue(" Spousal Age Allowance");
        pnd_Bl_Allw.setInitialValue(" Blind Allowance");
        pnd_Sp_Bl_Allw.setInitialValue(" Spouse Blind Allowance");
        pnd_W9_Not_Rcvd.setInitialValue("We have not received your Form W9.");
        pnd_W9_Rcvd.setInitialValue("We received your Form W9.");
        pnd_W8_Not_Rcvd.setInitialValue("We have not received your Form W-8BEN.");
        pnd_W8_Rcvd.setInitialValue("We received your Form W-8BEN.");
        pnd_And_Txt.setInitialValue("and");
        pnd_And_Txt2.setInitialValue(" and");
        pnd_With_Txt.setInitialValue("with");
        pnd_Allw_Txt.setInitialValue("allowance(s) ");
        pnd_Flat_Txt2.setInitialValue("a flat amount of ");
        pnd_Flat_Txt.setInitialValue("Flat amount of ");
        pnd_Addl_Txt.setInitialValue("an additional amount of");
        pnd_Exempt.setInitialValue("You are exempt from withholding.");
        pnd_Ls_Default.setInitialValue("Single with zero allowances.");
        pnd_Pp_Default.setInitialValue("Married with 3 exemptions.");
        pnd_Tax_Amt_Lit.setInitialValue("of the taxable amount");
        pnd_Mdo_Default.setInitialValue("10%.");
        pnd_Rtb_Default.setInitialValue("20.00%");
        pnd_No_With_Allowed.setInitialValue("No Withholding allowed.");
        pnd_Do_Not_Withhold.setInitialValue("Do not withhold.");
        pnd_Deflt1.setInitialValue("You did not elect tax withholding. Therefore, ");
        pnd_Deflt2.setInitialValue("we must withhold using the default status -  ");
        pnd_Deflt3.setInitialValue("we must withhold using the mandatory default - ");
        pnd_Nra_Msg.setInitialValue("We will withhold at a rate of ");
        pnd_Defltac1.setInitialValue("You elected to rollover your payments.");
        pnd_Defltac2.setInitialValue("Therefore, no taxes will be withheld.");
        pnd_Ct_Cds.setInitialValue("4A8B3C5D1F");
        pnd_Ct_With_Txt.setInitialValue("You elected Withholding Code ");
        pnd_Ct_Addl_Txt.setInitialValue("with additional amount of");
        pnd_Ct_With_Cde_E.setInitialValue("E");
        pnd_Comma.setInitialValue(",");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn545() throws Exception
    {
        super("Adsn545");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* *********  M A I N   L O G I C    *****************
        //*   TESTING-MODE := FALSE !!!!!!!!!!! CHANGE BEFORE MOVE TO PROD- MN
        testing_Mode.setValue(true);                                                                                                                                      //Natural: ASSIGN TESTING-MODE := TRUE
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-FIELDS
        sub_Initialize_Fields();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM POPULATE-TAX-LINKAGE
        sub_Populate_Tax_Linkage();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Pp_Pymt_Ind().equals("Y")))                                                                                    //Natural: IF ADS-PP-PYMT-IND = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM SETUP-FOR-TAX-CALL-PP
            sub_Setup_For_Tax_Call_Pp();
            if (condition(Global.isEscape())) {return;}
            pnd_Pp.reset();                                                                                                                                               //Natural: RESET #PP
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Mdo_Pymt_Ind().equals("Y") || pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Ls_Pymt_Ind().equals("Y")                   //Natural: IF ADS-MDO-PYMT-IND = 'Y' OR ADS-LS-PYMT-IND = 'Y' OR ADS-RTB-T-PYMT-IND = 'Y' OR ADS-RTB-C-PYMT-IND = 'Y'
            || pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Rtb_T_Pymt_Ind().equals("Y") || pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Rtb_C_Pymt_Ind().equals("Y")))
        {
                                                                                                                                                                          //Natural: PERFORM SETUP-FOR-TAX-CALL-LS
            sub_Setup_For_Tax_Call_Ls();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *********  S U B R O U T I N E S  *****************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-FIELDS
        //* **********************************
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-TAX-LINKAGE
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-FOR-TAX-CALL-LS
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-FOR-TAX-CALL-PP
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-ELECTION
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NRA-DETERMINATION
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-FEDERAL-ELECTION-MSG
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-FEDERAL-ELECTION-DATA
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-FEDERAL-DEFAULT-TEXT
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-FEDERAL-ELECTION-TEXT
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FEDERAL-ELECTION-METHOD-NA
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FEDERAL-ELECTION-METHOD-1
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FEDERAL-ELECTION-METHOD-2
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FEDERAL-ELECTION-METHOD-3
        //* ************************************           /* 092818 - START
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONNECTICUT-STATE-ELECTION-MSG
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-STATE-ELECTION-MSG
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-STATE-ELECTION-DATA
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-ALL-ALLOWANCES-RTN
        //* *****************************          /* WAGE TABLE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STATE-ELECTION-METHOD-1
        //* *****************************          /* FLAT AMOUNT
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STATE-ELECTION-METHOD-2
        //* *****************************          /* FIXED PCT
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STATE-ELECTION-METHOD-3
        //* *****************************          /* FIXED % OF FEDERAL
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STATE-ELECTION-METHOD-4
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STATE-MANDATORY-DEFAULT-TEXT
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-STATE-WAGE-TABLE-TEXT
    }
    private void sub_Initialize_Fields() throws Exception                                                                                                                 //Natural: INITIALIZE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Stat_Dscr.getValue(1).setValue("Single ");                                                                                                                    //Natural: ASSIGN #STAT-DSCR ( 1 ) := 'Single '
        pnd_Stat_Dscr.getValue(2).setValue("Married ");                                                                                                                   //Natural: ASSIGN #STAT-DSCR ( 2 ) := 'Married '
        pnd_Stat_Dscr.getValue(3).setValue("Married filing jointly ");                                                                                                    //Natural: ASSIGN #STAT-DSCR ( 3 ) := 'Married filing jointly '
        pnd_Stat_Dscr.getValue(4).setValue("Married filing separately ");                                                                                                 //Natural: ASSIGN #STAT-DSCR ( 4 ) := 'Married filing separately '
        pnd_Stat_Dscr.getValue(5).setValue("Married but withhold at a higher single rate ");                                                                              //Natural: ASSIGN #STAT-DSCR ( 5 ) := 'Married but withhold at a higher single rate '
        pnd_Stat_Dscr.getValue(6).setValue("Married with one income  ");                                                                                                  //Natural: ASSIGN #STAT-DSCR ( 6 ) := 'Married with one income  '
        pnd_Stat_Dscr.getValue(7).setValue("Married with two incomes ");                                                                                                  //Natural: ASSIGN #STAT-DSCR ( 7 ) := 'Married with two incomes '
        pnd_Stat_Dscr.getValue(8).setValue("Head of Household ");                                                                                                         //Natural: ASSIGN #STAT-DSCR ( 8 ) := 'Head of Household '
        pnd_Pp.reset();                                                                                                                                                   //Natural: RESET #PP
        pnd_Ls.reset();                                                                                                                                                   //Natural: RESET #LS
        pnd_Nra_Frm_W8_Info.reset();                                                                                                                                      //Natural: RESET #NRA-FRM-W8-INFO
        pnd_Nra_Frm_W9_Info.reset();                                                                                                                                      //Natural: RESET #NRA-FRM-W9-INFO
        pnd_Nra_Flag.reset();                                                                                                                                             //Natural: RESET #NRA-FLAG
        pnd_Us_Res_Flag.reset();                                                                                                                                          //Natural: RESET #US-RES-FLAG
        //*  RESET ADS-TIAA-MSG-AREA
        //*  RESET ADS-CREF-MSG-AREA
    }
    private void sub_Populate_Tax_Linkage() throws Exception                                                                                                              //Natural: POPULATE-TAX-LINKAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        pdaTxwa0010.getPnd_Txwa0010_Function().setValue("EL");                                                                                                            //Natural: MOVE 'EL' TO #TXWA0010.FUNCTION
        pdaTxwa0010.getPnd_Txwa0010_Call_Sys().setValue("SE");                                                                                                            //Natural: MOVE 'SE' TO #TXWA0010.CALL-SYS
        pdaTxwa0010.getPnd_Txwa0010_Orig_Cde().setValue("NZ");                                                                                                            //Natural: MOVE 'NZ' TO #TXWA0010.ORIG-CDE
        pdaTxwa0010.getPnd_Txwa0010_Tin_Type().setValue("1");                                                                                                             //Natural: MOVE '1' TO #TXWA0010.TIN-TYPE
        pdaTxwa0010.getPnd_Txwa0010_Tin().setValue(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Tx_Ss_Num());                                                                     //Natural: MOVE ADS-TX-SS-NUM TO #TXWA0010.TIN
        pdaTxwa0010.getPnd_Txwa0010_Contract_Nbr().setValue(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Tx_Tiaa_No());                                                           //Natural: MOVE ADS-TX-TIAA-NO TO #TXWA0010.CONTRACT-NBR
        if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_No().equals(getZero())))                                                                         //Natural: IF ADS-TX-TIAA-PAYEE-NO = 0
        {
            pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_No().setValue(1);                                                                                          //Natural: MOVE 01 TO ADS-TX-TIAA-PAYEE-NO
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_No().equals(getZero())))                                                                         //Natural: IF ADS-TX-CREF-PAYEE-NO = 0
        {
            pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_No().setValue(1);                                                                                          //Natural: MOVE 01 TO ADS-TX-CREF-PAYEE-NO
        }                                                                                                                                                                 //Natural: END-IF
        pdaTxwa0010.getPnd_Txwa0010_Payee_Cde().setValueEdited(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_No(),new ReportEditMask("99"));                         //Natural: MOVE EDITED ADS-TX-TIAA-PAYEE-NO ( EM = 99 ) TO #TXWA0010.PAYEE-CDE
        pdaTxwa0010.getPnd_Txwa0010_Pymnt_Dte().setValue(Global.getDATN());                                                                                               //Natural: MOVE *DATN TO #TXWA0010.PYMNT-DTE
        pdaTxwa0010.getPnd_Txwa0010_Citizen_Cde_I().setValue(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Ctzn_Cde());                                                            //Natural: MOVE ADS-CTZN-CDE TO #TXWA0010.CITIZEN-CDE-I
        pdaTxwa0010.getPnd_Txwa0010_Res_Cde_I().setValue(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Rsdncy_Cde());                                                              //Natural: MOVE ADS-RSDNCY-CDE TO #TXWA0010.RES-CDE-I
        pdaTxwa0010.getPnd_Txwa0010_Settl_Option().setValue(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Settl_Option());                                                         //Natural: MOVE ADS-SETTL-OPTION TO #TXWA0010.SETTL-OPTION
        pdaTxwa0010.getPnd_Txwa0010_Term_Years().setValue(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Term_Years());                                                             //Natural: MOVE ADS-TERM-YEARS TO #TXWA0010.TERM-YEARS
        pdaTxwa0010.getPnd_Txwa0010_Spouse_Ind().setValue(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Spouse_Ind());                                                             //Natural: MOVE ADS-SPOUSE-IND TO #TXWA0010.SPOUSE-IND
        pdaTxwa0010.getPnd_Txwa0010_Money_Source().setValue(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Money_Source());                                                         //Natural: MOVE ADS-MONEY-SOURCE TO #TXWA0010.MONEY-SOURCE
        pdaTxwa0010.getPnd_Txwa0010_Lob().setValue(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Lob());                                                                           //Natural: MOVE ADS-LOB TO #TXWA0010.LOB
        pdaTxwa0010.getPnd_Txwa0010_Pymnt_Typ_Ind().setValue(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Pymnt_Typ_Ind());                                                       //Natural: MOVE ADS-PYMNT-TYP-IND TO #TXWA0010.PYMNT-TYP-IND
        //*  030512 START
        //*  USED IN TXWN0050
        if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Pymnt_Typ_Ind().equals("M")))                                                                                  //Natural: IF ADS-PYMNT-TYP-IND = 'M'
        {
            pdaTxwa0010.getPnd_Txwa0010_Pymnt_Typ_Ind().setValue("C");                                                                                                    //Natural: ASSIGN #TXWA0010.PYMNT-TYP-IND := 'C'
            pdaTxwa0010.getPnd_Txwa0010_Settl_Option().setValue("W2");                                                                                                    //Natural: ASSIGN #TXWA0010.SETTL-OPTION := 'W2'
            pdaTxwa0010.getPnd_Txwa0010_Money_Source().reset();                                                                                                           //Natural: RESET #TXWA0010.MONEY-SOURCE
            //*  030512 END
        }                                                                                                                                                                 //Natural: END-IF
        pdaTxwa0010.getPnd_Txwa0010_Hardship_Ind().setValue(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Hardship_Ind());                                                         //Natural: MOVE ADS-HARDSHIP-IND TO #TXWA0010.HARDSHIP-IND
        pdaTxwa0010.getPnd_Txwa0010_Dob().setValue(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Dob());                                                                           //Natural: MOVE ADS-DOB TO #TXWA0010.DOB
        pdaTxwa0010.getPnd_Txwa0010_Dod().setValue(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Dod());                                                                           //Natural: MOVE ADS-DOD TO #TXWA0010.DOD
        pdaTxwa0010.getPnd_Txwa0010_Roth_First_Contrib_Dte().setValue(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Roth_Frst_Cntrbtn_Dte());                                      //Natural: MOVE ADS-ROTH-FRST-CNTRBTN-DTE TO #TXWA0010.ROTH-FIRST-CONTRIB-DTE
        if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Settl_Option().equals("30")))                                                                                  //Natural: IF ADS-SETTL-OPTION = '30'
        {
            pdaTxwa0010.getPnd_Txwa0010_Orig_Cde().setValue("SS");                                                                                                        //Natural: MOVE 'SS' TO #TXWA0010.ORIG-CDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Setup_For_Tax_Call_Ls() throws Exception                                                                                                             //Natural: SETUP-FOR-TAX-CALL-LS
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pnd_Ls.reset();                                                                                                                                                   //Natural: RESET #LS
        pnd_Pp.reset();                                                                                                                                                   //Natural: RESET #PP
        if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Ls_Pymt_Ind().equals("Y")))                                                                                    //Natural: IF ADS-LS-PYMT-IND = 'Y'
        {
            pnd_Ls.setValue("Y");                                                                                                                                         //Natural: MOVE 'Y' TO #LS
        }                                                                                                                                                                 //Natural: END-IF
        //*  TIAA LS CALL
        if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Tx_Tiaa_No().equals(" ")))                                                                                     //Natural: IF ADS-TX-TIAA-NO = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  LUMP SUM HAS NO RTB
            if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Rtb_T_Pymt_Ind().equals("N")))                                                                             //Natural: IF ADS-RTB-T-PYMT-IND = 'N'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaTxwa0010.getPnd_Txwa0010_Contract_Nbr().setValue(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Tx_Tiaa_No());                                                   //Natural: MOVE ADS-TX-TIAA-NO TO #TXWA0010.CONTRACT-NBR
                pdaTxwa0010.getPnd_Txwa0010_Payee_Cde().setValueEdited(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_No(),new ReportEditMask("99"));                 //Natural: MOVE EDITED ADS-TX-TIAA-PAYEE-NO ( EM = 99 ) TO #TXWA0010.PAYEE-CDE
                //*    IF ADS-RTB-T-PYMT-IND = 'Y'
                //*      MOVE 'RTB'                    TO #TXWA0010.SETTL-OPTION
                //*      MOVE ADS-ROLLVR-IND           TO #TXWA0010.ROLL-ELIG-IND
                //*    END-IF
                pnd_Prod_Field.setValue("TIAA");                                                                                                                          //Natural: MOVE 'TIAA' TO #PROD-FIELD
                                                                                                                                                                          //Natural: PERFORM READ-ELECTION
                sub_Read_Election();
                if (condition(Global.isEscape())) {return;}
                //*  NRA TAX
                if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Tax_Type().equals("N")))                                                                                    //Natural: IF #TXWA0010.FED-TAX-TYPE = 'N'
                {
                    ws_Msg_Out.reset();                                                                                                                                   //Natural: RESET WS-MSG-OUT
                                                                                                                                                                          //Natural: PERFORM NRA-DETERMINATION
                    sub_Nra_Determination();
                    if (condition(Global.isEscape())) {return;}
                    pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_T_F_Lump_Msg_Lns().setValue(ws_Msg_Out);                                                                         //Natural: MOVE WS-MSG-OUT TO ADS-T-F-LUMP-MSG-LNS
                    ws_Msg_Out.reset();                                                                                                                                   //Natural: RESET WS-MSG-OUT
                    //*  092818 - START
                    if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Rsdncy_Cde().equals("08")))                                                                        //Natural: IF ADS-RSDNCY-CDE = '08'
                    {
                                                                                                                                                                          //Natural: PERFORM CONNECTICUT-STATE-ELECTION-MSG
                        sub_Connecticut_State_Election_Msg();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM CREATE-STATE-ELECTION-MSG
                        sub_Create_State_Election_Msg();
                        if (condition(Global.isEscape())) {return;}
                        //*  092818 - END
                    }                                                                                                                                                     //Natural: END-IF
                    pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_T_S_Lump_Msg_Lns().setValue(ws_Msg_Out);                                                                         //Natural: MOVE WS-MSG-OUT TO ADS-T-S-LUMP-MSG-LNS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ws_Msg_Out.reset();                                                                                                                                   //Natural: RESET WS-MSG-OUT
                                                                                                                                                                          //Natural: PERFORM CREATE-FEDERAL-ELECTION-MSG
                    sub_Create_Federal_Election_Msg();
                    if (condition(Global.isEscape())) {return;}
                    pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_T_F_Lump_Msg_Lns().setValue(ws_Msg_Out);                                                                         //Natural: MOVE WS-MSG-OUT TO ADS-T-F-LUMP-MSG-LNS
                    ws_Msg_Out.reset();                                                                                                                                   //Natural: RESET WS-MSG-OUT
                    //*  092818 - START
                    if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Rsdncy_Cde().equals("08")))                                                                        //Natural: IF ADS-RSDNCY-CDE = '08'
                    {
                                                                                                                                                                          //Natural: PERFORM CONNECTICUT-STATE-ELECTION-MSG
                        sub_Connecticut_State_Election_Msg();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM CREATE-STATE-ELECTION-MSG
                        sub_Create_State_Election_Msg();
                        if (condition(Global.isEscape())) {return;}
                        //*  092818 - END
                    }                                                                                                                                                     //Natural: END-IF
                    pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_T_S_Lump_Msg_Lns().setValue(ws_Msg_Out);                                                                         //Natural: MOVE WS-MSG-OUT TO ADS-T-S-LUMP-MSG-LNS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CREF LS CALL
        if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Tx_Cref_No().equals(" ")))                                                                                     //Natural: IF ADS-TX-CREF-NO = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  IF ADS-RTB-C-PYMT-IND = 'N'        /* LUMP SUM HAS NO RTB
            //*    IGNORE
            //*  ELSE
            pdaTxwa0010.getPnd_Txwa0010_Contract_Nbr().setValue(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Tx_Cref_No());                                                       //Natural: MOVE ADS-TX-CREF-NO TO #TXWA0010.CONTRACT-NBR
            pdaTxwa0010.getPnd_Txwa0010_Payee_Cde().setValueEdited(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_No(),new ReportEditMask("99"));                     //Natural: MOVE EDITED ADS-TX-CREF-PAYEE-NO ( EM = 99 ) TO #TXWA0010.PAYEE-CDE
            //* *  IF ADS-RTB-C-PYMT-IND = 'Y'
            //* *    MOVE 'RTB'                    TO #TXWA0010.SETTL-OPTION
            //* *    MOVE ADS-ROLLVR-IND           TO #TXWA0010.ROLL-ELIG-IND
            //* *  END-IF
            pnd_Prod_Field.setValue("CREF");                                                                                                                              //Natural: MOVE 'CREF' TO #PROD-FIELD
                                                                                                                                                                          //Natural: PERFORM READ-ELECTION
            sub_Read_Election();
            if (condition(Global.isEscape())) {return;}
            //*  NRA TAX
            if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Tax_Type().equals("N")))                                                                                        //Natural: IF #TXWA0010.FED-TAX-TYPE = 'N'
            {
                ws_Msg_Out.reset();                                                                                                                                       //Natural: RESET WS-MSG-OUT
                                                                                                                                                                          //Natural: PERFORM NRA-DETERMINATION
                sub_Nra_Determination();
                if (condition(Global.isEscape())) {return;}
                pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_C_F_Lump_Msg_Lns().setValue(ws_Msg_Out);                                                                             //Natural: MOVE WS-MSG-OUT TO ADS-C-F-LUMP-MSG-LNS
                ws_Msg_Out.reset();                                                                                                                                       //Natural: RESET WS-MSG-OUT
                //*  092818 - START
                if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Rsdncy_Cde().equals("08")))                                                                            //Natural: IF ADS-RSDNCY-CDE = '08'
                {
                                                                                                                                                                          //Natural: PERFORM CONNECTICUT-STATE-ELECTION-MSG
                    sub_Connecticut_State_Election_Msg();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM CREATE-STATE-ELECTION-MSG
                    sub_Create_State_Election_Msg();
                    if (condition(Global.isEscape())) {return;}
                    //*  092818 - END
                }                                                                                                                                                         //Natural: END-IF
                pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_C_S_Lump_Msg_Lns().setValue(ws_Msg_Out);                                                                             //Natural: MOVE WS-MSG-OUT TO ADS-C-S-LUMP-MSG-LNS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ws_Msg_Out.reset();                                                                                                                                       //Natural: RESET WS-MSG-OUT
                                                                                                                                                                          //Natural: PERFORM CREATE-FEDERAL-ELECTION-MSG
                sub_Create_Federal_Election_Msg();
                if (condition(Global.isEscape())) {return;}
                pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_C_F_Lump_Msg_Lns().setValue(ws_Msg_Out);                                                                             //Natural: MOVE WS-MSG-OUT TO ADS-C-F-LUMP-MSG-LNS
                ws_Msg_Out.reset();                                                                                                                                       //Natural: RESET WS-MSG-OUT
                //*  092818 - START
                if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Rsdncy_Cde().equals("08")))                                                                            //Natural: IF ADS-RSDNCY-CDE = '08'
                {
                                                                                                                                                                          //Natural: PERFORM CONNECTICUT-STATE-ELECTION-MSG
                    sub_Connecticut_State_Election_Msg();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM CREATE-STATE-ELECTION-MSG
                    sub_Create_State_Election_Msg();
                    if (condition(Global.isEscape())) {return;}
                    //*  092818 - END
                }                                                                                                                                                         //Natural: END-IF
                pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_C_S_Lump_Msg_Lns().setValue(ws_Msg_Out);                                                                             //Natural: MOVE WS-MSG-OUT TO ADS-C-S-LUMP-MSG-LNS
            }                                                                                                                                                             //Natural: END-IF
            //*  END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Setup_For_Tax_Call_Pp() throws Exception                                                                                                             //Natural: SETUP-FOR-TAX-CALL-PP
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        pnd_Pp.reset();                                                                                                                                                   //Natural: RESET #PP
        pnd_Ls.reset();                                                                                                                                                   //Natural: RESET #LS
        if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Pp_Pymt_Ind().equals("Y")))                                                                                    //Natural: IF ADS-PP-PYMT-IND = 'Y'
        {
            pnd_Pp.setValue("Y");                                                                                                                                         //Natural: MOVE 'Y' TO #PP
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Tx_Tiaa_No().equals(" ")))                                                                                     //Natural: IF ADS-TX-TIAA-NO = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTxwa0010.getPnd_Txwa0010_Contract_Nbr().setValue(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Tx_Tiaa_No());                                                       //Natural: MOVE ADS-TX-TIAA-NO TO #TXWA0010.CONTRACT-NBR
            pdaTxwa0010.getPnd_Txwa0010_Payee_Cde().setValueEdited(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Tx_Tiaa_Payee_No(),new ReportEditMask("99"));                     //Natural: MOVE EDITED ADS-TX-TIAA-PAYEE-NO ( EM = 99 ) TO #TXWA0010.PAYEE-CDE
            pdaTxwa0010.getPnd_Txwa0010_Roll_Elig_Ind().setValue(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Pp_Rollvr_Ind());                                                   //Natural: MOVE ADS-PP-ROLLVR-IND TO #TXWA0010.ROLL-ELIG-IND
            pnd_Prod_Field.setValue("TIAA");                                                                                                                              //Natural: MOVE 'TIAA' TO #PROD-FIELD
            if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Pp_Rollvr_Ind().equals("Y") && pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Settl_Option().equals("AC")))          //Natural: IF ADS-TX-MSG-LINKAGE.ADS-PP-ROLLVR-IND = 'Y' AND ADS-TX-MSG-LINKAGE.ADS-SETTL-OPTION = 'AC'
            {
                pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_T_F_Pp_Msg_Lns().setValue(DbsUtil.compress(pnd_Defltac1, pnd_Defltac2));                                             //Natural: COMPRESS #DEFLTAC1 #DEFLTAC2 INTO ADS-T-F-PP-MSG-LNS
                getReports().write(0, "adsn545 1 ","=",pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_T_F_Pp_Msg_Lns(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")); //Natural: WRITE 'adsn545 1 ' '=' ADS-T-F-PP-MSG-LNS ( EM = X ( 70 ) )
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM READ-ELECTION
                sub_Read_Election();
                if (condition(Global.isEscape())) {return;}
                //*  NRA TAX
                if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Tax_Type().equals("N")))                                                                                    //Natural: IF #TXWA0010.FED-TAX-TYPE = 'N'
                {
                    ws_Msg_Out.reset();                                                                                                                                   //Natural: RESET WS-MSG-OUT
                                                                                                                                                                          //Natural: PERFORM NRA-DETERMINATION
                    sub_Nra_Determination();
                    if (condition(Global.isEscape())) {return;}
                    pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_T_F_Pp_Msg_Lns().setValue(ws_Msg_Out);                                                                           //Natural: MOVE WS-MSG-OUT TO ADS-T-F-PP-MSG-LNS
                    getReports().write(0, "adsn545 2 ","=",pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_T_F_Pp_Msg_Lns(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")); //Natural: WRITE 'adsn545 2 ' '=' ADS-T-F-PP-MSG-LNS ( EM = X ( 70 ) )
                    if (Global.isEscape()) return;
                    ws_Msg_Out.reset();                                                                                                                                   //Natural: RESET WS-MSG-OUT
                    //*  092818 - START
                    if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Rsdncy_Cde().equals("08")))                                                                        //Natural: IF ADS-RSDNCY-CDE = '08'
                    {
                                                                                                                                                                          //Natural: PERFORM CONNECTICUT-STATE-ELECTION-MSG
                        sub_Connecticut_State_Election_Msg();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM CREATE-STATE-ELECTION-MSG
                        sub_Create_State_Election_Msg();
                        if (condition(Global.isEscape())) {return;}
                        //*  092818 - END
                    }                                                                                                                                                     //Natural: END-IF
                    pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_T_S_Pp_Msg_Lns().setValue(ws_Msg_Out);                                                                           //Natural: MOVE WS-MSG-OUT TO ADS-T-S-PP-MSG-LNS
                    getReports().write(0, "adsn545 1 ","=",pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_T_S_Pp_Msg_Lns(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")); //Natural: WRITE 'adsn545 1 ' '=' ADS-T-S-PP-MSG-LNS ( EM = X ( 70 ) )
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ws_Msg_Out.reset();                                                                                                                                   //Natural: RESET WS-MSG-OUT
                                                                                                                                                                          //Natural: PERFORM CREATE-FEDERAL-ELECTION-MSG
                    sub_Create_Federal_Election_Msg();
                    if (condition(Global.isEscape())) {return;}
                    pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_T_F_Pp_Msg_Lns().setValue(ws_Msg_Out);                                                                           //Natural: MOVE WS-MSG-OUT TO ADS-T-F-PP-MSG-LNS
                    getReports().write(0, "adsn545 3 ","=",pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_T_F_Pp_Msg_Lns(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")); //Natural: WRITE 'adsn545 3 ' '=' ADS-T-F-PP-MSG-LNS ( EM = X ( 70 ) )
                    if (Global.isEscape()) return;
                    ws_Msg_Out.reset();                                                                                                                                   //Natural: RESET WS-MSG-OUT
                    //*  092818 - START
                    if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Rsdncy_Cde().equals("08")))                                                                        //Natural: IF ADS-RSDNCY-CDE = '08'
                    {
                                                                                                                                                                          //Natural: PERFORM CONNECTICUT-STATE-ELECTION-MSG
                        sub_Connecticut_State_Election_Msg();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM CREATE-STATE-ELECTION-MSG
                        sub_Create_State_Election_Msg();
                        if (condition(Global.isEscape())) {return;}
                        //*  092818 - END
                    }                                                                                                                                                     //Natural: END-IF
                    pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_T_S_Pp_Msg_Lns().setValue(ws_Msg_Out);                                                                           //Natural: MOVE WS-MSG-OUT TO ADS-T-S-PP-MSG-LNS
                    getReports().write(0, "adsn545 2 ","=",pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_T_S_Pp_Msg_Lns(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")); //Natural: WRITE 'adsn545 2 ' '=' ADS-T-S-PP-MSG-LNS ( EM = X ( 70 ) )
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Tx_Cref_No().equals(" ")))                                                                                     //Natural: IF ADS-TX-CREF-NO = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaTxwa0010.getPnd_Txwa0010_Contract_Nbr().setValue(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Tx_Cref_No());                                                       //Natural: MOVE ADS-TX-CREF-NO TO #TXWA0010.CONTRACT-NBR
            pdaTxwa0010.getPnd_Txwa0010_Payee_Cde().setValueEdited(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Tx_Cref_Payee_No(),new ReportEditMask("99"));                     //Natural: MOVE EDITED ADS-TX-CREF-PAYEE-NO ( EM = 99 ) TO #TXWA0010.PAYEE-CDE
            pdaTxwa0010.getPnd_Txwa0010_Roll_Elig_Ind().setValue(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Pp_Rollvr_Ind());                                                   //Natural: MOVE ADS-PP-ROLLVR-IND TO #TXWA0010.ROLL-ELIG-IND
            pnd_Prod_Field.setValue("CREF");                                                                                                                              //Natural: MOVE 'CREF' TO #PROD-FIELD
            if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Pp_Rollvr_Ind().equals("Y") && pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Settl_Option().equals("AC")))          //Natural: IF ADS-PP-ROLLVR-IND = 'Y' AND ADS-TX-MSG-LINKAGE.ADS-SETTL-OPTION = 'AC'
            {
                pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_C_F_Pp_Msg_Lns().setValue(DbsUtil.compress(pnd_Defltac1, pnd_Defltac2));                                             //Natural: COMPRESS #DEFLTAC1 #DEFLTAC2 INTO ADS-C-F-PP-MSG-LNS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM READ-ELECTION
                sub_Read_Election();
                if (condition(Global.isEscape())) {return;}
                //*  NRA TAX
                if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Tax_Type().equals("N")))                                                                                    //Natural: IF #TXWA0010.FED-TAX-TYPE = 'N'
                {
                    ws_Msg_Out.reset();                                                                                                                                   //Natural: RESET WS-MSG-OUT
                                                                                                                                                                          //Natural: PERFORM NRA-DETERMINATION
                    sub_Nra_Determination();
                    if (condition(Global.isEscape())) {return;}
                    pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_C_F_Pp_Msg_Lns().setValue(ws_Msg_Out);                                                                           //Natural: MOVE WS-MSG-OUT TO ADS-C-F-PP-MSG-LNS
                    ws_Msg_Out.reset();                                                                                                                                   //Natural: RESET WS-MSG-OUT
                    //*  092818 - START
                    if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Rsdncy_Cde().equals("08")))                                                                        //Natural: IF ADS-RSDNCY-CDE = '08'
                    {
                                                                                                                                                                          //Natural: PERFORM CONNECTICUT-STATE-ELECTION-MSG
                        sub_Connecticut_State_Election_Msg();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM CREATE-STATE-ELECTION-MSG
                        sub_Create_State_Election_Msg();
                        if (condition(Global.isEscape())) {return;}
                        //*  092818 - END
                    }                                                                                                                                                     //Natural: END-IF
                    pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_C_S_Pp_Msg_Lns().setValue(ws_Msg_Out);                                                                           //Natural: MOVE WS-MSG-OUT TO ADS-C-S-PP-MSG-LNS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ws_Msg_Out.reset();                                                                                                                                   //Natural: RESET WS-MSG-OUT
                                                                                                                                                                          //Natural: PERFORM CREATE-FEDERAL-ELECTION-MSG
                    sub_Create_Federal_Election_Msg();
                    if (condition(Global.isEscape())) {return;}
                    pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_C_F_Pp_Msg_Lns().setValue(ws_Msg_Out);                                                                           //Natural: MOVE WS-MSG-OUT TO ADS-C-F-PP-MSG-LNS
                    ws_Msg_Out.reset();                                                                                                                                   //Natural: RESET WS-MSG-OUT
                    //*  092818 - START
                    if (condition(pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Rsdncy_Cde().equals("08")))                                                                        //Natural: IF ADS-RSDNCY-CDE = '08'
                    {
                                                                                                                                                                          //Natural: PERFORM CONNECTICUT-STATE-ELECTION-MSG
                        sub_Connecticut_State_Election_Msg();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM CREATE-STATE-ELECTION-MSG
                        sub_Create_State_Election_Msg();
                        if (condition(Global.isEscape())) {return;}
                        //*  092818 - END
                    }                                                                                                                                                     //Natural: END-IF
                    pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_C_S_Pp_Msg_Lns().setValue(ws_Msg_Out);                                                                           //Natural: MOVE WS-MSG-OUT TO ADS-C-S-PP-MSG-LNS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Read_Election() throws Exception                                                                                                                     //Natural: READ-ELECTION
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        if (condition(testing_Mode.getBoolean()))                                                                                                                         //Natural: IF TESTING-MODE
        {
            //*  EM 031708 START
            //*  EM 031708 END
            getReports().write(0, pdaTxwa0010.getPnd_Txwa0010_Function(),pdaTxwa0010.getPnd_Txwa0010_Call_Sys(),pdaTxwa0010.getPnd_Txwa0010_Orig_Cde(),                   //Natural: WRITE #TXWA0010.FUNCTION #TXWA0010.CALL-SYS #TXWA0010.ORIG-CDE #TXWA0010.TIN-TYPE #TXWA0010.TIN #TXWA0010.PAYEE-CDE #TXWA0010.PYMNT-DTE #TXWA0010.CITIZEN-CDE-I #TXWA0010.RES-CDE-I #TXWA0010.TERM-YEARS #TXWA0010.SPOUSE-IND #TXWA0010.MONEY-SOURCE #TXWA0010.LOB #TXWA0010.PYMNT-TYP-IND #TXWA0010.FED-TAX-TYPE #TXWA0010.HARDSHIP-IND #TXWA0010.DOB #TXWA0010.DOD #TXWA0010.ROTH-FIRST-CONTRIB-DTE
                pdaTxwa0010.getPnd_Txwa0010_Tin_Type(),pdaTxwa0010.getPnd_Txwa0010_Tin(),pdaTxwa0010.getPnd_Txwa0010_Payee_Cde(),pdaTxwa0010.getPnd_Txwa0010_Pymnt_Dte(),
                pdaTxwa0010.getPnd_Txwa0010_Citizen_Cde_I(),pdaTxwa0010.getPnd_Txwa0010_Res_Cde_I(),pdaTxwa0010.getPnd_Txwa0010_Term_Years(),pdaTxwa0010.getPnd_Txwa0010_Spouse_Ind(),
                pdaTxwa0010.getPnd_Txwa0010_Money_Source(),pdaTxwa0010.getPnd_Txwa0010_Lob(),pdaTxwa0010.getPnd_Txwa0010_Pymnt_Typ_Ind(),pdaTxwa0010.getPnd_Txwa0010_Fed_Tax_Type(),
                pdaTxwa0010.getPnd_Txwa0010_Hardship_Ind(),pdaTxwa0010.getPnd_Txwa0010_Dob(),pdaTxwa0010.getPnd_Txwa0010_Dod(),pdaTxwa0010.getPnd_Txwa0010_Roth_First_Contrib_Dte());
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Txwn0010.class , getCurrentProcessState(), pdaTxwa0010.getPnd_Txwa0010());                                                                        //Natural: CALLNAT 'TXWN0010' USING #TXWA0010
        if (condition(Global.isEscape())) return;
        pnd_Tax_Errors_Info_Pnd_Tax_Error_Code.setValue(pdaTxwa0010.getPnd_Txwa0010_Pnd_Error_Code());                                                                    //Natural: ASSIGN #TAX-ERROR-CODE := #ERROR-CODE
        pnd_Tax_Errors_Info_Pnd_Tax_Error_Desc.setValue(pdaTxwa0010.getPnd_Txwa0010_Pnd_Error_Desc());                                                                    //Natural: ASSIGN #TAX-ERROR-DESC := #ERROR-DESC
    }
    private void sub_Nra_Determination() throws Exception                                                                                                                 //Natural: NRA-DETERMINATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        if (condition(testing_Mode.getBoolean()))                                                                                                                         //Natural: IF TESTING-MODE
        {
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_W8_Ben_Ind());                                                                                          //Natural: WRITE '=' #TXWA0010.W8-BEN-IND
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_W9_Ind());                                                                                              //Natural: WRITE '=' #TXWA0010.W9-IND
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_Fed_Tax_Type());                                                                                        //Natural: WRITE '=' #TXWA0010.FED-TAX-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_Citizen_Cde_I());                                                                                       //Natural: WRITE '=' #TXWA0010.CITIZEN-CDE-I
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_Res_Cde_I());                                                                                           //Natural: WRITE '=' #TXWA0010.RES-CDE-I
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_Nra_Tax_Rate());                                                                                        //Natural: WRITE '=' #TXWA0010.NRA-TAX-RATE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Nra_Frm_W8_Info.setValue(pdaTxwa0010.getPnd_Txwa0010_W8_Ben_Ind());                                                                                           //Natural: MOVE #TXWA0010.W8-BEN-IND TO #NRA-FRM-W8-INFO
        pnd_Nra_Frm_W9_Info.setValue(pdaTxwa0010.getPnd_Txwa0010_W9_Ind());                                                                                               //Natural: MOVE #TXWA0010.W9-IND TO #NRA-FRM-W9-INFO
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Citizen_Cde_I().equals("01") || pdaTxwa0010.getPnd_Txwa0010_Citizen_Cde_I().equals("11") || pdaTxwa0010.getPnd_Txwa0010_Citizen_Cde_I().equals("US"))) //Natural: IF #TXWA0010.CITIZEN-CDE-I = '01' OR = '11' OR = 'US'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Nra_Flag.reset();                                                                                                                                             //Natural: RESET #NRA-FLAG
        pnd_Nra_1001_Flag.reset();                                                                                                                                        //Natural: RESET #NRA-1001-FLAG
        pnd_Us_Res_Flag.reset();                                                                                                                                          //Natural: RESET #US-RES-FLAG
        //*  NRA TAX
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Tax_Type().equals("N")))                                                                                            //Natural: IF #TXWA0010.FED-TAX-TYPE = 'N'
        {
            pnd_Nra_Flag.setValue("Y");                                                                                                                                   //Natural: MOVE 'Y' TO #NRA-FLAG
            pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Nra_Flag().setValue("Y");                                                                                                //Natural: MOVE 'Y' TO ADS-NRA-FLAG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  FEDERAL
            pnd_Nra_Flag.setValue("N");                                                                                                                                   //Natural: MOVE 'N' TO #NRA-FLAG
            pdaAdsa540.getAds_Tx_Msg_Linkage_Ads_Nra_Flag().setValue("N");                                                                                                //Natural: MOVE 'N' TO ADS-NRA-FLAG
        }                                                                                                                                                                 //Natural: END-IF
        //*  US RESIDENT
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Res_Cde_I().greaterOrEqual("01") && pdaTxwa0010.getPnd_Txwa0010_Res_Cde_I().lessOrEqual("57")))                         //Natural: IF #TXWA0010.RES-CDE-I = '01' THRU '57'
        {
            pnd_Us_Res_Flag.setValue("Y");                                                                                                                                //Natural: MOVE 'Y' TO #US-RES-FLAG
        }                                                                                                                                                                 //Natural: END-IF
        //* *  ???????????/  COMMENTED OUT BY ME:  NO MESSAGE FIELDS IN ADSA555
        //*  IF #US-RES-FLAG = 'Y'
        //*   IF #NRA-FRM-W9-INFO = ' '
        //*     MOVE #W9-NOT-RCVD TO NAZ-NRA-MSG-LNS-1
        //*   ELSE
        //*     MOVE #W9-RCVD     TO NAZ-NRA-MSG-LNS-1
        //*   END-IF
        //*  ELSE
        //*   IF #NRA-FRM-W8-INFO = ' '
        //*     MOVE #W8-NOT-RCVD TO NAZ-NRA-MSG-LNS-1
        //*   ELSE
        //*     MOVE #W8-RCVD     TO NAZ-NRA-MSG-LNS-1
        //*   END-IF
        //*  END-IF
        ws_Edit_Pct.setValue(pdaTxwa0010.getPnd_Txwa0010_Nra_Tax_Rate());                                                                                                 //Natural: MOVE #TXWA0010.NRA-TAX-RATE TO WS-EDIT-PCT
        //* *MOVE EDITED WS-EDIT-PCT (EM=Z9.99%) TO #TAX-FIX-PCT /* 072014
        //*  072014
        pnd_Tax_Fix_Pct.setValueEdited(ws_Edit_Pct,new ReportEditMask("ZZ9.99%"));                                                                                        //Natural: MOVE EDITED WS-EDIT-PCT ( EM = ZZ9.99% ) TO #TAX-FIX-PCT
        ws_Msg_Out.setValue(DbsUtil.compress(pnd_Nra_Msg, pnd_Tax_Fix_Pct));                                                                                              //Natural: COMPRESS #NRA-MSG #TAX-FIX-PCT INTO WS-MSG-OUT
    }
    private void sub_Create_Federal_Election_Msg() throws Exception                                                                                                       //Natural: CREATE-FEDERAL-ELECTION-MSG
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        if (condition(testing_Mode.getBoolean()))                                                                                                                         //Natural: IF TESTING-MODE
        {
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Option(),pdaTxwa0010.getPnd_Txwa0010_Settl_Option(),pdaTxwa0010.getPnd_Txwa0010_Contract_Nbr(), //Natural: WRITE '=' #TXWA0010.FED-ELC-OPTION #TXWA0010.SETTL-OPTION #TXWA0010.CONTRACT-NBR #PROD-FIELD
                pnd_Prod_Field);
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Method());                                                                                      //Natural: WRITE '=' #TXWA0010.FED-ELC-METHOD
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_Fed_Filing_Status());                                                                                   //Natural: WRITE '=' #TXWA0010.FED-FILING-STATUS
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_Fed_Allowances());                                                                                      //Natural: WRITE '=' #TXWA0010.FED-ALLOWANCES
            if (Global.isEscape()) return;
            if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Add_Wh().greater(getZero())))                                                                                   //Natural: IF #TXWA0010.FED-ADD-WH > 0
            {
                getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_Fed_Add_Wh());                                                                                      //Natural: WRITE '=' #TXWA0010.FED-ADD-WH
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Flat_Amt().greater(getZero())))                                                                                 //Natural: IF #TXWA0010.FED-FLAT-AMT > 0
            {
                getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_Fed_Flat_Amt());                                                                                    //Natural: WRITE '=' #TXWA0010.FED-FLAT-AMT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_Fed_Fix_Pct());                                                                                         //Natural: WRITE '=' #TXWA0010.FED-FIX-PCT
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_Roll_Elig_Ind());                                                                                       //Natural: WRITE '=' #TXWA0010.ROLL-ELIG-IND
            if (Global.isEscape()) return;
            getReports().write(0, "------------------------------------");                                                                                                //Natural: WRITE '------------------------------------'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_A.reset();                                                                                                                                                    //Natural: RESET #A
        pnd_Tax_Flat_Amt.reset();                                                                                                                                         //Natural: RESET #TAX-FLAT-AMT
        pnd_Tax_Fix_Pct.reset();                                                                                                                                          //Natural: RESET #TAX-FIX-PCT
        pnd_Filing_Stat.reset();                                                                                                                                          //Natural: RESET #FILING-STAT
        pnd_Tax_Exempt.reset();                                                                                                                                           //Natural: RESET #TAX-EXEMPT
                                                                                                                                                                          //Natural: PERFORM LOAD-FEDERAL-ELECTION-DATA
        sub_Load_Federal_Election_Data();
        if (condition(Global.isEscape())) {return;}
        //*  D= DEFAULT
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Option().equals("D") || pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Option().equals("M")))                              //Natural: IF #TXWA0010.FED-ELC-OPTION = 'D' OR = 'M'
        {
                                                                                                                                                                          //Natural: PERFORM CREATE-FEDERAL-DEFAULT-TEXT
            sub_Create_Federal_Default_Text();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  N= OPT OUT 0 WITHHOLD
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Option().equals("N")))                                                                                          //Natural: IF #TXWA0010.FED-ELC-OPTION = 'N'
        {
            ws_Msg_Out.setValue(pnd_Do_Not_Withhold);                                                                                                                     //Natural: MOVE #DO-NOT-WITHHOLD TO WS-MSG-OUT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  NO ELECTION
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Option().equals(" ")))                                                                                          //Natural: IF #TXWA0010.FED-ELC-OPTION = ' '
        {
            ws_Msg_Out.setValue(" ");                                                                                                                                     //Natural: MOVE ' ' TO WS-MSG-OUT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ELECTION ENTERED
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Option().equals("Y")))                                                                                          //Natural: IF #TXWA0010.FED-ELC-OPTION = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM CREATE-FEDERAL-ELECTION-TEXT
            sub_Create_Federal_Election_Text();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Load_Federal_Election_Data() throws Exception                                                                                                        //Natural: LOAD-FEDERAL-ELECTION-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        //*  FILING STATUS
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Filing_Status().greater(getZero())))                                                                                //Natural: IF #TXWA0010.FED-FILING-STATUS > 0
        {
            pnd_A.setValue(pdaTxwa0010.getPnd_Txwa0010_Fed_Filing_Status());                                                                                              //Natural: MOVE #TXWA0010.FED-FILING-STATUS TO #A
            pnd_Filing_Stat.setValue(pnd_Stat_Dscr.getValue(pnd_A));                                                                                                      //Natural: MOVE #STAT-DSCR ( #A ) TO #FILING-STAT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Filing_Stat.setValue(" ");                                                                                                                                //Natural: MOVE ' ' TO #FILING-STAT
        }                                                                                                                                                                 //Natural: END-IF
        //*  NUMBER OF ALLOW
        pnd_Tax_Exempt.setValue(pdaTxwa0010.getPnd_Txwa0010_Fed_Allowances());                                                                                            //Natural: MOVE #TXWA0010.FED-ALLOWANCES TO #TAX-EXEMPT
        //*  ADDITIONAL AMT ENTERED
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Add_Wh().greater(getZero())))                                                                                       //Natural: IF #TXWA0010.FED-ADD-WH > 0
        {
            pnd_Tax_Addl_Amt.setValue(pdaTxwa0010.getPnd_Txwa0010_Fed_Add_Wh());                                                                                          //Natural: MOVE #TXWA0010.FED-ADD-WH TO #TAX-ADDL-AMT
            ws_Edit_Flat_Amt.setValue(pnd_Tax_Addl_Amt);                                                                                                                  //Natural: MOVE #TAX-ADDL-AMT TO WS-EDIT-FLAT-AMT
            ws_Amt_Out.setValueEdited(ws_Edit_Flat_Amt,new ReportEditMask("+ZZ,ZZ9.99"));                                                                                 //Natural: MOVE EDITED WS-EDIT-FLAT-AMT ( EM = +ZZ,ZZ9.99 ) TO WS-AMT-OUT
            DbsUtil.examine(new ExamineSource(ws_Amt_Out), new ExamineSearch("+"), new ExamineReplace("$"));                                                              //Natural: EXAMINE WS-AMT-OUT FOR '+' REPLACE '$'
            pnd_Tax_D_Amt.setValue(ws_Amt_Out, MoveOption.LeftJustified);                                                                                                 //Natural: MOVE LEFT JUSTIFIED WS-AMT-OUT TO #TAX-D-AMT
        }                                                                                                                                                                 //Natural: END-IF
        //*  FLAT AMT ENTERED
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Flat_Amt().greater(getZero())))                                                                                     //Natural: IF #TXWA0010.FED-FLAT-AMT > 0
        {
            pnd_Tax_Flat_Amt.setValue(pdaTxwa0010.getPnd_Txwa0010_Fed_Flat_Amt());                                                                                        //Natural: MOVE #TXWA0010.FED-FLAT-AMT TO #TAX-FLAT-AMT
            ws_Edit_Flat_Amt.setValue(pnd_Tax_Flat_Amt);                                                                                                                  //Natural: MOVE #TAX-FLAT-AMT TO WS-EDIT-FLAT-AMT
            ws_Amt_Out.setValueEdited(ws_Edit_Flat_Amt,new ReportEditMask("+ZZ,ZZ9.99"));                                                                                 //Natural: MOVE EDITED WS-EDIT-FLAT-AMT ( EM = +ZZ,ZZ9.99 ) TO WS-AMT-OUT
            DbsUtil.examine(new ExamineSource(ws_Amt_Out), new ExamineSearch("+"), new ExamineReplace("$"));                                                              //Natural: EXAMINE WS-AMT-OUT FOR '+' REPLACE '$'
            ws_Work_Flat_Amt.setValue(ws_Amt_Out, MoveOption.LeftJustified);                                                                                              //Natural: MOVE LEFT JUSTIFIED WS-AMT-OUT TO WS-WORK-FLAT-AMT
        }                                                                                                                                                                 //Natural: END-IF
        ws_Edit_Pct.setValue(pdaTxwa0010.getPnd_Txwa0010_Fed_Fix_Pct());                                                                                                  //Natural: MOVE #TXWA0010.FED-FIX-PCT TO WS-EDIT-PCT
        //* *MOVE EDITED WS-EDIT-PCT (EM=Z9.99%) TO #TAX-FIX-PCT /* 072014
        //*  072014
        pnd_Tax_Fix_Pct.setValueEdited(ws_Edit_Pct,new ReportEditMask("ZZ9.99%"));                                                                                        //Natural: MOVE EDITED WS-EDIT-PCT ( EM = ZZ9.99% ) TO #TAX-FIX-PCT
    }
    private void sub_Create_Federal_Default_Text() throws Exception                                                                                                       //Natural: CREATE-FEDERAL-DEFAULT-TEXT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Method().equals(" ")))                                                                                          //Natural: IF #TXWA0010.FED-ELC-METHOD = ' '
        {
                                                                                                                                                                          //Natural: PERFORM FEDERAL-ELECTION-METHOD-NA
            sub_Federal_Election_Method_Na();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ALLOWANCES ENTERED
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Method().equals("1")))                                                                                          //Natural: IF #TXWA0010.FED-ELC-METHOD = '1'
        {
                                                                                                                                                                          //Natural: PERFORM FEDERAL-ELECTION-METHOD-1
            sub_Federal_Election_Method_1();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  FLAT AMOUNT
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Method().equals("2")))                                                                                          //Natural: IF #TXWA0010.FED-ELC-METHOD = '2'
        {
                                                                                                                                                                          //Natural: PERFORM FEDERAL-ELECTION-METHOD-2
            sub_Federal_Election_Method_2();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  FIXED PERCENTAGE
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Method().equals("3")))                                                                                          //Natural: IF #TXWA0010.FED-ELC-METHOD = '3'
        {
                                                                                                                                                                          //Natural: PERFORM FEDERAL-ELECTION-METHOD-3
            sub_Federal_Election_Method_3();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_Federal_Election_Text() throws Exception                                                                                                      //Natural: CREATE-FEDERAL-ELECTION-TEXT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        //*  ALLOWANCES ENTERED
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Method().equals("1")))                                                                                          //Natural: IF #TXWA0010.FED-ELC-METHOD = '1'
        {
                                                                                                                                                                          //Natural: PERFORM FEDERAL-ELECTION-METHOD-1
            sub_Federal_Election_Method_1();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  FLAT AMOUNT
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Method().equals("2")))                                                                                          //Natural: IF #TXWA0010.FED-ELC-METHOD = '2'
        {
                                                                                                                                                                          //Natural: PERFORM FEDERAL-ELECTION-METHOD-2
            sub_Federal_Election_Method_2();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  FIXED PERCENTAGE
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Method().equals("3")))                                                                                          //Natural: IF #TXWA0010.FED-ELC-METHOD = '3'
        {
                                                                                                                                                                          //Natural: PERFORM FEDERAL-ELECTION-METHOD-3
            sub_Federal_Election_Method_3();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Federal_Election_Method_Na() throws Exception                                                                                                        //Natural: FEDERAL-ELECTION-METHOD-NA
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        if (condition(pnd_Pp.equals("Y")))                                                                                                                                //Natural: IF #PP = 'Y'
        {
            //*  PP DEFAULT
            ws_Msg_Out.setValue(DbsUtil.compress(pnd_Deflt1, pnd_Deflt2, pnd_Pp_Default));                                                                                //Natural: COMPRESS #DEFLT1 #DEFLT2 #PP-DEFAULT INTO WS-MSG-OUT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Settl_Option().equals("30")))                                                                                           //Natural: IF #TXWA0010.SETTL-OPTION = '30'
        {
            //*  MDO DEFAULT
            ws_Msg_Out.setValue(DbsUtil.compress(pnd_Deflt1, pnd_Deflt2, pnd_Mdo_Default));                                                                               //Natural: COMPRESS #DEFLT1 #DEFLT2 #MDO-DEFAULT INTO WS-MSG-OUT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ls.equals("Y")))                                                                                                                                //Natural: IF #LS = 'Y'
        {
            //*  LS DEFAULT
            ws_Msg_Out.setValue(DbsUtil.compress(pnd_Deflt1, pnd_Deflt2, pnd_Ls_Default));                                                                                //Natural: COMPRESS #DEFLT1 #DEFLT2 #LS-DEFAULT INTO WS-MSG-OUT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Settl_Option().equals("RTB")))                                                                                          //Natural: IF #TXWA0010.SETTL-OPTION = 'RTB'
        {
            //*  RTB DEFAULT
            ws_Msg_Out.setValue(DbsUtil.compress(pnd_Rtb_Default, pnd_Tax_Amt_Lit));                                                                                      //Natural: COMPRESS #RTB-DEFAULT #TAX-AMT-LIT INTO WS-MSG-OUT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Federal_Election_Method_1() throws Exception                                                                                                         //Natural: FEDERAL-ELECTION-METHOD-1
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  D= DEFAULT
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Option().equals("D") || pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Option().equals("M")))                              //Natural: IF #TXWA0010.FED-ELC-OPTION = 'D' OR = 'M'
        {
            if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Filing_Status().greater(getZero())))                                                                            //Natural: IF #TXWA0010.FED-FILING-STATUS > 0
            {
                if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Add_Wh().greater(getZero())))                                                                               //Natural: IF #TXWA0010.FED-ADD-WH > 0
                {
                    ws_Msg_Out.setValue(DbsUtil.compress(pnd_Deflt1, pnd_Deflt2, pnd_Filing_Stat, pnd_With_Txt, pnd_Tax_Exempt, pnd_Allw_Txt, pnd_And_Txt,                //Natural: COMPRESS #DEFLT1 #DEFLT2 #FILING-STAT #WITH-TXT #TAX-EXEMPT #ALLW-TXT #AND-TXT #ADDL-TXT #TAX-D-AMT INTO WS-MSG-OUT
                        pnd_Addl_Txt, pnd_Tax_D_Amt));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ws_Msg_Out.setValue(DbsUtil.compress(pnd_Deflt1, pnd_Deflt2, pnd_Filing_Stat, pnd_With_Txt, pnd_Tax_Exempt, pnd_Allw_Txt));                           //Natural: COMPRESS #DEFLT1 #DEFLT2 #FILING-STAT #WITH-TXT #TAX-EXEMPT #ALLW-TXT INTO WS-MSG-OUT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ws_Msg_Out.setValue(DbsUtil.compress(pnd_Deflt1, pnd_Deflt2, pnd_Tax_Exempt, pnd_Allw_Txt));                                                              //Natural: COMPRESS #DEFLT1 #DEFLT2 #TAX-EXEMPT #ALLW-TXT INTO WS-MSG-OUT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ELECTION ENTERED
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Option().equals("Y")))                                                                                          //Natural: IF #TXWA0010.FED-ELC-OPTION = 'Y'
        {
            if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Filing_Status().greater(getZero())))                                                                            //Natural: IF #TXWA0010.FED-FILING-STATUS > 0
            {
                if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Add_Wh().greater(getZero())))                                                                               //Natural: IF #TXWA0010.FED-ADD-WH > 0
                {
                    ws_Msg_Out.setValue(DbsUtil.compress(pnd_Filing_Stat, pnd_With_Txt, pnd_Tax_Exempt, pnd_Allw_Txt, pnd_And_Txt, pnd_Addl_Txt, pnd_Tax_D_Amt));         //Natural: COMPRESS #FILING-STAT #WITH-TXT #TAX-EXEMPT #ALLW-TXT #AND-TXT #ADDL-TXT #TAX-D-AMT INTO WS-MSG-OUT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ws_Msg_Out.setValue(DbsUtil.compress(pnd_Filing_Stat, pnd_With_Txt, pnd_Tax_Exempt, pnd_Allw_Txt));                                                   //Natural: COMPRESS #FILING-STAT #WITH-TXT #TAX-EXEMPT #ALLW-TXT INTO WS-MSG-OUT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ws_Msg_Out.setValue(DbsUtil.compress(pnd_Tax_Exempt, pnd_Allw_Txt));                                                                                      //Natural: COMPRESS #TAX-EXEMPT #ALLW-TXT INTO WS-MSG-OUT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Federal_Election_Method_2() throws Exception                                                                                                         //Natural: FEDERAL-ELECTION-METHOD-2
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  D= DEFAULT
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Option().equals("D") || pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Option().equals("M")))                              //Natural: IF #TXWA0010.FED-ELC-OPTION = 'D' OR = 'M'
        {
            ws_Msg_Out.setValue(DbsUtil.compress(pnd_Deflt1, pnd_Deflt2, pnd_Flat_Txt2, ws_Work_Flat_Amt));                                                               //Natural: COMPRESS #DEFLT1 #DEFLT2 #FLAT-TXT2 WS-WORK-FLAT-AMT INTO WS-MSG-OUT
        }                                                                                                                                                                 //Natural: END-IF
        //*  ELECTION ENTERED
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Option().equals("Y")))                                                                                          //Natural: IF #TXWA0010.FED-ELC-OPTION = 'Y'
        {
            ws_Msg_Out.setValue(DbsUtil.compress(pnd_Flat_Txt, ws_Work_Flat_Amt));                                                                                        //Natural: COMPRESS #FLAT-TXT WS-WORK-FLAT-AMT INTO WS-MSG-OUT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Federal_Election_Method_3() throws Exception                                                                                                         //Natural: FEDERAL-ELECTION-METHOD-3
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  D= DEFAULT
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Option().equals("D") || pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Option().equals("M")))                              //Natural: IF #TXWA0010.FED-ELC-OPTION = 'D' OR = 'M'
        {
            if (condition(pdaTxwa0010.getPnd_Txwa0010_Settl_Option().equals("RTB")))                                                                                      //Natural: IF #TXWA0010.SETTL-OPTION = 'RTB'
            {
                //*  RTB DEFAULT
                ws_Msg_Out.setValue(DbsUtil.compress(pnd_Tax_Fix_Pct, pnd_Tax_Amt_Lit));                                                                                  //Natural: COMPRESS #TAX-FIX-PCT #TAX-AMT-LIT INTO WS-MSG-OUT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ws_Msg_Out.setValue(DbsUtil.compress(pnd_Deflt1, pnd_Deflt2, pnd_Tax_Fix_Pct));                                                                           //Natural: COMPRESS #DEFLT1 #DEFLT2 #TAX-FIX-PCT INTO WS-MSG-OUT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ELECTION ENTERED
        if (condition(pdaTxwa0010.getPnd_Txwa0010_Fed_Elc_Option().equals("Y")))                                                                                          //Natural: IF #TXWA0010.FED-ELC-OPTION = 'Y'
        {
            ws_Msg_Out.setValue(DbsUtil.compress(pnd_Tax_Fix_Pct, pnd_Tax_Amt_Lit));                                                                                      //Natural: COMPRESS #TAX-FIX-PCT #TAX-AMT-LIT INTO WS-MSG-OUT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Connecticut_State_Election_Msg() throws Exception                                                                                                    //Natural: CONNECTICUT-STATE-ELECTION-MSG
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        if (condition(testing_Mode.getBoolean()))                                                                                                                         //Natural: IF TESTING-MODE
        {
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Elc_Option(),pdaTxwa0010.getPnd_Txwa0010_Settl_Option(),pdaTxwa0010.getPnd_Txwa0010_Contract_Nbr(),  //Natural: WRITE '=' #TXWA0010.ST-ELC-OPTION #TXWA0010.SETTL-OPTION #TXWA0010.CONTRACT-NBR #PROD-FIELD
                pnd_Prod_Field);
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Elc_Method());                                                                                       //Natural: WRITE '=' #TXWA0010.ST-ELC-METHOD
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Filing_Status());                                                                                    //Natural: WRITE '=' #TXWA0010.ST-FILING-STATUS
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Allowances());                                                                                       //Natural: WRITE '=' #TXWA0010.ST-ALLOWANCES
            if (Global.isEscape()) return;
            //*  DG: 10/10/2007
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Wh_Type());                                                                                          //Natural: WRITE '=' #TXWA0010.ST-WH-TYPE
            if (Global.isEscape()) return;
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Add_Wh().greater(getZero())))                                                                                    //Natural: IF #TXWA0010.ST-ADD-WH > 0
            {
                getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Add_Wh());                                                                                       //Natural: WRITE '=' #TXWA0010.ST-ADD-WH
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Flat_Amt().greater(getZero())))                                                                                  //Natural: IF #TXWA0010.ST-FLAT-AMT > 0
            {
                getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Flat_Amt());                                                                                     //Natural: WRITE '=' #TXWA0010.ST-FLAT-AMT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Fix_Pct());                                                                                          //Natural: WRITE '=' #TXWA0010.ST-FIX-PCT
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_Roll_Elig_Ind());                                                                                       //Natural: WRITE '=' #TXWA0010.ROLL-ELIG-IND
            if (Global.isEscape()) return;
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Allow().equals("Y")))                                                                                     //Natural: IF #TXWA0010.ST-SPOUSE-ALLOW EQ 'Y'
            {
                getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Allow(),pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Allow_Cnt());                               //Natural: WRITE '=' #TXWA0010.ST-SPOUSE-ALLOW #TXWA0010.ST-SPOUSE-ALLOW-CNT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Age_Allow().equals("Y")))                                                                                        //Natural: IF #TXWA0010.ST-AGE-ALLOW EQ 'Y'
            {
                getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Age_Allow(),pdaTxwa0010.getPnd_Txwa0010_St_Age_Allow_Cnt());                                     //Natural: WRITE '=' #TXWA0010.ST-AGE-ALLOW #TXWA0010.ST-AGE-ALLOW-CNT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Age_Allow().equals("Y")))                                                                                 //Natural: IF #TXWA0010.ST-SPOUSE-AGE-ALLOW EQ 'Y'
            {
                getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Age_Allow(),pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Age_Allow_Cnt());                       //Natural: WRITE '=' #TXWA0010.ST-SPOUSE-AGE-ALLOW #TXWA0010.ST-SPOUSE-AGE-ALLOW-CNT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Blind_Allow().equals("Y")))                                                                                      //Natural: IF #TXWA0010.ST-BLIND-ALLOW EQ 'Y'
            {
                getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Blind_Allow(),pdaTxwa0010.getPnd_Txwa0010_St_Blind_Allow_Cnt());                                 //Natural: WRITE '=' #TXWA0010.ST-BLIND-ALLOW #TXWA0010.ST-BLIND-ALLOW-CNT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Blind_Allow().equals("Y")))                                                                               //Natural: IF #TXWA0010.ST-SPOUSE-BLIND-ALLOW EQ 'Y'
            {
                getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Blind_Allow(),pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Blind_Allow_Cnt());                   //Natural: WRITE '=' #TXWA0010.ST-SPOUSE-BLIND-ALLOW #TXWA0010.ST-SPOUSE-BLIND-ALLOW-CNT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Hh_Ind().equals("Y")))                                                                                           //Natural: IF #TXWA0010.ST-HH-IND EQ 'Y'
            {
                getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Hh_Ind());                                                                                       //Natural: WRITE '=' #TXWA0010.ST-HH-IND
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "-------------------------------------");                                                                                               //Natural: WRITE '-------------------------------------'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_A.reset();                                                                                                                                                    //Natural: RESET #A
        pnd_Var_Text_Msg.reset();                                                                                                                                         //Natural: RESET #VAR-TEXT-MSG
        pnd_Tax_Flat_Amt.reset();                                                                                                                                         //Natural: RESET #TAX-FLAT-AMT
        pnd_Tax_Fix_Pct.reset();                                                                                                                                          //Natural: RESET #TAX-FIX-PCT
        pnd_Filing_Stat.reset();                                                                                                                                          //Natural: RESET #FILING-STAT
        pnd_Tax_D_Amt.reset();                                                                                                                                            //Natural: RESET #TAX-D-AMT
        pnd_Tax_Exempt.reset();                                                                                                                                           //Natural: RESET #TAX-EXEMPT
        ws_Work_Flat_Amt.reset();                                                                                                                                         //Natural: RESET WS-WORK-FLAT-AMT
        pnd_I.reset();                                                                                                                                                    //Natural: RESET #I
        //*  ADDITIONAL AMT ENTERED
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Add_Wh().greater(getZero())))                                                                                        //Natural: IF #TXWA0010.ST-ADD-WH > 0
        {
            pnd_Tax_Addl_Amt.setValue(pdaTxwa0010.getPnd_Txwa0010_St_Add_Wh());                                                                                           //Natural: MOVE #TXWA0010.ST-ADD-WH TO #TAX-ADDL-AMT
            ws_Edit_Flat_Amt.setValue(pnd_Tax_Addl_Amt);                                                                                                                  //Natural: MOVE #TAX-ADDL-AMT TO WS-EDIT-FLAT-AMT
            ws_Amt_Out.setValueEdited(ws_Edit_Flat_Amt,new ReportEditMask("+ZZ,ZZ9.99"));                                                                                 //Natural: MOVE EDITED WS-EDIT-FLAT-AMT ( EM = +ZZ,ZZ9.99 ) TO WS-AMT-OUT
            DbsUtil.examine(new ExamineSource(ws_Amt_Out), new ExamineSearch("+"), new ExamineReplace("$"));                                                              //Natural: EXAMINE WS-AMT-OUT FOR '+' REPLACE '$'
            pnd_Tax_D_Amt.setValue(ws_Amt_Out, MoveOption.LeftJustified);                                                                                                 //Natural: MOVE LEFT JUSTIFIED WS-AMT-OUT TO #TAX-D-AMT
        }                                                                                                                                                                 //Natural: END-IF
        //*  FLAT AMT
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Flat_Amt().greater(getZero())))                                                                                      //Natural: IF #TXWA0010.ST-FLAT-AMT > 0
        {
            pnd_Tax_Flat_Amt.setValue(pdaTxwa0010.getPnd_Txwa0010_St_Flat_Amt());                                                                                         //Natural: MOVE #TXWA0010.ST-FLAT-AMT TO #TAX-FLAT-AMT
            ws_Edit_Flat_Amt.setValue(pnd_Tax_Flat_Amt);                                                                                                                  //Natural: MOVE #TAX-FLAT-AMT TO WS-EDIT-FLAT-AMT
            ws_Amt_Out.setValueEdited(ws_Edit_Flat_Amt,new ReportEditMask("+ZZ,ZZ9.99"));                                                                                 //Natural: MOVE EDITED WS-EDIT-FLAT-AMT ( EM = +ZZ,ZZ9.99 ) TO WS-AMT-OUT
            DbsUtil.examine(new ExamineSource(ws_Amt_Out), new ExamineSearch("+"), new ExamineReplace("$"));                                                              //Natural: EXAMINE WS-AMT-OUT FOR '+' REPLACE '$'
            ws_Work_Flat_Amt.setValue(ws_Amt_Out, MoveOption.LeftJustified);                                                                                              //Natural: MOVE LEFT JUSTIFIED WS-AMT-OUT TO WS-WORK-FLAT-AMT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ct_Filing_Status.reset();                                                                                                                                     //Natural: RESET #CT-FILING-STATUS
        pnd_Ct_Filing_Status.setValue(pdaTxwa0010.getPnd_Txwa0010_St_Filing_Status());                                                                                    //Natural: ASSIGN #CT-FILING-STATUS := #TXWA0010.ST-FILING-STATUS
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Elc_Option().equals("Y")))                                                                                           //Natural: IF #TXWA0010.ST-ELC-OPTION = 'Y'
        {
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Elc_Method().equals("1")))                                                                                       //Natural: IF #TXWA0010.ST-ELC-METHOD = '1'
            {
                DbsUtil.examine(new ExamineSource(pnd_Ct_Cds_Pnd_Ct_Filing_Stts.getValue("*")), new ExamineSearch(pnd_Ct_Filing_Status), new ExamineGivingIndex(pnd_I));  //Natural: EXAMINE #CT-FILING-STTS ( * ) FOR #CT-FILING-STATUS GIVING INDEX #I
                if (condition(pnd_I.greater(getZero())))                                                                                                                  //Natural: IF #I > 0
                {
                    if (condition(pnd_Tax_D_Amt.equals(" ")))                                                                                                             //Natural: IF #TAX-D-AMT = ' '
                    {
                        ws_Msg_Out.setValue(DbsUtil.compress(pnd_Ct_With_Txt, pnd_Ct_Cds_Pnd_Ct_With_Cde.getValue(pnd_I)));                                               //Natural: COMPRESS #CT-WITH-TXT #CT-WITH-CDE ( #I ) INTO WS-MSG-OUT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ws_Msg_Out.setValue(DbsUtil.compress(pnd_Ct_With_Txt, pnd_Ct_Cds_Pnd_Ct_With_Cde.getValue(pnd_I), pnd_Ct_Addl_Txt, pnd_Tax_D_Amt));               //Natural: COMPRESS #CT-WITH-TXT #CT-WITH-CDE ( #I ) #CT-ADDL-TXT #TAX-D-AMT INTO WS-MSG-OUT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Elc_Method().equals("2")))                                                                                   //Natural: IF #TXWA0010.ST-ELC-METHOD = '2'
                {
                    if (condition(ws_Work_Flat_Amt.equals(" ")))                                                                                                          //Natural: IF WS-WORK-FLAT-AMT = ' '
                    {
                        ws_Msg_Out.setValue(DbsUtil.compress(pnd_Ct_With_Txt, pnd_Ct_With_Cde_E));                                                                        //Natural: COMPRESS #CT-WITH-TXT #CT-WITH-CDE-E INTO WS-MSG-OUT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ws_Msg_Out.setValue(DbsUtil.compress(pnd_Ct_With_Txt, pnd_Ct_With_Cde_E, pnd_Ct_Addl_Txt, ws_Work_Flat_Amt));                                     //Natural: COMPRESS #CT-WITH-TXT #CT-WITH-CDE-E #CT-ADDL-TXT WS-WORK-FLAT-AMT INTO WS-MSG-OUT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Elc_Option().equals("N")))                                                                                       //Natural: IF #TXWA0010.ST-ELC-OPTION = 'N'
            {
                ws_Msg_Out.setValue(DbsUtil.compress(pnd_Ct_With_Txt, pnd_Ct_With_Cde_E));                                                                                //Natural: COMPRESS #CT-WITH-TXT #CT-WITH-CDE-E INTO WS-MSG-OUT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Elc_Option().equals("D") || pdaTxwa0010.getPnd_Txwa0010_St_Elc_Option().equals("M")))                        //Natural: IF #TXWA0010.ST-ELC-OPTION = 'D' OR = 'M'
                {
                    ws_Edit_Pct.setValue(pdaTxwa0010.getPnd_Txwa0010_St_Fix_Pct());                                                                                       //Natural: MOVE #TXWA0010.ST-FIX-PCT TO WS-EDIT-PCT
                    pnd_Tax_Fix_Pct.setValueEdited(ws_Edit_Pct,new ReportEditMask("ZZ9.99%"));                                                                            //Natural: MOVE EDITED WS-EDIT-PCT ( EM = ZZ9.99% ) TO #TAX-FIX-PCT
                    ws_Msg_Out.setValue(DbsUtil.compress(pnd_Deflt1, pnd_Deflt3, pnd_Tax_Fix_Pct));                                                                       //Natural: COMPRESS #DEFLT1 #DEFLT3 #TAX-FIX-PCT INTO WS-MSG-OUT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  092818 - END
    }
    private void sub_Create_State_Election_Msg() throws Exception                                                                                                         //Natural: CREATE-STATE-ELECTION-MSG
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        if (condition(testing_Mode.getBoolean()))                                                                                                                         //Natural: IF TESTING-MODE
        {
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Elc_Option(),pdaTxwa0010.getPnd_Txwa0010_Settl_Option(),pdaTxwa0010.getPnd_Txwa0010_Contract_Nbr(),  //Natural: WRITE '=' #TXWA0010.ST-ELC-OPTION #TXWA0010.SETTL-OPTION #TXWA0010.CONTRACT-NBR #PROD-FIELD
                pnd_Prod_Field);
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Elc_Method());                                                                                       //Natural: WRITE '=' #TXWA0010.ST-ELC-METHOD
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Filing_Status());                                                                                    //Natural: WRITE '=' #TXWA0010.ST-FILING-STATUS
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Allowances());                                                                                       //Natural: WRITE '=' #TXWA0010.ST-ALLOWANCES
            if (Global.isEscape()) return;
            //*  DG: 10/10/2007
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Wh_Type());                                                                                          //Natural: WRITE '=' #TXWA0010.ST-WH-TYPE
            if (Global.isEscape()) return;
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Add_Wh().greater(getZero())))                                                                                    //Natural: IF #TXWA0010.ST-ADD-WH > 0
            {
                getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Add_Wh());                                                                                       //Natural: WRITE '=' #TXWA0010.ST-ADD-WH
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Flat_Amt().greater(getZero())))                                                                                  //Natural: IF #TXWA0010.ST-FLAT-AMT > 0
            {
                getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Flat_Amt());                                                                                     //Natural: WRITE '=' #TXWA0010.ST-FLAT-AMT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Fix_Pct());                                                                                          //Natural: WRITE '=' #TXWA0010.ST-FIX-PCT
            if (Global.isEscape()) return;
            getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_Roll_Elig_Ind());                                                                                       //Natural: WRITE '=' #TXWA0010.ROLL-ELIG-IND
            if (Global.isEscape()) return;
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Allow().equals("Y")))                                                                                     //Natural: IF #TXWA0010.ST-SPOUSE-ALLOW EQ 'Y'
            {
                getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Allow(),pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Allow_Cnt());                               //Natural: WRITE '=' #TXWA0010.ST-SPOUSE-ALLOW #TXWA0010.ST-SPOUSE-ALLOW-CNT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Age_Allow().equals("Y")))                                                                                        //Natural: IF #TXWA0010.ST-AGE-ALLOW EQ 'Y'
            {
                getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Age_Allow(),pdaTxwa0010.getPnd_Txwa0010_St_Age_Allow_Cnt());                                     //Natural: WRITE '=' #TXWA0010.ST-AGE-ALLOW #TXWA0010.ST-AGE-ALLOW-CNT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Age_Allow().equals("Y")))                                                                                 //Natural: IF #TXWA0010.ST-SPOUSE-AGE-ALLOW EQ 'Y'
            {
                getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Age_Allow(),pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Age_Allow_Cnt());                       //Natural: WRITE '=' #TXWA0010.ST-SPOUSE-AGE-ALLOW #TXWA0010.ST-SPOUSE-AGE-ALLOW-CNT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Blind_Allow().equals("Y")))                                                                                      //Natural: IF #TXWA0010.ST-BLIND-ALLOW EQ 'Y'
            {
                getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Blind_Allow(),pdaTxwa0010.getPnd_Txwa0010_St_Blind_Allow_Cnt());                                 //Natural: WRITE '=' #TXWA0010.ST-BLIND-ALLOW #TXWA0010.ST-BLIND-ALLOW-CNT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Blind_Allow().equals("Y")))                                                                               //Natural: IF #TXWA0010.ST-SPOUSE-BLIND-ALLOW EQ 'Y'
            {
                getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Blind_Allow(),pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Blind_Allow_Cnt());                   //Natural: WRITE '=' #TXWA0010.ST-SPOUSE-BLIND-ALLOW #TXWA0010.ST-SPOUSE-BLIND-ALLOW-CNT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Hh_Ind().equals("Y")))                                                                                           //Natural: IF #TXWA0010.ST-HH-IND EQ 'Y'
            {
                getReports().write(0, "=",pdaTxwa0010.getPnd_Txwa0010_St_Hh_Ind());                                                                                       //Natural: WRITE '=' #TXWA0010.ST-HH-IND
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "-------------------------------------");                                                                                               //Natural: WRITE '-------------------------------------'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_A.reset();                                                                                                                                                    //Natural: RESET #A
        pnd_Var_Text_Msg.reset();                                                                                                                                         //Natural: RESET #VAR-TEXT-MSG
        pnd_Tax_Flat_Amt.reset();                                                                                                                                         //Natural: RESET #TAX-FLAT-AMT
        pnd_Tax_Fix_Pct.reset();                                                                                                                                          //Natural: RESET #TAX-FIX-PCT
        pnd_Filing_Stat.reset();                                                                                                                                          //Natural: RESET #FILING-STAT
        pnd_Tax_D_Amt.reset();                                                                                                                                            //Natural: RESET #TAX-D-AMT
        pnd_Tax_Exempt.reset();                                                                                                                                           //Natural: RESET #TAX-EXEMPT
                                                                                                                                                                          //Natural: PERFORM LOAD-STATE-ELECTION-DATA
        sub_Load_State_Election_Data();
        if (condition(Global.isEscape())) {return;}
        //*  NO STATE WITHHOLDING ALLOWED
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Wh_Type().equals("N")))                                                                                              //Natural: IF #TXWA0010.ST-WH-TYPE = 'N'
        {
            ws_Msg_Out.setValue(pnd_No_With_Allowed);                                                                                                                     //Natural: MOVE #NO-WITH-ALLOWED TO WS-MSG-OUT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  EXEMPT
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Elc_Option().equals("X")))                                                                                           //Natural: IF #TXWA0010.ST-ELC-OPTION = 'X'
        {
            ws_Msg_Out.setValue(pnd_Exempt);                                                                                                                              //Natural: MOVE #EXEMPT TO WS-MSG-OUT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  DEFAULT
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Elc_Option().equals("D")))                                                                                           //Natural: IF #TXWA0010.ST-ELC-OPTION = 'D'
        {
            if (condition(pdaTxwa0010.getPnd_Txwa0010_Settl_Option().equals("RTB")))                                                                                      //Natural: IF #TXWA0010.SETTL-OPTION = 'RTB'
            {
                //*  RTB DEFAULT
                ws_Msg_Out.setValue(DbsUtil.compress(pnd_Rtb_Default, pnd_Tax_Amt_Lit));                                                                                  //Natural: COMPRESS #RTB-DEFAULT #TAX-AMT-LIT INTO WS-MSG-OUT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  MANDATORY
                if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Wh_Type().equals("M")))                                                                                      //Natural: IF #TXWA0010.ST-WH-TYPE = 'M'
                {
                                                                                                                                                                          //Natural: PERFORM STATE-MANDATORY-DEFAULT-TEXT
                    sub_State_Mandatory_Default_Text();
                    if (condition(Global.isEscape())) {return;}
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ws_Msg_Out.setValue(pnd_Do_Not_Withhold);                                                                                                             //Natural: MOVE #DO-NOT-WITHHOLD TO WS-MSG-OUT
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  MANDATORY DEFAULT
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Elc_Option().equals("M")))                                                                                           //Natural: IF #TXWA0010.ST-ELC-OPTION = 'M'
        {
                                                                                                                                                                          //Natural: PERFORM STATE-MANDATORY-DEFAULT-TEXT
            sub_State_Mandatory_Default_Text();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  'N' = OPT OUT
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Elc_Option().equals("N")))                                                                                           //Natural: IF #TXWA0010.ST-ELC-OPTION = 'N'
        {
            ws_Msg_Out.setValue(pnd_Do_Not_Withhold);                                                                                                                     //Natural: MOVE #DO-NOT-WITHHOLD TO WS-MSG-OUT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  ' ' NOT AVAIALABLE
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Elc_Option().equals(" ")))                                                                                       //Natural: IF #TXWA0010.ST-ELC-OPTION = ' '
            {
                ws_Msg_Out.setValue(" ");                                                                                                                                 //Natural: MOVE ' ' TO WS-MSG-OUT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* * #TXWA0010.ST-ELC-OPTION = 'Y' **
        //*  WAGE TABLE
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Elc_Method().equals("1")))                                                                                           //Natural: IF #TXWA0010.ST-ELC-METHOD = '1'
        {
                                                                                                                                                                          //Natural: PERFORM STATE-ELECTION-METHOD-1
            sub_State_Election_Method_1();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  FLAT AMOUNT
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Elc_Method().equals("2")))                                                                                           //Natural: IF #TXWA0010.ST-ELC-METHOD = '2'
        {
                                                                                                                                                                          //Natural: PERFORM STATE-ELECTION-METHOD-2
            sub_State_Election_Method_2();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  FIXED PERCENTAGE
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Elc_Method().equals("3")))                                                                                           //Natural: IF #TXWA0010.ST-ELC-METHOD = '3'
        {
                                                                                                                                                                          //Natural: PERFORM STATE-ELECTION-METHOD-3
            sub_State_Election_Method_3();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  FIXED % OF FEDERAL
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Elc_Method().equals("4")))                                                                                           //Natural: IF #TXWA0010.ST-ELC-METHOD = '4'
        {
                                                                                                                                                                          //Natural: PERFORM STATE-ELECTION-METHOD-4
            sub_State_Election_Method_4();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Load_State_Election_Data() throws Exception                                                                                                          //Natural: LOAD-STATE-ELECTION-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        //*  FILING STATUS
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Filing_Status().greater(getZero())))                                                                                 //Natural: IF #TXWA0010.ST-FILING-STATUS > 0
        {
            pnd_A.setValue(pdaTxwa0010.getPnd_Txwa0010_St_Filing_Status());                                                                                               //Natural: MOVE #TXWA0010.ST-FILING-STATUS TO #A
            pnd_Filing_Stat.setValue(pnd_Stat_Dscr.getValue(pnd_A));                                                                                                      //Natural: MOVE #STAT-DSCR ( #A ) TO #FILING-STAT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Filing_Stat.setValue(" ");                                                                                                                                //Natural: MOVE ' ' TO #FILING-STAT
        }                                                                                                                                                                 //Natural: END-IF
        //*  NO OF EXEMTIONS
        pnd_Tax_Exempt.setValue(pdaTxwa0010.getPnd_Txwa0010_St_Allowances());                                                                                             //Natural: MOVE #TXWA0010.ST-ALLOWANCES TO #TAX-EXEMPT
        //*  ADDITIONAL AMT ENTERED
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Add_Wh().greater(getZero())))                                                                                        //Natural: IF #TXWA0010.ST-ADD-WH > 0
        {
            pnd_Tax_Addl_Amt.setValue(pdaTxwa0010.getPnd_Txwa0010_St_Add_Wh());                                                                                           //Natural: MOVE #TXWA0010.ST-ADD-WH TO #TAX-ADDL-AMT
            ws_Edit_Flat_Amt.setValue(pnd_Tax_Addl_Amt);                                                                                                                  //Natural: MOVE #TAX-ADDL-AMT TO WS-EDIT-FLAT-AMT
            ws_Amt_Out.setValueEdited(ws_Edit_Flat_Amt,new ReportEditMask("+ZZ,ZZ9.99"));                                                                                 //Natural: MOVE EDITED WS-EDIT-FLAT-AMT ( EM = +ZZ,ZZ9.99 ) TO WS-AMT-OUT
            DbsUtil.examine(new ExamineSource(ws_Amt_Out), new ExamineSearch("+"), new ExamineReplace("$"));                                                              //Natural: EXAMINE WS-AMT-OUT FOR '+' REPLACE '$'
            pnd_Tax_D_Amt.setValue(ws_Amt_Out, MoveOption.LeftJustified);                                                                                                 //Natural: MOVE LEFT JUSTIFIED WS-AMT-OUT TO #TAX-D-AMT
        }                                                                                                                                                                 //Natural: END-IF
        //*  FLAT AMT
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Flat_Amt().greater(getZero())))                                                                                      //Natural: IF #TXWA0010.ST-FLAT-AMT > 0
        {
            pnd_Tax_Flat_Amt.setValue(pdaTxwa0010.getPnd_Txwa0010_St_Flat_Amt());                                                                                         //Natural: MOVE #TXWA0010.ST-FLAT-AMT TO #TAX-FLAT-AMT
            ws_Edit_Flat_Amt.setValue(pnd_Tax_Flat_Amt);                                                                                                                  //Natural: MOVE #TAX-FLAT-AMT TO WS-EDIT-FLAT-AMT
            ws_Amt_Out.setValueEdited(ws_Edit_Flat_Amt,new ReportEditMask("+ZZ,ZZ9.99"));                                                                                 //Natural: MOVE EDITED WS-EDIT-FLAT-AMT ( EM = +ZZ,ZZ9.99 ) TO WS-AMT-OUT
            DbsUtil.examine(new ExamineSource(ws_Amt_Out), new ExamineSearch("+"), new ExamineReplace("$"));                                                              //Natural: EXAMINE WS-AMT-OUT FOR '+' REPLACE '$'
            ws_Work_Flat_Amt.setValue(ws_Amt_Out, MoveOption.LeftJustified);                                                                                              //Natural: MOVE LEFT JUSTIFIED WS-AMT-OUT TO WS-WORK-FLAT-AMT
        }                                                                                                                                                                 //Natural: END-IF
        //*  CREATES #VAR-TEXT7
                                                                                                                                                                          //Natural: PERFORM CHECK-ALL-ALLOWANCES-RTN
        sub_Check_All_Allowances_Rtn();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Check_All_Allowances_Rtn() throws Exception                                                                                                          //Natural: CHECK-ALL-ALLOWANCES-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        pnd_Var_Text_Msg.reset();                                                                                                                                         //Natural: RESET #VAR-TEXT-MSG
        pnd_Var_Texta.reset();                                                                                                                                            //Natural: RESET #VAR-TEXTA
        //*  HEAD OF HOUSEHOLD
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Hh_Ind().equals("Y")))                                                                                               //Natural: IF #TXWA0010.ST-HH-IND = 'Y'
        {
            pnd_Var_Text_Msg_Pnd_Var_Text1.setValue(pnd_Stat_Dscr.getValue(8));                                                                                           //Natural: MOVE #STAT-DSCR ( 8 ) TO #VAR-TEXT1
        }                                                                                                                                                                 //Natural: END-IF
        //*  SPOUSAL ALLOWANCE
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Allow().equals("Y")))                                                                                         //Natural: IF #TXWA0010.ST-SPOUSE-ALLOW = 'Y'
        {
            pnd_Var_Text_Msg_Pnd_Var_Text2.setValue(pnd_Sp_Allw);                                                                                                         //Natural: MOVE #SP-ALLW TO #VAR-TEXT2
        }                                                                                                                                                                 //Natural: END-IF
        //*  AGE ALLOWANCE
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Age_Allow().equals("Y")))                                                                                            //Natural: IF #TXWA0010.ST-AGE-ALLOW = 'Y'
        {
            pnd_Var_Text_Msg_Pnd_Var_Text3.setValue(pnd_Age_Allw);                                                                                                        //Natural: MOVE #AGE-ALLW TO #VAR-TEXT3
        }                                                                                                                                                                 //Natural: END-IF
        //*  SPOUSAL AGE ALLOWANCE
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Age_Allow().equals("Y")))                                                                                     //Natural: IF #TXWA0010.ST-SPOUSE-AGE-ALLOW = 'Y'
        {
            pnd_Var_Text_Msg_Pnd_Var_Text4.setValue(pnd_Sp_Age_Allw);                                                                                                     //Natural: MOVE #SP-AGE-ALLW TO #VAR-TEXT4
        }                                                                                                                                                                 //Natural: END-IF
        //*  BLIND ALLOWANCE
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Blind_Allow().equals("Y")))                                                                                          //Natural: IF #TXWA0010.ST-BLIND-ALLOW = 'Y'
        {
            pnd_Var_Text_Msg_Pnd_Var_Text5.setValue(pnd_Bl_Allw);                                                                                                         //Natural: MOVE #BL-ALLW TO #VAR-TEXT5
        }                                                                                                                                                                 //Natural: END-IF
        //*  SPOUSAL BLIND ALLOWANCE
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Spouse_Blind_Allow().equals("Y")))                                                                                   //Natural: IF #TXWA0010.ST-SPOUSE-BLIND-ALLOW = 'Y'
        {
            pnd_Var_Text_Msg_Pnd_Var_Text6.setValue(pnd_Sp_Bl_Allw);                                                                                                      //Natural: MOVE #SP-BL-ALLW TO #VAR-TEXT6
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO 5
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(5)); pnd_I.nadd(1))
        {
            if (condition(pnd_Var_Text_Msg_Pnd_Var_Text1.equals(" ")))                                                                                                    //Natural: IF #VAR-TEXT1 = ' '
            {
                pnd_Var_Text_Msg_Pnd_Var_Text1.setValue(pnd_Var_Text_Msg_Pnd_Var_Text2);                                                                                  //Natural: MOVE #VAR-TEXT2 TO #VAR-TEXT1
                pnd_Var_Text_Msg_Pnd_Var_Text2.reset();                                                                                                                   //Natural: RESET #VAR-TEXT2
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Var_Text_Msg_Pnd_Var_Text2.equals(" ")))                                                                                                    //Natural: IF #VAR-TEXT2 = ' '
            {
                pnd_Var_Text_Msg_Pnd_Var_Text2.setValue(pnd_Var_Text_Msg_Pnd_Var_Text3);                                                                                  //Natural: MOVE #VAR-TEXT3 TO #VAR-TEXT2
                pnd_Var_Text_Msg_Pnd_Var_Text3.reset();                                                                                                                   //Natural: RESET #VAR-TEXT3
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Var_Text_Msg_Pnd_Var_Text3.equals(" ")))                                                                                                    //Natural: IF #VAR-TEXT3 = ' '
            {
                pnd_Var_Text_Msg_Pnd_Var_Text3.setValue(pnd_Var_Text_Msg_Pnd_Var_Text4);                                                                                  //Natural: MOVE #VAR-TEXT4 TO #VAR-TEXT3
                pnd_Var_Text_Msg_Pnd_Var_Text4.reset();                                                                                                                   //Natural: RESET #VAR-TEXT4
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Var_Text_Msg_Pnd_Var_Text4.equals(" ")))                                                                                                    //Natural: IF #VAR-TEXT4 = ' '
            {
                pnd_Var_Text_Msg_Pnd_Var_Text4.setValue(pnd_Var_Text_Msg_Pnd_Var_Text5);                                                                                  //Natural: MOVE #VAR-TEXT5 TO #VAR-TEXT4
                pnd_Var_Text_Msg_Pnd_Var_Text5.reset();                                                                                                                   //Natural: RESET #VAR-TEXT5
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Var_Text_Msg_Pnd_Var_Text5.equals(" ")))                                                                                                    //Natural: IF #VAR-TEXT5 = ' '
            {
                pnd_Var_Text_Msg_Pnd_Var_Text5.setValue(pnd_Var_Text_Msg_Pnd_Var_Text6);                                                                                  //Natural: MOVE #VAR-TEXT6 TO #VAR-TEXT5
                pnd_Var_Text_Msg_Pnd_Var_Text6.reset();                                                                                                                   //Natural: RESET #VAR-TEXT6
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Var_Text_Msg_Pnd_Var_Text1.equals(" ")))                                                                                                        //Natural: IF #VAR-TEXT1 = ' '
        {
            pnd_Var_Texta.setValue(" ");                                                                                                                                  //Natural: MOVE ' ' TO #VAR-TEXTA
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Var_Text_Msg_Pnd_Var_Text7.setValue(DbsUtil.compress(pnd_And_Txt, pnd_Var_Text_Msg_Pnd_Var_Text1));                                                       //Natural: COMPRESS #AND-TXT #VAR-TEXT1 INTO #VAR-TEXT7
            pnd_Var_Text_Msg_Pnd_Var_Text1.reset();                                                                                                                       //Natural: RESET #VAR-TEXT1
            pnd_Var_Text_Msg_Pnd_Var_Text1.setValue(pnd_Var_Text_Msg_Pnd_Var_Text7);                                                                                      //Natural: MOVE #VAR-TEXT7 TO #VAR-TEXT1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Var_Text_Msg_Pnd_Var_Text2.equals(" ")))                                                                                                        //Natural: IF #VAR-TEXT2 = ' '
        {
            pnd_Var_Texta.setValue(pnd_Var_Text_Msg_Pnd_Var_Text1);                                                                                                       //Natural: MOVE #VAR-TEXT1 TO #VAR-TEXTA
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Var_Text_Msg_Pnd_Var_Text3.equals(" ")))                                                                                                        //Natural: IF #VAR-TEXT3 = ' '
        {
            pnd_Var_Texta.setValue(DbsUtil.compress(CompressOption.WithDelimiters, ',', pnd_Var_Text_Msg_Pnd_Var_Text1, pnd_Var_Text_Msg_Pnd_Var_Text2));                 //Natural: COMPRESS #VAR-TEXT1 #VAR-TEXT2 INTO #VAR-TEXTA WITH DELIMITER ','
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Var_Text_Msg_Pnd_Var_Text4.equals(" ")))                                                                                                        //Natural: IF #VAR-TEXT4 = ' '
        {
            pnd_Var_Texta.setValue(DbsUtil.compress(CompressOption.WithDelimiters, ',', pnd_Var_Text_Msg_Pnd_Var_Text1, pnd_Var_Text_Msg_Pnd_Var_Text2,                   //Natural: COMPRESS #VAR-TEXT1 #VAR-TEXT2 #VAR-TEXT3 INTO #VAR-TEXTA WITH DELIMITER ','
                pnd_Var_Text_Msg_Pnd_Var_Text3));
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Var_Text_Msg_Pnd_Var_Text5.equals(" ")))                                                                                                        //Natural: IF #VAR-TEXT5 = ' '
        {
            pnd_Var_Texta.setValue(DbsUtil.compress(CompressOption.WithDelimiters, ',', pnd_Var_Text_Msg_Pnd_Var_Text1, pnd_Var_Text_Msg_Pnd_Var_Text2,                   //Natural: COMPRESS #VAR-TEXT1 #VAR-TEXT2 #VAR-TEXT3 #VAR-TEXT4 INTO #VAR-TEXTA WITH DELIMITER ','
                pnd_Var_Text_Msg_Pnd_Var_Text3, pnd_Var_Text_Msg_Pnd_Var_Text4));
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Var_Text_Msg_Pnd_Var_Text6.equals(" ")))                                                                                                        //Natural: IF #VAR-TEXT6 = ' '
        {
            pnd_Var_Texta.setValue(DbsUtil.compress(CompressOption.WithDelimiters, ',', pnd_Var_Text_Msg_Pnd_Var_Text2, pnd_Var_Text_Msg_Pnd_Var_Text3,                   //Natural: COMPRESS #VAR-TEXT2 #VAR-TEXT3 #VAR-TEXT4 #VAR-TEXT5 INTO #VAR-TEXTA WITH DELIMITER ','
                pnd_Var_Text_Msg_Pnd_Var_Text4, pnd_Var_Text_Msg_Pnd_Var_Text5));
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Var_Texta.setValue(DbsUtil.compress(CompressOption.WithDelimiters, ',', pnd_Var_Text_Msg_Pnd_Var_Text1, pnd_Var_Text_Msg_Pnd_Var_Text2,                   //Natural: COMPRESS #VAR-TEXT1 #VAR-TEXT2 #VAR-TEXT3 #VAR-TEXT4 #VAR-TEXT5 #VAR-TEXT6 INTO #VAR-TEXTA WITH DELIMITER ','
                pnd_Var_Text_Msg_Pnd_Var_Text3, pnd_Var_Text_Msg_Pnd_Var_Text4, pnd_Var_Text_Msg_Pnd_Var_Text5, pnd_Var_Text_Msg_Pnd_Var_Text6));
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_State_Election_Method_1() throws Exception                                                                                                           //Natural: STATE-ELECTION-METHOD-1
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Filing_Status().equals(getZero())))                                                                                  //Natural: IF #TXWA0010.ST-FILING-STATUS = 0
        {
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Add_Wh().greater(getZero())))                                                                                    //Natural: IF #TXWA0010.ST-ADD-WH > 0
            {
                ws_Msg_Out.setValue(DbsUtil.compress(pnd_Tax_Exempt, pnd_Allw_Txt, pnd_Var_Texta, pnd_And_Txt, pnd_Addl_Txt, pnd_Tax_D_Amt));                             //Natural: COMPRESS #TAX-EXEMPT #ALLW-TXT #VAR-TEXTA #AND-TXT #ADDL-TXT #TAX-D-AMT INTO WS-MSG-OUT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ws_Msg_Out.setValue(DbsUtil.compress(pnd_Tax_Exempt, pnd_Allw_Txt, pnd_Var_Texta));                                                                       //Natural: COMPRESS #TAX-EXEMPT #ALLW-TXT #VAR-TEXTA INTO WS-MSG-OUT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM CREATE-STATE-WAGE-TABLE-TEXT
            sub_Create_State_Wage_Table_Text();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_State_Election_Method_2() throws Exception                                                                                                           //Natural: STATE-ELECTION-METHOD-2
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        ws_Msg_Out.setValue(DbsUtil.compress(pnd_Flat_Txt, ws_Work_Flat_Amt));                                                                                            //Natural: COMPRESS #FLAT-TXT WS-WORK-FLAT-AMT INTO WS-MSG-OUT
    }
    private void sub_State_Election_Method_3() throws Exception                                                                                                           //Natural: STATE-ELECTION-METHOD-3
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        ws_Edit_Pct.setValue(pdaTxwa0010.getPnd_Txwa0010_St_Fix_Pct());                                                                                                   //Natural: MOVE #TXWA0010.ST-FIX-PCT TO WS-EDIT-PCT
        //* *MOVE EDITED WS-EDIT-PCT (EM=Z9.99%) TO #TAX-FIX-PCT /* 072014
        //*  072014
        pnd_Tax_Fix_Pct.setValueEdited(ws_Edit_Pct,new ReportEditMask("ZZ9.99%"));                                                                                        //Natural: MOVE EDITED WS-EDIT-PCT ( EM = ZZ9.99% ) TO #TAX-FIX-PCT
        ws_Msg_Out.setValue(DbsUtil.compress(pnd_Tax_Fix_Pct, pnd_Tax_Amt_Lit));                                                                                          //Natural: COMPRESS #TAX-FIX-PCT #TAX-AMT-LIT INTO WS-MSG-OUT
    }
    private void sub_State_Election_Method_4() throws Exception                                                                                                           //Natural: STATE-ELECTION-METHOD-4
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        ws_Edit_Pct.setValue(pdaTxwa0010.getPnd_Txwa0010_St_Fix_Pct());                                                                                                   //Natural: MOVE #TXWA0010.ST-FIX-PCT TO WS-EDIT-PCT
        //* *MOVE EDITED WS-EDIT-PCT (EM=Z9.99%) TO #TAX-FIX-PCT /* 072014
        //*  072014
        pnd_Tax_Fix_Pct.setValueEdited(ws_Edit_Pct,new ReportEditMask("ZZ9.99%"));                                                                                        //Natural: MOVE EDITED WS-EDIT-PCT ( EM = ZZ9.99% ) TO #TAX-FIX-PCT
        ws_Msg_Out.setValue(DbsUtil.compress(pnd_Tax_Fix_Pct, pnd_Fed_Amt_Lit));                                                                                          //Natural: COMPRESS #TAX-FIX-PCT #FED-AMT-LIT INTO WS-MSG-OUT
    }
    private void sub_State_Mandatory_Default_Text() throws Exception                                                                                                      //Natural: STATE-MANDATORY-DEFAULT-TEXT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        //*  WAGE TABLE
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Elc_Method().equals("1")))                                                                                           //Natural: IF #TXWA0010.ST-ELC-METHOD = '1'
        {
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Filing_Status().greater(getZero())))                                                                             //Natural: IF #TXWA0010.ST-FILING-STATUS > 0
            {
                if (condition(pdaTxwa0010.getPnd_Txwa0010_Settl_Option().equals("RTB")))                                                                                  //Natural: IF #TXWA0010.SETTL-OPTION = 'RTB'
                {
                    ws_Msg_Out.setValue(DbsUtil.compress(pnd_Filing_Stat, pnd_With_Txt, pnd_Tax_Exempt, pnd_Allw_Txt));                                                   //Natural: COMPRESS #FILING-STAT #WITH-TXT #TAX-EXEMPT #ALLW-TXT INTO WS-MSG-OUT
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ws_Msg_Out.setValue(DbsUtil.compress(pnd_Deflt1, pnd_Deflt2, pnd_Filing_Stat, pnd_With_Txt, pnd_Tax_Exempt, pnd_Allw_Txt));                           //Natural: COMPRESS #DEFLT1 #DEFLT2 #FILING-STAT #WITH-TXT #TAX-EXEMPT #ALLW-TXT INTO WS-MSG-OUT
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ws_Msg_Out.setValue(DbsUtil.compress(pnd_Deflt1, pnd_Deflt2, pnd_With_Txt, pnd_Tax_Exempt, pnd_Allw_Txt));                                                //Natural: COMPRESS #DEFLT1 #DEFLT2 #WITH-TXT #TAX-EXEMPT #ALLW-TXT INTO WS-MSG-OUT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  END OF WAGE TABLE
        }                                                                                                                                                                 //Natural: END-IF
        //*  FLAT AMOUNT
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Elc_Method().equals("2")))                                                                                           //Natural: IF #TXWA0010.ST-ELC-METHOD = '2'
        {
            if (condition(pdaTxwa0010.getPnd_Txwa0010_Settl_Option().equals("RTB")))                                                                                      //Natural: IF #TXWA0010.SETTL-OPTION = 'RTB'
            {
                ws_Msg_Out.setValue(DbsUtil.compress(pnd_Flat_Txt, ws_Work_Flat_Amt));                                                                                    //Natural: COMPRESS #FLAT-TXT WS-WORK-FLAT-AMT INTO WS-MSG-OUT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ws_Msg_Out.setValue(DbsUtil.compress(pnd_Deflt1, pnd_Deflt1, pnd_Flat_Txt, ws_Work_Flat_Amt));                                                            //Natural: COMPRESS #DEFLT1 #DEFLT1 #FLAT-TXT WS-WORK-FLAT-AMT INTO WS-MSG-OUT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  FIXED PERCENTAGE
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Elc_Method().equals("3")))                                                                                           //Natural: IF #TXWA0010.ST-ELC-METHOD = '3'
        {
            ws_Edit_Pct.setValue(pdaTxwa0010.getPnd_Txwa0010_St_Fix_Pct());                                                                                               //Natural: MOVE #TXWA0010.ST-FIX-PCT TO WS-EDIT-PCT
            //*  MOVE EDITED WS-EDIT-PCT (EM=Z9.99%) TO #TAX-FIX-PCT  /* 072014
            //*  072014
            pnd_Tax_Fix_Pct.setValueEdited(ws_Edit_Pct,new ReportEditMask("ZZ9.99%"));                                                                                    //Natural: MOVE EDITED WS-EDIT-PCT ( EM = ZZ9.99% ) TO #TAX-FIX-PCT
            if (condition(pdaTxwa0010.getPnd_Txwa0010_Settl_Option().equals("RTB")))                                                                                      //Natural: IF #TXWA0010.SETTL-OPTION = 'RTB'
            {
                ws_Msg_Out.setValue(DbsUtil.compress(pnd_Tax_Fix_Pct, pnd_Tax_Amt_Lit));                                                                                  //Natural: COMPRESS #TAX-FIX-PCT #TAX-AMT-LIT INTO WS-MSG-OUT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ws_Msg_Out.setValue(DbsUtil.compress(pnd_Deflt1, pnd_Deflt2, pnd_Tax_Fix_Pct));                                                                           //Natural: COMPRESS #DEFLT1 #DEFLT2 #TAX-FIX-PCT INTO WS-MSG-OUT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  FIXED % OF FEDERAL
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Elc_Method().equals("4")))                                                                                           //Natural: IF #TXWA0010.ST-ELC-METHOD = '4'
        {
            ws_Edit_Pct.setValue(pdaTxwa0010.getPnd_Txwa0010_St_Fix_Pct());                                                                                               //Natural: MOVE #TXWA0010.ST-FIX-PCT TO WS-EDIT-PCT
            //*  MOVE EDITED WS-EDIT-PCT (EM=Z9.99%) TO #TAX-FIX-PCT  /* 072014
            //*  072014
            pnd_Tax_Fix_Pct.setValueEdited(ws_Edit_Pct,new ReportEditMask("ZZ9.99%"));                                                                                    //Natural: MOVE EDITED WS-EDIT-PCT ( EM = ZZ9.99% ) TO #TAX-FIX-PCT
            if (condition(pdaTxwa0010.getPnd_Txwa0010_Settl_Option().equals("RTB")))                                                                                      //Natural: IF #TXWA0010.SETTL-OPTION = 'RTB'
            {
                ws_Msg_Out.setValue(DbsUtil.compress(pnd_Tax_Fix_Pct, pnd_Fed_Amt_Lit));                                                                                  //Natural: COMPRESS #TAX-FIX-PCT #FED-AMT-LIT INTO WS-MSG-OUT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ws_Msg_Out.setValue(DbsUtil.compress(pnd_Deflt1, pnd_Deflt2, pnd_Tax_Fix_Pct, pnd_Fed_Amt_Lit));                                                          //Natural: COMPRESS #DEFLT1 #DEFLT2 #TAX-FIX-PCT #FED-AMT-LIT INTO WS-MSG-OUT
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  NO METHOD
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Elc_Method().equals(" ")))                                                                                           //Natural: IF #TXWA0010.ST-ELC-METHOD = ' '
        {
            ws_Msg_Out.setValue(DbsUtil.compress(pnd_Deflt1, pnd_Deflt2, pnd_Do_Not_Withhold));                                                                           //Natural: COMPRESS #DEFLT1 #DEFLT2 #DO-NOT-WITHHOLD INTO WS-MSG-OUT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_State_Wage_Table_Text() throws Exception                                                                                                      //Natural: CREATE-STATE-WAGE-TABLE-TEXT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        //*  FILING STATUS
        if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Filing_Status().greater(getZero())))                                                                                 //Natural: IF #TXWA0010.ST-FILING-STATUS > 0
        {
            if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Allowances().greater(getZero())))                                                                                //Natural: IF #TXWA0010.ST-ALLOWANCES > 0
            {
                if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Add_Wh().greater(getZero())))                                                                                //Natural: IF #TXWA0010.ST-ADD-WH > 0
                {
                    ws_Msg_Out.setValue(DbsUtil.compress(pnd_Filing_Stat, pnd_With_Txt, pnd_Tax_Exempt, pnd_Allw_Txt, pnd_Var_Texta, pnd_And_Txt, pnd_Addl_Txt,           //Natural: COMPRESS #FILING-STAT #WITH-TXT #TAX-EXEMPT #ALLW-TXT #VAR-TEXTA #AND-TXT #ADDL-TXT #TAX-D-AMT INTO WS-MSG-OUT
                        pnd_Tax_D_Amt));
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ws_Msg_Out.setValue(DbsUtil.compress(pnd_Filing_Stat, pnd_With_Txt, pnd_Tax_Exempt, pnd_Allw_Txt, pnd_Var_Texta));                                    //Natural: COMPRESS #FILING-STAT #WITH-TXT #TAX-EXEMPT #ALLW-TXT #VAR-TEXTA INTO WS-MSG-OUT
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaTxwa0010.getPnd_Txwa0010_St_Add_Wh().greater(getZero())))                                                                                //Natural: IF #TXWA0010.ST-ADD-WH > 0
                {
                    ws_Msg_Out.setValue(DbsUtil.compress(pnd_Filing_Stat, pnd_With_Txt, pnd_Tax_Exempt, pnd_Allw_Txt, pnd_Var_Texta));                                    //Natural: COMPRESS #FILING-STAT #WITH-TXT #TAX-EXEMPT #ALLW-TXT #VAR-TEXTA INTO WS-MSG-OUT
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ws_Msg_Out.setValue(DbsUtil.compress(pnd_Filing_Stat, pnd_With_Txt, pnd_Tax_Exempt, pnd_Allw_Txt, pnd_Var_Texta));                                    //Natural: COMPRESS #FILING-STAT #WITH-TXT #TAX-EXEMPT #ALLW-TXT #VAR-TEXTA INTO WS-MSG-OUT
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ws_Msg_Out.setValue(DbsUtil.compress(pnd_Tax_Exempt, pnd_Allw_Txt, pnd_Var_Texta));                                                                           //Natural: COMPRESS #TAX-EXEMPT #ALLW-TXT #VAR-TEXTA INTO WS-MSG-OUT
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
