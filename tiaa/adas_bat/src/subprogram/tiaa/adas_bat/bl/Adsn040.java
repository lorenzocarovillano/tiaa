/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:53:17 PM
**        * FROM NATURAL SUBPROGRAM : Adsn040
************************************************************
**        * FILE NAME            : Adsn040.java
**        * CLASS NAME           : Adsn040
**        * INSTANCE NAME        : Adsn040
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: ACCESS NAZ-TABLE-DDM
**SAG SYSTEM: ANNUITIZATION SUNGARD
************************************************************************
* PROGRAM  : ADSN040
* SYSTEM   : ANNUITIZATION SUNGARD (ADAS)
* TITLE    : ACCESS NAZ-TABLE-DDM
* GENERATED: MAR 10,04
* FUNCTION : THIS SUBPROGRAM READS THE NAZ-TABLE-DDM FILE TO VALIDATE/
*          : OBTAIN FREQUENCY OF PAYMENTS.
* CALLED BY: ADSP040.
*          |
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* --------   -------   ---------------------------------------------
* 3/10/2004  T.SHINE   CLONED NAZN040 FOR ANNUITIZATION SUNGARD (ADAS)
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn040 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAdspda_M pdaAdspda_M;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Data_Passed;
    private DbsField pnd_Annty_Strt_Mm;

    private DbsGroup pnd_Annty_Strt_Mm__R_Field_1;
    private DbsField pnd_Annty_Strt_Mm_Pnd_Annty_Strt_Mm_A;
    private DbsField pnd_Payment_Freq;
    private DbsField pnd_Pymnt_Mde;

    private DbsGroup pnd_Pymnt_Mde__R_Field_2;
    private DbsField pnd_Pymnt_Mde_Pnd_Pymnt_Mde_A;
    private DbsField pnd_Record_Found;
    private DbsField pnd_A;
    private DbsField pnd_B;
    private DbsField pnd_Mode;
    private DbsField pnd_Max_Desc_Occurs;
    private DbsField pnd_W_Desc;

    private DbsGroup pnd_W_Desc__R_Field_3;

    private DbsGroup pnd_W_Desc_Pnd_W_Desc_N;
    private DbsField pnd_W_Desc_Pnd_W_Desc_N1;

    private DbsGroup pnd_W_Desc__R_Field_4;
    private DbsField pnd_W_Desc_Pnd_W_Desc_A1;
    private DbsField pnd_W_Desc_Pnd_W_Desc_N2;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte;
    private DbsField naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id;
    private DbsGroup naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup;
    private DbsField naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt;
    private DbsGroup naz_Table_Ddm_Count_Castnaz_Tbl_Scrn_Fld_CdeMuGroup;
    private DbsField naz_Table_Ddm_Count_Castnaz_Tbl_Scrn_Fld_Cde;
    private DbsGroup naz_Table_Ddm_Naz_Tbl_Scrn_Fld_CdeMuGroup;
    private DbsField naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Cde;
    private DbsGroup naz_Table_Ddm_Naz_Tbl_Scrn_Fld_DscrptnMuGroup;
    private DbsField naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn;
    private DbsField pnd_Naz_Table_Key;

    private DbsGroup pnd_Naz_Table_Key__R_Field_5;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id;
    private DbsField pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaAdspda_M = new PdaAdspda_M(parameters);
        pnd_Data_Passed = parameters.newFieldInRecord("pnd_Data_Passed", "#DATA-PASSED", FieldType.STRING, 1);
        pnd_Data_Passed.setParameterOption(ParameterOption.ByReference);
        pnd_Annty_Strt_Mm = parameters.newFieldInRecord("pnd_Annty_Strt_Mm", "#ANNTY-STRT-MM", FieldType.NUMERIC, 2);
        pnd_Annty_Strt_Mm.setParameterOption(ParameterOption.ByReference);

        pnd_Annty_Strt_Mm__R_Field_1 = parameters.newGroupInRecord("pnd_Annty_Strt_Mm__R_Field_1", "REDEFINE", pnd_Annty_Strt_Mm);
        pnd_Annty_Strt_Mm_Pnd_Annty_Strt_Mm_A = pnd_Annty_Strt_Mm__R_Field_1.newFieldInGroup("pnd_Annty_Strt_Mm_Pnd_Annty_Strt_Mm_A", "#ANNTY-STRT-MM-A", 
            FieldType.STRING, 2);
        pnd_Payment_Freq = parameters.newFieldInRecord("pnd_Payment_Freq", "#PAYMENT-FREQ", FieldType.STRING, 1);
        pnd_Payment_Freq.setParameterOption(ParameterOption.ByReference);
        pnd_Pymnt_Mde = parameters.newFieldInRecord("pnd_Pymnt_Mde", "#PYMNT-MDE", FieldType.NUMERIC, 3);
        pnd_Pymnt_Mde.setParameterOption(ParameterOption.ByReference);

        pnd_Pymnt_Mde__R_Field_2 = parameters.newGroupInRecord("pnd_Pymnt_Mde__R_Field_2", "REDEFINE", pnd_Pymnt_Mde);
        pnd_Pymnt_Mde_Pnd_Pymnt_Mde_A = pnd_Pymnt_Mde__R_Field_2.newFieldInGroup("pnd_Pymnt_Mde_Pnd_Pymnt_Mde_A", "#PYMNT-MDE-A", FieldType.STRING, 3);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Record_Found = localVariables.newFieldInRecord("pnd_Record_Found", "#RECORD-FOUND", FieldType.BOOLEAN, 1);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.PACKED_DECIMAL, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.PACKED_DECIMAL, 3);
        pnd_Mode = localVariables.newFieldInRecord("pnd_Mode", "#MODE", FieldType.STRING, 1);
        pnd_Max_Desc_Occurs = localVariables.newFieldInRecord("pnd_Max_Desc_Occurs", "#MAX-DESC-OCCURS", FieldType.PACKED_DECIMAL, 3);
        pnd_W_Desc = localVariables.newFieldInRecord("pnd_W_Desc", "#W-DESC", FieldType.STRING, 30);

        pnd_W_Desc__R_Field_3 = localVariables.newGroupInRecord("pnd_W_Desc__R_Field_3", "REDEFINE", pnd_W_Desc);

        pnd_W_Desc_Pnd_W_Desc_N = pnd_W_Desc__R_Field_3.newGroupArrayInGroup("pnd_W_Desc_Pnd_W_Desc_N", "#W-DESC-N", new DbsArrayController(1, 10));
        pnd_W_Desc_Pnd_W_Desc_N1 = pnd_W_Desc_Pnd_W_Desc_N.newFieldInGroup("pnd_W_Desc_Pnd_W_Desc_N1", "#W-DESC-N1", FieldType.NUMERIC, 2);

        pnd_W_Desc__R_Field_4 = pnd_W_Desc_Pnd_W_Desc_N.newGroupInGroup("pnd_W_Desc__R_Field_4", "REDEFINE", pnd_W_Desc_Pnd_W_Desc_N1);
        pnd_W_Desc_Pnd_W_Desc_A1 = pnd_W_Desc__R_Field_4.newFieldInGroup("pnd_W_Desc_Pnd_W_Desc_A1", "#W-DESC-A1", FieldType.STRING, 2);
        pnd_W_Desc_Pnd_W_Desc_N2 = pnd_W_Desc_Pnd_W_Desc_N.newFieldInGroup("pnd_W_Desc_Pnd_W_Desc_N2", "#W-DESC-N2", FieldType.STRING, 1);

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte", "NAZ-TBL-RCRD-UPDT-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_UPDT_DTE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Updt_Dte.setDdmHeader("LST UPDT");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id", "NAZ-TBL-UPDT-RACF-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "NAZ_TBL_UPDT_RACF_ID");
        naz_Table_Ddm_Naz_Tbl_Updt_Racf_Id.setDdmHeader("UPDT BY");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup = vw_naz_Table_Ddm.getRecord().newGroupInGroup("NAZ_TABLE_DDM_NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", 
            "NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt = naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup.newFieldArrayInGroup("naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt", 
            "NAZ-TBL-SECNDRY-DSCRPTN-TXT", FieldType.STRING, 80, new DbsArrayController(1, 1), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt.setDdmHeader("CDE/2ND/DSC");
        naz_Table_Ddm_Count_Castnaz_Tbl_Scrn_Fld_Cde = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Count_Castnaz_Tbl_Scrn_Fld_Cde", "C*NAZ-TBL-SCRN-FLD-CDE", 
            RepeatingFieldStrategy.CAsteriskVariable, "NAZ_TABLE_RCRD_NAZ_TBL_SCRN_FLD_CDE");
        naz_Table_Ddm_Naz_Tbl_Scrn_Fld_CdeMuGroup = vw_naz_Table_Ddm.getRecord().newGroupInGroup("NAZ_TABLE_DDM_NAZ_TBL_SCRN_FLD_CDEMuGroup", "NAZ_TBL_SCRN_FLD_CDEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SCRN_FLD_CDE");
        naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Cde = naz_Table_Ddm_Naz_Tbl_Scrn_Fld_CdeMuGroup.newFieldArrayInGroup("naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Cde", "NAZ-TBL-SCRN-FLD-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 100), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SCRN_FLD_CDE");
        naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Cde.setDdmHeader("HDING/CDE");
        naz_Table_Ddm_Naz_Tbl_Scrn_Fld_DscrptnMuGroup = vw_naz_Table_Ddm.getRecord().newGroupInGroup("NAZ_TABLE_DDM_NAZ_TBL_SCRN_FLD_DSCRPTNMuGroup", 
            "NAZ_TBL_SCRN_FLD_DSCRPTNMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SCRN_FLD_DSCRPTN");
        naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn = naz_Table_Ddm_Naz_Tbl_Scrn_Fld_DscrptnMuGroup.newFieldArrayInGroup("naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn", 
            "NAZ-TBL-SCRN-FLD-DSCRPTN", FieldType.STRING, 35, new DbsArrayController(1, 100), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SCRN_FLD_DSCRPTN");
        naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn.setDdmHeader("HD = FIELD");
        registerRecord(vw_naz_Table_Ddm);

        pnd_Naz_Table_Key = localVariables.newFieldInRecord("pnd_Naz_Table_Key", "#NAZ-TABLE-KEY", FieldType.STRING, 29);

        pnd_Naz_Table_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Naz_Table_Key__R_Field_5", "REDEFINE", pnd_Naz_Table_Key);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id = pnd_Naz_Table_Key__R_Field_5.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id", "#NAZ-TABLE-LVL1-ID", 
            FieldType.STRING, 6);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id = pnd_Naz_Table_Key__R_Field_5.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id", "#NAZ-TABLE-LVL2-ID", 
            FieldType.STRING, 3);
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id = pnd_Naz_Table_Key__R_Field_5.newFieldInGroup("pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id", "#NAZ-TABLE-LVL3-ID", 
            FieldType.STRING, 20);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_naz_Table_Ddm.reset();

        parameters.reset();
        localVariables.reset();
        pnd_Max_Desc_Occurs.setInitialValue(10);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn040() throws Exception
    {
        super("Adsn040");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pdaAdspda_M.getMsg_Info_Sub().reset();                                                                                                                            //Natural: RESET MSG-INFO-SUB
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id.setValue("NAZ009");                                                                                                       //Natural: ASSIGN #NAZ-TABLE-LVL1-ID := 'NAZ009'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id.setValue("FP");                                                                                                           //Natural: ASSIGN #NAZ-TABLE-LVL2-ID := 'FP'
        pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl3_Id.setValue("A");                                                                                                            //Natural: ASSIGN #NAZ-TABLE-LVL3-ID := 'A'
        if (condition(pnd_Data_Passed.equals("1")))                                                                                                                       //Natural: IF #DATA-PASSED EQ '1'
        {
                                                                                                                                                                          //Natural: PERFORM GET-PAYMENT-MODE
            sub_Get_Payment_Mode();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Data_Passed.equals("3")))                                                                                                                   //Natural: IF #DATA-PASSED EQ '3'
            {
                                                                                                                                                                          //Natural: PERFORM GET-FREQUENCY
                sub_Get_Frequency();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                          //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
                pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Invalid data", pnd_Data_Passed, "passed"));                                          //Natural: COMPRESS 'Invalid data' #DATA-PASSED 'passed' INTO MSG-INFO-SUB.##MSG
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-FREQUENCY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PAYMENT-MODE
    }
    private void sub_Get_Frequency() throws Exception                                                                                                                     //Natural: GET-FREQUENCY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_naz_Table_Ddm.startDatabaseRead                                                                                                                                //Natural: READ NAZ-TABLE-DDM BY NAZ-TBL-SUPER1 STARTING FROM #NAZ-TABLE-KEY
        (
        "READ",
        new Wc[] { new Wc("NAZ_TBL_SUPER1", ">=", pnd_Naz_Table_Key, WcType.BY) },
        new Oc[] { new Oc("NAZ_TBL_SUPER1", "ASC") }
        );
        READ:
        while (condition(vw_naz_Table_Ddm.readNextRow("READ")))
        {
            if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id) || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id))) //Natural: IF NAZ-TABLE-DDM.NAZ-TBL-RCRD-LVL1-ID NE #NAZ-TABLE-LVL1-ID OR NAZ-TABLE-DDM.NAZ-TBL-RCRD-LVL2-ID NE #NAZ-TABLE-LVL2-ID
            {
                if (true) break READ;                                                                                                                                     //Natural: ESCAPE BOTTOM ( READ. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.equals("Y") && naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.equals("C"))))                                          //Natural: ACCEPT IF NAZ-TBL-RCRD-ACTV-IND EQ 'Y' AND NAZ-TBL-RCRD-TYP-IND EQ 'C'
            {
                continue;
            }
            if (condition(naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt.getValue(1).getSubstring(1,1).equals(pnd_Pymnt_Mde_Pnd_Pymnt_Mde_A.getSubstring(1,                    //Natural: IF SUBSTR ( NAZ-TBL-SECNDRY-DSCRPTN-TXT ( 1 ) ,1,1 ) EQ SUBSTR ( #PYMNT-MDE-A,1,1 )
                1))))
            {
                pnd_Payment_Freq.setValue(naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.getSubstring(1,1));                                                                          //Natural: ASSIGN #PAYMENT-FREQ := SUBSTR ( NAZ-TBL-RCRD-LVL3-ID,1,1 )
                pnd_Record_Found.setValue(true);                                                                                                                          //Natural: ASSIGN #RECORD-FOUND := TRUE
                if (true) break READ;                                                                                                                                     //Natural: ESCAPE BOTTOM ( READ. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Record_Found.getBoolean()))                                                                                                                     //Natural: IF #RECORD-FOUND
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
            pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Invalid payment mode", pnd_Pymnt_Mde_Pnd_Pymnt_Mde_A));                                  //Natural: COMPRESS 'Invalid payment mode' #PYMNT-MDE-A INTO MSG-INFO-SUB.##MSG
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Payment_Mode() throws Exception                                                                                                                  //Natural: GET-PAYMENT-MODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_naz_Table_Ddm.startDatabaseRead                                                                                                                                //Natural: READ NAZ-TABLE-DDM BY NAZ-TBL-SUPER1 STARTING FROM #NAZ-TABLE-KEY
        (
        "READ2",
        new Wc[] { new Wc("NAZ_TBL_SUPER1", ">=", pnd_Naz_Table_Key, WcType.BY) },
        new Oc[] { new Oc("NAZ_TBL_SUPER1", "ASC") }
        );
        READ2:
        while (condition(vw_naz_Table_Ddm.readNextRow("READ2")))
        {
            if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl1_Id) || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.notEquals(pnd_Naz_Table_Key_Pnd_Naz_Table_Lvl2_Id))) //Natural: IF NAZ-TABLE-DDM.NAZ-TBL-RCRD-LVL1-ID NE #NAZ-TABLE-LVL1-ID OR NAZ-TABLE-DDM.NAZ-TBL-RCRD-LVL2-ID NE #NAZ-TABLE-LVL2-ID
            {
                if (true) break READ2;                                                                                                                                    //Natural: ESCAPE BOTTOM ( READ2. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.equals("Y") && naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.equals("C"))))                                          //Natural: ACCEPT IF NAZ-TBL-RCRD-ACTV-IND EQ 'Y' AND NAZ-TBL-RCRD-TYP-IND EQ 'C'
            {
                continue;
            }
            if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.getSubstring(1,1).equals(pnd_Payment_Freq)))                                                                 //Natural: IF SUBSTR ( NAZ-TBL-RCRD-LVL3-ID,1,1 ) EQ #PAYMENT-FREQ
            {
                pnd_Mode.setValue(naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt.getValue(1).getSubstring(1,1));                                                               //Natural: ASSIGN #MODE := SUBSTR ( NAZ-TBL-SECNDRY-DSCRPTN-TXT ( 1 ) ,1,1 )
                FOR1:                                                                                                                                                     //Natural: FOR #A 1 C*NAZ-TBL-SCRN-FLD-CDE
                for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(naz_Table_Ddm_Count_Castnaz_Tbl_Scrn_Fld_Cde)); pnd_A.nadd(1))
                {
                    if (condition(naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Cde.getValue(pnd_A).equals(" ")))                                                                        //Natural: IF NAZ-TBL-SCRN-FLD-CDE ( #A ) EQ ' '
                    {
                        if (true) break READ2;                                                                                                                            //Natural: ESCAPE BOTTOM ( READ2. )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Payment_Freq.equals("M")))                                                                                                          //Natural: IF #PAYMENT-FREQ EQ 'M'
                    {
                        pnd_Pymnt_Mde_Pnd_Pymnt_Mde_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Mode, naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Cde.getValue(pnd_A))); //Natural: COMPRESS #MODE NAZ-TBL-SCRN-FLD-CDE ( #A ) INTO #PYMNT-MDE-A LEAVING NO
                        pnd_Record_Found.setValue(true);                                                                                                                  //Natural: ASSIGN #RECORD-FOUND := TRUE
                        if (true) break READ2;                                                                                                                            //Natural: ESCAPE BOTTOM ( READ2. )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Payment_Freq.equals("A")))                                                                                                          //Natural: IF #PAYMENT-FREQ EQ 'A'
                    {
                        if (condition(pnd_Annty_Strt_Mm_Pnd_Annty_Strt_Mm_A.equals(naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Cde.getValue(pnd_A))))                                  //Natural: IF #ANNTY-STRT-MM-A EQ NAZ-TBL-SCRN-FLD-CDE ( #A )
                        {
                            pnd_Pymnt_Mde_Pnd_Pymnt_Mde_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Mode, naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Cde.getValue(pnd_A))); //Natural: COMPRESS #MODE NAZ-TBL-SCRN-FLD-CDE ( #A ) INTO #PYMNT-MDE-A LEAVING NO
                            pnd_Record_Found.setValue(true);                                                                                                              //Natural: ASSIGN #RECORD-FOUND := TRUE
                            if (true) break READ2;                                                                                                                        //Natural: ESCAPE BOTTOM ( READ2. )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_W_Desc.setValue(naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn.getValue(pnd_A));                                                                          //Natural: ASSIGN #W-DESC := NAZ-TBL-SCRN-FLD-DSCRPTN ( #A )
                    FOR2:                                                                                                                                                 //Natural: FOR #B 1 #MAX-DESC-OCCURS
                    for (pnd_B.setValue(1); condition(pnd_B.lessOrEqual(pnd_Max_Desc_Occurs)); pnd_B.nadd(1))
                    {
                        if (condition(! (DbsUtil.maskMatches(pnd_W_Desc_Pnd_W_Desc_A1.getValue(pnd_B),"NN"))))                                                            //Natural: IF #W-DESC-A1 ( #B ) NE MASK ( NN )
                        {
                            if (true) break FOR2;                                                                                                                         //Natural: ESCAPE BOTTOM ( FOR2. )
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_W_Desc_Pnd_W_Desc_N1.getValue(pnd_B).equals(pnd_Annty_Strt_Mm)))                                                                //Natural: IF #W-DESC-N1 ( #B ) EQ #ANNTY-STRT-MM
                        {
                            pnd_Pymnt_Mde_Pnd_Pymnt_Mde_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Mode, naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Cde.getValue(pnd_A))); //Natural: COMPRESS #MODE NAZ-TBL-SCRN-FLD-CDE ( #A ) INTO #PYMNT-MDE-A LEAVING NO
                            pnd_Record_Found.setValue(true);                                                                                                              //Natural: ASSIGN #RECORD-FOUND := TRUE
                            if (true) break READ2;                                                                                                                        //Natural: ESCAPE BOTTOM ( READ2. )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FOR1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FOR1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Record_Found.getBoolean()))                                                                                                                     //Natural: IF #RECORD-FOUND
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
            pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Invalid frequency of payments"));                                                        //Natural: COMPRESS 'Invalid frequency of payments' INTO MSG-INFO-SUB.##MSG
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
