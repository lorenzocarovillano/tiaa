/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:54:24 PM
**        * FROM NATURAL SUBPROGRAM : Adsn480
************************************************************
**        * FILE NAME            : Adsn480.java
**        * CLASS NAME           : Adsn480
**        * INSTANCE NAME        : Adsn480
************************************************************
***********************************************************************
* PROGRAM  : ADSN480
* SYSTEM   : ADAS - ANNUITIZATION SUNGUARD
* TITLE    : ADAS BATCH - MIT UPDATE
* GENERATED: MARCH 16, 2004
* AUTHOR   : EDWINA VILLANUEVA
* FUNCTION : THIS PROGRAM IS CLONED FROM THE ORIGINAL ADAM BATCH PROGRAM
*          : ADSN480 (PLS REFER TO ADSN480 FOR ORIGINAL HISTORY
*          : REVISIONS). IT UPDATES MIT RECORDS THAT WERE INITIALLY
*          : CREATED BY ADAS ONLINE REQUEST. ALSO, ADDITIONAL CODES WERE
*          : INCLUDED TO BE ABLE TO LOG AND INDEX MIT REQUESTS IN CWF
*          : AND CREATE MIT SUB-REQUESTS FOR RESETTLEMENTS.
*          :
***********************  MAINTENANCE LOG ******************************
*
*  D A T E   PROGRAMMER     D E S C R I P T I O N
*
* 07-16-04   E.VILLANUEVA   INCLUDE PLAN-NO TO AUDIT TRAIL MESSAGE
* 02/21/17   R.CARREON      RESTOW FOR PIN EXPANSION 02212017
*                           CHANGE IN EFSA9120
*
***********************************************************************

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn480 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAdsa480 pdaAdsa480;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfa8000 pdaCwfa8000;
    private PdaEfsa9120 pdaEfsa9120;
    private PdaEfsa902r pdaEfsa902r;
    private PdaIefa9000 pdaIefa9000;
    private PdaCdaobj pdaCdaobj;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Adp_Annty_Strt_Dte;

    private DbsGroup pnd_Adp_Annty_Strt_Dte__R_Field_1;
    private DbsField pnd_Adp_Annty_Strt_Dte_Pnd_Adp_Annty_Strt_Dte_A;
    private DbsField pnd_Accntng_Dte_A;
    private DbsField pnd_Mit_Msg_Redef;

    private DbsGroup pnd_Mit_Msg_Redef__R_Field_2;
    private DbsField pnd_Mit_Msg_Redef_Pnd_Mit_Msg13;
    private DbsField pnd_Mit_Msg_Redef_Pnd_Mit_Msg66;

    private DataAccessProgramView vw_tblrecrd;
    private DbsField tblrecrd_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField tblrecrd_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField tblrecrd_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsGroup tblrecrd_Naz_Tbl_Scrn_Fld_DscrptnMuGroup;
    private DbsField tblrecrd_Naz_Tbl_Scrn_Fld_Dscrptn;

    private DbsGroup tblrecrd__R_Field_3;
    private DbsField tblrecrd_Naz_Tbl_Scrn_Fld_Dscrptn_A60;
    private DbsGroup tblrecrd_Naz_Tbl_Scrn_Fld_CdeMuGroup;
    private DbsField tblrecrd_Naz_Tbl_Scrn_Fld_Cde;
    private DbsField tblrecrd_Naz_Tbl_Rcrd_Dscrptn_Txt;

    private DbsGroup tblrecrd__R_Field_4;
    private DbsField tblrecrd_Pnd_Tbl_Rcrd_Wpid;

    private DbsGroup tblrecrd__R_Field_5;
    private DbsField tblrecrd_Pnd_Tbl_Rcrd_Unit_Id;

    private DbsGroup tblrecrd__R_Field_6;
    private DbsField tblrecrd_Pnd_Tbl_Rcrd_Mit_Stts;
    private DbsField tblrecrd_Pnd_Dscrptn_Txt_Filler_2;
    private DbsField tblrecrd_Pnd_Tbl_Rcrd_Unit;
    private DbsField tblrecrd_Pnd_Dscrptn_Txt_Filler_3;
    private DbsField tblrecrd_Pnd_Tbl_Rcrd_Racf;
    private DbsField tblrecrd_Pnd_Dscrptn_Txt_Filler_4;
    private DbsField tblrecrd_Pnd_Tbl_Rcrd_Wpid_Switch;
    private DbsField tblrecrd_Pnd_Dscrptn_Txt_Filler_5;
    private DbsField tblrecrd_Pnd_Tbl_Rcrd_Work_Step;
    private DbsField tblrecrd_Pnd_Dscrptn_Txt_Filler_6;
    private DbsField pnd_Sd1;

    private DbsGroup pnd_Sd1__R_Field_7;
    private DbsField pnd_Sd1_Pnd_Sd1_Lvl1;
    private DbsField pnd_Sd1_Pnd_Sd1_Lvl2;
    private DbsField pnd_Sd1_Pnd_Sd1_Lvl3;

    private DbsGroup pnd_Sd1__R_Field_8;
    private DbsField pnd_Sd1_Pnd_Lvl_3_Stts;
    private DbsField pnd_Sd1_Pnd_Filler;
    private DbsField pnd_Add_Action;
    private DbsField pnd_Update_Action;
    private DbsField pnd_Create_Folder;
    private DbsField pnd_Create_Subrqst;
    private DbsField pnd_Subrqst_Doc_Text;
    private DbsField pnd_X;
    private DbsField pnd_Add_Mit;
    private DbsField pnd_I;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfa8000 = new PdaCwfa8000(localVariables);
        pdaEfsa9120 = new PdaEfsa9120(localVariables);
        pdaEfsa902r = new PdaEfsa902r(localVariables);
        pdaIefa9000 = new PdaIefa9000(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaAdsa480 = new PdaAdsa480(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Adp_Annty_Strt_Dte = localVariables.newFieldInRecord("pnd_Adp_Annty_Strt_Dte", "#ADP-ANNTY-STRT-DTE", FieldType.NUMERIC, 8);

        pnd_Adp_Annty_Strt_Dte__R_Field_1 = localVariables.newGroupInRecord("pnd_Adp_Annty_Strt_Dte__R_Field_1", "REDEFINE", pnd_Adp_Annty_Strt_Dte);
        pnd_Adp_Annty_Strt_Dte_Pnd_Adp_Annty_Strt_Dte_A = pnd_Adp_Annty_Strt_Dte__R_Field_1.newFieldInGroup("pnd_Adp_Annty_Strt_Dte_Pnd_Adp_Annty_Strt_Dte_A", 
            "#ADP-ANNTY-STRT-DTE-A", FieldType.STRING, 8);
        pnd_Accntng_Dte_A = localVariables.newFieldInRecord("pnd_Accntng_Dte_A", "#ACCNTNG-DTE-A", FieldType.STRING, 8);
        pnd_Mit_Msg_Redef = localVariables.newFieldInRecord("pnd_Mit_Msg_Redef", "#MIT-MSG-REDEF", FieldType.STRING, 79);

        pnd_Mit_Msg_Redef__R_Field_2 = localVariables.newGroupInRecord("pnd_Mit_Msg_Redef__R_Field_2", "REDEFINE", pnd_Mit_Msg_Redef);
        pnd_Mit_Msg_Redef_Pnd_Mit_Msg13 = pnd_Mit_Msg_Redef__R_Field_2.newFieldInGroup("pnd_Mit_Msg_Redef_Pnd_Mit_Msg13", "#MIT-MSG13", FieldType.STRING, 
            13);
        pnd_Mit_Msg_Redef_Pnd_Mit_Msg66 = pnd_Mit_Msg_Redef__R_Field_2.newFieldInGroup("pnd_Mit_Msg_Redef_Pnd_Mit_Msg66", "#MIT-MSG66", FieldType.STRING, 
            66);

        vw_tblrecrd = new DataAccessProgramView(new NameInfo("vw_tblrecrd", "TBLRECRD"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        tblrecrd_Naz_Tbl_Rcrd_Lvl1_Id = vw_tblrecrd.getRecord().newFieldInGroup("tblrecrd_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        tblrecrd_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        tblrecrd_Naz_Tbl_Rcrd_Lvl2_Id = vw_tblrecrd.getRecord().newFieldInGroup("tblrecrd_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        tblrecrd_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        tblrecrd_Naz_Tbl_Rcrd_Lvl3_Id = vw_tblrecrd.getRecord().newFieldInGroup("tblrecrd_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", FieldType.STRING, 
            20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        tblrecrd_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        tblrecrd_Naz_Tbl_Scrn_Fld_DscrptnMuGroup = vw_tblrecrd.getRecord().newGroupInGroup("TBLRECRD_NAZ_TBL_SCRN_FLD_DSCRPTNMuGroup", "NAZ_TBL_SCRN_FLD_DSCRPTNMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SCRN_FLD_DSCRPTN");
        tblrecrd_Naz_Tbl_Scrn_Fld_Dscrptn = tblrecrd_Naz_Tbl_Scrn_Fld_DscrptnMuGroup.newFieldArrayInGroup("tblrecrd_Naz_Tbl_Scrn_Fld_Dscrptn", "NAZ-TBL-SCRN-FLD-DSCRPTN", 
            FieldType.STRING, 35, new DbsArrayController(1, 100), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SCRN_FLD_DSCRPTN");
        tblrecrd_Naz_Tbl_Scrn_Fld_Dscrptn.setDdmHeader("HD = FIELD");

        tblrecrd__R_Field_3 = vw_tblrecrd.getRecord().newGroupInGroup("tblrecrd__R_Field_3", "REDEFINE", tblrecrd_Naz_Tbl_Scrn_Fld_Dscrptn);
        tblrecrd_Naz_Tbl_Scrn_Fld_Dscrptn_A60 = tblrecrd__R_Field_3.newFieldArrayInGroup("tblrecrd_Naz_Tbl_Scrn_Fld_Dscrptn_A60", "NAZ-TBL-SCRN-FLD-DSCRPTN-A60", 
            FieldType.STRING, 60, new DbsArrayController(1, 50));
        tblrecrd_Naz_Tbl_Scrn_Fld_CdeMuGroup = vw_tblrecrd.getRecord().newGroupInGroup("TBLRECRD_NAZ_TBL_SCRN_FLD_CDEMuGroup", "NAZ_TBL_SCRN_FLD_CDEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SCRN_FLD_CDE");
        tblrecrd_Naz_Tbl_Scrn_Fld_Cde = tblrecrd_Naz_Tbl_Scrn_Fld_CdeMuGroup.newFieldArrayInGroup("tblrecrd_Naz_Tbl_Scrn_Fld_Cde", "NAZ-TBL-SCRN-FLD-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 100), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SCRN_FLD_CDE");
        tblrecrd_Naz_Tbl_Scrn_Fld_Cde.setDdmHeader("HDING/CDE");
        tblrecrd_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_tblrecrd.getRecord().newFieldInGroup("tblrecrd_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        tblrecrd_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");

        tblrecrd__R_Field_4 = vw_tblrecrd.getRecord().newGroupInGroup("tblrecrd__R_Field_4", "REDEFINE", tblrecrd_Naz_Tbl_Rcrd_Dscrptn_Txt);
        tblrecrd_Pnd_Tbl_Rcrd_Wpid = tblrecrd__R_Field_4.newFieldInGroup("tblrecrd_Pnd_Tbl_Rcrd_Wpid", "#TBL-RCRD-WPID", FieldType.STRING, 6);

        tblrecrd__R_Field_5 = vw_tblrecrd.getRecord().newGroupInGroup("tblrecrd__R_Field_5", "REDEFINE", tblrecrd_Naz_Tbl_Rcrd_Dscrptn_Txt);
        tblrecrd_Pnd_Tbl_Rcrd_Unit_Id = tblrecrd__R_Field_5.newFieldInGroup("tblrecrd_Pnd_Tbl_Rcrd_Unit_Id", "#TBL-RCRD-UNIT-ID", FieldType.STRING, 8);

        tblrecrd__R_Field_6 = vw_tblrecrd.getRecord().newGroupInGroup("tblrecrd__R_Field_6", "REDEFINE", tblrecrd_Naz_Tbl_Rcrd_Dscrptn_Txt);
        tblrecrd_Pnd_Tbl_Rcrd_Mit_Stts = tblrecrd__R_Field_6.newFieldInGroup("tblrecrd_Pnd_Tbl_Rcrd_Mit_Stts", "#TBL-RCRD-MIT-STTS", FieldType.NUMERIC, 
            4);
        tblrecrd_Pnd_Dscrptn_Txt_Filler_2 = tblrecrd__R_Field_6.newFieldInGroup("tblrecrd_Pnd_Dscrptn_Txt_Filler_2", "#DSCRPTN-TXT-FILLER-2", FieldType.STRING, 
            1);
        tblrecrd_Pnd_Tbl_Rcrd_Unit = tblrecrd__R_Field_6.newFieldInGroup("tblrecrd_Pnd_Tbl_Rcrd_Unit", "#TBL-RCRD-UNIT", FieldType.STRING, 6);
        tblrecrd_Pnd_Dscrptn_Txt_Filler_3 = tblrecrd__R_Field_6.newFieldInGroup("tblrecrd_Pnd_Dscrptn_Txt_Filler_3", "#DSCRPTN-TXT-FILLER-3", FieldType.STRING, 
            1);
        tblrecrd_Pnd_Tbl_Rcrd_Racf = tblrecrd__R_Field_6.newFieldInGroup("tblrecrd_Pnd_Tbl_Rcrd_Racf", "#TBL-RCRD-RACF", FieldType.STRING, 8);
        tblrecrd_Pnd_Dscrptn_Txt_Filler_4 = tblrecrd__R_Field_6.newFieldInGroup("tblrecrd_Pnd_Dscrptn_Txt_Filler_4", "#DSCRPTN-TXT-FILLER-4", FieldType.STRING, 
            1);
        tblrecrd_Pnd_Tbl_Rcrd_Wpid_Switch = tblrecrd__R_Field_6.newFieldInGroup("tblrecrd_Pnd_Tbl_Rcrd_Wpid_Switch", "#TBL-RCRD-WPID-SWITCH", FieldType.STRING, 
            1);
        tblrecrd_Pnd_Dscrptn_Txt_Filler_5 = tblrecrd__R_Field_6.newFieldInGroup("tblrecrd_Pnd_Dscrptn_Txt_Filler_5", "#DSCRPTN-TXT-FILLER-5", FieldType.STRING, 
            1);
        tblrecrd_Pnd_Tbl_Rcrd_Work_Step = tblrecrd__R_Field_6.newFieldInGroup("tblrecrd_Pnd_Tbl_Rcrd_Work_Step", "#TBL-RCRD-WORK-STEP", FieldType.STRING, 
            6);
        tblrecrd_Pnd_Dscrptn_Txt_Filler_6 = tblrecrd__R_Field_6.newFieldInGroup("tblrecrd_Pnd_Dscrptn_Txt_Filler_6", "#DSCRPTN-TXT-FILLER-6", FieldType.STRING, 
            31);
        registerRecord(vw_tblrecrd);

        pnd_Sd1 = localVariables.newFieldInRecord("pnd_Sd1", "#SD1", FieldType.STRING, 29);

        pnd_Sd1__R_Field_7 = localVariables.newGroupInRecord("pnd_Sd1__R_Field_7", "REDEFINE", pnd_Sd1);
        pnd_Sd1_Pnd_Sd1_Lvl1 = pnd_Sd1__R_Field_7.newFieldInGroup("pnd_Sd1_Pnd_Sd1_Lvl1", "#SD1-LVL1", FieldType.STRING, 6);
        pnd_Sd1_Pnd_Sd1_Lvl2 = pnd_Sd1__R_Field_7.newFieldInGroup("pnd_Sd1_Pnd_Sd1_Lvl2", "#SD1-LVL2", FieldType.STRING, 3);
        pnd_Sd1_Pnd_Sd1_Lvl3 = pnd_Sd1__R_Field_7.newFieldInGroup("pnd_Sd1_Pnd_Sd1_Lvl3", "#SD1-LVL3", FieldType.STRING, 20);

        pnd_Sd1__R_Field_8 = pnd_Sd1__R_Field_7.newGroupInGroup("pnd_Sd1__R_Field_8", "REDEFINE", pnd_Sd1_Pnd_Sd1_Lvl3);
        pnd_Sd1_Pnd_Lvl_3_Stts = pnd_Sd1__R_Field_8.newFieldInGroup("pnd_Sd1_Pnd_Lvl_3_Stts", "#LVL-3-STTS", FieldType.STRING, 3);
        pnd_Sd1_Pnd_Filler = pnd_Sd1__R_Field_8.newFieldInGroup("pnd_Sd1_Pnd_Filler", "#FILLER", FieldType.STRING, 17);
        pnd_Add_Action = localVariables.newFieldInRecord("pnd_Add_Action", "#ADD-ACTION", FieldType.STRING, 2);
        pnd_Update_Action = localVariables.newFieldInRecord("pnd_Update_Action", "#UPDATE-ACTION", FieldType.STRING, 2);
        pnd_Create_Folder = localVariables.newFieldInRecord("pnd_Create_Folder", "#CREATE-FOLDER", FieldType.STRING, 2);
        pnd_Create_Subrqst = localVariables.newFieldInRecord("pnd_Create_Subrqst", "#CREATE-SUBRQST", FieldType.STRING, 2);
        pnd_Subrqst_Doc_Text = localVariables.newFieldInRecord("pnd_Subrqst_Doc_Text", "#SUBRQST-DOC-TEXT", FieldType.STRING, 80);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 8);
        pnd_Add_Mit = localVariables.newFieldInRecord("pnd_Add_Mit", "#ADD-MIT", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_tblrecrd.reset();

        localVariables.reset();
        pnd_Add_Action.setInitialValue("AR");
        pnd_Update_Action.setInitialValue("MO");
        pnd_Create_Folder.setInitialValue("AD");
        pnd_Create_Subrqst.setInitialValue("AS");
        pnd_Subrqst_Doc_Text.setInitialValue("THIS IS A RETIREMENT RESETTLEMENT FOR FINAL PREMIUMS");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn480() throws Exception
    {
        super("Adsn480");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Mit_Msg_Redef.setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Msg());                                                                                   //Natural: ASSIGN #MIT-MSG-REDEF = #MIT-MSG
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().reset();                                                                                                                //Natural: RESET ##MSG ##MSG-NR CWFA8000 EFSA9120
        pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().reset();
        pdaCwfa8000.getCwfa8000().reset();
        pdaEfsa9120.getEfsa9120().reset();
        if (condition(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Debug_On().getBoolean()))                                                                              //Natural: IF #MIT-DEBUG-ON
        {
            getReports().write(0, "----- BEGIN MIT UPDATE ADSN480 -----");                                                                                                //Natural: WRITE '----- BEGIN MIT UPDATE ADSN480 -----'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET WPID FROM NAZ TABLE
        if (condition(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Wpid().equals(" ")))                                                                                       //Natural: IF ADP-WPID EQ ' '
        {
            pnd_Sd1.reset();                                                                                                                                              //Natural: RESET #SD1
            pnd_Sd1_Pnd_Sd1_Lvl1.setValue("NAZ064");                                                                                                                      //Natural: ASSIGN #SD1-LVL1 := 'NAZ064'
            pnd_Sd1_Pnd_Sd1_Lvl2.setValue("ADS");                                                                                                                         //Natural: ASSIGN #SD1-LVL2 := 'ADS'
            vw_tblrecrd.startDatabaseRead                                                                                                                                 //Natural: READ TBLRECRD WITH NAZ-TBL-SUPER1 = #SD1
            (
            "PND_PND_L1000",
            new Wc[] { new Wc("NAZ_TBL_SUPER1", ">=", pnd_Sd1, WcType.BY) },
            new Oc[] { new Oc("NAZ_TBL_SUPER1", "ASC") }
            );
            PND_PND_L1000:
            while (condition(vw_tblrecrd.readNextRow("PND_PND_L1000")))
            {
                if (condition(tblrecrd_Naz_Tbl_Rcrd_Lvl1_Id.notEquals(pnd_Sd1_Pnd_Sd1_Lvl1) || tblrecrd_Naz_Tbl_Rcrd_Lvl2_Id.notEquals(pnd_Sd1_Pnd_Sd1_Lvl2)))            //Natural: IF NAZ-TBL-RCRD-LVL1-ID NE #SD1-LVL1 OR NAZ-TBL-RCRD-LVL2-ID NE #SD1-LVL2
                {
                    if (true) break PND_PND_L1000;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L1000. )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(tblrecrd_Naz_Tbl_Scrn_Fld_Cde.getValue(1).getSubstring(2,1).equals(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Annt_Typ_Cde())))               //Natural: IF SUBSTRING ( NAZ-TBL-SCRN-FLD-CDE ( 1 ) ,2,1 ) EQ ADP-ANNT-TYP-CDE
                {
                    pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Wpid().setValue(tblrecrd_Pnd_Tbl_Rcrd_Wpid);                                                                 //Natural: MOVE #TBL-RCRD-WPID TO ADP-WPID
                    if (true) break PND_PND_L1000;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L1000. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            if (condition(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Debug_On().getBoolean()))                                                                          //Natural: IF #MIT-DEBUG-ON
            {
                getReports().write(0, "SCRN-FLD",tblrecrd_Naz_Tbl_Scrn_Fld_Cde.getValue(1),"=",pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Annt_Typ_Cde(),                   //Natural: WRITE 'SCRN-FLD' NAZ-TBL-SCRN-FLD-CDE ( 1 ) '=' ADP-ANNT-TYP-CDE '=' ADP-WPID
                    "=",pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Wpid());
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Wpid().equals(" ")))                                                                                   //Natural: IF ADP-WPID EQ ' '
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("NO WPID FOUND IN NAZ-TABLE");                                                                         //Natural: ASSIGN ##MSG = 'NO WPID FOUND IN NAZ-TABLE'
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(720);                                                                                               //Natural: ASSIGN ##MSG-NR = 720
                pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Msg().setValue(DbsUtil.compress(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg()));                                 //Natural: COMPRESS ##MSG INTO #MIT-MSG
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET UNIT FROM NAZ TABLE
        if (condition(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Bps_Unit().equals(" ")))                                                                                   //Natural: IF ADP-BPS-UNIT EQ ' '
        {
            pnd_Sd1.reset();                                                                                                                                              //Natural: RESET #SD1
            pnd_Sd1_Pnd_Sd1_Lvl1.setValue("NAZ065");                                                                                                                      //Natural: ASSIGN #SD1-LVL1 := 'NAZ065'
            pnd_Sd1_Pnd_Sd1_Lvl2.setValue("ADS");                                                                                                                         //Natural: ASSIGN #SD1-LVL2 := 'ADS'
            vw_tblrecrd.startDatabaseRead                                                                                                                                 //Natural: READ TBLRECRD WITH NAZ-TBL-SUPER1 = #SD1
            (
            "PND_PND_L1280",
            new Wc[] { new Wc("NAZ_TBL_SUPER1", ">=", pnd_Sd1, WcType.BY) },
            new Oc[] { new Oc("NAZ_TBL_SUPER1", "ASC") }
            );
            PND_PND_L1280:
            while (condition(vw_tblrecrd.readNextRow("PND_PND_L1280")))
            {
                if (condition(tblrecrd_Naz_Tbl_Rcrd_Lvl1_Id.notEquals(pnd_Sd1_Pnd_Sd1_Lvl1) || tblrecrd_Naz_Tbl_Rcrd_Lvl2_Id.notEquals(pnd_Sd1_Pnd_Sd1_Lvl2)))            //Natural: IF NAZ-TBL-RCRD-LVL1-ID NE #SD1-LVL1 OR NAZ-TBL-RCRD-LVL2-ID NE #SD1-LVL2
                {
                    if (true) break PND_PND_L1280;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L1280. )
                }                                                                                                                                                         //Natural: END-IF
                pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Bps_Unit().setValue(tblrecrd_Pnd_Tbl_Rcrd_Unit_Id);                                                              //Natural: MOVE #TBL-RCRD-UNIT-ID TO ADP-BPS-UNIT
                if (condition(vw_tblrecrd.getAstCOUNTER().greater(1)))                                                                                                    //Natural: IF *COUNTER ( ##L1280. ) GT 1
                {
                    if (true) break PND_PND_L1280;                                                                                                                        //Natural: ESCAPE BOTTOM ( ##L1280. )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            if (condition(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Bps_Unit().equals(" ")))                                                                               //Natural: IF ADP-BPS-UNIT EQ ' '
            {
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("NO UNIT-ID FOUND IN NAZ-TABLE");                                                                      //Natural: ASSIGN ##MSG = 'NO UNIT-ID FOUND IN NAZ-TABLE'
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(730);                                                                                               //Natural: ASSIGN ##MSG-NR = 730
                pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Msg().setValue(DbsUtil.compress(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg()));                                 //Natural: COMPRESS ##MSG INTO #MIT-MSG
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Sd1.reset();                                                                                                                                                  //Natural: RESET #SD1
        if (condition(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Stts_Cde().notEquals("000")))                                                                              //Natural: IF ADP-STTS-CDE NE '000'
        {
            pnd_Sd1_Pnd_Sd1_Lvl1.setValue("NAZ063");                                                                                                                      //Natural: ASSIGN #SD1-LVL1 := 'NAZ063'
            pnd_Sd1_Pnd_Sd1_Lvl2.setValue("MIT");                                                                                                                         //Natural: ASSIGN #SD1-LVL2 := 'MIT'
            pnd_Sd1_Pnd_Lvl_3_Stts.setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Stts_Cde());                                                                         //Natural: ASSIGN #LVL-3-STTS := ADP-STTS-CDE
            vw_tblrecrd.startDatabaseFind                                                                                                                                 //Natural: FIND TBLRECRD WITH NAZ-TBL-SUPER1 = #SD1
            (
            "FIND01",
            new Wc[] { new Wc("NAZ_TBL_SUPER1", "=", pnd_Sd1, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_tblrecrd.readNextRow("FIND01", true)))
            {
                vw_tblrecrd.setIfNotFoundControlFlag(false);
                if (condition(vw_tblrecrd.getAstCOUNTER().equals(0)))                                                                                                     //Natural: IF NO RECORD FOUND
                {
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue("STATUS NOT FOUND");                                                                               //Natural: ASSIGN ##MSG = 'STATUS NOT FOUND'
                    pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr().setValue(710);                                                                                           //Natural: ASSIGN ##MSG-NR = 710
                    pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Msg().setValue(DbsUtil.compress(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg(), pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Stts_Cde())); //Natural: COMPRESS ##MSG ADP-STTS-CDE INTO #MIT-MSG
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-NOREC
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet761 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #ACTION-CDE;//Natural: VALUE #ADD-ACTION
        if (condition((pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Action_Cde().equals(pnd_Add_Action))))
        {
            decideConditionsMet761++;
            pnd_Add_Mit.setValue(true);                                                                                                                                   //Natural: MOVE TRUE TO #ADD-MIT
                                                                                                                                                                          //Natural: PERFORM ADD-MIT-REQUEST
            sub_Add_Mit_Request();
            if (condition(Global.isEscape())) {return;}
            if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().equals(" ")))                                                                                         //Natural: IF ##MSG EQ ' '
            {
                pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Action_Cde().setValue(pnd_Update_Action);                                                                        //Natural: MOVE #UPDATE-ACTION TO #ACTION-CDE
                                                                                                                                                                          //Natural: PERFORM UPDATE-MIT-REQUEST
                sub_Update_Mit_Request();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE #UPDATE-ACTION
        else if (condition((pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Action_Cde().equals(pnd_Update_Action))))
        {
            decideConditionsMet761++;
                                                                                                                                                                          //Natural: PERFORM UPDATE-MIT-REQUEST
            sub_Update_Mit_Request();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE #CREATE-SUBRQST
        else if (condition((pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Action_Cde().equals(pnd_Create_Subrqst))))
        {
            decideConditionsMet761++;
                                                                                                                                                                          //Natural: PERFORM CREATE-SUB-REQUEST
            sub_Create_Sub_Request();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(tblrecrd_Naz_Tbl_Scrn_Fld_Dscrptn.getValue(1).notEquals(" ") && pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().equals(" ")))                             //Natural: IF TBLRECRD.NAZ-TBL-SCRN-FLD-DSCRPTN ( 1 ) NE ' ' AND ##MSG EQ ' '
        {
                                                                                                                                                                          //Natural: PERFORM CREATE-AUDIT-TRAIL-FOLDER
            sub_Create_Audit_Trail_Folder();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Debug_On().getBoolean()))                                                                              //Natural: IF #MIT-DEBUG-ON
        {
            getReports().write(0, "----- END MIT UPDATE ADSN480 -----");                                                                                                  //Natural: WRITE '----- END MIT UPDATE ADSN480 -----'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ******************** S U B R O U T I N E S *********************
        //* ****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-MIT-REQUEST
        //* ****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-EFSN9120
        //* ****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-MIT-REQUEST
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-CWFN8000
        //* **********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-PROC
        //* ****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-SUB-REQUEST
        //* ****************************************************************
        //* ****************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-AUDIT-TRAIL-FOLDER
    }
    private void sub_Add_Mit_Request() throws Exception                                                                                                                   //Natural: ADD-MIT-REQUEST
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************************************
        pdaEfsa9120.getEfsa9120_Action().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Action_Cde());                                                                 //Natural: MOVE #ACTION-CDE TO EFSA9120.ACTION
        pdaEfsa9120.getEfsa9120_Cabinet_Prefix().setValue("P");                                                                                                           //Natural: MOVE 'P' TO EFSA9120.CABINET-PREFIX
        pdaEfsa9120.getEfsa9120_Pin_Nbr().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Unique_Id());                                                                 //Natural: MOVE ADP-UNIQUE-ID TO EFSA9120.PIN-NBR
        pdaEfsa9120.getEfsa9120_Wpid().getValue(1).setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Wpid());                                                             //Natural: MOVE ADP-WPID TO EFSA9120.WPID ( 1 )
        pdaEfsa9120.getEfsa9120_Tiaa_Rcvd_Dte().getValue(1).setValue(Global.getDATN());                                                                                   //Natural: MOVE *DATN TO EFSA9120.TIAA-RCVD-DTE ( 1 )
        pdaEfsa9120.getEfsa9120_Contract_Nbr().getValue(1,"*").setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Cntrcts_In_Rqst().getValue(1,":",10));                   //Natural: MOVE ADP-CNTRCTS-IN-RQST ( 1:10 ) TO EFSA9120.CONTRACT-NBR ( 1,* )
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Cde().setValue("G");                                                                                                          //Natural: MOVE 'G' TO EFSA9120.RQST-ORIGIN-CDE
        pdaEfsa9120.getEfsa9120_Multi_Rqst_Ind().getValue(1).setValue("0");                                                                                               //Natural: MOVE '0' TO EFSA9120.MULTI-RQST-IND ( 1 )
        pdaEfsa9120.getEfsa9120_Rqst_Entry_Op_Cde().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_User_Id());                                                         //Natural: MOVE #USER-ID TO EFSA9120.RQST-ENTRY-OP-CDE
        pdaEfsa9120.getEfsa9120_System().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Type_Of_Process());                                                            //Natural: MOVE #TYPE-OF-PROCESS TO EFSA9120.SYSTEM
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Unit_Cde().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Bps_Unit());                                                     //Natural: MOVE ADP-BPS-UNIT TO EFSA9120.RQST-ORIGIN-UNIT-CDE
        pnd_Adp_Annty_Strt_Dte_Pnd_Adp_Annty_Strt_Dte_A.setValueEdited(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Annty_Strt_Dte(),new ReportEditMask("YYYYMMDD"));         //Natural: MOVE EDITED ADP-ANNTY-STRT-DTE ( EM = YYYYMMDD ) TO #ADP-ANNTY-STRT-DTE-A
        if (condition(pnd_Adp_Annty_Strt_Dte.notEquals(getZero())))                                                                                                       //Natural: IF #ADP-ANNTY-STRT-DTE NE 0
        {
            DbsUtil.callnat(Adsn481.class , getCurrentProcessState(), pnd_Adp_Annty_Strt_Dte);                                                                            //Natural: CALLNAT 'ADSN481' #ADP-ANNTY-STRT-DTE
            if (condition(Global.isEscape())) return;
            pdaEfsa9120.getEfsa9120_Effective_Dte().getValue(1).setValue(pnd_Adp_Annty_Strt_Dte_Pnd_Adp_Annty_Strt_Dte_A);                                                //Natural: MOVE #ADP-ANNTY-STRT-DTE-A TO EFSA9120.EFFECTIVE-DTE ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Debug_On().getBoolean()))                                                                              //Natural: IF #MIT-DEBUG-ON
        {
            getReports().write(0, "***********************************************");                                                                                     //Natural: WRITE '***********************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "ADSN480 DISPLAYS BEFORE CALL EFSN9120 - ADD MIT");                                                                                     //Natural: WRITE 'ADSN480 DISPLAYS BEFORE CALL EFSN9120 - ADD MIT'
            if (Global.isEscape()) return;
            getReports().write(0, "***********************************************",NEWLINE,"=",pdaEfsa9120.getEfsa9120_Action(),NEWLINE,"=",pdaEfsa9120.getEfsa9120_Cabinet_Prefix(), //Natural: WRITE '***********************************************' / '=' EFSA9120.ACTION / '=' EFSA9120.CABINET-PREFIX / '=' EFSA9120.PIN-NBR / '=' EFSA9120.TIAA-RCVD-DTE ( 1 ) / '=' EFSA9120.WPID ( 1 ) / '=' EFSA9120.CONTRACT-NBR ( 1,* ) / '=' EFSA9120.RQST-ORIGIN-CDE / '=' EFSA9120.MULTI-RQST-IND ( 1 ) / '=' EFSA9120.RQST-ENTRY-OP-CDE / '=' EFSA9120.SYSTEM / '=' EFSA9120.RQST-ORIGIN-UNIT-CDE
                NEWLINE,"=",pdaEfsa9120.getEfsa9120_Pin_Nbr(),NEWLINE,"=",pdaEfsa9120.getEfsa9120_Tiaa_Rcvd_Dte().getValue(1),NEWLINE,"=",pdaEfsa9120.getEfsa9120_Wpid().getValue(1),
                NEWLINE,"=",pdaEfsa9120.getEfsa9120_Contract_Nbr().getValue(1,"*"),NEWLINE,"=",pdaEfsa9120.getEfsa9120_Rqst_Origin_Cde(),NEWLINE,"=",pdaEfsa9120.getEfsa9120_Multi_Rqst_Ind().getValue(1),
                NEWLINE,"=",pdaEfsa9120.getEfsa9120_Rqst_Entry_Op_Cde(),NEWLINE,"=",pdaEfsa9120.getEfsa9120_System(),NEWLINE,"=",pdaEfsa9120.getEfsa9120_Rqst_Origin_Unit_Cde());
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALL-EFSN9120
        sub_Call_Efsn9120();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Call_Efsn9120() throws Exception                                                                                                                     //Natural: CALL-EFSN9120
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************************************
        DbsUtil.callnat(Efsn9120.class , getCurrentProcessState(), pdaEfsa9120.getEfsa9120(), pdaEfsa9120.getEfsa9120_Id(), pdaEfsa902r.getEfsa902r(),                    //Natural: CALLNAT 'EFSN9120' EFSA9120 EFSA9120-ID EFSA902R CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Msg().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                           //Natural: MOVE ##MSG TO #MIT-MSG
        pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Msg_Nbr().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr());                                                    //Natural: MOVE ##MSG-NR TO #MIT-MSG-NBR
        if (condition(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Action_Cde().equals(pnd_Add_Action)))                                                                      //Natural: IF #ACTION-CDE EQ #ADD-ACTION
        {
            pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Mit_Log_Dte_Tme().setValue(pdaEfsa9120.getEfsa9120_Return_Date_Time().getValue(1));                                  //Natural: MOVE EFSA9120.RETURN-DATE-TIME ( 1 ) TO #ADSA480-MIT-UPDATE.ADP-MIT-LOG-DTE-TME
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Debug_On().getBoolean()))                                                                              //Natural: IF #MIT-DEBUG-ON
        {
            getReports().write(0, "*************************************");                                                                                               //Natural: WRITE '*************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "ADSN480 DISPLAYS AFTER CALL EFSN9120");                                                                                                //Natural: WRITE 'ADSN480 DISPLAYS AFTER CALL EFSN9120'
            if (Global.isEscape()) return;
            getReports().write(0, "*************************************",NEWLINE,"=",pdaEfsa9120.getEfsa9120_Wpid().getValue(1),NEWLINE,"=",pdaEfsa9120.getEfsa9120_Return_Date_Time().getValue(1), //Natural: WRITE '*************************************' / '=' EFSA9120.WPID ( 1 ) / '=' EFSA9120.RETURN-DATE-TIME ( 1 ) / '=' ADP-MIT-LOG-DTE-TME / '=' ADP-BPS-UNIT
                NEWLINE,"=",pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Mit_Log_Dte_Tme(),NEWLINE,"=",pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Bps_Unit());
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().notEquals(" ")))                                                                                          //Natural: IF ##MSG NE ' '
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("EFSN9120: RETURN CODE", pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr(),                    //Natural: COMPRESS 'EFSN9120: RETURN CODE' ##MSG-NR ##MSG INTO ##MSG
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg()));
            if (condition(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Debug_On().getBoolean()))                                                                          //Natural: IF #MIT-DEBUG-ON
            {
                getReports().write(0, pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                         //Natural: WRITE ##MSG
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_Mit_Request() throws Exception                                                                                                                //Natural: UPDATE-MIT-REQUEST
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************************************
        pdaCwfa8000.getCwfa8000_Action().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Action_Cde());                                                                 //Natural: ASSIGN CWFA8000.ACTION = #ACTION-CDE
        pdaCwfa8000.getCwfa8000_Rqst_Log_Dte_Tme().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Mit_Log_Dte_Tme());                                                  //Natural: ASSIGN CWFA8000.RQST-LOG-DTE-TME = ADP-MIT-LOG-DTE-TME
        pdaCwfa8000.getCwfa8000_Work_Prcss_Id().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Wpid());                                                                //Natural: ASSIGN CWFA8000.WORK-PRCSS-ID = ADP-WPID
        pdaCwfa8000.getCwfa8000_Wpid_Vldte_Ind().setValue("Y");                                                                                                           //Natural: ASSIGN CWFA8000.WPID-VLDTE-IND = 'Y'
        pdaCwfa8000.getCwfa8000_Mj_Pull_Ind().setValue("N");                                                                                                              //Natural: ASSIGN CWFA8000.MJ-PULL-IND = 'N'
        pdaCwfa8000.getCwfa8000_System().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Type_Of_Process());                                                            //Natural: ASSIGN CWFA8000.SYSTEM = #TYPE-OF-PROCESS
        pdaCwfa8000.getCwfa8000_Rqst_Log_Oprtr_Cde().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_User_Id());                                                        //Natural: ASSIGN CWFA8000.RQST-LOG-OPRTR-CDE = #USER-ID
        if (condition(tblrecrd_Pnd_Tbl_Rcrd_Racf.greater(" ")))                                                                                                           //Natural: IF TBLRECRD.#TBL-RCRD-RACF GT ' '
        {
            pdaCwfa8000.getCwfa8000_Empl_Racf_Id().setValue(tblrecrd_Pnd_Tbl_Rcrd_Racf);                                                                                  //Natural: ASSIGN CWFA8000.EMPL-RACF-ID = TBLRECRD.#TBL-RCRD-RACF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaCwfa8000.getCwfa8000_Empl_Racf_Id().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Entry_User_Id());                                                    //Natural: ASSIGN CWFA8000.EMPL-RACF-ID = ADP-ENTRY-USER-ID
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(tblrecrd_Pnd_Tbl_Rcrd_Unit.notEquals("     ") && tblrecrd_Pnd_Tbl_Rcrd_Unit.notEquals(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Bps_Unit())))        //Natural: IF TBLRECRD.#TBL-RCRD-UNIT NE '     ' AND TBLRECRD.#TBL-RCRD-UNIT NE ADP-BPS-UNIT
        {
            pdaCwfa8000.getCwfa8000_Unit_Cde().setValue(tblrecrd_Pnd_Tbl_Rcrd_Unit);                                                                                      //Natural: ASSIGN CWFA8000.UNIT-CDE = TBLRECRD.#TBL-RCRD-UNIT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaCwfa8000.getCwfa8000_Unit_Cde().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Bps_Unit());                                                             //Natural: ASSIGN CWFA8000.UNIT-CDE = ADP-BPS-UNIT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(tblrecrd_Pnd_Tbl_Rcrd_Work_Step.notEquals(" ")))                                                                                                    //Natural: IF TBLRECRD.#TBL-RCRD-WORK-STEP NE ' '
        {
            pdaCwfa8000.getCwfa8000_Step_Id().setValue(tblrecrd_Pnd_Tbl_Rcrd_Work_Step);                                                                                  //Natural: ASSIGN CWFA8000.STEP-ID = TBLRECRD.#TBL-RCRD-WORK-STEP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaCwfa8000.getCwfa8000_Step_Id().setValue(" ");                                                                                                              //Natural: ASSIGN CWFA8000.STEP-ID = ' '
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Adp_Annty_Strt_Dte_Pnd_Adp_Annty_Strt_Dte_A.setValueEdited(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Annty_Strt_Dte(),new ReportEditMask("YYYYMMDD"));         //Natural: MOVE EDITED ADP-ANNTY-STRT-DTE ( EM = YYYYMMDD ) TO #ADP-ANNTY-STRT-DTE-A
        if (condition(pnd_Adp_Annty_Strt_Dte.notEquals(getZero())))                                                                                                       //Natural: IF #ADP-ANNTY-STRT-DTE NE 0
        {
            DbsUtil.callnat(Adsn481.class , getCurrentProcessState(), pnd_Adp_Annty_Strt_Dte);                                                                            //Natural: CALLNAT 'ADSN481' #ADP-ANNTY-STRT-DTE
            if (condition(Global.isEscape())) return;
            pdaCwfa8000.getCwfa8000_Effctve_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Adp_Annty_Strt_Dte_Pnd_Adp_Annty_Strt_Dte_A);                         //Natural: MOVE EDITED #ADP-ANNTY-STRT-DTE-A TO CWFA8000.EFFCTVE-DTE ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Accntng_Dte().notEquals(getZero())))                                                                       //Natural: IF #ACCNTNG-DTE NE 0
        {
            pnd_Accntng_Dte_A.setValueEdited(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Accntng_Dte(),new ReportEditMask("YYYYMMDD"));                                      //Natural: MOVE EDITED #ACCNTNG-DTE ( EM = YYYYMMDD ) TO #ACCNTNG-DTE-A
            pdaCwfa8000.getCwfa8000_Trnsctn_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Accntng_Dte_A);                                                       //Natural: MOVE EDITED #ACCNTNG-DTE-A TO CWFA8000.TRNSCTN-DTE ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #X 1 TO 10
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(10)); pnd_X.nadd(1))
        {
            pdaCwfa8000.getCwfa8000_Cntrct_Nbr().getValue(pnd_X).setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Cntrcts_In_Rqst().getValue(pnd_X));                    //Natural: ASSIGN CWFA8000.CNTRCT-NBR ( #X ) = ADP-CNTRCTS-IN-RQST ( #X )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Add_Mit.getBoolean()))                                                                                                                          //Natural: IF #ADD-MIT
        {
            pdaCwfa8000.getCwfa8000_Status_Cde().setValue("0000");                                                                                                        //Natural: ASSIGN CWFA8000.STATUS-CDE = '0000'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaCwfa8000.getCwfa8000_Status_Cde().setValue(tblrecrd_Pnd_Tbl_Rcrd_Mit_Stts);                                                                                //Natural: ASSIGN CWFA8000.STATUS-CDE = TBLRECRD.#TBL-RCRD-MIT-STTS
            //*  042398 END
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Debug_On().getBoolean()))                                                                              //Natural: IF #MIT-DEBUG-ON
        {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-PROC
            sub_Display_Proc();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALL-CWFN8000
        sub_Call_Cwfn8000();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Add_Mit.getBoolean() && pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().equals(" ")))                                                                 //Natural: IF #ADD-MIT AND ##MSG EQ ' '
        {
            pdaCwfa8000.getCwfa8000_Status_Cde().setValue(tblrecrd_Pnd_Tbl_Rcrd_Mit_Stts);                                                                                //Natural: ASSIGN CWFA8000.STATUS-CDE = TBLRECRD.#TBL-RCRD-MIT-STTS
            pdaCwfa8000.getCwfa8000_Orgnl_Unit_Cde().reset();                                                                                                             //Natural: RESET CWFA8000.ORGNL-UNIT-CDE
            if (condition(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Debug_On().getBoolean()))                                                                          //Natural: IF #MIT-DEBUG-ON
            {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-PROC
                sub_Display_Proc();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALL-CWFN8000
            sub_Call_Cwfn8000();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Call_Cwfn8000() throws Exception                                                                                                                     //Natural: CALL-CWFN8000
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        DbsUtil.callnat(Cwfn8000.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaCwfpda_D.getDialog_Info_Sub(),            //Natural: CALLNAT 'CWFN8000' MSG-INFO-SUB PASS-SUB DIALOG-INFO-SUB CWFA8000
            pdaCwfa8000.getCwfa8000());
        if (condition(Global.isEscape())) return;
        pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Msg().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                           //Natural: MOVE ##MSG TO #MIT-MSG
        pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Msg_Nbr().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr());                                                    //Natural: MOVE ##MSG-NR TO #MIT-MSG-NBR
        if (condition(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().notEquals(" ")))                                                                                          //Natural: IF ##MSG NE ' '
        {
            pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("CWFN8000: RETURN CODE", pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg_Nr(),                    //Natural: COMPRESS 'CWFN8000: RETURN CODE' ##MSG-NR ##MSG INTO ##MSG
                pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg()));
            if (condition(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Debug_On().getBoolean()))                                                                          //Natural: IF #MIT-DEBUG-ON
            {
                getReports().write(0, pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                         //Natural: WRITE ##MSG
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Display_Proc() throws Exception                                                                                                                      //Natural: DISPLAY-PROC
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************
        getReports().write(0, "*****************************************************");                                                                                   //Natural: WRITE '*****************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "ADSN480 DISPLAYS BEFORE CALL TO CWFN8000 - UPDATE MIT");                                                                                   //Natural: WRITE 'ADSN480 DISPLAYS BEFORE CALL TO CWFN8000 - UPDATE MIT'
        if (Global.isEscape()) return;
        getReports().write(0, "*****************************************************");                                                                                   //Natural: WRITE '*****************************************************'
        if (Global.isEscape()) return;
        getReports().write(0, "FIELDS PASSED TO ADSN480 FROM CALLING MODULE:");                                                                                           //Natural: WRITE 'FIELDS PASSED TO ADSN480 FROM CALLING MODULE:'
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Unique_Id());                                                                                  //Natural: WRITE '=' ADP-UNIQUE-ID
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Stts_Cde());                                                                                   //Natural: WRITE '=' ADP-STTS-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Mit_Log_Dte_Tme());                                                                            //Natural: WRITE '=' ADP-MIT-LOG-DTE-TME
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Wpid());                                                                                       //Natural: WRITE '=' ADP-WPID
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Adp_Annty_Strt_Dte_Pnd_Adp_Annty_Strt_Dte_A);                                                                                       //Natural: WRITE '=' #ADP-ANNTY-STRT-DTE-A
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Bps_Unit());                                                                                   //Natural: WRITE '=' ADP-BPS-UNIT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_User_Id());                                                                                    //Natural: WRITE '=' #USER-ID
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Type_Of_Process());                                                                            //Natural: WRITE '=' #TYPE-OF-PROCESS
        if (Global.isEscape()) return;
        getReports().write(0, "FIELDS FROM LOOK-UP OF NAZ TABLE 63:");                                                                                                    //Natural: WRITE 'FIELDS FROM LOOK-UP OF NAZ TABLE 63:'
        if (Global.isEscape()) return;
        getReports().write(0, "=",tblrecrd_Pnd_Tbl_Rcrd_Mit_Stts);                                                                                                        //Natural: WRITE '=' #TBL-RCRD-MIT-STTS
        if (Global.isEscape()) return;
        getReports().write(0, "=",tblrecrd_Pnd_Tbl_Rcrd_Unit);                                                                                                            //Natural: WRITE '=' #TBL-RCRD-UNIT
        if (Global.isEscape()) return;
        getReports().write(0, "=",tblrecrd_Pnd_Tbl_Rcrd_Racf);                                                                                                            //Natural: WRITE '=' #TBL-RCRD-RACF
        if (Global.isEscape()) return;
        getReports().write(0, "=",tblrecrd_Pnd_Tbl_Rcrd_Wpid_Switch);                                                                                                     //Natural: WRITE '=' #TBL-RCRD-WPID-SWITCH
        if (Global.isEscape()) return;
        getReports().write(0, "=",tblrecrd_Pnd_Tbl_Rcrd_Work_Step,NEWLINE);                                                                                               //Natural: WRITE '=' #TBL-RCRD-WORK-STEP /
        if (Global.isEscape()) return;
        getReports().write(0, "FIELDS PASSED IN CWFA8000 TO MIT: ");                                                                                                      //Natural: WRITE 'FIELDS PASSED IN CWFA8000 TO MIT: '
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaCwfa8000.getCwfa8000_System());                                                                                                      //Natural: WRITE '=' CWFA8000.SYSTEM
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaCwfa8000.getCwfa8000_Rqst_Log_Dte_Tme());                                                                                            //Natural: WRITE '=' CWFA8000.RQST-LOG-DTE-TME
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaCwfa8000.getCwfa8000_Rqst_Log_Oprtr_Cde());                                                                                          //Natural: WRITE '=' CWFA8000.RQST-LOG-OPRTR-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaCwfa8000.getCwfa8000_Orgnl_Unit_Cde());                                                                                              //Natural: WRITE '=' CWFA8000.ORGNL-UNIT-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaCwfa8000.getCwfa8000_Unit_Cde());                                                                                                    //Natural: WRITE '=' CWFA8000.UNIT-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaCwfa8000.getCwfa8000_Empl_Racf_Id());                                                                                                //Natural: WRITE '=' CWFA8000.EMPL-RACF-ID
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaCwfa8000.getCwfa8000_Step_Id());                                                                                                     //Natural: WRITE '=' CWFA8000.STEP-ID
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaCwfa8000.getCwfa8000_Status_Cde());                                                                                                  //Natural: WRITE '=' CWFA8000.STATUS-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaCwfa8000.getCwfa8000_Wpid_Vldte_Ind());                                                                                              //Natural: WRITE '=' CWFA8000.WPID-VLDTE-IND
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaCwfa8000.getCwfa8000_Effctve_Dte());                                                                                                 //Natural: WRITE '=' CWFA8000.EFFCTVE-DTE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaCwfa8000.getCwfa8000_Trnsctn_Dte());                                                                                                 //Natural: WRITE '=' CWFA8000.TRNSCTN-DTE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaCwfa8000.getCwfa8000_Mj_Pull_Ind());                                                                                                 //Natural: WRITE '=' CWFA8000.MJ-PULL-IND
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaCwfa8000.getCwfa8000_Work_Prcss_Id());                                                                                               //Natural: WRITE '=' CWFA8000.WORK-PRCSS-ID
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaCwfa8000.getCwfa8000_Effctve_Dte(), new ReportEditMask ("YYYYMMDD"));                                                                //Natural: WRITE '=' CWFA8000.EFFCTVE-DTE ( EM = YYYYMMDD )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaCwfa8000.getCwfa8000_Trans_Dte(), new ReportEditMask ("YYYYMMDD"));                                                                  //Natural: WRITE '=' CWFA8000.TRANS-DTE ( EM = YYYYMMDD )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaCwfa8000.getCwfa8000_Spcl_Hndlng_Txt());                                                                                             //Natural: WRITE '=' CWFA8000.SPCL-HNDLNG-TXT
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #X 1 TO 10
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(10)); pnd_X.nadd(1))
        {
            if (condition(pdaCwfa8000.getCwfa8000_Cntrct_Nbr().getValue(pnd_X).equals(" ")))                                                                              //Natural: IF CWFA8000.CNTRCT-NBR ( #X ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "=",pdaCwfa8000.getCwfa8000_Cntrct_Nbr().getValue(pnd_X));                                                                          //Natural: WRITE '=' CWFA8000.CNTRCT-NBR ( #X )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    //*  'P' = POLICYHOLDER
    //*  PGM WILL GET FROM MIT FILE
    //*  OPTIONAL
    //*  'S'= SUBREQUEST
    //*  'I' = INTERNAL
    //*  'NOT' = NOTE
    //*  'N' = INTERNAL
    //*  'T' = TEXT
    //*  'A' = AUTOMATIC
    //*   0 = NO MICROJACKET
    private void sub_Create_Sub_Request() throws Exception                                                                                                                //Natural: CREATE-SUB-REQUEST
    {
        if (BLNatReinput.isReinput()) return;

        pdaEfsa9120.getEfsa9120_Action().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Action_Cde());                                                                 //Natural: ASSIGN EFSA9120.ACTION := #ACTION-CDE
        pdaEfsa9120.getEfsa9120_Cabinet_Prefix().setValue("P");                                                                                                           //Natural: ASSIGN EFSA9120.CABINET-PREFIX := 'P'
        pdaEfsa9120.getEfsa9120_Pin_Nbr().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Unique_Id());                                                                 //Natural: ASSIGN EFSA9120.PIN-NBR := ADP-UNIQUE-ID
        pdaEfsa9120.getEfsa9120_Tiaa_Rcvd_Dte().getValue(1).setValue(0);                                                                                                  //Natural: ASSIGN EFSA9120.TIAA-RCVD-DTE ( 1 ) := 0
        pdaEfsa9120.getEfsa9120_Wpid().getValue(1).setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Wpid());                                                             //Natural: ASSIGN EFSA9120.WPID ( 1 ) := ADP-WPID
        pdaEfsa9120.getEfsa9120_Wpid_Validate_Ind().getValue(1).setValue("Y");                                                                                            //Natural: ASSIGN EFSA9120.WPID-VALIDATE-IND ( 1 ) := 'Y'
        pdaEfsa9120.getEfsa9120_Related_Log_Dte_Tme().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Mit_Log_Dte_Tme());                                               //Natural: ASSIGN EFSA9120.RELATED-LOG-DTE-TME := ADP-MIT-LOG-DTE-TME
        pdaEfsa9120.getEfsa9120_Rqst_Entry_Op_Cde().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Entry_User_Id());                                                   //Natural: ASSIGN EFSA9120.RQST-ENTRY-OP-CDE := ADP-ENTRY-USER-ID
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Cde().setValue("S");                                                                                                          //Natural: ASSIGN EFSA9120.RQST-ORIGIN-CDE := 'S'
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Unit_Cde().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Bps_Unit());                                                     //Natural: ASSIGN EFSA9120.RQST-ORIGIN-UNIT-CDE := ADP-BPS-UNIT
        pdaEfsa9120.getEfsa9120_Unit_Cde().getValue(1).setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Bps_Unit());                                                     //Natural: ASSIGN EFSA9120.UNIT-CDE ( 1 ) := ADP-BPS-UNIT
        pdaEfsa9120.getEfsa9120_Doc_Category().setValue("I");                                                                                                             //Natural: ASSIGN EFSA9120.DOC-CATEGORY := 'I'
        pdaEfsa9120.getEfsa9120_Doc_Class().setValue("NOT");                                                                                                              //Natural: ASSIGN EFSA9120.DOC-CLASS := 'NOT'
        pdaEfsa9120.getEfsa9120_Doc_Direction().setValue("N");                                                                                                            //Natural: ASSIGN EFSA9120.DOC-DIRECTION := 'N'
        pdaEfsa9120.getEfsa9120_Doc_Format_Cde().setValue("T");                                                                                                           //Natural: ASSIGN EFSA9120.DOC-FORMAT-CDE := 'T'
        pdaEfsa9120.getEfsa9120_Doc_Text().getValue(1).setValue(pnd_Subrqst_Doc_Text);                                                                                    //Natural: ASSIGN EFSA9120.DOC-TEXT ( 1 ) := #SUBRQST-DOC-TEXT
        pdaEfsa9120.getEfsa9120_Processing_Type().getValue(1).setValue("A");                                                                                              //Natural: ASSIGN EFSA9120.PROCESSING-TYPE ( 1 ) := 'A'
        pdaEfsa9120.getEfsa9120_Batch_Nbr().setValue(0);                                                                                                                  //Natural: ASSIGN EFSA9120.BATCH-NBR := 0
        pdaEfsa9120.getEfsa9120_System().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Type_Of_Process());                                                            //Natural: ASSIGN EFSA9120.SYSTEM := #TYPE-OF-PROCESS
        if (condition(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Debug_On().getBoolean()))                                                                              //Natural: IF #MIT-DEBUG-ON
        {
            getReports().write(0, "******************************************************");                                                                              //Natural: WRITE '******************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "ADSN480 DISPLAYS BEFORE CALL EFSN9120 - CREATE-SUBRQST");                                                                              //Natural: WRITE 'ADSN480 DISPLAYS BEFORE CALL EFSN9120 - CREATE-SUBRQST'
            if (Global.isEscape()) return;
            getReports().write(0, "******************************************************",NEWLINE,"=",pdaEfsa9120.getEfsa9120_Action(),NEWLINE,"=",pdaEfsa9120.getEfsa9120_Pin_Nbr(),NEWLINE,"=",pdaEfsa9120.getEfsa9120_Wpid().getValue(1),NEWLINE,"=",pdaEfsa9120.getEfsa9120_Related_Log_Dte_Tme(),NEWLINE,"=",pdaEfsa9120.getEfsa9120_Rqst_Entry_Op_Cde(),NEWLINE,"=",pdaEfsa9120.getEfsa9120_Rqst_Origin_Unit_Cde(),NEWLINE,"=",pdaEfsa9120.getEfsa9120_Unit_Cde().getValue(1),NEWLINE,"=",pdaEfsa9120.getEfsa9120_Doc_Text().getValue(1),  //Natural: WRITE '******************************************************' / '=' EFSA9120.ACTION / '=' EFSA9120.PIN-NBR / '=' EFSA9120.WPID ( 1 ) / '=' EFSA9120.RELATED-LOG-DTE-TME / '=' EFSA9120.RQST-ENTRY-OP-CDE / '=' EFSA9120.RQST-ORIGIN-UNIT-CDE / '=' EFSA9120.UNIT-CDE ( 1 ) / '=' EFSA9120.DOC-TEXT ( 1 ) ( AL = 60 ) / '=' EFSA9120.SYSTEM
                new AlphanumericLength (60),NEWLINE,"=",pdaEfsa9120.getEfsa9120_System());
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALL-EFSN9120
        sub_Call_Efsn9120();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Create_Audit_Trail_Folder() throws Exception                                                                                                         //Natural: CREATE-AUDIT-TRAIL-FOLDER
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************************************
        pdaEfsa9120.getEfsa9120().reset();                                                                                                                                //Natural: RESET EFSA9120
        pdaEfsa9120.getEfsa9120_Action().setValue(pnd_Create_Folder);                                                                                                     //Natural: MOVE #CREATE-FOLDER TO EFSA9120.ACTION
        pdaEfsa9120.getEfsa9120_Cabinet_Prefix().setValue("P");                                                                                                           //Natural: MOVE 'P' TO EFSA9120.CABINET-PREFIX
        pdaEfsa9120.getEfsa9120_Pin_Nbr().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Unique_Id());                                                                 //Natural: MOVE ADP-UNIQUE-ID TO EFSA9120.PIN-NBR
        pdaEfsa9120.getEfsa9120_System().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Type_Of_Process());                                                            //Natural: MOVE #TYPE-OF-PROCESS TO EFSA9120.SYSTEM
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Unit_Cde().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Bps_Unit());                                                     //Natural: MOVE ADP-BPS-UNIT TO EFSA9120.RQST-ORIGIN-UNIT-CDE
        pdaEfsa9120.getEfsa9120_Doc_Direction().setValue("N");                                                                                                            //Natural: MOVE 'N' TO EFSA9120.DOC-DIRECTION
        pdaEfsa9120.getEfsa9120_Doc_Format_Cde().setValue("T");                                                                                                           //Natural: MOVE 'T' TO EFSA9120.DOC-FORMAT-CDE
        pdaEfsa9120.getEfsa9120_Doc_Retention_Cde().setValue("P");                                                                                                        //Natural: MOVE 'P' TO EFSA9120.DOC-RETENTION-CDE
        pdaEfsa9120.getEfsa9120_Doc_Category().setValue("I");                                                                                                             //Natural: MOVE 'I' TO EFSA9120.DOC-CATEGORY
        pdaEfsa9120.getEfsa9120_Doc_Class().setValue("AUD");                                                                                                              //Natural: MOVE 'AUD' TO EFSA9120.DOC-CLASS
        //*  USE ADS
        pdaEfsa9120.getEfsa9120_Doc_Specific().setValue("ADS");                                                                                                           //Natural: MOVE 'ADS' TO EFSA9120.DOC-SPECIFIC
        pdaEfsa9120.getEfsa9120_Last_Page_Flag().setValue("Y");                                                                                                           //Natural: MOVE 'Y' TO EFSA9120.LAST-PAGE-FLAG
        //*  USE
        pdaEfsa9120.getEfsa9120_Rqst_Entry_Op_Cde().setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_User_Id());                                                         //Natural: MOVE #USER-ID TO EFSA9120.RQST-ENTRY-OP-CDE
        pdaEfsa9120.getEfsa9120_Wpid().getValue(1).setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Wpid());                                                             //Natural: MOVE ADP-WPID TO EFSA9120.WPID ( 1 )
        FOR03:                                                                                                                                                            //Natural: FOR #I 1 TO 60
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(60)); pnd_I.nadd(1))
        {
            if (condition(tblrecrd_Naz_Tbl_Scrn_Fld_Dscrptn_A60.getValue(pnd_I).equals(" ")))                                                                             //Natural: IF TBLRECRD.NAZ-TBL-SCRN-FLD-DSCRPTN-A60 ( #I ) EQ ' '
            {
                tblrecrd_Naz_Tbl_Scrn_Fld_Dscrptn_A60.getValue(pnd_I).setValue(DbsUtil.compress("PLAN NBR : ", pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Plan_Nbr()));     //Natural: COMPRESS 'PLAN NBR : ' ADP-PLAN-NBR TO TBLRECRD.NAZ-TBL-SCRN-FLD-DSCRPTN-A60 ( #I )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaEfsa9120.getEfsa9120_Doc_Text().getValue(1,":",50).setValue(tblrecrd_Naz_Tbl_Scrn_Fld_Dscrptn_A60.getValue("*"));                                              //Natural: MOVE TBLRECRD.NAZ-TBL-SCRN-FLD-DSCRPTN-A60 ( * ) TO EFSA9120.DOC-TEXT ( 1:50 )
        pdaEfsa9120.getEfsa9120_Rqst_Log_Dte_Tme().getValue(1).setValue(pdaAdsa480.getPnd_Adsa480_Mit_Update_Adp_Mit_Log_Dte_Tme());                                      //Natural: MOVE #ADSA480-MIT-UPDATE.ADP-MIT-LOG-DTE-TME TO EFSA9120.RQST-LOG-DTE-TME ( 1 )
        if (condition(pdaAdsa480.getPnd_Adsa480_Mit_Update_Pnd_Mit_Debug_On().getBoolean()))                                                                              //Natural: IF #MIT-DEBUG-ON
        {
            getReports().write(0, "******************************************************");                                                                              //Natural: WRITE '******************************************************'
            if (Global.isEscape()) return;
            getReports().write(0, "ADSN480 DISPLAYS BEFORE CALL EFSN9120 - CREATE-FOLDER");                                                                               //Natural: WRITE 'ADSN480 DISPLAYS BEFORE CALL EFSN9120 - CREATE-FOLDER'
            if (Global.isEscape()) return;
            //*  USE
            getReports().write(0, "******************************************************",NEWLINE,"=",pdaEfsa9120.getEfsa9120_Action(),NEWLINE,"=",pdaEfsa9120.getEfsa9120_Pin_Nbr(),NEWLINE,"=",pdaEfsa9120.getEfsa9120_System(),NEWLINE,"=",pdaEfsa9120.getEfsa9120_Rqst_Origin_Unit_Cde(),NEWLINE,"=",pdaEfsa9120.getEfsa9120_Rqst_Entry_Op_Cde(),NEWLINE,"=",pdaEfsa9120.getEfsa9120_Wpid().getValue(1),NEWLINE,"=",pdaEfsa9120.getEfsa9120_Doc_Text().getValue(1),  //Natural: WRITE '******************************************************' / '=' EFSA9120.ACTION / '=' EFSA9120.PIN-NBR / '=' EFSA9120.SYSTEM / '=' EFSA9120.RQST-ORIGIN-UNIT-CDE / '=' EFSA9120.RQST-ENTRY-OP-CDE / '=' EFSA9120.WPID ( 1 ) / '=' EFSA9120.DOC-TEXT ( 1 ) ( AL = 60 ) / '=' EFSA9120.RQST-LOG-DTE-TME ( 1 )
                new AlphanumericLength (60),NEWLINE,"=",pdaEfsa9120.getEfsa9120_Rqst_Log_Dte_Tme().getValue(1));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALL-EFSN9120
        sub_Call_Efsn9120();
        if (condition(Global.isEscape())) {return;}
    }

    //
}
