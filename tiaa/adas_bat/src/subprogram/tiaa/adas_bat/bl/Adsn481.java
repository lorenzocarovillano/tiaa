/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:54:27 PM
**        * FROM NATURAL SUBPROGRAM : Adsn481
************************************************************
**        * FILE NAME            : Adsn481.java
**        * CLASS NAME           : Adsn481
**        * INSTANCE NAME        : Adsn481
************************************************************
************************************************************************
* SUBROUTINE ADSN481
* SYSTEM   : NEW ANNUITIZATION SYSTEM
* TITLE    : DATE VALIDATION/ADJUSTMENT FOR ANNUITY START DATE
* FUNCTION : ACCESS NAZ-TABLE-DDM TO GET HOLIDAY DATES.
* WRITTEN  : JULY, 97
* MOD DATE   MOD BY        DESCRIPTION OF CHANGES
* --------   ------------  --------------------------------------------
* 03/18/04   E.VILLANUEVA  THIS MODULE WAS CLONED FROM ADAM MODULE
*                          NAZN701 TO BE USED FOR ADAS ANNUITIZATION
*                          SUNGUARD PROCESS.
* 04/17/12   O. SOTTO      REVISED TO USE EXTERNALIZATION CALENDAR.
*                          SC 041712.
* 11/26/2013 E. MELNIK     RECOMPILED FOR NEW NECA4000.
*                          CREF/REA REDESIGN CHANGES.
* 03/23/17   E. MELNIK     RESTOWED FOR PIN EXPANSION.
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn481 extends BLNatBase
{
    // Data Areas
    private PdaNeca4000 pdaNeca4000;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Passed_Date;

    private DbsGroup pnd_Passed_Date__R_Field_1;
    private DbsField pnd_Passed_Date_Pnd_Passed_Ccyy;

    private DbsGroup pnd_Passed_Date__R_Field_2;
    private DbsField pnd_Passed_Date_Pnd_Passed_Cc;
    private DbsField pnd_Passed_Date_Pnd_Passed_Yy;
    private DbsField pnd_Passed_Date_Pnd_Passed_Mm;
    private DbsField pnd_Passed_Date_Pnd_Passed_Dd;

    private DbsGroup pnd_Passed_Date__R_Field_3;
    private DbsField pnd_Passed_Date_Pnd_Passed_Century;
    private DbsField pnd_Passed_Date_Pnd_Passed_Dte;

    private DbsGroup pnd_Passed_Date__R_Field_4;
    private DbsField pnd_Passed_Date_Pnd_Passed_Datex;

    private DataAccessProgramView vw_naz_Table_Ddm;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id;
    private DbsField naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt;
    private DbsGroup naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup;
    private DbsField naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt;
    private DbsGroup naz_Table_Ddm_Naz_Tbl_Scrn_Fld_CdeMuGroup;
    private DbsField naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Cde;
    private DbsGroup naz_Table_Ddm_Naz_Tbl_Scrn_Fld_DscrptnMuGroup;
    private DbsField naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn;
    private DbsField pnd_Date;
    private DbsField pnd_Day;
    private DbsField pnd_Leap_Year;
    private DbsField pnd_End_Of_Month;
    private DbsField pnd_Result;
    private DbsField pnd_Rem;
    private DbsField pnd_I;
    private DbsField pnd_W_Dte;
    private DbsField pnd_Naz_Table_Super3;

    private DbsGroup pnd_Naz_Table_Super3__R_Field_5;
    private DbsField pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Typ_Ind;
    private DbsField pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl1_Id;
    private DbsField pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl2_Id;
    private DbsField pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl3_Id;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaNeca4000 = new PdaNeca4000(localVariables);

        // parameters
        parameters = new DbsRecord();
        pnd_Passed_Date = parameters.newFieldInRecord("pnd_Passed_Date", "#PASSED-DATE", FieldType.NUMERIC, 8);
        pnd_Passed_Date.setParameterOption(ParameterOption.ByReference);

        pnd_Passed_Date__R_Field_1 = parameters.newGroupInRecord("pnd_Passed_Date__R_Field_1", "REDEFINE", pnd_Passed_Date);
        pnd_Passed_Date_Pnd_Passed_Ccyy = pnd_Passed_Date__R_Field_1.newFieldInGroup("pnd_Passed_Date_Pnd_Passed_Ccyy", "#PASSED-CCYY", FieldType.NUMERIC, 
            4);

        pnd_Passed_Date__R_Field_2 = pnd_Passed_Date__R_Field_1.newGroupInGroup("pnd_Passed_Date__R_Field_2", "REDEFINE", pnd_Passed_Date_Pnd_Passed_Ccyy);
        pnd_Passed_Date_Pnd_Passed_Cc = pnd_Passed_Date__R_Field_2.newFieldInGroup("pnd_Passed_Date_Pnd_Passed_Cc", "#PASSED-CC", FieldType.NUMERIC, 2);
        pnd_Passed_Date_Pnd_Passed_Yy = pnd_Passed_Date__R_Field_2.newFieldInGroup("pnd_Passed_Date_Pnd_Passed_Yy", "#PASSED-YY", FieldType.NUMERIC, 2);
        pnd_Passed_Date_Pnd_Passed_Mm = pnd_Passed_Date__R_Field_1.newFieldInGroup("pnd_Passed_Date_Pnd_Passed_Mm", "#PASSED-MM", FieldType.NUMERIC, 2);
        pnd_Passed_Date_Pnd_Passed_Dd = pnd_Passed_Date__R_Field_1.newFieldInGroup("pnd_Passed_Date_Pnd_Passed_Dd", "#PASSED-DD", FieldType.NUMERIC, 2);

        pnd_Passed_Date__R_Field_3 = parameters.newGroupInRecord("pnd_Passed_Date__R_Field_3", "REDEFINE", pnd_Passed_Date);
        pnd_Passed_Date_Pnd_Passed_Century = pnd_Passed_Date__R_Field_3.newFieldInGroup("pnd_Passed_Date_Pnd_Passed_Century", "#PASSED-CENTURY", FieldType.NUMERIC, 
            2);
        pnd_Passed_Date_Pnd_Passed_Dte = pnd_Passed_Date__R_Field_3.newFieldInGroup("pnd_Passed_Date_Pnd_Passed_Dte", "#PASSED-DTE", FieldType.NUMERIC, 
            6);

        pnd_Passed_Date__R_Field_4 = parameters.newGroupInRecord("pnd_Passed_Date__R_Field_4", "REDEFINE", pnd_Passed_Date);
        pnd_Passed_Date_Pnd_Passed_Datex = pnd_Passed_Date__R_Field_4.newFieldInGroup("pnd_Passed_Date_Pnd_Passed_Datex", "#PASSED-DATEX", FieldType.STRING, 
            8);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables

        vw_naz_Table_Ddm = new DataAccessProgramView(new NameInfo("vw_naz_Table_Ddm", "NAZ-TABLE-DDM"), "NAZ_TABLE_DDM", "NAZ_TABLE_RCRD");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind", "NAZ-TBL-RCRD-ACTV-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_ACTV_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.setDdmHeader("TBL/REC/ACTV");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind", "NAZ-TBL-RCRD-TYP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_TYP_IND");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Typ_Ind.setDdmHeader("TBL/TYP");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id", "NAZ-TBL-RCRD-LVL1-ID", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL1_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.setDdmHeader("TBL/NBR");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id", "NAZ-TBL-RCRD-LVL2-ID", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL2_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.setDdmHeader("TBL/ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id", "NAZ-TBL-RCRD-LVL3-ID", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_LVL3_ID");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.setDdmHeader("TBL/REC/CODE");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt = vw_naz_Table_Ddm.getRecord().newFieldInGroup("naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt", "NAZ-TBL-RCRD-DSCRPTN-TXT", 
            FieldType.STRING, 60, RepeatingFieldStrategy.None, "NAZ_TBL_RCRD_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Rcrd_Dscrptn_Txt.setDdmHeader("TBL/DSCRPTION");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup = vw_naz_Table_Ddm.getRecord().newGroupInGroup("NAZ_TABLE_DDM_NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", 
            "NAZ_TBL_SECNDRY_DSCRPTN_TXTMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt = naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_TxtMuGroup.newFieldArrayInGroup("naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt", 
            "NAZ-TBL-SECNDRY-DSCRPTN-TXT", FieldType.STRING, 80, new DbsArrayController(1, 100), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SECNDRY_DSCRPTN_TXT");
        naz_Table_Ddm_Naz_Tbl_Secndry_Dscrptn_Txt.setDdmHeader("CDE/2ND/DSC");
        naz_Table_Ddm_Naz_Tbl_Scrn_Fld_CdeMuGroup = vw_naz_Table_Ddm.getRecord().newGroupInGroup("NAZ_TABLE_DDM_NAZ_TBL_SCRN_FLD_CDEMuGroup", "NAZ_TBL_SCRN_FLD_CDEMuGroup", 
            RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SCRN_FLD_CDE");
        naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Cde = naz_Table_Ddm_Naz_Tbl_Scrn_Fld_CdeMuGroup.newFieldArrayInGroup("naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Cde", "NAZ-TBL-SCRN-FLD-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 100), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SCRN_FLD_CDE");
        naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Cde.setDdmHeader("HDING/CDE");
        naz_Table_Ddm_Naz_Tbl_Scrn_Fld_DscrptnMuGroup = vw_naz_Table_Ddm.getRecord().newGroupInGroup("NAZ_TABLE_DDM_NAZ_TBL_SCRN_FLD_DSCRPTNMuGroup", 
            "NAZ_TBL_SCRN_FLD_DSCRPTNMuGroup", RepeatingFieldStrategy.SubTableFieldArray, "NAZ_TABLE_RCRD_NAZ_TBL_SCRN_FLD_DSCRPTN");
        naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn = naz_Table_Ddm_Naz_Tbl_Scrn_Fld_DscrptnMuGroup.newFieldArrayInGroup("naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn", 
            "NAZ-TBL-SCRN-FLD-DSCRPTN", FieldType.STRING, 35, new DbsArrayController(1, 100), RepeatingFieldStrategy.SubTableFieldArrayNoGap, "NAZ_TBL_SCRN_FLD_DSCRPTN");
        naz_Table_Ddm_Naz_Tbl_Scrn_Fld_Dscrptn.setDdmHeader("HD = FIELD");
        registerRecord(vw_naz_Table_Ddm);

        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.DATE);
        pnd_Day = localVariables.newFieldInRecord("pnd_Day", "#DAY", FieldType.STRING, 9);
        pnd_Leap_Year = localVariables.newFieldInRecord("pnd_Leap_Year", "#LEAP-YEAR", FieldType.BOOLEAN, 1);
        pnd_End_Of_Month = localVariables.newFieldInRecord("pnd_End_Of_Month", "#END-OF-MONTH", FieldType.BOOLEAN, 1);
        pnd_Result = localVariables.newFieldInRecord("pnd_Result", "#RESULT", FieldType.PACKED_DECIMAL, 5);
        pnd_Rem = localVariables.newFieldInRecord("pnd_Rem", "#REM", FieldType.PACKED_DECIMAL, 3);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_W_Dte = localVariables.newFieldInRecord("pnd_W_Dte", "#W-DTE", FieldType.STRING, 8);
        pnd_Naz_Table_Super3 = localVariables.newFieldInRecord("pnd_Naz_Table_Super3", "#NAZ-TABLE-SUPER3", FieldType.STRING, 30);

        pnd_Naz_Table_Super3__R_Field_5 = localVariables.newGroupInRecord("pnd_Naz_Table_Super3__R_Field_5", "REDEFINE", pnd_Naz_Table_Super3);
        pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Typ_Ind = pnd_Naz_Table_Super3__R_Field_5.newFieldInGroup("pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Typ_Ind", 
            "#NAZ-TBL-RCRD-TYP-IND", FieldType.STRING, 1);
        pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl1_Id = pnd_Naz_Table_Super3__R_Field_5.newFieldInGroup("pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl1_Id", 
            "#NAZ-TBL-RCRD-LVL1-ID", FieldType.STRING, 6);
        pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl2_Id = pnd_Naz_Table_Super3__R_Field_5.newFieldInGroup("pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl2_Id", 
            "#NAZ-TBL-RCRD-LVL2-ID", FieldType.STRING, 3);
        pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl3_Id = pnd_Naz_Table_Super3__R_Field_5.newFieldInGroup("pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl3_Id", 
            "#NAZ-TBL-RCRD-LVL3-ID", FieldType.STRING, 20);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_naz_Table_Ddm.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn481() throws Exception
    {
        super("Adsn481");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  041712 START
        pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Passed_Date_Pnd_Passed_Datex);                                                                         //Natural: MOVE EDITED #PASSED-DATEX TO #DATE ( EM = YYYYMMDD )
        pdaNeca4000.getNeca4000_Function_Cde().setValue("CAL");                                                                                                           //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'CAL'
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pdaNeca4000.getNeca4000_Cal_Business_Day_Ind().getValue(1).equals("Y") || pnd_I.greater(20))) {break;}                                          //Natural: UNTIL NECA4000.CAL-BUSINESS-DAY-IND ( 1 ) = 'Y' OR #I GT 20
            pnd_W_Dte.setValueEdited(pnd_Date,new ReportEditMask("YYYYMMDD"));                                                                                            //Natural: MOVE EDITED #DATE ( EM = YYYYMMDD ) TO #W-DTE
            pdaNeca4000.getNeca4000_Cal_Key_For_Dte().compute(new ComputeParameters(false, pdaNeca4000.getNeca4000_Cal_Key_For_Dte()), pnd_W_Dte.val());                  //Natural: ASSIGN NECA4000.CAL-KEY-FOR-DTE := VAL ( #W-DTE )
            DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                        //Natural: CALLNAT 'NECN4000' NECA4000
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            if (condition(pdaNeca4000.getNeca4000_Cal_Business_Day_Ind().getValue(1).equals("Y")))                                                                        //Natural: IF NECA4000.CAL-BUSINESS-DAY-IND ( 1 ) = 'Y'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Date.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #DATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_I.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #I
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pnd_Passed_Date_Pnd_Passed_Datex.setValueEdited(pnd_Date,new ReportEditMask("YYYYMMDD"));                                                                         //Natural: MOVE EDITED #DATE ( EM = YYYYMMDD ) TO #PASSED-DATEX
        //* *DIVIDE 4 INTO #PASSED-CCYY GIVING #RESULT
        //* *  REMAINDER #REM
        //* *IF #REM EQ 0
        //* *  #LEAP-YEAR := TRUE
        //* *ELSE
        //* *  #LEAP-YEAR := FALSE
        //* *END-IF
        //* *
        //* *PERFORM READ-HOLIDAY-TABLE
        //* *MOVE EDITED #PASSED-DATEX TO #DATE (EM=YYYYMMDD)
        //* *MOVE EDITED #DATE (EM=N(9)) TO #DAY
        //* *IF #DAY = 'Sunday'
        //* *  PERFORM ADD-ONE-DAY
        //* *  PERFORM READ-HOLIDAY-TABLE
        //* *ELSE
        //* *  IF #DAY = 'Saturday'
        //* *    PERFORM ADD-ONE-DAY
        //* *    PERFORM ADD-ONE-DAY
        //* *    PERFORM READ-HOLIDAY-TABLE
        //* *  END-IF
        //* *END-IF
        //*  041712 END
        //* **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-HOLIDAY-TABLE
        //* **********************************************
        //* **********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-ONE-DAY
    }
    private void sub_Read_Holiday_Table() throws Exception                                                                                                                //Natural: READ-HOLIDAY-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Typ_Ind.setValue("C");                                                                                                      //Natural: ASSIGN #NAZ-TBL-RCRD-TYP-IND := 'C'
        pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl1_Id.setValue("NAZ006");                                                                                                 //Natural: ASSIGN #NAZ-TBL-RCRD-LVL1-ID := 'NAZ006'
        pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl2_Id.setValue("DT");                                                                                                     //Natural: ASSIGN #NAZ-TBL-RCRD-LVL2-ID := 'DT'
        pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl3_Id.setValue(pnd_Passed_Date_Pnd_Passed_Datex);                                                                         //Natural: ASSIGN #NAZ-TBL-RCRD-LVL3-ID := #PASSED-DATEX
        vw_naz_Table_Ddm.startDatabaseRead                                                                                                                                //Natural: READ NAZ-TABLE-DDM BY NAZ-TBL-SUPER3 STARTING FROM #NAZ-TABLE-SUPER3
        (
        "READ01",
        new Wc[] { new Wc("NAZ_TBL_SUPER3", ">=", pnd_Naz_Table_Super3, WcType.BY) },
        new Oc[] { new Oc("NAZ_TBL_SUPER3", "ASC") }
        );
        READ01:
        while (condition(vw_naz_Table_Ddm.readNextRow("READ01")))
        {
            if (condition(naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl1_Id.notEquals(pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl1_Id) || naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl2_Id.notEquals(pnd_Naz_Table_Super3_Pnd_Naz_Tbl_Rcrd_Lvl2_Id))) //Natural: IF NAZ-TABLE-DDM.NAZ-TBL-RCRD-LVL1-ID NE #NAZ-TBL-RCRD-LVL1-ID OR NAZ-TABLE-DDM.NAZ-TBL-RCRD-LVL2-ID NE #NAZ-TBL-RCRD-LVL2-ID
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(!(naz_Table_Ddm_Naz_Tbl_Rcrd_Actv_Ind.equals("Y"))))                                                                                            //Natural: ACCEPT IF NAZ-TABLE-DDM.NAZ-TBL-RCRD-ACTV-IND EQ 'Y'
            {
                continue;
            }
            //*  HOLIDAY
            if (condition(pnd_Passed_Date_Pnd_Passed_Datex.equals(naz_Table_Ddm_Naz_Tbl_Rcrd_Lvl3_Id.getSubstring(1,8))))                                                 //Natural: IF #PASSED-DATEX EQ SUBSTR ( NAZ-TBL-RCRD-LVL3-ID,1,8 )
            {
                                                                                                                                                                          //Natural: PERFORM ADD-ONE-DAY
                sub_Add_One_Day();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Add_One_Day() throws Exception                                                                                                                       //Natural: ADD-ONE-DAY
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************
        pnd_Passed_Date_Pnd_Passed_Dd.nadd(1);                                                                                                                            //Natural: ADD 1 TO #PASSED-DD
        if (condition(pnd_Passed_Date_Pnd_Passed_Mm.equals(2) && pnd_Passed_Date_Pnd_Passed_Dd.equals(30) && pnd_Leap_Year.getBoolean()))                                 //Natural: IF #PASSED-MM = 2 AND #PASSED-DD = 30 AND #LEAP-YEAR
        {
            pnd_Passed_Date_Pnd_Passed_Mm.setValue(3);                                                                                                                    //Natural: ASSIGN #PASSED-MM := 3
            pnd_Passed_Date_Pnd_Passed_Dd.setValue(1);                                                                                                                    //Natural: ASSIGN #PASSED-DD := 1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Passed_Date_Pnd_Passed_Mm.equals(2) && pnd_Passed_Date_Pnd_Passed_Dd.equals(29) && ! (pnd_Leap_Year.getBoolean())))                         //Natural: IF #PASSED-MM = 2 AND #PASSED-DD = 29 AND NOT #LEAP-YEAR
            {
                pnd_Passed_Date_Pnd_Passed_Mm.setValue(3);                                                                                                                //Natural: ASSIGN #PASSED-MM := 3
                pnd_Passed_Date_Pnd_Passed_Dd.setValue(1);                                                                                                                //Natural: ASSIGN #PASSED-DD := 1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Passed_Date_Pnd_Passed_Mm.equals(12) && pnd_Passed_Date_Pnd_Passed_Dd.equals(32)))                                                      //Natural: IF #PASSED-MM = 12 AND #PASSED-DD = 32
                {
                    pnd_Passed_Date_Pnd_Passed_Mm.setValue(1);                                                                                                            //Natural: ASSIGN #PASSED-MM := 1
                    pnd_Passed_Date_Pnd_Passed_Dd.setValue(1);                                                                                                            //Natural: ASSIGN #PASSED-DD := 1
                    pnd_Passed_Date_Pnd_Passed_Ccyy.nadd(1);                                                                                                              //Natural: ADD 1 TO #PASSED-CCYY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition((((((((pnd_Passed_Date_Pnd_Passed_Mm.equals(1) || pnd_Passed_Date_Pnd_Passed_Mm.equals(3)) || pnd_Passed_Date_Pnd_Passed_Mm.equals(5))  //Natural: IF ( ( #PASSED-MM = 1 OR = 3 OR = 5 OR = 7 OR = 8 OR = 10 ) AND #PASSED-DD = 32 ) OR ( ( #PASSED-MM = 4 OR = 6 OR = 9 OR = 11 ) AND #PASSED-DD = 31 )
                        || pnd_Passed_Date_Pnd_Passed_Mm.equals(7)) || pnd_Passed_Date_Pnd_Passed_Mm.equals(8)) || pnd_Passed_Date_Pnd_Passed_Mm.equals(10)) 
                        && pnd_Passed_Date_Pnd_Passed_Dd.equals(32)) || ((((pnd_Passed_Date_Pnd_Passed_Mm.equals(4) || pnd_Passed_Date_Pnd_Passed_Mm.equals(6)) 
                        || pnd_Passed_Date_Pnd_Passed_Mm.equals(9)) || pnd_Passed_Date_Pnd_Passed_Mm.equals(11)) && pnd_Passed_Date_Pnd_Passed_Dd.equals(31)))))
                    {
                        pnd_Passed_Date_Pnd_Passed_Dd.setValue(1);                                                                                                        //Natural: ASSIGN #PASSED-DD := 1
                        pnd_Passed_Date_Pnd_Passed_Mm.nadd(1);                                                                                                            //Natural: ADD 1 TO #PASSED-MM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
