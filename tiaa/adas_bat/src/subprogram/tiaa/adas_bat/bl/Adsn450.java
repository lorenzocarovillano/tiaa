/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:54:19 PM
**        * FROM NATURAL SUBPROGRAM : Adsn450
************************************************************
**        * FILE NAME            : Adsn450.java
**        * CLASS NAME           : Adsn450
**        * INSTANCE NAME        : Adsn450
************************************************************
************************************************************************
*
*  PROGRAM FUNCTIONS:
*
*  - CALLED BY ADSP401
*
*  - CREATE THE IA RESULT RECORD BY CALLING ACTUARIAL TO CALCULATE THE
*    IA BENEFIT INFORMATION
*
*  MAINTENANCE:
*
*  NAME             DATE  DESCRIPTION
*  ----             ----  -----------
* M.NACHBER   08312005    REL 6.5: TPA/IPRO CHANGES
*                         CALLING ACTUARIAL MODULE TWICE FOR TPA's and
*                         IPRO's from TPA'S
* M.NACHBER   01232006    REL 8:  ICAP/TNT CHANGES
*                         MOVE NEW VALUES TO NAZ-ACT-TYPE-OF-CALL
* M.NACHBER   04102006    CHANGES FOR ICAP IPRO/TPA
*
* M.NACHBER   12052006    MIMINUM DISTRIBUTION COMPLIANCE TEST (MDIB)
*                         CHANGED TO CALL A NEW ACTUARIAL MODULE
*                         AIAN034 TO PERFORM COMPLIANCE TEST
* M.NACHBER   03072007    ATRA/KEOGH
* D.GEARY     11212007    NEW MDIB RULE (AIAN033), FROM ADSP070
* O.SOTTO     02202008    TIAA RATE EXPANSION TO 99 OCCURS. SC 022008.
* E. MELNIK   03172008    ROTH 403B/401K CHANGES.  RECOMPILED FOR
*                         NEW ADSA401.CHANGES MARKED RS0
* E. MELNIK   01142009    TIAA ACCESS/STABLE RETURN ENHANCEMENTS. MARKED
*                         BY EM - 011409.
* C. MASON    3/31/2010   PCPOP PROJECT - CHANGES FOR SL OPTION SOLE
*                         PRIMARY BENE. ADD NEW IA ORIGIN CODES /* CM
* C. MASON    05/12/10    ADDED CODE TO CALL NEW IA ORIGIN CODE LOOKUP
* C. MASON    07/09/10    DISPLAY OUTPUT AFTER CALL TO AIAN022
* D.E.ANDER   09/21/10    ADD CALL TO AI-MASTER MARKED DEA PER ED
* O. SOTTO    04/27/2011  RECOMPILED TO GET UPDATED AIAL0330.
* O. SOTTO    12/08/2011  DI HUB CHANGES.  SC 120811.
* O. SOTTO    03/22/2012  RATE EXPANSION.  SC 032212.
*             04/17/2012  REVISED PROD FIX.  SC 041712.
* O. SOTTO    05/06/2013  PROD FIX - ADP-SOLE-PRMRY-BENE-RLTNSHP
*                         AND ADP-SOLE-PRMRY-BENE-DTE-OF-BRTH ARE NOT
*                         PASSED BY ADSP401 SO WE'll have to get these
*                         FROM PARTICIPANT RECORD. SC 050613.
* E. MELNIK   10/03/2013  CREF/REA REDISIGN CHANGES.  MARKED BY
*                         EM - 100313.
* O. SOTTO    03/12/2014  PRODUCTION FIX TO PREVENT ABEND IN AIAN022.
*                         CHANGES MARKED OS - 031214.
* E. MELNIK   05/07/2015  BENE IN THE PLAN CHANGES.
*                         MARKED BY EM - 050715.
* R. CARREON  02/21/2017  RESTOW FOR PIN EXPANSION
*                         CHANGES IN ADSA401, ADSL450, ADS-PRTCPNT
* R. CARREON  01/18/2018  ADDED CHECK FOR 'ROTH INDEXED'
* E. MELNIK   04/12/2019  POPULATED IRC CDE FOR ADSN605 CALL.
*                         MARKED BY EM - 041219.
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn450 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAdsa401 pdaAdsa401;
    private PdaAdsa450 pdaAdsa450;
    private PdaAiaa510 pdaAiaa510;
    private PdaAial0330 pdaAial0330;
    private LdaAdsl450 ldaAdsl450;
    private PdaAdspda_M pdaAdspda_M;
    private PdaAdsa605 pdaAdsa605;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Adp_Scnd_Annt_Rltnshp;
    private DbsField pnd_Return_Message;
    private DbsField pnd_Table_Code;
    private DbsField pnd_Table_Key;

    private DbsGroup pnd_Table_Key__R_Field_1;
    private DbsField pnd_Table_Key_Pnd_Table_Lvl1_Id;
    private DbsField pnd_Table_Key_Pnd_Table_Lvl2_Id;
    private DbsField pnd_Table_Key_Pnd_Table_Lvl3_Id;
    private DbsField pnd_Desc;
    private DbsField pnd_Long_Desc;

    private DbsGroup pnd_Long_Desc__R_Field_2;
    private DbsField pnd_Long_Desc_Pnd_Act_Option_Cde;
    private DbsField pnd_Misc_Fld;
    private DbsField pnd_Max_Rates;
    private DbsField pnd_Max_Rslt;
    private DbsField pnd_Max_Da_Reached;
    private DbsField pnd_Last_Da_Val;
    private DbsField pnd_Max_Std_Reached;
    private DbsField pnd_Last_Std_Val;
    private DbsField pnd_Max_Grd_Reached;
    private DbsField pnd_Last_Grd_Val;
    private DbsField pnd_Total_Standard;
    private DbsField pnd_Total_Graded;
    private DbsField pnd_Total_Guar;
    private DbsField pnd_A;
    private DbsField pnd_P;
    private DbsField pnd_B;
    private DbsField pnd_I;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_3;
    private DbsField pnd_Date_Pnd_Date_Yyyymm;
    private DbsField pnd_Date_Pnd_Date_Dd;

    private DbsGroup pnd_Date__R_Field_4;
    private DbsField pnd_Date_Pnd_Date_A;
    private DbsField pnd_Work_Ipro_Dte;

    private DbsGroup pnd_Work_Ipro_Dte__R_Field_5;
    private DbsField pnd_Work_Ipro_Dte_Pnd_Work_Dt_Cc;
    private DbsField pnd_Work_Ipro_Dte_Pnd_Work_Dt_Yy;
    private DbsField pnd_Work_Ipro_Dte_Pnd_Work_Dt_Mm;
    private DbsField pnd_Work_Ipro_Dte_Pnd_Work_Dt_Dd;

    private DataAccessProgramView vw_cntrct_Prt_Role;
    private DbsField cntrct_Prt_Role_Cntrct_Part_Ppcn_Nbr;
    private DbsField cntrct_Prt_Role_Cntrct_Part_Payee_Cde;
    private DbsField cntrct_Prt_Role_Cntrct_Final_Pay_Dte;
    private DbsField cntrct_Prt_Role_Cntrct_Final_Per_Pay_Dte;

    private DataAccessProgramView vw_ads_P;
    private DbsField ads_P_Rqst_Id;
    private DbsField ads_P_Adp_Sole_Prmry_Bene_Rltnshp;
    private DbsField ads_P_Adp_Sole_Prmry_Bene_Dte_Of_Brth;
    private DbsField pnd_Cp_Key;

    private DbsGroup pnd_Cp_Key__R_Field_6;
    private DbsField pnd_Cp_Key_Pnd_Cntrct_Part_Ppcn_Nbr;
    private DbsField pnd_Cp_Key_Pnd_Cntrct_Part_Payee_Cde;

    private DbsGroup pnd_Mdib_Parms;
    private DbsField pnd_Mdib_Parms_Pnd_Mdib_Option;
    private DbsField pnd_Mdib_Parms_Pnd_Mdib_Annt_Dob;
    private DbsField pnd_Mdib_Parms_Pnd_Mdib_2nd_Annt_Dob;
    private DbsField pnd_Mdib_Parms_Pnd_Mdib_Sttlmnt_Dte;
    private DbsField pnd_Mdib_Parms_Pnd_Mdib_Tot_Tiaa_Accum;
    private DbsField pnd_Mdib_Parms_Pnd_Mdib_Tot_Cref_Accum;
    private DbsField pnd_Mdib_Parms_Pnd_Mdib_Tot_Tiaa_Pymnt;
    private DbsField pnd_Mdib_Parms_Pnd_Mdib_Tot_Cref_Pymnt;
    private DbsField pnd_Mdib_Parms_Pnd_Mdib_2nd_Annt_Type;
    private DbsField pnd_Mdib_Parms_Pnd_Mdib_Pymnt_Mde;
    private DbsField pnd_Mdib_Parms_Pnd_Mdib_Return_Cde;
    private DbsField pnd_Mdib_Parms_Pnd_Mdib_Return_Msg;
    private DbsField pnd_Excluded_Optns;
    private DbsField pnd_Single_Life;
    private DbsField pnd_Two_Life;
    private DbsField pnd_Atra;
    private DbsField pnd_Data_Passed;
    private DbsField pnd_W_Date_A;

    private DbsGroup pnd_W_Date_A__R_Field_7;
    private DbsField pnd_W_Date_A_Pnd_W_Date_Yyyy;
    private DbsField pnd_W_Date_A_Pnd_W_Date_Mm;
    private DbsField pnd_W_Date_A_Pnd_W_Date_Dd;
    private DbsField pnd_Sub_Plan_Prefix;
    private DbsField pls_Debug_On;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAiaa510 = new PdaAiaa510(localVariables);
        pdaAial0330 = new PdaAial0330(localVariables);
        ldaAdsl450 = new LdaAdsl450();
        registerRecord(ldaAdsl450);
        registerRecord(ldaAdsl450.getVw_ads_Ia_Rslt_View());
        pdaAdspda_M = new PdaAdspda_M(localVariables);
        pdaAdsa605 = new PdaAdsa605(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaAdsa401 = new PdaAdsa401(parameters);
        pdaAdsa450 = new PdaAdsa450(parameters);
        pnd_Adp_Scnd_Annt_Rltnshp = parameters.newFieldInRecord("pnd_Adp_Scnd_Annt_Rltnshp", "#ADP-SCND-ANNT-RLTNSHP", FieldType.STRING, 1);
        pnd_Adp_Scnd_Annt_Rltnshp.setParameterOption(ParameterOption.ByReference);
        pnd_Return_Message = parameters.newFieldInRecord("pnd_Return_Message", "#RETURN-MESSAGE", FieldType.STRING, 30);
        pnd_Return_Message.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Table_Code = localVariables.newFieldInRecord("pnd_Table_Code", "#TABLE-CODE", FieldType.STRING, 3);
        pnd_Table_Key = localVariables.newFieldInRecord("pnd_Table_Key", "#TABLE-KEY", FieldType.STRING, 29);

        pnd_Table_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Table_Key__R_Field_1", "REDEFINE", pnd_Table_Key);
        pnd_Table_Key_Pnd_Table_Lvl1_Id = pnd_Table_Key__R_Field_1.newFieldInGroup("pnd_Table_Key_Pnd_Table_Lvl1_Id", "#TABLE-LVL1-ID", FieldType.STRING, 
            6);
        pnd_Table_Key_Pnd_Table_Lvl2_Id = pnd_Table_Key__R_Field_1.newFieldInGroup("pnd_Table_Key_Pnd_Table_Lvl2_Id", "#TABLE-LVL2-ID", FieldType.STRING, 
            3);
        pnd_Table_Key_Pnd_Table_Lvl3_Id = pnd_Table_Key__R_Field_1.newFieldInGroup("pnd_Table_Key_Pnd_Table_Lvl3_Id", "#TABLE-LVL3-ID", FieldType.STRING, 
            20);
        pnd_Desc = localVariables.newFieldInRecord("pnd_Desc", "#DESC", FieldType.STRING, 60);
        pnd_Long_Desc = localVariables.newFieldInRecord("pnd_Long_Desc", "#LONG-DESC", FieldType.STRING, 80);

        pnd_Long_Desc__R_Field_2 = localVariables.newGroupInRecord("pnd_Long_Desc__R_Field_2", "REDEFINE", pnd_Long_Desc);
        pnd_Long_Desc_Pnd_Act_Option_Cde = pnd_Long_Desc__R_Field_2.newFieldInGroup("pnd_Long_Desc_Pnd_Act_Option_Cde", "#ACT-OPTION-CDE", FieldType.NUMERIC, 
            2);
        pnd_Misc_Fld = localVariables.newFieldInRecord("pnd_Misc_Fld", "#MISC-FLD", FieldType.STRING, 80);
        pnd_Max_Rates = localVariables.newFieldInRecord("pnd_Max_Rates", "#MAX-RATES", FieldType.NUMERIC, 3);
        pnd_Max_Rslt = localVariables.newFieldInRecord("pnd_Max_Rslt", "#MAX-RSLT", FieldType.NUMERIC, 3);
        pnd_Max_Da_Reached = localVariables.newFieldInRecord("pnd_Max_Da_Reached", "#MAX-DA-REACHED", FieldType.BOOLEAN, 1);
        pnd_Last_Da_Val = localVariables.newFieldInRecord("pnd_Last_Da_Val", "#LAST-DA-VAL", FieldType.NUMERIC, 3);
        pnd_Max_Std_Reached = localVariables.newFieldInRecord("pnd_Max_Std_Reached", "#MAX-STD-REACHED", FieldType.BOOLEAN, 1);
        pnd_Last_Std_Val = localVariables.newFieldInRecord("pnd_Last_Std_Val", "#LAST-STD-VAL", FieldType.NUMERIC, 3);
        pnd_Max_Grd_Reached = localVariables.newFieldInRecord("pnd_Max_Grd_Reached", "#MAX-GRD-REACHED", FieldType.BOOLEAN, 1);
        pnd_Last_Grd_Val = localVariables.newFieldInRecord("pnd_Last_Grd_Val", "#LAST-GRD-VAL", FieldType.NUMERIC, 3);
        pnd_Total_Standard = localVariables.newFieldArrayInRecord("pnd_Total_Standard", "#TOTAL-STANDARD", FieldType.PACKED_DECIMAL, 14, 2, new DbsArrayController(1, 
            250));
        pnd_Total_Graded = localVariables.newFieldArrayInRecord("pnd_Total_Graded", "#TOTAL-GRADED", FieldType.PACKED_DECIMAL, 14, 2, new DbsArrayController(1, 
            250));
        pnd_Total_Guar = localVariables.newFieldInRecord("pnd_Total_Guar", "#TOTAL-GUAR", FieldType.PACKED_DECIMAL, 14, 2);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 3);
        pnd_P = localVariables.newFieldInRecord("pnd_P", "#P", FieldType.NUMERIC, 3);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.NUMERIC, 8);

        pnd_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Date__R_Field_3", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_Yyyymm = pnd_Date__R_Field_3.newFieldInGroup("pnd_Date_Pnd_Date_Yyyymm", "#DATE-YYYYMM", FieldType.NUMERIC, 6);
        pnd_Date_Pnd_Date_Dd = pnd_Date__R_Field_3.newFieldInGroup("pnd_Date_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);

        pnd_Date__R_Field_4 = localVariables.newGroupInRecord("pnd_Date__R_Field_4", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_A = pnd_Date__R_Field_4.newFieldInGroup("pnd_Date_Pnd_Date_A", "#DATE-A", FieldType.STRING, 8);
        pnd_Work_Ipro_Dte = localVariables.newFieldInRecord("pnd_Work_Ipro_Dte", "#WORK-IPRO-DTE", FieldType.STRING, 8);

        pnd_Work_Ipro_Dte__R_Field_5 = localVariables.newGroupInRecord("pnd_Work_Ipro_Dte__R_Field_5", "REDEFINE", pnd_Work_Ipro_Dte);
        pnd_Work_Ipro_Dte_Pnd_Work_Dt_Cc = pnd_Work_Ipro_Dte__R_Field_5.newFieldInGroup("pnd_Work_Ipro_Dte_Pnd_Work_Dt_Cc", "#WORK-DT-CC", FieldType.NUMERIC, 
            2);
        pnd_Work_Ipro_Dte_Pnd_Work_Dt_Yy = pnd_Work_Ipro_Dte__R_Field_5.newFieldInGroup("pnd_Work_Ipro_Dte_Pnd_Work_Dt_Yy", "#WORK-DT-YY", FieldType.NUMERIC, 
            2);
        pnd_Work_Ipro_Dte_Pnd_Work_Dt_Mm = pnd_Work_Ipro_Dte__R_Field_5.newFieldInGroup("pnd_Work_Ipro_Dte_Pnd_Work_Dt_Mm", "#WORK-DT-MM", FieldType.NUMERIC, 
            2);
        pnd_Work_Ipro_Dte_Pnd_Work_Dt_Dd = pnd_Work_Ipro_Dte__R_Field_5.newFieldInGroup("pnd_Work_Ipro_Dte_Pnd_Work_Dt_Dd", "#WORK-DT-DD", FieldType.NUMERIC, 
            2);

        vw_cntrct_Prt_Role = new DataAccessProgramView(new NameInfo("vw_cntrct_Prt_Role", "CNTRCT-PRT-ROLE"), "IAA_CNTRCT_PRTCPNT_ROLE", "IA_CONTRACT_PART");
        cntrct_Prt_Role_Cntrct_Part_Ppcn_Nbr = vw_cntrct_Prt_Role.getRecord().newFieldInGroup("cntrct_Prt_Role_Cntrct_Part_Ppcn_Nbr", "CNTRCT-PART-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PART_PPCN_NBR");
        cntrct_Prt_Role_Cntrct_Part_Payee_Cde = vw_cntrct_Prt_Role.getRecord().newFieldInGroup("cntrct_Prt_Role_Cntrct_Part_Payee_Cde", "CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PART_PAYEE_CDE");
        cntrct_Prt_Role_Cntrct_Final_Pay_Dte = vw_cntrct_Prt_Role.getRecord().newFieldInGroup("cntrct_Prt_Role_Cntrct_Final_Pay_Dte", "CNTRCT-FINAL-PAY-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PAY_DTE");
        cntrct_Prt_Role_Cntrct_Final_Per_Pay_Dte = vw_cntrct_Prt_Role.getRecord().newFieldInGroup("cntrct_Prt_Role_Cntrct_Final_Per_Pay_Dte", "CNTRCT-FINAL-PER-PAY-DTE", 
            FieldType.NUMERIC, 6, RepeatingFieldStrategy.None, "CNTRCT_FINAL_PER_PAY_DTE");
        registerRecord(vw_cntrct_Prt_Role);

        vw_ads_P = new DataAccessProgramView(new NameInfo("vw_ads_P", "ADS-P"), "ADS_PRTCPNT", "ADS_PRTCPNT");
        ads_P_Rqst_Id = vw_ads_P.getRecord().newFieldInGroup("ads_P_Rqst_Id", "RQST-ID", FieldType.STRING, 35, RepeatingFieldStrategy.None, "RQST_ID");
        ads_P_Adp_Sole_Prmry_Bene_Rltnshp = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Sole_Prmry_Bene_Rltnshp", "ADP-SOLE-PRMRY-BENE-RLTNSHP", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "ADP_SOLE_PRMRY_BENE_RLTNSHP");
        ads_P_Adp_Sole_Prmry_Bene_Dte_Of_Brth = vw_ads_P.getRecord().newFieldInGroup("ads_P_Adp_Sole_Prmry_Bene_Dte_Of_Brth", "ADP-SOLE-PRMRY-BENE-DTE-OF-BRTH", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ADP_SOLE_PRMRY_BENE_DTE_OF_BRTH");
        registerRecord(vw_ads_P);

        pnd_Cp_Key = localVariables.newFieldInRecord("pnd_Cp_Key", "#CP-KEY", FieldType.STRING, 12);

        pnd_Cp_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Cp_Key__R_Field_6", "REDEFINE", pnd_Cp_Key);
        pnd_Cp_Key_Pnd_Cntrct_Part_Ppcn_Nbr = pnd_Cp_Key__R_Field_6.newFieldInGroup("pnd_Cp_Key_Pnd_Cntrct_Part_Ppcn_Nbr", "#CNTRCT-PART-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Cp_Key_Pnd_Cntrct_Part_Payee_Cde = pnd_Cp_Key__R_Field_6.newFieldInGroup("pnd_Cp_Key_Pnd_Cntrct_Part_Payee_Cde", "#CNTRCT-PART-PAYEE-CDE", 
            FieldType.NUMERIC, 2);

        pnd_Mdib_Parms = localVariables.newGroupInRecord("pnd_Mdib_Parms", "#MDIB-PARMS");
        pnd_Mdib_Parms_Pnd_Mdib_Option = pnd_Mdib_Parms.newFieldInGroup("pnd_Mdib_Parms_Pnd_Mdib_Option", "#MDIB-OPTION", FieldType.NUMERIC, 2);
        pnd_Mdib_Parms_Pnd_Mdib_Annt_Dob = pnd_Mdib_Parms.newFieldInGroup("pnd_Mdib_Parms_Pnd_Mdib_Annt_Dob", "#MDIB-ANNT-DOB", FieldType.DATE);
        pnd_Mdib_Parms_Pnd_Mdib_2nd_Annt_Dob = pnd_Mdib_Parms.newFieldInGroup("pnd_Mdib_Parms_Pnd_Mdib_2nd_Annt_Dob", "#MDIB-2ND-ANNT-DOB", FieldType.DATE);
        pnd_Mdib_Parms_Pnd_Mdib_Sttlmnt_Dte = pnd_Mdib_Parms.newFieldInGroup("pnd_Mdib_Parms_Pnd_Mdib_Sttlmnt_Dte", "#MDIB-STTLMNT-DTE", FieldType.DATE);
        pnd_Mdib_Parms_Pnd_Mdib_Tot_Tiaa_Accum = pnd_Mdib_Parms.newFieldInGroup("pnd_Mdib_Parms_Pnd_Mdib_Tot_Tiaa_Accum", "#MDIB-TOT-TIAA-ACCUM", FieldType.NUMERIC, 
            11, 2);
        pnd_Mdib_Parms_Pnd_Mdib_Tot_Cref_Accum = pnd_Mdib_Parms.newFieldInGroup("pnd_Mdib_Parms_Pnd_Mdib_Tot_Cref_Accum", "#MDIB-TOT-CREF-ACCUM", FieldType.NUMERIC, 
            11, 2);
        pnd_Mdib_Parms_Pnd_Mdib_Tot_Tiaa_Pymnt = pnd_Mdib_Parms.newFieldInGroup("pnd_Mdib_Parms_Pnd_Mdib_Tot_Tiaa_Pymnt", "#MDIB-TOT-TIAA-PYMNT", FieldType.NUMERIC, 
            11, 2);
        pnd_Mdib_Parms_Pnd_Mdib_Tot_Cref_Pymnt = pnd_Mdib_Parms.newFieldInGroup("pnd_Mdib_Parms_Pnd_Mdib_Tot_Cref_Pymnt", "#MDIB-TOT-CREF-PYMNT", FieldType.NUMERIC, 
            11, 2);
        pnd_Mdib_Parms_Pnd_Mdib_2nd_Annt_Type = pnd_Mdib_Parms.newFieldInGroup("pnd_Mdib_Parms_Pnd_Mdib_2nd_Annt_Type", "#MDIB-2ND-ANNT-TYPE", FieldType.STRING, 
            1);
        pnd_Mdib_Parms_Pnd_Mdib_Pymnt_Mde = pnd_Mdib_Parms.newFieldInGroup("pnd_Mdib_Parms_Pnd_Mdib_Pymnt_Mde", "#MDIB-PYMNT-MDE", FieldType.STRING, 1);
        pnd_Mdib_Parms_Pnd_Mdib_Return_Cde = pnd_Mdib_Parms.newFieldInGroup("pnd_Mdib_Parms_Pnd_Mdib_Return_Cde", "#MDIB-RETURN-CDE", FieldType.NUMERIC, 
            3);
        pnd_Mdib_Parms_Pnd_Mdib_Return_Msg = pnd_Mdib_Parms.newFieldInGroup("pnd_Mdib_Parms_Pnd_Mdib_Return_Msg", "#MDIB-RETURN-MSG", FieldType.STRING, 
            30);
        pnd_Excluded_Optns = localVariables.newFieldArrayInRecord("pnd_Excluded_Optns", "#EXCLUDED-OPTNS", FieldType.STRING, 2, new DbsArrayController(1, 
            6));
        pnd_Single_Life = localVariables.newFieldArrayInRecord("pnd_Single_Life", "#SINGLE-LIFE", FieldType.STRING, 2, new DbsArrayController(1, 3));
        pnd_Two_Life = localVariables.newFieldArrayInRecord("pnd_Two_Life", "#TWO-LIFE", FieldType.STRING, 2, new DbsArrayController(1, 4));
        pnd_Atra = localVariables.newFieldArrayInRecord("pnd_Atra", "#ATRA", FieldType.STRING, 3, new DbsArrayController(1, 7));
        pnd_Data_Passed = localVariables.newFieldInRecord("pnd_Data_Passed", "#DATA-PASSED", FieldType.STRING, 1);
        pnd_W_Date_A = localVariables.newFieldInRecord("pnd_W_Date_A", "#W-DATE-A", FieldType.STRING, 8);

        pnd_W_Date_A__R_Field_7 = localVariables.newGroupInRecord("pnd_W_Date_A__R_Field_7", "REDEFINE", pnd_W_Date_A);
        pnd_W_Date_A_Pnd_W_Date_Yyyy = pnd_W_Date_A__R_Field_7.newFieldInGroup("pnd_W_Date_A_Pnd_W_Date_Yyyy", "#W-DATE-YYYY", FieldType.NUMERIC, 4);
        pnd_W_Date_A_Pnd_W_Date_Mm = pnd_W_Date_A__R_Field_7.newFieldInGroup("pnd_W_Date_A_Pnd_W_Date_Mm", "#W-DATE-MM", FieldType.NUMERIC, 2);
        pnd_W_Date_A_Pnd_W_Date_Dd = pnd_W_Date_A__R_Field_7.newFieldInGroup("pnd_W_Date_A_Pnd_W_Date_Dd", "#W-DATE-DD", FieldType.NUMERIC, 2);
        pnd_Sub_Plan_Prefix = localVariables.newFieldInRecord("pnd_Sub_Plan_Prefix", "#SUB-PLAN-PREFIX", FieldType.STRING, 3);
        pls_Debug_On = WsIndependent.getInstance().newFieldInRecord("pls_Debug_On", "+DEBUG-ON", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cntrct_Prt_Role.reset();
        vw_ads_P.reset();

        ldaAdsl450.initializeValues();

        parameters.reset();
        localVariables.reset();
        pnd_Max_Rates.setInitialValue(250);
        pnd_Max_Rslt.setInitialValue(125);
        pnd_Excluded_Optns.getValue(1).setInitialValue("TC");
        pnd_Excluded_Optns.getValue(2).setInitialValue("TE");
        pnd_Excluded_Optns.getValue(3).setInitialValue("TR");
        pnd_Excluded_Optns.getValue(4).setInitialValue("IO");
        pnd_Excluded_Optns.getValue(5).setInitialValue("AC");
        pnd_Excluded_Optns.getValue(6).setInitialValue("IR");
        pnd_Single_Life.getValue(1).setInitialValue("SL");
        pnd_Single_Life.getValue(2).setInitialValue("AC");
        pnd_Single_Life.getValue(3).setInitialValue("IR");
        pnd_Two_Life.getValue(1).setInitialValue("LF");
        pnd_Two_Life.getValue(2).setInitialValue("LH");
        pnd_Two_Life.getValue(3).setInitialValue("J");
        pnd_Two_Life.getValue(4).setInitialValue("LT");
        pnd_Atra.getValue(1).setInitialValue("AA1");
        pnd_Atra.getValue(2).setInitialValue("IPA");
        pnd_Atra.getValue(3).setInitialValue("AA2");
        pnd_Atra.getValue(4).setInitialValue("AA3");
        pnd_Atra.getValue(5).setInitialValue("IPB");
        pnd_Atra.getValue(6).setInitialValue("TPA");
        pnd_Atra.getValue(7).setInitialValue("TPB");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn450() throws Exception
    {
        super("Adsn450");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //* ***************************************************
        //*  WRITE 'PROCESSING PROGRAM:' *PROGRAM
        //*  FORMAT PS=24 SG=OFF                /* 08312005 MN
        //*  08312005 MN
        //*                                                                                                                                                               //Natural: FORMAT LS = 120 PS = 100 SG = OFF
                                                                                                                                                                          //Natural: PERFORM SETUP-FOR-ACT-CALL
        sub_Setup_For_Act_Call();
        if (condition(Global.isEscape())) {return;}
        //*  OS - 031214 START
        if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().notEquals(" ")))                                                                                       //Natural: IF #ERROR-STATUS NE ' '
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  OS - 031214 END
        //* *IF NAZ-ACT-RETURN-CODE NE 0                      /* 032212 START
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().notEquals(getZero())))                                                              //Natural: IF NAZ-ACT-RETURN-CODE-NBR NE 0
        {
            //* *MOVE NAZ-ACT-RETURN-CODE TO ADC-ERR-CDE (1)
            pdaAdsa401.getPnd_Adsa401_Adc_Err_Cde().getValue(1).setValueEdited(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr(),new ReportEditMask("Z99")); //Natural: MOVE EDITED NAZ-ACT-RETURN-CODE-NBR ( EM = Z99 ) TO ADC-ERR-CDE ( 1 )
            //*  DECIDE ON FIRST VALUE OF NAZ-ACT-RETURN-CODE  /* START DG 08/30/2007
            //*  032212 END
            short decideConditionsMet805 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF NAZ-ACT-RETURN-CODE-NBR;//Natural: VALUE 100
            if (condition((pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(100))))
            {
                decideConditionsMet805++;
                pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L64");                                                                                             //Natural: ASSIGN #ERROR-STATUS := 'L64'
            }                                                                                                                                                             //Natural: VALUE 212
            else if (condition((pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().equals(212))))
            {
                decideConditionsMet805++;
                pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L65");                                                                                             //Natural: ASSIGN #ERROR-STATUS := 'L65'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L15");                                                                                             //Natural: ASSIGN #ERROR-STATUS := 'L15'
            }                                                                                                                                                             //Natural: END-DECIDE
            //* *MOVE 'L15' TO #ERROR-STATUS                   /* END   DG 08/30/2007
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //* * EM - 100313 - START
            //* * IF TOTAL TIAA ACCUMULATION > 0 AND TOTAL TIAA GUARANTEED PAYMENT = 0
            //* * REJECT THE REQUEST.
            if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Actl_Amt().getValue("*").greater(getZero()) || pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Actl_Amt().getValue("*").greater(getZero()))) //Natural: IF #TIAA-STNDRD-ACTL-AMT ( * ) > 0 OR #TIAA-GRADED-ACTL-AMT ( * ) > 0
            {
                pnd_Total_Guar.reset();                                                                                                                                   //Natural: RESET #TOTAL-GUAR
                pnd_Total_Guar.nadd(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Guar_Amt().getValue("*"));                                                   //Natural: ASSIGN #TOTAL-GUAR := #TOTAL-GUAR + NAZ-ACT-TIAA-O-GRD-GUAR-AMT ( * )
                pnd_Total_Guar.nadd(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Guar_Amt().getValue("*"));                                                   //Natural: ASSIGN #TOTAL-GUAR := #TOTAL-GUAR + NAZ-ACT-TIAA-O-STD-GUAR-AMT ( * )
                if (condition(pnd_Total_Guar.equals(getZero())))                                                                                                          //Natural: IF #TOTAL-GUAR = 0
                {
                    pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L79");                                                                                         //Natural: ASSIGN #ERROR-STATUS := 'L79'
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* * EM - 100313 - END
            //* * 08312005 MN START
            //*  EXPANDED ACTUARIAL IA PAYOUT CALCULATION PROCESS IS REQUIRED FOR TPA
            //*  AND 'IPRO from TPA'.  IT CONSISTS OF 2 STEPS.  THE FIRST STEP
            //*  REPRESENTS CURRENT PROCESSING AND IS APPLICABLE TO THE ANNUITIZATION
            //*  OF ALL CONTRACTS WHILE THE SECOND STEP IS NEW AND SPECIFICALLY APPLIES
            //*  TO TPA AND 'IPRO (IO) from TPA' CONTRACTS.
            if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Lob().equals("TPA") || (pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Lob().equals("IPRO") && pdaAdsa401.getPnd_Adsa401_Adc_Orig_Lob_Ind().equals("1")))) //Natural: IF #ADSA401.#CNTRCT-LOB = 'TPA' OR ( #ADSA401.#CNTRCT-LOB = 'IPRO' AND #ADSA401.ADC-ORIG-LOB-IND = '1' )
            {
                                                                                                                                                                          //Natural: PERFORM SETUP-FOR-ACT-CALL2
                sub_Setup_For_Act_Call2();
                if (condition(Global.isEscape())) {return;}
                //*  OS - 031214 START
                if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().notEquals(" ")))                                                                               //Natural: IF #ERROR-STATUS NE ' '
                {
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
                //*  OS - 031214 END
                //*    IF NAZ-ACT-RETURN-CODE NE 0                  /* 032212 START
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().notEquals(getZero())))                                                      //Natural: IF NAZ-ACT-RETURN-CODE-NBR NE 0
                {
                    //*      MOVE NAZ-ACT-RETURN-CODE TO ADC-ERR-CDE (1)
                    //*  032212 END
                    pdaAdsa401.getPnd_Adsa401_Adc_Err_Cde().getValue(1).setValueEdited(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr(),new                //Natural: MOVE EDITED NAZ-ACT-RETURN-CODE-NBR ( EM = Z99 ) TO ADC-ERR-CDE ( 1 )
                        ReportEditMask("Z99"));
                    pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L33");                                                                                         //Natural: MOVE 'L33' TO #ERROR-STATUS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  EM - 011409 START
                    if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().equals(" ")))                                                                              //Natural: IF #ERROR-STATUS = ' '
                    {
                                                                                                                                                                          //Natural: PERFORM CREATE-IA-RSLT
                        sub_Create_Ia_Rslt();
                        if (condition(Global.isEscape())) {return;}
                        //*  EM - 011409 - END
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* * 08312005 MN END
                                                                                                                                                                          //Natural: PERFORM CREATE-IA-RSLT
                sub_Create_Ia_Rslt();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-FOR-ACT-CALL
        //*  EM - 100313
        //*  EM - 041219
        //* * 08312005  MN  START
        //* * 08312005  MN  START
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-FOR-ACT-CALL2
        //*  FOR EACH VINTAGE, THE GUARANTEED AMOUNT FROM THE SECOND CALL WILL BE
        //*  SUBTRACTED FROM THE TOTAL AMOUNT FROM THE FIRST CALL TO DERIVE THE
        //*  DIVIDEND AMOUNT.  SUBSEQUENTLY, THIS DIVIDEND AMOUNT AND THE
        //*  GUARANTEED AMOUNT FROM THE SECOND CALL WILL BE PASSED TO IA.
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-NAZ-ACT-CALC-INPUT-AREA
        //* * 1 NAZ-ACT-CALC-INPUT-AREA
        //*    NAZ-ACT-TIAA-GRADED-INFO(1:80)
        //*     NAZ-ACT-TIAA-STD-INFO(1:80)
        //*     NAZ-ACT-CREF-ANNUAL-INFO(1:20)
        //*     NAZ-ACT-CREF-MTHLY-INFO(1:20)
        //* * 08312005  MN  END
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-NAZ-ACT-CALC-RETURN-AREA
        //* * 1 NAZ-ACT-CALC-RETURN-AREA
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-IA-MASTER
        //*    AND (#TIAA-STNDRD-ACTL-AMT (*) GT 0
        //*    OR #TIAA-GRADED-ACTL-AMT (*) GT 0)
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-IA-RSLT
        //* * 08312005 MN END
        //* * 12052006 MN START
        //* ************************************************************050406*****
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TEST-MDIB-AIAN033
        //*  CM 3/31 END
        //* ************************************************************050406*****
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TEST-MDIB-AIAN034
        //*  ADI-DTL-TIAA-DA-STD-AMT(*)
        //*  ADI-DTL-TIAA-DA-GRD-AMT(*)
        //*  #MDIB-TOT-TIAA-PYMNT + ADI-DTL-TIAA-GRD-GRNTD-AMT (*)
        //*  #MDIB-TOT-TIAA-PYMNT + ADI-DTL-TIAA-GRD-DVDND-AMT (*)
        //*  #MDIB-TOT-TIAA-PYMNT + ADI-DTL-TIAA-STD-GRNTD-AMT (*)
        //*  #MDIB-TOT-TIAA-PYMNT + ADI-DTL-TIAA-STD-DVDND-AMT (*)
        //*   GETTING PAYMENT FREQUENCY
        //*    +RETURN-CODE    := '02'
    }
    private void sub_Setup_For_Act_Call() throws Exception                                                                                                                //Natural: SETUP-FOR-ACT-CALL
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //*  MOVE FIELDS FROM PDA TO NAZL510 TO CALL FOR IA BENEFITS
        pdaAiaa510.getNaz_Act_Calc_Input_Area().reset();                                                                                                                  //Natural: RESET NAZ-ACT-CALC-INPUT-AREA
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Cntrct_Nmbr().getValue("*").setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_All_Tiaa_Contracts().getValue(1, //Natural: MOVE #ALL-TIAA-CONTRACTS ( 1:6 ) TO NAZ-ACT-TIAA-CNTRCT-NMBR ( * )
            ":",6));
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Cert_Nmbr().getValue("*").setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_All_Cref_Contracts().getValue(1, //Natural: MOVE #ALL-CREF-CONTRACTS ( 1:6 ) TO NAZ-ACT-CREF-CERT-NMBR ( * )
            ":",6));
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Ia_Cntrct_Nmbr().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Ia_Tiaa_Nbr());                                        //Natural: MOVE ADP-IA-TIAA-NBR TO NAZ-ACT-TIAA-IA-CNTRCT-NMBR
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Ia_Cert_Nmbr().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Ia_Cref_Nbr());                                          //Natural: MOVE ADP-IA-CREF-NBR TO NAZ-ACT-CREF-IA-CERT-NMBR
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Curr_Bsnss_Date().setValue(pdaAdsa401.getPnd_Adsa401_Pnd_Cntl_Bsnss_Dte());                                         //Natural: MOVE #CNTL-BSNSS-DTE TO NAZ-ACT-CURR-BSNSS-DATE
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Asd_Date().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Annty_Strt_Dte());                                                //Natural: MOVE ADP-ANNTY-STRT-DTE TO NAZ-ACT-ASD-DATE
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Eff_Date().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Effctv_Dte());                                                    //Natural: MOVE ADP-EFFCTV-DTE TO NAZ-ACT-EFF-DATE
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ia_Next_Pymt_Dte().setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Ia_Next_Cycle_Check_Dte());                //Natural: MOVE #IA-NEXT-CYCLE-CHECK-DTE TO NAZ-ACT-IA-NEXT-PYMT-DTE
        //*  CM - REPLACE CALL TYPE LOGIC WITH CALL TO ORIGIN CODE ROUTINE
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Annt_Typ_Cde().equals("M")))                                                                                          //Natural: IF ADP-ANNT-TYP-CDE = 'M'
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().setValue("1");                                                                                   //Natural: ASSIGN NAZ-ACT-TYPE-OF-CALL := '1'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().setValue("2");                                                                                   //Natural: ASSIGN NAZ-ACT-TYPE-OF-CALL := '2'
            //*  DEA PER ED
                                                                                                                                                                          //Natural: PERFORM READ-IA-MASTER
            sub_Read_Ia_Master();
            if (condition(Global.isEscape())) {return;}
            //*  OS - 031214 START
            if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().notEquals(" ")))                                                                                   //Natural: IF #ERROR-STATUS NE ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            //*  OS - 031214 END
        }                                                                                                                                                                 //Natural: END-IF
        //* CM - SKIP IF ORGN CODE PREV SET
        if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Ia_Origin_Code().equals(getZero())))                                                                                  //Natural: IF #ADSA401.#IA-ORIGIN-CODE EQ 0
        {
            //*  SET PARAMETERS FOR CALL TO ADSA605
            pdaAdsa605.getPnd_Adsa605().reset();                                                                                                                          //Natural: RESET #ADSA605
            pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Plan_Nbr());                                                                  //Natural: ASSIGN #PLAN-NBR := #ADSA401.ADP-PLAN-NBR
            pdaAdsa605.getPnd_Adsa605_Pnd_Sub_Plan_Nbr().setValue(pdaAdsa401.getPnd_Adsa401_Adc_Sub_Plan_Nbr());                                                          //Natural: ASSIGN #SUB-PLAN-NBR := #ADSA401.ADC-SUB-PLAN-NBR
            pdaAdsa605.getPnd_Adsa605_Pnd_Acct_Code().getValue("*").setValue(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue("*"));                                     //Natural: ASSIGN #ACCT-CODE ( * ) := #ADSA401.ADC-ACCT-CDE ( * )
            pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue("*").setValue(pdaAdsa401.getPnd_Adsa401_Adp_Cntrcts_In_Rqst().getValue("*"));                          //Natural: ASSIGN #CONTRACT-NBRS ( * ) := #ADSA401.ADP-CNTRCTS-IN-RQST ( * )
            pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Lob_Ind().getValue(1).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Orig_Lob_Ind());                                            //Natural: ASSIGN #ORIGIN-LOB-IND ( 1 ) := #ADSA401.ADC-ORIG-LOB-IND
            pdaAdsa605.getPnd_Adsa605_Pnd_T395_Fund_Id().getValue("*").setValue(pdaAdsa401.getPnd_Adsa401_Adc_T395_Fund_Id().getValue("*"));                              //Natural: ASSIGN #T395-FUND-ID ( * ) := #ADSA401.ADC-T395-FUND-ID ( * )
            pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Roth_Plan_Type());                                                      //Natural: ASSIGN #ROTH-PLAN-TYPE := ADP-ROTH-PLAN-TYPE
            pdaAdsa605.getPnd_Adsa605_Pnd_Invstmnt_Grpng_Id().getValue("*").setValue(pdaAdsa401.getPnd_Adsa401_Adc_Invstmnt_Grpng_Id().getValue("*"));                    //Natural: ASSIGN #INVSTMNT-GRPNG-ID ( * ) := #ADSA401.ADC-INVSTMNT-GRPNG-ID ( * )
            pdaAdsa605.getPnd_Adsa605_Pnd_Adp_Irc_Cde().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Irc_Cde());                                                                //Natural: ASSIGN #ADP-IRC-CDE := #ADSA401.ADP-IRC-CDE
            if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Roth_Rqst_Ind().notEquals("Y")))                                                                                  //Natural: IF ADP-ROTH-RQST-IND NE 'Y'
            {
                pdaAdsa605.getPnd_Adsa605_Pnd_Tiaa_Orgn_Cde().setValue(true);                                                                                             //Natural: ASSIGN #TIAA-ORGN-CDE := TRUE
                //*  120811
                DbsUtil.callnat(Adsn605.class , getCurrentProcessState(), pdaAdsa605.getPnd_Adsa605());                                                                   //Natural: CALLNAT 'ADSN605' #ADSA605
                if (condition(Global.isEscape())) return;
                pdaAdsa401.getPnd_Adsa401_Pnd_Ia_Origin_Code().setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code());                                                     //Natural: ASSIGN #ADSA401.#IA-ORIGIN-CODE := NAZ-ACT-ORIGIN-CDE := #ADSA605.#ORIGIN-CODE
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Origin_Cde().setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code());
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Tiaa_Orgn_Cde().setValue(true);                                                                                        //Natural: ASSIGN #ROTH-TIAA-ORGN-CDE := TRUE
                //*  120811
                DbsUtil.callnat(Adsn605.class , getCurrentProcessState(), pdaAdsa605.getPnd_Adsa605());                                                                   //Natural: CALLNAT 'ADSN605' #ADSA605
                if (condition(Global.isEscape())) return;
                pdaAdsa401.getPnd_Adsa401_Pnd_Ia_Origin_Code().setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code());                                                     //Natural: ASSIGN #ADSA401.#IA-ORIGIN-CODE := NAZ-ACT-ORIGIN-CDE := #ADSA605.#ORIGIN-CODE
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Origin_Cde().setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code());
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Origin_Cde().setValue(pdaAdsa401.getPnd_Adsa401_Pnd_Ia_Origin_Code());                                          //Natural: ASSIGN NAZ-ACT-ORIGIN-CDE := #ADSA401.#IA-ORIGIN-CODE
        }                                                                                                                                                                 //Natural: END-IF
        //*  CM- END
        //* * EM - 011409 START
        //*  IF ADP-ANNT-TYP-CDE = 'M'                        /* NEW ISSUE
        //*    RESET NAZ-ACT-FNL-PAY-DATE
        //*   DECIDE FOR FIRST CONDITION
        //*     WHEN #ADSA401.ADC-ACCT-CDE (*) = 'Y'         /* STABLE RETURN
        //*       NAZ-ACT-TYPE-OF-CALL := 'Q'
        //*     WHEN #ADSA401.#CNTRCT-LOB = 'RS' OR = 'IP7'  /* ICAP RS AND
        //*         OR = 'TP7'                               /* ICAP TPA/IPRO
        //*       NAZ-ACT-TYPE-OF-CALL := 'C'
        //*     WHEN #ADSA401.#CNTRCT-LOB = 'RP'             /* ICAP RP
        //*       NAZ-ACT-TYPE-OF-CALL := 'E'
        //*     WHEN #ADSA401.#CNTRCT-LOB = 'SA' OR = 'SP'  /* TNT
        //*         OR = 'S2'
        //*       NAZ-ACT-TYPE-OF-CALL := 'N'
        //*     WHEN #ADSA401.#CNTRCT-LOB = 'TPA'            /* TPA
        //*       NAZ-ACT-TYPE-OF-CALL := 'T'
        //*     WHEN #ADSA401.#CNTRCT-LOB = 'IPRO' AND       /* IPRO FROM TPA
        //*         #ADSA401.ADC-ORIG-LOB-IND = '1'
        //*       NAZ-ACT-TYPE-OF-CALL := 'P'
        //*     WHEN SUBSTRING (ADC-SUB-PLAN-NBR,1,3) = 'AA1'  /* ATRA, ATRA IPRO
        //*         OR = 'IPA' OR = 'AA2' OR = 'SS1'
        //*         OR = 'AA3' OR = 'IPB'
        //*       NAZ-ACT-TYPE-OF-CALL := 'I'
        //*     WHEN SUBSTRING(ADC-SUB-PLAN-NBR,1,3)  = 'TPA'
        //*         OR = 'TPB'
        //*       NAZ-ACT-TYPE-OF-CALL := 'T'
        //*     WHEN NONE
        //*       NAZ-ACT-TYPE-OF-CALL := 'I'
        //*   END-DECIDE
        //*  ELSE                                             /* RESETTLEMENT
        //*   PERFORM READ-IA-MASTER
        //*   DECIDE FOR FIRST CONDITION
        //*     WHEN #ADSA401.ADC-ACCT-CDE (*) = 'Y'         /* STABLE RETURN
        //*       NAZ-ACT-TYPE-OF-CALL := 'R'
        //*     WHEN #ADSA401.#CNTRCT-LOB = 'RS'             /* ICAP RS
        //*       NAZ-ACT-TYPE-OF-CALL := 'B'
        //*     WHEN #ADSA401.#CNTRCT-LOB = 'RP'             /* ICAP RP
        //*       NAZ-ACT-TYPE-OF-CALL := 'D'
        //*     WHEN NONE
        //*       NAZ-ACT-TYPE-OF-CALL := 'F'
        //*   END-DECIDE
        //*  END-IF
        //* * EM - 011409 END
        //* ** THE FF. LINES ARE ADDED PER ED MELNIK's instructions 6-7-04
        pnd_Table_Code.setValue("AO");                                                                                                                                    //Natural: MOVE 'AO' TO #TABLE-CODE
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Grntee_Period().equals(getZero()) || pdaAdsa401.getPnd_Adsa401_Adp_Annty_Optn().equals("AC")))                        //Natural: IF ADP-GRNTEE-PERIOD EQ 0 OR ADP-ANNTY-OPTN EQ 'AC'
        {
            pnd_Table_Key.setValue(pdaAdsa401.getPnd_Adsa401_Adp_Annty_Optn());                                                                                           //Natural: MOVE ADP-ANNTY-OPTN TO #TABLE-KEY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Table_Key.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaAdsa401.getPnd_Adsa401_Adp_Annty_Optn(), pdaAdsa401.getPnd_Adsa401_Adp_Grntee_Period())); //Natural: COMPRESS ADP-ANNTY-OPTN ADP-GRNTEE-PERIOD TO #TABLE-KEY LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Adsn032.class , getCurrentProcessState(), pdaAdspda_M.getMsg_Info_Sub(), pnd_Table_Code, pnd_Table_Key, pnd_Desc, pnd_Long_Desc,                  //Natural: CALLNAT 'ADSN032' MSG-INFO-SUB #TABLE-CODE #TABLE-KEY #DESC #LONG-DESC #MISC-FLD
            pnd_Misc_Fld);
        if (condition(Global.isEscape())) return;
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde().setValue(pnd_Long_Desc_Pnd_Act_Option_Cde);                                                            //Natural: MOVE #ACT-OPTION-CDE TO NAZ-ACT-OPTION-CDE
        //* **
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Pymnt_Mode().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Pymnt_Mode());                                                  //Natural: MOVE ADP-PYMNT-MODE TO NAZ-ACT-PYMNT-MODE
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Grntee_Period());                                              //Natural: MOVE ADP-GRNTEE-PERIOD TO NAZ-ACT-GUAR-PERIOD
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ann_Dt_Of_Brth().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Frst_Annt_Dte_Of_Brth());                                   //Natural: MOVE ADP-FRST-ANNT-DTE-OF-BRTH TO NAZ-ACT-ANN-DT-OF-BRTH
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Frst_Annt_Sex_Cde().equals("M")))                                                                                     //Natural: IF ADP-FRST-ANNT-SEX-CDE = 'M'
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ann_Sex().setValue("1");                                                                                        //Natural: MOVE '1' TO NAZ-ACT-ANN-SEX
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ann_Sex().setValue("2");                                                                                        //Natural: MOVE '2' TO NAZ-ACT-ANN-SEX
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_2nd_Ann_Dt_Of_Brth().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Scnd_Annt_Dte_Of_Brth());                               //Natural: MOVE ADP-SCND-ANNT-DTE-OF-BRTH TO NAZ-ACT-2ND-ANN-DT-OF-BRTH
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Scnd_Annt_Sex_Cde().equals("M")))                                                                                     //Natural: IF ADP-SCND-ANNT-SEX-CDE = 'M'
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_2nd_Ann_Sex().setValue("1");                                                                                    //Natural: MOVE '1' TO NAZ-ACT-2ND-ANN-SEX
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_2nd_Ann_Sex().setValue("2");                                                                                    //Natural: MOVE '2' TO NAZ-ACT-2ND-ANN-SEX
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Actl_Amt().getValue("*").greater(getZero())))                                              //Natural: IF #TIAA-STNDRD-ACTL-AMT ( * ) GT 0
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Call().setValue("Y");                                                                                  //Natural: MOVE 'Y' TO NAZ-ACT-TIAA-STD-CALL
            //*  022008 START
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(1,":",pnd_Max_Rates).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Actl_Amt().getValue(1, //Natural: MOVE #TIAA-STNDRD-ACTL-AMT ( 1:#MAX-RATES ) TO NAZ-ACT-TIAA-STD-RATE-AMT ( 1:#MAX-RATES )
                ":",pnd_Max_Rates));
            //*  022008 END
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Cde().getValue(1,":",pnd_Max_Rates).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde().getValue(1, //Natural: MOVE #TIAA-RATE-CDE ( 1:#MAX-RATES ) TO NAZ-ACT-TIAA-STD-RATE-CDE ( 1:#MAX-RATES )
                ":",pnd_Max_Rates));
            //*  032212
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Gic_Code().getValue(1,":",pnd_Max_Rates).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Gic().getValue(1, //Natural: MOVE #TIAA-GIC ( 1:#MAX-RATES ) TO NAZ-ACT-TIAA-STD-GIC-CODE ( 1:#MAX-RATES )
                ":",pnd_Max_Rates));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Call().setValue("N");                                                                                  //Natural: MOVE 'N' TO NAZ-ACT-TIAA-STD-CALL
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Actl_Amt().getValue("*").greater(getZero())))                                              //Natural: IF #TIAA-GRADED-ACTL-AMT ( * ) GT 0
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Call().setValue("Y");                                                                               //Natural: MOVE 'Y' TO NAZ-ACT-TIAA-GRADED-CALL
            //*  022008 START
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Amt().getValue(1,":",pnd_Max_Rates).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Actl_Amt().getValue(1, //Natural: MOVE #TIAA-GRADED-ACTL-AMT ( 1:#MAX-RATES ) TO NAZ-ACT-TIAA-GRADED-RATE-AMT ( 1:#MAX-RATES )
                ":",pnd_Max_Rates));
            //*  022008 END
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Cde().getValue(1,":",pnd_Max_Rates).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde().getValue(1, //Natural: MOVE #TIAA-RATE-CDE ( 1:#MAX-RATES ) TO NAZ-ACT-TIAA-GRADED-RATE-CDE ( 1:#MAX-RATES )
                ":",pnd_Max_Rates));
            //*  032212
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Gic_Code().getValue(1,":",pnd_Max_Rates).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Gic().getValue(1, //Natural: MOVE #TIAA-GIC ( 1:#MAX-RATES ) TO NAZ-ACT-TIAA-GRADED-GIC-CODE ( 1:#MAX-RATES )
                ":",pnd_Max_Rates));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Call().setValue("N");                                                                               //Natural: MOVE 'N' TO NAZ-ACT-TIAA-GRADED-CALL
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Stndrd_Annual_Actl_Amt().getValue("*").greater(getZero())))                                       //Natural: IF #CREF-STNDRD-ANNUAL-ACTL-AMT ( * ) GT 0
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Call().setValue("Y");                                                                               //Natural: MOVE 'Y' TO NAZ-ACT-CREF-ANNUAL-CALL
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Amt().getValue(1,":",20).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Stndrd_Annual_Actl_Amt().getValue(1, //Natural: MOVE #CREF-STNDRD-ANNUAL-ACTL-AMT ( 1:20 ) TO NAZ-ACT-CREF-ANNUAL-RATE-AMT ( 1:20 )
                ":",20));
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO #FUND-CNT
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaAdsa401.getPnd_Adsa401_Pnd_Fund_Cnt())); pnd_I.nadd(1))
            {
                if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Stndrd_Annual_Actl_Amt().getValue(pnd_I).notEquals(getZero())))                           //Natural: IF #CREF-STNDRD-ANNUAL-ACTL-AMT ( #I ) NE 0
                {
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(pnd_I).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Rate_Cde().getValue(pnd_I)); //Natural: MOVE #CREF-RATE-CDE ( #I ) TO NAZ-ACT-CREF-ANNUAL-RATE-CDE ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Call().setValue("N");                                                                               //Natural: MOVE 'N' TO NAZ-ACT-CREF-ANNUAL-CALL
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Graded_Mnthly_Actl_Amt().getValue("*").greater(getZero())))                                       //Natural: IF #CREF-GRADED-MNTHLY-ACTL-AMT ( * ) GT 0
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Call().setValue("Y");                                                                                //Natural: MOVE 'Y' TO NAZ-ACT-CREF-MTHLY-CALL
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Amt().getValue(1,":",20).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Graded_Mnthly_Actl_Amt().getValue(1, //Natural: MOVE #CREF-GRADED-MNTHLY-ACTL-AMT ( 1:20 ) TO NAZ-ACT-CREF-MTHLY-RATE-AMT ( 1:20 )
                ":",20));
            FOR02:                                                                                                                                                        //Natural: FOR #I = 1 TO #FUND-CNT
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaAdsa401.getPnd_Adsa401_Pnd_Fund_Cnt())); pnd_I.nadd(1))
            {
                if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Graded_Mnthly_Actl_Amt().getValue(pnd_I).notEquals(getZero())))                           //Natural: IF #CREF-GRADED-MNTHLY-ACTL-AMT ( #I ) NE 0
                {
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(pnd_I).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Rate_Cde().getValue(pnd_I)); //Natural: MOVE #CREF-RATE-CDE ( #I ) TO NAZ-ACT-CREF-MTHLY-RATE-CDE ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Call().setValue("N");                                                                                //Natural: MOVE 'N' TO NAZ-ACT-CREF-MTHLY-CALL
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tpa_Ipro_Issue_Date().setValue(pdaAdsa401.getPnd_Adsa401_Adc_Cntrct_Issue_Dte());                                   //Natural: ASSIGN NAZ-ACT-TPA-IPRO-ISSUE-DATE := #ADSA401.ADC-CNTRCT-ISSUE-DTE
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Sub_Plan_Code().setValue(pdaAdsa401.getPnd_Adsa401_Adc_Sub_Plan_Nbr());                                             //Natural: ASSIGN NAZ-ACT-SUB-PLAN-CODE := #ADSA401.ADC-SUB-PLAN-NBR
        //*  DG  08/31/2007
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_State_At_Issue().setValue(pdaAdsa401.getPnd_Adsa401_Adp_State_Of_Issue());                                          //Natural: MOVE ADP-STATE-OF-ISSUE TO NAZ-ACT-STATE-AT-ISSUE
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
            getReports().write(0, "***************    ADSN450  ***************",NEWLINE,"********   CALLING  AIAN022 FIRST TIME *********");                              //Natural: WRITE '***************    ADSN450  ***************' /'********   CALLING  AIAN022 FIRST TIME *********'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM DISPLAY-NAZ-ACT-CALC-INPUT-AREA
            sub_Display_Naz_Act_Calc_Input_Area();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* * 08312005  MN  END
        DbsUtil.callnat(Aian022.class , getCurrentProcessState(), pdaAiaa510.getNaz_Act_Calc_Input_Area());                                                               //Natural: CALLNAT 'AIAN022' NAZ-ACT-CALC-INPUT-AREA
        if (condition(Global.isEscape())) return;
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
            getReports().write(0, NEWLINE,"********   AFTER  AIAN022 FIRST TIME *********");                                                                              //Natural: WRITE /'********   AFTER  AIAN022 FIRST TIME *********'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM DISPLAY-NAZ-ACT-CALC-RETURN-AREA
            sub_Display_Naz_Act_Calc_Return_Area();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Setup_For_Act_Call2() throws Exception                                                                                                               //Natural: SETUP-FOR-ACT-CALL2
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //*  CALL AIAN022 AGAIN USING THE GUARANTEED COMMUTED VALUE AMOUNTS BY
        //*  VINTAGE, DIVIDED INTO STANDARD AND GRADED COMPONENTS.  THE MODULE WILL
        //*  PASS BACK ANOTHER SET OF GUARANTEED AND DIVIDEND IA PAYOUT AMOUNTS.
        //*  AFTER THE FIRST CALL, AIAN022 PASSED BACK THE GUARANTEED AND
        //*  DIVIDEND IA PAYOUT AMOUNTS. THESE ARE TO SUMMARIZE INTO A TOTALS
        //*  CM - REPLACE CALL TYPE LOGIC WITH CALL TO ORIGIN CODE ROUTINE
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Annt_Typ_Cde().equals("M")))                                                                                          //Natural: IF ADP-ANNT-TYP-CDE = 'M'
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().setValue("1");                                                                                   //Natural: ASSIGN NAZ-ACT-TYPE-OF-CALL := '1'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call().setValue("2");                                                                                   //Natural: ASSIGN NAZ-ACT-TYPE-OF-CALL := '2'
            //*  022008
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tpa_Ipro_Issue_Date().setValue(pdaAdsa401.getPnd_Adsa401_Adc_Cntrct_Issue_Dte());                                   //Natural: ASSIGN NAZ-ACT-TPA-IPRO-ISSUE-DATE := #ADSA401.ADC-CNTRCT-ISSUE-DTE
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Sub_Plan_Code().setValue(pdaAdsa401.getPnd_Adsa401_Adc_Sub_Plan_Nbr());                                             //Natural: ASSIGN NAZ-ACT-SUB-PLAN-CODE := #ADSA401.ADC-SUB-PLAN-NBR
        FOR03:                                                                                                                                                            //Natural: FOR #A 1 #MAX-RATES
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Rates)); pnd_A.nadd(1))
        {
            //*  041712 START
            if (condition(((pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Rate_Cde().getValue(pnd_A).equals(" ") || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Rate_Cde().getValue(pnd_A).equals("00"))  //Natural: IF NAZ-ACT-TIAA-O-STD-RATE-CDE ( #A ) EQ ' ' OR EQ '00' AND NAZ-ACT-TIAA-O-GRD-RATE-CDE ( #A ) EQ ' ' OR EQ '00'
                && (pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Rate_Cde().getValue(pnd_A).equals(" ") || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Rate_Cde().getValue(pnd_A).equals("00")))))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  041712 END
            }                                                                                                                                                             //Natural: END-IF
            pnd_Total_Standard.getValue(pnd_A).compute(new ComputeParameters(false, pnd_Total_Standard.getValue(pnd_A)), pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Guar_Amt().getValue(pnd_A).add(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Divd_Amt().getValue(pnd_A))); //Natural: COMPUTE #TOTAL-STANDARD ( #A ) = NAZ-ACT-TIAA-O-STD-GUAR-AMT ( #A ) + NAZ-ACT-TIAA-O-STD-DIVD-AMT ( #A )
            pnd_Total_Graded.getValue(pnd_A).compute(new ComputeParameters(false, pnd_Total_Graded.getValue(pnd_A)), pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Guar_Amt().getValue(pnd_A).add(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Divd_Amt().getValue(pnd_A))); //Natural: COMPUTE #TOTAL-GRADED ( #A ) = NAZ-ACT-TIAA-O-GRD-GUAR-AMT ( #A ) + NAZ-ACT-TIAA-O-GRD-DIVD-AMT ( #A )
            //*  OS - COMMENTED-OUT THE FOLLOWING               /* 041712 START
            //*  IF NAZ-ACT-TIAA-O-STD-GUAR-AMT(#A) = 0 AND    /* RS0
            //*      NAZ-ACT-TIAA-O-STD-DIVD-AMT(#A) = 0 AND
            //*      NAZ-ACT-TIAA-O-GRD-GUAR-AMT(#A) = 0 AND
            //*      NAZ-ACT-TIAA-O-GRD-DIVD-AMT(#A) = 0
            //*    ESCAPE BOTTOM
            //*  END-IF                                         /* 041712 END
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Guar_Comm_Amt().getValue("*").greater(getZero())))                                         //Natural: IF #TIAA-STNDRD-GUAR-COMM-AMT ( * ) GT 0
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Call().setValue("Y");                                                                                  //Natural: MOVE 'Y' TO NAZ-ACT-TIAA-STD-CALL
            //*  022008 START
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(1,":",pnd_Max_Rates).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Guar_Comm_Amt().getValue(1, //Natural: MOVE #TIAA-STNDRD-GUAR-COMM-AMT ( 1:#MAX-RATES ) TO NAZ-ACT-TIAA-STD-RATE-AMT ( 1:#MAX-RATES )
                ":",pnd_Max_Rates));
            //*  022008 END
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Cde().getValue(1,":",pnd_Max_Rates).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde().getValue(1, //Natural: MOVE #TIAA-RATE-CDE ( 1:#MAX-RATES ) TO NAZ-ACT-TIAA-STD-RATE-CDE ( 1:#MAX-RATES )
                ":",pnd_Max_Rates));
            //*  032212
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Gic_Code().getValue(1,":",pnd_Max_Rates).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Gic().getValue(1, //Natural: MOVE #TIAA-GIC ( 1:#MAX-RATES ) TO NAZ-ACT-TIAA-STD-GIC-CODE ( 1:#MAX-RATES )
                ":",pnd_Max_Rates));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Call().setValue("N");                                                                                  //Natural: MOVE 'N' TO NAZ-ACT-TIAA-STD-CALL
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Guar_Comm_Amt().getValue("*").greater(getZero())))                                         //Natural: IF #TIAA-GRADED-GUAR-COMM-AMT ( * ) GT 0
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Call().setValue("Y");                                                                               //Natural: MOVE 'Y' TO NAZ-ACT-TIAA-GRADED-CALL
            //*  022008 START
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Amt().getValue(1,":",pnd_Max_Rates).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Guar_Comm_Amt().getValue(1, //Natural: MOVE #TIAA-GRADED-GUAR-COMM-AMT ( 1:#MAX-RATES ) TO NAZ-ACT-TIAA-GRADED-RATE-AMT ( 1:#MAX-RATES )
                ":",pnd_Max_Rates));
            //*  022008 END
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Cde().getValue(1,":",pnd_Max_Rates).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde().getValue(1, //Natural: MOVE #TIAA-RATE-CDE ( 1:#MAX-RATES ) TO NAZ-ACT-TIAA-GRADED-RATE-CDE ( 1:#MAX-RATES )
                ":",pnd_Max_Rates));
            //*  032212
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Gic_Code().getValue(1,":",pnd_Max_Rates).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Gic().getValue(1, //Natural: MOVE #TIAA-GIC ( 1:#MAX-RATES ) TO NAZ-ACT-TIAA-GRADED-GIC-CODE ( 1:#MAX-RATES )
                ":",pnd_Max_Rates));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Call().setValue("N");                                                                               //Natural: MOVE 'N' TO NAZ-ACT-TIAA-GRADED-CALL
        }                                                                                                                                                                 //Natural: END-IF
        //*  DG  08/31/2007
        pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_State_At_Issue().setValue(pdaAdsa401.getPnd_Adsa401_Adp_State_Of_Issue());                                          //Natural: MOVE ADP-STATE-OF-ISSUE TO NAZ-ACT-STATE-AT-ISSUE
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
            getReports().write(0, "***************    ADSN450  ***************",NEWLINE,"********   CALLING  AIAN022 SECOND TIME *********");                             //Natural: WRITE '***************    ADSN450  ***************' /'********   CALLING  AIAN022 SECOND TIME *********'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM DISPLAY-NAZ-ACT-CALC-INPUT-AREA
            sub_Display_Naz_Act_Calc_Input_Area();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Aian022.class , getCurrentProcessState(), pdaAiaa510.getNaz_Act_Calc_Input_Area());                                                               //Natural: CALLNAT 'AIAN022' NAZ-ACT-CALC-INPUT-AREA
        if (condition(Global.isEscape())) return;
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
            getReports().write(0, NEWLINE,"********   AFTER  AIAN022 SECOND TIME *********");                                                                             //Natural: WRITE /'********   AFTER  AIAN022 SECOND TIME *********'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM DISPLAY-NAZ-ACT-CALC-RETURN-AREA
            sub_Display_Naz_Act_Calc_Return_Area();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* * NAZ-ACT-RETURN-CODE NE 0                     /* EM - 011409 START
        //*  032212
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code_Nbr().notEquals(getZero())))                                                              //Natural: IF NAZ-ACT-RETURN-CODE-NBR NE 0
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
            //*  EM - 011409 - END
            //*  022008
        }                                                                                                                                                                 //Natural: END-IF
        FOR04:                                                                                                                                                            //Natural: FOR #A 1 #MAX-RATES
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Rates)); pnd_A.nadd(1))
        {
            //*  041712 START
            if (condition(((pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Rate_Cde().getValue(pnd_A).equals(" ") || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Rate_Cde().getValue(pnd_A).equals("00"))  //Natural: IF NAZ-ACT-TIAA-O-STD-RATE-CDE ( #A ) EQ ' ' OR EQ '00' AND NAZ-ACT-TIAA-O-GRD-RATE-CDE ( #A ) EQ ' ' OR EQ '00'
                && (pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Rate_Cde().getValue(pnd_A).equals(" ") || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Rate_Cde().getValue(pnd_A).equals("00")))))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  041712 END
            }                                                                                                                                                             //Natural: END-IF
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Divd_Amt().getValue(pnd_A).compute(new ComputeParameters(false, pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Divd_Amt().getValue(pnd_A)),  //Natural: COMPUTE NAZ-ACT-TIAA-O-STD-DIVD-AMT ( #A ) = #TOTAL-STANDARD ( #A ) - NAZ-ACT-TIAA-O-STD-GUAR-AMT ( #A )
                pnd_Total_Standard.getValue(pnd_A).subtract(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Guar_Amt().getValue(pnd_A)));
            //*  EM - 011409 START
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Divd_Amt().getValue(pnd_A).less(getZero())))                                           //Natural: IF NAZ-ACT-TIAA-O-STD-DIVD-AMT ( #A ) < 0
            {
                pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L72");                                                                                             //Natural: ASSIGN #ERROR-STATUS := 'L72'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
                //*  EM - 011409 END
            }                                                                                                                                                             //Natural: END-IF
            pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Divd_Amt().getValue(pnd_A).compute(new ComputeParameters(false, pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Divd_Amt().getValue(pnd_A)),  //Natural: COMPUTE NAZ-ACT-TIAA-O-GRD-DIVD-AMT ( #A ) = #TOTAL-GRADED ( #A ) - NAZ-ACT-TIAA-O-GRD-GUAR-AMT ( #A )
                pnd_Total_Graded.getValue(pnd_A).subtract(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Guar_Amt().getValue(pnd_A)));
            //*  EM - 011409 START
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Divd_Amt().getValue(pnd_A).less(getZero())))                                           //Natural: IF NAZ-ACT-TIAA-O-GRD-DIVD-AMT ( #A ) < 0
            {
                pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L72");                                                                                             //Natural: ASSIGN #ERROR-STATUS := 'L72'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
                //*  EM - 011409 END
            }                                                                                                                                                             //Natural: END-IF
            //*  OS - COMMENTED-OUT THE FOLLOWING               /* 041712 START
            //*  IF #TOTAL-STANDARD(#A) = 0             AND             /* RS0
            //*      NAZ-ACT-TIAA-O-STD-GUAR-AMT(#A) = 0 AND
            //*      #TOTAL-GRADED(#A) = 0               AND
            //*      NAZ-ACT-TIAA-O-GRD-GUAR-AMT(#A) = 0
            //*    ESCAPE BOTTOM
            //*  END-IF                                         /* 041712 END
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Display_Naz_Act_Calc_Input_Area() throws Exception                                                                                                   //Natural: DISPLAY-NAZ-ACT-CALC-INPUT-AREA
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(0, "*",new RepeatItem(100));                                                                                                                   //Natural: WRITE '*' ( 100 )
        if (Global.isEscape()) return;
        //*  DG  08/31/2007
        //*  CM  5/25/2010
        //*  022008
        getReports().write(0, "=====> VALUES IN NAZL510 PARM BEFORE CALL TO AIAN022 < =====",NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Cntrct_Nmbr().getValue("*"),NEWLINE,new  //Natural: WRITE '=====> VALUES IN NAZL510 PARM BEFORE CALL TO AIAN022 < =====' /5T '=' NAZ-ACT-TIAA-CNTRCT-NMBR ( * ) /5T '=' NAZ-ACT-CREF-CERT-NMBR ( * ) /5T '=' NAZ-ACT-TIAA-IA-CNTRCT-NMBR 50T '=' NAZ-ACT-CREF-IA-CERT-NMBR /5T '=' NAZ-ACT-SUB-PLAN-CODE 50T '=' NAZ-ACT-TPA-IPRO-ISSUE-DATE ( EM = MM/DD/YYYY ) /5T '=' NAZ-ACT-CURR-BSNSS-DATE ( EM = MM/DD/YYYY ) 50T '=' NAZ-ACT-ASD-DATE ( EM = MM/DD/YYYY ) /5T '=' NAZ-ACT-TYPE-OF-CALL 50T '=' NAZ-ACT-OPTION-CDE /5T '=' NAZ-ACT-GUAR-PERIOD 50T '=' NAZ-ACT-GUAR-PERIOD-MTH /5T '=' NAZ-ACT-PYMNT-MODE 50T '=' NAZ-ACT-ANN-DT-OF-BRTH ( EM = MM/DD/YYYY ) /5T '=' NAZ-ACT-ANN-SEX 50T '=' NAZ-ACT-2ND-ANN-DT-OF-BRTH ( EM = MM/DD/YYYY ) /5T '=' NAZ-ACT-2ND-ANN-SEX 50T '=' NAZ-ACT-TIAA-GRADED-CALL /5T '=' NAZ-ACT-TIAA-STD-CALL 50T '=' NAZ-ACT-CREF-ANNUAL-CALL /5T '=' NAZ-ACT-CREF-MTHLY-CALL 50T '=' NAZ-ACT-STATE-AT-ISSUE /5T '=' NAZ-ACT-ORIGIN-CDE
            TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Cert_Nmbr().getValue("*"),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Ia_Cntrct_Nmbr(),new 
            TabSetting(50),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Ia_Cert_Nmbr(),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Sub_Plan_Code(),new 
            TabSetting(50),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tpa_Ipro_Issue_Date(), new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Curr_Bsnss_Date(), 
            new ReportEditMask ("MM/DD/YYYY"),new TabSetting(50),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Asd_Date(), new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new 
            TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Type_Of_Call(),new TabSetting(50),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde(),NEWLINE,new 
            TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period(),new TabSetting(50),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Guar_Period_Mth(),NEWLINE,new 
            TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Pymnt_Mode(),new TabSetting(50),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ann_Dt_Of_Brth(), 
            new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ann_Sex(),new TabSetting(50),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_2nd_Ann_Dt_Of_Brth(), 
            new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_2nd_Ann_Sex(),new TabSetting(50),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Call(),NEWLINE,new 
            TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Call(),new TabSetting(50),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Call(),NEWLINE,new 
            TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Call(),new TabSetting(50),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_State_At_Issue(),NEWLINE,new 
            TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Origin_Cde());
        if (Global.isEscape()) return;
        FOR05:                                                                                                                                                            //Natural: FOR #P 1 TO #MAX-RATES
        for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(pnd_Max_Rates)); pnd_P.nadd(1))
        {
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Cde().getValue(pnd_P).equals(" ")))                                              //Natural: IF NAZ-ACT-TIAA-GRADED-RATE-CDE ( #P ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  032212
                getReports().write(0, NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Cde().getValue(pnd_P),new              //Natural: WRITE /5T '=' NAZ-ACT-TIAA-GRADED-RATE-CDE ( #P ) 40T '=' NAZ-ACT-TIAA-GRADED-RATE-AMT ( #P ) /5T '=' NAZ-ACT-TIAA-GRADED-GIC-CODE ( #P )
                    TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Rate_Amt().getValue(pnd_P),NEWLINE,new TabSetting(5),"=",
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Graded_Gic_Code().getValue(pnd_P));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  022008
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR06:                                                                                                                                                            //Natural: FOR #P 1 TO #MAX-RATES
        for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(pnd_Max_Rates)); pnd_P.nadd(1))
        {
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Cde().getValue(pnd_P).equals(" ")))                                                 //Natural: IF NAZ-ACT-TIAA-STD-RATE-CDE ( #P ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  032212
                getReports().write(0, NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Cde().getValue(pnd_P),new                 //Natural: WRITE /5T '=' NAZ-ACT-TIAA-STD-RATE-CDE ( #P ) 40T '=' NAZ-ACT-TIAA-STD-RATE-AMT ( #P ) /5T '=' NAZ-ACT-TIAA-STD-GIC-CODE ( #P )
                    TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Rate_Amt().getValue(pnd_P),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Gic_Code().getValue(pnd_P));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR07:                                                                                                                                                            //Natural: FOR #P 1 TO 20
        for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(20)); pnd_P.nadd(1))
        {
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(pnd_P).equals(" ")))                                              //Natural: IF NAZ-ACT-CREF-ANNUAL-RATE-CDE ( #P ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Cde().getValue(pnd_P),new              //Natural: WRITE /5T '=' NAZ-ACT-CREF-ANNUAL-RATE-CDE ( #P ) 40T '=' NAZ-ACT-CREF-ANNUAL-RATE-AMT ( #P )
                    TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Annual_Rate_Amt().getValue(pnd_P));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR08:                                                                                                                                                            //Natural: FOR #P 1 TO 20
        for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(20)); pnd_P.nadd(1))
        {
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(pnd_P).equals(" ")))                                               //Natural: IF NAZ-ACT-CREF-MTHLY-RATE-CDE ( #P ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Cde().getValue(pnd_P),new               //Natural: WRITE /5T '=' NAZ-ACT-CREF-MTHLY-RATE-CDE ( #P ) 40T '=' NAZ-ACT-CREF-MTHLY-RATE-AMT ( #P )
                    TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_Mthly_Rate_Amt().getValue(pnd_P));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Ia_Next_Pymt_Dte(),new TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Eff_Date(),NEWLINE,new  //Natural: WRITE /5T '=' NAZ-ACT-IA-NEXT-PYMT-DTE 40T '=' NAZ-ACT-EFF-DATE /5T '=' NAZ-ACT-FNL-PAY-DATE 40T '=' NAZ-ACT-EXPANSION-AREA /5T '=' NAZ-ACT-RETURN-CODE
            TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Fnl_Pay_Date(),new TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Expansion_Area(),NEWLINE,new 
            TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code());
        if (Global.isEscape()) return;
    }
    private void sub_Display_Naz_Act_Calc_Return_Area() throws Exception                                                                                                  //Natural: DISPLAY-NAZ-ACT-CALC-RETURN-AREA
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        getReports().write(0, "*",new RepeatItem(100));                                                                                                                   //Natural: WRITE '*' ( 100 )
        if (Global.isEscape()) return;
        getReports().write(0, "=====> VALUES IN NAZL510 PARM AFTER CALL TO AIAN022 < =====",NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Code(),NEWLINE,new  //Natural: WRITE '=====> VALUES IN NAZL510 PARM AFTER CALL TO AIAN022 < =====' /5T '=' NAZ-ACT-RETURN-CODE /5T '=' NAZ-ACT-RETURN-ERROR-MSG /5T '=' NAZ-ACT-STD-FNL-PP-PAY-DTE ( EM = MM/DD/YYYY ) 50T '=' NAZ-ACT-STD-FNL-PAY-DTE ( EM = MM/DD/YYYY )
            TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Return_Error_Msg(),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Std_Fnl_Pp_Pay_Dte(), 
            new ReportEditMask ("MM/DD/YYYY"),new TabSetting(50),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Std_Fnl_Pay_Dte(), new ReportEditMask 
            ("MM/DD/YYYY"));
        if (Global.isEscape()) return;
        FOR09:                                                                                                                                                            //Natural: FOR #P 1 TO #MAX-RATES
        for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(pnd_Max_Rates)); pnd_P.nadd(1))
        {
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Rate_Cde().getValue(pnd_P).equals(" ")))                                               //Natural: IF NAZ-ACT-TIAA-O-GRD-RATE-CDE ( #P ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  032212
                //*  032212
                //*  032212
                //*  032212
                getReports().write(0, NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Rate_Cde().getValue(pnd_P),new               //Natural: WRITE /5T '=' NAZ-ACT-TIAA-O-GRD-RATE-CDE ( #P ) 40T '=' NAZ-ACT-TIAA-O-GRD-GUAR-AMT ( #P ) /5T '=' NAZ-ACT-TIAA-O-GRD-GIC-CODE ( #P ) /5T '=' NAZ-ACT-TIAA-O-GRD-DIVD-AMT ( #P ) 45T '=' NAZ-ACT-TIAA-GRD-EFF-RATE-INT ( #P ) /5T '=' NAZ-MODAL-ADJUSTMENT ( #P ) 50T '=' NAZ-MODAL-NUMERATOR ( #P ) /5T '=' NAZ-MODAL-DENOMINATOR ( #P )
                    TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Guar_Amt().getValue(pnd_P),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Gic_Code().getValue(pnd_P),NEWLINE,new 
                    TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Divd_Amt().getValue(pnd_P),new TabSetting(45),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Grd_Eff_Rate_Int().getValue(pnd_P),NEWLINE,new 
                    TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Modal_Adjustment().getValue(pnd_P),new TabSetting(50),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Modal_Numerator().getValue(pnd_P),NEWLINE,new 
                    TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Modal_Denominator().getValue(pnd_P));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR10:                                                                                                                                                            //Natural: FOR #P 1 TO #MAX-RATES
        for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(pnd_Max_Rates)); pnd_P.nadd(1))
        {
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Rate_Cde().getValue(pnd_P).equals(" ")))                                               //Natural: IF NAZ-ACT-TIAA-O-STD-RATE-CDE ( #P ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  032212
                getReports().write(0, NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Rate_Cde().getValue(pnd_P),new               //Natural: WRITE /5T '=' NAZ-ACT-TIAA-O-STD-RATE-CDE ( #P ) 40T '=' NAZ-ACT-TIAA-O-STD-GUAR-AMT ( #P ) /5T '=' NAZ-ACT-TIAA-O-STD-GIC-CODE ( #P ) /5T '=' NAZ-ACT-TIAA-O-STD-DIVD-AMT ( #P ) 45T '=' NAZ-ACT-TIAA-STD-EFF-RATE-INT ( #P ) /5T '=' NAZ-ACT-TIAA-STD-FNL-GUAR-AMT ( #P ) 50T '=' NAZ-ACT-TIAA-STD-FNL-DIVD-AMT ( #P )
                    TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Guar_Amt().getValue(pnd_P),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Gic_Code().getValue(pnd_P),NEWLINE,new 
                    TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Divd_Amt().getValue(pnd_P),new TabSetting(45),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Eff_Rate_Int().getValue(pnd_P),NEWLINE,new 
                    TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Fnl_Guar_Amt().getValue(pnd_P),new TabSetting(50),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Fnl_Divd_Amt().getValue(pnd_P));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR11:                                                                                                                                                            //Natural: FOR #P 1 TO 20
        for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(20)); pnd_P.nadd(1))
        {
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Rate_Cde().getValue(pnd_P).equals(" ")))                                               //Natural: IF NAZ-ACT-CREF-O-ANN-RATE-CDE ( #P ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Rate_Cde().getValue(pnd_P),new               //Natural: WRITE /5T '=' NAZ-ACT-CREF-O-ANN-RATE-CDE ( #P ) 40T '=' NAZ-ACT-CREF-O-ANN-RATE-AMT ( #P ) /5T '=' NAZ-ACT-CREF-O-ANN-PYMNT-UNITS ( #P ) 50T '=' NAZ-ACT-CREF-O-ANN-UNIT-VALUE ( #P )
                    TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Rate_Amt().getValue(pnd_P),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Pymnt_Units().getValue(pnd_P),new 
                    TabSetting(50),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Unit_Value().getValue(pnd_P));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR12:                                                                                                                                                            //Natural: FOR #P 1 TO 20
        for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(20)); pnd_P.nadd(1))
        {
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Rate_Cde().getValue(pnd_P).equals(" ")))                                               //Natural: IF NAZ-ACT-CREF-O-MTH-RATE-CDE ( #P ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Rate_Cde().getValue(pnd_P),new               //Natural: WRITE /5T '=' NAZ-ACT-CREF-O-MTH-RATE-CDE ( #P ) 40T '=' NAZ-ACT-CREF-O-MTH-RATE-AMT ( #P ) /5T '=' NAZ-ACT-CREF-O-MTH-PYMNT-UNITS ( #P ) 50T '=' NAZ-ACT-CREF-O-MTH-UNIT-VALUE ( #P )
                    TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Rate_Amt().getValue(pnd_P),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Pymnt_Units().getValue(pnd_P),new 
                    TabSetting(50),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Unit_Value().getValue(pnd_P));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR13:                                                                                                                                                            //Natural: FOR #P 1 TO 20
        for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(20)); pnd_P.nadd(1))
        {
            getReports().write(0, NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Cref_Mortality().getValue(pnd_P),new TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Cref_Int_Rate().getValue(pnd_P),NEWLINE,new  //Natural: WRITE /5T '=' NAZ-CREF-MORTALITY ( #P ) 40T '=' NAZ-CREF-INT-RATE ( #P ) /5T '=' NAZ-CREF-SETBACK ( #P ) 40T '=' NAZ-AGE1-CREF ( #P ) /5T '=' NAZ-AGE2-CREF ( #P ) 40T '=' NAZ-CREF-ANN-FACTOR ( #P ) /5T '=' NAZ-CREF-AIAN021-LINKAGE-ARRAY ( #P )
                TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Cref_Setback().getValue(pnd_P),new TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age1_Cref().getValue(pnd_P),NEWLINE,new 
                TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age2_Cref().getValue(pnd_P),new TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Cref_Ann_Factor().getValue(pnd_P),NEWLINE,new 
                TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Cref_Aian021_Linkage_Array().getValue(pnd_P));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR14:                                                                                                                                                            //Natural: FOR #P 1 TO #MAX-RATES
        for (pnd_P.setValue(1); condition(pnd_P.lessOrEqual(pnd_Max_Rates)); pnd_P.nadd(1))
        {
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Interest_Rate_Grd().getValue(pnd_P).greater(getZero())))                                         //Natural: IF NAZ-GUAR-INTEREST-RATE-GRD ( #P ) GT 0
            {
                getReports().write(0, NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Mort_Table_Grd().getValue(pnd_P),new                   //Natural: WRITE /5T '=' NAZ-GUAR-MORT-TABLE-GRD ( #P ) 40T '=' NAZ-GUAR-INTEREST-RATE-GRD ( #P ) /5T '=' NAZ-GUAR-SET-BACK-GRD ( #P ) 40T '=' NAZ-TOTAL-MORT-TABLE-GRD ( #P ) /5T '=' NAZ-AGE1-GUAR-GRD ( #P ) 40T '=' NAZ-AGE2-GUAR-GRD ( #P ) /5T '=' NAZ-GUAR-ANN-FACTOR-GRD ( #P ) /5T '=' NAZ-GUAR-AIAN021-LINKAGE-GRD ( #P ) /5T '=' NAZ-TOTAL-MORT-TABLE-GRD ( #P ) 40T '=' NAZ-TOTAL-INTEREST-RATE-GRD ( #P ) /5T '='NAZ-TOTAL-SET-BACK-GRD ( #P ) /5T '=' NAZ-AGE1-TOTAL-GRD ( #P ) 40T '=' NAZ-AGE2-TOTAL-GRD ( #P ) /5T '=' NAZ-TOTAL-ANN-FACTOR-GRD ( #P ) /5T '=' NAZ-TOTAL-AIAN021-LINKAGE-GRD ( #P ) /5T '=' NAZ-GUAR-MORT-TABLE ( #P ) 40T '=' NAZ-GUAR-INTEREST-RATE ( #P ) /5T '=' NAZ-GUAR-SET-BACK ( #P ) 40T '=' NAZ-AGE1-GUAR ( #P ) /5T '=' NAZ-AGE2-GUAR ( #P ) 40T '=' NAZ-GUAR-ANN-FACTOR ( #P ) /5T '=' NAZ-GUAR-AIAN021-LINKAGE-ARRAY ( #P ) /5T '=' NAZ-TOTAL-MORT-TABLE ( #P ) 40T '=' NAZ-TOTAL-INTEREST-RATE ( #P ) /5T '=' NAZ-TOTAL-SET-BACK ( #P ) 40T '=' NAZ-AGE1-TOTAL ( #P ) /5T '=' NAZ-AGE2-TOTAL ( #P ) 40T '=' NAZ-TOTAL-ANN-FACTOR ( #P ) /5T '=' NAZ-TOTAL-AIAN021-LINKAGE-ARRAY ( #P )
                    TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Interest_Rate_Grd().getValue(pnd_P),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Set_Back_Grd().getValue(pnd_P),new 
                    TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Mort_Table_Grd().getValue(pnd_P),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age1_Guar_Grd().getValue(pnd_P),new 
                    TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age2_Guar_Grd().getValue(pnd_P),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Ann_Factor_Grd().getValue(pnd_P),NEWLINE,new 
                    TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Aian021_Linkage_Grd().getValue(pnd_P),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Mort_Table_Grd().getValue(pnd_P),new 
                    TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Interest_Rate_Grd().getValue(pnd_P),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Set_Back_Grd().getValue(pnd_P),NEWLINE,new 
                    TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age1_Total_Grd().getValue(pnd_P),new TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age2_Total_Grd().getValue(pnd_P),NEWLINE,new 
                    TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Ann_Factor_Grd().getValue(pnd_P),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Aian021_Linkage_Grd().getValue(pnd_P),NEWLINE,new 
                    TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Mort_Table().getValue(pnd_P),new TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Interest_Rate().getValue(pnd_P),NEWLINE,new 
                    TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Set_Back().getValue(pnd_P),new TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age1_Guar().getValue(pnd_P),NEWLINE,new 
                    TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age2_Guar().getValue(pnd_P),new TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Ann_Factor().getValue(pnd_P),NEWLINE,new 
                    TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Guar_Aian021_Linkage_Array().getValue(pnd_P),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Mort_Table().getValue(pnd_P),new 
                    TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Interest_Rate().getValue(pnd_P),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Set_Back().getValue(pnd_P),new 
                    TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age1_Total().getValue(pnd_P),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Age2_Total().getValue(pnd_P),new 
                    TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Ann_Factor().getValue(pnd_P),NEWLINE,new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Total_Aian021_Linkage_Array().getValue(pnd_P));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, new TabSetting(5),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Deferral_Period(),new TabSetting(40),"=",pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Certain_Period()); //Natural: WRITE 5T '=' NAZ-DEFERRAL-PERIOD 40T '=' NAZ-CERTAIN-PERIOD
        if (Global.isEscape()) return;
        //*  032212 END
    }
    private void sub_Read_Ia_Master() throws Exception                                                                                                                    //Natural: READ-IA-MASTER
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //*  041712
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Ia_Tiaa_Nbr().notEquals(" ") && (pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Account_T().equals("Y")                 //Natural: IF ADP-IA-TIAA-NBR NE ' ' AND ( #ACCOUNT-T = 'Y' OR #ACCOUNT-R = 'Y' )
            || pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Account_R().equals("Y"))))
        {
            pnd_Cp_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(pdaAdsa401.getPnd_Adsa401_Adp_Ia_Tiaa_Nbr());                                                                    //Natural: MOVE ADP-IA-TIAA-NBR TO #CNTRCT-PART-PPCN-NBR
            pnd_Cp_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(1);                                                                                                             //Natural: MOVE 01 TO #CNTRCT-PART-PAYEE-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Cp_Key_Pnd_Cntrct_Part_Ppcn_Nbr.setValue(pdaAdsa401.getPnd_Adsa401_Adp_Ia_Cref_Nbr());                                                                    //Natural: MOVE ADP-IA-CREF-NBR TO #CNTRCT-PART-PPCN-NBR
            pnd_Cp_Key_Pnd_Cntrct_Part_Payee_Cde.setValue(1);                                                                                                             //Natural: MOVE 01 TO #CNTRCT-PART-PAYEE-CDE
        }                                                                                                                                                                 //Natural: END-IF
        vw_cntrct_Prt_Role.startDatabaseFind                                                                                                                              //Natural: FIND CNTRCT-PRT-ROLE WITH CNTRCT-PAYEE-KEY = #CP-KEY
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PAYEE_KEY", "=", pnd_Cp_Key, WcType.WITH) }
        );
        FIND01:
        while (condition(vw_cntrct_Prt_Role.readNextRow("FIND01", true)))
        {
            vw_cntrct_Prt_Role.setIfNotFoundControlFlag(false);
            //*  OS - 031214 START
            if (condition(vw_cntrct_Prt_Role.getAstCOUNTER().equals(0)))                                                                                                  //Natural: IF NO RECORDS FOUND
            {
                pnd_Return_Message.setValue(DbsUtil.compress("Missing PPCN", pnd_Cp_Key_Pnd_Cntrct_Part_Ppcn_Nbr, "in IA"));                                              //Natural: COMPRESS 'Missing PPCN' #CNTRCT-PART-PPCN-NBR 'in IA' INTO #RETURN-MESSAGE
                pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L80");                                                                                             //Natural: ASSIGN #ERROR-STATUS := 'L80'
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            //*  OS - 031214 END
            if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Annty_Optn().equals("IR")))                                                                                       //Natural: IF ADP-ANNTY-OPTN EQ 'IR'
            {
                pnd_Date.setValue(cntrct_Prt_Role_Cntrct_Final_Pay_Dte);                                                                                                  //Natural: MOVE CNTRCT-FINAL-PAY-DTE TO #DATE
                pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Fnl_Pay_Date().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_Pnd_Date_A);                          //Natural: MOVE EDITED #DATE-A TO NAZ-ACT-FNL-PAY-DATE ( EM = YYYYMMDD )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(cntrct_Prt_Role_Cntrct_Final_Per_Pay_Dte.greater(getZero())))                                                                               //Natural: IF CNTRCT-FINAL-PER-PAY-DTE GT 0
                {
                    pnd_Date_Pnd_Date_Yyyymm.setValue(cntrct_Prt_Role_Cntrct_Final_Per_Pay_Dte);                                                                          //Natural: MOVE CNTRCT-FINAL-PER-PAY-DTE TO #DATE-YYYYMM
                    pnd_Date_Pnd_Date_Dd.setValue(1);                                                                                                                     //Natural: MOVE 01 TO #DATE-DD
                    pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Fnl_Pay_Date().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_Pnd_Date_A);                      //Natural: MOVE EDITED #DATE-A TO NAZ-ACT-FNL-PAY-DATE ( EM = YYYYMMDD )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Create_Ia_Rslt() throws Exception                                                                                                                    //Natural: CREATE-IA-RSLT
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //*  MOVE FIELDS FROM PDA AND ACTUARIAL INTO IA RESULT RECORD
        //*  032212 START
        //*  032212 END
        ldaAdsl450.getVw_ads_Ia_Rslt_View().reset();                                                                                                                      //Natural: RESET ADS-IA-RSLT-VIEW #MAX-DA-REACHED #LAST-DA-VAL #MAX-STD-REACHED #LAST-STD-VAL #MAX-GRD-REACHED #LAST-GRD-VAL
        pnd_Max_Da_Reached.reset();
        pnd_Last_Da_Val.reset();
        pnd_Max_Std_Reached.reset();
        pnd_Last_Std_Val.reset();
        pnd_Max_Grd_Reached.reset();
        pnd_Last_Grd_Val.reset();
        ldaAdsl450.getAds_Ia_Rslt_View_Rqst_Id().setValue(pdaAdsa401.getPnd_Adsa401_Adc_Rqst_Id());                                                                       //Natural: MOVE ADC-RQST-ID TO ADS-IA-RSLT-VIEW.RQST-ID
        //*  MOVE ADC-STTS-CDE              TO ADI-STTS-CDE /* PER EMELNIK 6-7-03
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Nbrs().getValue("*").setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_All_Tiaa_Contracts().getValue("*"));           //Natural: MOVE #ALL-TIAA-CONTRACTS ( * ) TO ADI-TIAA-NBRS ( * )
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Cref_Nbrs().getValue("*").setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_All_Cref_Contracts().getValue("*"));           //Natural: MOVE #ALL-CREF-CONTRACTS ( * ) TO ADI-CREF-NBRS ( * )
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Tiaa_Nbr().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Ia_Tiaa_Nbr());                                                           //Natural: MOVE ADP-IA-TIAA-NBR TO ADI-IA-TIAA-NBR
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Tiaa_Payee_Cde().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Ia_Tiaa_Payee_Cd());                                                //Natural: MOVE ADP-IA-TIAA-PAYEE-CD TO ADI-IA-TIAA-PAYEE-CDE
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Cref_Nbr().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Ia_Cref_Nbr());                                                           //Natural: MOVE ADP-IA-CREF-NBR TO ADI-IA-CREF-NBR
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Cref_Payee_Cde().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Ia_Cref_Payee_Cd());                                                //Natural: MOVE ADP-IA-CREF-PAYEE-CD TO ADI-IA-CREF-PAYEE-CDE
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Curncy_Cde().setValue("1");                                                                                                    //Natural: MOVE '1' TO ADI-CURNCY-CDE
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Pymnt_Mode());                                                             //Natural: MOVE ADP-PYMNT-MODE TO ADI-PYMNT-MODE
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Optn_Cde().setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Option_Cde());                                               //Natural: MOVE NAZ-ACT-OPTION-CDE TO ADI-OPTN-CDE
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Lst_Actvty_Dte().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Lst_Actvty_Dte());                                                     //Natural: MOVE ADP-LST-ACTVTY-DTE TO ADI-LST-ACTVTY-DTE
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Annty_Strt_Dte().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Annty_Strt_Dte());                                                     //Natural: MOVE ADP-ANNTY-STRT-DTE TO ADI-ANNTY-STRT-DTE
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Annt_Typ_Cde().equals("M")))                                                                                          //Natural: IF ADP-ANNT-TYP-CDE = 'M'
        {
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Frst_Pymnt_Due_Dte().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Annty_Strt_Dte());                                             //Natural: MOVE ADP-ANNTY-STRT-DTE TO ADI-FRST-PYMNT-DUE-DTE
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde().setValue("ACT");                                                                                             //Natural: MOVE 'ACT' TO ADI-IA-RCRD-CDE
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr().setValue(1);                                                                                                   //Natural: MOVE 1 TO ADI-SQNCE-NBR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Rsttlmnt_Cnt().equals("1")))                                                                                      //Natural: IF ADP-RSTTLMNT-CNT = '1'
            {
                ldaAdsl450.getAds_Ia_Rslt_View_Adi_Frst_Pymnt_Due_Dte().setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Ia_Next_Cycle_Check_Dte());                 //Natural: MOVE #IA-NEXT-CYCLE-CHECK-DTE TO ADI-FRST-PYMNT-DUE-DTE
                ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde().setValue("RS ");                                                                                         //Natural: MOVE 'RS ' TO ADI-IA-RCRD-CDE
                ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr().setValue(12);                                                                                              //Natural: MOVE 12 TO ADI-SQNCE-NBR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Rsttlmnt_Cnt().equals("2")))                                                                                  //Natural: IF ADP-RSTTLMNT-CNT = '2'
                {
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Frst_Pymnt_Due_Dte().setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Ia_Next_Cycle_Check_Dte());             //Natural: MOVE #IA-NEXT-CYCLE-CHECK-DTE TO ADI-FRST-PYMNT-DUE-DTE
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde().setValue("RS ");                                                                                     //Natural: MOVE 'RS ' TO ADI-IA-RCRD-CDE
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr().setValue(22);                                                                                          //Natural: MOVE 22 TO ADI-SQNCE-NBR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Finl_Periodic_Py_Dte().setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Std_Fnl_Pp_Pay_Dte());                           //Natural: MOVE NAZ-ACT-STD-FNL-PP-PAY-DTE TO ADI-FINL-PERIODIC-PY-DTE
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Finl_Prtl_Py_Dte().setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Std_Fnl_Pay_Dte());                                  //Natural: MOVE NAZ-ACT-STD-FNL-PAY-DTE TO ADI-FINL-PRTL-PY-DTE
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Ivc_Amt().setValue(pdaAdsa401.getPnd_Adsa401_Pnd_Total_Tiaa_Ivc_Amount());                                                //Natural: MOVE #TOTAL-TIAA-IVC-AMOUNT TO ADI-TIAA-IVC-AMT
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Ivc_Gd_Ind().setValue(pdaAdsa401.getPnd_Adsa401_Adc_Tiaa_Ivc_Gd_Ind());                                                   //Natural: MOVE ADC-TIAA-IVC-GD-IND TO ADI-TIAA-IVC-GD-IND
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Cref_Ivc_Amt().setValue(pdaAdsa401.getPnd_Adsa401_Pnd_Total_Cref_Ivc_Amount());                                                //Natural: MOVE #TOTAL-CREF-IVC-AMOUNT TO ADI-CREF-IVC-AMT
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Cref_Ivc_Gd_Ind().setValue(pdaAdsa401.getPnd_Adsa401_Adc_Cref_Ivc_Gd_Ind());                                                   //Natural: MOVE ADC-CREF-IVC-GD-IND TO ADI-CREF-IVC-GD-IND
        FOR15:                                                                                                                                                            //Natural: FOR #A 1 #MAX-RATES
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Rates)); pnd_A.nadd(1))
        {
            if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde().getValue(pnd_A).equals(" ") || pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde().getValue(pnd_A).equals("00"))) //Natural: IF #TIAA-RATE-CDE ( #A ) EQ ' ' OR EQ '00'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_A.greater(pnd_Max_Rslt)))                                                                                                                   //Natural: IF #A GT #MAX-RSLT
            {
                pnd_Max_Da_Reached.setValue(true);                                                                                                                        //Natural: ASSIGN #MAX-DA-REACHED := TRUE
                pnd_Last_Da_Val.setValue(pnd_A);                                                                                                                          //Natural: ASSIGN #LAST-DA-VAL := #A
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(pnd_A).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde().getValue(pnd_A)); //Natural: MOVE #TIAA-RATE-CDE ( #A ) TO ADI-DTL-TIAA-DA-RATE-CD ( #A )
            //*  032212
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Contract_Id().getValue(pnd_A).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Gic().getValue(pnd_A));           //Natural: MOVE #TIAA-GIC ( #A ) TO ADI-CONTRACT-ID ( #A )
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(pnd_A).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Actl_Amt().getValue(pnd_A)); //Natural: MOVE #TIAA-STNDRD-ACTL-AMT ( #A ) TO ADI-DTL-TIAA-DA-STD-AMT ( #A )
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_A).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Actl_Amt().getValue(pnd_A)); //Natural: MOVE #TIAA-GRADED-ACTL-AMT ( #A ) TO ADI-DTL-TIAA-DA-GRD-AMT ( #A )
            //* * 08312005 MN START
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt().getValue(pnd_A).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Guar_Comm_Amt().getValue(pnd_A)); //Natural: MOVE #TIAA-STNDRD-GUAR-COMM-AMT ( #A ) TO ADI-DTL-TIAA-GUAR-COMMUT-STD-AMT ( #A )
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt().getValue(pnd_A).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Guar_Comm_Amt().getValue(pnd_A)); //Natural: MOVE #TIAA-GRADED-GUAR-COMM-AMT ( #A ) TO ADI-DTL-TIAA-GUAR-COMMUT-GRD-AMT ( #A )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  032212 - END
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Orig_Lob_Ind().getValue("*").setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Orig_Lob_Ind().getValue("*"));              //Natural: MOVE #ORIG-LOB-IND ( * ) TO ADI-ORIG-LOB-IND ( * )
        FOR16:                                                                                                                                                            //Natural: FOR #A 1 #MAX-RATES
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Rates)); pnd_A.nadd(1))
        {
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Rate_Cde().getValue(pnd_A).equals(" ") || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Rate_Cde().getValue(pnd_A).equals("00"))) //Natural: IF NAZ-ACT-TIAA-O-GRD-RATE-CDE ( #A ) EQ ' ' OR EQ '00'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_A.greater(pnd_Max_Rslt)))                                                                                                                   //Natural: IF #A GT #MAX-RSLT
            {
                pnd_Max_Grd_Reached.setValue(true);                                                                                                                       //Natural: ASSIGN #MAX-GRD-REACHED := TRUE
                pnd_Last_Grd_Val.setValue(pnd_A);                                                                                                                         //Natural: ASSIGN #LAST-GRD-VAL := #A
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //* * IF NAZ-ACT-TIAA-O-GRD-RATE-CDE (1) GT '0'
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_A).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Rate_Cde().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-O-GRD-RATE-CDE ( #A ) TO ADI-DTL-TIAA-IA-RATE-CD ( #A )
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Contract_Id().getValue(pnd_A).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Gic_Code().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-O-GRD-GIC-CODE ( #A ) TO ADI-CONTRACT-ID ( #A )
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Eff_Rte_Intrst().getValue(pnd_A).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Grd_Eff_Rate_Int().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-GRD-EFF-RATE-INT ( #A ) TO ADI-DTL-TIAA-EFF-RTE-INTRST ( #A )
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_A).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Guar_Amt().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-O-GRD-GUAR-AMT ( #A ) TO ADI-DTL-TIAA-GRD-GRNTD-AMT ( #A )
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_A).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Divd_Amt().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-O-GRD-DIVD-AMT ( #A ) TO ADI-DTL-TIAA-GRD-DVDND-AMT ( #A )
            //* *END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR17:                                                                                                                                                            //Natural: FOR #A 1 #MAX-RATES
        for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(pnd_Max_Rates)); pnd_A.nadd(1))
        {
            if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Rate_Cde().getValue(pnd_A).equals(" ") || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Rate_Cde().getValue(pnd_A).equals("00"))) //Natural: IF NAZ-ACT-TIAA-O-STD-RATE-CDE ( #A ) EQ ' ' OR EQ '00'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_A.greater(pnd_Max_Rslt)))                                                                                                                   //Natural: IF #A GT #MAX-RSLT
            {
                pnd_Max_Std_Reached.setValue(true);                                                                                                                       //Natural: ASSIGN #MAX-STD-REACHED := TRUE
                pnd_Last_Std_Val.setValue(pnd_A);                                                                                                                         //Natural: ASSIGN #LAST-STD-VAL := #A
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //* *IF NAZ-ACT-TIAA-O-STD-RATE-CDE (1) GT '0'
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_A).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Rate_Cde().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-O-STD-RATE-CDE ( #A ) TO ADI-DTL-TIAA-IA-RATE-CD ( #A )
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Contract_Id().getValue(pnd_A).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Gic_Code().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-O-STD-GIC-CODE ( #A ) TO ADI-CONTRACT-ID ( #A )
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Eff_Rte_Intrst().getValue(pnd_A).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Eff_Rate_Int().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-STD-EFF-RATE-INT ( #A ) TO ADI-DTL-TIAA-EFF-RTE-INTRST ( #A )
            //* *END-IF
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_A).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Guar_Amt().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-O-STD-GUAR-AMT ( #A ) TO ADI-DTL-TIAA-STD-GRNTD-AMT ( #A )
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_A).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Divd_Amt().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-O-STD-DIVD-AMT ( #A ) TO ADI-DTL-TIAA-STD-DVDND-AMT ( #A )
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_A).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Fnl_Guar_Amt().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-STD-FNL-GUAR-AMT ( #A ) TO ADI-DTL-FNL-STD-PAY-AMT ( #A )
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_A).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Fnl_Divd_Amt().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-STD-FNL-DIVD-AMT ( #A ) TO ADI-DTL-FNL-STD-DVDND-AMT ( #A )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  032212 END
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Rate_Cd().getValue(1,":",20).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Rate_Cde().getValue(1,     //Natural: MOVE #CREF-RATE-CDE ( 1:20 ) TO ADI-DTL-CREF-DA-RATE-CD ( 1:20 )
            ":",20));
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Acct_Cd().getValue(1,":",20).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Acct_Cde().getValue(1,        //Natural: MOVE #CREF-ACCT-CDE ( 1:20 ) TO ADI-DTL-CREF-ACCT-CD ( 1:20 )
            ":",20));
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Rate_Cde().getValue("*").notEquals(" ")))                                                  //Natural: IF NAZ-ACT-CREF-O-ANN-RATE-CDE ( * ) NE ' '
        {
            FOR18:                                                                                                                                                        //Natural: FOR #I = 1 TO #FUND-CNT
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaAdsa401.getPnd_Adsa401_Pnd_Fund_Cnt())); pnd_I.nadd(1))
            {
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Rate_Cde().getValue(pnd_I).notEquals(" ")))                                        //Natural: IF NAZ-ACT-CREF-O-ANN-RATE-CDE ( #I ) NE ' '
                {
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Rate_Cd().getValue(pnd_I).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Rate_Cde().getValue(pnd_I)); //Natural: MOVE NAZ-ACT-CREF-O-ANN-RATE-CDE ( #I ) TO ADI-DTL-CREF-IA-RATE-CD ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Rate_Cd().getValue(1,":",20).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Rate_Cde().getValue(1, //Natural: MOVE NAZ-ACT-CREF-O-ANN-RATE-CDE ( 1:20 ) TO ADI-DTL-CREF-IA-RATE-CD ( 1:20 )
            ":",20));
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Annl_Amt().getValue(1,":",20).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Rate_Amt().getValue(1, //Natural: MOVE NAZ-ACT-CREF-O-ANN-RATE-AMT ( 1:20 ) TO ADI-DTL-CREF-DA-ANNL-AMT ( 1:20 )
            ":",20));
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Nbr_Units().getValue(1,":",20).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Pymnt_Units().getValue(1, //Natural: MOVE NAZ-ACT-CREF-O-ANN-PYMNT-UNITS ( 1:20 ) TO ADI-DTL-CREF-ANNL-NBR-UNITS ( 1:20 )
            ":",20));
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Annl_Unit_Val().getValue(1,":",20).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Ann_Unit_Value().getValue(1, //Natural: MOVE NAZ-ACT-CREF-O-ANN-UNIT-VALUE ( 1:20 ) TO ADI-DTL-CREF-IA-ANNL-UNIT-VAL ( 1:20 )
            ":",20));
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt().getValue(1,":",20).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Stndrd_Annual_Actl_Amt().getValue(1, //Natural: MOVE #CREF-STNDRD-ANNUAL-ACTL-AMT ( 1:20 ) TO ADI-DTL-CREF-ANNL-AMT ( 1:20 )
            ":",20));
        if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Rate_Cde().getValue("*").notEquals(" ")))                                                  //Natural: IF NAZ-ACT-CREF-O-MTH-RATE-CDE ( * ) NE ' '
        {
            FOR19:                                                                                                                                                        //Natural: FOR #I = 1 TO #FUND-CNT
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaAdsa401.getPnd_Adsa401_Pnd_Fund_Cnt())); pnd_I.nadd(1))
            {
                if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Rate_Cde().getValue(pnd_I).notEquals(" ")))                                        //Natural: IF NAZ-ACT-CREF-O-MTH-RATE-CDE ( #I ) NE ' '
                {
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Rate_Cd().getValue(pnd_I).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Rate_Cde().getValue(pnd_I)); //Natural: MOVE NAZ-ACT-CREF-O-MTH-RATE-CDE ( #I ) TO ADI-DTL-CREF-IA-RATE-CD ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue(1,":",20).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Rate_Amt().getValue(1, //Natural: MOVE NAZ-ACT-CREF-O-MTH-RATE-AMT ( 1:20 ) TO ADI-DTL-CREF-DA-MNTHLY-AMT ( 1:20 )
            ":",20));
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Nbr_Units().getValue(1,":",20).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Pymnt_Units().getValue(1, //Natural: MOVE NAZ-ACT-CREF-O-MTH-PYMNT-UNITS ( 1:20 ) TO ADI-DTL-CREF-MNTHLY-NBR-UNITS ( 1:20 )
            ":",20));
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Ia_Mnthly_Unit_Val().getValue(1,":",20).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Cref_O_Mth_Unit_Value().getValue(1, //Natural: MOVE NAZ-ACT-CREF-O-MTH-UNIT-VALUE ( 1:20 ) TO ADI-DTL-CREF-IA-MNTHLY-UNIT-VAL ( 1:20 )
            ":",20));
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt().getValue(1,":",20).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Cref_Graded_Mnthly_Actl_Amt().getValue(1, //Natural: MOVE #CREF-GRADED-MNTHLY-ACTL-AMT ( 1:20 ) TO ADI-DTL-CREF-MNTHLY-AMT ( 1:20 )
            ":",20));
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Cref_Sttlmnt().setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Account_C());                                             //Natural: MOVE #ACCOUNT-C TO ADI-CREF-STTLMNT
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Re_Sttlmnt().setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Account_R());                                          //Natural: MOVE #ACCOUNT-R TO ADI-TIAA-RE-STTLMNT
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Tiaa_Sttlmnt().setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Account_T());                                             //Natural: MOVE #ACCOUNT-T TO ADI-TIAA-STTLMNT
        //* * 12052006 MN START
        //*  INITIAL SETTLEMENT
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Annt_Typ_Cde().equals("M")))                                                                                          //Natural: IF ADP-ANNT-TYP-CDE = 'M'
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Annty_Optn().equals(pnd_Two_Life.getValue("*")) || (pdaAdsa401.getPnd_Adsa401_Adp_Annty_Optn().equals(pnd_Single_Life.getValue("*"))  //Natural: IF ADP-ANNTY-OPTN = #TWO-LIFE ( * ) OR ( ADP-ANNTY-OPTN = #SINGLE-LIFE ( * ) AND ADP-GRNTEE-PERIOD > 0 )
                && pdaAdsa401.getPnd_Adsa401_Adp_Grntee_Period().greater(getZero()))))
            {
                if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Sub_Plan_Nbr().getSubstring(1,3).equals(pnd_Atra.getValue("*")) || pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Sub_Lob().equals("ROTH")  //Natural: IF SUBSTRING ( ADC-SUB-PLAN-NBR,1,3 ) = #ATRA ( * ) OR #ADSA401.#CNTRCT-SUB-LOB = 'ROTH' OR #ADSA401.#CNTRCT-SUB-LOB = 'ROTH INDEXED'
                    || pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Sub_Lob().equals("ROTH INDEXED")))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM TEST-MDIB-AIAN033
                    sub_Test_Mdib_Aian033();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().notEquals(" ")))                                                                           //Natural: IF #ERROR-STATUS NE ' '
                    {
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Annty_Optn().equals(pnd_Excluded_Optns.getValue("*"))))                                                           //Natural: IF ADP-ANNTY-OPTN EQ #EXCLUDED-OPTNS ( * )
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* *      DG 11/07 START:  BYPASS MDIB TEST ON CONDITIONS
                if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Sub_Plan_Nbr().getSubstring(1,3).equals(pnd_Atra.getValue("*")) || pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Sub_Lob().equals("ROTH")  //Natural: IF SUBSTRING ( ADC-SUB-PLAN-NBR,1,3 ) = #ATRA ( * ) OR #ADSA401.#CNTRCT-SUB-LOB = 'ROTH' OR #ADSA401.#CNTRCT-SUB-LOB = 'ROTH INDEXED'
                    || pdaAdsa401.getPnd_Adsa401_Pnd_Cntrct_Sub_Lob().equals("ROTH INDEXED")))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM TEST-MDIB-AIAN034
                    sub_Test_Mdib_Aian034();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().equals("L63")))                                                                            //Natural: IF #ERROR-STATUS = 'L63'
                    {
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* * 12052006 MN END
        //*  RS0
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Roth_Rqst_Ind().equals("Y")))                                                                                         //Natural: IF #ADSA401.ADP-ROTH-RQST-IND = 'Y'
        {
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Roth_Rqst_Ind().setValue("Y");                                                                                             //Natural: MOVE 'Y' TO ADS-IA-RSLT-VIEW.ADI-ROTH-RQST-IND
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #ADSA401.ADP-SRVVR-IND = 'Y'          /* CM EM - 050715 START
        //*   MOVE 'Y' TO ADS-IA-RSLT-VIEW.ADI-SRVVR-IND
        //*  END-IF
        ldaAdsl450.getAds_Ia_Rslt_View_Adi_Srvvr_Ind().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Srvvr_Ind());                                                               //Natural: MOVE #ADSA401.ADP-SRVVR-IND TO ADS-IA-RSLT-VIEW.ADI-SRVVR-IND
        PND_PND_L5100:                                                                                                                                                    //Natural: STORE ADS-IA-RSLT-VIEW
        ldaAdsl450.getVw_ads_Ia_Rslt_View().insertDBRow("PND_PND_L5100");
        //*  032212 START
        pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Iarslt_Isn().getValue(1).setValue(ldaAdsl450.getVw_ads_Ia_Rslt_View().getAstISN("PND_PND_L5100"));                              //Natural: MOVE *ISN ( ##L5100. ) TO #CUR-IARSLT-ISN ( 1 )
        if (condition(pnd_Max_Da_Reached.getBoolean() || pnd_Max_Std_Reached.getBoolean() || pnd_Max_Grd_Reached.getBoolean()))                                           //Natural: IF #MAX-DA-REACHED OR #MAX-STD-REACHED OR #MAX-GRD-REACHED
        {
            pnd_P.reset();                                                                                                                                                //Natural: RESET #P ADI-DTL-TIAA-DA-RATE-CD ( * ) ADI-DTL-TIAA-DA-STD-AMT ( * ) ADI-DTL-TIAA-DA-GRD-AMT ( * ) ADI-DTL-TIAA-GUAR-COMMUT-STD-AMT ( * ) ADI-DTL-TIAA-GUAR-COMMUT-GRD-AMT ( * ) ADI-DTL-TIAA-IA-RATE-CD ( * ) ADI-CONTRACT-ID ( * ) ADI-DTL-TIAA-EFF-RTE-INTRST ( * ) ADI-DTL-TIAA-STD-GRNTD-AMT ( * ) ADI-DTL-TIAA-STD-DVDND-AMT ( * ) ADI-DTL-FNL-STD-PAY-AMT ( * ) ADI-DTL-FNL-STD-DVDND-AMT ( * ) ADI-DTL-TIAA-GRD-GRNTD-AMT ( * ) ADI-DTL-TIAA-GRD-DVDND-AMT ( * )
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue("*").reset();
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Std_Amt().getValue("*").reset();
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue("*").reset();
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt().getValue("*").reset();
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt().getValue("*").reset();
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue("*").reset();
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Contract_Id().getValue("*").reset();
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Eff_Rte_Intrst().getValue("*").reset();
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue("*").reset();
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue("*").reset();
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Std_Pay_Amt().getValue("*").reset();
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue("*").reset();
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue("*").reset();
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue("*").reset();
            if (condition(pnd_Max_Da_Reached.getBoolean()))                                                                                                               //Natural: IF #MAX-DA-REACHED
            {
                FOR20:                                                                                                                                                    //Natural: FOR #A #LAST-DA-VAL #MAX-RATES
                for (pnd_A.setValue(pnd_Last_Da_Val); condition(pnd_A.lessOrEqual(pnd_Max_Rates)); pnd_A.nadd(1))
                {
                    if (condition(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde().getValue(pnd_A).equals(" ") || pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde().getValue(pnd_A).equals("00"))) //Natural: IF #TIAA-RATE-CDE ( #A ) EQ ' ' OR EQ '00'
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_P.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #P
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Rate_Cd().getValue(pnd_P).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Rate_Cde().getValue(pnd_A)); //Natural: MOVE #TIAA-RATE-CDE ( #A ) TO ADI-DTL-TIAA-DA-RATE-CD ( #P )
                    //*  032212
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Contract_Id().getValue(pnd_P).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Gic().getValue(pnd_A));   //Natural: MOVE #TIAA-GIC ( #A ) TO ADI-CONTRACT-ID ( #P )
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Std_Amt().getValue(pnd_P).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Actl_Amt().getValue(pnd_A)); //Natural: MOVE #TIAA-STNDRD-ACTL-AMT ( #A ) TO ADI-DTL-TIAA-DA-STD-AMT ( #P )
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Da_Grd_Amt().getValue(pnd_P).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Actl_Amt().getValue(pnd_A)); //Natural: MOVE #TIAA-GRADED-ACTL-AMT ( #A ) TO ADI-DTL-TIAA-DA-GRD-AMT ( #P )
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Guar_Commut_Std_Amt().getValue(pnd_P).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Guar_Comm_Amt().getValue(pnd_A)); //Natural: MOVE #TIAA-STNDRD-GUAR-COMM-AMT ( #A ) TO ADI-DTL-TIAA-GUAR-COMMUT-STD-AMT ( #P )
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Guar_Commut_Grd_Amt().getValue(pnd_P).setValue(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Guar_Comm_Amt().getValue(pnd_A)); //Natural: MOVE #TIAA-GRADED-GUAR-COMM-AMT ( #A ) TO ADI-DTL-TIAA-GUAR-COMMUT-GRD-AMT ( #P )
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_P.reset();                                                                                                                                                //Natural: RESET #P
            if (condition(pnd_Max_Std_Reached.getBoolean()))                                                                                                              //Natural: IF #MAX-STD-REACHED
            {
                FOR21:                                                                                                                                                    //Natural: FOR #A #LAST-STD-VAL #MAX-RATES
                for (pnd_A.setValue(pnd_Last_Std_Val); condition(pnd_A.lessOrEqual(pnd_Max_Rates)); pnd_A.nadd(1))
                {
                    if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Rate_Cde().getValue(pnd_A).equals(" ") || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Rate_Cde().getValue(pnd_A).equals("00"))) //Natural: IF NAZ-ACT-TIAA-O-STD-RATE-CDE ( #A ) EQ ' ' OR EQ '00'
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_P.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #P
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_P).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Rate_Cde().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-O-STD-RATE-CDE ( #A ) TO ADI-DTL-TIAA-IA-RATE-CD ( #P )
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Contract_Id().getValue(pnd_P).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Gic_Code().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-O-STD-GIC-CODE ( #A ) TO ADI-CONTRACT-ID ( #P )
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Eff_Rte_Intrst().getValue(pnd_P).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Eff_Rate_Int().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-STD-EFF-RATE-INT ( #A ) TO ADI-DTL-TIAA-EFF-RTE-INTRST ( #P )
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Grntd_Amt().getValue(pnd_P).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Guar_Amt().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-O-STD-GUAR-AMT ( #A ) TO ADI-DTL-TIAA-STD-GRNTD-AMT ( #P )
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Std_Dvdnd_Amt().getValue(pnd_P).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Divd_Amt().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-O-STD-DIVD-AMT ( #A ) TO ADI-DTL-TIAA-STD-DVDND-AMT ( #P )
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Std_Pay_Amt().getValue(pnd_P).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Fnl_Guar_Amt().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-STD-FNL-GUAR-AMT ( #A ) TO ADI-DTL-FNL-STD-PAY-AMT ( #P )
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Fnl_Std_Dvdnd_Amt().getValue(pnd_P).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Std_Fnl_Divd_Amt().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-STD-FNL-DIVD-AMT ( #A ) TO ADI-DTL-FNL-STD-DVDND-AMT ( #P )
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_P.reset();                                                                                                                                                //Natural: RESET #P
            if (condition(pnd_Max_Grd_Reached.getBoolean()))                                                                                                              //Natural: IF #MAX-GRD-REACHED
            {
                FOR22:                                                                                                                                                    //Natural: FOR #A #LAST-GRD-VAL #MAX-RATES
                for (pnd_A.setValue(pnd_Last_Grd_Val); condition(pnd_A.lessOrEqual(pnd_Max_Rates)); pnd_A.nadd(1))
                {
                    if (condition(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Rate_Cde().getValue(pnd_A).equals(" ") || pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Rate_Cde().getValue(pnd_A).equals("00"))) //Natural: IF NAZ-ACT-TIAA-O-GRD-RATE-CDE ( #A ) EQ ' ' OR EQ '00'
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_P.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #P
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Ia_Rate_Cd().getValue(pnd_P).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Rate_Cde().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-O-GRD-RATE-CDE ( #A ) TO ADI-DTL-TIAA-IA-RATE-CD ( #P )
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Contract_Id().getValue(pnd_P).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Gic_Code().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-O-GRD-GIC-CODE ( #A ) TO ADI-CONTRACT-ID ( #P )
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Eff_Rte_Intrst().getValue(pnd_P).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_Grd_Eff_Rate_Int().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-GRD-EFF-RATE-INT ( #A ) TO ADI-DTL-TIAA-EFF-RTE-INTRST ( #P )
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Grntd_Amt().getValue(pnd_P).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Guar_Amt().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-O-GRD-GUAR-AMT ( #A ) TO ADI-DTL-TIAA-GRD-GRNTD-AMT ( #P )
                    ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Tiaa_Grd_Dvdnd_Amt().getValue(pnd_P).setValue(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Divd_Amt().getValue(pnd_A)); //Natural: MOVE NAZ-ACT-TIAA-O-GRD-DIVD-AMT ( #A ) TO ADI-DTL-TIAA-GRD-DVDND-AMT ( #P )
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde().equals("ACT")))                                                                                //Natural: IF ADI-IA-RCRD-CDE = 'ACT'
            {
                ldaAdsl450.getAds_Ia_Rslt_View_Adi_Sqnce_Nbr().setValue(2);                                                                                               //Natural: MOVE 2 TO ADI-SQNCE-NBR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaAdsl450.getAds_Ia_Rslt_View_Adi_Ia_Rcrd_Cde(),  //Natural: COMPRESS ADI-IA-RCRD-CDE '1' TO ADI-IA-RCRD-CDE LEAVING NO
                    "1"));
            }                                                                                                                                                             //Natural: END-IF
            PND_PND_L5530:                                                                                                                                                //Natural: STORE ADS-IA-RSLT-VIEW
            ldaAdsl450.getVw_ads_Ia_Rslt_View().insertDBRow("PND_PND_L5530");
            pdaAdsa401.getPnd_Adsa401_Pnd_Cur_Iarslt_Isn().getValue(2).setValue(ldaAdsl450.getVw_ads_Ia_Rslt_View().getAstISN("PND_PND_L5530"));                          //Natural: MOVE *ISN ( ##L5530. ) TO #CUR-IARSLT-ISN ( 2 )
            //*  032212 END
        }                                                                                                                                                                 //Natural: END-IF
        //*  CREATE-IA-RSLT
    }
    private void sub_Test_Mdib_Aian033() throws Exception                                                                                                                 //Natural: TEST-MDIB-AIAN033
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(0, Global.getPROGRAM(),"********** Beginning of TEST-MDIB-AIAN033 ************");                                                              //Natural: WRITE *PROGRAM '********** Beginning of TEST-MDIB-AIAN033 ************'
        if (Global.isEscape()) return;
        //*  MDIB TEST PROGRAM (AIAN033)    /* DG  11/28/07
        pdaAial0330.getPnd_Aian033_Linkage().reset();                                                                                                                     //Natural: RESET #AIAN033-LINKAGE
        pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code().setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Optn_Cde());                                             //Natural: ASSIGN #AIAN033-OPTION-CODE := ADI-OPTN-CDE
        pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Issue_Date().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Annty_Strt_Dte());                                             //Natural: ASSIGN #AIAN033-ISSUE-DATE := ADP-ANNTY-STRT-DTE
        pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_1st_Ann_Birth_Date().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Frst_Annt_Dte_Of_Brth());                              //Natural: ASSIGN #AIAN033-1ST-ANN-BIRTH-DATE := ADP-FRST-ANNT-DTE-OF-BRTH
        //* * 050613 START
        vw_ads_P.startDatabaseFind                                                                                                                                        //Natural: FIND ADS-P WITH ADS-P.RQST-ID = #ADSA401.RQST-ID
        (
        "FIND02",
        new Wc[] { new Wc("RQST_ID", "=", pdaAdsa401.getPnd_Adsa401_Rqst_Id(), WcType.WITH) }
        );
        FIND02:
        while (condition(vw_ads_P.readNextRow("FIND02")))
        {
            vw_ads_P.setIfNotFoundControlFlag(false);
            pdaAdsa401.getPnd_Adsa401_Adp_Sole_Prmry_Bene_Rltnshp().setValue(ads_P_Adp_Sole_Prmry_Bene_Rltnshp);                                                          //Natural: ASSIGN #ADSA401.ADP-SOLE-PRMRY-BENE-RLTNSHP := ADS-P.ADP-SOLE-PRMRY-BENE-RLTNSHP
            pdaAdsa401.getPnd_Adsa401_Adp_Sole_Prmry_Bene_Dte_Of_Brth().setValue(ads_P_Adp_Sole_Prmry_Bene_Dte_Of_Brth);                                                  //Natural: ASSIGN #ADSA401.ADP-SOLE-PRMRY-BENE-DTE-OF-BRTH := ADS-P.ADP-SOLE-PRMRY-BENE-DTE-OF-BRTH
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //* * 050613 END
        //*  CM 3/31 START
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Sole_Prmry_Bene_Rltnshp().notEquals(" ") && pdaAdsa401.getPnd_Adsa401_Adp_Sole_Prmry_Bene_Dte_Of_Brth().notEquals(getZero()))) //Natural: IF #ADSA401.ADP-SOLE-PRMRY-BENE-RLTNSHP NE ' ' AND #ADSA401.ADP-SOLE-PRMRY-BENE-DTE-OF-BRTH NE 0
        {
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Birth_Date().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Sole_Prmry_Bene_Dte_Of_Brth());                    //Natural: ASSIGN #AIAN033-2ND-ANN-BIRTH-DATE := #ADSA401.ADP-SOLE-PRMRY-BENE-DTE-OF-BRTH
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Type().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Sole_Prmry_Bene_Rltnshp());                              //Natural: ASSIGN #AIAN033-2ND-ANN-TYPE := #ADSA401.ADP-SOLE-PRMRY-BENE-RLTNSHP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Birth_Date().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Scnd_Annt_Dte_Of_Brth());                          //Natural: ASSIGN #AIAN033-2ND-ANN-BIRTH-DATE := ADP-SCND-ANNT-DTE-OF-BRTH
            pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Type().setValue(pnd_Adp_Scnd_Annt_Rltnshp);                                                            //Natural: ASSIGN #AIAN033-2ND-ANN-TYPE := #ADP-SCND-ANNT-RLTNSHP
        }                                                                                                                                                                 //Natural: END-IF
        pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Guar_Years().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Grntee_Period());                                              //Natural: ASSIGN #AIAN033-GUAR-YEARS := ADP-GRNTEE-PERIOD
        getReports().write(0, Global.getPROGRAM(),"***** Before calling MDIB test prm AIAN033 *****",NEWLINE,"#AIAN033-INPUT-DATA:",NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code(), //Natural: WRITE *PROGRAM '***** Before calling MDIB test prm AIAN033 *****' /'#AIAN033-INPUT-DATA:' /'=' #AIAN033-OPTION-CODE /'=' #AIAN033-GUAR-YEARS /'=' #AIAN033-GUAR-MONTHS /'=' #AIAN033-ISSUE-DATE /'=' #AIAN033-1ST-ANN-BIRTH-DATE /'=' #AIAN033-2ND-ANN-BIRTH-DATE /'=' #AIAN033-BENEFICIARY-BIRTH-DATE /'=' #AIAN033-2ND-ANN-TYPE /'=' #AIAN033-BEN-TYPE /'#AIAN033-OUTPUT-DATA:' /'=' #AIAN033-MD-EXP-LIFE /'=' #AIAN033-MDIB-EXP-LIFE /'=' #AIAN033-MINIMUM-LIFE-EXP /'=' #AIAN033-MINIMUM-PCT-ALLOW /'=' #AIAN033-RETURN-CODE-GUAR-PGM /'=' #AIAN033-RETURN-CODE-GUAR /'=' #AIAN033-RETURN-MSG-GUAR /'=' #AIAN033-RETURN-CODE-PCT-PGM /'=' #AIAN033-RETURN-CODE-PCT /'=' #AIAN033-RETURN-MSG-PCT
            NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Guar_Years(),NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Guar_Months(),
            NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Issue_Date(),NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_1st_Ann_Birth_Date(),
            NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Birth_Date(),NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Beneficiary_Birth_Date(),
            NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Type(),NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Ben_Type(),
            NEWLINE,"#AIAN033-OUTPUT-DATA:",NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Md_Exp_Life(),NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Mdib_Exp_Life(),
            NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Minimum_Life_Exp(),NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Minimum_Pct_Allow(),
            NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm(),NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar(),
            NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar(),NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_Pgm(),
            NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct(),NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Pct());
        if (Global.isEscape()) return;
        //*  DG 11/07
        DbsUtil.callnat(Aian033.class , getCurrentProcessState(), pdaAial0330.getPnd_Aian033_Linkage(), pdaAiaa510.getNaz_Act_Calc_Input_Area());                         //Natural: CALLNAT 'AIAN033' #AIAN033-LINKAGE NAZ-ACT-CALC-INPUT-AREA
        if (condition(Global.isEscape())) return;
        getReports().write(0, Global.getPROGRAM(),"***** After calling MDIB test prm AIAN033 *****",NEWLINE,"#AIAN033-INPUT-DATA:",NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Option_Code(), //Natural: WRITE *PROGRAM '***** After calling MDIB test prm AIAN033 *****' /'#AIAN033-INPUT-DATA:' /'=' #AIAN033-OPTION-CODE /'=' #AIAN033-GUAR-YEARS /'=' #AIAN033-GUAR-MONTHS /'=' #AIAN033-ISSUE-DATE /'=' #AIAN033-1ST-ANN-BIRTH-DATE /'=' #AIAN033-2ND-ANN-BIRTH-DATE /'=' #AIAN033-BENEFICIARY-BIRTH-DATE /'=' #AIAN033-2ND-ANN-TYPE /'=' #AIAN033-BEN-TYPE /'#AIAN033-OUTPUT-DATA:' /'=' #AIAN033-MD-EXP-LIFE /'=' #AIAN033-MDIB-EXP-LIFE /'=' #AIAN033-MINIMUM-LIFE-EXP /'=' #AIAN033-MINIMUM-PCT-ALLOW /'=' #AIAN033-RETURN-CODE-GUAR-PGM /'=' #AIAN033-RETURN-CODE-GUAR /'=' #AIAN033-RETURN-MSG-GUAR /'=' #AIAN033-RETURN-CODE-PCT-PGM /'=' #AIAN033-RETURN-CODE-PCT /'=' #AIAN033-RETURN-MSG-PCT / *PROGRAM '********** END OF TEST-MDIB (AIAN033) ************'
            NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Guar_Years(),NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Guar_Months(),
            NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Issue_Date(),NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_1st_Ann_Birth_Date(),
            NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Birth_Date(),NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Beneficiary_Birth_Date(),
            NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_2nd_Ann_Type(),NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Ben_Type(),
            NEWLINE,"#AIAN033-OUTPUT-DATA:",NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Md_Exp_Life(),NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Mdib_Exp_Life(),
            NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Minimum_Life_Exp(),NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Minimum_Pct_Allow(),
            NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar_Pgm(),NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar(),
            NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar(),NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct_Pgm(),
            NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct(),NEWLINE,"=",pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Pct(),
            NEWLINE,Global.getPROGRAM(),"********** END OF TEST-MDIB (AIAN033) ************");
        if (Global.isEscape()) return;
        //*  NORMAL -
        //*  NO ACTUARIAL ERRORS
        //*  GENERIC ACTUARIAL ERROR CODE
        short decideConditionsMet1648 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #AIAN033-RETURN-CODE-GUAR = 300 AND #AIAN033-RETURN-CODE-PCT = 300
        if (condition(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar().equals(300) && pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct().equals(300)))
        {
            decideConditionsMet1648++;
            ignore();
        }                                                                                                                                                                 //Natural: WHEN #AIAN033-RETURN-CODE-GUAR = 301
        if (condition(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Guar().equals(301)))
        {
            decideConditionsMet1648++;
            pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L66");                                                                                                 //Natural: ASSIGN #ERROR-STATUS := 'L66'
            pnd_Return_Message.setValue(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Guar());                                                                //Natural: ASSIGN #RETURN-MESSAGE := #AIAN033-RETURN-MSG-GUAR
        }                                                                                                                                                                 //Natural: WHEN #AIAN033-RETURN-CODE-PCT = 302
        if (condition(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Code_Pct().equals(302)))
        {
            decideConditionsMet1648++;
            pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L67");                                                                                                 //Natural: ASSIGN #ERROR-STATUS := 'L67'
            pnd_Return_Message.setValue(pdaAial0330.getPnd_Aian033_Linkage_Pnd_Aian033_Return_Msg_Pct());                                                                 //Natural: ASSIGN #RETURN-MESSAGE := #AIAN033-RETURN-MSG-PCT
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet1648 == 0))
        {
            pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L15");                                                                                                 //Natural: ASSIGN #ERROR-STATUS := 'L15'
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*   TEST-MDIB-AIAN033
    }
    private void sub_Test_Mdib_Aian034() throws Exception                                                                                                                 //Natural: TEST-MDIB-AIAN034
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(0, Global.getPROGRAM(),"********** Beginning of TEST-MDIB-AIAN034 ************");                                                              //Natural: WRITE *PROGRAM '********** Beginning of TEST-MDIB-AIAN034 ************'
        if (Global.isEscape()) return;
        //*  032212
        //*  032212
        //*  032212
        //*  032212
        //*  032212
        //*  032212
        pnd_Mdib_Parms.reset();                                                                                                                                           //Natural: RESET #MDIB-PARMS #RETURN-MESSAGE
        pnd_Return_Message.reset();
        pnd_Mdib_Parms_Pnd_Mdib_2nd_Annt_Dob.setValue(pdaAdsa401.getPnd_Adsa401_Adp_Scnd_Annt_Dte_Of_Brth());                                                             //Natural: ASSIGN #MDIB-2ND-ANNT-DOB := ADP-SCND-ANNT-DTE-OF-BRTH
        pnd_Mdib_Parms_Pnd_Mdib_2nd_Annt_Type.setValue(pnd_Adp_Scnd_Annt_Rltnshp);                                                                                        //Natural: ASSIGN #MDIB-2ND-ANNT-TYPE := #ADP-SCND-ANNT-RLTNSHP
        pnd_Mdib_Parms_Pnd_Mdib_Option.setValue(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Optn_Cde());                                                                           //Natural: ASSIGN #MDIB-OPTION := ADI-OPTN-CDE
        pnd_Mdib_Parms_Pnd_Mdib_Annt_Dob.setValue(pdaAdsa401.getPnd_Adsa401_Adp_Frst_Annt_Dte_Of_Brth());                                                                 //Natural: ASSIGN #MDIB-ANNT-DOB := ADP-FRST-ANNT-DTE-OF-BRTH
        pnd_Mdib_Parms_Pnd_Mdib_Sttlmnt_Dte.setValue(pdaAdsa401.getPnd_Adsa401_Adp_Effctv_Dte());                                                                         //Natural: ASSIGN #MDIB-STTLMNT-DTE := ADP-EFFCTV-DTE
        pnd_Mdib_Parms_Pnd_Mdib_Tot_Tiaa_Accum.nadd(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Stndrd_Actl_Amt().getValue("*"));                                   //Natural: ASSIGN #MDIB-TOT-TIAA-ACCUM := #MDIB-TOT-TIAA-ACCUM + #TIAA-STNDRD-ACTL-AMT ( * )
        pnd_Mdib_Parms_Pnd_Mdib_Tot_Tiaa_Accum.nadd(pdaAdsa450.getPnd_Adsa450_Actuarial_Data_Pnd_Tiaa_Graded_Actl_Amt().getValue("*"));                                   //Natural: ASSIGN #MDIB-TOT-TIAA-ACCUM := #MDIB-TOT-TIAA-ACCUM + #TIAA-GRADED-ACTL-AMT ( * )
        pnd_Mdib_Parms_Pnd_Mdib_Tot_Cref_Accum.nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Annl_Amt().getValue("*"));                                                //Natural: ASSIGN #MDIB-TOT-CREF-ACCUM := #MDIB-TOT-CREF-ACCUM + ADI-DTL-CREF-ANNL-AMT ( * )
        pnd_Mdib_Parms_Pnd_Mdib_Tot_Cref_Accum.nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Mnthly_Amt().getValue("*"));                                              //Natural: ASSIGN #MDIB-TOT-CREF-ACCUM := #MDIB-TOT-CREF-ACCUM + ADI-DTL-CREF-MNTHLY-AMT ( * )
        pnd_Mdib_Parms_Pnd_Mdib_Tot_Tiaa_Pymnt.nadd(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Guar_Amt().getValue("*"));                                   //Natural: ASSIGN #MDIB-TOT-TIAA-PYMNT := #MDIB-TOT-TIAA-PYMNT + NAZ-ACT-TIAA-O-GRD-GUAR-AMT ( * )
        pnd_Mdib_Parms_Pnd_Mdib_Tot_Tiaa_Pymnt.nadd(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Grd_Divd_Amt().getValue("*"));                                   //Natural: ASSIGN #MDIB-TOT-TIAA-PYMNT := #MDIB-TOT-TIAA-PYMNT + NAZ-ACT-TIAA-O-GRD-DIVD-AMT ( * )
        pnd_Mdib_Parms_Pnd_Mdib_Tot_Tiaa_Pymnt.nadd(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Guar_Amt().getValue("*"));                                   //Natural: ASSIGN #MDIB-TOT-TIAA-PYMNT := #MDIB-TOT-TIAA-PYMNT + NAZ-ACT-TIAA-O-STD-GUAR-AMT ( * )
        pnd_Mdib_Parms_Pnd_Mdib_Tot_Tiaa_Pymnt.nadd(pdaAiaa510.getNaz_Act_Calc_Input_Area_Naz_Act_Tiaa_O_Std_Divd_Amt().getValue("*"));                                   //Natural: ASSIGN #MDIB-TOT-TIAA-PYMNT := #MDIB-TOT-TIAA-PYMNT + NAZ-ACT-TIAA-O-STD-DIVD-AMT ( * )
        pnd_Mdib_Parms_Pnd_Mdib_Tot_Cref_Pymnt.nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Annl_Amt().getValue("*"));                                             //Natural: ASSIGN #MDIB-TOT-CREF-PYMNT := #MDIB-TOT-CREF-PYMNT + ADI-DTL-CREF-DA-ANNL-AMT ( * )
        pnd_Mdib_Parms_Pnd_Mdib_Tot_Cref_Pymnt.nadd(ldaAdsl450.getAds_Ia_Rslt_View_Adi_Dtl_Cref_Da_Mnthly_Amt().getValue("*"));                                           //Natural: ASSIGN #MDIB-TOT-CREF-PYMNT := #MDIB-TOT-CREF-PYMNT + ADI-DTL-CREF-DA-MNTHLY-AMT ( * )
        pnd_Data_Passed.setValue("3");                                                                                                                                    //Natural: ASSIGN #DATA-PASSED := '3'
        pnd_W_Date_A.setValueEdited(pdaAdsa401.getPnd_Adsa401_Adp_Annty_Strt_Dte(),new ReportEditMask("YYYYMMDD"));                                                       //Natural: MOVE EDITED ADP-ANNTY-STRT-DTE ( EM = YYYYMMDD ) TO #W-DATE-A
        getReports().write(0, Global.getPROGRAM(),"********** Inside of TEST-MDIB-AIAN034 ************",NEWLINE,Global.getPROGRAM(),"***** Before calling ADSN040   ************", //Natural: WRITE *PROGRAM '********** Inside of TEST-MDIB-AIAN034 ************' / *PROGRAM '***** Before calling ADSN040   ************' /'=' #DATA-PASSED /'=' #W-DATE-MM /'=' #MDIB-PYMNT-MDE /'=' ADI-PYMNT-MODE
            NEWLINE,"=",pnd_Data_Passed,NEWLINE,"=",pnd_W_Date_A_Pnd_W_Date_Mm,NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Pymnt_Mde,NEWLINE,"=",ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode());
        if (Global.isEscape()) return;
        DbsUtil.callnat(Adsn040.class , getCurrentProcessState(), pdaAdspda_M.getMsg_Info_Sub(), pnd_Data_Passed, pnd_W_Date_A_Pnd_W_Date_Mm, pnd_Mdib_Parms_Pnd_Mdib_Pymnt_Mde,  //Natural: CALLNAT 'ADSN040' MSG-INFO-SUB #DATA-PASSED #W-DATE-MM #MDIB-PYMNT-MDE ADI-PYMNT-MODE
            ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode());
        if (condition(Global.isEscape())) return;
        getReports().write(0, Global.getPROGRAM(),"***** After calling ADSN040   ************",NEWLINE,"=",pnd_Data_Passed,NEWLINE,"=",pnd_W_Date_A_Pnd_W_Date_Mm,        //Natural: WRITE *PROGRAM '***** After calling ADSN040   ************' /'=' #DATA-PASSED /'=' #W-DATE-MM /'=' #MDIB-PYMNT-MDE /'=' ADI-PYMNT-MODE /'=' ##RETURN-CODE
            NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Pymnt_Mde,NEWLINE,"=",ldaAdsl450.getAds_Ia_Rslt_View_Adi_Pymnt_Mode(),NEWLINE,"=",pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code());
        if (Global.isEscape()) return;
        if (condition(pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E")))                                                                                     //Natural: IF ##RETURN-CODE = 'E'
        {
            pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L63");                                                                                                 //Natural: ASSIGN #ERROR-STATUS := 'L63'
            pnd_Return_Message.setValue(pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                                       //Natural: ASSIGN #RETURN-MESSAGE := MSG-INFO-SUB.##MSG
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  MDIB TEST PROGRAM (AIAN034)
        getReports().write(0, Global.getPROGRAM(),"***** Before calling MDIB test prm AIAN034 *****",NEWLINE,"#MDIB-PARMS:",NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Option,   //Natural: WRITE *PROGRAM '***** Before calling MDIB test prm AIAN034 *****' /'#MDIB-PARMS:' /'=' #MDIB-OPTION /'=' #MDIB-ANNT-DOB /'=' #MDIB-2ND-ANNT-DOB /'=' #MDIB-STTLMNT-DTE /'=' #MDIB-TOT-TIAA-ACCUM /'=' #MDIB-TOT-CREF-ACCUM /'=' #MDIB-TOT-TIAA-PYMNT /'=' #MDIB-TOT-CREF-PYMNT /'=' #MDIB-2ND-ANNT-TYPE /'=' #MDIB-PYMNT-MDE /'=' #MDIB-RETURN-CDE /'=' #MDIB-RETURN-MSG
            NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Annt_Dob,NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_2nd_Annt_Dob,NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Sttlmnt_Dte,
            NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Tot_Tiaa_Accum,NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Tot_Cref_Accum,NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Tot_Tiaa_Pymnt,
            NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Tot_Cref_Pymnt,NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_2nd_Annt_Type,NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Pymnt_Mde,
            NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Return_Cde,NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Return_Msg);
        if (Global.isEscape()) return;
        DbsUtil.callnat(Aian034.class , getCurrentProcessState(), pnd_Mdib_Parms);                                                                                        //Natural: CALLNAT 'AIAN034' #MDIB-PARMS
        if (condition(Global.isEscape())) return;
        getReports().write(0, Global.getPROGRAM(),"***** AFTER  calling MDIB test prm AIAN034 *****",NEWLINE,"#MDIB-PARMS:",NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Option,   //Natural: WRITE *PROGRAM '***** AFTER  calling MDIB test prm AIAN034 *****' / '#MDIB-PARMS:' /'=' #MDIB-OPTION /'=' #MDIB-ANNT-DOB /'=' #MDIB-2ND-ANNT-DOB /'=' #MDIB-STTLMNT-DTE /'=' #MDIB-TOT-TIAA-ACCUM /'=' #MDIB-TOT-CREF-ACCUM /'=' #MDIB-TOT-TIAA-PYMNT /'=' #MDIB-TOT-CREF-PYMNT /'=' #MDIB-2ND-ANNT-TYPE /'=' #MDIB-PYMNT-MDE /'=' #MDIB-RETURN-CDE /'=' #MDIB-RETURN-MSG / *PROGRAM '********** END OF TEST-MDIB (AIAN034) ************'
            NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Annt_Dob,NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_2nd_Annt_Dob,NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Sttlmnt_Dte,
            NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Tot_Tiaa_Accum,NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Tot_Cref_Accum,NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Tot_Tiaa_Pymnt,
            NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Tot_Cref_Pymnt,NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_2nd_Annt_Type,NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Pymnt_Mde,
            NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Return_Cde,NEWLINE,"=",pnd_Mdib_Parms_Pnd_Mdib_Return_Msg,NEWLINE,Global.getPROGRAM(),"********** END OF TEST-MDIB (AIAN034) ************");
        if (Global.isEscape()) return;
        if (condition(pnd_Mdib_Parms_Pnd_Mdib_Return_Cde.notEquals(getZero())))                                                                                           //Natural: IF #MDIB-RETURN-CDE NE 0
        {
            pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue("L63");                                                                                                 //Natural: ASSIGN #ERROR-STATUS := 'L63'
            pnd_Return_Message.setValue(pnd_Mdib_Parms_Pnd_Mdib_Return_Msg);                                                                                              //Natural: ASSIGN #RETURN-MESSAGE := #MDIB-RETURN-MSG
        }                                                                                                                                                                 //Natural: END-IF
        //*   TEST-MDIB-AIAN034
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=120 PS=100 SG=OFF");
    }
}
