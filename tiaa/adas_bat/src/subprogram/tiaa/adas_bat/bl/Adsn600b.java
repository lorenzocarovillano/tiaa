/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:54:52 PM
**        * FROM NATURAL SUBPROGRAM : Adsn600b
************************************************************
**        * FILE NAME            : Adsn600b.java
**        * CLASS NAME           : Adsn600b
**        * INSTANCE NAME        : Adsn600b
************************************************************
************************************************************************
*
* PROGRAM:  ADSN600B (FORMERLY NAZN600B)
* DATE   :  07/11/1997
* FUNCTION: CREATE HISTORICAL RATES
*
*
*
*
*
*
*
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn600b extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    public DbsRecord parameters;
    private DbsField pnd_Reval_Dte_A;
    private DbsField pnd_Nai_Pymnt_Mode;
    private DbsField pnd_Cntrct_Py_Dte_Key;

    private DbsGroup pnd_Cntrct_Py_Dte_Key__R_Field_1;

    private DbsGroup pnd_Cntrct_Py_Dte_Key_Hist_Structure;
    private DbsField pnd_Cntrct_Py_Dte_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Cntrct_Py_Dte_Key_Cntrct_Payee_Cde;
    private DbsField pnd_Cntrct_Py_Dte_Key_Fund_Invrse_Lst_Pd_Dte;
    private DbsField pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde;

    private DbsGroup pnd_Cntrct_Py_Dte_Key__R_Field_2;
    private DbsField pnd_Cntrct_Py_Dte_Key__Filler1;
    private DbsField pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Cde;
    private DbsField pnd_Nai_Dtl_Cref_Ia_Rate_Cd;
    private DbsField pnd_Nai_Dtl_Cref_Annl_Nbr_Units;
    private DbsField pnd_Nai_Dtl_Cref_Annl_Unit_Val;
    private DbsField pnd_Nai_Dtl_Cref_Annl_Amt;

    private DataAccessProgramView vw_iaa_Old_Cref_Rates;
    private DbsField iaa_Old_Cref_Rates_Fund_Lst_Pd_Dte;
    private DbsField iaa_Old_Cref_Rates_Fund_Invrse_Lst_Pd_Dte;
    private DbsField iaa_Old_Cref_Rates_Trans_Dte;
    private DbsField iaa_Old_Cref_Rates_Trans_User_Area;
    private DbsField iaa_Old_Cref_Rates_Trans_User_Id;
    private DbsField iaa_Old_Cref_Rates_Trans_Verify_Id;
    private DbsField iaa_Old_Cref_Rates_Trans_Verify_Dte;
    private DbsField iaa_Old_Cref_Rates_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Old_Cref_Rates_Cntrct_Payee_Cde;
    private DbsField iaa_Old_Cref_Rates_Cntrct_Mode_Ind;
    private DbsField iaa_Old_Cref_Rates_Rcrd_Srce;
    private DbsField iaa_Old_Cref_Rates_Rcrd_Status;
    private DbsField iaa_Old_Cref_Rates_Cmpny_Fund_Cde;

    private DbsGroup iaa_Old_Cref_Rates__R_Field_3;
    private DbsField iaa_Old_Cref_Rates_Cmpny_Cde;
    private DbsField iaa_Old_Cref_Rates_Fund_Cde;
    private DbsField iaa_Old_Cref_Rates_Cntrct_Tot_Per_Amt;
    private DbsField iaa_Old_Cref_Rates_Cntrct_Tot_Units;
    private DbsField iaa_Old_Cref_Rates_Lst_Trans_Dte;
    private DbsField iaa_Old_Cref_Rates_Count_Castcref_Rate_Data_Grp;

    private DbsGroup iaa_Old_Cref_Rates_Cref_Rate_Data_Grp;
    private DbsField iaa_Old_Cref_Rates_Cref_Rate_Cde;
    private DbsField iaa_Old_Cref_Rates_Cref_Rate_Dte;
    private DbsField iaa_Old_Cref_Rates_Cref_Per_Pay_Amt;
    private DbsField iaa_Old_Cref_Rates_Cref_No_Units;
    private DbsField pnd_Wdte8;

    private DbsGroup pnd_Wdte8__R_Field_4;
    private DbsField pnd_Wdte8_Pnd_W_Yyyy;
    private DbsField pnd_Wdte8_Pnd_W_Mm;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pnd_Reval_Dte_A = parameters.newFieldInRecord("pnd_Reval_Dte_A", "#REVAL-DTE-A", FieldType.STRING, 8);
        pnd_Reval_Dte_A.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Pymnt_Mode = parameters.newFieldInRecord("pnd_Nai_Pymnt_Mode", "#NAI-PYMNT-MODE", FieldType.NUMERIC, 3);
        pnd_Nai_Pymnt_Mode.setParameterOption(ParameterOption.ByReference);
        pnd_Cntrct_Py_Dte_Key = parameters.newFieldInRecord("pnd_Cntrct_Py_Dte_Key", "#CNTRCT-PY-DTE-KEY", FieldType.STRING, 23);
        pnd_Cntrct_Py_Dte_Key.setParameterOption(ParameterOption.ByReference);

        pnd_Cntrct_Py_Dte_Key__R_Field_1 = parameters.newGroupInRecord("pnd_Cntrct_Py_Dte_Key__R_Field_1", "REDEFINE", pnd_Cntrct_Py_Dte_Key);

        pnd_Cntrct_Py_Dte_Key_Hist_Structure = pnd_Cntrct_Py_Dte_Key__R_Field_1.newGroupInGroup("pnd_Cntrct_Py_Dte_Key_Hist_Structure", "HIST-STRUCTURE");
        pnd_Cntrct_Py_Dte_Key_Cntrct_Ppcn_Nbr = pnd_Cntrct_Py_Dte_Key_Hist_Structure.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Cntrct_Py_Dte_Key_Cntrct_Payee_Cde = pnd_Cntrct_Py_Dte_Key_Hist_Structure.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Cntrct_Py_Dte_Key_Fund_Invrse_Lst_Pd_Dte = pnd_Cntrct_Py_Dte_Key_Hist_Structure.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Fund_Invrse_Lst_Pd_Dte", 
            "FUND-INVRSE-LST-PD-DTE", FieldType.NUMERIC, 8);
        pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde = pnd_Cntrct_Py_Dte_Key_Hist_Structure.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde", "CMPNY-FUND-CDE", 
            FieldType.STRING, 3);

        pnd_Cntrct_Py_Dte_Key__R_Field_2 = pnd_Cntrct_Py_Dte_Key_Hist_Structure.newGroupInGroup("pnd_Cntrct_Py_Dte_Key__R_Field_2", "REDEFINE", pnd_Cntrct_Py_Dte_Key_Cmpny_Fund_Cde);
        pnd_Cntrct_Py_Dte_Key__Filler1 = pnd_Cntrct_Py_Dte_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Py_Dte_Key__Filler1", "_FILLER1", FieldType.STRING, 
            1);
        pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Cde = pnd_Cntrct_Py_Dte_Key__R_Field_2.newFieldInGroup("pnd_Cntrct_Py_Dte_Key_Pnd_Fund_Cde", "#FUND-CDE", FieldType.STRING, 
            2);
        pnd_Nai_Dtl_Cref_Ia_Rate_Cd = parameters.newFieldInRecord("pnd_Nai_Dtl_Cref_Ia_Rate_Cd", "#NAI-DTL-CREF-IA-RATE-CD", FieldType.STRING, 2);
        pnd_Nai_Dtl_Cref_Ia_Rate_Cd.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Dtl_Cref_Annl_Nbr_Units = parameters.newFieldInRecord("pnd_Nai_Dtl_Cref_Annl_Nbr_Units", "#NAI-DTL-CREF-ANNL-NBR-UNITS", FieldType.NUMERIC, 
            10, 4);
        pnd_Nai_Dtl_Cref_Annl_Nbr_Units.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Dtl_Cref_Annl_Unit_Val = parameters.newFieldInRecord("pnd_Nai_Dtl_Cref_Annl_Unit_Val", "#NAI-DTL-CREF-ANNL-UNIT-VAL", FieldType.NUMERIC, 
            7, 4);
        pnd_Nai_Dtl_Cref_Annl_Unit_Val.setParameterOption(ParameterOption.ByReference);
        pnd_Nai_Dtl_Cref_Annl_Amt = parameters.newFieldInRecord("pnd_Nai_Dtl_Cref_Annl_Amt", "#NAI-DTL-CREF-ANNL-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Nai_Dtl_Cref_Annl_Amt.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();

        vw_iaa_Old_Cref_Rates = new DataAccessProgramView(new NameInfo("vw_iaa_Old_Cref_Rates", "IAA-OLD-CREF-RATES"), "IAA_OLD_CREF_RATES", "IA_OLD_RATES", 
            DdmPeriodicGroups.getInstance().getGroups("IAA_OLD_CREF_RATES"));
        iaa_Old_Cref_Rates_Fund_Lst_Pd_Dte = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Fund_Lst_Pd_Dte", "FUND-LST-PD-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "FUND_LST_PD_DTE");
        iaa_Old_Cref_Rates_Fund_Invrse_Lst_Pd_Dte = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Fund_Invrse_Lst_Pd_Dte", "FUND-INVRSE-LST-PD-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "FUND_INVRSE_LST_PD_DTE");
        iaa_Old_Cref_Rates_Trans_Dte = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Trans_Dte", "TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "TRANS_DTE");
        iaa_Old_Cref_Rates_Trans_User_Area = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Trans_User_Area", "TRANS-USER-AREA", 
            FieldType.STRING, 6, RepeatingFieldStrategy.None, "TRANS_USER_AREA");
        iaa_Old_Cref_Rates_Trans_User_Id = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Trans_User_Id", "TRANS-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "TRANS_USER_ID");
        iaa_Old_Cref_Rates_Trans_Verify_Id = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Trans_Verify_Id", "TRANS-VERIFY-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "TRANS_VERIFY_ID");
        iaa_Old_Cref_Rates_Trans_Verify_Dte = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Trans_Verify_Dte", "TRANS-VERIFY-DTE", 
            FieldType.TIME, RepeatingFieldStrategy.None, "TRANS_VERIFY_DTE");
        iaa_Old_Cref_Rates_Cntrct_Ppcn_Nbr = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        iaa_Old_Cref_Rates_Cntrct_Payee_Cde = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        iaa_Old_Cref_Rates_Cntrct_Mode_Ind = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Cntrct_Mode_Ind", "CNTRCT-MODE-IND", 
            FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, "CNTRCT_MODE_IND");
        iaa_Old_Cref_Rates_Rcrd_Srce = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Rcrd_Srce", "RCRD-SRCE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "RCRD_SRCE");
        iaa_Old_Cref_Rates_Rcrd_Status = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Rcrd_Status", "RCRD-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "RCRD_STATUS");
        iaa_Old_Cref_Rates_Cmpny_Fund_Cde = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Cmpny_Fund_Cde", "CMPNY-FUND-CDE", FieldType.STRING, 
            3, RepeatingFieldStrategy.None, "CMPNY_FUND_CDE");

        iaa_Old_Cref_Rates__R_Field_3 = vw_iaa_Old_Cref_Rates.getRecord().newGroupInGroup("iaa_Old_Cref_Rates__R_Field_3", "REDEFINE", iaa_Old_Cref_Rates_Cmpny_Fund_Cde);
        iaa_Old_Cref_Rates_Cmpny_Cde = iaa_Old_Cref_Rates__R_Field_3.newFieldInGroup("iaa_Old_Cref_Rates_Cmpny_Cde", "CMPNY-CDE", FieldType.STRING, 1);
        iaa_Old_Cref_Rates_Fund_Cde = iaa_Old_Cref_Rates__R_Field_3.newFieldInGroup("iaa_Old_Cref_Rates_Fund_Cde", "FUND-CDE", FieldType.STRING, 2);
        iaa_Old_Cref_Rates_Cntrct_Tot_Per_Amt = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Cntrct_Tot_Per_Amt", "CNTRCT-TOT-PER-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, "CNTRCT_TOT_PER_AMT");
        iaa_Old_Cref_Rates_Cntrct_Tot_Units = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Cntrct_Tot_Units", "CNTRCT-TOT-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3, RepeatingFieldStrategy.None, "CNTRCT_TOT_UNITS");
        iaa_Old_Cref_Rates_Lst_Trans_Dte = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Lst_Trans_Dte", "LST-TRANS-DTE", FieldType.TIME, 
            RepeatingFieldStrategy.None, "LST_TRANS_DTE");
        iaa_Old_Cref_Rates_Count_Castcref_Rate_Data_Grp = vw_iaa_Old_Cref_Rates.getRecord().newFieldInGroup("iaa_Old_Cref_Rates_Count_Castcref_Rate_Data_Grp", 
            "C*CREF-RATE-DATA-GRP", RepeatingFieldStrategy.CAsteriskVariable, "IA_OLD_RATES_CREF_RATE_DATA_GRP");

        iaa_Old_Cref_Rates_Cref_Rate_Data_Grp = vw_iaa_Old_Cref_Rates.getRecord().newGroupInGroup("iaa_Old_Cref_Rates_Cref_Rate_Data_Grp", "CREF-RATE-DATA-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Cref_Rates_Cref_Rate_Cde = iaa_Old_Cref_Rates_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_OLD_CREF_RATES_CREF_RATE_CDE", "CREF-RATE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_CDE", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Cref_Rates_Cref_Rate_Dte = iaa_Old_Cref_Rates_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_OLD_CREF_RATES_CREF_RATE_DTE", "CREF-RATE-DTE", 
            FieldType.DATE, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_RATE_DTE", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Cref_Rates_Cref_Per_Pay_Amt = iaa_Old_Cref_Rates_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_OLD_CREF_RATES_CREF_PER_PAY_AMT", "CREF-PER-PAY-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_PER_PAY_AMT", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        iaa_Old_Cref_Rates_Cref_No_Units = iaa_Old_Cref_Rates_Cref_Rate_Data_Grp.newFieldArrayInGroup("IAA_OLD_CREF_RATES_CREF_NO_UNITS", "CREF-NO-UNITS", 
            FieldType.PACKED_DECIMAL, 9, 3, new DbsArrayController(1, 5) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "TIAA_NO_UNITS", "IA_OLD_RATES_CREF_RATE_DATA_GRP");
        registerRecord(vw_iaa_Old_Cref_Rates);

        pnd_Wdte8 = localVariables.newFieldInRecord("pnd_Wdte8", "#WDTE8", FieldType.STRING, 8);

        pnd_Wdte8__R_Field_4 = localVariables.newGroupInRecord("pnd_Wdte8__R_Field_4", "REDEFINE", pnd_Wdte8);
        pnd_Wdte8_Pnd_W_Yyyy = pnd_Wdte8__R_Field_4.newFieldInGroup("pnd_Wdte8_Pnd_W_Yyyy", "#W-YYYY", FieldType.NUMERIC, 4);
        pnd_Wdte8_Pnd_W_Mm = pnd_Wdte8__R_Field_4.newFieldInGroup("pnd_Wdte8_Pnd_W_Mm", "#W-MM", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_iaa_Old_Cref_Rates.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn600b() throws Exception
    {
        super("Adsn600b");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        pnd_Wdte8.setValue(pnd_Reval_Dte_A);                                                                                                                              //Natural: ASSIGN #WDTE8 := #REVAL-DTE-A
        pnd_Wdte8_Pnd_W_Mm.nsubtract(1);                                                                                                                                  //Natural: SUBTRACT 1 FROM #W-MM
        if (condition(pnd_Wdte8_Pnd_W_Mm.equals(getZero())))                                                                                                              //Natural: IF #W-MM = 0
        {
            pnd_Wdte8_Pnd_W_Mm.setValue(12);                                                                                                                              //Natural: ASSIGN #W-MM := 12
            pnd_Wdte8_Pnd_W_Yyyy.nsubtract(1);                                                                                                                            //Natural: SUBTRACT 1 FROM #W-YYYY
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Cntrct_Py_Dte_Key_Fund_Invrse_Lst_Pd_Dte.compute(new ComputeParameters(false, pnd_Cntrct_Py_Dte_Key_Fund_Invrse_Lst_Pd_Dte), DbsField.subtract(100000000,     //Natural: ASSIGN #CNTRCT-PY-DTE-KEY.FUND-INVRSE-LST-PD-DTE := 100000000 - VAL ( #WDTE8 )
            pnd_Wdte8.val()));
        vw_iaa_Old_Cref_Rates.startDatabaseFind                                                                                                                           //Natural: FIND ( 1 ) IAA-OLD-CREF-RATES WITH CNTRCT-PY-DTE-KEY = #CNTRCT-PY-DTE-KEY
        (
        "FC",
        new Wc[] { new Wc("CNTRCT_PY_DTE_KEY", "=", pnd_Cntrct_Py_Dte_Key, WcType.WITH) },
        1
        );
        FC:
        while (condition(vw_iaa_Old_Cref_Rates.readNextRow("FC", true)))
        {
            vw_iaa_Old_Cref_Rates.setIfNotFoundControlFlag(false);
            if (condition(vw_iaa_Old_Cref_Rates.getAstCOUNTER().equals(0)))                                                                                               //Natural: IF NO RECORDS FOUND
            {
                vw_iaa_Old_Cref_Rates.setValuesByName(pnd_Cntrct_Py_Dte_Key_Hist_Structure);                                                                              //Natural: MOVE BY NAME HIST-STRUCTURE TO IAA-OLD-CREF-RATES
                iaa_Old_Cref_Rates_Trans_Dte.setValue(Global.getTIMX());                                                                                                  //Natural: ASSIGN IAA-OLD-CREF-RATES.TRANS-DTE := *TIMX
                iaa_Old_Cref_Rates_Lst_Trans_Dte.setValue(iaa_Old_Cref_Rates_Trans_Dte);                                                                                  //Natural: ASSIGN IAA-OLD-CREF-RATES.LST-TRANS-DTE := IAA-OLD-CREF-RATES.TRANS-DTE
                iaa_Old_Cref_Rates_Cntrct_Mode_Ind.setValue(pnd_Nai_Pymnt_Mode);                                                                                          //Natural: ASSIGN IAA-OLD-CREF-RATES.CNTRCT-MODE-IND := #NAI-PYMNT-MODE
                iaa_Old_Cref_Rates_Fund_Lst_Pd_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Wdte8);                                                              //Natural: MOVE EDITED #WDTE8 TO IAA-OLD-CREF-RATES.FUND-LST-PD-DTE ( EM = YYYYMMDD )
                iaa_Old_Cref_Rates_Cref_Rate_Cde.getValue(1).setValue(pnd_Nai_Dtl_Cref_Ia_Rate_Cd);                                                                       //Natural: ASSIGN IAA-OLD-CREF-RATES.CREF-RATE-CDE ( 1 ) := #NAI-DTL-CREF-IA-RATE-CD
                iaa_Old_Cref_Rates_Cref_No_Units.getValue(1).setValue(pnd_Nai_Dtl_Cref_Annl_Nbr_Units);                                                                   //Natural: ASSIGN IAA-OLD-CREF-RATES.CREF-NO-UNITS ( 1 ) := #NAI-DTL-CREF-ANNL-NBR-UNITS
                iaa_Old_Cref_Rates_Cref_Per_Pay_Amt.getValue(1).setValue(pnd_Nai_Dtl_Cref_Annl_Amt);                                                                      //Natural: ASSIGN IAA-OLD-CREF-RATES.CREF-PER-PAY-AMT ( 1 ) := #NAI-DTL-CREF-ANNL-AMT
                iaa_Old_Cref_Rates_Cntrct_Tot_Per_Amt.setValue(pnd_Nai_Dtl_Cref_Annl_Amt);                                                                                //Natural: ASSIGN IAA-OLD-CREF-RATES.CNTRCT-TOT-PER-AMT := #NAI-DTL-CREF-ANNL-AMT
                iaa_Old_Cref_Rates_Cntrct_Tot_Units.setValue(pnd_Nai_Dtl_Cref_Annl_Nbr_Units);                                                                            //Natural: ASSIGN IAA-OLD-CREF-RATES.CNTRCT-TOT-UNITS := #NAI-DTL-CREF-ANNL-NBR-UNITS
                iaa_Old_Cref_Rates_Rcrd_Srce.setValue("NZ");                                                                                                              //Natural: ASSIGN IAA-OLD-CREF-RATES.RCRD-SRCE := 'NZ'
                iaa_Old_Cref_Rates_Rcrd_Status.setValue("A");                                                                                                             //Natural: ASSIGN IAA-OLD-CREF-RATES.RCRD-STATUS := 'A'
                vw_iaa_Old_Cref_Rates.insertDBRow();                                                                                                                      //Natural: STORE IAA-OLD-CREF-RATES
                //*    IAA-OLD-CREF-RATES.TRANS-USER-AREA
                //*    IAA-OLD-CREF-RATES.TRANS-USER-ID
                //*    IAA-OLD-CREF-RATES.TRANS-VERIFY-ID
                //*    IAA-OLD-CREF-RATES.TRANS-VERIFY-DTE
            }                                                                                                                                                             //Natural: END-NOREC
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //
}
