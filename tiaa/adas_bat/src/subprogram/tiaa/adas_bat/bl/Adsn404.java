/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:54:09 PM
**        * FROM NATURAL SUBPROGRAM : Adsn404
************************************************************
**        * FILE NAME            : Adsn404.java
**        * CLASS NAME           : Adsn404
**        * INSTANCE NAME        : Adsn404
************************************************************
************************************************************************
*
*  PROGRAM FUNCTIONS:
*
*  FOR AC ROLLOVERS
*
*  - CHECKS IVC ROLLOVER ELIGIBILITY RULES
*
*  MAINTENANCE:
*
*  NAME          DATE       DESCRIPTION
*  ----          ----       -----------
* M.NACHBER    04192005    REMOVE #EGTRRA IVC ORDERING RULE PROCESSING
* M.NACHBER    09/06/2005  REL 6.5 FOR IPRO/TPA
*                          NO CHANGES, JUST RECOMPLILED TO PICK UP
*                          A NEW VERSION OF ADSA401
* 03/17/2008 E.MELNIK     ROTH 403B/401K CHANGES.  RECOMPILED FOR
*                         NEW ADSA401.
* 04/16/2010              RESTOW FOR CHANGE TO PDA ADSA401
* 03/01/2012 O. SOTTO     RESTOW FOR CHANGE TO PDA ADSA401
* 09/24/2013 E. MELNIK    RECOMPILED FOR NEW ADSA401.  CREF/REA REDESIGN
*                         CHANGES.
* 03/23/17 E. MELNIK      RESTOWED FOR PIN EXPANSION.
* 04/12/19 E. MELNIK      RESTOWED FOR UPDATED ADSA401 WITH IRC CDE.
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn404 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAdsa401 pdaAdsa401;
    private PdaAdspda_M pdaAdspda_M;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Table_Code;
    private DbsField pnd_Table_Key;

    private DbsGroup pnd_Table_Key__R_Field_1;
    private DbsField pnd_Table_Key_Pnd_Table_Lvl1_Id;
    private DbsField pnd_Table_Key_Pnd_Table_Lvl2_Id;
    private DbsField pnd_Table_Key_Pnd_Table_Lvl3_Id;
    private DbsField pnd_Desc;
    private DbsField pnd_Long_Desc;
    private DbsField pnd_Misc_Fld;
    private DbsField pnd_I;
    private DbsField pnd_Max_Frm_Ircs;
    private DbsField pnd_Stts_Cd;
    private DbsField pnd_Ivc_Elgblty;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAdspda_M = new PdaAdspda_M(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaAdsa401 = new PdaAdsa401(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Table_Code = localVariables.newFieldInRecord("pnd_Table_Code", "#TABLE-CODE", FieldType.STRING, 3);
        pnd_Table_Key = localVariables.newFieldInRecord("pnd_Table_Key", "#TABLE-KEY", FieldType.STRING, 29);

        pnd_Table_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Table_Key__R_Field_1", "REDEFINE", pnd_Table_Key);
        pnd_Table_Key_Pnd_Table_Lvl1_Id = pnd_Table_Key__R_Field_1.newFieldInGroup("pnd_Table_Key_Pnd_Table_Lvl1_Id", "#TABLE-LVL1-ID", FieldType.STRING, 
            6);
        pnd_Table_Key_Pnd_Table_Lvl2_Id = pnd_Table_Key__R_Field_1.newFieldInGroup("pnd_Table_Key_Pnd_Table_Lvl2_Id", "#TABLE-LVL2-ID", FieldType.STRING, 
            3);
        pnd_Table_Key_Pnd_Table_Lvl3_Id = pnd_Table_Key__R_Field_1.newFieldInGroup("pnd_Table_Key_Pnd_Table_Lvl3_Id", "#TABLE-LVL3-ID", FieldType.STRING, 
            20);
        pnd_Desc = localVariables.newFieldInRecord("pnd_Desc", "#DESC", FieldType.STRING, 60);
        pnd_Long_Desc = localVariables.newFieldInRecord("pnd_Long_Desc", "#LONG-DESC", FieldType.STRING, 80);
        pnd_Misc_Fld = localVariables.newFieldInRecord("pnd_Misc_Fld", "#MISC-FLD", FieldType.STRING, 80);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Max_Frm_Ircs = localVariables.newFieldInRecord("pnd_Max_Frm_Ircs", "#MAX-FRM-IRCS", FieldType.PACKED_DECIMAL, 3);
        pnd_Stts_Cd = localVariables.newFieldInRecord("pnd_Stts_Cd", "#STTS-CD", FieldType.STRING, 3);
        pnd_Ivc_Elgblty = localVariables.newFieldInRecord("pnd_Ivc_Elgblty", "#IVC-ELGBLTY", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Max_Frm_Ircs.setInitialValue(15);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn404() throws Exception
    {
        super("Adsn404");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Ivc_Rlvr_Cde().equals(" ")))                                                                                          //Natural: IF ADP-IVC-RLVR-CDE = ' '
        {
            pnd_Stts_Cd.setValue("L11");                                                                                                                                  //Natural: MOVE 'L11' TO #STTS-CD
                                                                                                                                                                          //Natural: PERFORM SET-STTS-CDS
            sub_Set_Stts_Cds();
            if (condition(Global.isEscape())) {return;}
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Table_Code.setValue("IVC");                                                                                                                                   //Natural: ASSIGN #TABLE-CODE := 'IVC'
        pnd_Table_Key_Pnd_Table_Lvl1_Id.setValue("NAZ034");                                                                                                               //Natural: ASSIGN #TABLE-LVL1-ID := 'NAZ034'
        pnd_Table_Key_Pnd_Table_Lvl2_Id.setValue("NAZ");                                                                                                                  //Natural: ASSIGN #TABLE-LVL2-ID := 'NAZ'
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO #MAX-FRM-IRCS
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Frm_Ircs)); pnd_I.nadd(1))
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Frm_Irc_Cde().getValue(pnd_I).equals(" ")))                                                                       //Natural: IF ADP-FRM-IRC-CDE ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_I.greater(1) && pdaAdsa401.getPnd_Adsa401_Adp_Frm_Irc_Cde().getValue(pnd_I).equals(pdaAdsa401.getPnd_Adsa401_Adp_Frm_Irc_Cde().getValue(pnd_I.getDec().subtract(1))))) //Natural: IF #I GT 1 AND ADP-FRM-IRC-CDE ( #I ) = ADP-FRM-IRC-CDE ( #I - 1 )
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Table_Key_Pnd_Table_Lvl3_Id.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaAdsa401.getPnd_Adsa401_Adp_Frm_Irc_Cde().getValue(pnd_I),         //Natural: COMPRESS ADP-FRM-IRC-CDE ( #I ) ADP-TO-IRC-CDE INTO #TABLE-LVL3-ID LEAVING NO SPACE
                pdaAdsa401.getPnd_Adsa401_Adp_To_Irc_Cde()));
                                                                                                                                                                          //Natural: PERFORM GET-INFO-FROM-TABLE
            sub_Get_Info_From_Table();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().equals("E")))                                                                                 //Natural: IF MSG-INFO-SUB.##RETURN-CODE = 'E'
            {
                if (condition(pnd_Ivc_Elgblty.equals(" ")))                                                                                                               //Natural: IF #IVC-ELGBLTY = ' '
                {
                    pnd_Ivc_Elgblty.setValue("N");                                                                                                                        //Natural: MOVE 'N' TO #IVC-ELGBLTY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Ivc_Elgblty.equals("Y")))                                                                                                           //Natural: IF #IVC-ELGBLTY = 'Y'
                    {
                        pnd_Stts_Cd.setValue("L12");                                                                                                                      //Natural: MOVE 'L12' TO #STTS-CD
                                                                                                                                                                          //Natural: PERFORM SET-STTS-CDS
                        sub_Set_Stts_Cds();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) return;                                                                                                                      //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Ivc_Elgblty.equals(" ")))                                                                                                               //Natural: IF #IVC-ELGBLTY = ' '
                {
                    pnd_Ivc_Elgblty.setValue("Y");                                                                                                                        //Natural: MOVE 'Y' TO #IVC-ELGBLTY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Ivc_Elgblty.equals("N")))                                                                                                           //Natural: IF #IVC-ELGBLTY = 'N'
                    {
                        pnd_Stts_Cd.setValue("L12");                                                                                                                      //Natural: MOVE 'L12' TO #STTS-CD
                                                                                                                                                                          //Natural: PERFORM SET-STTS-CDS
                        sub_Set_Stts_Cds();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) return;                                                                                                                      //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  IVC NOT ELIGIBLE TO BE ROLLED OVER
        if (condition(pnd_Ivc_Elgblty.equals("N")))                                                                                                                       //Natural: IF #IVC-ELGBLTY = 'N'
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Ivc_Rlvr_Cde().equals("Y")))                                                                                      //Natural: IF ADP-IVC-RLVR-CDE = 'Y'
            {
                pnd_Stts_Cd.setValue("L13");                                                                                                                              //Natural: MOVE 'L13' TO #STTS-CD
                                                                                                                                                                          //Natural: PERFORM SET-STTS-CDS
                sub_Set_Stts_Cds();
                if (condition(Global.isEscape())) {return;}
                //*   04192005   MN
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
                //*   04192005   MN
            }                                                                                                                                                             //Natural: END-IF
            //*   04192005   MN
        }                                                                                                                                                                 //Natural: END-IF
        //*  ELSE
        pdaAdsa401.getPnd_Adsa401_Adp_Ivc_Pymnt_Rule().setValue("P");                                                                                                     //Natural: ASSIGN ADP-IVC-PYMNT-RULE := 'P'
        //* *  04192005  MN START
        //*  END-IF
        //*  ELSE                   /* IVC ELIGIBLE TO BE ROLLED OVER
        //*   IF ADP-ALT-DEST-RLVR-DEST NE ' ' AND
        //*       SUBSTR(ADP-ALT-DEST-RLVR-DEST,1,3) NE 'IRA'
        //*     IF ADC-TRK-RO-IVC = 'N'
        //*       ADP-IVC-PYMNT-RULE := 'P'
        //*     ELSE
        //*       ADP-IVC-PYMNT-RULE := 'O'
        //*     END-IF
        //*   ELSE
        //*     ADP-IVC-PYMNT-RULE := 'O'
        //*  END-IF
        //*  END-IF
        //* *  04192005 MN END
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-INFO-FROM-TABLE
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-STTS-CDS
    }
    private void sub_Get_Info_From_Table() throws Exception                                                                                                               //Natural: GET-INFO-FROM-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.callnat(Adsn032.class , getCurrentProcessState(), pdaAdspda_M.getMsg_Info_Sub(), pnd_Table_Code, pnd_Table_Key, pnd_Desc, pnd_Long_Desc,                  //Natural: CALLNAT 'ADSN032' MSG-INFO-SUB #TABLE-CODE #TABLE-KEY #DESC #LONG-DESC #MISC-FLD
            pnd_Misc_Fld);
        if (condition(Global.isEscape())) return;
    }
    private void sub_Set_Stts_Cds() throws Exception                                                                                                                      //Natural: SET-STTS-CDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue(pnd_Stts_Cd);                                                                                               //Natural: MOVE #STTS-CD TO #ERROR-STATUS ADC-ERR-CDE ( 1 )
        pdaAdsa401.getPnd_Adsa401_Adc_Err_Cde().getValue(1).setValue(pnd_Stts_Cd);
        short decideConditionsMet395 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #STTS-CD;//Natural: VALUE 'L13'
        if (condition((pnd_Stts_Cd.equals("L13"))))
        {
            decideConditionsMet395++;
            pdaAdsa401.getPnd_Adsa401_Adc_Err_Msg().getValue(1).setValue("IVC NOT ELGBLE FOR RLVR");                                                                      //Natural: MOVE 'IVC NOT ELGBLE FOR RLVR' TO ADC-ERR-MSG ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'L11'
        else if (condition((pnd_Stts_Cd.equals("L11"))))
        {
            decideConditionsMet395++;
            pdaAdsa401.getPnd_Adsa401_Adc_Err_Msg().getValue(1).setValue("IVC RLVR INFO MISSING");                                                                        //Natural: MOVE 'IVC RLVR INFO MISSING' TO ADC-ERR-MSG ( 1 )
        }                                                                                                                                                                 //Natural: VALUE 'L12'
        else if (condition((pnd_Stts_Cd.equals("L12"))))
        {
            decideConditionsMet395++;
            pdaAdsa401.getPnd_Adsa401_Adc_Err_Msg().getValue(1).setValue("MIXED IVC RLVR ELGBLTY RULES");                                                                 //Natural: MOVE 'MIXED IVC RLVR ELGBLTY RULES' TO ADC-ERR-MSG ( 1 )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO #FUND-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaAdsa401.getPnd_Adsa401_Pnd_Fund_Cnt())); pnd_I.nadd(1))
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Tkr_Symbl().getValue(pnd_I).equals(" ")))                                                                         //Natural: IF ADC-TKR-SYMBL ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  MOVE #STTS-CD TO ADC-ACCT-STTS-CDE (#I)
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //
}
