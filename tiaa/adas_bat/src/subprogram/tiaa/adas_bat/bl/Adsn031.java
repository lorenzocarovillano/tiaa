/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:53:07 PM
**        * FROM NATURAL SUBPROGRAM : Adsn031
************************************************************
**        * FILE NAME            : Adsn031.java
**        * CLASS NAME           : Adsn031
**        * INSTANCE NAME        : Adsn031
************************************************************
**SAG GENERATOR: SHELL-TIAA                       VERSION: 3.2.2
**SAG TITLE: ACCESS TABLE-ENTRY
**SAG SYSTEM: ANNUITIZATION SUNGARD
************************************************************************
* PROGRAM  : ADSN031
* SYSTEM   : NEW ANNUITIZATION SYSTEM
* TITLE    : ACCESS TABLE-ENTRY
* GENERATED: APR 07,04
* FUNCTION : THIS SUBPROGRAM READS THE TABLE-ENTRY FILE TO TRANSLATE
*          : THE TWO BYTE ALPHA STATE CODE FROM DAI-CONTRACT INTO
*          : THE TWO BYTE NUMERIC STATE CODE OR VICE VERSA.
* MOD DATE   MOD BY    DESCRIPTION OF CHANGES
* --------   -------   ---------------------------------------------
* 4/07/2004  T.SHINE   CLONED NAZN031 FOR ANNUITIZATION SUNGARD (ADAS)
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn031 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAdspda_M pdaAdspda_M;

    // Local Variables
    public DbsRecord localVariables;

    // parameters
    private DbsField pnd_Alpha_Code;
    private DbsField pnd_Numeric_Code;
    private DbsField pnd_Record_Found;

    private DataAccessProgramView vw_table_Entry;
    private DbsField table_Entry_Entry_Actve_Ind;
    private DbsField table_Entry_Entry_Table_Id_Nbr;
    private DbsField table_Entry_Entry_Cde;
    private DbsField table_Entry_Entry_Dscrptn_Txt;
    private DbsField table_Entry_Entry_Updte_Tmstmp_Tme;
    private DbsField table_Entry_Entry_Updte_Tmstmp_Dte;
    private DbsField table_Entry_Entry_Updte_User_Id_Nme;
    private DbsField table_Entry_Entry_Mgrtn_Ind;
    private DbsField table_Entry_Entry_Mgrtn_Tmstmp_Tme;
    private DbsField table_Entry_Entry_Mgrtn_Tmstmp_Dte;
    private DbsField table_Entry_Entry_Mgrtn_User_Id_Nme;
    private DbsField table_Entry_Entry_Mgrtn_Rqst_Tmstmp_Dte;
    private DbsField table_Entry_Entry_Mgrtn_Rqst_User_Id_Nme;
    private DbsField pnd_Table_Super;

    private DbsGroup pnd_Table_Super__R_Field_1;
    private DbsField pnd_Table_Super_Pnd_Sp_Table_Id;
    private DbsField pnd_Table_Super_Pnd_Sp_Code;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // parameters
        parameters = new DbsRecord();
        pdaAdspda_M = new PdaAdspda_M(parameters);
        pnd_Alpha_Code = parameters.newFieldInRecord("pnd_Alpha_Code", "#ALPHA-CODE", FieldType.STRING, 2);
        pnd_Alpha_Code.setParameterOption(ParameterOption.ByReference);
        pnd_Numeric_Code = parameters.newFieldInRecord("pnd_Numeric_Code", "#NUMERIC-CODE", FieldType.STRING, 2);
        pnd_Numeric_Code.setParameterOption(ParameterOption.ByReference);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Record_Found = localVariables.newFieldInRecord("pnd_Record_Found", "#RECORD-FOUND", FieldType.BOOLEAN, 1);

        vw_table_Entry = new DataAccessProgramView(new NameInfo("vw_table_Entry", "TABLE-ENTRY"), "TABLE_ENTRY_DB48", "TABLE_DATA_1");
        table_Entry_Entry_Actve_Ind = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Actve_Ind", "ENTRY-ACTVE-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "ENTRY_ACTVE_IND");
        table_Entry_Entry_Table_Id_Nbr = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Table_Id_Nbr", "ENTRY-TABLE-ID-NBR", FieldType.NUMERIC, 
            6, RepeatingFieldStrategy.None, "ENTRY_TABLE_ID_NBR");
        table_Entry_Entry_Table_Id_Nbr.setDdmHeader("TABLE ID");
        table_Entry_Entry_Cde = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Cde", "ENTRY-CDE", FieldType.STRING, 20, RepeatingFieldStrategy.None, 
            "ENTRY_CDE");
        table_Entry_Entry_Cde.setDdmHeader("ENTRY CODE");
        table_Entry_Entry_Dscrptn_Txt = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Dscrptn_Txt", "ENTRY-DSCRPTN-TXT", FieldType.STRING, 
            60, RepeatingFieldStrategy.None, "ENTRY_DSCRPTN_TXT");
        table_Entry_Entry_Updte_Tmstmp_Tme = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Updte_Tmstmp_Tme", "ENTRY-UPDTE-TMSTMP-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ENTRY_UPDTE_TMSTMP_TME");
        table_Entry_Entry_Updte_Tmstmp_Dte = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Updte_Tmstmp_Dte", "ENTRY-UPDTE-TMSTMP-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ENTRY_UPDTE_TMSTMP_DTE");
        table_Entry_Entry_Updte_User_Id_Nme = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Updte_User_Id_Nme", "ENTRY-UPDTE-USER-ID-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_UPDTE_USER_ID_NME");
        table_Entry_Entry_Mgrtn_Ind = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Mgrtn_Ind", "ENTRY-MGRTN-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "ENTRY_MGRTN_IND");
        table_Entry_Entry_Mgrtn_Tmstmp_Tme = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Mgrtn_Tmstmp_Tme", "ENTRY-MGRTN-TMSTMP-TME", 
            FieldType.TIME, RepeatingFieldStrategy.None, "ENTRY_MGRTN_TMSTMP_TME");
        table_Entry_Entry_Mgrtn_Tmstmp_Dte = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Mgrtn_Tmstmp_Dte", "ENTRY-MGRTN-TMSTMP-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ENTRY_MGRTN_TMSTMP_DTE");
        table_Entry_Entry_Mgrtn_User_Id_Nme = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Mgrtn_User_Id_Nme", "ENTRY-MGRTN-USER-ID-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_MGRTN_USER_ID_NME");
        table_Entry_Entry_Mgrtn_Rqst_Tmstmp_Dte = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Mgrtn_Rqst_Tmstmp_Dte", "ENTRY-MGRTN-RQST-TMSTMP-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "ENTRY_MGRTN_RQST_TMSTMP_DTE");
        table_Entry_Entry_Mgrtn_Rqst_User_Id_Nme = vw_table_Entry.getRecord().newFieldInGroup("table_Entry_Entry_Mgrtn_Rqst_User_Id_Nme", "ENTRY-MGRTN-RQST-USER-ID-NME", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "ENTRY_MGRTN_RQST_USER_ID_NME");
        registerRecord(vw_table_Entry);

        pnd_Table_Super = localVariables.newFieldInRecord("pnd_Table_Super", "#TABLE-SUPER", FieldType.STRING, 26);

        pnd_Table_Super__R_Field_1 = localVariables.newGroupInRecord("pnd_Table_Super__R_Field_1", "REDEFINE", pnd_Table_Super);
        pnd_Table_Super_Pnd_Sp_Table_Id = pnd_Table_Super__R_Field_1.newFieldInGroup("pnd_Table_Super_Pnd_Sp_Table_Id", "#SP-TABLE-ID", FieldType.NUMERIC, 
            6);
        pnd_Table_Super_Pnd_Sp_Code = pnd_Table_Super__R_Field_1.newFieldInGroup("pnd_Table_Super_Pnd_Sp_Code", "#SP-CODE", FieldType.STRING, 20);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_table_Entry.reset();

        parameters.reset();
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn031() throws Exception
    {
        super("Adsn031");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //* ******************** START OF MAIN PROGRAM LOGIC **********************
        //*  STATE/PROVINCE TABLE
        pnd_Table_Super_Pnd_Sp_Table_Id.setValue(3);                                                                                                                      //Natural: ASSIGN #SP-TABLE-ID := 3
        short decideConditionsMet100 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #ALPHA-CODE NE ' '
        if (condition(pnd_Alpha_Code.notEquals(" ")))
        {
            decideConditionsMet100++;
            vw_table_Entry.startDatabaseRead                                                                                                                              //Natural: READ TABLE-ENTRY BY TABLE-ID-ENTRY-CDE STARTING FROM #TABLE-SUPER
            (
            "READ01",
            new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_Table_Super, WcType.BY) },
            new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") }
            );
            READ01:
            while (condition(vw_table_Entry.readNextRow("READ01")))
            {
                if (condition(table_Entry_Entry_Table_Id_Nbr.notEquals(pnd_Table_Super_Pnd_Sp_Table_Id)))                                                                 //Natural: IF ENTRY-TABLE-ID-NBR NE #SP-TABLE-ID
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(!(table_Entry_Entry_Actve_Ind.equals("Y") && table_Entry_Entry_Dscrptn_Txt.getSubstring(1,2).equals(pnd_Alpha_Code))))                      //Natural: ACCEPT IF ENTRY-ACTVE-IND EQ 'Y' AND SUBSTR ( ENTRY-DSCRPTN-TXT,1,2 ) EQ #ALPHA-CODE
                {
                    continue;
                }
                pnd_Record_Found.setValue(true);                                                                                                                          //Natural: ASSIGN #RECORD-FOUND := TRUE
                pnd_Numeric_Code.setValue(table_Entry_Entry_Cde.getSubstring(1,2));                                                                                       //Natural: ASSIGN #NUMERIC-CODE := SUBSTR ( ENTRY-CDE,1,2 )
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN #NUMERIC-CODE NE ' '
        else if (condition(pnd_Numeric_Code.notEquals(" ")))
        {
            decideConditionsMet100++;
            if (condition(((pnd_Numeric_Code.equals("00") || pnd_Numeric_Code.equals("3E")) || (DbsUtil.is(pnd_Numeric_Code.getText(),"N2") && pnd_Numeric_Code.greater(60))))) //Natural: IF #NUMERIC-CODE EQ '00' OR EQ '3E' OR ( #NUMERIC-CODE IS ( N2 ) AND VAL ( #NUMERIC-CODE ) GT 60 )
            {
                pnd_Alpha_Code.setValue("NY");                                                                                                                            //Natural: ASSIGN #ALPHA-CODE := 'NY'
                pnd_Record_Found.setValue(true);                                                                                                                          //Natural: ASSIGN #RECORD-FOUND := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                vw_table_Entry.startDatabaseRead                                                                                                                          //Natural: READ TABLE-ENTRY BY TABLE-ID-ENTRY-CDE STARTING FROM #TABLE-SUPER
                (
                "READ02",
                new Wc[] { new Wc("TABLE_ID_ENTRY_CDE", ">=", pnd_Table_Super, WcType.BY) },
                new Oc[] { new Oc("TABLE_ID_ENTRY_CDE", "ASC") }
                );
                READ02:
                while (condition(vw_table_Entry.readNextRow("READ02")))
                {
                    if (condition(table_Entry_Entry_Table_Id_Nbr.notEquals(pnd_Table_Super_Pnd_Sp_Table_Id)))                                                             //Natural: IF ENTRY-TABLE-ID-NBR NE #SP-TABLE-ID
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(!(table_Entry_Entry_Actve_Ind.equals("Y") && table_Entry_Entry_Cde.getSubstring(1,2).equals(pnd_Numeric_Code))))                        //Natural: ACCEPT IF ENTRY-ACTVE-IND EQ 'Y' AND SUBSTR ( ENTRY-CDE,1,2 ) EQ #NUMERIC-CODE
                    {
                        continue;
                    }
                    pnd_Record_Found.setValue(true);                                                                                                                      //Natural: ASSIGN #RECORD-FOUND := TRUE
                    pnd_Alpha_Code.setValue(table_Entry_Entry_Dscrptn_Txt.getSubstring(1,2));                                                                             //Natural: ASSIGN #ALPHA-CODE := SUBSTR ( ENTRY-DSCRPTN-TXT,1,2 )
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Record_Found.getBoolean()))                                                                                                                     //Natural: IF #RECORD-FOUND
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Return_Code().setValue("E");                                                                                              //Natural: ASSIGN MSG-INFO-SUB.##RETURN-CODE := 'E'
            pdaAdspda_M.getMsg_Info_Sub_Pnd_Pnd_Msg().setValue(DbsUtil.compress("Invalid state code", pnd_Alpha_Code, pnd_Numeric_Code, "passed"));                       //Natural: COMPRESS 'Invalid state code' #ALPHA-CODE #NUMERIC-CODE 'passed' INTO MSG-INFO-SUB.##MSG
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
