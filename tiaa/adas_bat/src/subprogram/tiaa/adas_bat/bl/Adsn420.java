/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-13 10:54:12 PM
**        * FROM NATURAL SUBPROGRAM : Adsn420
************************************************************
**        * FILE NAME            : Adsn420.java
**        * CLASS NAME           : Adsn420
**        * INSTANCE NAME        : Adsn420
************************************************************
************************************************************************
*
*  PROGRAM FUNCTIONS:
*
*  - CALCULATES TOTAL GRADED/STANDARD SETTLEMENT AMOUNTS FOR TIAA
*    AND MONTHLY/ANNUAL SETTLEMENT AMOUNTS FOR EACH REQUESTED CREF
*    FUND.
*  - CALCULATES TIAA RATE BY RATE GRADED/STANDARD SETTLEMENT AMOUNTS.
*  - CALLS ACTUARIAL MODULE FOR GRADED BENEFITS PROCESSING.
*
*  MAINTENANCE:
*
*  NAME          DATE       DESCRIPTION
*  ----          ----       -----------
*  M.NACHBER    08312005    TPA & IPRO CHANGES:
*                             CALCULATING GRADED/STANDARD GUARANTEED
*                             COMMUTED AMOUNTS
*  O. SOTTO     02/20/2008  EXPAND TIAA RATES TO 99 OCCURS. SC 022008.
* E. MELNIK     03/17/2008  403B/401K CHANGES.  RECOMPILED FOR
*                           NEW ADSA401.
* 01142009 E. MELNIK   TIAA ACCESS/STABLE RETURN ENHANCEMENTS.  MARKED
*                      BY EM - 011409.
* 04/06/10 D.E.ANDER   ADABAS REPLATFORM PCPOP MARKED DEA
* 05/20/10 C.MASON     CHANGED TO CALL ADSN605 FOR ORIGIN CODE
* 11/17/10 E. MELNIK   SVSAA CHANGES.  MARKED BY 111710.
* 05/27/11 E. MELNIK   MEOW CHANGES.  MARKED BY EM - 052711.
* 03/22/12 O. SOTTO    RATE EXPANSION. SC 032212.
* 10/04/13 E. MELNIK   CREF/REA REDESIGN CHANGES.
*                      MARKED BY EM - 100413.
* 03/23/17 E. MELNIK   RESTOWED FOR PIN EXPANSION.
* 04/12/19 E. MELNIK   POPULATED IRC CDE FOR ADSN605 CALL.
*                      MARKED BY EM - 041219.
************************************************************************
*

************************************************************ */

package tiaa.adas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Adsn420 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaAdsa401 pdaAdsa401;
    private PdaNazpda_M pdaNazpda_M;
    private PdaAiaa079 pdaAiaa079;
    private PdaAdsa605 pdaAdsa605;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Mat_Ratio;
    private DbsField pnd_Total_Tiaa_Grd_Amt;
    private DbsField pnd_Total_Tiaa_Std_Amt;
    private DbsField pnd_Tiaa_Amt;
    private DbsField pnd_Temp_Result;
    private DbsField pnd_Stts_Cd;
    private DbsField pnd_Total_Tiaa_Amt;
    private DbsField pnd_Difference;
    private DbsField pnd_Dte;

    private DbsGroup pnd_Dte__R_Field_1;
    private DbsField pnd_Dte_Pnd_Dte_N;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_L;
    private DbsField pnd_X;
    private DbsField pnd_Y;
    private DbsField pnd_Z;
    private DbsField pnd_Count;
    private DbsField pnd_Indx;
    private DbsField pnd_Tiaa_Stbl_Tckr;
    private DbsField pnd_Tiaa_Stbl_Ic654;
    private DbsField pnd_Sub_Plan_Prefix;
    private DbsField pnd_Fund_Sttlmnt_Amt_Lmt;
    private DbsField pnd_Max_Rate;
    private DbsField pls_Debug_On;
    private DbsField pls_Ticker;
    private DbsField pls_Invstmnt_Grpng_Id;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaNazpda_M = new PdaNazpda_M(localVariables);
        pdaAiaa079 = new PdaAiaa079(localVariables);
        pdaAdsa605 = new PdaAdsa605(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaAdsa401 = new PdaAdsa401(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_Mat_Ratio = localVariables.newFieldInRecord("pnd_Mat_Ratio", "#MAT-RATIO", FieldType.PACKED_DECIMAL, 9, 7);
        pnd_Total_Tiaa_Grd_Amt = localVariables.newFieldInRecord("pnd_Total_Tiaa_Grd_Amt", "#TOTAL-TIAA-GRD-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Total_Tiaa_Std_Amt = localVariables.newFieldInRecord("pnd_Total_Tiaa_Std_Amt", "#TOTAL-TIAA-STD-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Tiaa_Amt = localVariables.newFieldInRecord("pnd_Tiaa_Amt", "#TIAA-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Temp_Result = localVariables.newFieldInRecord("pnd_Temp_Result", "#TEMP-RESULT", FieldType.PACKED_DECIMAL, 5, 4);
        pnd_Stts_Cd = localVariables.newFieldInRecord("pnd_Stts_Cd", "#STTS-CD", FieldType.STRING, 3);
        pnd_Total_Tiaa_Amt = localVariables.newFieldInRecord("pnd_Total_Tiaa_Amt", "#TOTAL-TIAA-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Difference = localVariables.newFieldInRecord("pnd_Difference", "#DIFFERENCE", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Dte = localVariables.newFieldInRecord("pnd_Dte", "#DTE", FieldType.STRING, 8);

        pnd_Dte__R_Field_1 = localVariables.newGroupInRecord("pnd_Dte__R_Field_1", "REDEFINE", pnd_Dte);
        pnd_Dte_Pnd_Dte_N = pnd_Dte__R_Field_1.newFieldInGroup("pnd_Dte_Pnd_Dte_N", "#DTE-N", FieldType.NUMERIC, 8);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 4);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 4);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 4);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 4);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.INTEGER, 4);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.INTEGER, 4);
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.INTEGER, 4);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.NUMERIC, 2);
        pnd_Indx = localVariables.newFieldInRecord("pnd_Indx", "#INDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Tiaa_Stbl_Tckr = localVariables.newFieldArrayInRecord("pnd_Tiaa_Stbl_Tckr", "#TIAA-STBL-TCKR", FieldType.STRING, 10, new DbsArrayController(1, 
            3));
        pnd_Tiaa_Stbl_Ic654 = localVariables.newFieldArrayInRecord("pnd_Tiaa_Stbl_Ic654", "#TIAA-STBL-IC654", FieldType.STRING, 10, new DbsArrayController(1, 
            3));
        pnd_Sub_Plan_Prefix = localVariables.newFieldInRecord("pnd_Sub_Plan_Prefix", "#SUB-PLAN-PREFIX", FieldType.STRING, 3);
        pnd_Fund_Sttlmnt_Amt_Lmt = localVariables.newFieldInRecord("pnd_Fund_Sttlmnt_Amt_Lmt", "#FUND-STTLMNT-AMT-LMT", FieldType.NUMERIC, 9, 2);
        pnd_Max_Rate = localVariables.newFieldInRecord("pnd_Max_Rate", "#MAX-RATE", FieldType.PACKED_DECIMAL, 3);
        pls_Debug_On = WsIndependent.getInstance().newFieldInRecord("pls_Debug_On", "+DEBUG-ON", FieldType.BOOLEAN, 1);
        pls_Ticker = WsIndependent.getInstance().newFieldArrayInRecord("pls_Ticker", "+TICKER", FieldType.STRING, 10, new DbsArrayController(1, 100));
        pls_Invstmnt_Grpng_Id = WsIndependent.getInstance().newFieldArrayInRecord("pls_Invstmnt_Grpng_Id", "+INVSTMNT-GRPNG-ID", FieldType.STRING, 4, 
            new DbsArrayController(1, 100));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Tiaa_Stbl_Tckr.getValue(1).setInitialValue("TIAA#");
        pnd_Tiaa_Stbl_Tckr.getValue(2).setInitialValue("TSAF#");
        pnd_Tiaa_Stbl_Tckr.getValue(3).setInitialValue("TSVX#");
        pnd_Tiaa_Stbl_Ic654.getValue(1).setInitialValue("TIA0");
        pnd_Tiaa_Stbl_Ic654.getValue(2).setInitialValue("TSA0");
        pnd_Tiaa_Stbl_Ic654.getValue(3).setInitialValue("TSV0");
        pnd_Fund_Sttlmnt_Amt_Lmt.setInitialValue(9999999);
        pnd_Max_Rate.setInitialValue(250);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Adsn420() throws Exception
    {
        super("Adsn420");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        setupReports();
        //* ***************************************************
        //* * 08312005 MN START
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
            getReports().write(0, "PROCESSING PROGRAM:",Global.getPROGRAM());                                                                                             //Natural: WRITE 'PROCESSING PROGRAM:' *PROGRAM
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  FORMAT PS=24 SG=OFF
        //* * 08312005 MN END                                                                                                                                             //Natural: FORMAT LS = 120 PS = 100
        //*  EM - 111710 START
                                                                                                                                                                          //Natural: PERFORM CALC-CNTRCT-MAT-SETTL-AMTS
        sub_Calc_Cntrct_Mat_Settl_Amts();
        if (condition(Global.isEscape())) {return;}
        //*  EM - 052711 START
        if (condition(pnd_Stts_Cd.notEquals(" ")))                                                                                                                        //Natural: IF #STTS-CD NE ' '
        {
            if (condition(true)) return;                                                                                                                                  //Natural: ESCAPE ROUTINE
            //*  EM - 052711 END
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO #FUND-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaAdsa401.getPnd_Adsa401_Pnd_Fund_Cnt())); pnd_I.nadd(1))
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Tkr_Symbl().getValue(pnd_I).equals(" ")))                                                                         //Natural: IF ADC-TKR-SYMBL ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  EM - 100413 - CHANGE TO USE IC654
            pnd_Indx.reset();                                                                                                                                             //Natural: RESET #INDX
            DbsUtil.examine(new ExamineSource(pls_Ticker.getValue("*")), new ExamineSearch(pdaAdsa401.getPnd_Adsa401_Adc_Tkr_Symbl().getValue(pnd_I)),                    //Natural: EXAMINE +TICKER ( * ) FOR ADC-TKR-SYMBL ( #I ) GIVING INDEX #INDX
                new ExamineGivingIndex(pnd_Indx));
            if (condition(pnd_Indx.equals(getZero())))                                                                                                                    //Natural: IF #INDX = 0
            {
                pnd_Stts_Cd.setValue("L78");                                                                                                                              //Natural: MOVE 'L78' TO #STTS-CD
                                                                                                                                                                          //Natural: PERFORM SET-STTS-CDS
                sub_Set_Stts_Cds();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) return;                                                                                                                              //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pls_Invstmnt_Grpng_Id.getValue(pnd_Indx).equals(pnd_Tiaa_Stbl_Ic654.getValue("*"))))                                                            //Natural: IF +INVSTMNT-GRPNG-ID ( #INDX ) = #TIAA-STBL-IC654 ( * )
            {
                //*  IF ADC-TKR-SYMBL (#I) = #TIAA-STBL-TCKR (*)
                                                                                                                                                                          //Natural: PERFORM CALC-TIAA-RATE-AMTS
                sub_Calc_Tiaa_Rate_Amts();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  EM - 111710 END
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALC-CNTRCT-MAT-SETTL-AMTS
        //* *******************************************
        //* ********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALC-PROD-TOT-MAT-SETTL-AMT
        //* ********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALC-PROD-TOT-GRD-SETTL-AMT
        //* ***********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALC-PROD-TOT-MNTHLY-SETTL-AMT
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALC-TIAA-RATE-AMTS
        //* * 08312005  MN START
        //* * 08312005 MN START
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-TIAA-RATE-MODULE
        //*  EM - 100413
        //*  EM - 041219
        //* *#AIAA079-RATE-IN (1:99) := ADC-DTL-TIAA-RATE-CDE(1:99)
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-STTS-CDS
    }
    private void sub_Calc_Cntrct_Mat_Settl_Amts() throws Exception                                                                                                        //Natural: CALC-CNTRCT-MAT-SETTL-AMTS
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #J = 1 TO #FUND-CNT
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pdaAdsa401.getPnd_Adsa401_Pnd_Fund_Cnt())); pnd_J.nadd(1))
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Tkr_Symbl().getValue(pnd_J).equals(" ")))                                                                         //Natural: IF ADC-TKR-SYMBL ( #J ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  EM - 100413 - CHANGE TO USE IC654
            pnd_Indx.reset();                                                                                                                                             //Natural: RESET #INDX
            DbsUtil.examine(new ExamineSource(pls_Ticker.getValue("*")), new ExamineSearch(pdaAdsa401.getPnd_Adsa401_Adc_Tkr_Symbl().getValue(pnd_J)),                    //Natural: EXAMINE +TICKER ( * ) FOR ADC-TKR-SYMBL ( #J ) GIVING INDEX #INDX
                new ExamineGivingIndex(pnd_Indx));
            if (condition(pnd_Indx.equals(getZero())))                                                                                                                    //Natural: IF #INDX = 0
            {
                pnd_Stts_Cd.setValue("L78");                                                                                                                              //Natural: MOVE 'L78' TO #STTS-CD
                                                                                                                                                                          //Natural: PERFORM SET-STTS-CDS
                sub_Set_Stts_Cds();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pls_Invstmnt_Grpng_Id.getValue(pnd_Indx).equals(pnd_Tiaa_Stbl_Ic654.getValue(1)) || pls_Invstmnt_Grpng_Id.getValue(pnd_Indx).equals(pnd_Tiaa_Stbl_Ic654.getValue(2))  //Natural: IF +INVSTMNT-GRPNG-ID ( #INDX ) = #TIAA-STBL-IC654 ( * ) OR = 'REA1' OR = 'REX1' OR = 'WA50'
                || pls_Invstmnt_Grpng_Id.getValue(pnd_Indx).equals(pnd_Tiaa_Stbl_Ic654.getValue(3)) || pls_Invstmnt_Grpng_Id.getValue(pnd_Indx).equals("REA1") 
                || pls_Invstmnt_Grpng_Id.getValue(pnd_Indx).equals("REX1") || pls_Invstmnt_Grpng_Id.getValue(pnd_Indx).equals("WA50")))
            {
                //*  IF ADC-TKR-SYMBL (#J) = #TIAA-STBL-TCKR (*)    /* EM - 111710
                //*      OR = 'TREA#' OR = 'WA51#'                  /* EM - 011409
                pdaAdsa401.getPnd_Adsa401_Pnd_Total_Tiaa_Ivc_Amount().nadd(pdaAdsa401.getPnd_Adsa401_Adc_Ivc_Amt().getValue(pnd_J));                                      //Natural: ADD ADC-IVC-AMT ( #J ) TO #TOTAL-TIAA-IVC-AMOUNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAdsa401.getPnd_Adsa401_Pnd_Total_Cref_Ivc_Amount().nadd(pdaAdsa401.getPnd_Adsa401_Adc_Ivc_Amt().getValue(pnd_J));                                      //Natural: ADD ADC-IVC-AMT ( #J ) TO #TOTAL-CREF-IVC-AMOUNT
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALC-PROD-TOT-MAT-SETTL-AMT
            sub_Calc_Prod_Tot_Mat_Settl_Amt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  /* EM 052711 START
        if (condition((pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue("*").greater(pnd_Fund_Sttlmnt_Amt_Lmt)) || (pdaAdsa401.getPnd_Adsa401_Adc_Stndrd_Annl_Actl_Amt().getValue("*").greater(pnd_Fund_Sttlmnt_Amt_Lmt)))) //Natural: IF ( ADC-GRD-MNTHLY-ACTL-AMT ( * ) GT #FUND-STTLMNT-AMT-LMT ) OR ( ADC-STNDRD-ANNL-ACTL-AMT ( * ) GT #FUND-STTLMNT-AMT-LMT )
        {
            pnd_Stts_Cd.setValue("L73");                                                                                                                                  //Natural: MOVE 'L73' TO #STTS-CD
                                                                                                                                                                          //Natural: PERFORM SET-STTS-CDS
            sub_Set_Stts_Cds();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  /* EM 052711 END
    }
    private void sub_Calc_Prod_Tot_Mat_Settl_Amt() throws Exception                                                                                                       //Natural: CALC-PROD-TOT-MAT-SETTL-AMT
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************
        //*  EM - 100413 - CHANGE TO USE IC654
        if (condition(pls_Invstmnt_Grpng_Id.getValue(pnd_Indx).equals(pnd_Tiaa_Stbl_Ic654.getValue("*"))))                                                                //Natural: IF +INVSTMNT-GRPNG-ID ( #INDX ) = #TIAA-STBL-IC654 ( * )
        {
            //*  IF ADC-TKR-SYMBL (#J) = #TIAA-STBL-TCKR (*)     /* EM - 111710
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Typ().getValue(pnd_J).notEquals(" ")))                                                                 //Natural: IF ADC-GRD-MNTHLY-TYP ( #J ) NE ' '
            {
                                                                                                                                                                          //Natural: PERFORM CALC-PROD-TOT-GRD-SETTL-AMT
                sub_Calc_Prod_Tot_Grd_Settl_Amt();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Typ().getValue(pnd_J).notEquals(" ")))                                                                 //Natural: IF ADC-GRD-MNTHLY-TYP ( #J ) NE ' '
            {
                                                                                                                                                                          //Natural: PERFORM CALC-PROD-TOT-MNTHLY-SETTL-AMT
                sub_Calc_Prod_Tot_Mnthly_Settl_Amt();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaAdsa401.getPnd_Adsa401_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_J).compute(new ComputeParameters(true, pdaAdsa401.getPnd_Adsa401_Adc_Stndrd_Annl_Actl_Amt().getValue(pnd_J)),  //Natural: COMPUTE ROUNDED ADC-STNDRD-ANNL-ACTL-AMT ( #J ) = ADC-ACCT-ACTL-AMT ( #J ) - ADC-GRD-MNTHLY-ACTL-AMT ( #J )
            pdaAdsa401.getPnd_Adsa401_Adc_Acct_Actl_Amt().getValue(pnd_J).subtract(pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_J)));
    }
    private void sub_Calc_Prod_Tot_Grd_Settl_Amt() throws Exception                                                                                                       //Natural: CALC-PROD-TOT-GRD-SETTL-AMT
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************
        if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Typ().getValue(pnd_J).equals("P") || pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Typ().getValue(pnd_J).equals("%"))) //Natural: IF ADC-GRD-MNTHLY-TYP ( #J ) = 'P' OR = '%'
        {
            pnd_Temp_Result.compute(new ComputeParameters(true, pnd_Temp_Result), pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Qty().getValue(pnd_J).divide(100));            //Natural: COMPUTE ROUNDED #TEMP-RESULT = ADC-GRD-MNTHLY-QTY ( #J ) / 100
            pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_J).compute(new ComputeParameters(true, pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_J)),  //Natural: COMPUTE ROUNDED ADC-GRD-MNTHLY-ACTL-AMT ( #J ) = #TEMP-RESULT * ADC-ACCT-ACTL-AMT ( #J )
                pnd_Temp_Result.multiply(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Actl_Amt().getValue(pnd_J)));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Typ().getValue(pnd_J).equals("D") || pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Typ().getValue(pnd_J).equals("$"))) //Natural: IF ADC-GRD-MNTHLY-TYP ( #J ) = 'D' OR = '$'
            {
                pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_J).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Qty().getValue(pnd_J));             //Natural: MOVE ADC-GRD-MNTHLY-QTY ( #J ) TO ADC-GRD-MNTHLY-ACTL-AMT ( #J )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_J).greater(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Actl_Amt().getValue(pnd_J))))        //Natural: IF ADC-GRD-MNTHLY-ACTL-AMT ( #J ) GT ADC-ACCT-ACTL-AMT ( #J )
        {
            pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_J).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Actl_Amt().getValue(pnd_J));                  //Natural: MOVE ADC-ACCT-ACTL-AMT ( #J ) TO ADC-GRD-MNTHLY-ACTL-AMT ( #J )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Calc_Prod_Tot_Mnthly_Settl_Amt() throws Exception                                                                                                    //Natural: CALC-PROD-TOT-MNTHLY-SETTL-AMT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************
        if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Typ().getValue(pnd_J).equals("P") || pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Typ().getValue(pnd_J).equals("%"))) //Natural: IF ADC-GRD-MNTHLY-TYP ( #J ) = 'P' OR = '%'
        {
            pnd_Temp_Result.compute(new ComputeParameters(true, pnd_Temp_Result), pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Qty().getValue(pnd_J).divide(100));            //Natural: COMPUTE ROUNDED #TEMP-RESULT = ADC-GRD-MNTHLY-QTY ( #J ) / 100
            pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_J).compute(new ComputeParameters(true, pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_J)),  //Natural: COMPUTE ROUNDED ADC-GRD-MNTHLY-ACTL-AMT ( #J ) = #TEMP-RESULT * ADC-ACCT-ACTL-AMT ( #J )
                pnd_Temp_Result.multiply(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Actl_Amt().getValue(pnd_J)));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Typ().getValue(pnd_J).equals("D") || pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Typ().getValue(pnd_J).equals("$"))) //Natural: IF ADC-GRD-MNTHLY-TYP ( #J ) = 'D' OR = '$'
            {
                pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_J).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Qty().getValue(pnd_J));             //Natural: MOVE ADC-GRD-MNTHLY-QTY ( #J ) TO ADC-GRD-MNTHLY-ACTL-AMT ( #J )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_J).greater(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Actl_Amt().getValue(pnd_J))))        //Natural: IF ADC-GRD-MNTHLY-ACTL-AMT ( #J ) GT ADC-ACCT-ACTL-AMT ( #J )
        {
            pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_J).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Actl_Amt().getValue(pnd_J));                  //Natural: MOVE ADC-ACCT-ACTL-AMT ( #J ) TO ADC-GRD-MNTHLY-ACTL-AMT ( #J )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Calc_Tiaa_Rate_Amts() throws Exception                                                                                                               //Natural: CALC-TIAA-RATE-AMTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        pnd_Mat_Ratio.compute(new ComputeParameters(true, pnd_Mat_Ratio), pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_I).divide(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Actl_Amt().getValue(pnd_I))); //Natural: COMPUTE ROUNDED #MAT-RATIO = ADC-GRD-MNTHLY-ACTL-AMT ( #I ) / ADC-ACCT-ACTL-AMT ( #I )
        //*  GRADED RQST
        if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_I).notEquals(getZero())))                                                          //Natural: IF ADC-GRD-MNTHLY-ACTL-AMT ( #I ) NE 0
        {
                                                                                                                                                                          //Natural: PERFORM CALL-TIAA-RATE-MODULE
            sub_Call_Tiaa_Rate_Module();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Stts_Cd.notEquals(" ")))                                                                                                                    //Natural: IF #STTS-CD NE ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR03:                                                                                                                                                            //Natural: FOR #J = 1 TO #TIAA-RATE-CNT
        for (pnd_J.setValue(1); condition(pnd_J.lessOrEqual(pdaAdsa401.getPnd_Adsa401_Pnd_Tiaa_Rate_Cnt())); pnd_J.nadd(1))
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_J).equals(" ") || pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Rate_Cde().getValue(pnd_J).equals("00"))) //Natural: IF ADC-DTL-TIAA-RATE-CDE ( #J ) = ' ' OR = '00'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_S_G_Code().getValue(pnd_J).equals("S")))                                                          //Natural: IF #AIAA079-S-G-CODE ( #J ) = 'S'
            {
                pnd_Total_Tiaa_Std_Amt.compute(new ComputeParameters(true, pnd_Total_Tiaa_Std_Amt), pnd_Total_Tiaa_Std_Amt.add((pnd_Mat_Ratio.multiply(pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_J))))); //Natural: COMPUTE ROUNDED #TOTAL-TIAA-STD-AMT = #TOTAL-TIAA-STD-AMT + ( #MAT-RATIO * ADC-DTL-TIAA-ACTL-AMT ( #J ) )
                pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_J).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_J));     //Natural: ASSIGN ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #J ) := ADC-DTL-TIAA-ACTL-AMT ( #J )
                pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Std_Amt().getValue(pnd_J).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Amt().getValue(pnd_J)); //Natural: ASSIGN ADC-TPA-GUARANT-COMMUT-STD-AMT ( #J ) := ADC-TPA-GUARANT-COMMUT-AMT ( #J )
                //* * 08312005  MN END
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_J).compute(new ComputeParameters(true, pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_J)),  //Natural: COMPUTE ROUNDED ADC-DTL-TIAA-GRD-ACTL-AMT ( #J ) = #MAT-RATIO * ADC-DTL-TIAA-ACTL-AMT ( #J )
                    pnd_Mat_Ratio.multiply(pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_J)));
                pnd_Total_Tiaa_Grd_Amt.nadd(pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_J));                                                       //Natural: ADD ADC-DTL-TIAA-GRD-ACTL-AMT ( #J ) TO #TOTAL-TIAA-GRD-AMT
                pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_J).compute(new ComputeParameters(true, pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_J)),  //Natural: COMPUTE ROUNDED ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #J ) = ADC-DTL-TIAA-ACTL-AMT ( #J ) - ADC-DTL-TIAA-GRD-ACTL-AMT ( #J )
                    pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_J).subtract(pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_J)));
                //* * 08312005  MN START
                pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Grd_Amt().getValue(pnd_J).compute(new ComputeParameters(true, pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Grd_Amt().getValue(pnd_J)),  //Natural: COMPUTE ROUNDED ADC-TPA-GUARANT-COMMUT-GRD-AMT ( #J ) = #MAT-RATIO * ADC-TPA-GUARANT-COMMUT-AMT ( #J )
                    pnd_Mat_Ratio.multiply(pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Amt().getValue(pnd_J)));
                pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Std_Amt().getValue(pnd_J).compute(new ComputeParameters(true, pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Std_Amt().getValue(pnd_J)),  //Natural: COMPUTE ROUNDED ADC-TPA-GUARANT-COMMUT-STD-AMT ( #J ) = ADC-TPA-GUARANT-COMMUT-AMT ( #J ) - ADC-TPA-GUARANT-COMMUT-GRD-AMT ( #J )
                    pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Amt().getValue(pnd_J).subtract(pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Grd_Amt().getValue(pnd_J)));
                //* * 08312005  MN END
            }                                                                                                                                                             //Natural: END-IF
            pnd_K.setValue(pnd_J);                                                                                                                                        //Natural: ASSIGN #K := #J
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* * 08312005 MN START
        if (condition(pls_Debug_On.getBoolean()))                                                                                                                         //Natural: IF +DEBUG-ON
        {
            getReports().write(0, "*",new RepeatItem(100));                                                                                                               //Natural: WRITE '*' ( 100 )
            if (Global.isEscape()) return;
            getReports().write(0, Global.getPROGRAM());                                                                                                                   //Natural: WRITE *PROGRAM
            if (Global.isEscape()) return;
            getReports().write(0, "Before ADC-DTL-TIAA-STNDRD-ACTL-AMT rounding:");                                                                                       //Natural: WRITE 'Before ADC-DTL-TIAA-STNDRD-ACTL-AMT rounding:'
            if (Global.isEscape()) return;
            getReports().write(0, new TabSetting(5),"=",pnd_K,new TabSetting(50),"=",pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_K),NEWLINE,new //Natural: WRITE 5T '=' #K 50T '=' ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #K ) /5T '=' ADC-DTL-TIAA-ACTL-AMT ( #K ) 50T '=' ADC-DTL-TIAA-GRD-ACTL-AMT ( #K )
                TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_K),new TabSetting(50),"=",pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_K));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* * 08312005 MN END
        if (condition(pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_S_G_Code().getValue(pnd_K).equals("S")))                                                              //Natural: IF #AIAA079-S-G-CODE ( #K ) = 'S'
        {
            pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_K).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_K));         //Natural: ASSIGN ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #K ) = ADC-DTL-TIAA-ACTL-AMT ( #K )
            pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Std_Amt().getValue(pnd_K).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Tpa_Guarant_Commut_Amt().getValue(pnd_K));  //Natural: ASSIGN ADC-TPA-GUARANT-COMMUT-STD-AMT ( #K ) := ADC-TPA-GUARANT-COMMUT-AMT ( #K )
            //* * 08312005 MN END
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tiaa_Amt.compute(new ComputeParameters(true, pnd_Tiaa_Amt), pnd_Total_Tiaa_Grd_Amt.add(pnd_Total_Tiaa_Std_Amt));                                          //Natural: COMPUTE ROUNDED #TIAA-AMT = #TOTAL-TIAA-GRD-AMT + #TOTAL-TIAA-STD-AMT
            if (condition(pnd_Tiaa_Amt.notEquals(pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_I))))                                                   //Natural: IF #TIAA-AMT NE ADC-GRD-MNTHLY-ACTL-AMT ( #I )
            {
                pnd_Difference.compute(new ComputeParameters(true, pnd_Difference), pdaAdsa401.getPnd_Adsa401_Adc_Grd_Mnthly_Actl_Amt().getValue(pnd_I).subtract(pnd_Tiaa_Amt)); //Natural: COMPUTE ROUNDED #DIFFERENCE = ADC-GRD-MNTHLY-ACTL-AMT ( #I ) - #TIAA-AMT
                pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_K).nadd(pnd_Difference);                                                               //Natural: ADD #DIFFERENCE TO ADC-DTL-TIAA-GRD-ACTL-AMT ( #K )
                pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_K).compute(new ComputeParameters(true, pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_K)),  //Natural: COMPUTE ROUNDED ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #K ) = ADC-DTL-TIAA-ACTL-AMT ( #K ) - ADC-DTL-TIAA-GRD-ACTL-AMT ( #K )
                    pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_K).subtract(pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_K)));
                //* * 08312005 MN START
                if (condition(pls_Debug_On.getBoolean()))                                                                                                                 //Natural: IF +DEBUG-ON
                {
                    getReports().write(0, "*",new RepeatItem(100));                                                                                                       //Natural: WRITE '*' ( 100 )
                    if (Global.isEscape()) return;
                    getReports().write(0, Global.getPROGRAM());                                                                                                           //Natural: WRITE *PROGRAM
                    if (Global.isEscape()) return;
                    getReports().write(0, "After ADC-DTL-TIAA-STNDRD-ACTL-AMT rounding:");                                                                                //Natural: WRITE 'After ADC-DTL-TIAA-STNDRD-ACTL-AMT rounding:'
                    if (Global.isEscape()) return;
                    getReports().write(0, new TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Stndrd_Actl_Amt().getValue(pnd_K),new TabSetting(50),"=",pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Actl_Amt().getValue(pnd_K),NEWLINE,new  //Natural: WRITE 5T '=' ADC-DTL-TIAA-STNDRD-ACTL-AMT ( #K ) 50T '=' ADC-DTL-TIAA-ACTL-AMT ( #K ) /5T '=' ADC-DTL-TIAA-GRD-ACTL-AMT ( #K )
                        TabSetting(5),"=",pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Grd_Actl_Amt().getValue(pnd_K));
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //* * 08312005 MN END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Call_Tiaa_Rate_Module() throws Exception                                                                                                             //Natural: CALL-TIAA-RATE-MODULE
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        //* *
        //*  NEW ISSUE
        //*  DEA
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Annt_Typ_Cde().equals("M")))                                                                                          //Natural: IF ADP-ANNT-TYP-CDE = 'M'
        {
            pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Call_Type().setValue("1");                                                                                      //Natural: ASSIGN #AIAA079-CALL-TYPE := '1'
            //*  RESETTLEMENT
            //*  DEA
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Call_Type().setValue("2");                                                                                      //Natural: ASSIGN #AIAA079-CALL-TYPE := '2'
            //*  DEA
        }                                                                                                                                                                 //Natural: END-IF
        pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Contract().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Ia_Tiaa_Nbr());                                                   //Natural: ASSIGN #AIAA079-CONTRACT := ADP-IA-TIAA-NBR
        pnd_Dte.setValueEdited(pdaAdsa401.getPnd_Adsa401_Adp_Annty_Strt_Dte(),new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED ADP-ANNTY-STRT-DTE ( EM = YYYYMMDD ) TO #DTE
        pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Effective_Date().setValue(pnd_Dte_Pnd_Dte_N);                                                                       //Natural: ASSIGN #AIAA079-EFFECTIVE-DATE := #DTE-N
        //*                                                    /* DEA
        //*  CM - SET PARAMETERS FOR CALL TO ADSA605
        pdaAdsa605.getPnd_Adsa605().reset();                                                                                                                              //Natural: RESET #ADSA605
        pdaAdsa605.getPnd_Adsa605_Pnd_Plan_Nbr().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Plan_Nbr());                                                                      //Natural: ASSIGN #PLAN-NBR := #ADSA401.ADP-PLAN-NBR
        pdaAdsa605.getPnd_Adsa605_Pnd_Sub_Plan_Nbr().setValue(pdaAdsa401.getPnd_Adsa401_Adc_Sub_Plan_Nbr());                                                              //Natural: ASSIGN #SUB-PLAN-NBR := #ADSA401.ADC-SUB-PLAN-NBR
        pdaAdsa605.getPnd_Adsa605_Pnd_Acct_Code().getValue("*").setValue(pdaAdsa401.getPnd_Adsa401_Adc_Acct_Cde().getValue("*"));                                         //Natural: ASSIGN #ACCT-CODE ( * ) := #ADSA401.ADC-ACCT-CDE ( * )
        pdaAdsa605.getPnd_Adsa605_Pnd_Contract_Nbrs().getValue("*").setValue(pdaAdsa401.getPnd_Adsa401_Adp_Cntrcts_In_Rqst().getValue("*"));                              //Natural: ASSIGN #CONTRACT-NBRS ( * ) := #ADSA401.ADP-CNTRCTS-IN-RQST ( * )
        pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Lob_Ind().getValue(1).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Orig_Lob_Ind());                                                //Natural: ASSIGN #ORIGIN-LOB-IND ( 1 ) := #ADSA401.ADC-ORIG-LOB-IND
        pdaAdsa605.getPnd_Adsa605_Pnd_T395_Fund_Id().getValue("*").setValue(pdaAdsa401.getPnd_Adsa401_Adc_T395_Fund_Id().getValue("*"));                                  //Natural: ASSIGN #T395-FUND-ID ( * ) := #ADSA401.ADC-T395-FUND-ID ( * )
        pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Plan_Type().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Roth_Plan_Type());                                                          //Natural: ASSIGN #ROTH-PLAN-TYPE := ADP-ROTH-PLAN-TYPE
        pdaAdsa605.getPnd_Adsa605_Pnd_Invstmnt_Grpng_Id().getValue("*").setValue(pdaAdsa401.getPnd_Adsa401_Adc_Invstmnt_Grpng_Id().getValue("*"));                        //Natural: ASSIGN #INVSTMNT-GRPNG-ID ( * ) := #ADSA401.ADC-INVSTMNT-GRPNG-ID ( * )
        pdaAdsa605.getPnd_Adsa605_Pnd_Adp_Irc_Cde().setValue(pdaAdsa401.getPnd_Adsa401_Adp_Irc_Cde());                                                                    //Natural: ASSIGN #ADP-IRC-CDE := #ADSA401.ADP-IRC-CDE
        //*  DEA
        //*  CM
        if (condition(pdaAdsa401.getPnd_Adsa401_Adp_Roth_Rqst_Ind().equals("Y")))                                                                                         //Natural: IF ADP-ROTH-RQST-IND = 'Y'
        {
            pdaAdsa605.getPnd_Adsa605_Pnd_Roth_Tiaa_Orgn_Cde().setValue(true);                                                                                            //Natural: ASSIGN #ROTH-TIAA-ORGN-CDE := TRUE
            //*  CM
            //*  CM
            DbsUtil.callnat(Adsn605.class , getCurrentProcessState(), pdaAdsa605.getPnd_Adsa605());                                                                       //Natural: CALLNAT 'ADSN605' #ADSA605
            if (condition(Global.isEscape())) return;
            pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Origin().setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code());                                                 //Natural: ASSIGN #AIAA079-ORIGIN := #ADSA605.#ORIGIN-CODE
            //*  CM
            getReports().write(0, "=",pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Origin());                                                                            //Natural: WRITE '=' #AIAA079-ORIGIN
            if (Global.isEscape()) return;
            //*  DEA
            //*  CM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAdsa605.getPnd_Adsa605_Pnd_Tiaa_Orgn_Cde().setValue(true);                                                                                                 //Natural: ASSIGN #TIAA-ORGN-CDE := TRUE
            //*  CM
            //*  CM
            DbsUtil.callnat(Adsn605.class , getCurrentProcessState(), pdaAdsa605.getPnd_Adsa605());                                                                       //Natural: CALLNAT 'ADSN605' #ADSA605
            if (condition(Global.isEscape())) return;
            pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Origin().setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code());                                                 //Natural: ASSIGN #AIAA079-ORIGIN := #ADSA605.#ORIGIN-CODE
            //*  CM
            getReports().write(0, "=",pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Origin());                                                                            //Natural: WRITE '=' #AIAA079-ORIGIN
            if (Global.isEscape()) return;
            //*  DEA
            //*  CM
        }                                                                                                                                                                 //Natural: END-IF
        pdaAdsa401.getPnd_Adsa401_Pnd_Ia_Origin_Code().setValue(pdaAdsa605.getPnd_Adsa605_Pnd_Origin_Code());                                                             //Natural: ASSIGN #ADSA401.#IA-ORIGIN-CODE := #ADSA605.#ORIGIN-CODE
        //*  CM
        //*  032212
        getReports().write(0, "=",pdaAdsa401.getPnd_Adsa401_Pnd_Ia_Origin_Code());                                                                                        //Natural: WRITE '=' #ADSA401.#IA-ORIGIN-CODE
        if (Global.isEscape()) return;
        pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Rate_In().getValue(1,":",pnd_Max_Rate).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Dtl_Tiaa_Rate_Cde().getValue(1,       //Natural: ASSIGN #AIAA079-RATE-IN ( 1:#MAX-RATE ) := ADC-DTL-TIAA-RATE-CDE ( 1:#MAX-RATE )
            ":",pnd_Max_Rate));
        pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Gic_Code_In().getValue(1,":",pnd_Max_Rate).setValue(pdaAdsa401.getPnd_Adsa401_Adc_Contract_Id().getValue(1,         //Natural: ASSIGN #AIAA079-GIC-CODE-IN ( 1:#MAX-RATE ) := ADC-CONTRACT-ID ( 1:#MAX-RATE )
            ":",pnd_Max_Rate));
        //*  GRADED-LINKAGE
        DbsUtil.callnat(Aian079.class , getCurrentProcessState(), pdaAiaa079.getPnd_Aiaa079_Linkage());                                                                   //Natural: CALLNAT 'AIAN079' #AIAA079-LINKAGE
        if (condition(Global.isEscape())) return;
        //* *IF #AIAA079-RETURN-CODE NE 0                   /* 032212
        //*  032212
        if (condition(pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Return_Code_Nbr().notEquals(getZero())))                                                              //Natural: IF #AIAA079-RETURN-CODE-NBR NE 0
        {
            pnd_Stts_Cd.setValue("L10");                                                                                                                                  //Natural: MOVE 'L10' TO #STTS-CD
                                                                                                                                                                          //Natural: PERFORM SET-STTS-CDS
            sub_Set_Stts_Cds();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Set_Stts_Cds() throws Exception                                                                                                                      //Natural: SET-STTS-CDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        if (condition(pnd_Stts_Cd.equals("L10")))                                                                                                                         //Natural: IF #STTS-CD = 'L10'
        {
            pdaAdsa401.getPnd_Adsa401_Adc_Err_Cde().getValue(1).setValue(pdaAiaa079.getPnd_Aiaa079_Linkage_Pnd_Aiaa079_Return_Code_Nbr());                                //Natural: MOVE #AIAA079-RETURN-CODE-NBR TO ADC-ERR-CDE ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaAdsa401.getPnd_Adsa401_Adc_Err_Cde().getValue(1).setValue(pnd_Stts_Cd);                                                                                    //Natural: MOVE #STTS-CD TO ADC-ERR-CDE ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        pdaAdsa401.getPnd_Adsa401_Pnd_Error_Status().setValue(pnd_Stts_Cd);                                                                                               //Natural: MOVE #STTS-CD TO #ERROR-STATUS
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO #FUND-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaAdsa401.getPnd_Adsa401_Pnd_Fund_Cnt())); pnd_I.nadd(1))
        {
            if (condition(pdaAdsa401.getPnd_Adsa401_Adc_Tkr_Symbl().getValue(pnd_I).equals(" ")))                                                                         //Natural: IF ADC-TKR-SYMBL ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  MOVE #STTS-CD TO ADC-ACCT-STTS-CDE (#I)
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=120 PS=100");
    }
}
