/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:28:49 AM
**        * FROM NATURAL SUBPROGRAM : Mdmn190
************************************************************
**        * FILE NAME            : Mdmn190.java
**        * CLASS NAME           : Mdmn190
**        * INSTANCE NAME        : Mdmn190
************************************************************
************************************************************************
* SUBPROGRAM : MDMN190  -  12 BYTE PIN VERSION OF NASN190
* WRITTEN    : DON MEADE
* DATE       : JULY 19, 2017
*
* FUNCTION   : THIS SUBPROGRAM UPDATES CWF FROM THE NAME & ADDRESS
*              SYSTEMS. THE FIELDS IN THE NASG000 GLOBAL AREA IS USED
*              TO DECIDE ON THE TYPE OF UPDATE REQUIRED.
************************************************************************
*    DATE     MOD BY                    DESCRIPTION
************************************************************************

************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mdmn190 extends BLNatBase
{
    // Data Areas
    public DbsRecord parameters;
    private PdaMdma190 pdaMdma190;
    private PdaEfsa9120 pdaEfsa9120;
    private PdaEfsa902r pdaEfsa902r;
    private PdaErla1000 pdaErla1000;
    private PdaCwfa8010 pdaCwfa8010;
    private PdaCwfa6510 pdaCwfa6510;
    private PdaCdaobj pdaCdaobj;
    private PdaCwfpda_D pdaCwfpda_D;
    private PdaCwfpda_M pdaCwfpda_M;
    private PdaCwfpda_P pdaCwfpda_P;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I;
    private DbsField pnd_Cdms_In_Routing;
    private DbsField pnd_Route_To_Next_Unit;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaEfsa9120 = new PdaEfsa9120(localVariables);
        pdaEfsa902r = new PdaEfsa902r(localVariables);
        pdaErla1000 = new PdaErla1000(localVariables);
        pdaCwfa8010 = new PdaCwfa8010(localVariables);
        pdaCwfa6510 = new PdaCwfa6510(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);
        pdaCwfpda_M = new PdaCwfpda_M(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);

        // parameters
        parameters = new DbsRecord();
        pdaMdma190 = new PdaMdma190(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Cdms_In_Routing = localVariables.newFieldInRecord("pnd_Cdms_In_Routing", "#CDMS-IN-ROUTING", FieldType.STRING, 1);
        pnd_Route_To_Next_Unit = localVariables.newFieldInRecord("pnd_Route_To_Next_Unit", "#ROUTE-TO-NEXT-UNIT", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Mdmn190() throws Exception
    {
        super("Mdmn190");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        if (condition(pdaMdma190.getMdma190_Pnd_Gda_Slo_Exists().equals("Y")))                                                                                            //Natural: IF MDMA190.#GDA-SLO-EXISTS = 'Y'
        {
                                                                                                                                                                          //Natural: PERFORM UPDATE-CWF-RTN
            sub_Update_Cwf_Rtn();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM STORE-CWF-RTN
            sub_Store_Cwf_Rtn();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaMdma190.getMdma190_Pnd_Error_Message().notEquals(" ")))                                                                                          //Natural: IF MDMA190.#ERROR-MESSAGE NE ' '
        {
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            pdaErla1000.getErla1000_Err_Pgm_Name().setValue(Global.getPROGRAM());                                                                                         //Natural: ASSIGN ERLA1000.ERR-PGM-NAME := *PROGRAM
            pdaErla1000.getErla1000_Err_Pgm_Level().setValue(Global.getLEVEL());                                                                                          //Natural: ASSIGN ERLA1000.ERR-PGM-LEVEL := *LEVEL
            pdaErla1000.getErla1000_Err_Nbr().setValue(Global.getERROR_NR());                                                                                             //Natural: ASSIGN ERLA1000.ERR-NBR := *ERROR-NR
            pdaErla1000.getErla1000_Err_Line_Nbr().setValue(Global.getERROR_LINE());                                                                                      //Natural: ASSIGN ERLA1000.ERR-LINE-NBR := *ERROR-LINE
            pdaErla1000.getErla1000_Err_Status_Cde().setValue("U");                                                                                                       //Natural: ASSIGN ERLA1000.ERR-STATUS-CDE := 'U'
            pdaErla1000.getErla1000_Err_Type_Cde().setValue("N");                                                                                                         //Natural: ASSIGN ERLA1000.ERR-TYPE-CDE := 'N'
            //*  ERLA1000.ERR-NOTES      := MDMA190.#ERROR-MESSAGE
            pdaErla1000.getErla1000_Err_Notes().setValue(DbsUtil.compress("PIN", pdaMdma190.getMdma190_Pnd_Wpid_Pin(), pdaMdma190.getMdma190_Pnd_Error_Message()));       //Natural: COMPRESS 'PIN' MDMA190.#WPID-PIN MDMA190.#ERROR-MESSAGE INTO ERLA1000.ERR-NOTES
            DbsUtil.callnat(Erln1000.class , getCurrentProcessState(), pdaErla1000.getErla1000());                                                                        //Natural: CALLNAT 'ERLN1000' ERLA1000
            if (condition(Global.isEscape())) return;
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaMdma190.getMdma190_Pnd_Et_Ind().equals("Y")))                                                                                                //Natural: IF MDMA190.#ET-IND = 'Y'
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CWF-RTN
        //* ***********************************************************************
        //*  PINE: MDMA190 PIN N7, CWFA8010 PIN N12
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-CWF-RTN
        //* ***********************************************************************
        //*  PINE: MDMA190 PIN N7, EFSA9120 PIN N12
    }
    private void sub_Update_Cwf_Rtn() throws Exception                                                                                                                    //Natural: UPDATE-CWF-RTN
    {
        if (BLNatReinput.isReinput()) return;

        pdaCwfa8010.getCwfa8010_Action_Cde().setValue("MO");                                                                                                              //Natural: ASSIGN CWFA8010.ACTION-CDE := 'MO'
        pdaCwfa8010.getCwfa8010_System().setValue(pdaMdma190.getMdma190_Pnd_Wpid_System());                                                                               //Natural: ASSIGN CWFA8010.SYSTEM := MDMA190.#WPID-SYSTEM
        pdaCwfa8010.getCwfa8010_Rqst_Log_Dte_Tme().setValue(pdaMdma190.getMdma190_Pnd_Slo_Rqst_Log_Dte_Tme());                                                            //Natural: ASSIGN CWFA8010.RQST-LOG-DTE-TME := MDMA190.#SLO-RQST-LOG-DTE-TME
        pdaCwfa8010.getCwfa8010_Pin_Nbr().setValue(pdaMdma190.getMdma190_Pnd_Wpid_Pin());                                                                                 //Natural: ASSIGN CWFA8010.PIN-NBR := MDMA190.#WPID-PIN
        pdaCwfa8010.getCwfa8010_Oprtr_Cde().setValue(pdaMdma190.getMdma190_Pnd_Gda_User_Id());                                                                            //Natural: ASSIGN CWFA8010.OPRTR-CDE := MDMA190.#GDA-USER-ID
        pdaCwfa8010.getCwfa8010_Wpid_Vldte_Ind().setValue("Y");                                                                                                           //Natural: ASSIGN CWFA8010.WPID-VLDTE-IND := 'Y'
        pdaCwfa8010.getCwfa8010_Oprtr_Unit_Cde().setValue(pdaMdma190.getMdma190_Pnd_Wpid_Unit());                                                                         //Natural: ASSIGN CWFA8010.OPRTR-UNIT-CDE := MDMA190.#WPID-UNIT
        pdaCwfa8010.getCwfa8010_Spcl_Hndlng_Msg().setValue(pdaMdma190.getMdma190_Pnd_Wpid_Special_Handling_Text());                                                       //Natural: ASSIGN CWFA8010.SPCL-HNDLNG-MSG := MDMA190.#WPID-SPECIAL-HANDLING-TEXT
        //*    IF NEXT UNIT IS FILLED IN, THEN THIS WORK REQUEST REQUIRES
        //*  VERIFICATION AND MUST BE ROUTED TO CDCOR. IF NOT, THEN WE MUST REVIEW
        //*  THE STANDARD ROUTING TO DECIDE IF THIS NEEDS TO BE ROUTED TO ANOTHER
        //*  UNIT, OR IT CAN BE CORPORATELY CLOSED.
        //*    IF THE REVIEW OF THE STANDARD ROUTING INDICATES THE CDMS IS NOT PART
        //*  OF THIS WORK NORMALLY, THE PROGRAM CANNOT DETERMINE WHETHER THE WORK
        //*  REQUEST SHOULD BE CLOSED, OR WHERE IT SHOULD BE ROUTED TO. THE WORK
        //*  REQUEST WILL BE UPDATED WITH A STATUS 7600 - IN UNIT BUT MUST BE
        //*  MANUALLY ROUTED.
        if (condition(pdaMdma190.getMdma190_Pnd_Wpid_Next_Unit().equals(" ")))                                                                                            //Natural: IF MDMA190.#WPID-NEXT-UNIT = ' '
        {
            pdaCwfa6510.getCwfa6510_Pnd_Wpid().setValue(pdaMdma190.getMdma190_Pnd_Wpid());                                                                                //Natural: ASSIGN CWFA6510.#WPID := MDMA190.#WPID
            DbsUtil.callnat(Cwfn6510.class , getCurrentProcessState(), pdaCwfa6510.getCwfa6510(), pdaCwfpda_M.getMsg_Info_Sub());                                         //Natural: CALLNAT 'CWFN6510' CWFA6510 MSG-INFO-SUB
            if (condition(Global.isEscape())) return;
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO CWFA6510.#SUM-OF-UNITS-IN-ROUTING
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaCwfa6510.getCwfa6510_Pnd_Sum_Of_Units_In_Routing())); pnd_I.nadd(1))
            {
                if (condition(pdaCwfa6510.getCwfa6510_Pnd_Units_In_Routing().getValue(pnd_I).equals("CDMSA") || pdaCwfa6510.getCwfa6510_Pnd_Units_In_Routing().getValue(pnd_I).equals("CDCOR")  //Natural: IF CWFA6510.#UNITS-IN-ROUTING ( #I ) = 'CDMSA' OR = 'CDCOR' OR = 'CDMSR'
                    || pdaCwfa6510.getCwfa6510_Pnd_Units_In_Routing().getValue(pnd_I).equals("CDMSR")))
                {
                    pnd_Cdms_In_Routing.setValue("Y");                                                                                                                    //Natural: ASSIGN #CDMS-IN-ROUTING := 'Y'
                    //*  MEANS THAT THIS IS THE LAST UNIT IN ROUTING
                    if (condition(pnd_I.equals(20)))                                                                                                                      //Natural: IF #I = 20
                    {
                        pdaMdma190.getMdma190_Pnd_Wpid_Next_Unit().reset();                                                                                               //Natural: RESET MDMA190.#WPID-NEXT-UNIT
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Route_To_Next_Unit.setValue("Y");                                                                                                                 //Natural: ASSIGN #ROUTE-TO-NEXT-UNIT := 'Y'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Route_To_Next_Unit.equals("Y")))                                                                                                    //Natural: IF #ROUTE-TO-NEXT-UNIT = 'Y'
                    {
                        pdaMdma190.getMdma190_Pnd_Wpid_Next_Unit().setValue(pdaCwfa6510.getCwfa6510_Pnd_Units_In_Routing().getValue(pnd_I));                              //Natural: ASSIGN MDMA190.#WPID-NEXT-UNIT := CWFA6510.#UNITS-IN-ROUTING ( #I )
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            if (condition(pnd_Cdms_In_Routing.equals(" ")))                                                                                                               //Natural: IF #CDMS-IN-ROUTING = ' '
            {
                pdaMdma190.getMdma190_Pnd_Wpid_Status().setValue("7600");                                                                                                 //Natural: ASSIGN MDMA190.#WPID-STATUS := '7600'
                pdaMdma190.getMdma190_Pnd_Wpid_Priority_Cde().setValue("9");                                                                                              //Natural: ASSIGN MDMA190.#WPID-PRIORITY-CDE := '9'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaCwfa8010.getCwfa8010_Unit_Cde().setValue(pdaMdma190.getMdma190_Pnd_Wpid_Next_Unit());                                                                          //Natural: ASSIGN CWFA8010.UNIT-CDE := MDMA190.#WPID-NEXT-UNIT
        pdaCwfa8010.getCwfa8010_Status_Cde().setValue(pdaMdma190.getMdma190_Pnd_Wpid_Status());                                                                           //Natural: ASSIGN CWFA8010.STATUS-CDE := MDMA190.#WPID-STATUS
        pdaCwfa8010.getCwfa8010_Due_Dte_Prty_Cde().setValue(pdaMdma190.getMdma190_Pnd_Wpid_Priority_Cde());                                                               //Natural: ASSIGN CWFA8010.DUE-DTE-PRTY-CDE := MDMA190.#WPID-PRIORITY-CDE
        //*    IF THERE IS A NEXT UNIT TO BE ROUTED TO, THEN THE STATUS MUST BE
        //*  WITHIN 7000 TO 7499. IF IT ISN't, then 7100 will be used. If there is
        //*  NOT A NEXT UNIT, AND THE STATUS WAS NOT SET BY THE CALLING PROGRAM,
        //*  THEN THE WORK IS COMPLETE. IF THE WORK WAS COMPLETED BY THE
        //*  VERIFICATION UNIT, THEN IT IS CLOSED WITH A 8001, ELSE CLOSE IT WITH
        //*  A 8000.
        //*    IF THEN STATUS IS 7990, THIS MEANS THAT A NEXT UNIT WAS NOT SENT BY
        //*  THE CALLING PROGRAM, AND THAT A SEARCH OF THE STANDARD ROUTING COULD
        //*  NOT DETERMINE WHAT SHOULD BE DONE NEXT WITH THE WORK REQUEST.
        if (condition(pdaCwfa8010.getCwfa8010_Unit_Cde().equals(" ")))                                                                                                    //Natural: IF CWFA8010.UNIT-CDE = ' '
        {
            if (condition(pdaMdma190.getMdma190_Pnd_Wpid_Status().equals(" ")))                                                                                           //Natural: IF MDMA190.#WPID-STATUS = ' '
            {
                //*  CLOSE WITH VERIFY COMPLETE
                if (condition(pdaCwfa8010.getCwfa8010_Oprtr_Cde().equals("CDCOR")))                                                                                       //Natural: IF CWFA8010.OPRTR-CDE = 'CDCOR'
                {
                    pdaCwfa8010.getCwfa8010_Status_Cde().setValue("8001");                                                                                                //Natural: ASSIGN CWFA8010.STATUS-CDE := '8001'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaCwfa8010.getCwfa8010_Status_Cde().setValue("8000");                                                                                                //Natural: ASSIGN CWFA8010.STATUS-CDE := '8000'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaMdma190.getMdma190_Pnd_Wpid_Status().greaterOrEqual("7000") && pdaMdma190.getMdma190_Pnd_Wpid_Status().lessOrEqual("7499")))                 //Natural: IF MDMA190.#WPID-STATUS = '7000' THRU '7499'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaCwfa8010.getCwfa8010_Status_Cde().setValue("7100");                                                                                                    //Natural: ASSIGN CWFA8010.STATUS-CDE := '7100'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaCwfa8010.getCwfa8010_Status_Cde().greaterOrEqual("8000") && pdaCwfa8010.getCwfa8010_Status_Cde().lessOrEqual("8799")))                           //Natural: IF CWFA8010.STATUS-CDE = '8000' THRU '8799'
        {
            pdaCwfa8010.getCwfa8010_Crprte_Status_Ind().setValue("9");                                                                                                    //Natural: ASSIGN CWFA8010.CRPRTE-STATUS-IND := '9'
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Cwfn8010.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaCwfpda_D.getDialog_Info_Sub(),            //Natural: CALLNAT 'CWFN8010' MSG-INFO-SUB PASS-SUB DIALOG-INFO-SUB CWFA8010
            pdaCwfa8010.getCwfa8010());
        if (condition(Global.isEscape())) return;
        pdaMdma190.getMdma190_Pnd_Error_Message().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                    //Natural: ASSIGN MDMA190.#ERROR-MESSAGE := MSG-INFO-SUB.##MSG
        if (condition(pdaMdma190.getMdma190_Pnd_Error_Message().equals(" ")))                                                                                             //Natural: IF MDMA190.#ERROR-MESSAGE = ' '
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaCwfa8010.getCwfa8010_Status_Cde().setValue("7990");                                                                                                        //Natural: ASSIGN CWFA8010.STATUS-CDE := '7990'
            DbsUtil.callnat(Cwfn8010.class , getCurrentProcessState(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub(), pdaCwfpda_D.getDialog_Info_Sub(),        //Natural: CALLNAT 'CWFN8010' MSG-INFO-SUB PASS-SUB DIALOG-INFO-SUB CWFA8010
                pdaCwfa8010.getCwfa8010());
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  UPDATE-CWF-RTN
    }
    private void sub_Store_Cwf_Rtn() throws Exception                                                                                                                     //Natural: STORE-CWF-RTN
    {
        if (BLNatReinput.isReinput()) return;

        pdaEfsa9120.getEfsa9120_Action().setValue("AR");                                                                                                                  //Natural: ASSIGN EFSA9120.ACTION := 'AR'
        pdaEfsa9120.getEfsa9120_Cabinet_Prefix().setValue("P");                                                                                                           //Natural: ASSIGN EFSA9120.CABINET-PREFIX := 'P'
        pdaEfsa9120.getEfsa9120_Pin_Nbr().setValue(pdaMdma190.getMdma190_Pnd_Wpid_Pin());                                                                                 //Natural: ASSIGN EFSA9120.PIN-NBR := MDMA190.#WPID-PIN
        pdaEfsa9120.getEfsa9120_Tiaa_Rcvd_Dte().getValue(1).setValue(Global.getDATN());                                                                                   //Natural: ASSIGN EFSA9120.TIAA-RCVD-DTE ( 1 ) := *DATN
        pdaEfsa9120.getEfsa9120_Wpid().getValue(1).setValue(pdaMdma190.getMdma190_Pnd_Wpid());                                                                            //Natural: ASSIGN EFSA9120.WPID ( 1 ) := MDMA190.#WPID
        pdaEfsa9120.getEfsa9120_Unit_Cde().getValue(1).setValue(pdaMdma190.getMdma190_Pnd_Wpid_Next_Unit());                                                              //Natural: ASSIGN EFSA9120.UNIT-CDE ( 1 ) := MDMA190.#WPID-NEXT-UNIT
        pdaEfsa9120.getEfsa9120_Status_Cde().getValue(1).setValue(pdaMdma190.getMdma190_Pnd_Wpid_Status());                                                               //Natural: ASSIGN EFSA9120.STATUS-CDE ( 1 ) := MDMA190.#WPID-STATUS
        pdaEfsa9120.getEfsa9120_Multi_Rqst_Ind().getValue(1).setValue("0");                                                                                               //Natural: ASSIGN EFSA9120.MULTI-RQST-IND ( 1 ) := '0'
        pdaEfsa9120.getEfsa9120_Gen_Fldr_Ind().getValue(1).setValue("N");                                                                                                 //Natural: ASSIGN EFSA9120.GEN-FLDR-IND ( 1 ) := 'N'
        pdaEfsa9120.getEfsa9120_Rqst_Entry_Op_Cde().setValue(pdaMdma190.getMdma190_Pnd_Gda_User_Id());                                                                    //Natural: ASSIGN EFSA9120.RQST-ENTRY-OP-CDE := MDMA190.#GDA-USER-ID
        pdaEfsa9120.getEfsa9120_System().setValue(pdaMdma190.getMdma190_Pnd_Wpid_System());                                                                               //Natural: ASSIGN EFSA9120.SYSTEM := MDMA190.#WPID-SYSTEM
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Cde().setValue("I");                                                                                                          //Natural: ASSIGN EFSA9120.RQST-ORIGIN-CDE := 'I'
        pdaEfsa9120.getEfsa9120_Spcl_Handling_Txt().getValue(1).setValue(pdaMdma190.getMdma190_Pnd_Wpid_Special_Handling_Text());                                         //Natural: ASSIGN EFSA9120.SPCL-HANDLING-TXT ( 1 ) := MDMA190.#WPID-SPECIAL-HANDLING-TEXT
        pdaEfsa9120.getEfsa9120_Rqst_Origin_Unit_Cde().setValue(pdaMdma190.getMdma190_Pnd_Wpid_Unit());                                                                   //Natural: ASSIGN EFSA9120.RQST-ORIGIN-UNIT-CDE := MDMA190.#WPID-UNIT
        pdaEfsa9120.getEfsa9120_Wpid_Validate_Ind().getValue(1).setValue("Y");                                                                                            //Natural: ASSIGN EFSA9120.WPID-VALIDATE-IND ( 1 ) := 'Y'
        pdaEfsa9120.getEfsa9120_Wpid_Validate_Ind().getValue(1).setValue("Y");                                                                                            //Natural: ASSIGN EFSA9120.WPID-VALIDATE-IND ( 1 ) := 'Y'
        if (condition(pdaEfsa9120.getEfsa9120_Status_Cde().getValue(1).greaterOrEqual("8000") && pdaEfsa9120.getEfsa9120_Status_Cde().getValue(1).lessOrEqual("8799")))   //Natural: IF EFSA9120.STATUS-CDE ( 1 ) = '8000' THRU '8799'
        {
            pdaEfsa9120.getEfsa9120_Corp_Status().getValue(1).setValue("9");                                                                                              //Natural: ASSIGN EFSA9120.CORP-STATUS ( 1 ) := '9'
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Efsn9120.class , getCurrentProcessState(), pdaEfsa9120.getEfsa9120(), pdaEfsa9120.getEfsa9120_Id(), pdaEfsa902r.getEfsa902r(),                    //Natural: CALLNAT 'EFSN9120' EFSA9120 EFSA9120-ID EFSA902R CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
            pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
        if (condition(Global.isEscape())) return;
        pdaMdma190.getMdma190_Pnd_Error_Message().setValue(pdaCwfpda_M.getMsg_Info_Sub_Pnd_Pnd_Msg());                                                                    //Natural: ASSIGN MDMA190.#ERROR-MESSAGE := MSG-INFO-SUB.##MSG
        if (condition(pdaMdma190.getMdma190_Pnd_Error_Message().equals(" ")))                                                                                             //Natural: IF MDMA190.#ERROR-MESSAGE = ' '
        {
            pdaMdma190.getMdma190_Pnd_Slo_Rqst_Log_Dte_Tme().setValue(pdaEfsa9120.getEfsa9120_Return_Date_Time().getValue(1));                                            //Natural: ASSIGN #SLO-RQST-LOG-DTE-TME := EFSA9120.RETURN-DATE-TIME ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaEfsa9120.getEfsa9120_Status_Cde().getValue(1).setValue("7990");                                                                                            //Natural: ASSIGN EFSA9120.STATUS-CDE ( 1 ) := '7990'
            DbsUtil.callnat(Efsn9120.class , getCurrentProcessState(), pdaEfsa9120.getEfsa9120(), pdaEfsa9120.getEfsa9120_Id(), pdaEfsa902r.getEfsa902r(),                //Natural: CALLNAT 'EFSN9120' EFSA9120 EFSA9120-ID EFSA902R CDAOBJ DIALOG-INFO-SUB MSG-INFO-SUB PASS-SUB
                pdaCdaobj.getCdaobj(), pdaCwfpda_D.getDialog_Info_Sub(), pdaCwfpda_M.getMsg_Info_Sub(), pdaCwfpda_P.getPass_Sub());
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  STORE-CWF-RTN
    }

    //
}
