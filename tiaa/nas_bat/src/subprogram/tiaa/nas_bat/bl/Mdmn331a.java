/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION         * 2021-05-14 01:29:34 AM
**        * FROM NATURAL SUBPROGRAM : Mdmn331a
************************************************************
**        * FILE NAME            : Mdmn331a.java
**        * CLASS NAME           : Mdmn331a
**        * INSTANCE NAME        : Mdmn331a
************************************************************
************************************************************************
* PROGRAM NAME : MDMN331A - PIN EXPANSION CLONE OF MDMN330A
* DESCRIPTION  : MDM RETRIEVE PREVIOUS ADDRESS
*                THIS SUBPROGRAM IS CALLED TO GET MDM ADDRESS INFO.
* WRITTEN BY   : DON MEADE
* DATE WRITTEN : JUNE 20, 2017 - SEE PINE COMMENTS
************************************************************************
*    DATE      USERID                   DESCRIPTION
* 09/06/17     MEADED  LOOK FOR ITD AS WELL AS TIAA, CREF CONTRACTS.
*              ALSO A TIMING TWEAK. SEE "ITD."
************************************************************************

************************************************************ */

package tiaa.nas_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Mdmn331a extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    public DbsRecord parameters;
    private PdaMdma331 pdaMdma331;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Max_In;
    private DbsField pnd_Max_Out;
    private DbsField pnd_Data_In;

    private DbsGroup pnd_Data_In__R_Field_1;
    private DbsField pnd_Data_In_Pnd_I_Tiaa_Cref_Ind;
    private DbsField pnd_Data_In_Pnd_I_Contract_Number;
    private DbsField pnd_Data_In_Pnd_I_Payee_Code_A2;
    private DbsField pnd_Data_In_Pnd_I_Address_Usage;
    private DbsField pnd_Data_In_Pnd_I_Inquire_Date;
    private DbsField pnd_Data_In_Pnd_I_Inquire_Time;
    private DbsField pnd_Return;

    private DbsGroup pnd_Return__R_Field_2;
    private DbsField pnd_Return_Pnd_Rtrn_Code;
    private DbsField pnd_Return_Pnd_Rtrn_Text;
    private DbsField pnd_Data_Out;

    private DbsGroup pnd_Data_Out__R_Field_3;

    private DbsGroup pnd_Data_Out_Pnd_Temp_Data;
    private DbsField pnd_Data_Out_Pnd_O_Ssn;
    private DbsField pnd_Data_Out_Pnd_O_Pin;
    private DbsField pnd_Data_Out_Pnd_O_Preferred_Last_Name;
    private DbsField pnd_Data_Out_Pnd_O_Preferred_First_Name;
    private DbsField pnd_Data_Out_Pnd_O_Preferred_Middle_Name;
    private DbsField pnd_Data_Out_Pnd_O_Preferred_Prefix;
    private DbsField pnd_Data_Out_Pnd_O_Preferred_Suffix;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Address_Type_Code;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Address_Line_1;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Address_Line_2;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Address_Line_3;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Address_Line_4;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Address_City;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Address_St_Prov;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Address_Zip_Code;

    private DbsGroup pnd_Data_Out__R_Field_4;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Address_Zip1_5;

    private DbsGroup pnd_Data_Out__R_Field_5;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Address_Zip1_7;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Address_Country;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Check_Saving_Ind;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Bank_Account_Number;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Bank_Aba_Eft_Number;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Eft_Pay_Type;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Address_Postal_Data;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Address_Geographic_Code;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Address_Status_Code;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Address_Status_Date;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Address_Override_Code;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Address_Last_Update_Date;

    private DbsGroup pnd_Data_Out__R_Field_6;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Addr_Last_Update_Date_A8;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Address_Last_Update_Time;

    private DbsGroup pnd_Data_Out__R_Field_7;
    private DbsField pnd_Data_Out_Pnd_O_Prvs_Addr_Last_Update_Time_A7;
    private DbsField pnd_Data_Out_Pnd_O_Line_Of_Business;
    private DbsField pnd_Data_Out_Pnd_O_Contract_Status_Code;
    private DbsField pnd_Data_Out_Pnd_O_Contract_Status_Year;
    private DbsField pnd_Data_Out_Pnd_O_Pin_A12;

    private DbsGroup pnd_Data_Out__R_Field_8;
    private DbsField pnd_Data_Out_Pnd_O_Pin_N7;
    private DbsField pnd_Data_Out_Pnd_O_Pin_Blanks;

    private DbsGroup pnd_Data_Out__R_Field_9;
    private DbsField pnd_Data_Out_Pnd_O_Pin_N12;
    private DbsField pnd_City_State_Zip;
    private DbsField pnd_Date;
    private DbsField pnd_No_Date_Time;
    private DbsField pnd_Save_Code;
    private DbsField pnd_Save_Text;
    private DbsField pnd_Time;
    private DbsField pnd_Timestamp;

    private DbsGroup pnd_Timestamp__R_Field_10;
    private DbsField pnd_Timestamp_Pnd_Date_A10;
    private DbsField pnd_Timestamp_Pnd_Time_A8;
    private DbsField pls_Trace;

    public void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        // parameters
        parameters = new DbsRecord();
        pdaMdma331 = new PdaMdma331(parameters);
        parameters.setRecordName("parameters");
        registerRecord(parameters);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Max_In = localVariables.newFieldInRecord("pnd_Max_In", "#MAX-IN", FieldType.NUMERIC, 5);
        pnd_Max_Out = localVariables.newFieldInRecord("pnd_Max_Out", "#MAX-OUT", FieldType.NUMERIC, 5);
        pnd_Data_In = localVariables.newFieldArrayInRecord("pnd_Data_In", "#DATA-IN", FieldType.STRING, 1, new DbsArrayController(1, 32560));

        pnd_Data_In__R_Field_1 = localVariables.newGroupInRecord("pnd_Data_In__R_Field_1", "REDEFINE", pnd_Data_In);
        pnd_Data_In_Pnd_I_Tiaa_Cref_Ind = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Tiaa_Cref_Ind", "#I-TIAA-CREF-IND", FieldType.STRING, 
            1);
        pnd_Data_In_Pnd_I_Contract_Number = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Contract_Number", "#I-CONTRACT-NUMBER", FieldType.STRING, 
            10);
        pnd_Data_In_Pnd_I_Payee_Code_A2 = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Payee_Code_A2", "#I-PAYEE-CODE-A2", FieldType.STRING, 
            2);
        pnd_Data_In_Pnd_I_Address_Usage = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Address_Usage", "#I-ADDRESS-USAGE", FieldType.STRING, 
            1);
        pnd_Data_In_Pnd_I_Inquire_Date = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Inquire_Date", "#I-INQUIRE-DATE", FieldType.STRING, 
            10);
        pnd_Data_In_Pnd_I_Inquire_Time = pnd_Data_In__R_Field_1.newFieldInGroup("pnd_Data_In_Pnd_I_Inquire_Time", "#I-INQUIRE-TIME", FieldType.STRING, 
            8);
        pnd_Return = localVariables.newFieldArrayInRecord("pnd_Return", "#RETURN", FieldType.STRING, 1, new DbsArrayController(1, 200));

        pnd_Return__R_Field_2 = localVariables.newGroupInRecord("pnd_Return__R_Field_2", "REDEFINE", pnd_Return);
        pnd_Return_Pnd_Rtrn_Code = pnd_Return__R_Field_2.newFieldInGroup("pnd_Return_Pnd_Rtrn_Code", "#RTRN-CODE", FieldType.STRING, 4);
        pnd_Return_Pnd_Rtrn_Text = pnd_Return__R_Field_2.newFieldInGroup("pnd_Return_Pnd_Rtrn_Text", "#RTRN-TEXT", FieldType.STRING, 80);
        pnd_Data_Out = localVariables.newFieldArrayInRecord("pnd_Data_Out", "#DATA-OUT", FieldType.STRING, 1, new DbsArrayController(1, 32360));

        pnd_Data_Out__R_Field_3 = localVariables.newGroupInRecord("pnd_Data_Out__R_Field_3", "REDEFINE", pnd_Data_Out);

        pnd_Data_Out_Pnd_Temp_Data = pnd_Data_Out__R_Field_3.newGroupInGroup("pnd_Data_Out_Pnd_Temp_Data", "#TEMP-DATA");
        pnd_Data_Out_Pnd_O_Ssn = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Ssn", "#O-SSN", FieldType.NUMERIC, 9);
        pnd_Data_Out_Pnd_O_Pin = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Pin", "#O-PIN", FieldType.NUMERIC, 7);
        pnd_Data_Out_Pnd_O_Preferred_Last_Name = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Preferred_Last_Name", "#O-PREFERRED-LAST-NAME", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Preferred_First_Name = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Preferred_First_Name", "#O-PREFERRED-FIRST-NAME", 
            FieldType.STRING, 30);
        pnd_Data_Out_Pnd_O_Preferred_Middle_Name = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Preferred_Middle_Name", "#O-PREFERRED-MIDDLE-NAME", 
            FieldType.STRING, 30);
        pnd_Data_Out_Pnd_O_Preferred_Prefix = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Preferred_Prefix", "#O-PREFERRED-PREFIX", 
            FieldType.STRING, 8);
        pnd_Data_Out_Pnd_O_Preferred_Suffix = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Preferred_Suffix", "#O-PREFERRED-SUFFIX", 
            FieldType.STRING, 8);
        pnd_Data_Out_Pnd_O_Prvs_Address_Type_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Address_Type_Code", "#O-PRVS-ADDRESS-TYPE-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Prvs_Address_Line_1 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Address_Line_1", "#O-PRVS-ADDRESS-LINE-1", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Prvs_Address_Line_2 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Address_Line_2", "#O-PRVS-ADDRESS-LINE-2", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Prvs_Address_Line_3 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Address_Line_3", "#O-PRVS-ADDRESS-LINE-3", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Prvs_Address_Line_4 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Address_Line_4", "#O-PRVS-ADDRESS-LINE-4", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Prvs_Address_City = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Address_City", "#O-PRVS-ADDRESS-CITY", 
            FieldType.STRING, 25);
        pnd_Data_Out_Pnd_O_Prvs_Address_St_Prov = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Address_St_Prov", "#O-PRVS-ADDRESS-ST-PROV", 
            FieldType.STRING, 2);
        pnd_Data_Out_Pnd_O_Prvs_Address_Zip_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Address_Zip_Code", "#O-PRVS-ADDRESS-ZIP-CODE", 
            FieldType.STRING, 10);

        pnd_Data_Out__R_Field_4 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_4", "REDEFINE", pnd_Data_Out_Pnd_O_Prvs_Address_Zip_Code);
        pnd_Data_Out_Pnd_O_Prvs_Address_Zip1_5 = pnd_Data_Out__R_Field_4.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Address_Zip1_5", "#O-PRVS-ADDRESS-ZIP1-5", 
            FieldType.STRING, 5);

        pnd_Data_Out__R_Field_5 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_5", "REDEFINE", pnd_Data_Out_Pnd_O_Prvs_Address_Zip_Code);
        pnd_Data_Out_Pnd_O_Prvs_Address_Zip1_7 = pnd_Data_Out__R_Field_5.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Address_Zip1_7", "#O-PRVS-ADDRESS-ZIP1-7", 
            FieldType.STRING, 7);
        pnd_Data_Out_Pnd_O_Prvs_Address_Country = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Address_Country", "#O-PRVS-ADDRESS-COUNTRY", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Prvs_Check_Saving_Ind = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Check_Saving_Ind", "#O-PRVS-CHECK-SAVING-IND", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Prvs_Bank_Account_Number = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Bank_Account_Number", "#O-PRVS-BANK-ACCOUNT-NUMBER", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Prvs_Bank_Aba_Eft_Number = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Bank_Aba_Eft_Number", "#O-PRVS-BANK-ABA-EFT-NUMBER", 
            FieldType.STRING, 35);
        pnd_Data_Out_Pnd_O_Prvs_Eft_Pay_Type = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Eft_Pay_Type", "#O-PRVS-EFT-PAY-TYPE", 
            FieldType.STRING, 2);
        pnd_Data_Out_Pnd_O_Prvs_Address_Postal_Data = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Address_Postal_Data", "#O-PRVS-ADDRESS-POSTAL-DATA", 
            FieldType.STRING, 32);
        pnd_Data_Out_Pnd_O_Prvs_Address_Geographic_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Address_Geographic_Code", 
            "#O-PRVS-ADDRESS-GEOGRAPHIC-CODE", FieldType.STRING, 2);
        pnd_Data_Out_Pnd_O_Prvs_Address_Status_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Address_Status_Code", "#O-PRVS-ADDRESS-STATUS-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Prvs_Address_Status_Date = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Address_Status_Date", "#O-PRVS-ADDRESS-STATUS-DATE", 
            FieldType.NUMERIC, 8);
        pnd_Data_Out_Pnd_O_Prvs_Address_Override_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Address_Override_Code", "#O-PRVS-ADDRESS-OVERRIDE-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Prvs_Address_Last_Update_Date = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Address_Last_Update_Date", 
            "#O-PRVS-ADDRESS-LAST-UPDATE-DATE", FieldType.NUMERIC, 8);

        pnd_Data_Out__R_Field_6 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_6", "REDEFINE", pnd_Data_Out_Pnd_O_Prvs_Address_Last_Update_Date);
        pnd_Data_Out_Pnd_O_Prvs_Addr_Last_Update_Date_A8 = pnd_Data_Out__R_Field_6.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Addr_Last_Update_Date_A8", 
            "#O-PRVS-ADDR-LAST-UPDATE-DATE-A8", FieldType.STRING, 8);
        pnd_Data_Out_Pnd_O_Prvs_Address_Last_Update_Time = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Address_Last_Update_Time", 
            "#O-PRVS-ADDRESS-LAST-UPDATE-TIME", FieldType.NUMERIC, 7);

        pnd_Data_Out__R_Field_7 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_7", "REDEFINE", pnd_Data_Out_Pnd_O_Prvs_Address_Last_Update_Time);
        pnd_Data_Out_Pnd_O_Prvs_Addr_Last_Update_Time_A7 = pnd_Data_Out__R_Field_7.newFieldInGroup("pnd_Data_Out_Pnd_O_Prvs_Addr_Last_Update_Time_A7", 
            "#O-PRVS-ADDR-LAST-UPDATE-TIME-A7", FieldType.STRING, 7);
        pnd_Data_Out_Pnd_O_Line_Of_Business = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Line_Of_Business", "#O-LINE-OF-BUSINESS", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Contract_Status_Code = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Contract_Status_Code", "#O-CONTRACT-STATUS-CODE", 
            FieldType.STRING, 1);
        pnd_Data_Out_Pnd_O_Contract_Status_Year = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Contract_Status_Year", "#O-CONTRACT-STATUS-YEAR", 
            FieldType.NUMERIC, 4);
        pnd_Data_Out_Pnd_O_Pin_A12 = pnd_Data_Out_Pnd_Temp_Data.newFieldInGroup("pnd_Data_Out_Pnd_O_Pin_A12", "#O-PIN-A12", FieldType.STRING, 12);

        pnd_Data_Out__R_Field_8 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_8", "REDEFINE", pnd_Data_Out_Pnd_O_Pin_A12);
        pnd_Data_Out_Pnd_O_Pin_N7 = pnd_Data_Out__R_Field_8.newFieldInGroup("pnd_Data_Out_Pnd_O_Pin_N7", "#O-PIN-N7", FieldType.NUMERIC, 7);
        pnd_Data_Out_Pnd_O_Pin_Blanks = pnd_Data_Out__R_Field_8.newFieldInGroup("pnd_Data_Out_Pnd_O_Pin_Blanks", "#O-PIN-BLANKS", FieldType.STRING, 5);

        pnd_Data_Out__R_Field_9 = pnd_Data_Out_Pnd_Temp_Data.newGroupInGroup("pnd_Data_Out__R_Field_9", "REDEFINE", pnd_Data_Out_Pnd_O_Pin_A12);
        pnd_Data_Out_Pnd_O_Pin_N12 = pnd_Data_Out__R_Field_9.newFieldInGroup("pnd_Data_Out_Pnd_O_Pin_N12", "#O-PIN-N12", FieldType.NUMERIC, 12);
        pnd_City_State_Zip = localVariables.newFieldInRecord("pnd_City_State_Zip", "#CITY-STATE-ZIP", FieldType.STRING, 35);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.DATE);
        pnd_No_Date_Time = localVariables.newFieldInRecord("pnd_No_Date_Time", "#NO-DATE-TIME", FieldType.BOOLEAN, 1);
        pnd_Save_Code = localVariables.newFieldInRecord("pnd_Save_Code", "#SAVE-CODE", FieldType.STRING, 4);
        pnd_Save_Text = localVariables.newFieldInRecord("pnd_Save_Text", "#SAVE-TEXT", FieldType.STRING, 80);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);
        pnd_Timestamp = localVariables.newFieldInRecord("pnd_Timestamp", "#TIMESTAMP", FieldType.STRING, 18);

        pnd_Timestamp__R_Field_10 = localVariables.newGroupInRecord("pnd_Timestamp__R_Field_10", "REDEFINE", pnd_Timestamp);
        pnd_Timestamp_Pnd_Date_A10 = pnd_Timestamp__R_Field_10.newFieldInGroup("pnd_Timestamp_Pnd_Date_A10", "#DATE-A10", FieldType.STRING, 10);
        pnd_Timestamp_Pnd_Time_A8 = pnd_Timestamp__R_Field_10.newFieldInGroup("pnd_Timestamp_Pnd_Time_A8", "#TIME-A8", FieldType.STRING, 8);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Max_In.setInitialValue(32);
        pnd_Max_Out.setInitialValue(490);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    public Mdmn331a() throws Exception
    {
        super("Mdmn331a");
        initializeFields(false);
        initializeValues();
    }

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    public void main(Object... parms) throws Exception
    {
        try
        {
            parameters.setParameterDataIn(parms);
            runMain();
            parameters.setParameterDataOut(parms);
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }

    private void runMain() throws Exception
    {
        //*  ASSIGN DEFAULTS
        //*  ITD
        //*  DEFAULT TO TIAA
        short decideConditionsMet228 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN SUBSTRING ( #MDMA331.#I-CONTRACT-NUMBER,1,3 ) = 'ITD'
        if (condition(pdaMdma331.getPnd_Mdma331_Pnd_I_Contract_Number().getSubstring(1,3).equals("ITD")))
        {
            decideConditionsMet228++;
            pdaMdma331.getPnd_Mdma331_Pnd_I_Tiaa_Cref_Ind().setValue("I");                                                                                                //Natural: ASSIGN #MDMA331.#I-TIAA-CREF-IND := 'I'
        }                                                                                                                                                                 //Natural: WHEN #MDMA331.#I-TIAA-CREF-IND = ' '
        if (condition(pdaMdma331.getPnd_Mdma331_Pnd_I_Tiaa_Cref_Ind().equals(" ")))
        {
            decideConditionsMet228++;
            pdaMdma331.getPnd_Mdma331_Pnd_I_Tiaa_Cref_Ind().setValue("T");                                                                                                //Natural: ASSIGN #MDMA331.#I-TIAA-CREF-IND := 'T'
        }                                                                                                                                                                 //Natural: WHEN #MDMA331.#I-PAYEE-CODE = 0 OR #MDMA331.#I-PAYEE-CODE-A2 = ' '
        if (condition(pdaMdma331.getPnd_Mdma331_Pnd_I_Payee_Code().equals(getZero()) || pdaMdma331.getPnd_Mdma331_Pnd_I_Payee_Code_A2().equals(" ")))
        {
            decideConditionsMet228++;
            pdaMdma331.getPnd_Mdma331_Pnd_I_Payee_Code_A2().setValue("00");                                                                                               //Natural: ASSIGN #MDMA331.#I-PAYEE-CODE-A2 := '00'
        }                                                                                                                                                                 //Natural: WHEN #MDMA331.#I-ADDRESS-USAGE = ' '
        if (condition(pdaMdma331.getPnd_Mdma331_Pnd_I_Address_Usage().equals(" ")))
        {
            decideConditionsMet228++;
            pdaMdma331.getPnd_Mdma331_Pnd_I_Address_Usage().setValue("O");                                                                                                //Natural: ASSIGN #MDMA331.#I-ADDRESS-USAGE := 'O'
        }                                                                                                                                                                 //Natural: WHEN #MDMA331.#I-INQUIRE-DATE = MASK ( YYYY'-'MM'-'DD ) AND #MDMA331.#I-INQUIRE-TIME NE ' '
        if (condition(DbsUtil.maskMatches(pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Date(),"YYYY'-'MM'-'DD") && pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Time().notEquals(" ")))
        {
            decideConditionsMet228++;
            pnd_Timestamp_Pnd_Date_A10.setValue(pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Date());                                                                          //Natural: ASSIGN #DATE-A10 := #MDMA331.#I-INQUIRE-DATE
            pnd_Timestamp_Pnd_Time_A8.setValue(pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Time());                                                                           //Natural: ASSIGN #TIME-A8 := #MDMA331.#I-INQUIRE-TIME
            pnd_Time.setValueEdited(new ReportEditMask("YYYY-MM-DDHH:II:SS"),pnd_Timestamp);                                                                              //Natural: MOVE EDITED #TIMESTAMP TO #TIME ( EM = YYYY-MM-DDHH:II:SS )
            pnd_Time.nsubtract(50);                                                                                                                                       //Natural: SUBTRACT 50 FROM #TIME
            pnd_Timestamp.setValueEdited(pnd_Time,new ReportEditMask("YYYY-MM-DDHH:II:SS"));                                                                              //Natural: MOVE EDITED #TIME ( EM = YYYY-MM-DDHH:II:SS ) TO #TIMESTAMP
            pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Date().setValue(pnd_Timestamp_Pnd_Date_A10);                                                                          //Natural: ASSIGN #MDMA331.#I-INQUIRE-DATE := #DATE-A10
            pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Time().setValue(pnd_Timestamp_Pnd_Time_A8);                                                                           //Natural: ASSIGN #MDMA331.#I-INQUIRE-TIME := #TIME-A8
        }                                                                                                                                                                 //Natural: WHEN #MDMA331.#I-INQUIRE-DATE = ' ' OR NOT ( #MDMA331.#I-INQUIRE-DATE = MASK ( YYYY'-'MM'-'DD ) )
        if (condition(pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Date().equals(" ") || ! (DbsUtil.maskMatches(pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Date(),
            "YYYY'-'MM'-'DD"))))
        {
            decideConditionsMet228++;
            pnd_No_Date_Time.setValue(true);                                                                                                                              //Natural: ASSIGN #NO-DATE-TIME := TRUE
            pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Date().setValueEdited(Global.getDATX(),new ReportEditMask("YYYY-MM-DD"));                                             //Natural: MOVE EDITED *DATX ( EM = YYYY-MM-DD ) TO #MDMA331.#I-INQUIRE-DATE
            pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Time().setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS"));                                               //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS ) TO #MDMA331.#I-INQUIRE-TIME
            if (condition(pls_Trace.getBoolean()))                                                                                                                        //Natural: IF +TRACE
            {
                getReports().write(0, "Inq Date:",pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Date(),"Inq Time:",pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Time());             //Natural: WRITE 'Inq Date:' #MDMA331.#I-INQUIRE-DATE 'Inq Time:' #MDMA331.#I-INQUIRE-TIME
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #MDMA331.#I-INQUIRE-TIME = ' ' OR NOT ( #MDMA331.#I-INQUIRE-HOUR = 00 THRU 23 AND #MDMA331.#I-INQUIRE-COLON1 = ':' AND #MDMA331.#I-INQUIRE-MINUTE = 00 THRU 59 AND #MDMA331.#I-INQUIRE-COLON2 = ':' AND #MDMA331.#I-INQUIRE-SECOND = 00 THRU 99 )
        if (condition((pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Time().equals(" ") || ! ((((((pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Hour().greaterOrEqual(0) 
            && pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Hour().lessOrEqual(23)) && pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Colon1().equals(":")) && (pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Minute().greaterOrEqual(0) 
            && pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Minute().lessOrEqual(59))) && pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Colon2().equals(":")) && (pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Second().greaterOrEqual(0) 
            && pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Second().lessOrEqual(99)))))))
        {
            decideConditionsMet228++;
            pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Time().setValue("00:00:01");                                                                                          //Natural: ASSIGN #MDMA331.#I-INQUIRE-TIME := '00:00:01'
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet228 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM FETCH-MDMP0010
        sub_Fetch_Mdmp0010();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Return_Pnd_Rtrn_Code.equals("0000")))                                                                                                           //Natural: IF #RTRN-CODE = '0000'
        {
            if (condition(pnd_No_Date_Time.getBoolean()))                                                                                                                 //Natural: IF #NO-DATE-TIME
            {
                if (condition(DbsUtil.maskMatches(pnd_Data_Out_Pnd_O_Prvs_Addr_Last_Update_Date_A8,"YYYYMMDD")))                                                          //Natural: IF #DATA-OUT.#O-PRVS-ADDR-LAST-UPDATE-DATE-A8 = MASK ( YYYYMMDD )
                {
                    pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Data_Out_Pnd_O_Prvs_Addr_Last_Update_Date_A8);                                             //Natural: MOVE EDITED #DATA-OUT.#O-PRVS-ADDR-LAST-UPDATE-DATE-A8 TO #DATE ( EM = YYYYMMDD )
                    pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Date().setValueEdited(pnd_Date,new ReportEditMask("YYYY-MM-DD"));                                             //Natural: MOVE EDITED #DATE ( EM = YYYY-MM-DD ) TO #MDMA331.#I-INQUIRE-DATE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Data_Out_Pnd_O_Prvs_Addr_Last_Update_Time_A7.greater(" ")))                                                                             //Natural: IF #DATA-OUT.#O-PRVS-ADDR-LAST-UPDATE-TIME-A7 > ' '
                {
                    pnd_Time.setValueEdited(new ReportEditMask("HHIISST"),pnd_Data_Out_Pnd_O_Prvs_Addr_Last_Update_Time_A7);                                              //Natural: MOVE EDITED #DATA-OUT.#O-PRVS-ADDR-LAST-UPDATE-TIME-A7 TO #TIME ( EM = HHIISST )
                    pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Time().setValueEdited(pnd_Time,new ReportEditMask("HH:II:SS"));                                               //Natural: MOVE EDITED #TIME ( EM = HH:II:SS ) TO #MDMA331.#I-INQUIRE-TIME
                    pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Second().setValue(1);                                                                                         //Natural: ASSIGN #I-INQUIRE-SECOND := 01
                    //*    #MDMA331.#I-INQUIRE-TIME := '00:00:01'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pls_Trace.getBoolean()))                                                                                                                    //Natural: IF +TRACE
                {
                    getReports().write(0, "Inq Date:",pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Date(),"Inq Time:",pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Time());         //Natural: WRITE 'Inq Date:' #MDMA331.#I-INQUIRE-DATE 'Inq Time:' #MDMA331.#I-INQUIRE-TIME
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM FETCH-MDMP0010
                sub_Fetch_Mdmp0010();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaMdma331.getPnd_Mdma331_Pnd_O_Return_Code().setValue(pnd_Return_Pnd_Rtrn_Code);                                                                                 //Natural: ASSIGN #MDMA331.#O-RETURN-CODE := #RTRN-CODE
        pdaMdma331.getPnd_Mdma331_Pnd_O_Return_Text().setValue(pnd_Return_Pnd_Rtrn_Text);                                                                                 //Natural: ASSIGN #MDMA331.#O-RETURN-TEXT := #RTRN-TEXT
        pdaMdma331.getPnd_Mdma331_Pnd_O_Pda_Data().setValuesByName(pnd_Data_Out_Pnd_Temp_Data);                                                                           //Natural: MOVE BY NAME #TEMP-DATA TO #MDMA331.#O-PDA-DATA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FETCH-MDMP0010
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DERIVE-MAINFRAME-FIELDS
    }
    private void sub_Fetch_Mdmp0010() throws Exception                                                                                                                    //Natural: FETCH-MDMP0010
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        gdaMdmg0001.getPnd_Pnd_Mdmg0001().reset();                                                                                                                        //Natural: RESET ##MDMG0001
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Date_Sent().setValueEdited(Global.getDATX(),new ReportEditMask("YYYY-MM-DD"));                                        //Natural: MOVE EDITED *DATX ( EM = YYYY-MM-DD ) TO ##MSG-DATE-SENT
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Time_Sent().setValueEdited(Global.getTIMX(),new ReportEditMask("HH:II:SS"));                                          //Natural: MOVE EDITED *TIMX ( EM = HH:II:SS ) TO ##MSG-TIME-SENT
        //*  PINE
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Guid().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "mfsync::", Global.getINIT_PROGRAM(),                 //Natural: COMPRESS 'mfsync::' *INIT-PROGRAM #MDMA331.#I-TIAA-CREF-IND #MDMA331.#I-CONTRACT-NUMBER #MDMA331.#I-PAYEE-CODE-A2 ##MSG-DATE-SENT ##MSG-TIME-SENT INTO ##MSG-GUID LEAVING NO
            pdaMdma331.getPnd_Mdma331_Pnd_I_Tiaa_Cref_Ind(), pdaMdma331.getPnd_Mdma331_Pnd_I_Contract_Number(), pdaMdma331.getPnd_Mdma331_Pnd_I_Payee_Code_A2(), 
            gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Date_Sent(), gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Time_Sent()));
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Ping_Ind().setValue("N");                                                                                             //Natural: ASSIGN ##MSG-PING-IND := 'N'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Sender_Appl_Id().setValue("LEGACY");                                                                                      //Natural: ASSIGN ##SENDER-APPL-ID := 'LEGACY'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Tgt_Appl_Id().setValue("MDM");                                                                                            //Natural: ASSIGN ##TGT-APPL-ID := 'MDM'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Tgt_Module_Code().setValue("N");                                                                                          //Natural: ASSIGN ##TGT-MODULE-CODE := 'N'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Tgt_Module_Name().setValue("MDMN331");                                                                                    //Natural: ASSIGN ##TGT-MODULE-NAME := 'MDMN331'
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Pda_Ctr().setValue(1);                                                                                                    //Natural: ASSIGN ##PDA-CTR := 1
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Pda_Length().setValue(pnd_Max_In);                                                                                        //Natural: ASSIGN ##PDA-LENGTH := #MAX-IN
        pnd_Data_In_Pnd_I_Tiaa_Cref_Ind.setValue(pdaMdma331.getPnd_Mdma331_Pnd_I_Tiaa_Cref_Ind());                                                                        //Natural: ASSIGN #DATA-IN.#I-TIAA-CREF-IND := #MDMA331.#I-TIAA-CREF-IND
        pnd_Data_In_Pnd_I_Contract_Number.setValue(pdaMdma331.getPnd_Mdma331_Pnd_I_Contract_Number());                                                                    //Natural: ASSIGN #DATA-IN.#I-CONTRACT-NUMBER := #MDMA331.#I-CONTRACT-NUMBER
        pnd_Data_In_Pnd_I_Payee_Code_A2.setValue(pdaMdma331.getPnd_Mdma331_Pnd_I_Payee_Code_A2());                                                                        //Natural: ASSIGN #DATA-IN.#I-PAYEE-CODE-A2 := #MDMA331.#I-PAYEE-CODE-A2
        pnd_Data_In_Pnd_I_Address_Usage.setValue(pdaMdma331.getPnd_Mdma331_Pnd_I_Address_Usage());                                                                        //Natural: ASSIGN #DATA-IN.#I-ADDRESS-USAGE := #MDMA331.#I-ADDRESS-USAGE
        pnd_Data_In_Pnd_I_Inquire_Date.setValue(pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Date());                                                                          //Natural: ASSIGN #DATA-IN.#I-INQUIRE-DATE := #MDMA331.#I-INQUIRE-DATE
        pnd_Data_In_Pnd_I_Inquire_Time.setValue(pdaMdma331.getPnd_Mdma331_Pnd_I_Inquire_Time());                                                                          //Natural: ASSIGN #DATA-IN.#I-INQUIRE-TIME := #MDMA331.#I-INQUIRE-TIME
        gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_In().getValue("*").setValue(pnd_Data_In.getValue("*"));                                                              //Natural: ASSIGN ##DATA-IN ( * ) := #DATA-IN ( * )
        getReports().write(0, "Guid",gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Msg_Guid());                                                                                 //Natural: WRITE 'Guid' ##MSG-GUID
        if (Global.isEscape()) return;
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0010"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0010'
        if (condition(Global.isEscape())) return;
        if (condition(pls_Trace.getBoolean() && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Pda_Length().notEquals(pnd_Max_Out)))                                             //Natural: IF +TRACE AND ##PDA-LENGTH NE #MAX-OUT
        {
            getReports().write(0, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Pda_Length(),"Different PDA Length",pnd_Max_Out);                                               //Natural: WRITE ##PDA-LENGTH 'Different PDA Length' #MAX-OUT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Return.getValue("*").setValue(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue("*"));                                                         //Natural: ASSIGN #RETURN ( * ) := ##DATA-RESPONSE ( * )
        pnd_Data_Out.getValue("*").setValue(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Out().getValue("*"));                                                            //Natural: ASSIGN #DATA-OUT ( * ) := ##DATA-OUT ( * )
                                                                                                                                                                          //Natural: PERFORM DERIVE-MAINFRAME-FIELDS
        sub_Derive_Mainframe_Fields();
        if (condition(Global.isEscape())) {return;}
        //*  FETCH-MDMP0010
    }
    private void sub_Derive_Mainframe_Fields() throws Exception                                                                                                           //Natural: DERIVE-MAINFRAME-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Data_Out_Pnd_O_Contract_Status_Code.equals(" ") || pnd_Data_Out_Pnd_O_Contract_Status_Code.equals("H") || pnd_Data_Out_Pnd_O_Contract_Status_Code.equals("Y"))) //Natural: IF #DATA-OUT.#O-CONTRACT-STATUS-CODE = ' ' OR = 'H' OR = 'Y'
        {
            pnd_Data_Out_Pnd_O_Contract_Status_Code.setValue(" ");                                                                                                        //Natural: ASSIGN #DATA-OUT.#O-CONTRACT-STATUS-CODE := ' '
            pnd_Data_Out_Pnd_O_Contract_Status_Year.setValue(0);                                                                                                          //Natural: ASSIGN #DATA-OUT.#O-CONTRACT-STATUS-YEAR := 0
        }                                                                                                                                                                 //Natural: END-IF
        pnd_City_State_Zip.reset();                                                                                                                                       //Natural: RESET #CITY-STATE-ZIP
        //*  FOREIGN
        //*  CANADIAN
        short decideConditionsMet343 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #DATA-OUT.#O-PRVS-ADDRESS-TYPE-CODE = 'F'
        if (condition(pnd_Data_Out_Pnd_O_Prvs_Address_Type_Code.equals("F")))
        {
            decideConditionsMet343++;
            pnd_Data_Out_Pnd_O_Prvs_Address_Zip_Code.setValue("FORGN");                                                                                                   //Natural: ASSIGN #DATA-OUT.#O-PRVS-ADDRESS-ZIP-CODE := 'FORGN'
            pnd_Data_Out_Pnd_O_Prvs_Address_Postal_Data.setValue("FORGN");                                                                                                //Natural: ASSIGN #DATA-OUT.#O-PRVS-ADDRESS-POSTAL-DATA := 'FORGN'
        }                                                                                                                                                                 //Natural: WHEN #DATA-OUT.#O-PRVS-ADDRESS-TYPE-CODE = 'C' OR #DATA-OUT.#O-PRVS-ADDRESS-COUNTRY = 'CANADA' OR = 'CA'
        if (condition(pnd_Data_Out_Pnd_O_Prvs_Address_Type_Code.equals("C") || pnd_Data_Out_Pnd_O_Prvs_Address_Country.equals("CANADA") || pnd_Data_Out_Pnd_O_Prvs_Address_Country.equals("CA")))
        {
            decideConditionsMet343++;
            //*  NOT APPLICABLE
            //*  UNKNOWN
            if (condition(pnd_Data_Out_Pnd_O_Prvs_Address_City.notEquals("N/A") && pnd_Data_Out_Pnd_O_Prvs_Address_St_Prov.notEquals("UN")))                              //Natural: IF #DATA-OUT.#O-PRVS-ADDRESS-CITY NE 'N/A' AND #DATA-OUT.#O-PRVS-ADDRESS-ST-PROV NE 'UN'
            {
                pnd_City_State_Zip.setValue(DbsUtil.compress(pnd_Data_Out_Pnd_O_Prvs_Address_City, pnd_Data_Out_Pnd_O_Prvs_Address_St_Prov, pnd_Data_Out_Pnd_O_Prvs_Address_Zip1_7)); //Natural: COMPRESS #DATA-OUT.#O-PRVS-ADDRESS-CITY #DATA-OUT.#O-PRVS-ADDRESS-ST-PROV #DATA-OUT.#O-PRVS-ADDRESS-ZIP1-7 INTO #CITY-STATE-ZIP
                //*  DOMESTIC
            }                                                                                                                                                             //Natural: END-IF
            pnd_Data_Out_Pnd_O_Prvs_Address_Zip_Code.setValue("CANAD");                                                                                                   //Natural: ASSIGN #DATA-OUT.#O-PRVS-ADDRESS-ZIP-CODE := 'CANAD'
            pnd_Data_Out_Pnd_O_Prvs_Address_Postal_Data.setValue("CANAD");                                                                                                //Natural: ASSIGN #DATA-OUT.#O-PRVS-ADDRESS-POSTAL-DATA := 'CANAD'
            pnd_Data_Out_Pnd_O_Prvs_Address_Country.setValue("CANADA");                                                                                                   //Natural: ASSIGN #DATA-OUT.#O-PRVS-ADDRESS-COUNTRY := 'CANADA'
        }                                                                                                                                                                 //Natural: WHEN #DATA-OUT.#O-PRVS-ADDRESS-TYPE-CODE = 'U' OR = 'D' OR #DATA-OUT.#O-PRVS-ADDRESS-COUNTRY = 'USA' OR = 'US'
        if (condition(pnd_Data_Out_Pnd_O_Prvs_Address_Type_Code.equals("U") || pnd_Data_Out_Pnd_O_Prvs_Address_Type_Code.equals("D") || pnd_Data_Out_Pnd_O_Prvs_Address_Country.equals("USA") 
            || pnd_Data_Out_Pnd_O_Prvs_Address_Country.equals("US")))
        {
            decideConditionsMet343++;
            //*  NOT APPLICABLE
            //*  UNKNOWN
            if (condition(pnd_Data_Out_Pnd_O_Prvs_Address_City.notEquals("N/A") && pnd_Data_Out_Pnd_O_Prvs_Address_St_Prov.notEquals("UN")))                              //Natural: IF #DATA-OUT.#O-PRVS-ADDRESS-CITY NE 'N/A' AND #DATA-OUT.#O-PRVS-ADDRESS-ST-PROV NE 'UN'
            {
                pnd_City_State_Zip.setValue(DbsUtil.compress(pnd_Data_Out_Pnd_O_Prvs_Address_City, pnd_Data_Out_Pnd_O_Prvs_Address_St_Prov));                             //Natural: COMPRESS #DATA-OUT.#O-PRVS-ADDRESS-CITY #DATA-OUT.#O-PRVS-ADDRESS-ST-PROV INTO #CITY-STATE-ZIP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Data_Out_Pnd_O_Line_Of_Business.notEquals("D") && pnd_Data_Out_Pnd_O_Prvs_Address_Zip1_5.greater("00000") && DbsUtil.maskMatches(pnd_Data_Out_Pnd_O_Prvs_Address_Zip1_5, //Natural: IF #DATA-OUT.#O-LINE-OF-BUSINESS NE 'D' AND ( #DATA-OUT.#O-PRVS-ADDRESS-ZIP1-5 > '00000' AND #DATA-OUT.#O-PRVS-ADDRESS-ZIP1-5 = MASK ( 99999 ) )
                "99999")))
            {
                pnd_City_State_Zip.setValue(DbsUtil.compress(pnd_City_State_Zip, pnd_Data_Out_Pnd_O_Prvs_Address_Zip1_5));                                                //Natural: COMPRESS #CITY-STATE-ZIP #DATA-OUT.#O-PRVS-ADDRESS-ZIP1-5 INTO #CITY-STATE-ZIP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Data_Out_Pnd_O_Prvs_Address_Country.setValue("USA");                                                                                                      //Natural: ASSIGN #DATA-OUT.#O-PRVS-ADDRESS-COUNTRY := 'USA'
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet343 > 0))
        {
            short decideConditionsMet368 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #DATA-OUT.#O-PRVS-ADDRESS-LINE-1 = ' '
            if (condition(pnd_Data_Out_Pnd_O_Prvs_Address_Line_1.equals(" ")))
            {
                decideConditionsMet368++;
                pnd_Data_Out_Pnd_O_Prvs_Address_Line_1.setValue(pnd_City_State_Zip);                                                                                      //Natural: ASSIGN #DATA-OUT.#O-PRVS-ADDRESS-LINE-1 := #CITY-STATE-ZIP
            }                                                                                                                                                             //Natural: WHEN #DATA-OUT.#O-PRVS-ADDRESS-LINE-2 = ' '
            else if (condition(pnd_Data_Out_Pnd_O_Prvs_Address_Line_2.equals(" ")))
            {
                decideConditionsMet368++;
                pnd_Data_Out_Pnd_O_Prvs_Address_Line_2.setValue(pnd_City_State_Zip);                                                                                      //Natural: ASSIGN #DATA-OUT.#O-PRVS-ADDRESS-LINE-2 := #CITY-STATE-ZIP
            }                                                                                                                                                             //Natural: WHEN #DATA-OUT.#O-PRVS-ADDRESS-LINE-3 = ' '
            else if (condition(pnd_Data_Out_Pnd_O_Prvs_Address_Line_3.equals(" ")))
            {
                decideConditionsMet368++;
                pnd_Data_Out_Pnd_O_Prvs_Address_Line_3.setValue(pnd_City_State_Zip);                                                                                      //Natural: ASSIGN #DATA-OUT.#O-PRVS-ADDRESS-LINE-3 := #CITY-STATE-ZIP
            }                                                                                                                                                             //Natural: WHEN #DATA-OUT.#O-PRVS-ADDRESS-LINE-4 = ' '
            else if (condition(pnd_Data_Out_Pnd_O_Prvs_Address_Line_4.equals(" ")))
            {
                decideConditionsMet368++;
                pnd_Data_Out_Pnd_O_Prvs_Address_Line_4.setValue(pnd_City_State_Zip);                                                                                      //Natural: ASSIGN #DATA-OUT.#O-PRVS-ADDRESS-LINE-4 := #CITY-STATE-ZIP
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet343 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  PINE - TRANSLATE ALPHA TO NUMERIC
        if (condition(pnd_Data_Out_Pnd_O_Pin_Blanks.equals(" ") && pnd_Data_Out_Pnd_O_Pin_A12.notEquals(" ")))                                                            //Natural: IF #DATA-OUT.#O-PIN-BLANKS = ' ' AND #O-PIN-A12 NE ' '
        {
            pnd_Data_Out_Pnd_O_Pin_N12.setValue(pnd_Data_Out_Pnd_O_Pin_N7);                                                                                               //Natural: ASSIGN #DATA-OUT.#O-PIN-N12 := #DATA-OUT.#O-PIN-N7
        }                                                                                                                                                                 //Natural: END-IF
        //*  DERIVE-MAINFRAME-FIELDS
    }

    //
}
